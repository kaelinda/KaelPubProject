//
//  UIColor+Random.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/21.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Random)
+(UIColor *) randomColor;
@end
