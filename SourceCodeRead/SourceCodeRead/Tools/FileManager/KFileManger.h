//
//  KFileManger.h
//  MYWKWebviewDemo
//
//  Created by Kael on 2017/12/11.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KFileManger : NSObject

#pragma mark - **************** 路径获取

/**
 返回沙盒路径

 @return 沙盒路径
 */
+(NSString *)directoryHome;

/**
 返回沙盒document文件路径

 @return 沙盒document文件路径
 */
+(NSString *)documentPath;

/**
 返回沙盒libary文件路径

 @return 沙盒libary文件路径
 */
+(NSString *)libaryPath;

/**
 返回沙盒cache文件路径

 @return 沙盒cache文件路径
 */
+(NSString *)cachePath;

/**
 返回沙盒临时缓存路径

 @return 临时缓存文件路径
 */
+(NSString *)tmpPath;

/**
 返回APP工程内的文件资源路径

 @param name 文件名
 @param type 文件类型
 @return 文件资源路径
 */
+(NSString *)appSourceName:(NSString *)name andType:(NSString *)type;

/**
 创建文件夹

 @param path 文件路径
 @param dirName 文件夹名称
 @return 创建结果 是否成功
 */
+(BOOL)createDirWithPath:(NSString *)path andDirectoryName:(NSString *)dirName;

/**
 创建指定类型的文件到指定的路径

 @param path 文件路径
 @param fileName 文件名称
 @param type 文件类型
 @return 创建结果 是否成功
 */
+(BOOL)createFileToPath:(NSString *)path andFileName:(NSString *)fileName andType:(NSString *)type;

/**
 写入文件

 @param obj 对象
 @param path 路径
 @return 是否写入成功
 */
+(BOOL)writeFile:(NSObject *)obj toPath:(NSString *)path;

/**
 删除指定路径下的文件资源

 @param path 文件所在路径
 @return 删除文件操作的结果
 */
+(BOOL)deleteFileWithPath:(NSString *)path;

/**
 copy指定路径下的文件资源到指定路径下

 @param sourcePath 文件资源
 @param toPath 目标路径
 @return copy结果是否成功
 */
+ (BOOL)copyMissingFile:(NSString *)sourcePath toPath:(NSString *)toPath;

/**
 copy指定URL下的文件资源到指定URL路径下

 @param sourceURL 资源文件的URL
 @param toPath 目标URL路径
 @return copy结果 是否成功
 */
+(BOOL)copyMissFile:(NSURL *)sourceURL toPath:(NSURL *)toPath;


/**
 检测某一路径下 是否存在某个文件

 @param path 检测路径
 @param fileName 文件名
 @return 是否存在某个文件的结果
 */
+(BOOL)checkAtPath:(NSString *)path isExistsFile:(NSString *)fileName;

/**
 检测某个路径（文件）是否已存在

 @param fullPath 某文件的全路径
 @return 检测结果  是否存在
 */
+(BOOL)checkIsExistsFile:(NSString *)fullPath;


/**
 获取一个当前文件（不是文件夹）的URL 专门用于iOS8系统下WKWebView使用

 @param sourceURL 资源文件路径
 @return 适配iOS8之后的新资源文件路径
 */
+(NSURL *)fix8WKWebViewBugWithFileURL:(NSURL *)sourceURL;

/**
 获取一个适配iOS8系统下WKWebView的资源文件夹的新URL
 
 --FeedbackH5
 ----pages
 ------feedback.html
 ----css
 ----js
 ----images
 
 如果文件结构是这样的，得到的新资源文件夹URL，需要拼接下newURL+"pages/feedback.html"
 
 @param sourceURL 资源文件夹的原文件夹路径
 @param directoryName 资源文件夹的新名字
 @return 资源文件夹的新URL
 */
+(NSURL *)fix8WKWebViewBugWithDirectoryURL:(NSURL *)sourceURL andNewDirectoryName:(NSString *)directoryName;


@end
