//
//  KFileManger.m
//  MYWKWebviewDemo
//
//  Created by Kael on 2017/12/11.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "KFileManger.h"

@implementation KFileManger
#pragma mark - **************** 目录获取
+(NSString *)directoryHome{
    return  NSHomeDirectory();
}

+(NSString *)documentPath{
    
    //[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"app_home_doc: %@",documentsDirectory);
    
    return documentsDirectory;
}

//获取Library目录
+(NSString *)libaryPath{
    
    //[NSHomeDirectory() stringByAppendingPathComponent:@"Library"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *libraryDirectory = [paths objectAtIndex:0];
    
    NSLog(@"app_home_lib: %@",libraryDirectory);
    return libraryDirectory;
    
}
//获取Cache目录
+(NSString *)cachePath{
    
    NSArray *cacPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *cachePath = [cacPath objectAtIndex:0];
    
    NSLog(@"app_home_lib_cache: %@",cachePath);
    
    return cachePath;
}

//获取Tmp目录
+(NSString *)tmpPath{
    
    NSString *tmpDirectory = NSTemporaryDirectory();
    
    NSLog(@"app_home_tmp: %@",tmpDirectory);
    
    return tmpDirectory;
}

+(NSString *)appSourceName:(NSString *)name andType:(NSString *)type{
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:type];
    return path;
}



#pragma mark - **************** 文件操作
//创建文件夹
+(BOOL)createDirWithPath:(NSString *)path andDirectoryName:(NSString *)dirName{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [path stringByAppendingPathComponent:dirName];
    BOOL isSuccess = [fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
    if (isSuccess) {
        NSLog(@"文件件创建成功:%@",fullPath);
    }else
    {
        NSLog(@"文件件创建失败:%@",fullPath);
    }
    return isSuccess;
}


//创建文件
+(BOOL)createFileToPath:(NSString *)path andFileName:(NSString *)fileName andType:(NSString *)type{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fileFullPath = [fileName stringByAppendingPathComponent:type];
    NSString *filePath = [path stringByAppendingPathComponent:fileFullPath];
    
    BOOL res=[fileManager createFileAtPath:filePath contents:nil attributes:nil];
    
    if (res) {
        NSLog(@"文件创建成功: %@" ,filePath);
    }else {
        NSLog(@"文件创建失败: %@" ,filePath);
    }
    return res;
}

+(BOOL)writeFile:(NSObject *)obj toPath:(NSString *)path{
    BOOL res = NO;
//    res = [obj writ];
    NSString *str = @"";
    [str writeToFile:@"" atomically:YES encoding:NSUTF8StringEncoding error:nil];
    return res;
}

//删除文件
+(BOOL)deleteFileWithPath:(NSString *)path{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL res=[fileManager removeItemAtPath:path error:nil];
    
    BOOL isEx = [fileManager isExecutableFileAtPath:path];
    if (!isEx) {
        NSLog(@"文件不存在:%@",path);
        return NO;
    }
    if (res) {
        
        NSLog(@"文件删除成功:%@",path);
        
    }else{
        
        NSLog(@"文件删除失败:%@",path);
    }
    return res;
}

+ (BOOL)copyMissingFile:(NSString *)sourcePath toPath:(NSString *)toPath
{
    BOOL retVal = YES; // If the file already exists, we'll return success…
    NSString * finalLocation = [toPath stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];

    }else{
        NSLog(@"文件已存在");
        [[NSFileManager defaultManager] removeItemAtPath:finalLocation error:nil];
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];
    }
    
    if (retVal) {
        NSLog(@"文件拷贝成功");
    }else{
        NSLog(@"文件拷贝失败");
    }
    return retVal;
}

+(BOOL)copyMissFile:(NSURL *)sourceURL toPath:(NSURL *)toPath{
    
    BOOL retVal = YES;
    
    NSString * finalLocation = [toPath.absoluteString stringByAppendingPathComponent:[sourceURL.absoluteString lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtURL:sourceURL toURL:toPath error:nil];
    }else{
        NSLog(@"文件已存在");
        [[NSFileManager defaultManager] removeItemAtPath:finalLocation error:nil];
        retVal = [[NSFileManager defaultManager] copyItemAtURL:sourceURL toURL:toPath error:nil];

    }

    if (retVal) {
        NSLog(@"文件拷贝成功");
    }else{
        NSLog(@"文件拷贝失败");
    }
    return retVal;
}

+(BOOL)checkAtPath:(NSString *)path isExistsFile:(NSString *)fileName{
    BOOL res = YES;
    NSString *finalPath = [path stringByAppendingString:fileName];
    res = [[NSFileManager defaultManager] fileExistsAtPath:finalPath];
    return res;
}

+(BOOL)checkIsExistsFile:(NSString *)fullPath{
    BOOL res = YES;
    res = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
    return res;
}

+(NSURL *)fix8WKWebViewBugWithFileURL:(NSURL *)sourceURL{
    NSURL *resultURL;
    NSError *error = nil;
    if (!sourceURL.fileURL || ![sourceURL checkResourceIsReachableAndReturnError:&error]) {
        return nil;
    }
    // Create "/temp/www" directory
    NSFileManager *fileManager= [NSFileManager defaultManager];
    NSURL *temDirURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:@"www"];
    [fileManager createDirectoryAtURL:temDirURL withIntermediateDirectories:YES attributes:nil error:&error];
    
    resultURL = [temDirURL URLByAppendingPathComponent:sourceURL.lastPathComponent];
    // Now copy given file to the temp directory
    [fileManager removeItemAtURL:resultURL error:&error];
    [fileManager copyItemAtURL:sourceURL toURL:resultURL error:&error];
    return resultURL;
}

+(NSURL *)fix8WKWebViewBugWithDirectoryURL:(NSURL *)sourceURL andNewDirectoryName:(NSString *)directoryName{
    NSURL *resultURL;
    NSError *error = nil;
    if (!sourceURL.fileURL || ![sourceURL checkResourceIsReachableAndReturnError:&error]) {
        return nil;
    }
    // Create "/temp/www" directory
    NSFileManager *fileManager= [NSFileManager defaultManager];
    NSURL *temDirURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:@"www"];
    [fileManager createDirectoryAtURL:temDirURL withIntermediateDirectories:YES attributes:nil error:&error];
    resultURL = [temDirURL URLByAppendingPathComponent:directoryName];
    // Now copy given file to the temp directory
    [fileManager removeItemAtURL:resultURL error:&error];
    [fileManager copyItemAtURL:sourceURL toURL:resultURL error:&error];
    return resultURL;
}

@end
