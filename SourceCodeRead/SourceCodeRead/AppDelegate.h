//
//  AppDelegate.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/9.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

