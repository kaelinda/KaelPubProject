//
//  TableTreeViewController.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/11.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "TableTreeViewController.h"

#import "KLTreeView.h"

@interface TableTreeViewController ()

@property (nonatomic, strong) KLTreeView *treeView;
@property (nonatomic, strong) NSMutableArray *dataSource;


@property (nonatomic, strong) NSDictionary *resultData;


@end

@implementation TableTreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initBaseData];
    [self initBaseView];

}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    NSData *lexueData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"lexue" ofType:@"json"]];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:lexueData options:NSJSONReadingAllowFragments error:nil];
    _resultData =  [NSDictionary dictionaryWithDictionary:result];
    _dataSource = [NSMutableArray arrayWithArray:[_resultData objectForKey:@"rpbd"]];
}

- (void)initBaseView {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _treeView = [[KLTreeView alloc] initWithDataSource:_dataSource];
    _treeView.frame = CGRectMake(0, 64, 414, 736-64);
    _treeView.backgroundColor = [UIColor colorWithRed:0.60 green:0.85 blue:0.53 alpha:1.00];
    [self.view addSubview:_treeView];
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
