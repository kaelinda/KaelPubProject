//
//  ClassesDetailModel.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KLBaseModel.h"
#import "TeachcerModel.h"

@interface ClassesDetailModel : KLBaseModel

@property (nonatomic, copy) NSString *csid;

@property (nonatomic, copy) NSString *cstt;

@property (nonatomic, copy) NSString *tctp;

@property (nonatomic, copy) NSString *tstcut;

@property (nonatomic, copy) NSString *tsta;

@property (nonatomic, copy) NSString *tsen;

@property (nonatomic, copy) NSString *sltas;

@property (nonatomic, strong) NSArray *trbs;

@property (nonatomic, copy) NSString *rsty;
















@end
