//
//  KLBaseModel.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYModel.h"

@interface KLBaseModel : NSObject<YYModel>


@end
