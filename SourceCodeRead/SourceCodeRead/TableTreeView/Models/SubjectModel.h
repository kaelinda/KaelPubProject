//
//  SubjectModel.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KLBaseModel.h"
#import "ClassesItemsModel.h"
@interface SubjectModel : KLBaseModel

@property (nonatomic, copy) NSString *stid;

@property (nonatomic, copy) NSString *stme;

@property (nonatomic, strong) NSArray *tklst;






@end
