//
//  ClassesModel.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KLBaseModel.h"
#import "SubjectModel.h"
#import "YYModel.h"

@interface ClassesModel :KLBaseModel

@property (nonatomic, strong) NSNumber *rpco;

@property (nonatomic, strong) NSNumber *ver;

@property (nonatomic, strong) NSNumber *tsrp;

@property (nonatomic, strong) NSArray *rpbd;


@end
