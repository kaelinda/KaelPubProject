//
//  ClassesItemsModel.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KLBaseModel.h"
#import "ClassesDetailModel.h"


@interface ClassesItemsModel : KLBaseModel

@property (nonatomic, copy) NSString *trid;

@property (nonatomic, copy) NSString *tpid;

@property (nonatomic, copy) NSString *trnme;

@property (nonatomic, copy) NSString *crct;

@property (nonatomic, strong) NSArray *crsws;

@property (nonatomic, strong) NSArray *tklst;









@end
