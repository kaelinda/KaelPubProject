//
//  KLTreeView.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/11.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "KLTreeView.h"
#import "ClassesCell.h"
static NSString *identify = @"cellIdentify";

@interface KLTreeView ()<UITableViewDelegate, UITableViewDataSource>
{
    
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSource;


@end

@implementation KLTreeView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseView];
    }
    return self;
}

-(instancetype)initWithDataSource:(NSMutableArray *)dataSource{
    self = [self init];
    if (self) {
        _dataSource = [dataSource copy];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {

}

- (void)initBaseView {
    
    [self addSubview:[UIView new]];
    _tableView = [self tableView];
    [self addSubview:_tableView];
    
    [_tableView registerClass:[ClassesCell class] forCellReuseIdentifier:identify];
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    if (_tableView) {
        _tableView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    }
}
#pragma mark - **************** UI准备

#pragma mark lazy loading...
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    return _tableView;
}

#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSMutableDictionary *sectionItem = [_dataSource objectAtIndex:section];
//    NSArray *tklst = [sectionItem objectForKey:@"tklst"];
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ClassesCell *cell = [tableView dequeueReusableCellWithIdentifier:identify forIndexPath:indexPath];
//    UITableViewCell *cell = [tableView nib];

    if (!cell) {
        cell = [[ClassesCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        cell.contentView.backgroundColor = [UIColor lightGrayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
//    cell.contentView.backgroundColor = [UIColor randomColor];
    
    cell.typeNum = indexPath.row;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    ClassesCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    奇数 返回高度为 50；偶数返回高度为 150
//    if (cell.typeNum % 2 == 1) {
//        return 50;
//    }
//    return 150;
//    if (indexPath.row == 0) {
//        return 70*indexPath.row;
//    }
    // 这里的高度应该交给Model去计算
    return  60*indexPath.row;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 100;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   
    NSMutableDictionary *item = [_dataSource objectAtIndex:section];
    NSString *stme = [item objectForKey:@"stme"];
    
    UIView *sectionHeaderView = [UIView new];
    sectionHeaderView.backgroundColor = [UIColor colorWithRed:0.63 green:0.80 blue:0.82 alpha:1.00];
    sectionHeaderView.frame = CGRectMake(0, 0, 414, 50);
    
    CGSize textSize = CGSizeMake(100, 40);
    UILabel *sectionText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
    sectionText.text = stme;
    sectionText.backgroundColor = [UIColor clearColor];
    
    sectionText.center = CGPointMake(textSize.width/2 + 10, sectionHeaderView.center.y);
    [sectionHeaderView addSubview:sectionText];
    
    return sectionHeaderView;
}










/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
