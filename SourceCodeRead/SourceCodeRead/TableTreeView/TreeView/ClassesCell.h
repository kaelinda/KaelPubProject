//
//  ClassesCell.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContainerTabView.h"
@interface ClassesCell : UITableViewCell

@property (nonatomic, assign) NSInteger typeNum;

@end
