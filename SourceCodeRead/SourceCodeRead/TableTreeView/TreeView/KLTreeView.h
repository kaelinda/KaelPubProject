//
//  KLTreeView.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/11.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Random.h"

@interface KLTreeView : UIView

/**
 用数据源的方式初始化视图

 @param dataSource 数据源
 @return 视图实例
 */
-(instancetype)initWithDataSource:(NSMutableArray *)dataSource;


@end
