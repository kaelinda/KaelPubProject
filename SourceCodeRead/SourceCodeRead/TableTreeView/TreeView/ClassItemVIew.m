//
//  ClassItemVIew.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "ClassItemVIew.h"

@implementation ClassItemVIew
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
}

- (void)initBaseView {
    [self initContentView];
}

-(void)initContentView{
    UILabel *collectionsClassLabel = [[UILabel alloc] init];
    collectionsClassLabel.text = @"四级目录";
    collectionsClassLabel.font = [UIFont systemFontOfSize:11];
    collectionsClassLabel.frame = CGRectMake(0, 0, 414, 30);
    [self addSubview:collectionsClassLabel];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
