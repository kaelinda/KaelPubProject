//
//  ContainerTabView.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "ContainerTabView.h"

@implementation ContainerTabView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
}

- (void)initBaseView {
//    [self initContainerWithNum:3];
}
-(void)initContainerWithNum:(NSInteger)num{
    
    // 移除之前的视图
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:[ReusedView class]]) {
            [subview removeFromSuperview];
        }
    }
    
    // 准备添加新的视图
    CGSize itemSize = CGSizeMake(414, 40);
    
    for (int i=0; i<num; i++) {
        ReusedView *itemView = [[ReusedView alloc] init];
        itemView.hasChildLeval = NO;
        itemView.backgroundColor = [UIColor whiteColor];
        itemView.frame = CGRectMake(0, i*itemSize.height, itemSize.width, itemSize.height);
        [self addSubview:itemView];

        if (num%2 == 0) {
            [itemView setHasChildLeval:YES];
        }else{
            [itemView setHasChildLeval:NO];
        }
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
