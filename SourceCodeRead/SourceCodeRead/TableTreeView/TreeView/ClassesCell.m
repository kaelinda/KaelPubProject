//
//  ClassesCell.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "ClassesCell.h"
#import "ReusedView.h"
@implementation ClassesCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initBaseData];

        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
}

- (void)initBaseView {
    UILabel *secondLab = [[UILabel alloc] init];
    secondLab.frame = CGRectMake(0, 0, 414, 20);
    secondLab.font = [UIFont systemFontOfSize:10];
    secondLab.text = @"二级目录";
    [self.contentView addSubview:secondLab];
 
    self.contentView.backgroundColor = [UIColor clearColor];
    
}

-(void)setTypeNum:(NSInteger)typeNum{
    CGRect frame = CGRectMake(0, 20, 414, 40*typeNum);
    
    ContainerTabView *container = [[ContainerTabView alloc] init];
    container.frameChangeHandle = ^(CGRect frame) {
        NSLog(@"容器的 frame 变了");
    };
    container.frame = frame;
    container.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:container];
    [container initContainerWithNum:typeNum];

}



@end
