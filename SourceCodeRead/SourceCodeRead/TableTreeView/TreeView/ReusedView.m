//
//  ReusedView.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "ReusedView.h"

@implementation ReusedView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
}

- (void)initBaseView {
   
    
}
-(void)setHasChildLeval:(BOOL)hasChildLeval{
    _hasChildLeval = hasChildLeval;
    if (_hasChildLeval) {
        [self initCollectionLabel];
    }else{
        [self initClassItemView];
    }
}
-(void)initCollectionLabel{
    UILabel *collectionsClassLabel = [[UILabel alloc] init];
    collectionsClassLabel.text = @"三级目录";
    collectionsClassLabel.font = [UIFont systemFontOfSize:11];
    collectionsClassLabel.frame = CGRectMake(30, 0, 414, 20);
    collectionsClassLabel.backgroundColor = [UIColor randomColor];
    [self addSubview:collectionsClassLabel];

}
-(void) initClassItemView{
    ClassItemVIew *itemView = [[ClassItemVIew alloc] init];
    itemView.frame = CGRectMake(40, 20, 414, 20);
    itemView.backgroundColor = [UIColor randomColor];
    [self addSubview:itemView];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
