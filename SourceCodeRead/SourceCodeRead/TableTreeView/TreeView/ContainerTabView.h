//
//  ContainerTabView.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReusedView.h"
#import "UIColor+Random.h"

@interface ContainerTabView : UIView

@property (nonatomic, copy) void(^ frameChangeHandle)(CGRect);

-(void)initContainerWithNum:(NSInteger)num;

@end
