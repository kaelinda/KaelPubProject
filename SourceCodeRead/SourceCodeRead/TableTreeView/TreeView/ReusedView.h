//
//  ReusedView.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/12.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ClassItemVIew.h"

@interface ReusedView : UIView

@property (nonatomic, strong) ReusedView *childView;

@property (nonatomic, assign) BOOL hasChildLeval;


@end
