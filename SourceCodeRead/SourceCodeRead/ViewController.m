//
//  ViewController.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/9.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "ViewController.h"
#import "KChainsCoding.h"
#import "TableTreeViewController.h"
#import "UIView+FrameChains.h"

NSString *tableTreeView = @"tableTreeView";
NSString *SDWebImage = @"SDWebImage";
NSString *ReactiveCocoa = @"ReactiveCocoa";
NSString *YYModel = @"YYModel";
NSString *AFNetworking = @"AFNetworking";
NSString *BlocksKit = @"BlocksKit";
NSString *CocoaAsyncSocket = @"CocoaAsyncSocket";
NSString *Masonry = @"Masonry";
NSString *MultiQueue = @"MultiQueue";




@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    UITableView *_tableView;
    NSMutableArray *_dataSource;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    self.title = @"model list";
    [self.view addSubview:[UIView new]];
    
    [self initDatasource];
    _tableView = [self tableView];
    [self.view addSubview:_tableView];
    
}
- (void) initDatasource{
    _dataSource = [NSMutableArray arrayWithObjects:tableTreeView,SDWebImage,ReactiveCocoa,YYModel,AFNetworking,BlocksKit,CocoaAsyncSocket,Masonry,MultiQueue, nil];
//    [_dataSource addObject:tableTreeView];
}
#pragma mark lazy loading...
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, 414, 736-64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = [UIColor redColor];
        
    }
    return _tableView;
}

#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
//        cell.contentView.backgroundColor = [UIColor colorWithRed:0.60 green:0.85 blue:0.53 alpha:1.00];
    }
    cell.textLabel.text = _dataSource[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeStr = _dataSource[indexPath.row];
    // 树状视图
    if ([typeStr isEqualToString:tableTreeView]) {
        TableTreeViewController *treeViewVC = [[TableTreeViewController alloc] init];
        treeViewVC.title = tableTreeView;
        [self.navigationController pushViewController:treeViewVC animated:YES];
    }
    NSLog(@"点击：%@",typeStr);
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    return 44 + indexPath.row * 10;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
