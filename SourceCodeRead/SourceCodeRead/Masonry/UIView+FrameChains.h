//
//  UIView+FrameChains.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/22.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef UIView *(^SetFrameValueBlock)(CGFloat);
typedef UIView *(^SetViewSizeBlock)(CGSize);
typedef UIView *(^SetViewOriginBlock)(CGPoint);



@interface UIView (FrameChains)

#pragma mark - **************** 赋值方法

/**
 链式设置视图frame
 */
-(UIView *(^)(CGRect))kframe;

/**
 链式设置视图宽度
 */
-(SetFrameValueBlock)kwidth;

/**
 链式设置视图高度
 */
-(SetFrameValueBlock)kheight;

/**
 链式设置视图位置origin.x
 */
-(SetFrameValueBlock)koriginX;

/**
 链式设置视图位置origin.y
 */
-(SetFrameValueBlock)koriginY;

/**
 链式设置视图尺寸size
 */
-(SetViewSizeBlock)ksize;

/**
 链式设置视图位置origin
 */
-(SetViewOriginBlock)korigin;

/**
 链式设置视图中心位置 center
 */
-(SetViewOriginBlock)kcenter;

#pragma mark - **************** 取值方法
-(CGFloat)viewWidth;

-(CGFloat)viewHeight;

-(CGSize)viewSize;

-(CGPoint)viewOrigin;

-(CGFloat)viewOriginX;

-(CGFloat)viewOriginY;

-(CGPoint)viewCenter;



@end
