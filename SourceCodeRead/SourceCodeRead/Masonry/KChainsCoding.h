//
//  KChainsCoding.h
//  SourceCodeRead
//
//  Created by Kael on 2018/2/22.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KChainsCoding : NSObject

@property (nonatomic, assign) CGFloat count;

-(KChainsCoding *(^)(CGFloat))add;

-(KChainsCoding *(^)(CGFloat))del;


@end
