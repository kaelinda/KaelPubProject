//
//  KChainsCoding.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/22.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "KChainsCoding.h"

@implementation KChainsCoding


-(KChainsCoding *(^)(CGFloat))add{

    return ^(CGFloat num){
        _count = _count + num;
        return self;
    };
}

-(KChainsCoding *(^)(CGFloat))del{
    return ^(CGFloat num){
        _count = _count - num;
        return self;
    };
}


@end
