//
//  UIView+FrameChains.m
//  SourceCodeRead
//
//  Created by Kael on 2018/2/22.
//  Copyright © 2018年 Kael. All rights reserved.
//

#import "UIView+FrameChains.h"

@implementation UIView (FrameChains)

-(UIView *(^)(CGRect))kframe{
    return ^(CGRect frame){
        self.frame = frame;
        return self;
    };
    
}

-(SetFrameValueBlock)kwidth{
    CGPoint origin = self.frame.origin;
    CGSize size = self.frame.size;
    
    return ^(CGFloat width){
        self.frame = CGRectMake(origin.x, origin.y, width, size.height);
        return self;
    };
}

-(SetFrameValueBlock)kheight{
    CGPoint origin = self.frame.origin;
    CGSize size = self.frame.size;
    return ^(CGFloat height){
        self.frame = CGRectMake(origin.x, origin.y, size.width, height);
        return self;
    };
}

-(SetFrameValueBlock)koriginX{
    CGPoint origin = self.frame.origin;
    CGSize size = self.frame.size;
    return ^(CGFloat originX){
        self.frame = CGRectMake(originX, origin.y, size.width, size.height);
        return self;
    };
}

-(SetFrameValueBlock)koriginY{
    CGPoint origin = self.frame.origin;
    CGSize size = self.frame.size;
    return ^(CGFloat originY){
        self.frame = CGRectMake(origin.x, originY, size.width, size.height);
        return self;
    };
}

-(SetViewSizeBlock)ksize{
    CGPoint origin = self.frame.origin;
    return ^(CGSize size){
        self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
        return self;
    };
}

-(SetViewOriginBlock)korigin{
    CGSize size = self.frame.size;
    return ^(CGPoint origin){
        self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
        return self;
    };
}

-(SetViewOriginBlock)kcenter{

    return ^(CGPoint center){
        self.center = center;
        return self;
    };
}


-(CGSize)viewSize{
    [self layoutIfNeeded];
    return self.frame.size;
}
-(CGPoint)viewOrigin{
    [self layoutIfNeeded];
    return self.frame.origin;
}
-(CGFloat)viewWidth{
   return self.viewSize.width;
}
-(CGFloat)viewHeight{
    return self.viewSize.height;
}
-(CGFloat)viewOriginX{
    return self.viewOrigin.x;
}
-(CGFloat)viewOriginY{
    return self.viewOrigin.y;
}
-(CGPoint)viewCenter{
    [self layoutIfNeeded];
    return self.center;
}


@end
