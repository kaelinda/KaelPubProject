//
//  NotificationService.m
//  NotificationService
//
//  Created by lailibo on 2017/5/8.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "NotificationService.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max

@interface NotificationService ()<NSURLSessionDelegate,NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
@property (nonatomic, strong) NSString *myAttemptFliePath;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [[UNMutableNotificationContent alloc]init];
    self.bestAttemptContent = [request.content mutableCopy];
    
    
    NSLog(@"我收到的推送%@",request.content);
    // Modify the notification content here...
    
//    NSURLSession * session = [NSURLSession sharedSession];
    
//    NSDictionary *dic = [[NSDictionary alloc]init];
//    
//    di
    NSString * attachmentPath = self.bestAttemptContent.userInfo[@"imageAbsouluteString"];
    //if exist image gif video audio
    if (attachmentPath.length) {
        //download
        [self backgGroundSessionResumeWithAttachmentUrl:attachmentPath];
        //
        //[self normalSessionResumeWtihAttachmentUrl:attachmentPath];
    }else{
        self.contentHandler(self.bestAttemptContent);
    }

}

// 普通下载 下载 image gif  audio
- (void)normalSessionResumeWtihAttachmentUrl:(NSString *)attachment
{
    NSURLSession * session = [NSURLSession sharedSession];
    //if exist image gif audio
    //download
    NSURLSessionTask * task = [session dataTaskWithURL:[NSURL URLWithString:attachment] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            NSString * localPath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(),response.suggestedFilename];
            
            if ([data writeToFile:localPath atomically:YES]) {
                UNNotificationAttachment * attachment = [UNNotificationAttachment attachmentWithIdentifier:@"myAttachment" URL:[NSURL fileURLWithPath:localPath] options:nil error:nil];
                self.bestAttemptContent.attachments = @[attachment];
            }
        }
        self.contentHandler(self.bestAttemptContent);
    }];
    [task resume];
}


// 后台下载下载 下载 image gif  audio video 如果推送下载视频需要使用后台下载，大小不能超过50M/
- (void)backgGroundSessionResumeWithAttachmentUrl:(NSString *)attachment
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.sh.pangea.healthTV.NotificationService"];
    // 此处必须设置Appgroup否则下载会失败。group.com.sh.pangea.healthTV
    configuration.sharedContainerIdentifier = @"group.com.sh.pangea.healthTV";
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:[NSURL URLWithString:attachment]];
    [task resume];
}

//下载任务完成,这个方法在下载完成时触发，它包含了已经完成下载任务得 Session Task,Download Task和一个指向临时下载文件得文件路径
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSString * localPath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(),downloadTask.response.suggestedFilename];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    if([fileManager fileExistsAtPath:localPath])
    {
        NSLog([fileManager removeItemAtPath:localPath error:&error]?@"deleted":@"not deleted");
    }
    if(![fileManager fileExistsAtPath:localPath])
    {
        if ([fileManager moveItemAtPath:[location path] toPath:localPath error:&error]) {
            self.myAttemptFliePath = localPath;
            self.bestAttemptContent.title = [NSString stringWithFormat:@"%@", self.bestAttemptContent.title];
//            self.bestAttemptContent.subtitle = [NSString stringWithFormat:@"%@",@""];
            self.bestAttemptContent.body = [NSString stringWithFormat:@"%@",self.bestAttemptContent.body];
            UNNotificationAttachment * attachment = [UNNotificationAttachment attachmentWithIdentifier:@"myAttachment" URL:[NSURL fileURLWithPath:self.myAttemptFliePath] options:nil error:nil];
            self.bestAttemptContent.attachments = @[attachment];
            self.contentHandler(self.bestAttemptContent);
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(nullable NSError *)error
{
    if (error==nil) {
        NSLog(@"任务: %@ 成功完成", task);
        [session invalidateAndCancel];
        session = nil;
        
    }else {
        self.bestAttemptContent.title = [NSString stringWithFormat:@"%@ fBackRemoteActive", self.bestAttemptContent.title];
        self.bestAttemptContent.subtitle = @"fBackActive";
        self.contentHandler(self.bestAttemptContent);
        
        NSLog(@"任务: %@ 发生错误: %@", task, [error localizedDescription]);
    }
}


- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}
#endif

@end
