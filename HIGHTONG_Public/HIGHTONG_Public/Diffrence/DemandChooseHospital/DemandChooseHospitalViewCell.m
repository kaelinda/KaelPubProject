//
//  DemandChooseHospitalViewCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/1/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DemandChooseHospitalViewCell.h"
#import "Masonry.h"

@implementation DemandChooseHospitalViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *BaseView = [self contentView];
        [BaseView removeAllSubviews];

        _hospitalIcon = [UIImageView new];
        [BaseView addSubview:_hospitalIcon];
        [_hospitalIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(BaseView.mas_left).offset(20*kRateSize);
            make.centerY.equalTo(BaseView);
            make.width.equalTo(@(30*kRateSize));
            make.height.equalTo(@(30*kRateSize));
        }];
        _hospitalIcon.contentMode = UIViewContentModeScaleAspectFit;
        
        _hospitalName = [UILabel new];
        _hospitalName.numberOfLines= 1;
        _hospitalName.textAlignment = NSTextAlignmentLeft;

        [BaseView addSubview:_hospitalName];
        [_hospitalName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_hospitalIcon.mas_right).offset(10*kRateSize);
            make.right.equalTo(BaseView.mas_right).offset(-10*kRateSize);
            make.top.equalTo(BaseView.mas_top).offset(2*kRateSize);
            make.bottom.equalTo(BaseView.mas_bottom).offset(2*kRateSize);
        }];
        _hospitalName.font = [UIFont systemFontOfSize:18];
        _hospitalName.textColor = UIColorFromRGB(0x333333);

        
    }
  
    return self;
    
}

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
