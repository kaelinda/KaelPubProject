//
//  DemandWYYControl.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/1/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "HospitalViewController.h"

@interface DemandChooseHospitalControl : HT_FatherViewController

@property(nonatomic,assign)HospitalType HospitalType; //选择类型
@property (nonatomic,copy)NSString *YiYuanTitle;


@end
