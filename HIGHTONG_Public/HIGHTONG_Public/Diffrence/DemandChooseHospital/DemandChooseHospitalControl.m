//
//  DemandWYYControl.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/1/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DemandChooseHospitalControl.h"
#import "DemandChooseHospitalViewCell.h"
#import "HTRequest.h"
#import "DemandSecondViewcontrol.h"
#import "CustomAlertView.h"
#import "MJRefresh.h"
#import <CoreLocation/CoreLocation.h>


@interface DemandChooseHospitalControl ()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,CLLocationManagerDelegate,CustomAlertViewDelegate>
{
   
//    NSMutableDictionary *_categoryStringDIC;//选择的category字典

    HTRequest *request;//
    
    NSInteger pageNo;//请求页数

}

@property (nonatomic,strong) UITableView *tableView_base;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableDictionary *resultDic;
@property (nonatomic,strong) NSMutableDictionary *hospitalDic;

@property (nonatomic,strong) CustomAlertView *custom;
@property (nonatomic,strong) CustomAlertView *custom1;

@end

@implementation DemandChooseHospitalControl


- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];

    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
//    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
//    title.backgroundColor = [UIColor clearColor];
//    title.text = @"选择医院";
//    [title setTextColor:[UIColor whiteColor]];
//    [self setNaviBarSearchView:title];
    if ([self.YiYuanTitle isEqualToString:@"更换医院"]) {
        [self setNaviBarTitle:@"更换医院"];
    }
    else
    {
        [self setNaviBarTitle:@"选择医院"];
    }

    self.tableView_base.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(15, 0, kDeviceWidth-15, 0.5*kRateSize)];
    self.tableView_base.tableFooterView.backgroundColor = App_line_color;

    if (![self.YiYuanTitle isEqualToString:@"更换医院"]) {
        if (![CLLocationManager locationServicesEnabled] || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)) {
            
            //    UIAlertView *_custom = [[UIAlertView alloc]initWithTitle:@"定位服务未开启" message:@"请在系统设置中开启定位服务\n（设置>隐私>定位服务>开启屏聚江西）" delegate:self cancelButtonTitle:@"我知道了" otherButtonTitles:nil, nil];
            //        [_custom show];
            //        _custom=[[CustomAlertView alloc] initWithMessage:@"是否清空缓存数据？" andButtonNum:1 withDelegate:self];
            _custom = [[CustomAlertView alloc]initWithTitle:@"定位服务未开启" andRemandMessage:@"请在系统设置中开启定位服务\n（设置>隐私>定位服务>开启屏聚江西）" andButtonNum:1 andCancelButtonMessage:@"我知道了" andOtherButtonMessage:nil withDelegate:self];
            _custom.tag = 200;
            [_custom show];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAlert) name:@"APP_WILL_ENTER_BACKGROUND" object:nil];
    }

    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRegion) name:@"GET_REGIONCODE_WITH_LOACTIONINFO" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    pageNo = 1;
    
    self.dataArray = [NSMutableArray array];
    self.hospitalDic = [NSMutableDictionary dictionary];
    self.resultDic = [NSMutableDictionary dictionary];
    
    [self baseSetup];
    
}

- (void)baseSetup{
    self.tableView_base = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, kDeviceWidth, KDeviceHeight - 44)];
    self.tableView_base.delegate  = self;
    self.tableView_base.dataSource = self;
    self.tableView_base.backgroundColor = App_background_color;
    [self.view addSubview:self.tableView_base];
    
    [self refreshUISetup];
    
    [self refreshYJHospitalTableCell];

    
    [self showLoading];
    
}

- (void)refreshUISetup
{
    
    self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    
}


#pragma  mark- 请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    switch (status) {
        case 0:
        {
            
            if ([type isEqualToString:EPG_REGION_LIST]) {
                NSLog(@"EPG_REGION_LIST返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDIC = result;
                NSLog(@"%@",resDIC);
                NSArray *resArray = [resDIC objectForKey:@"regionList"];
                NSLog(@"%@",resArray);
                self.dataArray.array = resArray;
                
                [self hideLoading];

                [self.tableView_base reloadData];
            }
            
            if ([type isEqualToString:EPG_REGION_INFO]) {
                NSLog(@"EPG_REGION_INFO返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDic = result;
                self.resultDic.dictionary = resDic;
                NSLog(@"%@",self.resultDic);
                
                NSDictionary *tempDic = [resDic objectForKey:@"region"];
                self.hospitalDic.dictionary = tempDic;
                NSLog(@"%@",self.hospitalDic);
                
                [self pushControl];

            }
            
            if ([type isEqualToString:EPG_FIND_REGION]) {
                NSLog(@"EPG_FIND_REGION返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDic = result;
                
                if([[resDic objectForKey:@"ret"] integerValue] == 0){
                    self.resultDic.dictionary = resDic;
                    NSLog(@"%@",self.resultDic);
                    
                    NSDictionary *tempDic = [resDic objectForKey:@"region"];
                    self.hospitalDic.dictionary = tempDic;
                    NSLog(@"%@",self.hospitalDic);
                    if (![self.YiYuanTitle isEqualToString:@"更换医院"]) {
                        [self alertviews];
                    }
                    
                }
            }
            
            if ([type isEqualToString:YJ_INTRODUCE_LIST]) {
                NSLog(@"YJ_INTRODUCE_LIST返回数据: %ld result:%@  type:%@",status,result,type );
                if ([[result objectForKey:@"ret"] integerValue] == 0){
                    
                    pageNo++;//如果请求成功，则页码自增
                    
                    NSDictionary *resDIC = result;
                    
                    [self.dataArray addObjectsFromArray:[resDIC objectForKey:@"list"]];

                    [self.tableView_base reloadData];

                }
    
                [self hideLoading];
           
            }
        
                break;
            
            default:
            {

                NSLog(@"请求失败码%ld %@",status,type);
                
                if ([type isEqualToString:EPG_REGION_LIST]) {
                    
                [AlertLabel AlertInfoWithText:@"当前网络不好，请重新进入页面加载" andWith:self.view withLocationTag:1];
                    
                }

            }
                break;
        }
    }
}

#pragma mark - tabelView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return self.dataArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50*kRateSize;;

}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *Progranidentifier = @"Progranidentifier";
        DemandChooseHospitalViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
        if (!cell) {
            cell = [[DemandChooseHospitalViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
        }
    
        cell.backgroundColor = App_background_color;
    
        NSDictionary *hospital = [self.dataArray objectAtIndex:indexPath.row];

        NSString *titile = [hospital objectForKey:@"name"];
        NSString *url = [hospital objectForKey:@"picLink"];
    if (is_Separate_API) {
        titile = [hospital objectForKey:@"name"];
        url = [hospital objectForKey:@"imageUrl"];
    }
    
    cell.hospitalName.text = titile;

    [cell.hospitalIcon sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(url)] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];
    
        return cell;
}

#pragma mark - cell点击后执行
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",indexPath);
    
    if (is_Separate_API) {
        NSString *hospitalCode = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"instanceCode"];
        
        NSString *hospitalName = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"name"];
        
        [[NSUserDefaults standardUserDefaults] setObject:hospitalCode forKey:@"InstanceCode"];
        
        [[NSUserDefaults standardUserDefaults] setObject:hospitalName forKey:@"hospitalName"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"tableviewcell所在的code ＝ %@",hospitalCode);

    }else{
    
    NSString *hospitalCode = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"code"];
    
    NSString *hospitalName = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    [[NSUserDefaults standardUserDefaults] setObject:hospitalCode forKey:@"InstanceCode"];
    
    [[NSUserDefaults standardUserDefaults] setObject:hospitalName forKey:@"hospitalName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"tableviewcell所在的code ＝ %@",hospitalCode);
    }
    
    [self pushControl];
    
//    request = [[HTRequest alloc]initWithDelegate:self];
//
//    [request EPGGetRegionInfoWithRegionCode:hospitalCode andTimeout:10];
}

#pragma mark - 界面跳转
- (void)pushControl
{
    [self removeMovieControllerNotification];
    
    if ([self.YiYuanTitle isEqualToString:@"更换医院"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        
        switch (self.HospitalType) {
            case HospitalTypeYiYuan:
            {
                HospitalViewController *hospi = [[HospitalViewController alloc]init];
                hospi.HospitalType = HospitalTypeYiYuan;
                hospi.YiYuanTitle = self.YiYuanTitle;
                [self.navigationController pushViewController:hospi animated:YES];
            }
                
                break;
                
            case HospitalTypeYiVideo:
            {
                HospitalViewController *hospi = [[HospitalViewController alloc]init];
                hospi.HospitalType = HospitalTypeYiVideo;
                hospi.YiYuanTitle = self.YiYuanTitle;
                [self.navigationController pushViewController:hospi animated:YES];
            }
                
                break;
                
            case HospitalTypeYiZhuanJia:
            {
                HospitalViewController *hospi = [[HospitalViewController alloc]init];
                hospi.HospitalType = HospitalTypeYiZhuanJia;
                hospi.YiYuanTitle = self.YiYuanTitle;
                [self.navigationController pushViewController:hospi animated:YES];
            }
                
                break;
                
            case HospitalTypeYiZhuShou:
            {
                HospitalViewController *hospi = [[HospitalViewController alloc]init];
                hospi.HospitalType = HospitalTypeYiZhuShou;
                hospi.YiYuanTitle = self.YiYuanTitle;
                [self.navigationController pushViewController:hospi animated:YES];
            }
                
                break;
                
            default:
                break;
        }
    }
    
    
//    if (self.HospitalType == HospitalTypeYiYuan) {
//        
//        HospitalViewController *hospi = [[HospitalViewController alloc]init];
//        hospi.HospitalType = HospitalTypeYiYuan;
//        [self.navigationController pushViewController:hospi animated:YES];
//
//    }else
//    {
//        
//        HospitalViewController *hospi = [[HospitalViewController alloc]init];
//        hospi.HospitalType = HospitalTypeYiVideo;
//        [self.navigationController pushViewController:hospi animated:YES];
//        
//    }
}

#pragma  mark - navigation上的按钮
- (void)Back
{
    [self removeMovieControllerNotification];
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"返回点播首页");
}

#pragma mark -定位成功后所弹的提示框
- (void)alertviews
{

    NSString *message = [NSString stringWithFormat:@"定位地址为：%@",[self.hospitalDic objectForKey:@"name"]];

    NSLog(@"%@",message);
    
    if (!_custom1) {
        _custom1 = [[CustomAlertView alloc]initWithTitle:@"已完成定位" andRemandMessage:message andButtonNum:2 andCancelButtonMessage:@"继续选择医院" andOtherButtonMessage:@"跳转至该医院" withDelegate:self];
        _custom1.tag = 100;
        [_custom1 show];
    }

    
}

#pragma mark -接收到定位成功的通知后，调用请求医院信息数据
- (void)loadRegion
{
    request = [[HTRequest alloc]initWithDelegate:self];
    if (is_Separate_API) {
        NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
        NSString *longitude = @"114.989792";//0.000000
        NSString *latitude  = @"27.116745";//0.000000
        if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
            longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
            latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
        }
        
        [request YJIntroduceNearbyWithLongitude:longitude withLatitude:latitude];
    }else{
        [request EPGFindRegionWithTimeout:10];//
        
    }
}

- (void)loadData
{
    
    [self.tableView_base.mj_footer endRefreshing];
    
    [self refreshYJHospitalTableCell];

}

- (void)refreshYJHospitalTableCell
{
    request = [[HTRequest alloc]initWithDelegate:self];
    if (is_Separate_API) {
        [request YJIntroduceListWithAreaId:@"" withPageNo:[NSString stringWithFormat: @"%ld", (long)pageNo] withPageSize:@"100"];
    }else{
        [request EPGGetRegionListWithTimeout:10];
    }
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 100)
    {//清除缓存alert
        if(buttonIndex==0)
        {
            NSString *hospitalCode = [self.hospitalDic objectForKey:@"code"];
            
            [[NSUserDefaults standardUserDefaults] setObject:hospitalCode forKey:@"InstanceCode"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if(self.custom1)
            {
                [self.custom1 resignKeyWindow];
                _custom1 = nil;
            }
            
            [self pushControl];
        }
        else
        {
            if(self.custom1)
            {
                [self.custom1 resignKeyWindow];
                _custom1 = nil;
            }
            return;
        }
    }
    else
    {
        if (self.custom) {
            
            [self.custom resignKeyWindow];
            _custom = nil;
        }
    }
    
    return;
    
}

- (void)removeAlert
{    
    if (self.custom) {
        [self.custom hide];
        
        [self.custom resignKeyWindow];
        _custom = nil;
    }
}

- (void)removeMovieControllerNotification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"APP_WILL_ENTER_BACKGROUND" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"GET_REGIONCODE_WITH_LOACTIONINFO" object:nil];
}


#pragma mark - 工具

- (void)showLoading
{
    [self showCustomeHUD];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}
- (void)hideLoading
{
    [self hiddenCustomHUD];
//    [MBProgressHUD hideAllHUDsForView:self.view  animated:YES];
}
@end
