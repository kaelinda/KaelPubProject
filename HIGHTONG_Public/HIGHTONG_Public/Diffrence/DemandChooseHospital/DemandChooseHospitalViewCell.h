//
//  DemandChooseHospitalViewCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/1/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemandChooseHospitalViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *hospitalName; //医院名称
@property (nonatomic,strong) UIImageView *hospitalIcon; //医院图标

@end
