//
//  JKChannelDetailHeaderView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JKChannelDetailHeaderView.h"

#define kCycleScrollViewHeight (188*kDeviceRate)

@interface JKChannelDetailHeaderView()
{

    CGFloat kWidth;
    CGFloat kHeight;

}
@end

@implementation JKChannelDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];

    if (self) {
        //初始化数据
        [self initData];
        //添加子视图
        [self initSubViews];
        
    }
    
    return self;
}

#pragma  mark - 初始化数据和子视图
-(void)initData{

    kWidth = self.frame.size.width;
    kHeight = self.frame.size.height;

    _cycleTypes = [[NSMutableArray alloc]init];
    _cycleParames= [[NSMutableArray alloc]init];
    _cycleImages = [[NSMutableArray alloc]init];
    _cycleLinks= [[NSMutableArray alloc]init];
    _cycleTitles = [[NSMutableArray alloc]init];

}
-(void)initSubViews{
    
    //初始化轮播图
    [self initCycleScrollView];
    //初始化选择器界面
    [self initFilterView];

}

-(void)initCycleScrollView{
    
    WS(wself);
//    /** 轮播文字label字体颜色 */
//    @property (nonatomic, strong) UIColor *titleLabelTextColor;
//    
//    /** 轮播文字label字体大小 */
//    @property (nonatomic, strong) UIFont  *titleLabelTextFont;
//    
//    /** 轮播文字label背景颜色 */
//    @property (nonatomic, strong) UIColor *titleLabelBackgroundColor;
//    
//    /** 轮播文字label高度 */
//    @property (nonatomic, assign) CGFloat titleLabelHeight;
    CGRect cycleRect = CGRectMake(0, 0, kDeviceWidth, kCycleScrollViewHeight);
    
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:cycleRect delegate:self placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
    _cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    _cycleScrollView.titlesGroup = _cycleTitles;
    _cycleScrollView.titleLabelTextFont = HT_FONT_THIRD;
    _cycleScrollView.titleLabelTextColor = HT_COLOR_FONT_INVERSE;
    _cycleScrollView.titleLabelBackgroundColor = HT_COLOR_ALERTCUSTOM;
    _cycleScrollView.titleLabelHeight = 40*kDeviceRate;
    _cycleScrollView.pageDotImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
    _cycleScrollView.currentPageDotImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"]; // 自定义分页控件小圆标颜色
    _cycleScrollView.autoScrollTimeInterval = PAGESCROLLTIME;
    [self addSubview:_cycleScrollView];
    
    [_cycleScrollView setBackgroundColor:App_white_color];
    __block NSMutableArray *images = _cycleLinks;
    __block NSMutableArray *imagesData = _cycleImagesData;
    __block NSMutableArray *types = _cycleTypes;
    __block NSMutableArray *parames = _cycleParames;
//    __block NSMutableArray *ids ;
    _cycleScrollView.clickItemOperationBlock = ^(NSInteger index) {
        NSLog(@">>>>>  %ld", (long)index);
        NSString *eventProgram = [imagesData objectAtIndex:index];
        if (isEmptyStringOrNilOrNull(eventProgram)) {
            eventProgram = @"";
        }
        
        if (wself.didSelectedImagesBclok) {
            wself.didSelectedImagesBclok(index,[images objectAtIndex:index],eventProgram,[types objectAtIndex:index],[parames objectAtIndex:index],[wself.cycleTitles objectAtIndex:index]);
        }
    };
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _cycleScrollView.imageURLStringsGroup = _cycleLinks;
    });

}

-(void)initFilterView{
    
    WS(wself);
    _filterView = [[JKChannelDetailFilterView alloc] init];
    _filterView.frame = CGRectMake(0, kCycleScrollViewHeight, _cycleScrollView.frame.size.width, 0*kRateSize);
    [_filterView setBackgroundColor:App_white_color];
    _filterView.clipsToBounds = YES;
    [self addSubview:_filterView];
    
    __block UIView *filter = _filterView;
    __block UIView *cycleView = _cycleScrollView;
    
    _filterView.filtViewRectChangedBlock = ^(CGRect filterFrame){
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.35 animations:^{
                filter.frame = filterFrame;
                CGFloat lineHigh = filterFrame.size.height > 0 ? 10 : 0;
                
                wself.frame = CGRectMake(wself.frame.origin.x, wself.frame.origin.y, wself.frame.size.width, filter.frame.size.height+cycleView.frame.size.height + lineHigh);
                if (wself.headerViewReactChangedBlock) {
                    wself.headerViewReactChangedBlock(wself.frame);
                }

            }];
        });
    };
    
    _filterView.filtBtnSelected = ^(NSInteger index){
        NSLog(@"------>> 我选中了第%ld个选择器按钮",index);
        NSString *filterID;
        NSString *filterName;
        
        if (index >= wself.filtersData.count) {
            filterID= @"";
            filterName= @"";
        }else{
        
            filterID= [[wself.filtersData objectAtIndex:index] objectForKey:@"id"];
            filterName= [[wself.filtersData objectAtIndex:index] objectForKey:@"name"];
        }
        
        
        if (wself.headerFilterSelectedAtIndex) {
            wself.headerFilterSelectedAtIndex(index,filterID,filterName);
        }
    };
    
}

#pragma mark - reload 子视图
-(void)reloadHeaderResult:(NSDictionary *)headerResult{

    _headerResult = [NSMutableDictionary dictionaryWithDictionary:headerResult];

    NSArray *images = [[headerResult objectForKey:@"info"] objectForKey:@"carousel"];
    _cycleImagesData = [NSMutableArray arrayWithArray:images];
    [self reloadCycleImagesData:_cycleImagesData];
    
    if ([headerResult objectForKey:@"info"]) {
        NSDictionary *filters = [[headerResult objectForKey:@"info"] objectForKey:@"filter"];
        [self reloadHeaderFiltersViewWith:filters];
    }

}

-(void)reloadHeaderCycleImagesData:(NSArray *)imagesData{
    
    [self reloadCycleImagesData:imagesData];

}

-(void)reloadCycleImagesData:(NSArray *)imagesData{
//这里需要把 imageData 转换成 _cycleLinks 和 _cycleTitles
    NSLog(@"---->> 刷新数据的");
    [_cycleLinks removeAllObjects];
    [_cycleTitles removeAllObjects];

    for (int i = 0; i<imagesData.count; i++) {
       
        NSString *imageLink = [[imagesData objectAtIndex:i] objectForKey:@"image"];
        NSString *title = [[imagesData objectAtIndex:i] objectForKey:@"title"];
        NSString *type = [[imagesData objectAtIndex:i] objectForKey:@"eventType"];
        NSString *param = [[NSString alloc]init];
        
        if ([[imagesData objectAtIndex:i] objectForKey:@"eventParam"]) {
             param = [[imagesData objectAtIndex:i] objectForKey:@"eventParam"];
        }
        else
        {
            param = @"";
        }

        [_cycleLinks addObject:NSStringPM_IMGFormat(imageLink)];
        [_cycleTitles addObject:title];
        [_cycleTypes addObject:type];
        [_cycleParames addObject:param];

    }
    
    [_cycleScrollView setTitlesGroup:_cycleTitles];
    [_cycleScrollView setImageURLStringsGroup:_cycleLinks];
}

-(void)reloadHeaderFiltersViewWith:(NSDictionary *)filtersDic{

    NSLog(@"------>> 刷新选择器");
    NSMutableArray *filters = [NSMutableArray array];
    NSArray *itemArray = [filtersDic objectForKey:@"items"];
    _filtersData = [NSMutableArray arrayWithArray:itemArray];
    for (int i = 0; i<itemArray.count; i++) {
        
        [filters addObject:[[_filtersData objectAtIndex:i] objectForKey:@"name"]];
        
    }
    
    [_filterView loadFilters:filters];
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    
//    [AlertLabel AlertInfoWithText:@"点我干哈 找屎啊！！" andWith:self.cycleScrollView withLocationTag:0];
}










@end
