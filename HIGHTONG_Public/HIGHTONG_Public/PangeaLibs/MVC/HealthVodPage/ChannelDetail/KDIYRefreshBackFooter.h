//
//  KDIYRefreshBackFooter.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/7/12.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>

#define foot_StateRefreshing @"正在刷新,请稍后..."
#define foot_StatePulling @"松开刷新数据"
#define foot_StateIdle @"继续上拉刷新数据"
#define foot_StateNoMoreData @"后面没有内容了哦!"

@interface KDIYRefreshBackFooter : MJRefreshBackFooter

@end
