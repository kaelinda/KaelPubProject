//
//  JKChannelDetailFilterView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JKChannelDetailFilterView.h"

#define kFilterBtn_Border_Normal_Color [[UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.00] CGColor]

@interface JKChannelDetailFilterView()
{

    CGFloat kWidth;
    CGFloat kHeight;
    
}

@end

@implementation JKChannelDetailFilterView

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    kWidth = frame.size.width;
    kHeight = frame.size.height;

}
-(instancetype)init{

    self = [super init];
    if (self) {
       
        [self initData];
        
        [self initSubViews];
    }
    return self;
}

-(void)initData{

    kWidth = 0;
    kHeight = 0;
//    _filters = [NSMutableArray arrayWithObjects:@"糖尿病",@"高血压",@"哮喘",@"老年痴呆",@"中风",@"慢性咽炎",@"癌症", nil];
    _filters = [[NSMutableArray alloc]init];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reloadSubView];
    });
}

-(void)initSubViews{


    
    
}

-(void)reloadSubView{

    [self removeAllSubviews];

//    if (_filters.count) {
    
        CGFloat kCornerRadius = 13;
        
        CGSize btnSize = CGSizeMake((self.frame.size.width - 41)/4, kCornerRadius * 2);
        
        NSInteger lineNum = (_filters.count)/4 + 1;
        kHeight = 20 + lineNum * 5 + lineNum * btnSize.height;
        //    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, kWidth, kHeight);
        
        if (_filtViewRectChangedBlock) {
            self.filtViewRectChangedBlock (CGRectMake(self.frame.origin.x, self.frame.origin.y, kWidth, _filters.count>0 ? kHeight : 0));
        }
    
    if (_filters.count == 0) {
        return;
    }

        for (int i=0; i<=_filters.count; i++) {
            CGRect btnRect = CGRectMake(10 + (i%4) * (btnSize.width + 5), 10 + (i/4) * (btnSize.height+8), btnSize.width, btnSize.height);
            UIButton *button;
            if (i<_filters.count) {
                
                NSString *filteTitle = [_filters objectAtIndex:i];
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setTitle:filteTitle forState:UIControlStateNormal];
                [button setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
                [button setTitleColor:App_selected_color forState:UIControlStateSelected];
                [button setTitleColor:App_selected_color forState:UIControlStateHighlighted];
                button.frame = btnRect;
                [self addSubview:button];
                
                
            }else if(i == _filters.count){
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setImage:[UIImage imageNamed:@"healthtv_icon_screen"] forState:UIControlStateNormal];
                button.frame = btnRect;
                [self addSubview:button];
                
            }
            
            [button setBackgroundImage:[UIImage imageNamed:@"healthtv_filter_item"] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"healthtv_filter_item_pressed"] forState:UIControlStateHighlighted];
            
            button.showsTouchWhenHighlighted = NO;
            [button.titleLabel setFont:App_font(11)];
            button.tag = 100+i;//避免tag == 0；的其他子视图 当然 其实也不存在
            [button addTarget:self action:@selector(filtBtnTouchDown:) forControlEvents:UIControlEventTouchDown];
            [button addTarget:self action:@selector(filtBtnTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(filtBtnTouchDragOutside:) forControlEvents:UIControlEventTouchDragOutside];
            
        }
//    }
    
    
    
}


-(void)filtBtnTouchDragOutside:(UIButton *)button{
    NSLog(@"你干嘛拖拽按钮啊？");
    NSInteger index = button.tag - 100;
    if (index < _filters.count) {
        
    }else if (index == _filters.count){
        
        
    }
    
}
-(void)filtBtnTouchDown:(UIButton *)button{
    NSLog(@"filter 按下按钮");

}

-(void)filtBtnTouchUpInside:(UIButton *)button{
     NSLog(@"filter 按钮抬起");
    NSInteger index = button.tag - 100;
    if (index < _filters.count) {
        
    }else if (index == _filters.count){
    
    
    }
    
    if (self.filtBtnSelected) {
        self.filtBtnSelected(index);
    }

}

-(void)loadFilters:(NSArray *)filters{

    _filters = [NSMutableArray arrayWithArray:filters];
    
//    if (_filters.count>0) {
        [self reloadSubView];
//    }else{
//        return;
//    }

}

@end
