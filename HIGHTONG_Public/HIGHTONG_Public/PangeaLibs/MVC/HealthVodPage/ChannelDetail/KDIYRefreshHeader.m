//
//  KDIYRefreshHeader.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "KDIYRefreshHeader.h"
#import "LoadingView.h"
@interface KDIYRefreshHeader(){


}
@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIImageView *loadingImage;
@property (assign, nonatomic) BOOL hasAnimation;
@property (weak, nonatomic) UIImageView *arrowImage;

@end



@implementation KDIYRefreshHeader



-(void)startAnimation{
    
    if (_hasAnimation) {
        
    }else{
        [self.loadingImage.layer addAnimation:_animation forKey:@"rotate-layer"];
        _hasAnimation = YES;
    }


}


-(void)endAnimation{
    if (_hasAnimation) {
        [self.loadingImage.layer removeAnimationForKey:@"rotate-layer"];
        _hasAnimation = NO;
    }else{
    
    }

}


#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    // 设置控件的高度
    self.mj_h = 50;
    _hasAnimation = NO;
    
    [super prepare];
 
//**************************************
    // 对Y轴进行旋转（指定Z轴的话，就和UIView的动画一样绕中心旋转）
    _animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    // 设定动画选项
    _animation.duration = 2; // 持续时间
    _animation.repeatCount = CGFLOAT_MAX; // 重复次数
    // 设定旋转角度
    _animation.fromValue = [NSNumber numberWithFloat:0.0]; // 起始角度
    _animation.toValue = [NSNumber numberWithFloat: 2*M_PI]; // 终止角度
    
//**************************************
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithRed:1.0 green:0.5 blue:0.0 alpha:1.0];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
//************************************
    UIImageView *loading = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    loading.frame = CGRectMake(0, 0, 30, 30);
    [self addSubview:loading];
    self.loadingImage = loading;
//************************************
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_common_refresh_down"]];
    arrowView.frame = CGRectMake(100, 0, 19, 31);
    [arrowView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:arrowView];
    
    self.arrowImage = arrowView;
}

-(void)upArrow{
    CGAffineTransform upTransform = CGAffineTransformIdentity;
    upTransform = CGAffineTransformRotate(upTransform, M_PI);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.arrowImage.transform = upTransform;
    }];
}
-(void)downArrow{
    
    CGAffineTransform downTransform = CGAffineTransformIdentity;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.arrowImage.transform = downTransform;
    }];
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews
{
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    self.loadingImage.center = CGPointMake(self.mj_w*0.5-100, self.mj_h * 0.5);
    self.arrowImage.center = self.loadingImage.center;
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change
{
    [super scrollViewContentSizeDidChange:change];
    
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change
{
    [super scrollViewPanStateDidChange:change];
    
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            self.label.text = loading_StateIdle;
            [self endAnimation];
            self.loadingImage.hidden = YES;
                //**********
            self.arrowImage.hidden = NO;
            [self downArrow];
            break;
        case MJRefreshStatePulling:
            self.label.text = loading_StatePulling;
            [self endAnimation];
            self.loadingImage.hidden = YES;
                //**********
            self.arrowImage.hidden = NO;
            [self upArrow];
            break;
        case MJRefreshStateRefreshing:
            self.label.text = loading_StateRefreshing;
            [self startAnimation];
            self.loadingImage.hidden = NO;
                //**********
            self.arrowImage.hidden = YES;
            [self downArrow];
            break;
        case MJRefreshStateWillRefresh:
            self.label.text = @"4加载数据中(刷新中)";

            break;
        case MJRefreshStateNoMoreData:
         
            break;
        default:
            break;
    }
}

#pragma mark 监听拖拽比例（控件被拖出来的比例）
- (void)setPullingPercent:(CGFloat)pullingPercent
{
    [super setPullingPercent:pullingPercent];
    
    // 1.0 0.5 0.0
    // 0.5 0.0 0.5

//    self.label.textColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    self.label.textColor = [UIColor blackColor];

}
@end
