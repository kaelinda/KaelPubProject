//
//  JKChannelDetailFilterView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKChannelDetailFilterView : UIView

/**
 *  @author Kael
 *
 *  @brief 筛选条件
 */
@property (nonatomic,strong) NSMutableArray *filters;

/**
 *  @author Kael
 *
 *  @brief 监控 filtersView 的 frame 变化
 */
@property (nonatomic,copy) void (^ filtViewRectChangedBlock)(CGRect filterFrame);

/**
 *  @author Kael
 *
 *  @brief 监控选择器按钮的选中状态
 */
@property (nonatomic,copy) void (^ filtBtnSelected)(NSInteger index);

/**
 *  @author Kael
 *
 *  @brief 重载filters的选择条件
 *  @param filters 选择条件
 */
-(void)loadFilters:(NSArray *)filters;



@end
