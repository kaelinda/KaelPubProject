//
//  JKChannelDetailViewController.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/29.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "UIButton_Block.h"
#import "JKChannelDetailHeaderView.h"
#import "YJProgramListCell.h"
#import "MJRefresh.h"
#import "KDIYRefreshHeader.h"
#import "TMPDataManager.h"

@interface JKChannelDetailViewController : HT_FatherViewController<UITableViewDelegate,UITableViewDataSource>


/**
 *  @author Kael
 *
 *  @brief 频道详情界面的标题
 */
@property (nonatomic,strong) NSString *detailTitle;

/**
 *  @author Kael
 *
 *  @brief 该页面频道的 ID
 */
@property (nonatomic,strong) NSString *pageChannelID;

/**
 *  @author Kael
 *
 *  @brief 该页面频道的名称
 */
@property (nonatomic,strong) NSString *pageChannelName;

/**
 *  @author Kael
 *
 *  @brief 该页面过滤器大类的 ID
 */
@property (nonatomic,strong) NSString *selectID;

/**
 *  @author Kael
 *
 *  @brief 主tableView
 */
@property (nonatomic,strong) UITableView *contentTableView;

/**
 *  @author Kael
 *
 *  @brief 主tableView的 Header视图
 */
@property (nonatomic,strong) JKChannelDetailHeaderView *headerView;

/**
 *  @author Kael
 *
 *  @brief 子频道类型的数据源
 */
@property (nonatomic,strong) NSMutableArray *childrenArray;

/**
 *  @author Kael
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar;
    
/**
*  @author Kael
*
*  @brief 是否需要导航条
*/
@property (nonatomic,assign) BOOL isNeedTabBar;

/**
 *  @author Kael
 *
 *  @brief 节目信息请求结束后 回调该方法 夫控制器可用这个方法来获取每个自控制器的展示数据
 */
@property (nonatomic,copy) void (^ pageCahnnelInfo)(NSDictionary * channelInfo,NSString *pageChannelID);


/**
 *  @author Kael
 *
 *  @brief 频道详情信息字典 用来返回给父控制器的
 */
@property (nonatomic,strong) NSMutableDictionary *channelInfoDic;

/**
 *  @author Kael
 *
 *  @brief 重新请求数据
 *  @param ChannelID 频道ID
 */
-(void)reloadChannelDataWith:(NSString *)ChannelID;

/**
 *  @author Kael
 *
 *  @brief 直接加载现有数据
 *  @param channelInfo 频道信息
 *  @param chaildList  子频道列表
 *  @param hasChild    是否含有子频道
 */
-(void)reloadContentViewDataWith:(NSDictionary *)channelInfo andChildChannelList:(NSArray *)chaildList andHasChild:(NSString *)hasChild;

/**
 *  @author Kael
 *
 *  @brief 频道ID路径
 */
@property (nonatomic,strong) NSString *TrackId;
/**
 *  @author Kael
 *
 *  @brief 频道名称路径
 */
@property (nonatomic,strong) NSString *TrackName;
@end
