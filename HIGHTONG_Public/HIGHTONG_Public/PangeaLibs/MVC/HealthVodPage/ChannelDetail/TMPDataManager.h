//
//  TMPDataManager.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/7/4.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMPDataManager : NSObject

/**
 *  @author Kael
 *
 *  @brief 得到单利对象
 */
+(TMPDataManager *)shareDataManager;



- (NSDictionary *)GetChannelInfo;

- (NSDictionary *)GetChannelVideo;

- (NSDictionary *)GetChildren;

- (NSDictionary *)GetQueryProgram;




@end
