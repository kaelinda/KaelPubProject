//
//  JKChannelDetailViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/29.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JKChannelDetailViewController.h"

#import "PolyMedicFilterViewController.h"

#import "YJHealthVideoSecondViewcontrol.h"

#import "PolyMedicFilterDetailViewController.h"

#import "HTRequest.h"

#import "VTMagic.h"

#import "AboutUSViewController.h"

#import "HTVideoAVPlayerVC.h"

#import "HealthVodCollectionModel.h"

#import "JKYSHHotHealthyVodCell.h"

#define kSection_Header_Height (40*kDeviceRate)
#define kSection_Footer_Height (35*kDeviceRate)
#define kSection_GrayLine_Height (5*kDeviceRate)
#define kSection_Footer_MaxNum (4)

@interface JKChannelDetailViewController ()<HTRequestDelegate>

@property (nonatomic, strong) NSMutableArray *menuNameList;//频道名称列表

@property (nonatomic, strong) NSMutableArray *slideHealthVodList;//广告列表

@property (nonatomic, strong) NSMutableArray *menuList;//频道列表

@property (nonatomic,strong) UIButton *moreHealthVodBtn;//更多频道按钮

@property (nonatomic,copy)NSString *hasChild;//是否有子频道

/**
 *  首页类型
 */
@property (nonatomic,assign) HomePageType pagetype;

@end


@implementation JKChannelDetailViewController
-(instancetype)init{
    self = [super init];
    if (self) {
        _isNeedNavBar = YES;
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
}

-(void)reloadChannelDataWith:(NSString *)ChannelID{

    self.pageChannelID = ChannelID;
    NSString *homeFlag = [self.pageChannelID isEqualToString:@"0"] ? @"1":@"2";
    
    NSString *instanceCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    instanceCode = [instanceCode length]?instanceCode:@"";
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request YJHealthyVodChannelInfoWithChannelId:self.pageChannelID withInstanceCode:instanceCode withHomeFlag:homeFlag withMaxCount:@"0"];
}

-(void)reloadContentViewDataWith:(NSDictionary *)channelInfo andChildChannelList:(NSArray *)chaildList andHasChild:(NSString *)hasChild{
    
    self.hasChild = hasChild;
    
    self.slideHealthVodList = [NSMutableArray array];
    
    NSMutableArray *tempArr = [[channelInfo objectForKey:@"info"]objectForKey:@"carousel"];
    
    for (NSMutableDictionary *dic in tempArr) {
        
        if ([[dic objectForKey:@"eventType"]isEqualToString:@"play"]) {
            
            [dic setObject:[dic objectForKey:@"title"] forKey:@"name"];
            
            [dic setObject:[dic objectForKey:@"eventParam"] forKey:@"id"];
            
            [self.slideHealthVodList addObject:dic];
        }
    }

    [_headerView reloadHeaderResult:channelInfo];
    
    _childrenArray = chaildList.copy;

    [_contentTableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_contentTableView.mj_header endRefreshing];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:App_white_color];

    //初始化导航条
    [self setupNavigationBar];
    //初始化 tableview
    [self setupTableView];

    if (_pageChannelID != nil) {
        //有ID那就需要刷新界面了
//        [self loadNewChannelData];
    }
        [self reloadChannelDataWith:self.pageChannelID];
}

-(void)setupNavigationBar{
    WS(wself);
    [self setNaviBarTitle:@"频道详情"];
    if (_detailTitle.length>0) {
        [self setNaviBarTitle:_detailTitle];
    }

    UIButton_Block *backBtn = [[UIButton_Block alloc] init];
    [backBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
    backBtn.Click = ^(UIButton_Block *btn,NSString *title){
        [wself.navigationController popViewControllerAnimated:YES];
    };
    [self setNaviBarLeftBtn:backBtn];
    
    [self hideNaviBar:!_isNeedNavBar];
}

-(void)setupTableView{

    UIView *nilView = [UIView new];
    nilView.frame = CGRectZero;
    [self.view addSubview:nilView];
    
    _childrenArray = [NSMutableArray array];

    self.pagetype = kApp_HomePageType;
    
    CGFloat JkChannelTabBarHeight;
    
    JkChannelTabBarHeight = _isNeedTabBar ? 48 : 0;
    
    CGRect tableViewReact;
    
    switch (self.pagetype) {
        case kHomePageType_JKTV_v1_1:
            
            tableViewReact = CGRectMake(0,_isNeedNavBar ? 64+kTopSlimSafeSpace : 0, kDeviceWidth, _isNeedNavBar ? (KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace) : (KDeviceHeight-64-44-kTopSlimSafeSpace-kBottomSafeSpace));
            
            break;
            
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        case kHomePageType_JKTV_V1_5:
            
            tableViewReact = CGRectMake(0,_isNeedNavBar ? 64+kTopSlimSafeSpace : 0, kDeviceWidth, _isNeedNavBar ? (KDeviceHeight-64 - JkChannelTabBarHeight-kTopSlimSafeSpace-kBottomSafeSpace) : (KDeviceHeight-64-44 - JkChannelTabBarHeight-kTopSlimSafeSpace-kBottomSafeSpace));
            
            break;
        default:
             tableViewReact = CGRectMake(0,_isNeedNavBar ? 64+kTopSlimSafeSpace : 0, kDeviceWidth, _isNeedNavBar ? (KDeviceHeight-64 - JkChannelTabBarHeight-kTopSlimSafeSpace-kBottomSafeSpace) : (KDeviceHeight-64-44 - JkChannelTabBarHeight-kTopSlimSafeSpace-kBottomSafeSpace));
            break;
    }
    
    _contentTableView = [[UITableView alloc] initWithFrame:tableViewReact style:UITableViewStyleGrouped];
    _contentTableView.delegate = self;
    _contentTableView.dataSource = self;
    _contentTableView.showsVerticalScrollIndicator = NO;
    _contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _contentTableView.backgroundColor = App_background_color;
    [self.view addSubview:_contentTableView];
    
    _contentTableView.tableHeaderView = [self setupHeaderView];
    
    _contentTableView.delaysContentTouches = NO;
    
    KDIYRefreshHeader *diyHeader = [KDIYRefreshHeader headerWithRefreshingBlock:^{
        
        [self reloadChannelDataWith:self.pageChannelID];
        
    //请求数据 并刷新界面
//        [wself loadNewChannelData];

    }];
    
    _contentTableView.mj_header = diyHeader;
    
    return;
}

-(UIView *)setupHeaderView{

    WS(wself);
    
    CGRect headerReact = CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, 188*kDeviceRate);
    
    if (_headerView == nil) {
        _headerView = [[JKChannelDetailHeaderView alloc] initWithFrame:headerReact];
        [_headerView setBackgroundColor:App_background_color];
        _headerView.didSelectedImagesBclok = ^(NSInteger index,NSString *webLink,NSString *channelID,NSString *types,NSString *parames,NSString *title){
            
            NSLog(@"\n------>> 我选中了第 %ld 张图 \n------>> webLink: %@ \n------>> channelID: %@\n------>> types: %@\n------>> parames: %@------>> title: %@",index,webLink,channelID,types,parames,title);
            if ([types isEqualToString:@"none"]) {
                NSLog(@"嘿嘿嘿，我啥也不干");
            }
            else if ([types isEqualToString:@"play"]) {
                NSLog(@"嘿嘿嘿，我要进播放器了");
        
                HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
                
                HTVC.recommendArray = wself.slideHealthVodList;

                HTVC.contentID = parames;
                HTVC.titleStr = title;
                
                
                NSMutableDictionary *polyMedic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"description",@"",@"duration",parames,@"id",@"",@"imageUrl",title,@"name",@"",@"releaseDate",@"",@"releaseYear",webLink,@"imageLink", nil];
                HTVC.polyMedicDic = polyMedic;
                
                
                NSString *ENtype = [NSString stringWithFormat:@"4"];
                
                NSString *TrackId = [[NSString alloc]init];
                
                NSString *TrackName = [[NSString alloc]init];
                
                //判断是否是由首页进入的一级界面
                if (self.TrackId.length == 0) {
                    TrackId = [NSString stringWithFormat:@"1,%@",self.pageChannelID];
                    
                    TrackName = [NSString stringWithFormat:@"健康,%@",self.pageChannelName];
                }
                else
                {
                    TrackId = [NSString stringWithFormat:@"%@",self.TrackId];
                    
                    TrackName = [NSString stringWithFormat:@"%@",self.TrackName];
                }
                
                NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:HTVC.contentID,@"itemId",HTVC.titleStr,@"name",ENtype,@"ENtype",TrackId,@"TrackId",TrackName,@"TrackName", nil];
                
                NSLog(@"asdasdasdasd-------%@",paramDic);
                
                HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
                [HTVC setPolyModel:healthVodModel];
                [wself.navigationController pushViewController:HTVC animated:NO];

            }
            else if ([types isEqualToString:@"link"]) {
                NSLog(@"嘿嘿嘿，我要跳到网页了");
                
                AboutUSViewController *webView = [[AboutUSViewController alloc] init];
                webView.urlLink = parames;

                [wself.navigationController pushViewController:webView animated:NO];

            }
            
        };
        
        __block UIView *header = _headerView;
        __block UITableView *tableView = _contentTableView;
        _headerView.headerViewReactChangedBlock = ^(CGRect frame){
            
            header.frame = frame;
            tableView.tableHeaderView = header;
            
        };
        
        _headerView.headerFilterSelectedAtIndex = ^(NSInteger index,NSString *filterID,NSString *filterName){
            NSLog(@"成功接收到第%ld个按钮的响应事件 filterID: %@    filterName : %@",index,filterID,filterName);
            
            if (filterID.length == 0) {
                PolyMedicFilterViewController *filterVC = [[PolyMedicFilterViewController alloc]init];
                filterVC.polyChannelID = wself.pageChannelID;
                
                NSString *TrackId = [[NSString alloc]init];
                
                NSString *TrackName = [[NSString alloc]init];
                
                //判断是否是由首页进入的一级界面(由于没有真实数据，很可能出问题！！！！！)
                if (self.TrackId.length == 0) {
                    TrackId = [NSString stringWithFormat:@"1,%@",filterVC.polyChannelID];
                    
                    TrackName = [NSString stringWithFormat:@"健康,%@",filterName];
                }
                else
                {
                    TrackId = [NSString stringWithFormat:@"%@",self.TrackId];
                    
                    TrackName = [NSString stringWithFormat:@"%@",self.TrackName];
                }
                
                filterVC.TrackId = TrackId;
                
                filterVC.TrackName = TrackName;
                
                [wself.navigationController pushViewController:filterVC animated:YES];
            }
            else
            {
            
                YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
                yj.categoryID = wself.pageChannelID;
                yj.name = filterName;
                yj.selectID = wself.selectID;
                yj.itemID = filterID;
                
                NSLog(@"categoryID%@,name%@,selectID%@,itemID%@",yj.categoryID,yj.name,yj.selectID,yj.itemID);
                
                NSString *TrackId = [[NSString alloc]init];
                
                NSString *TrackName = [[NSString alloc]init];
                
                //判断是否是由首页进入的一级界面
                if (self.TrackId.length == 0) {
                    TrackId = [NSString stringWithFormat:@"1,%@,%@",yj.categoryID,yj.itemID];
                    
                    TrackName = [NSString stringWithFormat:@"健康,%@,%@",self.pageChannelName,yj.name];
                }
                else
                {
                    TrackId = [NSString stringWithFormat:@"%@,%@",self.TrackId,yj.itemID];
                    
                    TrackName = [NSString stringWithFormat:@"%@,%@",self.TrackName,yj.name];
                }
                
                yj.TrackId = TrackId;
                
                yj.TrackName = TrackName;
                
                [wself.navigationController pushViewController:yj  animated:YES];
            }
        };
    }
    
    return _headerView;
}

-(void)loadNewChannelData{

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_contentTableView.mj_header endRefreshing];
    });

}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_CHANNELINFO]) {
        
        NSLog(@"YJ_HEALTHYVOD_CHANNELINFO返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            self.slideHealthVodList = [NSMutableArray array];
            
            NSMutableArray *tempArr = [[result objectForKey:@"info"]objectForKey:@"carousel"];
            
            for (NSMutableDictionary *dic in tempArr) {
                
                if ([[dic objectForKey:@"eventType"]isEqualToString:@"play"]) {
                    
                    [dic setObject:[dic objectForKey:@"title"] forKey:@"name"];
                    
                    [dic setObject:[dic objectForKey:@"eventParam"] forKey:@"id"];
                    
                    [self.slideHealthVodList addObject:dic];
                }
                    
            }
            
            [_headerView reloadHeaderResult:result];
            
            self.pageChannelName = [[result objectForKey:@"info"]objectForKey:@"name"];
            
            NSLog(@"本页面的大名是----%@",self.pageChannelName);
            
            NSArray *channelList = [result objectForKey:@"list"];
            
            NSMutableArray *channelIdList = [[NSMutableArray alloc]init];
            
            for (NSDictionary *dic in channelList) {
                
                NSString *temp = [dic objectForKey:@"id"];
                
                [channelIdList addObject:temp];
            }
            
            if ([[[result objectForKey:@"info"]objectForKey:@"filter"]objectForKey:@"id"] != nil) {
                
                //获取过滤器大类ID
                self.selectID = [[[result objectForKey:@"info"]objectForKey:@"filter"]objectForKey:@"id"];
                
            }
            
            _hasChild = [NSString stringWithFormat:@"%@",[[result objectForKey:@"info"]objectForKey:@"hasChild"]];
            
            
            //存储是否有子频道和header所需数据
            _channelInfoDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:result,@"result", _hasChild,@"channelHasChlid", nil];
            
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            if ([_hasChild isEqualToString:@"0"]) {//如果有子频道，则请求下级频道节目，没有子频道，则请求本级节目
                [request YJHealthyVodProgramListWithChannelId:self.pageChannelID withPageNo:@"1" withPageSize:@"50" withKeyword:nil withFilter:nil];
            }
            else
            {
                [request YJHealthyVodChannelProgramListWithChannelId:channelIdList withMaxCount:@"4"];
            }
        }else
        {
            
        }
        
    }
    if ([type isEqualToString:YJ_HEALTHYVOD_CHANNELPROGRAMLIST]) {
        
        NSLog(@"YJ_HEALTHYVOD_CHANNELPROGRAMLIST返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
           
            _childrenArray = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
            
            //存储节目列表信息
            [_channelInfoDic setObject:_childrenArray forKey:@"childVideoList"];

            [_contentTableView reloadData];
            
            [_contentTableView.mj_header endRefreshing];

        }
        //如果 父控制器 想获取界面的数据 可以通过这个方法获取
        if (self.pageCahnnelInfo) {
            self.pageCahnnelInfo(_channelInfoDic,self.pageChannelID);
        }

    }
    if ([type isEqualToString:YJ_HEALTHYVOD_PROGRAMLIST]) {
        
        NSLog(@"YJ_HEALTHYVOD_PROGRAMLIST返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            _childrenArray = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
            
            //存储节目列表信息
            [_channelInfoDic setObject:_childrenArray forKey:@"childVideoList"];

            [_contentTableView reloadData];
            
            [_contentTableView.mj_header endRefreshing];
        }
        //如果 父控制器 想获取界面的数据 可以通过这个方法获取
        if (self.pageCahnnelInfo) {
            self.pageCahnnelInfo(_channelInfoDic,self.pageChannelID);
        }
    }
    
    [_contentTableView.mj_header endRefreshing];
}

#pragma mark - tableView 代理方法
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *itemArr = [NSArray array];
    if (![_hasChild isEqualToString:@"0"]) {
        itemArr = [[_childrenArray objectAtIndex:section] objectForKey:@"programList"];
    }
    else
    {
        itemArr = _childrenArray;
    }
    
    if (itemArr.count<=0) {
        return 0;
    }
    
    if (itemArr.count > 4) {
        return 2;
    }
    return (itemArr.count-1)/2 +1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    if (![_hasChild isEqualToString:@"0"]) {
        return _childrenArray.count;
    }
    else
    {
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 141*kDeviceRate;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    JKYSHHotHealthyVodCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[JKYSHHotHealthyVodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }

    if (![_hasChild isEqualToString:@"0"]) {
        cell.dateArray.array = [self arrayFOR_ALLArray:[[_childrenArray objectAtIndex:indexPath.section] objectForKey:@"programList"] withBeginnumb:indexPath.row];
    }
    else
    {
        cell.dateArray.array = [self arrayFOR_ALLArray:_childrenArray withBeginnumb:indexPath.row];
    }
    
    [cell update];
    
    cell.leftView.subTitle.font = HT_FONT_THIRD;
    cell.rightView.subTitle.font = HT_FONT_THIRD;
    cell.leftView.subTitle.textColor = HT_COLOR_FONT_FIRST;
    cell.rightView.subTitle.textColor = HT_COLOR_FONT_FIRST;

    cell.block = ^(UIButton_Block*btn)
    {
        
        NSLog(@"选中了啥？------>> %@",btn.polyMedicDic);
        
        NSMutableArray *tempHealthVodList = [NSMutableArray array];
        
        if (![_hasChild isEqualToString:@"0"]) {
            tempHealthVodList = [[_childrenArray objectAtIndex:indexPath.section] objectForKey:@"programList"];
        }
        else
        {
            tempHealthVodList = _childrenArray;
        }
        
        NSString *channelId = [[_childrenArray objectAtIndex:indexPath.section] objectForKey:@"id"];
        
        NSString *channelName = [[_childrenArray objectAtIndex:indexPath.section] objectForKey:@"name"];
        
        HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
        
        HTVC.recommendArray = tempHealthVodList;
        
        HTVC.polyMedicDic = btn.polyMedicDic;
        NSLog(@"gggggg－－－%@",btn.polyMedicDic);
        HTVC.titleStr = btn.eventName;
        
        NSString *ENtype = [NSString stringWithFormat:@"0"];
        
        NSString *TrackId = [[NSString alloc]init];
        
        NSString *TrackName = [[NSString alloc]init];
        
        //判断是否是由首页进入的一级界面
        if (self.TrackId.length == 0) {
            TrackId = [NSString stringWithFormat:@"1,%@,%@",self.pageChannelID,channelId];
            
            TrackName = [NSString stringWithFormat:@"健康,%@,%@",self.pageChannelName,channelName];

        }
        else
        {
            TrackId = [NSString stringWithFormat:@"%@,%@",self.TrackId,channelId];
            
            TrackName = [NSString stringWithFormat:@"%@,%@",self.TrackName,channelName];
        }
        
        NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btn.serviceIDName,@"itemId",btn.eventName,@"name",ENtype,@"ENtype",TrackId,@"TrackId",TrackName,@"TrackName", nil];
        
        NSLog(@"节目model－－－－－－－%@",paramDic);
        
        HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
        [HTVC setPolyModel:healthVodModel];
        
        [self.navigationController pushViewController:HTVC animated:NO];
    
    };
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if (![_hasChild isEqualToString:@"0"]) {
        return section == 0 ? kSection_Header_Height - kSection_GrayLine_Height : kSection_Header_Height;
    }
    else
    {
        return 5*kSection_GrayLine_Height;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   

    if (![_hasChild isEqualToString:@"0"]) {
        
        //整个header
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, section == 0 ? kSection_Header_Height - kSection_GrayLine_Height : kSection_Header_Height);
        [sectionHeaderView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        
        
        //白色部分的 header
        UIView *headerContentView = [UIView new];
        headerContentView.frame = CGRectMake(0, section == 0 ? 0 : kSection_GrayLine_Height, kDeviceWidth, kSection_Header_Height-kSection_GrayLine_Height);
        headerContentView.backgroundColor = HT_COLOR_FONT_INVERSE;
        [sectionHeaderView addSubview:headerContentView];
        
        
        //段头标题视图
        UILabel *sectionTitleLabel = [[UILabel alloc] init];
        sectionTitleLabel.frame = CGRectMake(12*kDeviceRate, 0, kDeviceWidth * 2/3, kSection_Header_Height-kSection_GrayLine_Height);
        if (_childrenArray.count > 0) {
            sectionTitleLabel.text = [[_childrenArray objectAtIndex:section] objectForKey:@"name"];
        }
        
        sectionTitleLabel.font = HT_FONT_SECOND;
        sectionTitleLabel.textColor = HT_COLOR_FONT_FIRST;
        [headerContentView addSubview:sectionTitleLabel];
        
        //竖线
        CGSize lineSize = CGSizeMake(1*kDeviceRate, 16*kDeviceRate);
        UIView *line = [UIView new];
        line.frame = CGRectMake(6*kDeviceRate,(headerContentView.frame.size.height-lineSize.height)/2, lineSize.width, lineSize.height);
        line.backgroundColor = App_selected_color;
        //        orangeLine.layer.cornerRadius = 2;
        [headerContentView addSubview:line];
        line.center = CGPointMake(sectionTitleLabel.frame.origin.x-8, sectionTitleLabel.center.y);
        
        return sectionHeaderView;
    }
    else
    {
        
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_GrayLine_Height);
        [sectionHeaderView setBackgroundColor:HT_COLOR_FONT_INVERSE];
        return sectionHeaderView;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (![_hasChild isEqualToString:@"0"]) {
        NSArray *sectionItems = [[_childrenArray objectAtIndex:section] objectForKey:@"programList"];
        if (sectionItems.count>=kSection_Footer_MaxNum) {
            
            return kSection_Footer_Height;
        }
    }
    return 0.000001f;//这里不能直接设置为0
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (![_hasChild isEqualToString:@"0"]) {
        if (section>=_childrenArray.count) {
            return nil;
        }
        NSArray *sectionItems = [[_childrenArray objectAtIndex:section] objectForKey:@"programList"];
        if (sectionItems.count<kSection_Footer_MaxNum) {
            UIView *footerView = [UIView new];
            footerView.frame = CGRectZero;
            footerView.backgroundColor = HT_COLOR_FONT_INVERSE;
            return footerView;
        }
        
        UIView *footerView = [UIView new];
        footerView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Footer_Height);
        footerView.backgroundColor = HT_COLOR_FONT_INVERSE;
        
        //更多按钮
        CGSize moreBtnSize = CGSizeMake(kDeviceWidth, footerView.frame.size.height);
        UIButton_Block *moreBtn = [[UIButton_Block alloc] init];
        moreBtn.frame = CGRectMake(0, 0, moreBtnSize.width, moreBtnSize.height);
        moreBtn.center = footerView.center;
    
        [moreBtn setTitle:@"点此查看更多"forState:UIControlStateNormal];
        
        moreBtn.titleLabel.font = HT_FONT_THIRD;
        
        [moreBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        
        [footerView addSubview:moreBtn];
        
        moreBtn.Click = ^(UIButton_Block *btn,NSString *channel){
            
            NSLog(@"点击查看更多");
            
            NSString *channelName = [[_childrenArray objectAtIndex:section] objectForKey:@"name"];
            NSString *channelID = [[_childrenArray objectAtIndex:section] objectForKey:@"id"];
            NSString *kparentId = [[_childrenArray objectAtIndex:section] objectForKey:@"parentId"];
            
            NSLog(@"name :%@   id : %@  parentId : %@",channelName,channelID,kparentId);
            
            if (kparentId.length == 0) {
                NSLog(@"我有子频道 ");
                
                JKChannelDetailViewController *jk = [[JKChannelDetailViewController alloc]init];
                jk.detailTitle = channelName;
                jk.pageChannelID = channelID;
                
                NSString *TrackId = [NSString stringWithFormat:@"1,%@,%@",self.pageChannelID,jk.pageChannelID];
                
                NSString *TrackName = [NSString stringWithFormat:@"健康,%@,%@",self.pageChannelName,jk.detailTitle];
                
                jk.TrackId = TrackId;
                
                jk.TrackName = TrackName;
;
                
                [self.navigationController pushViewController:jk animated:YES];
            }else{
                NSLog(@"俺没有子频道 ");
                
                YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
                yj.categoryID = channelID;
                yj.name = channelName;
                
                NSString *TrackId = [[NSString alloc]init];
                
                NSString *TrackName = [[NSString alloc]init];
                
                //判断是否是由首页进入的一级界面
                if (self.TrackId.length == 0) {
                    TrackId = [NSString stringWithFormat:@"1,%@,%@",self.pageChannelID,yj.categoryID];
                    
                    TrackName = [NSString stringWithFormat:@"健康,%@,%@",self.pageChannelName,yj.name];
                }
                else
                {
                    TrackId = [NSString stringWithFormat:@"%@,%@",self.TrackId,yj.categoryID];
                    
                    TrackName = [NSString stringWithFormat:@"%@,%@",self.TrackName,yj.name];
                }
                
                yj.TrackId = TrackId;
                
                yj.TrackName = TrackName;
                
                [self.navigationController pushViewController:yj  animated:YES];
            }
                        
        };
        
        //    UIButton_Block *
        UIView *lineView = [UIView new];
        lineView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        lineView.frame = CGRectMake(0, 0, kDeviceWidth, 0.5);
        [footerView addSubview:lineView];
        
        
        return footerView;
    }
    return nil;
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    
    if (array.count > index*2) {
        [anser addObject:array[index*2]];
    }
    if (array.count > index*2+1) {
        [anser addObject:array[index*2+1]];
    }
    
    return anser;
}

#pragma mark - VTMagicReuseProtocol
- (void)vtm_prepareForReuse
{
    NSLog(@"clear old data if needed:%@", self.pageChannelID);
    
    [_headerView reloadHeaderResult:nil];
    
    _childrenArray = nil;
    
    [_contentTableView reloadData];
}

#pragma mark - 用户体验优化
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}

@end
