    //
    //  KDIYRefreshHeader.h
    //  HIGHTONG_Public
    //
    //  Created by Kael on 16/7/1.
    //  Copyright © 2016年 创维海通. All rights reserved.
    //

#import <MJRefresh/MJRefresh.h>

#define loading_StateRefreshing @"正在刷新,请稍后..."
#define loading_StatePulling @"松开刷新数据"
#define loading_StateIdle @"下拉刷新"

@interface KDIYRefreshHeader : MJRefreshHeader

@property (nonatomic,strong) CABasicAnimation *animation;


@end
