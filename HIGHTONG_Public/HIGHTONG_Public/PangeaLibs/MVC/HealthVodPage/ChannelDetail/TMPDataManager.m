//
//  TMPDataManager.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/7/4.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "TMPDataManager.h"

static TMPDataManager *_dataManager;

@interface TMPDataManager()
{

    NSString *_jsonPath;

}


@end


@implementation TMPDataManager

+(TMPDataManager *)shareDataManager{
    
    if (_dataManager == nil) {
        _dataManager = [[TMPDataManager alloc] init];
    }
    
    return _dataManager;
}


-(NSDictionary *)GetChannelInfo{
    _jsonPath = [[NSBundle mainBundle]pathForResource:@"channel_info"ofType:@"json"];
    NSData *jdata = [[NSData alloc]initWithContentsOfFile:_jsonPath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jdata options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"reslut : %@",result);
    return result;
}


-(NSDictionary *)GetChannelVideo{
    _jsonPath = [[NSBundle mainBundle]pathForResource:@"channel_video"ofType:@"json"];
    NSData *jdata = [[NSData alloc]initWithContentsOfFile:_jsonPath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jdata options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"reslut : %@",result);
    return result;

}

-(NSDictionary *)GetChildren{
    _jsonPath = [[NSBundle mainBundle]pathForResource:@"get_children"ofType:@"json"];
    NSData *jdata = [[NSData alloc]initWithContentsOfFile:_jsonPath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jdata options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"reslut : %@",result);
    return result;

}


-(NSDictionary *)GetQueryProgram{
    _jsonPath = [[NSBundle mainBundle]pathForResource:@"query_program"ofType:@"json"];
    NSData *jdata = [[NSData alloc]initWithContentsOfFile:_jsonPath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jdata options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"reslut : %@",result);
    return result;

}




@end
