//
//  JKChannelDetailHeaderView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDCycleScrollView.h"
#import "JKChannelDetailFilterView.h"


@interface JKChannelDetailHeaderView : UIView<SDCycleScrollViewDelegate>

/**
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有类型
 */
@property (nonatomic,strong) NSMutableArray *cycleTypes;

/**
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有参数（包括条转链接，id等）
 */
@property (nonatomic,strong) NSMutableArray *cycleParames;
/**
 *  @author Kael
 *
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有数据
 */
@property (nonatomic,strong) NSMutableArray *cycleImagesData;

/**
 *  @author Kael
 *
 *  @brief 轮播图标题数组
 */
@property (nonatomic,strong) NSMutableArray *cycleTitles;

/**
 *  @author Kael
 *
 *  @brief 轮播图 本地图片数组
 */
@property (nonatomic,strong) NSMutableArray *cycleImages;

/**
 *  @author Kael
 *
 *  @brief 轮播图网络图片数组
 */
@property (nonatomic,strong) NSMutableArray *cycleLinks;

/**
 *  @author Kael
 *
 *  @brief 整个header的数据
 */
@property (nonatomic,strong) NSMutableDictionary *headerResult;

/**
 *  @author Kael
 *
 *  @brief 轮播图视图
 */
@property (nonatomic,strong) SDCycleScrollView *cycleScrollView;

/**
 *  @author Kael
 *
 *  @brief 选择器数组 包含所有数据
 */
@property (nonatomic,strong) NSMutableArray *filtersData;

/**
 *  @author Kael
 *
 *  @brief 频道选择器视图
 */
@property (nonatomic,strong)  JKChannelDetailFilterView *filterView;

/**
 *  @author Kael
 *
 *  @brief 选中轮播图中的某一个按钮
 */
@property (nonatomic,strong) void (^ didSelectedImagesBclok)(NSInteger index,NSString *webLink,NSString *channelID,NSString *types,NSString *parames,NSString *title);

/**
 *  @author Kael
 *
 *  @brief 监控 header 改变 frame
 */
@property (nonatomic,strong) void (^ headerViewReactChangedBlock)(CGRect frame);

/**
 *  @author Kael
 *
 *  @brief header中的filter 中的选择条件序列号 后期可能会有 ID 和 string （title之类的数值）
 */
@property (nonatomic,strong) void (^ headerFilterSelectedAtIndex) (NSInteger index,NSString *filterID,NSString *filterName);

/**
 *  @author Kael
 *
 *  @brief 重新加载轮播图数据
 *  @param imagesData 轮播图数组
 */
-(void)reloadHeaderCycleImagesData:(NSArray *)imagesData;

/**
 *  @author Kael
 *
 *  @brief 重载过滤器
 *  @param filtersArray 过滤条件数组
 */
-(void)reloadHeaderFiltersViewWith:(NSDictionary *)filtersDic;

/**
 *  @author Kael
 *
 *  @brief 重载header数据
 *  @param hederResult 整个字典
 */
-(void)reloadHeaderResult:(NSDictionary *)headerResult;


@end
