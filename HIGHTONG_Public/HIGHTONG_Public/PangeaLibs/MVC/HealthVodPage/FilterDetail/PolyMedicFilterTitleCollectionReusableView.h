//
//  PolyMedicFilterTitleCollectionReusableView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PolyMedicFilterTitleCollectionReusableView : UICollectionReusableView
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) NSString *titile;
@end
