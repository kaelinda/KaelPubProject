//
//  PolyMedicFilterDetailViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/6.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicFilterDetailViewController.h"
#import "KaelTool.h"
#import "ListDetailCollectionViewCell.h"
#import "PolyFilterCollectionView.h"
#import <QuartzCore/CAMediaTiming.h>
#import "HTRequest.h"
#import "HTVideoAVPlayerVC.h"
#import "HealthVodCollectionModel.h"
@interface PolyMedicFilterDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,HTRequestDelegate>
{
    UIButton *backBtn;//返回按钮
    UIButton *filterBtn;//筛选按钮
    PolyFilterCollectionView *greenView;

}
@property (nonatomic,strong)UILabel *filterLabel;
@property (nonatomic,strong)UIView *leftView;
@property (nonatomic, strong) UICollectionView *mainCollectionVC;
@property (nonatomic, strong) NSMutableArray *detailArr;
@end

@implementation PolyMedicFilterDetailViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:backBtn];
    [self setNaviBarTitle:@"筛选"];
    filterBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"icon_nav_filter_normal" imgHighlight:nil target:self action:@selector(goBackVV:)];
    [self setNaviBarRightBtn:filterBtn];
    
    
    _leftView = [[UIView alloc]init];
    [_leftView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_leftView];
    
    
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64+kTopSlimSafeSpace);
        make.left.mas_equalTo(10*kRateSize);
        make.width.mas_equalTo((kDeviceWidth-20)*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
    }];
    
    UILabel *leftLine = [[UILabel alloc]init];
    [leftLine setBackgroundColor:App_orange_color];
    leftLine.layer.cornerRadius = 2;
    leftLine.layer.masksToBounds = YES;
    [_leftView addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5*kRateSize);
        make.left.mas_equalTo(0*kRateSize);
        make.width.mas_equalTo(3*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
    }];
    
    
        _filterLabel = [[UILabel alloc] init];
        _filterLabel.backgroundColor = [UIColor clearColor];
        _filterLabel.font = [UIFont systemFontOfSize:14.0f*kRateSize];
    _filterLabel.textColor =  UIColorFromRGB(0x999999);
        _filterLabel.textAlignment = NSTextAlignmentLeft;
        [_leftView addSubview:_filterLabel];
        if (_filterDetailStr.length<=0) {
            
            _filterDetailStr = @"";
            _leftView.hidden = YES;
        }else
        {
            NSMutableString *filterString = [NSMutableString stringWithString:_filterDetailStr];
    
            [filterString deleteCharactersInRange:NSMakeRange(filterString.length-1, 1)];
            _filterDetailStr = [NSString stringWithFormat:@"%@",filterString];
            [_filterLabel setText:[NSString stringWithFormat:@"筛选条件：%@",_filterDetailStr]];
            [_filterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_leftView.mas_top).offset(5*kRateSize);
                make.left.mas_equalTo(leftLine.mas_right).offset(6*kRateSize);
                make.width.mas_equalTo(kDeviceWidth-20*kRateSize);
                make.height.mas_equalTo(20*kRateSize);
            }];
        }

    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request YJHealthyVodProgramListWithChannelId:_polyChannelID withPageNo:@"0" withPageSize:@"100" withKeyword:_filterDetailStr withFilter:_filterDetailArr];
    self.view.backgroundColor = App_background_color;
}



-(void)goBackVV:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    switch (status) {
        case 0:
        {
            if ([type isEqualToString:YJ_HEALTHYVOD_PROGRAMLIST])
            {
                int ret = [[result objectForKey:@"ret"] intValue];
                
                if (ret == 0) {
                    NSDictionary *Dic = [[NSDictionary alloc] initWithDictionary:result];
                    NSLog(@"*****？？？****%@",Dic);
                    _detailArr = [NSMutableArray array];
                    _detailArr = [Dic objectForKey:@"list"];
                    [self inintCollectionView];

                }
            }
        }
    }
}

-(void)inintCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];

    layout.minimumInteritemSpacing = 10;
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _mainCollectionVC = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace+(_leftView.hidden?0:30*kRateSize), kDeviceWidth , KDeviceHeight-64-(_leftView.hidden?0:30*kRateSize)-kTopSlimSafeSpace-kBottomSafeSpace) collectionViewLayout:layout];
    
    _mainCollectionVC.backgroundColor = App_background_color;
    _mainCollectionVC.delegate = self;
    _mainCollectionVC.dataSource = self;
    _mainCollectionVC.showsVerticalScrollIndicator = YES;
    _mainCollectionVC.allowsMultipleSelection = NO;
    [self.view addSubview:_mainCollectionVC];
    
    
    [self.mainCollectionVC registerClass:[ListDetailCollectionViewCell class] forCellWithReuseIdentifier:@"listCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _detailArr.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ListDetailCollectionViewCell *cell = (ListDetailCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"listCell" forIndexPath:indexPath];
    NSMutableDictionary *detailDic = _detailArr[indexPath.row ];
    cell.filterDetailLabel.text = [detailDic objectForKey:@"name"];
    [cell.filterDetailIV sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([detailDic objectForKey:isUsePolymedicine_2_0?@"imageUrl":@"image"])] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((kDeviceWidth - 30*kRateSize) / 2.0,  130*kRateSize);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(10*kRateSize, 10*kRateSize, 10*kRateSize, 10*kRateSize);
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    dictionary = [_detailArr objectAtIndex:indexPath.row];
    
    NSLog(@"uuuuuuuuuaaaaaaa----%@",dictionary);
    
    HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
    HTVC.recommendArray = self.detailArr;
    HTVC.polyMedicDic = dictionary;
    HTVC.contentID = [dictionary objectForKey:@"id"];
    HTVC.titleStr = [dictionary objectForKey:@"name"];
    
    
    NSString *ENtype = [NSString stringWithFormat:@"3"];
    
    NSString *TrackId = [NSString stringWithFormat:@"%@",self.TrackId];

    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:HTVC.contentID,@"itemId",HTVC.titleStr,@"name",ENtype,@"ENtype",TrackId,@"TrackId", nil];
    
    
    HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
    [HTVC setPolyModel:healthVodModel];
    [self.navigationController pushViewController:HTVC animated:NO];
    
}


//返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
