//
//  ChannelsModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ChannelsModel.h"

@implementation ChannelsModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}


-(void)setValue:(id)value forKey:(NSString *)key{
    
    if ([key isEqualToString:@"id"]) {
        
        self.channelID = value;
    }else if ([key isEqualToString:@"全部"])
    {
        self.channelID = @"";
    }else if ([key isEqualToString:@"name"])
    {
        self.channelName = value;
    }
    else {
        [super setValue:value forKey:key];
    }
}
@end
