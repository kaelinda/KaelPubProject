//
//  ListDetailCollectionViewCell.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/7.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ListDetailCollectionViewCell.h"

@implementation ListDetailCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {

//        [self.contentView setBackgroundColor:[UIColor magentaColor]];
//        _filterDetailIV = [[UIImageView alloc]initWithFrame:CGRectMake(0*kRateSize, 0, frame.size.width-0*kRateSize, frame.size.height - 40*kRateSize)];
//        [_filterDetailIV setBackgroundColor:[UIColor redColor]];
//        [self.contentView addSubview:_filterDetailIV];
//        
//        _filterDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0*kRateSize, _filterDetailIV.frame.size.height, frame.size.width-0*kRateSize, 40*kRateSize)];
//        [_filterDetailLabel setBackgroundColor:[UIColor orangeColor]];
//        [self.contentView addSubview:_filterDetailLabel];
        
        
        [self setupViews];
    }
    
    
    return self;
}


//-(instancetype)init
//{
//    if (self = [super init]) {
//    
//        [self setupViews];
//    }
//    
//    return self;
//}


-(void)setupViews
{
//    [self.contentView setBackgroundColor:[UIColor magentaColor]];

    _filterDetailIV = [[UIImageView alloc]init];
    [_filterDetailIV setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_filterDetailIV];
    [_filterDetailIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).offset(0*kRateSize);
        make.left.mas_equalTo(self.contentView).offset(0);
        make.width.mas_equalTo(self.contentView.mas_width);
//        make.height.mas_equalTo(self.contentView.mas_height-40);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-20*kRateSize);
    }];
    
    
//    _filterDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0*kRateSize, _filterDetailIV.frame.size.height, frame.size.width-0*kRateSize, 40*kRateSize)];
//            [_filterDetailLabel setBackgroundColor:[UIColor orangeColor]];
//            [self.contentView addSubview:_filterDetailLabel];
    
    _filterDetailLabel = [[UILabel alloc]init];
    [_filterDetailLabel setFont:[UIFont systemFontOfSize:12*kRateSize]];
    _filterDetailLabel.textColor = UIColorFromRGB(0x999999);
    [self.contentView addSubview:_filterDetailLabel];
    [_filterDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_filterDetailIV.mas_bottom);
        make.left.mas_equalTo(self.contentView);
        make.width.mas_equalTo(self.contentView.mas_width);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
    }];
    
    
    
    
    
}



@end
