//
//  PolyMedicFilterTitleCollectionReusableView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicFilterTitleCollectionReusableView.h"

@implementation PolyMedicFilterTitleCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor greenColor]];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10*kRateSize, 0*kRateSize, kDeviceWidth , 15*kRateSize)];
        _titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        _titleLabel.textColor = UIColorFromRGB(0x666666);
        [self addSubview:_titleLabel];
        
    }
    return self;
}

//-(instancetype)init{
//    self = [super init];
//    if (self) {
//        [self setupSubViews];
//    }
//    
//    return self;
//    
//}
//
//
//-(void)setupSubViews
//{
//    _titleLabel = [[UILabel alloc]init];
//    _titleLabel.backgroundColor = [UIColor greenColor];
//    _titleLabel.font = [UIFont systemFontOfSize:18];
//
//    [self addSubview:_titleLabel];
//    
//    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(10*kRateSize);
//        make.top.mas_equalTo(0);
//        make.width.mas_equalTo(self.width);
//        make.height.mas_equalTo(30*kRateSize);
//    }];
//    
//    
//}

- (void)setTitile:(NSString *)titile{
    _titile = titile;
    
    _titleLabel.text = _titile;
}
@end
