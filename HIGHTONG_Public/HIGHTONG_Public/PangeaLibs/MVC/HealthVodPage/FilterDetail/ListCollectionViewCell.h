//
//  ListCollectionViewCell.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChannelsModel.h"
#import "UIButton_Block.h"
typedef void(^SelectChannelBtnReturn)(UIButton_Block*buttonBlock);
@interface ListCollectionViewCell : UICollectionViewCell
@property (nonatomic, copy) ChannelsModel *model;
@property (nonatomic, copy) UIButton_Block *channelBtn;
@property (nonatomic, copy) UILabel *titleLabel;
@property (nonatomic,copy) SelectChannelBtnReturn SelectChannelBtnReturn;
@end
