//
//  PolyMedicFilterViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicFilterViewController.h"
#import "PolyMedicFilterDetailViewController.h"
#import "PolyFilterCollectionView.h"
#import "HTRequest.h"
@interface PolyMedicFilterViewController ()<HTRequestDelegate>
{
    UIButton *backBtn;//返回按钮
   

}
@property (nonatomic,strong)PolyFilterCollectionView *polyMedicFileterView;
@end
@implementation PolyMedicFilterViewController
-(void)viewDidLoad
{
    [super viewDidLoad];

    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    
    [self setNaviBarLeftBtn:backBtn];
    
    [self setNaviBarTitle:@"筛选"];
    
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request YJHealthyVodChannelFiltersWithChannelId:_polyChannelID withIncludeYear:@"1"];
    
    [self showCustomeHUD];
}



-(void)initPolyFilterCollectionView:(NSMutableArray *)filterArr
{
    
    WS(wSelf);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self hiddenCustomHUD];

    });

    
    UIView *nilview = [UIView new];
    nilview.frame = CGRectZero;
    [self.view addSubview:nilview];
    _polyMedicFileterView = [[PolyFilterCollectionView alloc]initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace)];
    [_polyMedicFileterView setHeaderArray:filterArr];
    _polyMedicFileterView.confirmButtonClick = ^(NSMutableArray *selectLastArr,NSString *selectString,BOOL isSelect){
        
        PolyMedicFilterDetailViewController  *polyDetail = [[PolyMedicFilterDetailViewController alloc]init];
        polyDetail.filterDetailArr = selectLastArr;
        polyDetail.filterDetailStr = selectString;
        polyDetail.polyChannelID = wSelf.polyChannelID;
        
        polyDetail.TrackId = wSelf.TrackId;
        
        [wSelf.navigationController pushViewController:polyDetail animated:YES];
        
        
    };
    [_polyMedicFileterView setBackgroundColor:App_background_color];
    
    [self.view addSubview:_polyMedicFileterView];
    
    self.view.backgroundColor = App_background_color;
}



-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    switch (status) {
        case 0:
        {
            if ([type isEqualToString:YJ_HEALTHYVOD_CHANNELFILTERS])
            {
                int ret = [[result objectForKey:@"ret"] intValue];
                
                if (ret == 0) {
                     NSDictionary *Dic = [[NSDictionary alloc] initWithDictionary:result];
                    
                    NSMutableArray *arr = [NSMutableArray arrayWithObject:[Dic objectForKey:@"list"]];
                    [self initPolyFilterCollectionView:arr];
                }else
                {
                    
                }
                
            }

        }
            
    }
    
}


//返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
