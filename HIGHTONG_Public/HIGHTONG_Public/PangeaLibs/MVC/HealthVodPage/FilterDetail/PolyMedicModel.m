//
//  PolyMedicModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicModel.h"

@implementation PolyMedicModel
- (instancetype)init{
    if (self = [super init]) {
        self.channels = [NSMutableArray arrayWithCapacity:0];
    }
    return self;
}
-(void)setValue:(id)value forKey:(NSString *)key{
    
    if ([key isEqualToString:@"id"]) {
        
        self.typeID = value;
        
    }
    else if ([key isEqualToString:@"name"])
    {
        _name = value;
    }else if([key isEqualToString:@"items"]){
        
        if ([value count]>0) {
            
            for (NSDictionary *dic in value) {
            ChannelsModel *model = [[ChannelsModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.channels addObject:model];
        }
            
        }
                
    } else {
        
//        [super setValue:value forKey:key];
    }
}






@end
