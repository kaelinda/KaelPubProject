//
//  PolyFilterCollectionView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/12.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
typedef void(^ConfirmButtonClick)(NSMutableArray *selectLastArr,NSString *selectString,BOOL isSelect);

@interface PolyFilterCollectionView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,copy)ConfirmButtonClick confirmButtonClick;
@property (nonatomic,copy)NSMutableArray *headerArray;//总数据
@end
