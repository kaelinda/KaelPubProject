//
//  PolyMedicFilterDetailViewController.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/6.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface PolyMedicFilterDetailViewController : HT_FatherViewController
@property (nonatomic,strong)NSMutableArray *filterDetailArr;
@property (nonatomic,strong)NSString *filterDetailStr;
@property (nonatomic,strong)NSString *polyChannelID;

@property (nonatomic,copy)NSString *TrackId;//频道路径
@end
