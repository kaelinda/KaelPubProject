//
//  PolyMedicFilterFooterCollectionReusableView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PolyMedicFilterFooterCollectionReusableView : UICollectionReusableView
@property (nonatomic,strong)UILabel *lineFooter;
@end
