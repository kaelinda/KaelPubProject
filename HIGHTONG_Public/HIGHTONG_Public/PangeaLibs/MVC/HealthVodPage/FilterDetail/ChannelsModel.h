//
//  ChannelsModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelsModel : NSObject
@property (nonatomic,strong)NSString *channelID;
@property (nonatomic,strong)NSString *channelName;

@end
