//
//  PolyMedicFilterFooterCollectionReusableView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicFilterFooterCollectionReusableView.h"

@implementation PolyMedicFilterFooterCollectionReusableView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _lineFooter = [[UILabel alloc] initWithFrame:CGRectMake(10*kRateSize, 8*kRateSize, kDeviceWidth-20*kRateSize, 1*kRateSize)];
        
        [self addSubview:_lineFooter];
        
    }
    return self;
}
@end
