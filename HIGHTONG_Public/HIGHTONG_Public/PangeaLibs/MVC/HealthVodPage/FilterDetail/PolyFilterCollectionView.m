//
//  PolyFilterCollectionView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/12.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyFilterCollectionView.h"
#import "PolyMedicFilterTitleCollectionReusableView.h"
#import "ListCollectionViewCell.h"
#import "PolyMedicModel.h"
#import "ChannelsModel.h"
#import "PolyMedicFilterFooterCollectionReusableView.h"
#import "MJExtension.h"
@interface PolyFilterCollectionView ()
{
    NSMutableArray* _indexs;
    NSMutableArray *selectLastArr;
}
@property (nonatomic, strong) UICollectionView *mainCollectionVC;
@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) NSMutableArray *dataArry;   // 频道的数组
@property (nonatomic, strong) UIButton *confirmButton;

@end

@implementation PolyFilterCollectionView
//-(instancetype)init{
//    self = [super init];
//    if (self) {
//        self.backgroundColor = [UIColor clearColor];//视图背景色透明
//        [self setupSubViews];
//    }
//    return self;
//}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {

        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }
    return self;
}

-(void)setHeaderArray:(NSMutableArray *)headerArray
{
//    if (_headerArray) {
//        _headerArray = nil;
//    }
//    _headerArray = [NSMutableArray array];
    _headerArray = headerArray;
    
    [self setupSubViews];

}



- (void)setupSubViews{
 
    _dataArry = [NSMutableArray array];
//    _headerArray = [NSMutableArray array];
//    
//    NSString * path = [[NSBundle mainBundle]pathForResource:@"FilterJson" ofType:@"json" ];
//    NSData * jsonData = [[NSData alloc] initWithContentsOfFile:path];
//    
//    NSError * error ;
//    NSMutableDictionary* jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
//    NSLog(@"我的字典%@",jsonObj);
//    if (!jsonObj || error) {
//        NSLog(@"JSON解析失败");
//    }
//    
//    _headerArray = [jsonObj objectForKey:@"list"];
    
//    [[_headerArray objectAtIndex:0] enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//            PolyMedicModel *model = [[PolyMedicModel alloc] init];
//            
//            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//            dic = obj;
//            if ([[dic objectForKey:@"items"] count]>0) {
//                NSMutableDictionary *dicton = [NSMutableDictionary dictionary];
//                [dicton setObject:@"全部" forKey:@"id"];
//                [dicton setObject:@"全部" forKey:@"name"];
//                //            if ([[dic objectForKey:@"items"] count]>0) {
//                
//                [[dic objectForKey:@"items"] insertObject:dicton atIndex:0];
//                [model setValuesForKeysWithDictionary:dic];
//                
//                //            }
// 
//        }
//            
//            [self.dataArry addObject:model];
//
//            
//    }];
    
    [[_headerArray objectAtIndex:0] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        PolyMedicModel *model = [[PolyMedicModel alloc] init];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic = obj;
        if ([[dic objectForKey:@"items"] count]>0) {
            NSMutableDictionary *dicton = [NSMutableDictionary dictionary];
            [dicton setObject:@"全部" forKey:@"id"];
            [dicton setObject:@"全部" forKey:@"name"];
//            if ([[dic objectForKey:@"items"] count]>0) {
            
                [[dic objectForKey:@"items"] insertObject:dicton atIndex:0];
                [model setValuesForKeysWithDictionary:dic];
                
//            }
        }
        
        
        [self.dataArry addObject:model];
        
        
        
    }];
 
    [self createCollectionVC];

    _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];// buttonWithType:UIButtonTypeCustom];
    _confirmButton.layer.borderWidth = 1;
    _confirmButton.layer.cornerRadius = 8;
    _confirmButton.layer.masksToBounds = YES;
    _confirmButton.layer.borderColor = App_selected_color.CGColor;
    
    //    [_confirmButton setImage:[UIImage imageNamed:@"btn_sure_line-frame"] forState:UIControlStateNormal];
    [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    _confirmButton.adjustsImageWhenHighlighted = NO;
    [_confirmButton setBackgroundImage:[self imageWithColor:App_white_color] forState:UIControlStateNormal];
    [_confirmButton setBackgroundImage:[self imageWithColor:App_selected_color] forState:UIControlStateHighlighted];
    [_confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
    [_confirmButton setTitleColor:App_white_color  forState:UIControlStateHighlighted];
    [self addSubview:_confirmButton];
    [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(10*kRateSize);
        make.bottom.mas_equalTo(self.bottom).offset(-20*kRateSize);
        make.width.mas_equalTo(self.frame.size.width-20*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
    }];
    
    [_confirmButton addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];

}


-(void)confirmClick:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    selectLastArr = [NSMutableArray array];
    NSString *selectString = @"";
    for (int i =0; i<_indexs.count; i++) {
        PolyMedicModel *model = (PolyMedicModel*)_dataArry[[[_indexs objectAtIndex:i] section]];
        if ([model.channels count]>0) {
            ChannelsModel *channelModel = model.channels[[[_indexs objectAtIndex:i] row]];
            NSMutableDictionary *itemDIC = [[NSMutableDictionary alloc]init];
            
            NSMutableArray *arr = [NSMutableArray array];
            NSString *itemString = @"";
            if ([channelModel.channelName isEqualToString:@"全部"]) {
                
                itemString = @"";
            }else
            {
                [arr addObject:channelModel.channelID];
                [itemDIC setObject:model.typeID forKey:@"id"];
                [itemDIC setObject:arr forKey:@"items"];
                [selectLastArr addObject:itemDIC];
                
                
                itemString = channelModel.channelName;
                selectString = [selectString stringByAppendingString:[NSString stringWithFormat:@"%@/",itemString]];
            }
            
            
        }
        
    }
    
    NSLog(@"%@",selectString);
    
    
    if (self.confirmButtonClick) {
        self.confirmButtonClick(selectLastArr,selectString,sender.selected);
    }
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)createCollectionVC{
    
    _layout = [[UICollectionViewFlowLayout alloc] init];
    _layout.headerReferenceSize = CGSizeMake(self.frame.size.width, 15*kRateSize);
    _layout.footerReferenceSize = CGSizeMake(self.frame.size.width, 20*kRateSize);
    // 1.设置最小行间距
    _layout.minimumLineSpacing = 0;
    // 2.设置最小列间距 (大于最小列间距后 自适应距离大小)
    _layout.minimumInteritemSpacing = 0;
    _layout.itemSize = CGSizeMake(((((self.frame.size.width)/5)*kRateSize)), 25*kRateSize);
    [_layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    
    _mainCollectionVC = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width , (self.frame.size.height-60-20)) collectionViewLayout:_layout];
    _mainCollectionVC.backgroundColor = App_background_color;
    _mainCollectionVC.delegate = self;
    _mainCollectionVC.dataSource = self;
    _mainCollectionVC.showsVerticalScrollIndicator = YES;
    //    _mainCollectionVC.pagingEnabled = YES;
    //    _mainCollectionVC.clipsToBounds = NO;
    //    _mainCollectionVC.userInteractionEnabled = YES;
    //    self.view.userInteractionEnabled = YES;
    _mainCollectionVC.allowsMultipleSelection = NO;
    [self addSubview:_mainCollectionVC];
    
    
    UIView *collectionHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, -10*kRateSize, kDeviceWidth, 10*kRateSize)];
    
    _mainCollectionVC.contentInset = UIEdgeInsetsMake(10*kRateSize, 0, 0, 0);
    collectionHeadView.backgroundColor = [UIColor clearColor];
    [_mainCollectionVC addSubview:collectionHeadView];
    
    
    [self.mainCollectionVC registerClass:[ListCollectionViewCell class] forCellWithReuseIdentifier:@"listCell"];
    
    [self.mainCollectionVC registerClass:[PolyMedicFilterTitleCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"titleCell"];
    
    [self.mainCollectionVC registerClass:[PolyMedicFilterFooterCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerCell"];
    
    _indexs = [NSMutableArray array];
    
    
    
    for (int i= 0; i<_dataArry.count; i++) {
        
        [_indexs addObject:[NSIndexPath indexPathForItem:0 inSection:i]];
        [self collectionView:_mainCollectionVC didSelectItemAtIndexPath:_indexs[i]];
        
        
    }
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    PolyMedicModel *model = _dataArry[section];
    return model.channels.count;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    
    return self.dataArry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ListCollectionViewCell *cell = (ListCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"listCell" forIndexPath:indexPath];
    PolyMedicModel *model = _dataArry[indexPath.section ];
    if (model.channels.count>0) {
        ChannelsModel *channelModel = model.channels[indexPath.item];
        [cell setModel:channelModel];
    }
    
    if([_indexs containsObject:indexPath]){
        cell.titleLabel.textColor = App_selected_color;
    }else
        cell.titleLabel.textColor = UIColorFromRGB(0x999999);
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSMutableString *str = nil;
    
    str = [((PolyMedicModel *)(_dataArry[indexPath.section])).name mutableCopy];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        PolyMedicFilterTitleCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"titleCell" forIndexPath:indexPath];
        view.backgroundColor = [UIColor clearColor];
        view.titile = str;
        return view;
    }else {
        PolyMedicFilterFooterCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footerCell" forIndexPath:indexPath];
        view.lineFooter.backgroundColor = App_line_color;
        if (indexPath.section == _dataArry.count-1) {
            
            view.lineFooter.backgroundColor = [UIColor clearColor];
        }
        
        
        return view;
        
    }
    
    
    
}

//
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    return YES;
}






#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
        if(![_indexs containsObject:indexPath]){
        ListCollectionViewCell *originCell = (ListCollectionViewCell*)[collectionView cellForItemAtIndexPath:_indexs[indexPath.section]];
        //        [originCell setBackgroundColor:[UIColor blackColor]];
        originCell.titleLabel.textColor = UIColorFromRGB(0x999999);
        
        [_indexs replaceObjectAtIndex:indexPath.section withObject:indexPath];
        
        ListCollectionViewCell *cell = (ListCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
        cell.titleLabel.textColor = App_selected_color;
        
    }
    
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{

    NSLog(@"^^^^要取消的section%ld*****要取消的item%ld",[indexPath section ],[indexPath item]);
    
}



#pragma mark --- UICollectionViewDelegateFlowLayout


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(5*kRateSize, 10*kRateSize, 5*kRateSize, 10*kRateSize);
}
////每个section中不同的行之间的行间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{

    return 15*kRateSize;
}



@end
