//
//  ListCollectionViewCell.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ListCollectionViewCell.h"

@implementation ListCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {

//        _mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , frame.size.height - 30)];
//        
//        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0*kRateSize, frame.size.width, frame.size.height)];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [self.contentView addSubview:_titleLabel];

//        [self setupViews];
        
    }
    return self;
}


-(void)setupViews
{
    _titleLabel = [[UILabel alloc]init];
    [_titleLabel setBackgroundColor:[UIColor redColor]];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).offset(2*kRateSize);
        make.left.mas_equalTo(self.contentView).offset(0*kRateSize);
        make.bottom.mas_equalTo(self.contentView).offset(0*kRateSize);
        make.right.mas_equalTo(self.contentView).offset(0*kRateSize);
    }];
}


-(void)setModel:(ChannelsModel *)model
{
    _model = model;
    
    [_titleLabel setText:[NSString stringWithFormat:@"%@",_model.channelName]];
    
}
@end
