//
//  ListDetailCollectionViewCell.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/7/7.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListDetailCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView *filterDetailIV;
@property (nonatomic,strong) UILabel     *filterDetailLabel;
@end
