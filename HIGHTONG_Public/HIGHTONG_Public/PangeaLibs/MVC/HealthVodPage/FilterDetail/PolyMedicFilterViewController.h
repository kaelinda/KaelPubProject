//
//  PolyMedicFilterViewController.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface PolyMedicFilterViewController : HT_FatherViewController
@property (nonatomic,copy)NSString *polyChannelID;

@property (nonatomic,copy)NSString *TrackId;//频道路径

@property (nonatomic,copy)NSString *TrackName;//频道名称路径

@end
