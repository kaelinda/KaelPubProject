//
//  PolyMedicModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChannelsModel.h"
@interface PolyMedicModel : NSObject
@property (nonatomic,strong)NSString *typeID;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSMutableArray *channels;

@end
