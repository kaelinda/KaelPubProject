//
//  MoreHealthVodViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface MoreHealthVodViewController : HT_FatherViewController

@property (nonatomic,copy)NSMutableArray *healthVodList;

@property (copy , nonatomic) void(^backValue)(NSString *lableText);

@end
