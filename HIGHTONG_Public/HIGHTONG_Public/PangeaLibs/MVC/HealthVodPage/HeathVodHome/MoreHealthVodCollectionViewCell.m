//
//  MoreHealthVodCollectionViewCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MoreHealthVodCollectionViewCell.h"
#import "Masonry.h"

@implementation MoreHealthVodCollectionViewCell

// CollectionViewCell 初始化
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    

    [self setupUI];

    
    return self;
}

- (void)setupUI {
	
    _backImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth/3, kDeviceWidth/3)];
    _backImg.backgroundColor = UIColorFromRGB(0xe8e8e8);
    [self.contentView addSubview:_backImg];
    
    _backgroundImg = [[UIImageView alloc]initWithFrame:CGRectMake(0.5*kRateSize, 0*kRateSize, kDeviceWidth/3 - 1*kRateSize, kDeviceWidth/3 - 1*kRateSize)];
    _backgroundImg.backgroundColor = App_white_color;
    [_backImg addSubview:_backgroundImg];
    
    
    
    //台标
    self.linkImg = [[UIImageView alloc] init];
    self.linkImg.contentMode = UIViewContentModeScaleAspectFit;
    self.linkImg.backgroundColor = [UIColor clearColor];
    [_backgroundImg addSubview:self.linkImg];
    
    [self.linkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backImg.mas_centerX);
        make.top.equalTo(_backImg.mas_top).offset(12.5*kRateSize);
        make.height.equalTo(@(55*kRateSize));
        make.width.equalTo(@(85*kRateSize));
    }];
    
    //台名
    self.linkName = [[UILabel alloc] init];
    self.linkName.textColor = UIColorFromRGB(0X333333);
    self.linkName.font = App_font(13*kRateSize);
    self.linkName.textAlignment = NSTextAlignmentCenter;
    [_backgroundImg addSubview:self.linkName];
    
    [self.linkName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backImg.mas_centerX);
        make.top.equalTo(self.linkImg.mas_bottom).offset(10*kRateSize);
        make.bottom.equalTo(_backImg.mas_bottom).offset(-15*kRateSize);
        make.width.equalTo(@(105*kRateSize));
        
    }];
    
}

@end
