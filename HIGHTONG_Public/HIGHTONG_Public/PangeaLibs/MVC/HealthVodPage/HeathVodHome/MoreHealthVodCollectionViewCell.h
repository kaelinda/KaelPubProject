//
//  MoreHealthVodCollectionViewCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreHealthVodCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic, strong)UIImageView *backImg;//cell底图

@property (nonatomic, strong)UIImageView *backgroundImg;//cell背景框

@property (nonatomic, strong)UIImageView *linkImg;//台标

@property (nonatomic, strong)UILabel *linkName;//台名

@end
