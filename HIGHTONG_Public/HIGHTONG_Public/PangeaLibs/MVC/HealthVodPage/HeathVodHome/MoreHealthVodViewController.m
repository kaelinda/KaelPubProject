//
//  MoreHealthVodViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/7/1.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MoreHealthVodViewController.h"
#import "HTRequest.h"
#import "MoreHealthVodCollectionViewCell.h"
#import "YJHealthVideoSecondViewcontrol.h"
#import "TMPDataManager.h"
#import "JKChannelDetailViewController.h"


@interface MoreHealthVodViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,HTRequestDelegate>

{
    NSMutableArray *_HealthVodArray;//频道列表
}

@property (strong, nonatomic)UICollectionView *moreItemCollectionView;



@end

@implementation MoreHealthVodViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
    _HealthVodArray = [NSMutableArray array];
    
    _HealthVodArray = _healthVodList;
    
    [self setNaviBarTitle:@"全部频道"];
    
    [self moreItemCollectionViewSetup];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_GETCHILDCHANNEL]) {
        
        NSLog(@"YJ_HEALTHYVOD_GETCHILDCHANNEL返回数据: %ld result:%@  type:%@",status,result,type );

        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            _HealthVodArray = [result objectForKey:@"list"];
            
            [self.moreItemCollectionView reloadData];
            
        }
    }
}

#pragma mark -- UICollectionViewDataSource

//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _HealthVodArray.count;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"MoreItemCollectionView";
    MoreHealthVodCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *tempDic = [_HealthVodArray objectAtIndex:indexPath.row];
    
    if ([tempDic objectForKey:@"name"]) {
        cell.linkName.text = [tempDic objectForKey:@"name"];
    }
    
    if ([tempDic objectForKey:@"imageUrl"]) {
        
        [cell.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([tempDic objectForKey:@"imageUrl"])] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];//公版改版
    }
    

    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout

//定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kDeviceWidth/3, kDeviceWidth/3);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark --UICollectionViewDelegate

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"item======%ld",(long)indexPath.item);
    NSLog(@"row=======%ld",(long)indexPath.row);
    NSLog(@"section===%ld",(long)indexPath.section);
    
    NSDictionary *tempDic = [_HealthVodArray objectAtIndex:indexPath.row];
    
    self.backValue([tempDic objectForKey:@"id"]);
    
    [self.navigationController popViewControllerAnimated:YES];
  
}

//当cell高亮时返回是否高亮
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    MoreHealthVodCollectionViewCell* cell = (MoreHealthVodCollectionViewCell *)[colView cellForItemAtIndexPath:indexPath];
    //设置(Highlight)高亮下的颜色
    [cell.backgroundImg setBackgroundColor:App_background_color];
}

- (void)collectionView:(UICollectionView *)colView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    MoreHealthVodCollectionViewCell* cell = (MoreHealthVodCollectionViewCell *)[colView cellForItemAtIndexPath:indexPath];
    //设置(Nomal)正常状态下的颜色
    [cell.backgroundImg setBackgroundColor:App_white_color];
}



#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
    
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"返回首页");
    
}

- (void)moreItemCollectionViewSetup {
	
    //确定是水平滚动，还是垂直滚动
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    flowLayout.minimumInteritemSpacing = 0.0;
    flowLayout.minimumLineSpacing = 0.0;
    
    self.moreItemCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace) collectionViewLayout:flowLayout];
    
    
    self.moreItemCollectionView.dataSource=self;
    self.moreItemCollectionView.delegate=self;
    

    //注册Cell，必须要有
    [self.moreItemCollectionView registerClass:[MoreHealthVodCollectionViewCell class] forCellWithReuseIdentifier:@"MoreItemCollectionView"];
    
    [self.view addSubview:self.moreItemCollectionView];
    
    [self.moreItemCollectionView setBackgroundColor:App_background_color];
    
}

@end
