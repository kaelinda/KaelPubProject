//
//  HealthVodViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "VTMagicController.h"

@interface HealthVodViewController : VTMagicController

@property (nonatomic,copy)NSString *HealthVodViewTitle;//界面名称

- (void)setHealthId:(NSString *)healthId andTrackId:(NSString *)trackId andTrackName:(NSString *)trackName;

@end
