//
//  HealthVodViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/6/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HealthVodViewController.h"
#import "MoreHealthVodViewController.h"
#import "Masonry.h"
#import "HTRequest.h"
#import "JKChannelDetailViewController.h"

@interface HealthVodViewController ()<VTMagicViewDataSource, VTMagicViewDelegate,HTRequestDelegate>

@property (nonatomic, strong) NSMutableArray *menuNameList;//频道名称列表

@property (nonatomic, strong) NSMutableArray *menuList;//频道列表

@property (nonatomic,strong) UIButton *moreHealthVodBtn;//更多频道按钮

@property (nonatomic, strong) NSMutableArray *cacheList;//频道列表

@property (nonatomic, strong) NSString *chooseId;//选中id

@property (nonatomic, strong) NSString *trackId;//

@property (nonatomic, strong) NSString *trackName;//


/**
 *  首页类型
 */
@property (nonatomic,assign) HomePageType pagetype;

@end

@implementation HealthVodViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    
    [self setNaviBarLeftBtn: (self.navigationController.viewControllers.count >= 2) ? Back : nil];
    
    [self setNaviBarTitle:@"健康"];

    _menuList = [NSMutableArray array];
    _menuNameList = [NSMutableArray array];
    _cacheList = [NSMutableArray array];
    _chooseId = [[NSString alloc]init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.magicView.navigationColor = [UIColor whiteColor];
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDefault;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 3*kDeviceRate;
    self.magicView.itemSpacing = 25*kDeviceRate;
    self.magicView.itemScale = 1.2;
    
    [self integrateComponents];
    
    self.pagetype = kApp_HomePageType;
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    NSString *instanceCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    instanceCode = [instanceCode length]?instanceCode:@"";
    
    switch (self.pagetype) {
        case kHomePageType_JKTV_v1_1:
            
            [request YJHealthyVodGetChildChannelWithInstanceCode:instanceCode withParentId:@"0" withHomeFlag:@"2" withMaxCount:@"0"];
            
            break;
            
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        case kHomePageType_JKTV_V1_5:
        {
            [request YJHealthyVodGetTopChannelWithInstanceCode:instanceCode];
        }
            break;
        default:
            [request YJHealthyVodGetTopChannelWithInstanceCode:instanceCode];

            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_GETCHILDCHANNEL]||[type isEqualToString:YJ_HEALTHYVOD_GETTOPCHANNEL]) {
        
        NSLog(@"YJ_HEALTHYVOD_GETCHILDCHANNEL或者YJ_HEALTHYVOD_GETTOPCHANNEL返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            _menuList = [result objectForKey:@"list"];
            
            NSMutableArray *tempArray = [NSMutableArray array];
            
            for (NSDictionary *dic in _menuList) {
                
                if ([dic objectForKey:@"name"]) {
                    
                    [_menuNameList addObject:[dic objectForKey:@"name"]];
                    
                }
                
                if ([dic objectForKey:@"id"]) {
                    
                    [tempArray addObject:[dic objectForKey:@"id"]];
                    
                }
               
            }
            
            [self.magicView reloadData];
            
            for(id obj in tempArray){
                NSLog(@"%@",obj);
                
                NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:obj,@"id", nil];
                
                [_cacheList addObject:paramDic];
                
            }
            
            if (_chooseId) {
                
                [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    
                    dic = obj;
                    
                    if ([_chooseId isEqualToString:[dic objectForKey:@"id"]]) {
                        [self switchToPage:idx animated:YES];
                        
                        JKChannelDetailViewController *jk = [self viewControllerAtPage:idx];
                        
                        jk.TrackName = _trackName;
                        
                        jk.TrackId = _trackId;
                        
                    }
                    
                }];
            }
        }
    }
}



#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    
    return _menuNameList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        menuItem.titleLabel.font = HT_FONT_SECOND;
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    static NSString *gridIdd = @"grid.Identifier";
    JKChannelDetailViewController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
    if (!viewController) {
        viewController = [[JKChannelDetailViewController alloc] init];
        
        viewController.pageCahnnelInfo = ^(NSDictionary *channelInfoDic,NSString *pageChannelID){
        
            NSLog(@"pageChannelID--->>:%@",pageChannelID);
            
            for (NSMutableDictionary *dic in _cacheList) {
                if ([[dic objectForKey:@"id"]isEqualToString:pageChannelID]) {
                    
                    [dic setObject:channelInfoDic forKey:@"channelInfoDic"];
                }
            }
        };
    }

    viewController.isNeedNavBar = NO;
    viewController.isNeedTabBar = YES;
    viewController.pageChannelID = [[_menuList objectAtIndex:pageIndex] objectForKey:@"id"];
    return viewController;
}

#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(JKChannelDetailViewController *)viewController atPage:(NSUInteger)pageIndex
{
    viewController.pageChannelID = [[_menuList objectAtIndex:pageIndex] objectForKey:@"id"];
    
    NSLog(@"index:%ld viewDidAppeare:%@", (long)pageIndex, viewController.pageChannelID);
    
    for (NSDictionary *dic in _cacheList) {
        
        NSLog(@"eeeeeeeeeee---%@",dic);
        
        if ([[dic objectForKey:@"id"]isEqualToString:viewController.pageChannelID]) {
            if (![[dic objectForKey:@"channelInfoDic"]objectForKey:@"childVideoList"]) {
                [viewController reloadChannelDataWith:viewController.pageChannelID];
            }
            else
            {

            NSString *hasChild = [[NSString alloc]initWithString:[[dic objectForKey:@"channelInfoDic"]objectForKey:@"channelHasChlid"]];
                
            NSLog(@"eeeeeeeeeeeggg---%@",hasChild);
                
            [viewController reloadContentViewDataWith:[[dic objectForKey:@"channelInfoDic"]objectForKey:@"result"]andChildChannelList:[[dic objectForKey:@"channelInfoDic"]objectForKey:@"childVideoList"] andHasChild:hasChild];
            }
        }
    }
}

- (void)magicView:(VTMagicView *)magicView viewDidDisappeare:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    //    NSLog(@"index:%ld viewDidDisappeare:%@", (long)pageIndex, viewController.view);
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex
{
    //    NSLog(@"didSelectItemAtIndex:%ld", (long)itemIndex);
}

#pragma mark - actions
- (void)gotoMore
{
    NSLog(@"打开更多频道接界面");
    
    MoreHealthVodViewController *more = [[MoreHealthVodViewController alloc]init];
    
    more.healthVodList = _menuList;
    
    more.backValue = ^(NSString *tel)
    {

        [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {

            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([tel isEqualToString:[dic objectForKey:@"id"]]) {
                [self switchToPage:idx animated:YES];
            }
        }];
    };
    
    [self.navigationController pushViewController:more animated:YES];
}

#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
	
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"返回首页");
}

- (void)integrateComponents
{
    CGSize btnSize = CGSizeMake(48, 39);
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(self.view.frame.size.width-btnSize.width, 0, btnSize.width, btnSize.height);
    [rightButton addTarget:self action:@selector(gotoMore) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_default"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_press"] forState:UIControlStateHighlighted];
    rightButton.backgroundColor = CLEARCOLOR;
    self.magicView.rightNavigatoinItem = rightButton;

    NSLog(@"%@",self.magicView.rightNavigatoinItem.backgroundColor);
}

- (void)setHealthId:(NSString *)healthId andTrackId:(NSString *)trackId andTrackName:(NSString *)trackName
{
    
    _chooseId = healthId;
    
    _trackId = trackId;
    
    _trackName = trackName;
    
    if (_menuList) {
        
        [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([_chooseId isEqualToString:[dic objectForKey:@"id"]]) {
                [self switchToPage:idx animated:YES];
                
                JKChannelDetailViewController *jk = [self viewControllerAtPage:idx];
                
                jk.TrackName = _trackName;
                
                jk.TrackId = _trackId;
                
            }
        }];
    }
}



@end
