//
//  PubVideoHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/8.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubVideoHomeViewController.h"
#import "HTRequest.h"
#import "PubVideoChannelViewController.h"
#import "PubVideoMoreChannelController.h"

@interface PubVideoHomeViewController ()<VTMagicViewDataSource, VTMagicViewDelegate,HTRequestDelegate>

@property (nonatomic, strong) NSMutableArray *menuNameList;//频道名称列表

@property (nonatomic, strong) NSMutableArray *menuList;//频道列表

@property (nonatomic,strong) UIButton *morePubVodBtn;//更多频道按钮

@property (nonatomic, strong) NSMutableArray *cacheList;//历史频道列表

@property (nonatomic, strong) NSString *chooseId;//选中channelid

@property (nonatomic, strong) NSString *chooseProgramId;//选中programid

@property (nonatomic, strong) NSString *trackId;

@property (nonatomic, strong) NSString *trackName;

@property (nonatomic, strong) NSString *enType;



@end

@implementation PubVideoHomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    
    [self setNaviBarLeftBtn: (self.navigationController.viewControllers.count >= 2) ? Back : nil];
    

    [self setNaviBarTitle:@"轻松一刻"];
    
    _menuList = [NSMutableArray array];
    _menuNameList = [NSMutableArray array];
    _cacheList = [NSMutableArray array];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.magicView.navigationColor = [UIColor whiteColor];
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDefault;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 3*kDeviceRate;
    self.magicView.itemSpacing = 25*kDeviceRate;
    self.magicView.itemScale = 1.2;
    
    [self integrateComponents];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [self showCustomeHUD];
    [request PubCommonNavigationChannelListWithCategory:@"0"];
        
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    
    return _menuNameList;
    
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        menuItem.titleLabel.font = HT_FONT_SECOND;
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    
    
    static NSString *gridIdd = @"grid.Identifier";
    PubVideoChannelViewController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
    if (!viewController) {
        viewController = [[PubVideoChannelViewController alloc] init];
        
        viewController.pageCahnnelInfo = ^(NSMutableArray *channelInfoArr,NSString *pageChannelID,NSInteger pageNo){
            
            NSLog(@"pageChannelID--->>:%@",pageChannelID);
            
            for (NSMutableDictionary *dic in _cacheList) {
                if ([[dic objectForKey:@"id"]isEqualToString:pageChannelID]) {
                    
                    [dic setObject:channelInfoArr forKey:@"channelInfoDic"];
                    
                    [dic setObject:[NSString stringWithFormat:@"%ld",(long)pageNo] forKey:@"pageNo"];
                }
            }
            
            NSLog(@"cacheList--->>:%@",_cacheList);
        };
    }
    
    viewController.isNeedNavBar = NO;
    viewController.isNeedTabBar = NO;
    viewController.pageChannelID = [[_menuList objectAtIndex:pageIndex] objectForKey:@"id"];
    viewController.pageChannelName = [[_menuList objectAtIndex:pageIndex] objectForKey:@"name"];
    [viewController setIsMovePage:YES];

    return viewController;
    
    
}

#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(PubVideoChannelViewController *)viewController atPage:(NSUInteger)pageIndex
{
    viewController.pageChannelID = [[_menuList objectAtIndex:pageIndex] objectForKey:@"id"];
    
    viewController.pageChannelName = [[_menuList objectAtIndex:pageIndex] objectForKey:@"name"];
    NSLog(@"index:%ld viewDidAppeare:%@", (long)pageIndex, viewController.pageChannelID);
    
    for (NSDictionary *dic in _cacheList) {
        
        if ([[dic objectForKey:@"id"]isEqualToString:viewController.pageChannelID]) {
            if (![dic objectForKey:@"channelInfoDic"]) {
                
                NSInteger tempPageNo = [[dic objectForKey:@"pageNo"]integerValue];
                
                [viewController reloadChannelDataWith:viewController.pageChannelID withChannelPage:tempPageNo];
            }
            else
            {
                
                NSInteger tempPageNo = [[dic objectForKey:@"pageNo"]integerValue];
                
                [viewController reloadCacheDataWith:[dic objectForKey:@"channelInfoDic"] withChannelPage:tempPageNo];
            }
        }
    }
}

- (void)magicView:(VTMagicView *)magicView viewDidDisappeare:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)integrateComponents
{
    CGSize btnSize = CGSizeMake(48, 39);
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(self.view.frame.size.width-btnSize.width, 0, btnSize.width, btnSize.height);
    [rightButton addTarget:self action:@selector(gotoMore) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_default"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_press"] forState:UIControlStateHighlighted];
    rightButton.backgroundColor = CLEARCOLOR;
    self.magicView.rightNavigatoinItem = rightButton;
    
    NSLog(@"%@",self.magicView.rightNavigatoinItem.backgroundColor);
}

#pragma mark - actions
- (void)gotoMore
{
    NSLog(@"打开更多频道接界面");
    
    PubVideoMoreChannelController *more = [[PubVideoMoreChannelController alloc]init];
    
    more.pubChannelList = _menuList;
    
    more.backValue = ^(NSString *tel)
    {
        
        [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([tel isEqualToString:[dic objectForKey:@"id"]]) {
                [self switchToPage:idx animated:YES];
            }
        }];
        
        
    };
    
    [self.navigationController pushViewController:more animated:YES];
    
}

#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"返回首页");
    
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    if ([type isEqualToString:PUB_COMMON_NAVIGATIONCHANNELLIST]) {
        
        NSLog(@"PUB_COMMON_NAVIGATIONCHANNELLIST返回数据: %ld result:%@  type:%@",(long)status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            [self hiddenCustomHUD];
            _menuList = [result objectForKey:@"channelList"];
            
            NSMutableArray *tempArray = [NSMutableArray array];
            
            for (NSDictionary *dic in _menuList) {
                
                if ([dic objectForKey:@"name"]) {
                    
                    [_menuNameList addObject:[dic objectForKey:@"name"]];
                    
                }
                
                if ([dic objectForKey:@"id"]) {
                    
                    [tempArray addObject:[dic objectForKey:@"id"]];
                    
                }
                
            }
            
            [self.magicView reloadData];
            
            for(id obj in tempArray){
                NSLog(@"%@",obj);
                
                NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:obj,@"id",@"1",@"pageNo", nil];
                
                [_cacheList addObject:paramDic];
                
            }
            
            if (_chooseId) {
                
                [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    
                    dic = obj;
                    
                    if ([_chooseId isEqualToString:[dic objectForKey:@"id"]]) {
                        [self switchToPage:idx animated:YES];
                        
                        PubVideoChannelViewController *jk = [self viewControllerAtPage:idx];
                        jk.pageChannelName = [dic objectForKey:@"name"];
                        jk.pageChannelID = [dic objectForKey:@"id"];
                        
                        if (_chooseProgramId.length) {
                            
                            jk.trackName = _trackName;
                            
                            jk.trackId = _trackId;
                            
                            jk.enType = _enType;
                            
                            [jk getLocationPubProgramId:_chooseProgramId];
                            
                            _chooseProgramId = nil;
                            
                            
                        }else
                        {
                            
                            jk.trackName = _trackName;
                            
                            jk.trackId = _trackId;
                            
                            jk.enType = _enType;
                            
                            
                        }
                        
                    }
                    
                }];
            }
        }
    }
}


- (void)setPubChannelId:(NSString *)pubChannelId andPubProgramId:(NSString *)pubProgramId andTrackId:(NSString *)trackId andTrackName:(NSString *)trackName andEnType:(NSString *)enType
{
    
    _chooseId = [[NSString alloc]init];
    
    _chooseProgramId = [[NSString alloc]init];
    
    _trackId = [[NSString alloc]init];
    
    _trackName = [[NSString alloc]init];
    
    _enType = [[NSString alloc]init];
    
    _chooseId = pubChannelId;
    
    _chooseProgramId = pubProgramId;
    
    _trackId = trackId;
    
    _trackName = trackName;
    
    _enType = enType;

    if (_menuList) {
        
        [_menuList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([_chooseId isEqualToString:[dic objectForKey:@"id"]]) {
                [self switchToPage:idx animated:YES];
                
                PubVideoChannelViewController *jk = [self viewControllerAtPage:idx];
                jk.pageChannelName = [dic objectForKey:@"name"];
                jk.pageChannelID = [dic objectForKey:@"id"];
                
                
                if (_chooseProgramId.length) {
                    
                    jk.trackName = _trackName;
                    
                    jk.trackId = _trackId;
                    
                    jk.enType = _enType;
                    
                    [jk getLocationPubProgramId:_chooseProgramId];
                    
                    _chooseProgramId = nil;
                    
                    
                }else
                {
                    
                    jk.trackName = _trackName;
                    
                    jk.trackId = _trackId;
                    
                    jk.enType = _enType;
                    
                    
                }
                                
            }
        }];
        
    }
    
}


@end
