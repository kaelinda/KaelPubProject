//
//  PubVideoChannelViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface PubVideoChannelViewController : HT_FatherViewController

/**
 *  @author Dirk
 *
 *  @brief 节目信息请求结束后 回调该方法 夫控制器可用这个方法来获取每个自控制器的展示数据
 */
@property (nonatomic,copy) void (^ pageCahnnelInfo)(NSMutableArray * channelInfoArr,NSString *pageChannelID,NSInteger pageNo);

/**
 *  @author Dirk
 *
 *  @brief 频道详情界面的标题
 */
@property (nonatomic,strong) NSString *detailTitle;

/**
 *  @author Dirk
 *
 *  @brief 该页面频道的 ID
 */
@property (nonatomic,strong) NSString *pageChannelID;


/**
 *  @author Dirk
 *
 *  @brief 该页面频道的名称
 */
@property (nonatomic,strong) NSString *pageChannelName;

/**
 *  @author Dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar;

/**
 *  @author Dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedTabBar;

/**
 *  @author Dirk
 *
 *  @brief 主tableView
 */
@property (nonatomic,strong) UITableView *contentTableView;

/**
 *  @author Dirk
 *
 *  @brief 子频道类型的数据源
 */
@property (nonatomic,strong) NSMutableArray *programInfoListArray;

/**
 *  @author Dirk
 *
 *  @brief 请求页面数据（第一次时）
 */
-(void)reloadChannelDataWith:(NSString *)ChannelID withChannelPage:(NSInteger)channelPage;


/**
 *  @author Dirk
 *
 *  @brief 加载缓存数据
 */
-(void)reloadCacheDataWith:(NSMutableArray *)cacheDataArr withChannelPage:(NSInteger)channelPage;

/**
 *  @author Dirk
 *
 *  @brief 是否移动界面
 */
@property (nonatomic, assign) BOOL isMovePage;

/**
 *  @author Dirk
 *
 *  @brief 获取定位视频
 */
-(void)getLocationPubProgramId:(NSString *)pubProgramId;

@property (nonatomic, strong) NSString *trackId;

@property (nonatomic, strong) NSString *trackName;

@property (nonatomic, strong) NSString *enType;

@end
