//
//  PubVideoChannelViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubVideoChannelViewController.h"
#import "HTRequest.h"
#import "UIButton_Block.h"
#import "PubVideoProgramCell.h"
#import "ShareViewVertical.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import <UMSocialCore/UMSocialCore.h>
#import "HumourTableViewCell.h"
#import "HumourModel.h"
#import "HumourPlayer.h"
#import "MJRefresh.h"
#import "ShareView.h"
#import "CustomAlertView.h"
#import "NetWorkNotification_shared.h"
#import "PubVodPlayCollectionModel.h"
#import "PubVodInteractiveCollectionModel.h"
#import "UserLoginViewController.h"
#import "HTTimerManager.h"
#import "SAIInformationManager.h"
#import "SystermTimeControl.h"
#import "DateTools.h"
#import "PlayerHeader.h"
#import "ShareCollectionModel.h"
@interface PubVideoChannelViewController ()<HTRequestDelegate,UITableViewDelegate,UITableViewDataSource,shareViewDelegate,CustomAlertViewDelegate,HTTimerManagerDelegate>
{
    NSIndexPath *_currentIndexPath;
    NSString *_currentPlayID;
    
    NSInteger pageNo;//页码
    
}

@property (nonatomic,strong)NSString  *netInfo;

/**
 *  @author Dirk
 *
 *  @brief 临时存储点赞节目Id
 */
@property (nonatomic, strong) NSString *tempSupportId;

/**
 *  @author Dirk
 *
 *  @brief 临时存储点赞节目name
 */
@property (nonatomic, strong) NSString *tempSupportName;


/**
 *  @author Dirk
 *
 *  @brief share界面
 */
@property (nonatomic, strong) ShareViewVertical *shareViewVertical;

/**
 *  @author Dirk
 *
 *  @brief 用户交互model
 */
@property (nonatomic, strong) PubVodInteractiveCollectionModel *pubVodInteractiveCollection;


//信息采集


/**
 *  @author Dirk
 *
 *  @brief 播放路径model信息采集
 */
@property (nonatomic, strong) PubVodPlayCollectionModel *pubVodPlayModel;
@property (nonatomic, strong) HTTimerManager *timeManager;
@property (nonatomic, strong) NSString *watchDuration;
@property (nonatomic, strong) ShareCollectionModel *shareUmengModel;
/**
 *  @author Dirk
 *
 *  @brief share链接
 */
@property (nonatomic, strong) NSString *appLink;

/**
 *  @author Dirk
 *
 *  @brief share图片链接
 */
@property (nonatomic, strong) NSString *umengImageStr;

/**
 *  @author Dirk
 *
 *  @brief share内容
 */
@property (nonatomic, strong) NSString *umengContent;


/**
 友盟分享的shareID
 */
@property (nonatomic, strong) NSString *umengShareID;


/**
 友盟分享的shareTitle
 */
@property (nonatomic, strong) NSString *umengShareTitle;


@property (assign, nonatomic)  SharePlayerType sharePlayerType;


/**
 *  @author Dirk
 *
 *  @brief 该页面频道的 ID
 */
@property (nonatomic,strong) NSString *chooseProgramId;

@property (nonatomic, strong) NSString *currentPlayUrl;

@property (nonatomic, strong) HumourPlayer *humourPlayer;
@property(nonatomic,  strong)HumourTableViewCell *currentCell;
@property(nonatomic,  assign)BOOL isFullWindowRoation;

@end

@implementation PubVideoChannelViewController

-(instancetype)init{
    self = [super init];
    if (self) {
        _isNeedNavBar = YES;
        
        
    
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didFininshPlayer) name:@"ROTE_PUB_PLAYER" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NetWorkNotification_shared shardNetworkNotification]setCanShowToast:YES];//实时网络监测
        //注册通知的观察
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChange:) name:@"NETCHANGED" object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(msgStopPlayer) name:@"MSG_STOPPLAYER" object:nil];
        
        //插拔耳机
        // 使用这个category的应用不会随着手机静音键打开而静音，可在手机静音下播放声音
        NSError *setCategoryError = nil;
        BOOL success = [[AVAudioSession sharedInstance]
                        setCategory: AVAudioSessionCategoryPlayback
                        error: &setCategoryError];
        
        if (!success) { /* handle the error in setCategoryError */ }
        
        // 监听耳机插入和拔掉通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:) name:AVAudioSessionRouteChangeNotification object:nil];
        if (_timeManager) {
            _timeManager = nil;
        }
        _timeManager = [[HTTimerManager alloc]init];
        _timeManager.delegate = self;
        
        _watchDuration = nil;
        
    }
    return self;
}




/**
 *  耳机插入、拔出事件
 */
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            NSLog(@"Headphone/Line plugged in");
            
            [self actionPlayForVideo];

            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            
            
            [self actionPauseForVideo];

            NSLog(@"Headphone/Line was pulled. Stopping player....");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }}


-(void)colloctionCurrentHumourVideoInfo
{
    if (_humourPlayer.isCollectionInfo) {
        
        _pubVodPlayModel.watchDuration = _watchDuration;
        if (USER_ACTION_COLLECTION) {
            
            
            [SAIInformationManager pub_humourInformation:_pubVodPlayModel];
            
            [_timeManager reset];
            
            _watchDuration = nil;
            
            _humourPlayer.isCollectionInfo = NO;
            
            _pubVodPlayModel = nil;
        }
    }
}



-(void)msgStopPlayer
{
    [_humourPlayer.player playerClear];
    [_humourPlayer removeFromSuperview];
    
    _currentCell.humourBV.hidden = YES;
    
    _currentCell.displayHumourBV.hidden = NO;
    [self toSmalllScreenForCurrentCell];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
}


-(void)networkChange:(NSNotification*)noti
{
    NSString *notiStr = [noti object];
    
    if ([notiStr isEqualToString:@"蜂窝网络"]) {
        [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_play"] forState:UIControlStateNormal];
        [_humourPlayer.playButton setTag:ENGINE_STATUS_PLAY];
        
        [_humourPlayer.player pause];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
            nameAlert.tag = 11;
            [nameAlert show];
        });
        
    }
    
    
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 11) {
        //取消
        if (buttonIndex == 1) {
            
            [self toSmalllScreenForCurrentCellIsFullWindowRoation];
        }
        //确定
        if (buttonIndex == 0) {
            
            return;
            
        }
    }
    
    if (alertView.tag == 12) {
        //取消
        if (buttonIndex == 1) {
            [self toSmalllScreenForCurrentCellIsFullWindowRoation];
            
        }
        //确定
        if (buttonIndex == 0) {
            //            [alertView hide];
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            
            [request PubCommonProgramInfoPlayUrltWithProgramId:_currentPlayID];
            
        }
    }
    
}





-(void)didFininshPlayer
{
    
    [self toSmalllScreenForCurrentCell];
}



-(void)setIsMovePage:(BOOL)isMovePage
{
    _isMovePage = isMovePage;
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];

    _currentPlayID = 0;
    if (_isMovePage == YES) {
        [self colloctionCurrentHumourVideoInfo];
        [_humourPlayer.player playerClear];
        [_humourPlayer removeFromSuperview];
        
        _currentCell.humourBV.hidden = YES;
        
        _currentCell.displayHumourBV.hidden = NO;
        
    }

    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:App_white_color];

    pageNo = 1;
    
    self.programInfoListArray = [NSMutableArray array];
    
    self.tempSupportId = [[NSString alloc]init];
    
    self.chooseProgramId = [[NSString alloc]init];
    
    //初始化导航条
    [self setupNavigationBar];
    //初始化 tableview
    [self setupTableView];
    //初始化 分享界面
    [self setupShareView];
    
    if (_pageChannelID != nil) {
        //有ID那就需要刷新界面了
        //        [self loadNewChannelData];
    }
    
    
    [self reloadChannelDataWith:self.pageChannelID withChannelPage:pageNo];
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    if ([type isEqualToString:PUB_COMMON_PROGRAMINFOLIST]) {
        
        NSLog(@"PUB_COMMON_PROGRAMINFOLIST返回数据: %ld result:%@  type:%@",(long)status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            pageNo++;
            
            NSDictionary *resDIC = result;
            
            if(IsNilOrNull(self.programInfoListArray)) {
                
                self.programInfoListArray = [NSMutableArray array];
                
            }
            

            [self.programInfoListArray addObjectsFromArray:[resDIC objectForKey:@"programList"]];
        
            //如果 父控制器 想获取界面的数据 可以通过这个方法获取
            if (self.pageCahnnelInfo) {
                self.pageCahnnelInfo(_programInfoListArray,self.pageChannelID,pageNo);
            }
            
            [self.contentTableView reloadData];
            
            if (_chooseProgramId.length) {
                
                [_programInfoListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    
                    dic = obj;
                    
                    if ([self.chooseProgramId isEqualToString:[dic objectForKey:@"id"]]) {
                        
                        [self.contentTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idx
                                                           
                                                                             inSection:0]
                         
                                         atScrollPosition:UITableViewScrollPositionTop
                         
                                                 animated:YES];
                        
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                        _currentCell = [self.contentTableView cellForRowAtIndexPath:indexPath];
                        
                        [self scrollCurrentPositionCell:indexPath];
                        
                        NSLog(@"我就是你在外边选中的cell");
                        
                        self.chooseProgramId = nil;
                        
                    }
                    
                }];
                
            }
            
        }
        
    }
    
    if ([type isEqualToString:PUB_COMMON_INSERTPRAISE]) {
        
        NSLog(@"PUB_COMMON_INSERTPRAISE返回数据: %ld result:%@  type:%@",(long)status,result,type );
        
        if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]) {
            
            if (USER_ACTION_COLLECTION) {
                
                //此处放用户交互中点赞的信息采集
                
            [SAIInformationManager pub_humourInteractiveInformation:_pubVodInteractiveCollection];
                
            }
            
            [_programInfoListArray enumerateObjectsUsingBlock:^(NSMutableDictionary *obj, NSUInteger idx, BOOL * stop) {
                
                if ([[obj objectForKey:@"id"]isEqualToString:self.tempSupportId]) {
                    
                    *stop = YES;
                }
                
                if (*stop) {
                    
                    [obj setObject:@"1" forKey:@"isPraise"];
                    
                    NSString *tempPraise = [obj objectForKey:@"praise"];
                    
                    NSInteger tempInt = [tempPraise integerValue];
                    
                    tempPraise = [NSString stringWithFormat:@"%ld",tempInt + 1];
                    
                    [obj setObject:tempPraise forKey:@"praise"];
                    
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                    HumourTableViewCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
                    
                    cell.programDic = obj;
                    
                    [cell update];
                    
                    HumourModel *humourModel = [[HumourModel alloc]initWithDict:obj];
                    
                    [cell setHumourModel:humourModel];
                    
                    //如果 父控制器 想获取界面的数据 可以通过这个方法获取
                    if (self.pageCahnnelInfo) {
                        self.pageCahnnelInfo(_programInfoListArray,self.pageChannelID,pageNo);
                    }
                   
                }
                
            }];
        }else if([[result objectForKey:@"ret"]integerValue] == -3 && [result objectForKey:@"ret"])//已经点赞过的，会给出一个假的点赞效果
        {
            
            [_programInfoListArray enumerateObjectsUsingBlock:^(NSMutableDictionary *obj, NSUInteger idx, BOOL * stop) {
                
                if ([[obj objectForKey:@"id"]isEqualToString:self.tempSupportId]) {
                    
                    *stop = YES;
                }
                
                if (*stop) {
                    
                    [obj setObject:@"1" forKey:@"isPraise"];
                    
                    NSString *tempPraise = [obj objectForKey:@"praise"];
                    
                    NSInteger tempInt = [tempPraise integerValue];
                    
                    tempPraise = [NSString stringWithFormat:@"%ld",tempInt + 1];
                    
                    [obj setObject:tempPraise forKey:@"praise"];
                    
                    
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                    HumourTableViewCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
                    
                    cell.programDic = obj;
                    
                    [cell update];
                    
                    HumourModel *humourModel = [[HumourModel alloc]initWithDict:obj];
                    
                    [cell setHumourModel:humourModel];
                    
                    //如果 父控制器 想获取界面的数据 可以通过这个方法获取
                    if (self.pageCahnnelInfo) {
                        self.pageCahnnelInfo(_programInfoListArray,self.pageChannelID,pageNo);
                    }
                    
                }
                
            }];
            
            
        }
        
        _pubVodInteractiveCollection = nil;
        
    }

    
    
    if ([type isEqualToString:PUB_COMMON_PROGRAMINFOPLAYURL]) {
        
        
        NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        
        if (ret == 0) {
            
            NSMutableDictionary *playDic = [[NSMutableDictionary alloc]initWithDictionary:result];
            
            _currentPlayUrl = [playDic objectForKey:@"playUrl"];
            
            
            HumourModel *humourModel = [[HumourModel alloc] initWithDict:[_programInfoListArray objectAtIndex:_currentIndexPath.row]];
            
           
                [self playHumourProgramWithPlayUrl:humourModel];


        }else
        {
            if (_humourPlayer) {
                _humourPlayer.isCollectionInfo = NO;
            }
        }
        
    }

    
}


-(void)playHumourProgramWithPlayUrl:(HumourModel*)humourModel
{

    WS(wself);
    
    
    if (_humourPlayer) {
        [self colloctionCurrentHumourVideoInfo];
        [_humourPlayer.player playerClear];
        [_humourPlayer removeFromSuperview];
        _humourPlayer = nil;
        _humourPlayer = [[HumourPlayer alloc] initWithFrame:_currentCell.humourBV.bounds];
        [_humourPlayer.player setFilePath:_currentPlayUrl];
        _humourPlayer.titleLabel.text = humourModel.title;
        [_humourPlayer.player playAction];
        _humourPlayer.isMaskShowing = YES;
        [_currentCell.humourBV addSubview:_humourPlayer];
        _humourPlayer.humourModel = humourModel;

        
    }else
    {
        _humourPlayer = [[HumourPlayer alloc]initWithFrame:_currentCell.humourBV.bounds];
        [_humourPlayer.player setFilePath:_currentPlayUrl];
        _humourPlayer.titleLabel.text = humourModel.title;
        [_humourPlayer.player playAction];
        _humourPlayer.isMaskShowing = YES;
        [_currentCell.humourBV addSubview:_humourPlayer];
        _humourPlayer.humourModel = humourModel;
    }
    
    
    [_humourPlayer setIsfullScreen:NO];
    _humourPlayer.fullWindowBtn.tag = 22;
    [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_bigScreen_normal"] forState:UIControlStateNormal];
    [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_bigScreen_highlight"] forState:UIControlStateHighlighted];
    
    [_humourPlayer.fullWindowBtn addTarget:self action:@selector(fullWindowBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _humourPlayer.humourPlayerAction = ^(BOOL isAction) {
    if (isAction) {
        //添加信息采集
        [wself.timeManager start];
        
        NSString *TrackId = [[NSString alloc]init];
        
        NSString *TrackName = [[NSString alloc]init];
        
        //判断是否是由首页进入的一级界面
        if (self.trackId.length == 0) {
            TrackId = [NSString stringWithFormat:@"1,%@",self.pageChannelID];
        
            TrackName = [NSString stringWithFormat:@"轻松一刻,%@",self.pageChannelName];
        }
        else
        {
            TrackId = [NSString stringWithFormat:@"%@",self.trackId];
            
            
            TrackName = [NSString stringWithFormat:@"%@",self.trackName];
        }
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:humourModel.playID,@"itemId",humourModel.title,@"name",wself.enType,@"ENtype",[NSString stringWithFormat:@"%@,%@",TrackId,humourModel.playID],@"TrackId",[NSString stringWithFormat:@"%@,%@",TrackName,humourModel.title],@"TrackName", nil];
        wself.pubVodPlayModel = [PubVodPlayCollectionModel pubVodPlayWithDict:dictionary];
        wself.pubVodPlayModel.watchTime = [SystermTimeControl getNowServiceTimeString];

    }
        
        
        wself.humourPlayer.errorRefresh = ^()
        {
            wself.humourPlayer.errorMaskView.hidden = YES;
            
            [wself.humourPlayer.player playerClear];
            [wself.humourPlayer.player setFilePath:wself.currentPlayUrl];
            
            [wself.humourPlayer.player playAction];
            
            
        };

        
        
};
    
    
    
    
    [_contentTableView reloadData];
    
}

-(void)setupNavigationBar{
    WS(wself);
    [self setNaviBarTitle:@"频道详情"];
    if (_detailTitle.length>0) {
        [self setNaviBarTitle:_detailTitle];
    }
    
    UIButton_Block *backBtn = [[UIButton_Block alloc] init];
    [backBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
    backBtn.Click = ^(UIButton_Block *btn,NSString *title){
        [wself.navigationController popViewControllerAnimated:YES];
    };
    [self setNaviBarLeftBtn:backBtn];
    
    [self hideNaviBar:!_isNeedNavBar];
}

-(void)setupTableView{
    UIView *nilView = [UIView new];
    nilView.frame = CGRectZero;
    [self.view addSubview:nilView];
    self.view.userInteractionEnabled = YES;
    _programInfoListArray = [NSMutableArray array];
    
    CGRect tableViewReact;

    tableViewReact = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-HT_HIGHT_SECONDNAVIBAR-kTopSlimSafeSpace-kBottomSafeSpace);

    
    _contentTableView = [[UITableView alloc] initWithFrame:tableViewReact style:UITableViewStyleGrouped];
    _contentTableView.delegate = self;
    _contentTableView.dataSource = self;
    _contentTableView.showsVerticalScrollIndicator = YES;
    _contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _contentTableView.backgroundColor = App_background_color;
    [self.view addSubview:_contentTableView];
    
    _contentTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadData)];

//    _contentTableView.delaysContentTouches = NO;
    
    
    return;
}
-(void)reloadChannelDataWith:(NSString *)ChannelID withChannelPage:(NSInteger)channelPage
{
    
    self.pageChannelID = ChannelID;

    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request PubCommonProgramInfoListWithChannelId:self.pageChannelID withCurrentPage:pageNo withPageSize:10];
}

- (void)reloadCacheDataWith:(NSMutableArray *)cacheDataArr withChannelPage:(NSInteger)channelPage
{
    
    pageNo = channelPage;
        
    _programInfoListArray = [NSMutableArray arrayWithArray:cacheDataArr];
    
    [_contentTableView reloadData];
    
}

#pragma mark - tableView 代理方法
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1*kDeviceRate;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //整个header
    UIView *sectionHeaderView = [UIView new];
    sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, 1*kDeviceRate);
    [sectionHeaderView setBackgroundColor:App_white_color];
    
    return sectionHeaderView;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.programInfoListArray.count;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 247*kDeviceRate;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HumourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[HumourTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    NSDictionary *cellDic = [NSDictionary dictionary];
    
    cellDic = [self.programInfoListArray objectAtIndex:indexPath.row];
    
    cell.programDic = [self.programInfoListArray objectAtIndex:indexPath.row];
    
    [cell update];
    
    
    HumourModel *humourModel = [[HumourModel alloc]initWithDict:[self.programInfoListArray objectAtIndex:indexPath.row]];
    [cell setHumourModel:humourModel];
    
    cell.playHumourBtn.tag = indexPath.row;
    
    [cell.playHumourBtn addTarget:self action:@selector(startPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([[cellDic objectForKey:@"id"] isEqualToString:_currentPlayID]) {
        
        cell.displayHumourBV.hidden = YES;
        cell.humourBV.hidden = NO;
    }else
    {
        cell.displayHumourBV.hidden = NO;
        cell.humourBV.hidden = YES;
    }
    
  
    cell.supportBlock = ^(UIButton_Block *btn, NSString *name) {
        
        if(IsLogin){
            
            NSLog(@"我是点赞按钮");
            
            if (btn.selected == YES) {
                
                NSLog(@"已经点赞过了");
                
            }else
            {
                
                NSLog(@"好的，开始点赞");
                
                HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                
                self.tempSupportId = [btn.polyMedicDic objectForKey:@"id"];
                
                self.tempSupportName = [btn.polyMedicDic objectForKey:@"name"];
                
                [request PubCommonInsertPraiseWithCategory:@"0" withProgramId:[btn.polyMedicDic objectForKey:@"id"]];
                
                //信息采集
                NSString *systemTime = [SystermTimeControl getNowServiceTimeString];
                
               NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:systemTime,@"systemTime",self.tempSupportId,@"programId",self.tempSupportName,@"programName", nil];
                            
               _pubVodInteractiveCollection = [PubVodInteractiveCollectionModel pubVodInteractiveWithDict:dic];
                
            }

            
        }else{
            
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            [self actionPauseForVideo];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kNormalLogin;
            _isMovePage = NO;
            user.dismissBlock = ^(){
                _isMovePage = YES;
                [self actionPlayForVideo];
                
            };
            
            user.isLawDocBlock = ^(BOOL isDoc){

                if (isDoc) {
                    _isMovePage = YES;
                    [self actionPlayForVideo];
                    
                }
            };
            
            [self.navigationController pushViewController:user animated:YES];
        }
    };
    
    cell.shareBlock = ^(UIButton_Block *btn, NSString *name) {
        
        NSLog(@"我是分享按钮");
 
        _umengImageStr = [btn.polyMedicDic objectForKey:@"imageUrl"];
        
        self.umengContent = [NSString stringWithFormat:@"我正在看%@，太精彩了！安装#%@#手机客户端，电视点播免费看啦！快快安装一起看吧。",[btn.polyMedicDic objectForKey:@"name"],kAPP_NAME];
        
        _umengShareID = [btn.polyMedicDic objectForKey:@"id"];
        
        _umengShareTitle = [btn.polyMedicDic objectForKey:@"name"];
        
        
        self.appLink = [KaelTool getChannelId:[NSString stringWithFormat:@"%@",[btn.polyMedicDic objectForKey:@"id"]] andChannelTitle:[NSString stringWithFormat:@"%@",[btn.polyMedicDic objectForKey:@"name"]] withPlayType:CurrentPlayRelax];
        
        _shareViewVertical.hidden = NO;
        
    };
    
    return cell;
}


-(void)startPlayVideo:(UIButton *)sender
{
    _currentIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    
    HumourModel *humourModel = [[HumourModel alloc]initWithDict:[_programInfoListArray objectAtIndex:sender.tag]];
    
    _currentPlayID = humourModel.playID;
    
    
    if ([UIDevice currentDevice].systemVersion.floatValue>=8||[UIDevice currentDevice].systemVersion.floatValue<7) {
        _currentCell = (HumourTableViewCell *)sender.superview.superview.superview;
    }else{//ios7系统 UITableViewCell上多了一个层级UITableViewCellScrollView
        self.currentCell = (HumourTableViewCell *)sender.superview.superview.subviews;
    }
    
    
    if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
            nameAlert.tag = 12;
            [nameAlert show];
        });
        
    }else
    {
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request PubCommonProgramInfoPlayUrltWithProgramId:_currentPlayID];
    }
}

-(void)scrollCurrentPositionCell:(NSIndexPath*)currentIndexPathPosition
{
    _currentIndexPath = [NSIndexPath indexPathForRow:currentIndexPathPosition.row inSection:0];
    
    HumourModel *humourModel = [[HumourModel alloc]initWithDict:[_programInfoListArray objectAtIndex:currentIndexPathPosition.row]];
    
    _currentPlayID = humourModel.playID;
    
    
    
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request PubCommonProgramInfoPlayUrltWithProgramId:_currentPlayID];
}


-(void)toSmalllScreenForCurrentCell
{
    WS(wself);
    
    HumourTableViewCell *currentCell = (HumourTableViewCell *)[_contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_currentIndexPath.row inSection:0]];
    
    [self colloctionCurrentHumourVideoInfo];

    [_humourPlayer removeFromSuperview];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
    [UIView animateWithDuration:0.2 animations:^{
        _humourPlayer.transform = CGAffineTransformIdentity;

        _humourPlayer.frame = currentCell.humourBV.bounds;
        [_humourPlayer.player changeInterfaceOrientation:currentCell.humourBV.bounds];
        [_humourPlayer setIsfullScreen:NO];
        [currentCell.humourBV addSubview:_humourPlayer];
        [_humourPlayer.topBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.height.mas_equalTo(45*kDeviceRate);
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
        }];
        
        [_humourPlayer.playerLoadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.width.mas_equalTo(_humourPlayer.player.frameView.mas_width);
            make.height.mas_equalTo(_humourPlayer.player.frameView.mas_height);
        }];

        [_humourPlayer.mainPlayButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_humourPlayer.player.frameView.mas_centerX);
            make.centerY.mas_equalTo(_humourPlayer.player.frameView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(55*kDeviceRate, 55*kDeviceRate));
        }];
        
        [_humourPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0*kDeviceRate);
            make.height.mas_equalTo(45*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.topBarView.mas_left).mas_equalTo(10*kDeviceRate);
            make.right.mas_equalTo(_humourPlayer.topBarView.mas_right);
        }];
        [_humourPlayer.bottomBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.height.mas_equalTo(45*kDeviceRate);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
        }];
        
        [_humourPlayer.fullWindowBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.bottomBarView.mas_bottom);
            make.height.mas_equalTo(35*kDeviceRate);
            make.right.mas_equalTo(_humourPlayer.bottomBarView.mas_right);
            make.width.mas_equalTo(35*kDeviceRate);
        }];
        
        [_humourPlayer.playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.bottomBarView.mas_bottom).offset(-5*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.bottomBarView.mas_left);
            make.width.mas_equalTo(35*kDeviceRate);
            make.height.mas_equalTo(35*kDeviceRate);
        }];
        
        _humourPlayer.fullWindowBtn.tag = 22;
        [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_bigScreen_normal"] forState:UIControlStateNormal];
        [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_bigScreen_highlight"] forState:UIControlStateHighlighted];
        [_humourPlayer.fullWindowBtn addTarget:self action:@selector(fullWindowBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_humourPlayer.endMaskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.width.mas_equalTo(_humourPlayer.player.frameView.mas_width);
            make.height.mas_equalTo(_humourPlayer.player.frameView.mas_height);
        }];

        
        [_humourPlayer.refreshBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.endMaskView.mas_bottom).offset(-100*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.endMaskView.mas_left).offset(150*kDeviceRate);
            make.width.mas_equalTo(23*kDeviceRate);
            make.height.mas_equalTo(25*kDeviceRate);
            
            
        }];
        [_humourPlayer.refreshLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.refreshBtn.mas_bottom).offset(10*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.refreshBtn.mas_right).offset(10*kDeviceRate);
            make.width.mas_equalTo(50*kDeviceRate);
            make.height.mas_equalTo(45*kDeviceRate);
        }];

        
        [_humourPlayer.errorMaskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.width.mas_equalTo(_humourPlayer.player.frameView.mas_width);
            make.height.mas_equalTo(_humourPlayer.player.frameView.mas_height);
        }];

        
        
        
        
        _humourPlayer.refreshBlock = ^()
        {
            wself.humourPlayer.endMaskView.hidden = YES;

            [wself.humourPlayer.player playerClear];
            [wself.humourPlayer.player setFilePath:wself.currentPlayUrl];
            
            [wself.humourPlayer.player playAction];
            
            
        };
        
        
        
        
               
        
    }];
    
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
     
    
    
}






//弹出列表方法presentSnsIconSheetView需要设置delegate为self
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}






#pragma mark - ------- app 进入前后台


-(void)actionPlayForVideo
{
    if (_humourPlayer.isByUserPause) {
        
        return;
    }else
    {
    [_humourPlayer.playButton setTag:ENGINE_STATUS_PAUSE];
    [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_pause"] forState:UIControlStateNormal];
    [_humourPlayer.player play];
    }
}


-(void)actionPauseForVideo
{
    [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_play"] forState:UIControlStateNormal];
    [_humourPlayer.playButton setTag:ENGINE_STATUS_PLAY];
    [_humourPlayer.player pause];

}




-(void)appEnterBackGround{
    if (_humourPlayer)
    {
        _humourPlayer.mainPlayButton.hidden = YES;

        _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];
        
        [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_play"] forState:UIControlStateNormal];
        [_humourPlayer.playButton setTag:ENGINE_STATUS_PLAY];
        [_humourPlayer.player pause];
    }
}






-(void)appEnterForeground{
    if (_humourPlayer) {
        
        _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];
        
        _humourPlayer.mainPlayButton.hidden = YES;
        if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
            [_humourPlayer.playButton setTag:ENGINE_STATUS_PLAY];
            [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_play"] forState:UIControlStateNormal];
            [_humourPlayer.player pause];
        }else
        {
            if (_humourPlayer.isByUserPause == YES) {
                
                return;
            }else
            {
            [_humourPlayer.playButton setTag:ENGINE_STATUS_PAUSE];
            [_humourPlayer.playButton setImage:[UIImage imageNamed:@"btn_hum_player_pause"] forState:UIControlStateNormal];
            [_humourPlayer.player play];
            }
        }
        
        
    }
    
}


-(void)fullWindowBtnClick:(UIButton*)sender
{
    if (sender.tag == 22) {
        
        [self toFullScreenWithInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
        [_humourPlayer setIsfullScreen:YES];
        
    }else
    {
        
        [self toSmalllScreenForCurrentCell];
        [_humourPlayer setIsfullScreen:NO];
    }
    
    
}

-(void)toFullScreenWithInterfaceOrientation:(UIInterfaceOrientation )interfaceOrientation{
    
    [_humourPlayer removeFromSuperview];
    
    WS(wself);
    [UIView animateWithDuration:0.2 animations:^{
        //        [[UIApplication sharedApplication]setStatusBarHidden:YES];
        _humourPlayer.transform = CGAffineTransformIdentity;
        if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft) {
            _humourPlayer.transform = CGAffineTransformMakeRotation(-M_PI_2);
            
            
        }else if(interfaceOrientation==UIInterfaceOrientationLandscapeRight){
            _humourPlayer.transform = CGAffineTransformMakeRotation(M_PI_2);
            
        }
        
        _humourPlayer.frame = CGRectMake(0, 0, kDeviceWidth,KDeviceHeight);
        [_humourPlayer.player changeInterfaceOrientation:CGRectMake(0, 0, KDeviceHeight, kDeviceWidth)];
        [_humourPlayer setIsfullScreen:YES];
        [_humourPlayer.topBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(45*kDeviceRate);
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(KDeviceHeight);
        }];

        [_humourPlayer.bottomBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(45*kDeviceRate);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(KDeviceHeight);
        }];
        
        
        [_humourPlayer.mainPlayButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_humourPlayer.player.frameView.mas_centerX);
            make.centerY.mas_equalTo(_humourPlayer.player.frameView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(55*kDeviceRate, 55*kDeviceRate));
        }];
        
        
        [_humourPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0*kDeviceRate).offset(20);
            make.height.mas_equalTo(45*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left).offset(28*kDeviceRate+kTopSafeSpace);
            make.width.mas_equalTo(_humourPlayer.player.frameView.mas_width).multipliedBy(0.6);
        }];
        
        [_humourPlayer.returnBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top).offset(25*kDeviceRate);
            make.height.mas_equalTo(35*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left).offset(kTopSafeSpace);
            make.width.mas_equalTo(35*kDeviceRate);
        }];
        
       
        
        
        
        [_humourPlayer.shareBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0*kDeviceRate).offset(20);
            make.height.mas_equalTo(45*kDeviceRate);
            make.right.mas_equalTo(_humourPlayer.topBarView.mas_right).offset(-20*kDeviceRate);
            make.width.mas_equalTo(150*kDeviceRate);
        }];
        
        _humourPlayer.fullWindowBtn.tag = 33;
        [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_smallScreen_normal"] forState:UIControlStateNormal];
        [_humourPlayer.fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_smallScreen_highlight"] forState:UIControlStateHighlighted];

        
        [_humourPlayer.fullWindowBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.bottomBarView.mas_bottom);
            make.height.mas_equalTo(35*kDeviceRate);
            make.right.mas_equalTo(_humourPlayer.bottomBarView.mas_right).offset(-kTopSafeSpace);
            make.width.mas_equalTo(35*kDeviceRate);
        }];
        [_humourPlayer.fullWindowBtn addTarget:self action:@selector(fullWindowBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [_humourPlayer.playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.bottomBarView.mas_bottom).offset(-5*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.bottomBarView.mas_left).offset(kTopSafeSpace);
            make.width.mas_equalTo(35*kDeviceRate);
            make.height.mas_equalTo(35*kDeviceRate);
        }];
        
        
        [_humourPlayer.playerLoadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
        }];
        
        [_humourPlayer.endMaskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
        }];
        
        [_humourPlayer.refreshBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.endMaskView.mas_bottom).offset(-100*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.endMaskView.mas_left).offset(150*kDeviceRate);
            make.width.mas_equalTo(23*kDeviceRate);
            make.height.mas_equalTo(25*kDeviceRate);
            
            
        }];
        
        
        [_humourPlayer.refreshLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(_humourPlayer.refreshBtn.mas_bottom).offset(10*kDeviceRate);
            make.left.mas_equalTo(_humourPlayer.refreshBtn.mas_right).offset(10*kDeviceRate);
            make.width.mas_equalTo(50*kDeviceRate);
            make.height.mas_equalTo(45*kDeviceRate);
        }];
        
        [_humourPlayer.errorMaskView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
        }];
        
        
       

        
        
        
        [_humourPlayer.shareView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.player.frameView.mas_top);
            make.left.mas_equalTo(_humourPlayer.player.frameView.mas_left);
            make.right.mas_equalTo(_humourPlayer.player.frameView.mas_right);
            make.bottom.mas_equalTo(_humourPlayer.player.frameView.mas_bottom);
        }];
        
        
        
       
        
        
        
        [_humourPlayer.progressTimeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_humourPlayer.player.frameView.mas_centerX);
            make.centerY.mas_equalTo(_humourPlayer.player.frameView.mas_centerY);
            make.height.mas_equalTo(70*kRateSize);
            make.width.mas_equalTo(130*kRateSize);
            
        }];
        
        
        
        [_humourPlayer.progressDirectionIV mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(_humourPlayer.progressTimeView.mas_centerY).offset(-15*kRateSize);
            make.centerX.mas_equalTo(_humourPlayer.progressTimeView.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
        }];
        
        
        
        
        [_humourPlayer.progressTimeLable_top mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(_humourPlayer.progressTimeView.mas_bottom).offset(-13*kRateSize);
            make.left.mas_equalTo(_humourPlayer.progressTimeView.mas_left).offset(5*kRateSize);
            make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
            
        }];
        
        
        [_humourPlayer.progressTimeLable_bottom mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(_humourPlayer.progressTimeView.mas_bottom).offset(-13*kRateSize);
            make.right.mas_equalTo(_humourPlayer.progressTimeView.mas_right).offset(-5*kRateSize);
            make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
            
        }];
        
        
        
        
        
        
        
//        亮度视图尺寸****************
        
        [_humourPlayer.brightnessVODView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.mas_equalTo(_humourPlayer.player.frameView.mas_centerX);
            make.centerY.mas_equalTo(_humourPlayer.player.frameView.mas_centerY);
            make.height.mas_equalTo(125*kRateSize);
            make.width.mas_equalTo(125*kRateSize);
            
        }];
        
        [_humourPlayer.brightnessVODProgress mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_humourPlayer.brightnessVODView.mas_bottom);
            make.centerX.mas_equalTo(_humourPlayer.brightnessVODView);
            make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
        }];
        
        
        [[UIApplication sharedApplication].keyWindow addSubview:_humourPlayer];

        
        _humourPlayer.returnBlock = ^{
            
            [wself toSmalllScreenForCurrentCell];
        };
    }];
    
    
    
    _humourPlayer.shareBlock = ^{
        
        wself.humourPlayer.shareView.delegate = wself;
        wself.humourPlayer.shareView.hidden = !wself.humourPlayer.shareView.hidden;
        wself.humourPlayer.shareView.installedWX = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
        wself.humourPlayer.shareView.installedQQ = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ];

    };
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];

}



#pragma mark scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView ==_contentTableView){
        if (_humourPlayer==nil) {
            return;
        }
        
        if (_contentTableView.superview) {
            CGRect rectInTableView = [_contentTableView rectForRowAtIndexPath:_currentIndexPath];
            CGRect rectInSuperview = [_contentTableView convertRect:rectInTableView toView:[_contentTableView superview]];
            if (rectInSuperview.origin.y<-self.currentCell.humourBV.frame.size.height||rectInSuperview.origin.y>KDeviceHeight-NaviBarHeight-kTopSafeSpace-kBottomSafeSpace) {//往上拖动
                

            [self toSmalllScreenForCurrentCellIsFullWindowRoation];

                
            }else{
                if ([self.currentCell.humourBV.subviews containsObject:_humourPlayer]) {
                    NSLog(@"走的这个");
                }else{
                    [self toSmalllScreenForCurrentCell];
                }
            }
            
            
        }
        
    }
}


-(void)toSmalllScreenForCurrentCellIsFullWindowRoation
{
    
        [_humourPlayer.player playerClear];
        [_humourPlayer removeFromSuperview];

        HumourTableViewCell *currentCell = (HumourTableViewCell *)[_contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_currentIndexPath.row inSection:0]];
        
        currentCell.humourBV.hidden = YES;
    
        currentCell.displayHumourBV.hidden = NO;
    _currentPlayID = 0;
//    [_contentTableView reloadData];
        
}


#pragma  mark - tableViewSetionHeaderView的点击事件
- (void)programClick:(UITapGestureRecognizer *)tap{
    
    NSLog(@"点击了播放");
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark 采集时间的时长

-(void)countingTo:(NSTimeInterval)time timeToShowString:(NSString*)timeToShowString
{
    //    label.text =[NSString stringWithFormat:@"%.3f/%@",time,timeToShowString];
    _watchDuration = [NSString stringWithFormat:@"%.0f",time];
}


#pragma mark - VTMagicReuseProtocol
- (void)vtm_prepareForReuse
{
    NSLog(@"clear old data if needed:%@", self.pageChannelID);
    
    _programInfoListArray = nil;
    
    self.pageChannelID = nil;
    
    self.pageChannelName = nil;
    
    pageNo = 1;
    
    [_contentTableView reloadData];
    
    
}

- (void)loadData
{
    
    [self.contentTableView.mj_footer endRefreshing];
    
    [self reloadChannelDataWith:self.pageChannelID withChannelPage:pageNo];
    
}

- (void)setupShareView
{
    
    WS(wself);
    _shareViewVertical = [[ShareViewVertical alloc]init];
    _shareViewVertical.hidden = YES;
    [_shareViewVertical setInstalledQQ:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]];
    [_shareViewVertical setInstalledWX:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]];
    [wself.view addSubview:_shareViewVertical];
    
    [_shareViewVertical mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.view);
    }];
    
    _shareViewVertical.returnBlock = ^(NSInteger tag){

        NSString *tempLink;
        
        if (!isEmptyStringOrNilOrNull(wself.appLink)) {
            tempLink = wself.appLink;
        }else{
            tempLink = @"";
        }
     
        switch (tag) {
            case 0:
            {
                
                //                [wself WechatSessionShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringPM_IMGFormat(wself.umengImageStr) shareType:Wechat currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.isNeedNavBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengShareID,@"shareID",wself.umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"0",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        
                    }
                };
                
            }
                break;
            case 1:
            {
                
//                                [wself wechatTimelineShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringPM_IMGFormat(wself.umengImageStr) shareType:WechatZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.isNeedNavBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengShareID,@"shareID",wself.umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"0",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                    }
                };
            }
                break;
            case 2:
                
            {
                
                //                [wself tencetQQSahreAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringPM_IMGFormat(wself.umengImageStr) shareType:QQ currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.isNeedNavBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengShareID,@"shareID",wself.umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"0",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                    }
                };
                
            }
                break;
            case 3:
                
            {
                
                //                [wself QQZoneShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringPM_IMGFormat(wself.umengImageStr) shareType:QQZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.isNeedNavBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengShareID,@"shareID",wself.umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"0",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                    }
                };
                
            }
                break;
            default:
                break;
        }
        
    };
}

- (void)getLocationPubProgramId:(NSString *)pubProgramId
{
    self.chooseProgramId = pubProgramId;
    
    if (_programInfoListArray) {
        [_programInfoListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([self.chooseProgramId isEqualToString:[dic objectForKey:@"id"]]) {
                
                [self.contentTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idx
                                                               
                                                                                 inSection:0]
                 
                                             atScrollPosition:UITableViewScrollPositionTop
                 
                                                     animated:YES];
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
 
                _currentCell = [self.contentTableView cellForRowAtIndexPath:indexPath];
                
                [self scrollCurrentPositionCell:indexPath];
                
                NSLog(@"我就是你在外边选中的cell");
                
                self.chooseProgramId = nil;
                
            }
            
        }];
    }

    
}

@end
