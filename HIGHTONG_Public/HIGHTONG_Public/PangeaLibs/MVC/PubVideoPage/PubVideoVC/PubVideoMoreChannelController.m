//
//  PubVideoMoreChannelController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/12.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubVideoMoreChannelController.h"
#import "PubVideoMoreChannelCell.h"

@interface PubVideoMoreChannelController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

{
    NSMutableArray *_pubChannelArray;//频道列表
}

@property (strong, nonatomic)UICollectionView *moreItemCollectionView;

@end

@implementation PubVideoMoreChannelController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
    _pubChannelArray = [NSMutableArray array];
    
    _pubChannelArray = _pubChannelList;
    
    [self setNaviBarTitle:@"全部分类"];
    
    [self moreItemCollectionViewSetup];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -- UICollectionViewDataSource

//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _pubChannelArray.count;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"MoreItemCollectionView";
    PubVideoMoreChannelCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *tempDic = [_pubChannelArray objectAtIndex:indexPath.row];
    
    if ([tempDic objectForKey:@"name"]) {
        cell.linkName.text = [tempDic objectForKey:@"name"];
    }

    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout

//定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(79*kDeviceRate, 40*kDeviceRate);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
//    return UIEdgeInsetsMake(5*kDeviceRate, 5*kDeviceRate, 5*kDeviceRate, 5*kDeviceRate);
}

//头部加载
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *view=[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];
    view.frame = CGRectMake(0,0,kDeviceWidth-30*kDeviceRate, 50*kDeviceRate);
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 25*kDeviceRate, kDeviceWidth-30*kDeviceRate, 15*kDeviceRate)];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        label.text = @"点击切换分类";
        label.font = HT_FONT_THIRD;
        label.textColor = HT_COLOR_FONT_ASSIST;
        label.backgroundColor = App_white_color;
    }
    
    view.backgroundColor = App_white_color;
    
    [view addSubview:label];

    return view;
}

//头部试图的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(kDeviceWidth-30*kDeviceRate, 55*kDeviceRate);
}

#pragma mark --UICollectionViewDelegate

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"item======%ld",(long)indexPath.item);
    NSLog(@"row=======%ld",(long)indexPath.row);
    NSLog(@"section===%ld",(long)indexPath.section);
    
    NSDictionary *tempDic = [_pubChannelArray objectAtIndex:indexPath.row];
    
    self.backValue([tempDic objectForKey:@"id"]);
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

//当cell高亮时返回是否高亮
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    PubVideoMoreChannelCell* cell = (PubVideoMoreChannelCell *)[colView cellForItemAtIndexPath:indexPath];
    //设置(Highlight)高亮下的颜色
    [cell.backgroundImg setBackgroundColor:App_background_color];
}

- (void)collectionView:(UICollectionView *)colView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    PubVideoMoreChannelCell* cell = (PubVideoMoreChannelCell *)[colView cellForItemAtIndexPath:indexPath];
    //设置(Nomal)正常状态下的颜色
    [cell.backgroundImg setBackgroundColor:App_white_color];
}



#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
    
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"返回首页");
    
}

- (void)moreItemCollectionViewSetup {
    
    //确定是水平滚动，还是垂直滚动
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    flowLayout.minimumInteritemSpacing = 5*kDeviceRate;
    flowLayout.minimumLineSpacing = 10*kDeviceRate;
    
    self.moreItemCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(15*kDeviceRate, 44+kTopSlimSafeSpace, kDeviceWidth-30*kDeviceRate, KDeviceHeight-44-kTopSlimSafeSpace-kBottomSafeSpace) collectionViewLayout:flowLayout];
    
    
    self.moreItemCollectionView.dataSource=self;
    self.moreItemCollectionView.delegate=self;
    
    
    //注册Cell，必须要有
    [self.moreItemCollectionView registerClass:[PubVideoMoreChannelCell class] forCellWithReuseIdentifier:@"MoreItemCollectionView"];
    
    [self.moreItemCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionReusableView"];
    
    [self.view addSubview:self.moreItemCollectionView];
    
    [self.view setBackgroundColor:App_white_color];
    
    [self.moreItemCollectionView setBackgroundColor:App_white_color];
    
}


@end
