//
//  HumourModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourModel.h"

@implementation HumourModel


- (instancetype)initWithDict:(NSDictionary *)dict
 {
         self = [super init];
        if (self) {
                 // 第一个参数是字典的数值
                 // 第二个参数是类的属性
        [self setValue:dict[@"imageUrl"] forKeyPath:@"coverURL"];
        [self setValue:dict[@"name"] forKeyPath:@"title"];
        [self setValue:dict[@"date"] forKeyPath:@"dateStr"];
        [self setValue:dict[@"playTimes"] forKeyPath:@"playCount"];
        [self setValue:dict[@"duration"] forKeyPath:@"lengthStr"];
        [self setValue:dict[@"id"] forKeyPath:@"playID"];
        [self setValue:dict[@"favorites"] forKeyPath:@"favorites"];
        [self setValue:dict[@"praise"] forKeyPath:@"praise"];
        [self setValue:dict[@"isPraise"] forKeyPath:@"isPraise"];
        [self setValue:dict[@"isFavorite"] forKeyPath:@"isFavorite"];
        [self setValue:dict[@"shareUrl"] forKeyPath:@"shareUrl"];

             }
         return self;
}

@end
