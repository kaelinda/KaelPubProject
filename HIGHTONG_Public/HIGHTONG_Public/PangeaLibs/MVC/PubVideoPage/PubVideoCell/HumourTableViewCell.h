//
//  HumourTableViewCell.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/12.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HumourModel.h"
//#import "UGCProgramControlView.h"
#import "UGCPlayer.h"
#import "UIButton_Block.h"
typedef void (^PubVideoProgramCell_TO_Share)(UIButton_Block*,NSString*);

typedef void (^PubVideoProgramCell_TO_Support)(UIButton_Block*,NSString*);
typedef void(^Humour_block)(UIButton *button);
typedef void(^FullWindow_block)(UIButton *button);
@interface HumourTableViewCell : UITableViewCell

@property(nonatomic,copy)PubVideoProgramCell_TO_Share shareBlock;

@property(nonatomic,copy)PubVideoProgramCell_TO_Support supportBlock;
@property (nonatomic, copy) Humour_block humourBlock;
@property (nonatomic, copy) FullWindow_block fullWindowBlock;
@property (nonatomic, strong) UIImageView *cellBottomIV;
@property (nonatomic, strong) UIImageView *humourBV;
@property (nonatomic, strong) UIButton *fullWindowBtn;
@property (nonatomic, strong) HumourModel *humourModel;
@property (nonatomic, strong) UIImageView *displayHumourBV;
@property (nonatomic, strong) UILabel *disPlayTitleLabel;
@property (nonatomic, strong) UILabel *disPlayDateLabel;
@property (nonatomic, strong) UIImageView *disPlayDurationIV;
@property (nonatomic, strong) UILabel *disPlayDurationLabel;

@property (nonatomic, strong) NSMutableDictionary *programDic;
/**
 *  @author Dirk
 *
 *  @brief 分享按钮
 */
@property (nonatomic, strong) UIButton_Block *shareBtn;

/**
 *  @author Dirk
 *
 *  @brief 点赞按钮
 */
@property (nonatomic, strong) UIButton_Block *supportBtn;
/**
 *  @author Dirk
 *
 *  @brief 按钮分割线
 */
@property (nonatomic, strong) UIImageView *lineImageView;

@property (nonatomic, strong) UGCPlayer *ugcPlayer;


@property (nonatomic, strong) UIButton *playHumourBtn;

- (void)update;

@end
