//
//  HumourCircleView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/15.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourCircleView.h"
#define PI 3.14159265358979323846

@implementation HumourCircleView

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if(self) {}
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(void)drawRect:(CGRect)rect{
    //An opaque type that represents a Quartz 2D drawing environment.
    //一个不透明类型的Quartz 2D绘画环境,相当于一个画布,你可以在上面任意绘画
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //void CGContextAddArc(CGContextRef c,CGFloat x, CGFloat y,CGFloat radius,CGFloat startAngle,CGFloat endAngle, int clockwise)
    //1弧度＝180°/π （≈57.3°） 度＝弧度×180°/π 360°＝360×π/180 ＝2π 弧度
    //x,y为圆点坐标，radius半径，startAngle为开始的弧度，endAngle为 结束的弧度，clockwise 0为顺时针，1为逆时针。
    
    //CGContextDrawPath方法的第2个参数：
    //kCGPathFill:填充;
    //kCGPathEOFill表示用奇偶规则,
    //kCGPathStroke路径,
    //kCGPathFillStroke路径填充,
    //kCGPathEOFillStroke表示描线，不是填充
    
//    //画扇形
//    CGContextMoveToPoint(context, 60, 60);//移动画笔到指定坐标点
//    UIColor* aColor = [UIColor colorWithRed:1 green:0.0 blue:0 alpha:1];//红色
//    CGContextSetFillColorWithColor(context, aColor.CGColor);//填充颜色
//    CGContextAddArc(context, 60, 60, 40, 0, 1.5*PI, 0); //添加一个圆
//    CGContextDrawPath(context, kCGPathFill); //填充路径
//    
//    //画圆
//    UIColor* bColor = [UIColor colorWithRed:1 green:0.0 blue:0 alpha:1];//红色
//    CGContextSetFillColorWithColor(context, bColor.CGColor);//填充颜色
//    CGContextAddArc(context, 60, 160, 40, 0, 2*PI, 0); //添加一个圆
//    CGContextDrawPath(context, kCGPathFill); //填充路径
    
    //画弧线
//    CGContextSetRGBStrokeColor(context,0,0,1,1.0);//画笔线的颜色：这里是蓝色
    //也可以使用CGContextSetStrokeColorWithColor方法：就可以用UIColor颜色参数
    CGContextSetStrokeColorWithColor(context, App_selected_color.CGColor);
    CGContextSetLineWidth(context, 5.5);//线的宽度
    CGContextAddArc(context, 60, 60, 41, 0, 2*PI, 0); //添加一个圆（弧）：这里起点弧为1.5*PI，终点弧为0*PI
    CGContextDrawPath(context, kCGPathStroke); //绘制路径
    
}



@end
