//
//  HumourModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HumourModel : NSObject
@property (nonatomic, strong) NSString * coverURL;
@property (nonatomic, strong) NSString * descriptionDe;
@property (nonatomic, strong) NSString  *lengthStr;
@property (nonatomic, strong) NSString * playUrl;
@property (nonatomic, assign) NSInteger  playCount;
@property (nonatomic, strong) NSString * playersize;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * dateStr;
@property (nonatomic, strong) NSString *playID;
@property (nonatomic, strong) NSString *favorites;
@property (nonatomic, strong) NSString *praise;
@property (nonatomic, strong) NSString *isPraise;
@property (nonatomic, strong) NSString *isFavorite;
@property (nonatomic, strong) NSString *shareUrl;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
