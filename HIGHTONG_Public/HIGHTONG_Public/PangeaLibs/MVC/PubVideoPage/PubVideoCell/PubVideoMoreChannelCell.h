//
//  PubVideoMoreChannelCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/12.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PubVideoMoreChannelCell : UICollectionViewCell

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic, strong)UIImageView *backgroundImg;//cell背景框

@property (nonatomic, strong)UILabel *linkName;//台名

@end
