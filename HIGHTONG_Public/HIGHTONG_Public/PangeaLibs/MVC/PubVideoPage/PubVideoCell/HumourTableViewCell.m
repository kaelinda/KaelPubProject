//
//  HumourTableViewCell.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/12.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourTableViewCell.h"

@implementation HumourTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.programDic = [[NSMutableDictionary alloc]init];;

        [self setupViews];
        
        
    }
    
    return self;
}



//-(void)setProgramDic:(NSMutableDictionary *)programDic
//{
//    _programDic = programDic;
//}


-(void)setupViews
{
    __weak __typeof(&*self)weakSelf = self;
//    self.programDic = [[NSMutableDictionary alloc]init];;
//    self.programDic = _programDic;
    self.contentView.userInteractionEnabled = YES;
    [self.contentView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.3]];
    _humourBV = [[UIImageView alloc]init];
    _humourBV.userInteractionEnabled = YES;
    [_humourBV setFrame:CGRectMake(0, 0, kDeviceWidth, (247-40)*kDeviceRate)];
    
    [self.contentView addSubview:_humourBV];
    
    
    _displayHumourBV = [[UIImageView alloc]init];
    _displayHumourBV.userInteractionEnabled = YES;
    [_displayHumourBV setBackgroundColor:[UIColor clearColor]];
    [_displayHumourBV setFrame:CGRectMake(0, 0, kDeviceWidth, (247-40)*kDeviceRate)];
    
    [self.contentView addSubview:_displayHumourBV];
    
    
    
    self.supportBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    self.supportBtn.frame = CGRectMake(kDeviceWidth*2/3, self.frame.size.height -50*kDeviceRate, kDeviceWidth/3, 50*kDeviceRate);
    
    [self addSubview:self.supportBtn];
    
    [self.supportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(40*kDeviceRate));
        make.width.equalTo(@(kDeviceWidth/2-0.5*kDeviceRate));
    }];
    
    
    [self.supportBtn setImage:[UIImage imageNamed:@"btn_hum_player_support_normal"] forState:UIControlStateNormal];
    [self.supportBtn setImage:[UIImage imageNamed:@"btn_hum_player_support_selected"] forState:UIControlStateSelected];
    
    
    
    self.supportBtn.imageEdgeInsets = UIEdgeInsetsMake(2.5*kDeviceRate, -2.5*kDeviceRate, 2.5*kDeviceRate, 40);
    [self.supportBtn setTitle:@"" forState:UIControlStateNormal];
    
    self.supportBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    
    [self.supportBtn setTitleColor:HT_COLOR_FONT_SECOND forState:UIControlStateNormal];
    
    [self.supportBtn setTitleColor:HT_COLOR_FONT_SECOND forState:UIControlStateHighlighted];
    
    [self.supportBtn setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    self.supportBtn.titleLabel.font = HT_FONT_THIRD;
    
    
    self.shareBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    self.shareBtn.frame = CGRectMake(kDeviceWidth*2/3, 200*kDeviceRate, kDeviceWidth/3, 50*kDeviceRate);
    
    [self addSubview:self.shareBtn];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(40*kDeviceRate));
        make.width.equalTo(@(kDeviceWidth/2-0.5*kDeviceRate));
    }];
    
    
    [self.shareBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_normal"] forState:UIControlStateNormal];
    [self.shareBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_highlight"] forState:UIControlStateHighlighted];
    
    
    self.shareBtn.imageEdgeInsets = UIEdgeInsetsMake(2.5*kDeviceRate, -2.5*kDeviceRate, 2.5*kDeviceRate, 40);
    [self.shareBtn setTitle:@"分享" forState:UIControlStateNormal];
    
    self.shareBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    
    [self.shareBtn setTitleColor:HT_COLOR_FONT_SECOND forState:UIControlStateNormal];
    
    [self.shareBtn setTitleColor:HT_COLOR_FONT_SECOND forState:UIControlStateHighlighted];
    
    [self.shareBtn setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    
    self.shareBtn.titleLabel.font = HT_FONT_THIRD;
    
    _lineImageView = [[UIImageView alloc]init];
    [self addSubview:_lineImageView];
    _lineImageView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _lineImageView.image = [UIImage imageNamed:@"img_hum_separorline"];
    _lineImageView.contentMode = UIViewContentModeScaleAspectFit;


    [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.supportBtn.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.shareBtn.mas_left);
        make.height.equalTo(@(40*kDeviceRate));
    }];
    
   
    
    
    
    _disPlayDateLabel = [[UILabel alloc]init];
    [_disPlayDateLabel setBackgroundColor:[UIColor clearColor]];
    _disPlayDateLabel.textAlignment = NSTextAlignmentCenter;
    _disPlayDateLabel.textColor = UIColorFromRGB(0xffffff);
    _disPlayDateLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
    [_displayHumourBV addSubview:_disPlayDateLabel];
    
    
    [_disPlayDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf.displayHumourBV.mas_right).offset(-10*kDeviceRate);
        make.width.mas_equalTo(120*kDeviceRate);
        make.height.mas_equalTo(35*kDeviceRate);
        make.top.mas_equalTo(weakSelf.displayHumourBV.mas_top).offset(10*kDeviceRate);
    }];
    
    
    _disPlayTitleLabel = [[UILabel alloc]init];
    [_disPlayTitleLabel setBackgroundColor:[UIColor clearColor]];
    _disPlayTitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _disPlayTitleLabel.textAlignment = NSTextAlignmentLeft;
    _disPlayTitleLabel.textColor = UIColorFromRGB(0xffffff);
    _disPlayDateLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
    [_displayHumourBV addSubview:_disPlayTitleLabel];
    
    
    [_disPlayTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.displayHumourBV.mas_left).offset(12*kDeviceRate);
        make.right.mas_equalTo(weakSelf.disPlayDateLabel.mas_left).offset(0*kDeviceRate);
        make.height.mas_equalTo(35*kDeviceRate);
        make.top.mas_equalTo(weakSelf.displayHumourBV.mas_top).offset(10*kDeviceRate);
    }];

    
    
    _disPlayDurationIV = [[UIImageView alloc]init];
    _disPlayDurationIV.userInteractionEnabled = YES;
    [_disPlayDurationIV setImage:[UIImage imageNamed:@"img_hum_player_duration"]];
    [_displayHumourBV addSubview:_disPlayDurationIV];
    
    
    [_disPlayDurationIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(62*kDeviceRate);
        make.height.mas_equalTo(24*kDeviceRate);
        make.right.mas_equalTo(_displayHumourBV.mas_right).offset(-12*kDeviceRate);
        make.bottom.mas_equalTo(weakSelf.displayHumourBV.mas_bottom).offset(-12*kDeviceRate);
    }];
    

    _disPlayDurationLabel = [[UILabel alloc]init];
    [_disPlayDurationLabel setBackgroundColor:[UIColor clearColor]];
    _disPlayDurationLabel.textAlignment = NSTextAlignmentRight;
    _disPlayDurationLabel.textColor = UIColorFromRGB(0xffffff);
    _disPlayDurationLabel.font = [UIFont systemFontOfSize:10*kDeviceRate];
    [_disPlayDurationIV addSubview:_disPlayDurationLabel];
    
    
    [_disPlayDurationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf.disPlayDurationIV.mas_right).offset(-12*kDeviceRate);
        make.width.mas_equalTo(50*kDeviceRate);
        make.height.mas_equalTo(24*kDeviceRate);
        make.bottom.mas_equalTo(weakSelf.disPlayDurationIV.mas_bottom).offset(0*kDeviceRate);
    }];
    
        _playHumourBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playHumourBtn setImage:[UIImage imageNamed:@"btn_hum_player_action"] forState:UIControlStateNormal];
        [_displayHumourBV addSubview:_playHumourBtn];
    
        [_playHumourBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(weakSelf.displayHumourBV.mas_centerX);
            make.centerY.mas_equalTo(weakSelf.displayHumourBV.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(55*kDeviceRate, 55*kDeviceRate));
        }];
    
    
}

- (void)update
{
    
    WS(wself);

    if ([self.programDic objectForKey:@"isPraise"]||[[self.programDic objectForKey:@"isPraise"]integerValue] == 1) {
        
        self.supportBtn.selected = YES;
        
    }else
    {
        
        self.supportBtn.selected = NO;
        
    }
    
    [self.supportBtn setTitle:[self.programDic objectForKey:@"praise"] forState:UIControlStateNormal];
    
    
    self.supportBtn.polyMedicDic = self.programDic;
    self.supportBtn.Click = ^(UIButton_Block *btn, NSString *name){
        if (wself.supportBlock) {
            wself.supportBlock(btn,name);
        }
    };
    
    self.shareBtn.polyMedicDic = self.programDic;
    self.shareBtn.Click = ^(UIButton_Block *btn, NSString *name){
        if (wself.shareBlock) {
            wself.shareBlock(btn,name);
        }
    };
    
}


-(void)setHumourModel:(HumourModel *)humourModel
{
    [self.displayHumourBV sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(humourModel.coverURL)] placeholderImage:[UIImage imageNamed:@"bg_ugc_home_item_one_onloading"]];
    [self.disPlayTitleLabel setText:[NSString stringWithFormat:@"%@",humourModel.title]];
    [self.disPlayDateLabel setText:[NSString stringWithFormat:@"%@",humourModel.dateStr]];
    [self.disPlayDurationLabel setText:[NSString stringWithFormat:@"%@",[self durationStringWithTime:[humourModel.lengthStr integerValue]]]];
    
}

-(NSString *)durationStringWithTime:(NSInteger)seconds
{
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    
    return format_time;
    
}


@end
