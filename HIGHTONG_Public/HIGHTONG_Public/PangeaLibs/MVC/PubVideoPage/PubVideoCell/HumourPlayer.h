//
//  HumourPlayer.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/13.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGCPlayer.h"
#import "UIButton_Block.h"
#import "PlayerMaskView.h"
#import "HumourLoadingView.h"
#import "HumourModel.h"
#import <UMSocialCore/UMSocialCore.h>
#import "WXApi.h"
#import "ShareView.h"
#import "HumourErrorMaskView.h"
#import <TencentOpenAPI/QQApiInterface.h>
typedef void (^HumourPlayer_TO_refresh)();
typedef void (^HumourPlayer_TO_errorRefresh)();
typedef void (^HumourPlayer_TO_return)();
typedef void (^HumourPlayer_TO_share)();
typedef void (^HumourPlayer_Action)(BOOL isAction);
//// 枚举值，包含水平移动方向和垂直移动方向
//// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, HT_HUM_PanDirection){
    HT_HUM_PanDirectionHorizontalMoved, //横向移动
    HT_HUM_PanDirectionVerticalMoved    //纵向移动
};
@interface HumourPlayer : UIView
{
    NSDate *startDate;
}
@property (nonatomic, copy) HumourPlayer_Action humourPlayerAction;
@property (nonatomic, copy) HumourPlayer_TO_refresh refreshBlock;
@property (nonatomic, copy) HumourPlayer_TO_return returnBlock;
@property (nonatomic, copy) HumourPlayer_TO_share shareBlock;
@property (nonatomic, copy) HumourPlayer_TO_errorRefresh errorRefresh;

@property (nonatomic, assign)BOOL isMaskShowing;
@property (nonatomic, strong) UGCPlayer *player;
@property (nonatomic, strong) UIImageView *topBarView;
@property (nonatomic, strong) UIImageView *bottomBarView;
@property (nonatomic, strong) UIButton *fullWindowBtn;
@property (nonatomic, strong)UIButton *playButton;
@property (nonatomic, strong)UIButton *mainPlayButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton_Block *returnBtn;
@property (nonatomic, strong) UIButton_Block *shareBtn;
@property (nonatomic, strong) UIButton *scaleBtn;
@property (nonatomic, strong) UIImageView *timeView;
@property (nonatomic, assign) BOOL isfullScreen;
@property (nonatomic, strong) UIImageView *endMaskView;
@property (nonatomic, strong) HumourErrorMaskView *errorMaskView;
@property (nonatomic, strong) UIButton_Block *refreshBtn;
@property (nonatomic, strong) UILabel *refreshLabel;
@property (nonatomic, strong) ShareView *shareView;
@property (nonatomic, strong) HumourModel *humourModel;
@property (nonatomic, assign)BOOL isByUserPause;
@property (nonatomic, strong)UIImageView *progressTimeView;
@property (nonatomic, strong)UIImageView *progressDirectionIV;
@property (nonatomic, strong)UILabel *progressTimeLable_top;
@property (nonatomic, strong)UILabel *progressTimeLable_bottom;
@property (nonatomic, strong)UIProgressView *brightnessVODProgress;
@property (nonatomic, strong)UIImageView *brightnessVODView;
@property (nonatomic, strong)UIProgressView*volumeVODProgressV;
@property (nonatomic, assign)  BOOL isCollectionInfo;
@property (nonatomic, strong) HumourLoadingView *playerLoadingView;

@end
