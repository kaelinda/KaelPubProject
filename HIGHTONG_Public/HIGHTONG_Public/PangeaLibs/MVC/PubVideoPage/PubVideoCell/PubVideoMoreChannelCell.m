//
//  PubVideoMoreChannelCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/12.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubVideoMoreChannelCell.h"
#import "Masonry.h"

@implementation PubVideoMoreChannelCell

// CollectionViewCell 初始化
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    
    [self setupUI];
    
    
    return self;
}

- (void)setupUI {
    
    _backgroundImg = [[UIImageView alloc]initWithFrame:CGRectMake(0.5*kRateSize, 0.5*kRateSize, 78*kDeviceRate, 39*kDeviceRate)];
    _backgroundImg.backgroundColor = App_white_color;
    [self.contentView addSubview:_backgroundImg];
    self.contentView.backgroundColor = UIColorFromRGB(0xd6d6d6);

    //台名
    self.linkName = [[UILabel alloc] init];
    self.linkName.textColor = HT_COLOR_FONT_FIRST;
    self.linkName.font = HT_FONT_SECOND;
    self.linkName.textAlignment = NSTextAlignmentCenter;
    [_backgroundImg addSubview:self.linkName];
    
    [self.linkName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backgroundImg.mas_centerX);
        make.centerY.equalTo(_backgroundImg.mas_centerY);
        make.height.equalTo(@(40*kRateSize));
        make.width.equalTo(@(79*kRateSize));
        
    }];
    
}

@end
