//
//  PubVideoProgramCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^PubVideoProgramCell_TO_Share)(UIButton_Block*,NSString*);

typedef void (^PubVideoProgramCell_TO_Support)(UIButton_Block*,NSString*);



@interface PubVideoProgramCell : UITableViewCell

@property(nonatomic,copy)PubVideoProgramCell_TO_Share shareBlock;

@property(nonatomic,copy)PubVideoProgramCell_TO_Support supportBlock;

/**
 *  @author Dirk
 *
 *  @brief 遮罩Image
 */
@property (nonatomic,strong) UIImageView *maskImage;


@property (nonatomic, strong) NSMutableDictionary *programDic;
//@property (nonatomic, strong) UIView *shareView;
//@property (nonatomic, strong) UIView *supportView;

- (void)update;

@end
