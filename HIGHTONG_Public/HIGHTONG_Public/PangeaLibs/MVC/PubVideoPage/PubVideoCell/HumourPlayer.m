//
//  HumourPlayer.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/13.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourPlayer.h"
#import "HTVolumeUtil.h"
#define ADD_GESTURE
@interface HumourPlayer ()<UIGestureRecognizerDelegate>
@property (nonatomic,strong)NSTimer *playedTime;
@property(nonatomic, strong)UISlider        *progressSlider;
@property (nonatomic,strong)UILabel *currentPositionLabel;
@property (nonatomic,strong)UILabel *durationPostionLabel;
@property (nonatomic,strong)UIButton *returnButton;
@property (assign, atomic) int  autoHiddenSecend;
@property (nonatomic,strong)NSTimer *autoHiddenTimer;
@property (nonatomic,strong)UIPanGestureRecognizer *pan;
/** 定义一个实例变量，保存枚举值 */
@property (nonatomic, assign) PanDirection        panDirection;
/**
 * 亮度的VIEW和声音的View
 */
/** 用来保存快进的总时长 */
@property (nonatomic, assign) CGFloat             sumTime;
@property (nonatomic,assign)CGFloat systemVODBrightness;



@end
@implementation HumourPlayer

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViewsWithFrame:frame];
        
               
    }
    return self;
}



-(void)setupViewsWithFrame:(CGRect)frame
{
    
    __weak __typeof(&*self)weakSelf = self;

    
    self.userInteractionEnabled = YES;
    
    _player = [[UGCPlayer alloc]initWithPlayerType:KHTPlayer_AVPlay withFatherViewFrame:frame];
    _player.userInteractionEnabled = YES;

    [self addSubview:_player];
    
    
    
    _topBarView = [[UIImageView alloc]init];
    [_topBarView setImage:[UIImage imageNamed:@"bg_player_playbill_listview_epg"]];
    _topBarView.userInteractionEnabled = YES;

    [self addSubview:_topBarView];
    
    
#ifdef ADD_GESTURE
    //图片点击手势
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self addGestureRecognizer:tapGesture];
    
#endif
    
    [_topBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(45*kDeviceRate);

    }];
    
    _bottomBarView = [[UIImageView alloc]init];
    [_bottomBarView setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
    _bottomBarView.userInteractionEnabled = YES;

    [self addSubview:_bottomBarView];
    
    [_bottomBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(45*kDeviceRate);
        
    }];

    
    
    _titleLabel = [[UILabel alloc]init];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    _titleLabel.textColor = UIColorFromRGB(0xffffff);
    [_topBarView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left).offset(10*kDeviceRate);
        make.width.mas_equalTo(_topBarView).multipliedBy(0.6);
        make.height.mas_equalTo(45*kDeviceRate);

    }];
    
    _returnBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];//
    [_returnBtn setImage:[UIImage imageNamed:@"btn_hum_player_return_normal"] forState:UIControlStateNormal];
    [_returnBtn setImage:[UIImage imageNamed:@"btn_hum_player_return_highlight"] forState:UIControlStateHighlighted];

    _returnBtn.hidden = YES;
    [_topBarView addSubview:_returnBtn];
    
    [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(self.mas_left).offset(0*kDeviceRate);
        make.width.mas_equalTo(35*kDeviceRate);
        make.height.mas_equalTo(35*kDeviceRate);

    }];
    
    _returnBtn.Click = ^(UIButton_Block *buttonBlock, NSString *name) {
        
        if (weakSelf.returnBlock) {
            
            weakSelf.returnBlock();
        }
    };
    
    
    
    _shareBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    [_shareBtn setBackgroundColor:[UIColor clearColor]];
    [_shareBtn setTitle:@"分享" forState:UIControlStateNormal];
    _shareBtn.hidden = YES;
    [_topBarView addSubview:_shareBtn];
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.right.mas_equalTo(self.mas_right).offset(-50*kDeviceRate);
        make.width.mas_equalTo(45*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
        
    }];
    
    
    _shareView = [[ShareView alloc]init];
    _shareView.userInteractionEnabled = YES;
    _shareView.hidden = YES;
    [self addSubview:_shareView];
    
    [_shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(self.mas_height);
        
    }];

    
    _shareBtn.Click = ^(UIButton_Block *buttonBlock, NSString *name)
    {
        if (weakSelf.shareBlock) {
            weakSelf.shareBlock();
        };
        
//        weakSelf.shareView.hidden = NO;
        
        
    };
    
    
   
    
    
    
    _fullWindowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _fullWindowBtn.backgroundColor = [UIColor clearColor];
    [_fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_smallScreen_normal"] forState:UIControlStateNormal];
    [_fullWindowBtn setImage:[UIImage imageNamed:@"btn_hum_player_smallScreen_highlight"] forState:UIControlStateHighlighted];
    [_bottomBarView addSubview:_fullWindowBtn];
    
    [_fullWindowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_bottomBarView.mas_bottom);
        make.right.mas_equalTo(self.mas_right);
        make.width.mas_equalTo(35*kDeviceRate);
        make.height.mas_equalTo(35*kDeviceRate);
    }];
    
    
    
    _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [_bottomBarView addSubview:_playButton];
    [_playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_bottomBarView.mas_bottom).offset(-5*kDeviceRate);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(35*kDeviceRate);
        make.height.mas_equalTo(35*kDeviceRate);
    }];
    
    
    
    
    _mainPlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_mainPlayButton setImage:[UIImage imageNamed:@"btn_hum_player_action"] forState:UIControlStateNormal];
    _mainPlayButton.hidden = YES;
    [self addSubview:_mainPlayButton];
    
    [_mainPlayButton addTarget:self action:@selector(playMainButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    [_mainPlayButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(55*kDeviceRate, 55*kDeviceRate));
    }];
    
    
    
    _currentPositionLabel = [[UILabel alloc]init];
    [_currentPositionLabel setBackgroundColor:[UIColor clearColor]];
    _currentPositionLabel.textAlignment = NSTextAlignmentCenter;
    _currentPositionLabel.textColor = UIColorFromRGB(0xffffff);
    
    [_bottomBarView addSubview:_currentPositionLabel];
    
    [_currentPositionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_bottomBarView.mas_bottom);
        make.left.mas_equalTo(_playButton.mas_right);
        make.width.mas_equalTo(65*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);

    }];
    
    
    _durationPostionLabel = [[UILabel alloc]init];
    [_durationPostionLabel setBackgroundColor:[UIColor clearColor]];
    _durationPostionLabel.textAlignment = NSTextAlignmentRight;
    _durationPostionLabel.textColor = UIColorFromRGB(0xffffff);
    [_bottomBarView addSubview:_durationPostionLabel];
    
    [_durationPostionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_bottomBarView.mas_bottom);
        make.right.mas_equalTo(_fullWindowBtn.mas_left).offset(-5*kDeviceRate);
        make.width.mas_equalTo(65*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
    }];
    
    
    self.progressSlider = [[UISlider alloc]init];
    [self.progressSlider setMaximumValue:1000.00];
    [self.progressSlider setMinimumValue:0.00];
    // slider开始滑动事件
    [self.progressSlider setThumbImage:[UIImage imageNamed:@"ic_ugc_player_seekbar_progress_cursor"] forState:UIControlStateNormal];
    self.progressSlider.userInteractionEnabled = YES;
    [self.progressSlider setMaximumTrackImage:[UIImage imageNamed:@"bg_hum_player_progress_normal"] forState:UIControlStateNormal];
    [self.progressSlider setMinimumTrackImage:[UIImage imageNamed:@"bg_hum_player_progress_pressed"] forState:UIControlStateNormal];
    [self.progressSlider setBackgroundColor:[UIColor clearColor]];
    [_bottomBarView addSubview:self.progressSlider];
    
    [self.progressSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_currentPositionLabel.mas_right).offset(18*kDeviceRate);
        make.bottom.mas_equalTo(_bottomBarView.mas_bottom).offset(-0*kDeviceRate);
        make.right.mas_equalTo(_durationPostionLabel.mas_left).offset(-18*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
    }];
    
    
    
    [self.progressSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
    // slider滑动中事件
    [self.progressSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    // slider结束滑动事件
    [self.progressSlider addTarget:self action:@selector(progressSliderTouchEnd:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchUpOutside];
        self.progressSlider.continuous = NO;
    self.progressSlider.enabled = YES;

    
    _endMaskView = [[UIImageView alloc]init];
    _endMaskView.userInteractionEnabled = YES;
    _endMaskView.hidden = YES;
    [_endMaskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self addSubview:_endMaskView];
    
    
    [_endMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(self.mas_height);
    }];
    
    _refreshBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    [_refreshBtn setImage:[UIImage imageNamed:@"btn_hum_player_refresh"] forState:UIControlStateNormal];
//    [_refreshBtn setBackgroundColor:[UIColor redColor]];
    [_endMaskView addSubview:_refreshBtn];

    [_refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(200*kDeviceRate);
        make.left.mas_equalTo(self.mas_left).offset(150*kDeviceRate);
        make.width.mas_equalTo(23*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        

    }];
    
    _refreshLabel = [[UILabel alloc]init];
    [_refreshLabel setBackgroundColor:[UIColor clearColor]];
    _refreshLabel.textAlignment = NSTextAlignmentLeft;
    _refreshLabel.text = @"重播";
    _refreshLabel.font = [UIFont systemFontOfSize:12*kDeviceRate];
    _refreshLabel.textColor = UIColorFromRGB(0xffffff);
    [_endMaskView addSubview:_refreshLabel];
    
    [_refreshLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(200*kDeviceRate);
        make.left.mas_equalTo(_refreshBtn.mas_right).offset(10*kDeviceRate);
        make.width.mas_equalTo(50*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
    }];
    
    
    
    _errorMaskView = [[HumourErrorMaskView alloc]init];
    _errorMaskView.hidden = YES;
    _errorMaskView.userInteractionEnabled = YES;
    [self addSubview:_errorMaskView];
    
    [_errorMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(self.mas_height);
    }];

    
    
    _refreshBtn.Click = ^(UIButton_Block *buttonBlock, NSString *name) {
        
        if (weakSelf.refreshBlock) {
            
            weakSelf.refreshBlock();
        }
        
    };
    
    
    
    _errorMaskView.humourRefresh = ^{
         __strong typeof(HumourPlayer *) strongSelf = weakSelf;
        if (strongSelf.errorRefresh) {
            strongSelf.errorRefresh();
        }
    };

    
    
    _progressTimeView = [[UIImageView alloc]init];
    _progressTimeView.hidden = YES;
    [_progressTimeView setImage:[UIImage imageNamed:@"bg_player_full_fast_action"]];
    [weakSelf addSubview:_progressTimeView];
    
    
    
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    [_progressTimeView addSubview:_progressDirectionIV];
    
    
    _progressTimeLable_top = [[UILabel alloc]init];
    _progressTimeLable_top.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_top.textColor = App_selected_color;
    _progressTimeLable_top.backgroundColor = [UIColor clearColor];
    _progressTimeLable_top.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_top.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_top.shadowOffset = CGSizeMake(1.0, 1.0);
    [_progressTimeView addSubview:_progressTimeLable_top];
    
    
    _progressTimeLable_bottom = [[UILabel alloc]init];
    _progressTimeLable_bottom.textAlignment = NSTextAlignmentLeft;
    _progressTimeLable_bottom.textColor = UIColorFromRGB(0xffffff);
    _progressTimeLable_bottom.backgroundColor = [UIColor clearColor];
    _progressTimeLable_bottom.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_bottom.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_bottom.hidden = NO;
    _progressTimeLable_bottom.shadowOffset = CGSizeMake(1.0, 1.0);
    [_progressTimeView addSubview:_progressTimeLable_bottom];
    
    
    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.height.mas_equalTo(70*kRateSize);
        make.width.mas_equalTo(130*kRateSize);
        
    }];
    
    
    
    [_progressDirectionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_progressTimeView.mas_centerY).offset(-15*kRateSize);
        make.centerX.mas_equalTo(_progressTimeView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
    }];
    
    
    
    
    [_progressTimeLable_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(weakSelf.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.left.mas_equalTo(weakSelf.progressTimeView.mas_left).offset(5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_progressTimeLable_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(weakSelf.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.right.mas_equalTo(weakSelf.progressTimeView.mas_right).offset(-5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
        
    }];
    
    
    
    
    
    
    _brightnessVODView = [[UIImageView alloc]init];
    _brightnessVODView.image = [UIImage imageNamed:@"bg_player_brightness.png"];
    _brightnessVODView.hidden = YES;
    [weakSelf addSubview:_brightnessVODView];
    _brightnessVODProgress = [[UIProgressView alloc]init];
    _brightnessVODProgress.trackImage = [UIImage imageNamed:@"video_num_bg.png"];
    _brightnessVODProgress.progressImage = [UIImage imageNamed:@"video_num_front.png"];
    _brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
    _brightnessVODProgress.hidden = NO;
    
    
    _volumeVODProgressV = [[UIProgressView alloc]init];
    
    [_brightnessVODView addSubview:_brightnessVODProgress];
    [self addSubview:_volumeVODProgressV];
    
    
    //亮度视图尺寸****************
    
    [_brightnessVODView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessVODProgress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.brightnessVODView.mas_bottom);
        make.centerX.mas_equalTo(weakSelf.brightnessVODView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
    
    _playerLoadingView = [[HumourLoadingView alloc]init];
    
    [_playerLoadingView setHidden:NO];
    [self addSubview:_playerLoadingView];
    
    
    [_playerLoadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(self.mas_height);
    }];
    
    
    [self playerStatusObserve];

    
    
    
}


-(void)setIsfullScreen:(BOOL)isfullScreen
{
    _isfullScreen = isfullScreen;
    if (_isfullScreen) {
        
//        _shareBtn.hidden = NO;
        _returnBtn.hidden = NO;

    }else
    {
//        _shareBtn.hidden = YES;
        _returnBtn.hidden = YES;
        _shareView.hidden = YES;

    }
}



-(void)playMainButtonClick:(UIButton*)sender
{
    [_player play];
    [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
    _isByUserPause = NO;
    _mainPlayButton.hidden = YES;
    NSLog(@"我点击了播放");
    //            }

}


-(void)playButtonClick:(UIButton*)sender
{

    sender.selected = !sender.selected;
    if (sender.selected) {
        
        NSLog(@"我点击了暂停");
        [_player pause];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        _isByUserPause = YES;
        
        _mainPlayButton.hidden = NO;
        
    }else
    {
        [_player play];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
        _isByUserPause = NO;
        _mainPlayButton.hidden = YES;

        NSLog(@"我点击了播放");
    }
 
   
    
}


-(void)playerStatusObserve{
    
    WS(wSelf);
    
    if (_player) {
        
        _player.DHPlayerStatusBlock = ^(DHPlayerStatus status, NSArray *statusArr) {
            
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //                [self playerObserveDHPlayerPlayStatus:status andPlayerStatusArray:statusArr];
            //            });
            dispatch_async(dispatch_get_main_queue(), ^{
                [wSelf playerObserveDHPlayerPlayStatus:status andPlayerStatusArray:statusArr];
                
            });
            
        };
        
        _player.DHPlayerErrorStatusBlock = ^(DHPLAYERERRORSTATUS status,int errorStatus,NSString *errorString)
        {
            wSelf.errorMaskView.hidden = NO;
            
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //
            //                ZSToast(errorString);
            //
            //            });
            
            //            NSLog(@"error : \n errorString:%@\n errorStatus:%d \n status:%lu",errorString,errorStatus,(unsigned long)status);
            
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (_player) {
//                    
//                    NSLog(@"当前节目的错误LOG是%d%@",errorStatus,errorString);
//                    
//                    CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"当前节目无法播放,是否重新播放?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:weakSelf];
//                    nameAlert.tag = 12;
//                    [nameAlert show];
//                    
//                }
//                
//            });
            
            //            MAIN();
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //
            //                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"当前节目无法播放,是否重新播放?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:weakSelf];
            //                nameAlert.tag = 12;
            //                [nameAlert show];
            //
            //
            //
            //            });
            
            
        };
        
        
        _player.DHPlayerMessageStatusBlock = ^(DHPLAYERMESSAGESTATUS status,int messageStatus,NSString *messageString) {
            
            //           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //               NSLog(@"=====player%@",weakSelf);
            //
            ////               [self playerObserveDHPlayerMessageStatus:status andMessageValueStatus:messageStatus andMessageString:messageString];
            //           });
        };
        
        
        
    }
    
}



-(void)progressSliderTouchBegan:(UISlider*)slider
{
        [NSObject cancelPreviousPerformRequestsWithTarget:self];

        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            // 暂停timer

            [_playedTime setFireDate:[NSDate distantFuture]];
        }
}

-(void)progressSliderValueChanged:(UISlider*)slider
{
    //    [_player progressSeekValue:slider.value];
    
    
  
        //拖动改变视频播放进度
        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
    
            //            CGFloat value   = slider.value - self.sliderVODLastValue;
            //            if (value > 0) { style = @">>"; }
            //            if (value < 0) { style = @"<<"; }
            //
            //            self.sliderVODLastValue    = slider.value/1000;
            //            // 暂停
            [self.player.avPlayer pause];
            [_playedTime setFireDate:[NSDate distantFuture]];
            [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [_playButton setTag:ENGINE_STATUS_PLAY];
            
            CGFloat total           = ((CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale)/1000;
            
            //计算出拖动的当前秒数
            NSInteger dragedSeconds = floorf(total * slider.value);
            
            //转换成CMTime才能给player来控制播放进度
            
            
            NSString *currentTime = [self durationStringWithTime:(int)dragedSeconds];

            if (total > 0) {
                // 当总时长 > 0时候才能拖动slider
                _currentPositionLabel.text  = currentTime;

            }else {
                // 此时设置slider值为0
                slider.value = 0;
            }
            
        }else { // player状态加载失败
            // 此时设置slider值为0
            slider.value = 0;
        }
    
    
}


-(void)progressSliderTouchEnd:(UISlider*)slider
{
   
        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
    
            // 继续开启timer
            [_playedTime setFireDate:[NSDate date]];

                       // 视频总时间长度
            CGFloat total           = (CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale;
            
            //计算出拖动的当前秒数
            NSInteger dragedSeconds = (floorf(total * slider.value)/1000);
            
            [self.player progressSeekValue:dragedSeconds];
            
            [_playButton setTag:ENGINE_STATUS_PAUSE];
            [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            _mainPlayButton.hidden = YES;
            _currentPositionLabel.text = [NSString stringWithFormat:@"%@",[_player durationStringWithTime:dragedSeconds]];
            
            

            
        }
    
    
    
}




-(void)tapGesture:(UITapGestureRecognizer*)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateRecognized) {
        
        self.isMaskShowing = !self.isMaskShowing;
        
    }
    
}

-(void)setIsMaskShowing:(BOOL)isMaskShowing
{
    _isMaskShowing = isMaskShowing;
    
    if (_isMaskShowing == YES) {
        
        [self showControlViewBar];
        [self reStartHiddenTimer];
    }else
    {
        
        [self hiddenControlViewBar];
    }
}


-(void)showControlViewBar
{
    [self reStartHiddenTimer];
    
    _bottomBarView.hidden = NO;

    _topBarView.hidden = NO;
    
    if (_isfullScreen) {
        
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }
   
    
    
}

-(void)hiddenControlViewBar
{
    
    _bottomBarView.hidden = YES;
    
   
    _topBarView.hidden = YES;
    
    if (_isfullScreen) {
        if (_shareView.hidden == NO) {
            _shareView.hidden = YES;
        }
        [[UIApplication sharedApplication]setStatusBarHidden:YES];
    }

    
}


-(void)reStartHiddenTimer
{
    
    self.autoHiddenSecend = 5;
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
    self.autoHiddenTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(autoHiddenPanel:) userInfo:nil repeats:YES];
}

- (void)autoHiddenPanel:(NSTimer *)sender
{
    if (self.autoHiddenSecend == 0) {
        [self hiddenPanlwithBool:YES];
    } else {
        self.autoHiddenSecend --;
    }
}


-(void)hiddenPanlwithBool:(BOOL)bol
{
    if (bol) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
        
        [self setIsMaskShowing:NO];
        
    } else {
        
        [self setIsMaskShowing:YES];
        
    }
}


#pragma mark 播放器错误状态


-(void)playerObserveDHPlayerPlayStatus:(DHPlayerStatus)playerStatus andPlayerStatusArray:(NSArray*)statusArr
{
    __weak __typeof(&*self)weakSelf = self;
    
    switch (playerStatus) {
            /*
             case DHPlayerStatus_init:{}break;
             case DHPlayerStatus_stop:{}break;
             case DHPlayerStatus_pause:{}break;
             case DHPlayerStatus_ready:{}break;
             case DHPlayerStatus_failed:{}break;
             case DHPlayerStatus_offline:{}break;
             case DHPlayerStatus_playing:{}break;
             case DHPlayerStatus_buffering:{}break;
             case DHPlayerStatus_connecting:{}break;
             case DHPlayerStatus_sizeChanged:{}break;
             case DHPlayerStatus_MSG:{}break;
             case DHPlayerStatus_startToPlay:{}break;
             */
            
        case DHPlayerStatus_play:{
            NSLog(@"开始播放");
            if (weakSelf.playedTime) {
                
                [weakSelf.playedTime invalidate];
                
                weakSelf.playedTime = nil;
                
//                [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
                [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
                NSLog(@"听了播放器");
            }
            
            if(!weakSelf.playedTime){
                weakSelf.playedTime = [NSTimer scheduledTimerWithTimeInterval:1.0f target:weakSelf selector:@selector(updateProgress) userInfo:nil repeats:YES];
                [[NSRunLoop currentRunLoop] addTimer:weakSelf.playedTime forMode:NSRunLoopCommonModes];
                
                weakSelf.pan = [[UIPanGestureRecognizer alloc]initWithTarget:weakSelf action:@selector(panDirection:)];
                weakSelf.pan.delegate                = weakSelf;
                [weakSelf addGestureRecognizer:weakSelf.pan];
                
                [_playedTime setFireDate:[NSDate date]];
                [_playButton setTag:ENGINE_STATUS_PAUSE];
                [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            }

            
         
            
            _isCollectionInfo = YES;
            
            if (_humourPlayerAction) {
                
                _humourPlayerAction(YES);
            }
            
            
        }break;
        case DHPlayerStatus_stop:{
            if (weakSelf.playedTime) {
                
                [weakSelf.playedTime invalidate];
                
                weakSelf.playedTime = nil;
                NSLog(@"我就要停播放器");
                
            }
            

            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            
            weakSelf.progressSlider.value = 0;
            weakSelf.progressSlider.enabled = NO;
            weakSelf.currentPositionLabel.text = @"00:00";
            weakSelf.durationPostionLabel.text = @"00:00";
 
                NSMutableArray *notiArr = [NSMutableArray arrayWithArray:[statusArr mutableCopy]];
                
                if (notiArr.count>0) {
                    
                    NSString *str = [[notiArr objectAtIndex:0] objectForKey:@"avPlayerEnd"];
                    if ([str isEqualToString:@"1"]) {
                        _endMaskView.hidden = NO;
   [[NSNotificationCenter defaultCenter] postNotificationName:@"ROTE_PUB_PLAYER" object:nil];
                        
//                        [weakSelf autoContinuePlayer];
                        
                    }
                }
                
            
            NSLog(@"stop掉player");
        }break;
        case DHPlayerStatus_pause:{
            NSLog(@"暂停播放器");
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
            
        }break;
        case DHPlayerStatus_ready:{
            NSLog(@"准备播放");
//            _playerLoadingView.hidden = YES;
//            
//            [weakSelf.playButton setTag:ENGINE_STATUS_PAUSE];
//            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
//            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
//            
//            weakSelf.playButton.enabled = NO;
//            [weakSelf reStartHiddenTimer];
            //            if (duration == 0)
            //                self.progressSlider.enabled = NO;
            //            else
            //                self.progressSlider.enabled = YES;
            
        }break;
        case DHPlayerStatus_failed:{
            NSLog(@"播放失败");
            [weakSelf.playButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            weakSelf.playButton.enabled = YES;
//            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_netError];
            
        }break;
        case DHPlayerStatus_offline:{
            if (!statusArr) {
                NSLog(@"主播已下线");
                //记得 release掉播放器哦
//                [self controlViewClearPlayer];
            }
        }break;
        case DHPlayerStatus_playing:{
            NSLog(@"playing");
            
            
//            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
            
        }break;
        case DHPlayerStatus_buffering:{
            //            NSLog(@"buffering");
            _playerLoadingView.hidden = YES;
            NSMutableArray *notiArr = [NSMutableArray arrayWithArray:[statusArr mutableCopy]];

            NSLog(@"====//buffer%@",[notiArr objectAtIndex:0]);
//            if ([notiArr count]>0) {
//                MUInt32 wParam = [[notiArr objectAtIndex:0] unsignedIntValue];
//                MLong lParam = [[notiArr objectAtIndex:1] intValue];
//                //                        UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
//                //                        MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
//                //                        MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];
//                MUInt32   bufferingProgress;
//                
//                bufferingProgress = lParam;
//                
//                NSString *text = [NSString stringWithFormat:@"%d%%", bufferingProgress];
//                if (bufferingProgress<100) {
////                    NSLog(@"buffering progress-%@----- %d",text,weakSelf.hadpic);
//                    
////                    if (weakSelf.hadpic) {
////                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd];
////                        
////                    }else{
////                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];
////                        
////                    }
//                    
//                }else{
////                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
//                    
//                }
//                
//            }
            
            
            
        }break;
        case DHPlayerStatus_connecting:{
            NSLog(@"connecting连接中");
//            weakSelf.playButton.enabled = NO;
            
        }break;
        case DHPlayerStatus_sizeChanged:{
            NSLog(@"尺寸改变，或者 加载出第一帧来了");
//            _hadpic = YES;
            
        }break;
        case DHPlayerStatus_MSG:{
            NSLog(@"MSG 干吗用的");
        }break;
        case DHPlayerStatus_startToPlay:{
            _playerLoadingView.hidden = NO;

            if ([statusArr count]>0) {

                //                        UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
                //                        MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
                //                        MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];
                
//                _playerDurationPosition = (CGFloat)lParam;
                
            }
            
            
            
            NSLog(@"start to play");
            weakSelf.playButton.enabled = YES;
            weakSelf.progressSlider.enabled = YES;
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
            
            
            
        }break;
            
        default:
            break;
    }
    
}


-(void)playerObserveDHPlayerErrorStatus:(DHPLAYERERRORSTATUS)errorStatus andErrorValueStatus:(int)errorValueStatus andErrorSting:(NSString*)errorString
{

    switch (errorStatus) {
        case DHPLAYERERRORSTATUS_UNSUPPORTED_SCHEME:
        {
            ZSToast(errorString);
        }
            break;
        case DHPLAYERERRORSTATUS_NETWORK_CONNECTFAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_STREAM_OPEN:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_STREAM_SEEK:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATARECEIVE_TIMEOUT:
        {
            
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_FORMAT_UNSUPPORTED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_FORMAT_MALFORMED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DNS_RESOLVE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DNS_RESOLVE_TIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NETWORK_CONNECTIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATARECEIVE_FAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATASEND_TIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATASEND_FAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATAERROR_HTML:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS__SOURCE_BAD_VALUE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_BAD_VALUE_DISPLAY_INIT_FAILED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NOAUDIO_VIDEOUNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NOVIDEO_AUDIOUNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_AVCODEC_UNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_OPERATION_CANNOTEXECUTE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_HTTPFAIL:
        {
            
            ZSToast(errorString);
            
        }
            break;
            
            
        default:
            break;
    }
//    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
    
}


-(void)playerObserveDHPlayerMessageStatus:(DHPLAYERMESSAGESTATUS)messageStatus andMessageValueStatus:(int)messageValueStatus andMessageString:(NSString*)messageString
{
    switch (messageStatus) {
        case DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOVIDEO:
        {
            ZSToast(messageString);
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOAUDIO:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_VCODEC_UNSUPPORTVIDEO:
        {
            ZSToast(messageString);
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_ACODEC_UNSUPPORTAUDIO:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_VCODEC_DECODE_ERROR:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_ACODEC_DECODE_ERROR:
        {
            ZSToast(messageString);
            
        }
            break;
            
        default:
            break;
    }
}




#pragma mark 触摸手势事件


/**
 *  pan手势事件
 *
 *  @param pan UIPanGestureRecognizer
 */
- (void)panDirection:(UIPanGestureRecognizer *)pan
{
    if (_isfullScreen) {
        CGPoint locationPoint = [pan locationInView:self];
        
        // 我们要响应水平移动和垂直移动
        // 根据上次和本次移动的位置，算出一个速率的point
        CGPoint veloctyPoint = [pan velocityInView:self];
        
        // 判断是垂直移动还是水平移动
        // 判断是垂直移动还是水平移动
        switch (pan.state) {
            case UIGestureRecognizerStateBegan:{ // 开始移动
                // 使用绝对值来判断移动的方向
                CGFloat x = fabs(veloctyPoint.x);
                CGFloat y = fabs(veloctyPoint.y);
                if (x > y) { // 水平移动
                    // 取消隐藏
                    self.progressTimeView.hidden = NO;
                    self.panDirection = PanDirectionHorizontalMoved;
                    [self controlTouchBegin];
                }
                else if (x < y){ // 垂直移动
                    self.panDirection = HT_HUM_PanDirectionVerticalMoved;
                    // 开始滑动的时候,状态改为正在控制音量
                    if (locationPoint.x > self.bounds.size.width / 2) {
                        //                    self.isVolume = YES;
                        
                        _panDirection = PanDirectionVerticalVolumeMoved;
                    }else { // 状态改为显示亮度调节
                        //                    self.isVolume = NO;
                        self.brightnessVODView.hidden = NO;
                        
                        _panDirection = PanDirectionVerticalBrighteMoved;
                    }
                }
                break;
            }
            case UIGestureRecognizerStateChanged:{ // 正在移动
                switch (self.panDirection) {
                    case PanDirectionHorizontalMoved:{
                        [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
                        break;
                    }
                    case PanDirectionVerticalVolumeMoved:{
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                    case PanDirectionVerticalBrighteMoved:{
                        self.brightnessVODView.hidden = NO;
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                        
                        //                case HTPanDirectionVerticalMoved:{
                        //                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        //                    break;
                        //                }
                    default:
                        break;
                }
                break;
            }
            case UIGestureRecognizerStateEnded:{ // 移动停止
                // 移动结束也需要判断垂直或者平移
                // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
                switch (self.panDirection) {
                    case PanDirectionHorizontalMoved:{
                        
                        
                        [self controlTouchEnd];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            // 隐藏视图
                            self.progressTimeView.hidden = YES;
                        });
                    }
                    case PanDirectionVerticalVolumeMoved:{
                        
                        // 且，把状态改为不再控制音量
                        //                    _isVolume = NO;
                        break;
                    }
                    case PanDirectionVerticalBrighteMoved:{
                        self.brightnessVODView.hidden = YES;
                        // 且，把状态改为不再控制音量
                        //                    _isVolume = NO;
                        break;
                    }
                        
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }

    }else
    {
        return;
    }
    //根据在view上Pan的位置，确定是调音量还是亮度
    
}




-(void)controlTouchBegin
{
    if (_isfullScreen) {
        
        // 给sumTime初值
        CMTime time       = self.player.avPlayerItem.currentTime;
        self.sumTime      = time.value/time.timescale;
        
        // 暂停视频播放
        [self.player pause];
        [self.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [self.playButton setTag:ENGINE_STATUS_PLAY];
        // 暂停timer
        [_playedTime setFireDate:[NSDate distantFuture]];
    }else
    {
        return;
    }
    
    
    
}



-(void)controlTouchEnd
{
    if (_isfullScreen) {
        
        // 继续播放
        [self.player play];
        [_playButton setTag:ENGINE_STATUS_PAUSE];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
        _mainPlayButton.hidden = YES;
        
        
        [_playedTime setFireDate:[NSDate date]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 隐藏视图
            self.progressTimeView.hidden = YES;
        });
        // 快进、快退时候把开始播放按钮改为播放状态
        //            self.controlView.startVODBtn.selected = YES;
        //            self.isPauseByUser                 = NO;
        
        [self.player progressSeekValue:self.sumTime];
        // 把sumTime滞空，不然会越加越多
        self.sumTime = 0;
    }else
    {
        return;
    }
    
}




/**
 *  pan水平移动的方法
 *
 *  @param value void
 */
- (void)horizontalMoved:(CGFloat)value
{
    //    // 快进快退的方法
    //    NSString *style = @"";
    //    if (value < 0) { style = @"<<"; }
    //    if (value > 0) { style = @">>"; }
    
    if (_isfullScreen) {
        
        NSString *nowTime = @"";
        
        NSString *durationTime = @"";
        
        if (value >0) {
            
            [self.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_forward"]];
        }else
        {
            [self.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_backward"]];
        }
        
        
        // 每次滑动需要叠加时间
        self.sumTime += value / 200;
        
        // 需要限定sumTime的范围
        CMTime totalTime           = self.player.avPlayerItem.duration;
        CGFloat totalMovieDuration = ((CGFloat)totalTime.value/totalTime.timescale);
        if (self.sumTime > totalMovieDuration) { self.sumTime = totalMovieDuration;}
        if (self.sumTime < 0){ self.sumTime = 0; }
        
        
        // 当前快进的时间
        nowTime  = [self durationStringWithTime:(NSInteger)self.sumTime];
        // 总时间
        durationTime = [self durationStringWithTime:(NSInteger)totalMovieDuration];
        
        
        
        // 给label赋值
        //    self.controlView.horizontalLabel.text = [NSString stringWithFormat:@"%@ %@ / %@",style, nowTime, durationTime];
        
        
        [self.progressTimeLable_top setText:[NSString stringWithFormat:@"%@",nowTime]];
        [self.progressTimeLable_bottom setText:[NSString stringWithFormat:@"/%@",durationTime]];

    }else
    {
        return;
    }
    
    
    
}


/**
 *  pan垂直移动的方法
 *
 *  @param value void
 */
- (void)verticalMoved:(CGFloat)value
{

    if (_isfullScreen) {
        
        NSInteger index = (NSInteger)value;
        
        switch (_panDirection) {
            case PanDirectionVerticalVolumeMoved:
                
            {
                if(index>0)
                {
                    if(index%5==0)
                    {//每10个像素声音减一格
                        
                        [self volumeAdd:-0.05];
                    }
                }
                else
                {
                    if(index%5==0)
                    {//每10个像素声音增加一格
                        [self volumeAdd:+0.05];
                    }
                }
                
                
            }
                break;
            case PanDirectionVerticalBrighteMoved:
            {
                
                if(index>0)
                {
                    if(index%5==0)
                    {//每10个像素亮度减一格
                        [self brightnessAdd:-0.05];
                    }
                }
                else
                {
                    if(index%5==0)
                    {//每10个像素亮度增加一格
                        
                        [self brightnessAdd:0.05];
                    }
                }
                
            }
                break;
                
            default:
                break;
        }
 
    }else
    {
        return;
    }
    
    
    
    
    
}


//声音增加
- (void)volumeAdd:(CGFloat)step{
    [HTVolumeUtil shareInstance].volumeValue +=step;
}
//亮度增加
- (void)brightnessAdd:(CGFloat)step{
    [UIScreen mainScreen].brightness += step;
    self.brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
}




-(void)updateProgress
{
    if (_player.avPlayerItem.duration.timescale != 0) {
        _progressSlider.value     = (CMTimeGetSeconds([_player.avPlayerItem currentTime]) / (_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale))*1000;//当前进度
        
        _currentPositionLabel.text = [self durationStringWithTime:(NSInteger)CMTimeGetSeconds([_player.avPlayer currentTime])];
        _durationPostionLabel.text   = [self durationStringWithTime:(NSInteger)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale];
        
//        if (_currentPositionLabel.text) {
//            
//            _playerLoadingView.hidden = YES;
//
//        }
        
        //            CGFloat total           = ((CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale)/1000;
        //
        //            CGFloat current        = ((CGFloat)[_player.avPlayer currentTime].value / _player.avPlayerItem.duration.timescale)/1000;
        //
        //            if (current>=total) {
        //
        //                _isFinish = YES;
        //            }
    }

}



- (NSString *)durationStringWithTime:(NSInteger)seconds
{
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    
    return format_time;

}
@end
