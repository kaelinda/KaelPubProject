//
//  HumourErrorMaskView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourErrorMaskView.h"

@implementation HumourErrorMaskView
-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

-(void)setupSubViews
{
    WS(wself);
    self.userInteractionEnabled = YES;
    [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];

    
    
    _backgroundIV = [[UIImageView alloc]init];
    [_backgroundIV setBackgroundColor:[UIColor clearColor]];
    _backgroundIV.userInteractionEnabled = YES;
    [self addSubview:_backgroundIV];
    
    [_backgroundIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200*kDeviceRate, 90*kDeviceRate));
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    
    [_backgroundIV addGestureRecognizer:tap];
    _topErrorLabel = [[UILabel alloc]init];
    [_topErrorLabel setBackgroundColor:[UIColor clearColor]];
    _topErrorLabel.textAlignment = NSTextAlignmentCenter;
    _topErrorLabel.text = @"呃呃,加载失败了......";
    _topErrorLabel.font = [UIFont systemFontOfSize:12*kDeviceRate];
    _topErrorLabel.textColor = UIColorFromRGB(0xffffff);
    [_backgroundIV addSubview:_topErrorLabel];
    
    [_topErrorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.backgroundIV.mas_top).offset(15*kDeviceRate);
        make.left.mas_equalTo(wself.backgroundIV.mas_left).offset(0*kDeviceRate);
        make.right.mas_equalTo(wself.backgroundIV.mas_right);
        make.height.mas_equalTo(45*kDeviceRate);
    }];
    
    _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_refreshBtn setImage:[UIImage imageNamed:@"btn_hum_player_refresh"] forState:UIControlStateNormal];
    [_backgroundIV addSubview:_refreshBtn];
    
    [_refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.backgroundIV.mas_bottom).offset(-15*kDeviceRate);
        make.left.mas_equalTo(wself.backgroundIV.mas_left).offset(60*kDeviceRate);
        make.width.mas_equalTo(13*kDeviceRate);
        make.height.mas_equalTo(15*kDeviceRate);
        
    }];
    
    _refreshLabel = [[UILabel alloc]init];
    [_refreshLabel setBackgroundColor:[UIColor clearColor]];
    _refreshLabel.textAlignment = NSTextAlignmentLeft;
    _refreshLabel.text = @"刷新重试";
    _refreshLabel.font = [UIFont systemFontOfSize:12*kDeviceRate];
    _refreshLabel.textColor = App_selected_color;
    [_backgroundIV addSubview:_refreshLabel];
    
    [_refreshLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.refreshBtn.mas_right).offset(5*kDeviceRate);
        make.bottom.mas_equalTo(wself.refreshBtn.mas_bottom);
        make.right.mas_equalTo(_backgroundIV.mas_right);
        make.height.mas_equalTo(15*kDeviceRate);
    }];

    
    

}



-(void)tapGesture:(UITapGestureRecognizer*)tapGesture
{
    NSLog(@"点击了gesture");
    
    if (tapGesture.state == UIGestureRecognizerStateRecognized) {
        
        if (_humourRefresh) {
            
            _humourRefresh();
        }
    }
}



@end
