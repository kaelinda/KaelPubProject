//
//  HumourLoadingView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/15.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HumourLoadingView.h"
@interface HumourLoadingView ()
{
    
    NSTimer *_resetProgressTimer;
    
}

@end
@implementation HumourLoadingView

-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

-(void)setupSubViews
{
    WS(wself);
    
    _loadingView  = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"img_hum_maskView_loading"]];
    [self addSubview:_loadingView];
    CGSize loadingSize = CGSizeMake(65, 65);
    [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(loadingSize);
        make.centerX.mas_equalTo(wself.centerX);
        make.centerY.mas_equalTo(wself.centerY);
        //                make.left.mas_equalTo(wself).offset(200*kRateSize);
        //                make.top.mas_equalTo(wself).offset(110*kRateSize);
        
    }];

    
    _progressLabel = [[UILabel alloc] init];
    [_progressLabel setBackgroundColor:CLEARCOLOR];
    [_progressLabel setTextColor:[UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1]];
    [_progressLabel setFont:[UIFont systemFontOfSize:13.0f]];
    [_progressLabel setText:@"10%"];
    [_progressLabel setHidden:NO];
    [_progressLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_progressLabel];
    
    [_progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerX.mas_equalTo(_loadingView.mas_centerX);
        make.centerY.mas_equalTo(_loadingView.mas_centerY);
    }];

    
    if (_resetProgressTimer) {
        [_resetProgressTimer invalidate];
        _resetProgressTimer = nil;
    }
    
    
    _resetProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(resetProgressWith:) userInfo:nil repeats:YES];

}



-(void)resetProgressWith:(CGFloat)progressValue{
    
    
    if (_progressLabel) {
        
        NSMutableString *progressString = [NSMutableString stringWithFormat:@"%@",_progressLabel.text];
        if (progressString.length>=2) {
            
            if (![progressString rangeOfString:@"%"].location == NSNotFound) {
                [progressString deleteCharactersInRange:NSMakeRange([progressString rangeOfString:@"%"].location, 1)];
            }
            
            if ([progressString integerValue]>94) {
                [_resetProgressTimer invalidate];
                _resetProgressTimer = nil;
                return;
            }
        }
        
        progressValue = [progressString integerValue] + arc4random()%2;
        _progressLabel.text = [NSString stringWithFormat:@"%d%%",(int)progressValue];
        NSLog(@"缓冲进度 -- %@",_progressLabel.text);
        
    }else{
        
    }
}


-(void)clearProgressWithStatus:(BOOL)isStart{
    NSString *value;
    
    if (isStart) {
        
        value = [NSString stringWithFormat:@"%d%%",35+arc4random()%4];
        _progressLabel.text = value;
        NSLog(@"随机初始值 %@",value);
        
        if (_resetProgressTimer) {
            [_resetProgressTimer invalidate];
            _resetProgressTimer = nil;
        }
            _resetProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(resetProgressWith:) userInfo:nil repeats:YES];
        
    }else{
        value = [NSString  stringWithFormat:@"%d%%",97+arc4random()%5];
        _progressLabel.text = value;
        NSLog(@"随机最终值 %@",value);
        
    }
}


-(void)setHidden:(BOOL)hidden{
    
    
    
    if (hidden==NO) {
        [self clearProgressWithStatus:YES];
        NSLog(@"显示的时候随机初始值 %@",_progressLabel.text);
        
    }else if (hidden==YES){
        [self clearProgressWithStatus:NO];
        NSLog(@"结束的时候随机结束值 %@",_progressLabel.text);
        
    }
    
    if (_loadingView) {
        if (_loadingView.hidden==hidden) {
            if (hidden) {
                NSLog(@"loading视图隐藏重复了~");
            }else{
                NSLog(@"loading视图显示重复了~");
            }
            _loadingView.hidden = hidden;
            
            return;
        }else{
            _loadingView.hidden = hidden;
            
        }
    }else{
        NSLog(@"还没创建loading视图呢~");
    }
    
    [super setHidden:hidden];
    
}


@end
