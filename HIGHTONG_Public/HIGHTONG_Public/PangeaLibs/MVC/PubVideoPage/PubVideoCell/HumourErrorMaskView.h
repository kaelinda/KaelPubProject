//
//  HumourErrorMaskView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^HumourPlayer_TO_refresh)();
@interface HumourErrorMaskView : UIView
@property (nonatomic, strong) UIImageView *backgroundIV;
@property (nonatomic, strong) UILabel *topErrorLabel;
@property (nonatomic, strong) UIButton *refreshBtn;
@property (nonatomic, strong) UILabel *refreshLabel;
@property (nonatomic, copy) HumourPlayer_TO_refresh humourRefresh;




@end
