//
//  PubVideoProgramCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubVideoProgramCell.h"
#import "Masonry.h"

@interface PubVideoProgramCell()

/**
 *  @author Dirk
 *
 *  @brief 视频时长显示区域
 */
@property (nonatomic, strong) UIView *programTimeView;

/**
 *  @author Dirk
 *
 *  @brief 视频时长图标
 */
@property (nonatomic, strong) UIImageView *timeTaskImageView;

/**
 *  @author Dirk
 *
 *  @brief 视频名称
 */
@property (nonatomic, strong) UILabel *titleLabel;

/**
 *  @author Dirk
 *
 *  @brief 视频发布时间
 */
@property (nonatomic, strong) UILabel *dateLabel;

/**
 *  @author Dirk
 *
 *  @brief 视频时长
 */
@property (nonatomic, strong) UILabel *timeLabel;

/**
 *  @author Dirk
 *
 *  @brief 视频图片
 */
@property (nonatomic, strong) UIImageView *programImage;

/**
 *  @author Dirk
 *
 *  @brief 分享按钮
 */
@property (nonatomic, strong) UIButton_Block *shareBtn;

/**
 *  @author Dirk
 *
 *  @brief 点赞按钮
 */
@property (nonatomic, strong) UIButton_Block *supportBtn;


@end

@implementation PubVideoProgramCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.programDic = [[NSMutableDictionary alloc]init];;
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{

    UIView *superView = self.contentView;
    superView.backgroundColor = [UIColor grayColor];
    
    
    self.programImage = [[UIImageView alloc]init];;
    [superView addSubview:self.programImage];
    [self.programImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.top.equalTo(superView.mas_top);
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(superView.mas_bottom).offset(-50*kDeviceRate);
        
    }];
    
    
    self.maskImage = [[UIImageView alloc]init];
    self.maskImage.frame = CGRectMake(kDeviceWidth*2/3, 200*kDeviceRate, kDeviceWidth/3, 50*kDeviceRate);
    [self.programImage addSubview:self.maskImage];
    [self.programImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.programImage);
    }];
    
    
    
    self.titleLabel = [[UILabel alloc]init];;
    [self.programImage addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.programImage.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(self.programImage.mas_top).offset(2*kDeviceRate);;
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(100*kDeviceRate));

    }];
    
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = HT_FONT_THIRD;
    self.titleLabel.textColor = HT_COLOR_FONT_INVERSE;
    
    
    self.dateLabel = [[UILabel alloc]init];;
    [self.programImage addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.programImage.mas_right).offset(-6*kDeviceRate);
        make.top.equalTo(self.programImage.mas_top).offset(2*kDeviceRate);;
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(100*kDeviceRate));
        
    }];

    self.dateLabel.textAlignment = NSTextAlignmentRight;
    self.dateLabel.font = HT_FONT_THIRD;
    self.dateLabel.textColor = HT_COLOR_FONT_INVERSE;
    
    
    self.programTimeView = [[UIView alloc]init];;
    [self.programImage addSubview:self.programTimeView];
    [self.programTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.programImage.mas_right).offset(-12*kDeviceRate);
        make.bottom.equalTo(self.programImage.mas_bottom).offset(-2*kDeviceRate);;
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(60*kDeviceRate));
        
    }];
    
    
    self.timeLabel = [[UILabel alloc]init];;
    [self.programTimeView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.programTimeView.mas_right);
        make.top.equalTo(self.programTimeView.mas_top);
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(40*kDeviceRate));
        
    }];
    
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.font = HT_FONT_FOURTH;
    self.timeLabel.textColor = HT_COLOR_FONT_INVERSE;

    
    self.timeTaskImageView = [[UIImageView alloc]init];;
    [self.programTimeView addSubview:self.timeTaskImageView];
    [self.timeTaskImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.programTimeView.mas_left);
        make.top.equalTo(self.programTimeView.mas_top);
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(20*kDeviceRate));
    }];
    
    
    self.supportBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    
    self.supportBtn.frame = CGRectMake(kDeviceWidth*2/3, self.frame.size.height -50*kDeviceRate, kDeviceWidth/3, 50*kDeviceRate);
    
    [self addSubview:self.supportBtn];
    
    [self.supportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.bottom.equalTo(superView.mas_bottom);
        make.height.equalTo(@(50*kDeviceRate));
        make.width.equalTo(@(kDeviceWidth/3));
    }];
    
    
    [self.supportBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_normal"] forState:UIControlStateNormal];
    [self.supportBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_pressed"] forState:UIControlStateSelected];

    
    
    self.supportBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 60);
    [self.supportBtn setTitle:@"" forState:UIControlStateNormal];
    
    self.supportBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    
    [self.supportBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateNormal];
    
    [self.supportBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateHighlighted];
    self.supportBtn.titleLabel.font = HT_FONT_SECOND;
    
    
    self.shareBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    self.shareBtn.frame = CGRectMake(kDeviceWidth*2/3, self.frame.size.height - 50*kDeviceRate, kDeviceWidth/3, 50*kDeviceRate);
    
    [self addSubview:self.shareBtn];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(superView.mas_bottom);
        make.height.equalTo(@(50*kDeviceRate));
        make.width.equalTo(@(kDeviceWidth/3));
    }];

    
    [self.shareBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_normal"] forState:UIControlStateNormal];
    [self.shareBtn setImage:[UIImage imageNamed:@"btn_hum_player_share_normal"] forState:UIControlStateHighlighted];


    self.shareBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 60);
    [self.shareBtn setTitle:@"分享" forState:UIControlStateNormal];

    self.shareBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);

    [self.shareBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateNormal];

    [self.shareBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateHighlighted];
    self.shareBtn.titleLabel.font = HT_FONT_SECOND;
    

    
}

- (void)update
{
    
    WS(wself);
    
    self.titleLabel.text = [self.programDic objectForKey:@"name"];
    
    self.dateLabel.text = [self.programDic objectForKey:@"date"];
    
    self.timeLabel.text = [self.programDic objectForKey:@"duration"];
    
    [self.programImage sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([self.programDic objectForKey:@"imageUrl"])] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];//公版改版
    
    if ([self.programDic objectForKey:@"isPraise"]||[[self.programDic objectForKey:@"isPraise"]integerValue] == 1) {
        
        self.supportBtn.selected = YES;
        
    }else
    {
        
        self.supportBtn.selected = NO;
        
    }
    
    [self.supportBtn setTitle:[self.programDic objectForKey:@"praise"] forState:UIControlStateNormal];
    
    
    self.supportBtn.polyMedicDic = self.programDic;
    self.supportBtn.Click = ^(UIButton_Block *btn, NSString *name){
        if (wself.supportBlock) {
            wself.supportBlock(btn,name);
        }
    };
    
    self.shareBtn.polyMedicDic = self.programDic;
    self.shareBtn.Click = ^(UIButton_Block *btn, NSString *name){
        if (wself.shareBlock) {
            wself.shareBlock(btn,name);
        }
    };
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
