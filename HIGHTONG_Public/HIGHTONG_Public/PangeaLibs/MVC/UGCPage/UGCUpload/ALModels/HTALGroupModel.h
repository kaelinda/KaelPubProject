//
//  HTALGroupModel.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface HTALGroupModel : NSObject

//组名（相册名）
@property (nonatomic, copy)NSString *groupName;

//缩略图
@property (nonatomic, strong)UIImage *thumbImage;

//组里面图片的个数
@property (nonatomic, assign)NSInteger assetsCount;

//类型？
@property (nonatomic, copy)NSString *type;

@property (nonatomic, strong)ALAssetsGroup *group;

@end
