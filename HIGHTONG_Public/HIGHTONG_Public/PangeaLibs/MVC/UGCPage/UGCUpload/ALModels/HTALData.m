//
//  HTALData.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALData.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface HTALData ()

@property (nonatomic, strong)ALAssetsLibrary *library;

@end

@implementation HTALData


+ (instancetype)defaultGroup
{
    return [[self alloc] init];
}

+ (ALAssetsLibrary *)defaultAssetsLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    
    return library;
}

- (ALAssetsLibrary *)library
{
    if (nil == _library) {
        _library = [self.class defaultAssetsLibrary];
    }
    
    return _library;
}

#pragma mark 获取所有组
- (void)getAllGroupWithIsPhoto:(BOOL)isPhoto withBlock:(HTALDataCallBackBlock)callBlock
{
    NSMutableArray *groupArr = [[NSMutableArray alloc] init];
    
    [self.library enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        if (group) {
            
            if (isPhoto) {
                [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            }else{
                [group setAssetsFilter:[ALAssetsFilter allVideos]];
            }
            
            if (group.numberOfAssets > 0) {//过滤空相册

                HTALGroupModel *model = [[HTALGroupModel alloc] init];
                model.group = group;
                model.groupName = [group valueForProperty:ALAssetsGroupPropertyName];//@"ALAssetsGroupPropertyName"
                model.thumbImage = [UIImage imageWithCGImage:[group posterImage]];
                model.assetsCount = [group numberOfAssets];
                [groupArr addObject:model];
            }
        }else{
            
            if (groupArr.count > 0 ) {
                
                callBlock(groupArr);
            }else{
            
                callBlock([NSArray array]);//@"所有相册皆为空"
            }
        }
    } failureBlock:^(NSError *error) {
       
        NSLog(@"获取组失败\n%@",error);
    }];
}

- (void)getPhotosInAllGroupWithCallBack:(HTALDataCallBackBlock)callBackBlock
{
    [self getAllGroupWithIsPhoto:YES withBlock:callBackBlock];
}

- (void)getVideosInAllGroupWithCallBack:(HTALDataCallBackBlock)callBackBlock
{
    [self getAllGroupWithIsPhoto:NO withBlock:callBackBlock];
}

- (void)getGroupAssetWithGroupModel:(HTALGroupModel *)groupModel andFinished:(HTALDataCallBackBlock)callBackBlock
{
    NSMutableArray *assetArr = [[NSMutableArray alloc] init];
    [groupModel.group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result) {
            
            [assetArr addObject:result];
        }else{
        
            callBackBlock(assetArr);
        }
    }];
}

- (void)getImageWithURL:(NSURL *)url andCallBack:(HTALDataCallBackBlock)callBackBlock
{
    [self.library assetForURL:url resultBlock:^(ALAsset *asset) {
        dispatch_async(dispatch_get_main_queue(), ^{
           
            callBackBlock([UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]]);
        });
    } failureBlock:^(NSError *error) {
        
        NSLog(@"获取image失败\n%@",error);
    }];
}



@end
