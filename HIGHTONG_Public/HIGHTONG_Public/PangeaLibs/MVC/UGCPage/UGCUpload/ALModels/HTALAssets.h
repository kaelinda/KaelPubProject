//
//  HTALAssets.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface HTALAssets : NSObject

@property (nonatomic, strong)ALAsset *asset;

@property (nonatomic, assign)BOOL isVideo;

//缩略图
- (UIImage *)thumbnailImg;

//压缩原图
- (UIImage *)compressionImg;

//全尺寸图
- (UIImage *)fullScreenImg;

//原图
- (UIImage *)fullResolutionImg;

//url
- (NSURL *)assetURL;

//时长
- (NSString *)videoDuration;

//大小
- (CGSize)photoSize;

//字节数（大小）
- (CGFloat)videoSize;


//-------------------------#warning 存到沙盒后，上传完成或者失败确认不需要后应该删掉，省内存，应添加删除的方法

/**
 将ALasset中得到的URL转换成能够上传的URL

 @param compressionTypeStr 压缩的水平 （不需要压缩时，随意传个字符串即可）
 @return 沙盒中的URL
 */
- (NSURL *)compressAssetUrlWithCompressionType:(NSString *)compressionTypeStr;


@end
