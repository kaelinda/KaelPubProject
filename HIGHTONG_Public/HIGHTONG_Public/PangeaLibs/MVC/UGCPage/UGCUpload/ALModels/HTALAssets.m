//
//  HTALAssets.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALAssets.h"
#import "AssetsLabraryHeader.h"
#import <AVFoundation/AVFoundation.h>

@implementation HTALAssets

- (UIImage *)thumbnailImg
{
    if (IOS9_OR_LATER) {
        return [UIImage imageWithCGImage:[self.asset aspectRatioThumbnail]];
    }else{
    
        return [UIImage imageWithCGImage:[self.asset thumbnail]];
    }
}

- (UIImage *)compressionImg
{
    UIImage *fullScreenImg = [UIImage imageWithCGImage:[[self.asset defaultRepresentation] fullScreenImage]];
    NSData *data = UIImageJPEGRepresentation(fullScreenImg, 0.1);
    UIImage *img = [UIImage imageWithData:data];
    
    fullScreenImg = nil;
    data = nil;
    return img;
}

- (UIImage *)fullScreenImg
{
    return [UIImage imageWithCGImage:[[self.asset defaultRepresentation] fullScreenImage]];
}

- (UIImage *)fullResolutionImg
{
    ALAssetRepresentation *representation = [self.asset defaultRepresentation];
    CGImageRef iref = [representation fullResolutionImage];
    
    UIImage *img = [UIImage imageWithCGImage:iref scale:[representation scale] orientation:(UIImageOrientation)[representation orientation]];
    return img;
}

- (BOOL)isVideo
{
    NSString *type = [self.asset valueForProperty:ALAssetPropertyType];
    
    return [type isEqualToString:ALAssetTypeVideo];
}

- (CGFloat)videoSize
{
    CGFloat size;
    
    size = [[self.asset defaultRepresentation] size]/1024/1024;
    
    return size;
}

- (NSURL *)assetURL
{

    return [[self.asset defaultRepresentation] url];
}

- (CGSize)photoSize
{
    return [[self.asset defaultRepresentation] dimensions];
}

//将原始视频的URL转化为NSData数据,写入沙盒
- (void)videoWithUrl:(NSString *)url withFileName:(NSString *)fileName
{
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        if (url) {
            [assetLibrary assetForURL:[NSURL URLWithString:url] resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *rep = [asset defaultRepresentation];
                NSString *pathDocuments = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *imagePath = [NSString stringWithFormat:@"%@/Image", pathDocuments];
                NSString *dbFilePath = [imagePath stringByAppendingPathComponent:fileName];
                char const *cvideoPath = [dbFilePath UTF8String];
                FILE *file = fopen(cvideoPath, "a+");
                if (file) {
                    const int bufferSize = 11024 * 1024;
                    // 初始化一个1M的buffer
                    Byte *buffer = (Byte*)malloc(bufferSize);
                    NSUInteger read = 0, offset = 0, written = 0;
                    NSError* err = nil;
                    if (rep.size != 0)
                    {
                        do {
                            read = [rep getBytes:buffer fromOffset:offset length:bufferSize error:&err];
                            written = fwrite(buffer, sizeof(char), read, file);
                            offset += read;
                        } while (read != 0 && !err);//没到结尾，没出错，ok继续
                    }
                    // 释放缓冲区，关闭文件
                    free(buffer);
                    buffer = NULL;
                    fclose(file);
                    file = NULL;
                }
            } failureBlock:nil];
        }
    });
}


- (NSURL *)compressAssetUrlWithCompressionType:(NSString *)compressionTypeStr
{
    NSURL *originalUrl = [self assetURL];
    NSLog(@"原始的URL\n %@",originalUrl);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];//用时间戳给文件重命名，避免冲突
    [formatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    
//    NSData *data = [NSData dataWithContentsOfURL:originalUrl];
//    CGFloat totalSize = (float)data.length / 1024 / 1024;
//    NSLog(@"压缩前的大小\n %f",totalSize);
    
    NSURL *compressedUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mov",[formatter stringFromDate:[NSDate date]]]];//存在沙盒中的路径
    NSLog(@"自己沙盒中的URL\n %@",compressedUrl);

    
    //是否压缩部分
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:originalUrl options:nil];//用原始URL初始化
    NSArray *compressTypeArr = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    if ([compressTypeArr containsObject:compressionTypeStr]) {//支持压缩，格式要符合三种压缩格式之一

        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:compressionTypeStr];
        exportSession.outputURL = compressedUrl;
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.shouldOptimizeForNetworkUse = YES;
    
        [exportSession exportAsynchronouslyWithCompletionHandler:^(void){
            switch (exportSession.status){
                case AVAssetExportSessionStatusCancelled:
                {
                    NSLog(@"AVAssetExportSessionStatusCancelled");
                    break;
                }
                case AVAssetExportSessionStatusUnknown:
                {
                    NSLog(@"AVAssetExportSessionStatusUnknown");
                    break;
                }
                case AVAssetExportSessionStatusWaiting:
                {
                    NSLog(@"AVAssetExportSessionStatusWaiting");
                    break;
                }
                case AVAssetExportSessionStatusExporting:
                {
                    NSLog(@"AVAssetExportSessionStatusExporting");
                    break;
                }
                case AVAssetExportSessionStatusCompleted:
                {
                    NSLog(@"压缩成功");
                    
                    NSData *tempData = [NSData dataWithContentsOfURL:compressedUrl];
                    CGFloat tempTotalSize = (float)tempData.length / 1024 / 1024;
                    NSLog(@"压缩后的大小\n %f",tempTotalSize);
                    
                    break;
                }
                case AVAssetExportSessionStatusFailed:
                {
                    NSLog(@"压缩失败啦");
                    break;
                }
            }
        }];
        
    }else{//不支持压缩
    
        return compressedUrl;
    }
    
    return [[NSURL alloc] init];
}


- (NSString *)videoDuration
{

    long long breakPoint=[[self.asset valueForProperty:ALAssetPropertyDuration] longLongValue];
    if (breakPoint<60) {
        return [NSString stringWithFormat:@"0:%.2ld",(long)breakPoint];
    }
    
    else if (breakPoint<3600){
        return [NSString stringWithFormat:@"%.2lld:%.2lld",breakPoint/60,breakPoint%60];
        
    }
    
    else if (breakPoint<3600*60){
        return [NSString stringWithFormat:@"%.2lld:%.2lld:%.2lld",breakPoint/3600,(breakPoint%3600)/60,breakPoint%60];
    }
    return [NSString stringWithFormat:@"00:00:00"];
}

@end
