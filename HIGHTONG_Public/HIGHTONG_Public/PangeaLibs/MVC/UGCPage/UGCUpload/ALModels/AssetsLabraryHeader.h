//
//  AssetsLabraryHeader.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#ifndef AssetsLabraryHeader_h
#define AssetsLabraryHeader_h


#define GetImage(imageName)  [UIImage imageNamed:imageName]


//#define IOS9_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"9.0"] != NSOrderedAscending )
#define IOS9_OR_LATER  ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending)

//资源类型-视频还是照片
typedef NS_ENUM(NSUInteger, ALVideoPhotoAssetType) {
    ALShowPhoto = 0,
    ALShowVideo,
};

//单选还是多选
typedef NS_ENUM(NSUInteger, ALVideoPhotoChoiceType) {
    ALSingleChoice = 0,
    ALMutipleChoice,
};

typedef NS_ENUM(NSUInteger, ALVideoCompressionType) {
    ALHighestQuality = 0,//高
    ALMediumQuality,//中
    ALLowQuality,//低
};


static NSString *PICKER_TAKE_DONE = @"PICKER_TAKE_DONE";

// 图片最多显示9张，超过9张取消单击事件
static NSInteger const KPhotoShowMaxCount = 9;

#endif 
