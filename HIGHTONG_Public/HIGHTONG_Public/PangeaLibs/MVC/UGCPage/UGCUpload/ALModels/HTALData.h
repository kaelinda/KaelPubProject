//
//  HTALData.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTALGroupModel.h"

typedef void(^HTALDataCallBackBlock)(id obj);

@interface HTALData : NSObject


+ (instancetype)defaultGroup;

/**
 获取所有组中的照片

 @param callBackBlock 回调
 */
- (void)getPhotosInAllGroupWithCallBack:(HTALDataCallBackBlock)callBackBlock;

/**
 获取所有组中的视频

 @param callBackBlock 回调
 */
- (void)getVideosInAllGroupWithCallBack:(HTALDataCallBackBlock)callBackBlock;

/**
 根据group获取Asset

 @param groupModel group
 @param callBackBlock 回调
 */
- (void)getGroupAssetWithGroupModel:(HTALGroupModel *)groupModel andFinished:(HTALDataCallBackBlock)callBackBlock;

/**
 根据URL获取UIimage

 @param url Asset中的URL
 @param callBackBlock 回调
 */
- (void)getImageWithURL:(NSURL *)url andCallBack:(HTALDataCallBackBlock)callBackBlock;


@end
