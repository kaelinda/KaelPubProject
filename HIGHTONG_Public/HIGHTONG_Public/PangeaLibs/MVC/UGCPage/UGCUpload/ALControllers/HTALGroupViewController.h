//
//  HTALGroupViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "AssetsLabraryHeader.h"
#import "HTALViewController.h"

@interface HTALGroupViewController : HT_FatherViewController

@property (nonatomic, assign)ALGroupType groupType;

@property (nonatomic, assign)ALVideoPhotoAssetType assetType;//资源类型

@property (nonatomic, assign)ALVideoPhotoChoiceType choiceType;//单选or多选

@property (nonatomic, assign)NSInteger maxCount;

@property (nonatomic, strong)NSMutableArray *selectAssets;

@property (nonatomic, assign)BOOL topShowPhotoPicter;


- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType;


@end
