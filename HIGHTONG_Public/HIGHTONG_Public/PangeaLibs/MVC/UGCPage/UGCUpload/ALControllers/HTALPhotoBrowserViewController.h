//
//  HTALPhotoBrowserViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^BrowserVCBlock)(NSArray *assetsArr);

@interface HTALPhotoBrowserViewController : HT_FatherViewController

@property (nonatomic, strong)UIImage *SelectImg;

@property (nonatomic, strong)NSMutableArray *dataArr;

@property (nonatomic, copy)BrowserVCBlock browseBlock;


@end
