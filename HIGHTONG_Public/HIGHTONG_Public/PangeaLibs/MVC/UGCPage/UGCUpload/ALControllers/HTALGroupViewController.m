//
//  HTALGroupViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTALGroupViewController.h"
#import "HTALGroupTableViewCell.h"
#import "HTALData.h"
#import "HTALGroupModel.h"
#import "HTALAssetsViewController.h"

@interface HTALGroupViewController ()<UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong)UIButton *cancelBtn;

@property (nonatomic, strong)UITableView *tableview;

@property (nonatomic, strong)NSArray *groupArr;

@property (nonatomic, copy)NSString *selectGroupURL;


@end

@implementation HTALGroupViewController

- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType
{
    self = [super init];
    if (self) {
        
        self.assetType = assetType;
        
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

    [self setUpNaviBar];//设置导航条
    
    [self setAlbumAuthorization];//访问相册的权限
}

- (void)setUpNaviBar
{
    [self setNaviBarTitle:@"系统相册"];
    
    _cancelBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_user_cancel" imgHighlight:@"btn_user_cancel" imgSelected:@"btn_user_cancel" target:self action:@selector(cancelBtnSenderDown)];
    
    [self setNaviBarRightBtn:_cancelBtn];
    
    self.view.backgroundColor = UIColorFromRGB(0xffffff);
}

#pragma mark 取消按钮 回到调取相册之前
- (void)cancelBtnSenderDown
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setAlbumAuthorization
{
    NSString *tipTextWhenNoPhotosAuthorization;
    ALAuthorizationStatus authorizationStatus = [ALAssetsLibrary authorizationStatus];
    if (authorizationStatus == ALAuthorizationStatusRestricted || authorizationStatus == ALAuthorizationStatusDenied) {
        NSDictionary *mainInfoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [mainInfoDictionary objectForKey:@"CFBundleDisplayName"];
        tipTextWhenNoPhotosAuthorization = [NSString stringWithFormat:@"请在设备的\"设置-隐私-照片\"选项中，允许%@访问你的手机相册",appName];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:tipTextWhenNoPhotosAuthorization delegate:self cancelButtonTitle:nil otherButtonTitles:@"朕知道了", nil];
        [alertView show];

        [self dismissViewControllerAnimated:NO completion:nil];
        
    }else{
        
        [self setUpTableview];
        
        [self getGroupData];
    }
}

- (void)setUpTableview
{
    UIView *tempView = [[UIView alloc] init];
    [self.view addSubview:tempView];
    
    _tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    _tableview.separatorStyle = UITableViewCellEditingStyleNone;
    _tableview.delegate = self;
    _tableview.showsHorizontalScrollIndicator = NO;
    _tableview.showsVerticalScrollIndicator = NO;
    
    [self.view addSubview:_tableview];
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.groupArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"HTALGroupTableViewCellIdentifier";
    
    HTALGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        
        cell = [[HTALGroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    cell.groupModel = self.groupArr[indexPath.row];
    
    return cell;
}

#pragma mark -<UITableViewDelegate>
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82*kDeviceRate;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HTALGroupModel *groupModel = self.groupArr[indexPath.row];
    
    self.selectGroupURL = [[groupModel.group valueForProperty:ALAssetsGroupPropertyURL] absoluteString];
    
    HTALAssetsViewController *assetsVc = [[HTALAssetsViewController alloc] initWithShowType:self.assetType];//init
    
    assetsVc.choiceType = self.choiceType;
    assetsVc.selectedAssetsBlock = ^(NSMutableArray *selectedAssets){
        //回传选择的照片，实现选择记忆
        self.selectAssets = selectedAssets;
    };
    assetsVc.selectPickerAssets = self.selectAssets;
    assetsVc.assetGroup = groupModel;
    assetsVc.topShowPhotoPicker = self.topShowPhotoPicter;
    assetsVc.maxCount = self.maxCount;
    [self.navigationController pushViewController:assetsVc animated:YES];
}


//获取相册
- (void)getGroupData
{
    HTALData *data = [HTALData defaultGroup];
    
    __weak typeof(self)weakSelf = self;
    if (self.groupType == KAssetsLabraryVideo) {//视频
        
        [data getVideosInAllGroupWithCallBack:^(NSArray *groupArr) {
            
            if (groupArr.count == 0 || groupArr == nil) {
                                
                ZSToast(@"相册内暂无视频资源");
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                return ;
            }
            
            self.groupArr = [[groupArr reverseObjectEnumerator] allObjects];
            
            if (self.groupType) {
                
                //
                [self goToHistoryGroup];
            }
            
            weakSelf.tableview.dataSource = self;
            [weakSelf.tableview reloadData];
        }];
    }else{//照片
        
        [data getPhotosInAllGroupWithCallBack:^(NSArray *groupArr) {
            
            self.groupArr = [[groupArr reverseObjectEnumerator] allObjects];
            
            if (self.groupType) {
                
                //
                [self goToHistoryGroup];
            }
            
            weakSelf.tableview.dataSource = self;
            [weakSelf.tableview reloadData];
        }];
    }
}

- (void)goToHistoryGroup
{
    //这里可以使用单例或者文件本地化存储实现记忆功能
    
    //当前用以下方法代替组别记忆功能
    HTALGroupModel *gp = nil;
    for (HTALGroupModel *group in self.groupArr) {
        
        //        if ((self.groupType == KAssetsLabraryCameraRoll || self.groupType == KAssetsLabraryVideo) && ([group.groupName isEqualToString:@"Camera Roll"] || [group.groupName isEqualToString:@"相机胶卷"])) {
        //            gp = group;
        //            break;
        //        }else if (self.groupType == KAssetsLabrarySavePhotos && ([group.groupName isEqualToString:@"Saved Photos"] || [group.groupName isEqualToString:@"保存相册"])){
        //            gp = group;
        //            break;
        //        }else if (self.groupType == KAssetsLabraryPhotoStream &&  ([group.groupName isEqualToString:@"Stream"] || [group.groupName isEqualToString:@"我的照片流"])){
        //            gp = group;
        //            break;
        //        }
        
        
        if (self.groupType == KAssetsLabraryCameraRoll){
            
            gp = group;
            break;
        }else if (self.groupType == KAssetsLabrarySavePhotos){
            
            gp = group;
            break;
        }else if (self.groupType == KAssetsLabraryPhotoStream){
            
            gp = group;
            break;
        }else if (self.groupType == KAssetsLabraryVideo){
            
            gp = group;
            break;
        }
        //        else if (self.groupType == KAssetsLabraryDefaultGroup){
        //
        //            gp = group;
        //        }
        
    }
    if (!gp) return ;
    [self setupAssetsVCWithGroup:gp];
}

- (void)setupAssetsVCWithGroup:(HTALGroupModel *)group
{    
    HTALAssetsViewController *assetsVc = [[HTALAssetsViewController alloc] initWithShowType:self.assetType];

    assetsVc.choiceType = self.choiceType;
    
    assetsVc.selectedAssetsBlock = ^(NSMutableArray *selectedAssets){
        //回传选择的照片，实现选择记忆
        self.selectAssets = selectedAssets;
    };
    assetsVc.selectPickerAssets = self.selectAssets;
    assetsVc.assetGroup = group;
    assetsVc.topShowPhotoPicker = self.topShowPhotoPicter;
    assetsVc.maxCount = self.maxCount;
    [self.navigationController pushViewController:assetsVc animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
