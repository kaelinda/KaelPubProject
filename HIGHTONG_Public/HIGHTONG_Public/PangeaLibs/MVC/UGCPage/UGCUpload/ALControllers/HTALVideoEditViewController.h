//
//  HTALVideoEditViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "UGCButtonMarkView.h"
#import "HTALAssets.h"

typedef NS_ENUM(NSUInteger, VideoURLType) {
    KLocationAlbumUrlType,
    KNetworkUrlType,
};

typedef void(^VideoModefySuccessBlock)();

@interface HTALVideoEditViewController : HT_FatherViewController

@property (nonatomic, copy)NSString *videoPathStr;

@property (nonatomic, assign)VideoURLType urlType;

@property (nonatomic, strong)NSDictionary *reEditData;//已上传跳进来需要要传的数据

@property (nonatomic, strong)HTALAssets *asset;

@property (nonatomic, copy)VideoModefySuccessBlock modefySuccessBlock;

@end
