//
//  HTALAssetsViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTALAssetsViewController.h"
#import "HTALPickerCollectionView.h"
#import "HTALAssetCollectionCell.h"
#import "HTALPhotoBrowserViewController.h"
#import "HTALVideoEditViewController.h"
#import "HTALAssets.h"
#import "HTALData.h"
#import "CustomAlertView.h"

static NSString *const _cellIdentifier = @"cellIden";

typedef NS_ENUM(NSUInteger, AssetViewBtnTagType) {
    KReturnTag = 30000,
    KCancelTag,
    KPreviewTag,
    KSendTag
};

@interface HTALAssetsViewController ()<CustomAlertViewDelegate>

@property (nonatomic, strong)UIButton *returnBtn;//返回按钮

@property (nonatomic, strong)UIButton *cancelBtn;//取消按钮

@property (nonatomic, strong)HTALPickerCollectionView *collectionView;//照片展示

@property (nonatomic, strong)UIView *footerBackView;

@property (nonatomic, strong)UIButton *sendBtn;//选择使用按钮

@property (nonatomic, strong)UIButton *previewBtn;//预览按钮

@property (nonatomic, weak)UILabel *makeView;//选择使用按钮上面覆盖的统计几个视频照片的lable

@property (nonatomic, weak)UIToolbar *toolBar;

@property (nonatomic, assign)NSUInteger privateTempMaxCount;

@property (nonatomic, strong)NSMutableArray *assetArr;

@property (nonatomic, strong)NSMutableArray *selectAssetArr;


// 1 - 相册浏览器的数据源是 selectAssets， 0 - 相册浏览器的数据源是 assets
@property (nonatomic, assign)BOOL isPreview;
// 是否发送原图
@property (nonatomic, assign)BOOL isOriginal;

@end

@implementation HTALAssetsViewController

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    // 赋值给上一个控制器,以便记录上次选择的照片
    if (self.selectedAssetsBlock) {
        self.selectedAssetsBlock(self.selectAssetArr);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType
{
    self = [super init];
    if (self) {
        self.assetType = assetType;
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *placeholdView = [[UIView alloc] init];
    [self.view addSubview:placeholdView];
    
    [self getAssetData];
    
    [self setUpViewNaviBar];
    
    [self setUpViewFooterBar];
}


#pragma mark - getter
#pragma mark Get Data
- (NSMutableArray *)selectAssetArr{
    if (!_selectAssetArr) {
        _selectAssetArr = [NSMutableArray array];
    }
    return _selectAssetArr;
}


- (void)setSelectPickerAssets:(NSArray *)selectPickerAssets{
    NSSet *set = [NSSet setWithArray:selectPickerAssets];
    _selectPickerAssets = [set allObjects];
    
    if (!self.assetArr) {
        self.assetArr = [NSMutableArray arrayWithArray:selectPickerAssets];
    }else{
        [self.assetArr addObjectsFromArray:selectPickerAssets];
    }
    
    self.selectAssetArr = [selectPickerAssets mutableCopy];
    
    
    self.collectionView.lastDataArr = nil;
    self.collectionView.isRecorderSelectPicker = YES;
    self.collectionView.selectAssetArr = self.selectAssetArr;
    
    
    NSInteger count = self.selectAssetArr.count;
    self.makeView.hidden = !count;
    self.makeView.text = [NSString stringWithFormat:@"%ld",(long)count];
    self.sendBtn.enabled = (count > 0);
    self.previewBtn.enabled = (count > 0);
    
    [self updateToolbar];
}


- (void)setTopShowPhotoPicker:(BOOL)topShowPhotoPicker{
    _topShowPhotoPicker = topShowPhotoPicker;
    
    if (self.topShowPhotoPicker == YES) {
        NSMutableArray *reSortArray= [[NSMutableArray alloc] init];
        for (id obj in [self.collectionView.dataArray reverseObjectEnumerator]) {
            [reSortArray addObject:obj];
        }
        
        HTALAssets *lgAsset = [[HTALAssets alloc] init];
        [reSortArray insertObject:lgAsset atIndex:0];
        
        self.collectionView.collectionViewShowOrderType = PhotoCollectionViewShowOrderTimeAsc;
        
        self.collectionView.topShowPhotoPicker = topShowPhotoPicker;
        self.collectionView.dataArray = reSortArray;
        [self.collectionView reloadData];
    }
}

#pragma mark - setter
-(void)setMaxCount:(NSInteger)maxCount{
    _maxCount = maxCount;
    
    if (!_privateTempMaxCount) {
        _privateTempMaxCount = maxCount;
    }
    
    if (self.selectAssetArr.count == maxCount){
        maxCount = 0;
    }else if (self.selectPickerAssets.count - self.selectAssetArr.count > 0) {
        maxCount = _privateTempMaxCount;
    }
    
    self.collectionView.maxCount = maxCount;
}

- (void)setAssetGroup:(HTALGroupModel *)assetGroup
{
    if (!assetGroup.groupName.length) {
        return;
    }
    _assetGroup = assetGroup;
    
    [self setNaviBarTitle:assetGroup.groupName];
}


- (void)updateToolbar
{
    NSInteger count = self.selectAssetArr.count;
    self.sendBtn.enabled = (count > 0);
    self.previewBtn.enabled = (count > 0);
    
    if (count>0) {
        [self.sendBtn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [self.previewBtn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    }else{
        [self.sendBtn setTitleColor:UIColorFromRGB(0xe8e8e8) forState:UIControlStateNormal];
        [self.previewBtn setTitleColor:UIColorFromRGB(0xe8e8e8) forState:UIControlStateNormal];
    }
}


//--------------------分割线

#pragma mark collectionView
- (HTALPickerCollectionView *)collectionView{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        
        HTALPickerCollectionView *collectionView = [[HTALPickerCollectionView alloc] initWithFrame:CGRectMake(0, 64, kDeviceWidth, KDeviceHeight-64-50*kDeviceRate) collectionViewLayout:layout];
        //时间置顶
        collectionView.collectionViewShowOrderType = PhotoCollectionViewShowOrderTimeDesc;
        collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [collectionView registerClass:[HTALAssetCollectionCell class] forCellWithReuseIdentifier:_cellIdentifier];
        
        collectionView.collectViewDidSelectBlock = ^(HTALPickerCollectionView *collectionView, HTALAssets *deleteAssets){
            NSLog(@"3  collectionBlock被实现了");
            
            if (self.selectPickerAssets.count == 0){
                
                self.selectAssetArr = [NSMutableArray arrayWithArray:collectionView.selectAssetArr];
            } else if (deleteAssets == nil) {
                
                [self.selectAssetArr addObject:[collectionView.selectAssetArr lastObject]];
            } else if(deleteAssets) { //取消所选的照片
                //根据url删除对象
                for (HTALAssets *selectAsset in self.selectPickerAssets) {
                    if ([selectAsset.assetURL isEqual:deleteAssets.assetURL]) {
                        [self.selectAssetArr removeObject:selectAsset];
                    }
                }
            }

            [self updateToolbar];
        };
        
        collectionView.choiceType = _choiceType;
        
        collectionView.assetType = _assetType;
                
        [self.view insertSubview:_collectionView = collectionView belowSubview:self.footerBackView];
    }
    return _collectionView;
}


- (void)getAssetData
{
    if (!self.assetArr) {
        self.assetArr = [NSMutableArray array];
    }
    
    __block NSMutableArray *tempArr = [NSMutableArray array];
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[HTALData defaultGroup] getGroupAssetWithGroupModel:self.assetGroup andFinished:^(NSArray *assets) {
            
            [assets enumerateObjectsUsingBlock:^(ALAsset *asset, NSUInteger idx, BOOL *stop) {
                
                HTALAssets *lgAsset = [[HTALAssets alloc] init];
                lgAsset.asset = asset;
                [tempArr addObject:lgAsset];
            }];
            
            weakSelf.collectionView.dataArray = tempArr;
            [self.assetArr setArray:tempArr];
        }];
    });
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(allButtonSenderDown:)];
    [self setNaviBarLeftBtn:_returnBtn];
    _returnBtn.tag = KReturnTag;
    
    _cancelBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_user_cancel" imgHighlight:@"btn_user_cancel" imgSelected:@"btn_user_cancel" target:self action:@selector(allButtonSenderDown:)];
    [self setNaviBarRightBtn:_cancelBtn];
    _cancelBtn.tag = KCancelTag;
        
    self.view.backgroundColor = UIColorFromRGB(0xffffff);
}

- (void)allButtonSenderDown:(UIButton *)btn
{
    switch (btn.tag) {
        case KReturnTag:
        {//返回
            NSLog(@"返回按钮被点击了");

            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        }
        case KCancelTag:
        {//取消
            NSLog(@"取消按钮被点击了");
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            break;
        }
        case KPreviewTag:
        {//预览
            NSLog(@"预览按钮被点击了");
//            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.selectAssetArr,@"isOriginal":@(self.isOriginal)}];
//                NSLog(@"%@",@(self.isOriginal));
//            });
//
//            [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.selectAssetArr,@"isOriginal":@(self.isOriginal)}];
//            NSLog(@"%@",@(self.isOriginal));

            
            HTALPhotoBrowserViewController *preview = [[HTALPhotoBrowserViewController alloc] init];
            
            if (preview.browseBlock) {
                preview.browseBlock(self.selectAssetArr);
            }
            
            preview.dataArr = self.selectAssetArr;
            
            [self.navigationController pushViewController:preview animated:YES];
            
            break;
        }
        case KSendTag:
        {//使用
            
            if (_assetType == ALShowPhoto) {//照片
                
                NSLog(@"使用图片，保存使用");
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.selectAssetArr}];
                    NSLog(@"选中的照片\n%@",_selectAssetArr);
                });
            }else if (_assetType == ALShowVideo){//视频
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
////                    [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.selectAssetArr,@"isOriginal":@(self.isOriginal)}];
////                    NSLog(@"%@",@(self.isOriginal));
//                    
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetVideoSuccess" object:nil];
//                });
    
                
                HTALAssets *asset = self.selectAssetArr[0];
                
                //本地相册上传，需要先判断视频的大小，如果大于500M则直接弹框提示视频过大取消上传
                CGFloat assetSize = [asset videoSize];
                
                if (assetSize>500) {
                    
                    CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"文件过大，请选择小于500mb的视频" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
            
                    [idAlert show];
                    
                    return;
                }
                
                HTALVideoEditViewController *editView = [[HTALVideoEditViewController alloc] init];
                editView.urlType = KLocationAlbumUrlType;
                editView.asset = asset;
                [self.navigationController pushViewController:editView animated:YES];
            }
            break;
        }
    }
}

- (void)setUpViewFooterBar
{
    __weak typeof(self) weakSelf = self;
    
    _footerBackView = [[UIView alloc] init];
    [self.view addSubview:_footerBackView];
    _footerBackView.backgroundColor = UIColorFromRGB(0x3ab7f5);
    [_footerBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.width.bottom.mas_equalTo(weakSelf.view);
        make.height.mas_equalTo(50*kDeviceRate);
    }];
    
    
    _previewBtn = [self setUpButtonWithTitle:@"预览" andTitleColor:UIColorFromRGB(0xf7f7f7) andFont:18*kDeviceRate andTag:KPreviewTag andTarget:self andSelect:@selector(allButtonSenderDown:)];
    [_footerBackView addSubview:_previewBtn];
    [_previewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(121*kDeviceRate);
        make.height.left.centerY.mas_equalTo(weakSelf.footerBackView);
    }];
    _previewBtn.enabled = YES;
    
    
    if (_assetType == ALShowPhoto) {
        
        _previewBtn.hidden = NO;
    }else{
    
        _previewBtn.hidden = YES;
    }
    
    
    _sendBtn = [self setUpButtonWithTitle:@"使用" andTitleColor:UIColorFromRGB(0xf7f7f7) andFont:18*kDeviceRate andTag:KSendTag andTarget:self andSelect:@selector(allButtonSenderDown:)];
    [_footerBackView addSubview:_sendBtn];
    [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.previewBtn);
        make.right.bottom.mas_equalTo(weakSelf.footerBackView);
    }];
    _sendBtn.enabled = YES;
}

- (UIButton *)setUpButtonWithTitle:(NSString *)titleStr andTitleColor:(UIColor *)color andFont:(CGFloat)fontSize andTag:(NSInteger)tagSize andTarget:(id)target andSelect:(SEL)select
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitle:titleStr forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    btn.tag = tagSize;
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    return btn;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
