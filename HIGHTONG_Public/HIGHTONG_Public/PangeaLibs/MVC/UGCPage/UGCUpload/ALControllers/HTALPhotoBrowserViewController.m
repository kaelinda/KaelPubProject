//
//  HTALPhotoBrowserViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTALPhotoBrowserViewController.h"
#import "AssetsLabraryHeader.h"
#import "HTALAssets.h"
#import "UIView+SaveViewToPhotos.h"

@interface HTALPhotoBrowserViewController ()<UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
    CGFloat lastScale;
    CGFloat _firstX;
    CGFloat _firstY;
}
@property (nonatomic, strong)UIView *backView;

@property (nonatomic, strong)UIScrollView *scrollview;

@property (nonatomic, strong)UIView *snapShotView;
@property (nonatomic, strong)UIImageView *showPhotoImgView;//
@property (nonatomic, strong)UIButton *cancelBtn;//重拍按钮
@property (nonatomic, strong)UIButton *okBtn;//使用照片
@property (nonatomic, strong)UIView *upBackView;//上半部遮罩
@property (nonatomic, strong)UIView *downBackView;//

@property (nonatomic, strong)HTALAssets *asset;

@property (nonatomic, assign)CGSize photoSize;


@end

@implementation HTALPhotoBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self hideNaviBar:YES];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    
    
    
    // 放大缩小手势
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
    [pinchRecognizer setDelegate:self];
    [self.view addGestureRecognizer:pinchRecognizer];
    
    
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    [self.view addGestureRecognizer:panRecognizer];
    
    
//    UIPinchGestureRecognizer *pinGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(changeScale:)];
//    [self.view addGestureRecognizer:pinGesture];
//    self.view.userInteractionEnabled = YES;
//    self.view.multipleTouchEnabled = YES;
//    
//    //图片拖拉手势
//    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(changePoint:)];
//    [self.view addGestureRecognizer:panGesture];
    
    
    _asset = _dataArr[0];
    NSLog(@"asset\n%@，数组\n%@",_asset,_dataArr);
    
    
    _browseBlock = ^(NSArray *assetArr){
    
        _asset = assetArr[0];
        NSLog(@"预览传递过来的\n%@", assetArr);
    };
    
    [self setUpMainUI];
    
//    [self makeUI];
    

}

#pragma mark Gesture
//- (void)changeScale:(UIPinchGestureRecognizer *)sender {
//    UIView *view = sender.view;
//    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
//        view.transform = CGAffineTransformScale(view.transform, sender.scale, sender.scale);
//        sender.scale = 1.0;
//    }
//}
//
//- (void)changePoint:(UIPanGestureRecognizer *)sender {
//    UIView *view = sender.view;
//    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
//        CGPoint translation = [sender translationInView:view.superview];
//        [view setCenter:CGPointMake(view.center.x+translation.x, view.center.y+translation.y)];
//        [sender setTranslation:CGPointZero inView:view.superview];
//    }
//}

// 移动
-(void)move:(id)sender {
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        _firstX = [self.showPhotoImgView center].x;
        _firstY = [self.showPhotoImgView center].y;
    }
    
    translatedPoint = CGPointMake(_firstX+translatedPoint.x, _firstY+translatedPoint.y);
    
    [self.showPhotoImgView setCenter:translatedPoint];
//    [self.view showOverlayWithFrame:photoImage.frame];
}

-(void)scale:(UIPinchGestureRecognizer*)sender {
    
    //当手指离开屏幕时,将lastscale设置为1.0
    if([sender state] == UIGestureRecognizerStateEnded) {
        lastScale = 1.0;
        return;
    }
    
    CGFloat scale = 1.0 - (lastScale - [(UIPinchGestureRecognizer*)sender scale]);
    CGAffineTransform currentTransform = self.showPhotoImgView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
    
    [self.showPhotoImgView setTransform:newTransform];
    lastScale = [sender scale];
}

- (void)makeUI
{
    _scrollview = [[UIScrollView alloc] init];
    _scrollview.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_scrollview];
//    _scrollview.pagingEnabled = YES;
    _scrollview.bounces = YES;
    _scrollview.showsHorizontalScrollIndicator = NO;
    _scrollview.showsVerticalScrollIndicator = NO;
    _scrollview.frame = [UIScreen mainScreen].bounds;//CGRectMake(0, 64, kDeviceWidth, KDeviceHeight-64-50*kDeviceRate);
    
    _scrollview.maximumZoomScale = 1.5;   //最大的放大比例
    _scrollview.minimumZoomScale = 0.5;   //最小的放大比例
    
    _scrollview.delegate = self;
    
    UIImage *image = _asset.fullScreenImg;
    _showPhotoImgView = [[UIImageView alloc] initWithImage:image];
    _showPhotoImgView.contentMode = UIViewContentModeScaleAspectFit;
    [_scrollview addSubview:_showPhotoImgView];
    
    CGRect frame;
    
    frame.size.width = _scrollview.frame.size.width;
    frame.size.height = frame.size.width*(_showPhotoImgView.size.height/_showPhotoImgView.size.width);
    
    _showPhotoImgView.frame = frame;
    _showPhotoImgView.center = _scrollview.center;
    
    
    _scrollview.contentSize = _showPhotoImgView.frame.size;
    
//    _scrollview.contentInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    [self setUpDecorateView];
}

- (void)setUpDecorateView
{

    _backView = [[UIView alloc] init];
    [self.scrollview addSubview:_backView];
    _backView.frame = self.scrollview.bounds;
    _backView.center = self.view.center;
    
    _upBackView = [[UIView alloc] init];
    [self.backView addSubview:_upBackView];
    _upBackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//    [_upBackView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.width.centerX.mas_equalTo(weakSelf.view);
//        make.height.mas_equalTo(225*kDeviceRate);
//    }];
    
    _upBackView.frame = CGRectMake(0, 0, kDeviceWidth, 225*kDeviceRate);
    _upBackView.userInteractionEnabled = YES;
    
    _downBackView = [[UIView alloc] init];
    [self.backView addSubview:_downBackView];
    _downBackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//    [_downBackView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(weakSelf.upBackView);
//        make.centerX.mas_equalTo(weakSelf.upBackView.mas_centerX);
//        make.bottom.mas_equalTo(weakSelf.view.mas_bottom);
//    }];
    _downBackView.frame = CGRectMake(0, KDeviceHeight-225*kDeviceRate, kDeviceWidth, 225*kDeviceRate);
    _downBackView.userInteractionEnabled = YES;
    
    
    _snapShotView = [[UIView alloc] init];
    [self.backView addSubview:_snapShotView];
    _snapShotView.layer.borderWidth = 1.0;
    _snapShotView.layer.borderColor = [[UIColor whiteColor] CGColor];
//    [_snapShotView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(weakSelf.upBackView.mas_bottom);
//        make.bottom.mas_equalTo(weakSelf.downBackView.mas_top);
//        make.width.centerX.mas_equalTo(weakSelf.view);
//    }];
    _snapShotView.userInteractionEnabled = YES;
    _snapShotView.frame = CGRectMake(0, 225*kDeviceRate, kDeviceWidth, 217*kDeviceRate);
    
    
    UIView *blackUpview = [[UIView alloc] init];
    [_upBackView addSubview:blackUpview];
    blackUpview.backgroundColor = [UIColor colorWithRed:31/255 green:31/255 blue:31/255 alpha:1];
//    [blackUpview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.width.top.mas_equalTo(weakSelf.upBackView);
//        make.height.mas_equalTo(64);
//    }];
    blackUpview.frame = CGRectMake(0, 0, kDeviceWidth, 64);
    
    UIView *blackDownView = [[UIView alloc] init];
    [_downBackView addSubview:blackDownView];
    blackDownView.backgroundColor = [UIColor colorWithRed:31/255 green:31/255 blue:31/255 alpha:1];
//    [blackDownView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.width.bottom.mas_equalTo(weakSelf.downBackView);
//        make.height.mas_equalTo(50*kDeviceRate);
//    }];
    blackDownView.frame = CGRectMake(0, (225-50)*kDeviceRate, kDeviceWidth, 50*kDeviceRate);
    
    _cancelBtn = [self setUpButtonWithTitle:@"取消" andTitleColor:UIColorFromRGB(0xdfdfdf) andFont:18*kDeviceRate andTag:301 andTarget:self andSelect:@selector(buttonSenderDown:)];
    [blackDownView addSubview:_cancelBtn];
//    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(121*kDeviceRate);
//        make.height.left.centerY.mas_equalTo(blackDownView);
//    }];
    _cancelBtn.frame = CGRectMake(0, 0, 121*kDeviceRate, 50*kDeviceRate);
    
    _okBtn = [self setUpButtonWithTitle:@"确定" andTitleColor:UIColorFromRGB(0xdfdfdf) andFont:18*kDeviceRate andTag:302 andTarget:self andSelect:@selector(buttonSenderDown:)];
    [blackDownView addSubview:_okBtn];
//    [_okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(weakSelf.cancelBtn);
//        make.right.bottom.mas_equalTo(blackDownView);
//    }];
    _okBtn.frame = CGRectMake(kDeviceWidth-121*kDeviceRate, 0, 121*kDeviceRate, 50*kDeviceRate);

}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _showPhotoImgView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat xcenter = scrollView.center.x , ycenter = scrollView.center.y;
    
    //目前contentsize的width是否大于原scrollview的contentsize，如果大于，设置imageview中心x点为contentsize的一半，以固定imageview在该contentsize中心。如果不大于说明图像的宽还没有超出屏幕范围，可继续让中心x点为屏幕中点，此种情况确保图像在屏幕中心。
    
    xcenter = scrollView.contentSize.width > scrollView.frame.size.width ? scrollView.contentSize.width/2 : xcenter;
    
    ycenter = scrollView.contentSize.height > scrollView.frame.size.height ? scrollView.contentSize.height/2 : ycenter;
    
    [_showPhotoImgView setCenter:CGPointMake(xcenter-10, ycenter-7)];
    
    _backView.center = self.view.center;

}

- (void)setUpMainUI
{

//    _showPhotoImgView = [[UIImageView alloc] init];
//    [self.view addSubview:_showPhotoImgView];
////    [_showPhotoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.width.mas_equalTo(weakSelf.view.mas_width);
////        make.height.mas_equalTo(weakSelf.view.mas_height);
////        make.left.mas_equalTo(weakSelf.view.mas_left);
////        make.top.mas_equalTo(weakSelf.view.mas_top);
////    }];
//    
//    [_showPhotoImgView setImage:_asset.fullScreenImg];
//    _showPhotoImgView.frame = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight);
    
    
    
    _backView = [[UIView alloc] init];
    [self.view addSubview:_backView];
    _backView.frame = CGRectMake(0, 225*kDeviceRate, kDeviceWidth, 217*kDeviceRate);
    _backView.contentMode = UIViewContentModeCenter;
    
    _showPhotoImgView = [[UIImageView alloc] init];
    [_backView addSubview:_showPhotoImgView];
    //    [_showPhotoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.width.mas_equalTo(weakSelf.view.mas_width);
    //        make.height.mas_equalTo(weakSelf.view.mas_height);
    //        make.left.mas_equalTo(weakSelf.view.mas_left);
    //        make.top.mas_equalTo(weakSelf.view.mas_top);
    //    }];
    
    [_showPhotoImgView setImage:_asset.fullScreenImg];
    _showPhotoImgView.frame = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight);
    _showPhotoImgView.clipsToBounds = NO;
    _showPhotoImgView.center = _backView.center;
    
    CGFloat kHeight = _backView.frame.size.height/2 - _showPhotoImgView.size.height/2;
    _showPhotoImgView.frame = CGRectMake(0, kHeight, kDeviceWidth, KDeviceHeight);

    

    
    _upBackView = [[UIView alloc] init];
    [self.view addSubview:_upBackView];
    _upBackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//    [_upBackView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.width.centerX.mas_equalTo(weakSelf.view);
//        make.height.mas_equalTo(225*kDeviceRate);
//    }];
    _upBackView.userInteractionEnabled = YES;
    _upBackView.frame = CGRectMake(0, 0, kDeviceWidth, 225*kDeviceRate);
    
    
    _downBackView = [[UIView alloc] init];
    [self.view addSubview:_downBackView];
    _downBackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//    [_downBackView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(weakSelf.upBackView);
//        make.centerX.mas_equalTo(weakSelf.upBackView.mas_centerX);
//        make.bottom.mas_equalTo(weakSelf.view.mas_bottom);
//    }];
    _downBackView.userInteractionEnabled = YES;
    _downBackView.frame = CGRectMake(0, KDeviceHeight-225*kDeviceRate, kDeviceWidth, 225*kDeviceRate);
    
    
    _snapShotView = [[UIView alloc] init];
    [self.view addSubview:_snapShotView];
    _snapShotView.layer.borderWidth = 1.0;
    _snapShotView.layer.borderColor = [[UIColor whiteColor] CGColor];
//    [_snapShotView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(weakSelf.upBackView.mas_bottom);
//        make.bottom.mas_equalTo(weakSelf.downBackView.mas_top);
//        make.width.centerX.mas_equalTo(weakSelf.view);
//    }];
    _snapShotView.userInteractionEnabled = YES;
    _snapShotView.frame = CGRectMake(0, 225*kDeviceRate, kDeviceWidth, 217*kDeviceRate);
//    _snapShotView.backgroundColor = [UIColor yellowColor];
    
    
    UIView *blackUpview = [[UIView alloc] init];
    [_upBackView addSubview:blackUpview];
    blackUpview.backgroundColor = [UIColor colorWithRed:31/255 green:31/255 blue:31/255 alpha:1];
//    [blackUpview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.width.top.mas_equalTo(weakSelf.upBackView);
//        make.height.mas_equalTo(64);
//    }];
    blackUpview.frame = CGRectMake(0, 0, kDeviceWidth, 64);
    
    
    UIView *blackDownView = [[UIView alloc] init];
    [_downBackView addSubview:blackDownView];
    blackDownView.backgroundColor = [UIColor colorWithRed:31/255 green:31/255 blue:31/255 alpha:1];
//    [blackDownView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.width.bottom.mas_equalTo(weakSelf.downBackView);
//        make.height.mas_equalTo(50*kDeviceRate);
//    }];
    blackDownView.frame = CGRectMake(0, (225-50)*kDeviceRate, kDeviceWidth, 50*kDeviceRate);
    
    
    _cancelBtn = [self setUpButtonWithTitle:@"取消" andTitleColor:UIColorFromRGB(0xdfdfdf) andFont:18*kDeviceRate andTag:301 andTarget:self andSelect:@selector(buttonSenderDown:)];
    [blackDownView addSubview:_cancelBtn];
//    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(121*kDeviceRate);
//        make.height.left.centerY.mas_equalTo(blackDownView);
//    }];
    _cancelBtn.frame = CGRectMake(0, 0, 121*kDeviceRate, 50*kDeviceRate);
    
    
    _okBtn = [self setUpButtonWithTitle:@"确定" andTitleColor:UIColorFromRGB(0xdfdfdf) andFont:18*kDeviceRate andTag:302 andTarget:self andSelect:@selector(buttonSenderDown:)];
    [blackDownView addSubview:_okBtn];
//    [_okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(weakSelf.cancelBtn);
//        make.right.bottom.mas_equalTo(blackDownView);
//    }];
    _okBtn.frame = CGRectMake(kDeviceWidth-121*kDeviceRate, 0, 121*kDeviceRate, 50*kDeviceRate);
}


- (void)buttonSenderDown:(UIButton *)btn
{
    if (btn.tag == 301) {//取消按钮
        
//        [self.navigationController popViewControllerAnimated:YES];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else{//确定按钮
    
        NSLog(@"使用图片，保存使用");
        
        //截图-----
        UIImage *img = [_backView getViewImageWithSize:CGSizeMake(kDeviceWidth, 217*kDeviceRate)];
        
#pragma mark ---------------
        
        
        UIGraphicsBeginImageContext(_showPhotoImgView.frame.size);//全屏截图，包括window
        
        [_showPhotoImgView.layer renderInContext:UIGraphicsGetCurrentContext()];

        UIGraphicsEndImageContext();
        
        //    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
        
//        float originX =_sampleView.frame.origin.x;
//        float originy =_sampleView.frame.origin.y;
//        float width =_sampleView.frame.size.width;
//        float height =_sampleView.frame.size.height;
        //你需要的区域起点,宽,高;
        
//        UIImageView *snapImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,200, 170,170)];
//        snapImg.backgroundColor = [UIColorredColor];
//        snapImg.image = imgeee;
//        [self.viewaddSubview:snapImg];
        
        
        
        
        
        [self.dataArr addObject:img];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.dataArr}];
            NSLog(@"确定要使用的照片\n%@",_dataArr);
        });
    }
}

- (UIButton *)setUpButtonWithTitle:(NSString *)titleStr andTitleColor:(UIColor *)color andFont:(CGFloat)fontSize andTag:(NSInteger)tagSize andTarget:(id)target andSelect:(SEL)select
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitle:titleStr forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    btn.tag = tagSize;
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    return btn;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
