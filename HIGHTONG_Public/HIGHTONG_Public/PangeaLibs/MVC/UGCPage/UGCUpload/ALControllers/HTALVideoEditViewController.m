//
//  HTALVideoEditViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTALVideoEditViewController.h"
#import "UGCAuthItemInputView.h"
#import "CustomBorderView.h"
#import "UGCPlayer.h"
#import "UGCUploadManager.h"
#import "HTRequest.h"
#import "CameraAndAlbum.h"
#import "HTALViewController.h"
#import "HTALAssets.h"
#import "UGCLocalListCenter.h"
#import "CustomAlertView.h"
#import "UGCProgramControlView.h"
#import "HTRequest.h"
#import "UGCManagerHomeViewController.h"
#import "NetWorkNotification_shared.h"


@interface HTALVideoEditViewController ()<HTRequestDelegate, UITextFieldDelegate, CustomAlertViewDelegate, HTRequestDelegate>
{
    BOOL isSameName;
    BOOL isDescript;
    BOOL isCover;
    BOOL isCategory;//标记分类是否改变
    BOOL isTag;
    BOOL categoryIsNull;//标记分类是否为空
    
    CGSize classViewSize;//分裂列表的size
    CGSize markViewSize;//标签列表的size
    CGFloat classViewHeight;//分类view的动态高
    
    NSString *_classSelectStr;//选中的分类按钮的title
    NSString *_classSelectID;//选中的分类按钮的id
    NSString *_tagSelectStr;//选中的标签拼接title或者id字符串
    NSString *_nameStr;//name
    UIImage *_pictureFile;//封面图
    NSString *_remarks;//描述
    
    NSString *_programIdStr;//点播节目ID
    NSString *_coverImgStr;//封面图
}
@property (nonatomic, strong)UIButton *returnBtn;
@property (nonatomic, strong)UIButton *saveBtn;
@property (nonatomic, strong)UIScrollView *scrollview;

@property (nonatomic, strong)UGCPlayer *playerVC;
@property (nonatomic, strong)UGCProgramControlView *playerControlView;
@property (nonatomic, strong)UIImageView *coverImg;

@property (nonatomic, strong)UGCButtonMarkView *classButtonView;
@property (nonatomic, strong)UGCButtonMarkView *markButtonView;

@property (nonatomic, strong)UIView *basicInfoView;
@property (nonatomic, strong)UIView *classLabView;
@property (nonatomic, strong)UIView *MarkLabView;

@property (nonatomic, strong)NSMutableDictionary *uploadDic;//上传参数dic

@property (nonatomic, strong)NSMutableArray *categoryArr;//分类数组
@property (nonatomic, strong)NSMutableArray *tagArr;//标签数组


@property (nonatomic, strong)UIImageView *classUpLine;
@property (nonatomic, strong)UILabel *classLab;
@property (nonatomic, strong)UILabel *mustLab;
@property (nonatomic, strong)UIImageView *classDownLine;
@property (nonatomic, strong)UIImageView *markTopLine;
@property (nonatomic, strong)UILabel *markLab;

@property (nonatomic, strong)NSMutableArray *tagSelectArr;//当前页面所选择标签id数组
@property (nonatomic, strong)NSMutableArray *locTagArr;//已上传进入编辑页面所携带的标签id数组
@property (nonatomic, strong)NSMutableArray *locCategoryArr;//已上传进入编辑页面所携带的分类ID数组
@property (nonatomic, strong)NSMutableArray *locTagCopyArr;//已上传进入编辑页面所携带的标签id 备份 数组（用来做是否改变标记）

@property (nonatomic, strong)UGCAuthItemInputView *nameInput;
@property (nonatomic, strong)UGCAuthItemInputView *scriptionInput;

@property (nonatomic, assign)BOOL isChangePosterImg;

@property (nonatomic, strong)NSString *netInfo;
@end

@implementation HTALVideoEditViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(stopPlayer) name:@"MSG_STOPPLAYER" object:nil];

}

-(void)stopPlayer
{
    [_playerControlView controlViewClearPlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NETCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"信息编辑界面返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);

    if (status == 0 && result.count>0) {
        if ([type isEqualToString:UGC_COMMON_GETCATEGORYLIST]) {//2.12   获取分类列表

            if ([[result objectForKey:@"ret"] integerValue] == 0) {

                _categoryArr = [NSMutableArray arrayWithArray:[result objectForKey:@"categoryList"]];
                NSLog(@"分类列表\n%@",_categoryArr);

                [self refreshClassView:[NSArray arrayWithArray:_categoryArr]];

                if (_tagArr.count > 0) {
                    [self refreshMarkView:[NSArray arrayWithArray:_tagArr]];
                }
            }
        }

        if ([type isEqualToString:UGC_COMMON_GETTAGLIST]) {//2.10   获取标签列表

            if ([[result objectForKey:@"ret"] integerValue] == 0) {

                _tagArr = [NSMutableArray arrayWithArray:[result objectForKey:@"tagList"]];

                NSLog(@"标签列表\n%@",_tagArr);

                if (_categoryArr.count > 0) {
                    [self refreshClassView:[NSArray arrayWithArray:_categoryArr]];
                }

                [self refreshMarkView:[NSArray arrayWithArray:_tagArr]];
            }
        }
        if ([type isEqualToString:UGC_COMMON_GETPROGRAMURL]) {//点播播放
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                
                NSLog(@"点播播放的返回值\n%@",result);

                NSString *typeStr = [result objectForKey:@"type"];
                if ([typeStr isEqualToString:@"1"]) {
                    
                    NSDictionary *urlDic = [[result objectForKey:@"list"] lastObject];
                    
                    _videoPathStr = [urlDic objectForKey:@"url"];
                }
                
                if ([typeStr isEqualToString:@"0"]) {
                 
                    _videoPathStr = [result objectForKey:@"url"];
                }
                
                [_playerControlView.player setFilePath:_videoPathStr];
                
                [_playerControlView controlViewActionPlay:NO];
            }
        }
        if ([type isEqualToString:UGC_COMMON_UPDATEPROGRAM]) {
            
            [self hiddenCustomHUD];
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                
                NSLog(@"修改上传成功");
                
                if (_modefySuccessBlock) {
                    _modefySuccessBlock();
                }
                
                //返回之前停止清空播放器
                [self stopVideoPlayer];
                
                [self.navigationController popViewControllerAnimated:YES];
            }else{
            
                ZSToast(@"更改信息失败，请重新上传");
            }
        }
    }else{

        NSLog(@"请求失败~~~~~~失败");
        
        [self hiddenCustomHUD];
        
        ZSToast(@"更改信息失败，请重新编辑上传");
    }
}

- (void)dataAnalysis
{
    _categoryArr = [NSMutableArray arrayWithArray:[[UGCLocalListCenter defaultCenter] getCategoryArr]];
    _tagArr = [NSMutableArray arrayWithArray:[[UGCLocalListCenter defaultCenter] getTagArr]];
    
    
    if (_categoryArr.count == 0) {
        HTRequest *classListRequest = [[HTRequest alloc] initWithDelegate:self];
        [classListRequest UGCCommonGetCategoryListWithUserId:nil];
    }
    
    if (_tagArr.count == 0) {
        HTRequest *tagListRequest = [[HTRequest alloc] initWithDelegate:self];
        [tagListRequest UGCCommonGetTagList];
    }
    

    if (_urlType == KNetworkUrlType) {//已上传页面进来的
        
        NSString *tempCategoryID = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"categoryId"])?[_reEditData objectForKey:@"categoryId"]:@"";//选中的分类按钮的id
        
        _locCategoryArr = [NSMutableArray array];
        
        if (!isEmptyStringOrNilOrNull(tempCategoryID)) {
            
            [_locCategoryArr addObject:tempCategoryID];
        }
    
        _tagSelectStr = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"tags"])?[_reEditData objectForKey:@"tags"]:@"";//选中的标签拼接title或者id字符串
        
        if (!isEmptyStringOrNilOrNull(_tagSelectStr)) {
            NSArray *arr = [_tagSelectStr componentsSeparatedByString:@","];
            _locTagArr = [NSMutableArray arrayWithArray:arr];
            _locTagCopyArr = [NSMutableArray arrayWithArray:arr];
        }
       
        _nameStr = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"name"])?[_reEditData objectForKey:@"name"]:@"";
        _remarks = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"remarks"])?[_reEditData objectForKey:@"remarks"]:@"";
        _coverImgStr = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"cover"]) ?[_reEditData objectForKey:@"cover"]:@"";
        _programIdStr = !isEmptyStringOrNilOrNull([_reEditData objectForKey:@"programId"])?[_reEditData objectForKey:@"programId"]:@"";
        
        
        if (_programIdStr.length>0) {
            
            HTRequest *playUrlRequest = [[HTRequest alloc] initWithDelegate:self];
            [playUrlRequest UGCCommonGetProgramUrlWithProgramId:_programIdStr];
        }
        
    }else{//本地相册进来的
        
        _pictureFile = [_asset thumbnailImg];//获取封面的缩略图---本地
        
        _classSelectStr = @"";//选中的分类按钮的title
        _classSelectID = @"";//选中的分类按钮的id
        _tagSelectStr = @"";//选中的标签拼接title或者id字符串
        _nameStr = @"";//名称
        _remarks = @"";//描述
        _videoPathStr = [NSString stringWithFormat:@"%@",[_asset assetURL]];//播放路径
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NetWorkNotification_shared shardNetworkNotification]setCanShowToast:YES];//实时网络监测
    //注册通知的观察
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChange:) name:@"NETCHANGED" object:nil];
    classViewHeight = 0;
    
    _tagSelectArr = [NSMutableArray array];

    [self dataAnalysis];
    
    [self addNotification];
    
    [self setUpViewNaviBar];
    
    [self setUpPlayerVC];
    
    [self setUpUIFrame];
    
    [self refreshClassView:_categoryArr];
    [self refreshMarkView:_tagArr];

}

- (void)addNotification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectDone:) name:PICKER_TAKE_DONE object:nil];
    });
}


- (void)selectDone:(NSNotification *)noti
{
    NSArray *selectArray = noti.userInfo[@"selectAssets"];
    
    NSLog(@"通知返回来的数组：\n%@",selectArray);
    
//    HTALAssets *asset = selectArray[0];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        _pictureFile = [asset thumbnailImg];
//                
//        [_coverImg setImage:_pictureFile];
//        
//        _isChangePosterImg = YES;
//    });
    
    if (selectArray.count>1) {//预览后使用截图
        
        UIImage *img = [selectArray lastObject];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            _pictureFile = img;
            
            [_coverImg setImage:_pictureFile];
            
            _isChangePosterImg = YES;
            
        });
    }else{//相册直接确认使用全图
        
        HTALAssets *asset = selectArray[0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            _pictureFile = [asset thumbnailImg];
            
            [_coverImg setImage:_pictureFile];
            
            _isChangePosterImg = YES;
        });
    }
}

- (void)setUpPlayerVC
{
    self.view.userInteractionEnabled = YES;
    if (_urlType == KLocationAlbumUrlType) {//本地用AV

        _playerControlView = [[UGCProgramControlView alloc]initWithPlayerType:KHTPlayer_AVPlay andFatherFrame:CGRectMake(0, 64, kDeviceWidth, 210*kDeviceRate) isALVideoEdite:YES];
        
        _playerControlView.frame = CGRectMake(0, 64, kDeviceWidth, 210*kDeviceRate);
        
    }else{//网络用ARC
        
        _playerControlView = [[UGCProgramControlView alloc]initWithPlayerType:KHTPlayer_ARCPlay andFatherFrame:CGRectMake(0, 64, kDeviceWidth, 210*kDeviceRate) isALVideoEdite:YES];
        
        _playerControlView.frame = CGRectMake(0, 64, kDeviceWidth, 210*kDeviceRate);
    }
    
    _playerControlView.isHTALVideoEditType = YES;
    [_playerControlView.player setFilePath:[_videoPathStr copy]];
    
    [self.view addSubview:_playerControlView];
    
    
    if (_urlType == KLocationAlbumUrlType) {
        
        [_playerControlView controlViewActionPlay:NO];

    }else
    {
            if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
                    nameAlert.tag = 12;
                    [nameAlert show];
                });
            }else
            {
                [_playerControlView controlViewActionPlay:NO];

            }

    }
    

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)networkChange:(NSNotification*)noti
{
    NSString *notiStr = [noti object];
    
    
    
    if (_urlType == KLocationAlbumUrlType) {
        
        return;
    }else
    {
    
    if ([notiStr isEqualToString:@"蜂窝网络"]) {
        [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_playerControlView.playButton setTag:ENGINE_STATUS_PLAY];
        
        [_playerControlView.player pause];
        

        dispatch_async(dispatch_get_main_queue(), ^{
            
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
            nameAlert.tag = 11;
            [nameAlert show];
        });
    }
        
    }
}

#pragma mark - ------- app 进入前后台
-(void)appEnterBackGround{
    if (_playerControlView)
    {
        [_playerControlView.player pause];
        [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_playerControlView.playButton setTag:ENGINE_STATUS_PLAY];
        [_playerControlView.player applicationWillResignActive];
    }
}

-(void)appEnterForeground{
    if (_playerControlView) {
        
        [_playerControlView.player applicationDidBecomeActive];
        
        if (_urlType == KLocationAlbumUrlType) {
            if (_playerControlView.isByUserPause == YES) {
                
                return;
            }else
            {
                [_playerControlView.playButton setTag:ENGINE_STATUS_PAUSE];
                [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
                [_playerControlView.player play];
            }
            return;
        }else
        {
        if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
            [_playerControlView.playButton setTag:ENGINE_STATUS_PLAY];
            [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [_playerControlView.player pause];
        }else
        {
            if (_playerControlView.isByUserPause == YES) {
                
                return;
            }else
            {
                [_playerControlView.playButton setTag:ENGINE_STATUS_PAUSE];
                [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
                [_playerControlView.player play];
            }
        }
        
        }
    }
    
}


- (void)setUpUIFrame
{
    _scrollview = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollview];
    _scrollview.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _scrollview.pagingEnabled = NO;
    _scrollview.bounces = YES;
    _scrollview.showsHorizontalScrollIndicator = NO;
    _scrollview.showsVerticalScrollIndicator = NO;
    _scrollview.frame = CGRectMake(0, 64+210*kDeviceRate, kDeviceWidth, KDeviceHeight-210*kDeviceRate-64);
    
#pragma mark   视频基本信息背景view
    _basicInfoView = [[UIView alloc] init];
    [_scrollview addSubview:_basicInfoView];
    _basicInfoView.frame = CGRectMake(0, 0, kDeviceWidth, 152*kDeviceRate);

    
#pragma mark  名称、描述、封面
    {
        _nameInput = [[UGCAuthItemInputView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 44*kDeviceRate) withTitleName:@"名称" andPlaceholdStr:@"请输入15字以内的名称" andIsHiddenArrow:YES];
        _nameInput.textField.clearButtonMode = UITextFieldViewModeNever;
        [_basicInfoView addSubview:_nameInput];
        _nameInput.textField.delegate = self;
        
        
        UILabel *nameMustLab = [self setLabWithText:@"必填" andTextColor:UIColorFromRGB(0x999999) andFont:14*kDeviceRate];
        [_nameInput addSubview:nameMustLab];
        nameMustLab.textAlignment = NSTextAlignmentRight;
        nameMustLab.frame = CGRectMake(kDeviceWidth-52*kDeviceRate, 0*kDeviceRate, 40*kDeviceRate, 43*kDeviceRate);
        
        _scriptionInput = [[UGCAuthItemInputView alloc] initWithFrame:CGRectMake(0, 44*kDeviceRate, kDeviceWidth, 44*kDeviceRate) withTitleName:@"描述"  andPlaceholdStr:@"请输入20字以内的描述" andIsHiddenArrow:YES];
        [_basicInfoView addSubview:_scriptionInput];
        _scriptionInput.textField.delegate = self;
//        _scriptionInput.textField.borderStyle = UITextBorderStyleNone;
        
        UGCAuthItemInputView *posterInput = [[UGCAuthItemInputView alloc] initWithFrame:CGRectMake(0, 88*kDeviceRate, kDeviceWidth, 64*kDeviceRate) withTitleName:@"封面" andPlaceholdStr:@"点击修改封面图" andIsHiddenArrow:NO];
        [_basicInfoView addSubview:posterInput];
        posterInput.lineImg.hidden = YES;
        posterInput.btnBlock = ^(){
            
            NSLog(@"更换封面按钮被点击了");
            
            CustomAlertView *albumAlert = [[CustomAlertView alloc]initWithButtonNum:0 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"从手机相册选择" withDelegate:self];
            
            albumAlert.tag = 100;
            [albumAlert show];
        };
        
        _coverImg = [[UIImageView alloc] initWithFrame:CGRectMake(kDeviceWidth-100*kDeviceRate, (posterInput.frame.size.height-40)/2*kDeviceRate, 70*kDeviceRate, 40*kDeviceRate)];
        [posterInput addSubview:_coverImg];
        _coverImg.backgroundColor = [UIColor colorWithRed:162/255.0 green:162/255.0 blue:162/255.0 alpha:1];
//        _coverImg.image = [_asset thumbnailImg];
    }
    

    //基本信息赋值
    if (_urlType == KNetworkUrlType) {
        
        if (!isEmptyStringOrNilOrNull(_nameStr)) {
            _nameInput.textField.text = _nameStr;
//            _nameInput.placeLab.hidden = YES;
        }
        
        if (!isEmptyStringOrNilOrNull(_remarks)) {
            _scriptionInput.textField.text = _remarks;
//            _scriptionInput.placeLab.hidden = YES;
        }
        
        if (!isEmptyStringOrNilOrNull(_coverImgStr)) {
            [_coverImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(_coverImgStr)]];
        }
    }else{
        _coverImg.image = _pictureFile;
    }
    
    
    UIImageView *basicInfoDownLine = [self setUpImageView];
    [_scrollview addSubview:basicInfoDownLine];
    basicInfoDownLine.frame = CGRectMake(0, 152*kDeviceRate, kDeviceWidth, 1*kDeviceRate);
    
    
#pragma mark--------------------------分类列表------------------------------
    _classButtonView = [[UGCButtonMarkView alloc] init];
    [_scrollview addSubview:_classButtonView];
    _classButtonView.choiseType = KBtnSingleChoiseType;
    
    if (_urlType == KNetworkUrlType) {
        
        _classButtonView.selectArr = _locCategoryArr;
    }
    
    
    _classUpLine = [self setUpImageView];
    [_classButtonView addSubview:_classUpLine];

    
    _classLab = [self setLabWithText:@"分类" andTextColor:UIColorFromRGB(0x333333) andFont:15*kDeviceRate];
    [_classButtonView addSubview:_classLab];
    _classLab.textAlignment = NSTextAlignmentLeft;

    
    _mustLab = [self setLabWithText:@"必填" andTextColor:UIColorFromRGB(0x999999) andFont:14*kDeviceRate];
    [_classButtonView addSubview:_mustLab];
    _mustLab.textAlignment = NSTextAlignmentRight;

    
    _classDownLine = [self setUpImageView];
    [_classButtonView addSubview:_classDownLine];

    
    if (_categoryArr.count == 0) {
        _classButtonView.hidden = YES;
    }
    
    
    
#pragma mark--------------------------标签列表-------------------------------
    _markButtonView = [[UGCButtonMarkView alloc] init];
    [_scrollview addSubview:_markButtonView];
    _markButtonView.choiseType = KBtnMutiplyChoiseType;
    
    if (_urlType == KNetworkUrlType) {
        
        _markButtonView.selectArr = _locTagArr;
    }
    
    _markTopLine = [self setUpImageView];
    [_markButtonView addSubview:_markTopLine];

    
    _markLab = [self setLabWithText:@"标签" andTextColor:UIColorFromRGB(0x333333) andFont:15*kDeviceRate];
    [_markButtonView addSubview:_markLab];
    _markLab.textAlignment = NSTextAlignmentLeft;


    if (_tagArr.count == 0) {
        _markButtonView.hidden = YES;
    }
}


//根据分类数组刷新分类view的frame
- (void)refreshClassView:(NSArray *)array
{
    classViewSize = [_classButtonView setUpViewWithFrame:CGRectMake(0, 157*kDeviceRate, kDeviceWidth, 120) dataArr:array];
    _classButtonView.btnBlock = ^(NSInteger index, NSDictionary *dic){
        
        categoryIsNull = NO;

        if (dic.count>0) {
            
            _classSelectStr = [dic objectForKey:@"name"]?[dic objectForKey:@"name"]:@"";//选中的分类title
            
            _classSelectID = [dic objectForKey:@"id"]?[dic objectForKey:@"id"]:@"";
            
        }else{
        
            _classSelectStr = @"";
            _classSelectID = @"";
            
            categoryIsNull = YES;
        }
    };
    
    classViewHeight = classViewSize.height;

    _classUpLine.frame = CGRectMake(0, 0, kDeviceWidth, 1*kDeviceRate);

    _classLab.frame = CGRectMake(13*kDeviceRate, 10*kDeviceRate, 100*kDeviceRate, 15*kDeviceRate);

    _mustLab.frame = CGRectMake(kDeviceWidth-100*kDeviceRate, 10*kDeviceRate, 88*kDeviceRate, 15*kDeviceRate);

    _classDownLine.frame = CGRectMake(0, classViewHeight-1*kDeviceRate, kDeviceWidth, 1*kDeviceRate);

    _scrollview.contentSize = CGSizeMake(kDeviceWidth, 157*kDeviceRate + classViewHeight);
}


//根据标签数组刷新标签view的frame
- (void)refreshMarkView:(NSArray *)array
{
    WS(weakSelf);

    markViewSize = [_markButtonView setUpViewWithFrame:CGRectMake(0, 161*kDeviceRate + classViewHeight, kDeviceRate, 120) dataArr:_tagArr];//加间隔4
    
    __block BOOL isSame;
    
    _markButtonView.btnBlock = ^(NSInteger index, NSDictionary *dic){
        
        if (weakSelf.urlType == KNetworkUrlType && weakSelf.locTagCopyArr.count > 0) {//已上传修改信息       上传传过来的标签数组
            
            isSame = NO;
            
            NSString *tagID = [dic objectForKey:@"id"];
            
            [weakSelf.locTagArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                if ([tagID isEqualToString:obj]) {
                    
                    *stop = YES;
                    
                    isSame = YES;
                }
                
                if (*stop) {
                    
                    [weakSelf.locTagArr removeObject:obj];
                }
            }];
            
            if (!isSame) {
                [weakSelf.locTagArr addObject:tagID];
            }
            
            NSLog(@"KNetworkUrlType--选中的标签数组\n%@",weakSelf.locTagArr);
        }
        else{//直接本地相册获取 或 再编辑视频无标签~~~

            isSame = NO;
            
            NSString *tagID = [dic objectForKey:@"id"];
            
            NSLog(@"mark---%@",tagID);
            
            if (_tagSelectArr.count == 0) {//添加第一个
                
                [weakSelf.tagSelectArr addObject:tagID];
            }else{
                
                [weakSelf.tagSelectArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    if ([tagID isEqualToString:obj]) {//再次点击同一个按钮，取消
                        
                        *stop = YES;
                        
                        isSame = YES;
                    }
                    
                    if (*stop == YES) {
                        
                        [weakSelf.tagSelectArr removeObject:obj];
                    }
                }];
                
                if (!isSame) {
                    
                    [weakSelf.tagSelectArr addObject:tagID];
                }
            }
            
            NSLog(@"KLocationAlbumUrlType--选中的标签数组\n%@",weakSelf.tagSelectArr);
        }
    };
    
    _markTopLine.frame = CGRectMake(0, 0, kDeviceWidth, 1*kDeviceRate);

    _markLab.frame = CGRectMake(13*kDeviceRate, 10*kDeviceRate, kDeviceWidth, 15*kDeviceRate);

    _scrollview.contentSize = CGSizeMake(kDeviceWidth, 157*kDeviceRate + classViewHeight + markViewSize.height + 10);
}

//多选标签拼接字符串
- (NSString *)getSelectMarksTitleWith:(NSMutableArray *)array
{
    NSString *resultStr;
    
    for (int i = 0; i<array.count; i++) {

        if (resultStr.length==0) {//[resultStr isEqualToString:@""]
            
            resultStr = array[i];//[NSString stringWithFormat:@"%@",array[i]];
        }else{
        
            resultStr = [NSString stringWithFormat:@"%@,%@",resultStr,array[i]];
        }
    }
    
    return resultStr;
}

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {//调相册
        if(buttonIndex==0)
        {//手机相册
            NSLog(@"确定");
            
            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowPhoto];//单选视频
            pickerVc.groupType = KAssetsLabraryCameraRoll;//视频是照片的具体分类
            pickerVc.maxCount = 1;//设置最多能选几张图片
            pickerVc.choiceType = ALSingleChoice;
            
            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
            [self presentViewController:nav animated:YES completion:nil];
            
        }else{//取消
            
            return;
        }
    }
    
    if (alertView.tag == 11) {
        //取消
        if (buttonIndex == 1) {
//            [alertView hide];
            return;
        }
        //确定
        if (buttonIndex == 0) {
            [_playerControlView.playButton setTag:ENGINE_STATUS_PAUSE];
            [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [_playerControlView.player play];
        }
    }
    if (alertView.tag == 12) {
        //取消
        if (buttonIndex == 1) {
//            [alertView hide];
            return;
        }
        //确定
        if (buttonIndex == 0) {
            [_playerControlView.playButton setTag:ENGINE_STATUS_PAUSE];
            [_playerControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [_playerControlView controlViewActionPlay:NO];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (_nameInput.textField == textField) {
        
        NSInteger realLength = textField.text.length + string.length - range.length;
        
        BOOL isUsed = realLength > 15 ? NO : YES;//toBeString.length
        return isUsed;
    }
    if (_scriptionInput.textField == textField) {
        
        NSInteger realLength = textField.text.length + string.length - range.length;
        
        BOOL isUsed = realLength > 20 ? NO : YES;
        return isUsed;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_nameInput.textField resignFirstResponder];
    [_scriptionInput.textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [_nameInput.textField resignFirstResponder];
    [_scriptionInput.textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameInput.textField resignFirstResponder];
    [_scriptionInput.textField resignFirstResponder];
}

- (UIImageView *)setUpImageView
{
    UIImageView *lineImg = [[UIImageView alloc] init];
    lineImg.backgroundColor = UIColorFromRGB(0xe8e8e8);
    
    return lineImg;
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(returnBtnSenderDown)];
    [self setNaviBarLeftBtn:_returnBtn];
    
    [self setNaviBarTitle:@"信息编辑"];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;

    
    _saveBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.m_viewNaviBar addSubview:_saveBtn];
    if (_urlType == KLocationAlbumUrlType) {
    
        [_saveBtn setTitle:@"保存并上传" forState:UIControlStateNormal];
        _saveBtn.frame = CGRectMake(kDeviceWidth-90, 20, 80, 44);
    }else{
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        _saveBtn.frame = CGRectMake(kDeviceWidth-40, 20, 30, 44);
    }
    [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _saveBtn.backgroundColor = [UIColor clearColor];
    _saveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _saveBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    [_saveBtn addTarget:self action:@selector(saveBtnSenderDown) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark  保存按钮触发方法
- (void)saveBtnSenderDown
{
    if (_urlType == KNetworkUrlType) {//网络从已上传过来的需要判断参数是否发生变化，没有变化则不上传直接pop
        
        //都相同则假上传
        if ([_nameInput.textField.text isEqualToString:_nameStr]) {
            //未换名称
            isSameName = YES;
        }else{
        
            _nameStr = _nameInput.textField.text;
        }
        
        if ([_scriptionInput.textField.text isEqualToString:_remarks]) {
            //未更新描述
            isDescript = YES;
        }else if ([_scriptionInput.textField.text isEqualToString:@""]){
        
            _remarks = @"";
        }else{
        
            _remarks = _scriptionInput.textField.text;
        }
        
        if (!_isChangePosterImg) {
            //未更新过封面
            
            isCover = YES;
        }

        if (isEmptyStringOrNilOrNull(_classSelectID) && _locCategoryArr.count>0) {//!isEmptyStringOrNilOrNull(_classSelectID)
            //未改变分类
            
            isCategory = YES;
            
            if (categoryIsNull) {
                isCategory = NO;
            }
            
        }else if ([_classSelectID isEqualToString:[_locCategoryArr objectAtIndex:0]]){
            //取消了之前传过来的分类,后又再次选择了之前传递过来的分类
            
            isCategory = YES;
            
        }else{//选择了新的分类
        
            [_locCategoryArr removeAllObjects];
            
            [_locCategoryArr addObject:_classSelectID];
        }
        
    
        if (_locTagCopyArr.count == 0 && _tagSelectArr.count == 0){//再次编辑，传入数据没有标签，当前页面也没有选择新标签
            
            isTag = YES;//未改变
            _tagSelectStr = @"";
        }else{//进入时有标签
            
            if (_locTagCopyArr.count>0) {
                
                if (_locTagArr.count == _locTagCopyArr.count) {//判断原有标签是否被改变   1 数量相等时
                    
                    if (![self filterArr:_locTagArr andArr2:_locTagCopyArr]) {//没有不同项时
                     
                        isTag = YES;//未改变
                        
                        _tagSelectStr = [self getSelectMarksTitleWith:_locTagCopyArr];
                    }else{//有不同项时
                    
                        _tagSelectStr = [self getSelectMarksTitleWith:_locTagArr];
                    }
                }else{//数量不等时
                
                    if (_locTagArr.count>0) {
                        
                        _tagSelectStr = [self getSelectMarksTitleWith:_locTagArr];
                    }else{
                        
                        _tagSelectStr = @"";
                    }
                }
            }else{//原来没有标签
            
                if (_tagSelectArr.count>0) {
                    
                    _tagSelectStr = [self getSelectMarksTitleWith:_tagSelectArr];
                }else{
                
                    _tagSelectStr = @"";
                }
            }
        }
        
        
        if (isSameName == YES && isDescript == YES && isCover == YES && isCategory == YES && isTag == YES) {
            
            //返回之前停止清空播放器
            [self stopVideoPlayer];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            return;
        }else{//有改变，调用上传接口
            
            if (_nameInput.textField.text.length == 0 && categoryIsNull) {
                
                CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"必填项不能为空" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                
                [idAlert show];
                
                return;
            }
            if (_nameInput.textField.text.length == 0) {
                CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请输入视频名称" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                
                [idAlert show];
                return;
            }
            if (categoryIsNull) {
                CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请选择视频分类" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                
                [idAlert show];
                                
                return;
            }
            
            HTRequest *modifyRequest = [[HTRequest alloc] initWithDelegate:self];
            [modifyRequest UGCCommonUpdateProgramWithProgramId:_programIdStr andName:_nameStr andCover:_pictureFile andCategoryId:_classSelectID andTags:_tagSelectStr andRemarks:_remarks];
            
//            [self.navigationController popViewControllerAnimated:YES];
            
            [self showCustomeHUD];
        }
    }
    else{//本地相册进来的
        
        if (_nameInput.textField.text.length == 0 && _classSelectStr.length == 0) {
            
            CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"必填项不能为空" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
            
            [idAlert show];
            
            return;
        }
        if (_nameInput.textField.text.length == 0) {
            CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请输入视频名称" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
            
            [idAlert show];
            return;
        }
        if (_classSelectStr.length == 0) {
            CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请选择视频分类" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
            
            [idAlert show];
            return;
        }
        
        _nameStr = _nameInput.textField.text;
        _remarks = _scriptionInput.textField.text;
        
        _uploadDic = [[NSMutableDictionary alloc] init];
        
        [_uploadDic setObject:_videoPathStr.length?_videoPathStr:@"" forKey:@"fileUrl"];
        [_uploadDic setObject:_nameStr.length?_nameStr:@"" forKey:@"name"];
        [_uploadDic setObject:_classSelectStr.length?_classSelectStr:@"" forKey:@"category"];
        [_uploadDic setObject:_classSelectID.length?_classSelectID:@"" forKey:@"categoryId"];
        [_uploadDic setObject:_remarks.length?_remarks:@"" forKey:@"remarks"];
        
        
        if (_pictureFile) {
            [_uploadDic setObject:_pictureFile forKey:@"pictureFile"];
        }

        if (_tagSelectArr.count>0) {
            _tagSelectStr = [self getSelectMarksTitleWith:_tagSelectArr];
        }else{
        
            _tagSelectStr = @"";
        }
        
        [_uploadDic setObject:_tagSelectStr forKey:@"tags"];//拼接标签ID
        
        NSLog(@"选中的标签数组---%@",_tagSelectArr);
        
        NSLog(@"_tagSelectStr:%@",_tagSelectStr);
        
        NSLog(@"上传参数字典\n%@",_uploadDic);

        
        [[UGCUploadManager shareInstance] addTaskWithDateDic:_uploadDic];
        
        ZSToast(@"上传开始，您可以在“上传中”查看\n上传状态");
        
        //返回之前停止清空播放器
        [self stopVideoPlayer];
        
        //回到跟视图
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (BOOL)filterArr:(NSArray *)arr1 andArr2:(NSArray *)arr2 {
    NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",arr1];
    //得到两个数组中不同的数据
    NSArray * reslutFilteredArray = [arr2 filteredArrayUsingPredicate:filterPredicate];
    if (reslutFilteredArray.count > 0) {
        return YES;//有不同的
    }
    return NO;//没有不同的
}

- (void)stopVideoPlayer
{
    [_playerControlView controlViewClearPlayer];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NETCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}


- (void)returnBtnSenderDown
{
    [self stopVideoPlayer];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (UILabel *)setLabWithText:(NSString *)text andTextColor:(UIColor *)color andFont:(CGFloat)fontSize
{
    UILabel *lab = [[UILabel alloc] init];
    lab.text = text;
    lab.textColor = color;
    lab.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    
    return lab;
}

- (void)dealloc
{
    [_playerControlView controlViewClearPlayer];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NETCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PICKER_TAKE_DONE object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
