//
//  HTALAssetsViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AssetsLabraryHeader.h"
#import "HTALGroupViewController.h"
#import "HTALGroupModel.h"

@interface HTALAssetsViewController : HT_FatherViewController

@property (nonatomic, assign)ALGroupType groupType;//取哪个相册

@property (nonatomic, assign)ALVideoPhotoAssetType assetType;//照片还是视频

@property (nonatomic, strong)HTALGroupModel *assetGroup;

@property (nonatomic, assign)NSInteger maxCount;//最大数

@property (nonatomic, strong)NSArray *selectPickerAssets;//需要记录选中的值的数据

@property (nonatomic, assign)BOOL topShowPhotoPicker;//置顶展示图片

@property (nonatomic, copy) void(^selectedAssetsBlock)(NSMutableArray *selectedAssets);

@property (nonatomic, assign)ALVideoPhotoChoiceType choiceType;//单选or多选

- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType;

@end
