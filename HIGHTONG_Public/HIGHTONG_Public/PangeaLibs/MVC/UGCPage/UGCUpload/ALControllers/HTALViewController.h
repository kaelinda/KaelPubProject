//
//  HTALViewController.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HT_FatherViewController.h"
#import "AssetsLabraryHeader.h"


typedef void(^ALCallBackBlock)(id obj);

typedef NS_ENUM(NSUInteger, ALGroupType) {
    KAssetsLabraryDefaultGroup = 0,
    KAssetsLabraryCameraRoll,
    KAssetsLabrarySavePhotos,
    KAssetsLabraryPhotoStream,
    KAssetsLabraryVideo,
};

@interface HTALViewController : HT_FatherViewController

@property (nonatomic, assign)ALGroupType groupType;//是否需要push到内容控制器, 默认显示组

@property (nonatomic, assign)ALVideoPhotoAssetType assetType;//资源类型 视频还是照片

@property (nonatomic, copy)ALCallBackBlock callBackBlock;

@property (nonatomic, assign)ALVideoPhotoChoiceType choiceType;//单选还是多选

@property (nonatomic, assign)NSInteger maxCount;//每次选择图片的最小数，默认最大数是9

@property (nonatomic, strong)NSArray *selectPickers;//记录选中的值

@property (nonatomic, assign)BOOL topShowPhotoPicker;//置顶展示图片

//- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType andChoiceType:(ALVideoPhotoChoiceType)choiceType;

- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType;

@end
