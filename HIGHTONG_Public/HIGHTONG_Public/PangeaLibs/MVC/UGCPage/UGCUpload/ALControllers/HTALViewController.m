//
//  HTALViewController.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALViewController.h"
#import "HTALGroupViewController.h"

@interface HTALViewController ()

@property (nonatomic, assign)BOOL isOriginal;//是否发送原图 1原图  0压缩图

@end

@implementation HTALViewController

//- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType andChoiceType:(ALVideoPhotoChoiceType)choiceType
- (instancetype)initWithShowType:(ALVideoPhotoAssetType)assetType
{
    self = [super init];
    if (self) {
        self.assetType = assetType;
//        self.choiceType = choiceType;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    NSLog(@"图片选择器的VC进入viewWillAppear");
    
    HTALGroupViewController *groupVC = [[HTALGroupViewController alloc] initWithShowType:self.assetType];
    
    groupVC.choiceType = self.choiceType;

    groupVC.groupType = self.groupType;
    
    groupVC.selectAssets = [_selectPickers mutableCopy];
    
    groupVC.maxCount = _maxCount;
    
    groupVC.topShowPhotoPicter = _topShowPhotoPicker;
    
    [self.navigationController pushViewController:groupVC animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];

//    [self addNotification];
}

//- (void)addNotification
//{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectDone:) name:PICKER_TAKE_DONE object:nil];
//    });
//}

//****此处需要重新理解一下
//- (void)selectDone:(NSNotification *)noti
//{
//    NSArray *selectArray = noti.userInfo[@"selectAssets"];//*****
//    
//    NSLog(@"通知返回来的数组：\n%@",selectArray);
//    
//    self.isOriginal = [noti.userInfo[@"isOriginal"] boolValue];//****
//    
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//    
//        if (self.callBackBlock) {
//            self.callBackBlock(selectArray);
//        }
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//    });
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
