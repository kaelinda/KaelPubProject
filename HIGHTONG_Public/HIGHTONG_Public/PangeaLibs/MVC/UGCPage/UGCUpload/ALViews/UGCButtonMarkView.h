//
//  UGCButtonMarkView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UIButtonMaskViewChoiseType) {
    KBtnSingleChoiseType = 0,
    KBtnMutiplyChoiseType,
};

//typedef void (^UGCButtonMarkViewBlock)(NSInteger index, NSString *title);

typedef void (^UGCButtonMarkViewBlock)(NSInteger index, NSDictionary *dic);


@interface UGCButtonMarkView : UIView

@property (nonatomic, copy)UGCButtonMarkViewBlock btnBlock;

@property (nonatomic, assign)UIButtonMaskViewChoiseType choiseType;

@property (nonatomic, strong)NSArray *selectArr;//未点击选择前显示的之前选中的标签数组

- (void)setUpViewWithFrame:(CGRect)frame andDataArr:(NSArray *)array;

- (CGSize)setUpViewWithFrame:(CGRect)frame dataArr:(NSArray *)array;


@end
