//
//  HTALGroupTableViewCell.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTALGroupModel.h"

@interface HTALGroupTableViewCell : UITableViewCell

@property (nonatomic, strong)HTALGroupModel *groupModel;

@end
