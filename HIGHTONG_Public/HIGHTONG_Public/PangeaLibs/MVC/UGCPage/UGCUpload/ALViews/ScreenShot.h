//
//  ScreenShot.h
//  HealthCA
//
//  Created by Kael on 2016/10/4.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#ifndef ScreenShot_h
#define ScreenShot_h

#import "UIView+SaveViewToPhotos.h"//UIView 截屏类别
#import "UIScrollView+SaveContentViewToPhotos.h"//UIScrollView 超出屏幕部分截屏（ContentView）


#endif /* ScreenShot_h */
