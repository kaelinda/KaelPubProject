//
//  HTALAssetCollectionCell.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALAssetCollectionCell.h"

@implementation HTALAssetCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _photoView = [[HTALPhotoImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:_photoView];
    }
    
    return self;
}

- (void)setCellAsset:(HTALAssets *)cellAsset
{
    _cellAsset = cellAsset;
    _photoView.viewAsset = cellAsset;
}

@end
