//
//  HTALAssetCollectionCell.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/17.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTALPhotoImageView.h"
#import "HTALAssets.h"

@interface HTALAssetCollectionCell : UICollectionViewCell

@property (nonatomic, strong)HTALPhotoImageView *photoView;

@property (nonatomic, strong)HTALAssets *cellAsset;


@end
