//
//  UIView+SaveViewToImage.m
//  SaveViewToImage
//
//  Created by Kael on 2016/10/4.
//  Copyright © 2016年 创维海通. All rights reserved.
//
#import "UIView+SaveViewToPhotos.h"

#import <objc/runtime.h>

static char *const completedBlockKey  = "completedBlockKey";

@implementation UIView (SaveViewToPhotos)

- (void)saveViewImageToPhotosWithSize:(CGSize)size
                         andCompleted:(void (^)(BOOL success))completedBlock{
    //保存block
    objc_setAssociatedObject(self, completedBlockKey, completedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width, size.height),NO, 2.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultImage= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(resultImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
}

//回调方法
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error
                                              contextInfo: (void *) contextInfo
{
    void(^completedBlock)(BOOL success) = objc_getAssociatedObject(self,completedBlockKey );
    if(error != NULL){
        !completedBlock?:completedBlock(NO);
    }else{
        !completedBlock?:completedBlock(YES);
    }
}


-(UIImage *)getViewImageWithSize:(CGSize)size{
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width, size.height),NO, 2.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultImage= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}


@end
