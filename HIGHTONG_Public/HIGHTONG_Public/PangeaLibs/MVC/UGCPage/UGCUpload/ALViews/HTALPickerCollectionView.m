//
//  HTALPickerCollectionView.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/21.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALPickerCollectionView.h"
#import "HTALAssetCollectionCell.h"
#import "HTALPhotoImageView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AssetsLabraryHeader.h"

@interface HTALPickerCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, assign, getter=isFirstLoading)BOOL firstLoading;

@end

@implementation HTALPickerCollectionView

- (void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    
    //需要记录选中的值的数据
    if (self.isRecorderSelectPicker) {
        NSMutableArray *selectAssetArr = [NSMutableArray array];
        for (HTALAssets *selectAsset in self.selectAssetArr) {
            for (HTALAssets *dataAsset in self.dataArray) {
                
                if ([selectAsset isKindOfClass:[UIImage class]] || [dataAsset isKindOfClass:[UIImage class]]) {
                    
                    continue;
                }
                if ([selectAsset.asset.defaultRepresentation.url isEqual:dataAsset.asset.defaultRepresentation.url]) {
                    
                    [selectAssetArr addObject:dataAsset];
                    
                    break;
                }
            }
        }
        _selectAssetArr = selectAssetArr;
    }
    
    [self reloadData];
}

- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.dataSource = self;
        self.delegate = self;
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        
        _selectAssetArr = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const cellIden = @"cellIden";
    HTALAssetCollectionCell *cell = (HTALAssetCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIden forIndexPath:indexPath];
    HTALAssets *asset = self.dataArray[indexPath.row];
    if ([asset isKindOfClass:[HTALAssets class]]) {
        
        cell.cellAsset = asset;
    }
        
    if (_assetType == ALShowVideo) {//视频
        
        cell.photoView.timeLab.hidden = NO;
    }else{//照片
    
        cell.photoView.timeLab.hidden = YES;
    }
    
    cell.photoView.maskViewFlag = NO;
    
    for (NSInteger i = 0; i<self.selectAssetArr.count; i++) {
        if ([[self.selectAssetArr[i] assetURL] isEqual:asset.assetURL]) {
            cell.photoView.maskViewFlag = YES;
        }
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(85*kDeviceRate, 85*kDeviceRate);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
   
    return UIEdgeInsetsMake(5*kDeviceRate, 5*kDeviceRate, 5*kDeviceRate, 5*kDeviceRate);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!self.lastDataArr) {
        self.lastDataArr = [NSMutableArray array];
    }
    
    HTALAssetCollectionCell *cell = (HTALAssetCollectionCell *) [self cellForItemAtIndexPath:indexPath];
    
    HTALAssets *asset = self.dataArray[indexPath.row];
    
    // 如果没有就添加到数组里面，存在就移除
    if (cell.photoView.isMaskViewFlag) {
        
        [self.selectAssetArr removeObject:asset];
        [self.lastDataArr removeObject:asset];
    }else{
        
        if (_choiceType == ALMutipleChoice) {//多选
            // 判断图片数超过最大数
            NSUInteger maxCount = (self.maxCount < 0) ? KPhotoShowMaxCount :  self.maxCount;
            if (self.selectAssetArr.count >= maxCount) {
                NSString *format = [NSString stringWithFormat:@"最多只能选择%zd张图片",maxCount];
                if (maxCount == 0) {
                    format = [NSString stringWithFormat:@"您最多只能选择9张图片"];
                }
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提醒" message:format delegate:self cancelButtonTitle:nil otherButtonTitles:@"好的", nil];
                [alertView show];
                return;
            }
            
            [self.selectAssetArr addObject:asset];
            [self.lastDataArr addObject:asset];
        }else{//单选
            
            if (self.selectAssetArr.count >=1 || self.lastDataArr.count >= 1) {
             
                [self.selectAssetArr removeLastObject];
                [self.lastDataArr removeLastObject];
                
                [self.selectAssetArr addObject:asset];
                [self.lastDataArr addObject:asset];
                
                [self reloadData];
            }else{
            
                [self.selectAssetArr addObject:asset];
                [self.lastDataArr addObject:asset];
            }
        }
    }
    
    if (_collectViewDidSelectBlock) {
        
        NSLog(@"collectionviewBlock被点击了");
        
        if (cell.photoView.isMaskViewFlag) {
            //再次点击删除
            _collectViewDidSelectBlock(self, asset);
        }else{
            
            _collectViewDidSelectBlock(self, nil);
        }
    }
    
    cell.photoView.maskViewFlag = !cell.photoView.isMaskViewFlag;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.collectionViewShowOrderType == PhotoCollectionViewShowOrderTimeDesc) {//升序
        
        if (!self.firstLoading && self.contentSize.height > [[UIScreen mainScreen] bounds].size.height) {
            
            [self scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.dataArray.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
            
            self.contentOffset = CGPointMake(self.contentOffset.x, self.contentOffset.y + 100);
            self.firstLoading = YES;
        }
    }else if (self.collectionViewShowOrderType == PhotoCollectionViewShowOrderTimeAsc){//降序
        
        if (!self.firstLoading && self.contentSize.height > [[UIScreen mainScreen] bounds].size.height) {
            
            [self scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            
            self.contentOffset = CGPointMake(self.contentOffset.x, -self.contentOffset.y);
            self.firstLoading = YES;
        }
    }
}


@end
