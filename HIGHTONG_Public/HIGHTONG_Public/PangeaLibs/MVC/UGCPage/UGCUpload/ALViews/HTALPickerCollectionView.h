//
//  HTALPickerCollectionView.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/21.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTALAssets.h"
#import "AssetsLabraryHeader.h"

typedef NS_ENUM(NSUInteger, PhotoCollectionViewShowOrderType) {
    PhotoCollectionViewShowOrderTimeDesc = 0,//升序
    PhotoCollectionViewShowOrderTimeAsc,//降序
};

@class HTALPickerCollectionView;

typedef void(^HTALPickerCollectionViewDidSelectBlock)(HTALPickerCollectionView *collectionView, HTALAssets *selectAsset);

@interface HTALPickerCollectionView : UICollectionView

@property (nonatomic, copy)HTALPickerCollectionViewDidSelectBlock collectViewDidSelectBlock;

// scrollView滚动的升序降序
@property (nonatomic, assign)PhotoCollectionViewShowOrderType collectionViewShowOrderType;

// 保存所有的数据
@property (nonatomic, strong)NSArray *dataArray;

// 保存选中的图片
@property (nonatomic, strong)NSMutableArray *selectAssetArr;

// 最后保存的一次图片
@property (nonatomic, strong)NSMutableArray *lastDataArr;

// 限制最大数
@property (nonatomic, assign)NSInteger maxCount;

// 置顶展示图片
@property (nonatomic, assign)BOOL topShowPhotoPicker;

// 记录选中的值
@property (nonatomic, assign)BOOL isRecorderSelectPicker;

//单选还是多选
@property (nonatomic, assign)ALVideoPhotoChoiceType choiceType;

@property (nonatomic, assign)ALVideoPhotoAssetType assetType;


@end
