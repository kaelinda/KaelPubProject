//
//  HTALPhotoImageView.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/21.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALPhotoImageView.h"


@implementation HTALPhotoImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //下面两句为适配iOS9及以后
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
        
        
        _tickImageView = [[UIImageView alloc] init];
        _tickImageView.frame = CGRectMake(self.bounds.size.width - 31*kDeviceRate, 1*kDeviceRate, 30*kDeviceRate, 30*kDeviceRate);
        _tickImageView.image = [UIImage imageNamed:@"sl_user_common_del_checkbox_normal"];//btn_user_radio_normal_2
        [self addSubview:_tickImageView];
        
        
        _timeLab = [[UILabel alloc] init];
        _timeLab.frame = CGRectMake(0, self.bounds.size.height-15*kDeviceRate, self.bounds.size.width, 15*kDeviceRate);
        _timeLab.textAlignment = NSTextAlignmentRight;
        _timeLab.font = [UIFont systemFontOfSize:10];
        [self addSubview:_timeLab];
        _timeLab.text = @"hshshshs";
        _timeLab.textColor = [UIColor whiteColor];
    }
    
    return self;
}

- (void)setMaskViewFlag:(BOOL)maskViewFlag
{
    _maskViewFlag = maskViewFlag;
    
    if (!maskViewFlag){
        
        [self.tickImageView setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];//btn_user_radio_normal_2
    }else{
        [self.tickImageView setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];//btn_user_radio_pressed_2
    }
    
    self.animationRightTick = maskViewFlag;
}

- (void)setAnimationRightTick:(BOOL)animationRightTick
{
    _animationRightTick = animationRightTick;
}

- (void)setViewAsset:(HTALAssets *)viewAsset
{
    _viewAsset = viewAsset;
    
    self.image = _viewAsset.thumbnailImg;
    
    NSLog(@"时长时长时长----\n%@",_viewAsset.videoDuration);
    
    self.timeLab.text = _viewAsset.videoDuration;
}


@end
