//
//  HTALPhotoImageView.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/21.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTALAssets.h"

@interface HTALPhotoImageView : UIImageView

@property (nonatomic, assign, getter=isMaskViewFlag)BOOL maskViewFlag;

@property (nonatomic, assign)BOOL animationRightTick;

@property (nonatomic, strong)HTALAssets *viewAsset;

@property (nonatomic, strong)UIImageView *tickImageView;

@property (nonatomic, strong)UILabel *timeLab;


@end
