//
//  UIView+SaveViewToImage.h
//  SaveViewToPhotos
//
//  Created by Kael on 2016/10/4.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SaveViewToPhotos)

    /**
     * @pragam size:尺寸
     * @pragma completedBlock:图片存储结果的回调
     */
- (void)saveViewImageToPhotosWithSize:(CGSize)size
                         andCompleted:(void (^)(BOOL success))completedBlock;

- (UIImage *)getViewImageWithSize:(CGSize)size;


@end
