//
//  UGCButtonMarkView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCButtonMarkView.h"

@interface UGCButtonMarkView ()

@property (nonatomic, strong)UIButton *currentBtn;

@property (nonatomic, strong)NSArray *dataArr;

@end

@implementation UGCButtonMarkView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xffffff);
    }
    return self;
}

- (CGSize)setUpViewWithFrame:(CGRect)frame dataArr:(NSArray *)array
{
    CGSize size;
    
    _dataArr = [NSArray arrayWithArray:array];
    
    for (int i = 0; i < array.count; i++){
        
        NSString *name = [[array objectAtIndex:i] objectForKey:@"name"];
        
        if (name.length>10) {
            name = [name substringToIndex:10];
            name = [NSString stringWithFormat:@"%@...",name];
        }
        
//        NSString *name = [array objectAtIndex:i];
        
        static UIButton *recordBtn =nil;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGRect rect = [name boundingRectWithSize:CGSizeMake(self.frame.size.width - 20, 30) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:btn.titleLabel.font} context:nil];
        
        CGFloat BtnW = rect.size.width + 20;
        CGFloat BtnH = rect.size.height + 10;
        
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = BtnH/2;
        if (i == 0){//第一个
            
            //第一个按钮的Y值距上一个控件多少
            btn.frame =CGRectMake(13, 40, BtnW, BtnH);
            
        }else{
            
            CGFloat yuWidth = self.frame.size.width - 20 -recordBtn.frame.origin.x -recordBtn.frame.size.width;
            
            if (yuWidth >= rect.size.width+12) {//中间部分按钮，最后一个的时候加间隔距离，否则紧贴右边距
                
                btn.frame =CGRectMake(recordBtn.frame.origin.x + recordBtn.frame.size.width + 10, recordBtn.frame.origin.y, BtnW, BtnH);
                
            }else{//单独余一个的时候
                
                btn.frame =CGRectMake(13, recordBtn.frame.origin.y+recordBtn.frame.size.height+10, BtnW, BtnH);
            }
        }
        
        
        
        btn.backgroundColor = [UIColor whiteColor];
        btn.layer.borderWidth = 1.0;
        btn.layer.borderColor = [UIColorFromRGB(0xcccccc) CGColor];
        [btn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn setTitle:name forState:UIControlStateNormal];
        
        
        if (_selectArr.count>0) {
            for (int j = 0; j<_selectArr.count; j++) {
                
                if ([[_selectArr objectAtIndex:j] isEqualToString:[[array objectAtIndex:i] objectForKey:@"id"]]) {
                    
                    btn.layer.borderColor = [UIColorFromRGB(0x3ab7f5) CGColor];
                    [btn setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
                 
                    btn.selected = YES;
                }
            }
        }
        
        if (_choiseType == KBtnSingleChoiseType && _selectArr.count==1) {//已上传进入信息编辑的单选
            
            if ([[_selectArr firstObject] isEqualToString:[[array objectAtIndex:i] objectForKey:@"id"]]) {
                
                _currentBtn = btn;
            }
        }
        
        
        [self addSubview:btn];
        
        recordBtn = btn;
        
        btn.tag = 100 + i;
        btn.adjustsImageWhenHighlighted = NO;//去掉点击的高亮
        [btn addTarget:self action:@selector(BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        self.frame = CGRectMake(0, frame.origin.y, [UIScreen mainScreen].bounds.size.width,CGRectGetMaxY(btn.frame)+15);
    }
    
    size = self.frame.size;
    
    return size;
}

- (void)BtnClick:(UIButton *)button
{
    __weak typeof(self) weakSelf = self;
    
    if (_choiseType == KBtnSingleChoiseType) {//单选
        
        if (button != _currentBtn) {
            
            if (_currentBtn != nil) {//点击过button按钮
                
                UIButton *btn = (UIButton *)[self viewWithTag:self.currentBtn.tag];
                
                btn.layer.borderColor = [UIColorFromRGB(0xcccccc) CGColor];
                
                [btn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                
                self.currentBtn.selected = NO;
                
                self.currentBtn = button;
                
                button.layer.borderColor = [UIColorFromRGB(0x3ab7f5) CGColor];
                
                [button setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
                
                self.currentBtn.selected = YES;
                
            }else{//第一次点击按钮
                
                self.currentBtn.selected = NO;
                
                self.currentBtn = button;
                
                button.layer.borderColor = [UIColorFromRGB(0x3ab7f5) CGColor];
                
                [button setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
                
                self.currentBtn.selected = YES;
            }
            
            if (weakSelf.btnBlock) {
                
                weakSelf.btnBlock(button.tag, [_dataArr objectAtIndex:button.tag-100]);
            }
            
        }else{//点击当前选中的按钮的时候（取消之前选择）
            
            self.currentBtn.selected = NO;
            
            self.currentBtn = nil;//一定要置空currentBtn，否则在取消之后再次点击没有UI效果
            
            button.layer.borderColor = [UIColorFromRGB(0xcccccc) CGColor];
            
            [button setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
            
            if (weakSelf.btnBlock) {
                                
                weakSelf.btnBlock(button.tag, [NSDictionary dictionary]);//[_dataArr objectAtIndex:button.tag-100]
            }
        }

    }
    else{//多选-----------else if (_choiseType == KBtnMutiplyChoiseType)
        
//        button.selected = !button.selected;
        
        if (button.selected == YES) {//取消选中
            
            button.layer.borderColor = [UIColorFromRGB(0xcccccc) CGColor];
            
            [button setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
            
            button.selected = NO;
            
        }else{//选中
        
            button.layer.borderColor = [UIColorFromRGB(0x3ab7f5) CGColor];
            
            [button setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            button.selected = YES;
            
        }
        
        if (weakSelf.btnBlock) {
            
            weakSelf.btnBlock(button.tag, [_dataArr objectAtIndex:button.tag-100]);
        }
    }
}


- (void)setUpViewWithFrame:(CGRect)frame andDataArr:(NSArray *)array
{
    for (int i = 0; i < array.count; i ++){
        
        NSString *name = array[i];
        static UIButton *recordBtn =nil;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGRect rect = [name boundingRectWithSize:CGSizeMake(self.frame.size.width -20, 30) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:btn.titleLabel.font} context:nil];
        
        CGFloat BtnW = rect.size.width + 20;
        CGFloat BtnH = rect.size.height + 10;
        
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = BtnH/2;
        if (i == 0){//第一个
            
            btn.frame =CGRectMake(13, 15, BtnW, BtnH);
            
            //            NSLog(@"\n11Y  %f,\nX  %f,\nHEIGHT  %f,\nWIDTH  %f", btn.frame.origin.y,btn.frame.origin.x,btn.frame.size.height,btn.frame.size.width);
        }else{
            
            CGFloat yuWidth = self.frame.size.width - 20 -recordBtn.frame.origin.x -recordBtn.frame.size.width;
            
            if (yuWidth >= rect.size.width+12) {//中间部分按钮，最后一个的时候加间隔距离
                
                btn.frame =CGRectMake(recordBtn.frame.origin.x + recordBtn.frame.size.width + 10, recordBtn.frame.origin.y, BtnW, BtnH);
                
                //                NSLog(@"\n22Y  %f,\nX  %f,\nHEIGHT  %f,\nWIDTH  %f", btn.frame.origin.y,btn.frame.origin.x,btn.frame.size.height,btn.frame.size.width);
                
            }else{//单独余一个的时候
                
                btn.frame =CGRectMake(13, recordBtn.frame.origin.y+recordBtn.frame.size.height+10, BtnW, BtnH);
                
                //                NSLog(@"\n33Y  %f,\nX  %f,\nHEIGHT  %f,\nWIDTH  %f", btn.frame.origin.y,btn.frame.origin.x,btn.frame.size.height,btn.frame.size.width);
            }
        }
        
        btn.backgroundColor = [UIColor whiteColor];
        
        btn.layer.borderWidth = 1.0;
        btn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [btn setTitle:name forState:UIControlStateNormal];
        [self addSubview:btn];
        
        recordBtn = btn;
        
        btn.tag = 100 + i;
        
        btn.adjustsImageWhenHighlighted = NO;//去掉点击的高亮
        
        [btn addTarget:self action:@selector(BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        //        self.frame = CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width,CGRectGetMaxY(btn.frame)+15);
        self.frame = CGRectMake(0, frame.origin.y, [UIScreen mainScreen].bounds.size.width,CGRectGetMaxY(btn.frame)+15);
        
    }
}


@end
