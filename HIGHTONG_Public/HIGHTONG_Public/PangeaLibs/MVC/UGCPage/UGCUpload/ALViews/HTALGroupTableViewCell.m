//
//  HTALGroupTableViewCell.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/16.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "HTALGroupTableViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Masonry.h"
#import "AssetsLabraryHeader.h"

@interface HTALGroupTableViewCell ()

@property (nonatomic, strong)UIImageView *posterImg;
@property (nonatomic, strong)UILabel *nameLab;
@property (nonatomic, strong)UILabel *countLab;

@end

@implementation HTALGroupTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        __weak typeof(self) weakSelf = self;
                
        _posterImg = [[UIImageView alloc] init];
        [self.contentView addSubview:_posterImg];
        _posterImg.contentMode = UIViewContentModeScaleAspectFit;
        [_posterImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.contentView.mas_left).offset(10*kDeviceRate);
            make.centerY.mas_equalTo(weakSelf.contentView.mas_centerY);
//            make.top.mas_equalTo(weakSelf.contentView.mas_top);
            make.height.mas_equalTo(62*kDeviceRate);//weakSelf.contentView.mas_height
            make.width.mas_equalTo(65*kDeviceRate);
        }];
//        _posterImg.layer.cornerRadius = 0.3;
//        _posterImg.clipsToBounds = YES;
        
        _nameLab = [[UILabel alloc] init];
        [self.contentView addSubview:_nameLab];
        _nameLab.textAlignment = NSTextAlignmentLeft;
        _nameLab.textColor = UIColorFromRGB(0x000000);
        _nameLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.posterImg.mas_right).offset(5*kDeviceRate);
            make.top.mas_equalTo(weakSelf.posterImg.mas_top).offset(10*kDeviceRate);
            make.width.mas_equalTo(240*kDeviceRate);
        }];
        
        _countLab = [[UILabel alloc] init];
        [self.contentView addSubview:_countLab];
        _countLab.textAlignment = NSTextAlignmentLeft;
        [_countLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.nameLab.mas_left);
            make.bottom.mas_equalTo(weakSelf.posterImg.mas_bottom).offset(-10*kDeviceRate);
            make.width.mas_equalTo(weakSelf.nameLab.mas_width);
        }];
        
        UIView *line = [[UIView alloc] init];
        [self.contentView addSubview:line];
        line.backgroundColor = [UIColor lightGrayColor];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(1*kDeviceRate);
            make.width.mas_equalTo(weakSelf.contentView.mas_width);
            make.centerX.mas_equalTo(weakSelf.contentView.mas_centerX);
            make.bottom.mas_equalTo(weakSelf.contentView.mas_bottom);
        }];
        
        UIImageView *arrowImg = [[UIImageView alloc] init];
        [self.contentView addSubview:arrowImg];
        arrowImg.image = GetImage(@"ic_user_arrow");
        [arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(13*kDeviceRate);
            make.centerY.mas_equalTo(weakSelf.contentView.mas_centerY);
            make.right.mas_equalTo(weakSelf.contentView.mas_right).offset(-13*kDeviceRate);
        }];
    }
    return self;
}

- (void)setGroupModel:(HTALGroupModel *)groupModel
{
    _groupModel = groupModel;
    
    self.posterImg.image = groupModel.thumbImage;
    self.nameLab.text = groupModel.groupName;
    self.countLab.text = [NSString stringWithFormat:@"%ld",(long)groupModel.assetsCount];
}

@end
