//
//  UGCAuthItemInputView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UGCAuthItemInputViewClickBtnBlock)();

@interface UGCAuthItemInputView : UIView

@property (nonatomic, strong)UILabel *titleLab;

@property (nonatomic, strong)UITextField *textField;

//@property (nonatomic, strong)UILabel *placeLab;

@property (nonatomic, strong)UIImageView *arrowImg;

@property (nonatomic, strong)UIImageView *lineImg;

@property (nonatomic, strong)UIButton *clickBtn;//点击

@property (nonatomic, copy)UGCAuthItemInputViewClickBtnBlock btnBlock;


- (id)initInputViewWithTitleName:(NSString *)titleName andPlaceholdStr:(NSString *)placeholdStr andIsHiddenArrow:(BOOL)isHidden andTitleLabWidth:(CGFloat)titleWidth andTitleLeftSepWidth:(CGFloat)leftSep;

- (id)initWithFrame:(CGRect)frame withTitleName:(NSString *)titleName andPlaceholdStr:(NSString *)placeholdStr andIsHiddenArrow:(BOOL)isHidden;

@end
