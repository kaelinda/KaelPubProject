//
//  JobClassTableViewCell.h
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/27.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobClassTableViewCell : UITableViewCell

@property (nonatomic, strong)UILabel *jobClassLab;

@end
