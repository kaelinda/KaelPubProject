//
//  JobClassTableViewCell.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/27.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "JobClassTableViewCell.h"
#import "Masonry.h"

@implementation JobClassTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        __weak typeof(self) weakSelf = self;

        _jobClassLab = [[UILabel alloc] init];
        [self.contentView addSubview:_jobClassLab];
        [_jobClassLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.contentView.mas_left).offset(18*kDeviceRate);
            make.top.mas_equalTo(weakSelf.contentView.mas_top);
            make.right.mas_equalTo(weakSelf.contentView.mas_right);
            make.bottom.mas_equalTo(weakSelf.contentView.mas_bottom).offset(-1*kDeviceRate);
        }];
        _jobClassLab.textAlignment = NSTextAlignmentLeft;
        _jobClassLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
        _jobClassLab.textColor = UIColorFromRGB(0x333333);
        _jobClassLab.userInteractionEnabled = YES;
        
        UIImageView *lineImg = [[UIImageView alloc] init];
        [self.contentView addSubview:lineImg];
        lineImg.backgroundColor = UIColorFromRGB(0xe8e8e8);
        [lineImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.bottom.mas_equalTo(weakSelf.contentView);
            make.top.mas_equalTo(weakSelf.jobClassLab.mas_bottom);
        }];
    }
    
    return self;
}

@end
