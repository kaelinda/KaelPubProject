//
//  CustomBorderView.m
//  PhotoAlbumDemo
//
//  Created by  吴伟 on 2017/3/27.
//  Copyright © 2017年  吴伟. All rights reserved.
//

#import "CustomBorderView.h"
#import "Masonry.h"

@implementation CustomBorderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);

        _topBorder = [self setBorderImg];
        [_topBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.centerX.mas_equalTo(self);
            make.height.mas_equalTo(0.5*kDeviceRate);
        }];
        
        _underBorder = [self setBorderImg];
        [_underBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(_topBorder);
            make.bottom.mas_equalTo(self.mas_bottom);
            make.left.mas_equalTo(self.mas_left);
        }];
    }
    
    return self;
}

- (UIImageView *)setBorderImg
{
    UIImageView *img = [[UIImageView alloc] init];
    [self addSubview:img];
    img.backgroundColor = UIColorFromRGB(0xe8e8e8);
    
    return img;
}

@end
