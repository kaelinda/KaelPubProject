//
//  UGCAuthItemInputView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCAuthItemInputView.h"
#import "Masonry.h"

@interface UGCAuthItemInputView ()<UITextFieldDelegate>

@end

@implementation UGCAuthItemInputView


- (id)initWithFrame:(CGRect)frame withTitleName:(NSString *)titleName andPlaceholdStr:(NSString *)placeholdStr andIsHiddenArrow:(BOOL)isHidden
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);
        
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(13*kDeviceRate, 0, 40*kDeviceRate, self.frame.size.height-1*kDeviceRate)];
        _titleLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
        _titleLab.textColor = UIColorFromRGB(0x333333);
        _titleLab.text = titleName;
        [self addSubview:_titleLab];
       
        if (isHidden) {
            
            NSMutableAttributedString * attributedStr = [[NSMutableAttributedString alloc]initWithString:placeholdStr];
            NSDictionary *attributeDic = [NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x999999),NSForegroundColorAttributeName,[UIFont systemFontOfSize:14*kDeviceRate],NSFontAttributeName, nil];
            [attributedStr addAttributes:attributeDic range:NSMakeRange(0, attributedStr.length)];
            
            
            _textField = [[UITextField alloc] initWithFrame:CGRectMake(53*kDeviceRate, 0, self.frame.size.width-53*kDeviceRate, self.frame.size.height - 1*kDeviceRate)];//self.frame.size.width-53*kDeviceRate-12*kDeviceRate
            _textField.returnKeyType = UIReturnKeyDone;
            _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            _textField.textAlignment = NSTextAlignmentLeft;
            _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            _textField.borderStyle = UITextBorderStyleNone;
            _textField.font = [UIFont systemFontOfSize:14*kDeviceRate];
            _textField.textColor = UIColorFromRGB(0x333333);
            [self addSubview:_textField];
            
            _textField.centerY = _titleLab.centerY;
            
            _textField.attributedPlaceholder = attributedStr;
            
        }else{
            
            _clickBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [self addSubview:_clickBtn];
            _clickBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            [_clickBtn setTitle:placeholdStr forState:UIControlStateNormal];
            [_clickBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            _clickBtn.frame = CGRectMake(53*kDeviceRate, 0, self.frame.size.width-53*kDeviceRate-25*kDeviceRate, self.frame.size.height - 1*kDeviceRate);
            
            _clickBtn.adjustsImageWhenHighlighted = NO;
            
            _clickBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
            _clickBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [_clickBtn addTarget:self action:@selector(clickBtnSenderDown) forControlEvents:UIControlEventTouchUpInside];
            
            
            _arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(kDeviceWidth-20*kDeviceRate, (self.frame.size.height-14)/2*kDeviceRate, 13*kDeviceRate, 13*kDeviceRate)];//正常应该-25，但现在的图自带右边距，所以-20
            [self addSubview:_arrowImg];

            _arrowImg.image = [UIImage imageNamed:@"ic_user_arrow"];
        }
        
        
        _lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1*kDeviceRate, kDeviceWidth, 1*kDeviceRate)];
        [self addSubview:_lineImg];
        [_lineImg setBackgroundColor:HT_COLOR_SPLITLINE];

    }
    
    return self;
}


- (id)initInputViewWithTitleName:(NSString *)titleName andPlaceholdStr:(NSString *)placeholdStr andIsHiddenArrow:(BOOL)isHidden andTitleLabWidth:(CGFloat)titleWidth andTitleLeftSepWidth:(CGFloat)leftSep
{
    self = [super init];
    if (self) {
        
        __weak typeof(self) weakSelf = self;
        
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
        _titleLab.textColor = UIColorFromRGB(0x333333);
        _titleLab.text = titleName;
        [self addSubview:_titleLab];
        
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(weakSelf.mas_top);
            make.left.mas_equalTo(weakSelf.mas_left).offset(leftSep);
            make.width.mas_equalTo(titleWidth);
            make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-1*kDeviceRate);
        }];
        
        
        if (isHidden) {
            
            NSMutableAttributedString * attributedStr = [[NSMutableAttributedString alloc]initWithString:placeholdStr];
            NSDictionary *attributeDic = [NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x999999),NSForegroundColorAttributeName,[UIFont systemFontOfSize:14*kDeviceRate],NSFontAttributeName, nil];
            [attributedStr addAttributes:attributeDic range:NSMakeRange(0, attributedStr.length)];
            
            _textField = [[UITextField alloc] init];
            _textField.delegate = self;
            _textField.returnKeyType = UIReturnKeyDone;
            _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            _textField.textAlignment = NSTextAlignmentLeft;
            _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            _textField.borderStyle = UITextBorderStyleNone;
            _textField.font = [UIFont systemFontOfSize:14*kDeviceRate];
            _textField.textColor = UIColorFromRGB(0x333333);
            [self addSubview:_textField];
            [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(weakSelf.titleLab.mas_right);
                make.right.mas_equalTo(weakSelf.mas_right).offset(-5*kDeviceRate);
                make.top.mas_equalTo(weakSelf.mas_top);
                make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-1*kDeviceRate);
            }];
            
            _textField.attributedPlaceholder = attributedStr;

//            [_textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
//            
//            _placeLab = [[UILabel alloc] init];
//            [_textField addSubview: _placeLab];
//            [_placeLab mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//            }];
//            _placeLab.text = placeholdStr;
//            _placeLab.font = [UIFont systemFontOfSize:14*kDeviceRate];
//            _placeLab.textColor = UIColorFromRGB(0x999999);

        }else{
        
            _clickBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [self addSubview:_clickBtn];
            _clickBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            [_clickBtn setTitle:placeholdStr forState:UIControlStateNormal];
            [_clickBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            
            [_clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(weakSelf.titleLab.mas_right);
                make.right.mas_equalTo(weakSelf.mas_right).offset(-25*kDeviceRate);
                make.top.mas_equalTo(weakSelf.mas_top);
                make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-1*kDeviceRate);
            }];
            
            _clickBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
            _clickBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [_clickBtn addTarget:self action:@selector(clickBtnSenderDown) forControlEvents:UIControlEventTouchUpInside];
            
            
            _arrowImg = [[UIImageView alloc] init];
            [self addSubview:_arrowImg];
            _arrowImg.image = [UIImage imageNamed:@"ic_user_arrow"];
            [_arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(13*kDeviceRate);
                make.left.mas_equalTo(weakSelf.clickBtn.mas_right);
                make.centerY.mas_equalTo(weakSelf.clickBtn.mas_centerY);
            }];
        }
        
        
        _lineImg = [[UIImageView alloc] init];
        [self addSubview:_lineImg];
        [_lineImg setBackgroundColor:HT_COLOR_SPLITLINE];
        [_lineImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.mas_left).offset(15*kDeviceRate);
            make.bottom.mas_equalTo(weakSelf.mas_bottom);
            make.top.mas_equalTo(isHidden?weakSelf.textField.mas_bottom:weakSelf.clickBtn.mas_bottom);
            make.right.mas_equalTo(weakSelf.mas_right);
        }];        
    }
    
    return self;
}


//- (void)inputBlockValueChange:(UITextField *)textField
//{
//    if (textField.text.length>0) {
//        
//        self.placeLab.hidden = YES;
//        
//    }else{
//        self.placeLab.hidden = NO;
//    }
//}

#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_textField resignFirstResponder];
    
    return YES;
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    _textField.enabled = YES;
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    _textField.enabled = YES;
//}

- (void)clickBtnSenderDown
{
    NSLog(@"我我我哈哈哈撒傻哈哈");
    if (_btnBlock) {
        _btnBlock();
        
        NSLog(@"你你你哈哈哈撒傻哈哈");
    }
}

@end
