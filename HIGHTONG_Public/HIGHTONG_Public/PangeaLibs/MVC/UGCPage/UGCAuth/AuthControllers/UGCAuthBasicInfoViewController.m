//
//  UGCAuthBasicInfoViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCAuthBasicInfoViewController.h"
#import "UGCAuthItemInputView.h"
#import "UGCAuthCredUploadViewController.h"
#import "CustomBorderView.h"
#import "ChooseHospitalViewController.h"
#import "UGCJobClassViewController.h"
#import "MyGesture.h"
#import "CustomAlertView.h"
#import "CHECKFORMAT.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

@interface UGCAuthBasicInfoViewController ()<CustomAlertViewDelegate>
{
    NSString *_mobileStr;
}
@property (nonatomic, strong)UIButton *returnBtn;//返回按钮
@property (nonatomic, strong)UIView *backView;//VC大背景view

@property (nonatomic, strong)UIScrollView *scrollView;

@property (nonatomic, strong)CustomBorderView *headerView;//上部手机号提示认证view
@property (nonatomic, strong)CustomBorderView *bodyView;
@property (nonatomic, strong)CustomBorderView *footerView;
@property (nonatomic, strong)CustomBorderView *nextStepBack;
@property (nonatomic, strong)UGCAuthItemInputView *nameTF;
@property (nonatomic, strong)UGCAuthItemInputView *idTF;
@property (nonatomic, strong)UGCAuthItemInputView *phoneNumTF;
@property (nonatomic, strong)UGCAuthItemInputView *hospitalTF;
@property (nonatomic, strong)UGCAuthItemInputView *officeClassTF;
@property (nonatomic, strong)UGCAuthItemInputView *jobTitleTF;

@property (nonatomic, strong)UIButton *nextStepBtn;

@property (nonatomic, strong)NSMutableDictionary *paraDic;//参数字典

@end

@implementation UGCAuthBasicInfoViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _mobileStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    
    _paraDic = [[NSMutableDictionary alloc] init];
    
    [self setUpViewNaviBar];
    
    [self setUpMainUI];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];//获得键盘的rect
    
    CGFloat tempHeight = keyboardFrame.size.height;
    
    _scrollView.scrollEnabled = YES;
    
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, KDeviceHeight-tempHeight-64, 0);
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _scrollView.contentOffset = CGPointMake(0, 0);

        _scrollView.scrollEnabled = NO;

    });
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(returnBtnSenderDown)];
    [self setNaviBarLeftBtn:_returnBtn];
    
    [self setNaviBarTitle:@"医生认证"];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
}

- (void)returnBtnSenderDown
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpMainUI
{
    __weak typeof(self) weakSelf = self;
    
    UIView *tempView = [[UIView alloc] init];
    [self.view addSubview:tempView];
    
    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.view.mas_top).offset(64);
        make.width.centerX.mas_equalTo(weakSelf.view);
        make.bottom.mas_equalTo(weakSelf.view.mas_bottom);
    }];
    _scrollView.showsVerticalScrollIndicator = NO;
    
    _backView = [[UIView alloc] init];
    [_scrollView addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        make.width.mas_equalTo(weakSelf.scrollView.mas_width);
    }];
    
    
    _headerView = [[CustomBorderView alloc] init];
    [_backView addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(weakSelf.view.mas_width);
        make.centerX.mas_equalTo(weakSelf.view.mas_centerX);
        make.top.mas_equalTo(weakSelf.backView.mas_top).offset(9*kDeviceRate);
        make.height.mas_equalTo(90*kDeviceRate);
    }];
    
    {
        UIImageView *logoImg = [[UIImageView alloc] init];
        [_headerView addSubview:logoImg];
//        logoImg.backgroundColor = [UIColor redColor];
        logoImg.image = [UIImage imageNamed:@"doctor_infomation_hook"];
        
        [logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(50*kDeviceRate);
            make.height.mas_equalTo(65*kDeviceRate);
            make.centerY.mas_equalTo(weakSelf.headerView.mas_centerY);
            make.left.mas_equalTo(weakSelf.headerView.mas_left).offset(25*kDeviceRate);
        }];
        
        UILabel *mobileLab = [[UILabel alloc] init];
        UILabel *tipLab = [[UILabel alloc] init];
        [_headerView addSubview:mobileLab];
        [_headerView addSubview:tipLab];
        
        mobileLab.text = [NSString stringWithFormat:@"手机用户%@",_mobileStr];
        tipLab.text = @"您好，请您完善医生认证需要的信息";
        
        mobileLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
        tipLab.font = [UIFont systemFontOfSize:14*kDeviceRate];
        
        mobileLab.textAlignment = NSTextAlignmentLeft;
        tipLab.textAlignment = NSTextAlignmentLeft;
        
        mobileLab.textColor = UIColorFromRGB(0x333333);
        tipLab.textColor = UIColorFromRGB(0x333333);
        
        [mobileLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(logoImg.mas_right).offset(23*kDeviceRate);
            make.top.mas_equalTo(weakSelf.headerView.mas_top).offset(24*kDeviceRate);
            make.right.mas_equalTo(weakSelf.headerView.mas_right);
        }];
        
        [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.width.mas_equalTo(mobileLab);
            make.top.mas_equalTo(mobileLab.mas_bottom).offset(10*kDeviceRate);
        }];
        
    }
    
    //姓名、身份证、手机号
    _bodyView = [[CustomBorderView alloc] init];
    [_backView addSubview:_bodyView];
    [_bodyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(weakSelf.backView);
        make.top.mas_equalTo(weakSelf.headerView.mas_bottom).offset(15*kDeviceRate);
        make.height.mas_equalTo(134*kDeviceRate);//132
    }];
    
    {
        _nameTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"姓名" andPlaceholdStr:@"请输入您的姓名" andIsHiddenArrow:YES andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _idTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"身份证" andPlaceholdStr:@"请输入您的身份证号" andIsHiddenArrow:YES andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _phoneNumTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"手机号" andPlaceholdStr:_mobileStr andIsHiddenArrow:YES andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _phoneNumTF.lineImg.hidden = YES;
        
        [_bodyView addSubview:_nameTF];
        [_bodyView addSubview:_idTF];
        [_bodyView addSubview:_phoneNumTF];
        
        [_nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.mas_equalTo(weakSelf.bodyView);
            make.height.mas_equalTo(44*kDeviceRate);
            make.top.mas_equalTo(weakSelf.bodyView.mas_top).offset(1*kDeviceRate);
        }];
        
        [_idTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(weakSelf.nameTF);
            make.top.mas_equalTo(weakSelf.nameTF.mas_bottom);
            make.left.mas_equalTo(weakSelf.nameTF.mas_left);
        }];
        
        [_phoneNumTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(weakSelf.nameTF);
            make.top.mas_equalTo(weakSelf.idTF.mas_bottom);
            make.left.mas_equalTo(weakSelf.idTF.mas_left);
        }];
    }
    
    //医院、科室、职称
    _footerView = [[CustomBorderView alloc] init];
    [_backView addSubview:_footerView];
    [_footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.bodyView);
        make.centerX.mas_equalTo(weakSelf.backView.mas_centerX);
        make.top.mas_equalTo(weakSelf.bodyView.mas_bottom).offset(9*kDeviceRate);
    }];
    
    {
        _hospitalTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"医院" andPlaceholdStr:@"请选择您所在的医院" andIsHiddenArrow:NO andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _officeClassTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"科室" andPlaceholdStr:@"请输入您所在的科室" andIsHiddenArrow:YES andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _jobTitleTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"职称" andPlaceholdStr:@"请选择您的职称" andIsHiddenArrow:NO andTitleLabWidth:80*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
        _jobTitleTF.lineImg.hidden = YES;
        
        [_footerView addSubview:_hospitalTF];
        [_footerView addSubview:_officeClassTF];
        [_footerView addSubview:_jobTitleTF];
        
        [_hospitalTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.mas_equalTo(weakSelf.footerView);
            make.height.mas_equalTo(44*kDeviceRate);
            make.top.mas_equalTo(weakSelf.footerView.mas_top).offset(1*kDeviceRate);
        }];
        
        [_officeClassTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(weakSelf.hospitalTF);
            make.top.mas_equalTo(weakSelf.hospitalTF.mas_bottom);
            make.left.mas_equalTo(weakSelf.officeClassTF.mas_left);
        }];
        
        [_jobTitleTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(weakSelf.hospitalTF);
            make.top.mas_equalTo(weakSelf.officeClassTF.mas_bottom);
            make.left.mas_equalTo(weakSelf.officeClassTF.mas_left);
        }];
        
        
        _hospitalTF.btnBlock = ^(){//医院选择
        
            [weakSelf pickUpKeyboard];
            
            ChooseHospitalViewController *hospital = [[ChooseHospitalViewController alloc] init];
            hospital.refreshHome = ^(){
                NSString *hospitalStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
                [weakSelf.hospitalTF.clickBtn setTitle:hospitalStr forState:UIControlStateNormal];
                [weakSelf.hospitalTF.clickBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
            };
            [weakSelf.navigationController pushViewController:hospital animated:YES];
        };
        
        _jobTitleTF.btnBlock = ^(){//职称选择
        
            [weakSelf pickUpKeyboard];
            
            UGCJobClassViewController *jobClass = [[UGCJobClassViewController alloc] init];
            jobClass.jobBlock = ^(NSDictionary *dic){
            
                [weakSelf.jobTitleTF.clickBtn setTitle:[dic objectForKey:@"name"] forState:UIControlStateNormal];
                [weakSelf.jobTitleTF.clickBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
            };
            [weakSelf.navigationController pushViewController:jobClass animated:YES];
        };
    }
    
    //下一步按钮
    _nextStepBack = [[CustomBorderView alloc] init];
    [_backView addSubview:_nextStepBack];
    [_nextStepBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.footerView.mas_bottom).offset(15*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
        make.centerX.width.mas_equalTo(weakSelf.backView);
    }];
    
    _nextStepBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_backView addSubview:_nextStepBtn];
    [_nextStepBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [_nextStepBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    _nextStepBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
    _nextStepBtn.backgroundColor = HT_COLOR_FONT_INVERSE;
    [_nextStepBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.nextStepBack.mas_top).offset(1*kDeviceRate);
        make.height.mas_equalTo(43*kDeviceRate);
        make.centerX.width.mas_equalTo(weakSelf.backView);
    }];
    [_nextStepBtn addTarget:self action:@selector(nextStepBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //用户协议
    UILabel *protocalLab = [[UILabel alloc] init];
    [_backView addSubview:protocalLab];
    protocalLab.textColor = UIColorFromRGB(0x999999);
    protocalLab.font = [UIFont systemFontOfSize:13*kDeviceRate];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"我已同意遵守《医生认证与直播协议》"];
    [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x3ab7f5) range:NSMakeRange(6, 11)];
    protocalLab.attributedText = attrStr;
    
    [protocalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.backView.mas_left).offset(75*kDeviceRate);
        make.top.mas_equalTo(weakSelf.nextStepBack.mas_bottom).offset(125*kDeviceRate);
    }];
    protocalLab.userInteractionEnabled = YES;
    
    
    MyGesture *protocalGes = [[MyGesture alloc] initWithTarget:self action:@selector(protocalJumpBtnSendDown)];
    protocalGes.numberOfTapsRequired = 1;
    [protocalLab addGestureRecognizer:protocalGes];
    
    
    UIButton *protocalJumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backView addSubview:protocalJumpBtn];
    [protocalJumpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.backView.mas_left).offset(75*kDeviceRate);
        make.top.mas_equalTo(weakSelf.nextStepBack.mas_bottom).offset(115*kDeviceRate);//点击区域增高10
        make.centerX.mas_equalTo(weakSelf.backView.mas_centerX);
        make.height.mas_equalTo(30*kDeviceRate);
    }];
    protocalJumpBtn.adjustsImageWhenHighlighted = NO;
    [protocalJumpBtn addTarget:self action:@selector(protocalJumpBtnSendDown) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(protocalLab.mas_bottom);
    }];
}

- (void)protocalJumpBtnSendDown
{
    NSLog(@"我点击了认证协议");
    
    KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
    know.urlStr = [UrlPageJumpManeger liveProtocolPageJump];
    know.existNaviBar = YES;
    [self.navigationController pushViewController:know animated:YES];
}

- (void)pickUpKeyboard
{
    [_nameTF.textField resignFirstResponder];
    [_idTF.textField resignFirstResponder];
    [_phoneNumTF.textField resignFirstResponder];
    [_hospitalTF.textField resignFirstResponder];
    [_officeClassTF.textField resignFirstResponder];
    [_jobTitleTF.textField resignFirstResponder];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self pickUpKeyboard];
}

- (void)nextStepBtnSenderDown:(UIButton *)btn
{
    btn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
    });
    
    if (_nameTF.textField.text.length>0) {
        [_paraDic setObject:_nameTF.textField.text forKey:@"name"];
        NSLog(@"%@",_paraDic);
    }else{
        CustomAlertView *nameAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"请输入完整的个人信息,以便更好的完成\n您的身份认证" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOKButtonMessage:@"" withDelegate:self];
        [nameAlert show];
        
        return;
    }
    
    if (![CHECKFORMAT judgeIdentityStringValid:_idTF.textField.text]) {
        
        CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请输入正确的18位身份证号" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
        
        [idAlert show];
        
        return;

    }else{
    
        [_paraDic setObject:_idTF.textField.text forKey:@"idNumber"];
    }
    
    if (_phoneNumTF.textField.text.length == 0) {

        [_paraDic setObject:_mobileStr forKey:@"mobile"];
    }else if(_phoneNumTF.textField.text.length != 11 || ![CHECKFORMAT checkPhoneNumber:_phoneNumTF.textField.text]){
        CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请输入正确的11位手机号" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
        
        [idAlert show];
        
        return;

    }else{
        [_paraDic setObject:_phoneNumTF.textField.text forKey:@"mobile"];
    }
    
    if (_hospitalTF.clickBtn.titleLabel.text.length == 0) {
        
        CustomAlertView *nameAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"请输入完整的个人信息,以便更好的完成\n您的身份认证" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOKButtonMessage:@"" withDelegate:self];
        [nameAlert show];
        
        return;

    }else{
    
        [_paraDic setObject:_hospitalTF.clickBtn.titleLabel.text forKey:@"hospital"];
    }
    
    if (_officeClassTF.textField.text.length == 0) {
        CustomAlertView *nameAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"请输入完整的个人信息,以便更好的完成\n您的身份认证" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOKButtonMessage:@"" withDelegate:self];
        [nameAlert show];
        
        return;
    }else{
    
        [_paraDic setObject:_officeClassTF.textField.text forKey:@"department"];
    }
    
    if (_jobTitleTF.clickBtn.titleLabel.text.length == 0) {
        
        CustomAlertView *nameAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"请输入完整的个人信息,以便更好的完成\n您的身份认证" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOKButtonMessage:@"" withDelegate:self];
        [nameAlert show];
        
        return;
    }else{
    
        [_paraDic setObject:_jobTitleTF.clickBtn.titleLabel.text forKey:@"academicTitle"];
    }
    
    UGCAuthCredUploadViewController *gredVC = [[UGCAuthCredUploadViewController alloc] init];
    
    gredVC.basicInfoDic = [_paraDic mutableCopy];
    
    [self.navigationController pushViewController:gredVC animated:YES];
}

-(UIImageView *)honLineImage
{
    UIImageView *lineImage = [[UIImageView alloc] init];
    [_backView addSubview:lineImage];
    lineImage.backgroundColor = HT_COLOR_SPLITLINE;
    
    return lineImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
