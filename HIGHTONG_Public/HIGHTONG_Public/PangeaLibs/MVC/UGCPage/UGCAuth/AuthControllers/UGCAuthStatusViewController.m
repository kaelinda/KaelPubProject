//
//  UGCAuthStatusViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCAuthStatusViewController.h"

@interface UGCAuthStatusViewController ()

@property (nonatomic, strong)UIButton *returnBtn;
@property (nonatomic, strong)UIView *backView;
@property (nonatomic, strong)UIImageView *logoImg;
@property (nonatomic, strong)UILabel *statusLab;
@property (nonatomic, strong)UILabel *nameLab;
@property (nonatomic, strong)UILabel *hospitalLab;
@property (nonatomic, strong)UILabel *officeLab;
@property (nonatomic, strong)UILabel *professionLab;

@property (nonatomic, strong)UIImageView *headerImg;
@property (nonatomic, strong)NSDictionary *dataDic;

@end

@implementation UGCAuthStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpViewNaviBar];
    
    [self setUpMainUI];
    
    if (_isStatus) {//已认证
        
        _dataDic = [NSDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"AuthenticSuccessedData"]];

        if (_dataDic) {
            NSLog(@"已认证后的数据 ------ %@",_dataDic);
        }
        
        [self setUpAuthenticedView];
    }else{//认证中
        
        [self setUpAuthenticingView];
    }
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(returnBtnSenderDown)];
    [self setNaviBarLeftBtn:_returnBtn];
    
    [self setNaviBarTitle:@"医生认证"];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
}

- (void)returnBtnSenderDown
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpMainUI
{
    __weak typeof(self) weakSelf = self;
    
    _backView = [[UIView alloc] init];
    [self.view addSubview:_backView];
    _backView.backgroundColor = UIColorFromRGB(0xffffff);
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.view.mas_top).offset(64);
        make.width.centerX.bottom.mas_equalTo(weakSelf.view);
    }];
    
    UILabel *mobileLab = [self setLabWithText:@"如需更改资料，请拨打客服电话010-000000" andTextColor:UIColorFromRGB(0x969696) andFont:11];
    [_backView addSubview:mobileLab];
    mobileLab.textAlignment = NSTextAlignmentCenter;
    [mobileLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(weakSelf.backView);
        make.bottom.mas_equalTo(weakSelf.backView.mas_bottom).offset(-10*kDeviceRate);
    }];
}

//认证中view
- (void)setUpAuthenticingView
{
    __weak typeof(self) weakSelf = self;
    
    _logoImg = [[UIImageView alloc] init];
    [_backView addSubview:_logoImg];
    _logoImg.image = [UIImage imageNamed:@"certification_ing"];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.backView.mas_top).offset(168*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.backView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(170*kDeviceRate, 155*kDeviceRate));
    }];
    
    _statusLab = [self setLabWithText:@"您当前处于认证中，请耐心等候" andTextColor:UIColorFromRGB(0x6f6f6f) andFont:15*kDeviceRate];
    [_backView addSubview:_statusLab];
    _statusLab.textAlignment = NSTextAlignmentCenter;
    [_statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.backView);
        make.top.mas_equalTo(weakSelf.logoImg.mas_bottom).offset(33*kDeviceRate);
    }];
}


- (UIImage *)createImageWithColor:(UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

//已认证view
- (void)setUpAuthenticedView
{
    __weak typeof(self) weakSelf = self;
    
    _logoImg = [[UIImageView alloc] init];
    [_backView addSubview:_logoImg];
//    _logoImg.image = [UIImage imageNamed:@"certification_ed"];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.backView.mas_top).offset(111*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.backView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(180*kDeviceRate, 160*kDeviceRate));
    }];
    
    
    _headerImg = [[UIImageView alloc] init];
    [_logoImg addSubview:_headerImg];
    _headerImg.backgroundColor = [UIColor redColor];
    [_headerImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(145*kDeviceRate);
        make.top.centerX.mas_equalTo(weakSelf.logoImg);
    }];
    _headerImg.layer.masksToBounds = YES;    
    [_headerImg.layer setCornerRadius:(145*kDeviceRate/2)];
    
    NSString *photoStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];
    [_headerImg sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[self createImageWithColor:[UIColor colorWithRed:143/255.0 green:212/255.0 blue:142/255.0 alpha:1]]];


    
    UIImageView *headBaseImg = [[UIImageView alloc] init];
    [_logoImg addSubview:headBaseImg];
    headBaseImg.image = [UIImage imageNamed:@"certification_decoration"];
    [headBaseImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(150*kDeviceRate, 55*kDeviceRate));
        make.centerX.bottom.mas_equalTo(weakSelf.logoImg);
    }];
    
    
    _statusLab = [self setLabWithText:@"恭喜您！您已通过认证~" andTextColor:UIColorFromRGB(0x69a148) andFont:21*kDeviceRate];
    [_backView addSubview:_statusLab];
    _statusLab.textAlignment = NSTextAlignmentCenter;
    [_statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.backView);
        make.top.mas_equalTo(weakSelf.logoImg.mas_bottom).offset(33*kDeviceRate);
    }];
    
    
    NSString *nameStr = [NSString stringWithFormat:@"%@ %@",[_dataDic objectForKey:@"academicTitle"],[_dataDic objectForKey:@"name"]];
    
    _nameLab = [self setLabWithText:nameStr andTextColor:UIColorFromRGB(0x757575) andFont:15];//@"副主任医师 葛炮"
    [_backView addSubview:_nameLab];
    _nameLab.textAlignment = NSTextAlignmentCenter;
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.statusLab.mas_bottom).offset(15*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.statusLab.mas_centerX);
    }];
    
    
    NSString *hospitalStr = [NSString stringWithFormat:@"所属医院：%@",[_dataDic objectForKey:@"hospital"]];
    
    _hospitalLab = [self setLabWithText:hospitalStr andTextColor:UIColorFromRGB(0x757575) andFont:15];//@"所属医院：广州医科大学第三附属医院"
    [_backView addSubview:_hospitalLab];
    _hospitalLab.textAlignment = NSTextAlignmentCenter;
    [_hospitalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.nameLab.mas_bottom).offset(10*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.nameLab.mas_centerX);
    }];
    
    NSString *officeStr = [NSString stringWithFormat:@"所在科室：%@",[_dataDic objectForKey:@"department"]];

    _officeLab = [self setLabWithText:officeStr andTextColor:UIColorFromRGB(0x757575) andFont:15];//@"所在科室：耳鼻喉科"
    [_backView addSubview:_officeLab];
    _officeLab.textAlignment = NSTextAlignmentCenter;
    [_officeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.hospitalLab.mas_bottom).offset(10*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.hospitalLab.mas_centerX);
    }];
    
    NSString *professionStr = [NSString stringWithFormat:@"医生职称：%@",[_dataDic objectForKey:@"academicTitle"]];
    
    _professionLab = [self setLabWithText:professionStr andTextColor:UIColorFromRGB(0x757575) andFont:15];//@"医生职称：副主任医师"
    [_backView addSubview:_professionLab];
    _professionLab.textAlignment = NSTextAlignmentCenter;
    [_professionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.officeLab.mas_bottom).offset(10*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.officeLab.mas_centerX);
    }];
}

- (UILabel *)setLabWithText:(NSString *)text andTextColor:(UIColor *)color andFont:(CGFloat)fontSize
{
    UILabel *lab = [[UILabel alloc] init];
    lab.text = text;
    lab.textColor = color;
    lab.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    
    return lab;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
