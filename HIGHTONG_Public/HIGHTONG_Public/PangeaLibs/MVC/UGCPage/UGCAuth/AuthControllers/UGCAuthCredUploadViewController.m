//
//  UGCAuthCredUploadViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCAuthCredUploadViewController.h"
#import "CustomBorderView.h"
#import "UGCAuthItemInputView.h"
#import "CameraAndAlbum.h"
#import "HTALViewController.h"
#import "HTALAssets.h"
#import "UGCHomeViewController.h"
#import "HTRequest.h"
#import "CustomAlertView.h"


typedef NS_ENUM(NSUInteger, AuthViewButtonTagType) {
    KCodeAuthBtnTag = 12121,//执业编码认证
    KSurgeonAuthBtnTag,//军医认证
    KSurgeonPhotoBtnTag,//军官证照片
    KBadgesPhotoBtnTag,//胸牌照片
    KSubmitBtnTag,//提交按钮
    KreturnBtnTag,
};

@interface UGCAuthCredUploadViewController ()<UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,HTRequestDelegate,CustomAlertViewDelegate,UITextFieldDelegate>
{
    NSString *_physicianType;
    
    UIImage *_surgeonPhotoImage;
    
    UIImage *_badgePhotoImage;
}
@property (nonatomic, strong)UIButton *returnBtn;//返回按钮
@property (nonatomic, strong)UIScrollView *backScrView;
@property (nonatomic, strong)UIView *headerView;
@property (nonatomic, strong)UIView *scrContentView;
@property (nonatomic, strong)UIView *professionCodeAuthView;
@property (nonatomic, strong)UIView *surgeonAuthView;

@property (nonatomic, strong)UIButton *codeAuthBtn;//执业编码认证
@property (nonatomic, strong)UIImageView *codeAuthRunImg;
@property (nonatomic, strong)UIButton *surgeonAuthBtn;//军医认证
@property (nonatomic, strong)UIImageView *surgeonAuthRunImg;
@property (nonatomic, strong)UIButton *submitBtn;//提交按钮

@property (nonatomic, strong)UIButton *surgeonPhotoBtn;//军官证照片
@property (nonatomic, strong)UIButton *badgePhotoBtn;//胸牌照片
@property (nonatomic, strong)CameraAndAlbum *changeHeadImgView;

@property (nonatomic, copy)NSString *photoFlagStr;//军官证照片还是胸牌照片

//@property (nonatomic, strong)UGCAuthItemInputView *codeTF;

@property (nonatomic, strong)UITextField *textField;
@property (nonatomic, strong)UILabel *textFieldLab;

@end

@implementation UGCAuthCredUploadViewController

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_changeHeadImgView resignKeyWindow];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"基本信息字典：\n%@",_basicInfoDic);
    
    _physicianType = @"0";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(takePhotoSuccess:) name:@"TakePhotoSuccess" object:nil];
    
    [self addNotification];
    
    [self setUpViewNaviBar];
    
    [self setUpMainUI];
    
    [self setUpHeaderView];
    
    [self setUpProfessionCodeAuthView];
    
    [self setUpSurgeonAuthView];
    
    [self selectPhotoView];
}

- (void)addNotification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectDone:) name:PICKER_TAKE_DONE object:nil];
    });
}

//****此处需要重新理解一下
- (void)selectDone:(NSNotification *)noti
{
    NSArray *selectArray = noti.userInfo[@"selectAssets"];

    NSLog(@"通知返回来的数组：\n%@",selectArray);
    
    if (selectArray.count>1) {//预览后使用截图
        
        UIImage *img = [selectArray lastObject];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            if ([_photoFlagStr isEqualToString:@"1"]) {
                
                [_surgeonPhotoBtn setImage:img forState:UIControlStateNormal];
                
                _surgeonPhotoImage = img;
            }else if ([_photoFlagStr isEqualToString:@"2"]){
                
                [_badgePhotoBtn setImage:img forState:UIControlStateNormal];
                
                _badgePhotoImage = img;
            }
            
        });
    }else{//相册直接确认使用全图
    
        HTALAssets *asset = selectArray[0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            if ([_photoFlagStr isEqualToString:@"1"]) {
                
                [_surgeonPhotoBtn setImage:[asset thumbnailImg] forState:UIControlStateNormal];
                
                _surgeonPhotoImage = [asset thumbnailImg];
            }else if ([_photoFlagStr isEqualToString:@"2"]){
                
                [_badgePhotoBtn setImage:[asset thumbnailImg] forState:UIControlStateNormal];
                
                _badgePhotoImage = [asset thumbnailImg];
            }
            
        });
    }
    
//    HTALAssets *asset = selectArray[0];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        if ([_photoFlagStr isEqualToString:@"1"]) {
//            
//            [_surgeonPhotoBtn setImage:[asset thumbnailImg] forState:UIControlStateNormal];
//            
//            _surgeonPhotoImage = [asset thumbnailImg];
//        }else if ([_photoFlagStr isEqualToString:@"2"]){
//            
//            [_badgePhotoBtn setImage:[asset thumbnailImg] forState:UIControlStateNormal];
//            
//            _badgePhotoImage = [asset thumbnailImg];
//        }
//
//    });
}

- (void)takePhotoSuccess:(NSNotification *)notification
{
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = notification.userInfo;

    NSLog(@"照片信息：%@",dic);
    
    if ([_photoFlagStr isEqualToString:@"1"]) {
        
        [_surgeonPhotoBtn setImage:[dic objectForKey:@"photo"] forState:UIControlStateNormal];
        
        _surgeonPhotoImage = [dic objectForKey:@"photo"];
        
    }else if ([_photoFlagStr isEqualToString:@"2"]){
    
        [_badgePhotoBtn setImage:[dic objectForKey:@"photo"] forState:UIControlStateNormal];
        
        _badgePhotoImage = [dic objectForKey:@"photo"];
    }
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(AuthViewBtnSenderDown:)];
    [self setNaviBarLeftBtn:_returnBtn];
    _returnBtn.tag = KreturnBtnTag;
    
    [self setNaviBarTitle:@"医生认证"];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
}

- (void)setUpMainUI
{
    __weak typeof(self) weakSelf = self;
    
    _headerView = [[UIView alloc] init];
    [self.view addSubview:_headerView];
    _headerView.backgroundColor = UIColorFromRGB(0xffffff);
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(weakSelf.view);
        make.height.mas_equalTo(44*kDeviceRate);
        make.top.mas_equalTo(weakSelf.view.mas_top).offset(64);
    }];
    
    
    UIImageView *headerLine = [[UIImageView alloc] init];
    [_headerView addSubview:headerLine];
    headerLine.backgroundColor = UIColorFromRGB(0xe8e8e8);
    [headerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1*kDeviceRate);
        make.centerX.left.bottom.mas_equalTo(_headerView);
    }];
    
    
    _backScrView = [[UIScrollView alloc] init];
    [self.view addSubview:_backScrView];
    _backScrView.backgroundColor = [UIColor clearColor];
    _backScrView.delegate = self;
    _backScrView.pagingEnabled = YES;
    _backScrView.bounces = NO;
    _backScrView.showsHorizontalScrollIndicator = NO;
    _backScrView.showsVerticalScrollIndicator = NO;
    [_backScrView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(weakSelf.view);
        make.height.mas_equalTo(227*kDeviceRate);
        make.top.mas_equalTo(weakSelf.headerView.mas_bottom).offset(13*kDeviceRate);
    }];
    
    _scrContentView = [[UIView alloc] init];
    [_backScrView addSubview:_scrContentView];
    [_scrContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(weakSelf.backScrView);
        make.height.mas_equalTo(weakSelf.backScrView.mas_height);
    }];
    
    
    _professionCodeAuthView = [[UIView alloc] init];
    [_scrContentView addSubview:_professionCodeAuthView];
    [_professionCodeAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(weakSelf.backScrView.mas_height);
        make.width.mas_equalTo(kDeviceWidth);
        make.centerY.mas_equalTo(weakSelf.backScrView.mas_centerY);
        make.left.mas_equalTo(0);
    }];
    _professionCodeAuthView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    _surgeonAuthView = [[UIView alloc] init];
    [_scrContentView addSubview:_surgeonAuthView];
    [_surgeonAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.professionCodeAuthView);
        make.left.mas_equalTo(weakSelf.professionCodeAuthView.mas_right);
        make.centerY.mas_equalTo(weakSelf.backScrView.mas_centerY);
    }];
    _surgeonAuthView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    [_scrContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf.surgeonAuthView.mas_right);
    }];
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_submitBtn];
    [_submitBtn setImage:[UIImage imageNamed:@"doctor_coding_submit_normal"] forState:UIControlStateNormal];    
    [_submitBtn setImage:[UIImage imageNamed:@"doctor_coding_submit_pressed"] forState:UIControlStateHighlighted];
    [_submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backScrView.mas_bottom);
        make.height.mas_equalTo(40*kDeviceRate);
        make.width.mas_equalTo(350*kDeviceRate);
        make.centerX.mas_equalTo(weakSelf.view.mas_centerX);
    }];
    _submitBtn.tag = KSubmitBtnTag;
    [_submitBtn addTarget:self action:@selector(AuthViewBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UILabel *tipLab = [self setUpLab];
    UILabel *numLab = [self setUpLab];
    
    tipLab.text = @"执业编码认证之后不可修改，请认真填写。如需变更认证资料请";
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"拨打客服电话：010-12345678"];
    [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x3ab7f5) range:NSMakeRange(7, 12)];
    numLab.attributedText = attrStr;
    
    [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.submitBtn.mas_bottom).offset(19*kDeviceRate);
        make.left.mas_equalTo(weakSelf.view.mas_left).offset(15*kDeviceRate);
    }];
    
    [numLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tipLab.mas_bottom).offset(8*kDeviceRate);
        make.left.mas_equalTo(tipLab.mas_left);
    }];
}

- (void)setUpHeaderView
{
    __weak typeof(self) weakSelf = self;
    
    _codeAuthBtn = [self setUpButtonWithTitle:@"执业编码认证" andTitleColor:UIColorFromRGB(0x3ab7f5) andFont:15 andTag:KCodeAuthBtnTag andTarget:self andSelect:@selector(AuthViewBtnSenderDown:)];
    
    [_headerView addSubview:_codeAuthBtn];
    [_codeAuthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kDeviceWidth/2);
        make.top.mas_equalTo(weakSelf.headerView.mas_top);
        make.bottom.mas_equalTo(weakSelf.headerView.mas_bottom).offset(-2*kDeviceRate);
        make.left.mas_equalTo(weakSelf.headerView.mas_left);
    }];
    
    
    _codeAuthRunImg = [[UIImageView alloc] init];
    [_headerView addSubview:_codeAuthRunImg];
    _codeAuthRunImg.backgroundColor = UIColorFromRGB(0x3ab7f5);
    [_codeAuthRunImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(2*kDeviceRate);
        make.width.mas_equalTo(105*kDeviceRate);
        make.bottom.mas_equalTo(weakSelf.headerView.mas_bottom);
        make.centerX.mas_equalTo(weakSelf.codeAuthBtn.mas_centerX);
    }];
    
    
    _surgeonAuthBtn = [self setUpButtonWithTitle:@"军医认证" andTitleColor:UIColorFromRGB(0x666666) andFont:14 andTag:KSurgeonAuthBtnTag andTarget:self andSelect:@selector(AuthViewBtnSenderDown:)];
    
    [_headerView addSubview:_surgeonAuthBtn];
    [_surgeonAuthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.codeAuthBtn);
        make.left.mas_equalTo(weakSelf.codeAuthBtn.mas_right);
        make.top.mas_equalTo(weakSelf.codeAuthBtn.mas_top);
    }];
    
    
    _surgeonAuthRunImg = [[UIImageView alloc] init];
    [_headerView addSubview:_surgeonAuthRunImg];
    _surgeonAuthRunImg.backgroundColor = UIColorFromRGB(0x3ab7f5);
    _surgeonAuthRunImg.hidden = YES;
    [_surgeonAuthRunImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.codeAuthRunImg);
        make.centerX.mas_equalTo(weakSelf.surgeonAuthBtn.mas_centerX);
        make.bottom.mas_equalTo(weakSelf.codeAuthRunImg);
    }];
}

- (void)setUpProfessionCodeAuthView
{
    __weak typeof(self) weakSelf = self;
    
    CustomBorderView *backView = [[CustomBorderView alloc] init];
    [_professionCodeAuthView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.centerX.width.mas_equalTo(weakSelf.professionCodeAuthView);
        make.height.mas_equalTo(151*kDeviceRate);
    }];
    
    UILabel *infoLab = [self setLabWithText:@"《医师执业证书》是唯一验证医生身份的凭证，请提供执业编码进行实名认证！" andTextColor:UIColorFromRGB(0x757575) andFont:14];
    infoLab.textAlignment = NSTextAlignmentLeft;
    [backView addSubview:infoLab];
    [infoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backView.mas_top).offset(10.5*kDeviceRate);//10
        make.left.mas_equalTo(backView.mas_left).offset(15*kDeviceRate);
        make.right.mas_equalTo(backView.mas_right).offset(-15*kDeviceRate);
    }];
    infoLab.numberOfLines = 2;
    
    UILabel *midLab = [self setLabWithText:@"如右图所示" andTextColor:UIColorFromRGB(0x333333) andFont:14];
    [backView addSubview:midLab];
    midLab.textAlignment = NSTextAlignmentLeft;
    [midLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backView.mas_left).offset(55);//比尺寸图少2
        make.top.mas_equalTo(infoLab.mas_bottom).offset(36*kDeviceRate);
    }];
    
    UILabel *underLab = [self setLabWithText:@"医师执业证书红框内编码" andTextColor:UIColorFromRGB(0x757575) andFont:12];
    [backView addSubview:underLab];
    underLab.textAlignment = NSTextAlignmentLeft;
    [underLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backView.mas_left).offset(33*kDeviceRate);//比尺寸图多5
        make.top.mas_equalTo(midLab.mas_bottom).offset(15*kDeviceRate);
    }];
    
    
    UIImageView *certificateImg = [[UIImageView alloc] init];
    [backView addSubview:certificateImg];
    certificateImg.image = [UIImage imageNamed:@"practice_coding_position"];
    [certificateImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(backView.mas_bottom).offset(-15.5*kDeviceRate);//15
        make.right.mas_equalTo(backView.mas_right).offset(-42*kDeviceRate);
        make.width.mas_equalTo(100*kDeviceRate);
        make.height.mas_equalTo(75*kDeviceRate);
    }];
    
    
    CustomBorderView *codeBackView = [[CustomBorderView alloc] init];
    [_professionCodeAuthView addSubview:codeBackView];
    [codeBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.mas_equalTo(weakSelf.professionCodeAuthView);
        make.top.mas_equalTo(backView.mas_bottom).offset(18*kDeviceRate);
        make.height.mas_equalTo(41*kDeviceRate);
    }];
    
//    _codeTF = [[UGCAuthItemInputView alloc] initInputViewWithTitleName:@"执业编码" andPlaceholdStr:@"请输入15位执业证书编码" andIsHiddenArrow:YES andTitleLabWidth:75*kDeviceRate andTitleLeftSepWidth:18*kDeviceRate];
//    [codeBackView addSubview:_codeTF];
//    _codeTF.lineImg.hidden = YES;
//    [_codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.mas_equalTo(codeBackView);
//        make.height.mas_equalTo(40*kDeviceRate);
//        make.width.mas_equalTo(codeBackView.mas_width);
//    }];
    
    
    UILabel *titleLab = [[UILabel alloc] init];
    [codeBackView addSubview:titleLab];
    titleLab.text = @"执业编码";
    titleLab.font = [UIFont systemFontOfSize:15*kDeviceRate];
    titleLab.textColor = UIColorFromRGB(0x333333);
    titleLab.textAlignment = NSTextAlignmentLeft;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(codeBackView.mas_centerY);
        make.left.mas_equalTo(codeBackView.mas_left).offset(18*kDeviceRate);
        make.width.mas_equalTo(75*kDeviceRate);
    }];
    
    _textField = [[UITextField alloc] init];
    _textField.delegate = self;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _textField.textAlignment = NSTextAlignmentLeft;
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.font = [UIFont systemFontOfSize:14*kDeviceRate];
    _textField.textColor = UIColorFromRGB(0x333333);
    [codeBackView addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLab.mas_right);//.offset(15*kDeviceRate);
        make.right.mas_equalTo(codeBackView.mas_right).offset(-5*kDeviceRate);
        make.top.mas_equalTo(codeBackView.mas_top);
        make.bottom.mas_equalTo(codeBackView.mas_bottom).offset(-1*kDeviceRate);
    }];
    [_textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
    
    _textFieldLab = [[UILabel alloc] init];
    [_textField addSubview: _textFieldLab];
    [_textFieldLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    _textFieldLab.text = @"请输入15位执业证书编码";
    _textFieldLab.font = [UIFont systemFontOfSize:14*kDeviceRate];
    _textFieldLab.textColor = UIColorFromRGB(0x999999);

}

- (void)inputBlockValueChange:(UITextField *)textField
{
    if (textField.text.length>0) {
        
        self.textFieldLab.hidden = YES;
        
    }else{
        self.textFieldLab.hidden = NO;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    NSInteger realLength = textField.text.length + string.length - range.length;
    
    BOOL isused = realLength > 15 ? NO : YES;//toBeString.length
    
    return isused;
}


- (UILabel *)setLabWithText:(NSString *)text andTextColor:(UIColor *)color andFont:(CGFloat)fontSize
{
    UILabel *lab = [[UILabel alloc] init];
    lab.text = text;
    lab.textColor = color;
    lab.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    
    return lab;
}

- (void)setUpSurgeonAuthView
{
    __weak typeof(self) weakSelf = self;
    
    CustomBorderView *backView = [[CustomBorderView alloc] init];
    [_surgeonAuthView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(weakSelf.surgeonAuthView);
        make.bottom.mas_equalTo(weakSelf.surgeonAuthView.mas_bottom).offset(-24*kDeviceRate);
    }];
    
    UILabel *infoTitleLab = [self setLabWithText:@"上传军官证及胸牌" andTextColor:UIColorFromRGB(0x2d2d2d) andFont:14];
    infoTitleLab.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:infoTitleLab];
    [infoTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backView.mas_top).offset(15*kDeviceRate);
        make.centerX.mas_equalTo(backView.mas_centerX);
    }];
    
    UILabel *contentLab = [self setLabWithText:@"军医用户需上传军官证及胸牌，请确保姓名、医院、职称、头像等信息清晰可见，上传资料仅用于认证，患者及第三方不可见。" andTextColor:UIColorFromRGB(0x6f6f6f) andFont:13];
    [backView addSubview:contentLab];
    contentLab.textAlignment = NSTextAlignmentLeft;
    contentLab.numberOfLines = 3;
    [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(infoTitleLab.mas_bottom).offset(11*kDeviceRate);
        make.left.mas_equalTo(backView.mas_left).offset(14*kDeviceRate);
        make.right.mas_equalTo(backView.mas_right).offset(-25*kDeviceRate);
    }];
    
    //军官证照片
    _surgeonPhotoBtn = [self setUpCustomBtnWithNormalImg:@"select_photo" andTag:KSurgeonPhotoBtnTag andTarget:self andSelect:@selector(AuthViewBtnSenderDown:)];
    [backView addSubview:_surgeonPhotoBtn];
    [_surgeonPhotoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(64*kDeviceRate, 62*kDeviceRate));
        make.top.mas_equalTo(contentLab.mas_bottom).offset(15*kDeviceRate);
        make.left.mas_equalTo(backView.mas_left).offset(14*kDeviceRate);
    }];
    _surgeonPhotoBtn.tag = KSurgeonPhotoBtnTag;
    [_surgeonPhotoBtn addTarget:self action:@selector(AuthViewBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    _surgeonPhotoBtn.layer.cornerRadius = 3.0;
    _surgeonPhotoBtn.clipsToBounds=YES;
    
    //胸牌照片
    _badgePhotoBtn = [self setUpCustomBtnWithNormalImg:@"select_photo" andTag:KBadgesPhotoBtnTag andTarget:self andSelect:@selector(AuthViewBtnSenderDown:)];
    [backView addSubview:_badgePhotoBtn];
    [_badgePhotoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(weakSelf.surgeonPhotoBtn);
        make.left.mas_equalTo(weakSelf.surgeonPhotoBtn.mas_right).offset(26*kDeviceRate);
        make.top.mas_equalTo(weakSelf.surgeonPhotoBtn.mas_top);
    }];
    _badgePhotoBtn.tag = KBadgesPhotoBtnTag;
    [_badgePhotoBtn addTarget:self action:@selector(AuthViewBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    _badgePhotoBtn.layer.cornerRadius = 3.0;
    _badgePhotoBtn.clipsToBounds=YES;
    
    UILabel *surgeonPhotoLab = [self setLabWithText:@"军官证照片" andTextColor:UIColorFromRGB(0x2d2d2d) andFont:14];
    UILabel *badgePhotoLab = [self setLabWithText:@"胸牌照片" andTextColor:UIColorFromRGB(0x2d2d2d) andFont:14];
    
    [backView addSubview:surgeonPhotoLab];
    [backView addSubview:badgePhotoLab];
    
    [surgeonPhotoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.surgeonPhotoBtn.mas_centerX);
        make.top.mas_equalTo(weakSelf.surgeonPhotoBtn.mas_bottom).offset(9*kDeviceRate);
    }];
    
    [badgePhotoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.badgePhotoBtn.mas_centerX);
        make.top.mas_equalTo(weakSelf.badgePhotoBtn.mas_bottom).offset(9*kDeviceRate);
    }];
}

- (UIButton *)setUpCustomBtnWithNormalImg:(NSString *)imgStr andTag:(NSInteger)tagSize andTarget:(id)target andSelect:(SEL)select
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imgStr] forState:UIControlStateNormal];
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    btn.tag = tagSize;
    
    return btn;
}

- (UIButton *)setUpButtonWithTitle:(NSString *)titleStr andTitleColor:(UIColor *)color andFont:(CGFloat)fontSize andTag:(NSInteger)tagSize andTarget:(id)target andSelect:(SEL)select
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitle:titleStr forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:fontSize*kDeviceRate];
    btn.tag = tagSize;
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    return btn;
}

- (void)AuthViewBtnSenderDown:(UIButton *)btn
{
    switch (btn.tag) {
        case KCodeAuthBtnTag:
        {//执业编码认证
            _codeAuthBtn.userInteractionEnabled = NO;
            _surgeonAuthBtn.userInteractionEnabled = YES;
            
            [_codeAuthBtn setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            [_surgeonAuthBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            
            _codeAuthBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
            _surgeonAuthBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            
            _codeAuthRunImg.hidden = NO;
            _surgeonAuthRunImg.hidden = YES;
            
            [_backScrView setContentOffset:CGPointMake(0, 0)];
            
            _physicianType = @"0";
            
            break;
        }
        case KSurgeonAuthBtnTag:
        {//军医认证
            
            _codeAuthBtn.userInteractionEnabled = YES;
            _surgeonAuthBtn.userInteractionEnabled = NO;
            
            [_codeAuthBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            [_surgeonAuthBtn setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            _codeAuthBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            _surgeonAuthBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
            
            _codeAuthRunImg.hidden = YES;
            _surgeonAuthRunImg.hidden = NO;
            
            [_backScrView setContentOffset:CGPointMake(kDeviceWidth, 0)];
            
            _physicianType = @"1";
            
            break;
        }
        case KSurgeonPhotoBtnTag:
        {//军官证照片
            [UIView animateWithDuration:0.5 animations:^{
                
                self.changeHeadImgView.alpha = 1.0;
                
                _photoFlagStr = @"1";
                
                [self.changeHeadImgView show];
                
            } completion:^(BOOL finished) {
                
            }];

            break;
        }
        case KBadgesPhotoBtnTag:
        {//胸牌照片
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.changeHeadImgView.alpha = 1.0;
                
                _photoFlagStr = @"2";
                
                [self.changeHeadImgView show];
                
            } completion:^(BOOL finished) {
                
            }];
            
            break;
        }
        case KSubmitBtnTag:
        {//提交按钮
            
            btn.enabled = NO;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                btn.enabled = YES;
            });
            
            if ([_physicianType isEqualToString:@"0"]) {//执业编码认证
                
                if (_textField.text.length != 15) {//_codeTF.textField.text.length != 15
                    CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请输入完整的15位执业证书编码" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                    
                    [idAlert show];
                    return;
                }
                
                 
                HTRequest *commitRequest = [[HTRequest alloc] initWithDelegate:self];
                [commitRequest UGCCommonIdentificationWithIdNumber:[_basicInfoDic objectForKey:@"idNumber"] andName:[_basicInfoDic objectForKey:@"name"] andMobile:[_basicInfoDic objectForKey:@"mobile"] andHospital:[_basicInfoDic objectForKey:@"hospital"] andDepartment:[_basicInfoDic objectForKey:@"department"] andAcademicTitle:[_basicInfoDic objectForKey:@"academicTitle"] andPhysicianType:_physicianType andPhysicianCode:_textField.text andOfficialCard:nil andChestCard:nil];//_codeTF.textField.text

            }else if ([_physicianType isEqualToString:@"1"]){//军医认证
            
                if (_surgeonPhotoImage == nil) {
                    
                    CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请选择军官证照片" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                    
                    [idAlert show];
                    return;
                }
                if (_badgePhotoImage == nil) {
                    CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"请选择胸牌照片" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                    
                    [idAlert show];
                    return;

                }
                

                HTRequest *commitRequest = [[HTRequest alloc] initWithDelegate:self];
                [commitRequest UGCCommonIdentificationWithIdNumber:[_basicInfoDic objectForKey:@"idNumber"] andName:[_basicInfoDic objectForKey:@"name"] andMobile:[_basicInfoDic objectForKey:@"mobile"] andHospital:[_basicInfoDic objectForKey:@"hospital"] andDepartment:[_basicInfoDic objectForKey:@"department"] andAcademicTitle:[_basicInfoDic objectForKey:@"academicTitle"] andPhysicianType:_physicianType andPhysicianCode:@"" andOfficialCard:_surgeonPhotoImage andChestCard:_badgePhotoImage];
                

            }
                        
            break;
        }
        case KreturnBtnTag:
        {//返回按钮
            //中断认证都返回到跟视图
            [self.navigationController popToRootViewControllerAnimated:YES];
           
            break;
        }
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"医生认证接口返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
    
    if (status == 0 && result.count > 0) {
        if ([type isEqualToString:UGC_COMMON_IDENTIFICATION]) {
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                
                NSLog(@"认证成功");
                
                CustomAlertView *nameAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"发起认证成功，我们将尽快审核您的资\n料，请留意电话及短信通知" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOKButtonMessage:@"" withDelegate:self];
                [nameAlert show];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            if ([[result objectForKey:@"ret"] integerValue] == -2) {//身份认证有问题
                
                CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"上传信息失败，请稍后重试！" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                [idAlert show];
            }
            if ([[result objectForKey:@"ret"] integerValue] == -9) {
                
                CustomAlertView *idAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"上传信息失败，请稍后重试！" andButtonNum:1 andCancelButtonMessage:@"朕知道了" andOtherButtonMessage:@"" withDelegate:self];
                [idAlert show];
            }
        }
    }
}

- (void)selectPhotoView
{
    WS(wself);
    _changeHeadImgView = [[CameraAndAlbum alloc] init];
    [self.view addSubview:_changeHeadImgView];
    
    _changeHeadImgView.backBtn.Click = ^(UIButton_Block *btn, NSString *name){//阴影按钮，点击view消失
        
        [UIView animateWithDuration:0.5 animations:^{
            
            wself.changeHeadImgView.alpha = 0.0;
        } completion:^(BOOL finished) {
            
            wself.changeHeadImgView.hidden = YES;
        }];
    };
    
    _changeHeadImgView.cameraBtn.Click = ^(UIButton_Block *btn, NSString *name){//相机按钮
        
        wself.changeHeadImgView.hidden = YES;
        wself.changeHeadImgView.alpha = 0.0;
        
        if (isBeforeIOS7) {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = wself;
            imagePicker.allowsEditing = YES;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [wself presentViewController:imagePicker animated:YES completion:nil];
        }
        if (isAfterIOS8) {
            AVAuthorizationStatus auth = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            switch (auth) {
                case AVAuthorizationStatusRestricted:{ //
                    break;
                }
                case AVAuthorizationStatusDenied:{ // 拒绝
                    break;
                }
                default:{ // 允许
                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = wself;
                    imagePicker.allowsEditing = YES;
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [wself presentViewController:imagePicker animated:YES completion:nil];
                    break;
                }
            }
        }
    };
    
    _changeHeadImgView.albumBtn.Click = ^(UIButton_Block *btn, NSString *name){//相册按钮
        
        wself.changeHeadImgView.hidden = YES;
        
        HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowPhoto];//单选视频
        pickerVc.groupType = KAssetsLabraryCameraRoll;//视频是照片的具体分类
        pickerVc.maxCount = 1;//设置最多能选几张图片
        pickerVc.choiceType = ALSingleChoice;
        
        HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
        [wself presentViewController:nav animated:YES completion:nil];
        
    };
}

// 改变图片大小(90*90 32bit 算下来约10k) 90*90*32 /8 /1024
- (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //编辑图片，主要是缩放
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        
        image = [self scaleFromImage:image toSize:CGSizeMake(90, 90)];
        
        NSData *data;
        if (UIImagePNGRepresentation(image) == nil)
        {
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(image);
        }
        //关闭相册界面
        
        if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }else{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        

        //此处存储照片以备上传使用
        //缺存储~~~~~
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:image,@"photo", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TakePhotoSuccess" object:nil userInfo:dic];
        
        
        //上传服务器
//        dispatch_queue_t q = dispatch_queue_create("upload", NULL);
//        dispatch_async(q, ^{
//            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
//            [request NewUploadHeadPhotoWithImageData:image andWidth:45 andHeight:45 andTimeout:10];//[UIImage imageWithContentsOfFile:filePath]
//        });
//        
//        [self showCustomeHUD];
    }
}


//取消相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _backScrView) {
        
        if (scrollView.contentOffset.x == 0) {
            
            _codeAuthBtn.userInteractionEnabled = NO;
            _surgeonAuthBtn.userInteractionEnabled = YES;
            
            [_codeAuthBtn setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            [_surgeonAuthBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            
            _codeAuthBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
            _surgeonAuthBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            
            _codeAuthRunImg.hidden = NO;
            _surgeonAuthRunImg.hidden = YES;
            
            _physicianType = @"0";
        }
        else if (scrollView.contentOffset.x == kDeviceWidth){
            
            _codeAuthBtn.userInteractionEnabled = YES;
            _surgeonAuthBtn.userInteractionEnabled = NO;
            
            [_codeAuthBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            [_surgeonAuthBtn setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            _codeAuthBtn.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            _surgeonAuthBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
            
            _codeAuthRunImg.hidden = YES;
            _surgeonAuthRunImg.hidden = NO;
            
            _physicianType = @"1";
        }
    }
}

- (UILabel *)setUpLab
{
    UILabel *lab = [[UILabel alloc] init];
    [self.view addSubview:lab];
    lab.font = [UIFont systemFontOfSize:11*kDeviceRate];
    lab.textAlignment = NSTextAlignmentLeft;
    lab.textColor = UIColorFromRGB(0x757575);
    return lab;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TakePhotoSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PICKER_TAKE_DONE object:nil];
}


@end
