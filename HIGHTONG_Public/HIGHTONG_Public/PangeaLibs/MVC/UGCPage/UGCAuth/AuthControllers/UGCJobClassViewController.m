//
//  UGCJobClassViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCJobClassViewController.h"
#import "JobClassTableViewCell.h"
#import "HTRequest.h"
#import "UGCLocalListCenter.h"

@interface UGCJobClassViewController ()<UITableViewDelegate,UITableViewDataSource,HTRequestDelegate>

@property (nonatomic, strong)UIButton *returnBtn;

@property (nonatomic, strong)UITableView *tableview;

@property (nonatomic, strong)NSMutableArray *dataArr;

@end

@implementation UGCJobClassViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


//- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
//{
//    NSLog(@"医生职称接口返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
//
//    if (status == 0 && result.count > 0) {
//        if ([type isEqualToString:UGC_COMMON_GETACADEMICTITLE]) {
//            if ([[result objectForKey:@"ret"] integerValue] == 0) {
//                
//                _dataArr = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
//                
//                NSLog(@"%@",_dataArr);
//                
//                [_tableview reloadData];
//            }
//        }
//    }
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    HTRequest *academicTitleRequest = [[HTRequest alloc] initWithDelegate:self];
//    [academicTitleRequest UGCCommonGetAcademicTitleWithReturn];
    
//    _dataArr = [NSMutableArray array];
    
    [self setUpViewNaviBar];
    
    [self setUpTableView];
}

- (void)setUpViewNaviBar
{
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(returnBtnSenderDown)];
    [self setNaviBarLeftBtn:_returnBtn];
    
    [self setNaviBarTitle:@"选择职称"];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
}

- (void)returnBtnSenderDown
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpTableView
{
    UIView *tempView = [[UIView alloc] init];
    [self.view addSubview:tempView];
    
    _tableview = [[UITableView alloc] init];
    _tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    [self.view addSubview:_tableview];
    _tableview.separatorStyle = UITableViewCellEditingStyleNone;
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.showsHorizontalScrollIndicator = NO;
    _tableview.showsVerticalScrollIndicator = NO;
    
    _dataArr = [NSMutableArray arrayWithArray:[UGCLocalListCenter getUGCAcademicTitleList]];
    
    [_tableview reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_jobBlock) {
        _jobBlock(_dataArr[indexPath.row]);
    }
    
    [self returnBtnSenderDown];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_dataArr.count>0) {
        return _dataArr.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44*kDeviceRate;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *iden = @"idenhahahahaha";
    
    JobClassTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:iden];
    if (!cell) {
        
        cell = [[JobClassTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    cell.jobClassLab.text = [[_dataArr objectAtIndex:indexPath.row] objectForKey:@"name"];

    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
