//
//  UGCJobClassViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

typedef void(^UGCJobClassViewControllerBlock)(NSDictionary *selectDic);

@interface UGCJobClassViewController : HT_FatherViewController

@property (nonatomic, copy)UGCJobClassViewControllerBlock jobBlock;

@end
