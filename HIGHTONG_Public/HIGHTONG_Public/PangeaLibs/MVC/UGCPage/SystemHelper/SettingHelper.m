//
//  SettingHelper.m
//  MyLiveFunction
//
//  Created by Kael on 2017/3/8.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import "SettingHelper.h"

@implementation SettingHelper


+(CGFloat)systemVersion{

    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    return [phoneVersion floatValue];
}

+(void)doSomeThingWithActionType:(kSystemActionType)type andActionContent:(NSString *)content{
    
    NSURL *url;
    switch (type) {
        case kSafari:
        {
            url = [NSURL URLWithString:content];
        }
            break;
        case kTelPhone:
        {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",content]];
        }
            case kMessage:
        {

        }
        default:
            break;
    }
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        
        if ([SettingHelper systemVersion]>=10) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
                
            }];
        }else{
            [[UIApplication sharedApplication] openURL:url];

        }
    }
    
}

+(void)openURLSettingWithType:(kSettingType)type andURLString:(NSString *)urlStr{
    
    if ([SettingHelper systemVersion]>=10) {
        //ios10 以后必须使用下面的方法 只打开本应用的所有权限设置开关
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        
        if( [[UIApplication sharedApplication]canOpenURL:url] ) {
            [[UIApplication sharedApplication]openURL:url options:@{}completionHandler:^(BOOL success) {
            
            }];
        }

    }else if (type == kBoundleID){
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"prefs:root=%@",urlStr]];
        
        if( [[UIApplication sharedApplication]canOpenURL:url] ) {
            [[UIApplication sharedApplication]openURL:url];
        }
    }
    else{
        
        NSString *urlStr = [[[SettingHelper getSettinArr] objectAtIndex:(type-2)] objectForKey:@"url"];
        NSURL *url = [NSURL URLWithString:urlStr];
        if( [[UIApplication sharedApplication]canOpenURL:url] ) {
            [[UIApplication sharedApplication]openURL:url];
        }
        
    }
    
    
}

+(NSArray *)getSettinArr{
    NSMutableArray *dataSourceArr = [NSMutableArray new];
    //隐私
    [dataSourceArr addObject:@{
                                @"name":@"隐私",
                                @"url":@"prefs:root=Privacy",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"定位服务",
                                @"url":@"prefs:root=LOCATION_SERVICES",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"日历 邮件 通讯录",
                                @"url":@"prefs:root=ACCOUNT_SETTINGS",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"通讯录",
                                @"url":@"prefs:root=Privacy&path=CONTACTS",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"照片",
                                @"url":@"prefs:root=Privacy&path=PHOTOS",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"相机",
                                @"url":@"prefs:root=Privacy&path=CAMERA",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"健康",
                                @"url":@"prefs:root=Privacy&path=HEALTH",
                                @"argument":@""}];
    
    //的一级界面
    [dataSourceArr addObject:@{@"name":@"飞行模式",
                                @"url":@"prefs:root=AIRPLANE_MODE",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"WIFI",
                                @"url":@"prefs:root=WIFI",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"蜂窝网络",
                                @"url":@"prefs:root=MOBILE_DATA_SETTINGS_ID",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"个人热点",
                                @"url":@"prefs:root=INTERNET_TETHERING",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"蓝牙",
                                @"url":@"prefs:root=Bluetooth",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"通用",
                                @"url":@"prefs:root=General",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"通知中心",
                                @"url":@"prefs:root=NOTIFICATIONS_ID",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"显示与亮度",
                                @"url":@"prefs:root=DISPLAY&BRIGHTNESS",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"壁纸",
                                @"url":@"prefs:root=Wallpaper",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"声音",
                                @"url":@"prefs:root=Sounds",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"存储",
                                @"url":@"prefs:root=STORE",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"备忘录",
                                @"url":@"prefs:root=NOTES",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"Safari",
                                @"url":@"prefs:root=SAFARI",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"音乐🎵",
                                @"url":@"prefs:root=MUSIC",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"照片与相机设置",
                                @"url":@"prefs:root=Photos",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"iCloud",
                                @"url":@"prefs:root=CASTLE",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"FaceTime",
                                @"url":@"prefs:root=FACETIME",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"关于本机",
                                @"url":@"prefs:root=General&path=About",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"软件更新",
                                @"url":@"prefs:root=General&path=SOFTWARE_UPDATE_LINK",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"日期和时间",
                                @"url":@"prefs:root=General&path=DATE_AND_TIME",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"辅助功能",
                                @"url":@"prefs:root=General&path=ACCESSIBILITY",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"键盘⌨️",
                                @"url":@"prefs:root=General&path=Keyboard",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"VPN设置",
                                @"url":@"prefs:root=General&path=VPN",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"自动锁屏",
                                @"url":@"prefs:root=General&path=AUTOLOCK",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"语言与地区",
                                @"url":@"prefs:root=General&path=INTERNATIONAL",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"描述文件",
                                @"url":@"prefs:root=General&path=ManagedConfigurationList",
                                @"argument":@""}];
    [dataSourceArr addObject:@{@"name":@"电话铃声",
                                @"url":@"prefs:root=Sounds&path=Ringtone",
                                @"argument":@""}];

    return [dataSourceArr copy];
}

@end
