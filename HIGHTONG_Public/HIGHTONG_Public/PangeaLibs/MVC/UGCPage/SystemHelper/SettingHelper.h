//
//  SettingHelper.h
//  MyLiveFunction
//
//  Created by Kael on 2017/3/8.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, kSettingType) {
    kBoundleID = 0,//大于等于iOS8 小于iOS10 使用，捞偏门的很好用。但是iOS 10以后被 kSettingsURLString 替代
    kSettingsURLString = 1,//ios8 以后才有的，iOS10以后必须使用这个了👬
    
    //小于iOS10 可使用下面的通用方法
    //--------------------------隐私相关的隐私功能
    kPrivacy = 2,//隐私设置
    kLOCATION_SERVICES = 3,//定位服务
    kACCOUNT_SETTINGS = 4,//日历📅 邮件 📧 通讯录
    kCONTACTS = 5,//通讯录
    kPrivacy_Photos = 6,//照片 相册开关
    kPrivacy_CAMERA = 7,//相机 大写P
    kPrivacy_HEALTH = 8,//健康 大写P

    //----------------------设置功能
    kAIRPLANE_MODE = 9,//飞行模式
    kWIFI = 10,//wifi
    kMOBILE_DATA_SETTINGS_ID = 11,//移动蜂窝网络
    kINTERNET_TETHERING = 12,//个人热点
    kBluetooth = 13,//蓝牙
    kGeneral = 14,//通用
    kNOTIFICATIONS_ID = 15,//通知
    kDISPLAY_BRIGHTNESS = 16,//显示与亮度
    kWallpaper = 17,//壁纸
    kSounds = 18,//声音
    kSTORE = 19,//存储
    kNOTES = 20,//备忘录
    kSAFARI = 21,//浏览器
    kMUSIC = 22,//音乐
    kPhotos = 23,//照片与相机设置
    kCASTLE = 24,//iCloud
    kFACETIME = 25,//FaceTime
    kAbout = 26,//关于本机
    kSOFTWARE_UPDATE_LINK = 27,//软件更新
    kDATE_AND_TIME = 28,//日期和时间
    kACCESSIBILITY = 29,//辅助功能
    kKeyboard = 30,//键盘
    kVPN = 31,//VPN
    kAUTOLOCK = 32,//自动锁屏
    kINTERNATIONAL = 33,//语言与地区
    kManagedConfigurationList = 34,//描述文件
    kRingtone = 35,//电话铃声
    
};

typedef NS_ENUM(NSUInteger, kSystemActionType) {
    
    kSafari,//浏览器 只需要传入完整的链接进来就行 例如： @"https://www.baidu.com",
    kTelPhone,// @"tel:15718808963"
    kMessage,// 发信息需要导入框架 这个就暂不实现了。
};

@interface SettingHelper : NSObject

/**
 判断大的系统版本  iOS7  iOS8 iOS9 iOS10 这样的版本。

 @return 返回一个系统版本号的数值
 */
+(CGFloat)systemVersion;


/**
 代开设置界面设置功能开关

 @param type 设置类型
 @param urlStr 设置所携带参数 主要是boundleID
 */
+(void)openURLSettingWithType:(kSettingType)type andURLString:(NSString *)urlStr;

/**
 openURL 做一些事情；目前只支持打开浏览器和拨打电话。发短信需要导入框架后期支持敬请期待

 @param type openURL 行为类型
 @param content 行为具体内容
 */
+(void)doSomeThingWithActionType:(kSystemActionType)type andActionContent:(NSString *)content;


@end
