//
//  SystemAuthHelper.h
//  MyLiveFunction
//
//  Created by Kael on 2017/3/20.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>//相机
#import <AssetsLibrary/AssetsLibrary.h>//相册
#import <Photos/Photos.h>//相册
#import <CoreLocation/CLLocation.h>//定位
#import <CoreLocation/CLLocationManager.h>
#import <UserNotifications/UserNotifications.h>//iOS10以后的远程推送

//联网权限
#import  <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTCellularData.h>

typedef void (^SystemBlcok)(BOOL);

#define kSystemVersion_After8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define kSystemVersion_After10 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)


@interface SystemAuthHelper : NSObject


+(void)getMicroAuthWithBlock:(void (^)(BOOL grand))callBack;

+(void)getCameraAuthWithBlock:(void (^)(BOOL grand))callBack;


/**
 麦克风权限 验证

 @param statusBlock <#statusBlock description#>
 @return <#return value description#>
 */
+(BOOL)checkMicroPhoneAuthStatus:(void(^)(BOOL isAuth,AVAuthorizationStatus status) )statusBlock;

/**
 相机权限

 @param statusBlock <#statusBlock description#>
 @return <#return value description#>
 */
+(BOOL)checkCameraAuthStatus:(void(^)(BOOL isAuth,AVAuthorizationStatus status) )statusBlock;

/**
 相册权限

 @param statusBlock <#statusBlock description#>
 @return <#return value description#>
 */
+(BOOL)checkPhotosAuthStatus:(void (^) ( BOOL isAuth, ALAuthorizationStatus status))statusBlock;

/**
 位置定位权限

 @param statusBlock <#statusBlock description#>
 @return <#return value description#>
 */
+(BOOL)checkLocationAuthStatus:(void (^) ( BOOL isAuth, CLAuthorizationStatus status))statusBlock;








@end
