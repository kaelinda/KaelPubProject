//
//  SystemAuthHelper.m
//  MyLiveFunction
//
//  Created by Kael on 2017/3/20.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import "SystemAuthHelper.h"

@implementation SystemAuthHelper

+(void)getMicroAuthWithBlock:(void (^)(BOOL))callBack{
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
        callBack(granted);
    
    }];
}

+(void)getCameraAuthWithBlock:(void (^)(BOOL))callBack{
   
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        callBack(granted);
    }];
}

+(BOOL)checkMicroPhoneAuthStatus:(void (^)(BOOL, AVAuthorizationStatus))statusBlock{

    NSString *mediaType = AVMediaTypeAudio;
 
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        //无权限
        NSString *tips = [NSString stringWithFormat:@"请在iPhone的”设置-隐私-相机“选项中，允许%@访问你的手机相机",NSLocalizedString(@"AppName",@"GMChatDemo")];
        NSLog(@"%@",tips);
        
        if (statusBlock) {
            statusBlock(NO,authStatus);
        }
        return NO;
    }else{
        
        if (statusBlock) {
            statusBlock(YES,authStatus);
        }
        return YES;
    }
    
    return NO;
    /*
     //麦克风
     [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
     
     if (granted) {
     
     // 用户同意获取麦克风
     
     } else {
     
     // 用户不同意获取麦克风
     
     }
     statusBlock(granted,0);
     }];
     */
    
}

+(BOOL)checkCameraAuthStatus:(void(^)(BOOL isAuth,AVAuthorizationStatus status) )statusBlock{

    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        //无权限
        NSString *tips = [NSString stringWithFormat:@"请在iPhone的”设置-隐私-相机“选项中，允许%@访问你的手机相机",NSLocalizedString(@"AppName",@"GMChatDemo")];
        NSLog(@"%@",tips);
        
        if (statusBlock) {
            statusBlock(NO,authStatus);
        }
        return NO;
    }else{
        
        if (statusBlock) {
            statusBlock(YES,authStatus);
        }
        return YES;
    }
}

+(BOOL)checkPhotosAuthStatus:(void (^) ( BOOL isAuth, ALAuthorizationStatus status))statusBlock{
    ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
    if (authStatus == ALAuthorizationStatusDenied || authStatus == ALAuthorizationStatusRestricted) {
        // 没有权限
        if (statusBlock) {
            statusBlock( NO, authStatus);
        }
        return NO;
    }else{
        // 已经获取权限
        if (statusBlock) {
            statusBlock(YES,authStatus);
        }
        return YES;
    }
}


+(BOOL)checkLocationAuthStatus:(void (^) ( BOOL isAuth, CLAuthorizationStatus status))statusBlock{
    
    CLAuthorizationStatus authStatus = [CLLocationManager authorizationStatus];
    if (authStatus == kCLAuthorizationStatusDenied || authStatus == kCLAuthorizationStatusRestricted) {
        // 没有权限
        if (statusBlock) {
            statusBlock( NO, authStatus);
        }
        return NO;
    }else{
        // 已经获取权限
        if (statusBlock) {
            statusBlock(YES,authStatus);
        }
        return YES;
    }
}


+(BOOL)checkNotisAuthStatus:(void (^) ( BOOL isAuth, NSInteger status))statusBlock{
   
    BOOL isAuth = NO;

    if (kSystemVersion_After10) {
       
        [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            UNAuthorizationStatus authStatus = settings.authorizationStatus;
            
            if (authStatus == UNAuthorizationStatusNotDetermined || authStatus == UNAuthorizationStatusDenied) {
                statusBlock(NO,authStatus);
            }else{
                statusBlock(YES,authStatus);
            }
        }];
        return NO;
        
    }else if (kSystemVersion_After8) {
        
        //推荐 iOS8之后使用这个方法判断  iOS8之前版本使用上面方法判断（iOS8 及 iOS8以上版本 使用这个方法判断）
        UIUserNotificationType authType = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
        
        if (authType  == UIRemoteNotificationTypeNone) {
            isAuth = NO;
        }else{
            isAuth = YES;
        }
        statusBlock(isAuth,authType);
        return isAuth;
        
    }else{
        
        //ios 8之后就废弃了但没有移除掉 但是 iOS9 之后这判断就失效了（iOS8 之前使用这个方法）
        UIRemoteNotificationType kType = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (kType  == UIRemoteNotificationTypeNone) { //判断用户是否打开通知开关
            isAuth = NO;
        }else{
            isAuth = YES;
        }
        statusBlock(isAuth,kType);
        
    }
    
    return NO;
    
}


-(void)testNetStatus{
    //应用启动后，检测应用中是否有联网权限
    CTCellularData *cellularData = [[CTCellularData alloc]init];
    
    cellularData.cellularDataRestrictionDidUpdateNotifier =  ^(CTCellularDataRestrictedState state){
        
        //获取联网状态
        
        switch (state) {
                
            case kCTCellularDataRestricted:
                
                NSLog(@"Restricrted");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"已为“健康电视”关闭蜂窝移动数据"
                                         
                                                                 message:@"您可以在系统“设置”中为此应用打开蜂窝移动数据。"
                                         
                                                                delegate:nil
                                         
                                                       cancelButtonTitle:@"确定"
                                         
                                                       otherButtonTitles:nil];
                    
                    [alert show];
                    
                });
                
                break;
                
            case kCTCellularDataNotRestricted:
                
                NSLog(@"Not Restricted");                           
                
                break;       
                
            case kCTCellularDataRestrictedStateUnknown:                
                
                NSLog(@"Unknown");                            
                
                break;      
                
            default:           
                
            break;  };};
    
    //查询应用是否有联网功能
//    
//    CTCellularData *cellularData = [[CTCellularData alloc]init];
//    
//    CTCellularDataRestrictedState state = cellularData.restrictedState; 
//    
//    switch (state) {  
//            
//        case kCTCellularDataRestricted:        
//            
//            NSLog(@"Restricrted");            
//            
//            break;  
//            
//        case kCTCellularDataNotRestricted:       
//            
//            NSLog(@"Not Restricted");            
//            
//            break;    
//            
//        case kCTCellularDataRestrictedStateUnknown:       
//            
//            NSLog(@"Unknown");             
//            
//            break; 
//            
//        default:       
//            
//        break;}
}


@end
