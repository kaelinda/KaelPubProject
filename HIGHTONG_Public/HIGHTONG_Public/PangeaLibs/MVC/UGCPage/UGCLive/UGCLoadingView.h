//
//  UGCLoadingView.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, UGCLoadingType) {
    kUGCLoading_none,//隐藏loading视图
    kUGCLoading_offline,//主播已下线
    kUGCLoading_netError,//网络异常
    kUGCLoading_waitting,//等待主播
    kUGCLoading_ending,//结束
    kUGCLoading_activInd,//菊花 无mask
    kUGCLoading_activInd_mask,//有mask的菊花旋转
};

@interface UGCLoadingView : UIView

/**
 背景底图
 */
@property (nonatomic,strong) UIImageView *BGIMGView;

/**
 屏幕中间用来提醒区分的icon视图
 */
@property (nonatomic,strong) UIImageView *centerIcon;


@property (nonatomic,copy) void (^touchActionCallBack)(UGCLoadingType type);

@property (nonatomic,assign) UGCLoadingType loadingType;

@property (nonatomic,strong) UIImage *portraitImage;

@property (nonatomic,strong) UIImage *landscapeImage;

@property (nonatomic,strong) UIActivityIndicatorView *aivLoading;

@property (nonatomic,assign) UIInterfaceOrientation orientation;

@property (nonatomic,strong) UILabel *progressLab;

/**
 状态的文字描述
 */
@property (nonatomic,strong) UILabel *desLabel;

/**
 初始化方法

 @param type loading图片类型
 @param portraitImg 竖屏背景图
 @param landscapImg 横屏背景图
 @return 返回自身
 */
-(instancetype)initWithLoadingType:(UGCLoadingType)type andPortraitImg:(UIImage *)portraitImg andLandscapImg:(UIImage *)landscapImg;

-(void)changeCenterUIWithType:(UGCLoadingType)type;

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;


-(void)setBGIMGWithType:(UIInterfaceOrientation)orientation;

@end
