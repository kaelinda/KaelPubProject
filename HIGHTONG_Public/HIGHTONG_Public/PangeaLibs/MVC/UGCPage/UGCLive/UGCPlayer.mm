//
//  UGCPlayerView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/21.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCPlayer.h"
#include <sys/sysctl.h>
#define ADD_GESTURE

#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height

@interface UGCPlayer ()
{
    NSInteger playerCurrentPosition;
    
    NSInteger playerDurationPositon;
    
    NSString *playerHWDecoder;
}
@property (nonatomic,strong)UIButton *playButton;
@end

@implementation UGCPlayer


-(instancetype)initWithPlayerType:(HTPlayerType)selectPlayertype withFatherViewFrame:(CGRect)fatherFrameView
{
    self = [super init];
    if (self) {
        
        switch (selectPlayertype) {
            case KHTPlayer_AVPlay:
            {
                [self setSelectType:KHTPlayer_AVPlay];
                [self setupAVPlayerWithFatherView:fatherFrameView];
            }
                break;
                
            default:
                break;
        }
        
    }
    
    return self;
}


-(void)setupAVPlayerWithFatherView:(CGRect)viewRect
{
    
    [self configHTPlayerWithViewRect:viewRect];
    
    
    
}

-(void)setFilePath:(NSString *)filePath
{
    _filePath = [filePath copy];
    
}

/**
 *  设置Player相关参数
 */
- (void)configHTPlayerWithViewRect:(CGRect)viewRect
{
    isPlaying = NO;
    beRunning = NO;
        self.userInteractionEnabled = YES;
        
#ifdef _USE_OPENGL_
        self.frameView = [[OpenGLView alloc]initWithFrame:CGRectMake(0, 0, viewRect.size.width, viewRect.size.height)];
        
        self.frameView.contentScaleFactor = [UIScreen mainScreen].scale;
        printf("scale: %f\r\n", [UIScreen mainScreen].scale);
#else
        self.frameView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewRect.size.width, viewRect.size.height)];
        [self.frameView setBackgroundColor:[UIColor blackColor]];
        self.frameView.image = nil;
#endif
        self.frameView.userInteractionEnabled = YES;
        [self.frameView setBackgroundColor:[UIColor blackColor]];
        //    self.frameView.center = self.center;
        [self addSubview:self.frameView];
        
#ifdef ADD_GESTURE
    //图片放大缩小手势
    UIPinchGestureRecognizer *pinGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(changeScale:)];
    [self addGestureRecognizer:pinGesture];
    self.userInteractionEnabled = YES;
    self.multipleTouchEnabled = YES;
    
    //图片拖拉手势
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(changePoint:)];
    [self addGestureRecognizer:panGesture];
#endif
   
       
}


#pragma mark - layoutSubviews

-(void)layoutSubviews
{
    [super layoutSubviews];
    _avPlayerLayer.frame = self.frameView.bounds;
    
    [self layoutIfNeeded];
}

#pragma mark - 设置播放器AVPlayer的KVO监听
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _avPlayer.currentItem) {
        if ([keyPath isEqualToString:@"status"]) {
            if (_avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
                // 加载完成后，再添加平移手势
                // 添加平移手势，用来控制音量、亮度、快进快退
                // 跳到xx秒播放视频
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                
                [dic setObject:[NSNumber numberWithInt:7] forKey:@"statusTag"];
                [self setUIStatus:dic];
                
                if (self.currentTimeStamp) {
                    [self seekToTime:self.currentTimeStamp completionHandler:nil];
                }
                
            } else if (_avPlayer.currentItem.status == AVPlayerItemStatusFailed){
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                
                [dic setObject:[NSNumber numberWithInt:8] forKey:@"statusTag"];
                [self setUIStatus:dic];

            }
            
            
        } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
            // 计算缓冲进度
            NSTimeInterval timeInterval = [self availableDuration];
            CMTime tempDuration             = self.avPlayerItem.duration;
            CGFloat totalDuration       = CMTimeGetSeconds(tempDuration);
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:[NSNumber numberWithInt:6] forKey:@"statusTag"];
            NSMutableArray *arrayBuff = [NSMutableArray array];
            [arrayBuff addObject:[NSString stringWithFormat:@"%.2f",timeInterval/totalDuration]];
            [dic setObject:arrayBuff forKey:@"statusArray"];
            [self setUIStatus:dic];

//            // 如果缓冲和当前slider的差值超过0.1,自动播放，解决弱网情况下不会自动播放问题
//            if (!self.isPauseByUser && !self.didEnterBackground && (self.controlView.progressVODView.progress-self.controlView.videoVODSlider.value > 0.05)) { [self play]; }
            
        } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
            
            // 当缓冲是空的时候
            if (self.avPlayerItem.playbackBufferEmpty) {
//                self.state = HTPlayerStateBuffering;
//                [self bufferingSomeSecond];
                // 计算缓冲进度
                NSTimeInterval timeInterval = [self availableDuration];
                CMTime tempDuration             = self.avPlayerItem.duration;
                CGFloat totalDuration       = CMTimeGetSeconds(tempDuration);
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:[NSNumber numberWithInt:6] forKey:@"statusTag"];
                NSMutableArray *arrayBuff = [NSMutableArray array];
                [arrayBuff addObject:[NSString stringWithFormat:@"%.2f",timeInterval/totalDuration]];
                [dic setObject:arrayBuff forKey:@"statusArray"];
                [self setUIStatus:dic];

            }
            
        } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
            
            // 当缓冲好的时候
            if (self.avPlayerItem.playbackLikelyToKeepUp){
//                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//                [dic setObject:[NSNumber numberWithInt:7] forKey:@"statusTag"];
//                [self setUIStatus:dic];

            }
            
        }
    }
    
 }




/**
 *  计算缓冲进度
 *
 *  @return 缓冲进度
 */
- (NSTimeInterval)availableDuration {
    NSArray *loadedTimeRanges = [[_avPlayer currentItem] loadedTimeRanges];
    CMTimeRange timeRange     = [loadedTimeRanges.firstObject CMTimeRangeValue];// 获取缓冲区域
    float startSeconds        = CMTimeGetSeconds(timeRange.start);
    float durationSeconds     = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result     = startSeconds + durationSeconds;// 计算缓冲总进度
    return result;
}


/**
 *  从xx秒开始播放视频跳转
 *
 *  @param dragedSeconds 视频跳转的秒数
 */
- (void)seekToTime:(NSInteger)dragedSeconds completionHandler:(void (^)(BOOL finished))completionHandler
{
    if (self.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        // seekTime:completionHandler:不能精确定位
        // 如果需要精确定位，可以使用seekToTime:toleranceBefore:toleranceAfter:completionHandler:
        // 转换成CMTime才能给player来控制播放进度
        CMTime dragedCMTime = CMTimeMake(dragedSeconds, 1);
        [self.avPlayer seekToTime:dragedCMTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
            // 视频跳转回调
            if (completionHandler) { completionHandler(finished); }
            // 如果点击了暂停按钮
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            [dic setObject:[NSNumber numberWithInt:7] forKey:@"statusTag"];
            [self setUIStatus:dic];

            if (!self.avPlayerItem.isPlaybackLikelyToKeepUp) {
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                
                [dic setObject:[NSNumber numberWithInt:6] forKey:@"statusTag"];
                [self setUIStatus:dic];

            }
            
        }];
    }
}




-(void)changeInterfaceOrientation:(CGRect)viewRect
{

        [self.frameView setFrame:viewRect];
        _avPlayerLayer.frame = self.frameView.bounds;

}




#ifdef ADD_GESTURE
#pragma mark Gesture
- (void)changeScale:(UIPinchGestureRecognizer *)sender {
    UIView *view = sender.view;
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, sender.scale, sender.scale);
        sender.scale = 1.0;
    }
}

- (void)changePoint:(UIPanGestureRecognizer *)sender {
    UIView *view = sender.view;
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [sender translationInView:view.superview];
        [view setCenter:CGPointMake(view.center.x+translation.x, view.center.y+translation.y)];
        [sender setTranslation:CGPointZero inView:view.superview];
    }
}
#endif

- (void)statusReady:(NSMutableDictionary*)dic
{

    [self doPlay:dic];
}


- (void)doPlay:(NSDictionary *)dictionary
{

        /*
         如果不是从头开始播放这个点播流，在play之前进行seek操作
         */
        if (self.currentTimeStamp > 0){
            
            [self seekToTime:self.currentTimeStamp completionHandler:nil];
            self.currentTimeStamp = 0;
        }
        [_avPlayer play];
        beRunning = YES;

    
}

-(void)setSelectType:(HTPlayerType)selectType
{
    _selectType = selectType;
    
    
    
}






#pragma mark  不同的点击事件的tag

- (void)setUIStatus:(NSMutableDictionary*)dic
{
    NSNumber *state = [NSNumber numberWithInt:[[dic objectForKey:@"statusTag"] intValue]] ;
    NSArray *msgArr = [dic objectForKey:@"statusArray"];
    int status = [state intValue];
        switch (status) {
            case 0:
            {
//                if (_DHPlayerStatusBlock) {
//                    _DHPlayerStatusBlock(DHPlayerStatus_play,msgArr);
//                }
            }
                break;
            case 1:
            {
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_startToPlay,msgArr);
                }
            }
                break;
            case 2:
            {
                isPlaying = NO;
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_pause,msgArr);
                }
            }
                break;
            case 3:
            {
                isPlaying = NO;
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_stop,msgArr);
                }
            }
                break;
            case 4:
            {
                isPlaying = NO;
                
                //不知道是什么东西
//                if (_DHPlayerStatusBlock) {
//                    _DHPlayerStatusBlock(DHPlayerStatus_init,msgArr);
//                }
            }
                break;
            case 5:
            {
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_ready,msgArr);
                }
            }
                break;
            case 6:
            {
                isPlaying = NO;
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_buffering,msgArr);
                }

            }
                break;
            case 7:
            {
                isPlaying = YES;
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_play,msgArr);
                }
            }
                break;
            case 8:
            {
                if (_DHPlayerStatusBlock) {
                    _DHPlayerStatusBlock(DHPlayerStatus_failed,msgArr);
                }
            }
                break;
            case 9:
            {
                if (/* DISABLES CODE */ (YES)) {
                    if (_DHPlayerStatusBlock) {
                        _DHPlayerStatusBlock(DHPlayerStatus_sizeChanged,nil);
                    }
                }else{
                    if (_DHPlayerStatusBlock) {
                        _DHPlayerStatusBlock(DHPlayerStatus_sizeChanged,msgArr);
                    }
                }

            }
                break;
                
            default:
                break;
        }
        

}



/**
 *  根据playerItem，来添加移除观察者
 *
 *  @param playerItem playerItem
 */
- (void)setAvPlayerItem:(AVPlayerItem *)playerItem
{
    _avPlayerItem = playerItem;
//    if (playerItem) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
//        [_avPlayerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
//        // 缓冲区空了，需要等待数据
//        [_avPlayerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
//        // 缓冲区有足够数据可以播放了
//        [_avPlayerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
//        // 添加playerLayer到self.layer
//
//        
//    }
}


- (void)moviePlayDidEnd:(NSNotification *)notification
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *kkDic = [[NSMutableDictionary alloc]init];
    [kkDic setObject:@"1" forKey:@"avPlayerEnd"];
    NSArray *array = [NSArray arrayWithObjects:kkDic, nil];
    [dic setObject:[NSNumber numberWithInt:3] forKey:@"statusTag"];
    [dic setObject:array forKey:@"statusArray"];
    [self performSelectorOnMainThread:@selector(setUIStatus:) withObject:dic waitUntilDone:NO];
}





#pragma mark 转化字符串的方法

/**
 *  根据时长求出字符串
 *
 *  @param time 时长
 *
 *  @return 时长字符串
 */
- (NSString *)durationStringWithTime:(int)time
{

    
    NSInteger NJumphour = (time/3600);
    NSInteger NJumpMinute = (time - (NJumphour*3600)) / 60 ;
    NSInteger NJumpSecond = (time -  NJumphour*3600 - NJumpMinute*60);
    NSString *NTimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",NJumphour,NJumpMinute,NJumpSecond];
    
    return NTimeString;
   
  }



#pragma mark buffer timer
- (void) initBufferingTimer
{
    if (bufferTime) {
        [bufferTime invalidate];
        bufferTime = nil;
    }
    
//    NSLog(@"start BufferingTimer");
    // 这里的6秒可以根据实际客户需要来修改达到自己的要求
    bufferTime = [NSTimer scheduledTimerWithTimeInterval:6.0f
                                                  target:self
                                                selector:@selector(timeFired:)
                                                userInfo:nil
                                                 repeats:NO];
}

#pragma mark stop bufferTimer
- (void) stopBufferingTimer
{
    if (bufferTime) {
        NSLog(@"stop BufferingTimer");
        [bufferTime invalidate];
        bufferTime = nil;
       
    }
}

- (void) timeFired:(NSTimer *) time
{
    bufferTime = nil;
    NSLog(@"已超过10秒没有接收到数据，是否确认主播停止直播");
    if (_DHPlayerStatusBlock) {
        _DHPlayerStatusBlock(DHPlayerStatus_offline,nil);
    }
    // 如果是离开页面，那么请记得整个还要release操作，stop只是简单停止播放，并不会释放资源
//    if (_arcPlayer) {
//        [_arcPlayer stop];
//    }
}



#pragma mark progress




-(void)playerPlayeWithFilePath:(NSString *)filePath
{

        if(nil == filePath || 0 == [filePath length])
        {
            return;
        }
        
            if (!_avPlayer) {
              
                
                // 初始化playerItem
                NSURL *playerItemUrl = [NSURL URLWithString:filePath];
                self.avPlayerItem  = [AVPlayerItem playerItemWithURL:playerItemUrl];
                [_avPlayerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.avPlayerItem];
                [self.avPlayerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
                // 缓冲区空了，需要等待数据
                [self.avPlayerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
                // 缓冲区有足够数据可以播放了
                [self.avPlayerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];

                // 每次都重新创建Player，替换replaceCurrentItemWithPlayerItem:，该方法阻塞线程
                self.avPlayer = [AVPlayer playerWithPlayerItem:self.avPlayerItem];
                
                // 初始化playerLayer
                self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
                
                            _avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                               [self.frameView.layer insertSublayer:self.avPlayerLayer atIndex:0];

                // 强制让系统调用layoutSubviews 两个方法必须同时写
                [self setNeedsLayout]; //是标记 异步刷新 会调但是慢
                [self layoutIfNeeded]; //加上此代码立刻刷新
                // 此处为默认视频填充模式

                
                [self performSelectorOnMainThread:@selector(statusAVReadyToPlay) withObject:nil waitUntilDone:NO];
                
            }
       

}






-(void)progressSeekValue:(CGFloat)seekValue
{
    
 //计算出拖动的当前秒数
        
        if (self.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            // seekTime:completionHandler:不能精确定位
            // 如果需要精确定位，可以使用seekToTime:toleranceBefore:toleranceAfter:completionHandler:
            // 转换成CMTime才能给player来控制播放进度
//            CMTime dragedCMTime = CMTimeMake(seekValue, 1);
//            [self.avPlayer seekToTime:dragedCMTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
//                // 视频跳转回调
//                // 如果点击了暂停按钮
//                if (finished) {
//                    [_avPlayer play];
//
//                }
//           
//                
//            }];
            
            
            @try {
                CMTime dragedCMTime = CMTimeMake(seekValue, 1);

                [self.avPlayer seekToTime:dragedCMTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
                    if (finished) {
                        [self.avPlayer play];
                    }
                }];
            } @catch (NSException *exception) {
                [self.avPlayer play];
            }

            
        }

    
    
    
   }




-(void)playAction
{
//        // 改为为播放完
//        self.playDidEnd         = NO;
//        self.playerItem         = nil;
//        self.didEnterBackground = NO;
//        // 视频跳转秒数置0
//        self.seekTime           = 0;
//        
//        //    [self interfaceOrientation:UIInterfaceOrientationPortrait];
//        // 移除通知
//        [[NSNotificationCenter defaultCenter] removeObserver:self];
        // 关闭定时器
//        [self.playTimer invalidate];
//        // 暂停
//        self.playTimer = nil;
////        if (_avPlayer) {
////            
////            [_avPlayer pause];
////        }
//        // 移除原来的layer
//        [self.avPlayerLayer removeFromSuperlayer];
//        // 替换PlayerItem为nil
//        [self.avPlayer replaceCurrentItemWithPlayerItem:nil];
//        // 把player置为nil
//        self.avPlayer = nil;
//
//        beRunning= NO;
        
        [self playerPlayeWithFilePath:_filePath];

}

-(void)play{
    
    if (_selectType == KHTPlayer_AVPlay) {
        [_avPlayer play];
    }
}



-(void)pause{
   
    
    if (_selectType == KHTPlayer_AVPlay) {
        [_avPlayer pause];
    }

}




-(void)statusAVReadyToPlay
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:[NSNumber numberWithInt:1] forKey:@"statusTag"];
    [self setUIStatus:dic];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:_avPlayerItem];

       [self doPlay:dic];
}


#pragma mark 播放器的停止事件

-(void)stop
{

        return;
  
}


-(void)playerClear
{
    

        if (_avPlayer) {
            
            [_avPlayer pause];
                    // 移除原来的layer
                [self.avPlayerLayer removeFromSuperlayer];
                    // 替换PlayerItem为nil
                [_avPlayer.currentItem cancelPendingSeeks];
                [_avPlayer.currentItem.asset cancelLoading];
                [_avPlayerLayer removeFromSuperlayer];
                    [self.avPlayer replaceCurrentItemWithPlayerItem:nil];
            if (_avPlayerItem) {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_avPlayerItem];
                [_avPlayerItem removeObserver:self forKeyPath:@"status"];
                [_avPlayerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
                [_avPlayerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
                [_avPlayerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
            }
            _avPlayer = nil;

        }
       

//    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

#pragma mark - 计时器事件
/**
 *  计时器事件
 */
- (void)playerTimerAction
{
    if (_avPlayerItem.duration.timescale != 0) {
//        self.controlView.videoVODSlider.value     = (CMTimeGetSeconds([_avPlayerItem currentTime]) / (_avPlayerItem.duration.value / _avPlayerItem.duration.timescale))*1000;//当前进度
        
//        self.controlView.currentVODTimeLabel.text = [self durationStringWithTime:(int)CMTimeGetSeconds([_player currentTime])];
//        self.controlView.totalVODTimeLabel.text   = [self durationStringWithTime:(int)_playerItem.duration.value / _playerItem.duration.timescale];
        
        
    }
}



-(void)applicationWillResignActive{

        
    if (_avPlayer)    {   [_avPlayer pause];  }
        
}

-(void)applicationDidBecomeActive{
    

        if (_avPlayer)    {   [_avPlayer play];  }

    
}




@end
