//
//  UGCLiveManager.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/15.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCLiveManager.h"

static UGCLiveManager *_manager = nil;


@interface UGCLiveManager (){
    NSDictionary *_cameraPragma;
}

@end

@implementation UGCLiveManager

+(instancetype)sharedUGCLiveManager{
    
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        _manager = [[self alloc] init];

        //appdelegate 里面已经处理过了  所以 就不需要
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraRecorderWillStop) name:@"recorderStop" object:nil];

    });
    
    return _manager;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseOBJ];

    }
    
    return self;
}
-(void)initBaseData{
    frameScale = 1.0;//默认不放缩
    _videoStreamQuality = kPushStream_SD;//默认标清
    _isScreenView = NO;
    _cameraType = CameraPosition_front;

    //开始进行网络监测
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [self startReachability];
    

}

-(void)initBaseOBJ{
    
}

//---
// 接收录制返回的状态值
#pragma mark - callback
-(void)HT_RreceivePublishStateCallBackWith:(id)pInstance andResult:(NSInteger)result andCode:(NSInteger)statusCode andObject:(NSObject *)OC_OBJ{
    
    receivePublishStateCallBack((__bridge void *)(pInstance), result, statusCode, (__bridge void *)(OC_OBJ));

}

//这里用来接收 加载状态
static void receivePublishStateCallBack(void *pInstance, int res, int code, void *pObj)
{
    NSLog(@"receivePublishStateCallBack res:%d  code:%d", res, code);
    UGCLiveManager *manager = (__bridge UGCLiveManager *)pObj;
    switch (res) {
            // 提示用户当前网络抖动，app层可以给用户一些友好提示网络原因导致的推流缓慢
            // 建议：只做提示
        case MPUBLISH_WARN_DELAY:
        {
            //当前网络抖动
            [manager receivStatusCallBackWithStatus:kVideoNet_Shake];
        }
            break;
            
            // 提示用户当前已经连接到服务器端，并开始推流。app层可以更新ui状态
            // 建议：只做提示
        case MPUBLISH_CONNECTED:
        {
            
            //已连接到服务器 准备推流
            [manager receivStatusCallBackWithStatus:kVideoNet_Connected];
        }
            break;
            
            // 提示用户当前已经开始自动重连，这会app可以让用户感知到当前正在尝试恢复网络
            // 建议：只做提示
        case MPUBLISH_ERR_AUTOCONNECTING:
        {
            // 后台已经启动重连网络操作
            [manager receivStatusCallBackWithStatus:kVideoNet_AutoConnected];
        }
            break;
            
            // 这个case包含首次推流打开socket就失败和重连后的打开socket失败。
            // 建议：间隔1秒，重连3次，如果还失败直接MediaStop（有需要可以接着重新推流尝试）
        case MPUBLISH_ERR_CONNECTERROR:
        {
            printf("connect failed, stop recorder\r\n");
            //连接失败，应该停止推流 然后
            [manager receivStatusCallBackWithStatus:kVideoNet_ConnectedErr];
        }
            break;
            
            // 这个case是音视频编码产生失败，恢复只能MediaStop后重新推流才可以
            // 建议：停止推流，重新推
        case MPUBLISH_ERR_ENCODEAUDIO:
        case MPUBLISH_ERR_ENCODEVIDEO:
        {
            //stop 后重新推流
            [manager receivStatusCallBackWithStatus:kVideoNet_EncodeErr];
        }
            break;
            
            // rtmp的socket断开或者不稳定，音视频编码失败
            // 推流过程返回的错误，主要是发送数据失败
            // 建议：友好提示和调用强制重连功能MediaPublisher_ForceReConnect(tempControl->m_pMediaPublisher);   （请记得是主线程调用）
        case MPUBLISH_ERR_SENDDATA:
        case MPUBLISH_ERR_STREAMING_N_TIMEOUT:
        case MPUBLISH_ERR_STREAMING_N_CONNFAIL:
        case MPUBLISH_ERR_STREAMING_N_RECVTIMEOUT:
        case MPUBLISH_ERR_STREAMING_N_RECVFAIL:
        case MPUBLISH_ERR_STREAMING_N_SENDTIMEOUT:
        case MPUBLISH_ERR_STREAMING_N_SENDFAIL:
        {
            printf("MPUBLISH_ERR_SENDDATA force re connect\r\n");
            
            //推流失败 强制重连
            [manager receivStatusCallBackWithStatus:kVideoNet_StreamErr];

        }
            break;
            
        default:
        {
            //未知错误，强制停止推流之后 重新推流
            [manager receivStatusCallBackWithStatus:kVideoNet_Unknown];
        }
            break;
    }
}
#pragma mark - 回调给外部
-(void)receivStatusCallBackWithStatus:(VideoNetStatus)status{
    
    if (_netStatusBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _netStatusBlock(status);

        });
        
    }
    
}

#pragma mark - 检测APP进入前后台
// 切换回前台后，尝试激活重新推流
- (void) appWillEnterForeground
{
    NSLog(@"UGC_进入前台");
    // 是否推流资源
    if (m_pMediaPublisher){
        MediaPublisher_Resume(m_pMediaPublisher);
    }
        
   
}

// 切换进后台后，暂停录制和推流
- (void) appWillEnterBackground
{
    NSLog(@"UGC_进入后台");
    
    // 是否推流资源
    if (m_pMediaPublisher)
    {
        MediaPublisher_Pause(m_pMediaPublisher);
    }
}

// 超出申请时间限制后，停止和释放掉推流和播放资源
- (void) cameraRecorderWillStop
{
    NSLog(@"cameraRecorderWillStop");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self MediaStop];
        [self closeCamera];
    });
}



#pragma mark - 启动摄像头
// 启动系统摄像头（包含授权摄像头和麦克风）
- (void) startCamera
{
    if (!m_preset) {
        //        [self showHudWithLabel:@"你需要先选择分辨率!"];
        return;
    }
//    NSArray *paths = @[[[NSBundle mainBundle] pathForResource:@"cat" ofType:@"zip"], [[NSBundle mainBundle] pathForResource:@"caishen" ofType:@"zip"]];

    //----------摄像头授权
//    NSDictionary *cameraPragma = @{@"FrameRate": @"30",
//                                   @"sessionPreset":m_preset};
    
    NSDictionary *cameraPragma = @{@"FrameRate": @"30",
                                   @"sessionPreset":m_preset,
                                   @"CameraPosition":[NSNumber numberWithBool:0],
                                   @"accessKey":UGC_accessKey,
                                   @"accessSecret":UGC_accessSecret,
                                   @"appKey":UGC_appkey};
    
    int res = [CameraManage openCamera:cameraPragma callbackBlock:^(bool authorize) {
       
        m_videoPermissionsState = authorize;
        // 授权摄像头
        if (!authorize) {
            //这里需要用户给摄像头授权
        }
        
    }];
    
    if (res != 0) {
        NSLog(@"初始化摄像头失败");
    }else{
        NSLog(@"初始化摄像头成功");
    }

    //----------麦克风授权
    [CameraManage getRecordPermissionForAudio:^(bool authorize) {
        m_audioPermissionsState = authorize;
        
        // 授权麦克风
        if (!authorize) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //用户需要授权给麦克风哦
            });
        }
    }];
}

#pragma mark - 关闭摄像头
// 关闭系统摄像头
- (void) closeCamera
{
    [CameraManage closePreview];
    [CameraManage closeCamera];
    
}

#pragma mark - 打开摄像头预览功能
// 打开摄像头预览
- (void) openPreviewWithSuperView:(UIView *)cameraView
{
    [CameraManage openPreview:cameraView];
    _isOpendCamera = YES;//已开启相机


    if (_preView != cameraView) {
        _preView = cameraView;

        // 添加手势
        UITapGestureRecognizer *panRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        [_preView addGestureRecognizer:panRecognizer];
        panRecognizer.numberOfTapsRequired = 1;
        panRecognizer.delegate = self;
    
        // 捏合手势
        UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc]
                                                            initWithTarget:self action:@selector(handlePinch:)];
        pinchGestureRecognizer.delegate = self;
        [_preView addGestureRecognizer:pinchGestureRecognizer];

    }else{
    
    }
}
#pragma mark - 关闭摄像头预览功能
// 关闭摄像头预览
- (void) closePreview
{
    [CameraManage closePreview];
    _isOpendCamera = NO;

}
#pragma mark - 开启&关闭 闪光灯
//开关闪光灯
-(void)switchTorchWithStatusIsClose:(BOOL)isClose andResultBlcok: (void (^)(BOOL isSuccess)) resultBlock{
    
    BOOL ret = NO;
    ret = [CameraManage setTorchState:isClose];
    resultBlock(ret);
    
}

#pragma mark - 切换前后置摄像头
-(void)switchToggleWithStatusIsFront:(BOOL)isFront andResultBlcok: (void (^)(BOOL isOpened)) resultBlock{

    if (isFront) {
        NSLog(@"开启前置摄像头");
        _cameraType = CameraPosition_front;
        if ([CameraManage setCameraState:CameraPosition_front]) {
            MediaPublisher_ResetContent(m_pMediaPublisher);

        }else{
            //提示前置摄像头打不开
            ZSToast(@"开启前置摄像头失败");
            resultBlock(NO);
        }
        
    }else{
        NSLog(@"开启后置摄像头~");
        _cameraType = CameraPosition_back;
        if ([CameraManage setCameraState:CameraPosition_back]) {
            MediaPublisher_ResetContent(m_pMediaPublisher);

        }else{
            //提示后置摄像头打不开
            ZSToast(@"开启后置摄像头失败");
            resultBlock(NO);

        }
    }
    
    if (resultBlock) {
        resultBlock(YES);
    }
    
}
#pragma mark - 手动聚焦 需要一个聚焦点
// 手动聚焦
- (void) singleTapOnView:(UIView *)superView andPoint:(CGPoint)tapPoint
{
    [CameraManage autoFocusAtPoint:tapPoint];
}
-(void)singleTap:(UITapGestureRecognizer *) recognizer{
    
    CGPoint touchPoint;
    if (_preView) {
        touchPoint = [recognizer locationInView:_preView];
        [CameraManage autoFocusAtPoint:touchPoint];
//        [self singleTapOnView:_preView andPoint:touchPoint];
    }

}
#pragma mark - 捏合手势缩放
// 手动缩放
- (void) handlePinch:(UIPinchGestureRecognizer*) recognizer
{
    // min
    if (beginGestureScale * recognizer.scale < 1)
        return;
    
    // max
    if (beginGestureScale * recognizer.scale > 10) {
        return;
    }
    
    frameScale = beginGestureScale * recognizer.scale;
    NSLog(@"handlePinch frameScale %f", frameScale);
    
    [CameraManage pichZoomWithScale:frameScale];
}
//手势的代理方法
- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *) gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
        beginGestureScale = frameScale;
    }
    
    return YES;
}

#pragma mark - 添加&移除 水印
// 添加水印
- (void) addwartermark
{
    [CameraManage addWatermarkWithImageName:@"logo.png" info:CGRectMake(150, 100, 120, 120)];
}

// 移除水印
- (void) removewartermark
{
    [CameraManage addWatermarkWithImageName:@"" info:CGRectZero];
}

#pragma mark - 开始推流
// 开始推流录制
// 建议：初始化和释放都应该放在主线程来处理.
/*推流之前应该检查下权限 全线通过之后在真正开始推流更合理。*/
-(void)startPushMediaStreamWith:(NSString *)streamIP andReturnBlock:(void (^)(NSInteger))returnBlock{
   
    _isScreenView = YES;
    
    //-----------------------------------权限检测
    _pushStreamURL = streamIP;
    //首先验证推流地址是否正确
    if ([streamIP length] == 0 || (![streamIP hasPrefix:@"rtmp"] && ![streamIP hasPrefix:@"http"])) {
        //推流地址有误 请查证后再次尝试
        if (returnBlock) {
            returnBlock(-1);
        }
        return;
    }
    
    //接着验证摄像头和麦克风的权限
    if (!m_videoPermissionsState || !m_audioPermissionsState) {
        //摄像头和麦克风权限失败
        if (returnBlock) {
            returnBlock(-2);
        }        return;
    }
    
    // check network
    if (_status == AFNetworkReachabilityStatusUnknown || _status == AFNetworkReachabilityStatusNotReachable) {
        
        //提示检查网络
        if (returnBlock) {
            returnBlock(-9);
        }        return;
    }
    //--------------------------检测完毕
    
    //准备推流
    //流量
    if (_status == AFNetworkReachabilityStatusReachableViaWWAN) {
        //当前是流量 我该怎么办 返回-10状态值 让控制器自己去处理业务
        if (returnBlock) {
            returnBlock(-10);
        }        return;
        
    }else {
 
        //走到这里说明 各项权限都已经走通 开始推流
        BOOL res = [self checkStartMediaRecorder];
        if (res) {
            NSLog(@"推流成功了");
            if (returnBlock) {
                returnBlock(0);
            }

        }else{
            [self MediaStop];
        }
        
    }
    
}


#pragma mark - 检测是否推流成功
-(BOOL)checkStartMediaRecorder{
    int res = 0;
    int width = 0;
    int height = 0;
    
    if (m_preset) {
        CGSize size = [CameraManage updateSizeWithPreset:m_preset];
        width = size.width * UGC_videoRate;
        height = size.height * UGC_videoRate;
    }
    
    char* cfgFilePath = "/data/local/tmp/ArcPlugin.ini";
    if (NULL == m_pMediaPublisher)
    {
        res = MediaPublisher_CreateInstance(&m_pMediaPublisher,cfgFilePath);
    }
    else
    {
        NSLog(@"ReClick Recorder");
        return NO;
    }
    if (res != 0) {
        //建议重试
        return NO;
    }
    
    // 排查rtmp的错误时候，打开一下, 发布版本不要打开
    
//        MediaPublisher_SetTracePath(m_pMediaPublisher, [filePath UTF8String]);
    CLIPINFO info = {0};
    info.bHasAudio = true;
    info.bHasVideo = true;
    info.lBitrate = 0;
    info.lFileType = 0;
    info.lHeight = height;
    info.lWidth = width;
    
    AUDIOINFO m_audioInfo = {0};
    m_audioInfo.lAudioType = AUDIO_CODEC_TYPE_AAC;//MV2_CODEC_TYPE_G711A;
    m_audioInfo.lChannel = 1;
    m_audioInfo.lBitsPerSample = 16;
    m_audioInfo.lSamplingRate = 44100;
    m_audioInfo.lBitrate = 64000;
    m_audioInfo.lBlockAlign = m_audioInfo.lChannel * (m_audioInfo.lBitsPerSample/8);
    
    VIDEOINFO videoInfo = {0};
    videoInfo.lVideoType = VIDEO_CODEC_TYPE_H264;
    videoInfo.lPicWidth = width;
    videoInfo.lPicHeight = height;
    videoInfo.fFPS = 30;
    videoInfo.lBitrate = width*height*UGC_Bitrate; // width*height*1 = 流畅  width*height*2 = 标清 width*height*3 or width*height*4 = 高清
    videoInfo.lRotationDegree = 0;
    
    // 设置rtmp参数
    res = MediaPublisher_SetClipInfo(m_pMediaPublisher,&info);
    
    // 设置音频参数
    res = MediaPublisher_SetAudioInfo(m_pMediaPublisher,&m_audioInfo);
    
    if (MEDIA_ERR_INVALID_PARAM == res)
        // 不支持音频采样率或者比特率，建议参考testbed的输入就好
        return false;
    
    int recodTimeSpan = 100;
    res = MediaPublisher_SetConfig(m_pMediaPublisher,CFG_RECORDER_AUDIO_FRAME_TIMESPAN,&recodTimeSpan);
    
    // 设置视频参数
    res = MediaPublisher_SetVideoInfo(m_pMediaPublisher,&videoInfo);
    // 接收录制返回的状态值（很关键）有些需要反馈给app处理ui和适当重录制
    MediaPublisher_RegisterPublishStateCallback(m_pMediaPublisher, receivePublishStateCallBack, (__bridge void*)self);
    
    const char *expr = [_pushStreamURL UTF8String];
    char *buf = new char[strlen(expr)+1];
    strcpy(buf, expr);
    
    // 启动录制(需要推流地址)
    res = MediaPublisher_Start(m_pMediaPublisher, buf);
    delete buf;
    
    if (res != 0) {
        //        [self showHudWithLabel:@"start rtmp failed!"];
        return false;
    }

    //开始录制以后锁住摄像头方向
    [CameraManage lockOrientation];

    //别忘了更新UI
    
    return true;
}

#pragma mark - 停止推流
//停止推流
-(void)MediaStop{
    // 是否推流资源
    if (NULL != m_pMediaPublisher)
    {
        MediaPublisher_Stop(m_pMediaPublisher);
        MediaPublisher_ReleaseInstance(m_pMediaPublisher);
        m_pMediaPublisher = NULL;
    }
    
    // 去掉方向锁定
    [CameraManage unlockOrientation];
    //记得更新UI
}


-(void)appLocked{
    // 是否推流资源
    if (m_pMediaPublisher)
    {
        MediaPublisher_Pause(m_pMediaPublisher);
    }
    
    if (_preView) {
        [self closePreview];
    }
    
}

-(void)appUnlock{
    if (_preView) {
        [self openPreviewWithSuperView:_preView];//重新开启预览
        //调整摄像头方向
        if (_cameraType == CameraPosition_back) {
            [self switchToggleWithStatusIsFront:NO andResultBlcok:nil];
        }
    }
    
    if (m_pMediaPublisher) {
        MediaPublisher_Resume(m_pMediaPublisher);//继续推流
    }

}

-(void)restartLivefunction{
    
    if (_preView) {
//        [self startCamera];
        [self openPreviewWithSuperView:_preView];
        if (_cameraType == CameraPosition_back) {
            [self switchToggleWithStatusIsFront:NO andResultBlcok:nil];
        }
    }
    
    if (m_pMediaPublisher) {
        MediaPublisher_Resume(m_pMediaPublisher);
    }
    return;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self openPreviewWithSuperView:_preView];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self startPushMediaStreamWith:_pushStreamURL andReturnBlock:nil];
        });

    });
    
    
    

}

#pragma mark ------->> 美颜功能  无美颜不直播

/**
 是否镜像 （主要给前置摄像头设置）
 
 @param state 是否
 @param position 摄像头方向
 */
+(void)enableVideoMirrored:(BOOL)state position:(CameraPosition)position{
    
    return [CameraManage enableVideoMirrored:state position:position];
}

/**
 设置磨皮程度
 
 @param level 磨皮的程度(1-100)
 */
+(void)setFaceSkinSoftenLevel:(unsigned int)level{
    NSLog(@"磨皮程度：%u",level);
    if (level > 100 || level < 1) {
        NSLog(@"请选择合适的磨皮程度");
    }
    return [CameraManage setFaceSkinSoftenLevel:level];
    
}

/**
 设置美白参数
 
 @param level 设置脸部美白的参数(1-100)
 */
+ (void) setFaceBrightLevel:(unsigned int) level{
    NSLog(@"美白参数：%u",level);
    if (level > 100 || level < 1) {
        NSLog(@"请选择合适的美白参数");
    }
    return [CameraManage setFaceBrightLevel:level];
}


/**
 设置曝光度
 
 @param level 曝光的程度(1-100)
 @return 设置结果
 */
+ (BOOL) setExposureLevel:(float) level{
    NSLog(@"曝光度：%f",level);
    if (level > 100 || level < 1) {
        NSLog(@"请选择合适的曝光度");
    }
    return [CameraManage setExposureLevel:level];
}


/**
 打开或者关闭 美颜功能

 @param state 是否开启美颜
 @return return sucess or error code
 */
+ (int) setBeautyState:(BOOL) state{
//    return [CameraManage setBeautyState:state];
    return 2;
}


/**
 打开或者关闭脸部识别功能

 @param state 是否开启脸部识别功能
 @return return sucess or error code
 */
+ (int) setFaceRecognizeState:(BOOL) state{
    
//    return [CameraManage setFaceRecognizeState:state];
    return 2;
}


#pragma mark - 网络监测
- (void) startReachability
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
                
            case AFNetworkReachabilityStatusNotReachable:{
                
                NSLog(@"无网络");
                _status = AFNetworkReachabilityStatusNotReachable;
                
                if (m_pMediaPublisher) {
                    dispatch_async(dispatch_get_main_queue(), ^{

                    });
                }
                break;
            }
                
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                
                NSLog(@"WiFi网络");
                _status = AFNetworkReachabilityStatusReachableViaWiFi;
                break;
            }
                
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                
                NSLog(@"移动网络");
                _status = AFNetworkReachabilityStatusReachableViaWWAN;
                
                if (m_pMediaPublisher) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self realWithNetworkChange];
                    });
                }
                break;
            }
                
            default:
                break;
                
        }
        
    }];
}

-(void)retryPush{
    [self MediaStop];
    //走到这里说明 各项权限都已经走通 开始推流
    BOOL res = [self checkStartMediaRecorder];
    if (res) {
        NSLog(@"推流成功了");
    }else{
        [self MediaStop];
    }

}

#pragma mark - 网络变化
- (void) realWithNetworkChange
{
    [self receivStatusCallBackWithStatus:kVideoNet_4G];
    
}

-(void)setKpreset:(NSString *)kpreset{

    _kpreset = [kpreset copy];
    m_preset = [kpreset copy];
}

-(void)cheackUserAuthWith:( void (^)(BOOL,UserAuthType))resultBlock{


    BOOL isCamera = NO;

    //检测相机权限📷
   isCamera = [SystemAuthHelper checkCameraAuthStatus:^(BOOL, AVAuthorizationStatus) {
        
    }];
    if (!isCamera) {
        resultBlock(NO,kUserAuth_Camera);
        return;
    }
    
    //检测麦克风权限🎤
//    isMicroPhone = [CameraManage getrecord];
}

-(void)lockOrientation{
    [CameraManage lockOrientation];
}

-(void)unlockOrientation{
    [CameraManage unlockOrientation];
}

+(NSArray *)getSupportPresetsWithPosition:(CameraPosition)state{
    return [CameraManage getSupportPresetsWithPosition:state];
}







@end
