//
//  UGCRecorderView.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCRecorderView.h"


@interface UGCRecorderView(){

    CGFloat _space;
    CGFloat _bottm;
    CGSize _btnSize;
    CGFloat _top;
    CGFloat _margin;

}

@end

@implementation UGCRecorderView

#pragma mark - testAction
-(void)test{
    
    _scroLabel.hidden = NO;
    [_scroLabel beginScrolling];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [_scroLabel endScrolling];
        _scroLabel.hidden = YES;
    });

}

#pragma mark - init
-(instancetype)initWithType:(NSString *)type{
    self = [super init];
    if (self) {
        //do something
//        [self initBaseView];
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initBaseData];
        
        [self initBaseView];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self initManager];
        });
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {

    _margin = 12;//左右边距
    _space = 36;//按钮间隙
    _bottm = -10;//底部距离
    _top = 46;//36;//顶部距离
    
    _btnSize = CGSizeMake(34*kRateSize, 34*kRateSize);
    
    _preset = @"AVCaptureSessionPreset1280x720";//1280P
    
    _pushStreamIP = @"";

    _subOrientation = UIInterfaceOrientationPortrait;
    
}
-(void)initManager{

    [[UGCLiveManager sharedUGCLiveManager] setKpreset:_preset];
    [[UGCLiveManager sharedUGCLiveManager] startCamera];
    [[UGCLiveManager sharedUGCLiveManager] openPreviewWithSuperView:_BGView];
    
//    [UGCLiveManager setBeautyState:YES];//开启  关闭美颜
//    [UGCLiveManager setFaceRecognizeState:YES];//脸部识别
//    [UGCLiveManager enableVideoMirrored:NO position:CameraPosition_front];
//    [UGCLiveManager setExposureLevel:60];//曝光度
//    [UGCLiveManager setFaceBrightLevel:90];//美白参数
//    [UGCLiveManager setFaceSkinSoftenLevel:90];//磨皮
    
    [UGCLiveManager sharedUGCLiveManager].netStatusBlock = ^(VideoNetStatus status){
        switch (status) {
            case kVideoNet_4G:{
                ZSToast(@"您正在使用4G网络");
                
            }break;
            case kVideoNet_Shake:{
            
                ZSToast(@"网络抖动~")
            }break;
            case kVideoNet_Connected:{
            
                ZSToast(@"链接成功")
            }break;
            case kVideoNet_StreamErr:{
                ZSToast(@"推流失败，重新连接ing~")
                [self retryPushStream];
                
            }break;
            case kVideoNet_EncodeErr:{
                ZSToast(@"编码错误")
                [self retryPushStream];

            }break;
            case kVideoNet_ConnectedErr:{
            
                ZSToast(@"连接错误")
                [self retryPushStream];

            }break;
            case kVideoNet_AutoConnected:{
                ZSToast(@"自动重连ing~")
            }break;
            case kVideoNet_Unknown:{
                ZSToast(@"未知错误")
                [self retryPushStream];

            }break;
                
            default:
                break;
        }
    };
}


- (void)initBaseView {
    
    __weak typeof(&*self)weakSelf = self;
    
    //摄像头捕捉画面的图层
    _BGView = [UIView new];
    [_BGView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_BGView];
    
    [_BGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    NSString *scrollTitle = @"..........";
    _scroLabel = [TXScrollLabelView scrollWithTitle:scrollTitle type:TXScrollLabelViewTypeLeftRight velocity:0.4 options:UIViewAnimationOptionCurveEaseInOut];
    _scroLabel.frame = CGRectMake(0, 0, 540, 30);
    _scroLabel.scrollInset = UIEdgeInsetsMake(0, 10 , 0, 10);
    _scroLabel.tx_centerX = 15;
    _scroLabel.scrollSpace = 10;
    _scroLabel.font = [UIFont systemFontOfSize:15];
    _scroLabel.textAlignment = NSTextAlignmentCenter;
    _scroLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.40];
    _scroLabel.hidden = YES;
    [self addSubview:_scroLabel];
    
    [_scroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.mas_top).offset(20);
        make.left.mas_equalTo(weakSelf);
        make.right.mas_equalTo(weakSelf);
        make.height.mas_equalTo(26);
    }];
    

    
    //用户头像视图
    CGSize headerSize = CGSizeMake(123*kRateSize, 32*kRateSize);
    
    _userHeaderView = [[LiverHeaderView alloc] init];
    [_userHeaderView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.40]];
    _userHeaderView.layer.cornerRadius = headerSize.height/2;
    [self addSubview:_userHeaderView];
    
    [_userHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(headerSize);
        make.left.mas_equalTo(weakSelf.mas_left).offset(26);
        make.top.mas_equalTo(weakSelf.mas_top).offset(_top);
    }];
    
    

    //--------------------
    _lampSwitchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_lampSwitchBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_torch_unclick"] forState:UIControlStateNormal];
//    _lampSwitchBtn.enabled = NO;
    [self setLampBtnUIWithStatus:kLamp_unenable];
    [_lampSwitchBtn addTarget:self action:@selector(lampBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_lampSwitchBtn];
    
    [_lampSwitchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(_btnSize);
        make.left.mas_equalTo(weakSelf.mas_left).offset(_btnSize.width + _space*2);
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(_bottm);
    }];
    
    //--------------------
    _cameraSwitchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cameraSwitchBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_face_rotating"] forState:UIControlStateNormal];
    [_cameraSwitchBtn addTarget:self action:@selector(cameraBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_cameraSwitchBtn];
    
    [_cameraSwitchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(_btnSize);
//        make.left.mas_equalTo(weakSelf.mas_left).offset(space);
        make.right.mas_equalTo(weakSelf.lampSwitchBtn.mas_left).offset(-_space);
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(_bottm);
    }];



    //----------------旋转
    _viewRotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_viewRotateBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_screen_landscape"] forState:UIControlStateNormal];
    [self addSubview:_viewRotateBtn];
    [_viewRotateBtn addTarget:self action:@selector(rotateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_viewRotateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(_btnSize);
//        make.left.mas_equalTo(weakSelf.mas_left).offset(btnSize.width*2 + space *3);
        make.left.mas_equalTo(weakSelf.lampSwitchBtn.mas_right).offset(_space);
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-10);
    }];
    
    [self setButtonPropertyWith:_lampSwitchBtn];
    [self setButtonPropertyWith:_cameraSwitchBtn];
    [self setButtonPropertyWith:_viewRotateBtn];
//---------------------
    
    _startLiveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startLiveBtn setImage:[UIImage imageNamed:@"btn_ugc_start_live"] forState:UIControlStateNormal];
    [_startLiveBtn addTarget:self action:@selector(startLiveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_startLiveBtn];
    
    [_startLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(260, 58));
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY).offset(100);
        
    }];
    
    
    CGFloat inset = 17*kRateSize;
    
    _liveTitleView = [[JSTextView alloc] init];
    _liveTitleView.backgroundColor =[UIColor blackColor];
    _liveTitleView.layer.cornerRadius = 28;
    _liveTitleView.alpha = 0.7;
    _liveTitleView.textColor = [UIColor whiteColor];
    _liveTitleView.myPlaceholder = @"给直播写个标题吧,记得要在15字以内哟~";
    _liveTitleView.myPlaceholderColor = [UIColor lightGrayColor];
    _liveTitleView.font = [UIFont systemFontOfSize:20*kRateSize];
    _liveTitleView.returnKeyType = UIReturnKeyDone;
    [_liveTitleView setTextContainerInset:UIEdgeInsetsMake(inset, inset, inset, inset)];
    
    _liveTitleView.editable = YES;
    _liveTitleView.delegate = self;
    _liveTitleView.scrollEnabled = NO;
    _liveTitleView.tintColor = UIColorFromRGB(0x54cffd);
    
    [self addSubview:_liveTitleView];
    
    
    [_liveTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(280*kRateSize, 90*kRateSize));
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
//        make.centerY.mas_equalTo(weakSelf.mas_centerY).offset(-50*kRateSize);
        make.top.mas_equalTo(weakSelf.mas_top).offset(163*kRateSize + 20);
        
    }];
   
    _inputFinishedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_inputFinishedBtn setImage:[UIImage imageNamed:@"btn_ugc_recorder_inputfinished"] forState:UIControlStateNormal];
    [_inputFinishedBtn addTarget:self action:@selector(inputFinishedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_inputFinishedBtn];
    
    [_inputFinishedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(160*kRateSize, 40*kRateSize));
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
//        make.centerY.mas_equalTo(weakSelf.mas_centerY).offset(100);
        make.top.mas_equalTo(weakSelf.liveTitleView.mas_bottom).offset(26);
        
    }];

    [self setHiddenStatusWithIsValidTitle:NO];
    
    
}

-(void)retryPushStream{
    [[UGCLiveManager sharedUGCLiveManager] MediaStop];
    [self pushStream];

}

-(void)refreshUILayoutWithType:(UIInterfaceOrientation)interfaceOrientation{
  
    __weak typeof(&*self)weakSelf = self;

    switch (interfaceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            [_lampSwitchBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(_btnSize);
                make.centerX.mas_equalTo(weakSelf.mas_centerX);
                make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(_bottm);
            }];
            
            [_startLiveBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(260, 58));
                make.centerX.mas_equalTo(weakSelf.mas_centerX);
                make.bottom.mas_equalTo(weakSelf.lampSwitchBtn.mas_top).offset(-58);
                
            }];
        }
            break;
            
        default:{
           
            [_lampSwitchBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(_btnSize);
                make.left.mas_equalTo(weakSelf.mas_left).offset(_btnSize.width + _space*2);
                make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(_bottm);
            }];

            [_startLiveBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(260, 58));
                make.centerX.mas_equalTo(weakSelf.mas_centerX);
                make.bottom.mas_equalTo(weakSelf.lampSwitchBtn.mas_top).offset(-114);
                
            }];
        }
            break;
    }
    
    [self layoutIfNeeded];
}

-(void)setButtonPropertyWith:(UIButton *)btn{
    NSLog(@"这里要记得注释掉");
//    [btn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
//    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btn.layer setCornerRadius:34*kRateSize/2];
}

-(UIButton *)creatButtonWithType:(UIButtonType)type andTitle:(NSString *)title{
    UIButton *btn = [UIButton buttonWithType:type];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return btn;
}

-(UIButton *)creatButtonWithNorImage:(NSString *)normalImage andHightImage:(NSString *)hightImage andTouchUpInsideSelector:(SEL)selector{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (hightImage) {
        [btn setImage:[UIImage imageNamed:hightImage] forState:UIControlStateHighlighted];
    }
    
    [btn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

-(UIView *)creatViewWithColor:(UIColor *)color{
    UIView *view = [UIView new];
    [view setBackgroundColor:color];
    return view;
}


-(void)showNotiWith:(NSString *)notiStr andDelyTime:(NSNumber *)dely{

    if (_scroLabel) {
        __weak typeof(&*self)weakSelf = self;
        //清理下
        _scroLabel.hidden = YES;
        [_scroLabel removeFromSuperview];
        _scroLabel = nil;
        
        //重新创建
        _scroLabel = [TXScrollLabelView scrollWithTitle:notiStr type:TXScrollLabelViewTypeLeftRight velocity:0.4 options:UIViewAnimationOptionCurveEaseInOut];
        _scroLabel.frame = CGRectMake(0, 0, 540, 30);
        _scroLabel.scrollInset = UIEdgeInsetsMake(0, 10 , 0, 10);
        _scroLabel.tx_centerX = 15;
        _scroLabel.scrollSpace = 10;
        _scroLabel.font = [UIFont systemFontOfSize:15];
        _scroLabel.textAlignment = NSTextAlignmentCenter;
        _scroLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.40];

        [self addSubview:_scroLabel];
        
        [_scroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(weakSelf.mas_top).offset(20);
            make.left.mas_equalTo(weakSelf);
            make.right.mas_equalTo(weakSelf);
            make.height.mas_equalTo(26);
        }];
        
        //显示 并开始动画
        _scroLabel.hidden = NO;
        [_scroLabel beginScrolling];
        
        //一定时间后消失
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([dely floatValue] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_scroLabel endScrolling];
            _scroLabel.hidden = YES;
        });
    }
    
}

#pragma mark - ------隐藏状态管理
-(void)setHiddenStatusWithIsValidTitle:(BOOL)valid{
   
    if (valid) {
        
        _userHeaderView.hidden = NO;
        _startLiveBtn.hidden = NO;
        _viewRotateBtn.hidden = NO;
        _lampSwitchBtn.hidden = NO;
        _cameraSwitchBtn.hidden = NO;
        
        _liveTitleView.hidden = YES;
        _inputFinishedBtn.hidden = YES;
        
    }else{
        
        _userHeaderView.hidden = YES;
        _startLiveBtn.hidden = YES;
        _viewRotateBtn.hidden = YES;
        _lampSwitchBtn.hidden = YES;
        _cameraSwitchBtn.hidden = YES;
        
        _liveTitleView.hidden = NO;
        _inputFinishedBtn.hidden = NO;
        
    }

}

#pragma mark - ----按钮响应事件
-(void)inputFinishedBtnAction:(UIButton *)btn{
    //取消第一响应者
    [_liveTitleView resignFirstResponder];
    
    //标题确定后的操作
    [self channelTitleConfirm];
}

//横竖屏按钮
-(void)rotateBtnAction:(UIButton *)btn{
    if (_startLiveBtn.hidden) {
        return;
    }
    
    btn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
    });
    
    _isCanRotate = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _isCanRotate = NO;
    });
    
    if (!btn.selected) {
        //横屏
        [self changeOrientation:UIInterfaceOrientationLandscapeLeft];

        //更换btn图片  和 UI布局
        [btn setImage:[UIImage imageNamed:@"ic_ugc_camera_screen_portrait"] forState:UIControlStateNormal];
        
    }else{
        //竖屏
        [self changeOrientation:UIInterfaceOrientationPortrait];

        //更换btn图片   和  UI布局
        [btn setImage:[UIImage imageNamed:@"ic_ugc_camera_screen_landscape"] forState:UIControlStateNormal];

        
    }
    
    btn.selected = !btn.selected;
    
}

//闪光灯按钮
-(void)lampBtnAction:(UIButton *)btn{
    
    if (!btn.selected) {
        [[UGCLiveManager sharedUGCLiveManager] switchTorchWithStatusIsClose:YES andResultBlcok:^(BOOL isOpened) {
            [self setLampBtnUIWithStatus:kLamp_close];
        }];
    }else{
        [[UGCLiveManager sharedUGCLiveManager] switchTorchWithStatusIsClose:NO andResultBlcok:^(BOOL isOpened) {
            [self setLampBtnUIWithStatus:kLamp_open];
        }];
    }
    btn.selected = !btn.selected;

}

-(void)setLampBtnUIWithStatus:(LampStatusType)status{
    switch (status) {
        case kLamp_unenable:
        {
            [_lampSwitchBtn setEnabled:NO];
            [_lampSwitchBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_torch_unclick"] forState:UIControlStateNormal];
        }break;
            
        case kLamp_open:
        {
            [_lampSwitchBtn setEnabled:YES];
            [_lampSwitchBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_torch_open"] forState:UIControlStateNormal];

        }break;
       
        case kLamp_close:
        {
            [_lampSwitchBtn setEnabled:YES];
            [_lampSwitchBtn setImage:[UIImage imageNamed:@"ic_ugc_camera_torch_close"] forState:UIControlStateNormal];

        }break;
            
        default:
            break;
    }
}

-(void)cameraBtnAction:(UIButton *)btn{

    if (!btn.selected) {
        
        [[UGCLiveManager sharedUGCLiveManager] switchToggleWithStatusIsFront:NO andResultBlcok:^(BOOL isOpened) {
            NSLog(@"开启了后置摄像头");
            //这里需要更换闪光灯按钮UI
            if (isOpened) {
                [self setLampBtnUIWithStatus:kLamp_open];
            }
        }];
    }else{
        [[UGCLiveManager sharedUGCLiveManager] switchTorchWithStatusIsClose:NO andResultBlcok:^(BOOL isSuccess) {
            [[UGCLiveManager sharedUGCLiveManager] switchToggleWithStatusIsFront:YES andResultBlcok:^(BOOL isOpened) {
                NSLog(@"开启了前置摄像头");
                //这里需要更换闪光灯按钮UI
                if (isOpened) {
                    [self setLampBtnUIWithStatus:kLamp_unenable];
                }
                
            }];
        }];
        
        
    }
    btn.selected = !btn.selected;

}
-(void)startLiveBtnAction:(UIButton *)btn{

    //暂且认为成功了
    NSString *WIFIStr = [GETBaseInfo getNetworkType];
    
    if ([WIFIStr hasSuffix:@"G"] && !_isCanUse4G) {
        //如果是非WIFI 提示下
      
        CustomAlertView *alert =  [[CustomAlertView alloc]initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"检测到当前您使用的是非wifi网络可能会\n产生流量费用，您确定要继续直播吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"去直播" withDelegate:self];
        
        alert.tag = 2222;
        [alert show];
        
        return;
    }
    
    [self startAction];
}

//经过判断条件之后的行为
-(void)startAction{
   
    [self pushStreamUpdateUIWithSuccessu:YES];
    
    if (_startLiveAction) {
        
        NSNumber *flag = @1;
        if (UIInterfaceOrientationIsPortrait(_subOrientation)) { flag = @1;  }
        else{  flag = @0;  }
        
        _startLiveAction(_liveTitleView.text,flag);
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
  
    if (alertView.tag == 2222) {
        switch (buttonIndex) {
            case 1:
            {
                //取消
                _isCanUse4G = NO;
                
                
            }
                break;
            case 0:{
                //确定
                _isCanUse4G = YES;
                [self startAction];
                
            }break;
                
            default:
                break;
        }

    }
    
}

-(void)pushStream{
    [[UGCLiveManager sharedUGCLiveManager] startPushMediaStreamWith:_pushStreamIP andReturnBlock:^(NSInteger retTag) {
        /*
         0:推流成功
         -1:推流地址不合规范
         -2:权限不允许 摄像头 麦克风权限
         -9:检查网络
         -10:流量
         */
        switch (retTag) {
            case 0:
            {
                NSLog(@"推流成功");
                [self pushStreamUpdateUIWithSuccessu:YES];
            }
                break;
            case -1:
            {
                NSLog(@"推流地址不合规范");
                [self pushStreamUpdateUIWithSuccessu:NO];
            }
                break;
            case -2:
            {
                NSLog(@"摄像头麦克风 等权限问题");
                [self pushStreamUpdateUIWithSuccessu:NO];

            }
                break;
            case -9:
            {
                NSLog(@"网络状况不好，请检查网络");
                [self pushStreamUpdateUIWithSuccessu:NO];

            }
                break;
            case -10:
            {
//                NSLog(@"当前使用的是流量，是否继续？");
                //走到这里说明 各项权限都已经走通 开始推流
                BOOL res = [[UGCLiveManager sharedUGCLiveManager] checkStartMediaRecorder];
                if (res) {
                    NSLog(@"推流成功了");
                    [self pushStreamUpdateUIWithSuccessu:YES];

                }else{
                    [[UGCLiveManager sharedUGCLiveManager] MediaStop];
                    [self pushStreamUpdateUIWithSuccessu:NO];

                }
                
            }
                break;
                
            default:
                break;
        }
        //异常处理
        if (retTag != 0) {
            _startLiveBtn.selected = NO;
        }
        NSLog(@"%ld",(long)retTag);
    }];

}

-(void)changeOrientation:(UIInterfaceOrientation)orientation{
    _isCanRotate = YES;
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:orientation] forKey:@"orientation"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _isCanRotate = NO;
    });

}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    _subOrientation = toInterfaceOrientation;
    [self refreshUILayoutWithType:toInterfaceOrientation];
}

//推流成功 更新UI
-(void)pushStreamUpdateUIWithSuccessu:(BOOL)isSuccessu{
   
    if (isSuccessu) {
        [_startLiveBtn setHidden:YES];
        [_viewRotateBtn setHidden:YES];
    }else{
        [_startLiveBtn setHidden:NO];
        [_viewRotateBtn setHidden:NO];
    }

}



-(void)refreshHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum{
    [_userHeaderView setLiverHeaderImage:headerImage andName:name andCountNum:countNum];
}


-(void)creatSessionFailed{
    [self pushStreamUpdateUIWithSuccessu:NO];
}

#pragma mark - ------UITextViewDelegate协议中的方法
//将要进入编辑模式
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {return YES;}
//已经进入编辑模式
- (void)textViewDidBeginEditing:(UITextView *)textView {}
//将要结束/退出编辑模式
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {return YES;}
//已经结束/退出编辑模式
- (void)textViewDidEndEditing:(UITextView *)textView {}
//当textView的内容发生改变的时候调用
- (void)textViewDidChange:(UITextView *)textView {
    
    NSString *placeholder = @"给直播写个标题吧,记得要在15字以内哟";
    if ([textView.text isEqualToString:placeholder]) {
        textView.text = @"";
    }
    
    
}
//选中textView 或者输入内容的时候调用
- (void)textViewDidChangeSelection:(UITextView *)textView {}
//从键盘上将要输入到textView 的时候调用
//rangge  光标的位置
//text  将要输入的内容
//返回YES 可以输入到textView中  NO不能
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        
        
        [self channelTitleConfirm];
        
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    return YES;
}

-(void)channelTitleConfirm{
   
    if (_selectedSessionNameBlock) {
        NSString *name = [NSString stringWithFormat:@"%@",_liveTitleView.text];
        if (name.length >0) {
            _selectedSessionNameBlock(name);
            [self setHiddenStatusWithIsValidTitle:YES];
        }else{
            ZSToast(@"频道名不可为空");

        }
    }
    
}

-(void)dealloc{


}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
