//
//  UGCRecorderView.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UGCLiveManager.h"
#import "LiverHeaderView.h"
#import "TXScrollLabelView.h"

#import "CustomAlertView.h"
#import "JSTextView.h"

@protocol UGCRcorderProtocol <NSObject>


@end

typedef NS_ENUM(NSUInteger, LampStatusType) {
    kLamp_unenable,
    kLamp_open,
    kLamp_close,
};
typedef NS_ENUM(NSUInteger, RecorderBtnTagType) {
    kRecorderBtnTag_lamp = 1,
    kRecorderBtnTag_camera = 2,
    kRecorderBtnTag_rotate = 3,
    kRecorderBtnTag_startLive = 4,
};

@interface UGCRecorderView : UIView <UITextViewDelegate,CustomAlertViewDelegate>

//------------代理

@property (nonatomic,strong) id<UGCRcorderProtocol>delegate;
@property (nonatomic,copy)  void (^selectedSessionNameBlock)(NSString *name);

@property (nonatomic,copy) void (^ startLiveAction) (NSString *name,NSNumber *flag);

//------------状态标记
@property (nonatomic,assign) UIInterfaceOrientation subOrientation;

@property (nonatomic,assign) BOOL isCanRotate;

//-------------

//-------------信息存储变量
@property (nonatomic,copy) NSString *pushStreamIP;
@property (nonatomic,copy) NSString *preset;

//-------------


@property (nonatomic,strong) UIView *BGView;

@property (nonatomic,strong) LiverHeaderView *userHeaderView;

@property (nonatomic,strong) TXScrollLabelView *scroLabel;

/**
 标题输入视图
 */
@property (nonatomic,strong) JSTextView *liveTitleView;

/**
 前后摄像头转换
 */
@property (nonatomic,strong) UIButton *cameraSwitchBtn;

/**
 闪光灯开关 仅在后置摄像头的时候才可开启
 */
@property (nonatomic,strong) UIButton *lampSwitchBtn;

/**
 屏幕旋转
 */
@property (nonatomic,strong) UIButton *viewRotateBtn;

/**
 开始直播按钮
 */
@property (nonatomic,strong) UIButton *startLiveBtn;

/**
 输入完城按钮
 */
@property (nonatomic,strong) UIButton *inputFinishedBtn;

@property (nonatomic,assign) BOOL isCanUse4G;


/**
 视图控制器将要旋转  自动旋转屏幕的时候使用

 @param toInterfaceOrientation 方向
 @param duration 时间长度
 */
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

-(void)pushStream;

-(void)refreshHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum;

/**
 推流成功、失败 更新UI

 @param isSuccessu 是否推流成功
 */
-(void)pushStreamUpdateUIWithSuccessu:(BOOL)isSuccessu;

/**
 显示跑马灯通知

 @param notiStr 通知消息
 @param dely 显示时间
 */
-(void)showNotiWith:(NSString *)notiStr andDelyTime:(NSNumber *)dely;

@end
