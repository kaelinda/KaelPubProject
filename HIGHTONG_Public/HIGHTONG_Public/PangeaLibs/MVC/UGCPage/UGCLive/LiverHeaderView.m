//
//  LiverHeaderView.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/28.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "LiverHeaderView.h"


@interface LiverHeaderView()
{
    CGFloat _cornerRadius;
    CGFloat _width;
    CGFloat _space;
    CGFloat _margin;
    

}

@end

@implementation LiverHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self initBaseData];
        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
    _width = 32*kRateSize;
    _cornerRadius = 32*kRateSize/2;
    _space = 4;
    _margin = 4;
    
}

- (void)initBaseView {
    self.clipsToBounds = YES;
    __weak typeof(&*self)weakSelf = self;
    
    //头像
    CGSize headerSize = CGSizeMake(_width -_space, _width -_space);
    
    _headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_ugc_live_recorder_icon"]];//ic_ugc_live_recorder_icon
    _headerImageView.layer.cornerRadius = headerSize.width/2;
    [self addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(headerSize);
        make.centerY.mas_equalTo(weakSelf);
        make.left.mas_equalTo(weakSelf.mas_left).offset(_margin/2);
    }];
    
    //名称
    _liverNameLabel = [KaelTool setlabelPropertyWithBackgroundColor:[UIColor clearColor] andNSTextAlignment:NSTextAlignmentLeft andTextColor:[UIColor whiteColor] andFont:[UIFont systemFontOfSize:12*kRateSize]];
    _liverNameLabel.text = @"";
    [self addSubview:_liverNameLabel];
    
    [_liverNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.headerImageView.mas_right).offset(_space);
        make.right.mas_equalTo(weakSelf).offset(-_margin*2);
        make.top.mas_equalTo(weakSelf).offset(_margin);
        make.height.mas_equalTo(16*kRateSize);
        
    }];
    
    
    //热度图
    _fireCountImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_ugc_hot_fire"]];
    
    [self addSubview:_fireCountImageView];
    
    [_fireCountImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(8*kRateSize, 10*kRateSize));
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-_margin);
        make.left.mas_equalTo(weakSelf.headerImageView.mas_right).offset(_space);
    }];
    //热度量
    _countNumLabel = [KaelTool setlabelPropertyWithBackgroundColor:[UIColor clearColor] andNSTextAlignment:NSTextAlignmentLeft andTextColor:[UIColor whiteColor] andFont:[UIFont systemFontOfSize:12*kRateSize]];
    _countNumLabel.text = @"";
    [self addSubview:_countNumLabel];
    
    [_countNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.fireCountImageView.mas_right).offset(_margin);
        make.top.mas_equalTo(weakSelf.fireCountImageView);
        make.right.mas_equalTo(weakSelf.mas_right).offset(2*_space);
        make.bottom.mas_equalTo(weakSelf.bottom).offset(-_margin);
    }];
    
}


-(void)setLiverHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum{

    if (headerImage) {
        _headerImageView.image = headerImage;
    }
    if (NotEmptyStringAndNilAndNull(name)) {
        _liverNameLabel.text = name;
    }
    if (countNum) {
        
        if ([countNum integerValue] >= 10000) {
            _countNumLabel.text = [NSString stringWithFormat:@"%f万",[countNum floatValue]/10000.0];
        }else{
            _countNumLabel.text =  [countNum stringValue];
//            if ([countNum integerValue] == 0) {
//                _countNumLabel.text = @"";
//            }
        }
    }

}

-(void)setHidden:(BOOL)hidden{
    [super setHidden:hidden];
   
    if (hidden) {
        
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(0, 32*kRateSize));
        }];
        
        [UIView animateWithDuration:kHeaderAnimationTime animations:^{
        
            self.alpha = 0;
            
        }];
        
    }else{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(123*kRateSize, 32*kRateSize));
        }];
        
        [UIView animateWithDuration:kHeaderAnimationTime animations:^{
            self.alpha = 1;
           
           
        }];
    }
    // tell constraints they need updating
    [self setNeedsUpdateConstraints];
    
    // update constraints now so we can animate the change
    [self updateConstraintsIfNeeded];
    
    [UIView animateWithDuration:kHeaderAnimationTime animations:^{
        [self layoutIfNeeded];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
