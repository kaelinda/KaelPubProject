//
//  UGCPlayerView.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCPlayerView.h"
#import "CustomAlertView.h"

@interface UGCPlayerView()<CustomAlertViewDelegate>
{
    CGFloat _top;
}

@end

@implementation UGCPlayerView

-(instancetype)initWithUIType:(BOOL)isPortrait{
    self = [super init];
    if (self) {
        
        _isPortrait = isPortrait;
        //初始化 数据
        [self initBaseData];
        //初始化 基本视图
        [self initBaseView];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame andIsPortrait:(BOOL)isPortrait{
    self = [super initWithFrame:frame];
    if (self) {
        _isPortrait = isPortrait;
        _kSize = CGSizeMake(frame.size.width<frame.size.height ? frame.size.width : frame.size.height, frame.size.height > frame.size.width ? frame.size.height:frame.size.width);
        //初始化 数据
        [self initBaseData];
        //初始化 基本视图
        [self initBaseView];

    }
    return self;
}


#pragma mark - **************** 初始化操作
- (void)initBaseData {
    _top = 46;//顶部距离

    _playLink = @"http://cdn.tumbo.com.cn/follomeFront//file/library/library_moment_video/2016/11/02/20161102171719373752095.mp4";
    _playLink = @"";

}

- (void)initBaseView {
    [self setBackgroundColor:[UIColor greenColor]];
    __weak typeof(&*self)weakSelf = self;
    
//    //背景视图 用来提示
//    _bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1.png"]];
//    [_bgImageView setBackgroundColor:[UIColor clearColor]];
//    [_bgImageView setContentMode:UIViewContentModeScaleToFill];
//
//    [self addSubview:_bgImageView];
//    [self bringSubviewToFront:_bgImageView];
//    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
    

    //播放器视图
    _player = [[UGCPlayer alloc] initWithPlayerType:KHTPlayer_ARCPlay withFatherViewFrame:CGRectMake(0, 0, _kSize.width, _kSize.height)];
    [_player changeInterfaceOrientation:CGRectMake(0, 0, _kSize.width, _kSize.height)];
    [_player setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:_player];
    
    //容器视图
    _contentView = [[UIView alloc] init];
    [_contentView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_contentView];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    //---------------
    _loadingView = [[UGCLoadingView alloc] initWithLoadingType:kUGCLoading_waitting andPortraitImg:nil andLandscapImg:nil];
    [_contentView addSubview:_loadingView];
    
    [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //分享按钮
    _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_shareBtn setImage:[UIImage imageNamed:@"icon_ugc_shard"] forState:UIControlStateNormal];
    [_shareBtn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:_shareBtn];
    
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(80, 40));
        make.right.mas_equalTo(weakSelf.mas_right).offset(-12);
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-12);
    }];
    
   
//    [_contentView bringSubviewToFront:_loadingView];
   
    //头像视图
    CGSize headerSize = CGSizeMake(123*kRateSize, 32*kRateSize);
    
    _userHeaderView = [[LiverHeaderView alloc] init];
    [_userHeaderView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.40]];
    _userHeaderView.layer.cornerRadius = headerSize.height/2;
    [self addSubview:_userHeaderView];
    
    [_userHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(headerSize);
//        make.height.mas_equalTo(headerSize.height);
//        make.width.mas_equalTo(headerSize.width);
        make.left.mas_equalTo(weakSelf.mas_left).offset(26);
        make.top.mas_equalTo(weakSelf.mas_top).offset(_top);
        
    }];
    
    
    _aivLoading = [[UIActivityIndicatorView alloc] init];
    _aivLoading.color = App_selected_color;
    [_aivLoading setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
    _aivLoading.layer.cornerRadius = 10;
    [self addSubview:_aivLoading];
    [_aivLoading mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    //-----------------
    [_aivLoading stopAnimating];
    
    
    //观察播放器状态 单独挪到这里来处理
    [self playerStatusObserv];
    
    if (NotEmptyStringAndNilAndNull(_playLink)) {
        [_player setFilePath:_playLink];
        [_player playAction];

    }
    
}

-(void)shareBtnAction:(UIButton *)btn{
    NSLog(@"我要分享什么玩意儿？");
    btn.selected = !btn.selected;
    __weak typeof(&*self)wself = self;
    if (!_shareView) {
        _shareView = [[ShareViewVertical alloc] init];
        [_shareView setInstalledQQ:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]];
        [_shareView setInstalledWX:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]];
        [self addSubview:_shareView];
        [_shareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(wself);
        }];
        
        //暂停
        [_player pause];
        
        _shareView.shareBlock = ^(ShareType type){
            switch (type) {
                case Wechat:{
                ZSToast(@"微信分享")
                }
                    break;
                case WechatZone:{
                ZSToast(@"朋友圈分享")
                }
                    break;
                case QQ:{
                ZSToast(@"QQ分享")
                }
                    break;
                case QQZone:{
                ZSToast(@"QQ空间分享")
                }
                    break;
                    
                default:
                    break;
            }
        };
        
        _shareView.dismissHandle = ^(BOOL isDismiss){
            if (isDismiss) {
                //隐藏暂停视图 继续播放
                [wself.player play];
            }else{
                //出现分享视图 暂停下
                [wself.player pause];
            }
        };

    }else{
        _shareView.hidden = NO;
    }
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    _kSize = frame.size;
}
-(void)setIsPortrait:(BOOL)isPortrait{
    
    _isPortrait = isPortrait;
    
    if (_isPortrait) {
        [self refreshUIWithOrientation:UIInterfaceOrientationPortrait];
    }else{
        [self refreshUIWithOrientation:UIInterfaceOrientationLandscapeRight];
    }
}

-(void)setPlayLink:(NSString *)playLink{
    _playLink = [playLink copy];
}

-(void)refreshUIWithOrientation:(UIInterfaceOrientation)orientation{
   
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
        {
            [UIView animateWithDuration:0.25 animations:^{
                [_player changeInterfaceOrientation:CGRectMake(0, 0, _kSize.width, _kSize.height)];

            }];
        }
            break;
        case UIInterfaceOrientationLandscapeRight:
        case UIInterfaceOrientationLandscapeLeft:
        {
        //
            [UIView animateWithDuration:0.25 animations:^{

                [_player changeInterfaceOrientation:CGRectMake(0, 0,_kSize.height, _kSize.width)];

            }];
        }
            break;
            
        default:
            break;
    }
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    _orientation = toInterfaceOrientation;
    [self refreshUIWithOrientation:toInterfaceOrientation];
    [_loadingView willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}
-(void)changeOrientation:(UIInterfaceOrientation)orientation{
    _isCanRotate = YES;
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:orientation] forKey:@"orientation"];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _isCanRotate = NO;
    });
}
#pragma mark - 外部控制内部
-(void)refreshHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum{
    [_userHeaderView setLiverHeaderImage:headerImage andName:name andCountNum:countNum];
}

#pragma mark - 播放器状态
-(void)receiveStatus:(NSNotification *)noti{

    
    
}

-(void)playerStatusObserv{
    __weak typeof(&*self)weakSelf = self;
    //                        MUInt32 wParam = [[notiArr objectAtIndex:0] unsignedIntValue];
    //    MLong lParam = [[notiArr objectAtIndex:1] intValue];
    //                        UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
    //                        MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
    //                        MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];
    if (_player) {
        _player.DHPlayerStatusBlock = ^(DHPlayerStatus status, NSArray *statusArr) {
           
            switch (status) {
               
                case DHPlayerStatus_play:{

                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                    NSLog(@"will 开始播放");
                }break;
                case DHPlayerStatus_buffering:{
                    NSMutableArray *notiArr = [NSMutableArray arrayWithArray:statusArr];
                    
                    if (notiArr) {

                        MLong lParam = [[notiArr objectAtIndex:1] intValue];
                        MUInt32   bufferingProgress;
                        bufferingProgress = lParam;
                        
                        NSString *text = [NSString stringWithFormat:@"%d%%", bufferingProgress];
                        if (bufferingProgress<100) {
                            NSLog(@"buffering progress-%@----- %d",text,weakSelf.hadpic);
                            
                            if (weakSelf.hadpic) {
                                [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd];
                                
                            }else{
                                [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];

                            }
                            
                        }else{
                            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];

                        }
                    }
                    
                }break;
                case DHPlayerStatus_sizeChanged:{
                    NSLog(@"尺寸改变，或者 加载出第一帧来了");
                    _hadpic = YES;
                    
                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                    
                }break;
                case DHPlayerStatus_playing:{
                    
                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                    NSLog(@"playing");
                }break;
                case DHPlayerStatus_stop:{
                    NSLog(@"stop掉player");

                    if (weakSelf.hadpic) {
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_offline];
                    }else{
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_waitting];
                    }
                    _hadpic = NO;
                }break;
                case DHPlayerStatus_pause:{
                    NSLog(@"暂停播放器");
                }break;
                case DHPlayerStatus_ready:{
                    NSLog(@"准备播放");
                }break;
                case DHPlayerStatus_failed:{

                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_netError];
                    NSLog(@"播放失败");
                }break;
                case DHPlayerStatus_offline:{
                    if (!statusArr) {
                        NSLog(@"主播已下线");
                        //记得 release掉播放器哦
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_offline];
                    }
                }break;

                case DHPlayerStatus_connecting:{
                    if (weakSelf.hadpic) {
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd];
                        
                    }else{
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];
                        
                    }
                    NSLog(@"connecting连接中");
                }break;

                case DHPlayerStatus_MSG:{
                    NSLog(@"MSG 干吗用的");
                }break;
                case DHPlayerStatus_startToPlay:{
                    NSLog(@"start to play");
                }break;
                    
                default:
                    break;
            }
            
        };
        
        
        _player.DHPlayerErrorStatusBlock = ^(DHPLAYERERRORSTATUS status,int errorStatus,NSString *errorString)
        {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                
                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"当前节目无法观看,是否重新播放?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:weakSelf];
                nameAlert.tag = 12;
                [nameAlert show];
                
                
                //先暂时这样保留
                //                [self playerObserveDHPlayerErrorStatus:status andErrorValueStatus:errorStatus andErrorSting:errorString];
                
            });
            
            
        };        

        
        
        
    }
    
}



-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 12) {
        //取消
        if (buttonIndex == 1) {
            //返回上一界面
            if (_UGCPlayerHanle) {
                _UGCPlayerHanle(kPlayerHandle_goBack);
            }
            return;
        }
        //确定
        if (buttonIndex == 0) {
            //            [alertView hide];
            //            [_player playAction];
            
            //继续播放
            if (_UGCPlayerHanle) {
                _UGCPlayerHanle(kPlayerHandle_continue);
            }
        }
    }
    
    
}





-(void)liverOffline{

    //    [self.loadingView changeCenterUIWithType:kUGCLoading_offline];
//    ZSToast(@"主播已下线~");
    NSLog(@"这个下线通知不准确，没用上");

}

-(void)dealloc{
    [_player playerClear];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
