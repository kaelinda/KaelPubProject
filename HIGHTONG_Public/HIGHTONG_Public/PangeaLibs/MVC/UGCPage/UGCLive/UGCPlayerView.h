//
//  UGCPlayerView.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGCPlayer.h"
#import "LiverHeaderView.h"
#import "UGCLoadingView.h"
#import "ShareViewVertical.h"
#import <UMSocialCore/UMSocialCore.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>


typedef NS_ENUM(NSUInteger, UGCPlayerHanleType) {
    kPlayerHandle_continue,
    kPlayerHandle_goBack,
    kPlayerHandle_error,
};

@interface UGCPlayerView : UIView
{
    CGSize _kSize;
}

/**
 是否是竖屏
 */
@property (nonatomic,assign) BOOL isPortrait;

/**
 是否可以旋转屏幕
 */
@property (nonatomic,assign) BOOL isCanRotate;

/**
 是否已经加载出来了第一帧
 */
@property (nonatomic,assign) BOOL hadpic;

@property (nonatomic,assign) UIInterfaceOrientation orientation;


/**
 播放链接
 */
@property (nonatomic,copy) NSString *playLink;

/*
 背景视图在最底层
 其次是播放器视图
 最上层是浮动的 可控子视图
 */



/**
 背景视图
 */
@property (nonatomic,strong) UIImageView *bgImageView;

/**
 播放器视图
 */
@property (nonatomic,strong) UGCPlayer *player;

/**
 盛放各种子视图
 */
@property (nonatomic,strong) UIView *contentView;

/**
 加载底图
 */
@property (nonatomic,strong) UGCLoadingView *loadingView;

/**
 头像
 */
@property (nonatomic,strong) LiverHeaderView *userHeaderView;

/**
 分享
 */
@property (nonatomic,strong) UIButton *shareBtn;

@property (nonatomic,strong) UIActivityIndicatorView *aivLoading;

@property (nonatomic,strong) ShareViewVertical *shareView;


@property (nonatomic,copy) void (^UGCPlayerHanle)(UGCPlayerHanleType type);

-(instancetype)initWithUIType:(BOOL)isPortrait;

-(instancetype)initWithFrame:(CGRect)frame andIsPortrait:(BOOL)isPortrait;

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

-(void)refreshHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum;


/**
 主播下线
 */
-(void)liverOffline;

@end
