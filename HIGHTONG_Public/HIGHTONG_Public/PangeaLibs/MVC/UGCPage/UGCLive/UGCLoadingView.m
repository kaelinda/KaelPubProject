//
//  UGCLoadingView.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCLoadingView.h"

@interface UGCLoadingView()
{

    NSString *_portraitBGImg;
    NSString *_lanscapBGImg;
    
    NSString *_offline_Img;
    NSString *_waiting_Img;
    NSString *_net_error_Img;
    NSString *_endImg_Img;
    
    NSString *_des_wating;
    NSString *_des_offline;
    NSString *_des_netErr;
    NSString *_des_ending;
    
}

@end

@implementation UGCLoadingView

-(instancetype)initWithLoadingType:(UGCLoadingType)type andPortraitImg:(UIImage *)portraitImg andLandscapImg:(UIImage *)landscapImg{
    self = [super init];
    if (self) {
        
        //初始化数据源
        [self initBaseData];
        //初始化基本视图
        [self initBaseView];
        
        if (portraitImg) {
            _portraitImage = portraitImg;
        }
        if (_lanscapBGImg) {
            _landscapeImage = landscapImg;
        }
        [self changeCenterUIWithType:kUGCLoading_none];
       
        _loadingType = type;
        
        if (type != kUGCLoading_none) {
            [self changeCenterUIWithType:type];
        }
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //初始化数据源
        [self initBaseData];
        //初始化基本视图
        [self initBaseView];
    }
    return self;
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
   
    _portraitBGImg = @"bg_ugc_player_portrait";
    _lanscapBGImg = @"bg_ugc_player_lanscap";
    
    _offline_Img = @"icon_no_line";
    _waiting_Img = @"icon_waite";
    _endImg_Img = @"icon_end";
    _net_error_Img = @"icon_net_error";
    
    _des_ending = @"直播已结束";
    _des_netErr = @"";
    _des_offline = @"主播掉线啦，请稍等片刻";
    _des_wating = @"主播迟到了，正在玩命赶往直播间...";
    
    _loadingType = kUGCLoading_waitting;
    
}

- (void)initBaseView {

    __weak typeof(&*self)weakSelf = self;
    
    //背景视图
    _BGIMGView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:_portraitBGImg]];
    _BGIMGView.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:_BGIMGView];
    
    [_BGIMGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    _centerIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:_waiting_Img]];
    _centerIcon.contentMode = UIViewContentModeCenter;
    [self addSubview:_centerIcon];
    
    [_centerIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.mas_left).offset(0);
        make.right.mas_equalTo(weakSelf.mas_right).offset(0);
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
    _aivLoading = [[UIActivityIndicatorView alloc] init];
    _aivLoading.color = App_selected_color;
    _aivLoading.layer.cornerRadius = 10;
    [_aivLoading setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
    [self addSubview:_aivLoading];
    
    [_aivLoading mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
    _desLabel = [KaelTool setlabelPropertyWithBackgroundColor:[UIColor clearColor] andNSTextAlignment:NSTextAlignmentCenter andTextColor:[UIColor whiteColor] andFont:[UIFont systemFontOfSize:14*kRateSize]];
    _desLabel.text = _des_wating;
    [self addSubview:_desLabel];
    
    [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(80);
        make.top.mas_equalTo(weakSelf.centerIcon.mas_bottom).offset(30);
        make.left.mas_equalTo(weakSelf);
        make.right.mas_equalTo(weakSelf);
    }];
                 
    
    

    
}

-(void)changeCenterUIWithType:(UGCLoadingType)type{

    static NSObject *lock = nil;
    
    if (!lock) {
        lock = [[NSString alloc] init];
    }
    
    @synchronized (lock) {
        if (_loadingType == type && type == kUGCLoading_activInd) {
            return;
        }
        
        _loadingType = type;
        
        //更改中间视图
        switch (type) {
            case kUGCLoading_none:{
                [self setHidden:YES];
            }break;
            case kUGCLoading_waitting:
            {
                _centerIcon.image = [UIImage imageNamed:_waiting_Img];
                _desLabel.text = _des_wating;
            }
                break;
            case kUGCLoading_offline:{
                _centerIcon.image = [UIImage imageNamed:_offline_Img];
                _desLabel.text = _des_offline;
                
            }break;
            case kUGCLoading_ending:{
                _centerIcon.image = [UIImage imageNamed:_endImg_Img];
                _desLabel.text = _des_ending;
                
            }break;
            case kUGCLoading_netError:{
                _centerIcon.image = [UIImage imageNamed:_net_error_Img];
                _desLabel.text = _des_netErr;
                
            }break;
            case kUGCLoading_activInd:{
                [_aivLoading startAnimating];
                _desLabel.text = @"";
                
                
            }break;
            case kUGCLoading_activInd_mask:{
                [_aivLoading startAnimating];
                _desLabel.text = @"";
            }break;
                
            default:
                break;
        }
        
        //特殊处理 除去渐变背景图
        if (_loadingType == kUGCLoading_activInd) {
            [self changeToActivIndType];
            
        }else if(_loadingType == kUGCLoading_activInd_mask){
            [self changeToActivIndMaskType];
        }
        else if(_loadingType != kUGCLoading_none){
            //不是loading菊花  不是隐藏 的情况
            [self changeToCenterIconType];
            
        }
    }
    
    return;
}

-(void)changeToActivIndType{
    //大图显示
    self.hidden = NO;
    //菊花显示 并开启动画
    _aivLoading.hidden = NO;
    [_aivLoading startAnimating];
    
    //中心图标 隐藏；渐变背景图 隐藏；描述label隐藏
    _centerIcon.hidden = YES;
    _BGIMGView.hidden = YES;
    _desLabel.hidden = YES;
}
-(void)changeToActivIndMaskType{
    //大图显示
    self.hidden = NO;
    //菊花显示；开始动画；
    _aivLoading.hidden = NO;
    [_aivLoading startAnimating];
    //图标隐藏；渐变图显示；描述label隐藏；
    _centerIcon.hidden = YES ;
    _BGIMGView.hidden = NO;
    _desLabel.hidden = YES;
}
-(void)changeToCenterIconType{
    //显示大图 这是的前提
    self.hidden = NO;
    //中心图标 显示
    _centerIcon.hidden = NO;
    //渐变背景图显示 描述label 显示
    _BGIMGView.hidden = NO;
    _desLabel.hidden = NO;
    //菊花隐藏 并停止动画
    _aivLoading.hidden = YES;
    [_aivLoading stopAnimating];
    
    [self setBGIMGWithType:_orientation];
}



-(void)setHidden:(BOOL)hidden{
   
    [super setHidden:hidden];
   
    if (_loadingType == kUGCLoading_activInd && hidden == YES) {
        [_aivLoading stopAnimating];
        _aivLoading.hidden = YES;
    }
    if (_loadingType == kUGCLoading_none) {
        [_aivLoading stopAnimating];
        _aivLoading.hidden = YES;
        _centerIcon.hidden = YES;
        _BGIMGView.hidden = YES;
    }
    
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{

    _orientation = toInterfaceOrientation;
    return;
//    if (_loadingType == kUGCLoading_none || (_loadingType == kUGCLoading_activInd))
//    {    return;    }
//
//    
//    [self setBGIMGWithType:toInterfaceOrientation];
}

-(void)setBGIMGWithType:(UIInterfaceOrientation)orientation{
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        _BGIMGView.image = [UIImage imageNamed:_portraitBGImg];
    }else{
        _BGIMGView.image = [UIImage imageNamed:_lanscapBGImg];
    }
}

-(void)changeTransrAnimatonView:(UIView *)anmationView{
    
    //定义个转场动画
    CATransition *animation = [CATransition animation];
    //转场动画持续时间
    animation.duration = 0.4f;
    //计时函数，从头到尾的流畅度？？？
    animation.timingFunction=UIViewAnimationCurveEaseInOut;
    //转场动画类型
    animation.type = kCATransitionReveal;
    //转场动画将去的方向
    animation.subtype = kCATransitionFromBottom;
    animation.removedOnCompletion = YES;
    
    [anmationView.layer addAnimation:animation forKey:@"trans"];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
