//
//  UGCLiveViewController.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

#import "UGCLiveManager.h"

#import "TXScrollLabelView.h"

#import "UGCRecorderView.h"//录制视图⏺直播
#import "UGCPlayerView.h"//播放者视图

#import "NetWorkNotification_shared.h"

#import "CustomAlertView.h"

#define kRefreshTime 5


#define  kTagCloseVC  101
#define  kTagPlayer4G  111
#define  kTagPlayURL4G 112 //获取到URL了 确定下是否需要播放
#define  kTagRecorder4G 122



typedef NS_ENUM(NSUInteger, UGCUserType) {
    kUGCUser_Player,//观看者
    kUGCUser_Recorder,//直播者
};

typedef NS_ENUM(NSUInteger, UGCAuthType) {
    kUGCAuntType_audio = 0,
    kUGCAuthType_video,
};

@interface UGCLiveViewController : HT_FatherViewController <HTRequestDelegate,CustomAlertViewDelegate>
{
    NSTimer *_hotNumTimer;
}

@property (nonatomic,assign) UGCUserType userType;

@property (nonatomic,strong) UGCRecorderView *recorderView;

@property (nonatomic,strong) UGCPlayerView *playerView;

/**
 频道ID 跟用户身份绑定 用户确定 频道ID就不会改变
 */
@property (nonatomic,copy) NSString *channelID;

/**
 会话ID 每次直播 都会重新创建会话sessionID就会改变
 */
@property (nonatomic,copy) NSString *sessionID;

@property (nonatomic,copy) NSString *programID;

/**
 医师姓名
 */
@property (nonatomic,copy) NSString *userName;

/**
 暂未用到
 */
@property (nonatomic,strong) TXScrollLabelView *scrollLabel;

/**
 是否横屏
 */
@property (nonatomic,assign) BOOL isPortrait;

/**
 是否可以横屏
 */
@property (nonatomic,assign) BOOL isCanRotate;

@property (nonatomic,assign) BOOL isCanUse4G;

/**
 直播状态 1 直播中  2 停止
 */
@property (nonatomic,assign) NSInteger liveStatus;

/**
 初始化方法

 @param userType 播放器 还是 推流器 如果是直播播放器记得 isPortrait 属性 默认是竖屏的
 @param channelID 频道ID
 @param userName 用户名
 @return 返回自身对象
 */
-(instancetype)initWithUserType:(UGCUserType)userType andChannelID:(NSString *)channelID andUserName:(NSString *)userName;

/**
 判断有无权限 是否可以开始播放

 @param callBack 回调方法
 */
+(void)canInitRecorderWithCallBack:(void (^)(BOOL graned,UGCAuthType type))callBack;

/**
 远程推送 如果是直播者recoder 跑马灯形式

 @param info 推送info
 */
-(void)showAlertWithInfo:(NSString *)info;

/**
 清理播放器 推流器状态
 */
-(void)cleanStatus;

@end
