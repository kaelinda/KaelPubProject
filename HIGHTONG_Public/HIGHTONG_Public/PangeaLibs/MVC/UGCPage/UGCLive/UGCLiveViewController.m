//
//  UGCLiveViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCLiveViewController.h"

#import "MLMOptionSelectView.h"

@interface UGCLiveViewController ()

@property (nonatomic,strong) MLMOptionSelectView *optionView;

@property (nonatomic,strong) NSMutableArray *optionDataArr;

@end

@implementation UGCLiveViewController
+(void)canInitRecorderWithCallBack:(void (^)(BOOL graned,UGCAuthType type))callBack{
    [SystemAuthHelper getMicroAuthWithBlock:^(BOOL grand) {
        if (grand) {
            
            [SystemAuthHelper getCameraAuthWithBlock:^(BOOL grand) {

                
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callBack(grand,kUGCAuthType_video);
                    });
                    
                return ;
                
            }];
            
        }else{

            callBack(grand,kUGCAuntType_audio);
        }
    }];
}

#pragma mark - ------初始化
-(instancetype)initWithUserType:(UGCUserType)userType andChannelID:(NSString *)channelID andUserName:(NSString *)userName{
    self = [super init];
    if (self) {
        _userType= userType;
        _channelID = channelID;
        
        //默认是竖屏的
        _isPortrait = YES;
        
        _userName = userName;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
    //等截面加载完成之后再弹出键盘⌨️
    if (_recorderView && _recorderView.liveTitleView.hidden==NO) {
        [_recorderView.liveTitleView becomeFirstResponder];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    //清理通知
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NETCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MSG_STOPPLAYER" object:nil];

    //清理定时器
    if (_hotNumTimer) {
        [_hotNumTimer invalidate];
        _hotNumTimer = nil;
    }
    
    //清理视图垃圾
    [self cleanStatus];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.m_viewNaviBar setHidden:YES];
    [self changeOrientation:UIInterfaceOrientationPortrait];
    [self initBaseData];
    [self initBaseView];
    
//    [self initTestSlider];
    
    //可接收 网络状态实时监控的通知
    [NetWorkNotification_shared shardNetworkNotification].canShowToast = YES;
    
   
    NSLog(@"分辨率集合\n前置：%@ \n后置：%@",[UGCLiveManager getSupportPresetsWithPosition:CameraPosition_front],[UGCLiveManager getSupportPresetsWithPosition:CameraPosition_back]);
   
    //观察者只添加一次  所以要在viewdidload里面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netStatusObserv:) name:@"NETCHANGED" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanStatus) name:@"MSG_STOPPLAYER" object:nil];//  推送来临 页面退出  
   
}
#pragma mark - **************** 初始化操作
- (void)initBaseData {
    //定时刷新访问量
    [self refreshHotNum];
    _hotNumTimer = [NSTimer scheduledTimerWithTimeInterval:kRefreshTime target:self selector:@selector(refreshHotNum) userInfo:nil repeats:YES];
    
    //APP进入前后台的观察 只有播放器才需要 推流已经做了处理了
    if (_userType == kUGCUser_Player) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
   
}

- (void)initBaseView {
   
    switch (_userType) {
        case kUGCUser_Player:
        {
            [self initPlayerView];
            //发请求之前先给个loading
            [_playerView.loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];
            [self getLiveURLWithChannelID:_channelID];
        }
            break;
        case kUGCUser_Recorder:
        {
            [self initRecorderView];

        }
            break;
        default:
            break;
    }
   
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"ic_ugc_quit"] forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    
    CGSize closeSize = CGSizeMake(100, 40);
    
    __weak typeof(&*self)weakSelf = self;
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(closeSize);
        make.right.mas_equalTo(weakSelf.view.mas_right).offset(-10);
        make.centerY.mas_equalTo(weakSelf.view.mas_top).offset(64);
    }];
    
    
}

-(void) initTestSlider{

    UISlider *slid1 = [[UISlider alloc] init];
    slid1.tag = 1;
    slid1.value = 0.5;
    slid1.maximumValue = 100;
    slid1.minimumValue = 1;
    [slid1 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slid1];
    
    
    UISlider *slid2 = [[UISlider alloc] init];
    slid2.tag = 2;
    slid2.value = 0.5;
    slid2.maximumValue = 100;
    slid2.minimumValue = 1;
    [slid2 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slid2];
    
    UISlider *slid3 = [[UISlider alloc] init];
    slid3.tag = 3;
    slid3.value = 0.5;
    slid3.maximumValue = 100;
    slid3.minimumValue = 1;
    [slid3 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slid3];
    
    UISlider *slid4 = [[UISlider alloc] init];
    slid4.tag = 4;
    slid4.value = 0.5;
    slid4.maximumValue = 100;
    slid4.minimumValue = 1;
    [slid4 addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slid4];
    
    
    
    
    
    [slid1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(self.view.mas_centerY);
    }];
    
    [slid2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(slid1.mas_bottom).offset(10);
    }];
    
    [slid3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(slid2.mas_bottom).offset(10);
    }];
    
    [slid4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(slid3.mas_bottom).offset(10);
    }];

    
    
}

-(void)sliderAction:(UISlider *)slider{
    switch (slider.tag) {

        case 1:{
            [UGCLiveManager setExposureLevel:slider.value];
        
        }break;
        case 2:{
            [UGCLiveManager setFaceBrightLevel:slider.value];

        }break;
        case 3:{
            [UGCLiveManager setFaceSkinSoftenLevel:slider.value];

        }break;
        case 4:
        {
            [UGCLiveManager setExposureLevel:slider.value];
  
        }
            break;
            
        default:
            break;
    }

}



#pragma mark - ------下面才是真正的代码👇
-(void)initPlayerView{
    __weak typeof(self)weakSelf = self;
    _playerView = [[UGCPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andIsPortrait:_isPortrait];
    [_playerView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:_playerView];
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    
    //还是自动布局好用
    [_playerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [_playerView refreshHeaderImage:nil andName:_userName andCountNum:0];
    _playerView.UGCPlayerHanle = ^(UGCPlayerHanleType type){
        switch (type) {
            case kPlayerHandle_goBack:
            {
                //清理播放状态 返回上界面
                [weakSelf cleanStatus];
            }
                break;
            case kPlayerHandle_continue:{
                //继续播放
                [weakSelf.playerView.player playAction];
                
            }break;
            case kPlayerHandle_error:{
            
            }break;
                
            default:
                break;
        }
    };
}

-(void)initRecorderView{
    _recorderView = [[UGCRecorderView alloc] init];
    [_recorderView setBackgroundColor:[UIColor whiteColor]];

    [self.view addSubview:_recorderView];
    
    [_recorderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    __weak typeof(&*self)weakSelf = self;
    _recorderView.selectedSessionNameBlock = ^(NSString *name){
        NSLog(@"我的直播标题是：%@",name);
    };
    _recorderView.startLiveAction = ^(NSString *name,NSNumber *flag){
        [weakSelf startLiveRequestWithName:name andScreenFlag:flag andChannelID:weakSelf.channelID];
    };
    NSDictionary *doctorDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"AuthenticSuccessedData"];
    NSString *nick_Name = [doctorDic objectForKey:@"name"];
    if (nick_Name.length <1) {
        nick_Name = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    }
    if (NotEmptyStringAndNilAndNull(nick_Name)) {
        [_recorderView refreshHeaderImage:nil andName:nick_Name andCountNum:0];
    }
}

-(void)showAlertWithInfo:(NSString *)info{

    if (_recorderView) {
        [_recorderView showNotiWith:info andDelyTime:@20];
    }

}

#pragma mark - 响应事件
-(void)closeView:(UIButton *)btn{
    
    if (_recorderView) {
        
        CustomAlertView *alert =  [[CustomAlertView alloc] initWithTitle:@"温馨提示" andRemandMessage:@"您确认要结束当前直播吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确认" withDelegate:self];
        alert.tag = kTagCloseVC;
        [alert show];
        return;
    }else{
        _isCanRotate = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self cleanStatus];
        });
    }
    
}
//清理状态
-(void)cleanStatus{
    
    switch (_userType) {
        case kUGCUser_Player:
        {
            //关掉播放器
            [_playerView.player playerClear];

            //返回上一界面
            [self gobackPreVC];
        }
            break;
        case kUGCUser_Recorder:
        {
            //推流状态 摄像头关掉
            [[UGCLiveManager sharedUGCLiveManager] MediaStop];
            [[UGCLiveManager sharedUGCLiveManager] closePreview];
            [[UGCLiveManager sharedUGCLiveManager] closeCamera];
            [UGCLiveManager sharedUGCLiveManager].preView = nil;
            [UGCLiveManager sharedUGCLiveManager].kpreset = nil;

            if (_sessionID.length>0) {
                //请求停掉
                [self stopLiveSessionWithSessionID];
                [self gobackPreVC];

            }else{
                [self gobackPreVC];
            }
           
        }
            break;
        default:
            break;
    }
   
}

-(void)gobackPreVC{
    
    if (_recorderView) {
        _recorderView.isCanRotate = YES;
    }
    
    if (_playerView) {
        _playerView.isCanRotate = YES;
    }

    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self changeOrientation:UIInterfaceOrientationPortrait];
        //这里可以添加延时 给清理状态一定的时间
        [self.navigationController popViewControllerAnimated:NO];
        
    }
}



#pragma mark - - request
//创建会话
-(void)startLiveRequestWithName:(NSString *)name andScreenFlag:(NSNumber *)sFlag andChannelID:(NSString *)channelID{

    HTRequest *krequest = [[HTRequest alloc] initWithDelegate:self];
    [krequest UGCCommonStartLiveWithName:name andScreenFlg:[sFlag stringValue] andChannelId:channelID];
}
//停止会话
-(void)stopLiveSessionWithSessionID{
    if (NotEmptyStringAndNilAndNull(_sessionID)) {
        HTRequest *krequest = [[HTRequest alloc] initWithDelegate:nil];
        [krequest UGCCommonStopLiveWithSessionId:_sessionID];
    }else{
        NSLog(@"sessionID 为空");
    }
   
}
//刷新热度
-(void)refreshHotNum{
    
    NSLog(@"刷新热度");
    
        if (NotEmptyStringAndNilAndNull(_channelID)) {
            HTRequest *krequest = [[HTRequest alloc] initWithDelegate:self];
            [krequest UGCCommonGetChannelCountWithChannelId:_channelID];
        }else{
            NSLog(@"没有频道ID 不能查询频道热度");
        }
    
    return;
    //下面的是点播播放量查询👇
    if (_playerView) {
        if (NotEmptyStringAndNilAndNull(_programID)) {
            HTRequest *krequest = [[HTRequest alloc] initWithDelegate:self];
            [krequest UGCCommonGetProgramPlayCountWithProgramId:_programID];
        }else{
            NSLog(@"没有节目ID 不能查询节目热度");
        }
    }
}

//获取直播URL
-(void)getLiveURLWithChannelID:(NSString *)channelID{
    HTRequest *krequest = [[HTRequest alloc] initWithDelegate:self];
    [krequest UGCCommonGetLiveUrlWithChannelId:channelID];
}

-(void)getProgramURL{
    HTRequest *krequest = [[HTRequest alloc] initWithDelegate:self];
    [krequest UGCCommonGetProgramUrlWithProgramId:_programID];
}

#pragma mark - ------>>请求回调方法
-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"status:%ld\n result:%@\n type:%@",status,result,type);
    
    //-----------------开始直播 创建会话
    if ([type isEqualToString:UGC_COMMON_STARTLIVE]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {
            NSLog(@"我要直播了：%@",result);

            if (_recorderView) {
                
                //推流地址
                NSString *streamIP = [result objectForKey:@"push"];
                _recorderView.pushStreamIP =[streamIP copy];
                //参数赋值 现在还没什么用
                NSString *SID = [result objectForKey:@"sessionId"];
                _sessionID = SID;

//                _liveStatus = [status integerValue];
                
                switch (_liveStatus) {
                    case 0:
                    {
                        NSLog(@"直播未就绪");
                    }
                        break;
                    case 1:{
                        NSLog(@"直播中");
                    
                    }break;
                    case 2:{
                        NSLog(@"停止");
                    }break;
                        
                    default:
                        break;
                }
                //这里应该监控各种状态
                //推流
                [_recorderView pushStream];
                
                
            }
        }else{
            ZSToast(@"直播失败，请重试~");
            if (_recorderView) {
                [_recorderView pushStreamUpdateUIWithSuccessu:NO];
            }
        }
    }
    
    //-----------------停止会话 其实不应该在这个类里面去观察 因为根本不需要关心结果调用了就行
    if ([type isEqualToString:UGC_COMMON_STOPLIVE]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {
        
            if (_recorderView) {
                //这里应该返回上一界面
                [self gobackPreVC];
                
            }
            
        }
    }
    
    //-----------------轮询刷新
    if ([type isEqualToString:UGC_COMMON_GETCHANNELCOUNT]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {
            
            /*
             count = 0;
             ret = 0;
             */
            NSString *countStr = [result objectForKey:@"count"];
            NSString *status = [result objectForKey:@"status"];
            _liveStatus = [status integerValue];
            
            if (_userType == kUGCUser_Player && _playerView) {
                [_playerView refreshHeaderImage:nil andName:_userName andCountNum:[NSNumber numberWithInteger:[countStr integerValue]]];
                
                if ([status integerValue] == 1) {
                    NSLog(@"直播中");                }
                if ([status integerValue] == 2) {
                    NSLog(@"直播停止");
                    [_playerView.player stop];
                    [_playerView.loadingView changeCenterUIWithType:kUGCLoading_ending];
                }
            }
            if (_userType == kUGCUser_Recorder && _recorderView) {
                
                [_recorderView refreshHeaderImage:nil andName:_userName andCountNum:[NSNumber numberWithInteger:[countStr integerValue]]];
            }

            
        }else{
            NSLog(@"轮询刷新失败~");
        }
 
    }

    //点播轮询 UGC_COMMON_GETPROGRAMPLAYCOUNT
    if ([type isEqualToString:UGC_COMMON_GETCHANNELCOUNT]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {

            //点播控件轮询更新UI
            
        }
    }
    
    //-----------------获取直播URL
    if ([type isEqualToString:UGC_COMMON_GETLIVEURL]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {
            
            NSString *type = [result objectForKey:@"type"];
            NSString *status = [NSString stringWithFormat:@"%@",[result objectForKey:@"status"]];
            NSString *screenFlg = [result objectForKey:@"screenFlg"];
            NSString *playLink;
            NSArray *linkList;
            
            _isPortrait = [screenFlg boolValue];
            if ([type integerValue] == 0) {
                //这个界面 只可能是 type = 0   type = 1是一个新的界面
                playLink = [result objectForKey:@"play"];
            }
            if ([type integerValue] == 1) {
                linkList = [result objectForKey:@"list"];
            }

            //如果获取不到播放链接 那就再去获取下
            if (!NotEmptyStringAndNilAndNull(playLink)) {
                
                //界面存在的时候 才去刷新 界面下是以后 就取消
                //这里取巧了  热度刷新的定时器存在 说明 界面存在  应该用别的变量的这里暂且使用这个了
                if (_hotNumTimer) {
                    if (_liveStatus != 2) {
                        //如果已下线
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_playerView.loadingView changeCenterUIWithType:kUGCLoading_ending];
                        });

                    }

                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (NotEmptyStringAndNilAndNull(_channelID)) {
                            [self getLiveURLWithChannelID:_channelID];
                        }
                    });
                }
                return;
            }
            
            //status: 1 直播中  2 停止
            if ([status integerValue] == 2) {
                [_playerView.loadingView changeCenterUIWithType:kUGCLoading_offline];
                ZSToast(@"主播已下线~");//因为主播下线后 播放者可能还在播放呢

            }else{
                //status 不是2的时候 默认是能播放的那就播去吧
                //检查网络
                NSString *nettype = [GETBaseInfo getNetworkType];
                if ([nettype hasSuffix:@"G"]) {
                    CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
                    nameAlert.tag = kTagPlayURL4G;
                    nameAlert.URLStr = [NSURL URLWithString:playLink];
                    [nameAlert show];
                }else{
                    //播放
                    //默认竖屏
                    if ([screenFlg integerValue]==0) {
                        
                        _playerView.isCanRotate = YES;
                        [self changeOrientation:UIInterfaceOrientationLandscapeLeft];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            if (_playerView) {
                                _playerView.isCanRotate = NO;
                            }
                        });
                    }
                    
                    [_playerView.player setFilePath:playLink];
                    [_playerView.player playAction];
                }
            }
            
            
            
           
            
            
        }else{
            NSLog(@"直播链接获取失败~");

            //界面存在的时候 才去刷新 界面下是以后 就取消
            //这里取巧了  热度刷新的定时器存在 说明 界面存在  应该用别的变量的这里暂且使用这个了
            if (_hotNumTimer) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_playerView.loadingView changeCenterUIWithType:kUGCLoading_ending];
                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (NotEmptyStringAndNilAndNull(_channelID)) {
                        [self getLiveURLWithChannelID:_channelID];
                    }
                });
            }
            
        }

    }
    
    //-----------------获取点播URL
    if ([type isEqualToString:UGC_COMMON_GETPROGRAMURL]) {
        NSString *ret = [result objectForKey:@"ret"];
        if (status == 0 && NotEmptyStringAndNilAndNull(ret) && [ret integerValue] ==0) {
            
            
            
        }
        
    }
    

}

#pragma  - mark ------------##########通知
-(void)netStatusObserv:(NSNotification *)noti{
    NSString *notiStr = [noti object];
    
    __weak typeof(&*self)weakSelf = self;
    
    if ([notiStr isEqualToString:@"蜂窝网络"]) {
        //主线程中执行UI操作
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if (_isCanUse4G) {
                return ;
            }
            if (weakSelf.userType == kUGCUser_Player) {
                //先暂停一下  点击继续之后再play一下
                [_playerView.player pause];
                
                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
                nameAlert.tag = 11;
                [nameAlert show];
            }
            
            if (weakSelf.userType == kUGCUser_Recorder) {
                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
                nameAlert.tag = 22;
                [nameAlert show];
                
            }
            
        });
        
    }
    if ([notiStr isEqualToString:@"WIFI网络"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //切换回WIFI我该怎么做。
            
        });
    }
    
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == kTagPlayer4G) {
        //取消
        if (buttonIndex == 1) {     [self cleanStatus];  return; }
        //确定
        if (buttonIndex == 0) {     [_playerView.player play]; return;  }
    }
    
    if (alertView.tag == kTagRecorder4G) {
        //取消
        if (buttonIndex == 1) {    [self cleanStatus];  return; }
        //确定
        if (buttonIndex == 0) {    return;   }
    }
    
    if (alertView.tag == kTagCloseVC) {
        //取消
        if (buttonIndex == 1) {  return; }
        //确定
        if (buttonIndex == 0) {
            _isCanRotate = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self cleanStatus];
            });  return;
        }
    }
    if (alertView.tag == kTagPlayURL4G) {
        //取消
        if (buttonIndex == 1) {
            _isCanRotate = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self cleanStatus];
            });
            return;
        }
        //确定
        if (buttonIndex == 0) {
            NSString *playLink = [NSString stringWithFormat:@"%@",alertView.URLStr];
            //播放
            [_playerView.player setFilePath:playLink];
            [_playerView.player playAction];
        }
        
    }
}


#pragma mark - ------- app 进入前后台

-(void)appEnterBackGround{
    if (_playerView)
    {
        [_playerView.player pause];
        [_playerView.player applicationWillResignActive];
    }
}

-(void)appEnterForeground{
    if (_playerView) {
        [_playerView.player play];
        [_playerView.player applicationDidBecomeActive];
    }
    
    
    return;
    
   
}

#pragma mark - ------- orientation

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (_userType == kUGCUser_Player) {
        [_playerView willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }else{
        [_recorderView willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }
    
    
}

//设置是否允许自动旋转
- (BOOL)shouldAutorotate {
    
    //点击了返回按钮则认为可旋屏 保证返回动画流畅
    //未点击返回按钮的时候 默认 _isCanRotate = NO;
    if (_isCanRotate) {
        return YES;
    }
    
    //旋屏状态决定于各个子视图状态
    //推流时旋屏状态决定于推流视图状态
    //播放时旋屏决定以播放器视图状态

    if (_userType == kUGCUser_Recorder) {
        return _recorderView.isCanRotate;
    }
    if (_userType == kUGCUser_Player) {
        return _playerView.isCanRotate;
    }
    
    return NO;
}

//设置支持的屏幕旋转方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

//设置presentation方式展示的屏幕方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


-(void)dealloc{
   
}
   
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
