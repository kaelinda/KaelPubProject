//
//  UGCLiveManager.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/15.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CameraManage.h"

//#import "AFNetworkReachabilityManager.h"
#include "mv3MediaPublisher_api.h"
#import "SystemAuthHelper.h"
#import "SettingHelper.h"
#import "UGCInfo.h"


//----------------
/** 申请用户权限的种类 */
typedef NS_ENUM(NSUInteger, UserAuthType) {
    kUserAuth_Camera,//相机
    kUserAuth_MicroPhone,//麦克风
    kUserAuth_Photos,//相册
};
/** 推流质量 */
typedef NS_ENUM(NSUInteger, PushStreamQuality) {
    kPushStream_Smooth,//流畅的
    kPushStream_SD,//标清
    kPushStram_HD,//高清
    kPushStream_Ultra_clear,//超清
};

/** 视频网络状况 */
typedef NS_ENUM(NSUInteger, VideoNetStatus) {
    kVideoNet_Shake,//网络抖动
    kVideoNet_Connected,//已连接
    kVideoNet_AutoConnected,//自动连接
    kVideoNet_ConnectedErr,//连接错误
    kVideoNet_EncodeErr,//编码失败
    kVideoNet_StreamErr,//推流失败
    kVideoNet_Unknown,//未知错误
    kVideoNet_4G,//4G状态
};

/** 视频码率最大值为1 */
static CGFloat UGC_videoRate = 0.8f;
/**
 视频比特率   1：流畅；2：标清；2：高清；4：超清；
 */
static NSInteger UGC_Bitrate = 2;

//------------------
#define kTag_OpenPreview       0x500
#define kTag_ClosePreview      0x501
#define kTag_Torch             0x502
#define kTag_Toggle            0x503
#define kTag_Preset            0x504
#define kTag_StartCamera       0x505
#define kTag_CloseCamera       0x506
#define kTag_Recorder          0x507
#define kTag_Stop              0x508
#define kTag_textField         0x509
#define kTag_showError         0x510
#define kTag_addwartermark     0x511
#define kTag_removewartermark  0x512
//------------------

//-------------------
/** 相机状态类型 */
typedef NS_ENUM(NSUInteger, CameraType) {
    kCameraType_selectPreset = 0,
    kCameraType_startCamera,
    kCameraType_startCameraFailed,
    kCameraType_closeCamera,
    kCameraType_recorder,
    kCameraType_stop
};
//------------------
/** 网络状态的回调 */
typedef void(^NetStatusBlock)(VideoNetStatus status);

@interface UGCLiveManager : NSObject<UIGestureRecognizerDelegate>
{
    CGFloat     beginGestureScale;
    CGFloat     frameScale;
    
    //----------------------------------
    NSString            *m_preset;
    
    void                *m_pMediaPublisher;
    BOOL                m_videoPermissionsState;
    BOOL                m_audioPermissionsState;
    
    AFNetworkReachabilityStatus _status;
    
    UIView *preView;//预览视图
    BOOL _isOpendCamera;
    CameraPosition _cameraType;
//    NetStatusBlock _netStatusBlock;
}

/**
 分辨率
 */
@property (nonatomic,copy) NSString *kpreset;

/**
 预览视图
 */
@property (nonatomic,strong) UIView *preView;//预览视图

/**
 手势
 */
@property (nonatomic,strong) UIView *touchView;

/**
 视图容器
 */
@property (nonatomic,strong) UIView *contentView;

/**
 推流地址
 */
@property (nonatomic,strong) NSString *pushStreamURL;

/**
 推流质量
 */
@property (nonatomic,assign) PushStreamQuality videoStreamQuality;

/**
 推流状态
 */
@property (nonatomic,copy) NetStatusBlock netStatusBlock;

/** 当前界面是否是直播界面，退出直播界面需要置为NO。*/
@property (nonatomic,assign) BOOL isScreenView;


/**
 单利获取

 @return 单利对象
 */
+(instancetype)sharedUGCLiveManager;

#pragma mark - **************** 前后台处理
/**
 APP进入后台之后 清理部分内容
 */
- (void) appWillEnterForeground;

/**
 APP进入前台 重启某些活动
 */
- (void) appWillEnterBackground;

#pragma mark - **************** 直播摄像头操作
/**
 主线程 摄像头关闭 清理
 */
- (void) cameraRecorderWillStop;

/**
 开启摄像头
 */
- (void) startCamera;

/**
 关闭摄像头
 */
- (void) closeCamera;

/**
 开启预览

 @param cameraView 预览视频展示视图
 */
- (void) openPreviewWithSuperView:(UIView *)cameraView;

/**
 关闭预览
 */
- (void) closePreview;

/**
 开关闪光灯

 @param isClose 是否关闭
 @param resultBlock 回调方法
 */
-(void)switchTorchWithStatusIsClose:(BOOL)isClose andResultBlcok: (void (^)(BOOL isOpened)) resultBlock;

/**
 切换前后置摄像头

 @param isFront 是否是前置
 @param resultBlock 回调方法
 */
-(void)switchToggleWithStatusIsFront:(BOOL)isFront andResultBlcok: (void (^)(BOOL isOpened)) resultBlock;

/**
 点触 聚焦

 @param superView 视图
 @param tapPoint 点
 */
- (void) singleTapOnView:(UIView *)superView andPoint:(CGPoint)tapPoint;

/**
 放缩聚焦

 @param recognizer 放缩手势
 */
- (void) handlePinch:(UIPinchGestureRecognizer*) recognizer;


/**
 添加水印
 */
- (void) addwartermark;

/**
 移除水银
 */
- (void) removewartermark;

/**
 开始推流(这里面包含权限的检测 相机权限、麦克风权限 推流地址合法性 网络状况检测)

 @param streamIP 推流地址
 */
-(void)startPushMediaStreamWith:(NSString *)streamIP andReturnBlock: (void (^)(NSInteger retTag))returnBlock;

/**
 检测是否推流成功

 @return 推流结果 BOOL
 */
-(BOOL)checkStartMediaRecorder;

/**
 停止推流
 */
-(void)MediaStop;

-(void)appLocked;

-(void)appUnlock;

//-(void)restartLivefunction;

/**
 锁住摄像头方向 🔐住
 */
-(void)lockOrientation;

/**
 解锁摄像头方向 解🔓
 */
-(void)unlockOrientation;

/**
 根据屏幕宽度 返回合适的码率

 @param width 宽度
 @param height 高度
 @return 码率集合
 */
+(NSArray *)getReferenceBitratesWithWidth:(CGFloat)width andHeight:(CGFloat)height;

/**
 获取设备支持的所有码率集合

 @param state 设备摄像头方向 前置 后置
 @return 返回的码率集合
 */
+ (NSArray *) getSupportPresetsWithPosition:(CameraPosition) state;

/**
 是否镜像 （主要给前置摄像头设置）

 @param state 是否
 @param position 摄像头方向
 */
+ (void) enableVideoMirrored:(BOOL) state position:(CameraPosition) position;


#pragma mark - **************** 美颜功能
/**
 设置磨皮程度

 @param level 磨皮的程度(1-100)
 */
+ (void) setFaceSkinSoftenLevel:(unsigned int) level;

/**
 设置美白参数

 @param level 设置脸部美白的参数(1-100)
 */
+ (void) setFaceBrightLevel:(unsigned int) level;

/**
 设置曝光度

 @param level 曝光的程度(1-100)
 @return 设置结果
 */
+ (BOOL) setExposureLevel:(float) level;

/**
 打开或者关闭 美颜功能
 
 @param state 是否开启美颜
 @return return sucess or error code
 */
+ (int) setBeautyState:(BOOL) state;

/**
 打开或者关闭脸部识别功能
 
 @param state 是否开启脸部识别功能
 @return return sucess or error code
 */
+ (int) setFaceRecognizeState:(BOOL) state;



#pragma mark - **************** 即时聊天 && 连麦
/**
 创建即时聊天会话(主播需要调用先创建会话)。sessionid 和 userid 是客户端与业务系统交 互，在创建直播流时候返回的直播会话 id 以及对应的用户 id(类似直播间以及成员)，accesskey 和 secretkey 是客户端与业务系统创建直播频道时候分配的。

 @param sessionID 直播ID 预分配
 @param userid 当前用户ID 预分配
 @param type 访问身份
 @param accessKey 访问钥匙
 @param accsessKey 加密Key
 */
//- (void) createLiveChatWithSessionID:(NSString *)sessionID andUserID:(NSString *)userid andAccessType:(ACCESSTYPE)type andAccesskey:(NSString *)accessKey andSecretKey:(NSString *)accsessKey;

/**
 加入即时聊天会话。(sessionid:创建即时聊天会话返回的连麦 id，userid 是客户端与业务 系统交互，在创建直播流时候返回的对应用户 id, accesskey 和 secretkey 是客户端与业务系 统创建直播频道时候分配的)。

 @param sessionID 直播ID 预分配
 @param userid 当前用户ID 预分配
 @param type 访问身份
 @param accessKey 访问钥匙
 @param accsessKey 加密Key
 */
//- (void) joinLiveChatWithSessionID:(NSString *)sessionID andUserID:(NSString *)userid andAccessType:(ACCESSTYPE)type andAccesskey:(NSString *)accessKey andSecretKey:(NSString *)accsessKey;

/**
 根据用户的 id 踢掉副播(仅限主播)

 @param userid 用户ID
 @return 返回 YES 为成功，非 NO 为失败
 */
- (BOOL) kickoffMemberFromLiveChatWithUserID:(NSString *)userid;

/**
 启动即时聊天(主要是初始化相关资源并且开始音视频数据传输)。
 */
+ (void) startLiveChat;

/**
 停止即时聊天(主播:离开并且关闭会话，释放相关资源 副播:离开会话、释放相关资源)
 */
+ (void) stopLiveChat;

/**
 设置新加入的副播显示区域(rect:相对于录制分辨率的区域，userid:加入直播间的副播身份标
 示 id，type:当前 userid 是主播还是副播的标示)

 @param rect 副主播区域
 @param userid 加入直播间的副播身份标示 id
 @param type 当前 userid 是主播还是副播的标示
 */
//+ (void) setLiveChatPosition:(CGRect) rect userID:(NSString *) userid accessType:(ACCESSTYPE) type;

/**
 获取即时聊天返回的状态值(根据不同状态值来处理，状态值参考 mpublishTypes.h 的 LIVECHATINFOTYPE)

 @param callback 回调
 @param pObj 对象
 */
//+ (void) getLiveChatInfo:(PFNLIVECHATINFOCALLBACK) callback target:(NSObject *) pObj;

/**
 获取即时聊天返回的错误值(根据不同状态值来处理，状态值参考 mpublishTypes.h 的 LIVECHATERRORTYPE)

 @param callback 回调
 @param pObj 对象
 */
//+ (void) getLiveChatError:(PFNLIVECHATERRORCALLBACK) callback target:(NSObject *) pObj;



#pragma mark - **************** 滤镜



#pragma mark - **************** 互动道具

/**
 该接口用来控制互动道具功能的启用和关闭，启用传 true，关闭传 false，具体的互动资源通过 openCamera 的参数设置进行添加，参考 openCamera 接口注释(请注意需要先保证脸部识别功 能打开为前提 setFaceRecognizeState)

 @param state 道具是否可用
 @return YES代表设置成功；NO代表设置失败
 */
+ (BOOL) enableSticker: (BOOL) state;


/**
 该接口用来切换互动道具资源，index 根据 openCamera 传入的 zip 资源包个数决定的。

 @param index 资源包 index [0 – (count – 1)]
 */
+ (void) switchSticker: (int) index;







@end
