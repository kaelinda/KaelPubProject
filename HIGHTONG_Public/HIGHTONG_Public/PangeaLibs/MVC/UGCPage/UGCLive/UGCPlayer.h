//
//  UGCPlayerView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/21.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerHeader.h"
#import "OpenGLView.h"
#import "OpenGLView.h"
#import "ARCPlayer.h"
#include "amcomdef.h"
//#define _USE_OPENGL_
#ifdef _USE_OPENGL_
#import "OpenGLView.h"
#import "UGCInfo.h"
#endif
// SelectPlayerType的播放模式
typedef NS_ENUM(NSInteger, HTPlayerType) {
    KHTPlayer_AVPlay,           // 用AVPlayer进行播放
    KHTPlayer_ARCPlay,     // 用当虹云播放器进行播放
};

typedef NS_ENUM(NSUInteger, DHPlayerStatus) {
    DHPlayerStatus_play,//开始播放
    DHPlayerStatus_startToPlay,//开始播放
    DHPlayerStatus_pause,//暂停
    DHPlayerStatus_stop,//接收到ENGINE_STATUS_STOP状态之后，必须在主线程调用stop接口
    DHPlayerStatus_connecting,//连接中
    DHPlayerStatus_ready,//接收到ENGINE_STATUS_READY状态之后，必须在主线程调用play接口
    DHPlayerStatus_buffering,//缓存中
    DHPlayerStatus_playing,//播放中
    DHPlayerStatus_failed,//播放失败
    DHPlayerStatus_sizeChanged,/*  1）显示第一帧的时候会返回这个状态值  2）视频分辨率变化的时候会返回这个状态值 */
    DHPlayerStatus_offline,//主播下线
    DHPlayerStatus_MSG,//这个是指的预埋的广告位？？？？？
};

typedef NS_ENUM(NSUInteger, DHPLAYERERRORSTATUS) {
    DHPLAYERERRORSTATUS_UNSUPPORTED_SCHEME,//不支持的流媒体协议
    DHPLAYERERRORSTATUS_NETWORK_CONNECTFAIL,//网络连接失败(建立TCP连接失败)
    DHPLAYERERRORSTATUS_STREAM_OPEN,//打开流媒体失败
    DHPLAYERERRORSTATUS_STREAM_SEEK,//Seek流媒体失败
    DHPLAYERERRORSTATUS_DATARECEIVE_TIMEOUT,//数据接收超时(比如: 30秒未收到任何数据)
    DHPLAYERERRORSTATUS_FORMAT_UNSUPPORTED,//文件格式不支持(比如flv)
    DHPLAYERERRORSTATUS_FORMAT_MALFORMED,//播放列表文件格式不对(比如m3u8格式无法识别)
    DHPLAYERERRORSTATUS_DNS_RESOLVE,//DNS解析错误
    DHPLAYERERRORSTATUS_DNS_RESOLVE_TIMEOUT,//DNS解析超时
    DHPLAYERERRORSTATUS_NETWORK_CONNECTIMEOUT,//网络连接超时(在一定长的时间内没有连接成功，也没有收到任何错误)
    DHPLAYERERRORSTATUS_DATARECEIVE_FAIL,//数据接收错误(接收数据过程中收到了服务器报的错误)
    DHPLAYERERRORSTATUS_DATASEND_TIMEOUT,//数据(网络请求)发送超时
    DHPLAYERERRORSTATUS_DATASEND_FAIL,//数据(网络请求)发送失败
    DHPLAYERERRORSTATUS_DATAERROR_HTML,//接收到的数据为html网页，而不是媒体数据，请求被劫持或者服务器返回鉴权页面之类
    DHPLAYERERRORSTATUS__SOURCE_BAD_VALUE,//传入参数不正确
    DHPLAYERERRORSTATUS_BAD_VALUE_DISPLAY_INIT_FAILED,//渲染器创建失败
    DHPLAYERERRORSTATUS_NOAUDIO_VIDEOUNSUPPORT,//媒体数据中没有audio同时video格式不支持
    DHPLAYERERRORSTATUS_NOVIDEO_AUDIOUNSUPPORT,//媒体数据中没有video同时audio格式不支持
    DHPLAYERERRORSTATUS_AVCODEC_UNSUPPORT,//媒体数据中有video和audio，但是Video和audio格式都不支持
    DHPLAYERERRORSTATUS_OPERATION_CANNOTEXECUTE,//当前操作无法执行，比如player初始化失败，app调用play就会通知app该操作无法执行
    DHPLAYERERRORSTATUS_HTTPFAIL,//HTTP服务器返回的错误值(比如100404表示收到了HTTP 404错误)
};


typedef NS_ENUM(NSUInteger, DHPLAYERMESSAGESTATUS) {
    DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOVIDEO,//媒体数据中没有 video
    DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOAUDIO,//媒体数据中没有 audio
    DHPLAYERMESSAGESTATUS_INFO_VCODEC_UNSUPPORTVIDEO,//Video格式不支持，audio支持
    DHPLAYERMESSAGESTATUS_INFO_ACODEC_UNSUPPORTAUDIO,//Audio格式不支持，video支持
    DHPLAYERMESSAGESTATUS_INFO_VCODEC_DECODE_ERROR,//Video解码失败，audio正常
    DHPLAYERMESSAGESTATUS_INFO_ACODEC_DECODE_ERROR,//Audio解码失败，video正常
};

#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height

@interface UGCPlayer : UIView
{
   
    BOOL                isPlaying;
    BOOL                beRunning;
    long                duration;
    NSTimer             *bufferTime;
}

@property(nonatomic, assign)MDWord          currentTimeStamp;

@property (nonatomic,assign)HTPlayerType selectType;
//文件路径
@property(nonatomic, strong)  NSString        *filePath;

#pragma mark  标识计时器，0.5秒一走
/** 计时器 */
@property (nonatomic, strong) NSTimer             *playTimer;

#pragma mark 默认是用OPGL来初始化视图

#ifdef _USE_OPENGL_
@property(nonatomic, strong)OpenGLView      *frameView;
#else
@property(nonatomic, strong)UIImageView     *frameView;
#endif

#pragma mark 以下是系统播放器AVPlayer的一些属性

/** 播放属性 */
@property (nonatomic, strong) AVPlayer            *avPlayer;
/** 播放属性 */
@property (nonatomic, strong) AVPlayerItem        *avPlayerItem;
/** playerLayer */
@property (nonatomic, strong) AVPlayerLayer       *avPlayerLayer;


@property(nonatomic,copy) void (^DHPlayerStatusBlock)(DHPlayerStatus status,NSArray *statusArr);

@property(nonatomic,copy) void (^DHPlayerErrorStatusBlock)(DHPLAYERERRORSTATUS status,int errorStatus,NSString *errorString);

@property(nonatomic,copy) void (^DHPlayerMessageStatusBlock)(DHPLAYERMESSAGESTATUS status,int messageStatus,NSString *messageString);



#pragma mark 供外部控制器设置播放速率

@property (nonatomic,assign)   int   playRate;


@property (nonatomic,assign) CGFloat progressValue;


#pragma mark 供外部调用
-(instancetype)initWithPlayerType:(HTPlayerType)selectPlayertype withFatherViewFrame:(CGRect)fatherFrameView;


#pragma mark 转化字符串
- (NSString *)durationStringWithTime:(int)time;



#pragma mark 供外部控制器滑动进行定值播放时间

-(void)progressSeekValue:(CGFloat)seekValue;


#pragma mark 供外部控制器的播放的事件

/**
 从头播放
 */
-(void)playAction;

/**
 仅仅是播放功能
 */
-(void)play;


#pragma mark 供外部控制器的暂停的事件


/**
 仅仅是暂停功能
 */
-(void)pause;
#pragma mark 供外部控制器控制停止播放的事件



-(void)stop;


#pragma mark 供外部调用播放器的当前postion
-(CGFloat)currentPositon;

#pragma mark 供外部调用播放器的总时长的position
-(CGFloat)durationPosition;


#pragma mark 供外部控制器展示比特率

-(NSNumber *)playerBirate;

#pragma mark 供外部控制器展示软解或者硬解标识

-(NSString *)playerHWDecoder;

#pragma mark 供外部控制器，停掉播放器且清楚播放器

/**
 清理播放器
 */
-(void)playerClear;

#pragma mark 供外部控制器旋屏

-(void)changeInterfaceOrientation:(CGRect)viewRect;

/**
 程序进入后台
 */
-(void)applicationWillResignActive;

/**
 程序进入前台
 */
-(void)applicationDidBecomeActive;


@end
