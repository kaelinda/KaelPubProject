//
//  LiverHeaderView.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/28.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kHeaderAnimationTime 0.4

@interface LiverHeaderView : UIView

/**
 用户头像
 */
@property (nonatomic,strong) UIImageView *headerImageView;
@property (nonatomic,strong) UIImageView *fireCountImageView;
@property (nonatomic,strong) UILabel *liverNameLabel;
@property (nonatomic,strong) UILabel *countNumLabel;

/**
 用户名
 */
@property (nonatomic,copy) NSString *name;

/**
 热度
 */
@property (nonatomic,assign) NSNumber *countNum;


-(void)setLiverHeaderImage:(UIImage *)headerImage andName:(NSString *)name andCountNum:(NSNumber *)countNum;

@end
