//
//  OpenGLView.m
//  HelloOpenGL
//
//  Created by  on 12-3-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "OpenGLView.h"

@implementation OpenGLView
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

#pragma mark init
// Replace initWithFrame with this
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {        
    }
    return self;
}

// Replace dealloc method with this
//- (void)dealloc
//{
//    [super dealloc];
//}

@end
