//
//  UGCProgramPlayerVC.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCProgramPlayerVC.h"
#import "UGCProgramControlView.h"
#import "HTRequest.h"
#import "HTEmptyView.h"
#import "NetWorkNotification_shared.h"
#import "CustomAlertView.h"
@interface UGCProgramPlayerVC ()<HTRequestDelegate,CustomAlertViewDelegate>
@property (nonatomic,strong)UGCProgramControlView *ugcControlView;
@property (nonatomic,assign)CGSize playerSize;
@property (nonatomic,strong)NSString *filePathStr;
@property (nonatomic,strong)HTEmptyView *emptyLoadingView;
@property (nonatomic,strong)NSString  *netInfo;
@property (nonatomic,assign)BOOL isRotate;
@end

@implementation UGCProgramPlayerVC
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

        _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];

    

}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _isRotate = YES;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[NSNotificationCenter defaultCenter]removeObserver:self];


}


- (instancetype)init
{
    self = [super init];
    if (self) {
 
        _isRotate = YES;

        [[NetWorkNotification_shared shardNetworkNotification]setCanShowToast:YES];//实时网络监测
        //注册通知的观察
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChange:) name:@"NETCHANGED" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(stopPlayer) name:@"MSG_STOPPLAYER" object:nil];
    }
    return self;
}

-(void)stopPlayer
{
    [_ugcControlView controlViewClearPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNaviBar:YES];

    //
    [self initBaseData];

    //zzsc 取消侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self initBaseView];

    if (_IsConstraintLandscape == YES) {
        [self setOrientationLandscape];
    }else
    {
        [self setOrientationPortrait];
    }

    [self setupBaseRequest];


   
    
}





-(void)ugcProgramProgramID:(NSString *)programID andProgramType:(NSString *)programType andIsConstraintLandscape:(BOOL)isConstraintLandscape andprogramTitleStr:(NSString *)programTitleStr
{
    _programID = programID;
    
    _programType = programType;
    
    _IsConstraintLandscape = isConstraintLandscape;
    
    _programTitleStr = programTitleStr;
    

    
}

-(void)initBaseData
{
    if (_filePathStr.length == 0 ) {
        _filePathStr = @"";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
    
}



-(void)networkChange:(NSNotification*)noti
{
    NSString *notiStr = [noti object];
    
    if ([notiStr isEqualToString:@"蜂窝网络"]) {
        [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_ugcControlView.playButton setTag:ENGINE_STATUS_PLAY];

        [_ugcControlView.player pause];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
            nameAlert.tag = 11;
            [nameAlert show];
        });
        
    }
    
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 11) {
        //取消
        if (buttonIndex == 1) {
            [self setOrientationPortrait];
            
            [_ugcControlView backPopViewController];

        }
        //确定
        if (buttonIndex == 0) {
//            [alertView hide];
            [_ugcControlView.playButton setTag:ENGINE_STATUS_PAUSE];
            [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [_ugcControlView.player play];

        }
    }
    
    if (alertView.tag == 13) {
        //取消
        if (buttonIndex == 1) {
            [self setOrientationPortrait];
            
            [_ugcControlView backPopViewController];
        }
        //确定
        if (buttonIndex == 0) {
//            [alertView hide];
            [_ugcControlView.playButton setTag:ENGINE_STATUS_PAUSE];
            [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [_ugcControlView.player setFilePath:_filePathStr];
            [_ugcControlView controlViewActionPlay:NO];

        }
    }
    
    if (alertView.tag == 14) {
        //取消
        if (buttonIndex == 1) {
            [self setOrientationPortrait];

            
            [_ugcControlView backPopViewController];
        }
        //确定
        if (buttonIndex == 0) {
            
        }
    }

    
}



#pragma mark - ------- app 进入前后台

-(void)appEnterBackGround{
    if (_ugcControlView)
    {
        _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];

        [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_ugcControlView.playButton setTag:ENGINE_STATUS_PLAY];
        [_ugcControlView.player pause];
        [_ugcControlView.player applicationWillResignActive];
    }
}

-(void)appEnterForeground{
    if (_ugcControlView) {
        
        _netInfo =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];

        [_ugcControlView.player applicationDidBecomeActive];

        if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
            [_ugcControlView.playButton setTag:ENGINE_STATUS_PLAY];
            [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [_ugcControlView.player pause];
        }else
        {
            if (_ugcControlView.isByUserPause == YES) {
                
                return;
            }else
            {
            [_ugcControlView.playButton setTag:ENGINE_STATUS_PAUSE];
            [_ugcControlView.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [_ugcControlView.player play];
            }
        }
        

    }
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"NETCHANGED" object:nil];

}





-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
//    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    if ([type isEqualToString:UGC_COMMON_GETPROGRAMURL]) {
//        [self hiddenCustomHUD];
            NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        if ( ret == 0) {
            
            NSString *playUrl = @"";
            
            if ([_programType intValue] == 1) {
                
                NSMutableArray *array = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
                
                if ([array count]>0) {
                    
                    playUrl = [[array lastObject] objectForKey:@"url"];

                }
                
            }
            
            
            if ([_programType intValue] == 0) {
                
                playUrl = [result objectForKey:@"url"];
            }
            
            if (isEmptyStringOrNilOrNull(playUrl)) {
                
                
                
                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andMessage:@"没有获取到数据" andButtonNum:1 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"" withDelegate:self];
                
                nameAlert.tag = 14;
                [nameAlert show];
            }else
            {
            
            [self programWithFilePath:playUrl];
            
            }
            
            
        }else
        {
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithTitle:@"温馨提示" andMessage:@"没有获取到数据" andButtonNum:1 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"" withDelegate:self];

            nameAlert.tag = 14;
            [nameAlert show];
            

        }
        
        
    }
    
    
}





-(void)programWithFilePath:(NSString *)filepath

{
     _filePathStr = filepath;
//#warning 暂时先这么加，一会再改
    
    
    if ([_netInfo isEqualToString:@"2G"]||[_netInfo isEqualToString:@"3G"]||[_netInfo isEqualToString:@"4G"]||[_netInfo isEqualToString:@"无网络"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
            nameAlert.tag = 13;
            [nameAlert show];
        });

    }else
    {
    [_ugcControlView.player setFilePath:_filePathStr];
    [_ugcControlView controlViewActionPlay:NO];

    }
}

-(void)setIsConstraintLandscape:(BOOL)IsConstraintLandscape
{
    
    
    _IsConstraintLandscape = IsConstraintLandscape;
    
   
}

-(void)setOrientationLandscape
{
    
    CGSize playSize = CGSizeMake(self.view.frame.size.width<self.view.frame.size.height ? self.view.frame.size.width : self.view.frame.size.height, self.view.frame.size.height > self.view.frame.size.width ? self.view.frame.size.height:self.view.frame.size.width);
    
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft) {
        
        
        [UIView animateWithDuration:0.25 animations:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeLeft] forKey:@"orientation"];
        }];
        
    }else
    {
        
        
        [UIView animateWithDuration:0.25 animations:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
        }];
    }

    
    _ugcControlView.frame = CGRectMake(0, 0, playSize.height, playSize.width);
    _isRotate = NO;

    [_ugcControlView.player changeInterfaceOrientation:CGRectMake(0, 0, playSize.height, playSize.width)];
}



-(void)setOrientationPortrait
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    
    _ugcControlView.frame = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight);
    _isRotate = NO;

    [_ugcControlView.player changeInterfaceOrientation:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
}


-(void)initBaseView {
    

    __weak __typeof(&*self)weakSelf = self;
    self.view.userInteractionEnabled = YES;
   
    if (!_ugcControlView) {
        if (_IsConstraintLandscape == YES) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
            CGSize playSize = CGSizeMake(self.view.frame.size.width<self.view.frame.size.height ? self.view.frame.size.width : self.view.frame.size.height, self.view.frame.size.height > self.view.frame.size.width ? self.view.frame.size.height:self.view.frame.size.width);

             _ugcControlView = [[UGCProgramControlView alloc]initWithPlayerType:KHTPlayer_ARCPlay andFatherFrame:CGRectMake(0, 0, playSize.height, playSize.width) isALVideoEdite:NO];
            
        }else
        {
            _ugcControlView = [[UGCProgramControlView alloc]initWithPlayerType:KHTPlayer_ARCPlay andFatherFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight) isALVideoEdite:NO];

            
        }
        
    }
    

//        [self programWithFilePath:@"http://cdn.tumbo.com.cn/follomeFront//file/library/library_moment_video/2016/11/02/20161102171719373752095.mp4"];
    [self.view addSubview:_ugcControlView];
    [_ugcControlView setTitleLabelStr:_programTitleStr];

    _ugcControlView.userInteractionEnabled = YES;
    [_ugcControlView setBackgroundColor:[UIColor blackColor]];
    _ugcControlView.goBackBlock=^()
    {
        
        [weakSelf setOrientationPortrait];

        [weakSelf.navigationController popViewControllerAnimated:NO];
        

    };
    
    
    
}


-(void)setBigScreenPlayer:(CGRect)screenRect
{
    
    [_ugcControlView setFrame:screenRect];
        [_ugcControlView.player changeInterfaceOrientation:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];

  }

-(void)setSmallScreenPlayer:(CGRect)screenRect
{
    [_ugcControlView setFrame:screenRect];

        [_ugcControlView.player changeInterfaceOrientation:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];

    
}

-(void)setupBaseRequest{

//    [self showCustomeHUD];
    
    
//    _emptyLoadingView = [[HTEmptyView alloc]initWithEmptyFatherView:self.view andEmptyImageString:@"bg_retry_gesture" andEmptyLabelText:nil emptyViewClick:^{
//        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//        
//        [request UGCCommonGetProgramUrlWithProgramId:_programID];
//    }];
//    _emptyLoadingView.hidden = NO;
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request UGCCommonGetProgramUrlWithProgramId:_programID];


}
#pragma mark - - orientation

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
        // 如果当前处于横屏显示状态
    _playerSize = CGSizeMake(self.view.frame.size.width<self.view.frame.size.height ? self.view.frame.size.width : self.view.frame.size.height, self.view.frame.size.height > self.view.frame.size.width ? self.view.frame.size.height:self.view.frame.size.width);
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        

        [self setBigScreenPlayer:CGRectMake(0, 0, _playerSize.height, _playerSize.width)];
        
    }else
    {

        
        [self setSmallScreenPlayer:CGRectMake(0, 0, _playerSize.width, _playerSize.height)];
    }
    
    
    
}





- (BOOL)shouldAutorotate {
    
    
    return _isRotate?YES:NO;
}

//设置支持的屏幕旋转方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

//设置presentation方式展示的屏幕方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return _IsConstraintLandscape? UIInterfaceOrientationLandscapeRight:UIInterfaceOrientationPortrait;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
