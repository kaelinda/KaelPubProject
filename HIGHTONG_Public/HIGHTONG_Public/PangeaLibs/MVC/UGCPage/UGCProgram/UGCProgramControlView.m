//
//  UGCProgramControlView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/4/1.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCProgramControlView.h"
#import <UMSocialCore/UMSocialCore.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import "ShareViewVertical.h"
#import "NetWorkNotification_shared.h"
#import "CustomAlertView.h"
#import "HTVolumeUtil.h"
#define ADD_GESTURE
@interface UGCProgramControlView ()<CustomAlertViewDelegate,UIGestureRecognizerDelegate>
{
    
    NSString *netWork;
}
@property (nonatomic,strong)NSTimer *playedTime;
@property (nonatomic,assign)CGFloat playerDurationPosition;
@property (nonatomic,assign)CGFloat playerCurrentPosition;
@property (nonatomic,assign)BOOL isMaskShowing;
@property (assign, atomic) int  autoHiddenSecend;
@property (nonatomic,strong)NSTimer *autoHiddenTimer;
@property (nonatomic, strong)ShareViewVertical *shareViewVertical;
@property (nonatomic,strong)UIPanGestureRecognizer *pan;
/** 定义一个实例变量，保存枚举值 */
@property (nonatomic, assign) PanDirection        panDirection;
/**
 * 亮度的VIEW和声音的View
 */
/** 用来保存快进的总时长 */
@property (nonatomic, assign) CGFloat             sumTime;
@property (nonatomic,assign)CGFloat systemVODBrightness;
@property (nonatomic,strong)UIProgressView *brightnessVODProgress;
@property (nonatomic,strong)UIImageView *brightnessVODView;
@property (nonatomic,strong)UIProgressView*volumeVODProgressV;
@property (nonatomic, assign) BOOL isFinish;
@end
@implementation UGCProgramControlView

- (instancetype)initWithPlayerType:(HTPlayerType)playerType andFatherFrame:(CGRect)fatherView isALVideoEdite:(BOOL)isALVideoEdite;
{
    self = [super init];
    if (self) {
        _isHTALVideoEditType = isALVideoEdite;
        [self setupViewsWithPlayerType:playerType andFatherViewFarme:fatherView];
        _selectPlayerType = playerType;


    }
    return self;
}




-(void)setupViewsWithPlayerType:(HTPlayerType)playerType andFatherViewFarme:(CGRect)viewFrame
{
    __weak __typeof(&*self)weakSelf = self;

    
    _player = [[UGCPlayer alloc] initWithPlayerType:playerType withFatherViewFrame:viewFrame];
    
    [_player setBackgroundColor:[UIColor yellowColor]];
    [self addSubview:_player];
    
    _loadingView = [[UGCLoadingView alloc] initWithLoadingType:kUGCLoading_none andPortraitImg:nil andLandscapImg:nil];
    [self addSubview:_loadingView];
    [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    if (_isHTALVideoEditType == NO) {
        
        [_loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];
    }else
    {
        [_loadingView changeCenterUIWithType:kUGCLoading_none];
    }
    

    
    if (_isHTALVideoEditType == NO) {
       
        _topBarIV = [[UIImageView alloc]init];
        _topBarIV.userInteractionEnabled = YES;
        [_topBarIV setImage:[UIImage imageNamed:@"bg_player_playbill_listview_epg"]];
        [self addSubview:_topBarIV];
        [_topBarIV addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_topBarIV"];
        
        [_topBarIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(weakSelf.mas_top).offset(0*kDeviceRate);
            make.left.mas_equalTo(weakSelf.mas_left);
            make.right.mas_equalTo(weakSelf.mas_right);
            make.height.mas_equalTo(88*kDeviceRate);
        }];
        
        
        _returnButton = [[UIButton alloc]init];
        [_returnButton setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
        [_returnButton addTarget:self action:@selector(returnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_topBarIV addSubview:_returnButton];
        [_returnButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(weakSelf.mas_left).offset(27*kDeviceRate);
            make.top.mas_equalTo(weakSelf.mas_top).offset(20+20*kDeviceRate);
            make.size.mas_equalTo(CGSizeMake(28*kDeviceRate, 28*kDeviceRate));
        }];
        
        _shareButton = [[UIButton alloc]init];
        [_shareButton setImage:[UIImage imageNamed:@"icon_ugc_shard_white"] forState:UIControlStateNormal];
        [_topBarIV addSubview:_shareButton];
        [_shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(weakSelf.mas_right).offset(-27*kDeviceRate);
            make.top.mas_equalTo(weakSelf.mas_top).offset(20+20*kDeviceRate);
            make.size.mas_equalTo(CGSizeMake(25*kDeviceRate, 25*kDeviceRate));
        }];
        
        [_shareButton addTarget:self action:@selector(shareButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        _titleLabel = [[UILabel alloc]init];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleLabel.font = HT_FONT_SECOND;
        _titleLabel.textColor = UIColorFromRGB(0xffffff);
        [_topBarIV addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_returnButton.mas_right).offset(5*kDeviceRate);
            make.right.mas_equalTo(_shareButton.mas_left).offset(-10);
            make.top.mas_equalTo(_topBarIV.mas_top).offset(20+20*kDeviceRate);
            make.height.mas_equalTo(28*kDeviceRate);
        }];

        
        
    }
    
    
    
    _bottomBarIV = [[UIImageView alloc]init];
    _bottomBarIV.userInteractionEnabled = YES;
    [_bottomBarIV setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
    [self addSubview:_bottomBarIV];
    [_bottomBarIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(weakSelf.mas_bottom);
        make.left.mas_equalTo(weakSelf.mas_left);
        make.right.mas_equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(60*kDeviceRate);
    }];
    
//    self.isMaskShowing = NO;

    [self setIsMaskShowing:YES];


#ifdef ADD_GESTURE
    //图片点击手势
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [weakSelf addGestureRecognizer:tapGesture];

#endif
    

    
    self.playButton= [[UIButton alloc]init];
    [self.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
    [self.playButton setTag:ENGINE_STATUS_PLAY];
    [_bottomBarIV addSubview:self.playButton];
    
    [self.playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-20*kDeviceRate);
        make.left.mas_equalTo(_bottomBarIV.mas_left).offset(12*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(45*kDeviceRate, 28*kDeviceRate));
    }];
    
    
    [self.playButton addTarget:self action:@selector(playerPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    _currentPositionLabel = [[UILabel alloc]init];
    _currentPositionLabel.textAlignment = NSTextAlignmentCenter;
    _currentPositionLabel.textColor = UIColorFromRGB(0xffffff);
    _currentPositionLabel.font = HT_FONT_FOURTH;
    _currentPositionLabel.text = @"00:00:00";
    [_currentPositionLabel setBackgroundColor:[UIColor clearColor]];
    [_bottomBarIV addSubview:_currentPositionLabel];
    [_currentPositionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.playButton.mas_right).offset(18*kDeviceRate);
        make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-10*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(80*kDeviceRate, 40*kDeviceRate));
    }];
    
    
    _durationPostionLabel = [[UILabel alloc]init];
    _durationPostionLabel.textAlignment = NSTextAlignmentCenter;
    _durationPostionLabel.textColor = UIColorFromRGB(0xffffff);
    _durationPostionLabel.font = HT_FONT_FOURTH;
    _durationPostionLabel.text = @"00:00:00";
    [_durationPostionLabel setBackgroundColor:[UIColor clearColor]];
    [_bottomBarIV addSubview:_durationPostionLabel];
    [_durationPostionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_bottomBarIV.mas_right);
        make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-10*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(80*kDeviceRate, 40*kDeviceRate));
    }];

 
    
    self.progressSlider = [[UISlider alloc]init];
    [self.progressSlider setMaximumValue:1000.00];
    [self.progressSlider setMinimumValue:0.00];
    // slider开始滑动事件
    [self.progressSlider setThumbImage:[UIImage imageNamed:@"ic_ugc_player_seekbar_progress_cursor"] forState:UIControlStateNormal];
    self.progressSlider.userInteractionEnabled = YES;
    [self.progressSlider setMaximumTrackImage:[UIImage imageNamed:@"bg_ugc_player_progress_normal"] forState:UIControlStateNormal];
    [self.progressSlider setMinimumTrackImage:[UIImage imageNamed:@"bg_ugc_player_progress_pressed"] forState:UIControlStateNormal];
    [self.progressSlider setBackgroundColor:[UIColor clearColor]];
    [_bottomBarIV addSubview:self.progressSlider];
    
    [self.progressSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_currentPositionLabel.mas_right).offset(18*kDeviceRate);
        make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-8*kDeviceRate);
        make.right.mas_equalTo(_durationPostionLabel.mas_left).offset(-18*kDeviceRate);
        make.height.mas_equalTo(45*kDeviceRate);
    }];
    
    
    
    [self.progressSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
    // slider滑动中事件
    [self.progressSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    // slider结束滑动事件
    [self.progressSlider addTarget:self action:@selector(progressSliderTouchEnd:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    //    self.progressSlider.continuous = NO;
    self.progressSlider.enabled = YES;
    
    
    _progressTimeView = [[UIImageView alloc]init];
    _progressTimeView.hidden = YES;
    [_progressTimeView setImage:[UIImage imageNamed:@"bg_player_full_fast_action"]];
    [weakSelf addSubview:_progressTimeView];
    
    
    
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    [_progressTimeView addSubview:_progressDirectionIV];
    
    
    _progressTimeLable_top = [[UILabel alloc]init];
    _progressTimeLable_top.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_top.textColor = App_selected_color;
    _progressTimeLable_top.backgroundColor = [UIColor clearColor];
    _progressTimeLable_top.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_top.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_top.shadowOffset = CGSizeMake(1.0, 1.0);
    [_progressTimeView addSubview:_progressTimeLable_top];
    
    
    _progressTimeLable_bottom = [[UILabel alloc]init];
    _progressTimeLable_bottom.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_bottom.textColor = UIColorFromRGB(0xffffff);
    _progressTimeLable_bottom.backgroundColor = [UIColor clearColor];
    _progressTimeLable_bottom.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_bottom.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_bottom.hidden = NO;
    _progressTimeLable_bottom.shadowOffset = CGSizeMake(1.0, 1.0);
    [_progressTimeView addSubview:_progressTimeLable_bottom];
    
    
    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.height.mas_equalTo(70*kRateSize);
        make.width.mas_equalTo(130*kRateSize);
        
    }];
    
    
    
    [_progressDirectionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_progressTimeView.mas_centerY).offset(-15*kRateSize);
        make.centerX.mas_equalTo(_progressTimeView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
    }];
    
    
    
    
    [_progressTimeLable_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(weakSelf.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.left.mas_equalTo(weakSelf.progressTimeView.mas_left).offset(5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_progressTimeLable_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(weakSelf.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.right.mas_equalTo(weakSelf.progressTimeView.mas_right).offset(-5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
        
    }];
    

    
    

    
    _brightnessVODView = [[UIImageView alloc]init];
    _brightnessVODView.image = [UIImage imageNamed:@"bg_player_brightness.png"];
    _brightnessVODView.hidden = YES;
    [weakSelf addSubview:_brightnessVODView];
    _brightnessVODProgress = [[UIProgressView alloc]init];
    _brightnessVODProgress.trackImage = [UIImage imageNamed:@"video_num_bg.png"];
    _brightnessVODProgress.progressImage = [UIImage imageNamed:@"video_num_front.png"];
    _brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
    _brightnessVODProgress.hidden = NO;
    
    
    _volumeVODProgressV = [[UIProgressView alloc]init];

    [_brightnessVODView addSubview:_brightnessVODProgress];
    [self addSubview:_volumeVODProgressV];


    //亮度视图尺寸****************
    
    [_brightnessVODView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessVODProgress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.brightnessVODView.mas_bottom);
        make.centerX.mas_equalTo(weakSelf.brightnessVODView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
  
  
    [self playerStatusObserve];
    

}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    UIView *listVIew = (UIView *)object;
    if (_topBarIV == listVIew && [@"hidden" isEqualToString:keyPath]) {
        if (_topBarIV.hidden == NO) {

            [[UIApplication sharedApplication]setStatusBarHidden:NO];

        }else
        {
            [[UIApplication sharedApplication]setStatusBarHidden:YES];

            
        }
    };
}


-(void)setTitleLabelStr:(NSString *)titleLabelStr
{
    _titleLabelStr = titleLabelStr;
    
    _titleLabel.text = _titleLabelStr;
}

-(void)shareButton:(UIButton *)btn{
    NSLog(@"我要分享什么玩意儿？");
    
    [self hiddenControlViewBar];
    
    btn.selected = !btn.selected;
    __weak typeof(&*self)wself = self;
    if (!_shareViewVertical) {
        _shareViewVertical = [[ShareViewVertical alloc] init];
        [_shareViewVertical setInstalledQQ:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]];
        [_shareViewVertical setInstalledWX:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]];
        [wself addSubview:_shareViewVertical];
        [_shareViewVertical mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(wself);
        }];
        
        [_player pause];
        
        [_playedTime setFireDate:[NSDate distantFuture]];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_playButton setTag:ENGINE_STATUS_PLAY];

        
        _shareViewVertical.shareBlock = ^(ShareType type){
            switch (type) {
                case Wechat:{
                    ZSToast(@"微信分享")
                }
                    break;
                case WechatZone:{
                    ZSToast(@"朋友圈分享")
                }
                    break;
                case QQ:{
                    ZSToast(@"QQ分享")
                }
                    break;
                case QQZone:{
                    ZSToast(@"QQ空间分享")
                }
                    break;
                    
                default:
                    break;
            }
        };
        
        _shareViewVertical.dismissHandle = ^(BOOL isDismiss){
                if (isDismiss) {
                //隐藏暂停视图 继续播放
                    [wself showControlViewBar];
                    if (wself.isByUserPause) {
                        
                        return ;
                    }else
                    {
                        [wself.playedTime setFireDate:[NSDate date]];
                        [wself.playButton setTag:ENGINE_STATUS_PAUSE];
                        [wself.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
                        [wself.player play];
                    }
            }else{
                //出现分享视图 暂停下
//                [wself.player pause];
                [wself.player pause];
                
                [wself.playedTime setFireDate:[NSDate distantFuture]];
                [wself.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
                [wself.playButton setTag:ENGINE_STATUS_PLAY];

            }
        };
 
        
        
        
    }else{
        _shareViewVertical.hidden = NO;
    }
}




//-(void)shareButton:(UIButton*)sender
//{
//    __weak __typeof(&*self)wself = self;
//    wself.shareView.delegate = wself;
//    wself.shareView.hidden = !wself.shareView.hidden;
//    wself.shareView.installedWX = [WXApi isWXAppInstalled];
//    wself.shareView.installedQQ = [QQApiInterface isQQInstalled];
//    //        [wself.backgroundView actionPauseForVideo];
//}

-(void)tapGesture:(UITapGestureRecognizer*)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateRecognized) {
        
        self.isMaskShowing = !self.isMaskShowing;
    
    }

}


-(void)setIsMaskShowing:(BOOL)isMaskShowing
{
    _isMaskShowing = isMaskShowing;
    
    if (_isMaskShowing == YES) {
        
        [self showControlViewBar];
        [self reStartHiddenTimer];
    }else
    {

        [self hiddenControlViewBar];
    }
}



-(void)showControlViewBar
{
    [self reStartHiddenTimer];

    _bottomBarIV.hidden = NO;
    if (_isHTALVideoEditType == NO) {
        [UIView animateWithDuration:0.25 animations:^{
            [[UIApplication sharedApplication]setStatusBarHidden:NO];
            
            _topBarIV.hidden = NO;
        }];
       
        
    }

}

-(void)hiddenControlViewBar
{

    _bottomBarIV.hidden = YES;
    
    if (_isHTALVideoEditType == NO) {
       [UIView animateWithDuration:0.25 animations:^{
           _topBarIV.hidden = YES;
           [[UIApplication sharedApplication]setStatusBarHidden:YES];

       }];
       

    }
    //        if (_shareView.hidden == NO) {
    //            _shareView.hidden = YES;
    //        }
    
   

}

-(void)reStartHiddenTimer
{

    self.autoHiddenSecend = 5;
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
    self.autoHiddenTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(autoHiddenPanel:) userInfo:nil repeats:YES];
}

- (void)autoHiddenPanel:(NSTimer *)sender
{
    if (self.autoHiddenSecend == 0) {
        [self hiddenPanlwithBool:YES];
    } else {
        self.autoHiddenSecend --;
    }
}



-(void)hiddenPanlwithBool:(BOOL)bol
{
    if (bol) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;

        [self setIsMaskShowing:NO];

    } else {
     
        [self setIsMaskShowing:YES];
        
    }
}






-(void)progressSliderTouchBegan:(UISlider*)slider
{
    if (_selectPlayerType == KHTPlayer_ARCPlay) {
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        [_playedTime setFireDate:[NSDate distantFuture]];

    }else
    {
        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            // 暂停timer
            [_playedTime setFireDate:[NSDate distantFuture]];
        }
    }
   }

-(void)progressSliderValueChanged:(UISlider*)slider
{
//    [_player progressSeekValue:slider.value];
    
    
    if (_selectPlayerType == KHTPlayer_ARCPlay) {
        
        [_player pause];
        
        [_playedTime setFireDate:[NSDate distantFuture]];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        [_playButton setTag:ENGINE_STATUS_PLAY];
        if (_playerDurationPosition) {
            
            
            CGFloat total  = (CGFloat)(_playerDurationPosition)/1000;
            
            //计算出拖动的当前秒数
            NSInteger dragedSeconds = floorf(total * slider.value);
            
            
            NSString *currentTime = [_player durationStringWithTime:(int)dragedSeconds];
            
            NSString *totalTime = [_player durationStringWithTime:(int)_playerDurationPosition];
            
            NSLog(@"===slider%.f",_playerDurationPosition);

            if (total>0) {
                
                _currentPositionLabel.text = [NSString stringWithFormat:@"%@",currentTime];
                
                _durationPostionLabel.text = [NSString stringWithFormat:@"%@",totalTime];
                
                
            }else
            {
                slider.value = 0;
            }
            
            
        }else
        {
            slider.value = 0;
        }

    }else
    {
        //拖动改变视频播放进度
        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {

//            CGFloat value   = slider.value - self.sliderVODLastValue;
//            if (value > 0) { style = @">>"; }
//            if (value < 0) { style = @"<<"; }
//            
//            self.sliderVODLastValue    = slider.value/1000;
//            // 暂停
            [self.player.avPlayer pause];
            
            CGFloat total           = ((CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale)/1000;
            
            //计算出拖动的当前秒数
            NSInteger dragedSeconds = floorf(total * slider.value);
            
            //转换成CMTime才能给player来控制播放进度
            
            //        CMTime dragedCMTime     = CMTimeMake(dragedSeconds, 1);
            //        // 拖拽的时长
            //        NSInteger proMin        = (NSInteger)CMTimeGetSeconds(dragedCMTime) / 60;//当前秒
            //        NSInteger proSec        = (NSInteger)CMTimeGetSeconds(dragedCMTime) % 60;//当前分钟
            //
            //        //duration 总时长
            //        NSInteger durMin        = (NSInteger)total / 60;//总秒
            //        NSInteger durSec        = (NSInteger)total % 60;//总分钟
            //
            //        NSString *currentTime   = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
            //        NSString *totalTime     = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
            
            
            NSString *currentTime = [_player durationStringWithTime:(int)dragedSeconds];
            
            if (total > 0) {
                // 当总时长 > 0时候才能拖动slider
                _currentPositionLabel.text  = currentTime;
                //            self.controlView.horizontalLabel.hidden = NO;
                //            self.controlView.horizontalLabel.text   = [NSString stringWithFormat:@"%@ %@ / %@",style, currentTime, totalTime];
            }else {
                // 此时设置slider值为0
                slider.value = 0;
            }
            
        }else { // player状态加载失败
            // 此时设置slider值为0
            slider.value = 0;
        }
    }
    
    
    
}


-(void)progressSliderTouchEnd:(UISlider*)slider
{
    if (_selectPlayerType == KHTPlayer_ARCPlay) {
        CGFloat total  = (CGFloat)(_playerDurationPosition)/1000;
        
        if (total>0) {
                     //计算出拖动的当前秒数
            NSInteger dragedSeconds = floorf(total * slider.value);
            
            [_player progressSeekValue:dragedSeconds];
            
            [_playedTime setFireDate:[NSDate date]];
            [_playButton setTag:ENGINE_STATUS_PAUSE];
            [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];

            _currentPositionLabel.text = [NSString stringWithFormat:@"%@",[_player durationStringWithTime:(int)dragedSeconds]];
            
            _durationPostionLabel.text = [NSString stringWithFormat:@"%@",[_player durationStringWithTime:_playerDurationPosition]];
            
            
        }else
        {
            slider.value = 0;
        }
 
        
    }else
    {
        if (self.player.avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            
            // 继续开启timer
            [_playedTime setFireDate:[NSDate date]];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                self.controlView.progressTimeView.hidden = YES;
//            });
            // 结束滑动时候把开始播放按钮改为播放状态
//            self.controlView.startVODBtn.selected = YES;
//            self.isPauseByUser                 = NO;
            
            // 滑动结束延时隐藏controlView
            //        [self autoFadeOutControlBar];
            // 视频总时间长度
            CGFloat total           = (CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale;
            
            //计算出拖动的当前秒数
            NSInteger dragedSeconds = (floorf(total * slider.value)/1000);
            
            [self.player progressSeekValue:dragedSeconds];
        }

    }
    
    
}




-(void)returnClick:(UIButton*)sender
{
    

    [self backPopViewController];
}


-(void)backPopViewController
{
    [self controlViewClearPlayer];
   
        if (_goBackBlock) {
            
            _goBackBlock();
        }
        
    

}


-(void)controlViewClearPlayer
{

    if (self.playedTime) {
        [self.playedTime invalidate];
        self.playedTime = nil;
    }
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
//    weakSelf.isMaskShowing = YES;
    [_topBarIV removeObserver:self forKeyPath:@"hidden"];

    [self.player playerClear];
}



-(void)controlViewActionPlay:(BOOL)isPlayButton
{
    if (isPlayButton == NO) {
        
         [_player playAction];
    }
}


-(void)controlViewActionPlay
{
    
    if ([netWork isEqualToString:@"WIFI"]) {
        
        [_player playAction];

    }else if ([netWork isEqualToString:@"无网络"])
    {
        return;
    }else if([netWork isEqualToString:@"2G"]||[netWork isEqualToString:@"3G"]||[netWork isEqualToString:@"4G"])
    {
        CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"检测到您当前使用的是非wifi网络，可能会产生流量费用，您确定继续播放吗？" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"继续" withDelegate:self];
        nameAlert.tag = 11;
        [nameAlert show];
    }

    
    
    
}



-(void)playerPlay:(UIButton*)sender
{
    
    sender.selected = !sender.selected;
    if (sender.selected) {
        
        NSLog(@"我点击了暂停");
        [_player pause];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
        
        _isByUserPause = YES;
        
    }else
    {
        [_player play];
        [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
        
        _isByUserPause = NO;
        NSLog(@"我点击了播放");
    }
    

}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 11) {
        //取消
        if (buttonIndex == 1) {
//            [alertView hide];
            return;
        }
        //确定
        if (buttonIndex == 0) {
//            [alertView hide];
            [_player playAction];


        }
    }
    
    if (alertView.tag == 12) {
        //取消
        if (buttonIndex == 1) {
//            [alertView hide];

            [self backPopViewController];
            return;
        }
        //确定
        if (buttonIndex == 0) {
//            [alertView hide];
//            [_player playAction];
            
            [self autoContinuePlayer];
        }
    }

    
}





-(void)playerStatusObserve{
    __weak typeof(&*self)weakSelf = self;
    
    if (_player) {
        
        _player.DHPlayerStatusBlock = ^(DHPlayerStatus status, NSArray *statusArr) {

//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self playerObserveDHPlayerPlayStatus:status andPlayerStatusArray:statusArr];
//            });
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf playerObserveDHPlayerPlayStatus:status andPlayerStatusArray:statusArr];

            });
            
        };
        
       _player.DHPlayerErrorStatusBlock = ^(DHPLAYERERRORSTATUS status,int errorStatus,NSString *errorString)
        {
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                
//                ZSToast(errorString);
//
//            });
            
//            NSLog(@"error : \n errorString:%@\n errorStatus:%d \n status:%lu",errorString,errorStatus,(unsigned long)status);
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (weakSelf.player) {
                    
                    NSLog(@"当前节目的错误LOG是%d%@",errorStatus,errorString);
                    
                    CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"当前节目无法播放,是否重新播放?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:weakSelf];
                    nameAlert.tag = 12;
                    [nameAlert show];
  
                }
                
            });
            
//            MAIN();
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//                CustomAlertView *nameAlert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"温馨提示" andDescribeString:@"当前节目无法播放,是否重新播放?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:weakSelf];
//                nameAlert.tag = 12;
//                [nameAlert show];
//
//               
//                
//            });
            
            
        };
        
        
       _player.DHPlayerMessageStatusBlock = ^(DHPLAYERMESSAGESTATUS status,int messageStatus,NSString *messageString) {
           
//           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//               NSLog(@"=====player%@",weakSelf);
//
////               [self playerObserveDHPlayerMessageStatus:status andMessageValueStatus:messageStatus andMessageString:messageString];
//           });
        };
        
        
        
    }
    
}


#pragma mark 播放器错误状态


-(void)playerObserveDHPlayerPlayStatus:(DHPlayerStatus)playerStatus andPlayerStatusArray:(NSArray*)statusArr
{
    __weak __typeof(&*self)weakSelf = self;
    
    switch (playerStatus) {
            /*
             case DHPlayerStatus_init:{}break;
             case DHPlayerStatus_stop:{}break;
             case DHPlayerStatus_pause:{}break;
             case DHPlayerStatus_ready:{}break;
             case DHPlayerStatus_failed:{}break;
             case DHPlayerStatus_offline:{}break;
             case DHPlayerStatus_playing:{}break;
             case DHPlayerStatus_buffering:{}break;
             case DHPlayerStatus_connecting:{}break;
             case DHPlayerStatus_sizeChanged:{}break;
             case DHPlayerStatus_MSG:{}break;
             case DHPlayerStatus_startToPlay:{}break;
             */
            
            
            //                    NSMutableDictionary *notiDic = [NSMutableDictionary dictionary];
            //
            //                    notiDic = (NSMutableDictionary*)noti.object;
            //
            //                    int status = [[notiDic objectForKey:@"statusTag"] intValue];
            
            
        case DHPlayerStatus_play:{
            NSLog(@"开始播放");
            if (weakSelf.playedTime) {
                
                [weakSelf.playedTime invalidate];
                
                weakSelf.playedTime = nil;

                [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
                [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
                NSLog(@"听了播放器");
            }
            
//            if (_selectPlayerType == KHTPlayer_ARCPlay) {
            
                if(!weakSelf.playedTime){
                    weakSelf.playedTime = [NSTimer scheduledTimerWithTimeInterval:0.5 target:weakSelf selector:@selector(updateProgress) userInfo:nil repeats:YES];
                    
                    weakSelf.pan = [[UIPanGestureRecognizer alloc]initWithTarget:weakSelf action:@selector(panDirection:)];
                    weakSelf.pan.delegate                = weakSelf;
                    [weakSelf addGestureRecognizer:weakSelf.pan];
                    
                    [_playedTime setFireDate:[NSDate date]];
                    [_playButton setTag:ENGINE_STATUS_PAUSE];
                    [_playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];

//                }
                
                
                
            }
            
            
            
            
        }break;
        case DHPlayerStatus_stop:{
            if (weakSelf.playedTime) {
                
                [weakSelf.playedTime invalidate];
                
                weakSelf.playedTime = nil;
                NSLog(@"我就要停播放器");
                
            }
            
            weakSelf.stopButton.enabled = NO;
            [weakSelf.stopButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            
            weakSelf.progressSlider.value = 0;
            weakSelf.progressSlider.enabled = NO;
            weakSelf.currentPositionLabel.text = @"00:00:00";
            weakSelf.durationPostionLabel.text = @"00:00:00";
            if (_selectPlayerType == KHTPlayer_ARCPlay) {
                
                if (_isFinish == YES) {
                    weakSelf.hadpic = NO;
                    [weakSelf autoContinuePlayer];
                }
                
            }else
            {
                NSMutableArray *notiArr = [NSMutableArray arrayWithArray:[statusArr mutableCopy]];
                
                if (notiArr.count>0) {
                    
                    NSString *str = [[notiArr objectAtIndex:0] objectForKey:@"avPlayerEnd"];
                    if ([str isEqualToString:@"1"]) {
                        
                        [weakSelf autoContinuePlayer];
                        
                    }
                }
                
            }
            
            NSLog(@"stop掉player");
        }break;
        case DHPlayerStatus_pause:{
            NSLog(@"暂停播放器");
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
            [weakSelf.statusLable setText:@"Pause"];
            
        }break;
        case DHPlayerStatus_ready:{
            NSLog(@"准备播放");
            [weakSelf.statusLable setText:@"Ready"];
            [weakSelf.playButton setTag:ENGINE_STATUS_PAUSE];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
            [weakSelf.statusLable setText:@"Pause"];
            
            weakSelf.playButton.enabled = NO;
            
            [weakSelf reStartHiddenTimer];
            //            if (duration == 0)
            //                self.progressSlider.enabled = NO;
            //            else
            //                self.progressSlider.enabled = YES;
            
        }break;
        case DHPlayerStatus_failed:{
            NSLog(@"播放失败");
            [weakSelf.statusLable setText:@"Open failed"];
            [weakSelf.playButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            weakSelf.playButton.enabled = YES;
            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_netError];
            
        }break;
        case DHPlayerStatus_offline:{
            if (!statusArr) {
                NSLog(@"主播已下线");
                //记得 release掉播放器哦
                [self controlViewClearPlayer];
            }
        }break;
        case DHPlayerStatus_playing:{
            NSLog(@"playing");
            
            [weakSelf.statusLable setText:@"Playing"];
            
            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
            
        }break;
        case DHPlayerStatus_buffering:{
//            NSLog(@"buffering");
            
            NSMutableArray *notiArr = [NSMutableArray arrayWithArray:[statusArr mutableCopy]];
            if ([notiArr count]>0) {
                
                
                if (_selectPlayerType == KHTPlayer_ARCPlay) {
                    MLong lParam = [[notiArr objectAtIndex:1] intValue];
                    //                        UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
                    //                        MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
                    //                        MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];
                    MUInt32   bufferingProgress;
                    
                    bufferingProgress = lParam;
                    
                    NSString *text = [NSString stringWithFormat:@"%d%%", bufferingProgress];
                    if (bufferingProgress<100) {
                        NSLog(@"buffering progress-%@----- %d",text,weakSelf.hadpic);
                        
                        if (weakSelf.hadpic) {
                            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd];
                            
                        }else{
                            [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_activInd_mask];
                            
                        }
                        
                    }else{
                        [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];
                        
                    }
                }
                

                
            }
            
            
            
        }break;
        case DHPlayerStatus_connecting:{
            NSLog(@"connecting连接中");
            [weakSelf.statusLable setText:@"Connecting"];
            weakSelf.playButton.enabled = NO;
            
        }break;
        case DHPlayerStatus_sizeChanged:{
            NSLog(@"尺寸改变，或者 加载出第一帧来了");
            _hadpic = YES;
            
        }break;
        case DHPlayerStatus_MSG:{
            NSLog(@"MSG 干吗用的");
        }break;
        case DHPlayerStatus_startToPlay:{
            
            if ([statusArr count]>0) {
                
                NSMutableArray *notiArr = [NSMutableArray arrayWithArray:[statusArr mutableCopy]];
                
                MLong lParam = [[notiArr objectAtIndex:1] intValue];
                //                        UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
                //                        MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
                //                        MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];
                
                _playerDurationPosition = (CGFloat)lParam;
                
            }
            
            
            
            
//            if (_selectPlayerType == KHTPlayer_AVPlay) {
//                
//                if(!weakSelf.playedTime){
//                    weakSelf.playedTime = [NSTimer scheduledTimerWithTimeInterval:0.5 target:weakSelf selector:@selector(updateProgress) userInfo:nil repeats:YES];
//                }
//                
//                [weakSelf.playButton setTag:ENGINE_STATUS_PAUSE];
//                [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_pause"] forState:UIControlStateNormal];
//                [weakSelf.playedTime setFireDate:[NSDate date]];
//
//            }else
//            {
            
            
            NSLog(@"start to play");
            weakSelf.playButton.enabled = YES;
            weakSelf.progressSlider.enabled = YES;
            [weakSelf.stopButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [weakSelf.playButton setTag:ENGINE_STATUS_PLAY];
            [weakSelf.playButton setImage:[UIImage imageNamed:@"icon_ugc_player_play"] forState:UIControlStateNormal];
            [weakSelf.playedTime setFireDate:[NSDate distantFuture]];
            
            [weakSelf.statusLable setText:@"Playing"];
//            }
            
            
        }break;
            
        default:
            break;
    }
    
}


-(void)playerObserveDHPlayerErrorStatus:(DHPLAYERERRORSTATUS)errorStatus andErrorValueStatus:(int)errorValueStatus andErrorSting:(NSString*)errorString
{
    __weak typeof(&*self)weakSelf = self;
    switch (errorStatus) {
        case DHPLAYERERRORSTATUS_UNSUPPORTED_SCHEME:
        {
            ZSToast(errorString);
        }
            break;
        case DHPLAYERERRORSTATUS_NETWORK_CONNECTFAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_STREAM_OPEN:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_STREAM_SEEK:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATARECEIVE_TIMEOUT:
        {
            
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_FORMAT_UNSUPPORTED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_FORMAT_MALFORMED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DNS_RESOLVE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DNS_RESOLVE_TIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NETWORK_CONNECTIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATARECEIVE_FAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATASEND_TIMEOUT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATASEND_FAIL:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_DATAERROR_HTML:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS__SOURCE_BAD_VALUE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_BAD_VALUE_DISPLAY_INIT_FAILED:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NOAUDIO_VIDEOUNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_NOVIDEO_AUDIOUNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_AVCODEC_UNSUPPORT:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_OPERATION_CANNOTEXECUTE:
        {
            ZSToast(errorString);
            
        }
            break;
        case DHPLAYERERRORSTATUS_HTTPFAIL:
        {
            
            ZSToast(errorString);
            
        }
            break;
            
            
        default:
            break;
    }
                    [weakSelf.loadingView changeCenterUIWithType:kUGCLoading_none];

}


-(void)playerObserveDHPlayerMessageStatus:(DHPLAYERMESSAGESTATUS)messageStatus andMessageValueStatus:(int)messageValueStatus andMessageString:(NSString*)messageString
{
    switch (messageStatus) {
        case DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOVIDEO:
        {
            ZSToast(messageString);
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_SPLITTER_NOAUDIO:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_VCODEC_UNSUPPORTVIDEO:
        {
            ZSToast(messageString);
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_ACODEC_UNSUPPORTAUDIO:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_VCODEC_DECODE_ERROR:
        {
            ZSToast(messageString);
            
        }
            break;
        case DHPLAYERMESSAGESTATUS_INFO_ACODEC_DECODE_ERROR:
        {
            ZSToast(messageString);
            
        }
            break;
            
        default:
            break;
    }
}



#pragma mark 触摸手势事件


/**
 *  pan手势事件
 *
 *  @param pan UIPanGestureRecognizer
 */
- (void)panDirection:(UIPanGestureRecognizer *)pan
{
    //根据在view上Pan的位置，确定是调音量还是亮度
    CGPoint locationPoint = [pan locationInView:self];
    
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:self];
    
    // 判断是垂直移动还是水平移动
    // 判断是垂直移动还是水平移动
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{ // 开始移动
            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            if (x > y) { // 水平移动
                // 取消隐藏
                self.progressTimeView.hidden = NO;
                self.panDirection = PanDirectionHorizontalMoved;
                [self controlTouchBegin];
            }
            else if (x < y){ // 垂直移动
                self.panDirection = HTPanDirectionVerticalMoved;
                // 开始滑动的时候,状态改为正在控制音量
                if (locationPoint.x > self.bounds.size.width / 2) {
                    //                    self.isVolume = YES;
                    
                    _panDirection = PanDirectionVerticalVolumeMoved;
                }else { // 状态改为显示亮度调节
                    //                    self.isVolume = NO;
                    self.brightnessVODView.hidden = NO;
                    
                    _panDirection = PanDirectionVerticalBrighteMoved;
                }
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{ // 正在移动
            switch (self.panDirection) {
                case PanDirectionHorizontalMoved:{
                    [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
                    break;
                }
                case PanDirectionVerticalVolumeMoved:{
                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                    self.brightnessVODView.hidden = NO;
                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                    break;
                }
                    
                    //                case HTPanDirectionVerticalMoved:{
                    //                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                    //                    break;
                    //                }
                default:
                    break;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:{ // 移动停止
            // 移动结束也需要判断垂直或者平移
            // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
            switch (self.panDirection) {
                case PanDirectionHorizontalMoved:{
                    
                    
                    [self controlTouchEnd];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        // 隐藏视图
                        self.progressTimeView.hidden = YES;
                    });
                }
                case PanDirectionVerticalVolumeMoved:{
                    
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                    self.brightnessVODView.hidden = YES;
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                    
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    
}




-(void)controlTouchBegin
{

    // 给sumTime初值
    CMTime time       = self.player.avPlayerItem.currentTime;
    self.sumTime      = time.value/time.timescale;
    
    // 暂停视频播放
    [self.player pause];
    // 暂停timer
    [_playedTime setFireDate:[NSDate distantFuture]];
    
}



-(void)controlTouchEnd{
    
        // 继续播放
            [self.player play];
            [_playedTime setFireDate:[NSDate date]];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 隐藏视图
                self.progressTimeView.hidden = YES;
            });
            // 快进、快退时候把开始播放按钮改为播放状态
//            self.controlView.startVODBtn.selected = YES;
//            self.isPauseByUser                 = NO;
            
            [self.player progressSeekValue:self.sumTime];
            // 把sumTime滞空，不然会越加越多
            self.sumTime = 0;
}




/**
 *  pan水平移动的方法
 *
 *  @param value void
 */
- (void)horizontalMoved:(CGFloat)value
{
    //    // 快进快退的方法
    //    NSString *style = @"";
    //    if (value < 0) { style = @"<<"; }
    //    if (value > 0) { style = @">>"; }
    NSString *nowTime = @"";
    
    NSString *durationTime = @"";
    
    if (value >0) {
        
        [self.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_forward"]];
    }else
    {
        [self.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_backward"]];
    }
  
        // 每次滑动需要叠加时间
        self.sumTime += value / 200;
        
        // 需要限定sumTime的范围
        CMTime totalTime           = self.player.avPlayerItem.duration;
        CGFloat totalMovieDuration = ((CGFloat)totalTime.value/totalTime.timescale);
        if (self.sumTime > totalMovieDuration) { self.sumTime = totalMovieDuration;}
        if (self.sumTime < 0){ self.sumTime = 0; }
        
        
        // 当前快进的时间
            nowTime  = [self.player durationStringWithTime:(int)self.sumTime];
        // 总时间
           durationTime = [self.player durationStringWithTime:(int)totalMovieDuration];
        
    
    
    
    // 给label赋值
    //    self.controlView.horizontalLabel.text = [NSString stringWithFormat:@"%@ %@ / %@",style, nowTime, durationTime];
    
    
    [self.progressTimeLable_top setText:[NSString stringWithFormat:@"%@",nowTime]];
    [self.progressTimeLable_bottom setText:[NSString stringWithFormat:@"/%@",durationTime]];

    
}


/**
 *  pan垂直移动的方法
 *
 *  @param value void
 */
- (void)verticalMoved:(CGFloat)value
{
    //    self.isVolume ? (self.volumeViewSlider.value -= value / 10000) : ([UIScreen mainScreen].brightness -= value / 10000);
    
    NSInteger index = (NSInteger)value;
    
    switch (_panDirection) {
        case PanDirectionVerticalVolumeMoved:
            
        {
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素声音减一格
                    
                    [self volumeAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素声音增加一格
                    [self volumeAdd:+0.05];
                }
            }
            
            
        }
            break;
        case PanDirectionVerticalBrighteMoved:
        {
            
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素亮度减一格
                    [self brightnessAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素亮度增加一格
                    
                    [self brightnessAdd:0.05];
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
    
}


//声音增加
- (void)volumeAdd:(CGFloat)step{
    [HTVolumeUtil shareInstance].volumeValue += step;;
}
//亮度增加
- (void)brightnessAdd:(CGFloat)step{
    [UIScreen mainScreen].brightness += step;
    self.brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
}





-(void)autoContinuePlayer
{
    [self controlViewClearPlayer];
    
    [self controlViewActionPlay:NO];
    
    
    
}


-(void)updateProgress
{
   
    
    if (_selectPlayerType == KHTPlayer_ARCPlay) {
        
        _playerCurrentPosition = [_player currentPositon];
        
        _currentPositionLabel.text =[NSString stringWithFormat:@"%@",[_player durationStringWithTime:_playerCurrentPosition]] ;
        
        _durationPostionLabel.text =[NSString stringWithFormat:@"%@",[_player durationStringWithTime:_playerDurationPosition]];
        
        _progressSlider.value = (_playerCurrentPosition/_playerDurationPosition)*1000;
        
        
        if (_playerCurrentPosition>0&&_playerCurrentPosition>0) {
            
            
            if (_currentPositionLabel.text>=_durationPostionLabel.text) {
                
                _playerCurrentPosition = _playerDurationPosition;
                _isFinish = YES;
            }
        }

    }else
    {
        if (_player.avPlayerItem.duration.timescale != 0) {
            _progressSlider.value     = (CMTimeGetSeconds([_player.avPlayerItem currentTime]) / (_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale))*1000;//当前进度
            
            _currentPositionLabel.text = [_player durationStringWithTime:(int)CMTimeGetSeconds([_player.avPlayer currentTime])];
            _durationPostionLabel.text   = [_player durationStringWithTime:(int)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale];
            
            
            
//            CGFloat total           = ((CGFloat)_player.avPlayerItem.duration.value / _player.avPlayerItem.duration.timescale)/1000;
//            
//            CGFloat current        = ((CGFloat)[_player.avPlayer currentTime].value / _player.avPlayerItem.duration.timescale)/1000;
//
//            if (current>=total) {
//                
//                _isFinish = YES;
//            }
        }

        
    }
    
}







@end
