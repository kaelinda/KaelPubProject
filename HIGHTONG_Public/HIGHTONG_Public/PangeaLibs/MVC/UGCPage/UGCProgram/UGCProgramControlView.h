//
//  UGCProgramControlView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/4/1.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGCPlayer.h"
#import "UGCLoadingView.h"
typedef void(^HT_UGC_PlayerGoBackBlock)();
//// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, HTPanDirection){
    HTPanDirectionHorizontalMoved, //横向移动
    HTPanDirectionVerticalMoved    //纵向移动
};
@interface UGCProgramControlView : UIView
@property (nonatomic,strong)UIView *playerBV;
@property (nonatomic,strong)UGCPlayer *player;
@property (nonatomic,strong)UIButton *playButton;
@property (nonatomic,strong)UIImageView *bottomBarIV;
@property (nonatomic,strong)UIImageView *topBarIV;
@property (nonatomic,strong)UIButton *stopButton;
@property (nonatomic,strong)UILabel *statusLable;
@property (nonatomic,assign)CGSize playerSize;
@property(nonatomic, strong)UISlider        *progressSlider;
@property (nonatomic,strong)UILabel *currentPositionLabel;
@property (nonatomic,strong)UILabel *durationPostionLabel;
@property (nonatomic,strong)UIButton *returnButton;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *shareButton;
@property (nonatomic,copy)HT_UGC_PlayerGoBackBlock goBackBlock;
@property (nonatomic,strong)UGCLoadingView *loadingView;
@property (nonatomic,strong)NSString *filePathStr;
@property (nonatomic,assign)BOOL  isHTALVideoEditType;
@property (nonatomic,assign)HTPlayerType selectPlayerType;
@property (nonatomic,assign) BOOL hadpic;
@property (nonatomic,assign)BOOL isByUserPause;
@property (nonatomic,strong)NSString *titleLabelStr;
/**
 * /进度条view
 */
@property (nonatomic,strong)UIImageView *progressTimeView;
@property (nonatomic,strong)UIImageView *progressDirectionIV;
@property (nonatomic,strong)UILabel *progressTimeLable_top;
@property (nonatomic,strong)UILabel *progressTimeLable_bottom;

- (instancetype)initWithPlayerType:(HTPlayerType)playerType andFatherFrame:(CGRect)fatherView isALVideoEdite:(BOOL)isALVideoEdite;
-(void)controlViewActionPlay:(BOOL)isPlayButton;
-(void)controlViewClearPlayer;
-(void)backPopViewController;

@end
