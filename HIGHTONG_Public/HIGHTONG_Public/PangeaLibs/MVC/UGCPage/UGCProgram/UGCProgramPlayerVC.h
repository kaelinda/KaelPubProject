//
//  UGCProgramPlayerVC.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "ShareView.h"
@interface UGCProgramPlayerVC : HT_FatherViewController
@property (nonatomic,strong)NSString *programID;
@property (nonatomic,assign)BOOL IsConstraintLandscape;
@property (nonatomic,strong)NSString *programType;
@property (nonatomic,strong)NSString *programTitleStr;

-(void)ugcProgramProgramID:(NSString *)programID andProgramType:(NSString *)programType andIsConstraintLandscape:(BOOL)isConstraintLandscape andprogramTitleStr:(NSString *)programTitleStr;

@end
