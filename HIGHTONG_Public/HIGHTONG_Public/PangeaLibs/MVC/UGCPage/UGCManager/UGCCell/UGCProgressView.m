//
//  UGCProgressView.m
//  
//
//  Created by 宋博闻 on 17/3/24.
//  Copyright (c) 2017年 宋博闻. All rights reserved.
//

#import "UGCProgressView.h"

@implementation UGCProgressView

- (UILabel *)label
{
    if (_label == nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 34*kDeviceRate, 34*kDeviceRate)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = HT_FONT_FIFTH;
        label.textColor = UIColorFromRGB(0x3ee13a);
        label.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        [self addSubview:label];
        _label = label;
    }
    return _label;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];

    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();//获取上下文
    
    CGPoint center = CGPointMake(18*kDeviceRate, 18*kDeviceRate);  //设置圆心位置
    CGFloat radius = 17*kDeviceRate;  //设置半径
    CGFloat startA = - M_PI_2;  //圆起点位置
    CGFloat endA = -M_PI_2 + M_PI * 2 * _progress;  //圆终点位置
    
    CGFloat endB =  M_PI * 3/2;  //圆终点位置

    UIBezierPath *path1 = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endB clockwise:YES];
    
    CGContextSetLineWidth(ctx, 1*kDeviceRate); //设置线条宽度
    [UIColorFromRGB(0xd9eef9) setStroke]; //设置描边颜色
    
    CGContextAddPath(ctx, path1.CGPath); //把路径添加到上下文
    
    CGContextStrokePath(ctx);  //渲染
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];
    
    CGContextSetLineWidth(ctx, 1*kDeviceRate); //设置线条宽度
    [UIColorFromRGB(0x50c7ff) setStroke]; //设置描边颜色
    
    CGContextAddPath(ctx, path.CGPath); //把路径添加到上下文
    
    CGContextStrokePath(ctx);  //渲染
    
}

- (void)drawProgress:(CGFloat )progress
{
    _progress = progress;
    
    NSLog(@"progress=====%f",_progress);
    
    [self setNeedsDisplay];
    
}

@end
