//
//  UGCUpLoadedCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGCUpLoadedCell : UITableViewCell

@property (nonatomic, strong)UIImageView *backImg;//cell底图

@property (nonatomic, strong)UIImageView *linkImg;//封面

@property (nonatomic, strong)UILabel *linkName;//台名

@property (nonatomic, strong)UILabel *linkTags;//描述

@property (nonatomic, strong)UILabel *linkSign;//标记

@property (nonatomic, strong)UIImageView *choiceImg;//选中标记

@property (nonatomic, strong)UIImageView *UnChoiceImg;//未选中标记

@property (nonatomic, strong)UIImageView *cellArrowImg;//箭头图片

@property (nonatomic, strong)NSMutableDictionary *cellDic;//cell的数据源

/**
 *  @author dirk
 *
 *  @brief 是否是删除状态
 */
@property (nonatomic,assign) BOOL isDeleteStatus;

//更新方法
- (void)upDate;

@end
