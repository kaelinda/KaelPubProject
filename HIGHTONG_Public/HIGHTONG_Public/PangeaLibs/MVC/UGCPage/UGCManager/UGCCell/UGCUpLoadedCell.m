//
//  UGCUpLoadedCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUpLoadedCell.h"

@interface UGCUpLoadedCell()

@end

@implementation UGCUpLoadedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self setupUI];
        
        self.cellDic = [NSMutableDictionary dictionary];
        
    }
    
    return self;
}

- (void)setupUI
{
    
    _backImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 82*kDeviceRate)];
    _backImg.backgroundColor = App_background_color;
    [self.contentView addSubview:_backImg];
    
    //台标
    self.linkImg = [[UIImageView alloc] init];
    self.linkImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.linkImg];
    
    //名称
    self.linkName = [[UILabel alloc] init];
    self.linkName.textColor = HT_COLOR_FONT_FIRST;
    self.linkName.font = HT_FONT_SECOND;
    self.linkName.textAlignment = NSTextAlignmentLeft;
    [_backImg addSubview:self.linkName];
        
    //描述
    self.linkTags = [[UILabel alloc] init];
    self.linkTags.textColor = HT_COLOR_FONT_SECOND;
    self.linkTags.font = HT_FONT_FOURTH;
    self.linkTags.textAlignment = NSTextAlignmentLeft;
    [_backImg addSubview:self.linkTags];
    
    //标签
    self.linkSign = [[UILabel alloc] init];
    self.linkSign.textColor = App_selected_color;
    self.linkSign.font = HT_FONT_FOURTH;
    self.linkSign.textAlignment = NSTextAlignmentLeft;
    
    [_backImg addSubview:self.linkSign];
    
    //选中图标
    self.choiceImg = [[UIImageView alloc]init];
    [self.choiceImg setImage:[UIImage imageNamed:@"sl_upload_delete_checkbox_checked"]];
    self.choiceImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.choiceImg];
    self.choiceImg.hidden = YES;
    
    //未选中图标
    self.UnChoiceImg = [[UIImageView alloc]init];
    [self.UnChoiceImg setImage:[UIImage imageNamed:@"sl_upload_delete_checkbox_normal"]];
    self.UnChoiceImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.UnChoiceImg];
    self.UnChoiceImg.hidden = YES;
    
    //箭头图标
    self.cellArrowImg = [[UIImageView alloc]init];
    [self.cellArrowImg setImage:[UIImage imageNamed:@"ic_upload_enter"]];
    self.cellArrowImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.cellArrowImg];
    
}

- (void)upDate
{
    
    [self refreshUI];
    
    [self refreshDate];
    
}

- (void)refreshUI
{
    
    if (_isDeleteStatus) {//删除状态
        
        //更新选中位置
        [self.choiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(20*kDeviceRate));
            make.width.equalTo(@(20*kDeviceRate));
                
        }];
        
        //更新未选中位置
        [self.UnChoiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(20*kRateSize));
            make.width.equalTo(@(20*kRateSize));
        }];
        
        //更新台标位置
        [self.linkImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_choiceImg.mas_right).offset(20*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(62*kDeviceRate));
            make.width.equalTo(@(110*kDeviceRate));
        }];
        
        //更新箭头位置
        [self.cellArrowImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backImg.mas_right);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(0*kDeviceRate));
            make.width.equalTo(@(0*kDeviceRate));
        }];
        
    }else
    {
        
        //更新选中位置
        [self.choiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(0*kDeviceRate));
            make.width.equalTo(@(0*kDeviceRate));
            
        }];
        
        //更新未选中位置
        [self.UnChoiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(0*kRateSize));
            make.width.equalTo(@(0*kRateSize));
        }];
        
        //更新台标位置
        [self.linkImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(62*kDeviceRate));
            make.width.equalTo(@(110*kDeviceRate));
        }];
        
        //更新箭头位置
        [self.cellArrowImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backImg.mas_right).offset(-12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(20*kDeviceRate));
            make.width.equalTo(@(20*kDeviceRate));
        }];
        
    }
    
    //更新视频名称位置
    [self.linkName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.linkImg.mas_right).offset(6*kDeviceRate);
        make.top.equalTo(self.backImg.mas_top).offset(15*kDeviceRate);
        make.height.equalTo(@(18*kDeviceRate));
        make.right.equalTo(self.backImg.mas_right).offset(-22*kDeviceRate);
    }];

    //更新视频描述位置
    [self.linkTags mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.linkImg.mas_right).offset(6*kDeviceRate);
        make.top.equalTo(self.linkName.mas_bottom).offset(8*kDeviceRate);
        make.height.equalTo(@(10*kDeviceRate));
        make.right.equalTo(self.backImg.mas_right).offset(-22*kDeviceRate);
    
    }];
    
    //更新视频标签位置
    [self.linkSign mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.linkImg.mas_right).offset(6*kDeviceRate);
        make.top.equalTo(self.linkTags.mas_bottom).offset(8*kDeviceRate);
        make.height.equalTo(@(10*kDeviceRate));
        make.right.equalTo(self.backImg.mas_right).offset(-22*kDeviceRate);
    }];
    
}

- (void)refreshDate
{
    
    if (NotEmptyStringAndNilAndNull([_cellDic objectForKey:@"cover"] )) {
        
        NSLog(@"%@",[NSURL URLWithString:NSStringPM_IMGFormat([_cellDic objectForKey:@"cover"])] );

        [self.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([_cellDic objectForKey:@"cover"])] placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"]];
            
    }else
    {
        [self.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([_cellDic objectForKey:@""])]placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"]];
    }
    
    
    [self.cellArrowImg setImage:[UIImage imageNamed:@"ic_upload_enter"]];
    
    self.linkName.text = [_cellDic objectForKey:@"name"];
    
    
    
    if (NotEmptyStringAndNilAndNull([_cellDic objectForKey:@"remarks"] )) {
        self.linkTags.text = [_cellDic objectForKey:@"remarks"];
    }else
    {
        self.linkTags.text = @"";
    }
    
    
    
    
    self.linkSign.text = [_cellDic objectForKey:@"tegsNames"];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
