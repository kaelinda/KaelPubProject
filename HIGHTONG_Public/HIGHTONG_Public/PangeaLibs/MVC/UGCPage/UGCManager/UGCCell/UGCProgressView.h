//
//  UGCProgressView.h
// 
//
//  Created by 宋博闻 on 17/3/24.
//  Copyright (c) 2017年 宋博闻. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGCProgressView : UIView

@property (assign, nonatomic)CGFloat progress;

@property (nonatomic, strong) UILabel *label;

- (void)drawProgress:(CGFloat )progress;


@end
