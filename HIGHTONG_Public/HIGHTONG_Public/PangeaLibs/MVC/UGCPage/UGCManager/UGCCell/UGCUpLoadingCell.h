//
//  UGCUpLoadingCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGCProgressView.h"


@interface UGCUpLoadingCell : UITableViewCell

@property (nonatomic, strong)UIImageView *backImg;//cell底图

@property (nonatomic, strong)UILabel *linkName;//台名

@property (nonatomic, strong)UILabel *linkTime;//时间

@property (nonatomic, strong)UILabel *linkNet;//网络状态

@property (nonatomic, strong)UIImageView *choiceImg;//选中标记

@property (nonatomic, strong)UIImageView *UnChoiceImg;//未选中标记
    
@property (nonatomic, strong)UIImageView *faultSign;//失败标记

@property (nonatomic, strong)NSMutableDictionary *cellDic;//cell的数据源
    
@property (nonatomic, strong)UGCProgressView *upLoadingTaskProgressView;//进度条



/**
 *  @author dirk
 *
 *  @brief 是否是删除状态
 */
@property (nonatomic,assign) BOOL isDeleteStatus;

//更新方法
- (void)upDate;


@end
