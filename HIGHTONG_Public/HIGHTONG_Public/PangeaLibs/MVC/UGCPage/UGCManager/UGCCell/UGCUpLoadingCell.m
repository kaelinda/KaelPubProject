//
//  UGCUpLoadingCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/24.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUpLoadingCell.h"

@interface UGCUpLoadingCell()

@end

@implementation UGCUpLoadingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self setupUI];
        
    }
    
    return self;
}

- (void)setupUI
{
   
    _backImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 70*kDeviceRate)];
    _backImg.backgroundColor = App_background_color;
    [self.contentView addSubview:_backImg];
    
    //名称
    self.linkName = [[UILabel alloc] init];
    self.linkName.textColor = HT_COLOR_FONT_FIRST;
    self.linkName.font = HT_FONT_SECOND;
    self.linkName.textAlignment = NSTextAlignmentLeft;
    [_backImg addSubview:self.linkName];
    
    //时间
    self.linkTime = [[UILabel alloc] init];
    self.linkTime.textColor = HT_COLOR_FONT_SECOND;
    self.linkTime.font = HT_FONT_FOURTH;
    self.linkTime.textAlignment = NSTextAlignmentLeft;
    [_backImg addSubview:self.linkTime];
    
    //网络状态
    self.linkNet = [[UILabel alloc] init];
    self.linkNet.textColor = HT_COLOR_FONT_ASSIST;
    self.linkNet.font = HT_FONT_FOURTH;
    self.linkNet.textAlignment = NSTextAlignmentRight;
    
    [_backImg addSubview:self.linkNet];
    
    //选中图标
    self.choiceImg = [[UIImageView alloc]init];
    [self.choiceImg setImage:[UIImage imageNamed:@"sl_upload_delete_checkbox_checked"]];
    self.choiceImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.choiceImg];
    self.choiceImg.hidden = YES;
    
    //未选中图标
    self.UnChoiceImg = [[UIImageView alloc]init];
    [self.UnChoiceImg setImage:[UIImage imageNamed:@"sl_upload_delete_checkbox_normal"]];
    self.UnChoiceImg.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.UnChoiceImg];
    self.UnChoiceImg.hidden = YES;
    
    //失败图标
    self.faultSign = [[UIImageView alloc]init];
    [self.faultSign setImage:[UIImage imageNamed:@"btn_uploaditem_error"]];
    self.faultSign.contentMode = UIViewContentModeScaleAspectFit;
    [_backImg addSubview:self.faultSign];
    self.faultSign.hidden = YES;
    
    self.upLoadingTaskProgressView = [[UGCProgressView alloc]initWithFrame:CGRectMake(kDeviceWidth - 48*kDeviceRate, 17*kDeviceRate, 36*kDeviceRate, 36*kDeviceRate)];
    [_backImg addSubview:self.upLoadingTaskProgressView];

    
}

- (void)upDate
{
    
    [self refreshUI];
    
    [self refreshDate];
    
}

- (void)refreshUI
{
    
    if (_isDeleteStatus) {//删除状态
        
        //更新选中位置
        [self.choiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(20*kDeviceRate));
            make.width.equalTo(@(20*kDeviceRate));
            
        }];
        
        //更新未选中位置
        [self.UnChoiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(20*kRateSize));
            make.width.equalTo(@(20*kRateSize));
        }];
        
        //更新视频名称位置
        [self.linkName mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.backImg.mas_left).offset(52*kDeviceRate);
            make.top.equalTo(self.backImg.mas_top).offset(15*kDeviceRate);
            make.height.equalTo(@(18*kDeviceRate));
            make.width.equalTo(@(200*kDeviceRate));
        }];
        
        //更新视频时间位置
        [self.linkTime mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.backImg.mas_left).offset(52*kDeviceRate);
            make.top.equalTo(self.linkName.mas_bottom).offset(12*kDeviceRate);
            make.height.equalTo(@(10*kDeviceRate));
            make.width.equalTo(@(120*kDeviceRate));
            
        }];
        
    }else
    {
        
        //更新选中位置
        [self.choiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(0*kDeviceRate));
            make.width.equalTo(@(0*kDeviceRate));
            
        }];
        
        //更新未选中位置
        [self.UnChoiceImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(0*kRateSize));
            make.width.equalTo(@(0*kRateSize));
        }];
        
        //更新视频名称位置
        [self.linkName mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.backImg.mas_left).offset(12*kDeviceRate);
            make.top.equalTo(self.backImg.mas_top).offset(15*kDeviceRate);
            make.height.equalTo(@(18*kDeviceRate));
            make.width.equalTo(@(200*kDeviceRate));
        }];
        
        //更新视频时间位置
        [self.linkTime mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.backImg.mas_left).offset(12*kDeviceRate);
            make.top.equalTo(self.linkName.mas_bottom).offset(12*kDeviceRate);
            make.height.equalTo(@(10*kDeviceRate));
            make.width.equalTo(@(120*kDeviceRate));
            
        }];
    }
    
    //更新视频网络状态位置
    [self.linkNet mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backImg.mas_right).offset(-62*kDeviceRate);
        make.centerY.equalTo(self.linkTime.mas_centerY);
        make.height.equalTo(@(12*kDeviceRate));
        make.width.equalTo(@(60*kDeviceRate));
    }];
    
    //更新失败标记位置
    [self.faultSign mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backImg.mas_right).offset(-13*kDeviceRate);
        make.bottom.equalTo(self.backImg.mas_bottom).offset(-13*kDeviceRate);
        make.height.equalTo(@(16*kDeviceRate));
        make.width.equalTo(@(16*kDeviceRate));
    }];
    
    //更新任务进度控件位置
    [self.upLoadingTaskProgressView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backImg.mas_right).offset(-12*kDeviceRate);
        make.centerY.equalTo(self.backImg.mas_centerY);
        make.height.equalTo(@(36*kDeviceRate));
        make.width.equalTo(@(36*kDeviceRate));
    }];
    
}

- (void)refreshDate
{
    
    self.linkName.text = [_cellDic objectForKey:@"name"];

    self.linkTime.text = [_cellDic objectForKey:@"time"];
    
    if ([[_cellDic objectForKey:@"status"]isEqualToString:@"0"]) {
        
        self.linkNet.text = @"上传失败";
        
        self.linkNet.textColor = UIColorFromRGB(0xf35959);
        
        self.faultSign.hidden = NO;
        
        self.upLoadingTaskProgressView.hidden = YES;
        
    }else if([[_cellDic objectForKey:@"status"]isEqualToString:@"1"])
    {
        
        self.linkNet.text = @"等待上传";
        
        self.linkNet.textColor = HT_COLOR_FONT_ASSIST;
        
        self.faultSign.hidden = YES;
        
        self.upLoadingTaskProgressView.hidden = YES;
        
    }else
    {
        
        self.faultSign.hidden = YES;
        
        self.linkNet.textColor = HT_COLOR_FONT_ASSIST;
        
        self.upLoadingTaskProgressView.hidden = NO;
        
    }    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
