//
//  UGCUpLoadedHeaderView.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/31.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUpLoadedHeaderView.h"

@implementation UGCUpLoadedHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xfef9f3);
        
        
        self.headerViewLabel = [[UILabel alloc]init];
        self.headerViewLabel.textColor = HT_COLOR_FONT_SECOND;
        self.headerViewLabel.font = HT_FONT_FIFTH;
        self.headerViewLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:self.headerViewLabel];
        
        //更新视频名称位置
        [self.headerViewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
    }
    return self;
}

- (void)changeTitleWithUnReviewedNum:(NSString *)unReviewedNum
{
    
    NSString *tempStr = [NSString stringWithFormat:@"你有%@个审核未通过，点击查看",unReviewedNum];
    
    NSArray *number = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    NSMutableAttributedString *attributeString  = [[NSMutableAttributedString alloc]initWithString:tempStr];
    
    for (int i = 0; i < tempStr.length; i ++) {
        //这里的小技巧，每次只截取一个字符的范围
        NSString *a = [tempStr substringWithRange:NSMakeRange(i, 1)];
        //判断装有0-9的字符串的数字数组是否包含截取字符串出来的单个字符，从而筛选出符合要求的数字字符的范围NSMakeRange
        if ([number containsObject:a]) {
            [attributeString setAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xff0000),NSFontAttributeName:HT_FONT_FIFTH} range:NSMakeRange(i, 1)];
        }
        
    }
    //完成查找数字，最后将带有字体下划线的字符串显示在UILabel上
    self.headerViewLabel.attributedText = attributeString;
}

@end
