//
//  UGCUpLoadedHeaderView.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/31.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGCUpLoadedHeaderView : UIView

@property(nonatomic,strong)UILabel *headerViewLabel;

- (void)changeTitleWithUnReviewedNum:(NSString *)unReviewedNum;

@end
