//
//  UGCUpLoadingManagerViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

typedef void(^UGCUpLoadingManagerViewController_Block)();

@interface UGCUpLoadingManagerViewController : HT_FatherViewController


@property (nonatomic,strong) UGCUpLoadingManagerViewController_Block headerClick;

/**
 *  @author dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar;

/**
 *  @author dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedTabBar;

/**
 *  @author dirk
 *
 *  @brief 是否是删除状态
 */
@property (nonatomic,assign) BOOL isDeleteStatus;

//更新头视图
- (void)reloadTableViewHeaderWithUnReview:(NSString *)unReview;

@end
