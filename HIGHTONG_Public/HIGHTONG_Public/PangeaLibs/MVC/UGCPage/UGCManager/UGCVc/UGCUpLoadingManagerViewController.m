//
//  UGCUpLoadingManagerViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUpLoadingManagerViewController.h"
#import "UGCUpLoadingCell.h"
#import "HTUGCUpLoadInterface.h"
#import "UGCUploadManager.h"
#import "UGCProgressView.h"
#import "DataPlist.h"
#import "MyGesture.h"
#import "CustomAlertView.h"
#import "UGCUpLoadedHeaderView.h"
#import "GeneralPromptView.h"
#import "HTALViewController.h"
#import "UrlPageJumpManeger.h"
#import "KnowledgeBaseViewController.h"

@interface UGCUpLoadingManagerViewController ()<UITableViewDelegate,UITableViewDataSource,CustomAlertViewDelegate>
{
    
    UGCUpLoadedHeaderView *_headerView;
    
    BOOL first;
}
    
@property (nonatomic, strong)UIImageView *addView;
    
@property (nonatomic, strong)UIButton *addBtn;

@property (nonatomic, strong)UIButton *deleteBtn;//编辑底部添加按钮

@property (nonatomic, strong)UIImageView *deleteView;//编辑底部添加视图

@property (nonatomic, strong)UITableView *upLoadingTaskView;//编辑底部添加视图

@property (nonatomic, assign)CGFloat uptask;//编辑底部添加视图

@property (nonatomic, strong)NSMutableArray *selectArr;//选择数组

@property (nonatomic, strong)NSMutableArray *upLoadTaskArr;//任务数组

@property (nonatomic, strong)UGCUploadManager *ugcManager;//任务数组
    
@property (nonatomic, strong)NSString *upLoadingSpeed;

@property (nonatomic, strong)NSString *upLoadingIndexStr;

@property (nonatomic, strong)CustomAlertView *addNewVideocustom;
@property (nonatomic, strong)CustomAlertView *deleteVideocustom;

@property (nonatomic,strong)GeneralPromptView *promptView;

    
    
@end

@implementation UGCUpLoadingManagerViewController

-(instancetype)init{
    self = [super init];
    if (self) {
        
        _isNeedNavBar = NO;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideNaviBar:!_isNeedNavBar];
    // Do any additional setup after loading the view.
    
    self.upLoadTaskArr = [NSMutableArray array];
    
    self.selectArr = [NSMutableArray array];
    
    self.upLoadingSpeed = [[NSString alloc]init];
    
    first =YES;
    
    _ugcManager = [UGCUploadManager shareInstance];
    
    [self setupTableView];
    
    [self deleteViewSetup];
    
    [self addViewSetup];
    
    [self promptViewSetup];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reFreshUI) name:@"UGC_STARTUPLOADTASK" object:nil];
        
    });
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if (first) {
        

        
        first = NO;
    }

    
    [self showCustomeHUD];
    
    if (_isDeleteStatus) {
        
        _deleteView.hidden = NO;
        
        _addView.hidden = YES;

    }else
    {
        
        [self.selectArr removeAllObjects];
        
        [self callCheckDataListSelected];
        
        _deleteView.hidden = YES;
        
        _addView.hidden = NO;
        
    }
    
    //开始任务管理器操作
    [self upLoadManagerTask];
    
    [self reFreshUI];

}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)reFreshUI
{
    
    [self.upLoadTaskArr removeAllObjects];
    
    [self.upLoadingTaskView reloadData];
    
    //获取最新数据源，刷新界面
    NSMutableArray *tempArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
    
    for (NSMutableDictionary *dic in tempArr) {
        
        if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
            
            self.upLoadTaskArr = [dic objectForKey:@"list"];
            
            break;
            
        }
    }
    
    if (self.upLoadTaskArr.count == 0) {
        
        _promptView.hidden = NO;
        
        _upLoadingTaskView.backgroundColor = [UIColor clearColor];
        
        _upLoadingTaskView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }else
    {
        _promptView.hidden = YES;
        
        _upLoadingTaskView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        
        _upLoadingTaskView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    
    [self hiddenCustomHUD];
    
    [self.upLoadingTaskView reloadData];
    
}
    
- (void)upLoadManagerTask
{
    
    WS(ws);
    //结果回调
    _ugcManager.UGCUploadManagerResultBlock = ^(NSInteger status,NSString *sign){
        
        if (ws.isDeleteStatus) {
            
            if (status == 0) {
                
                if (ws.selectArr.count) {
          
                    for (int i=0; i<ws.selectArr.count; i++) {
                        
                        NSString *cellID = [ws.selectArr objectAtIndex:i];
                        
                        if ([cellID isEqualToString:sign]) {
                            
                            [ws.selectArr removeObjectAtIndex:i];
                            
                            [ws callCheckDataListSelected];
                            
                            break;
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        ws.upLoadingIndexStr = nil;
        
        ws.upLoadingSpeed = @"等待网络";
        
        ws.uptask = 0;

        [ws reFreshUI];
        
    };

    
    //速度,进度回调
    _ugcManager.UGCUploadManagerSpeedBlock = ^(NSString *speed,CGFloat uploadProgress){
        
        ws.upLoadingSpeed = [NSString stringWithFormat:@"%@/s",speed];
        
        ws.uptask = uploadProgress;
    
        NSLog(@"cell里面上传速度------%@,cell里面上传进度------%f",ws.upLoadingSpeed,ws.uptask);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (ws.upLoadingIndexStr.length) {
                
                NSIndexPath *indexPath=[NSIndexPath indexPathForRow:[ws.upLoadingIndexStr integerValue] inSection:0];
                [ws.upLoadingTaskView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        });
        
    };
    
}

#pragma mark - tableView 代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 70*kDeviceRate;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.upLoadTaskArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic = [self.upLoadTaskArr objectAtIndex:indexPath.row];
    
    //热门电影视频列表
    UGCUpLoadingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uploading"];
    
    if (cell == nil) {
        
        cell = [[UGCUpLoadingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uploading"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    if ([[dic objectForKey:@"status"]isEqualToString:@"2"]) {
        
        self.upLoadingIndexStr = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
        
//        upLoadingIndex = indexPath.row;
        
        cell.upLoadingTaskProgressView.label.text = [NSString stringWithFormat:@"%.0f%%", _uptask*100];
        
        if (self.upLoadingSpeed.length) {
            cell.linkNet.text = self.upLoadingSpeed;
        }
        else
        {
            cell.linkNet.text = @"等待网络";
        }
        
        
        [cell.upLoadingTaskProgressView drawProgress:_uptask];

    }
    
    cell.isDeleteStatus = _isDeleteStatus;
    
    cell.cellDic = dic;
    
    [cell upDate];
    
    if (_isDeleteStatus) {
        
        cell.userInteractionEnabled = YES;
        
        cell.choiceImg.hidden = YES;
        
        cell.UnChoiceImg.hidden = NO;
        
        if (self.selectArr.count) {
            
            for (NSString *tempID in self.selectArr) {
                
                if ([tempID isEqualToString:[dic objectForKey:@"sign"]]) {
                    
                    cell.choiceImg.hidden = NO;
                    
                    cell.UnChoiceImg.hidden = YES;
                    
                    break;
                    
                }
                
            }
            
        }else
        {
            
        }

        
    }
    else
    {
        
        if ([[dic objectForKey:@"status"]isEqualToString:@"0"]) {
            
            cell.userInteractionEnabled = YES;
            
        }else
        {
            cell.userInteractionEnabled = NO;
        }
        
    }
    
    return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"didSelectRowAtIndexPath----%@",indexPath);
    
        NSMutableDictionary *dic = [self.upLoadTaskArr objectAtIndex:indexPath.row];;
    
    if (_isDeleteStatus) {
        
        NSString *sign = [dic objectForKey:@"sign"];
        
        if (self.selectArr.count) {
            
            BOOL flag = false;
            
            for (int i=0; i<self.selectArr.count; i++) {
                
                NSString *cellID = [self.selectArr objectAtIndex:i];
                
                if ([cellID isEqualToString:sign]) {
                    
                    [self.selectArr removeObjectAtIndex:i];
                    
                    flag = true;
                    
                    break;
                }
                
            }
            
            if (!flag) {
                
                [self.selectArr addObject:sign];
                
            }
            
        }else
        {
            
            [self.selectArr addObject:sign];
            
        }
        

        [self.upLoadingTaskView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        
        [self callCheckDataListSelected];

        
    }else
    {
        
        NSMutableArray *tempUpLoadTaskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
        
        if (tempUpLoadTaskArr.count > 0) {
            
            for (NSDictionary *dic in tempUpLoadTaskArr) {
                
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                    
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                    
                    for (NSMutableDictionary *tempDic in tempArr) {
                        
                        if ([[tempDic objectForKey:@"status"]isEqualToString:@"0"]) {
                            
                            [tempDic setObject:@"1" forKey:@"status"];
                            
                            NSLog(@"失败重新开始时写操作");
                            [DataPlist WriteToPlistArray:tempUpLoadTaskArr ToFile:UGC_UPLOADING_LIST_PLIST];
                            
                            self.upLoadTaskArr = tempArr;
                            
//                            [self.upLoadingTaskView reloadData];
                            
                            [self.upLoadingTaskView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
                            
                            [_ugcManager startUGCUploadManager];
                            
                            break;
                               
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
    }

}

- (void)setupTableView
{
    
    _upLoadingTaskView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 176*kDeviceRate)];

    _upLoadingTaskView.dataSource = self;
    
    _upLoadingTaskView.delegate = self;
    
    _upLoadingTaskView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    _upLoadingTaskView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_upLoadingTaskView];
    
    [_upLoadingTaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
    }];
    
    _headerView = [[UGCUpLoadedHeaderView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 30*kDeviceRate)];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerViewClick)];
    
    [_headerView addGestureRecognizer:tap];
    
}

- (void)deleteViewSetup
{
    
    //页面底端增加删除按钮
    _deleteView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-176*kDeviceRate, kDeviceWidth, 60*kDeviceRate)];
    //    [_editDeleteView setImage:[UIImage imageNamed:@"bg_user_add_fav_channel"]];
    _deleteView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_deleteView];
    _deleteView.userInteractionEnabled = YES;
    _deleteView.hidden = YES;
    
    [_deleteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@(60*kDeviceRate));
    }];
    
    UIView *editVerticalLine = [[UIView alloc]init];
    [_deleteView addSubview:editVerticalLine];
    editVerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [editVerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_deleteView.mas_top).offset(1);
        make.left.equalTo(_deleteView.mas_left);
        make.right.equalTo(_deleteView.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _deleteBtn = [[UIButton alloc]init];
    [_deleteView addSubview:_deleteBtn];
    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_deleteView.mas_centerX);
        make.centerY.equalTo(_deleteView.mas_centerY);
    }];
    
    _deleteBtn.titleLabel.font = HT_FONT_FIRST;
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"sl_upload_del_normal"] forState:UIControlStateNormal];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"sl_upload_del_check"] forState:UIControlStateHighlighted];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"ic_user_common_no_delete_item"] forState:UIControlStateDisabled];
    [_deleteBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_deleteBtn setTitle:@"删    除"forState:UIControlStateDisabled];
    _deleteBtn.enabled = NO;
    
    [_deleteBtn addTarget:self action:@selector(deleteUpLoadTask) forControlEvents:UIControlEventTouchUpInside];
    
}
    
- (void)addViewSetup
{
        
    //页面底端增加频道收藏条
    _addView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-176*kDeviceRate, kDeviceWidth, 60*kDeviceRate)];
        
    _addView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_addView];
    _addView.userInteractionEnabled = YES;
    _addView.hidden = YES;
    
    [_addView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@(60*kDeviceRate));
    }];
    
    UIView *VerticalLine = [[UIView alloc]init];
    [_addView addSubview:VerticalLine];
    VerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_addView.mas_top).offset(1);
        make.left.equalTo(_addView.mas_left);
        make.right.equalTo(_addView.mas_right);
        make.height.equalTo(@(1));
    }];
        
    UIImageView *addImg = [[UIImageView alloc] init];
    [addImg setImage:[UIImage imageNamed:@"sl_upload_new_normal"]];
    [_addView addSubview:addImg];
        
    [addImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_addView.mas_centerX);
        make.centerY.equalTo(_addView.mas_centerY);
    }];
        
    MyGesture *addGes = [[MyGesture alloc] initWithTarget:self action:@selector(addUpLoadTask)];
    addGes.numberOfTapsRequired = 1;
    [_addView addGestureRecognizer:addGes];
        
}

- (void)promptViewSetup
{
    
    _promptView = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 64+3*kRateSize, kDeviceWidth, KDeviceHeight-44-50*kRateSize) andImgName:@"ic_uploading_null" andTitle:@"这里是空的,快去上传图片和视频吧~"];
    [self.view addSubview:_promptView];
    
    [_promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(1);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
    }];
    
    _promptView.hidden = YES;
    
}


// 判断选中数据
- (void)callCheckDataListSelected
{
    
    NSLog(@"callCheckDataListSelected-------%@",self.selectArr);
    
    if (self.selectArr.count >0) {
        _deleteBtn.enabled = YES;
        
        NSString *btnStr = [NSString stringWithFormat:@"删    除 ( %lu )",(unsigned long)self.selectArr.count];
        
        [_deleteBtn setTitle:btnStr forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        
    } else {
        _deleteBtn.enabled = NO;
    }
    
}

#pragma mark 页面底端删除事件
- (void)deleteUpLoadTask
{
    
    if (!self.deleteVideocustom) {
        self.deleteVideocustom = [[CustomAlertView alloc]initWithTitle:@"温馨提示" andRemandMessage:[NSString stringWithFormat:@"您确认要删除这%lu项数据吗?",(unsigned long)self.selectArr.count] andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确认" withDelegate:self];
        self.deleteVideocustom.tag = 200;
        [self.deleteVideocustom show];
        
    }
    
}
    
#pragma mark 页面底端增加事件
- (void)addUpLoadTask
{
    
    BOOL enterProtocol = [[NSUserDefaults standardUserDefaults] objectForKey:@"enterProtocol"];
    
    if (enterProtocol) {
        
        self.addNewVideocustom = [[CustomAlertView alloc]initWithButtonNum:0 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"从手机相册选择" withDelegate:self];
        
        self.addNewVideocustom.tag = 100;
        
        [self.addNewVideocustom bottomShow];
        
        
    }else
    {
        

        
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [UrlPageJumpManeger upProtocolPageJump];
        know.existNaviBar = YES;
        [self.navigationController pushViewController:know animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"enterProtocol"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }

}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 100) {
        
        if(buttonIndex==0)
        {
            
            NSLog(@"我要跳转了");
            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowVideo];//单选视频
            pickerVc.groupType = KAssetsLabraryVideo;//视频是照片的具体分类
            pickerVc.maxCount = 1;//设置最多能选几张图片
            pickerVc.choiceType = ALSingleChoice;
            
            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
            [self presentViewController:nav animated:YES completion:nil];
            
        }
        else
        {
            
        }
        
        if(self.addNewVideocustom)
        {
            [self.addNewVideocustom resignKeyWindow];
            self.addNewVideocustom = nil;
        }
        
    }else
    {
        
        if(buttonIndex==0)
        {
            
            NSLog(@"我要删除了");
            
            //获取最新数据源，刷新界面
            NSMutableArray *tempArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
            
            for (NSMutableDictionary *dic in tempArr) {
                
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                    
                    NSMutableArray *tempArray = [dic objectForKey:@"list"];
                    
                    for (NSString *selectId in self.selectArr) {
                        
                        [tempArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
                            
                            NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:obj];
                            if ([[tempDic objectForKey:@"sign"]isEqualToString:selectId]) {
                                
                                *stop = YES;
                            }
                            
                            if (*stop) {
                                
                                if ([[tempDic objectForKey:@"status"]isEqualToString:@"2"]) {
                                    
                                    [self.ugcManager stopUGCUploadTask];
                                    
                                    self.upLoadingIndexStr = nil;
                                    
                                    self.upLoadingSpeed = @"等待网络";
                                    
                                    self.uptask = 0;
                                    
                                }
                                
                                [tempArray removeObject:obj];
                            }
                            
                        }];
                        
                    }
                    
                    self.upLoadTaskArr = tempArray;
                    
                    break;
                    
                }
            }
            
            
            NSLog(@"删除后写入的信息--------%@",self.upLoadTaskArr);
            [DataPlist WriteToPlistArray:tempArr ToFile:UGC_UPLOADING_LIST_PLIST];
            
            [self.selectArr removeAllObjects];
            
            [self callCheckDataListSelected];
            
            [self.upLoadingTaskView reloadData];

            
        }
        else
        {
            
            
        }
        
        if(self.deleteVideocustom)
        {
            [self.deleteVideocustom resignKeyWindow];
            self.deleteVideocustom = nil;
        }
        
    }
    
    return;
    
}

- (void)reloadTableViewHeaderWithUnReview:(NSString *)unReview
{
    
    if ([unReview integerValue] > 0) {
        
        _upLoadingTaskView.tableHeaderView = _headerView;
        
        _upLoadingTaskView.tableHeaderView.hidden = NO;
        
        [_headerView changeTitleWithUnReviewedNum:[NSString stringWithFormat:@"%@",unReview]];
        
        [_promptView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top).offset(30*kDeviceRate);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
        }];
        
    }else
    {
        
        _upLoadingTaskView.tableHeaderView.hidden = YES;
        
        [_promptView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top).offset(1);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
        }];
        
    }
    
}

#pragma  mark - tableViewHeaderView的点击事件
- (void)headerViewClick
{

    if (_headerClick) {
        _headerClick();
    }
    
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
