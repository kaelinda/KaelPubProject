//
//  UGCUpLoadedManagerViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUpLoadedManagerViewController.h"
#import "MyGesture.h"
#import "HTRequest.h"
#import "UGCUpLoadedCell.h"
#import "UIButton_Block.h"
#import "UGCUpLoadedHeaderView.h"
#import "CustomAlertView.h"
#import "GeneralPromptView.h"
#import "HTALViewController.h"
#import "HTALVideoEditViewController.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

#define kSection_Header_Height (40*kDeviceRate)
#define kSection_GrayLine_Height (1*kDeviceRate)

@interface UGCUpLoadedManagerViewController ()<UITableViewDelegate,UITableViewDataSource,HTRequestDelegate,CustomAlertViewDelegate>
{
    
    UGCUpLoadedHeaderView *_headerView;

}

@property (nonatomic, strong)UIImageView *addView;
@property (nonatomic, strong)UIButton *addBtn;
@property (nonatomic, strong)UIButton *deleteBtn;//编辑底部添加按钮
@property (nonatomic, strong)UIImageView *deleteView;//编辑底部添加视图
@property (nonatomic, strong)NSMutableArray *upLoadedArr;//已上传任务数组
@property (nonatomic, strong)NSMutableArray *selectArr;//选择数组
@property (nonatomic, strong)UITableView *upLoadedView;//已上传任务视图

@property (nonatomic, strong)CustomAlertView *addNewVideocustom;
@property (nonatomic, strong)CustomAlertView *deleteVideocustom;

@property (nonatomic,strong)GeneralPromptView *promptView;

@property (nonatomic,strong)NSString *unReviewSelected;

@property (nonatomic,strong)NSString *reviewedSelected;

@property (nonatomic,strong)NSString *reviewingSelected;

@end

@implementation UGCUpLoadedManagerViewController

-(instancetype)init{
    self = [super init];
    if (self) {
        
        _isNeedNavBar = NO;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideNaviBar:!_isNeedNavBar];
    // Do any additional setup after loading the view.
    
    self.upLoadedArr = [NSMutableArray array];
    
    self.selectArr = [NSMutableArray array];
    
    self.unReviewSelected = [[NSString alloc]init];
    self.reviewedSelected = [[NSString alloc]init];
    self.reviewingSelected = [[NSString alloc]init];
    
    //给各个section打开状态赋值
    self.unReviewSelected = @"1";
    self.reviewedSelected = @"1";
    self.reviewingSelected = @"1";

    
    [self addViewSetup];
    
    [self deleteViewSetup];
    
    [self setupTableView];
    
    [self promptViewSetup];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isDeleteStatus) {//删除状态
        
        _addView.hidden = YES;
        
        _deleteView.hidden = NO;
        
    }else//增加状态
    {
        
        [self.selectArr removeAllObjects];
        
        [self callCheckDataListSelected];
        
        _addView.hidden = NO;
        
        _deleteView.hidden = YES;
        
    }

     [self requestData];
    
    [self.upLoadedView reloadData];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    //获取自己的点播列表
    if ([type isEqualToString:UGC_COMMON_GETPROGRAMLISTBYUSERINFO]) {
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            
 
            self.upLoadedArr = [NSMutableArray array];
                
            NSMutableArray *tempArr = [NSMutableArray array];
                
            tempArr = [result objectForKey:@"programList"];
                
            self.upLoadedArr = [self handleDateWithDateArr:tempArr];
                
            NSLog(@"整理后的数据------%@",self.upLoadedArr);
        
            [self.upLoadedView reloadData];
       
        }else if([[result objectForKey:@"ret"] integerValue] == -1 && [result objectForKey:@"ret"]){
            
            self.upLoadedArr = [NSMutableArray array];
            
            NSMutableArray *tempArr = [NSMutableArray array];
            
            self.upLoadedArr = [self handleDateWithDateArr:tempArr];
            
            [self.upLoadedView reloadData];
            
        }else{
            
            _promptView.hidden = NO;
            
        }
    }
    
    //删除点播视频
    if ([type isEqualToString:UGC_COMMON_DELETEPROGRAM]) {
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            
            for (NSString *selectId in self.selectArr) {
                
                for (NSMutableDictionary *dic in self.upLoadedArr) {
                    
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                    
                    [tempArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
                        
                        NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:obj];
                        if ([[tempDic objectForKey:@"programId"]isEqualToString:selectId]) {
                            
                            *stop = YES;
                        }
                        
                        if (*stop) {
                            [tempArr removeObject:obj];
                        }
                        
                    }];

                }
                
            }
            
            [self.selectArr removeAllObjects];
            
            [self callCheckDataListSelected];
            
            for (NSMutableDictionary *dic in self.upLoadedArr) {
                
                if ([[dic objectForKey:@"UGCUpLoadedType"]isEqualToString:@"UGCUnReview"]) {
                    
                    NSMutableArray *unReview = [dic objectForKey:@"list"];
                    
                    if (unReview.count) {
                        
                        _upLoadedView.tableHeaderView = _headerView;
                        
                        [_headerView changeTitleWithUnReviewedNum:[NSString stringWithFormat:@"%lu",(unsigned long)unReview.count]];
                        
                    }else
                    {
                        
                        _upLoadedView.tableHeaderView.hidden = YES;
                        
                    }
                    
                    if (_callBackUnreviewNum) {
                        _callBackUnreviewNum([NSString stringWithFormat:@"%lu",(unsigned long)unReview.count]);
                    }

                    break;
                }
            }
            
            
            [self.upLoadedView reloadData];
            
        }
    }
    
    [self hiddenCustomHUD];

    
}

- (void)requestData
{
    
    [self showCustomeHUD];
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst UGCCommonGetProgramListByUserInfo];

}

#pragma mark - tableView 代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return self.upLoadedArr.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 82*kDeviceRate;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // 判断section的展开收起状态
    if ([[[self.upLoadedArr objectAtIndex:section]objectForKey:@"SetionSelected"] intValue] == 1) {
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        dic = [self.upLoadedArr objectAtIndex:section];
        
        NSMutableArray *arr = [NSMutableArray array];
        
        arr = [dic objectForKey:@"list"];
        
        return arr.count;
    }else
    {
        
        return 0;
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    NSMutableDictionary *tempDic = [[[self.upLoadedArr objectAtIndex:indexPath.section]objectForKey:@"list"]objectAtIndex:indexPath.row];
    
    UGCUpLoadedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uploaded"];
    
    if (cell == nil) {
        
        cell = [[UGCUpLoadedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uploaded"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    cell.isDeleteStatus = _isDeleteStatus;
    
    cell.cellDic = tempDic;
    
    [cell upDate];
    
    if (_isDeleteStatus) {//删除状态
        
        cell.choiceImg.hidden = YES;
        
        cell.UnChoiceImg.hidden = NO;
        
        if (self.selectArr.count) {
            
            for (NSString *tempID in self.selectArr) {
                
                if ([tempID isEqualToString:[tempDic objectForKey:@"programId"]]) {
                    
                    cell.choiceImg.hidden = NO;
                    
                    cell.UnChoiceImg.hidden = YES;
                    
                    break;
                    
                }
                
            }
            
        }else
        {
            
        }
        
    }else
    {
        
        if ([[[self.upLoadedArr objectAtIndex:indexPath.section]objectForKey:@"UGCUpLoadedType"]isEqualToString:@"UGCReviewing"])
        {
            
            cell.cellArrowImg.hidden = YES;
            
        }else
        {
            
            cell.cellArrowImg.hidden = NO;
            
        }
        
    }

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"点我了");
    
    NSMutableDictionary *tempDic = [[[self.upLoadedArr objectAtIndex:indexPath.section]objectForKey:@"list"]objectAtIndex:indexPath.row];
    
    if (_isDeleteStatus) {//删除状态
        
        NSString *selectId = [tempDic objectForKey:@"programId"];
        
        if (self.selectArr.count) {
            
            BOOL flag = false;
            
            for (int i=0; i<self.selectArr.count; i++) {
                
                NSString *cellID = [self.selectArr objectAtIndex:i];
                
                if ([cellID isEqualToString:selectId]) {
                    
                    [self.selectArr removeObjectAtIndex:i];
                    
                    flag = true;
                    
                    break;
                }
                
            }
            
            if (!flag) {
            
                [self.selectArr addObject:selectId];
                
            }
        
        }else
        {
            
            [self.selectArr addObject:selectId];
            
        }
        
        [self.upLoadedView reloadData];
        
        [self callCheckDataListSelected];
        
    }else//增加状态
    {
        
        if ([[[self.upLoadedArr objectAtIndex:indexPath.section]objectForKey:@"UGCUpLoadedType"]isEqualToString:@"UGCReviewing"]) {
            
            return;
            
        }else
        {
            
            NSMutableDictionary *tempDic = [[[self.upLoadedArr objectAtIndex:indexPath.section]objectForKey:@"list"]objectAtIndex:indexPath.row];
            
            HTALVideoEditViewController *editView = [[HTALVideoEditViewController alloc] init];
            
            editView.urlType = KNetworkUrlType;
            
            editView.reEditData = tempDic;
            
            editView.modefySuccessBlock = ^{
                                
                [self requestData];
                
            };
            
            //                editView.videoPathStr = [NSString stringWithFormat:@"%@",url];
            
            
//                            editView.asset = asset;
            
            [self.navigationController pushViewController:editView animated:YES];
            
        }
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.000001f;//这里不能直接设置为0
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kSection_Header_Height;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [self.upLoadedArr objectAtIndex:section];
    
    //整个header
    UIView *sectionHeaderView = [UIView new];
    sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Header_Height);
    [sectionHeaderView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    
    
    //白色部分的 header
    UIView *headerContentView = [UIView new];
    headerContentView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Header_Height-kSection_GrayLine_Height);
    headerContentView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [sectionHeaderView addSubview:headerContentView];
    
    
    //段头标题视图
    UILabel *sectionTitleLabel = [[UILabel alloc] init];
    sectionTitleLabel.frame = CGRectMake(12*kDeviceRate, 0, kDeviceWidth/2, kSection_Header_Height-kSection_GrayLine_Height);
    
    NSMutableArray *tempArr = [dic objectForKey:@"list"];
    
    sectionTitleLabel.text = [NSString stringWithFormat:@"%@ (%lu)", [dic objectForKey:@"name"],(unsigned long)tempArr.count];
    
    
    sectionTitleLabel.font = HT_FONT_SECOND;
    sectionTitleLabel.textColor = HT_COLOR_FONT_SECOND;
    [headerContentView addSubview:sectionTitleLabel];
    
    //箭头图标
    UIImageView *cellArrowImg = [[UIImageView alloc]init];
    [cellArrowImg setImage:[UIImage imageNamed:@"ic_upload_down"]];
    cellArrowImg.contentMode = UIViewContentModeScaleAspectFit;
    [sectionHeaderView addSubview:cellArrowImg];
    
    [cellArrowImg mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(sectionHeaderView.mas_right).offset(-12*kDeviceRate);
        make.centerY.equalTo(sectionHeaderView.mas_centerY);
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(20*kDeviceRate));
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sectionClick:)];
    
    sectionHeaderView.tag = 100 +section;
    
    [sectionHeaderView addGestureRecognizer:tap];
    
    if ([[dic objectForKey:@"SetionSelected"]intValue] == 1) {
        
        [cellArrowImg setImage:[UIImage imageNamed:@"ic_upload_down"]];
        
    }else
    {
        
        [cellArrowImg setImage:[UIImage imageNamed:@"ic_upload_enter"]];
        
    }
    
    return sectionHeaderView;
    
}

- (void)promptViewClick
{
    
    _promptView.hidden = YES;
    
    [self requestData];
    
}


#pragma  mark - tableViewHeaderView的点击事件
- (void)headerViewClick
{

//    for (int i = 0; i < self.upLoadedArr.count; i++) {
//        
//        NSMutableDictionary *dic = [self.upLoadedArr objectAtIndex:i];
//        
//        if (i == 2) {
//            
//            [dic setObject:@"1" forKey:@"SetionSelected"];
//            
//        }else
//        {
//            
//            [dic setObject:@"0" forKey:@"SetionSelected"];
//            
//        }
//    
//    }
    
    for (NSMutableDictionary *dic in self.upLoadedArr) {
        
        NSString *UGCUpLoadedType = [dic objectForKey:@"UGCUpLoadedType"];
        
        if ([UGCUpLoadedType isEqualToString:@"UGCReviewed"]) {
            
            self.reviewedSelected = @"0";
            
            [dic setObject:self.reviewedSelected  forKey:@"SetionSelected"];
            
        }else if([UGCUpLoadedType isEqualToString:@"UGCReviewing"]){
            
            self.reviewingSelected = @"0";
            
            [dic setObject:self.reviewingSelected  forKey:@"SetionSelected"];
            
        }else{
            
            self.unReviewSelected = @"1";
            
            [dic setObject:self.unReviewSelected  forKey:@"SetionSelected"];
            
        }
        
    }
    
    [self.upLoadedView reloadData];
    
}

#pragma  mark - tableViewSetionHeaderView的点击事件
- (void)sectionClick:(UITapGestureRecognizer *)tap{

    if ([[[self.upLoadedArr objectAtIndex:tap.view.tag - 100]objectForKey:@"SetionSelected"] intValue] == 0) {
        
        NSMutableDictionary *dic = [self.upLoadedArr objectAtIndex:tap.view.tag - 100];
        
        NSString *UGCUpLoadedType = [dic objectForKey:@"UGCUpLoadedType"];
        
        if ([UGCUpLoadedType isEqualToString:@"UGCReviewed"]) {
            
            self.reviewedSelected = @"1";
            
            [dic setObject:self.reviewedSelected  forKey:@"SetionSelected"];
            
        }else if([UGCUpLoadedType isEqualToString:@"UGCReviewing"]){
            
            self.reviewingSelected = @"1";
            
            [dic setObject:self.reviewingSelected  forKey:@"SetionSelected"];
            
        }else{
            
            self.unReviewSelected = @"1";
            
            [dic setObject:self.unReviewSelected  forKey:@"SetionSelected"];
            
        }

    }
    else
    {
        
        NSMutableDictionary *dic = [self.upLoadedArr objectAtIndex:tap.view.tag - 100];
        
        NSString *UGCUpLoadedType = [dic objectForKey:@"UGCUpLoadedType"];
        
        if ([UGCUpLoadedType isEqualToString:@"UGCReviewed"]) {
            
            self.reviewedSelected = @"0";
            
            [dic setObject:self.reviewedSelected  forKey:@"SetionSelected"];
            
        }else if([UGCUpLoadedType isEqualToString:@"UGCReviewing"]){
            
            self.reviewingSelected = @"0";
            
            [dic setObject:self.reviewingSelected  forKey:@"SetionSelected"];
            
        }else{
            
            self.unReviewSelected = @"0";
            
            [dic setObject:self.unReviewSelected  forKey:@"SetionSelected"];
            
        }

    }
    

    
    [self.upLoadedView reloadSections:[NSIndexSet indexSetWithIndex:tap.view.tag -100] withRowAnimation:UITableViewRowAnimationFade];
    
}

#pragma mark 页面底端增加事件
- (void)addUpLoadTask
{
    
    BOOL enterProtocol = [[NSUserDefaults standardUserDefaults] objectForKey:@"enterProtocol"];
    
    if (enterProtocol) {
        

        
        self.addNewVideocustom = [[CustomAlertView alloc]initWithButtonNum:0 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"从手机相册选择" withDelegate:self];
        
        self.addNewVideocustom.tag = 100;
        
        [self.addNewVideocustom bottomShow];
        
        
    }else
    {
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [UrlPageJumpManeger upProtocolPageJump];
        know.existNaviBar = YES;
        [self.navigationController pushViewController:know animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"enterProtocol"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }

    
}



#pragma mark 页面底端删除事件
- (void)deleteUpLoadTask
{
    
    if (!self.deleteVideocustom) {
        self.deleteVideocustom = [[CustomAlertView alloc]initWithTitle:@"温馨提示" andRemandMessage:[NSString stringWithFormat:@"您确认要删除这%lu项数据吗?",(unsigned long)self.selectArr.count] andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确认" withDelegate:self];

        self.deleteVideocustom.tag = 200;
        [self.deleteVideocustom show];
        
    }
    
}


#pragma  mark - 处理数据
- (NSMutableArray *)handleDateWithDateArr:(NSMutableArray *)dateArr
{
    
    NSMutableArray *returnArr = [NSMutableArray array];
    
    //未过审核
    NSArray *unReview = [NSMutableArray array];
    
    //审核通过
    NSArray *reviewed = [NSMutableArray array];
    
    //审核中
    NSArray *reviewing = [NSMutableArray array];
    
    //未过审核
    NSMutableArray *tempUnReview = [NSMutableArray array];
    
    //审核通过
    NSMutableArray *tempReviewed = [NSMutableArray array];
    
    //审核中
    NSMutableArray *tempReviewing = [NSMutableArray array];
    
    //拆除字符串
    for (NSMutableDictionary *dic in dateArr) {
        
        NSMutableDictionary *m_dic = [NSMutableDictionary dictionaryWithDictionary:dic];
        
        NSString *requestStr = [m_dic objectForKey:@"tags"];
        
        
        if (NotEmptyStringAndNilAndNull(requestStr)) {
            
            NSArray *testArr = [requestStr componentsSeparatedByString:@","];
            
            NSString *tagsIds = [[NSString alloc]init];
            
            NSString *tegsNames = [[NSString alloc]init];
            
            for (NSString *str in testArr) {
                
                NSRange range = [str rangeOfString:@":"]; //现获取要截取的字符串位置
                
                NSString * tegsName = [str substringFromIndex:range.location + 1]; //截取字符串
                
                NSString * tagsId = [str substringToIndex:range.location]; //截取字符串
                
                if (!tagsIds.length) {
                    
                    tagsIds = [NSString stringWithFormat:@"%@",tagsId];
                    
                }else
                {
                    tagsIds = [NSString stringWithFormat:@"%@,%@",tagsIds,tagsId];
                }
                
                if (!tegsNames.length) {
                    
                    tegsNames = [NSString stringWithFormat:@"%@",tegsName];
                    
                }else
                {
                    tegsNames = [NSString stringWithFormat:@"%@,%@",tegsNames,tegsName];
                }

        }
            
            tegsNames = [NSString stringWithFormat:@"标签：%@",tegsNames];
            
            [m_dic setObject:tagsIds forKey:@"tags"];//拼接标签ID
            
            [m_dic setObject:tegsNames forKey:@"tegsNames"];
        
            
    }else
    {
        
        
        NSString *tegsNames = [[NSString alloc]init];
        
        tegsNames = @"标签：";
        
        [m_dic setObject:tegsNames forKey:@"tegsNames"];
        
    }

        
        switch ([[m_dic objectForKey:@"status"] integerValue]) {
            case 0:
            {
                
                [tempReviewing addObject:m_dic];
//                [unReview addObject:m_dic];

            }
                
                break;
                
            case 1:
            {
                
                [tempReviewed addObject:m_dic];
                
            }
                
                break;
                
            case 2:
            {
                
                [tempUnReview addObject:m_dic];
//                [reviewing addObject:m_dic];
        
            }
                
                break;
                
            default:
                break;
        }
    }
    
    reviewed = [self dateSequenceWithsortArr:tempReviewed andKeyName:@"checkTime"];
    
    reviewing = [self dateSequenceWithsortArr:tempReviewing andKeyName:@"submitTime"];
    
    unReview = [self dateSequenceWithsortArr:tempUnReview andKeyName:@"submitTime"];
    
    
    
    NSMutableDictionary *m_dic1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:reviewed,@"list",@"已通过审核",@"name",@"UGCReviewed",@"UGCUpLoadedType",self.reviewedSelected,@"SetionSelected", nil];
    
    NSMutableDictionary *m_dic2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:reviewing,@"list",@"审核中",@"name",@"UGCReviewing",@"UGCUpLoadedType",self.reviewingSelected,@"SetionSelected", nil];
    
    NSMutableDictionary *m_dic3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:unReview,@"list",@"未通过审核",@"name",@"UGCUnReview",@"UGCUpLoadedType",self.unReviewSelected,@"SetionSelected", nil];
    
    if (unReview.count) {
        
        _upLoadedView.tableHeaderView = _headerView;
        
        [_headerView changeTitleWithUnReviewedNum:[NSString stringWithFormat:@"%lu",(unsigned long)unReview.count]];
        
    }else
    {
        
        _upLoadedView.tableHeaderView.hidden = YES;
        
    }
    
    if (_callBackUnreviewNum) {
        _callBackUnreviewNum([NSString stringWithFormat:@"%lu",(unsigned long)unReview.count]);
    }
    
    
    [returnArr addObject:m_dic1];
    [returnArr addObject:m_dic2];
    [returnArr addObject:m_dic3];
    
    return returnArr;
    
}


#pragma  mark - 界面UI初始化 ----start----
- (void)addViewSetup
{
    
    //页面底端增加频道收藏条
    _addView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-176*kDeviceRate, kDeviceWidth, 60*kDeviceRate)];
    
    _addView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_addView];
    _addView.userInteractionEnabled = YES;
    _addView.hidden = YES;
    
    [_addView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@(60*kDeviceRate));
    }];
    
    UIView *VerticalLine = [[UIView alloc]init];
    [_addView addSubview:VerticalLine];
    VerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_addView.mas_top).offset(1);
        make.left.equalTo(_addView.mas_left);
        make.right.equalTo(_addView.mas_right);
        make.height.equalTo(@(1));
    }];
    
    UIImageView *addImg = [[UIImageView alloc] init];
    [addImg setImage:[UIImage imageNamed:@"sl_upload_new_normal"]];
    [_addView addSubview:addImg];
    
    [addImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_addView.mas_centerX);
        make.centerY.equalTo(_addView.mas_centerY);
    }];
    
    MyGesture *addGes = [[MyGesture alloc] initWithTarget:self action:@selector(addUpLoadTask)];
    addGes.numberOfTapsRequired = 1;
    [_addView addGestureRecognizer:addGes];
    
}

- (void)deleteViewSetup
{
    
    //页面底端增加删除按钮
    _deleteView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-176*kDeviceRate, kDeviceWidth, 60*kDeviceRate)];
    //    [_editDeleteView setImage:[UIImage imageNamed:@"bg_user_add_fav_channel"]];
    _deleteView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_deleteView];
    _deleteView.userInteractionEnabled = YES;
    _deleteView.hidden = YES;
    
    [_deleteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@(60*kDeviceRate));
    }];
    
    UIView *editVerticalLine = [[UIView alloc]init];
    [_deleteView addSubview:editVerticalLine];
    editVerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [editVerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_deleteView.mas_top).offset(1);
        make.left.equalTo(_deleteView.mas_left);
        make.right.equalTo(_deleteView.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _deleteBtn = [[UIButton alloc]init];
    [_deleteView addSubview:_deleteBtn];
    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_deleteView.mas_centerX);
        make.centerY.equalTo(_deleteView.mas_centerY);
    }];
    
    _deleteBtn.titleLabel.font = HT_FONT_FIRST;
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"sl_upload_del_normal"] forState:UIControlStateNormal];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"sl_upload_del_check"] forState:UIControlStateHighlighted];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"ic_user_common_no_delete_item"] forState:UIControlStateDisabled];
    [_deleteBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_deleteBtn setTitle:@"删    除"forState:UIControlStateDisabled];
    _deleteBtn.enabled = NO;
    
    [_deleteBtn addTarget:self action:@selector(deleteUpLoadTask) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setupTableView
{
    
    _upLoadedView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 176*kDeviceRate)style:UITableViewStyleGrouped];
        
    _upLoadedView.dataSource = self;
    
    _upLoadedView.delegate = self;
    
    _upLoadedView.backgroundColor = HT_COLOR_BACKGROUND_APP;
 
    [self.view addSubview:_upLoadedView];
    
    [_upLoadedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
    }];
    
    _headerView = [[UGCUpLoadedHeaderView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 30*kDeviceRate)];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerViewClick)];
    
    [_headerView addGestureRecognizer:tap];
    
}

- (void)promptViewSetup
{
    
    _promptView = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 64+3*kRateSize, kDeviceWidth, KDeviceHeight-44-50*kRateSize) andImgName:@"btn_click_refresh" andTitle:@"页面加载失败,轻触重试"];
    [self.view addSubview:_promptView];
    
    [_promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(1);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-60*kDeviceRate);
    }];
    
    _promptView.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(promptViewClick)];
    
    [_promptView addGestureRecognizer:tap];
    
}
#pragma  mark - 界面UI初始化 ----end----


// 判断选中数据
- (void)callCheckDataListSelected
{
    
    if (self.selectArr.count >0) {
        _deleteBtn.enabled = YES;
        
        NSString *btnStr = [NSString stringWithFormat:@"删    除 ( %lu )",(unsigned long)self.selectArr.count];
        
        [_deleteBtn setTitle:btnStr forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        
    } else {
        _deleteBtn.enabled = NO;
    }
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    

    if (alertView.tag == 100) {
        
        if(buttonIndex==0)
        {
        
            NSLog(@"我要跳转了");
            
            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowVideo];//单选视频
            pickerVc.groupType = KAssetsLabraryVideo;//视频是照片的具体分类
            pickerVc.maxCount = 1;//设置最多能选几张图片
            pickerVc.choiceType = ALSingleChoice;
            
            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
            [self presentViewController:nav animated:YES completion:nil];
        
        }
        else
        {
            
        }
        
        if(self.addNewVideocustom)
        {
            [self.addNewVideocustom resignKeyWindow];
            self.addNewVideocustom = nil;
        }
        
    }else
    {
        
        if(buttonIndex==0)
        {
            
            [self showCustomeHUD];
            
            NSLog(@"我要删除了");
            
            NSString *selectID = [[NSString alloc]init];
            
            for (NSString *tempId in self.selectArr) {
                
                if (selectID.length) {
                    
                    selectID = [NSString stringWithFormat:@"%@,%@",selectID,tempId];
                    
                }else
                {
                    selectID = [NSString stringWithFormat:@"%@",tempId];
                }
                
            }
            
            HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
            
            [requst UGCCommonDeleteProgramWithProgramId:selectID andIsCleanedOut:@"0"];
            
        }
        else
        {


        }
        
        if(self.deleteVideocustom)
        {
            [self.deleteVideocustom resignKeyWindow];
            self.deleteVideocustom = nil;
        }
        
    }

    return;
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 *  排列数据按照日期先后进行排序
 *
 *  @param arr 原始数组
 *
 *  @return 排序后数组
 */
- (NSArray *)dateSequenceWithsortArr:(NSArray *)arr andKeyName:(NSString *)keyName
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:keyName ascending:NO];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}

@end
