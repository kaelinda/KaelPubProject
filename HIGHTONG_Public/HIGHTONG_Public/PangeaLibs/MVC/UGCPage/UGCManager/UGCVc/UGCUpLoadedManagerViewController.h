//
//  UGCUpLoadedManagerViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

typedef void(^UGCUpLoadedManagerViewController_Block)(NSString *unReviewNum);

@interface UGCUpLoadedManagerViewController : HT_FatherViewController

/**
 *  @author dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar; 

/**
 *  @author dirk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedTabBar;


@property (nonatomic,strong) UGCUpLoadedManagerViewController_Block callBackUnreviewNum;

/**
 *  @author dirk
 *
 *  @brief 是否是删除状态
 */
@property (nonatomic,assign) BOOL isDeleteStatus;

- (void)headerViewClick;

@end
