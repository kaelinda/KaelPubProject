//
//  UGCManagerHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/22.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCManagerHomeViewController.h"
#import "UGCUpLoadedManagerViewController.h"
#import "UGCUpLoadingManagerViewController.h"
#import "CustomAlertView.h"
#import "HTALViewController.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

@interface UGCManagerHomeViewController ()<VTMagicViewDataSource, VTMagicViewDelegate,CustomAlertViewDelegate>


@property (nonatomic, strong) NSMutableArray *menuNameList;//列表

@property (nonatomic, strong)UIButton *cleanBtn;
 
@property (nonatomic, strong)NSString *unReviewNum;

@property (nonatomic, strong)CustomAlertView *addNewVideocustom;

@property (nonatomic,strong) UIButton *rightNavBtn;

/**
 *  @author dirk
 *
 *  @brief 是否是删除状态
 */
@property (nonatomic,assign) BOOL isDeleteStatus;

@end

@implementation UGCManagerHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    
    [self setNaviBarLeftBtn: (self.navigationController.viewControllers.count >= 2) ? Back : nil];
    
    
    _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [_rightNavBtn setTitle:@"删除" forState:UIControlStateNormal];
    [_rightNavBtn setTitle:@"完成" forState:UIControlStateSelected];
    _rightNavBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_rightNavBtn addTarget:self action:@selector(dustbinBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setNaviBarRightBtnSize:CGSizeMake(40, 40) andRightBtn:_rightNavBtn andRightBtnTitleLabelFont:[UIFont systemFontOfSize:16]];
    
    _isDeleteStatus = NO;
    
    
    //    [self setNaviBarTitle:self.HealthVodViewTitle];
    [self setNaviBarTitle:@"管理"];
    
    _menuNameList = [NSMutableArray array];
    
    _menuNameList = @[@"已上传", @"上传中"];
    
    _unReviewNum = [[NSString alloc]init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.magicView.navigationColor = [UIColor whiteColor];
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDivide;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 44*kDeviceRate;
    self.magicView.sliderWidth = 78*kDeviceRate;
    self.magicView.sliderHeight = 2*kDeviceRate;
    self.magicView.itemScale = 1.0;
    self.magicView.scrollEnabled = NO;
    
    [self.magicView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    
    return _menuNameList;
    
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        menuItem.titleLabel.font = HT_FONT_SECOND;
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    
    if (pageIndex == 0) {
        
        static NSString *gridIdd = @"uploaded";
        UGCUpLoadedManagerViewController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
        if (!viewController) {
            viewController = [[UGCUpLoadedManagerViewController alloc] init];
                    }
        
        viewController.callBackUnreviewNum = ^(NSString *unReviewNum) {
            
            _unReviewNum = unReviewNum;
            
        };
        
        viewController.isNeedNavBar = NO;
        viewController.isNeedTabBar = YES;
        viewController.isDeleteStatus = _isDeleteStatus;
        
        return viewController;
        
    }else
    {
        
        static NSString *gridIdd = @"uploading";
        UGCUpLoadingManagerViewController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
        if (!viewController) {
            viewController = [[UGCUpLoadingManagerViewController alloc] init];
            
        }
        
        viewController.headerClick = ^{
            
            [self switchToPage:0 animated:YES];
            
            UGCUpLoadedManagerViewController *jk = [self viewControllerAtPage:0];
            
            [jk headerViewClick];
            
        };
        
        viewController.isNeedNavBar = NO;
        viewController.isNeedTabBar = YES;
        viewController.isDeleteStatus = _isDeleteStatus;
        
        return viewController;
        
    }

}

#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    
    NSLog(@"index:%ld viewDidAppeare:%@", (long)pageIndex, viewController.view);
    
    if (pageIndex == 0) {
        
        UGCUpLoadedManagerViewController *ugc = (UGCUpLoadedManagerViewController *)viewController;
        
        ugc.isDeleteStatus = _isDeleteStatus;
        
    }else
    {
        
        UGCUpLoadingManagerViewController *ugc = (UGCUpLoadingManagerViewController *)viewController;
        
        ugc.isDeleteStatus = _isDeleteStatus;
        
        [ugc reloadTableViewHeaderWithUnReview:_unReviewNum];
        
    }

}

- (void)magicView:(VTMagicView *)magicView viewDidDisappeare:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    //    NSLog(@"index:%ld viewDidDisappeare:%@", (long)pageIndex, viewController.view);
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex
{
    //    NSLog(@"didSelectItemAtIndex:%ld", (long)itemIndex);
}


#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    NSLog(@"返回首页");
    
}

#pragma mark 导航条上完成删除按钮
- (void)dustbinBtnSenderDown:(UIButton *)sender
{
    
    sender.selected = !sender.selected;
    
    _isDeleteStatus = !_isDeleteStatus;
    
    [self.magicView reloadData];
    
    NSLog(@"+++++++++++++++++++清空啦");
    
    if (sender.selected) {//垃圾桶
        
        
    }else{//取消
        
        
    }
}


#pragma mark 上传进入出发弹屏
- (void)makeAlert
{

    BOOL enterProtocol = [[NSUserDefaults standardUserDefaults] objectForKey:@"enterProtocol"];
    
    if (enterProtocol) {
        
        self.addNewVideocustom = [[CustomAlertView alloc]initWithButtonNum:0 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"从手机相册选择" withDelegate:self];
        
        self.addNewVideocustom.tag = 100;
        
        [self.addNewVideocustom show];

        
    }else
    {
        

        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [UrlPageJumpManeger upProtocolPageJump];
        know.existNaviBar = YES;
        [self.navigationController pushViewController:know animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"enterProtocol"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    
    
    
    
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 100) {
        
        if(buttonIndex==0)
        {
            
            NSLog(@"我要跳转了");
            
            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowVideo];//单选视频
            pickerVc.groupType = KAssetsLabraryVideo;//视频是照片的具体分类
            pickerVc.maxCount = 1;//设置最多能选几张图片
            pickerVc.choiceType = ALSingleChoice;
            
            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
            [self presentViewController:nav animated:YES completion:nil];
            
        }
        else
        {
            
        }
        
        if(self.addNewVideocustom)
        {
            [self.addNewVideocustom resignKeyWindow];
            self.addNewVideocustom = nil;
        }
        
    }
    
    return;
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
