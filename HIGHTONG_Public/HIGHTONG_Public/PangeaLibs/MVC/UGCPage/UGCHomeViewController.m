//
//  UGCHomeViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCHomeViewController.h"
#import "UGCPlayer.h"
@interface UGCHomeViewController ()
{
    UGCPlayer *player;
}
@property (nonatomic,strong) UIButton *rightNavBtn;
@property (nonatomic,strong) MLMOptionSelectView *optionView;
//@property (nonatomic,strong) UGCPlayerView *player;
@property (nonatomic,strong)UIButton *playButton;
@property (nonatomic,strong)UIButton *stopButton;
@property (nonatomic,strong)UILabel *statusLable;

@end

@implementation UGCHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initBaseView];
}

#pragma mark - **************** 初始化操作
- (void)initBaseData {
    
}

- (void)initBaseView {
    //初始化标题
    [self setNaviBarTitle:@"直播"];
    //初始化按钮
    _rightNavBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_close_web" imgHighlight:@"btn_back_click" target:self action:@selector(startLive:)];
    [self setNaviBarRightBtn:_rightNavBtn];
    
//    _rightNavBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"Action" target:self action:@selector(startLive:)];
//    [self setRightNavBtn:_rightNavBtn];
    
    //初始化选项控件
    _optionView = [[MLMOptionSelectView alloc] initOptionView];
    
    
    
    
    
//    [self.view setBackgroundColor:[UIColor redColor]];
   // UGCPlayerView *player = [[UGCPlayerView alloc]initWithPlayerType:HTPlayerARCPlay andFathterViewFrame:CGRectMake(40, 100, 200, 200)];
//    UGCPlayerView *player = [[UGCPlayerView alloc]initWithPlayerType:HTPlayerARCPlay];
    
    CGRect playerFrame = CGRectMake(10, 300, 200, 200);
    
    player = [[UGCPlayer alloc]initWithPlayerType:KHTPlayer_ARCPlay withFatherViewFrame:playerFrame];
    //[player setFrameView:CGRectMake(40, 100, 200, 200)];
//    player playerFrame;
    [player setFrame:playerFrame];
//    player.filePath = @"http://cdn.tumbo.com.cn/follomeFront//file/library/library_moment_video/2016/11/02/20161102171719373752095.mp4";
    [player setFilePath:@"http://cdn.tumbo.com.cn/follomeFront//file/library/library_moment_video/2016/11/02/20161102171719373752095.mp4"];
    [player setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:player];
   
    
    
    self.statusLable = [[UILabel alloc]init];
    
    [self.statusLable setBackgroundColor:[UIColor redColor]];
    
    self.statusLable.frame = CGRectMake(30, 100, 120, 30);
    
    [self.view addSubview:self.statusLable];
    
    
    
    self.playButton= [[UIButton alloc]init];
    
    self.playButton.frame = CGRectMake(30, 150, 100, 60);
    [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
    [self.playButton setTag:ENGINE_STATUS_PLAY];
    [self.playButton setBackgroundColor:[UIColor redColor]];
    
    [self.view addSubview:self.playButton];
    
    
    [self.playButton addTarget:self action:@selector(Mplayer:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    self.stopButton= [[UIButton alloc]init];
    
    self.stopButton.frame = CGRectMake(150, 150, 100, 60);
    [self.stopButton setTag:ENGINE_STATUS_STOP];
    [self.stopButton setTitle:@"Stop" forState:UIControlStateNormal];
    [self.stopButton setBackgroundColor:[UIColor redColor]];
    self.stopButton.enabled = NO;
    [self.view addSubview:self.stopButton];
    
    
    [self.stopButton addTarget:self action:@selector(Mstoper) forControlEvents:UIControlEventTouchUpInside];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveStatus:) name:HTPLAYER_STATUS_Noti object:nil];
    
    
   
    
}


-(void)receiveStatus:(NSNotification*)noti
{
    
    
    /*
     数组里的参数说明：
     1）wParam   播放器的状态，共有八个状态，定义在amconstant.h里
     ＊收到ENGINE_STATUS_STOP状态之后必须调用stop接口
     2）lParam   不同的状态对应不同的意义（不在下述列表内的状态不用处理lParam这个值）
     状态                      意义
     ENGINE_STATUS_PLAY          播放文件的时间戳
     ENGINE_STATUS_STOP          播放停止的原因，定义在AMMFPlayerError.h
     ENGINE_STATUS_READY         文件的长度，单位是毫秒，直播流返回0
     ENGINE_STATUS_MESSAGE       告警消息，定义在AMMFPlayerError.h
     ENGINE_STATUS_BUFFERING     缓冲进度
     3）viewHandle   UIView对象，值就是接口createPlayerEngine:withVideoframe:withView:第三个传入参数
     ＊主要用于多屏显示
     4）dwWidth      视频的宽，状态值为ENGINE_STATUS_VIDEO_SIZE_CHANGE有值，其余状态返回0
     5）dwHeight     视频的高，状态值为ENGINE_STATUS_VIDEO_SIZE_CHANGE有值，其余状态返回0
     */
    
 
    
    NSMutableDictionary *notiDic = [NSMutableDictionary dictionary];
    
    notiDic = (NSMutableDictionary*)noti.object;
    
    int status = [[notiDic objectForKey:@"statusTag"] intValue];
    
    NSMutableArray *notiArr = [NSMutableArray array];
    
    notiArr = [notiDic objectForKey:@"statusArray"];
    
    MUInt32 wParam = [[notiArr objectAtIndex:0] unsignedIntValue];
    MLong lParam = [[notiArr objectAtIndex:1] intValue];
    UIView *viewHandle = (UIView*)[notiArr objectAtIndex:2];
    MDWord dwWidth = [[notiArr objectAtIndex:3] unsignedIntValue];
    MDWord dwHeight = [[notiArr objectAtIndex:4] unsignedIntValue];

    
    switch (status) {
        case 0://init
        {
            self.playButton.enabled = YES;
            //            self.progressSlider.value = 0;
            //            self.timeLabel.text = @"00:00/00:00";
                        [self.statusLable setText:@""];
            [self.playButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
            break;
        case 1://start to play
        {
            self.stopButton.enabled = YES;
            //            self.progressSlider.enabled = YES;
            [self.stopButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
            [self.playButton setTag:ENGINE_STATUS_PAUSE];
            [self.statusLable setText:@"Playing"];
        }
            break;
        case 2://pause
        {
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            [self.playButton setTag:ENGINE_STATUS_PLAY];
            [self.statusLable setText:@"Pause"];
        }
            break;
        case 3://stop
        {
            self.stopButton.enabled = NO;
            [self.stopButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            [self.playButton setTag:ENGINE_STATUS_PLAY];
            MInt32  stopReason;
            stopReason = lParam;
            NSString* text = [NSString stringWithFormat:@"Stopped, reason: %d", stopReason];
            [self.statusLable setText: stopReason == 0 ? @"Stopped": text];
            //            self.progressSlider.value = 0;
            //            self.progressSlider.enabled = NO;
            //            self.timeLabel.text = @"00:00/00:00";
        }
            break;
        case 4://connecting
        {
            [self.statusLable setText:@"Connecting"];
            [self.playButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.playButton.enabled = YES;
            //            self.progressSlider.enabled = NO;
            //            if(!connectingTimer)
            //                connectingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countTime) userInfo:nil repeats:YES];
        }
            break;
        case 5://ready
        {
            [self.statusLable setText:@"Ready"];
            [self.playButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            self.playButton.enabled = YES;
            //            if (duration == 0)
            //                self.progressSlider.enabled = NO;
            //            else
            //                self.progressSlider.enabled = YES;
        }
            break;
        case 6://buffering
        {
            MUInt32   bufferingProgress;

            bufferingProgress = lParam;
            NSString* text = [NSString stringWithFormat:@"Buffering %d%%", bufferingProgress];
            [self.statusLable setText:text];
        }
            break;
        case 7://playing
        {
            [self.statusLable setText:@"Playing"];
        }
            break;
        case 8://failed
        {
            [self.statusLable setText:@"Open failed"];
            [self.playButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.playButton.enabled = YES;
        }
            break;
        case 9:
            //            [self.resolutionLabel setText:[NSString stringWithFormat:@"%dX%d", resolutionWidth, resolutionHeight]];
            //            self.ADTimeRemain.hidden = NO;
            
            break;
        default:
            break;
    }
    
}



-(void)Mplayer:(id)sender
{
    [player playAndPauseBtn:sender];
}


-(void)Mstoper
{
    [player playStop];
}




-(void)startLive:(UIButton *)btn{

    [SystemAuthHelper checkCameraAuthStatus:^(BOOL auth, AVAuthorizationStatus status) {

    }];
    
    UGCLiveViewController *UGCLive = [[UGCLiveViewController alloc] initWithType:kUGCUser_Player];
    [self.navigationController pushViewController:UGCLive animated:YES];
    
    //--------------------------------
//    CGPoint point = CGPointMake(self.view.bounds.size.width-30, 65);
//    [self initOptionProperty];
//    _optionView.vhShow = NO;
//    _optionView.edgeInsets = UIEdgeInsetsMake(64, 10, 10, 10);
//    _optionView.optionType = MLMOptionSelectViewTypeArrow;
//    //    [_cellView showViewCenter:self.view.center viewWidth:300];
//    [_optionView showTapPoint:point viewWidth:80 direction:MLMOptionSelectViewTop];

    
    
    
}

-(void)initOptionProperty{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
