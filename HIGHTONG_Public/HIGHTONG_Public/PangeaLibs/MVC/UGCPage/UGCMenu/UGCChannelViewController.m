//
//  UGCChannelViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCChannelViewController.h"
#import "UGCChannelCell.h"
#import "HTRequest.h"
#import "HTEmptyView.h"
#import "UGCLiveViewController.h"
@interface UGCChannelViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,HTRequestDelegate>
@property (nonatomic, strong) UICollectionView *hotCV;

@property (nonatomic, strong) NSMutableArray *columnChannelList;
@property (nonatomic, strong) HTEmptyView *emptyLoadingView;

@end

@implementation UGCChannelViewController
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self hideNaviBar:YES];
    _columnChannelList = [NSMutableArray array];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0;
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _hotCV = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-HT_HIGHT_SECONDNAVIBAR-44) collectionViewLayout:layout];
    _hotCV.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _hotCV.delegate = self;
    _hotCV.dataSource = self;
    _hotCV.showsVerticalScrollIndicator = YES;
    _hotCV.allowsMultipleSelection = NO;
    [self.view addSubview:_hotCV];
    
    [_hotCV registerClass:[UGCChannelCell class] forCellWithReuseIdentifier:@"listCell"];

   

//    [self initDataColumn];
    [self showCustomeHUD];
//    [self initRequestData];
}

-(void)setColumnInId:(NSString *)columnInId
{
    _columnInId = columnInId;
    _emptyLoadingView = [[HTEmptyView alloc]initWithEmptyFatherView:self.view andEmptyImageString:@"bg_retry_gesture" andEmptyLabelText:nil emptyViewClick:^{
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request UGCCommonGetColumnInfoContentWithColumnInId:_columnInId];
    }];
    _emptyLoadingView.hidden = NO;
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request UGCCommonGetColumnInfoContentWithColumnInId:_columnInId];
    
}

-(void)initRequestData
{
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request UGCCommonGetColumnInfoContentWithColumnInId:_columnInId];
}

-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    if ([type isEqualToString:UGC_COMMON_GETCOLUMNINFOCONTENT]) {

        NSDictionary *Dic = [[NSDictionary alloc] initWithDictionary:result];

        _columnChannelList = [Dic objectForKey:@"channelList"];
        [self hiddenCustomHUD];
        
        if ([[result objectForKey:@"channelList"] count]>0) {
            _emptyLoadingView.hidden = YES;

        }else
        {
            _emptyLoadingView.hidden = NO;
        }
        
        
        
        [_hotCV reloadData];
        
    }
}




-(void)initDataColumn
{
    NSMutableDictionary *diction = [[NSMutableDictionary alloc]initWithDictionary:[self jsonExchange:@"getColumnInfoContent"]];
    
    
    
    _columnChannelList = [diction objectForKey:@"channelList"];
    
    
}


-(NSDictionary *)jsonExchange:(NSString *)filePath
{
    NSString * path = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@",filePath] ofType:@"json" ];
    NSData * jsonData = [[NSData alloc] initWithContentsOfFile:path];
    
    NSError * error ;
    NSMutableDictionary* jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"我的字典%@",jsonObj);
    if (!jsonObj || error) {
        NSLog(@"JSON解析失败");
    }
    
    return jsonObj;
    
}





- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    

    return [_columnChannelList count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        UGCChannelCell *cell = (UGCChannelCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"listCell" forIndexPath:indexPath];
        
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [_columnChannelList objectAtIndex:indexPath.row];
        
    cell.channelNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"name"])?@"":[cellDic objectForKey:@"name"];
    
    
    cell.hospitalNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"hospital"])?@"":[cellDic objectForKey:@"hospital"];
    
    cell.doctorNameLabel.text = [NSString stringWithFormat:@"%@ 医师",isEmptyStringOrNilOrNull([cellDic objectForKey:@"doctorName"])?@"":[cellDic objectForKey:@"doctorName"]];
    
    
        cell.hotTextfield.text = [self hotValueChange:[NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"hot"])?@"0":[cellDic objectForKey:@"hot"]]];
    
    [cell tranAnimationWith:cell.videoIV andImageLink:isEmptyStringOrNilOrNull([cellDic objectForKey:@"pictureUrl"])?@"":[cellDic objectForKey:@"pictureUrl"]];

        return cell;
    
    
    
}



-(NSString *)hotValueChange:(NSString *)hotResult
{
    NSString *hotValue = @"";
    
    if ([hotResult intValue]<10000) {
        
        hotValue = hotResult;
        return hotValue;
    }else
    {    NSString *hotValue = @"";
        
        int hotWanValue = [hotResult intValue]/10000;
        
        int hotendValue = [hotResult intValue]%10000;
        
        int hotYiValue = hotWanValue/10000;
        if (hotYiValue) {
            
            hotValue = [NSString stringWithFormat:@"%d亿",hotYiValue];
            return hotValue;
        }else
        {
            
            if (hotendValue <=1000) {
                
                hotValue = [NSString stringWithFormat:@"%d.1万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=2000)
            {
                hotValue = [NSString stringWithFormat:@"%d.2万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=3000)
            {
                hotValue = [NSString stringWithFormat:@"%d.3万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=4000)
            {
                hotValue = [NSString stringWithFormat:@"%d.4万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=5000)
            {
                hotValue = [NSString stringWithFormat:@"%d.5万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=6000)
            {
                hotValue = [NSString stringWithFormat:@"%d.6万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=7000)
            {
                hotValue = [NSString stringWithFormat:@"%d.7万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=8000)
            {
                hotValue = [NSString stringWithFormat:@"%d.8万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue <=9000)
            {
                hotValue = [NSString stringWithFormat:@"%d.9万",hotWanValue];
                return hotValue;
                
            }else if (hotendValue>9000)
            {
                hotValue = [NSString stringWithFormat:@"%d万",hotWanValue+1];
                return hotValue;
                
            }else
            {
                return 0;
            }
            
            
        }
        
    }
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
        return CGSizeMake((_hotCV.width) / 1.0,  268*kDeviceRate);
   
    
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    
        return UIEdgeInsetsMake(0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate);
    
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    __weak __typeof(&*self)weakSelf = self;
    
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [[[_columnChannelList objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];
        
        [UGCLiveViewController canInitRecorderWithCallBack:^(BOOL graned,UGCAuthType type) {
            if (graned) {
                UGCLiveViewController *liveVC = [[UGCLiveViewController alloc] initWithUserType:kUGCUser_Player andChannelID:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"channelId"]]andUserName:[cellDic objectForKey:@"doctorName"]];
                
                NSString *screenFlag = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"screenFlg"])?@"":[cellDic objectForKey:@"screenFlg"]];
                
                if ([screenFlag length]<=0) {
                    
                    liveVC.isPortrait = NO;
                    
                }else
                {
                    liveVC.isPortrait = YES;
                }
                
                
                [weakSelf.navigationController pushViewController:liveVC animated:NO];
            }
            
            else{
                
                if (type == kUGCAuthType_video) {
                    
                    ZSToast(@"请开启摄像头权限");
                }else
                {
                    ZSToast(@"请开启麦克风权限");
                }
                
            }
            
            
            
            
        }];
        
        
        
        
    
    
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
