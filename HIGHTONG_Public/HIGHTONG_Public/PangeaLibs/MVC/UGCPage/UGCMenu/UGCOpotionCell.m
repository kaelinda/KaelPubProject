//
//  UGCOpotionCell.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/29.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCOpotionCell.h"

@implementation UGCOpotionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
     
    }
    return self;
}



-(void)setUpView
{
    __weak __typeof(&*self)weakSelf = self;

    _opotionIV = [[UIImageView alloc]init];
    [_opotionIV setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_opotionIV];
    [_opotionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.mas_left).offset(19*kDeviceRate);
        make.width.mas_equalTo(20*kDeviceRate);
//        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-5*kDeviceRate);
//        make.top.mas_equalTo(weakSelf.mas_top).offset(5*kDeviceRate);
        make.height.mas_equalTo(20*kDeviceRate);
        make.centerY.mas_equalTo(weakSelf.contentView.mas_centerY);
    }];
    
    
    _opotionLabel = [[UILabel alloc]init];
    [_opotionLabel setBackgroundColor:[UIColor clearColor]];
    _opotionLabel.textColor = UIColorFromRGB(0x000000);
    _opotionLabel.font = HT_FONT_SECOND;
    _opotionLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_opotionLabel];
    [_opotionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(0*kDeviceRate);
        make.left.mas_equalTo(_opotionIV.mas_right).offset(16*kDeviceRate);
        make.right.mas_equalTo(weakSelf.mas_right).offset(-20*kDeviceRate);
//        make.top.mas_equalTo(weakSelf.mas_top).offset(5*kDeviceRate);
        make.centerY.mas_equalTo(weakSelf.contentView.mas_centerY);
        make.height.mas_equalTo(20*kDeviceRate);
    }];
    
    _opotionLine = [[UIView alloc]init];
    [_opotionLine setBackgroundColor:UIColorFromRGB(0xe8e8e8)];
    [self.contentView addSubview:_opotionLine];
    [_opotionLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.mas_left).offset(10*kDeviceRate);
        make.right.mas_equalTo(weakSelf.mas_right).offset(-10*kDeviceRate);
        //        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-5*kDeviceRate);
        //        make.top.mas_equalTo(weakSelf.mas_top).offset(5*kDeviceRate);
        make.height.mas_equalTo(0.5*kDeviceRate);
        make.bottom.mas_equalTo(weakSelf.mas_bottom).offset(-0.2*kDeviceRate);
    }];


}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
