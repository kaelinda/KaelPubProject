//
//  UGCTimeProgramCell.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCTimeProgramCell.h"

@implementation UGCTimeProgramCell
- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self setupViews];
    }
    
    
    return self;
}


-(void)setupViews
{
    __weak __typeof(&*self)weakSelf = self;
    
    [self.contentView setBackgroundColor:App_background_color];
    _channelLiveIV = [[UIImageView alloc]init];
    [_channelLiveIV setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_channelLiveIV];
    [_channelLiveIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.contentView.mas_left);
        make.right.mas_equalTo(weakSelf.contentView.mas_right);
        make.bottom.mas_equalTo(weakSelf.contentView.mas_bottom).offset(-9*kDeviceRate);
        make.top.mas_equalTo(weakSelf.contentView.mas_top);
    }];
    
    
    _channelNameIV = [[UIImageView alloc]init];
    [_channelNameIV setBackgroundColor:[UIColor whiteColor]];
    [_channelLiveIV addSubview:_channelNameIV];
    [_channelNameIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_channelLiveIV.mas_bottom).offset(0*kDeviceRate);
        make.left.mas_equalTo(_channelLiveIV.mas_left);
        make.width.mas_equalTo(weakSelf.mas_width);
        make.height.mas_equalTo(49*kDeviceRate);
    }];
    
    
    
    _channelNameLabel = [[UILabel alloc]init];
    [_channelNameLabel setBackgroundColor:[UIColor whiteColor]];
    _channelNameLabel.textColor = UIColorFromRGB(0x000000);
    _channelNameLabel.font = HT_FONT_THIRD;
    [_channelNameIV addSubview:_channelNameLabel];
    [_channelNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_channelNameIV.mas_bottom).offset(-18*kDeviceRate);
        make.left.mas_equalTo(_channelNameIV.mas_left).offset(10*kDeviceRate);
        make.right.mas_equalTo(_channelNameIV.mas_right);
        make.top.mas_equalTo(18*kDeviceRate);
    }];
    
    _videoIV = [[UIImageView alloc]init];
    [_channelLiveIV addSubview:_videoIV];
    [_videoIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_channelLiveIV.mas_left);
        make.right.mas_equalTo(_channelLiveIV.mas_right);
        make.bottom.mas_equalTo(_channelNameIV.mas_top).offset(0*kDeviceRate);
        make.top.mas_equalTo(_channelLiveIV.mas_top).offset(0*kDeviceRate);
    }];
    

    
    
//    _logoBtn = [[UIButton alloc]init];
//    [_logoBtn setBackgroundColor:[HT_COLOR_ALERTCUSTOM colorWithAlphaComponent:0.5]];
//    _logoBtn.layer.borderWidth = 1;
//    _logoBtn.layer.cornerRadius = 5;
//    _logoBtn.layer.masksToBounds = YES;
//    [_logoBtn setTitle:@"时刻" forState:UIControlStateNormal];
//    [_logoBtn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
//    [_logoBtn.titleLabel setFont:HT_FONT_THIRD];
//    [_channelLiveIV addSubview:_logoBtn];
//    
//    [_logoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_channelLiveIV.mas_left).offset(12*kDeviceRate);
//        make.top.mas_equalTo(_channelLiveIV.mas_top).offset(9*kDeviceRate);
//        make.width.mas_equalTo(39*kDeviceRate);
//        make.height.mas_equalTo(18*kDeviceRate);
//    }];
    
    _hospitalBV = [[UIView alloc]init];
    [_hospitalBV setBackgroundColor:[HT_COLOR_ALERTCUSTOM colorWithAlphaComponent:0.4]];
    [_channelLiveIV addSubview:_hospitalBV];
    [_hospitalBV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_channelNameIV.mas_top);
        make.left.mas_equalTo(_channelLiveIV.mas_left);
        make.right.mas_equalTo(_channelLiveIV.mas_right);
        make.height.mas_equalTo(28*kDeviceRate);
    }];
    
    
    
    
    
    _doctorNameLabel = [[UILabel alloc]init];
    [_doctorNameLabel setBackgroundColor:[UIColor clearColor]];
    _doctorNameLabel.textColor = UIColorFromRGB(0xffffff);
    _doctorNameLabel.font = HT_FONT_THIRD;
    [_hospitalBV addSubview:_doctorNameLabel];
    [_doctorNameLabel setNumberOfLines:0];
    _doctorNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:isEmptyStringOrNilOrNull(_doctorNameLabel.text)?@"":_doctorNameLabel.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    paragraphStyle.alignment = NSTextAlignmentLeft;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_doctorNameLabel.text length])];
    _doctorNameLabel.attributedText = attributedString;
    
    [_doctorNameLabel sizeToFit];
    _doctorNameLabel.preferredMaxLayoutWidth = kDeviceWidth/2-24*kRateSize;
    [_doctorNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_hospitalBV.mas_left).offset(12*kRateSize);
        make.bottom.mas_equalTo(_hospitalBV.mas_bottom).offset(0*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
    }];


    _logoBtn = [[UIButton alloc]init];
    [_logoBtn setBackgroundColor:[HT_COLOR_ALERTCUSTOM colorWithAlphaComponent:0.5]];
//    _logoBtn.layer.borderWidth = 1;
    _logoBtn.layer.cornerRadius = 5;
    _logoBtn.layer.masksToBounds = YES;
    [_logoBtn setTitle:@"时刻" forState:UIControlStateNormal];
    [_logoBtn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [_logoBtn.titleLabel setFont:HT_FONT_THIRD];
    [_channelLiveIV addSubview:_logoBtn];
    
    [_logoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_channelLiveIV.mas_right).offset(-12*kDeviceRate);
        make.top.mas_equalTo(_channelLiveIV.mas_top).offset(9*kDeviceRate);
        make.width.mas_equalTo(39*kDeviceRate);
        make.height.mas_equalTo(18*kDeviceRate);
    }];
    
    
    
    
    _hotBV = [[UIImageView alloc]init];
    
    [_hotBV setBackgroundColor:[HT_COLOR_ALERTCUSTOM colorWithAlphaComponent:0.5]];
    [_channelLiveIV addSubview:_hotBV];
    
    [_hotBV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_channelLiveIV.mas_left).offset(0*kDeviceRate);
        make.top.mas_equalTo(_channelLiveIV.mas_top).offset(11*kDeviceRate);
        make.width.mas_equalTo(20*kDeviceRate);
        make.height.mas_equalTo(19*kDeviceRate);
    }];
    
    
    _hotIV = [[UIImageView alloc]init];
    
    [_hotIV setBackgroundColor:[UIColor clearColor]];
    [_hotIV setImage:[UIImage imageNamed:@"ic_ugc_hot_eye"]];
    [_hotBV addSubview:_hotIV];
    
    [_hotIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_hotBV.mas_centerX);
        make.centerY.mas_equalTo(_hotBV.mas_centerY);
        make.width.mas_equalTo(14*kDeviceRate);
        make.height.mas_equalTo(8*kDeviceRate);
    }];
    
    _hotTextfield = [[UITextField alloc]init];
    [_hotTextfield setBackground:[UIImage imageNamed:@"bg_ugc_home_hot"]];
    _hotTextfield.adjustsFontSizeToFitWidth = YES;
    _hotTextfield.textColor = [UIColor whiteColor];
    [_channelLiveIV addSubview:_hotTextfield];
    
    [_hotTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_hotBV.mas_right);
        make.top.mas_equalTo(_hotBV.mas_top);
        make.height.mas_equalTo(19*kDeviceRate);
    }];
    
    
}

-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink{
    
    CATransition *animation = [CATransition animation];
    //动画时间
    animation.duration = 1.0f;
    //display mode, slow at beginning and end
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //在动画执行完时是否被移除
    animation.removedOnCompletion = YES;
    //过渡效果
    animation.type = kCATransitionFade;
    //    animation.type = @"rippleEffect";
    
    //过渡方向
    animation.subtype = kCATransitionFromTop;
    //    //暂时不知,感觉与Progress一起用的,如果不加,Progress好像没有效果
    //    animation.fillMode = kCAFillModeForwards;
    //    //动画停止(在整体动画的百分比).
    //    animation.endProgress = 1;
    [imageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageLink)]  placeholderImage:[UIImage imageNamed:@"bg_ugc_home_item_two_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        [imageView.layer addAnimation:animation forKey:nil];
    }];
    
    
    
    
}

@end
