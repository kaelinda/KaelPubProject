//
//  UGCHotTopicViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCHotTopicViewController.h"
#import "ListDetailCollectionViewCell.h"
#import "UGCChannelCell.h"
#import "UGCProgramCell.h"
#import "UGCProgramPlayerVC.h"
#import "HTRequest.h"
#import "UGCBackProgramCell.h"
#import "UGCTimeProgramCell.h"
#import "HTEmptyView.h"
#import "UGCLiveViewController.h"
#import "MJRefresh.h"
#import "KDIYRefreshHeader.h"
#import "SettingHelper.h"
@interface UGCHotTopicViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,HTRequestDelegate>
@property (nonatomic, strong) UICollectionView *hotCV;

@property (nonatomic, strong) NSMutableArray *columnArray;

@property (nonatomic, strong) NSMutableArray *columnContentArray;

@property (nonatomic, strong) HTEmptyView *emptyLoadingView;

@property (nonatomic, assign)BOOL isEnterProgramVC;

@end

@implementation UGCHotTopicViewController


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //界面回来后再重新刷新一下，避免热度值有所改变
    
    if (_isEnterProgramVC == YES) {
        
        [self initRequestDataWithColumnInId:_columnInId];
    }
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNaviBar:YES];
    _columnArray = [NSMutableArray array];
    _columnContentArray = [NSMutableArray array];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0;
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _hotCV = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-HT_HIGHT_SECONDNAVIBAR-44) collectionViewLayout:layout];
    _hotCV.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _hotCV.delegate = self;
    _hotCV.dataSource = self;
    _hotCV.showsVerticalScrollIndicator = YES;
    _hotCV.allowsMultipleSelection = NO;
    [self.view addSubview:_hotCV];
    
    KDIYRefreshHeader *diyHeader = [KDIYRefreshHeader headerWithRefreshingBlock:^{
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request UGCCommonGetColumnInfoContentWithColumnInId:_columnInId];
    }];

    _hotCV.mj_header = diyHeader;

    
    
    [_hotCV registerClass:[UGCChannelCell class] forCellWithReuseIdentifier:@"listCell"];

    [_hotCV registerClass:[UGCProgramCell class] forCellWithReuseIdentifier:@"programCell"];

    [_hotCV registerClass:[UGCBackProgramCell class] forCellWithReuseIdentifier:@"backProgramCell"];
    
    [_hotCV registerClass:[UGCTimeProgramCell class] forCellWithReuseIdentifier:@"timeProgramCell"];
    
    
}

-(void)setColumnInId:(NSString *)columnInId
{
    _emptyLoadingView = [[HTEmptyView alloc]initWithEmptyFatherView:self.view andEmptyImageString:@"bg_retry_gesture" andEmptyLabelText:nil emptyViewClick:^{
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request UGCCommonGetColumnInfoContentWithColumnInId:_columnInId];
    }];
    
    _emptyLoadingView.hidden = NO;

    _columnInId = columnInId;
    
    
    [self initRequestDataWithColumnInId:_columnInId];
}


-(void)initRequestDataWithColumnInId:(NSString *)columnInId
{
    [self showCustomeHUD];
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request UGCCommonGetColumnInfoContentWithColumnInId:columnInId];
}

-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
//    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    if ([type isEqualToString:UGC_COMMON_GETCOLUMNINFOCONTENT]) {
    
        NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        
        if (ret == 0) {

        NSMutableDictionary *diction = [[NSMutableDictionary alloc]initWithDictionary:result];
        
        NSMutableDictionary *dictonaryChannel = [NSMutableDictionary dictionaryWithObjectsAndKeys:[diction objectForKey:@"channelList"],@"columnList",@"channel",@"Type", nil];
        
        NSMutableArray *backProgramArr = [NSMutableArray array];
        NSMutableArray *timeProgramArr = [NSMutableArray array];
        for (NSDictionary *dic in [diction objectForKey:@"programList"]) {
            
            if ([[dic objectForKey:@"type"]isEqualToString:@"0"]) {
                
                [backProgramArr addObject:dic];
            }else
            {
                [timeProgramArr addObject:dic];
            }
        }
    
        
        NSMutableDictionary *dictonaryBackProgram = [NSMutableDictionary dictionaryWithObjectsAndKeys:backProgramArr,@"columnList",@"backProgram",@"Type", nil];
        
        NSMutableDictionary *dictonaryTimeProgram = [NSMutableDictionary dictionaryWithObjectsAndKeys:timeProgramArr,@"columnList",@"timeProgram",@"Type", nil];
        
        
        
        NSMutableArray *dicArray = [NSMutableArray array];
        
        [dicArray insertObject:dictonaryChannel atIndex:0];
        [dicArray insertObject:dictonaryBackProgram atIndex:1];
        [dicArray insertObject:dictonaryTimeProgram atIndex:2];
        _columnArray = [dicArray mutableCopy];
        
        [self hiddenCustomHUD];
            
            
            
         
        if ([[result objectForKey:@"channelList"] count]>0 || [[result objectForKey:@"programList"] count]>0) {
            
            _emptyLoadingView.hidden = YES;
        }else
        {
            _emptyLoadingView.hidden = NO;

        }
        [_hotCV reloadData];
        
        [_hotCV.mj_header endRefreshing];
            
        }
    }
}




-(void)initDataColumn
{
    NSMutableDictionary *diction = [[NSMutableDictionary alloc]initWithDictionary:[self jsonExchange:@"getColumnInfoContent"]];
    
    
    NSMutableDictionary *dictonaryChannel = [NSMutableDictionary dictionaryWithObjectsAndKeys:[diction objectForKey:@"channelList"],@"columnList",@"channel",@"Type", nil];
    
    NSMutableArray *backProgramArr = [NSMutableArray array];
    NSMutableArray *timeProgramArr = [NSMutableArray array];
    for (NSDictionary *dic in [diction objectForKey:@"programList"]) {
        
        if ([[dic objectForKey:@"type"]isEqualToString:@"0"]) {
            
            [backProgramArr addObject:dic];
        }else
        {
            [timeProgramArr addObject:dic];
        }
    }
    
    
//    NSMutableDictionary *dictonaryProgram = [NSMutableDictionary dictionaryWithObjectsAndKeys:[diction objectForKey:@"programList"],@"columnList",@"program",@"Type", nil];

        NSMutableDictionary *dictonaryBackProgram = [NSMutableDictionary dictionaryWithObjectsAndKeys:backProgramArr,@"columnList",@"backProgram",@"Type", nil];
    
        NSMutableDictionary *dictonaryTimeProgram = [NSMutableDictionary dictionaryWithObjectsAndKeys:timeProgramArr,@"columnList",@"timeProgram",@"Type", nil];


    
    NSMutableArray *dicArray = [NSMutableArray array];
    
    [dicArray insertObject:dictonaryChannel atIndex:0];
    [dicArray insertObject:dictonaryBackProgram atIndex:1];
    [dicArray insertObject:dictonaryTimeProgram atIndex:2];
    _columnArray = [dicArray mutableCopy];


}






-(NSDictionary *)jsonExchange:(NSString *)filePath
{
    NSString * path = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@",filePath] ofType:@"json" ];
    NSData * jsonData = [[NSData alloc] initWithContentsOfFile:path];
    
    NSError * error ;
    NSMutableDictionary* jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"我的字典%@",jsonObj);
    if (!jsonObj || error) {
        NSLog(@"JSON解析失败");
    }
    
    return jsonObj;

}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return _columnArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
     _columnContentArray = [[_columnArray objectAtIndex:section] objectForKey:@"columnList"];
    
    return [[[_columnArray objectAtIndex:section] objectForKey:@"columnList"] count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"channel"]) {
    
        UGCChannelCell *cell = (UGCChannelCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"listCell" forIndexPath:indexPath];
        
        NSDictionary *cellDic = [NSDictionary dictionary];

        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];

        NSString *nameStr = @"";
        
        nameStr = isEmptyStringOrNilOrNull([cellDic objectForKey:@"name"])?@"":[cellDic objectForKey:@"name"];
        
        cell.channelNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"sessionName"])?nameStr:[cellDic objectForKey:@"sessionName"];


        cell.hospitalNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"hospital"])?@"":[cellDic objectForKey:@"hospital"];

        cell.doctorNameLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"doctorName"])?@"":[NSString stringWithFormat:@"%@医师",[cellDic objectForKey:@"doctorName"]]];


        cell.hotTextfield.text = [self hotValueChange:[NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"hot"])?@"0":[cellDic objectForKey:@"hot"]]];
        
        [cell tranAnimationWith:cell.videoIV andImageLink:isEmptyStringOrNilOrNull([cellDic objectForKey:@"pictureUrl"])?@"":[cellDic objectForKey:@"pictureUrl"]];

        return cell;
    }else if([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"timeProgram"])
    {
    
        UGCTimeProgramCell *cell = (UGCTimeProgramCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"timeProgramCell" forIndexPath:indexPath];
        NSDictionary *cellDic = [NSDictionary dictionary];

        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];

        cell.channelNameLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"name"])?@"":[cellDic objectForKey:@"name"]];

        cell.doctorNameLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"doctorName"])?@"":[NSString stringWithFormat:@"%@医师",[cellDic objectForKey:@"doctorName"]]];

        cell.hotTextfield.text = [self hotValueChange:[NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"hot"])?@"0":[cellDic objectForKey:@"hot"]]];

        [cell tranAnimationWith:cell.videoIV andImageLink:isEmptyStringOrNilOrNull([cellDic objectForKey:@"cover"])?@"":[cellDic objectForKey:@"cover"]];
        
        return cell;
    
    }
    else
    {
        UGCBackProgramCell *cell = (UGCBackProgramCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"backProgramCell" forIndexPath:indexPath];
        
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];
        cell.channelNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"name"])?@"":[cellDic objectForKey:@"name"];
        
        
        cell.hospitalNameLabel.text = isEmptyStringOrNilOrNull([cellDic objectForKey:@"hospital"])?@"":[cellDic objectForKey:@"hospital"];
        
        cell.doctorNameLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"doctorName"])?@"":[cellDic objectForKey:@"doctorName"]];
        
        
        cell.hotTextfield.text = [self hotValueChange:[NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"hot"])?@"0":[cellDic objectForKey:@"hot"]]];
        
        [cell tranAnimationWith:cell.videoIV andImageLink:isEmptyStringOrNilOrNull([cellDic objectForKey:@"cover"])?@"":[cellDic objectForKey:@"cover"]];
        
        return cell;
        
    }
    
    
   
    
}


-(NSString *)hotValueChange:(NSString *)hotResult
{
    NSString *hotValue = @"";
    
    if ([hotResult intValue]<10000) {
        
        hotValue = hotResult;
        return hotValue;
    }else
    {    NSString *hotValue = @"";

        int hotWanValue = [hotResult intValue]/10000;
        
        int hotendValue = [hotResult intValue]%10000;
        
        int hotYiValue = hotWanValue/10000;
        if (hotYiValue) {
            
            hotValue = [NSString stringWithFormat:@"%d亿",hotYiValue];
            return hotValue;
        }else
        {
        
        if (hotendValue <=1000) {
            
            hotValue = [NSString stringWithFormat:@"%d.1万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=2000)
        {
            hotValue = [NSString stringWithFormat:@"%d.2万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=3000)
        {
            hotValue = [NSString stringWithFormat:@"%d.3万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=4000)
        {
            hotValue = [NSString stringWithFormat:@"%d.4万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=5000)
        {
            hotValue = [NSString stringWithFormat:@"%d.5万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=6000)
        {
            hotValue = [NSString stringWithFormat:@"%d.6万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=7000)
        {
            hotValue = [NSString stringWithFormat:@"%d.7万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=8000)
        {
            hotValue = [NSString stringWithFormat:@"%d.8万",hotWanValue];
            return hotValue;

        }else if (hotendValue <=9000)
        {
            hotValue = [NSString stringWithFormat:@"%d.9万",hotWanValue];
            return hotValue;

        }else if (hotendValue>9000)
        {
            hotValue = [NSString stringWithFormat:@"%d万",hotWanValue+1];
            return hotValue;

        }else
        {
            return 0;
        }
        
        
    }
    
    }
    
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"channel"]){
        
            return CGSizeMake((_hotCV.width) / 1.0,  268*kRateSize);
        }else if ([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"backProgram"])
        {
            return CGSizeMake((_hotCV.width) / 1.0,  268*kDeviceRate);

        }else
        {
            return CGSizeMake((_hotCV.width -10*kDeviceRate) / 2.0, 162*kDeviceRate);
        }
        
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if ([[[_columnArray objectAtIndex:section] objectForKey:@"Type"] isEqualToString:@"channel"]){
    
        return UIEdgeInsetsMake(0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate);
    }else if ([[[_columnArray objectAtIndex:section] objectForKey:@"Type"] isEqualToString:@"backProgram"])
    {
        return UIEdgeInsetsMake(0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate);

    }
    
    else
    {
        return UIEdgeInsetsMake(0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate, 0*kDeviceRate);

    }
}



#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    __weak __typeof(&*self)weakSelf = self;

    _isEnterProgramVC = YES;
    if ([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"channel"])
    {
        
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];
        
        
        
        [UGCLiveViewController canInitRecorderWithCallBack:^(BOOL graned,UGCAuthType type) {
            if (graned) {
                UGCLiveViewController *liveVC = [[UGCLiveViewController alloc] initWithUserType:kUGCUser_Player andChannelID:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"channelId"]]andUserName:[cellDic objectForKey:@"doctorName"]];
                
                NSString *screenFlag = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"screenFlg"])?@"":[cellDic objectForKey:@"screenFlg"]];
                
                if ([screenFlag length]<=0) {
                    
                    liveVC.isPortrait = NO;
                    
                }else
                {
                    liveVC.isPortrait = YES;
                }

                
                [weakSelf.navigationController pushViewController:liveVC animated:NO];
            }
            
            else{
                
                if (type == kUGCAuthType_video) {
                    
                    
                    if (isAfterIOS10) {
                        [SettingHelper openURLSettingWithType:kSettingsURLString andURLString:nil];
                        
                    }else
                    {
                        ZSToast(@"请开启摄像头权限");

                    }
                    
                }else
                {
                    if (isAfterIOS10) {
                        [SettingHelper openURLSettingWithType:kBoundleID andURLString:nil];
                        
                    }else
                    {
                        ZSToast(@"请开启麦克风权限");
                        
                    }

                }
                
            }
            
            
            
            
        }];
        

        
        
    }else if([[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"Type"] isEqualToString:@"timeProgram"])
    {
        
//        UGCTimeProgramCell *cell = (UGCTimeProgramCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"timeProgramCell" forIndexPath:indexPath];
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];

        BOOL isConstraintLandscape;
        UGCProgramPlayerVC *ugcPlayer = [[UGCProgramPlayerVC alloc]init];
        
        NSString *screenFlag = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"screenFlg"])?@"":[cellDic objectForKey:@"screenFlg"]];
        
        if ([screenFlag length]<=0 || [screenFlag integerValue] == 0) {
            //默认横屏 0：横屏   1：竖屏
            isConstraintLandscape = YES;
            
        }else
        {
            isConstraintLandscape = NO;
        }
        
        ugcPlayer.IsConstraintLandscape = isConstraintLandscape;
        
        [ugcPlayer ugcProgramProgramID:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"programId"]] andProgramType:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"type"]] andIsConstraintLandscape:isConstraintLandscape andprogramTitleStr:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"name"]]];
        
            [self.navigationController pushViewController:ugcPlayer animated:YES];

    }else
    {
        NSDictionary *cellDic = [NSDictionary dictionary];
        
        cellDic = [[[_columnArray objectAtIndex:indexPath.section] objectForKey:@"columnList"] objectAtIndex:indexPath.row];
        
        BOOL isConstraintLandscape;
        UGCProgramPlayerVC *ugcPlayer = [[UGCProgramPlayerVC alloc]init];
        
        NSString *screenFlag = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull([cellDic objectForKey:@"screenFlg"])?@"":[cellDic objectForKey:@"screenFlg"]];
        
        if ([screenFlag length]<=0 || [screenFlag integerValue] == 0) {
            
            isConstraintLandscape = YES;
            
        }else
        {
            isConstraintLandscape = NO;
        }
        ugcPlayer.IsConstraintLandscape = isConstraintLandscape;
        [ugcPlayer ugcProgramProgramID:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"programId"]] andProgramType:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"type"]] andIsConstraintLandscape:isConstraintLandscape andprogramTitleStr:[NSString stringWithFormat:@"%@",[cellDic objectForKey:@"name"]]];
        
        [self.navigationController pushViewController:ugcPlayer animated:YES];
    }
    
   
    
    
    
    

   
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
