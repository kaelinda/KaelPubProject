//
//  UGCHomeViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/3/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//
#define WEAK(weaks,s)  __weak __typeof(&*s)weaks = s;

#import "UGCHomeViewController.h"
#import "UGCHotTopicViewController.h"
#import "UGCChannelViewController.h"
#import "UGCProgramViewController.h"
#import "UGCPlayer.h"
#import "HTRequest.h"
#import "MLMOptionSelectView.h"
#import "UGCOpotionCell.h"
#import "UGCManagerHomeViewController.h"
#import "UserLoginViewController.h"
#import "HTALViewController.h"
#import "UGCAuthBasicInfoViewController.h"
#import "HTYJUserCenter.h"
#import "UGCUpLoadedManagerViewController.h"
#import "SettingHelper.h"
#define RENZHENGTAG     11600
#define RENZHENGINGTAG  11601
#define RENZHENGFAILE   11602
@interface UGCHomeViewController ()<HTRequestDelegate,CustomAlertViewDelegate>
{
    UGCPlayer *player;
    NSMutableArray *listArray;

}
@property (nonatomic,strong) UIButton *rightNavBtn;
@property (nonatomic, strong) MLMOptionSelectView *cellView;
//@property (nonatomic,strong) UGCPlayerView *player;
@property (nonatomic,strong)UIButton *playButton;
@property (nonatomic,strong)UIButton *stopButton;
@property (nonatomic,strong)UILabel *statusLable;
@property (nonatomic,strong)NSMutableArray *columnList;
@property (nonatomic,strong)NSMutableArray *colounMenuArr;
@property (nonatomic,strong)NSMutableDictionary  *AuthenticSuccessedData;
@end

@implementation UGCHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //初始化标题
    [self setNaviBarTitle:@"直播"];

    _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [_rightNavBtn setTitle:@"开播" forState:UIControlStateNormal];
    _rightNavBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_rightNavBtn setTitleColor:UIColorFromRGB(0xffce6f) forState:UIControlStateNormal];
    [_rightNavBtn addTarget:self action:@selector(startLive:) forControlEvents:UIControlEventTouchUpInside];

    [self setNaviBarRightBtnSize:CGSizeMake(40, 40) andRightBtn:_rightNavBtn andRightBtnTitleLabelFont:[UIFont systemFontOfSize:16]];

    
    
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
  
    self.magicView.navigationColor = HT_COLOR_BACKGROUND_APP;
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleCenter;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 10.0;
    self.magicView.navigationInset = UIEdgeInsetsMake(0, 30, 0, 30);
//    self.magicView.itemSpacing = 3*kDeviceRate;
//    self.magicView.itemScale = 1.1;
    self.magicView.itemSpacing = 50*kDeviceRate;
    _columnList = [NSMutableArray array];
    _colounMenuArr = [NSMutableArray array];
    
    
    [self initRequestData];

}


-(void)opentionView
{
    
    
    listArray = [NSMutableArray array];
    
    listArray = [[self jsonExchange:@"opotionView"] objectForKey:@"optionList"];
    
    _cellView = [[MLMOptionSelectView alloc] initOptionView];
    

    
    
    [self showView];
}




-(NSDictionary *)jsonExchange:(NSString *)filePath
{
    NSString * path = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@",filePath] ofType:@"json" ];
    NSData * jsonData = [[NSData alloc] initWithContentsOfFile:path];
    
    NSError * error ;
    NSMutableDictionary* jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"我的字典%@",jsonObj);
    if (!jsonObj || error) {
        NSLog(@"JSON解析失败");
    }
    
    return jsonObj;
    
}

- (void)showView {
    [self defaultCell];
    _cellView.vhShow = NO;
    _cellView.optionType = MLMOptionSelectViewTypeArrow;
    _cellView.edgeInsets = UIEdgeInsetsMake(65, 10, 10, 10);
    [_cellView showTapPoint:CGPointMake(SCREEN_WIDTH -35, 64 + 1) viewWidth:131*kDeviceRate direction:MLMOptionSelectViewBottom];
    


   
}


- (void)defaultCell {
    

    WEAK(weaklistArray, listArray);
    WEAK(weakSelf, self);
    _cellView.canEdit = NO;
    [_cellView registerClass:[UGCOpotionCell class] forCellReuseIdentifier:@"DefaultCell"];
    _cellView.cell = ^(NSIndexPath *indexPath){
        UGCOpotionCell *cell = (UGCOpotionCell*)[weakSelf.cellView dequeueReusableCellWithIdentifier:@"DefaultCell"];

        NSMutableDictionary *diction = [[NSMutableDictionary alloc]init];
        
        diction = [weaklistArray objectAtIndex:indexPath.row];
        
        cell.opotionLabel.text = [NSString stringWithFormat:@"%@",[diction objectForKey:@"title"]];
        
            [cell.opotionIV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[diction objectForKey:@"pictureUrl"]]]];


        
        
        return cell;
    };
    _cellView.optionCellHeight = ^{
        return 50.f;
    };
    _cellView.rowNumber = ^(){
        return (NSInteger)weaklistArray.count;
    };
    
    _cellView.selectedOption= ^(NSIndexPath *indexPath)
    {
     
//        [weaklistArray objectAtIndex:indexPath.row];
        
        if (indexPath.row == 0) {
            [UGCLiveViewController canInitRecorderWithCallBack:^(BOOL graned,UGCAuthType type) {
                if (graned) {
                    UGCLiveViewController *liveVC = [[UGCLiveViewController alloc] initWithUserType:kUGCUser_Recorder andChannelID:[NSString stringWithFormat:@"%@",[weakSelf.AuthenticSuccessedData objectForKey:@"channelId"]] andUserName:@""];

                    [weakSelf.navigationController pushViewController:liveVC animated:NO];
                }
                
                else{
                    
                    if (type == kUGCAuthType_video) {
                        
                        
                        if (isAfterIOS10) {
                            [SettingHelper openURLSettingWithType:kSettingsURLString andURLString:nil];
                            
                        }else
                        {
                            ZSToast(@"请开启摄像头权限");
                            
                        }
                        
                    }else
                    {
                        if (isAfterIOS10) {
                            [SettingHelper openURLSettingWithType:kBoundleID andURLString:nil];
                            
                        }else
                        {
                            ZSToast(@"请开启麦克风权限");
                            
                        }
                        
                    }

                    
                }
                
                
                
                
            }];

            
            
        }else if (indexPath.row == 1)
        {
            NSLog(@"上传的类写此");
            
//            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowVideo];//单选视频
//            pickerVc.groupType = KAssetsLabraryVideo;//视频是照片的具体分类
//            pickerVc.maxCount = 1;//设置最多能选几张图片
//            pickerVc.choiceType = ALSingleChoice;
//            
//            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
//            [weakSelf presentViewController:nav animated:YES completion:nil];
            UGCManagerHomeViewController *homeManager = [[UGCManagerHomeViewController alloc]init];
            
            [weakSelf.navigationController pushViewController:homeManager animated:YES];
            
            [homeManager makeAlert];
            
        }else
        {
            UGCManagerHomeViewController *homeManager = [[UGCManagerHomeViewController alloc]init];
            
            [weakSelf.navigationController pushViewController:homeManager animated:YES];
            
        }
        
        
    };
    
}


-(void)initRequestData
{
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request UGCCommonGetColumnInfoWithReturn];
}


#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    if (_colounMenuArr.count>0) {
        
        return _colounMenuArr;
    }

    return 0;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        //        menuItem.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17*kRateSize];
        menuItem.titleLabel.font = HT_FONT_SECOND;   //App_font(12*kDeviceRate);
    }
    return menuItem;
}


- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
      NSLog(@"滑动的PageIndex%ld",pageIndex);
    
  
//    if (0==pageIndex) {

    static NSString *hotTopic = @"HotTopic.identifier";
    UGCHotTopicViewController *hotTopicVC = [magicView dequeueReusablePageWithIdentifier:hotTopic];

    if (!hotTopicVC) {
        hotTopicVC = [[UGCHotTopicViewController alloc] init];
        if (_columnList.count>0) {
             NSString *columnInId = [NSString stringWithFormat:@"%@",[[_columnList objectAtIndex:pageIndex] objectForKey:@"id"]];
//                hotTopicVC.columnInId = columnInId;
            [hotTopicVC setColumnInId:columnInId];
        }
    }
//        recomViewController.menuInfo = menuInfo;
    return hotTopicVC;

//    }else if (1 == pageIndex)
//    {
//       
//        static NSString *channel = @"Channel.identifier";
//        UGCChannelViewController *channelVC = [magicView dequeueReusablePageWithIdentifier:channel];
//        if (!channelVC) {
//            channelVC = [[UGCChannelViewController alloc] init];
//            NSString *columnInId = [NSString stringWithFormat:@"%@",[[_columnList objectAtIndex:pageIndex] objectForKey:@"id"]];
//
//            [channelVC setColumnInId:columnInId];
//
//        }
//        return channelVC;
//
//        
//    }else
//    {
//        static NSString *program = @"Program.identifier";
//        UGCProgramViewController *programVC = [magicView dequeueReusablePageWithIdentifier:program];
//        if (!programVC) {
//            programVC = [[UGCProgramViewController alloc] init];
//            NSString *columnInId = [NSString stringWithFormat:@"%@",[[_columnList objectAtIndex:pageIndex] objectForKey:@"id"]];
//
//            [programVC setColumnInId:columnInId];
//
//        }
//        return programVC;
//
//    }
    
 
    
}


#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppear:(UGCHotTopicViewController *)viewController atPage:(NSUInteger)pageIndex {
        NSLog(@"index:%ld viewDidAppear:%@", (long)pageIndex, viewController.view);
//    if ([viewController isKindOfClass:[UGCHotTopicViewController class]]) {
//        
//        NSLog(@"我是热门额")
//    }else if ([viewController isKindOfClass:[UGCChannelViewController class]])
//    {
//        NSLog(@"我是直播额");
//    }else
//    {
//        NSLog(@"我是时刻");
//    }
    
    
//    if (_columnList.count>0) {
//        NSString *columnInId = [NSString stringWithFormat:@"%@",[[_columnList objectAtIndex:pageIndex] objectForKey:@"id"]];
//        [viewController setColumnInId:columnInId];
//        
//    }


    
}



-(void)startLive:(UIButton *)btn{
    
//    UGCLiveViewController *liveVC = [[UGCLiveViewController alloc] initWithUserType:kUGCUser_Recorder andChannelID:@"13290" andUserName:@"无fuck说"];
//    [self.navigationController pushViewController:liveVC animated:YES];
//  
//    return;

//    UGCManagerHomeViewController *homeManager = [[UGCManagerHomeViewController alloc]init];
//    
//    [self.navigationController pushViewController:homeManager animated:YES];
//    
//    return;

    if (IsLogin == NO) {
        
        UserLoginViewController *user = [[UserLoginViewController alloc]init];
        
        [self.navigationController pushViewController:user animated:YES];
        
    }else
    {
        __weak __typeof(&*self)weakSelf = self;
           MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //显示的文字
                HUD.labelText = @"正在校验";
                //是否有庶罩
                HUD.dimBackground = NO;
                [HUD show:YES];
        
        [[HTYJUserCenter userCenter] checkDoctorAuthWithCallBack:^(UGCAuthenticStatus authStatus, NSDictionary *dic) {
            
            [HUD hide:YES];
            
            switch (authStatus) {
                case KAuthenticed:
                {
                    _AuthenticSuccessedData = [NSMutableDictionary dictionaryWithDictionary:dic];
                    
                    //[[NSUserDefaults standardUserDefaults]objectForKey:@"AuthenticSuccessedData"]
                        
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [weakSelf opentionView];

                       });
                }
                    break;
                case KAuthenticing:
                {
                    CustomAlertView *renzhengAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"您的医生身份正在认证中,无法开启直\n播我们会尽快完成您的认证申请,请见\n谅" andButtonNum:1 andCancelButtonMessage:@"我知道了" andOKButtonMessage:@"" withDelegate:self];
                    
                    renzhengAlert.tag = RENZHENGINGTAG;
                    [renzhengAlert show];

                }
                    break;
                case KunAuthentic:
                {
                    CustomAlertView *renzhengAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"尊敬的用户您好,需要认证医生身份才\n能进行直播,现在去认证吗?" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"我认证" withDelegate:self];
                    
                    renzhengAlert.tag = RENZHENGTAG;
                    [renzhengAlert show];

                }
                    break;
                    case KAuthenticFailed:
                {
                    CustomAlertView *renzhengAlert = [[CustomAlertView alloc] initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"身份校验失败,请重试！" andButtonNum:2 andCancelButtonMessage:@"取消" andOKButtonMessage:@"重试" withDelegate:self];
                    
                    renzhengAlert.tag = RENZHENGFAILE;
                    [renzhengAlert show];
                }
                    break;
                    
                default:
                    break;
            }
        

        }];

        
        
        
        
        
        
        
        
    }

    
    



    
    
    
}


- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case RENZHENGTAG:
        {
            if (buttonIndex == 0) {
                
                UGCAuthBasicInfoViewController *basicInfo = [[UGCAuthBasicInfoViewController alloc]init];
                
                [self.navigationController pushViewController:basicInfo animated:YES];
 
            }else
            {
                return;
            }
            
        }
            break;
        case RENZHENGINGTAG:
        {
            if (buttonIndex == 0) {
                
                return;
            }else
            {
                
            }
        }
            break;
        case RENZHENGFAILE:
        {
            if (buttonIndex == 0) {
                
                MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //显示的文字
                HUD.labelText = @"正在校验";
                //是否有庶罩
                HUD.dimBackground = NO;
                [HUD show:YES];

                
            [[HTYJUserCenter userCenter]checkDoctorAuthWithCallBack:^(UGCAuthenticStatus authStatus, NSDictionary *dic) {
                [HUD hide:YES];

                NSLog(@"失败重新试了");
            }];
            }else
            {
                
            }
        }
            break;
            
        default:
            break;
    }
}


-(void)initOptionProperty{

}


-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    if ([type isEqualToString:UGC_COMMON_GETCOLUMNINFO]) {
        
        _columnList = [result objectForKey:@"list"];
        for (NSDictionary *dicti in _columnList) {
            
            [_colounMenuArr addObject:[dicti objectForKey:@"name"]];
        }
        
        [self.magicView reloadData];

    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
