//
//  UGCBackProgramCell.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/3/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGCBackProgramCell : UICollectionViewCell
@property (nonatomic,strong)UIImageView *channelLiveIV;
@property (nonatomic,strong)UIImageView *videoIV;
@property (nonatomic,strong)UIButton *logoBtn;
@property (nonatomic,strong)UIView *hospitalBV;
@property (nonatomic,strong)UILabel *hospitalNameLabel;
@property (nonatomic,strong)UILabel *doctorNameLabel;
@property (nonatomic,strong)UIImageView *channelNameIV;
@property (nonatomic,strong)UILabel *channelNameLabel;
@property (nonatomic,strong)UIImageView *hotBV;
@property (nonatomic,strong)UIImageView *hotIV;
@property (nonatomic,strong)UILabel *hotValueLabel;
@property (nonatomic,strong)UITextField *hotTextfield;
-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink;

@end
