//
//  NewSearchViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/20.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//
#define numToString(a) [NSString stringWithFormat:@"index%ld",(a)]

#define StringToString(a) [NSString stringWithFormat:@"index%@",(a)]
#import "NewSearchViewController.h"
#import "HTRequest.h"
#import "HotSearchTableViewCell.h"
#define HeaderTag 1000
#import "LocalCollectAndPlayRecorder.h"

#import "DemandHomeView.h"

#import "headScroll.h"

#import "UIButton_Block.h"


#import "ProgramListCell.h"
#import "Tool.h"

#import "LiveTuiJianCell.h"

#import "EPGCell.h"
#import "DemandVideoPlayerVC.h"
#import "SearchHistoryTableViewCell.h"
#import "MBProgressHUD.h"

#import "AlertLabel.h"

#import "EpgSmallVideoPlayerVC.h"

#import "SAIInformationManager.h"
#import "TV_EPGTableViewCell.h"

#import "VODShowTableViewCell.h"
#import "CustomAlertView.h"
#import "LocalCollectAndPlayRecorder.h"

#import "NewHotSearchTableViewCell.h"
#import "NewHistorySearchTableViewCell.h"
#import "NewKeyWordSearchTableViewCell.h"
#import "UserLoginViewController.h"

static NSInteger sss= 0;
@interface NewSearchViewController ()<HTRequestDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,CustomAlertViewDelegate>
{
    UITextField *_seachTextView;//搜索文本
    
    //记录搜索处于逻辑状态
    //    1:热搜词，点播优先，直播
    //    2: 搜索历史，无点播直播状态  但head有区别，2.1直接显示， 2.2 搜索不到内容显示
    //    3:搜索联想词点播联想词优先，直播联想词后  只有点播无结果才会请求直播联想词 ，
    //    4:搜索结果页，分为直播和点播结果可以切换还可以分日期  单独tableView，搜索历史和热刺可以共用一个tableView改变cell和header footer
    //    四种状态不能共存，互斥性
    
    //状态  State
    BOOL StateHotSeatch;//热搜
    BOOL StateHistory;//搜索历史
    BOOL StateAssociate;//联想词
    BOOL StateResualtNO;//搜索结果
    BOOL StateResualtHave;//搜索结果
    
    
    UIView *HeaderView;//头视图
    UIView *FooterView;//脚视图
    
    NSString *searchKey;//搜索词
    
    UIButton *_cancle_searchBtn;//搜索取消按钮
    
    NSMutableArray *_EPGArray;//直播数组
    NSMutableArray *_VODArray;//点播数组
    
    
    //多种状态时
    UIView *resaultBase ;//结果页基础
    UIButton_Block *selectedBTN;//短重的直播点播
    
    NSMutableDictionary *VODEPG_HaveDictionary;//直播点播有数据是的数组
    
    NSInteger _currenIndex;//搜索有数据时，当前页面
    
    UIButton_Block *VODBtn;//点播按钮
    UIButton_Block *EPGBtn;//直播按钮
    UIView *VODEPGLine;//直播点播按钮下划线
    UIView *VODEPGLine2;//直播点播按钮下划线
    
    
    
    NSMutableDictionary *EPGDiction;//天数数组
    NSMutableArray *EPGArray;//天数数组
    NSMutableArray *EPGAllKeyArray;//所有key
    NSMutableArray *EPGAllDateList;//所有key
    NSInteger EPG_SECTION_Number;//epg  天数分组数据
    
    UIButton *backBtn;
    
    UIScrollView *VODEPG_Search_ScrollView;//承载直播点播tableview的滚动视图
    UITableView *VOD_TableView;//点播列表视图
    UITableView *EPG_TableView;//直播列表视图
    
    BOOL first;//第一次进入搜索页
    
    BOOL isNOSearchResault;//无搜索结果
    
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    
    UIImageView *NOResault;//没有搜索结果
    
    NSArray *_pngArr;
    
    NSDictionary *_addScheduleItemDic;//ww全局记录被预约节目属性字典
    NSString *_delScheduleEventID;//ww取消预约节目的ID
    
    
    NSString *defaultSearchStr;//搜索词
}
@property (nonatomic,strong)NSMutableArray *HHAArray;////热词联想历史数组，存放数据源
@property (nonatomic,strong)NSMutableArray *HistoryHotArray;////热词联想历史数组，存放数据源
@property (nonatomic,strong)NSMutableDictionary *HistoryHotDiction;
@property (nonatomic,strong)UITableView *HHATableView;////ww...联想词table
@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation NewSearchViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickClearBtn:) name:UITextFieldTextDidChangeNotification object:nil ];
    
    if (first) {
        //2*第一次进入搜索页，状态转变为热搜状态
        [self changeState:&StateHotSeatch];//公版改进
        
        first = NO;
    }
    
    [MobCountTool pushInWithPageView:@"SearchPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    
    [MobCountTool popOutWithPageView:@"SearchPage"];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList) name:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil];
    
   
    first = YES;
    //初始化这个存放直播点播有数据时的数组
    VODEPG_HaveDictionary = [NSMutableDictionary dictionary];
    EPGAllDateList= [NSMutableArray array];
    EPGAllKeyArray = [NSMutableArray array];
    
    defaultSearchStr = [[NSString alloc]init];
    //2.1创建请求类
    
    //3.初始化存放数组
    self.HHAArray = [NSMutableArray array];
    self.HistoryHotArray = [NSMutableArray array];//公版改进
    self.HistoryHotDiction = [NSMutableDictionary dictionary];
    
    _VODArray = [NSMutableArray array];
    _EPGArray = [NSMutableArray array];//初始化，直播和点播存储数组
    
    
    [self setNaviBarLeftBtn:nil];
    
    
    
    _cancle_searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _cancle_searchBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"取消" target:self action:@selector(cancel_search:)];
    _cancle_searchBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_user_cancel" imgHighlight:@"btn_user_cancel_pressed" imgSelected:@"btn_user_cancel" target:self action:@selector(cancel_search:)];
    
    [self setNaviBarRightBtn:_cancle_searchBtn];
    
    //4:创建HHA   tableView
    [self setUpTableView];
    
    //1:navigationBar
    [self setSearchBar];
    
    [_seachTextView becomeFirstResponder];
    
    
    //创建展示结果的视图中，让两个tableView加载数据
    if (!resaultBase) {
        
        [self setupResaultBase];
        
    }
    
    [self show];
    
    _pngArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"search_search"],[UIImage imageNamed:@"btn_user_cancel"], nil];
}

//创建展示结果页，只创建一次用来展示直播点播搜索结果
- (void)setupResaultBase
{
    
    NSInteger high = 0;;
    NSInteger HEADERhigh = 0;;
    if (_IS_SUPPORT_VOD) {
        high = 0;
    }else
    {
        high = -30;
        HEADERhigh = -8;
    }
    resaultBase = [[UIView alloc]initWithFrame:CGRectMake(0, 64+high,kDeviceWidth, KDeviceHeight - 64-high+90)];
    
    resaultBase.backgroundColor = UIColorFromRGB(0xeeeeee);
    
    VODEPG_Search_ScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,  (45+HEADERhigh)-5*kRateSize, kDeviceWidth, KDeviceHeight-64-(44+HEADERhigh)-high)];
    VODEPG_Search_ScrollView.contentSize = CGSizeMake(kDeviceWidth * 2, KDeviceHeight-64-(44+HEADERhigh));
    //    VODEPG_Search_ScrollView.backgroundColor =UIColorFromRGB(0xdfdfdf);
    VODEPG_Search_ScrollView.backgroundColor =UIColorFromRGB(0xf7f7f7);
    
    VOD_TableView = [[UITableView alloc]initWithFrame:CGRectMake(kDeviceWidth, 0, kDeviceWidth, KDeviceHeight-64-(44+HEADERhigh))];
    //    VOD_TableView.backgroundColor = [UIColor yellowColor];
    VOD_TableView.delegate  = self;
    VOD_TableView.dataSource = self;
    [VODEPG_Search_ScrollView addSubview:VOD_TableView];
    
    
    EPG_TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-(44+HEADERhigh)-high)];
    //    EPG_TableView.backgroundColor = [UIColor redColor];
    EPG_TableView.delegate = self;
    EPG_TableView.dataSource = self;
    [VODEPG_Search_ScrollView addSubview:EPG_TableView];
    
    VOD_TableView.backgroundColor = UIColorFromRGB(0xf7f7f7);
    EPG_TableView.backgroundColor = UIColorFromRGB(0xf7f7f7);
    
    VODEPG_Search_ScrollView.pagingEnabled = YES;
    VODEPG_Search_ScrollView.delegate = self;
    [resaultBase addSubview:VODEPG_Search_ScrollView];
    
    if (_IS_SUPPORT_VOD) {
        VODEPG_Search_ScrollView.scrollEnabled = YES;
    }else
    {
        VODEPG_Search_ScrollView.scrollEnabled = NO;
    }
    
    [self.view addSubview:resaultBase];
    {
        UIFont *font = [UIFont systemFontOfSize:16*kRateSize];
        EPGBtn = [[UIButton_Block alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth/2, (44+HEADERhigh)-5*kRateSize)];
        
        [resaultBase addSubview:EPGBtn];
        
        EPGBtn.backgroundColor = UIColorFromRGB(0xffffff);
        [EPGBtn setTitle:@"直播" forState:UIControlStateNormal];
        [EPGBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        selectedBTN = EPGBtn;
        EPGBtn.selected = YES;
        [EPGBtn setTitleColor:App_selected_color forState:UIControlStateSelected];
        
        EPGBtn.titleLabel.font = font;
        
        __weak UIButton_Block *WEPGBtn = EPGBtn;
        __weak UIButton_Block *WVODBtn = VODBtn;
        __weak UIView *WVODEPGLine = VODEPGLine;
        __weak UIView *WVODEPGLine2 = VODEPGLine2;
        __weak UIScrollView *WScrollView = VODEPG_Search_ScrollView;
        EPGBtn.Click = ^ (UIButton_Block*btn,NSString *name)
        {
            WEPGBtn.selected = YES;
            WVODBtn.selected = NO;
            if (WEPGBtn.selected) {
                WVODBtn.backgroundColor = [UIColor whiteColor];
                WEPGBtn.backgroundColor =UIColorFromRGB(0xffffff);
                WVODEPGLine.hidden = NO;
                WVODEPGLine2.hidden = YES;
                
            }else
            {
                WVODBtn.backgroundColor =UIColorFromRGB(0xffffff);
                WEPGBtn.backgroundColor = [UIColor whiteColor];
                WVODEPGLine2.hidden = NO;
                WVODEPGLine.hidden = YES;
            }
            
            
            
            
            [WScrollView setContentOffset:CGPointMake(0, 0)];
            _currenIndex = 0;
            //            [EPG_TableView reloadData];
            
        };
        
        VODBtn = [[UIButton_Block alloc]initWithFrame:CGRectMake(kDeviceWidth/2, 0, kDeviceWidth/2,  (44+HEADERhigh)-5*kRateSize)];
        [VODBtn setTitle:@"点播" forState:UIControlStateNormal];
        [VODBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [VODBtn setTitleColor:App_selected_color forState:UIControlStateSelected];
        [resaultBase addSubview:VODBtn];
        //        VODBtn.backgroundColor = [UIColor whiteColor];
        VODBtn.titleLabel.font = font;
        VODBtn.Click = ^ (UIButton_Block*btn,NSString *name)
        {
            WEPGBtn.selected = NO;
            WVODBtn.selected = YES;
            if (WVODBtn.selected) {
                WVODBtn.backgroundColor = [UIColor whiteColor];
                WEPGBtn.backgroundColor =UIColorFromRGB(0xffffff);
                WVODEPGLine.hidden = NO;
                WVODEPGLine2.hidden = YES;
                
            }else
            {
                WVODBtn.backgroundColor =UIColorFromRGB(0xffffff);
                WEPGBtn.backgroundColor = [UIColor whiteColor];
                WVODEPGLine2.hidden = NO;
                WVODEPGLine.hidden = YES;
            }
            
            
            
            [WScrollView setContentOffset:CGPointMake(kDeviceWidth, 0)];
            _currenIndex = 1;
            //            [VOD_TableView reloadData];
            
        };
        
        
        
        UIView *line = [UIView new];
        line.backgroundColor = UIColorFromRGB(0xeeeeee);
        [resaultBase addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(resaultBase.mas_left);
            make.right.equalTo(resaultBase.mas_right);
            make.height.equalTo(@(1));
            make.top.equalTo(EPGBtn.mas_bottom).offset(-1);
        }];
        
        VODEPGLine = [[UIView alloc]init ];
        [resaultBase addSubview:VODEPGLine];
        VODEPGLine.backgroundColor = App_selected_color;
        [VODEPGLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(EPGBtn.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
            make.width.equalTo(@(100));
            make.centerX.equalTo(EPGBtn.mas_centerX);
        }];
        
        VODEPGLine2 = [[UIView alloc]init ];
        [resaultBase addSubview:VODEPGLine2];
        VODEPGLine2.backgroundColor = App_selected_color;
        [VODEPGLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(VODBtn.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
            make.width.equalTo(@(100));
            make.centerX.equalTo(VODBtn.mas_centerX);
        }];
        
        
        EPGBtn.selected = YES;
        if (EPGBtn.selected) {
            VODBtn.backgroundColor = UIColorFromRGB(0xffffff);
            EPGBtn.backgroundColor = [UIColor whiteColor];
            VODEPGLine.hidden = NO;
            VODEPGLine2.hidden = YES;
        }else
        {
            VODBtn.backgroundColor = [UIColor whiteColor];
            EPGBtn.backgroundColor = UIColorFromRGB(0xffffff);
            VODEPGLine2.hidden = NO;
            VODEPGLine.hidden = YES;
        }
        
    }
    
}

- (void)setUpTableView
{
    
    UIView *view =[UIView new];
    [self.view addSubview:view];
    
    NOResault = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64,kDeviceWidth, KDeviceHeight-64)];
    //    NOResault.backgroundColor = [UIColor lightGrayColor];
    NOResault.backgroundColor = UIColorFromRGB(0xf7f7f7);
    [self.view addSubview:NOResault];
    
    
    
    UILabel *NOSearchLabel = [UILabel new];
    [NOResault addSubview:NOSearchLabel];
    NOSearchLabel.textAlignment = NSTextAlignmentCenter;
    NOSearchLabel.text = @"暂无搜索结果，换个词试试吧";
    NOSearchLabel.textColor = UIColorFromRGB(0x5a5a5a);
    [NOSearchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(NOResault.mas_centerX);
        make.centerY.equalTo(NOResault.mas_centerY);
        make.width.equalTo(NOResault.mas_width);
        make.height.equalTo(@(30*kRateSize));
    }];
    UIImageView *search = [[UIImageView alloc]init];
    [NOResault addSubview:search];
    search.image = [UIImage imageNamed:@"ic_search_result_empty"];
    [search mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(NOSearchLabel.mas_centerX);
        make.bottom.equalTo(NOSearchLabel.mas_top);
        make.width.equalTo(@(60*kRateSize));
        make.height.equalTo(@(60*kRateSize));
    }];
    
    
    
    self.HHATableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,kDeviceWidth, KDeviceHeight-64)];
    self.HHATableView.delegate =self;
    self.HHATableView.dataSource = self;
    self.HHATableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.HHATableView];
    self.HHATableView.backgroundColor = UIColorFromRGB(0xf7f7f7);
}

//滚动的vod epg 切换按钮颜色
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //
    if (StateHistory || StateAssociate) {
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    }
    
    [_seachTextView resignFirstResponder];
    int page = 0;
    if (scrollView == VODEPG_Search_ScrollView) {
        page =  (  scrollView.contentOffset.x + kDeviceWidth/2 ) /kDeviceWidth;
        if (page == 1) {
            EPGBtn.selected = NO;
            VODBtn.selected = YES;
            _currenIndex = 1;
            
            VODBtn.selected = YES;
            if (VODBtn.selected) {
                EPGBtn.backgroundColor = UIColorFromRGB(0xffffff);
                VODBtn.backgroundColor = [UIColor whiteColor];
                VODEPGLine2.hidden = NO;
                VODEPGLine.hidden = YES;
            }else
            {
                EPGBtn.backgroundColor = [UIColor whiteColor];
                VODBtn.backgroundColor = UIColorFromRGB(0xffffff);
                VODEPGLine.hidden = NO;
                VODEPGLine2.hidden = YES;
            }
            
            {//请求不到数据的处理
                UILabel *view = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-44)];
                
                UILabel *NOSearchLabel = [UILabel new];
                [view addSubview:NOSearchLabel];
                NOSearchLabel.textAlignment = NSTextAlignmentCenter;
                NOSearchLabel.text = @"暂无点播搜索结果";
                NOSearchLabel.textColor = UIColorFromRGB(0x5a5a5a);
                [NOSearchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(view.mas_centerX);
                    make.centerY.equalTo(view.mas_centerY);
                    make.width.equalTo(view.mas_width);
                    make.height.equalTo(@(30*kRateSize));
                }];
                
                
                UIImageView *search = [[UIImageView alloc]init];
                [view addSubview:search];
                search.image = [UIImage imageNamed:@"ic_search_result_empty"];
                [search mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(NOSearchLabel.mas_centerX);
                    make.bottom.equalTo(NOSearchLabel.mas_top);
                    make.width.equalTo(@(60*kRateSize));
                    make.height.equalTo(@(60*kRateSize));
                }];
                
                
                VOD_TableView.tableFooterView = view;
            }
            
            [VOD_TableView reloadData];
        }
        if (page ==0) {
            EPGBtn.selected = YES;
            VODBtn.selected = NO;
            _currenIndex = 0;
            EPGBtn.selected = YES;
            if (EPGBtn.selected) {
                VODBtn.backgroundColor = UIColorFromRGB(0xffffff);
                EPGBtn.backgroundColor = [UIColor whiteColor];
                VODEPGLine.hidden = NO;
                VODEPGLine2.hidden = YES;
            }else
            {
                EPGBtn.backgroundColor = [UIColor whiteColor];
                EPGBtn.backgroundColor = UIColorFromRGB(0xffffff);
                VODEPGLine2.hidden = NO;
                VODEPGLine.hidden = YES;
            }
            //
            
            {//请求不到数据的处理  设置Footer
                UILabel *view = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-44)];
                
                UILabel *NOSearchLabel = [UILabel new];
                [view addSubview:NOSearchLabel];
                NOSearchLabel.textAlignment = NSTextAlignmentCenter;
                NOSearchLabel.text = @"暂无直播搜索结果";
                NOSearchLabel.textColor = UIColorFromRGB(0x5a5a5a);
                [NOSearchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(view.mas_centerX);
                    make.centerY.equalTo(view.mas_centerY);
                    make.width.equalTo(view.mas_width);
                    make.height.equalTo(@(30*kRateSize));
                }];
                
                
                UIImageView *search = [[UIImageView alloc]init];
                [view addSubview:search];
                search.image = [UIImage imageNamed:@"ic_search_result_empty"];
                [search mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(NOSearchLabel.mas_centerX);
                    make.bottom.equalTo(NOSearchLabel.mas_top);
                    make.width.equalTo(@(60*kRateSize));
                    make.height.equalTo(@(60*kRateSize));
                }];
                
                EPG_TableView.tableFooterView = view;
            }
            
            [EPG_TableView reloadData];
            
        }
    }
    
    
    //header跟随tabelView动
    if (scrollView == EPG_TableView)
    {
        //YOUR_HEIGHT 为最高的那个headerView的高度
        CGFloat sectionHeaderHeight = 40*kRateSize;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}






#pragma mark - tableView dataSourse 数据源
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    sss++;
    if (StateResualtHave) {
        if (_currenIndex== 0) {
            //            NSLog(@"计算组花的时间");
            if (EPGAllDateList.count) {
                return EPGAllDateList.count;
            }
            return 1;
        }
    }else if (StateHotSeatch){//公版改进
        return self.HistoryHotArray.count;
    }
    return 1;
}

- (NSInteger)coutofHotandHistory{
    NSArray *hotArray = [self.HistoryHotDiction objectForKey:@"Hot"];
    NSArray *historyArray = [self.HistoryHotDiction objectForKey:@"History"];
    if (hotArray.count && historyArray.count) {
        return 2;
    }else if (!hotArray.count && !historyArray.count){
        return 0;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (StateResualtNO) {
//        return self.HHAArray.count;
//        
//    }
    if (StateResualtHave) {
        //        return 6;//test   925
        NSArray *Array1 ;
        
        if (_currenIndex == 1) {
            Array1 = [VODEPG_HaveDictionary objectForKey:@"VOD"];
            
            {
                
            }
            
            
            
            return  [Tool numberOfCellRowsWithArray:Array1 withRowNum:2];;
        }else
        {
            if (EPGAllDateList.count > section) {
                NSDictionary *dic = [EPGAllDateList objectAtIndex:section];
                NSArray * serviceList = [dic objectForKey:@"serviceList"];
                return serviceList.count;
            }else
            {
                return 0;
            }
            
            
            
        }
    }
    if(StateHotSeatch){//公版改进
//        return [[self.HistoryHotArray objectAtIndex:section] count];
        
        if ([[self.HistoryHotArray objectAtIndex:section] count]<=0) {
            return 0;
        }
        
        if ([[self.HistoryHotArray objectAtIndex:section] count]>10) {
            return 5;
        }
        return ([[self.HistoryHotArray objectAtIndex:section] count] + 1)/2;
    }
    
    if(StateAssociate){
        return self.HHAArray.count;
    }
    
    return 0;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (StateHotSeatch) {
        if ((indexPath.section == 1) || self.HistoryHotArray.count == 1) {//公版改进------热搜词
            
            static NSString *NewHotSearchIdentifier = @"NewHotSearchIdentifier";
            NewHotSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NewHotSearchIdentifier];
            if (!cell) {
                cell = [[NewHotSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NewHotSearchIdentifier];
            }
            
            cell.dateArray.array = [self arrayFOR_ALLArray:[self.HistoryHotArray objectAtIndex:indexPath.section] withBeginnumb:indexPath.row];
            
            [cell update];
 
            cell.block = ^(UIButton_Block*btn)
            {
                
                NSLog(@"%@",btn.Diction);
                
                if (StateResualtHave) {
                    return;
                }else
                {
                    [self show];
                }
                [self show];
                
                [VODEPG_Search_ScrollView setContentOffset:CGPointMake(0, 0)];

                if (StateHistory) {
                    self.HHATableView.hidden = YES;
                    //搜索    1  会出现一些直播的数据 ，点播没有，   2  搜超能陆战队会出现直播点播都有， 3搜 念念会出现点播数据，没有直播
                }
                if (StateResualtNO) {
                    
                }
                //        键盘收起来
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                
//                [UIView animateWithDuration:0.2 animations:^{
//                    searchView.frame = CGRectMake(8*kRateSize, searchView.frame.origin.y, 24, 24);
//                }];
                
                if (indexPath.row < self.HHAArray.count) {
                    NSDictionary *dic = btn.Diction;
                    NSLog(@"%@",dic);
                    NSString *searchText;
                    if ((!StateHotSeatch) && (indexPath.section ==0)) {
                        searchText  = [dic objectForKey:@"groupName"];
                    }else
                    {
                        searchText  = [dic objectForKey:@"searchName"];
                    }
                    
                    if (StateResualtNO ) {
                        searchText  = [dic objectForKey:@"searchName"];
                    }
                    if (StateAssociate) {
                        searchText  = [[dic objectForKey:@"properties"]objectForKey:@"name"];
                    }
                    
                    NSLog(@"搜索关键字%@",searchText);
                    
                    _seachTextView.text = searchText;
                    NSLog(@"搜索关键字%@",searchText);
                }
                
                if (StateHotSeatch) {
                    NSDictionary *dic = btn.Diction;
                    if (indexPath.section == 0 && [self historyhave]) {
                        _seachTextView.text  = [dic objectForKey:@"searchName"];
                    }else
                    {
                        _seachTextView.text  = [dic objectForKey:@"groupName"];
                    }
                }
                
                HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                
                if (_IS_SUPPORT_VOD) {
                    //点播的搜索
                    [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
                }
                
                
                //采集  VOD搜索  关键字  搜索时刻
                
                NSString *chanelContentSearchKeyAndTime = [NSString stringWithFormat:@"%@+%@+%@+%@:00",@"",@"",_seachTextView.text,[SAIInformationManager serviceTimeTransformStr]];
                
                [SAIInformationManager VODSearchHotKeyandSearchTime:chanelContentSearchKeyAndTime];
                
                
                
                //over
                
                //直播的搜索
                NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
                NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
                
                [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
                
                if ([self addORNotAddSearchRecord:_seachTextView.text]) {
                    [self addSearchRecordWithtitle:_seachTextView.text fresh:NO];
                }


                
            };
            
            return cell;   
            
        }else{//公版改进
            
            static NSString *NewHistoryIdentifier = @"NewHistoryIdentifier";
            NewHistorySearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NewHistoryIdentifier];
            if (!cell) {
                cell = [[NewHistorySearchTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NewHistoryIdentifier];
            }
            
            cell.dateArray.array = [self arrayFOR_ALLArray:[self.HistoryHotArray objectAtIndex:indexPath.section] withBeginnumb:indexPath.row];
            
            [cell update];
            
            cell.block = ^(UIButton_Block*btn)
            {
                
                if (StateResualtHave) {
                    return;
                }else
                {
                    [self show];
                }
                [self show];
                
                [VODEPG_Search_ScrollView setContentOffset:CGPointMake(0, 0)];

                if (StateHistory) {
                    self.HHATableView.hidden = YES;
                    //搜索    1  会出现一些直播的数据 ，点播没有，   2  搜超能陆战队会出现直播点播都有， 3搜 念念会出现点播数据，没有直播
                }
                if (StateResualtNO) {
                    
                }
                //        键盘收起来
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                
//                [UIView animateWithDuration:0.2 animations:^{
//                    searchView.frame = CGRectMake(8*kRateSize, searchView.frame.origin.y, 24, 24);
//                }];
                
                if (indexPath.row < self.HHAArray.count) {
                    NSDictionary *dic = btn.Diction;
                    NSLog(@"%@",dic);
                    NSString *searchText;
                    if ((!StateHotSeatch) && (indexPath.section ==0)) {
                        searchText  = [dic objectForKey:@"name"];
                    }else
                    {
                        searchText  = [dic objectForKey:@"searchName"];
                    }
                    
                    if (StateResualtNO ) {
                        searchText  = [dic objectForKey:@"searchName"];
                    }
                    if (StateAssociate) {
                        searchText  = [[dic objectForKey:@"properties"]objectForKey:@"name"];
                    }
                    
                    NSLog(@"搜索关键字%@",searchText);
                    
                    _seachTextView.text = searchText;
                    NSLog(@"搜索关键字%@",searchText);
                }
                
                if (StateHotSeatch) {
                    NSDictionary *dic = btn.Diction;
                    if (indexPath.section == 0 && [self historyhave]) {
                        _seachTextView.text  = [dic objectForKey:@"searchName"];
                    }else
                    {
                        _seachTextView.text  = [dic objectForKey:@"name"];
                    }
                }
                
                HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                
                if (_IS_SUPPORT_VOD) {
                    //点播的搜索
                    [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
                }
                
                
                //采集  VOD搜索  关键字  搜索时刻
                
                NSString *chanelContentSearchKeyAndTime = [NSString stringWithFormat:@"%@+%@+%@+%@:00",@"",@"",_seachTextView.text,[SAIInformationManager serviceTimeTransformStr]];
                
                [SAIInformationManager VODSearchHotKeyandSearchTime:chanelContentSearchKeyAndTime];
                
                
                
                //over
                
                //直播的搜索
                NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
                NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
                
                [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
                
                if ([self addORNotAddSearchRecord:_seachTextView.text]) {
                    [self addSearchRecordWithtitle:_seachTextView.text fresh:NO];
                }

                
                
            };

            return cell;
        }
        
    }
//    else if (StateHistory)
//    {
//        static NSString *HistoryIdentifier = @"HistoryIdentifier";
//        SearchHistoryTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:HistoryIdentifier];
//        if (!cell) {
//            cell = [[SearchHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:HistoryIdentifier];
//        }
//        NSDictionary *valueDIC = [self.HHAArray objectAtIndex:indexPath.row];
//        [cell updataWithTitle:[valueDIC objectForKey:@"searchName"]];
//        cell.BLOCK = ^(UIButton_Block*btn)
//        {
//            //删除操作
//            
//            [self deleteSearchRecordWithtitle:btn.eventName];
//            NSLog(@"%@",btn.eventName);
//            
//        };
//        
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }else if (StateResualtNO)
//    {
//        
//        static NSString *HistoryIdentifier = @"HistoryIdentifier";
//        SearchHistoryTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:HistoryIdentifier];
//        if (!cell) {
//            cell = [[SearchHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:HistoryIdentifier];
//        }
//        NSDictionary *valueDIC = [self.HHAArray objectAtIndex:indexPath.row];
//        
//        
//        [cell updataWithTitle:[valueDIC objectForKey:@"searchName"]];
//        
//        cell.BLOCK = ^(UIButton_Block*btn)
//        {
//            //删除操作
//            
//            [self deleteSearchRecordWithtitle:btn.eventName];
//            NSLog(@"%@",btn.eventName);
//            
//        };
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//        
//        
//    }
    else if (StateResualtHave)
    {
        if (StateResualtHave && _currenIndex ==1) {
            
            //        NSLog(@"第三层  点播分类、电影、综艺、电视剧");
            static NSString *Progranidentifier = @"Progranidentifier";
            VODShowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
            if (!cell) {
                cell = [[VODShowTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
                cell.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            
            cell.dateArray.array = [Tool arrayFOR_ALLArray:_VODArray withBeginnumb:indexPath.row];
            [cell update];
            tableView.delaysContentTouches = YES;
            cell.block = ^(UIButton_Block*btn)
            {
                NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
                
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.TRACKNAME = @"_";
                demandVC.TRACKID = @"_";
                demandVC.EN = @"5";
                demandVC.contentID = btn.serviceIDName;
                demandVC.contentTitleName = btn.eventName;
                
                
                [self.navigationController pushViewController:demandVC animated:YES];
            };
            UIColor *color = UIColorFromRGB(0xf7f7f7);
            cell.contentView.backgroundColor = color;
            cell.backgroundColor = color;
            
            UIView *view = [UIView new];
            view.backgroundColor = UIColorFromRGB(0xa4a4a4);
            VOD_TableView.tableFooterView = view;
            VOD_TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return cell;
        }else
        {//ww...EPG
            static NSString *TVEPGCELL = @"TVEPGCELL";
            TV_EPGTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TVEPGCELL];
            if (!cell) {
                cell = [[TV_EPGTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TVEPGCELL];
            }
            NSMutableDictionary *dic =  [EPGAllDateList objectAtIndex:indexPath.section];
            
            
            NSLog(@"搜索有预约按钮数据源：%@",EPGAllDateList);
            NSArray *array = [dic objectForKey:@"serviceList"];
            NSDictionary *Day_Chanel = [array objectAtIndex:indexPath.row];
            
            cell.diction.dictionary = Day_Chanel;
            
            [cell setUpView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            cell.CLICK = ^(UIButton_Block*btn)
            {
                NSDictionary *dic = btn.Diction;
                NSLog(@"字典啊  %@",dic);
                if (btn.isYuYue) {
                    
                    if (IsLogin) {
                        NSString *yuyueid = [btn.Diction objectForKey:@"eventId"];
                        if (btn.selected) {
                            
                            if (yuyueid) {
                                HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                                [_request DelEventReserveWithEventID:yuyueid andTimeout:10];
                                
                                _delScheduleEventID = yuyueid;
                            }
                        }else
                        {
                            if (yuyueid) {
                                HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                                [_request AddEventReserveWithEventID:yuyueid andTimeout:10];
                                
                                _addScheduleItemDic = btn.Diction;
                            }
                        }
                    }else
                    {
//                        [AlertLabel AlertInfoWithText:@"您还未登录，不能预约" andWith:self.view withLocationTag:10010];
                        if (isAuthCodeLogin) {

//                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
//                            user.loginType = kVideoLogin;
//                            [self.view addSubview:user.view];
//
//                            user.backBlock = ^()
//                            {
//
//                              [self changeState:&StateResualtHave];
//                                
//                            };
//                            
//                            user.tokenBlock = ^(NSString *token)
//                            {
//
//                                 [self changeState:&StateResualtHave];
//                                
//                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];
////
//    
//                            };
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kNormalLogin;
                            user.dismissBlock = ^(){
                                


                            };
                            
                            user.isLawDocBlock = ^(BOOL isDoc){
                                
                                [self show];
                                HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                                //点播的搜索
                                [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
                                
                                NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
                                NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
                                //直播的搜索
                                [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
//                                [EPG_TableView reloadData];
//                                [VOD_TableView reloadData];

                            };
                            
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:user];
                            [self presentViewController:nav animated:YES completion:nil];

                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:10010];
                        }
                    }
                }else
                {//ww...点击跳进直播播放器
                    //这里 界面跳转 并且传值
                    NSLog(@"  爱疯%@",btn.Diction);
                    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                    if (btn.isLive) {
                        live.currentPlayType = CurrentPlayLive;
                    }else
                    {
                        
                        live.currentPlayType = CurrentPlayEvent;
                    }
                    live.serviceID = btn.channelID;//电视台的id
                    live.serviceName = btn.serviceIDName;;//电视台的名字
                    live.eventURLID = [btn.Diction objectForKey:@"eventId"];;//电视台的id
                    live.eventName = [btn.Diction objectForKey:@"eventName"];;//电视台的id
                    live.IsFirstChannelist = YES;
                    live.TRACKID = @"_";
                    live.TRACKNAME = @"_";
                    live.EN = @"5";
                    NSDictionary *dic = btn.Diction;
                    NSString *endtimetime = [dic objectForKey:@"endTime"];
                    live.currentDateStr = [Tool MMddssToyyMMddStringFromDate: endtimetime andFormaterString:@"YYYY-MM-dd"];
                    [self.navigationController pushViewController:live animated:YES];
                    
                }
            };
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
            view.backgroundColor = UIColorFromRGB(0xa4a4a4);
            EPG_TableView.tableFooterView = view;
            return cell;
        }
    }
    else
    {
        
        static NSString *Hotidentifier = @"LianxiangIdentifier";
        NewKeyWordSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Hotidentifier];
        if (!cell) {
            cell = [[NewKeyWordSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Hotidentifier];
        }
        
        self.HHATableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.HHATableView.tableFooterView = [UIView new];
        
        NSMutableDictionary *valueDIC = [NSMutableDictionary dictionary];
        
        valueDIC = [self.HHAArray objectAtIndex:indexPath.row];
        
        cell.cellDic = valueDIC;

        [cell update];
        
        return cell;

    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (StateResualtHave) {
        if (_currenIndex == 0) {
            
            
            NSArray*  Array = [self sortArr:EPGAllKeyArray];
            NSString *title = @"";
            if (Array.count > section){
                title = [Array objectAtIndex:section
                         ];
            }
            return title;
        }
    }
    return @"";
    
}

#pragma mark - tableView Delegate 代理方法
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    return [UIView new];
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==32) {
        if (buttonIndex==1) {
            
            return;
        }else
        {
            [self deleteAllSearchText];
            [self.HHATableView reloadData];
        }
    }
    
    
    if (alertView.tag==33) {
        if (buttonIndex==1) {
            
            return;
        }else
        {
            [self deleteAllSearchText];
            [self.HHATableView reloadData];
        }
    }
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (StateHotSeatch) {
        return  [self setupHeaderWithSection:section];
    }
    UIFont *font = [UIFont systemFontOfSize:15*kRateSize];
    
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 35*kRateSize)];
    header.backgroundColor = [UIColor clearColor];
    UIView *contentView = [UIView new];
    [header addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(header.mas_left);
        make.top.equalTo(header.mas_top).offset(section==0? (0*kRateSize) : (5*kRateSize));
        make.bottom.equalTo(header.mas_bottom).offset(0*kRateSize-1);
        make.right.equalTo(header.mas_right);
    }];
    contentView.backgroundColor = App_white_color;
    
    
    NSString *title = @"";
    if (StateResualtHave) {
        if (_currenIndex == 0) {
            NSArray*  Array = [self sortArr:EPGAllKeyArray];
            
            if (Array.count > section){
                title = [Array objectAtIndex:section];
            }
        }
    }
    
    UILabel *weekLable = [UILabel new];
    [contentView addSubview:weekLable];
    [weekLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(10*kRateSize);
        make.top.equalTo(contentView.mas_top).offset(0*kRateSize);
        make.bottom.equalTo(contentView.mas_bottom).offset(-0*kRateSize);
        make.width.equalTo(@(60*kRateSize));
    }];
    weekLable.text = [Tool weekDayStringFromDate:title];
    
    UILabel *MMddLable = [UILabel new];
    [contentView addSubview:MMddLable];
    [MMddLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weekLable.mas_right).offset(8*kRateSize);
        make.top.equalTo(contentView.mas_top).offset(0*kRateSize);
        make.bottom.equalTo(contentView.mas_bottom).offset(-0*kRateSize);
        make.width.equalTo(@(180*kRateSize));
    }];
    MMddLable.text = [Tool MMddStringFromDate:title];
    weekLable.font = font;
    MMddLable.font = font;
    UIColor *textColor = UIColorFromRGB(0x666666);
    weekLable.textColor = textColor;
    MMddLable.textColor = textColor;
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (StateResualtHave&&_currenIndex==0 &&EPGAllDateList.count) {
        return 44.5*kDeviceRate;
    }
    if(StateHotSeatch){
        if ([[self.HistoryHotArray objectAtIndex:section] count]) {
            return 44.5*kDeviceRate;
        }else{
            return 0;
        }
        
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (StateResualtHave) {//有搜索结果
        if (_currenIndex == 1) {//点播搜索结果
            return 80*kRateSize;
            
        }else{//直播搜索结果

            NSDictionary *dic         = [EPGAllDateList objectAtIndex:indexPath.section];
            NSArray *array            = [dic objectForKey:@"serviceList"];
            NSDictionary *dicccc      = [array objectAtIndex:indexPath.row];
            NSDictionary *epginfoinfo = [dicccc objectForKey:@"epgList"];
            NSArray *jiemuArray       = [epginfoinfo objectForKey:@"epgList"];
            CGFloat highForRow        = (jiemuArray.count)*(55*kRateSize+1*kRateSize); //+ 10*kRateSize;
            return highForRow;
            
        }
    }else if(StateAssociate)
    {
        return 44*kDeviceRate;
        
    }else
    {
        return 30*kDeviceRate;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (StateResualtHave) {
        return;
    }else
    {
        [self show];
    }
    [self show];
    
    [VODEPG_Search_ScrollView setContentOffset:CGPointMake(0, 0)];
    

    if (StateHistory) {
        self.HHATableView.hidden = YES;
        //搜索    1  会出现一些直播的数据 ，点播没有，   2  搜超能陆战队会出现直播点播都有， 3搜 念念会出现点播数据，没有直播
    }
    if (StateResualtNO) {
        
    }
    //        键盘收起来
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];

//    [UIView animateWithDuration:0.2 animations:^{
//        searchView.frame = CGRectMake(8*kRateSize, searchView.frame.origin.y, 24, 24);
//    }];
    
    if (indexPath.row < self.HHAArray.count) {
        NSDictionary *dic = self.HHAArray[indexPath.row];
        NSLog(@"%@",dic);
        NSString *searchText;
        if ((!StateHotSeatch) && (indexPath.section ==0)) {
            searchText  = [dic objectForKey:@"name"];
        }else
        {
            searchText  = [dic objectForKey:@"searchName"];
        }
        
        if (StateResualtNO ) {
            searchText  = [dic objectForKey:@"searchName"];
        }
        if (StateAssociate) {
            if ([dic objectForKey:@"properties"]) {
                
                searchText  = [[dic objectForKey:@"properties"]objectForKey:@"name"];
                
            }else
            {
                searchText  = [dic objectForKey:@"name"];
            }

        }
        
        NSLog(@"搜索关键字%@",searchText);
        
        _seachTextView.text = searchText;
        NSLog(@"搜索关键字%@",searchText);
    }
    
    if (StateHotSeatch) {
        NSDictionary *dic = [self.HistoryHotArray[indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.section == 0 && [self historyhave]) {
            _seachTextView.text  = [dic objectForKey:@"searchName"];
        }else
        {
            _seachTextView.text  = [dic objectForKey:@"name"];
        }
    }
    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
    if (_IS_SUPPORT_VOD) {
        //点播的搜索
        [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
    }
    
    
    //采集  VOD搜索  关键字  搜索时刻
    
    NSString *chanelContentSearchKeyAndTime = [NSString stringWithFormat:@"%@+%@+%@+%@:00",@"",@"",_seachTextView.text,[SAIInformationManager serviceTimeTransformStr]];
    
    [SAIInformationManager VODSearchHotKeyandSearchTime:chanelContentSearchKeyAndTime];
    
    
    
    //over
    
    //直播的搜索
    NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
    NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
    
    [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
    
    if ([self addORNotAddSearchRecord:_seachTextView.text]) {
        [self addSearchRecordWithtitle:_seachTextView.text fresh:NO];
    }
}


#pragma mark-工具块
/**
 *  显示loading框
 */
- (void)show{
    [self showCustomeHUDAndHiddenAfterDelay:3];
    NOResault.hidden = YES;
}
/**
 *  隐藏loading框
 */
- (void)hide{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if (StateResualtNO) {
        _cancle_searchBtn.titleLabel.text = @"取消";
        
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
        self.HHATableView.hidden=  YES;;
        NOResault.hidden = NO;
        
    }else{
        NOResault.hidden = YES;
    }
}

/**
 *  取到直播搜索记录的对应的字典数据
 *
 *  @param indexx 第几段，即不同的日期
 *
 *  @return 该日期下的节目信息
 */
- (NSMutableDictionary*)dicWithSection:(NSInteger)indexx
{
    NSMutableDictionary *anser = [NSMutableDictionary dictionary];
    if (EPGAllKeyArray.count > indexx) {
        NSString *key = [EPGAllKeyArray objectAtIndex:indexx];
        anser.dictionary = [EPGDiction objectForKey:key];
    }
    return anser;
}
/**
 *
 */
- (NSArray*)diction:(NSDictionary*)dic andRow:(NSInteger)rows
{

    NSArray *allkeys = [dic allKeys];
    NSArray *anser;
    NSMutableArray *ALL = [NSMutableArray array];
    for (NSString *key in allkeys) {
        NSArray *epg = [dic objectForKey:key];
        for (NSArray *jijii in epg) {
            [ALL addObject:jijii];
        }
    }
    if ( rows < ALL.count) {
        anser = ALL[rows];
    }
    return anser;
}

//#warning ww..  这个方法没用貌似
- (NSDictionary*)EpgInfoWithSection:(NSInteger)section andRows:(NSInteger)rows
{

    NSMutableDictionary *anser = [NSMutableDictionary dictionary];
    
    return anser;
}
/**
 *  排列数据按照日期先后进行排序
 *
 *  @param arr 原始数组
 *
 *  @return 排序后数组
 */
- (NSArray *)sortArr:(NSArray *)arr
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}

-(NSString *)getFormOrAfterDateWithDays:(NSInteger )days{
    
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *mydateformat = [[NSDateFormatter alloc] init];
    [mydateformat setDateFormat:@"yyyy-MM-dd"];
    
    
    NSDate *mydate = [NSDate date];
    NSString *mydateStr= [mydateformat stringFromDate:mydate];
    NSArray *arr = [mydateStr componentsSeparatedByString:@"-"];
    
    NSString *lastStr = [NSString stringWithFormat:@"%ld",[[arr lastObject] integerValue]+days];
    
    if ([lastStr integerValue]>30) {
        lastStr = @"30";
    }
    if ([lastStr integerValue]<1) {
        lastStr = @"1";
    }
    
    return [NSString stringWithFormat:@"%@-%@-%@",[arr objectAtIndex:0],[arr objectAtIndex:1],lastStr];
}

//TOOL
//改变搜搜的状态
- (void)changeState:(BOOL*)State
{
    StateHistory = NO;
    StateAssociate = NO;
    StateHotSeatch = NO;
    StateResualtNO = NO;
    StateResualtHave = NO;
    
    *State = YES;
    
    
    if (StateResualtHave) {
        self.HHATableView.hidden = YES;
        resaultBase.hidden = NO;
        
    }else
    {
        self.HHATableView.hidden=  NO;;
        resaultBase.hidden = YES;
    }
    NOResault.hidden = YES;
    if (StateResualtNO) {
        _cancle_searchBtn.titleLabel.text = @"取消";
        
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
        
        self.HHATableView.hidden=  YES;;
        
    }else{
    }
    
    if (StateHistory) {
        NSLog(@"历史状态");
    }else if (StateAssociate){
        NSLog(@"联想状态");
    }else if (StateHotSeatch){
        NSLog(@"热搜状态");
    }
    else if (StateResualtNO){
        NSLog(@"无搜索结果状态");
    }
    else if (StateResualtHave){
        NSLog(@"有搜索结果状态");
    }
}

#pragma mark - TextField Delegate//
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    {  //切换模式
        
        HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
        if (_IS_SUPPORT_VOD) {
            
            if (textField.text.length) {
                [_request PubCommonGetResultByKeywordWithPageNo:@"1" withPageSize:@"10" withKey:textField.text];
                
//                if (StateResualtNO) {
                [self changeState:&StateAssociate];
//                }
            }else
            {
                [_request PubCommonVodRankByWeekWithWeekly:@""];
            }
            //点播搜索联想词
//            [_request VODTopSearchKeyWithKey:nil andCount:10 andTimeout:10];
            
        }else
        {
            //直播搜索联想词
//            [_request TopSearchKeyWithKey:nil andCount:10 andTimeout:10];
            [_request PubCommonVodRankByWeekWithWeekly:@""];
        }
    }
    NSLog(@"开始输入搜索关键词");
    
    if (defaultSearchStr.length) {
        textField.placeholder = defaultSearchStr;
    }else
    {
        textField.placeholder = @" 请输入您想要查找的关键字";
    }

//    [UIView animateWithDuration:0.2 animations:^{
//        searchView.frame = CGRectMake(8*kRateSize, searchView.frame.origin.y, 24, 24);
//    }];
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField clearButtonRectForBounds:CGRectMake((textField.size.width-45)*kRateSize, 0, 40*kRateSize,40*kRateSize)];
    if ([textField respondsToSelector:@selector(clearButtonRectForBounds:)]) {
        NSLog(@"改变了大小了哦");
    }
    
    if (_seachTextView.text.length) {
//        _cancle_searchBtn.titleLabel.text = @"搜索";
//        [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search"] forState:UIControlStateNormal];
//        [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search_pressed"] forState:UIControlStateHighlighted];
        
        
    }else
    {
        _cancle_searchBtn.titleLabel.text = @"取消";
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if([[_seachTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0){
    
        if (defaultSearchStr.length) {
            
            _seachTextView.text = defaultSearchStr;
            
        }else
        {
           
            [AlertLabel AlertInfoWithText:@" 请输入搜索视频名称" andWith:self.view withLocationTag:10000 ];
            return YES;
            
        }

    }
    
    if ([self addORNotAddSearchRecord:_seachTextView.text]) {
        [self addSearchRecordWithtitle:_seachTextView.text fresh:YES];
    }
        
    [self show];
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    //点播的搜索
    [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
        
    NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
    NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
    //直播的搜索
    [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
        
    //采集  VOD搜索  关键字  搜索时刻
        
    NSString *chanelContentSearchKeyAndTime = [NSString stringWithFormat:@"%@+%@+%@+%@:00",@"",@"",_seachTextView.text,[SAIInformationManager serviceTimeTransformStr]];
        
    [SAIInformationManager VODSearchHotKeyandSearchTime:chanelContentSearchKeyAndTime];

    
    return YES;
}
/*每输入一个字就搜索一次*/
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    return YES;
    
}
- (void)clickClearBtn:(NSNotification*)notifi
{
    NSLog(@"点击了清空按钮");
    UITextField *TF = notifi.object;
    
    NSString *keyWord = TF.text;
    
    NSLog(@"搜索关键字%@",keyWord);
    searchKey = keyWord;
    if (keyWord.length>=1) {
        {//如果有字输入，进入联想状态
//            _cancle_searchBtn.titleLabel.text = @"搜索";
//            [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search"] forState:UIControlStateNormal];
//            [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search_pressed"] forState:UIControlStateHighlighted];
            
            self.HHATableView.tableHeaderView = nil;
            self.HHATableView.tableFooterView = [UIView new];
            [self changeState:&StateAssociate];
            self.HHATableView.tableHeaderView = nil;
            self.HHATableView.separatorStyle =UITableViewCellSeparatorStyleNone;
            self.HHAArray = [NSMutableArray array];
            [self.HHATableView reloadData];
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            
            if (_IS_SUPPORT_VOD) {
                //点播搜索联想词
//                [request VODTopSearchKeyWithKey:keyWord andCount:10 andTimeout:10 ];
                
                [request PubCommonGetResultByKeywordWithPageNo:@"1" withPageSize:@"10" withKey:keyWord];
                
            }else{
                //直播搜索联想词
//                [request TopSearchKeyWithKey:keyWord andCount:10 andTimeout:10];
                [request PubCommonVodRankByWeekWithWeekly:@""];
            }
            
//            _cancle_searchBtn.titleLabel.text = @"搜索";
//            [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search"] forState:UIControlStateNormal];
//            [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search_pressed"] forState:UIControlStateHighlighted];
            
        }
    }else
    {
        _cancle_searchBtn.titleLabel.text = @"取消";
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
        
        
        {//如果无字输入，进入历史搜索状态
            [self changeState:&StateHotSeatch];//公版改进
            [self loadSearchRecord];
            
            [self.HHATableView reloadData];
            
        }
    }
    
    
    if (_seachTextView.text.length) {
//        _cancle_searchBtn.titleLabel.text = @"搜索";
//        [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search"] forState:UIControlStateNormal];
//        [_cancle_searchBtn setImage:[UIImage imageNamed:@"search_search_pressed"] forState:UIControlStateHighlighted];
        
        
    }else
    {
        _cancle_searchBtn.titleLabel.text = @"取消";
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
    }
}
#pragma mark -搜索记录的操作
- (void)historyList
{

    NSArray *array = [LocalCollectAndPlayRecorder SearchRecorder];
    
    [self.HHATableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.HHAArray.array = array;
}

#pragma mark - HT请求代理方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSDictionary *resultDIC = result;
    NSLog(@"%ld  %@  返回结果集----%@",(long)status,type,resultDIC);
    switch (status) {
        case 0:
        {
            //点播搜索热词关键词的话
            if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
                
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"keyList"];
                NSInteger numofList = [List count];
                
                //2.1搜索点播热搜词,if else 相对，else避免了一些问题
                if(numofList)
                {
                    //if 有点播搜索热词了，展示就可以了
                    
                    if (StateAssociate) {
                        self.HHAArray.array = List;
                        self.HHATableView.tableHeaderView = nil;
                        [self.HHATableView reloadData];
                        [self hide];
                    }else if (StateHotSeatch)
                    {
                        //公版改进
                        [self.HistoryHotArray removeAllObjects];
                        [self.HistoryHotDiction setObject:@"NO" forKey:@"History"];
                        [self.HistoryHotDiction setObject:@"NO" forKey:@"Hot"];
                        
                        NSMutableArray *array = [NSMutableArray array];
                        array.array= [self RECORED];
                        
                        [self.HistoryHotArray addObject:array];
                        
                        [self.HistoryHotArray addObject:List];
                        
                        if (array.count) {
                            [self.HistoryHotDiction setObject:@"YES" forKey:@"History"];
                        }else{
                            [self.HistoryHotDiction setObject:@"NO" forKey:@"History"];
                        }
                        if (List.count) {
                            [self.HistoryHotDiction setObject:@"YES" forKey:@"Hot"];
                        }else{
                            [self.HistoryHotDiction setObject:@"NO" forKey:@"Hot"];
                        }
                        
                        [self.HHATableView reloadData];
                        [self hide];
                    }
                    
                }else
                {
//                    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
//                    
//                    [self changeState:&StateHotSeatch];
//                    
//                    if (StateHotSeatch) {
//                        //else 没有搜索热词的花，直播搜索热词
////                        [_request TopSearchKeyWithKey:nil andCount:10 andTimeout:10];
//                        [_request PubCommonVodRankByWeekWithWeekly:@""];
//                    }else
//                    {
////                        [_request TopSearchKeyWithKey:searchKey andCount:10 andTimeout:10];
//                        [_request PubCommonVodRankByWeekWithWeekly:@""];
//                    }
                    
                    [self changeState:&StateHotSeatch];
                    
                    [self.HHATableView reloadData];
                }
            }
            
            
            //新点播热门搜索
            if ([type isEqualToString:PUB_COMMON_VODRANKBYWEEK]) {
                
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"dataList"];
                NSInteger numofList = [List count];
                
                //2.1搜索点播热搜词,if else 相对，else避免了一些问题
                if(numofList)
                {
                    
                    NSArray *useArr = [self dateSequenceWithsortArr:List andKeyName:@"nums"];
                    
                    defaultSearchStr = [[useArr objectAtIndex:0]objectForKey:@"groupName"];
                    
                    if (defaultSearchStr.length) {
                        _seachTextView.placeholder = defaultSearchStr;
                    }
                    
                    
                    //公版改进
                    [self.HistoryHotArray removeAllObjects];
                    [self.HistoryHotDiction setObject:@"NO" forKey:@"History"];
                    [self.HistoryHotDiction setObject:@"NO" forKey:@"Hot"];
                    
                    NSMutableArray *array = [NSMutableArray array];
                    array.array= [self RECORED];
                    
                    [self.HistoryHotArray addObject:array];
                    
                    [self.HistoryHotArray addObject:useArr];
                    
                    if (array.count) {
                        [self.HistoryHotDiction setObject:@"YES" forKey:@"History"];
                    }else{
                        [self.HistoryHotDiction setObject:@"NO" forKey:@"History"];
                    }
                    if (useArr.count) {
                        [self.HistoryHotDiction setObject:@"YES" forKey:@"Hot"];
                    }else{
                        [self.HistoryHotDiction setObject:@"NO" forKey:@"Hot"];
                    }
                    
                    [self.HHATableView reloadData];
                    [self hide];
                }
            }
            
            //新点播关键字搜索结果
            if ([type isEqualToString:PUB_COMMON_GETRESULTBYKEYWORD]) {
                
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"list"];
                NSInteger numofList = [List count];
                
                //2.1搜索点播热搜词,if else 相对，else避免了一些问题
                if(numofList)
                {
                    //if 有点播搜索热词了，展示就可以了
                    
                    NSArray *useArr = [self dateSequenceWithsortArr:List andKeyName:@"properties.nums"];
                    
                    if (StateAssociate) {
                        self.HHAArray.array = useArr;
                        self.HHATableView.tableHeaderView = nil;
                        [self.HHATableView reloadData];
                        [self hide];
                    }
        
                }else
                {
                    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
                    
                    
                    [self changeState:&StateHotSeatch];
                    
                    if (StateHotSeatch) {
                        //else 没有搜索热词的花，直播搜索热词
//                        [_request TopSearchKeyWithKey:nil andCount:10 andTimeout:10];
                        [_request PubCommonVodRankByWeekWithWeekly:@""];
                    }else
                    {
//                        [_request TopSearchKeyWithKey:searchKey andCount:10 andTimeout:10];
                        [_request PubCommonVodRankByWeekWithWeekly:@""];
                    }
                }
                
            }
            
            //点播搜索热词关键词的话
            if ([type isEqualToString:@"topSearchWords"]) {
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"keyList"];
                NSInteger numofList = [List count];
                
                //2.1搜索直播热搜词,
                if(numofList)
                {
                    //if 有直播搜索热词了，展示就可以了
                    if (StateHotSeatch) {
                        self.HHAArray.array = List;
                        
                        [self.HHATableView reloadData];
                        [self hide];
                    }else if (StateAssociate) {
                        self.HHAArray.array = List;
                        
                        [self.HHATableView reloadData];
                        [self hide];
                    }
                    
                }else
                {
                    //else 没有直播搜索热词的花不显示
                    self.HHATableView.separatorStyle =UITableViewCellSeparatorStyleNone;
                    
                    [self changeState:&StateHotSeatch];
                    
                    [self.HHATableView reloadData];
                }
                
            }
            
            
            //搜索到的VOD节目
            if([type isEqualToString:VOD_SEARCH])
            {
                [_seachTextView resignFirstResponder];
                NSLog(@"VOD搜索节目%@",resultDIC);
                _VODArray.array = [resultDIC objectForKey:@"vodList"];
                
                {//请求不到数据的处理
                    UILabel *view = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-44)];
                    view.text  =@"暂无点播搜索数据";
                    view.backgroundColor = [UIColor whiteColor];
                    view.textAlignment = NSTextAlignmentCenter;
                    VOD_TableView.tableFooterView = view;
                }
                
                if (_VODArray.count) {
                    if (_IS_SUPPORT_VOD) {
                        [VODEPG_HaveDictionary setObject:_VODArray forKey:@"VOD"];
                        [self changeState:&StateResualtHave];
                        [self hide];
                        _cancle_searchBtn.titleLabel.text = @"取消";
                        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
                        [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
                        
                        isNOSearchResault = NO;
                    }else
                    {
                        [_VODArray removeAllObjects];
                        if (!StateResualtHave) {
                            [self changeState:&StateResualtNO];
                            
                            {//让加载框小时掉
                                if (isNOSearchResault) {
                                    [self hide];
                                }
                                isNOSearchResault = YES;
                                
                            }
                            
                            [self historyList];
                            
                        }
                        
                    }
                    
                    
                }else{
                    [VODEPG_HaveDictionary setObject:[NSMutableArray array] forKey:@"VOD"];
                    if (!StateResualtHave) {
                        [self changeState:&StateResualtNO];
                        
                        {//让加载框小时掉
                            if (isNOSearchResault) {
                                [self hide];
                            }
                            isNOSearchResault = YES;
                            
                        }
                        
                        [self historyList];
                        
                    }
                }
                if (!_IS_SUPPORT_VOD) {
                    [VODEPG_HaveDictionary setObject:[NSMutableArray array] forKey:@"VOD"];
                    if (!StateResualtHave) {
                        [self changeState:&StateResualtNO];
                        
                        {//让加载框小时掉
                            if (isNOSearchResault) {
                                [self hide];
                            }
                            isNOSearchResault = YES;
                            
                        }
                        
                        [self historyList];
                        
                    }
                }
                if (StateResualtHave) {
                    
                    
                }else
                {
                    [self.HHATableView reloadData];
                }
                
                
                [EPG_TableView reloadData];
                [VOD_TableView reloadData];
                
            }
            
            
            //这个是新的搜索接口的回调方法日期都已经分类了
            if ([type isEqualToString:@"epgSearch"]) {
                NSLog(@"结果%@",result);
                [EPGAllDateList removeAllObjects];
                [self hide];
                {//请求不到数据的处理  设置Footer
                    UILabel *view = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-44)];
                    view.text  =@"暂无直播搜索数据";
                    view.backgroundColor = [UIColor whiteColor];
                    view.textAlignment = NSTextAlignmentCenter;
                    EPG_TableView.tableFooterView = view;
                }
                NSArray *array = [result objectForKey:@"dateList"];
                for (NSInteger i = array.count-1; i>=0; i--) {
                    NSDictionary *dic = [array objectAtIndex:i];
                    [EPGAllDateList addObject:dic];
                }
                
                if (EPGAllDateList.count) {
                    _cancle_searchBtn.titleLabel.text = @"取消";
                    [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateNormal];
                    [_cancle_searchBtn setImage:[UIImage imageNamed:@"btn_user_cancel_pressed"] forState:UIControlStateHighlighted];
                    
                }
                NSString * count = [result objectForKey:@"count"];
                [EPGAllKeyArray removeAllObjects];
                for (NSDictionary *dic in EPGAllDateList) {
                    NSString *string = [dic objectForKey:@"date"];
                    [EPGAllKeyArray addObject:string];
                    
                }
                
                if(!_IS_SUPPORT_VOD && !StateResualtHave && [count integerValue]>0)
                {
                    [self changeState:&StateResualtHave];
                }
                if (_IS_SUPPORT_VOD && [count integerValue]>0) {
                    [self changeState:&StateResualtHave];
                    [VODEPG_Search_ScrollView setContentOffset:CGPointMake(0, 0)];
                }else if(_IS_SUPPORT_VOD)
                {
                    [VODEPG_Search_ScrollView setContentOffset:CGPointMake(kDeviceWidth, 0)];
                }
                if (!_IS_SUPPORT_VOD&&[count integerValue] <= 0) {
                    [self changeState:&StateResualtNO];
                }
                [EPG_TableView reloadData];
                [VOD_TableView reloadData];
                
            }
            //点击cell中预约回看后的请求跳转
            
            //添加节目预约
            if ([type isEqualToString:EPG_ADDEVENTRESERVE]) {
                NSLog(@"预约结果%@",result);
                NSString *retString = [result objectForKey:@"ret"];
                NSInteger ret = [retString integerValue];
                if (ret==0) {
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                    [AlertLabel AlertInfoWithText:@"预约成功" andWith:self.view withLocationTag:10012];
                    
                    [_localListCenter addScheduleListWithDic:_addScheduleItemDic];
                    
                    [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
                        
                        [EPG_TableView reloadData];
                    }];
                    
                }else if(ret == -9 )
                {
                    [AlertLabel AlertInfoWithText:@"失败（连接个人中心失败或者参数错误）" andWith:self.view withLocationTag:10010];
                }else if(ret == 1 )
                {
                    [AlertLabel AlertInfoWithText:@"已经预约该节目" andWith:self.view withLocationTag:10011];
                }else if(ret == -2 )
                {
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:10012];
                }else if(ret == -1 )
                {
                    [AlertLabel AlertInfoWithText:@"节目不存在或者对应的频道不存在" andWith:self.view withLocationTag:10012];
                }else if(ret == -3 )
                {
                    [AlertLabel AlertInfoWithText:@"节目已经播出，不能预约" andWith:self.view withLocationTag:10012];
                }
            }
            
            
            //取消节目预约
            if ([type isEqualToString:EPG_DELEVENTRESERVE]) {
                NSLog(@"取消预约结果%@",result);
                NSString *retString = [result objectForKey:@"ret"];
                NSInteger ret = [retString integerValue];
                if (ret==0) {
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                    
                    [AlertLabel AlertInfoWithText:@"取消预约成功" andWith:self.view withLocationTag:10016];
                    
                    [_localListCenter singleDeletScheduledListWith:_delScheduleEventID andKey:@"eventID"];
                    
                    [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
                        
                        [EPG_TableView reloadData];
                    }];
                }else if(ret == -9 )
                {
                    [AlertLabel AlertInfoWithText:@"失败（连接个人中心失败或者参数错误）" andWith:self.view withLocationTag:10010];
                }else if(ret == 1 )
                {
                    [AlertLabel AlertInfoWithText:@"未预约该节目或者已经取消预约" andWith:self.view withLocationTag:10011];
                }else if(ret == -2 )
                {
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:10012];
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    if (status !=0) {
        //点播搜索热词关键词的话
        if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
            
            
            HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
            if (StateHotSeatch) {
                //else 没有搜索热词的花，直播搜索热词
//                [_request TopSearchKeyWithKey:nil andCount:10 andTimeout:10];
                [_request PubCommonVodRankByWeekWithWeekly:@""];
            }else
            {
//                [_request TopSearchKeyWithKey:searchKey andCount:10 andTimeout:10];
                [_request PubCommonVodRankByWeekWithWeekly:@""];
            }
            
        }
    }
}

//取消搜索按钮
- (void)cancel_search:(UIButton*)btn
{
    _cancle_searchBtn = btn;
//    if ([btn.titleLabel.text isEqualToString:@"搜索"]) {//[[btn currentTitle] isEqualToString:@"搜索"]
//        
//        [VODEPG_Search_ScrollView setContentOffset:CGPointMake(0, 0)];
//        if (_seachTextView.text.length == 0) {
//            [AlertLabel AlertInfoWithText:@" 请输入搜索视频名称" andWith:self.view withLocationTag:10000 ];
//            return;
//        }
//        
//        if([[_seachTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0){
//            
//            _seachTextView.text = defaultSearchStr;
//            
//        }
//        
//        if ([self addORNotAddSearchRecord:_seachTextView.text]) {
//            [self addSearchRecordWithtitle:_seachTextView.text fresh:YES];
//        }
//        
//        [_seachTextView canBecomeFirstResponder];
//        [self show];
//        HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
//        if (_IS_SUPPORT_VOD) {
//            //点播的搜索
//            [_request VODSearchWithOperationCode:nil andRootCategoryID:nil andCategoryID:nil andTopicCode:nil andKey:_seachTextView.text andStarSeq:1 andCount:10 andOrderType:2 andTimeout:10 ];
//        }
//        
//        
//        NSString *qiansantian = [HTRequest getFormOrAfterDateWithDays:-3];
//        NSString *housantian = [HTRequest getFormOrAfterDateWithDays:3];
//        //直播的搜索
//        [_request epgSearchOrderResultWith:_seachTextView.text andStartDate:qiansantian andEndDate:housantian andCount:@"100" andTimeout:10];
//        
//        //采集  VOD搜索  关键字  搜索时刻
//        
//        NSString *chanelContentSearchKeyAndTime = [NSString stringWithFormat:@"%@+%@+%@+%@:00",@"",@"",_seachTextView.text,[SAIInformationManager serviceTimeTransformStr]];
//        
//        [SAIInformationManager VODSearchHotKeyandSearchTime:chanelContentSearchKeyAndTime];
//        
//        
//        
//        //over
//        
//    }
    
    if ([btn.titleLabel.text isEqualToString:@"取消"]) {
        
        [[NSNotificationCenter defaultCenter]removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark -搜索记录方法
- (BOOL)historyhave{
    if ([[self.HistoryHotDiction objectForKey:@"History"] isEqualToString:@"YES"]) {
        return YES;
    }else{
        return NO;
    }
}

//增加搜素记录吗
- (BOOL)addORNotAddSearchRecord:(NSString*)searchtext
{
    
    if (!searchtext.length) {
        return NO;
    }
    NSArray *array = [LocalCollectAndPlayRecorder SearchRecorder];
    NSDictionary *dic = [NSDictionary dictionaryWithObject:searchtext forKey:@"searchName"];
    
    for (NSDictionary*dic1 in array) {
        if ([dic1 isEqualToDictionary:dic]) {
            return NO;
        }
    }
    return YES;
    
    
}
//增加搜索记录
- (void)addSearchRecordWithtitle:(NSString*)string fresh:(BOOL)fresh
{

    [LocalCollectAndPlayRecorder addSearchRecorderWithDic:[NSDictionary dictionaryWithObject:_seachTextView.text forKey:@"searchName"]];
    if (fresh) {
        //        [self loadSearchRecord];
    }
    
}
- (void)loadSearchRecord
{

    NSArray *array = [LocalCollectAndPlayRecorder SearchRecorder];
    
    if (array.count) {
        if (self.HistoryHotArray.count ==2) {
            [self.HistoryHotArray replaceObjectAtIndex:0 withObject:array];
            [self.HistoryHotDiction setObject:@"YES" forKey:@"History"];
            NSLog(@"%@   替换了  %@",self.self.HistoryHotArray,array);
        }else{
            [self.HistoryHotArray insertObject:array atIndex:0];
            [self.HistoryHotDiction setObject:@"YES" forKey:@"History"];
        }
        
    }else{
        
        if (self.HistoryHotArray.count == 2) {
            [self.HistoryHotArray removeObjectAtIndex:0];
            [self.HistoryHotDiction setObject:@"NO" forKey:@"History"];
        }
        
    }
    
    [self.HHATableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)deleteAllSearchText
{

    NSArray *array = [LocalCollectAndPlayRecorder SearchRecorder];
    
    for (NSDictionary *dic in array) {
        NSString *searchName = [dic objectForKey:@"searchName"];
        [LocalCollectAndPlayRecorder deleteSearchRecorderWithsearchName:searchName];
    }
    
    [self loadSearchRecord];
    
}

- (NSArray*)RECORED{

    NSArray *array = [LocalCollectAndPlayRecorder SearchRecorder];
    return array;
}

- (void)deleteSearchRecordWithtitle:(NSString*)string
{

    [LocalCollectAndPlayRecorder deleteSearchRecorderWithsearchName:string];
    
    [self loadSearchRecord];
}

#pragma mark - 设置搜索框
- (void)setSearchBar
{
    //设置搜索textField
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, 0, 295*kDeviceRate, 29*kDeviceRate)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = kSearch_bg_color;//[UIColor whiteColor];
//    view.layer.cornerRadius = size.height/2;
    view.layer.cornerRadius = 5*kDeviceRate;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UITextField alloc]initWithFrame:CGRectMake(32*kDeviceRate,6*kDeviceRate,260*kDeviceRate, 17*kDeviceRate)];
//    _seachTextView = [[UITextField alloc]init];
    _seachTextView.delegate = self;
    _seachTextView.returnKeyType = UIReturnKeySearch;
    
    NSString *text;
    if (kDeviceWidth == 414) {
        text = @" 请输入您想要查找的关键字";
    }else
    {
        text = @" 请输入您想要查找的关键字";
    }
    _seachTextView.placeholder = text;
    if (isAfterIOS8) {
        _seachTextView.preservesSuperviewLayoutMargins = YES;
    }
    _seachTextView.font = HT_FONT_THIRD;
    _seachTextView.textColor = HT_COLOR_ALERTCUSTOM;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    
    [view addSubview:_seachTextView];
    
//    [_seachTextView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(view.mas_left).offset(32*kDeviceRate);
//        make.top.equalTo(view.mas_top).offset(6*kDeviceRate);
//        make.bottom.equalTo(view.mas_bottom).offset(-6*kDeviceRate);
//        make.right.equalTo(view.mas_right).offset(5*kDeviceRate);
//    }];
    
//    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake(5*kDeviceRate,3*kDeviceRate, 17*kDeviceRate, 17*kDeviceRate)];
    UIImageView *searchview = [[UIImageView alloc]init];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    [searchview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(view.mas_top).offset(6*kDeviceRate);
        make.bottom.equalTo(view.mas_bottom).offset(-6*kDeviceRate);
        make.width.equalTo(@(17*kDeviceRate));
    }];
    
    [self setNaviBarSearchView:view];
//    [self.m_viewNaviBar addSubview:view];
    [self setNaviBarSearchViewFrame:CGRectMake(20*kDeviceRate, 27, 295*kDeviceRate, 29*kDeviceRate)];
    
}
#pragma mark - 设置Header 和 Footer
- (UIView*)setupHeaderWithSection:(NSInteger)section{
    if (StateHotSeatch) {
        
        UIView *head = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 44.5*kDeviceRate)];
        UIView *baseView = [[UIView alloc]init];
        UILabel *HeaderLable = [UILabel new];
        [head addSubview:baseView];
        [baseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(head.mas_left);
            make.top.equalTo(head.mas_top).offset(4*kDeviceRate);
            make.bottom.equalTo(head.mas_bottom).offset(-0.5*kDeviceRate);
            make.right.equalTo(head.mas_right);
        }];
        [baseView addSubview:HeaderLable];
        
        [HeaderLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(baseView.mas_left).offset(15*kDeviceRate);
            make.centerY.equalTo(baseView.mas_centerY);
            make.width.equalTo(@(200*kDeviceRate));
        }];
        
        baseView.backgroundColor = HT_COLOR_FONT_INVERSE;
        head.backgroundColor = HT_COLOR_SPLITLINE;
        HeaderLable.tag = HeaderTag + 2;
        HeaderLable.font = HT_FONT_THIRD;
        HeaderLable.textColor = HT_COLOR_FONT_ASSIST;
        
        if (section == 0 && self.HistoryHotArray.count == 2 ) {
            
            //更多按钮
            CGSize moreBtnSize = CGSizeMake(100, baseView.frame.size.height);
            UIButton_Block *moreBtn = [[UIButton_Block alloc] init];
            moreBtn.frame = CGRectMake(kDeviceWidth-80*kDeviceRate, 0, moreBtnSize.width, 30*kDeviceRate);
            [moreBtn setTitleColor:HT_COLOR_FONT_ASSIST forState:UIControlStateNormal];
            moreBtn.titleLabel.font = HT_FONT_THIRD;
            [baseView addSubview:moreBtn];
            
            [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(baseView.mas_right).offset(-12*kDeviceRate);
                make.top.equalTo(baseView.mas_top);
                make.bottom.equalTo(baseView.mas_bottom);
            }];
            
            [moreBtn setTitle:@"清空历史" forState:UIControlStateNormal];
            
            moreBtn.Click = ^(UIButton_Block *btn,NSString *hasChild){
                
                NSLog(@"点击了清空按钮");
                
                [_seachTextView resignFirstResponder];
                
                CustomAlertView *customAlert = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确定" withDelegate:self];
                
                customAlert.tag = 32;
                customAlert.messageStr = @"确定清空全部信息吗？";
                [customAlert show];
            };

            HeaderLable.tag = HeaderTag + 2;
            HeaderLable.text = @"搜索历史";
            return head;
        }else{

            HeaderLable.tag = HeaderTag + 1;
            HeaderLable.text = @" 热门搜索";
            return head;
            
        }
    }
    return [UIView new];
}

//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index{
    NSMutableArray *anser = [NSMutableArray array];
    
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];

    if (array.count > index*2) {
        
        tempDic = array[index*2];
        
        NSString *indexStr = [NSString stringWithFormat:@"%ld",index*2+1] ;
        
        [tempDic setObject:indexStr forKey:@"indexNum"];
        
        [anser addObject:tempDic];
    }
    if (array.count > index*2+1) {
        
        tempDic = array[index*2+1];
        
        NSString *indexStr = [NSString stringWithFormat:@"%ld",index*2+2] ;
        
        [tempDic setObject:indexStr forKey:@"indexNum"];
        
        [anser addObject:tempDic];

    }
    
    return anser;
}

#pragma  mark -改变了预约状态的后的回调
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    [_seachTextView resignFirstResponder];
    
}
#pragma mark- 状态栏按钮响应
//返回按钮操作
- (void)goBackPage
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma  mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 *  排列数据按照日期先后进行排序
 *
 *  @param arr 原始数组
 *
 *  @return 排序后数组
 */
- (NSArray *)dateSequenceWithsortArr:(NSArray *)arr andKeyName:(NSString *)keyName
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:keyName ascending:NO];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}


@end
