//
//  SearchHistoryTableViewCell.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/25.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "SearchHistoryTableViewCell.h"
#import "Masonry.h"

@interface SearchHistoryTableViewCell ()
{
    UIButton_Block *btn;//删除按钮
}
@end


@implementation SearchHistoryTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //        self.isRadius = NO;
        [self initView];
    }
    return self;
}
- (void)initView
{
    UIView *View =  self.contentView;
    btn = [UIButton_Block new];
    [View addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(View.mas_top).offset(5*kRateSize);
        make.right.equalTo(View.mas_right).offset(-5*kRateSize);
        make.bottom.equalTo(View.mas_bottom).offset(-10*kRateSize);
        make.width.equalTo(btn.mas_height);
    }];
    [btn setBackgroundImage:[UIImage imageNamed:@"sl_search_del_item_normal"] forState:UIControlStateNormal];
    
}
- (void)updataWithTitle:(NSString *)title
{
    
    WS(wSelf)
    self.textLabel.text = title;
    btn.eventName = title;
    btn.Click= ^(UIButton_Block*Btn,NSString*name){
        if (wSelf.BLOCK) {
            wSelf.BLOCK(Btn);
        }else
        {
            NSLog(@"没有实现搜索历史cell的block");
        }
    };
}


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
