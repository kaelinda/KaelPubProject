//
//  SearchHistoryTableViewCell.h
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/25.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButton_Block.h"
typedef void (^SearchHistoryBlock) (UIButton_Block*btn);
@interface SearchHistoryTableViewCell : UITableViewCell

//@property(nonatomic,copy)NSString *Diction;//传递给cell的字典

@property(nonatomic,copy)SearchHistoryBlock BLOCK;//传递给cell的字典

- (void)updataWithTitle:(NSString*)title;//更新状态吧
@end
