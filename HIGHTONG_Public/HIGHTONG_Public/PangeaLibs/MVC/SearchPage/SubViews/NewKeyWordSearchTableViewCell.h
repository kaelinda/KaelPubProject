//
//  NewKeyWordSearchTableViewCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewKeyWordSearchTableViewCell : UITableViewCell

@property(nonatomic,strong)UILabel *titleLb;//标题

@property(nonatomic,strong)UILabel *categoryLb;//分类

@property(nonatomic,strong)NSMutableDictionary *cellDic;

- (void)update;

@end
