//
//  CustomSearchBarView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CustomSearchBarViewBlock)();

@interface CustomSearchBarView : UIView

@property (nonatomic, strong)UILabel *placeholderLab;

@property (nonatomic, strong)UIImageView *searchLogoImg;

@property (nonatomic, strong)UIButton *senderBtn;

@property (nonatomic, copy)CustomSearchBarViewBlock searchBlock;

//- (id)initWithFrame:(CGRect)frame andPlaceStr:(NSString *)str andLogoImg:(UIImage *)img;

//- (id)initWithSize:(CGSize)size;
//
//- (id)initWithSize:(CGSize)size andCenter:(CGPoint)center;

@end
