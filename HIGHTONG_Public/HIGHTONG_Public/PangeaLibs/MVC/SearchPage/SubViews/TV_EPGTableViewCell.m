//
//  TV_EPGTableViewCell.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/21.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "TV_EPGTableViewCell.h"
#import "Tool.h"
#import "SearchSHOWView.h"

@interface TV_EPGTableViewCell ()
{
    UIImageView *TVLOGO;//台标
    UILabel *TVName ;//台名字
}
@end
#define  taggg 10909090
@implementation TV_EPGTableViewCell




- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        _array = [NSMutableArray array];
        _diction = [NSMutableDictionary dictionary];
    }
    return self;
}



- (void)setUpView
{
    WS(wss);
    UIView *view = self.contentView;
    [view removeAllSubviews];
    TVLOGO = [[UIImageView alloc]init];
    TVName = [[UILabel alloc]init];
    TVName.textAlignment = NSTextAlignmentCenter;
    TVLOGO.contentMode = UIViewContentModeScaleAspectFit;
    TVLOGO.image  = [UIImage imageNamed:@"bg_vod_gridview_item_onloading"];

    TVName.font = [UIFont systemFontOfSize:11*kRateSize];
    [view addSubview:TVLOGO];
     [view addSubview:TVName];
    [TVLOGO mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(30*kRateSize));
        make.height.equalTo(@(25*kRateSize));
        make.left.equalTo(wss.contentView.mas_left).offset(42.5*kRateSize);
        make.top.equalTo(wss.contentView.mas_top).offset(6*kRateSize);
    }];
    
    [TVName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(85*kRateSize));
        make.centerX.equalTo(TVLOGO.mas_centerX);
        make.top.equalTo(TVLOGO.mas_bottom).offset(4*kRateSize);
        make.height.equalTo(@(15*kRateSize));
    }];
    
    
        if ([self.diction count]) {
            NSString *imagelink = [self.diction objectForKey:@"imageLink"];
            NSString *serviceId = [self.diction objectForKey:@"serviceID"];
            NSString *channelName = [self.diction objectForKey:@"channelName"];
            [TVLOGO sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imagelink)]];
            TVName.text = channelName;
            NSDictionary * epgsssList = [self.diction objectForKey:@"epgList"];
            NSArray *epgList=[epgsssList objectForKey:@"epgList"];
            
            for (int i=0; i<epgList.count; i++) {
                NSDictionary *epgInfo = [epgList objectAtIndex:i];
                SearchSHOWView *searchSHOW = [[SearchSHOWView alloc]initWithFrame:CGRectMake(100*kRateSize, 56*i*kRateSize, kDeviceWidth-100*kRateSize, 56*kRateSize)];
                [view addSubview:searchSHOW];
                searchSHOW.tag = taggg + i;
                searchSHOW.serviceId = serviceId;
                searchSHOW.channelName = channelName;
                searchSHOW.epginfo  = epgInfo;
                [searchSHOW loadView];
                if ((i==0 && epgList.count>1)||(i>=1 && i < (epgList.count-1))) {//...ww 修改判断条件,修改line的frame,更换line的色值

                    UIView *line =[[UIView alloc]initWithFrame:CGRectMake(0, 55*kRateSize, searchSHOW.size.width, 1*kRateSize)];
                    [line setBackgroundColor:App_line_color];
                    [searchSHOW addSubview:line];
                }
                searchSHOW.SHOWCLICK = ^(UIButton_Block*btn)
                {
                    if (_CLICK) {
                        _CLICK(btn);
                    }else
                    {
                        NSLog(@"这个只有直播的直播展示dadadadadadaddaadcell 未实现电机时间");
                    }
                };
            }
        }
}

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}
@end
