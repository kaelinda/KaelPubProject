//
//  TV_EPGTableViewCell.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/21.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^SearchShoWBlock_TO_CELL) (UIButton_Block*btn);
@interface TV_EPGTableViewCell : UITableViewCell
@property(nonatomic,strong)NSMutableArray *array;//每个cell上的数据
@property(nonatomic,strong)NSMutableDictionary *diction;//每个cell上的数据

@property(nonatomic,copy)SearchShoWBlock_TO_CELL CLICK;




- (void)setUpView;

@end
