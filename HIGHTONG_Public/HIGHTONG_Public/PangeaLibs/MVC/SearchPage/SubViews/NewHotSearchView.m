//
//  NewHotSearchView.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/25.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "NewHotSearchView.h"

@implementation NewHotSearchView

- (instancetype)init
{
    if (self = [super init]) {
        
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
    
    self.imageBtn = [[UIButton alloc]init];
    self.imageBtn.titleLabel.font = HT_FONT_THIRD;
    
    [self.imageBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateDisabled];
    [superView addSubview:self.imageBtn];
    
    self.title = [UILabel new];
    [superView addSubview:self.title];
    
    [self.imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15*kDeviceRate);
        make.height.equalTo(@(17*kDeviceRate));
        make.width.equalTo(@(17*kDeviceRate));
        make.centerY.equalTo(superView.mas_centerY);
    }];
    
    self.imageBtn.clipsToBounds=YES;
    
    self.imageBtn.layer.cornerRadius=2*kDeviceRate;
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageBtn.mas_right).offset(5*kDeviceRate);;
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@(15*kDeviceRate));
        make.centerY.equalTo(superView.mas_centerY);
    }];

    self.title.font = HT_FONT_THIRD;
    self.title.textColor = HT_COLOR_ALERTCUSTOM;
    
    _Btn  = [UIButton_Block new];
    [self addSubview:_Btn];
    [_Btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    WS(wself);
    _Btn.Click = ^(UIButton_Block*btn,NSString * name)
    {
        if (wself.viewBlock!=nil) {
            NSLog(@"热门搜索cell知道点击了那个view");
            wself.viewBlock(btn);
        }
    };    
}
- (void)updata
{
    
    NSDictionary *dic = self.dic;
    
    _Btn.Diction = dic;
    
    if (dic) {
        
        self.hidden = NO;
        
        self.title.text = [self.dic objectForKey:@"groupName"];
        
        if ([[self.dic objectForKey:@"indexNum"]isEqualToString:@"1"]) {
            
            [_imageBtn setBackgroundColor:UIColorFromRGB(0xff9600)];
            
        }else if([[self.dic objectForKey:@"indexNum"]isEqualToString:@"2"]||[[self.dic objectForKey:@"indexNum"]isEqualToString:@"3"])
        {
            [_imageBtn setBackgroundColor:UIColorFromRGB(0xff9600)];
            
        }else
        {
            
            [_imageBtn setBackgroundColor:UIColorFromRGB(0x3ab7f5)];
            
        }
        
        [_imageBtn setTitle: [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"indexNum"]] forState:UIControlStateNormal];
        
    }else
    {
        
        self.hidden = YES;

    }

    
}

@end
