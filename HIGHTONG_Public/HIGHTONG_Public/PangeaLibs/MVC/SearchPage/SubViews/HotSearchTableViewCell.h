//
//  HotSearchTableViewCell.h
//  HIGHTONG
//
//  Created by testteam on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
@interface HotSearchTableViewCell : UITableViewCell

@property(nonatomic,strong)UIImage * image; //button 的背景图片，

@property(nonatomic,assign)BOOL isRadius;//是不是圆角//默认是圆角的 

@property(nonatomic,copy)NSString *title;//cell的标题

@property(nonatomic,strong)UILabel *label;

@property(nonatomic,strong)UIButton *btn;

@property(nonatomic,strong)UILabel *numLable;

@property(nonatomic,strong)UIButton_Block *deleteBtn;

@property(nonatomic,strong)UIButton_Block *goBtn;

@end
