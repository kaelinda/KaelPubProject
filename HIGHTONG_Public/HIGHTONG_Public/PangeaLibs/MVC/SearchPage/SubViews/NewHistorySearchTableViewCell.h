//
//  NewHistorySearchTableViewCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NewHistorySearchView.h"
#import "UIButton_Block.h"

typedef void (^NewHistorySearchTableViewCellBlock) (UIButton_Block*);

@interface NewHistorySearchTableViewCell : UITableViewCell

@property (nonatomic,copy)NewHistorySearchTableViewCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)NewHistorySearchView *leftView;//左边

@property (nonatomic,strong)NewHistorySearchView *rightView;//右边


- (void)update;//跟新数据

@end
