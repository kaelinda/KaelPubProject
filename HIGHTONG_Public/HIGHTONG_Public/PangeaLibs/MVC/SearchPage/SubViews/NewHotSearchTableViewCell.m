//
//  NewHotSearchTableViewCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/25.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "NewHotSearchTableViewCell.h"
#import "Masonry.h"

@implementation NewHotSearchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.dateArray = [NSMutableArray array];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    WS(wself);
    UIView *superView = self.contentView;
    self.leftView = [NewHotSearchView new];
    [superView addSubview:self.leftView];
    
    self.rightView = [NewHotSearchView new];
    [superView addSubview:self.rightView];
    
    
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.top.equalTo(superView.mas_top);
        make.right.equalTo(superView.mas_centerX);
        make.height.mas_equalTo(30*kDeviceRate);
        
    }];
    [self.leftView updata];
    
    self.leftView.viewBlock = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"zou视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(superView.mas_centerX);
        make.height.mas_equalTo(30*kDeviceRate);
    }];
    [self.rightView updata];
    self.rightView.viewBlock = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"you视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
}
- (void)update
{
    
    NSLog(@"这个破cell的各种值------%@",self.dateArray);
    
    
    if (self.dateArray.count >= 1) {
        NSDictionary *dic = self.dateArray[0];
        
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.leftView.dic = dic;
            self.rightView.dic = nil;
            
            [self.leftView updata];
            [self.rightView updata];
        }
    }
    if (self.dateArray.count >= 2) {
        NSDictionary *dic = self.dateArray[1];
        
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.rightView.dic = dic;
            
            [self.leftView updata];
            [self.rightView updata];
        }
    }
    
}


@end
