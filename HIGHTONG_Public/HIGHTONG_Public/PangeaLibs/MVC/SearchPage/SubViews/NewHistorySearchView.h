//
//  NewHistorySearchView.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^To_NewHistorySearch)(UIButton_Block*);

@interface NewHistorySearchView : UIView

@property(nonatomic,strong)To_NewHistorySearch viewBlock;

@property(nonatomic,strong)UIButton_Block *Btn;

@property(nonatomic,strong)UILabel *title;//标题

@property(nonatomic,strong)NSDictionary *dic;

- (void)updata;

@end
