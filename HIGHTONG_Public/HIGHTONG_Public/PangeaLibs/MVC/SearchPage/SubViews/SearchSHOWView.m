//
//  SearchSHOWView.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/21.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "SearchSHOWView.h"
#import "Tool.h"
#import "LocalCollectAndPlayRecorder.h"

@interface SearchSHOWView ()
{
    UILabel *Namelabel;
    UILabel *Timelabel;
    UIImageView *YUYUE;//预约按钮
    
}
@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;
@end

@implementation SearchSHOWView


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupMy];
        
        _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    }
    return self;
}

- (void)setupMy
{
    Namelabel = [[UILabel alloc]initWithFrame:CGRectMake(10*kRateSize, 11*kRateSize, 150*kRateSize, 15*kRateSize)];
    Timelabel = [[UILabel alloc]initWithFrame:CGRectMake(10*kRateSize, 38*kRateSize, 150*kRateSize, 13*kRateSize)];
    
    YUYUE= [[UIImageView alloc]init];
    _Btn = [[UIButton_Block alloc]initWithFrame:CGRectMake(0, 0, self.size.width,self.size.height)];
    
    UIFont *font = [UIFont systemFontOfSize:15*kRateSize];
    Namelabel.font = font;
    font = [UIFont systemFontOfSize:13*kRateSize];
    Timelabel.font = font;
    
    [self addSubview:Namelabel];
    [self addSubview:Timelabel];
    [self addSubview:_Btn];
    [self addSubview:YUYUE];
    
    WS(wss);
    
    [YUYUE mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(45*kRateSize));
        make.height.equalTo(@(24*kRateSize));
        make.left.equalTo(@(wss.size.width-60*kRateSize));
        make.centerY.equalTo(wss.mas_centerY);
    }];
}
-(void)loadView
{
   
    WS(wself);
    
    NSDictionary *dic = self.epginfo;
    NSString *eventId = [dic objectForKey:@"eventId"];
    
    NSString *startTime = [dic objectForKey:@"startTime"];
    NSString *endTime = [dic objectForKey:@"endTime"];
    
    NSString *startTime12 = [dic objectForKey:@"startTime"];
    NSString *endTime12 = [dic objectForKey:@"endTime"];
    
    NSString *eventName = [dic objectForKey:@"eventName"];
    
    startTime = [Tool MMddssToyyMMddStringFromDate:startTime andFormaterString:@"HH:mm"];
    endTime = [Tool MMddssToyyMMddStringFromDate:endTime andFormaterString:@"HH:mm"];
    NSString *time = [NSString stringWithFormat:@"%@-%@",startTime,endTime];

    
    Namelabel.text = eventName;
    Timelabel.text = time;
    
    _Btn.Diction = self.epginfo;
    _Btn.serviceIDName = self.channelName;
    _Btn.channelID = self.serviceId;
    _Btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        if (wself.SHOWCLICK) {
            wself.SHOWCLICK(wself.Btn);
        }else
        {
            NSLog(@"这个只有直播的直播展示cell 未实现电机时间");
        }
    };

    if ([Tool typePlayStartTime:startTime12 andEndTime:endTime12] == 1) {
        BOOL finishYuYue = [_localListCenter checkTheScheduledListwithID:eventId];
//****************搜索栏中的预约、已预约图片**************************************

        if (finishYuYue) {
            _Btn.selected = YES;
            YUYUE.image = [UIImage imageNamed:@"btn_live_foxpor_item_ordered"];
        }else
        {
            _Btn.selected = NO;
            YUYUE.image =  [UIImage imageNamed:@"btn_live_foxpor_item_order"];
        }
        _Btn.isYuYue = YES;
        _Btn.isLive = NO;
    }else if ([Tool typePlayStartTime:startTime12 andEndTime:endTime12] == 0)
    {
        _Btn.isLive = YES;
        YUYUE.image =  [UIImage imageNamed:@""];
        _Btn.isYuYue = NO;
    }
    else
    {
        YUYUE.image =  [UIImage imageNamed:@""];

         _Btn.isLive = NO;
        _Btn.isYuYue = NO;
    }
   
}

@end
