//
//  NewHistorySearchView.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "NewHistorySearchView.h"

@implementation NewHistorySearchView

- (instancetype)init
{
    if (self = [super init]) {
        
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
    
    self.title = [UILabel new];
    [superView addSubview:self.title];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15*kDeviceRate);;
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@(15*kDeviceRate));
        make.centerY.equalTo(superView.mas_centerY);
    }];
    
    self.title.font = HT_FONT_THIRD;
    self.title.textColor = HT_COLOR_ALERTCUSTOM;
    
    _Btn  = [UIButton_Block new];
    [self addSubview:_Btn];
    [_Btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    WS(wself);
    _Btn.Click = ^(UIButton_Block*btn,NSString * name)
    {
        if (wself.viewBlock!=nil) {
            NSLog(@"搜索历史cell知道点击了那个view");
            wself.viewBlock(btn);
        }
    };
}
- (void)updata
{
    
    NSDictionary *dic = self.dic;
    
    _Btn.Diction = dic;
    
    //    self.title = [self.dic objectForKey:@"groupName"];
    
    if (dic) {
        
        self.hidden = NO;
        
        self.title.text = [self.dic objectForKey:@"searchName"];
        
    }else
    {
        
        self.hidden = YES;
        
    }
    
    
}

@end
