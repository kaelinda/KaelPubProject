//
//  VODPhotoView.m
//  HIGHTONG
//
//  Created by testteam on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VODPhotoView.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton_Block.h"


@interface VODPhotoView ()
{
    
    UIButton_Block * _btn;
    UIImageView *BGview;
    UIImageView * Badge ;
    
}
@end


@implementation VODPhotoView

- (instancetype)init
{
    if (self = [super init]) {
        //        self.backgroundColor = [UIColor lightGrayColor];
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
    superView.backgroundColor = UIColorFromRGB(0xf7f7f7);
    UIView *ColorView = [UIView new];
    [superView addSubview:ColorView];
    [ColorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right).offset(-0*kRateSize);;
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom).offset(-0*kRateSize);
    }];
    ColorView.backgroundColor = [UIColor whiteColor];
    superView = ColorView;
    
    self.photo = [UIImageView new];
    [superView addSubview:self.photo];
    
    
    
    self.subTitle = [UILabel new];
    [superView addSubview:self.subTitle];
    
    
    self.timeTitle = [UILabel new];
    [superView addSubview:self.timeTitle];
    self.photo.backgroundColor = UIColorFromRGB(0xeeeeee);
    [self.photo mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.equalTo(superView.mas_centerY);
//        make.top.equalTo(superView.mas_top).offset(3*kRateSize);
        make.left.equalTo(superView.mas_left).offset(3*kRateSize);
        make.width.equalTo(@(51*kRateSize));
        make.height.equalTo(@(68*kRateSize));
//         make.bottom.equalTo(superView.mas_bottom).offset((-20)*kRateSize);
    }];
    
//    self.photo.backgroundColor = [UIColor redColor];
//    //    self.photo.contentMode = UIViewContentModeScaleAspectFit;
    self.photo.image =[UIImage imageNamed:@"poster"] ;
//
//    self.photo.backgroundColor = UIColorFromRGB(0xe3e2e3);
//    self.photo.layer.cornerRadius = 5;
//    self.photo.layer.masksToBounds = YES;
//
    Badge = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_common_special_net"]];
    [self.photo addSubview:Badge];
    [Badge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.photo.mas_top);
        make.right.equalTo(self.photo.mas_right);
        make.width.equalTo(@(21*kRateSize));
        make.height.equalTo(@(21*kRateSize));
        
    }];
    
    
    self.title = [UILabel new];
    self.title.textColor = [UIColor blackColor];
    [superView addSubview:self.title];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.photo.mas_right).offset(4*kRateSize);
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(self.photo.mas_bottom).offset(0*kRateSize);
        make.height.equalTo(@(20*kRateSize));
    }];
    
    
    _btn  = [UIButton_Block new];
    [superView addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(superView);
    }];
   
    [self.subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.photo.mas_right).offset(4*kRateSize);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(self.photo.mas_top).offset(0*kRateSize);
        make.height.equalTo(@(40*kRateSize));
        
    }];
    self.subTitle.numberOfLines = 2;
    self.subTitle.text = @"当前没有数据";

    UIFont *font  = [UIFont systemFontOfSize:13];
//    self.title.font = font;

    
    self.subTitle.font = font;
    self.subTitle.textAlignment = NSTextAlignmentLeft;
    font =  [UIFont systemFontOfSize:12];
    self.title.font = font;
//    self.subTitle.backgroundColor = [UIColor redColor];

}
- (void)updata
{
    WS(wself);
    
    
    //    contentID = 8e0e314d1cb24bd9b60b74e1a;
    //    imageLink = "http://192.168.3.23//CMS/20150814/2015081410400391451_450_645.jpg?id=652&crc=1484819196";
    //    name = "\U7834\U574f\U8005";
    //    seq = 3;
    NSDictionary *dic = self.dic;
    if (dic) {
        self.hidden = NO;
        //再赋值之前要判断   电影，  电视剧  综艺   或者其他， 以显示不同的效果
        if ([dic objectForKey:@"amount"] &&  [dic objectForKey:@"lastEpisode"]) {//电视剧应为有总集数

            NSString *lastEpisodeTitle = [dic objectForKey:@"lastEpisodeTitle"];
            //            if ([lastEpisode integerValue] ==  [amout integerValue]) {
            //                self.title.text = [NSString stringWithFormat:@"%@集全",amout];
            //            }else
            //            {
            //                self.title.text = [NSString stringWithFormat:@"更新至%@集",lastEpisode];
            //            }
            if (lastEpisodeTitle.length) {
                self.title.text = [NSString stringWithFormat:@"%@",lastEpisodeTitle];
            }
            
        }else
        {
            self.title.text = @"";
        }
        
        
        if (self.isMovie) {
            self.title.hidden = YES;
            BGview.hidden = YES;
        }else
        {
            self.title.hidden = NO;
            BGview.hidden = NO;
        }
        
        if([[dic objectForKey:@"flagPlayOuter"] isEqualToString:@"1"])
        {
            Badge.hidden = NO;
        }else{
            Badge.hidden = YES;
        }
       
        if ([dic objectForKey:@"name"]) {
            self.subTitle.text = [dic objectForKey:@"name"];
        }
        
        if ([dic objectForKey:@"imageLink"]) {
            [self.photo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"poster"]];
        }
        
        
        
        if ([dic objectForKey:@"contentID"]) {
            _btn.serviceIDName = [dic objectForKey:@"contentID"];
        }
        _btn.eventName = self.subTitle.text;
        _btn.Click = ^(UIButton_Block*btn,NSString * name)
        {
            if (wself.block) {
                NSLog(@"cell知道点击了那个view");
                wself.block(btn);
            }
        };
        
    }else
    {
        self.hidden = YES;
        self.photo.image =[UIImage imageNamed:@"poster"] ;
        self.title.text = @"";
        self.subTitle.text = @"";
        self.timeTitle.text = @"";
    }
    
}




@end

