//
//  SearchSHOWView.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/21.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
typedef void (^SearchShoWBlock) (UIButton_Block*btn);

@interface SearchSHOWView : UIView

@property (nonatomic,strong)NSDictionary *epginfo;

@property(nonatomic,strong)UIButton_Block *Btn;
@property(nonatomic,copy)SearchShoWBlock SHOWCLICK;

@property(nonatomic,copy)NSString *serviceId;
@property(nonatomic,copy)NSString *channelName;

- (void)loadView;

@end
