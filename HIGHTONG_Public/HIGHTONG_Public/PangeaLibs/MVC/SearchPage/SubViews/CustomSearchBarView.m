//
//  CustomSearchBarView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/23.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "CustomSearchBarView.h"

@implementation CustomSearchBarView

- (id)initWithFrame:(CGRect)frame andPlaceStr:(NSString *)str andLogoImg:(UIImage *)img
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *backView = [[UIView alloc] initWithFrame:self.frame];
        [self addSubview:backView];
        backView.backgroundColor = UIColorFromRGB(0xeeeeee);
        
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(backView.size.width, backView.size.height)];
        CAShapeLayer *masklayer = [[CAShapeLayer alloc] init];
        masklayer.path = maskPath.CGPath;
        backView.layer.mask = masklayer;
        
        
        
        _searchLogoImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8.5, 11, 11)];
        [backView addSubview:_searchLogoImg];
        _searchLogoImg.image = img;
        
        
        _placeholderLab = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, 270, backView.frame.size.height)];
        [backView addSubview:_placeholderLab];
        _placeholderLab.text = str;
        _placeholderLab.textColor = UIColorFromRGB(0x333333);
        _placeholderLab.font = [UIFont systemFontOfSize:10];
        _placeholderLab.textAlignment = NSTextAlignmentLeft;
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *backView = [[UIView alloc] initWithFrame:self.frame];
        [self addSubview:backView];
        backView.backgroundColor = UIColorFromRGB(0xeeeeee);
        
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(backView.size.width, backView.size.height)];
        CAShapeLayer *masklayer = [[CAShapeLayer alloc] init];
        masklayer.path = maskPath.CGPath;
        backView.layer.mask = masklayer;
        
        
        
        _searchLogoImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8.5, 11, 11)];
        [backView addSubview:_searchLogoImg];
        _searchLogoImg.image = [UIImage imageNamed:@"ic_search_logo"];
        
        
        _placeholderLab = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, 270, backView.frame.size.height)];
        [backView addSubview:_placeholderLab];
        _placeholderLab.textColor = UIColorFromRGB(0x333333);
        _placeholderLab.font = [UIFont systemFontOfSize:10*kDeviceRate];
        _placeholderLab.textAlignment = NSTextAlignmentLeft;
        _placeholderLab.userInteractionEnabled = YES;
        
        _senderBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [backView addSubview:_senderBtn];
        _senderBtn.frame = backView.frame;
        [_senderBtn addTarget:self action:@selector(clickSenderBtn) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    return self;
}

- (void)clickSenderBtn
{
    if (_searchBlock) {
        
        NSLog(@"搜索框被点击了");
        _searchBlock();
    }
}


//- (id)initWithSize:(CGSize)size
//{
//    self = [super init];
//    if (self) {
//        
//        UIView *backView = [[UIView alloc] init];
//        backView.size = size;
//        backView.center = self.center;
//        [self addSubview:backView];
//        backView.backgroundColor = UIColorFromRGB(0xeeeeee);
//        
//        
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(backView.size.width, backView.size.height)];
//        CAShapeLayer *masklayer = [[CAShapeLayer alloc] init];
//        masklayer.path = maskPath.CGPath;
//        backView.layer.mask = masklayer;
//        
//        
//        
//        _searchLogoImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8.5, 11, 11)];
//        [backView addSubview:_searchLogoImg];
//        _searchLogoImg.image = [UIImage imageNamed:@"ic_search_logo"];
//        
//        
//        _placeholderLab = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, 270, backView.frame.size.height)];
//        [backView addSubview:_placeholderLab];
//        _placeholderLab.textColor = UIColorFromRGB(0x333333);
//        _placeholderLab.font = [UIFont systemFontOfSize:10];
//        _placeholderLab.textAlignment = NSTextAlignmentLeft;
//        
//    }
//    return self;
//}
//
//- (id)initWithSize:(CGSize)size andCenter:(CGPoint)center
//{
//    self = [super init];
//    if (self) {
//        
//        UIView *backView = [[UIView alloc] init];
//        backView.size = size;
//        backView.center = center;//self.center;
//        [self addSubview:backView];
//        backView.backgroundColor = UIColorFromRGB(0xeeeeee);
//        
//        
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(backView.size.width, backView.size.height)];
//        CAShapeLayer *masklayer = [[CAShapeLayer alloc] init];
//        masklayer.path = maskPath.CGPath;
//        backView.layer.mask = masklayer;
//        
//        
//        
//        _searchLogoImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8.5, 11, 11)];
//        [backView addSubview:_searchLogoImg];
//        _searchLogoImg.image = [UIImage imageNamed:@"ic_search_logo"];
//        
//        
//        _placeholderLab = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, 270, backView.frame.size.height)];
//        [backView addSubview:_placeholderLab];
//        _placeholderLab.textColor = UIColorFromRGB(0x333333);
//        _placeholderLab.font = [UIFont systemFontOfSize:10];
//        _placeholderLab.textAlignment = NSTextAlignmentLeft;
//        
//    }
//    return self;
//}


@end
