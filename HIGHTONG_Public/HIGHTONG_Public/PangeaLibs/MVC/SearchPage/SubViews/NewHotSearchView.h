//
//  NewHotSearchView.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/25.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^To_NewHotSearch)(UIButton_Block*);

@interface NewHotSearchView : UIView

@property(nonatomic,strong)To_NewHotSearch viewBlock;

@property(nonatomic,strong)UIButton_Block *Btn;

@property(nonatomic,strong)UIButton *imageBtn;

@property(nonatomic,strong)UILabel *title;//标题

@property(nonatomic,strong)NSDictionary *dic;

- (void)updata;

@end
