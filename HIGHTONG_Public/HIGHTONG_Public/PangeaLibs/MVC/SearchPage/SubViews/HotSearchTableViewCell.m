//
//  HotSearchTableViewCell.m
//  HIGHTONG
//
//  Created by testteam on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HotSearchTableViewCell.h"
#import "Masonry.h"
@implementation HotSearchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        self.isRadius = NO;
        [self initView];
    }
    return self;
}
- (void)initView
{
    UIView *View =  self.contentView;
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
//        self.btn = [UIButton new];
    if (self.image) {
        [self.btn setBackgroundImage:self.image forState:UIControlStateNormal];
    }
    [View addSubview:self.btn];
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(View.mas_top).offset(5*kRateSize);
        make.left.equalTo(View.mas_left).offset(5*kRateSize);
        make.width.equalTo(@(40*kRateSize));
        make.height.equalTo(@(40*kRateSize));
    }];
    self.btn.layer.cornerRadius = 20*kRateSize;
//    self.btn.backgroundColor = [UIColor grayColor];
    
    self.numLable = [UILabel new];
    [View addSubview:self.numLable];
    [self.numLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.btn);
    }];
    
    self.numLable.backgroundColor = [UIColor clearColor];
    self.numLable.textColor = [UIColor whiteColor];

    if (isAfterIOS7) {
        self.numLable.textAlignment = NSTextAlignmentCenter;
    }else
    {
        self.numLable.textAlignment = NSTextAlignmentCenter;
    }
    
    
    
    
    
    
    self.label  = [UILabel new];

    if (self.title) {
        self.label.text = self.title;
    }

    [View addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(View.mas_top).offset(5);
        make.left.equalTo(self.btn.mas_right).offset(5*kRateSize);
        make.right.equalTo(View.mas_right);
        make.height.equalTo(@(40*kRateSize));
    }];
//    self.label.backgroundColor = [UIColor lightGrayColor];
    
//    self.deleteBtn = [UIButton_Block buttonWithType:UIButtonTypeRoundedRect];
    self.deleteBtn = [UIButton_Block new];
    [View addSubview:self.deleteBtn];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(View.mas_top).offset(5*kRateSize);
        make.right.equalTo(View.mas_right).offset(-55*kRateSize);
        make.width.equalTo(@(40*kRateSize));
        make.height.equalTo(@(40*kRateSize));
    }];
    
    
    self.goBtn = [UIButton_Block new];;
    [View addSubview:self.goBtn];
    [self.goBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(View.mas_top).offset(5*kRateSize);
        make.right.equalTo(View.mas_right).offset(-5*kRateSize);
        make.width.equalTo(@(40*kRateSize));
        make.height.equalTo(@(40*kRateSize));
    }];
//    self.goBtn.backgroundColor = [UIColor redColor];
    [self.goBtn setBackgroundImage:[UIImage imageNamed:@"icon_symbol_01"] forState:UIControlStateNormal];
    self.goBtn.userInteractionEnabled = NO;
    [self.deleteBtn setBackgroundImage:[UIImage imageNamed:@"sl_search_del_item_pressed"] forState:UIControlStateNormal];
//    self.deleteBtn.backgroundColor = [UIColor yellowColor];
    self.deleteBtn.hidden = YES;
    self.goBtn.hidden = YES;
}

@end
