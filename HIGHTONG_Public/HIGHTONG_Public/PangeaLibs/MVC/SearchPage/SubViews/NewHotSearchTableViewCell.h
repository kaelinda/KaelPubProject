//
//  NewHotSearchTableViewCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/25.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewHotSearchView.h"
#import "UIButton_Block.h"

typedef void (^NewHotSearchTableViewCellBlock) (UIButton_Block*);

@interface NewHotSearchTableViewCell : UITableViewCell

@property (nonatomic,copy)NewHotSearchTableViewCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)NewHotSearchView *leftView;//左边

@property (nonatomic,strong)NewHotSearchView *rightView;//右边


- (void)update;//跟新数据

@end
