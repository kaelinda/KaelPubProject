//
//  NewKeyWordSearchTableViewCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "NewKeyWordSearchTableViewCell.h"

@implementation NewKeyWordSearchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.cellDic = [NSMutableDictionary dictionary];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    UIView *superView = self;
    
    self.backgroundColor = HT_COLOR_FONT_INVERSE;
    
    self.titleLb = [UILabel new];
    self.titleLb.font = HT_FONT_THIRD;
    self.titleLb.textAlignment = NSTextAlignmentLeft;
    self.titleLb.textColor = HT_COLOR_ALERTCUSTOM;

    [superView addSubview:self.titleLb];
    
    self.categoryLb = [UILabel new];
    self.categoryLb.font = HT_FONT_THIRD;
    self.categoryLb.textAlignment = NSTextAlignmentRight;
    self.categoryLb.textColor = HT_COLOR_ALERTCUSTOM;
    
    [superView addSubview:self.categoryLb];

    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15*kDeviceRate);
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(200*kDeviceRate));
        make.centerY.equalTo(superView.mas_centerY);
    }];
    
    [self.categoryLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-15*kDeviceRate);;
        make.height.equalTo(@(20*kDeviceRate));
        make.width.equalTo(@(100*kDeviceRate));
        make.centerY.equalTo(superView.mas_centerY);
    }];

    
}

- (void)update
{
    
    NSLog(@"这个破cell的各种值------%@",self.cellDic);
    
    if ([self.cellDic objectForKey:@"properties"]) {
        self.titleLb.text = [[self.cellDic objectForKey:@"properties"]objectForKey:@"name"];
    }else
    {
        self.titleLb.text = [self.cellDic objectForKey:@"name"];
    }
    
    
    self.categoryLb.text = [[self.cellDic objectForKey:@"properties"]objectForKey:@"category"];
    
}


@end
