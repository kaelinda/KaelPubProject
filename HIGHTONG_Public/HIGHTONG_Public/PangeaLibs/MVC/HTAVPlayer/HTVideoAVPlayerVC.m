//
//  HTVideoAVPlayerVC.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTVideoAVPlayerVC.h"
#import "ShareView.h"
#import "EpgSmallVideoPlayerVC.h"
#import "KaelTool.h"
#import "UserLoginViewController.h"
#import "ShareCollectionModel.h"
@interface HTVideoAVPlayerVC ()<shareViewDelegate>
{
    NSString *shareLink;
    BOOL canGoOnAutoHidden;
    
    BOOL islock;
}
@end

@implementation HTVideoAVPlayerVC

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_backgroundView.autoHiddenTimer) {
        [_backgroundView.autoHiddenTimer invalidate];
        _backgroundView.autoHiddenTimer = nil;
    }
    [_backgroundView resetPlayer];
    [_backgroundView removeAllSubviews];

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //这里需要 把状态条改为白色的
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
    
}





-(void)setPolyMedicDic:(NSMutableDictionary *)polyMedicDic
{
    if (_polyMedicDic) {
        _polyMedicDic = nil;
    }
    
    _polyMedicDic = polyMedicDic;
}

- (id)init
{
    self = [super init];
    if (self) {
        canGoOnAutoHidden = YES;
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocationOrder:) name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
        
          }
    
    return self;
}


#pragma mark - 本地预约通知

-(void)receiveLocationOrder:(NSNotification*)notifi
{
    //    NSMutableDictionary *notiItem = [[NSMutableDictionary alloc] initWithDictionary:notifi.object];
    //    NSLog(@"11111 :%@ 22222:%@",notifi.userInfo,notifi.object);
    
//  [self.navigationController popViewControllerAnimated:YES];
    
//    _backgroundView.isConstraintLandscape = YES;
    [_backgroundView setIsConstraintLandscape:NO];
    
    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
    live.serviceID = [notifi.userInfo objectForKey:@"serviceID"];
    live.serviceName = [notifi.userInfo objectForKey:@"serviceName"];
    live.eventURLID = [notifi.userInfo valueForKey:@"eventID"];
    NSString *MtimeDateString = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"startTime"]];
    
    if ([MtimeDateString componentsSeparatedByString:@" "].count>0) {
        
        live.currentDateStr = [[MtimeDateString componentsSeparatedByString:@" "] objectAtIndex:0];
    }
    
    live.isLocationOrder = YES;
    live.isLocationOrderVOD = YES;
    
    
    
    
    
    [self.navigationController pushViewController:live animated:YES];
    
}





- (void)viewDidLoad {
    [super viewDidLoad];
 
    
    [self hideNaviBar:YES];
    [self.view setBackgroundColor:[UIColor clearColor]];
    //    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    //
    //    [request VODAssccationListWithContentID:@"34528cbc4bb94f5cb69d43964591774c" andCount:30 andTimeout:KTimeout];
    
    
    _backgroundView = [HTPlayerView sharedPlayerView];
    _backgroundView.currentPlayType = CurrentPlayVod;
    [_backgroundView setBackgroundColor:[UIColor blackColor]];
    if (!_polyMedicDic) {
        _backgroundView.contentID = _contentID;

    }else
    {
        _backgroundView.polyMedicDic =[NSMutableDictionary dictionaryWithDictionary:_polyMedicDic] ;

    }
    if (IS_RECOMMENDARRAY) {
        
        [_backgroundView setAsscciationArray:[NSMutableArray arrayWithArray:_recommendArray]];
        
    }
    _backgroundView.contentTitleStr = _titleStr;
//    _backgroundView.polyModel = _polyModel;
    [_backgroundView setPolyModel:_polyModel];
//    _backgroundView.polyMedicType = _polyMedicType;
    //    [_backgroundView setVideoURL:[NSURL URLWithString:@"http://baobab.wdjcdn.com/1457716884751linghunbanlv_x264.mp4"]];
    //    _backgroundView.controlView.currentPlayType = CurrentPlayVod;
    [self.view addSubview:_backgroundView];
    WS(wself);
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.height.mas_equalTo(194*kRateSize);
        make.width.mas_equalTo(wself.view.mas_width);
    }];
    _backgroundView.isConstraintLandscape = YES;
//    _backgroundView.isFullScreen = NO;
    _backgroundView.goBackBlock = ^{
        
        [wself.navigationController popViewControllerAnimated:YES];
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];        
        
        canGoOnAutoHidden = NO;

    };
    
    
    _backgroundView.shareBlock = ^(DemandModel*demand){
        NSLog(@"我已经实现了分享的BLOCK");
        
        _demandModel = [[DemandModel alloc]init];
        _demandModel = demand;
        
        shareLink = [KaelTool getChannelId:[NSString stringWithFormat:@"%@",_demandModel.CONTENTID] andChannelTitle:@"" withPlayType:CurrentPlayPolyMedic];

        
        if (shareLink <=0) {
            
            [AlertLabel AlertInfoWithText:@"服务器正在打盹，请稍后再试" andWith:wself.view withLocationTag:2];
            return;
        }
        
        wself.backgroundView.controlView.shareView.delegate = wself;
      wself.backgroundView.controlView.shareView.hidden = !wself.backgroundView.controlView.shareView.hidden;
        wself.backgroundView.controlView.shareView.installedWX = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
        wself.backgroundView.controlView.shareView.installedQQ = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ];
        [wself.backgroundView.controlView showControlViewAndIsFullScreen:YES];
        
        wself.backgroundView.controlView.shareView.returnBlock = ^(NSInteger tag) {
            
            NSString *tempLink;
            
            if (!isEmptyStringOrNilOrNull(shareLink)) {
                tempLink = shareLink;
            }else{
                tempLink = @"";
            }
            
            NSString* thumbURL = NSStringPM_IMGFormat(wself.demandModel.imageLink);

            NSString* umengShareID = wself.demandModel.CONTENTID;
            
            NSString* umengShareTitle = wself.demandModel.name;
            
            NSString *contentString = [NSString stringWithFormat:@"我正在看%@，太精彩了！安装#%@#手机客户端，电视点播免费看啦！快快安装一起看吧。",wself.demandModel.name,kAPP_NAME];

            
            switch (tag) {
                case 0:
                {
                    [wself.backgroundView.controlView.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechat currentController:wself];
                    wself.backgroundView.controlView.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                        if (isSuccess == 1) {
                            
                            [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                            NSLog(@"分享成功！");
                            wself.backgroundView.controlView.shareView.hidden = YES;
                            wself.backgroundView.isMaskShowing = YES;
                            [wself.backgroundView.controlView showControlViewAndIsFullScreen:YES];
                            
                            
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:umengShareID,@"shareID",umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"2",@"shartType",@"0",@"shartModel", nil];
                            wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                            [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                            
                        }
                    };
                    
                }
                    break;
                case 1:
                {
                    [wself.backgroundView.controlView.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechatZone currentController:wself];
                    wself.backgroundView.controlView.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                        if (isSuccess == 1) {
                            
                            [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                            NSLog(@"分享成功！");
                            wself.backgroundView.controlView.shareView.hidden = YES;
                            wself.backgroundView.isMaskShowing = YES;
                            [wself.backgroundView.controlView showControlViewAndIsFullScreen:YES];
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:umengShareID,@"shareID",umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"2",@"shartType",@"0",@"shartModel", nil];
                            wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                            [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }
                    };
                }
                    break;
                case 2:
                {
                    [wself.backgroundView.controlView.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQ currentController:wself];
                    wself.backgroundView.controlView.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                        if (isSuccess == 1) {
                            
                            [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                            NSLog(@"分享成功！");
                            wself.backgroundView.controlView.shareView.hidden = YES;
                            wself.backgroundView.isMaskShowing = YES;
                            [wself.backgroundView.controlView showControlViewAndIsFullScreen:YES];
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:umengShareID,@"shareID",umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"2",@"shartType",@"0",@"shartModel", nil];
                            wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                            [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }
                    };
                }
                    break;
                case 3:
                {
                    [wself.backgroundView.controlView.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQZone currentController:wself];
                    wself.backgroundView.controlView.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                        if (isSuccess == 1) {
                            
                            [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                            NSLog(@"分享成功！");
                            wself.backgroundView.controlView.shareView.hidden = YES;
                            wself.backgroundView.isMaskShowing = YES;
                            [wself.backgroundView.controlView showControlViewAndIsFullScreen:YES];
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:umengShareID,@"shareID",umengShareTitle,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"2",@"shartType",@"0",@"shartModel", nil];
                            wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                            [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }
                    };
                }
                    break;
                    
                default:
                    break;
            }
        };
        
    };
    
    _backgroundView.favorBlock = ^(BOOL isLogin){
        
        if (IsLogin == NO) {
            if (isAuthCodeLogin) {
//                [self changeLoginVerticalDirection];
//                [self setIsFavorClick:YES];
                [wself.backgroundView setIsConstraintLandscape:NO];
                wself.backgroundView.isPauseByUser = YES;
                [wself.backgroundView actionPauseForVideo];
                [wself setIsNeedAutoLoginClick:YES];
               
                UserLoginViewController *user = [[UserLoginViewController alloc] init];
                user.loginType = kVideoLogin;
                [wself.view addSubview:user.view];
                user.backBlock = ^(){
                    _isNeedAutoLoginClick = NO;
                    wself.backgroundView.isConstraintLandscape = YES;
                    wself.backgroundView.isPauseByUser = NO;

                    [wself.backgroundView actionPlayForVideo];
                };
                
                
                user.tokenBlock = ^(NSString *token){
                    _isNeedAutoLoginClick = NO;
                    wself.backgroundView.isConstraintLandscape = YES;
                    wself.backgroundView.isPauseByUser = NO;

                     [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.backgroundView withLocationTag:1];
                    [wself.backgroundView actionPlayForVideo];
 
                };
                
            }else
            {
                [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.backgroundView withLocationTag:1];
            }
        }
        
        
        NSLog(@"我已经实现了收藏的BLOCK");
    };
    _backgroundView.videoListBlock = ^(UIButton*button,BOOL isSelecte){
        NSLog(@"我已经实现了节目单列表的BLOCK");
    };
    
    
    _backgroundView.lockBlock = ^(UIButton*button,BOOL isSelecte)
    {
        islock = isSelecte;
    };
    
    
    
    [self listeningRotating];

    
}



/**
 *  监听设备旋转通知
 */
- (void)listeningRotating
{
    //    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onDeviceOrientationChange)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
}




/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange
{
    if (!canGoOnAutoHidden) {
        return;
    }
    
    NSInteger interfaceOrientation = [[UIDevice currentDevice] orientation];

    switch (interfaceOrientation) {
            //        case UIInterfaceOrientationPortraitUpsideDown:{
            //            self.controlView.fullScreenVODBtn.selected = YES;
            //            self.isFullScreen = YES;
            //
            //        }
            //            break;
        case 1:{
           
                [_backgroundView setIsFullScreen:NO];
            
        }
            break;
        case 4:{
            [_backgroundView setIsFullScreen:YES];
        }
            break;
        case 3:{
            [_backgroundView setIsFullScreen:YES];

        }
            break;
            
        default:
            break;
    }
    
    
//    [_backgroundView setIsMaskShowing:YES];

}



//弹出列表方法presentSnsIconSheetView需要设置delegate为self
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}



#pragma mark --
#pragma mark  自动旋转

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    WS(wself);
    
    
    float widht;
    float hight;
    
    if (isAfterIOS8)
    {
        widht = KDeviceHeight;
        hight = kDeviceWidth;
    }else    {
        widht = kDeviceWidth;
        hight = KDeviceHeight;
    }
    if(UIInterfaceOrientationIsLandscape(interfaceOrientation))
    {
        
        [_backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            //xiaoxiugai
            make.top.mas_equalTo(wself.view.mas_top).offset(0);
            make.left.mas_equalTo(wself.view.mas_left);
            make.width.mas_equalTo(widht);
            make.height.mas_equalTo(hight);
            make.bottom.mas_equalTo(wself.view.mas_bottom);
        }];
        
    }else
    {
        [_backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
            make.centerX.mas_equalTo(wself.view.mas_centerX);
            make.height.mas_equalTo(194*kRateSize);
            make.width.mas_equalTo(wself.view.mas_width);
        }];
    }
}


-(void)setIsNeedAutoLoginClick:(BOOL)isNeedAutoLoginClick
{
    _isNeedAutoLoginClick = isNeedAutoLoginClick;
    
    if (_isNeedAutoLoginClick) {
        
         [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    }else
    {
        
    }
}



- (BOOL)shouldAutorotate {
    return _isNeedAutoLoginClick||islock?NO:YES;
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0

- (NSUInteger)supportedInterfaceOrientations

#else

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

#endif

{
    
    
    return _backgroundView.isConstraintLandscape?  UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskAllButUpsideDown;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    //viewController初始显示的方向
    return _backgroundView.isConstraintLandscape ? UIInterfaceOrientationLandscapeRight:UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
