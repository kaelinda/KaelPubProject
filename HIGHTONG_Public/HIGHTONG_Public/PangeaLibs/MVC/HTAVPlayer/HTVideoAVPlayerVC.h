//
//  HTVideoAVPlayerVC.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "HTPlayerView.h"
#import "HTRequest.h"
#import <UMSocialCore/UMSocialCore.h>
#import "WXApi.h"
#import "ShareView.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "DemandModel.h"
#import "HealthVodCollectionModel.h"
#import "ShareCollectionModel.h"
@interface HTVideoAVPlayerVC : HT_FatherViewController<HTRequestDelegate>
@property (nonatomic,strong)HTPlayerView *backgroundView;
@property (nonatomic,strong)NSString *contentID;
@property (nonatomic,strong)NSString *titleStr;
@property (nonatomic,strong)NSString *polyMedicType;
@property (nonatomic,strong)NSMutableDictionary *polyMedicDic;
@property (nonatomic,strong)NSMutableArray *recommendArray;
@property (nonatomic,strong)DemandModel *demandModel;
@property (nonatomic,strong)HealthVodCollectionModel *polyModel;
@property (nonatomic,assign) BOOL    isNeedAutoLoginClick;
@property (nonatomic,strong)ShareCollectionModel *shareUmengModel;

/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;
@end
