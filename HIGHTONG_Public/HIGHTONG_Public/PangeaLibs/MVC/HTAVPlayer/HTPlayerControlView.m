//
//  HTPlayerControlView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTPlayerControlView.h"
static NSString * const kMASCellReuseIdentifier = @"kMASCellReuseIdentifier";
static NSString * const kMNSCellReuseIdentifier = @"kMASCellReuseIdentifier";

@implementation HTPlayerControlView

- (instancetype)init
{
    self = [super init];    
    if (self) {
      
        [self initControlSubViews];
        
    }
    return self;
}


-(void)initControlSubViews
{
    self.userInteractionEnabled = YES;
    
    
    _loadingMaskView = [[PlayerMaskView alloc] init];
    _loadingMaskView.backgroundColor = [UIColor clearColor];
    _loadingMaskView.programType = LoadingViewType;
    _loadingMaskView.hidden = YES;
    _loadingMaskView.tag = 4040;
    _loadingMaskView.maskViewReturnBlock = ^(){
        
        
    };
    
    
    
    
    _polyMedicineView = [[PolyMedicineLoadingView alloc]init];
    [self addSubview:_polyMedicineView];
   
    
    _videoListVODView = [[UIView alloc]init];
    [_videoListVODView setBackgroundColor:[UIColor clearColor]];
    _videoListVODView.userInteractionEnabled = YES;
    _videoListVODView.hidden = YES;
    _epgTopImageView                        = [[UIImageView alloc] init];
    _epgTopImageView.userInteractionEnabled = YES;
    _epgTopImageView.image                  = [UIImage imageNamed:@"bg_player_playbill_listview_epg"];
    
   
    
    _epgBottomImageView                        = [[UIImageView alloc] init];
    _epgBottomImageView.userInteractionEnabled = YES;
    _epgBottomImageView.image                  = [UIImage imageNamed:@"bg_player_controller_bottom"];
    
    
    _vodTopImageView                        = [[UIImageView alloc] init];
    _vodTopImageView.userInteractionEnabled = YES;
    _vodTopImageView.image                  = [UIImage imageNamed:@"bg_player_playbill_listview_epg"];
//    [_vodTopImageView setBackgroundColor:[UIColor redColor]];
    
    _serviceTitleVODLabel = [[UILabel alloc]init];

    [_serviceTitleVODLabel setBackgroundColor:[UIColor clearColor]];
    _serviceTitleVODLabel.font = [UIFont systemFontOfSize:18];
    _serviceTitleVODLabel.textColor = UIColorFromRGB(0xffffff);
    
    
    _backVODBtn = [[UIButton alloc]init];
    _backVODBtn.showsTouchWhenHighlighted = YES;
    [_backVODBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
    
    
    
    _shareVODBtn = [[UIButton alloc]init];
    [_shareVODBtn setImage:[UIImage imageNamed:@"btn_player_share_normal"] forState:UIControlStateNormal];
    [_shareVODBtn setImage:[UIImage imageNamed:@"btn_player_share_pressed"] forState:UIControlStateHighlighted];
    
    
    _favorVODBtn = [[UIButton alloc]init];
    
    _videoListVODBtn = [[UIButton alloc]init];
    _videoListVODBtn.showsTouchWhenHighlighted = YES;
    [_videoListVODBtn setImage:[UIImage imageNamed:@"ic_player_recommend_normal"] forState:UIControlStateNormal];
    [_videoListVODBtn setImage:[UIImage imageNamed:@"ic_player_recommend_pressed"] forState:UIControlStateSelected];
    
    
    
    
    
    
    _vodBottomImageView                        = [[UIImageView alloc] init];
    _vodBottomImageView.userInteractionEnabled = YES;
    [_vodBottomImageView setBackgroundColor:[UIColor clearColor]];
    _vodBottomImageView.image                  = [UIImage imageNamed:@"bg_player_controller_bottom"];
    
    
    _currentEPGTimeLabel               = [[UILabel alloc] init];
    _currentEPGTimeLabel.textColor     = [UIColor whiteColor];
    _currentEPGTimeLabel.font          = [UIFont systemFontOfSize:12.0f];
    _currentEPGTimeLabel.textAlignment = NSTextAlignmentCenter;
    [self.epgBottomImageView addSubview:_currentEPGTimeLabel];
    
    _currentVODTimeLabel               = [[UILabel alloc] init];
    _currentVODTimeLabel.backgroundColor = [UIColor clearColor];
    _currentVODTimeLabel.text = @"00:00:00";
    _currentVODTimeLabel.textColor = COLOR_FROM_RGB(0xeeeeee);
    _currentVODTimeLabel.font = [UIFont systemFontOfSize:11*kRateSize];
    _currentVODTimeLabel.textAlignment = NSTextAlignmentRight;
    
    
    
    _totalVODTimeLabel               = [[UILabel alloc] init];
    _totalVODTimeLabel.backgroundColor = [UIColor clearColor];
    _totalVODTimeLabel.text = @"00:00:00";
    _totalVODTimeLabel.font = [UIFont systemFontOfSize:11*kRateSize];
    
    _totalVODTimeLabel.textColor = COLOR_FROM_RGB(0xeeeeee);
    //_totalTimeL.textAlignment = NSTextAlignmentLeft;
    _totalVODTimeLabel.textAlignment = NSTextAlignmentLeft;
    
    _fullScreenVODBtn = [[UIButton alloc]init];
    [_fullScreenVODBtn setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal"] forState:UIControlStateNormal];

    
    _videoVODSlider                       = [[UISlider alloc] init];
    [_videoVODSlider setThumbImage:[UIImage imageNamed:@"ic_player_seekbar_progress_cursor"] forState:UIControlStateNormal];
    _videoVODSlider.userInteractionEnabled = YES;
    [_videoVODSlider setMaximumTrackImage:[UIImage imageNamed:@"bg_player_progress_normal"] forState:UIControlStateNormal];
    [_videoVODSlider setMinimumTrackImage:[UIImage imageNamed:@"bg_player_progress_pressed"] forState:UIControlStateNormal];
    [_videoVODSlider setBackgroundColor:[UIColor clearColor]];
    
    
    [_videoVODSlider setMaximumValue:1000.00];
    [_videoVODSlider setMinimumValue:0.00];
   
    
    
    _startVODBtn = [[UIButton alloc]init];//btn_hum_player_pause
    [_startVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_play_normal"] forState:UIControlStateNormal];
    [_startVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal"] forState:UIControlStateSelected];
    [_startVODBtn setBackgroundColor:[UIColor clearColor]];
    
    
    _backwardVODBtn = [[UIButton alloc]init];
    [_backwardVODBtn setImage:[UIImage imageNamed:@"btn_next"] forState:UIControlStateNormal];
    [_backwardVODBtn setImage:[UIImage imageNamed:@"btn_player_movenext_pressed"] forState:UIControlStateHighlighted];
    [_vodBottomImageView addSubview:_backwardVODBtn];
    
    
    
    _progressVODView                   = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressVODView.progressTintColor = [UIColor colorWithRed:89 green:89 blue:89 alpha:1];
    _progressVODView.progressTintColor = [UIColor colorWithRed:0.31 green:0.31 blue:0.31 alpha:1.00];
    
    _progressVODView.trackTintColor    = [UIColor clearColor];
    [self addSubview:_videoListVODView];
    
    
    
    _resolutionBtn = [[UIButton alloc]init];
    [_resolutionBtn setBackgroundColor:[UIColor clearColor]];
    
    [self.resolutionBtn addTarget:self action:@selector(resolutionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _progressTimeView = [[UIImageView alloc]init];
    _progressTimeView.hidden = YES;
    [_progressTimeView setImage:[UIImage imageNamed:@"bg_player_full_fast_action"]];
  
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    _progressTimeLable_top = [[UILabel alloc]init];
    _progressTimeLable_top.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_top.textColor = App_selected_color;
    _progressTimeLable_top.backgroundColor = [UIColor clearColor];
    _progressTimeLable_top.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_top.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_top.shadowOffset = CGSizeMake(1.0, 1.0);
    
    _progressTimeLable_bottom = [[UILabel alloc]init];
    _progressTimeLable_bottom.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_bottom.textColor = UIColorFromRGB(0xffffff);
    _progressTimeLable_bottom.backgroundColor = [UIColor clearColor];
    _progressTimeLable_bottom.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_bottom.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_bottom.hidden = NO;
    _progressTimeLable_bottom.shadowOffset = CGSizeMake(1.0, 1.0);
    
    
    _shareView = [[ShareView alloc]init];
    _shareView.hidden = YES;

    _brightnessVODView = [[UIImageView alloc]init];
    _brightnessVODView.image = [UIImage imageNamed:@"bg_player_brightness.png"];
    _brightnessVODView.hidden = YES;
    _brightnessVODProgress = [[UIProgressView alloc]init];
    _brightnessVODProgress.trackImage = [UIImage imageNamed:@"video_num_bg.png"];
    _brightnessVODProgress.progressImage = [UIImage imageNamed:@"video_num_front.png"];
    _brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
    _brightnessVODProgress.hidden = NO;
    
    
    _volumeVODProgressV = [[UIProgressView alloc]init];

    
    /**
     * /锁屏
     */
    
    _screenLockedVODIV = [[UIImageView alloc]init];
    _screenLockedVODIV.hidden = NO;
    _screenLockedVODIV.userInteractionEnabled = YES;
    [_screenLockedVODIV setBackgroundColor:[UIColor clearColor]];
    _lockedVODBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_lockedVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_unlock"] forState:UIControlStateNormal];
    [_lockedVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_lock"] forState:UIControlStateSelected];
//
//    [_lockedVODBtn addTarget:self action:@selector(changeTheLockedState:) forControlEvents:UIControlEventTouchUpInside];
    
    [_screenLockedVODIV addSubview:_lockedVODBtn];
    
    
    
    [_vodBottomImageView addSubview:_currentVODTimeLabel];
    [_vodBottomImageView addSubview:_totalVODTimeLabel];
    
    [_vodBottomImageView addSubview:_fullScreenVODBtn];
    [_vodBottomImageView addSubview:_startVODBtn];
    
    [_vodBottomImageView addSubview:_progressVODView];
    [_vodBottomImageView addSubview:_videoVODSlider];

    [_vodBottomImageView addSubview:_fullScreenVODBtn];
    [_vodBottomImageView addSubview:_resolutionBtn];

    [self addSubview:_loadingMaskView];

    [self addSubview:_progressTimeView];
    
    [_progressTimeView addSubview:_progressDirectionIV];
    [_progressTimeView addSubview:_progressTimeLable_bottom];
    [_progressTimeView addSubview:_progressTimeLable_top];
    [self addSubview:_epgTopImageView];
    [self addSubview:_epgBottomImageView];
    [self addSubview:_shareView];
    [self addSubview:_vodTopImageView];
    [self addSubview:_vodBottomImageView];
    [self addSubview:_brightnessVODView];
    [_brightnessVODView addSubview:_brightnessVODProgress];
    [self addSubview:_volumeVODProgressV];
    [self addSubview:_screenLockedVODIV];
    [_vodTopImageView addSubview:_serviceTitleVODLabel];
    
    [_vodTopImageView addSubview:_backVODBtn];
    [_vodTopImageView addSubview:_shareVODBtn];
    [_vodTopImageView addSubview:_favorVODBtn];
    
    [_vodTopImageView addSubview:_videoListVODBtn];



    
}



-(void)setContentID:(NSString *)contentID
{
    _contentID = contentID;
}

#pragma mark  多码率的相关代码

- (void)resolutionAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    // 显示隐藏分辨率View
    self.resolutionView.hidden = !sender.isSelected;
}

/**
 *  点击切换分别率按钮
 */
- (void)changeResolution:(UIButton *)sender
{
    // 隐藏分辨率View
    self.resolutionView.hidden  = YES;
    // 分辨率Btn改为normal状态
    self.resolutionBtn.selected = NO;
    // topImageView上的按钮的文字
    [sender setTitleColor:App_white_color forState:UIControlStateNormal];
    sender.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
    [sender setBackgroundImage:[UIImage imageNamed:@"bg_coderate_btn_click"] forState:UIControlStateNormal];
    [self.resolutionBtn setTitle:sender.titleLabel.text forState:UIControlStateNormal];
    if (self.resolutionBlock) { self.resolutionBlock(sender); }
}


#pragma mark currentPlayType的set方法


-(void)setCurrentPlayType:(CurrentPLAYType)currentPlayType
{
    _currentPlayType = currentPlayType;
    
    [self resetControlView];

    [self makePlayerSubViewsConstraints];
    
}

#pragma mark model类型

-(void)setDemandModel:(DemandModel *)demandModel
{
    if (_demandModel) {
        _demandModel = nil;
    }
    _demandModel = demandModel;
    
}

#pragma mark polyMedicine的set方法

-(void)setPolyMedicDic:(NSMutableDictionary *)polyMedicDic
{
    _polyMedicDic = polyMedicDic;
    
}


#pragma mark 推荐数组的set方法

-(void)setAsscciationArray:(NSMutableArray *)asscciationArray
{

    _asscciationArray = asscciationArray;
    if (_demandModel) {
        _demandModel = nil;
    }
    _demandModel = [[DemandModel alloc]init];
    if (!_videoListVODTV) {
        _videoListVODTV = [[UITableView alloc]init];
        _videoListVODTV.userInteractionEnabled = YES;
        _videoListVODTV.delegate = self;
        _videoListVODTV.dataSource = self;
        [_videoListVODTV setBackgroundColor:[UIColor clearColor]];
        
        //    _videoListTV.alpha = 0.8;
        //
        //    [_videoListTV setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"interBackView"]]];
        
        [_videoListVODTV setBackgroundView:[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];
        
        _videoListVODTV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_videoListVODTV registerClass:[DemandVideoListCell class] forCellReuseIdentifier:kMASCellReuseIdentifier];
        
        [_videoListVODView addSubview:_videoListVODTV];
    }
    
    
    
    
    //节目单的数据列表
    
    [_videoListVODTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_videoListVODView.mas_top);
        make.width.mas_equalTo(139*kRateSize
                               );
        make.bottom.mas_equalTo(_videoListVODView.mas_bottom);
        make.right.mas_equalTo(_videoListVODView.mas_right);
    }];
    
    [_videoListVODTV reloadData];
    
}


#pragma mark - 多码率数组的set方法

- (void)setResolutionArray:(NSMutableArray *)resolutionArray
{
    _resolutionArray = resolutionArray;
    
    [_resolutionBtn setTitle:[resolutionArray.firstObject objectForKey:isUsePolymedicine_2_0? @"bitRateName":@"bitLabel"] forState:UIControlStateNormal];
    _resolutionBtn.titleLabel.font  =  [UIFont boldSystemFontOfSize:11*kRateSize];
    // 添加分辨率按钮和分辨率下拉列表
    self.resolutionView = [[UIView alloc] init];
    self.resolutionView.hidden = YES;
    self.resolutionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.resolutionView];
    
    [self.resolutionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(30*resolutionArray.count*kRateSize);
        make.right.equalTo(self.resolutionBtn.mas_right).offset(0);
        make.bottom.equalTo(self.resolutionBtn.mas_top).offset(0);
    }];
    // 分辨率View上边的Btn
    for (int i = 0 ; i < resolutionArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 200+i;
        btn.frame = CGRectMake(0, 30*i, 50*kRateSize, 30*kRateSize);
        
        [btn setBackgroundColor:[UIColor clearColor]];
        [btn setTitleColor:UIColorFromRGB(0xeeeeee) forState:UIControlStateNormal];
        btn.showsTouchWhenHighlighted = YES;
        [self.resolutionView addSubview:btn];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
        [btn setTitle:[resolutionArray[i] objectForKey:isUsePolymedicine_2_0? @"bitRateName":@"bitLabel"] forState:UIControlStateNormal];
        
        if ([[resolutionArray.firstObject objectForKey:isUsePolymedicine_2_0? @"bitRateName":@"bitLabel"] isEqualToString:[resolutionArray[i] objectForKey:isUsePolymedicine_2_0? @"bitRateName":@"bitLabel"]]) {
            
            [btn setTitleColor:App_white_color forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
            [btn setBackgroundImage:[UIImage imageNamed:@"bg_coderate_btn_click"] forState:UIControlStateNormal];
        }
        
        
        
        [btn addTarget:self action:@selector(changeResolution:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}



#pragma mark 播放器的子视图的约束

-(void)makePlayerSubViewsConstraints{
    
    switch (_currentPlayType) {
        case CurrentPlayLive:
        {
            [self epgBarViewConstraints];
            
        }
            break;
        case CurrentPlayEvent:
        {
            [self epgBarViewConstraints];

        }
            break;
        case CurrentPlayTimeMove:
        {
            [self epgBarViewConstraints];

        }
            break;
        case CurrentPlayVod:
        {
            [self vodBarViewConstraints];

        }
            break;
        
            
        default:
            break;
    }
    
    
    
}

#pragma mark epg的信息条的约束

-(void)epgBarViewConstraints
{
    WS(wself);
    [_epgTopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.mas_top).offset(-15*kRateSize);
        make.left.mas_equalTo(wself.mas_left);
        make.height.mas_equalTo(80*kRateSize
                                );
        make.width.mas_equalTo(self);
    }];
    
    
    [_epgBottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.height.mas_equalTo(60*kRateSize
                                );
        make.width.mas_equalTo(wself.mas_width);
    }];
    
}


#pragma mark 点播的VOD信息条的约束

-(void)vodBarViewConstraints
{
    WS(wself);
    [_vodTopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.mas_top).offset(-15*kRateSize);
        make.left.mas_equalTo(wself.mas_left);
        make.height.mas_equalTo(80*kRateSize
                                );
        make.width.mas_equalTo(self);
    }];
    
    [_serviceTitleVODLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(wself.vodTopImageView.mas_left).offset(50*kRateSize+kTopSafeSpace);
        make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(36*kRateSize);        make.width.mas_equalTo(220*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
    
    [_backVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(27*kRateSize);
        make.left.mas_equalTo(wself.vodTopImageView.mas_left).offset(-5*kRateSize+kTopSafeSpace);
        make.width.mas_equalTo(55*kRateSize);
        make.height.mas_equalTo(44*kRateSize);
    }];
    
    
    [_screenLockedVODIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.mas_left).offset(kTopSafeSpace);
        make.centerY.mas_equalTo(wself.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize
                                         , 30*kRateSize
                                         ));
    }];
    
    [_lockedVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(7.5*kRateSize
                                     );
                make.left.mas_equalTo(7.5*kRateSize
                                      ).offset(0);
                make.size.mas_equalTo(CGSizeMake(20*kRateSize
                                                 , 20*kRateSize
                                                 ));
        
    }];

    
    [_shareVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.vodTopImageView.mas_right).offset(0*kRateSize-kTopSafeSpace
                                                                );
                make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(27*kRateSize
        );
        make.size.mas_equalTo(CGSizeMake(42*kRateSize
                                         , 45*kRateSize
                                         ));

    }];
    
    
    [_favorVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_shareVODBtn.mas_left).offset(0*kRateSize
                                                             );
        make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(27*kRateSize
        );
        make.size.mas_equalTo(CGSizeMake(42*kRateSize
                                         , 45*kRateSize
                                         ));

    }];
    
    [_videoListVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_favorVODBtn.mas_left);
                make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(27*kRateSize
        );
//        make.centerY.mas_equalTo(wself.vodTopImageView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize
                                         , 45*kRateSize
                                         ));

    }];
    
    
    
    
    [_vodBottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.height.mas_equalTo(60*kRateSize
                                );
        make.width.mas_equalTo(wself.mas_width);
    }];
    
    
    [_startVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-3*kRateSize);
        make.left.mas_equalTo(wself.vodBottomImageView.mas_left).offset(kTopSafeSpace);
        make.width.mas_equalTo(39*kRateSize);
        make.height.mas_equalTo(39*kRateSize);

    }];
    
    
    [_backwardVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-3*kRateSize
                                                                     );
        make.left.mas_equalTo(_startVODBtn.mas_right);
        make.width.mas_equalTo(39*kRateSize
                               );
        make.height.mas_equalTo(39*kRateSize
                                );

    }];
    
    
    
    [_fullScreenVODBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.vodBottomImageView.mas_right).offset(0*kRateSize);
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-4*kRateSize);
        make.width.mas_equalTo(35*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
    }];
    
    [self.currentVODTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_backwardVODBtn.mas_right);
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-11*kRateSize);
        make.width.mas_equalTo(60*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
        

    }];
    
    
    
    [self.resolutionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-11*kRateSize);//zzsc
        make.right.mas_equalTo(wself.vodBottomImageView.right).offset(-15*kRateSize-kTopSafeSpace);//zzsc
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
        
    }];
    
    
    
    [self.totalVODTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.resolutionBtn.mas_left).offset(0*kRateSize);
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-11*kRateSize);
        make.width.mas_equalTo(60*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
    }];
    
    [_progressVODView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom).offset(-19*kRateSize);
        make.left.mas_equalTo(_currentVODTimeLabel.mas_right).offset(10*kRateSize);
        make.right.mas_equalTo(_totalVODTimeLabel.mas_left).offset(-10*kRateSize);//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.height.mas_equalTo(2*kRateSize);
    }];
    
    [_videoVODSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(_currentVODTimeLabel.mas_right).offset(10*kRateSize);
        make.right.mas_equalTo(_totalVODTimeLabel.mas_left).offset(-10*kRateSize);
        
        make.centerY.mas_equalTo(wself.progressVODView.mas_centerY);
        make.height.mas_equalTo(16*kRateSize);

    }];
    
    
    [_videoListVODView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.vodTopImageView.mas_top).offset(10*kRateSize);
        make.width.mas_equalTo(139*kRateSize
                               );
        make.bottom.mas_equalTo(wself.vodBottomImageView.mas_bottom);
        make.right.mas_equalTo(wself.mas_right);
    }];

    

    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY);
        make.height.mas_equalTo(70*kRateSize);
        make.width.mas_equalTo(130*kRateSize);
        
    }];
    
    
    
    [_progressDirectionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_progressTimeView.mas_centerY).offset(-15*kRateSize);
        make.centerX.mas_equalTo(_progressTimeView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
    }];
    
    
    
    
    [_progressTimeLable_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.left.mas_equalTo(wself.progressTimeView.mas_left).offset(5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_progressTimeLable_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.right.mas_equalTo(wself.progressTimeView.mas_right).offset(-5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //xiaoxiugai
        make.top.mas_equalTo(wself.mas_top).offset(isAfterIOS6A?0: 0);
        make.left.mas_equalTo(wself.mas_left);
        make.width.mas_equalTo(wself);
        make.height.mas_equalTo(wself);
        make.bottom.mas_equalTo(wself.mas_bottom);
        
        
    }];

    
    
    //亮度视图尺寸****************
    
    [_brightnessVODView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessVODProgress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessVODView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessVODView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];

    
    [_loadingMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(wself);
        
    }];
    
    
    CGSize loadingSize = CGSizeMake(200*kRateSize, 60*kRateSize);
    [_polyMedicineView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(loadingSize);
        make.centerX.mas_equalTo(wself.centerX).offset(30*kRateSize);
        make.centerY.mas_equalTo(wself.centerY);
        
    }];
    

    
}



#pragma mark tableViewDelegate


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return _asscciationArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 37*kRateSize
        ;
   
}




//-(BOOL)isCurrentContentIDIndexFromRecommendArray:(NSMutableArray*)recommendArray WithContentID:(NSString *)contentID
//{
//    for (int i = 0; i<recommendArray.count; i++) {
//        
//        NSMutableDictionary *recommendDic = [[NSMutableDictionary alloc]init];
//        
//        recommendDic = [recommendArray objectAtIndex:i];
//        
//        
//        if ([contentID isEqualToString:[recommendDic objectForKey:@"id"]]) {
//            
//            return YES;
//            break;
//            
//        }
//    }
//    
//    return NO;
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        DemandVideoListCell *cell=[tableView dequeueReusableCellWithIdentifier:kMASCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[DemandVideoListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMASCellReuseIdentifier];
            
        }

        cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    if (IS_RECOMMENDARRAY == YES) {
        
        
        if (_asscciationArray.count>0) {
            NSDictionary *videoDic = [[NSDictionary alloc]initWithDictionary:[_asscciationArray objectAtIndex:indexPath.row]];
            
            if ([_contentID isEqualToString:[[_asscciationArray objectAtIndex:indexPath.row] objectForKey:@"id"]]) {
                
                cell.videoTitleLabel.textColor = App_selected_color;
            }else
            {
                cell.videoTitleLabel.textColor = [UIColor whiteColor];
            }
            
            
            cell.videoTitleLabel.text = [videoDic objectForKey:@"name"];
            
        }

        
    }else
    {
    
        if (_asscciationArray.count>0) {
            NSDictionary *videoDic = [[NSDictionary alloc]initWithDictionary:[_asscciationArray objectAtIndex:indexPath.row]];
            
            cell.videoTitleLabel.text = [videoDic objectForKey:@"name"];
     
        }
    }
    
        
        return cell;
        
        
   
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"点击了%ld",(long)indexPath.row);
        
        NSDictionary *videoDic = [[NSDictionary alloc]init];
        videoDic =  [_asscciationArray objectAtIndex:indexPath.row];
        _demandModel.seriesList = [[NSMutableArray alloc]init];
    _demandModel.seriesList = _asscciationArray;
    NSLog(@"获取的数据是%@",[[_asscciationArray objectAtIndex:indexPath.row] objectForKey:@"name"]);
            if (![[self.demandModel.seriesList objectAtIndex:indexPath.row] objectForKey:@"name"]) {
                _serviceTitleVODLabel.text = @"";
            }
    
dispatch_async(dispatch_get_main_queue(), ^{
    if ([self.delegate respondsToSelector:@selector(videoListIndexPathAssociateArr:andIndexPathRow:)]) {
        [self.delegate videoListIndexPathAssociateArr:_asscciationArray andIndexPathRow:indexPath.row];
    }

});

    
}





-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}





#pragma mark - 重置控制器代码



/** 多码率重置ControlView */
-(void)resetControlViewForResolution
{
    
    self.resolutionView.hidden  = YES;
    self.backgroundColor        = [UIColor clearColor];

}



/** 重置ControlView */
- (void)resetControlView
{
    self.videoVODSlider.value      = 0;
    self.progressVODView.progress  = 0;
    self.currentVODTimeLabel.text  = @"00:00:00";
    self.totalVODTimeLabel.text    = @"00:00:00";
//    self.horizontalLabel.hidden = YES;
//    self.repeatBtn.hidden       = YES;
    self.resolutionView.hidden  = YES;
    self.backgroundColor        = [UIColor clearColor];
//    self.downLoadBtn.enabled    = YES;
}


#pragma  mark  是否是全屏的信息条显示隐藏

- (void)showControlViewAndIsFullScreen:(BOOL)isFullScreen
{
    [[UIApplication sharedApplication]setStatusBarHidden:NO];

    if (_currentPlayType != CurrentPlayVod) {
        _epgTopImageView.hidden    = NO;
        _epgBottomImageView.hidden = NO;
        _currentVODTimeLabel.hidden = NO;
        _vodBottomImageView.hidden = YES;
        
        
    }else
    {
        _epgTopImageView.hidden    = YES;
        _epgBottomImageView.hidden = YES;
        _vodTopImageView.hidden    = NO;
        _vodBottomImageView.hidden = NO;
        
//        _videoListVODView.hidden = NO;
        if (isFullScreen == YES) {
            _lockedVODBtn.hidden    = NO;
            _favorVODBtn.hidden = NO;
            _shareVODBtn.hidden = NO;
            _videoListVODBtn.hidden = NO;
            _fullScreenVODBtn.hidden = YES;

        }else
        {
            _lockedVODBtn.hidden    = YES;
            _favorVODBtn.hidden = YES;
            _shareVODBtn.hidden = YES;
            _videoListVODBtn.hidden = YES;
            _fullScreenVODBtn.hidden = YES;

        }
        
        

    }

//    self.lockBtn.alpha         = 1;
}

- (void)hideControlViewAndIsFullScreen:(BOOL)isFullScreen
{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];

    _epgTopImageView.hidden    = YES;
    _epgBottomImageView.hidden = YES;
    _vodTopImageView.hidden    = YES;
    _vodBottomImageView.hidden = YES;
    _videoListVODBtn.hidden = YES;
    _favorVODBtn.hidden = YES;
    _shareVODBtn.hidden = YES;
    _videoListVODView.hidden = YES;
    _videoListVODTV.hidden = YES;
    
    if (_lockedVODBtn.selected == YES) {
        _lockedVODBtn.hidden = NO;
    }else
    {
        _lockedVODBtn.hidden = YES;
    }

    
    
//    _videoListVODView.hidden = YES;
//    self.lockBtn.alpha         = 0;
    // 隐藏resolutionView
    self.resolutionBtn.selected = YES;
    [self resolutionAction:self.resolutionBtn];
    

}


-(UIView*)adaptView:(UIView*)adaptView andForFaterView:(UIView*)fatherView andEadge:(UIEdgeInsets*)eadge
{
 
    
    if (KDeviceHeight==812) {
        [adaptView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(fatherView.mas_left);
            make.width.mas_equalTo(fatherView.mas_width);
            make.top.mas_equalTo(fatherView.mas_top);
            make.height.mas_equalTo(fatherView.mas_height);
        }];
    }
    
    return adaptView;
}




@end
