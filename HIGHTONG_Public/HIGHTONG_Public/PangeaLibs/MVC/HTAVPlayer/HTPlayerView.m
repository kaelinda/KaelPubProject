 //
//  HTPlayerView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTPlayerView.h"
#import "PolyMedicineLoadingView.h"
#import "DateTools.h"
#import "SystermTimeControl.h"
#import "SAIInformationManager.h"
#import "HTTimerManager.h"
#import "UserLoginViewController.h"
#import "LocalCollectAndPlayRecorder.h"
#import "HTVolumeUtil.h"
//**自动隐藏信息条的计时器*/


//// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, HTPanDirection){
    HTPanDirectionHorizontalMoved, //横向移动
    HTPanDirectionVerticalMoved    //纵向移动
};

//播放器的几种状态
typedef NS_ENUM(NSInteger, HTPlayerState) {
    HTPlayerStateFailed,     // 播放失败
    HTPlayerStateBuffering,  //缓冲中
    HTPlayerStatePlaying,    //播放中
    HTPlayerStateStopped,    //停止播放
    HTPlayerStatePause       //暂停播放
};



@interface HTPlayerView () <UIGestureRecognizerDelegate,UIAlertViewDelegate,HTTimerManagerDelegate>
{

    UITapGestureRecognizer *oneTap;
    NSDate *startDate;
    BOOL _isCollectionInfo;


}
@property (strong,nonatomic)  HTTimerManager *timeManager;
@property (strong,nonatomic)  NSString      * watchDuration;//采集 观看时长
/** 播放属性 */
@property (nonatomic, strong) AVPlayer            *player;
/** 播放属性 */
@property (nonatomic, strong) AVPlayerItem        *playerItem;
/** playerLayer */
@property (nonatomic, strong) AVPlayerLayer       *playerLayer;
/** 滑杆 */
@property (nonatomic, strong) UISlider            *volumeViewSlider;
/** 计时器 */
@property (nonatomic, strong) NSTimer             *timer;
/** 用来保存快进的总时长 */
@property (nonatomic, assign) CGFloat             sumTime;
/** 定义一个实例变量，保存枚举值 */
@property (nonatomic, assign) PanDirection        panDirection;
/** 播发器的几种状态 */
@property (nonatomic, assign) HTPlayerState       state;
/** 是否在调节音量*/
@property (nonatomic, assign) BOOL                isVolume;

/** 是否播放本地文件 */
@property (nonatomic, assign) BOOL                isLocalVideo;
/** slider上次的值 */
@property (nonatomic, assign) CGFloat             sliderVODLastValue;
/** 是否再次设置URL播放视频 */
@property (nonatomic, assign) BOOL                repeatToPlay;
/** 播放完了*/
@property (nonatomic, assign) BOOL                playDidEnd;
/** 进入后台*/
@property (nonatomic, assign) BOOL                didEnterBackground;

//#pragma mark - UITableViewCell PlayerView
//
///** palyer加到tableView */
//@property (nonatomic, strong) UITableView         *tableView;
///** player所在cell的indexPath */
//@property (nonatomic, strong) NSIndexPath         *indexPath;
///** cell上imageView的tag */
//@property (nonatomic, assign) NSInteger           cellImageViewTag;
///** ViewController中页面是否消失 */
//@property (nonatomic, assign) BOOL                viewDisappear;
///** 是否在cell上播放video */
//@property (nonatomic, assign) BOOL                isCellVideo;
///** 是否缩小视频在底部 */
//@property (nonatomic, assign) BOOL                isBottomVideo;
///** 是否切换分辨率*/
//@property (nonatomic, assign) BOOL                isChangeResolution;

@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;




@end




@implementation HTPlayerView
/**
 *  单例，用于列表cell上多个视频
 *
 *  @return HTPlayer
 */
+ (instancetype)sharedPlayerView
{
    static HTPlayerView *playerView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        playerView = [[HTPlayerView alloc] init];
        playerView.userInteractionEnabled = YES;
        
    });
    return playerView;
}



/**
 *  带初始化调用此方法
 */
- (instancetype)init
{
    self = [super init];
    if (self) {
        if (_timeManager) {
            _timeManager = nil;
        }
        _timeManager = [[HTTimerManager alloc]init];
        _timeManager.delegate = self;
        
        _watchDuration = nil;
        
        
        _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
        
        if (_errorManager) {
            _errorManager = nil;
        }
        _errorManager = [[PlayerErrorManager alloc]init];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_StopPlayer) name:@"MSG_STOPPLAYER" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PausePlayer) name:@"MSG_PAUSEPLAYER" object:nil];

        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PlayPlayer) name:@"MSG_PLAYPLAYER" object:nil];

    }
    return self;
}

-(void)receiveMsg_StopPlayer
{
    [self backButtonAction];
}

-(void)receiveMsg_PausePlayer
{
    [self actionPauseForVideo];
}

-(void)receiveMsg_PlayPlayer
{
    [self actionPlayForVideo];
}




#pragma mark - 设置横竖屏的代码
/**
 *  设置横屏的约束
 */
- (void)setOrientationLandscape
{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];

}

/**
 *  设置竖屏的约束
 */
- (void)setOrientationPortrait
{
    __weak __typeof(&*self)weakSelf = self;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    [_controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(weakSelf);
    }];

    
}

#pragma mark - 控制横竖屏的BOOL值

/*
 控制横竖屏
 */
-(void)setIsConstraintLandscape:(BOOL)isConstraintLandscape
{
    _isConstraintLandscape = isConstraintLandscape;
    if (isConstraintLandscape == YES) {
        [self setOrientationLandscape];
//        self.isFullScreen = YES;
        [self setIsFullScreen:YES];
    }else
    {
//        self.isFullScreen = NO;
        [self setIsFullScreen:NO];
        [self setOrientationPortrait];
    }
    
   
    
}

#pragma mark - 1.0.0推荐的时候需要playType

/*
 1.0.0推荐的时候需要playType
 */
-(void)setCurrentPlayType:(CurrentPLAYType)currentPlayType
{
    _currentPlayType = currentPlayType;
    
    _controlView = [[HTPlayerControlView alloc] init];
    _controlView.delegate = self;
    _controlView.currentPlayType = _currentPlayType;
    [self addSubview:_controlView];
    [_controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(self);
    }];

}

#pragma mark setter方法推荐数组/信息条title/传过来的字典PolyMedicDic/没有传字典contentID/设置全屏/是否展示上下信息条

/*
 推荐的数组
 */
-(void)setAsscciationArray:(NSMutableArray *)asscciationArray
{
    
    if (IS_RECOMMENDARRAY == YES) {
        _asscciationArray = asscciationArray;
        _controlView.asscciationArray = [[NSMutableArray alloc]init];

    }else
    {
    
    if (_controlView.asscciationArray) {
        _controlView.asscciationArray = nil;
    }
    _controlView.asscciationArray = [[NSMutableArray alloc]init];
    }
    [_controlView setAsscciationArray:asscciationArray];
    [_controlView.videoListVODTV reloadData];
}




-(void)setPolyModel:(HealthVodCollectionModel *)polyModel
{
    
    if (_polyModel) {
        _polyModel = nil;
        
    }
    _polyModel = polyModel;
    _polyModel.watchTime = [SystermTimeControl getNowServiceTimeString];

}

/*
 传过来的titleStr，进行信息条title的展现
 */
-(void)setContentTitleStr:(NSString *)contentTitleStr
{
    if (_contentTitleStr) {
        _contentTitleStr = nil;
    }
    _contentTitleStr = contentTitleStr;
    _controlView.serviceTitleVODLabel.text = _contentTitleStr;
}


/*
 传过来的字典进行确定contentID
 */
-(void)setPolyMedicDic:(NSMutableDictionary *)polyMedicDic
{
    if (_polyMedicDic) {
        _polyMedicDic = nil;
    }
    _polyMedicDic = [NSMutableDictionary dictionary];
    _polyMedicDic = polyMedicDic;
    
    
    if (IS_RECOMMENDARRAY == YES) {
        [self getContentIDActonPlay:[polyMedicDic objectForKey:@"id"]];
        
    }else
    {
    
    if (isUsePolymedicine_2_0) {
        
        [self getContentIDActonPlay:[polyMedicDic objectForKey:@"id"]];

    }else
    {
    [self getContentIDActonPlay:[polyMedicDic objectForKey:@"videoId"]];
    }
        
    }
    _polyMedicType = [polyMedicDic objectForKey:@"type"];
    
    
}

/*
 如果没有传字典，就用传过来的contentID进行播放
 */
-(void)setContentID:(NSString *)contentID
{
    if (_contentID) {
        _contentID = nil;
    }
    
    if (IS_RECOMMENDARRAY == YES) {
        
        _contentID = contentID;
        [self getContentIDActonPlay:_contentID];

    }else
    {
    
    _contentID = [contentID copy];
    if (isUsePolymedicine_2_0) {
        
        [self getContentIDActonPlay:_contentID];
        
    }else
    {
        [self getContentIDActonPlay:_contentID];
    }
    }

}


-(int)isCurrentContentIDIndexFromRecommendArray:(NSMutableArray*)recommendArray withContentID:(NSString *)contentID
{
    int currentIndex = 0;
    
    for (int i = 0; i<recommendArray.count; i++) {
        
        NSDictionary *recommendDic = [[NSMutableDictionary alloc]init];
        
        recommendDic = [recommendArray objectAtIndex:i];
        
        
        if ([contentID isEqualToString:[recommendDic objectForKey:@"id"]]) {
            
            currentIndex = i;
            break;
 
        }
        
    }
    
    return currentIndex;
}


/*设置是否全屏代码*/

-(void)setIsFullScreen:(BOOL)isFullScreen
{
    _isFullScreen = isFullScreen;

}


/*设置*/

-(void)setIsMaskShowing:(BOOL)isMaskShowing
{
    
    _isMaskShowing = isMaskShowing;
    
    if (_isMaskShowing == YES) {;
        NSLog(@"展示上下进度条");
        [_controlView showControlViewAndIsFullScreen:_isFullScreen];
        
        [self reStartHiddenTimer];
        
    }else
    {
        NSLog(@"不展示上下进度条");
        [_controlView hideControlViewAndIsFullScreen:_isFullScreen];
    }
    
    
    
    
}


/*
 赋值videoURL的setter方法
 */

-(void)setVideoURL:(NSURL *)videoURL
{
    _videoURL = videoURL;
    //    _videoURL = [NSURL URLWithString:@"http://192.168.3.24:8106/MP4/19bd24d4bfe94290b03b94b7e17028db/index.mp4"];
    _controlView.polyMedicineView.hidden = NO;
    _controlView.loadingMaskView.hidden = YES;
    self.repeatToPlay = NO;
    self.playDidEnd   = NO;
    // 添加通知声音什么的
    _errorManager.startErrorDate = [SystermTimeControl getNowServiceTimeDate];
    [self addNotifications];
    // 设置Player相关参数
    [self configHTPlayer];
    
    //剧集按钮
   

    
    
    
}




#pragma mark 获取到contentID进行播放器的播放

/*
 获取到contentID进行播放
 */
-(void)getContentIDActonPlay:(NSString *)contentID
{
    // 播放开始之前（加载中）设置站位图
    if (_contentID) {
        _contentID = nil;
    }
    _contentID = contentID;
    
//    _controlView.contentID = _contentID;
    [_controlView setContentID:_contentID];
//    _polyModel.hpid = _contentID;
    _polyModel.itemId = _contentID;
    [self addNotifications];
    
    
    
    
    if (IS_RECOMMENDARRAY == YES) {
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        [request YJHealthyVodMediaPlayListWithProgramId:_contentID withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];

    }else
    {
    
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    if (isUsePolymedicine_2_0) {
        
        [request YJHealthyVodMediaPlayListWithProgramId:_contentID withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];
    }else
    {
        [request YJCdnVideoUrlWithInstanceCode:@"" withMobile:@"" withVideoId:_contentID];
    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        
        
//        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
        if (isUsePolymedicine_2_0) {
            
            [request YJHealthyVodRecommendProgramWithProgramId:_contentID withMaxCount:@"20"];
            
        }else
        {
            [request YJCdnVideoRecommendWithInstanceCode:KInstanceCode WithVideoId:[[_asscciationArray objectAtIndex:0] objectForKey:@"videoId"] withType:[[_asscciationArray objectAtIndex:0] objectForKey:@"type"] withMaxCount:@"50"];
        }
//    });

    }
}




-(void)reStartHiddenTimer
{
    self.autoHiddenSecend = 5;
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
    self.autoHiddenTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(autoHiddenPanel:) userInfo:nil repeats:YES];
}





- (void) autoHiddenPanel:(NSTimer *)sender
{
    if (self.autoHiddenSecend == 0) {
        [self hiddenPanlwithBool:YES];
    } else {
        self.autoHiddenSecend --;
    }
}



- (void) hiddenPanlwithBool:(BOOL) bol
{
    if (bol) {
//        [UIView animateWithDuration:0.1 animations:^{
//            [_controlView hideControlView];
//        }];
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
        self.isMaskShowing = NO;

    } else {
        self.isMaskShowing = YES;
//        [_controlView showControlView];
//        [self reStartHiddenTimer];
        
    }
}

#pragma mark 设置播放器AVPlayer的相关参数

/**
 *  设置Player相关参数
 */
- (void)configHTPlayer
{
    // 初始化playerItem
    self.playerItem  = [AVPlayerItem playerItemWithURL:_videoURL];
    
    // 每次都重新创建Player，替换replaceCurrentItemWithPlayerItem:，该方法阻塞线程
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    
    // 初始化playerLayer
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    
    // 此处为默认视频填充模式
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    // 添加playerLayer到self.layer
    [self.layer insertSublayer:self.playerLayer atIndex:0];
    
    // 初始化显示controlView为YES
    self.isMaskShowing = YES;
    // 延迟隐藏controlView
    //    [self autoFadeOutControlBar];
    
    // 计时器
    [self createTimer];
    
    // 添加手势
    
    // 获取系统音量
    //    [self configureVolume];
    
    // 本地文件不设置HTPlayerStateBuffering状态
    //    if ([self.videoURL.scheme isEqualToString:@"file"]) {
    //        self.state = HTPlayerStatePlaying;
    //        self.isLocalVideo = YES;
    //        self.controlView.downLoadBtn.enabled = NO;
    //    } else {
    //        self.state = HTPlayerStateBuffering;
    //        self.isLocalVideo = NO;
    //    }
    
    //    self.state = HTPlayerStateBuffering;
    
    // 开始播放
    [self play];
    
    self.controlView.startVODBtn.selected = YES;
    self.isPauseByUser                 = NO;
    //采集   初始化服务器的时间
        //over

    // 强制让系统调用layoutSubviews 两个方法必须同时写
    [self setNeedsLayout]; //是标记 异步刷新 会调但是慢
    [self layoutIfNeeded]; //加上此代码立刻刷新
}




/**
 *  重置player
 */
- (void)resetPlayer
{
    // 改为为播放完
    self.playDidEnd         = NO;
    self.playerItem         = nil;
    self.didEnterBackground = NO;
    // 视频跳转秒数置0
    self.seekTime           = 0;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];

       //    [self interfaceOrientation:UIInterfaceOrientationPortrait];
    // 移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // 关闭定时器
    [self.timer invalidate];
    // 暂停
    [self pause];
    // 移除原来的layer
    [self.playerLayer removeFromSuperlayer];
    // 替换PlayerItem为nil
    [self.player replaceCurrentItemWithPlayerItem:nil];
    // 把player置为nil
    self.player = nil;
    if (self.isChangeResolution) { // 切换分辨率
        [self.controlView resetControlViewForResolution];
        self.isChangeResolution = NO;
    }else { // 重置控制层View
        [self.controlView resetControlView];
    }
    
}




/**
 *  创建timer
 */
- (void)createTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(playerTimerAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}



#pragma mark - 设置播放器AVPlayer的KVO监听
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.player.currentItem) {
        if ([keyPath isEqualToString:@"status"]) {
            if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
                _controlView.startVODBtn.enabled = YES;
                _errorManager.isAVLoading = YES;
                self.state = HTPlayerStatePlaying;
                // 加载完成后，再添加平移手势
                // 添加平移手势，用来控制音量、亮度、快进快退
                
                [_timeManager start];
                _isCollectionInfo = YES;
                _pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panDirection:)];
                _pan.delegate                = self;
                [self addGestureRecognizer:_pan];
                _controlView.polyMedicineView.hidden = YES;
                // 跳到xx秒播放视频
                if (self.seekTime) {
                    [self seekToTime:self.seekTime completionHandler:nil];
                }
                
            } else if (self.player.currentItem.status == AVPlayerItemStatusFailed){
                
                self.state = HTPlayerStateFailed;
                _errorManager.isAVLoading = YES;
                NSError *error = [self.player.currentItem error];
                NSLog(@"视频加载失败===%@",error.description);
                //                self.controlView.horizontalLabel.hidden = NO;
                //                self.controlView.horizontalLabel.text = @"视频加载失败";
                [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_InterNet andAVName:_polyModel.name andOPSCode:PlayerAVError andOPSST:[SystermTimeControl getNowServiceTimeString]];
                
            }
            
            
            _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
            [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_InterNet andAVName:_polyModel.name andOPSCode:PlayerOutTimeError andOPSST:[SystermTimeControl getNowServiceTimeString]];
            
        } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
            _errorManager.isAVLoading = NO;
            // 计算缓冲进度
            NSTimeInterval timeInterval = [self availableDuration];
            CMTime duration             = self.playerItem.duration;
            CGFloat totalDuration       = CMTimeGetSeconds(duration);
            [self.controlView.progressVODView setProgress:timeInterval / totalDuration animated:NO];
            
            // 如果缓冲和当前slider的差值超过0.1,自动播放，解决弱网情况下不会自动播放问题
            if (!self.isPauseByUser && !self.didEnterBackground && (self.controlView.progressVODView.progress-self.controlView.videoVODSlider.value > 0.05)) { [self play]; }
            
        } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
            
            // 当缓冲是空的时候
            if (self.playerItem.playbackBufferEmpty) {
                self.state = HTPlayerStateBuffering;
                [self bufferingSomeSecond];
            }
            
        } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
            
            // 当缓冲好的时候
            if (self.playerItem.playbackLikelyToKeepUp && self.state == HTPlayerStateBuffering){
                self.state = HTPlayerStatePlaying;
            }
            
        }
    }
}


/**
 *  从xx秒开始播放视频跳转
 *
 *  @param dragedSeconds 视频跳转的秒数
 */
- (void)seekToTime:(NSInteger)dragedSeconds completionHandler:(void (^)(BOOL finished))completionHandler
{
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        // seekTime:completionHandler:不能精确定位
        // 如果需要精确定位，可以使用seekToTime:toleranceBefore:toleranceAfter:completionHandler:
        // 转换成CMTime才能给player来控制播放进度
        CMTime dragedCMTime = CMTimeMake(dragedSeconds, 1);
        [self.player seekToTime:dragedCMTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
            // 视频跳转回调
            if (completionHandler) { completionHandler(finished); }
            // 如果点击了暂停按钮
            if (self.isPauseByUser) return ;
            [self play];
            if (!self.playerItem.isPlaybackLikelyToKeepUp && !self.isLocalVideo) {
                self.state = HTPlayerStateBuffering;
            }
            
        }];
    }
}

#pragma mark - 播放器的Setter方法

/**
 *  设置播放的状态
 *
 *  @param state HTPlayerState
 */
- (void)setState:(HTPlayerState)state
{
    _state = state;
    if (state == HTPlayerStatePlaying) {
        // 改为黑色的背景，不然站位图会显示
        UIImage *image = [self buttonImageFromColor:[UIColor blackColor]];
        self.layer.contents = (id) image.CGImage;
    } else if (state == HTPlayerStateFailed) {
        //        self.controlView.downLoadBtn.enabled = NO;
    }
    // 控制菊花显示、隐藏
    
    if (state == HTPlayerStateBuffering) {
        
        _controlView.loadingMaskView.hidden = NO;
    }else
    {
        _controlView.loadingMaskView.hidden = YES;
        
    }
    
    
}


/**
 *  计算缓冲进度
 *
 *  @return 缓冲进度
 */
- (NSTimeInterval)availableDuration {
    NSArray *loadedTimeRanges = [[_player currentItem] loadedTimeRanges];
    CMTimeRange timeRange     = [loadedTimeRanges.firstObject CMTimeRangeValue];// 获取缓冲区域
    float startSeconds        = CMTimeGetSeconds(timeRange.start);
    float durationSeconds     = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result     = startSeconds + durationSeconds;// 计算缓冲总进度
    return result;
}


#pragma mark - 缓冲较差时候

/**
 *  缓冲较差时候回调这里
 */
- (void)bufferingSomeSecond
{
    self.state = HTPlayerStateBuffering;
    // playbackBufferEmpty会反复进入，因此在bufferingOneSecond延时播放执行完之前再调用bufferingSomeSecond都忽略
    __block BOOL isBuffering = NO;
    if (isBuffering) return;
    isBuffering = YES;
    
    // 需要先暂停一小会之后再播放，否则网络状况不好的时候时间在走，声音播放不出来
    [self pause];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        // 如果此时用户已经暂停了，则不再需要开启播放了
        if (self.isPauseByUser) {
            isBuffering = NO;
            return;
        }
        
        [self play];
        // 如果执行了play还是没有播放则说明还没有缓存好，则再次缓存一段时间
        isBuffering = NO;
        if (!self.playerItem.isPlaybackLikelyToKeepUp) { [self bufferingSomeSecond]; }
        
    });
}

/**
 *  根据playerItem，来添加移除观察者
 *
 *  @param playerItem playerItem
 */
- (void)setPlayerItem:(AVPlayerItem *)playerItem
{
    if (_playerItem == playerItem) {return;}
    
    if (_playerItem) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_playerItem];
        [_playerItem removeObserver:self forKeyPath:@"status"];
        [_playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
        [_playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [_playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    }
    _playerItem = playerItem;
    if (playerItem) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        [playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
        [playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
        // 缓冲区空了，需要等待数据
        [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
        // 缓冲区有足够数据可以播放了
        [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    }
}


#pragma mark - 计时器事件
/**
 *  计时器事件
 */
- (void)playerTimerAction
{
    if (_playerItem.duration.timescale != 0) {
        self.controlView.videoVODSlider.value     = (CMTimeGetSeconds([_playerItem currentTime]) / (_playerItem.duration.value / _playerItem.duration.timescale))*1000;//当前进度
        
        self.controlView.currentVODTimeLabel.text = [self durationStringWithTime:(int)CMTimeGetSeconds([_player currentTime])];
        self.controlView.totalVODTimeLabel.text   = [self durationStringWithTime:(int)_playerItem.duration.value / _playerItem.duration.timescale];
        
        
    }
}


#pragma mark - 手势相关的代码

/**
 *  创建手势
 */
- (void)createGesture
{
    // 单击
    oneTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    [self.controlView addGestureRecognizer:oneTap];
    
}






#pragma mark - 手势Action

/**
 *   轻拍方法
 *
 *  @param gesture UITapGestureRecognizer
 */
- (void)tapAction:(UITapGestureRecognizer *)gesture
{
    if (_controlView.lockedVODBtn.selected == YES) {
        return;
    }else
    {
    
    
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        
        self.isMaskShowing = !self.isMaskShowing;
    
    }
        
    }
}





#pragma mark - layoutSubviews

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.playerLayer.frame = self.bounds;
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    // 只要屏幕旋转就显示控制层
//    self.isMaskShowing = YES;
       // fix iOS7 crash bug
    [self layoutIfNeeded];
}




/**
 *  播放
 */
- (void)play
{
    [_player play];
}

/**
 * 暂停
 */
- (void)pause
{
    [_player pause];
}

#pragma mark - 观察者、通知

/**
 *  添加观察者、通知
 */
- (void)addNotifications
{
    [self createGesture];
    //插拔耳机
    // 使用这个category的应用不会随着手机静音键打开而静音，可在手机静音下播放声音
    NSError *setCategoryError = nil;
    BOOL success = [[AVAudioSession sharedInstance]
                    setCategory: AVAudioSessionCategoryPlayback
                    error: &setCategoryError];
    
    if (!success) { /* handle the error in setCategoryError */ }
    
    // 监听耳机插入和拔掉通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:) name:AVAudioSessionRouteChangeNotification object:nil];
    
    //**************************////////////////////////////////
    // app退到后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayGround) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // slider开始滑动事件
    [self.controlView.videoVODSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
    // slider滑动中事件
    [self.controlView.videoVODSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    // slider结束滑动事件
    [self.controlView.videoVODSlider addTarget:self action:@selector(progressSliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchUpOutside];
//
//    // 播放按钮点击事件
    self.controlView.startVODBtn.enabled = YES;
    [self.controlView.startVODBtn addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.controlView.lockedVODBtn addTarget:self action:@selector(lockedVODBtnAction:) forControlEvents:UIControlEventTouchUpInside];
   
    // 返回按钮点击事件
    [self.controlView.backVODBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
//    // 全屏按钮点击事件
    [self.controlView.fullScreenVODBtn addTarget:self action:@selector(fullScreenAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.controlView.shareVODBtn addTarget:self action:@selector(shareButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.controlView.favorVODBtn addTarget:self action:@selector(favorButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.controlView.videoListVODBtn addTarget:self action:@selector(videoListButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.controlView.backwardVODBtn addTarget:self action:@selector(backwardButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
//    // 锁定屏幕方向点击事件
//    [self.controlView.lockBtn addTarget:self action:@selector(lockScreenAction:) forControlEvents:UIControlEventTouchUpInside];
//    // 重播
//    [self.controlView.repeatBtn addTarget:self action:@selector(repeatPlay:) forControlEvents:UIControlEventTouchUpInside];
//    // 下载
//    [self.controlView.downLoadBtn addTarget:self action:@selector(downloadVideo:) forControlEvents:UIControlEventTouchUpInside];
    __weak typeof(self) weakSelf = self;
    // 切换分辨率
    self.controlView.resolutionBlock = ^(UIButton *button) {
        // 记录切换分辨率的时刻
        NSInteger currentTime = (NSInteger)CMTimeGetSeconds([weakSelf.player currentTime]);
        
        NSString *videoStr = @"";
        if (isUsePolymedicine_2_0) {
            
            videoStr =[weakSelf.videoURLArray[button.tag-200] objectForKey:@"playLink"];
        }else
        {
            videoStr =[weakSelf.videoURLArray[button.tag-200] objectForKey:@"url"];
        }
        
        NSURL *videoURL = [NSURL URLWithString:videoStr];
        if ([videoURL isEqual:weakSelf.videoURL]) {
           //如果请求的还是和之前一样的话，则return
            return ;
        }
        weakSelf.isChangeResolution = YES;
        // reset player
        [weakSelf resetToPlayNewURL];
        weakSelf.videoURL = videoURL;
        // 从xx秒播放
        weakSelf.seekTime = currentTime;
        
    };
//    [_controlView.videoListVODView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"adPlayerView"];
}



/**
 *  在当前页面，设置新的Player的URL调用此方法
 */
- (void)resetToPlayNewURL
{
    self.repeatToPlay = YES;
    [self resetPlayer];
}


/**
 *  耳机插入、拔出事件
 */
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            NSLog(@"Headphone/Line plugged in");
            
            _state = HTPlayerStatePlaying;
            _controlView.startVODBtn.selected = YES;
            [self play];
            
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            
            _state = HTPlayerStatePause;
            _controlView.startVODBtn.selected = NO;

            [self pause];

            
            NSLog(@"Headphone/Line was pulled. Stopping player....");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }}


#pragma mark 播放暂停按钮事件

/**
 *  播放、暂停按钮事件
 *
 *  @param button UIButton
 */
- (void)startAction:(UIButton *)button
{
    button.selected    = !button.selected;
    self.isPauseByUser = !button.isSelected;
    if (_timeManager.counting) {
        [_timeManager pause];
    }else
    {
        [_timeManager start];
    }

    if (button.selected) {
        [self play];
//        [_controlView.startVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal"] forState:UIControlStateNormal];

        self.isPauseByUser = NO;
        if (self.state == HTPlayerStatePause) { self.state = HTPlayerStatePlaying; }
    } else {
        [self pause];
//        [_controlView.startVODBtn setImage:[UIImage imageNamed:@"btn_player_controller_play_normal"] forState:UIControlStateNormal];

        self.isPauseByUser = YES;
        if (self.state == HTPlayerStatePlaying) { self.state = HTPlayerStatePause;}
    }
}

#pragma mark 锁屏按钮事件


-(void)lockedVODBtnAction:(UIButton*)sender
{

    if (sender.selected) {
        [AlertLabel AlertInfoWithText:@"开启屏幕旋转" andWith:_controlView withLocationTag:2];
        _controlView.lockedVODBtn.selected = NO;
        if (_lockBlock) {
            
            _lockBlock(sender,_controlView.lockedVODBtn.selected);
        }

        [self setIsMaskShowing:YES];

    }else
    {
        [AlertLabel AlertInfoWithText:@"锁定屏幕旋转" andWith:_controlView withLocationTag:2];

        _controlView.lockedVODBtn.selected = YES;
        if (_lockBlock) {
            
            _lockBlock(sender,_controlView.lockedVODBtn.selected);
        }
        
        [self setIsMaskShowing:NO];

    }
    

}


#pragma mark 分享按钮事件


-(void)shareButtonAction
{
    if (_controlView.demandModel) {
        
        _controlView.demandModel = nil;
    }
    _controlView.demandModel = [[DemandModel alloc]init];
    _controlView.demandModel.resultDictionary = _polyMedicDic;
    _controlView.demandModel.NAME = [_polyMedicDic objectForKey:@"healthyTypeName"];
    
    _controlView.demandModel.name = isEmptyStringOrNilOrNull([_polyMedicDic objectForKey:@"name"])?_contentTitleStr:[_polyMedicDic objectForKey:@"name"];
    
    _controlView.demandModel.imageLink = isEmptyStringOrNilOrNull([_polyMedicDic objectForKey:@"imageUrl"])?[_polyMedicDic objectForKey:@"imageLink"]:[_polyMedicDic objectForKey:@"imageUrl"];
    
    
    
    _controlView.demandModel.CONTENTID = isEmptyStringOrNilOrNull([_polyMedicDic objectForKey:@"id"])?_contentID:[_polyMedicDic objectForKey:@"id"];
    NSLog(@"快快快快快%@",[_polyMedicDic objectForKey:@"name"]);

    if (_shareBlock) {
        _shareBlock(_controlView.demandModel);
//       BOOL isreturn =  _shareBlock(dic);
//        if (isreturn == YES)
//        {
//            _shareBlock(dic1);
//        }
        
    }else{
        return;
    }
}


#pragma  mark 收藏按钮事件

-(void)favorButtonAction:(UIButton*)sender
{
//    if (_favorBlock) {
//        _favorBlock(IsLogin);
//    }

    if (sender.selected) {
        
        
        if (IsLogin) {
        
            
            HTRequest *poliyMedic = [[HTRequest alloc]initWithDelegate:self];
            //[poliyMedic YJCdnVideoDelFavoriteWithVideoIds:[_polyMedicDic objectForKey:@"videoId"] withMobile:KMobileNum];
            if (isUsePolymedicine_2_0) {
                
                [poliyMedic YJHealthyVodDelFavoriteWithInstanceCode:KInstanceCode withMobile:IsLogin?KMobileNum:@"" withProgramIds:!poliyMedic?_contentID:[_polyMedicDic objectForKey:@"id"]];
            }else
            {
               [poliyMedic YJCdnVideoDelFavoriteWithInstanceCode:KInstanceCode WithVideoIds:[_polyMedicDic objectForKey:@"videoId"] withMobile:KMobileNum];
            }
           
        }
//        else
//        {
//            
//            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self withLocationTag:1];
//        }
        
    }else
    {
        
        
        if (IsLogin) {
            HTRequest *poliyMedic = [[HTRequest alloc]initWithDelegate:self];
           // [poliyMedic YJCdnVideoAddfavoriteWithVideoId:[_polyMedicDic objectForKey:@"videoId"] withMobile:KMobileNum];
            if (isUsePolymedicine_2_0) {
                
                [poliyMedic YJHealthyVodAddFavoriteWithInstanceCode:KInstanceCode withMobile:IsLogin?KMobileNum:@"" withProgramId:!_polyMedicDic?_contentID:[_polyMedicDic objectForKey:@"id"]];
            }else
            {
            [poliyMedic YJCdnVideoAddfavoriteWithInstanceCode:KInstanceCode WithVideoId:[_polyMedicDic objectForKey:@"videoId"] withMobile:KMobileNum];
                
               
            }
        }else
        {
           
            if (_favorBlock) {
                _favorBlock(IsLogin);
            }
            
            
            
//            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self withLocationTag:1];
        }

    }
    
    
   
}


#pragma mark 推荐按钮事件

-(void)videoListButtonAction:(UIButton*)sender
{
//    sender.selected = !sender.selected;
    if (_videoListBlock) {
        _videoListBlock(sender,sender.selected);
    }
    _controlView.videoListVODView.hidden = NO;
    _controlView.videoListVODTV.hidden = NO;
    _controlView.vodTopImageView.hidden = YES;
    _controlView.vodBottomImageView.hidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    
    if (IS_RECOMMENDARRAY == YES) {
        
        [_controlView setAsscciationArray:_asscciationArray];
    }else
    {
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    if (isUsePolymedicine_2_0) {
        
//        [request YJHealthyVodRecommendProgramWithProgramId:_contentID];
        [request YJHealthyVodRecommendProgramWithProgramId:_contentID withMaxCount:@"20"];
    }else
    {
    [request YJCdnVideoRecommendWithInstanceCode:KInstanceCode WithVideoId:_contentID withType:_polyMedicType withMaxCount:@"50"];
    }
        
    }
    
    
    
}


#pragma mark 下一曲按钮事件


-(void)backwardButtonAction:(UIButton*)sender
{
    
    
//    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//    if (isUsePolymedicine_2_0) {
//        
//        //        [request YJHealthyVodRecommendProgramWithProgramId:_contentID];
//        [request YJHealthyVodRecommendProgramWithProgramId:_contentID withMaxCount:@"20"];
//    }else
//    {
//        [request YJCdnVideoRecommendWithInstanceCode:KInstanceCode WithVideoId:_contentID withType:_polyMedicType withMaxCount:@"50"];
//    }

    
    
    if (_asscciationArray.count >0) {
        
        [self clearPlayerAndNextPlay];
        
        
        if (IS_RECOMMENDARRAY == YES) {
           
         

            
        }else
        {
        
        
        NSLog(@"下一曲的节目是%@",[[_asscciationArray objectAtIndex:0] objectForKey:@"name"]);
        _controlView.serviceTitleVODLabel.text = [[_asscciationArray objectAtIndex:0] objectForKey:@"name"];
        }
        //    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        //    [request VODPlayWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andName:@"" andWithMovie_type:@"1" andTimeout:KTimeout];
        //    HTRequest *requester = [[HTRequest alloc]initWithDelegate:self];
        //    [requester VODAssccationListWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andCount:20 andTimeout:KTimeout];
        [self nextButtonOrDidFinishPlayVideo];
    }else
    {
        
        [self resetToPlayNewURL];
        [self setVideoURL:_videoURL];        // 从xx秒播放
        
        self.seekTime = 0;

    }
    
    
   
    
    
    
}


#pragma mark 返回按钮事件


/**
 *  返回按钮事件
 */
- (void)backButtonAction
{
    WS(wself);
        if (_isConstraintLandscape == YES) {
//            [self setOrientationPortrait];
//            self.watchDuration = [self timeLengthForCurrent];
            
            if (USER_ACTION_COLLECTION) {
                
                
                
                _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
                
                [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_InterNet andAVName:_polyModel.name andOPSCode:PlayerLoadingExitError andOPSST:[SystermTimeControl getNowServiceTimeString]];
                
                
                
                if (_isCollectionInfo) {
                    _polyModel.watchDuration = _watchDuration;
                    [SAIInformationManager polyMedicineInfomation:_polyModel];
                    [_timeManager reset];
                    _watchDuration = nil;
                }
                
            }
            
           
            [self resetPlayer];
            self.isConstraintLandscape = NO;
            [self removeFromSuperview];
            if (self.autoHiddenTimer) {
                [self.autoHiddenTimer invalidate];
                self.autoHiddenTimer = nil;
            }
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            
            //                return;
            //            }
            // player加到控制器上，只有一个player时候
            [self.timer invalidate];
            [self pause];
            
            

            if (wself.goBackBlock) {
                wself.goBackBlock();
            }
            
  
        }else
        {
    
        if (!self.isFullScreen) {
            [self resetPlayer];
            self.isConstraintLandscape = NO;
            if (self.autoHiddenTimer) {
                [self.autoHiddenTimer invalidate];
                self.autoHiddenTimer = nil;
            }

            [self removeFromSuperview];
            // player加到控制器上，只有一个player时候
            [self.timer invalidate];
            [self pause];
            if (self.goBackBlock) {
                self.goBackBlock();
            }
        }else {
            
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            self.isFullScreen = NO;
            }
        }
    
}


#pragma mark 全屏按钮事件

/**
 //暂时没有这个全屏的按钮
 *  全屏按钮事件
 *
 *  @param sender 全屏Button
 */
- (void)fullScreenAction:(UIButton *)sender
{
    if (sender.selected == YES) {
        [self interfaceOrientation:UIInterfaceOrientationPortrait];
        return;
    }
    
    UIDeviceOrientation orientation             = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            [self interfaceOrientation:UIInterfaceOrientationPortrait];
        }
            break;
        case UIInterfaceOrientationPortrait:{
            [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
//            if (self.isBottomVideo || !self.isFullScreen) {
//                ZFPlayerShared.isAllowLandscape = YES;
//                [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
//            } else {
//                ZFPlayerShared.isAllowLandscape = NO;
//                [self interfaceOrientation:UIInterfaceOrientationPortrait];
//            }
            [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];

        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
//            if (self.isBottomVideo || !self.isFullScreen) {
//                ZFPlayerShared.isAllowLandscape = YES;
//                [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
//            } else {
//                ZFPlayerShared.isAllowLandscape = NO;
//                [self interfaceOrientation:UIInterfaceOrientationPortrait];
//            }
            
             [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
        }
            break;
            
        default: {
            if (_isConstraintLandscape == YES) {
//                ZFPlayerShared.isAllowLandscape = YES;
                [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
            } else {
//                ZFPlayerShared.isAllowLandscape = NO;
                [self interfaceOrientation:UIInterfaceOrientationPortrait];
            }
        }
            break;
    }
    
}


#pragma mark - slider事件

/**
 *  slider开始滑动事件
 *
 *  @param slider UISlider
 */
- (void)progressSliderTouchBegan:(UISlider *)slider
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        // 暂停timer
        [self.timer setFireDate:[NSDate distantFuture]];
    }
}

/**
 *  slider滑动中事件
 *
 *  @param slider UISlider
 */
- (void)progressSliderValueChanged:(UISlider *)slider
{
    //拖动改变视频播放进度
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        NSString *style = @"";
        CGFloat value   = slider.value - self.sliderVODLastValue;
        if (value > 0) { style = @">>"; }
        if (value < 0) { style = @"<<"; }
        
        self.sliderVODLastValue    = slider.value/1000;
        // 暂停
        [self pause];
        
        CGFloat total           = ((CGFloat)_playerItem.duration.value / _playerItem.duration.timescale)/1000;
        
        //计算出拖动的当前秒数
        NSInteger dragedSeconds = floorf(total * slider.value);
        
        //转换成CMTime才能给player来控制播放进度
        
//        CMTime dragedCMTime     = CMTimeMake(dragedSeconds, 1);
//        // 拖拽的时长
//        NSInteger proMin        = (NSInteger)CMTimeGetSeconds(dragedCMTime) / 60;//当前秒
//        NSInteger proSec        = (NSInteger)CMTimeGetSeconds(dragedCMTime) % 60;//当前分钟
//        
//        //duration 总时长
//        NSInteger durMin        = (NSInteger)total / 60;//总秒
//        NSInteger durSec        = (NSInteger)total % 60;//总分钟
//        
//        NSString *currentTime   = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
//        NSString *totalTime     = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
        
        
        NSString *currentTime = [self durationStringWithTime:(int)dragedSeconds];
  
    
        if (total > 0) {
            // 当总时长 > 0时候才能拖动slider
            self.controlView.currentVODTimeLabel.text  = currentTime;
//            self.controlView.horizontalLabel.hidden = NO;
//            self.controlView.horizontalLabel.text   = [NSString stringWithFormat:@"%@ %@ / %@",style, currentTime, totalTime];
        }else {
            // 此时设置slider值为0
            slider.value = 0;
        }
        
    }else { // player状态加载失败
        // 此时设置slider值为0
        slider.value = 0;
    }
}

/**
 *  slider结束滑动事件
 *
 *  @param slider UISlider
 */
- (void)progressSliderTouchEnded:(UISlider *)slider
{
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        
        // 继续开启timer
        [self.timer setFireDate:[NSDate date]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.controlView.progressTimeView.hidden = YES;
        });
        // 结束滑动时候把开始播放按钮改为播放状态
        self.controlView.startVODBtn.selected = YES;
        self.isPauseByUser                 = NO;
        
        // 滑动结束延时隐藏controlView
//        [self autoFadeOutControlBar];
        // 视频总时间长度
        CGFloat total           = (CGFloat)_playerItem.duration.value / _playerItem.duration.timescale;
        
        //计算出拖动的当前秒数
        NSInteger dragedSeconds = (floorf(total * slider.value)/1000);
        
        [self seekToTime:dragedSeconds completionHandler:nil];
    }
}





#pragma mark 触摸手势事件


/**
 *  pan手势事件
 *
 *  @param pan UIPanGestureRecognizer
 */
- (void)panDirection:(UIPanGestureRecognizer *)pan
{
    
    if (_controlView.lockedVODBtn.selected == YES) {
        return;
    }
    
    //根据在view上Pan的位置，确定是调音量还是亮度
    CGPoint locationPoint = [pan locationInView:self];
    
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:self];
    
    // 判断是垂直移动还是水平移动
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{ // 开始移动
            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            if (x > y) { // 水平移动
                // 取消隐藏
                self.controlView.progressTimeView.hidden = NO;
                self.panDirection = PanDirectionHorizontalMoved;
                // 给sumTime初值
                CMTime time       = self.player.currentTime;
                self.sumTime      = time.value/time.timescale;
                
                // 暂停视频播放
                [self pause];
                // 暂停timer
                [self.timer setFireDate:[NSDate distantFuture]];
            }
            else if (x < y){ // 垂直移动
                self.panDirection = HTPanDirectionVerticalMoved;
                // 开始滑动的时候,状态改为正在控制音量
                if (locationPoint.x > self.bounds.size.width / 2) {
//                    self.isVolume = YES;
                    
                    _panDirection = PanDirectionVerticalVolumeMoved;
                }else { // 状态改为显示亮度调节
//                    self.isVolume = NO;
                    _controlView.brightnessVODView.hidden = NO;
                    
                    _panDirection = PanDirectionVerticalBrighteMoved;
                }
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{ // 正在移动
            switch (self.panDirection) {
                case PanDirectionHorizontalMoved:{
                    [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
                    break;
                }
                case PanDirectionVerticalVolumeMoved:{
                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                   _controlView.brightnessVODView.hidden = NO;
                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                    break;
                }

//                case HTPanDirectionVerticalMoved:{
//                    [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
//                    break;
//                }
                default:
                    break;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:{ // 移动停止
            // 移动结束也需要判断垂直或者平移
            // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
            switch (self.panDirection) {
                case PanDirectionHorizontalMoved:{
                    
                    // 继续播放
                    [self play];
                    [self.timer setFireDate:[NSDate date]];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        // 隐藏视图
                        self.controlView.progressTimeView.hidden = YES;
                    });
                    // 快进、快退时候把开始播放按钮改为播放状态
                    self.controlView.startVODBtn.selected = YES;
                    self.isPauseByUser                 = NO;
                    
                    [self seekToTime:self.sumTime completionHandler:nil];
                    // 把sumTime滞空，不然会越加越多
                    self.sumTime = 0;
                    break;
                }
//                case HTPanDirectionVerticalMoved:{
//                    // 垂直移动结束后，把状态改为不再控制音量
//                    self.isVolume = NO;
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        self.controlView.progressTimeView.hidden = YES;
//                    });
//                    break;
//                }
                case PanDirectionVerticalVolumeMoved:{
                    
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                   _controlView.brightnessVODView.hidden = YES;
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                    
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}




/**
 *  pan水平移动的方法
 *
 *  @param value void
 */
- (void)horizontalMoved:(CGFloat)value
{
//    // 快进快退的方法
//    NSString *style = @"";
//    if (value < 0) { style = @"<<"; }
//    if (value > 0) { style = @">>"; }
    
    if (value >0) {
        
        [_controlView.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_forward"]];
    }else
    {
        [_controlView.progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_backward"]];
    }
    // 每次滑动需要叠加时间
    self.sumTime += value / 200;
    
    // 需要限定sumTime的范围
    CMTime totalTime           = self.playerItem.duration;
    CGFloat totalMovieDuration = ((CGFloat)totalTime.value/totalTime.timescale);
    if (self.sumTime > totalMovieDuration) { self.sumTime = totalMovieDuration;}
    if (self.sumTime < 0){ self.sumTime = 0; }
    
    // 当前快进的时间
    NSString *nowTime         = [self durationStringWithTime:(int)self.sumTime];
    // 总时间
    NSString *durationTime    = [self durationStringWithTime:(int)totalMovieDuration];
    // 给label赋值
//    self.controlView.horizontalLabel.text = [NSString stringWithFormat:@"%@ %@ / %@",style, nowTime, durationTime];
    
    
    [_controlView.progressTimeLable_top setText:[NSString stringWithFormat:@"%@",nowTime]];
    [_controlView.progressTimeLable_bottom setText:[NSString stringWithFormat:@"/%@",durationTime]];


}


/**
 *  pan垂直移动的方法
 *
 *  @param value void
 */
- (void)verticalMoved:(CGFloat)value
{
    //    self.isVolume ? (self.volumeViewSlider.value -= value / 10000) : ([UIScreen mainScreen].brightness -= value / 10000);
    
    NSInteger index = (NSInteger)value;
    
    switch (_panDirection) {
        case PanDirectionVerticalVolumeMoved:
            
        {
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素声音减一格
                    
                    [self volumeAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素声音增加一格
                    [self volumeAdd:+0.05];
                }
            }
            
            
        }
            break;
        case PanDirectionVerticalBrighteMoved:
        {
            
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素亮度减一格
                    [self brightnessAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素亮度增加一格
                    
                    [self brightnessAdd:0.05];
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
    
}


//声音增加
- (void)volumeAdd:(CGFloat)step{
    [HTVolumeUtil shareInstance].volumeValue += step;;
}
//亮度增加
- (void)brightnessAdd:(CGFloat)step{
    [UIScreen mainScreen].brightness += step;
    _controlView.brightnessVODProgress.progress = [UIScreen mainScreen].brightness;
}


#pragma mark 转化字符串的方法

/**
 *  根据时长求出字符串
 *
 *  @param time 时长
 *
 *  @return 时长字符串
 */
- (NSString *)durationStringWithTime:(int)time
{
    
    
    NSInteger NJumphour = (time/3600);
    NSInteger NJumpMinute = (time - (NJumphour*3600)) / 60 ;
    NSInteger NJumpSecond = (time -  NJumphour*3600 - NJumpMinute*60);
    NSString *NTimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",NJumphour,NJumpMinute,NJumpSecond];

    return NTimeString;
//    // 获取分钟
//    NSString *min = [NSString stringWithFormat:@"%02d",time / 60];
//    // 获取秒数
//    NSString *sec = [NSString stringWithFormat:@"%02d",time % 60];
//    return [NSString stringWithFormat:@"%@:%@", min, sec];
}





#pragma mark - NSNotification Action下一曲或者节目播放完毕等

/**
 *  播放完了
 *
 *  @param notification 通知
 */


-(void)clearPlayerAndNextPlay
{
    self.state            = HTPlayerStateStopped;
    self.controlView.backgroundColor  = RGBA(0, 0, 0, .6);
    self.playDidEnd                   = YES;
    [self resetPlayer];
    [_errorManager resetPlayerStatusInfo];
    //            self.controlView.repeatBtn.hidden = NO;
    // 初始化显示controlView为YES
    self.isMaskShowing                = YES;
    // 延迟隐藏controlView
    
//    [self setVideoURL:[NSURL URLWithString:[_asscciationArray objectAtIndex:0]]];
//    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//    [request VODPlayWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andName:@"" andWithMovie_type:@"1" andTimeout:KTimeout];
//    [request VODAssccationListWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andCount:20 andTimeout:KTimeout];
//    
//    [_controlView.videoListVODTV reloadData];
 
}





- (void)moviePlayDidEnd:(NSNotification *)notification
{
   
    self.isMaskShowing = NO;
    
    if (_asscciationArray.count>0) {
        [self clearPlayerAndNextPlay];
        NSLog(@"下一曲的节目是%@",[[_asscciationArray objectAtIndex:0] objectForKey:@"name"]);
        _controlView.serviceTitleVODLabel.text = [[_asscciationArray objectAtIndex:0] objectForKey:@"name"];
        
        //    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        //    [request VODPlayWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andName:@"" andWithMovie_type:@"1" andTimeout:KTimeout];
        //    HTRequest *requester = [[HTRequest alloc]initWithDelegate:self];
        //    [requester VODAssccationListWithContentID:[[_asscciationArray objectAtIndex:0] objectForKey:@"contentID"] andCount:20 andTimeout:KTimeout];
        
        [self nextButtonOrDidFinishPlayVideo];
    }else
    {
       
        [self resetToPlayNewURL];
        [self setVideoURL:_videoURL];        // 从xx秒播放

        self.seekTime = 0;
        
        
    }
    
    
    

    
    
  }





-(void)nextButtonOrDidFinishPlayVideo
{
//    self.watchDuration = [self timeLengthForCurrent];
    
    
    if (_isCollectionInfo) {
        
        _polyModel.watchDuration = _watchDuration;
        
        if (USER_ACTION_COLLECTION) {
            [SAIInformationManager polyMedicineInfomation:_polyModel];
            [_timeManager reset];
            _watchDuration = nil;
        }
  
    }
    
    if (_polyMedicDic) {
        _polyMedicDic = nil;
    }
    _polyMedicDic = [NSMutableDictionary dictionary];
    
    
    if (IS_RECOMMENDARRAY == YES) {

        int currentIndex = [self isCurrentContentIDIndexFromRecommendArray:_asscciationArray withContentID:_contentID];

        if (_asscciationArray.count <=currentIndex+1 ) {
            
            currentIndex = -1;
        }
        [self setPolyMedicDic:[_asscciationArray objectAtIndex:currentIndex+1]];

//       [htrequest YJHealthyVodMediaPlayListWithProgramId:[[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"id"] withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];
        
        
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"id"],@"itemId",[[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"name"],@"name",@"2",@"ENtype",[[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"id"],@"TrackId",[[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"name"],@"TrackName", nil];
        _polyModel = [HealthVodCollectionModel heaalthVodWithDict:dictionary];
        _polyModel.watchTime = [SystermTimeControl getNowServiceTimeString];

           _controlView.serviceTitleVODLabel.text = [[_asscciationArray objectAtIndex:currentIndex+1] objectForKey:@"name"];
        
    }else
    {
        
        
        
        _polyMedicDic = [_asscciationArray objectAtIndex:0];
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[_asscciationArray objectAtIndex:0] objectForKey:@"id"],@"itemId",[[_asscciationArray objectAtIndex:0] objectForKey:@"name"],@"name",@"2",@"ENtype",[[_asscciationArray objectAtIndex:0] objectForKey:@"id"],@"TrackId",[[_asscciationArray objectAtIndex:0] objectForKey:@"name"],@"TrackName", nil];
        _polyModel = [HealthVodCollectionModel heaalthVodWithDict:dictionary];
        _polyModel.watchTime = [SystermTimeControl getNowServiceTimeString];

    
    HTRequest  *htrequest = [[HTRequest alloc]initWithDelegate:self];
    if (isUsePolymedicine_2_0) {
        
        [htrequest YJHealthyVodMediaPlayListWithProgramId:[[_asscciationArray objectAtIndex:0] objectForKey:@"id"] withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];
    }else
    {
    [htrequest YJCdnVideoUrlWithInstanceCode:@"" withMobile:@"" withVideoId:[[_asscciationArray objectAtIndex:0] objectForKey:@"videoId"]];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        if (isUsePolymedicine_2_0) {
            
            [request YJHealthyVodRecommendProgramWithProgramId:[[_asscciationArray objectAtIndex:0] objectForKey:@"id"] withMaxCount:@"20"];
            
        }else
        {
        [request YJCdnVideoRecommendWithInstanceCode:KInstanceCode WithVideoId:_contentID withType:_polyMedicType withMaxCount:@"50"];
        }
    });
    }

}




-(BOOL)isFavourPlayer:(NSString *)favourStr
{
    
    if ([favourStr integerValue]==1) {
        [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
        _controlView.favorVODBtn.selected = YES;
        return YES;
    }else{
        _controlView.favorVODBtn.selected = NO;
        [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];

        return NO;
    }
}




#pragma 应用进入前后台


/**
 *  应用退到后台
 */
- (void)appDidEnterBackground
{
    [self.controlView showControlViewAndIsFullScreen:NO];
    self.didEnterBackground = YES;
    self.controlView.startVODBtn.selected = NO;
    [self pause];
    self.state = HTPlayerStatePause;
    [self.timer invalidate];
}

/**
 *  应用进入前台
 */
- (void)appDidEnterPlayGround
{
    
    
    [self addNotifications];
    [self.controlView showControlViewAndIsFullScreen:YES
     ];
    self.didEnterBackground = NO;
    self.isMaskShowing = YES;
    self.isFullScreen = YES;
    // 延迟隐藏controlView
    [self createTimer];
    if (!self.isPauseByUser) {
        self.state                         = HTPlayerStatePlaying;
        self.controlView.startVODBtn.selected = YES;
        self.isPauseByUser                 = NO;
        [self play];
    }
}


-(void)actionPauseForVideo
{
    if (self.isPauseByUser) {
        
        self.controlView.startVODBtn.selected = NO;
        [self pause];
        self.state = HTPlayerStatePause;
        [self.timer invalidate];
    }
    
    
}


-(void)actionPlayForVideo
{
    self.isMaskShowing = YES;
    // 延迟隐藏controlView
    [self createTimer];
    if (!self.isPauseByUser) {
        self.state                         = HTPlayerStatePlaying;
        self.controlView.startVODBtn.selected = YES;
        self.isPauseByUser                 = NO;
        [self play];
    }

}




#pragma mask HTRequest代理
/*
 HTRequest代理
 */

-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:YJ_CDN_VIDEO_RECOMMEND]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        if (ret == 0) {
            _asscciationArray = [[NSMutableArray alloc]init];
            if ([[result objectForKey:@"list"] count]>0) {
                _asscciationArray = [result objectForKey:@"list"];
                
                if (_controlView.asscciationArray) {
                    _controlView.asscciationArray = nil;
                }
                _controlView.asscciationArray = [[NSMutableArray alloc]init];
//                _controlView.asscciationArray = _asscciationArray;
                [_controlView setAsscciationArray:_asscciationArray];
                [_controlView.videoListVODTV reloadData];
            }else
            {
                [AlertLabel AlertInfoWithText:@"暂无推荐数据" andWith:self withLocationTag:2];
                
            }
            
        }else
        {
            [AlertLabel AlertInfoWithText:@"暂无推荐数据" andWith:self withLocationTag:2];
        }
        
    }
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_RECOMMENDPROGRAM]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        if (ret == 0) {
            
            NSLog(@"2.0接口的推荐节目返回的正确数据");
            
            _asscciationArray = [[NSMutableArray alloc]init];
            if ([[result objectForKey:@"list"] count]>0) {
                _asscciationArray = [result objectForKey:@"list"];
                if (_controlView.asscciationArray) {
                    _controlView.asscciationArray = nil;
                }
                _controlView.asscciationArray = [[NSMutableArray alloc]init];
//                _controlView.asscciationArray = _asscciationArray;
                [_controlView setAsscciationArray:_asscciationArray];
                [_controlView.videoListVODTV reloadData];
            }else
            {
                [AlertLabel AlertInfoWithText:@"暂无推荐数据" andWith:self withLocationTag:2];
                
            }
            
            
        }else
        {
            NSLog(@"2.0接口的推荐节目返回的错误数据");
            [AlertLabel AlertInfoWithText:@"暂无推荐数据" andWith:self withLocationTag:2];
            
        }
    }
    
    
    
    
    if ([type isEqualToString:YJ_CDN_VIDEO_URL]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        if (ret !=0) {
            [self isFavourPlayer:false];
            [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self withLocationTag:2];
            _controlView.polyMedicineView.hidden = YES;
            return;
        }
        if (ret == 0) {
            if ([[result objectForKey:@"playLink"] count]>0) {
                _videoURLArray = [NSMutableArray arrayWithArray:[result objectForKey:@"playLink"]];
                self.controlView.resolutionBtn.hidden = NO;
                self.controlView.resolutionArray = _videoURLArray;
                NSString *playlink = [[_videoURLArray objectAtIndex:0] objectForKey:@"url"];
                //            _videoURL = [NSURL URLWithString:playlink];
                [self setVideoURL:[NSURL URLWithString:playlink]];
                [self isFavourPlayer:[result objectForKey:@"favorite"]];
            }else
            {
                [self isFavourPlayer:false];
                [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self withLocationTag:2];
                _controlView.polyMedicineView.hidden = YES;

            }
            
        }
        
        
    }
    
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_MEDIAPLAYLIST]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        
        if (ret == 0) {
            [self isFavourPlayer:[result objectForKey:@"favorite"]];
            if ([[result objectForKey:@"list"] count]>0) {
                _videoURLArray = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
                self.controlView.resolutionBtn.hidden = NO;
                self.controlView.resolutionArray = _videoURLArray;
                NSString *playlink = [[_videoURLArray objectAtIndex:0] objectForKey:@"playLink"];
                //            _videoURL = [NSURL URLWithString:playlink];
                [self setVideoURL:[NSURL URLWithString:playlink]];
            }else
            {
                _isCollectionInfo = NO;
                [self isFavourPlayer:false];
                [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self withLocationTag:2];
                _controlView.polyMedicineView.hidden = YES;
            }
            
        }else
        {
            _isCollectionInfo = NO;
            [self isFavourPlayer:false];
            [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self withLocationTag:2];
            _controlView.polyMedicineView.hidden = YES;
        }
    }
    
    
    
    if ([type isEqualToString:YJ_CDN_VIDEO_DELFAVORITE]) {
        int ret = [[result objectForKey:@"ret"] intValue];
        if (ret == 0) {
            [AlertLabel AlertInfoWithText:@"取消收藏成功" andWith:self withLocationTag:1];
            _controlView.favorVODBtn.selected = NO;
            [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
            
//            [LocalCollectAndPlayRecorder deleteHealthyCollectionRecordWithDeleteDic:_polyMedicDic andUserType:YES andKey:@"videoId"];
            [_localListCenter deleteHealthyCollectionRecordWithDeleteDic:_polyMedicDic andUserType:YES andKey:@"videoId"];
        }else
        {
            [AlertLabel AlertInfoWithText:@"取消收藏失败" andWith:self withLocationTag:1];

        }
        
    }
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_DELFAVORITE]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        if (ret == 0) {
            [AlertLabel AlertInfoWithText:@"取消收藏成功" andWith:self withLocationTag:1];
            _controlView.favorVODBtn.selected = NO;
            [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
            
//            [LocalCollectAndPlayRecorder deleteHealthyCollectionRecordWithDeleteDic:_polyMedicDic andUserType:YES andKey:@"id"];
            [_localListCenter deleteHealthyCollectionRecordWithDeleteDic:_polyMedicDic andUserType:YES andKey:@"id"];
        }else
        {
            [AlertLabel AlertInfoWithText:@"取消收藏失败" andWith:self withLocationTag:1];

        }
    }
    
    
    
    
    
    if ([type isEqualToString:YJ_CDN_VIDEO_ADDFAVORITE]) {
        int ret = [[result objectForKey:@"ret"] intValue];
        
        if (ret == 0) {
            [AlertLabel AlertInfoWithText:@"收藏成功" andWith:self withLocationTag:1];
            //            [self favorSucceed:YES];
            
            _controlView.favorVODBtn.selected = YES;
            [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
            
//            [LocalCollectAndPlayRecorder addHealthyCollectionRecordWithDic:_polyMedicDic andUserType:YES andKey:@"videoId"];
            [_localListCenter addHealthyCollectionRecordWithDic:_polyMedicDic andUserType:YES andKey:@"videoId"];
        }else
        {
            [AlertLabel AlertInfoWithText:@"收藏失败" andWith:self withLocationTag:1];

        }
        
    }
    
    
    
    if ([type isEqualToString:YJ_HEALTHYVOD_ADDFAVORITE]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        
        if (ret == 0) {
            [AlertLabel AlertInfoWithText:@"收藏成功" andWith:self withLocationTag:1];
            //            [self favorSucceed:YES];
            
            _controlView.favorVODBtn.selected = YES;
            [_controlView.favorVODBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
            
//            [LocalCollectAndPlayRecorder addHealthyCollectionRecordWithDic:_polyMedicDic andUserType:YES andKey:@"id"];
            [_localListCenter addHealthyCollectionRecordWithDic:_polyMedicDic andUserType:YES andKey:@"id"];
        }else
        {
            [AlertLabel AlertInfoWithText:@"收藏失败" andWith:self withLocationTag:1];

        }
        
    }
    
    
}






- (void)dealloc
{
    self.playerItem = nil;
//    self.tableView = nil;
    // 移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark - Others

/**
 *  通过颜色来生成一个纯色图片
 */
- (UIImage *)buttonImageFromColor:(UIColor *)color{
    
    CGRect rect = self.bounds;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext(); return img;
}






#pragma mark 采集时间的时长

-(void)countingTo:(NSTimeInterval)time timeToShowString:(NSString*)timeToShowString
{
//    label.text =[NSString stringWithFormat:@"%.3f/%@",time,timeToShowString];
    _watchDuration = [NSString stringWithFormat:@"%.0f",time];
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self reStartHiddenTimer];
    
}



#pragma mark HTPlayerControlViewDelegate

-(void)videoListIndexPathAssociateArr:(NSMutableArray *)associateArr andIndexPathRow:(NSInteger)indePathRow
{
    
    NSLog(@"我点击了推荐列表的liebiao");
    [self clearPlayerAndNextPlay];
       self.isMaskShowing = NO;
//    self.watchDuration = [self timeLengthForCurrent];
    
    if (USER_ACTION_COLLECTION) {
    
        if (_isCollectionInfo) {
        
        _polyModel.watchDuration = _watchDuration;

        [SAIInformationManager polyMedicineInfomation:_polyModel];
            [_timeManager reset];
            _watchDuration = nil;
        }
    }
    
   
    

    if (_polyMedicDic) {
        _polyMedicDic = nil;
    }
    
    if (_polyModel) {
        _polyModel = nil;
    }
    
    _polyMedicDic = [NSMutableDictionary dictionary];
    
    
    if ([_asscciationArray count]>0 && (indePathRow <= _asscciationArray.count -1)) {
        
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"id"],@"itemId",[[associateArr objectAtIndex:indePathRow] objectForKey:@"name"],@"name",@"2",@"ENtype",[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"id"],@"TrackId",[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"name"],@"TrackName", nil];
    _polyModel = [HealthVodCollectionModel heaalthVodWithDict:dictionary];
    _polyModel.watchTime = [SystermTimeControl getNowServiceTimeString];
//    _polyMedicDic = [associateArr objectAtIndex:indePathRow];
    
        [self setPolyMedicDic:[associateArr objectAtIndex:indePathRow]];
    
    _controlView.serviceTitleVODLabel.text = [[associateArr objectAtIndex:indePathRow] objectForKey:@"name"];
        
        
        if (IS_RECOMMENDARRAY == YES) {
           
//            HTRequest  *htrequest = [[HTRequest alloc]initWithDelegate:self];
//            
//            
//            [htrequest YJHealthyVodMediaPlayListWithProgramId:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"id"] withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];
           
            
        }else
        {
   
    HTRequest  *htrequest = [[HTRequest alloc]initWithDelegate:self];

    if (isUsePolymedicine_2_0) {
        
        [htrequest YJHealthyVodMediaPlayListWithProgramId:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"id"] withMobile:IsLogin?KMobileNum:@"" withInstanceCode:KInstanceCode];
    }else
    {
    [htrequest YJCdnVideoUrlWithInstanceCode:@"" withMobile:@"" withVideoId:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"videoId"]];
    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        if (isUsePolymedicine_2_0) {
            
             [request YJHealthyVodRecommendProgramWithProgramId:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"id"] withMaxCount:@"20"];
        }else
        {
            [request YJCdnVideoRecommendWithInstanceCode:KInstanceCode WithVideoId:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"videoId"] withType:[[_asscciationArray objectAtIndex:indePathRow] objectForKey:@"type"] withMaxCount:@"50"];}
//    });

    
        }
    }
    
}



@end
