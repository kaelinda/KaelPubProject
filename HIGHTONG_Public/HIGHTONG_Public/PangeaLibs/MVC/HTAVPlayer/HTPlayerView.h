//
//  HTPlayerView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTPlayerControlView.h"
#import "HTRequest.h"
#import "HTPolymedicineInterface.h"
#import <UMSocialCore/UMSocialCore.h>
#import "DemandModel.h"
#import "HealthVodCollectionModel.h"
#import "PlayerErrorManager.h"
// 返回按钮的block
typedef void(^HTPlayerGoBackBlock)(void);

// 分享按钮的block
typedef void(^HTPlayerShareBlock)(DemandModel*demand);

// 收藏按钮的block
typedef void(^HTPlayerFavorBlock)(BOOL isSuccess);

// 推荐列表按钮的block
typedef void(^HTPlayerVideoListBlock)(UIButton *button,BOOL isSelect);

typedef void(^HTPlayerLockBlock)(UIButton *button,BOOL isSelect);



// playerLayer的填充模式（默认：等比例填充，直到一个维度到达区域边界）
typedef NS_ENUM(NSInteger, HTPlayerLayerGravity) {
    HTPlayerLayerGravityResize,           // 非均匀模式。两个维度完全填充至整个视图区域
    HTPlayerLayerGravityResizeAspect,     // 等比例填充，直到一个维度到达区域边界
    HTPlayerLayerGravityResizeAspectFill  // 等比例填充，直到填充满整个视图区域，其中一个维度的部分区域会被裁剪
};





@interface HTPlayerView : UIView<HTRequestDelegate,HTPlayerControlViewDelegate>
@property (nonatomic,strong)NSString *contentID;
@property (nonatomic,strong)NSString *contentTitleStr;
@property (nonatomic,strong)NSString *polyMedicType;
@property (nonatomic,strong)NSMutableDictionary *polyMedicDic;
@property (nonatomic,strong)NSMutableArray *recommendArray;
@property (nonatomic,strong)DemandModel *demandModel;
@property (nonatomic,strong)HealthVodCollectionModel *polyModel;
@property (nonatomic,strong)UIPanGestureRecognizer *pan;
/** 视频URL */
@property (nonatomic, strong) NSURL                *videoURL;
@property (nonatomic, strong) NSMutableArray       *videoURLArray;
@property (nonatomic, strong) NSMutableArray       *asscciationArray;
/** 控制层View */
@property (nonatomic, strong) HTPlayerControlView *controlView;
/** 返回按钮Block */
@property (nonatomic, copy  ) HTPlayerGoBackBlock  goBackBlock;
/** 分享按钮Block */
@property (nonatomic, copy  ) HTPlayerShareBlock  shareBlock;
//锁屏的按钮Block
@property (nonatomic, copy  ) HTPlayerLockBlock  lockBlock;

/** 收藏Block */
@property (nonatomic, copy  ) HTPlayerFavorBlock  favorBlock;
/** 收藏Block */
@property (nonatomic, copy  ) HTPlayerVideoListBlock  videoListBlock;
/** 设置playerLayer的填充模式 */
@property (nonatomic, assign) HTPlayerLayerGravity playerLayerGravity;
@property (nonatomic, assign) BOOL                isChangeResolution;
@property (assign, atomic) int  autoHiddenSecend;

/** 从xx秒开始播放视频跳转 */
@property (nonatomic, assign) NSInteger            seekTime;
@property (nonatomic, assign) CurrentPLAYType      currentPlayType;
@property (nonatomic, assign) BOOL     isConstraintLandscape;
@property (nonatomic,strong)NSTimer *autoHiddenTimer;
/** 是否为全屏 */
@property (nonatomic, assign) BOOL                isFullScreen;
/** 是否显示controlView*/
@property (nonatomic, assign) BOOL                isMaskShowing;

@property (nonatomic,strong)PlayerErrorManager *errorManager;

/** 是否被用户暂停 */
@property (nonatomic, assign) BOOL                isPauseByUser;

/**
 *  取消延时隐藏controlView的方法,在ViewController的delloc方法中调用
 *  用于解决：刚打开视频播放器，就关闭该页面，maskView的延时隐藏还未执行。
 */
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
;

/**
 *  单例，用于列表cell上多个视频
 *
 *  @return HTPlayer
 */
+ (instancetype)sharedPlayerView;


/**
 *  重置player
 */
- (void)resetPlayer;

/**
 *  在当前页面，设置新的Player的URL调用此方法
 */
- (void)resetToPlayNewURL;

/**
 *  播放
 */
- (void)play;

/**
 * 暂停
 */
- (void)pause;


-(void)actionPauseForVideo;

-(void)actionPlayForVideo;



@end
