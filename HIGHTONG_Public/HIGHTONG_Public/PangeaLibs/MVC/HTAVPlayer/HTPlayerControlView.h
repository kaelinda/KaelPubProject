//
//  HTPlayerControlView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemandVideoListCell.h"
#import "DemandModel.h"
#import "ShareView.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "MyGesture.h"
#import "PlayerMaskView.h"
#import "PolyMedicineLoadingView.h"
typedef void(^ChangeResolutionBlock)(UIButton *button);
typedef void(^SliderTapBlock)(CGFloat value);

@class HTPlayerControlView;

@protocol HTPlayerControlViewDelegate <NSObject>

//-(void)videoListIndexPath:(NSString*)contentID;


-(void)videoListIndexPathAssociateArr:(NSMutableArray *)associateArr andIndexPathRow:(NSInteger)indePathRow;

-(DemandModel *)getDemandModelForDemandModel:(DemandModel*)demandModel;

-(void)scrollViewDidScroll:(UIScrollView *)scrollView;

@end



@interface HTPlayerControlView : UIView<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)id<HTPlayerControlViewDelegate> delegate;
/**
 * 亮度的VIEW和声音的View
 */
@property (nonatomic,assign)CGFloat systemVODBrightness;
@property (nonatomic,strong)UIProgressView *brightnessVODProgress;
@property (nonatomic,strong)UIImageView *brightnessVODView;
@property (nonatomic,strong)UIProgressView*volumeVODProgressV;
/**
 * /加载视图
 */
@property (nonatomic,copy)PlayerMaskView *loadingMaskView;
@property (nonatomic,copy)PolyMedicineLoadingView *polyMedicineView;
/***分享View*****/
@property (nonatomic, strong) ShareView *shareView;
/**
 * 传过来的属性和字典
 */
@property (nonatomic, strong) NSString  *contentID;
@property (nonatomic, strong) NSMutableDictionary *polyMedicDic;
/** 开始播放按钮 */
@property (nonatomic, strong, readonly) UIButton                *startVODBtn;
/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton              *backwardVODBtn;
/**
 * 推荐的数组
 */
@property (nonatomic, strong) NSMutableArray        *asscciationArray;
/****收藏按钮***/
@property (nonatomic, strong) UIButton               *favorVODBtn;
/**
 * 分享按钮
 */
@property (nonatomic, strong) UIButton               *shareVODBtn;
/**
 * /推荐按钮
 */
@property (nonatomic, strong) UIButton               *videoListVODBtn;
/**
 * 推荐的底图View
 */
@property (nonatomic, strong) UIView                 *videoListVODView;
/**
 * 推荐的列表
 */
@property (nonatomic, strong) UITableView            *videoListVODTV;
/** EPG当前播放时长label */
@property (nonatomic, strong) UILabel                 *currentEPGTimeLabel;
/** EPG视频总时长label */
@property (nonatomic, strong) UILabel                 *totalEPGTimeLabel;
/** VOD当前播放时长label */
@property (nonatomic, strong) UILabel                 *currentVODTimeLabel;
/** VOD视频总时长label */
@property (nonatomic, strong) UILabel                 *totalVODTimeLabel;
@property (nonatomic, assign)     NSInteger channelSelectIndex;
@property (nonatomic, strong) UILabel                 *serviceTitleVODLabel;
/**
 * /进度条view
 */
@property (nonatomic,strong)UIImageView *progressTimeView;
@property (nonatomic,strong)UIImageView *progressDirectionIV;
@property (nonatomic,strong)UILabel *progressTimeLable_top;
@property (nonatomic,strong)UILabel *progressTimeLable_bottom;
/**
 * /锁屏
 */
@property (nonatomic,copy)UIImageView *screenLockedVODIV;//屏幕枷锁view
//锁屏按钮
@property (nonatomic,copy)UIButton *lockedVODBtn;

@property (nonatomic,strong)DemandModel *demandModel;

/** 缓冲进度条 */
@property (nonatomic, strong) UIProgressView          *progressVODView;
/** 滑杆 */
@property (nonatomic, strong) UISlider                *videoVODSlider;
/** 全屏按钮 */
@property (nonatomic, strong) UIButton                *fullScreenVODBtn;
/** 直播播放器bottomView*/
@property (nonatomic, strong) UIImageView             *epgBottomImageView;
/**点播播放器 bottomView*/
@property (nonatomic,strong) UIImageView                          *vodBottomImageView;
/**直播播放器 topView */
@property (nonatomic,strong) UIImageView                         *epgTopImageView;
/**点播播放器 topView */
@property (nonatomic, strong) UIImageView             *vodTopImageView;

/** 返回按钮*/
@property (nonatomic, strong) UIButton                *backVODBtn;
/** 切换分辨率按钮 */
@property (nonatomic, strong, readonly) UIButton                *resolutionBtn;
/** 分辨率的名称 */
@property (nonatomic, strong) NSMutableArray                           *resolutionArray;
/** 切换分辨率的block */
@property (nonatomic, copy  ) ChangeResolutionBlock             resolutionBlock;
/** 分辨率的View */
@property (nonatomic, strong) UIView                  *resolutionView;


@property (nonatomic,assign)CurrentPLAYType currentPlayType;
@property (nonatomic,assign)VODPLAYType vodPlayType;

/** 切换分辨率时候调用此方法*/
- (void)resetControlViewForResolution;
/** 重置ControlView */
- (void)resetControlView;
/** 显示top、bottom、lockBtn*/
- (void)showControlViewAndIsFullScreen:(BOOL)isFullScreen;
/** 隐藏top、bottom、lockBtn*/
- (void)hideControlViewAndIsFullScreen:(BOOL)isFullScreen;


@end
