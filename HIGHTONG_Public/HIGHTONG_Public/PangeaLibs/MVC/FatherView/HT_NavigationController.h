//
//  HT_NavigationController.h
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//



//********************************************
//    继承系统的 UINavigationController 把自定义的 NavigationBar添加到这上面  隐藏系统的 Navigation之后 就可以用自定义的了


#import <UIKit/UIKit.h>

@interface HT_NavigationController : UINavigationController


// 是否可右滑返回
- (void)navigationCanDragBack:(BOOL)bCanDragBack;



@end
