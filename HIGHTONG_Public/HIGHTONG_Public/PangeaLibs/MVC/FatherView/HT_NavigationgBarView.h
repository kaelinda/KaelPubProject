//
//  HT_NavigationgBarView.h
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//


//********************************************
//  自定义的 NavigationBar

#import <UIKit/UIKit.h>
#import "GlobalDefine.h"
#import "UtilityFunc.h"


#define kTitle_color_JX [UIColor colorWithRed:0.30 green:0.30 blue:0.30 alpha:1.00]
#define kNavTopSafeSpace ([UIScreen mainScreen].bounds.size.height == 812 ? 44 : 20)
#define kNavTopSpace (kNavTopSafeSpace - 20)

@interface HT_NavigationgBarView : UIView

@property (nonatomic, weak) UIViewController *m_viewCtrlParent;
@property (nonatomic, readonly) BOOL m_bIsCurrStateMiniMode;
@property (nonatomic, strong) UIImageView *m_imgViewBg;



/**
 最左侧按钮的尺寸

 @return 尺寸
 */
+ (CGRect)leftBtnFrame;

/**
 左侧偏右位置按钮的尺寸

 @return 尺寸
 */
+ (CGRect)LLeftBtnFrame;

/**
 最右侧按钮的尺寸

 @return 尺寸
 */
+ (CGRect)rightBtnFrame;

/**
 右侧偏左方位的按钮的尺寸

 @return 尺寸
 */
+(CGRect)rightBtnFrameSize:(CGSize)rightBtnSize;
+ (CGRect)RRightBtnFrame;
+ (CGSize)barBtnSize;
+(CGSize)rightBtnSize:(CGSize)btnSize;
+ (CGSize)barSize;
+ (CGRect)titleViewFrame;
+(CGRect)leftTitleLableFrame;
// 创建一个导航条按钮：使用默认的按钮图片。
+ (UIButton *)createNormalNaviBarBtnByTitle:(NSString *)strTitle target:(id)target action:(SEL)action;

// 创建一个导航条按钮：自定义按钮图片。
+ (UIButton *)createImgNaviBarBtnByImgNormal:(NSString *)strImg imgHighlight:(NSString *)strImgHighlight target:(id)target action:(SEL)action;
+ (UIButton *)createImgNaviBarBtnByImgNormal:(NSString *)strImg imgHighlight:(NSString *)strImgHighlight imgSelected:(NSString *)strImgSelected target:(id)target action:(SEL)action;




// 用自定义的按钮和标题替换默认内容
- (void)setLeftBtn:(UIButton *)btn;

- (void)setLLeftBtn:(UIButton *)btn;

/**
 *  设置右边按钮
 *
 *  @param btn 按钮
 */
- (void)setRightBtn:(UIButton *)btn;


-(void)setRightBtnSize:(CGSize)btnSize andRightBtn:(UIButton*)btn andBtnTitleLabelFont:(UIFont*)font;

- (void)setRRightBtn:(UIButton *)btn;


-(void)setRightBtnTitleColor:(UIColor*)titleColor;


/**
 *  设置页面标题
 *
 *  @param strTitle 标题
 */
- (void)setTitle:(NSString *)strTitle;

-(void)setLeftTitle:(NSString *)leftTItle;

-(void)setLeftTitleColor:(UIColor*)color;

/**
 *  设置搜索框
 *
 *  @param view 可以是搜索框的载体 也可以直接是搜索框
 */
-(void)setSearchBarView:(UIView *)view;

-(void)setSearchBarViewFrame:(CGRect)frame;



// 在导航条上覆盖一层自定义视图。比如：输入搜索关键字时，覆盖一个输入框在上面。
- (void)showCoverView:(UIView *)view;
- (void)showCoverView:(UIView *)view animation:(BOOL)bIsAnimation;
- (void)showCoverViewOnTitleView:(UIView *)view;
- (void)hideCoverView:(UIView *)view;






@end
