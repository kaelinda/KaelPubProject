//
//  HT_NavigationgBarView.m
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_NavigationgBarView.h"


#define FLOAT_TitleSizeNormal               21.0f
#define FLOAT_TitleSizeMini                 16.0f
#define RGB_TitleNormal                     RGB(80.0f, 80.0f, 80.0f)
#define RGB_TitleMini                       [UIColor blackColor]

@interface HT_NavigationgBarView ()

@property (nonatomic, readonly) UIButton *m_btnBack;
@property (nonatomic, readonly) UILabel *m_labelTitle;
@property (nonatomic, readonly) UIButton *m_btnLeft;
@property (nonatomic, readonly) UIButton *m_btnLLeft;
@property (nonatomic, readonly) UIButton *m_btnRight;
@property (nonatomic, readonly) UIButton *m_btnRRight;

@property (nonatomic, readonly) BOOL m_bIsBlur;


@property (nonatomic, strong) UIView *m_Searchview;
@property (nonatomic, strong) UILabel *m_LeftTitleLable;

@end


@implementation HT_NavigationgBarView

@synthesize m_btnBack = _btnBack;
@synthesize m_labelTitle = _labelTitle;
@synthesize m_imgViewBg = _imgViewBg;
@synthesize m_btnLeft = _btnLeft;
@synthesize m_btnLLeft = _btnLLeft;

@synthesize m_btnRight = _btnRight;
@synthesize m_btnRRight = _btnRRight;

@synthesize m_bIsBlur = _bIsBlur;

@synthesize m_Searchview = _searchview;
@synthesize m_LeftTitleLable = _leftTitleLable;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(CGRect)LLeftBtnFrame{

    return Rect([[self class] barBtnSize].width + [[self class] boundarySpace] + [[self class] widgetSpace], 28+kNavTopSpace, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
}

+(CGRect)leftBtnFrame{

    return Rect([[self class] boundarySpace], 28+kNavTopSpace, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
}

+(CGRect)rightBtnFrame{

//    return Rect(kDeviceWidth-40-9*kRateSize, 36, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
//UI优化后
    return Rect(kDeviceWidth-[[self class] barBtnSize].width-[[self class] boundarySpace], 28+kNavTopSpace, [[self class] barBtnSize].width, [[self class] barBtnSize].height);


}


+(CGRect)rightBtnFrameSize:(CGSize)rightBtnSize{
    
    //    return Rect(kDeviceWidth-40-9*kRateSize, 36, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
    //UI优化后
    return Rect(kDeviceWidth-[[self class] rightBtnSize:rightBtnSize].width-[[self class] boundarySpace], 15+kNavTopSpace,[[self class] rightBtnSize:rightBtnSize].width , [[self class] rightBtnSize:rightBtnSize].height);
    
    
}




+ (CGRect)RRightBtnFrame
{
//    return Rect(kDeviceWidth-[[self class] barBtnSize].width-40*kRateSize, 20, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
    //UI优化后
    return Rect(kDeviceWidth-[[self class] barBtnSize].width * 2-[[self class] widgetSpace] - [[self class] boundarySpace], 28+kNavTopSpace, [[self class] barBtnSize].width, [[self class] barBtnSize].height);

}
+(CGRect)cancelBtnFrame{

    return Rect(kDeviceWidth-(45+5)*kRateSize, 22+kNavTopSpace, [[self class] barBtnSize].width, [[self class] barBtnSize].height);

}

+ (CGSize)barBtnSize
{
    //UI优化后
    return Size(30, 28);
}


+(CGSize)rightBtnSize:(CGSize)btnSize
{
    return btnSize;
}



+ (CGSize)barSize
{
    return Size(kDeviceWidth, 64.0f+kNavTopSpace);
}

+ (CGRect)titleViewFrame
{
    CGFloat width = kDeviceWidth-8*kRateSize-100*kRateSize;
    CGFloat left = (kDeviceWidth-width)/2;

    return Rect(left, 27.0f+kNavTopSpace, width, 28.0f);
}
+(CGRect)leftTitleLableFrame{
    CGFloat width = 60*kRateSize;
    
    return Rect(50*kRateSize, 29.0f+kNavTopSpace, width, 28.0f);
}

/**
 按钮和边界的距离

 @return 按钮和边界的距离
 */
+(CGFloat)boundarySpace{
    return 13*kDeviceRate;
}

/**
 控件与控件之间的距离

 @return 控件和控件（右侧或者左侧 可能同事出现两个按钮的情况 按钮与按钮的距离）
 */
+(CGFloat)widgetSpace{
    return 15*kDeviceRate;
}

// 创建一个导航条按钮：使用默认的按钮图片。
+ (UIButton *)createNormalNaviBarBtnByTitle:(NSString *)strTitle target:(id)target action:(SEL)action
{
    UIButton *btn = [[self class] createImgNaviBarBtnByImgNormal:@"NaviBtn_Normal" imgHighlight:@"NaviBtn_Normal_H" target:target action:action];
    [btn setTitle:strTitle forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0xf6f6f6) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    btn.titleLabel.textAlignment = NSTextAlignmentRight;
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    btn.contentEdgeInsets = UIEdgeInsetsMake(5, 10, 0, 0);
    [UtilityFunc label:btn.titleLabel setMiniFontSize:8.0f forNumberOfLines:1];
    
    return btn;
}

// 创建一个导航条按钮：自定义按钮图片。
+ (UIButton *)createImgNaviBarBtnByImgNormal:(NSString *)strImg imgHighlight:(NSString *)strImgHighlight target:(id)target action:(SEL)action
{
    return [[self class] createImgNaviBarBtnByImgNormal:strImg imgHighlight:strImgHighlight imgSelected:strImg target:target action:action];
}
+ (UIButton *)createImgNaviBarBtnByImgNormal:(NSString *)strImg imgHighlight:(NSString *)strImgHighlight imgSelected:(NSString *)strImgSelected target:(id)target action:(SEL)action
{
    UIImage *imgNormal = [UIImage imageNamed:strImg];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    btn.showsTouchWhenHighlighted = YES;
    [btn setImage:imgNormal forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:(strImgHighlight ? strImgHighlight : strImg)] forState:UIControlStateHighlighted];
    [btn setImage:[UIImage imageNamed:(strImgSelected ? strImgSelected : strImg)] forState:UIControlStateSelected];
    
    CGFloat fDeltaWidth = ([[self class] barBtnSize].width - imgNormal.size.width)/2.0f;
    CGFloat fDeltaHeight = ([[self class] barBtnSize].height - imgNormal.size.height)/2.0f;
    fDeltaWidth = (fDeltaWidth >= 2.0f) ? fDeltaWidth/2.0f : 0.0f;
    fDeltaHeight = (fDeltaHeight >= 2.0f) ? fDeltaHeight/2.0f : 0.0f;
    [btn setImageEdgeInsets:UIEdgeInsetsMake(fDeltaHeight, fDeltaWidth, fDeltaHeight, fDeltaWidth)];
    
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(fDeltaHeight, -imgNormal.size.width, fDeltaHeight, fDeltaWidth)];
    
    return btn;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
//        _bIsBlur = (IsiOS7Later && Is4Inch);
        _bIsBlur = NO;
        
        [self initUI];
    }
    return self;
}



- (void)initUI
{
    self.backgroundColor = [UIColor clearColor];
    
    // 默认左侧显示返回按钮
    _btnBack = [[self class] createImgNaviBarBtnByImgNormal:@"NaviBtn_Back" imgHighlight:@"NaviBtn_Back_H" target:self action:@selector(btnBack:)];
    
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectZero];
    _labelTitle.backgroundColor = [UIColor clearColor];
    _labelTitle.textColor = UIColorFromRGB(0xf6f6f6);
    if ([APP_ID  isEqual: JXCABLE_SERIAL_NUMBER]) {
        _labelTitle.textColor = kTitle_color_JX;
        
    }
//    _labelTitle.font = [UIFont systemFontOfSize:FLOAT_TitleSizeNormal];
    _labelTitle.font = HT_FONT_FIRST;
    _labelTitle.textAlignment = NSTextAlignmentCenter;

    _leftTitleLable = [[UILabel alloc] initWithFrame:CGRectZero];
    _leftTitleLable.backgroundColor = [UIColor clearColor];
    _leftTitleLable.textColor = UIColorFromRGB(0xf6f6f6);
    _leftTitleLable.textAlignment = NSTextAlignmentLeft;
//    _leftTitleLable.font = [UIFont fontWithName:@"Thonburi" size:FLOAT_TitleSizeMini];
    
    
    _searchview = [[UIView alloc] initWithFrame:CGRectZero];
    _searchview.backgroundColor = [UIColor clearColor];
    
    
    _imgViewBg = [[UIImageView alloc] initWithFrame:self.bounds];
    _imgViewBg.image = [[UIImage imageNamed:@"bg_navgation_bar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    _imgViewBg.image = nil;
    _imgViewBg.backgroundColor = App_selected_color;
    if ([APP_ID  isEqual: JXCABLE_SERIAL_NUMBER]) {
        _imgViewBg.backgroundColor = [UIColor whiteColor];
    }
//    _imgViewBg.alpha = 0.8f;
    
    if (_bIsBlur)
    {// iOS7可设置是否需要现实磨砂玻璃效果
        _imgViewBg.alpha = 0.0f;
        UINavigationBar *naviBar = [[UINavigationBar alloc] initWithFrame:self.bounds];
        [self addSubview:naviBar];
    }else{}
    
    _labelTitle.frame = [[self class] titleViewFrame];
    _imgViewBg.frame = self.bounds;
    _leftTitleLable.frame = [[self class] leftTitleLableFrame];
    
    [self addSubview:_imgViewBg];
    [self addSubview:_searchview];
    [self addSubview:_labelTitle];
    [self addSubview:_leftTitleLable];
    
    [self setLeftBtn:_btnBack];
}

- (void)setTitle:(NSString *)strTitle
{
    [_labelTitle setText:strTitle];
    [_searchview setHidden:YES];//有标题就不能有 搜索框
    [_leftTitleLable setHighlighted:YES];
}
-(void)setLeftTitle:(NSString *)leftTItle{
    [_leftTitleLable setText:leftTItle];
    [_labelTitle setHidden:YES];//有左侧标题就不能有右侧标题
}


-(void)setLeftTitleColor:(UIColor*)color
{
    [_leftTitleLable setTextColor:color];
}


-(void)setSearchBarView:(UIView *)view{
    if (_searchview) {
        [_searchview removeFromSuperview];
        _searchview = nil;
        
    }else{}
    _searchview = view;
    if (_searchview) {
        _labelTitle.hidden = YES;//有搜索框 就不能有标题
        _searchview.frame = _labelTitle.frame;
        [self addSubview:_searchview];
    }

}
-(void)setSearchBarViewFrame:(CGRect)frame{
    _labelTitle.frame = frame;
    _searchview.frame = frame;
    
}
- (void)setLeftBtn:(UIButton *)btn
{
    if (_btnLeft)
    {
        [_btnLeft removeFromSuperview];
        _btnLeft = nil;
    }else{}
    
    _btnLeft = btn;
    if (_btnLeft)
    {
//        _btnLeft.frame = Rect(0.0f, 22.0f, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
        _btnLeft.frame = [[self class] leftBtnFrame];

        _btnLeft.showsTouchWhenHighlighted = YES;
        [self addSubview:_btnLeft];
    }else{}
}
-(void)setLLeftBtn:(UIButton *)btn{
    if (_btnLLeft)
    {
        [_btnLLeft removeFromSuperview];
        _btnLLeft = nil;
    }else{}
    
    _btnLLeft = btn;
    if (_btnLLeft)
    {
//        _btnLLeft.frame = Rect(2.0f+[[self class] barBtnSize].width, 22.0f, [[self class] barBtnSize].width, [[self class] barBtnSize].height);
        _btnLLeft.frame = [[self class] LLeftBtnFrame];

        [self addSubview:_btnLLeft];
    }else{}

}

- (void)setRightBtn:(UIButton *)btn
{
    if (_btnRight)
    {
        [_btnRight removeFromSuperview];
        _btnRight = nil;
    }else{}
    
    _btnRight = btn;
    if (_btnRight)
    {
        _btnRight.frame = [[self class] rightBtnFrame];
        
        _btnRight.titleLabel.center = CGPointMake(_btnRight.size.width/2, _btnRight.size.height/2);
        
        if (isAfterIOS7) {
            _btnRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }else{
            
            _btnRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }
        [self addSubview:_btnRight];
    }else{}
    

    
    }


-(void)setRightBtnSize:(CGSize)btnSize andRightBtn:(UIButton*)btn andBtnTitleLabelFont:(UIFont*)font{
    
    
    if (_btnRight)
    {
        [_btnRight removeFromSuperview];
        _btnRight = nil;
    }else{}
    
    _btnRight = btn;
    if (_btnRight)
    {
        _btnRight.size = btnSize;
        
        _btnRight.center = CGPointMake(kDeviceWidth - btnSize.width/2 - [[self class] boundarySpace] , 42);
        
        if (isAfterIOS7) {
            _btnRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }else{
            
            _btnRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }
        
        _btnRight.titleLabel.font = font;
        
        [self addSubview:_btnRight];
    }else{}
    
}


-(void)setRRightBtn:(UIButton *)btn{
    
    if (_btnRRight)
    {
        [_btnRRight removeFromSuperview];
        _btnRRight = nil;
    }else{}
    
    _btnRRight = btn;
    if (_btnRRight)
    {
        _btnRRight.frame = [[self class] RRightBtnFrame];
        
        _btnRRight.titleLabel.center = CGPointMake(_btnRRight.size.width/2, _btnRRight.size.height/2);
        
        if (isAfterIOS7) {
            _btnRRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }else{
            
            _btnRRight.titleLabel.textAlignment = NSTextAlignmentCenter;
            
        }
        [self addSubview:_btnRRight];
    }else{}

    
  
}


-(void)setRightBtnTitleColor:(UIColor*)titleColor
{
    if (_btnRight) {
        
        [_btnRight setTitleColor:titleColor forState:UIControlStateNormal];
    }
}


- (void)btnBack:(id)sender
{
    if (self.m_viewCtrlParent)
    {
        [self.m_viewCtrlParent.navigationController popViewControllerAnimated:YES];
    }else{APP_ASSERT_STOP}
}

- (void)showCoverView:(UIView *)view
{
    [self showCoverView:view animation:NO];
}
- (void)showCoverView:(UIView *)view animation:(BOOL)bIsAnimation
{
    if (view)
    {
        [self hideOriginalBarItem:YES];
        
        [view removeFromSuperview];
        
        view.alpha = 0.4f;
        [self addSubview:view];
        if (bIsAnimation)
        {
            [UIView animateWithDuration:0.2f animations:^()
             {
                 view.alpha = 1.0f;
             }completion:^(BOOL f){}];
        }
        else
        {
            view.alpha = 1.0f;
        }
    }else{APP_ASSERT_STOP}
}

- (void)showCoverViewOnTitleView:(UIView *)view
{
    if (view)
    {
        if (_labelTitle)
        {
            _labelTitle.hidden = YES;
        }else{}
        
        [view removeFromSuperview];
        view.frame = _labelTitle.frame;
        
        [self addSubview:view];
    }else{APP_ASSERT_STOP}
}

- (void)hideCoverView:(UIView *)view
{
    [self hideOriginalBarItem:NO];
    if (view && (view.superview == self))
    {
        [view removeFromSuperview];
    }else{}
}

#pragma mark -
- (void)hideOriginalBarItem:(BOOL)bIsHide
{
    if (_btnLeft)
    {
        _btnLeft.hidden = bIsHide;
    }else{}
    if (_btnBack)
    {
        _btnBack.hidden = bIsHide;
    }else{}
    if (_btnRight)
    {
        _btnRight.hidden = bIsHide;
    }else{}
    if (_labelTitle)
    {
        _labelTitle.hidden = bIsHide;
    }else{}
}

@end
