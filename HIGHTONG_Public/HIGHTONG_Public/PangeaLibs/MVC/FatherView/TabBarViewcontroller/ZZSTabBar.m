//
//  ZZSTabBar.m
//  ProgramFramDemo
//
//  Created by Kael on 15/7/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ZZSTabBar.h"


#import "ZZSButton.h"
@interface ZZSTabBar ()<zzsTabBarDelegate>


@property (nonatomic, weak) UIButton *selectButton;//记录 当前点击的按钮
@property (nonatomic, strong) NSMutableArray *arrButton;


@end


@implementation ZZSTabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if ([[UIDevice currentDevice].systemVersion integerValue]<=7.0) {
            self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_foot_01"]];
            
//            self.backgroundColor = [UIColor blueColor];
            
        }else{
            self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_foot_01"]];
        }
    }
    return self;
}





-(void)addButtonWithItem:(UITabBarItem *)item{
    
    // 创建按钮
    ZZSButton *button = [[ZZSButton alloc]init];
    [self addSubview:button];
    [self.arrButton addObject:button];
    
    button.item = item;
    // 点击监听
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchDown];
    
    if (self.arrButton.count == 1) {
        [self clickButton:button];
    }


}

- (void)clickButton:(UIButton *)button
{
    // 通知代理点击了哪个
    if ([self.delegate respondsToSelector:@selector(zzsTabBar:didSelectedButtonfrom:To:)]) {
        
        [self.delegate zzsTabBar:self didSelectedButtonfrom:self.selectButton.tag To:button.tag];
    }
    
    
    // 点击按钮状态改变
    self.selectButton.selected = NO;
    button.selected = YES;
    self.selectButton = button;
}
-(NSMutableArray *)arrButton{

    if (_arrButton == nil) {
        _arrButton = [[NSMutableArray alloc] init];
//        _arrButton = [NSMutableArray array];
    }
    return _arrButton;
}

// 子视图布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    //NSLog(@"%@",self.subviews);
    // 调整加号按钮的位置
    CGFloat h = self.frame.size.height;
    CGFloat w = self.frame.size.width;
    
    // 按钮的frame数据
    CGFloat buttonH = h;
    CGFloat buttonW = w / self.subviews.count;
    CGFloat buttonY = 0;
    
    for (int index = 0; index<self.arrButton.count; index++) {
        // 1.取出按钮
        ZZSButton *button = self.arrButton[index];
        
        // 2.设置按钮的frame
        CGFloat buttonX = index * buttonW;
        
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        
        // 3.绑定tag
        button.tag = index;
        
    }
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/





@end
