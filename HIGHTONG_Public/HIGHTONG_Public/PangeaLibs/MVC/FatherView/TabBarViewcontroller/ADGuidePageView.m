//
//  ADGuidePageView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/27.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ADGuidePageView.h"

#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

@interface ADGuidePageView()
{
    BOOL _ISSaveADImage;
    
    CGFloat  KWidth;
    CGFloat  KHigh;
    
}

/**
 动画广告视图
 */
@property (nonatomic,strong) FLAnimatedImageView *gifADView;


@end

@implementation ADGuidePageView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //初始化进出数据
        [self initData];
        //添加子视图
        [self creatSubViews];
        [self setBackgroundColor:CLEARCOLOR];
    }
    return self;
}

-(instancetype)init{
    self = [super init];

    if (self) {
        NSAssert(NO, @"请使用 initWithFrame 方法进行初始化");
    }
    return self;
}
-(void)ADListNotification:(NSNotification *)noti{

    ///---------- 获取广告链接
    NSArray *adlist = [[NSArray alloc] initWithArray:noti.object];
    NSString *picLink;
    if (adlist.count > 0) {
        picLink= [[adlist firstObject] objectForKey:@"picLink"];
        if (picLink.length<=1) {
            picLink = @"";
        }
    }
    
    //----------- 加载广告
    if (adlist.count == 1  && [picLink rangeOfString:@".gif"].location != NSNotFound) {
        NSString *URLStr;// = @"http://s.jktv.tv/images/new_year_ad.gif";
        if (picLink.length>0) {
            URLStr = picLink;
        }
        [self testGifADWithURLString: NSStringPM_IMGFormat(URLStr)];
    }else{
        [self loadingADList:adlist];
    }

    
}

#pragma mark - 数据源初始化 和数据源重置
-(void) initData{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ADListNotification:) name:@"AD_Noti" object:nil];

    
    //默认是引导页类型的
    _viewType = kGuideViewType;
    //初始化尺寸
    KWidth = self.frame.size.width;
    KHigh = self.frame.size.height;
    //初始化数据源空间
    _guidePagesArray = [NSMutableArray array];
    _ADListArray = [NSMutableArray array];

}
-(void)loadingADList:(NSArray *)adList{
    
    if (_guidePagesArray.count>0) {
        [UIApplication sharedApplication].statusBarHidden = NO;

        return;
    }
    if (adList.count<=0) {
        self.hidden = YES;
        return;
    }
    self.hidden = NO;
    _viewType = kADViewType;
    _ADListArray = [NSMutableArray arrayWithArray:adList];
    if (adList.count==0) {
        return;
    }
    [self creatADListView];
    
    
}

-(void)loadingGuidePages:(NSArray *)guidePages{
    _viewType = kGuideViewType;
    _guidePagesArray = [NSMutableArray arrayWithArray:guidePages];
    if (guidePages.count==0) {
        return;
    }
    [self creatGuidePageView];
    
    
}

#pragma mark - 界面初始化
-(void)creatSubViews{

    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        NSLog(@"第一次启动");
        //第一次启动 需要初始化的是 引导页
        _viewType = kGuideViewType;
        //引导页数组
        NSArray *guideList = @[@"guide-01",@"guide-02",@"guide-03"];
        [self loadingGuidePages:guideList];
        
    }else{
        NSLog(@"不是第一次启动");
        //等待广告页的 回调结果
        _viewType = kADViewType;
        self.backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"default-736h"]];
        self.backgroundImage.contentMode = UIViewContentModeScaleToFill;
        self.backgroundImage.frame = self.frame;
        [self addSubview:self.backgroundImage];
        
        [self setHidden:NO];


    }


}
-(void)creatGuidePageView{

    [UIApplication sharedApplication].statusBarHidden = YES;
    [self setBackgroundColor:CLEARCOLOR];
    
    if (_guideScrollView == nil) {
        _guideScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KWidth, KHigh)];
        _guideScrollView.delegate = self;
        _guideScrollView.contentSize = CGSizeMake(KWidth*(_guidePagesArray.count+1), KHigh);
        _guideScrollView.pagingEnabled = YES;
        _guideScrollView.bounces = NO;
        _guideScrollView.alwaysBounceHorizontal = NO;
        _guideScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _guideScrollView.showsHorizontalScrollIndicator = NO;
        [_guideScrollView setBackgroundColor:CLEARCOLOR];
        [self addSubview:_guideScrollView];

    }

    _guideScrollView.hidden = NO;
    
    for (int i=0; i<_guidePagesArray.count; i++) {
        UIImageView *guidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*KWidth, 0, KWidth, KHigh)];
        guidImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_guidePagesArray objectAtIndex:i]]];
        
        [_guideScrollView addSubview:guidImageView];
        
        if (i == (_guidePagesArray.count-1)) {
            guidImageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoMainPage)];
            tap.numberOfTapsRequired = 1;
            [guidImageView addGestureRecognizer:tap];
        }
        
    }
    
    [self.superview bringSubviewToFront:self];
}
- (void)creatADListView{
    [UIApplication sharedApplication].statusBarHidden = YES;
    [self setBackgroundColor:App_white_color];
    if (_ADScrollView == nil) {
        _ADScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KWidth, KHigh)];
        _ADScrollView.delegate = self;
        _ADScrollView.contentSize = CGSizeMake(KWidth*(_ADListArray.count), KHigh);
        _ADScrollView.pagingEnabled = YES;
        _ADScrollView.bounces = NO;
        _ADScrollView.alwaysBounceHorizontal = NO;
        _ADScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _ADScrollView.showsHorizontalScrollIndicator = NO;
        [_ADScrollView setBackgroundColor:CLEARCOLOR];
        [self addSubview:_ADScrollView];
        
    }
    _ADScrollView.hidden = NO;
    
    [_ADScrollView removeAllSubviews];

    NSInteger showTimeNum = 0;
    for (int i=0; i<[_ADListArray count]; i++) {
        UIImageView *guidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*kDeviceWidth, 0, kDeviceWidth, KDeviceHeight)];
        
        NSString *imageURL = IS_SUPPORT_ADList ? [[_ADListArray objectAtIndex:i] objectForKey:@"picLink"] : ([[[_ADListArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"picLink"]);
        showTimeNum  = (IS_SUPPORT_ADList ? [[[_ADListArray objectAtIndex:i] objectForKey:@"duration"] integerValue] : [[[[_ADListArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue]) +showTimeNum;

        UIImage *img;
        if (KDeviceHeight == 480) {
            
            img = [UIImage imageNamed:@"default-320h"];
        }else{
            img = [UIImage imageNamed:@"default-736h"];
        }
        
        [guidImageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageURL)] placeholderImage:img completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error) {
                [self gotoMainPage];
            }else{
                _adTimerBackImageView.hidden = NO;
                [self reStartADTimer];//这是个内循环 只是操纵label 改变数值 不跟外界交互
                [self reStartGotoTimer];//这个只是操纵页面跳转
            }
            
        }];
        NSLog(@"AD_Link------>>:%@",[NSURL URLWithString:NSStringPM_IMGFormat(imageURL)]);
        [_ADScrollView addSubview:guidImageView];
 
        guidImageView.userInteractionEnabled = YES;
        
        MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(gotoWebView:)];
        tap.urlLink = IS_SUPPORT_ADList ? [[_ADListArray objectAtIndex:i] objectForKey:@"urlLink"] : [[[_ADListArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"urlLink"];
        tap.numberOfTapsRequired = 1;
        [guidImageView addGestureRecognizer:tap];
        
    }
    
    
    //-----------
    _adTimerBackImageView = [[UIImageView alloc] init];
    _adTimerBackImageView.frame = CGRectMake(kDeviceWidth-10-105*kRateSize, 20, 90 * kRateSize, 30 * kRateSize);
    [_adTimerBackImageView setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.7]];
    _adTimerBackImageView.layer.cornerRadius = 3;
    _adTimerBackImageView.userInteractionEnabled = YES;
    _adTimerBackImageView.hidden = YES;
    [self addSubview:_adTimerBackImageView];
    
    if (_timeLabel == nil) {
        CGSize timeSize = CGSizeMake(20*kRateSize, 30*kRateSize);
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.frame = CGRectMake(0, 0, timeSize.width, timeSize.height);
        _timeLabel.font = [UIFont systemFontOfSize:18*kRateSize];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.text = [NSString stringWithFormat:@"%ld  ",showTimeNum];
        [_timeLabel setBackgroundColor:CLEARCOLOR];
        [_timeLabel setTextColor:UIColorFromRGB(0xffffff)];
        [_adTimerBackImageView addSubview:_timeLabel];
        //跳过广告 >
        CGSize skipSize = CGSizeMake(70*kRateSize, 30*kRateSize);
        
        _SkipADLabel = [[UILabel alloc] init];
        _SkipADLabel.font = [UIFont boldSystemFontOfSize:13*kRateSize];
        _SkipADLabel.textAlignment = NSTextAlignmentLeft;
        _SkipADLabel.text = @" 跳过广告>";
        [_SkipADLabel setBackgroundColor:CLEARCOLOR];
        [_SkipADLabel setTextColor:App_selected_color];
        [_adTimerBackImageView addSubview:_SkipADLabel];
        
        [_SkipADLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(skipSize);
            make.left.mas_equalTo(_timeLabel.mas_right).offset(-0.5);
            make.centerY.mas_equalTo(_timeLabel.mas_centerY);
        }];
        
        _SkipADLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *gotoHomeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoMainPage)];
        gotoHomeTap.numberOfTapsRequired = 1;
        [_SkipADLabel addGestureRecognizer:gotoHomeTap];
    }
    //-------------
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.backgroundImage removeFromSuperview];
    });


}
#pragma mark - 重启定时器
//再次展现广告页
-(void)continueADImage{
    if (_viewType == kGuideViewType) {
        return;
    }
    [UIApplication sharedApplication].statusBarHidden = YES;

    [self reStartADTimer];
    [self reStartGotoTimer];
    
}

-(void)reStartADTimer{
    if (_ADTimer == nil) {
        
        _ADTimer  = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timeLabelTextChange) userInfo:nil repeats:YES];
    }
}
-(void)reStartGotoTimer{
    if (_gotoTimer == nil) {
        
        NSInteger JumpSpace = IS_SUPPORT_ADList ? [[[_ADListArray objectAtIndex:0] objectForKey:@"duration"] integerValue] : [[[[_ADListArray objectAtIndex:0] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue];
        if (JumpSpace == 0) {
            JumpSpace =2;
        }
        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(JumpSpace * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _gotoTimer = [NSTimer scheduledTimerWithTimeInterval:JumpSpace target:self selector:@selector(gotoNextScrollPage) userInfo:nil repeats:YES];
//        });
        
    }else{
        [_gotoTimer invalidate];
        _gotoTimer = nil;
        NSInteger index = _ADOffset.x/kDeviceWidth+1;
        if (index>_ADListArray.count-1) {
            index = _ADListArray.count-1;
        }
        NSString *recicleTime = [NSString stringWithFormat:@"%@",IS_SUPPORT_ADList ? [[_ADListArray objectAtIndex:index] objectForKey:@"duration"] : [[[_ADListArray objectAtIndex:index] objectForKey:@"pictureRes"] objectForKey:@"duration"]];
        NSInteger JumpSpace = [recicleTime integerValue];
        if (JumpSpace <= 0) {
            JumpSpace =2;
        }
        
        _gotoTimer = [NSTimer scheduledTimerWithTimeInterval:JumpSpace target:self selector:@selector(gotoNextScrollPage) userInfo:nil repeats:YES];
        
        
    }
    
}
#pragma mark - 定时器控制的 数值变动 和 偏移量变动 操作

-(void)gotoNextScrollPage{
    if (_timeLabel.hidden || _ADScrollView.hidden) {
        
        return;
    }
    if (_ADScrollView.hidden) {
        NSLog(@"跳转了？");
        return;
    }
    
    CGSize contentSize = _ADScrollView.contentSize;
    if ((contentSize.width-kDeviceWidth)==_ADScrollView.contentOffset.x) {
        [self gotoMainPage];
        return;
    }
    _ADScrollView.contentOffset = CGPointMake(_ADOffset.x+kDeviceWidth, _ADOffset.y);
    [self reStartGotoTimer];
}

-(void)timeLabelTextChange{
    NSInteger timeInt = [_timeLabel.text integerValue];
    if (timeInt>0) {
        timeInt--;
        
    }else{
        [self gotoMainPage];
        //        _timeLabel.text = @"3";
    }
    NSString *currentTime = [NSString stringWithFormat:@"%ld  ",timeInt];
    _timeLabel.text = currentTime;
    
}


#pragma mark - 界面跳转操作
-(void)gotoWebView:(MyGesture *)tap{
    if (_adTimerBackImageView.hidden) {
        return;
    }
    if (tap.urlLink.length==0) {
        return;
    }
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_IMAGEVIEW" object:nil];
    
    [_ADTimer invalidate];
    _ADTimer = nil;
    
    [_gotoTimer invalidate];
    _gotoTimer = nil;
    
    _ISSaveADImage = YES;
    
    
    [_guideScrollView removeFromSuperview];
    if (_gotoWebViewBlock) {
        _gotoWebViewBlock(tap.urlLink);
    }

}

-(void)gotoMainPage{
    _adTimerBackImageView.hidden = YES;
    [UIApplication sharedApplication].statusBarHidden = NO;

    [_ADTimer invalidate];
    _ADTimer = nil;
    
    [_gotoTimer invalidate];
    _gotoTimer = nil;
    
    
    [self setBackgroundColor:CLEARCOLOR];
    if (_guideScrollView) {
        [self setHidden:YES];
        [_guideScrollView removeFromSuperview];
    }else{
        [KaelTool scaleBigAndHiddenAnamition:_ADScrollView andTime:0.75 andScale:1.5 andAnimationComplet:^{
            [self setHidden:YES];
            [self removeFromSuperview];
        }];
        [KaelTool scaleBigAndHiddenAnamition:_SkipADLabel andTime:0.75 andScale:1.5 andAnimationComplet:^{
            [self setHidden:YES];
            [self removeFromSuperview];
        }];
        
    }
    
    
    [_timeLabel removeFromSuperview];
    
    if (_gotoMainView) {
        _gotoMainView();
    }
    

}


#pragma mark - UIScrollView delegate
// scrollView 已经滑动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScroll");
    //    _timeLabel.text = @"4";
    _ADOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);
    //    [self timeLabelTextChange];
    if (_ADOffset.x<0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }
    
}

// scrollView 开始拖动
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDragging");
    [_ADTimer invalidate];
    _ADTimer = nil;
    
    [_gotoTimer invalidate];
    _gotoTimer = nil;
    
    
}

// scrollView 结束拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"scrollViewDidEndDragging");
    if (_ADScrollView) {
        [self reStartADTimer];
        [self reStartGotoTimer];
    }else if (_guideScrollView && _ADOffset.x > (_guidePagesArray.count-1)*kDeviceWidth) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [self gotoMainPage];
            
        });
    }
    
}

// scrollView 开始减速（以下两个方法注意与以上两个方法加以区别）
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDecelerating");
}

// scrollview 减速停止
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidEndDecelerating");
    
    if (_ADOffset.x!=scrollView.contentOffset.x) {
        NSInteger index = _ADOffset.x/kDeviceWidth+1;
        if (index>_ADListArray.count-1) {
            index = _ADListArray.count-1;
        }
        NSString *duration = IS_SUPPORT_ADList ? [[_ADListArray objectAtIndex:index] objectForKey:@"duration"] : [[[_ADListArray objectAtIndex:index] objectForKey:@"pictureRes"] objectForKey:@"duration"];
        _timeLabel.text =[NSString stringWithFormat:@"%@  ",duration];
        [self reStartGotoTimer];
        
        _ADOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);
        
    }
    if (scrollView.contentOffset.x<KWidth) {
        [UIView animateWithDuration:0.35 animations:^{
            scrollView.contentOffset = CGPointMake(0, 0);
            _ADOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);
        }];
        

    }
    
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (_ADOffset.x<0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }

}

-(void)setHidden:(BOOL)hidden{
    [super setHidden:hidden];

    
}


#pragma mark - 测试专用

-(void)testGifADWithURLString:(NSString *)urlStr{

    NSData *gifData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
    FLAnimatedImage *gifImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:gifData];
    
    _gifADView = [[FLAnimatedImageView alloc] init];
    _gifADView.frame = CGRectMake(0, 0, KWidth, KHigh);
    _gifADView.animatedImage = gifImage;
    
    [self addSubview:_gifADView];
    
    
    __weak typeof(&*_gifADView)weakImageView = _gifADView;
    __weak typeof(&*self)weakSelf = self;
    
    _gifADView.loopCompletionBlock = ^(NSUInteger loopCountRemaining){
        weakSelf.backgroundImage.image = nil;

        //跳转到项目中
        [weakSelf gotoMainPage];
        //移除动画视图
        [weakImageView removeFromSuperview];
        
    };
    
}




@end
