//
//  ADGuidePageView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/27.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyGesture.h"

typedef NS_ENUM(NSUInteger, ADGuideViewType) {
    kADViewType,
    kGuideViewType,
};


typedef void(^gotoBlock)(NSString *url);

@interface ADGuidePageView : UIView<UIScrollViewDelegate>

@property (nonatomic,assign) ADGuideViewType viewType;

/**
 *  @author Kael
 *
 *  @brief 背景图
 */
@property (nonatomic,strong) UIImageView *backgroundImage;

/**
 *  @author Kael
 *
 *  @brief 广告倒计时背景色
 */
@property (nonatomic,strong) UIImageView *adTimerBackImageView;
/**
 *  @author Kael
 *
 *  @brief 倒计时时间
 */
@property (nonatomic,strong) UILabel *timeLabel;

/**
 *  @author Kael
 *
 *  @brief 跳过按钮
 */
@property (nonatomic,strong) UILabel *SkipADLabel;

/**
 *  @author Kael
 *
 *  @brief 广告页
 */
@property (nonatomic,strong) UIScrollView *ADScrollView;//广告图

/**
 *  @author Kael
 *
 *  @brief 引导页
 */
@property (nonatomic,strong) UIScrollView *guideScrollView;//引导图

/**
 *  @author Kael
 *
 *  @brief 广告页视图偏移量
 */
@property (nonatomic,assign) CGPoint ADOffset;//记录广告的偏移量

/**
 *  @author Kael
 *
 *  @brief 广告时间控制器 只是控制广告时间的显示器上的变化
 */
@property (nonatomic,strong) NSTimer *ADTimer;//广告时间

/**
 *  @author Kael
 *
 *  @brief 每隔一段时间检测下显示器上的时间 和 广告数组中的数值进行对比 符合一定条件才跳转
 */
@property (nonatomic,strong) NSTimer *gotoTimer;//跳转定时器

/**
 *  @author Kael
 *
 *  @brief 引导页数组
 */
@property (nonatomic,strong) NSMutableArray *guidePagesArray;

/**
 *  @author Kael
 *
 *  @brief 广告页数组
 */
@property (nonatomic,strong) NSMutableArray *ADListArray;

/**
 *  @author Kael
 *
 *  @brief 跳转到广告页的回调
 */
@property (nonatomic,copy)  void (^gotoWebViewBlock)(NSString *url);

/**
 *  @author Kael
 *
 *  @brief 跳转到主界面的回调
 */
@property (nonatomic,copy) void(^gotoMainView)();

/**
 *  @author Kael
 *
 *  @brief 载入引导页
 *  @param guidePages 引导页图片数组
 */
- (void)loadingGuidePages:(NSArray *)guidePages;

/**
 *  @author Kael
 *
 *  @brief 载入广告页数组
 *  @param adList 广告页数组 （网络请求的返回结果）
 */
- (void)loadingADList:(NSArray *)adList;

/**
 *  @author Kael
 *
 *  @brief 重启定时器 广告页的倒计时重新开始计时
 */
-(void)continueADImage;







@end
