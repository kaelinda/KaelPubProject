//
//  TabBarViewontroller.m
//  HIGHTONG
//
//  Created by Kael on 15/7/26.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "TabBarViewontroller.h"
#import "BufferLivewController.h"
#import "LiveHomeViewController.h"
#import "HTVideoAVPlayerVC.h"
//这是要动真格的了
#import "HT_NavigationController.h"

#import "HomeViewController.h"
//#import "LiveViewControl.h"
#import "DemandViewController.h"
#import "ScrollDemandPageViewController.h"
#import "BufferDemandeViewController.h"
#import "MineViewcontrol.h"
#import "CSStickyHeaderFlowLayout.h"

//#import "HT_FatherViewController.h"
#import "MyGesture.h"
#import "AboutUSViewController.h"
#import "CustomAlertView.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandVideoPlayerVC.h"

#import "AppDelegate.h"
#import "NewHomeViewController.h"
#import "YJHomeViewController.h"
#import "JXCablePublicHome.h"
#import "JktvHomeViewController.h"
#import "ExtranetPromptViewController.h"
#import "JKYSHHomeViewController.h"
#import "HealthVodViewController.h"
#import "JXCabelDemandMenuVC.h"

@implementation TabBarViewontroller


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        //第一次启动是引导图
        [UIApplication sharedApplication].statusBarHidden = YES;
        
    }else{
        
    }

    if (_ISSaveADImage && _adGuideView) {
//        [self continueADImage];
        [_adGuideView continueADImage];
        _ISSaveADImage = NO;

    }

}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    KWidth = (kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight);
    KHigh  = (KDeviceHeight>kDeviceWidth?KDeviceHeight:kDeviceWidth);
      //修改 tabBaritem字体 未选中时颜色
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       UIColorFromRGB(0x697888), NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = App_selected_color;//tabBaritem 选中按钮 颜色
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateSelected];
    //页面跳转时 通知中心的  观察者 “pagechange”
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageChangeNotification:) name:@"pagechange" object:nil];
//这个通知已经迁移到了ADGudiePageView里面了
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ADListNotification:) name:@"AD_Noti" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoOrderPage:) name:@"LOCAL_NOTIFICATION_TO_TABVC" object:nil];
    //初始化 tabBar
    [self setupCustomTabBar];
    // 初始化所以子控制器
    [self setupAllChildViewControls];
    
    //初始化广告页 和引导图
    [self initADListScrollView];
    [UIApplication sharedApplication].statusBarHidden = YES;
}
-(void)initADListScrollView{
    WS(wself);

    _adGuideView = [[ADGuidePageView alloc] initWithFrame:CGRectMake(0, 0, KWidth, KHigh)];
    _adGuideView.gotoWebViewBlock = ^(NSString *url){
        //跳转到广告页
        NSLog(@"跳转到 广告页 ---->> %@",url);
        AboutUSViewController *webView = [[AboutUSViewController alloc] init];
        webView.urlLink = url;
        _ISSaveADImage = YES;
        [wself.navigationController pushViewController:webView animated:NO];
        
    };
    _adGuideView.gotoMainView = ^(){
        //跳转到主界面
        NSLog(@"跳转到主视图 --->>");
        
    };
    [self.view addSubview:_adGuideView];
    
}
- (void)pageChangeNotification:(NSNotification *)text{
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:text.object];
    NSInteger index = [[dic objectForKey:@"index"] integerValue];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setSelectedIndex:index];
    });
    
    if (IS_PUBLIC_ENV){
        HT_FatherViewController *fVC = [self.navigationController.viewControllers lastObject];
        
        ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
        //            extranetVC.ExtranetPromptTitle = @"江西有线";
        [fVC.navigationController pushViewController:extranetVC animated:YES];
        
    }

    NSLog(@"－－－－－接收到通知------%ld",(long)index);
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    
    
    
}

-(void)setupCustomTabBar{
    [self.tabBar setBackgroundColor:[UIColor whiteColor]];
    [self.tabBar setBackgroundImage:[UIImage new]];
}
- (void)setupAllChildViewControls{
    
    self.pagetype = kApp_HomePageType;
    switch (self.pagetype) {
        case kHomePageType_JXCable_v1_0:// 老版本的爱上TV
        {
            JXCablePublicHome *jxCableHome = [[JXCablePublicHome alloc] init];
            LiveHomeViewController *buffer = [[LiveHomeViewController alloc] init];
//            ScrollDemandPageViewController *Scrolldemand     = [[ScrollDemandPageViewController alloc] init];
            //全新改版点播一级界面
            JXCabelDemandMenuVC *demandMenuVC = [[JXCabelDemandMenuVC alloc]init];
            MineViewcontrol *mine         = [[MineViewcontrol alloc] init];
            
            [self addChildViewControl:jxCableHome title:@"首页" imageName:@"sl_main_foot_home_normal" selectedImageName:@"sl_main_foot_home_pressed"];
            [self addChildViewControl:buffer title:@"直播" imageName:@"sl_main_foot_live_normal" selectedImageName:@"sl_main_foot_live_pressed"];
            
//            [self addChildViewControl:Scrolldemand title:@"点播" imageName:@"sl_main_foot_vod_normal" selectedImageName:@"sl_main_foot_vod_pressed"];
            [self addChildViewControl:demandMenuVC title:@"点播" imageName:@"sl_main_foot_vod_normal" selectedImageName:@"sl_main_foot_vod_pressed"];

            [self addChildViewControl:mine title:@"我的" imageName:@"sl_main_foot_user_normal" selectedImageName:@"sl_main_foot_user_pressed"];
        }
            
            break;
            
        case kHomePageType_JKTV_v1_1:// 七个大块的版本 （已废弃）
            
        {
            JktvHomeViewController *jktvHome = [[JktvHomeViewController alloc]init];
            MineViewcontrol         *mine    = [[MineViewcontrol alloc] init];
            
            [self addChildViewControl:jktvHome title:@"首页" imageName:@"sl_main_foot_home_normal" selectedImageName:@"sl_main_foot_home_pressed"];
            
            [self addChildViewControl:mine title:@"我的" imageName:@"sl_main_foot_user_normal" selectedImageName:@"sl_main_foot_user_pressed"];
        }
            
            break;
            
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        {
            JKYSHHomeViewController *jkyshHome = [[JKYSHHomeViewController alloc]init];
            
            HealthVodViewController *healthVod = [[HealthVodViewController alloc]init];
            
            MineViewcontrol         *mine      = [[MineViewcontrol alloc] init];
            
            [self addChildViewControl:jkyshHome title:@"首页" imageName:@"sl_main_foot_home_normal" selectedImageName:@"sl_main_foot_home_pressed"];
            
            [self addChildViewControl:healthVod title:@"健康" imageName:@"sl_main_foot_health_normal" selectedImageName:@"sl_main_foot_health_pressed"];
            
            [self addChildViewControl:mine title:@"我的" imageName:@"sl_main_foot_user_normal" selectedImageName:@"sl_main_foot_user_pressed"];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HEALTHVOD"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
            
            break;
        case kHomePageType_JKTV_V1_5:{
            JKYSHHomeViewController *jkyshHome = [[JKYSHHomeViewController alloc]init];
                        
            HealthVodViewController *healthVod = [[HealthVodViewController alloc]init];
            
            MineViewcontrol         *mine      = [[MineViewcontrol alloc] init];
            
            [self addChildViewControl:jkyshHome title:@"首页" imageName:@"sl_main_foot_home_normal" selectedImageName:@"sl_main_foot_home_pressed"];
            
            
            [self addChildViewControl:healthVod title:@"健康" imageName:@"sl_main_foot_health_normal" selectedImageName:@"sl_main_foot_health_pressed"];
            
            [self addChildViewControl:mine title:@"我的" imageName:@"sl_main_foot_user_normal" selectedImageName:@"sl_main_foot_user_pressed"];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HEALTHVOD"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
            break;
        default:
            break;
    }

}
/**
 *  添加子控制器
 *
 *  @param childVc           子控制器
 *  @param title             标题
 *  @param imageName         图片
 *  @param selectedImageName 被选中图片
 */
- (void)addChildViewControl:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    // 设置标题图片
    childVc.title = title;
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    if (isAfterIOS7) {
        childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else {
        childVc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImageName];
    }
    
    
    if ([childVc isKindOfClass:[HomeViewController class]]) {
        
        
        
    }
    
    
    
    // 添加到导航控制器
//    HT_NavigationController *NavC = [[HT_NavigationController alloc] initWithRootViewController:childVc];
    [self addChildViewController:childVc];
    
        // 添加自定义item
//    [self.CustomtabBar addButtonWithItem:childVc.tabBarItem];
}



#pragma mark - zzstabBar delegate

-(void)zzsTabBar:(ZZSTabBar *)tabBar didSelectedButtonfrom:(NSInteger)from To:(NSInteger)to{
//页面之间传值可能会用到这个方法
    
    if ([APP_ID isEqualToString:@"562172033"]) {
        if (IS_PUBLIC_ENV &&(to == 1 || to == 2)) {
            
            ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
//            extranetVC.ExtranetPromptTitle = @"江西有线";
            [self.selectedViewController.navigationController pushViewController:extranetVC animated:YES];
            
        }
    }
    
}



- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
//    return [self.selectedViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{

    return UIInterfaceOrientationPortrait;

//    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
}


#pragma mark ---- 本地通知 页面跳转
-(void)gotoOrderPage:(NSNotification *)noti{

    NSMutableDictionary *notiItem = [[NSMutableDictionary alloc] initWithDictionary:noti.object];
    
    NSLog(@"预定消息：%@",notiItem);
    
    AboutUSViewController *webView = [[AboutUSViewController alloc] init];
    webView.urlLink = @"";

    /*
     
     {
     endTime = "2015-11-06 22:21:00";
     eventID = 842533;
     eventName = "\U5e7f\U5ba3\U65f6\U6bb5";
     imageLink = "http://192.168.3.60:8080/taibiao/phone/s/btv.png";
     levelImageLink = "http://192.168.3.60:8080/taibiao/phone/l/21.png";
     serviceID = 21;
     serviceName = "BTV\U5317\U4eac\U536b\U89c6";
     startTime = "2015-11-06 22:17:00";
     }
     
     */
    
    HT_FatherViewController *vvv = [self.navigationController.viewControllers lastObject];
    if ([vvv isKindOfClass:[EpgSmallVideoPlayerVC class]] || [vvv isKindOfClass:[DemandVideoPlayerVC class]] || [vvv isKindOfClass:[HTVideoAVPlayerVC class]]) {
        
        NSLog(@"1 预定 到时了 你该跳转界面了  %@",noti.object);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCALORDERNOTI_TO_LIVE" object:self userInfo:noti.object];
    }else{
        NSLog(@"2 预定 到时了 你该跳转界面了  %@",noti.object);
        EpgSmallVideoPlayerVC *livePlayerPage = [[EpgSmallVideoPlayerVC alloc] init];
        livePlayerPage.serviceID = [noti.object objectForKey:@"serviceID"];
        livePlayerPage.eventURLID = [noti.object objectForKey:@"eventID"];
        livePlayerPage.eventStartTime = [noti.object objectForKey:@"startTime"];
        livePlayerPage.eventEndTime = [noti.object objectForKey:@"endTime"];
        livePlayerPage.serviceName = [noti.object objectForKey:@"serviceName"];


        [vvv.navigationController pushViewController:livePlayerPage animated:NO];
    }
    
}


#pragma mark ---

-(void)imageAnimationAction{
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstStart"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstStart"];
        NSLog(@"第一次启动");
        [self guidePage];
    }else{
        NSLog(@"不是第一次启动");
        [self ADPage];
        
    }
    
}
#pragma mark - 引导页
-(void)continueADImage{
    [self reStartADTimer];
    [self reStartGotoTimer];

}
-(void)guidePage{
//    [UIApplication sharedApplication].statusBarHidden = YES;
    
    
    _guideScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KWidth, KHigh)];
    _guideScrollView.delegate = self;
    _guideScrollView.contentSize = CGSizeMake(KWidth*4, KHigh);
    _guideScrollView.pagingEnabled = YES;
    _guideScrollView.bounces = YES;
    _guideScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _guideScrollView.showsHorizontalScrollIndicator = NO;
    [_guideScrollView setBackgroundColor:CLEARCOLOR];
    
    [self.view addSubview:_guideScrollView];
    
    for (int i=0; i<4; i++) {
        UIImageView *guidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*KWidth, 0, KWidth, KHigh)];
        guidImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide-0%d",i+1]];
        
        [_guideScrollView addSubview:guidImageView];
        
        if (i==2) {
            guidImageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoMainPage)];
            tap.numberOfTapsRequired = 1;
            [guidImageView addGestureRecognizer:tap];
        }
       
    }
    
    
    
    [self.view bringSubviewToFront:_guideScrollView];
}

-(void)gotoWebView:(MyGesture *)tap{
    if (tap.urlLink.length==0) {
        return;
    }
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_IMAGEVIEW" object:nil];
    
    [_ADTimer invalidate];
    _ADTimer = nil;
    
    [_gotoTimer invalidate];
    _gotoTimer = nil;
    
    _ISSaveADImage = YES;

    
    [_guideScrollView removeFromSuperview];
//    [_timeLabel removeFromSuperview];
    
    AboutUSViewController *webView = [[AboutUSViewController alloc] init];
    webView.urlLink = tap.urlLink;
    
    [self.navigationController pushViewController:webView animated:NO];

}
-(void)gotoMainPage{

    [UIApplication sharedApplication].statusBarHidden = NO;

    [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_IMAGEVIEW" object:nil];
    [_ADTimer invalidate];
    _ADTimer = nil;

    [_gotoTimer invalidate];
    _gotoTimer = nil;

    [GoBtn setHidden:YES];
    [_guideScrollView setHidden:YES];
    [KaelTool scaleBigAndHiddenAnamition:_ADScrollView andTime:0.75 andScale:1.3 andAnimationComplet:^{
        [_timeLabel setHidden:YES];
        [_SkipADLabel setHidden:YES];
        
        [_guideScrollView removeFromSuperview];
        [_timeLabel removeFromSuperview];
    }];

    
}

-(void)timeLabelTextChange{
    NSInteger timeInt = [_timeLabel.text integerValue];
    if (timeInt>0) {
        timeInt--;
        
    }else{
        [self gotoMainPage];
//        _timeLabel.text = @"3";
    }
    NSString *currentTime = [NSString stringWithFormat:@"%ld  ",timeInt];
    _timeLabel.text = currentTime;

}

#pragma mark - 广告图
-(void)ADListNotification:(NSNotification *)noti{
    self.view.hidden = NO;
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstTime"];
        NSLog(@"第一次启动");
        return;
    }else{
        NSLog(@"不是第一次启动");
        
        
    }
    
    _ISSaveADImage = NO;
    
    _ADArray= [[NSArray alloc] initWithArray:noti.object];
    if (_ADArray.count==0) {
        [self gotoMainPage];
        return;
    }
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    if (_ADScrollView == nil) {
        _ADScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
        _ADScrollView.delegate = self;
        _ADScrollView.contentSize = CGSizeMake(kDeviceWidth*_ADArray.count, KDeviceHeight);
        _ADScrollView.pagingEnabled = YES;
        _ADScrollView.bounces = NO;
        _ADScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _ADScrollView.showsHorizontalScrollIndicator = NO;
        _ADScrollView.scrollEnabled = NO;
        [_ADScrollView setBackgroundColor:[UIColor whiteColor]];
        
        [self.view addSubview:_ADScrollView];
    }
    
    
    
    
    [_ADScrollView removeAllSubviews];
    NSInteger showTimeNum = 1;
    for (int i=0; i<[_ADArray count]; i++) {
        UIImageView *guidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*kDeviceWidth, 0, kDeviceWidth, KDeviceHeight)];
        
        NSString *imageURL = IS_SUPPORT_ADList ? [[_ADArray objectAtIndex:i] objectForKey:@"picLink"] : ([[[_ADArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"picLink"]);
        showTimeNum = IS_SUPPORT_ADList ? [[[_ADArray objectAtIndex:i] objectForKey:@"duration"] integerValue] : [[[[_ADArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue]+showTimeNum;
        [guidImageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageURL)] placeholderImage:nil];
        [_ADScrollView addSubview:guidImageView];
        
//        //最后一张上面加个按钮  现在不需要了
//        CGSize btnSize = CGSizeMake(70*kRateSize, 30*kRateSize);
//        if (i==([_ADArray count]-1)) {
//            
//            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            [btn setImage:[UIImage imageNamed:@"btn_ad_skip"] forState:UIControlStateNormal];
//            [btn addTarget:self action:@selector(gotoMainPage) forControlEvents:UIControlEventTouchUpInside];
//            btn.frame = CGRectMake([ADArray count]*kDeviceWidth-10*kRateSize-btnSize.width, KDeviceHeight-20*kRateSize-btnSize.height, btnSize.width, btnSize.height);
//            [_ADScrollView addSubview:btn];
//            
//        }
        
        guidImageView.userInteractionEnabled = YES;
        
        MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(gotoWebView:)];
        tap.urlLink = IS_SUPPORT_ADList ? [[_ADArray objectAtIndex:i] objectForKey:@"urlLink"] : [[[_ADArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"urlLink"];
        tap.numberOfTapsRequired = 1;
        [guidImageView addGestureRecognizer:tap];
        
    }
    
    
    //-----------
    if (_timeLabel == nil) {
        CGSize timeSize = CGSizeMake(20*kRateSize, 30*kRateSize);
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.frame = CGRectMake(kDeviceWidth-10-105*kRateSize, 20, timeSize.width, timeSize.height);
        _timeLabel.font = [UIFont systemFontOfSize:18*kRateSize];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.text = [NSString stringWithFormat:@"%ld  ",showTimeNum];
        [_timeLabel setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.7]];
        [_timeLabel setTextColor:UIColorFromRGB(0xffffff)];
        [self.view addSubview:_timeLabel];
        //跳过广告 >
        CGSize skipSize = CGSizeMake(70*kRateSize, 30*kRateSize);
        
        _SkipADLabel = [[UILabel alloc] init];
        _SkipADLabel.font = [UIFont boldSystemFontOfSize:13*kRateSize];
        _SkipADLabel.textAlignment = NSTextAlignmentLeft;
        _SkipADLabel.text = @" 跳过广告>";
        [_SkipADLabel setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.7]];
        [_SkipADLabel setTextColor:App_selected_color];
        [self.view addSubview:_SkipADLabel];
        
        [_SkipADLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(skipSize);
            make.left.mas_equalTo(_timeLabel.mas_right).offset(-0.5);
            make.centerY.mas_equalTo(_timeLabel.mas_centerY);
        }];
        
        _SkipADLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *gotoHomeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoMainPage)];
        gotoHomeTap.numberOfTapsRequired = 1;
        [_SkipADLabel addGestureRecognizer:gotoHomeTap];
    }
    //-------------
    
    [self reStartADTimer];//这是个内循环 只是操纵label 改变数值 不跟外界交互
    [self reStartGotoTimer];//这个只是操纵页面跳转
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        //这里是1.0f后 你要做的事情
    ////        [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_IMAGEVIEW" object:nil];
    //
    //    });
    
}


-(void)gotoNextScrollPage{
    if (_timeLabel.hidden||_ADScrollView.hidden) {
        
        return;
    }
    if (_ADScrollView.hidden) {
        NSLog(@"跳转了？");
        return;
    }

    CGSize contentSize = _ADScrollView.contentSize;
    if ((contentSize.width-kDeviceWidth)==_ADScrollView.contentOffset.x) {
        [self gotoMainPage];
        return;
    }
    _ADScrollView.contentOffset = CGPointMake(_ADOffset.x+kDeviceWidth, _ADOffset.y);
    [self reStartGotoTimer];
}
-(void)reStartGotoTimer{
    if (_gotoTimer == nil) {
        
        NSInteger JumpSpace =IS_SUPPORT_ADList ? [[[_ADArray objectAtIndex:0] objectForKey:@"duration"] integerValue] : [[[[_ADArray objectAtIndex:0] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue];
        if (JumpSpace == 0) {
            JumpSpace =2;
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(JumpSpace * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _gotoTimer = [NSTimer scheduledTimerWithTimeInterval:JumpSpace+1 target:self selector:@selector(gotoNextScrollPage) userInfo:nil repeats:YES];
        });

    }else{
        [_gotoTimer invalidate];
        _gotoTimer = nil;
        NSInteger index = _ADOffset.x/kDeviceWidth+1;
        if (index>_ADArray.count-1) {
            index = _ADArray.count-1;
        }
        NSString *recicleTime = [NSString stringWithFormat:@"%@",IS_SUPPORT_ADList ? [[_ADArray objectAtIndex:index] objectForKey:@"duration"] : [[[_ADArray objectAtIndex:index] objectForKey:@"pictureRes"] objectForKey:@"duration"]];
        NSInteger JumpSpace = [recicleTime integerValue];
        if (JumpSpace <= 0) {
            JumpSpace =2;
        }
       
        _gotoTimer = [NSTimer scheduledTimerWithTimeInterval:JumpSpace target:self selector:@selector(gotoNextScrollPage) userInfo:nil repeats:YES];

    
    }

}

-(void)reStartADTimer{
    if (_ADTimer == nil) {

        _ADTimer  = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timeLabelTextChange) userInfo:nil repeats:YES];
    }
}

-(void)ADPage{
    return;
    NSArray *ADArray= [[NSArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ADArray"]];
    if (_ADScrollView == nil) {
        _ADScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
        _ADScrollView.delegate = self;
        _ADScrollView.contentSize = CGSizeMake(kDeviceWidth*ADArray.count, KDeviceHeight);
        _ADScrollView.pagingEnabled = YES;
        _ADScrollView.bounces = NO;
        _ADScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _ADScrollView.showsHorizontalScrollIndicator = NO;
        [_ADScrollView setBackgroundColor:[UIColor whiteColor]];
        
        [self.view addSubview:_ADScrollView];
    }
    
    
    
    
    [_ADScrollView removeAllSubviews];
    NSInteger showTimeNum = 1;
    for (int i=0; i<[ADArray count]; i++) {
        UIImageView *guidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*kDeviceWidth, 0, kDeviceWidth, KDeviceHeight)];
        
        NSString *imageURL = IS_SUPPORT_ADList ? [[ADArray objectAtIndex:i] objectForKey:@"picLink"] : [[[ADArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"picLink"];
        showTimeNum = IS_SUPPORT_ADList ? [[[ADArray objectAtIndex:i] objectForKey:@"duration"] integerValue] : [[[[ADArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue]+showTimeNum;
        //        [guidImageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"guide-01"]];
        [guidImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageURL)] placeholderImage:nil];
        [_ADScrollView addSubview:guidImageView];
        
        if (i==([ADArray count]-1)) {
            
            //            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            //            [btn setImage:[UIImage imageNamed:@"btn_ad_skip"] forState:UIControlStateNormal];
            //            [btn addTarget:self action:@selector(gotoMainPage) forControlEvents:UIControlEventTouchUpInside];
            //            btn.frame = CGRectMake([ADArray count]*kDeviceWidth-10*kRateSize-btnSize.width, KDeviceHeight-20*kRateSize-btnSize.height, btnSize.width, btnSize.height);
            //            [_ADScrollView addSubview:btn];
            
            
        }
        guidImageView.userInteractionEnabled = YES;
        
        MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(gotoWebView:)];
        tap.urlLink = [[ADArray objectAtIndex:i] objectForKey:@"urlLink"];
        tap.numberOfTapsRequired = 1;
        [guidImageView addGestureRecognizer:tap];
        
    }
//    [self.view bringSubviewToFront:_ADScrollView];
    
}

// scrollView 已经滑动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScroll");
//    _timeLabel.text = @"4";
    _ADOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);
//    [self timeLabelTextChange];
    if (_ADOffset.x<0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }
    
}

// scrollView 开始拖动
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDragging");
    [_ADTimer invalidate];
    _ADTimer = nil;
    
    [_gotoTimer invalidate];
    _gotoTimer = nil;

    
}

// scrollView 结束拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"scrollViewDidEndDragging");
    if (_ADScrollView) {
        [self reStartADTimer];
        [self reStartGotoTimer];
    }else if (_guideScrollView && _ADOffset.x>2*kDeviceWidth) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [self gotoMainPage];

        });
    }

}

// scrollView 开始减速（以下两个方法注意与以上两个方法加以区别）
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDecelerating");
}

// scrollview 减速停止
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidEndDecelerating");

    if (_ADOffset.x!=scrollView.contentOffset.x) {
        NSInteger index = _ADOffset.x/kDeviceWidth+1;
        if (index>_ADArray.count-1) {
            index = _ADArray.count-1;
        }
        NSString *duration = IS_SUPPORT_ADList ? [[_ADArray objectAtIndex:index] objectForKey:@"duration"] : [[[_ADArray objectAtIndex:index] objectForKey:@"pictureRes"] objectForKey:@"duration"];
        _timeLabel.text =[NSString stringWithFormat:@"%@  ",duration];
        [self reStartGotoTimer];
        
        _ADOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);
 
    }
    
   
    
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{

}

@end
