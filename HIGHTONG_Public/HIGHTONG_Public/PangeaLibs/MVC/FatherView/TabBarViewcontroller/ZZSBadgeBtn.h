//
//  ZZSBadgeBtn.h
//  ProgramFramDemo
//
//  Created by Kael on 15/7/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZZSBadgeBtn : UIButton
@property (nonatomic, copy) NSString *badgeValue;

@end
