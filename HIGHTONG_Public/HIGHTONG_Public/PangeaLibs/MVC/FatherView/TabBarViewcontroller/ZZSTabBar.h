//
//  ZZSTabBar.h
//  ProgramFramDemo
//
//  Created by Kael on 15/7/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//


//这是我自己定制的 TabBar 

#import <UIKit/UIKit.h>

@class ZZSTabBar;
@protocol zzsTabBarDelegate <NSObject>
@optional
- (void)zzsTabBar:(ZZSTabBar *)tabBar didSelectedButtonfrom:(NSInteger)from To:(NSInteger)to;

@end


@interface ZZSTabBar : UIView
@property (nonatomic,weak) id<zzsTabBarDelegate> delegate;

/********** 下面的俩方法暂时 弃用 ***********/
//- (void)addButtonWithItem:(UITabBarItem *)item andTitle:(NSString *)title;//暂时 弃用
//- (void)addButtonWithItemWith:(NSString *)title;//暂时 弃用

/**
 *  添加 tabBarItem
 *
 *  @param item UITabBarItem
 */
- (void)addButtonWithItem:(UITabBarItem *)item;



@end
