//
//  ZZSButton.h
//  ProgramFramDemo
//
//  Created by Kael on 15/7/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  ZZSButton;
@protocol zzsBtnDelegate <NSObject>

- (void)zzsBtnDelegate:(ZZSButton *)tabBar didSelectedButtonfrom:(NSInteger)from to:(NSInteger)to;


@optional



@end


@interface ZZSButton : UIButton


@property (nonatomic,assign)id<zzsBtnDelegate> delegate;

@property (nonatomic, strong) UITabBarItem *item;



@end
