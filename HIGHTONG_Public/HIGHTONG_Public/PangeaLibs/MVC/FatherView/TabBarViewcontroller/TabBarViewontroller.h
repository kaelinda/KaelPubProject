//
//  TabBarViewontroller.h
//  HIGHTONG
//
//  Created by Kael on 15/7/26.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZZSTabBar.h"
#import "ADGuidePageView.h"


@interface TabBarViewontroller : UITabBarController<zzsTabBarDelegate,UIScrollViewDelegate>
{
    UIButton *GoBtn;
    BOOL _ISSaveADImage;

    CGFloat  KWidth;
    CGFloat  KHigh;
    ADGuidePageView *_adGuideView;


}


@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,strong) UILabel *SkipADLabel;
@property (nonatomic,strong) ZZSTabBar *CustomtabBar;

@property (nonatomic,strong) UIScrollView *ADScrollView;//广告图
@property (nonatomic,strong) UIScrollView *guideScrollView;//引导图
@property (nonatomic,assign) CGPoint ADOffset;//记录广告的偏移量
@property (nonatomic,strong) NSTimer *ADTimer;//广告时间
@property (nonatomic,strong) NSTimer *gotoTimer;//跳转定时器
@property (nonatomic,strong) NSArray *ADArray;
/**
 *  首页类型
 */
@property (nonatomic,assign) HomePageType pagetype;


@end
