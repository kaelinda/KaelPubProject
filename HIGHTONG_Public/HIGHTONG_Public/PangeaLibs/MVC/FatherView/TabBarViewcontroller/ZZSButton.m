//
//  ZZSButton.m
//  ProgramFramDemo
//
//  Created by Kael on 15/7/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ZZSButton.h"
#import "ZZSBadgeBtn.h"

#define ios7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)


@interface ZZSButton ()
{


}

// 数字提醒
@property (nonatomic, weak) ZZSBadgeBtn *badgeButton;

@end


@implementation ZZSButton


-(instancetype)initWithFrame:(CGRect)frame{

   self = [super initWithFrame:frame];
    if (self) {
        //这里可以自定义一些试图的
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        UIColor *titleColor = (ios7 ? [UIColor blackColor]:[UIColor whiteColor]);
        [self setTitleColor:titleColor forState:UIControlStateNormal];
        [self setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];

        // 提醒数字按钮
        ZZSBadgeBtn *badgeButton = [[ZZSBadgeBtn alloc]init];
        badgeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:badgeButton];
        self.badgeButton = badgeButton;
        
    }
    return self;
    

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
