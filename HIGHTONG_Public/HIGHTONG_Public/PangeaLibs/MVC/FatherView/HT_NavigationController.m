//
//  HT_NavigationController.m
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_NavigationController.h"
//#import "LiveViewControl.h"
#import "TabBarViewontroller.h"
#import "BufferLivewController.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandVideoPlayerVC.h"
#import "HTVideoAVPlayerVC.h"
#import "HTVideoAVPlayerVC.h"
@interface HT_NavigationController ()

@end




@implementation HT_NavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
//        self.wantsFullScreenLayout = YES;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    

}


// 是否可右滑返回
- (void)navigationCanDragBack:(BOOL)bCanDragBack
{
    if (isAfterIOS7)
    {
        self.interactivePopGestureRecognizer.enabled = bCanDragBack;
    }else{}
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBarHidden:NO];   // 使导航条有效
    [self.navigationBar setHidden:YES]; // 隐藏导航条，但由于导航条有效，系统的返回按钮页有效，所以可以使用系统的右滑返回手势。
    

    
}


- (BOOL)shouldAutorotate
{
    if ([self.topViewController isKindOfClass:[TabBarViewontroller class]]) {
        
        return YES;
    }else if ([self.topViewController isKindOfClass:[EpgSmallVideoPlayerVC class]] || [self.topViewController isKindOfClass:[DemandVideoPlayerVC class]] || [self.topViewController isKindOfClass:[HTVideoAVPlayerVC class]]){
        
        return [self.topViewController shouldAutorotate];
        
    }else{
        
        return NO;
    }
}
- (NSUInteger)supportedInterfaceOrientations
{

    if ([[self topViewController] isKindOfClass:[HTVideoAVPlayerVC class]]) {
        return [self.topViewController supportedInterfaceOrientations];
    }
    
    if([[self topViewController] isKindOfClass:[TabBarViewontroller class]])
        return UIInterfaceOrientationMaskPortrait;
    else
    {
        
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    
    if ([self.topViewController isKindOfClass:[TabBarViewontroller class]]) {
        
      
        
    }


    
//    return UIDeviceOrientationPortrait;
    
    return toInterfaceOrientation != UIDeviceOrientationPortraitUpsideDown;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
