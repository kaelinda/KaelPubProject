//
//  BONavigaitonView.h
//  HIGHTONG
//
//  Created by 赖利波 on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface BONavigaitonView : UIView
@property (nonatomic,strong) UIView *naviBackView;
@property (nonatomic,strong) UIView *statusView;
@property (nonatomic,strong) UIImageView *naviBarIV;
@end
