//
//  HT_FatherViewController.m
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface HT_FatherViewController ()
@end

@implementation HT_FatherViewController

@synthesize m_viewNaviBar = _viewNaviBar;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.wantsFullScreenLayout = YES;
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.wantsFullScreenLayout = YES;
}


-(void) initLoadingView{
    _customeHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _customeHUD.customView.backgroundColor = [UIColor whiteColor];
    _customeHUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    
    _customeHUD.mode = MBProgressHUDModeCustomView;
    _customeHUD.animationType = MBProgressHUDAnimationFade;
        _customeHUD.delegate = self;
    //    _customeHUD.color = [UIColor clearColor];//这儿表示无背景
    //    _customeHUD.labelText = @"正在搜索设备...";
    [_customeHUD hide:YES];
    
//        [_customeHUD show:YES];
    //
//        [_customeHUD hide:YES afterDelay:13];

}
-(void)showCustomeHUDAddTo:(UIView *)superView{

    if (_customeHUD) {
        [self hiddenCustomHUD];
    }
    _customeHUD = nil;
    
    _customeHUD = [MBProgressHUD showHUDAddedTo:superView animated:YES];
    //    _customeHUD.customView.backgroundColor = [UIColor redColor];
    //    _customeHUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    FLAnimatedImageView *animationView = [[FLAnimatedImageView alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gif_loading_pageCenter" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:gifData];
    animationView.animatedImage = image;
    animationView.frame = CGRectMake(0, 0, 100, 100);
    _customeHUD.customView = animationView;
    _customeHUD.mode = MBProgressHUDModeCustomView;
    _customeHUD.animationType = MBProgressHUDAnimationFade;
    _customeHUD.delegate = self;
    _customeHUD.color = [UIColor clearColor];//这儿表示无背景
    
    [_customeHUD show:YES];
}
-(void)showCustomeHUD{
    if (_customeHUD) {
        [self hiddenCustomHUD];
    }
    _customeHUD = nil;
 
    _customeHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

//    _customeHUD.customView.backgroundColor = [UIColor redColor];
//    _customeHUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    FLAnimatedImageView *animationView = [[FLAnimatedImageView alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gif_loading_pageCenter" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:gifData];
    animationView.animatedImage = image;
    animationView.frame = CGRectMake(0, 0, 100, 100);
    _customeHUD.customView = animationView;
    _customeHUD.mode = MBProgressHUDModeCustomView;
    _customeHUD.animationType = MBProgressHUDAnimationFade;
    _customeHUD.delegate = self;
    _customeHUD.color = [UIColor clearColor];//这儿表示无背景
    
    [_customeHUD show:YES];    
}

-(void)showCustomeHUDAndHiddenAfterDelay:(CGFloat)delay{
    if (_customeHUD) {
        [self hiddenCustomHUD];
    }
    _customeHUD = nil;
    _customeHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    _customeHUD.superview.frame = CGRectMake(0, 64, kDeviceWidth, KDeviceHeight);
//    _customeHUD.customView.backgroundColor = [UIColor whiteColor];
//    _customeHUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    FLAnimatedImageView *animationView = [[FLAnimatedImageView alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gif_loading_pageCenter" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:gifData];
    animationView.animatedImage = image;
    animationView.frame = CGRectMake(0, 0, 100, 100);
    _customeHUD.customView = animationView;
    _customeHUD.mode = MBProgressHUDModeCustomView;
    _customeHUD.animationType = MBProgressHUDAnimationFade;
    
    _customeHUD.delegate = self;

//    _customeHUD.dimBackground = YES;
    _customeHUD.color = [UIColor clearColor];//这儿表示无背景
    
    [_customeHUD show:YES];
    [_customeHUD hide:YES afterDelay:delay];
}

-(void)hiddenCustomHUD{

    dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });
    
}
-(void)hiddenCustomHUDFromView:(UIView *)superView{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:superView animated:YES];
    });
}
-(void)hiddenCustomHUDAfterDelay:(CGFloat)delay{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:App_background_color];
    
    _viewNaviBar = [[HT_NavigationgBarView alloc] initWithFrame:Rect(0.0f, 0.0f, [HT_NavigationgBarView barSize].width, [HT_NavigationgBarView barSize].height)];
    _viewNaviBar.m_viewCtrlParent = self;
    [self.view addSubview:_viewNaviBar];
}

- (void)dealloc
{
    [UtilityFunc cancelPerformRequestAndNotification:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_viewNaviBar && !_viewNaviBar.hidden)
    {
        [self.view bringSubviewToFront:_viewNaviBar];
    }else{}
    
}
- (BOOL)prefersHomeIndicatorAutoHidden {
    return YES;
}

#pragma mark -
-(void)setNaviBarMini{
    if (_viewNaviBar) {
    
        _viewNaviBar.frame = Rect(0.0f, 0.0f, [HT_NavigationgBarView barSize].width, 20);
        _viewNaviBar.m_imgViewBg.frame = Rect(0.0f, 0.0f, [HT_NavigationgBarView barSize].width, 20);
    }
}
- (void)bringNaviBarToTopmost
{
    if (_viewNaviBar)
    {
        [self.view bringSubviewToFront:_viewNaviBar];
    }else{}
}

- (void)hideNaviBar:(BOOL)bIsHide
{
    _viewNaviBar.hidden = bIsHide;
}

- (void)setNaviBarTitle:(NSString *)strTitle
{
    if (_viewNaviBar)
    {
        [_viewNaviBar setTitle:strTitle];
    }else{APP_ASSERT_STOP}
}
-(void)setNaviLeftTitle:(NSString *)leftTitle{
    if (_viewNaviBar)
    {
        [_viewNaviBar setLeftTitle:leftTitle];
    }else{APP_ASSERT_STOP}

    
}

- (void)setNaviBarLeftBtn:(UIButton *)btn
{
    if (_viewNaviBar)
    {
        [_viewNaviBar setLeftBtn:btn];
    }else{APP_ASSERT_STOP}
}

- (void)setNaviBarRightBtn:(UIButton *)btn
{
    if (_viewNaviBar)
    {
        [_viewNaviBar setRightBtn:btn];
    }else{APP_ASSERT_STOP}
}





- (void) setNaviBarRRightBtn:(UIButton *)btn{
    if (_viewNaviBar)
    {
        [_viewNaviBar setRRightBtn:btn];
    }else{APP_ASSERT_STOP}


}

-(void)setNaviBarRightBtnSize:(CGSize)btnSize andRightBtn:(UIButton*)rightBtn andRightBtnTitleLabelFont:(UIFont*)font{
    if (_viewNaviBar) {
        
        [_viewNaviBar setRightBtnSize:btnSize andRightBtn:rightBtn andBtnTitleLabelFont:font];
    }else
    {
        APP_ASSERT_STOP
    }
}

- (void)naviBarAddCoverView:(UIView *)view
{
    if (_viewNaviBar && view)
    {
        [_viewNaviBar showCoverView:view animation:YES];
    }else{}
}

- (void)naviBarAddCoverViewOnTitleView:(UIView *)view
{
    if (_viewNaviBar && view)
    {
        [_viewNaviBar showCoverViewOnTitleView:view];
    }else{}
}

- (void)naviBarRemoveCoverView:(UIView *)view
{
    if (_viewNaviBar)
    {
        [_viewNaviBar hideCoverView:view];
    }else{}
}
-(void)setNaviBarSearchBar:(BOOL)isHiden{


}
-(void)setNaviBarSearchView:(UIView *)view{

    if (_viewNaviBar) {
        if (view) {
            [_viewNaviBar setSearchBarView:view];
        }
    }else{}
    
}

-(void)setNaviBarSearchViewFrame:(CGRect)frame{
    if (_viewNaviBar) {
        
        [_viewNaviBar setSearchBarViewFrame:frame];
        
    }else{}

}


-(void)setNaviBarBackgroundColor:(UIColor *)color{
    if (_viewNaviBar) {
        if (isAfterIOS7) {
            [_viewNaviBar setTintColor:color];
        }else{
            [_viewNaviBar setBackgroundColor:color];
        }
    }else{}
}



-(void)setNaviLeftTitleColor:(UIColor*)color
{
    if ((_viewNaviBar)) {
        
        [_viewNaviBar setLeftTitleColor:color];
    }
}


-(void)setNaviRightBtnTitleColor:(UIColor*)color
{
    if ((_viewNaviBar)) {
        
        [_viewNaviBar setRightBtnTitleColor:color];
    }
}


-(void)setNaviBarBackgroundImage:(UIImage *)image{

    if (_viewNaviBar && image) {
        _viewNaviBar.m_imgViewBg.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
    }else{}
    
}



// 是否可右滑返回
- (void)navigationCanDragBack:(BOOL)bCanDragBack
{
    if (self.navigationController)
    {
        [((HT_NavigationController *)(self.navigationController)) navigationCanDragBack:bCanDragBack];
    }else{}
}



#pragma mark - 显示或者隐藏 tabBar的方法

//这个方法是应用iOS 6、7+系统
-(void)makeTabBarHidden:(BOOL)hide {
    // Custom code to hide TabBar
    if ( [self.tabBarController.view.subviews count] < 2 ) {
        return;
    }
    
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    } else {
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    }
    
    if (hide) {
        contentView.frame = self.tabBarController.view.bounds;
    } else {
        contentView.frame = CGRectMake(self.tabBarController.view.bounds.origin.x,
                                       self.tabBarController.view.bounds.origin.y,
                                       self.tabBarController.view.bounds.size.width,
                                       self.tabBarController.view.bounds.size.height -
                                       self.tabBarController.tabBar.frame.size.height);
    }
    
    self.tabBarController.tabBar.hidden = hide;
}


- (void)showTabBar

{
    if (self.tabBarController.tabBar.hidden == NO)
    {
        return;
    }
    UIView *contentView;
    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]])
        
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    
    else
        
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = NO;
    
}

- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = YES;
    
}

-(void)setStatusBarStyle:(UIStatusBarStyle)style{

    [[UIApplication sharedApplication] setStatusBarStyle:style];
}

#pragma mark - 横竖屏
-(void)changeOrientation:(UIInterfaceOrientation)orientation{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:orientation] forKey:@"orientation"];

    });
}

#pragma mark - 动画效果 POP
-(void)POPWithCATAnimationGoBack{
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade , kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
    
}
-(void)POPWithanimationGoback{
    
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    //动画有这几种    UIViewAnimationTransitionNone, UIViewAnimationTransitionFlipFromLeft, UIViewAnimationTransitionFlipFromRight,  UIViewAnimationTransitionCurlUp,  UIViewAnimationTransitionCurlDown, [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}




- (BOOL)shouldAutorotate{
           
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
   
    return UIInterfaceOrientationPortrait;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
