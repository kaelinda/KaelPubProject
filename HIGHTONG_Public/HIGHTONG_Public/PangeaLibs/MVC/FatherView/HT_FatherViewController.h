//
//  HT_FatherViewController.h
//  HIGHTONG
//
//  Created by Kael on 15/7/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.


//************************************
//  继承这个类 自带 navigationBar

#import <UIKit/UIKit.h>
#import "HT_NavigationgBarView.h"
#import "HT_NavigationController.h"
#import "LoadingView.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
@interface HT_FatherViewController : UIViewController<MBProgressHUDDelegate>
{

}
@property (nonatomic, strong) HT_NavigationgBarView *m_viewNaviBar;
@property (nonatomic, strong) MBProgressHUD *customeHUD;

-(void)showCustomeHUDAddTo:(UIView *)superView;
/**
 *  显示loading
 */
-(void)showCustomeHUD;
/**
 *  显示loading 然后几秒钟后消失
 *
 *  @param delay 显示的时间
 */
-(void)showCustomeHUDAndHiddenAfterDelay:(CGFloat)delay;
/**
 *  隐藏loading
 */
-(void)hiddenCustomHUD;
-(void)hiddenCustomHUDFromView:(UIView *)superView;
/**
 *  几秒终止后 隐藏loading
 *
 *  @param delay 延时隐藏时间
 */
-(void)hiddenCustomHUDAfterDelay:(CGFloat)delay;

-(void)setNaviBarMini;

/**
 *  把自定义的 NavigationBar 带到最前台
 */
- (void)bringNaviBarToTopmost;
/**
 *  隐藏 NavigationBar
 *
 *  @param bIsHide 是否隐藏
 */
- (void)hideNaviBar:(BOOL)bIsHide;
/**
 *  设置 页面标题
 *
 *  @param strTitle 标题
 */
- (void)setNaviBarTitle:(NSString *)strTitle;

- (void)setNaviLeftTitle:(NSString *)leftTitle;

/**
 *  设置导航左侧按钮
 *
 *  @param btn 按钮 不显示的时候可以传值为 nil
 */
- (void)setNaviBarLeftBtn:(UIButton *)btn;

/**
 *  设置导航右侧按钮
 *
 *  @param btn 按钮 不显示的时候可以传值为 nil
 */
- (void)setNaviBarRightBtn:(UIButton *)btn;//右侧单按钮
- (void)setNaviBarRRightBtn:(UIButton *)btn;//右侧双按钮时，右侧第二按钮

-(void)setNaviBarRightBtnSize:(CGSize)btnSize andRightBtn:(UIButton*)rightBtn andRightBtnTitleLabelFont:(UIFont*)font;


/**
 *  增加遮盖视图
 *
 *  @param view 遮盖视图
 */
- (void)naviBarAddCoverView:(UIView *)view;

/**
 *  在titleView上添加视图 遮挡
 *
 *  @param view 遮挡视图
 */
- (void)naviBarAddCoverViewOnTitleView:(UIView *)view;

/**
 *  移除遮挡
 *
 *  @param view 需要移除的目标
 */
- (void)naviBarRemoveCoverView:(UIView *)view;

/**
 *  隐藏搜索条
 *
 *  @param isHiden 是否隐藏
 */
-(void)setNaviBarSearchBar:(BOOL)isHiden;

/**
 *  给导航条添加一个UIView （该项目中新添加的只有SearchView）
 *
 *  @param view 需要添加的视图（载体）这是添加的所有元素的载体  也就是说需要添加的元素 要添加到某一个View上 然后把最终的这个View传入
 */
-(void)setNaviBarSearchView:(UIView *)view;
/**
 *  重置searchBar的frame 不调用这个方法采用默认居中的方案
 *
 *  @param frame searchbar的新frame
 */
-(void)setNaviBarSearchViewFrame:(CGRect)frame;

/**
 *  设置背景颜色
 *
 *  @param color 颜色 （UIColor）
 */
-(void)setNaviBarBackgroundColor:(UIColor *)color;


-(void)setNaviLeftTitleColor:(UIColor*)color;


-(void)setNaviRightBtnTitleColor:(UIColor*)color;


/**
 *  设置背景图片 （根据实际情况选择）
 *
 *  @param image 背景图片 （UIImage）
 */
-(void)setNaviBarBackgroundImage:(UIImage *)image;//设置背景图片

/**
 *  是否可右滑返回
 *
 */
- (void)navigationCanDragBack:(BOOL)bCanDragBack;

/**
 *  让tabBar隐藏或者显示 是英语iOS 6、7+
 *
 *  @param hide 是否隐藏
 */
-(void)makeTabBarHidden:(BOOL)hide;

/**
 *  显示tabBar
 */
- (void)showTabBar;
/**
 *  隐藏tabBar
 */
- (void)hideTabBar;

/**
 设置状态条的style

 @param style 状态条style
 */
-(void)setStatusBarStyle:(UIStatusBarStyle)style;

/**
 更换屏幕方向

 @param orientation 旋屏方向
 */
-(void)changeOrientation:(UIInterfaceOrientation)orientation;

#pragma mark - 动画效果 POP
/**
 *  CA动画 POP
 */
-(void)POPWithCATAnimationGoBack;

/**
 *  普通动画 POP
 */
-(void)POPWithanimationGoback;


@end
