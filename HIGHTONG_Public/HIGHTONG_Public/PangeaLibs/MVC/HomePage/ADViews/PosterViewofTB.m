//
//  PosterViewofTB.m
//  HIGHTONG
//
//  Created by HTYunjiang on 14-2-28.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import "PosterViewofTB.h"
//#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
@interface PosterImageTBCell()
//Action of PageControll



//#define kheigh 27
- (void) changeCurrentPage:(SMPageControl *)sender;

@end

@implementation PosterViewofTB

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view1 = [UIView new];
        [self addSubview:view1];
        // Initialization code
//        self.backgroundColor = [UIColor greenColor ];
        UIScrollView * scrollView = [[UIScrollView alloc] init];
        [scrollView setFrame:CGRectMake(0, 0, kDeviceWidth, (173+kheigh)*kRateSize)];
        scrollView.backgroundColor = [UIColor clearColor];
        UIView *view2 = [UIView new];
        [scrollView addSubview:view2];
        
        self.posterTableView = [[UITableView alloc] init];
        [self.posterTableView setFrame:CGRectMake(0, 0, (173+kheigh)*kRateSize, kDeviceWidth)];
        [self.posterTableView setCenter:CGPointMake(kDeviceWidth/2, ((173.0+kheigh)/2)*kRateSize)];
        [self.posterTableView setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
        [self.posterTableView setScrollsToTop:YES];
        [self.posterTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.posterTableView setPagingEnabled:YES];
        [self.posterTableView setDelegate:self];
        [self.posterTableView setDataSource:self];
        [self.posterTableView setBackgroundColor:[UIColor clearColor]];
        [self.posterTableView setShowsVerticalScrollIndicator:NO];
        [self.posterTableView setBackgroundColor:App_background_color];
        
        [scrollView setContentSize:CGSizeMake(kDeviceWidth, (173+kheigh)*kRateSize)];
        [scrollView addSubview:self.posterTableView];
        [self addSubview:scrollView];
        [self setUserInteractionEnabled:YES];
        
    
        
        self.posterPageControl = [[SMPageControl alloc] init];
        [self.posterPageControl setFrame:CGRectMake(kDeviceWidth-(130)*kRateSize, (173-20+kheigh)*kRateSize, (120)*kRateSize, 20*kRateSize)];
        [self.posterPageControl setNumberOfPages:0];
        //smpage sql
        [self.posterPageControl setPageIndicatorImage:[UIImage imageNamed:@"轮播标志"]];
        [self.posterPageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"轮播焦点-01"]];
        self.posterPageControl.indicatorMargin = 3.0f;//格子之间的间隙
//                self.posterPageControl.indicatorDiameter = 3.0f;
        [self.posterPageControl setEnabled:NO];
        self.posterPageControl.alignment = SMPageControlAlignmentRight;
//        self.posterPageControl.backgroundColor = [UIColor redColor];
        [self addSubview:self.posterPageControl];
        
        
    }
    return self;
}

#pragma mark update The PosterData
- (void) updateThePosterData:(NSMutableArray *)aPosterArr
{
    self.posterArray = aPosterArr;
    if (self.posterArray.count == 0) {
        //yj ??少default图片
        UIImageView *poster = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        poster.frame = CGRectMake(0, 0, kDeviceWidth, (173+kheigh)*kRateSize);
        
        
        poster.tag = 111;
        [self addSubview:poster];
        
    }
    //---------------自由滚动计时器
    if (self.posterArray.count>1) {
        if (_circleTimer==nil) {
            _isCanGo = YES;
            _circleTimer = [NSTimer scheduledTimerWithTimeInterval:PAGESCROLLTIME target:self selector:@selector(autoScroll) userInfo:nil repeats:YES];
            
        }
    }
    //------------------------------------------
    [self.posterPageControl setNumberOfPages:[self.posterArray count]];
    //    [self.posterPageControl setNumberOfPages:10];
    [self.posterTableView reloadData];
    
    //
    if (self.posterArray.count >= 1) {
        if (self.posterArray.count==1) {
            self.posterTableView.scrollEnabled = NO;
        }
        //---------------隐藏默认图片
        for (UIImageView *loadingImage in [self subviews]) {
            if (loadingImage.tag==111) {
                loadingImage.hidden = YES;
                [loadingImage removeFromSuperview];
            }
        }
        //--------------隐藏默认图片结束
        
        static int a = 0;
        if ([self.posterArray count] > 0 && a == 0) {
            NSIndexPath * indexPth = [NSIndexPath indexPathForRow:[self.posterArray count] inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPth.row*kDeviceWidth)];
            a = 1;
        }
    }
}
#pragma mark ------------  UITableViewDelegate -----------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.posterArray count] == 1) {
        return 2;
    } else {
        return [self.posterArray count] * 3;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kDeviceWidth;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identitify = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
    PosterImageTBCell * cell = [tableView dequeueReusableCellWithIdentifier:identitify];
    if (!cell) {
        cell = [[PosterImageTBCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identitify];
        cell.backgroundColor = [UIColor clearColor];
        [cell setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    NSURL *url ;
//    if (!HomeNewDisplayTypeOn) {
//        url = [NSURL URLWithString:NSStringIMGFormat([[self.posterArray objectAtIndex:indexPath.row% [self.posterArray count]] valueForKey:@"imageLink"])];
//    }else{
        url= [NSURL URLWithString:NSStringPM_IMGFormat([[self.posterArray objectAtIndex:indexPath.row% [self.posterArray count]] valueForKey:@"imageLink"])];

//    }

    [cell.posterHoderIV sd_setImageWithURL: url placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
    [cell.posterDescriptionL setText:[[self.posterArray objectAtIndex:indexPath.row% [self.posterArray count]]valueForKey:@"name"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (self.posterArray.count==0) {
        return;
    }
    
    
    //----------只有数组不为空的时候
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GOTODETAILOFVOD" object:[self.posterArray objectAtIndex:indexPath.row%[self.posterArray count]] userInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%lu",indexPath.row%[self.posterArray count]] forKey:@"index"]];
    
    
}

#pragma mark -
#pragma mark ------------  UIScrollViewDelegate -----------------
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.posterArray.count > 1) {
        //        int pageNum = (((int)self.posterTableView.contentOffset.y+160)%(320*[self.posterArray count])) / 320;
        int pageNum = (((int)self.posterTableView.contentOffset.y+(int) kDeviceWidth/2+(int)kDeviceWidth)%((int)kDeviceWidth*[self.posterArray count])) /(int) kDeviceWidth;
        
        [self.posterPageControl setCurrentPage:pageNum];
        //        [self.posterPageControl setCurrentImage:[UIImage imageNamed:@"smPageControl_ed.png"] forPage:pageNum];
        
        if (self.posterTableView.contentOffset.y == 0) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[self.posterArray count]  inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPath.row*kDeviceWidth)];
        } else if (self.posterTableView.contentOffset.y == kDeviceWidth*[self.posterArray count]*3 - kDeviceWidth){
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:([self.posterArray count]*2-1)  inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPath.row*kDeviceWidth)];
        }
    }
    
}
//开始拖拽
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
{
    NSLog(@"scrollViewWillBeginDragging");
    _isCanGo = NO;
    [_circleTimer invalidate];
    _circleTimer = nil;
    
    
}
//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    NSLog(@"scrollViewDidEndDragging");
    _isCanGo = YES;
    if (_circleTimer==nil) {
        _circleTimer = [NSTimer scheduledTimerWithTimeInterval:PAGESCROLLTIME target:self selector:@selector(autoScroll) userInfo:nil repeats:YES];
    }
    
}


-(void)autoScroll{
    if (_isCanGo) {
        
    }else{
        return;
    }
    
    if (self.posterArray.count > 1) {
        //        int pageNum = (((int)self.posterTableView.contentOffset.y+160+320)%(320*[self.posterArray count])) / 320;
        int pageNum = (((int)self.posterTableView.contentOffset.y+(int) kDeviceWidth/2+(int)kDeviceWidth)%((int)kDeviceWidth*[self.posterArray count])) /(int) kDeviceWidth;
        
        [self.posterPageControl setCurrentPage:pageNum];
        //        [self.posterPageControl setCurrentImage:[UIImage imageNamed:@"smPageControl_ed.png"] forPage:pageNum];
        
        if (self.posterTableView.contentOffset.y == 0) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[self.posterArray count]  inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPath.row*kDeviceWidth)];
            
        } else if (self.posterTableView.contentOffset.y == kDeviceWidth*[self.posterArray count]*3 - kDeviceWidth){
            
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:([self.posterArray count]*2-1)  inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPath.row*kDeviceWidth)];
            
        }else{
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNum inSection:0];
            [self.posterTableView setContentOffset:CGPointMake(0, indexPath.row*kDeviceWidth)];
            //            int pageNumbbbbb = (((int)self.posterTableView.contentOffset.y+160+320)%(320*[self.posterArray count])) / 320;
            int pageNumbbbbb = (((int)self.posterTableView.contentOffset.y+(int) kDeviceWidth/2+(int) kDeviceWidth)%((int) kDeviceWidth*[self.posterArray count])) / (int) kDeviceWidth;
            
            [self.posterPageControl setCurrentPage:pageNumbbbbb];
            
        }
        
        
    }
    
}


@end
