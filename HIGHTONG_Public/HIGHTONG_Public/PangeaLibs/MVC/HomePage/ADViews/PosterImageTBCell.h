//
//  PosterImageTBCell.h
//  HIGHTONG
//
//  Created by HTYunjiang on 14-2-28.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define KDeviceHeight [UIScreen mainScreen].bounds.size.height
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface PosterImageTBCell : UITableViewCell

@property (strong, nonatomic) UIImageView * posterHoderIV;      //加载中图片
@property (strong, nonatomic) UIImageView * poster_script_bgIV; //说明横条
@property (strong, nonatomic) UILabel * posterDescriptionL;     //海报说明


#define kheigh (27-21)

@end
