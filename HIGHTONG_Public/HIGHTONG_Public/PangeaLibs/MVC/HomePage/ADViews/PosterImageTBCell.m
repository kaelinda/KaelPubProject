//
//  PosterImageTBCell.m
//  HIGHTONG
//
//  Created by HTYunjiang on 14-2-28.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import "PosterImageTBCell.h"

@implementation PosterImageTBCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.poster_script_bgIV = [[UIImageView alloc] init];
        self.posterHoderIV = [[UIImageView alloc] init];
        self.posterDescriptionL = [[UILabel alloc] init];
        //yj ？？这里少loading图片
        [self.posterHoderIV setImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        
        [self.poster_script_bgIV setImage:[UIImage imageNamed:@"jxCable_home_pageController_bg"]];
//        [self.poster_script_bgIV setBackgroundColor:[UIColor blueColor]];
        
        [self.posterHoderIV setFrame:CGRectMake(0, 0, kDeviceWidth, (173+kheigh)*kRateSize)];
        [self.poster_script_bgIV setFrame:CGRectMake(0, (173-24+kheigh)*kRateSize, kDeviceWidth, 24*kRateSize)];
        [self.posterDescriptionL setFrame:CGRectMake(10*kRateSize, 0, 200*kRateSize, 24*kRateSize)];
        
        if (isAfterIOS6) {
            [self.posterDescriptionL setTextAlignment:NSTextAlignmentCenter];
        }else
        {
            [self.posterDescriptionL setTextAlignment:NSTextAlignmentCenter];
        }
    
        [self.posterDescriptionL setTextAlignment:NSTextAlignmentLeft];
        [self.posterDescriptionL setBackgroundColor:[UIColor clearColor]];
        [self.posterDescriptionL setFont:[UIFont systemFontOfSize:11*kDeviceRate]];
        [self.posterDescriptionL setTextColor: UIColorFromRGB(0xffffff)];
        
        [self.poster_script_bgIV addSubview:self.posterDescriptionL];
        [self addSubview:self.posterHoderIV];
        [self addSubview:self.poster_script_bgIV];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
