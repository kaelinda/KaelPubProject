//
//  PosterViewofTB.h
//  HIGHTONG
//
//  Created by HTYunjiang on 14-2-28.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PosterImageTBCell.h"
#import "SMPageControl.h"
@interface PosterViewofTB : UIView<UITableViewDelegate,UITableViewDataSource>
{
    BOOL _isCanGo;

}
@property (strong, nonatomic) UITableView   * posterTableView;
@property (strong, nonatomic) SMPageControl * posterPageControl;
@property (strong, nonatomic) NSArray       * posterArray;

@property (strong, nonatomic) NSTimer *circleTimer;

//update The PosterData
- (void) updateThePosterData:(NSArray *)aPosterArr;

@end
