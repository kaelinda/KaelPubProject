//
//  DetailViewcontrol.m
//  HIGHTONG
//
//  Created by Kael on 15/7/27.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DetailViewcontrol.h"
#import "HTRequest.h"

//#import "HotSearchTableViewCell.h"
@interface DetailViewcontrol ()<UITextFieldDelegate>

@property (nonatomic,strong)UITableView *tableview; //搜索结果tableView

@property(nonatomic,strong)NSMutableArray *dataArray;//tableView数据源

@property (nonatomic,assign)BOOL isHotSearch; //热搜推荐时的tableview


@end

@implementation DetailViewcontrol




-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"detail"];
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
////    backBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"back" target:self action:@selector(goBackPage)];
//    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"arrow_left_01" imgHighlight:@"arrow_left_01"  target:self action:@selector(goBackPage)];
//    [self setNaviBarLeftBtn:backBtn];
//    
//    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    cancelBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"取消" target:self action:@selector(cancel )];
//    cancelBtn.titleLabel.textColor = [UIColor whiteColor];
//    [self setNaviBarRightBtn:cancelBtn];
//    
//    
//    
//    [self setSeachBar];
//    self.dataArray = [NSMutableArray array];
//    [self setUpTableView];
//    for (NSInteger i =0; i<30; i++) {
//        [self.dataArray addObject:@""];
//    }
//    self.isHotSearch = YES;
//    [self.tableview reloadData];
    
}
//- (void)setSeachBar
//{
//    //设置搜索textField
//    
//    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
//    CGSize size = HTframe.size;
//    NSLog(@"%f %f ",size.width,size.height);
//    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
//    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
//    view.backgroundColor = viewBgC;
//    view.backgroundColor = [UIColor whiteColor];
//    view.layer.cornerRadius = size.height/2;
//    view.clipsToBounds = YES;
//    
//    UITextField *seachTextView = [[UITextField alloc]initWithFrame:CGRectMake(32 ,5,size.width - 35- 0 , 30)];
//    seachTextView.backgroundColor = [UIColor redColor];
//    seachTextView.delegate = self;
//    seachTextView.placeholder = @"\t输入影片名或主演";
//    seachTextView.preservesSuperviewLayoutMargins = YES;
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(seachTextView.size.width, seachTextView.size.height)];
//    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
//    masklayer.path = maskPath.CGPath;
//    seachTextView.layer.mask = masklayer;
//    UIButton *btn = [seachTextView valueForKey:@"_clearButton"];
//    [btn setImage:[UIImage imageNamed:@"icon_search_delete"] forState:UIControlStateNormal];
//    
//    [view addSubview:seachTextView];
//    
//    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake(seachTextView.frame.origin.x+5, seachTextView.frame.origin.y+5, 20, 20)];
//    searchview.image = [UIImage imageNamed:@"alarm_64"];
//    searchview.tag = 996633;
//    [view addSubview:searchview];
//    
//    
//    [self setNaviBarSearchView:view];
//    
////    [self setNaviBarSearchView:view];
//    
//}
//- (void)setUpTableView
//{
//    UIView *view =[UIView new];
//    [self.view addSubview:view];
//    self.tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,kDeviceWidth, KDeviceHeight)];
//    self.tableview.delegate =self;
//    self.tableview.dataSource = self;
//    [self.view addSubview:self.tableview];
//    
//}
//
//#pragma mark - tableView delegate
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return self.dataArray.count;
//}
//- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (self.isHotSearch) {
//        static NSString *Hotidentifier = @"HotIdentifier";
//        HotSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Hotidentifier];
//        if (!cell) {
//            cell = [[HotSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Hotidentifier];
//        }
//        
//        
//        [cell.btn setImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal]; 
//        cell.label.text = [NSString stringWithFormat:@"第%ld行数据哦哦",indexPath.row];
//        if (indexPath.row == 0) {
//            cell.btn.layer.cornerRadius = 0;
//            cell.label.font = [UIFont systemFontOfSize:15];
//            
//            [cell.btn setImage:[UIImage imageNamed:@"alarm_on_64"] forState:UIControlStateNormal];
//            cell.numLable.text = @"";
//          
//        }else if (indexPath.row ==1 || indexPath.row ==2 ||indexPath.row == 3 )
//        {
//            cell.label.font = [UIFont systemFontOfSize:13];
//            cell.btn.layer.cornerRadius = 20;
//            
//            [cell.btn setImage:[UIImage imageNamed:@"ic_search_hotwords_label_top"] forState:UIControlStateNormal];
//            cell.numLable.text = [NSString stringWithFormat:@"%ld",indexPath.row];
//        }
//        else
//        {
//            cell.label.font = [UIFont systemFontOfSize:13];
//            cell.btn.layer.cornerRadius = 20;
//            
//            [cell.btn setImage:[UIImage imageNamed:@"ic_search_hotwords_label_below"] forState:UIControlStateNormal];
//            cell.numLable.text = [NSString stringWithFormat:@"%ld",indexPath.row];
//        }
//        return cell;
//    }
//    return nil;
////    static NSString *identifier = @"identifier";
////    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
////    if (!cell) {
////        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
////    }
////    cell.textLabel.text = @"asdfsfasdfasdf";
////    return cell;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 50;
//}
//
//#pragma mark - TextField Delegate
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    
//    NSLog(@"开始输入搜索关键词");
//    textField.placeholder = @"\t输入视频名称";
//    UIView * searchView =   [textField.superview viewWithTag:996633];
//    [UIView animateWithDuration:0.2 animations:^{
//        searchView.frame = CGRectMake(8, searchView.frame.origin.y, 20, 20);
//    }];
//    
//    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//    [textField clearButtonRectForBounds:CGRectMake(textField.size.width-45, 0, 40,40)];
//    if ([textField respondsToSelector:@selector(clearButtonRectForBounds:)]) {
//        NSLog(@"改变了大小了哦");
//    }
//    
//}
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    NSLog(@"输入完毕");
//}
//
///*每输入一个字就搜索一次*/
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    
//    NSString *keyWord = [NSString stringWithFormat:@"%@%@",textField.text,string];
//    NSLog(@"搜索关键字： %@",keyWord);
//    
//    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//    [request TopSearchKeyWithKey:keyWord andCount:10 andTimeout:10];
//    
//    return YES;
//}
//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.view endEditing:YES];
//}
//
//#pragma mark - HT请求代理方法
//- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
//{
//    NSLog(@"status %ld result %@ type:%@",status, result,type);
//}
//
//
//
//
//- (void)cancel
//{
//    NSLog(@"取消搜索");
//}
//
//-(void)goBackPage{
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    //    [self animationGoback];
//    
//}




@end
