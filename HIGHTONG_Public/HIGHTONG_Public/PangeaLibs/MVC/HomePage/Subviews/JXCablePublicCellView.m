//
//  JXCablePublicCellView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JXCablePublicCellView.h"


@interface JXCablePublicCellView ()
{
    UIButton_Block * _btn;
}

@end


@implementation JXCablePublicCellView

- (instancetype)init
{
    if (self = [super init]) {
       
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    WS(wself);
    
    UIView *superView = self;
    
    self.postImg = [UIImageView new];
    [superView addSubview:self.postImg];
    
    
    [self.postImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top).offset(7*kRateSize);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    
   
    self.postImg.image =[UIImage imageNamed:@"poster"] ;
    
//    self.postImg.backgroundColor = [UIColor redColor];
    
    
    _btn  = [UIButton_Block new];
    [self addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.postImg);
    }];
    
    
    [_btn setImage:[UIImage imageNamed:@"jxCable_home_cell_bg"] forState:UIControlStateHighlighted];
    _btn.Click = ^(UIButton_Block*btn,NSString *url)
    {
        if (wself.ButtonBlock!=nil) {
            NSLog(@"cell知道点击了那个view");
            wself.ButtonBlock(btn,url);
        }
    };
}

- (void)refreshUI
{
    WS(wself);
    
    NSDictionary *dic = self.dataDic;
    if (dic) {
        self.hidden = NO;
        
        if ([dic objectForKey:@"imageUrl"]) {
            
//            [self.postImg sd_setImageWithURL:[dic objectForKey:@"imageUrl"] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];//公版改版
            
            //[self.postImg setImage:[dic objectForKey:@"imageUrl"]];
            
            self.postImg.image = [UIImage imageNamed:[dic objectForKey:@"imageUrl"]];
        }
        
        _btn.Click = ^(UIButton_Block*btn,NSString *url)
        {
            if (wself.ButtonBlock) {
                NSLog(@"江西有线首页cell知道点击了那个view");
                wself.ButtonBlock(btn,[dic objectForKey:@"jxURL"]);
            }
        };
        
    }else
    {
        self.hidden = YES;
        self.postImg.image =[UIImage imageNamed:@"poster"] ;
    }

}

@end
