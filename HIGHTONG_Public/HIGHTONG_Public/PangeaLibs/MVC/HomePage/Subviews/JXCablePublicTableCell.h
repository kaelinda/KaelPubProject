//
//  JXCablePublicTableCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
#import "JXCablePublicCellView.h"


typedef void(^JXCablePublicTableCellBlock)(UIButton_Block *,NSString *url);


@interface JXCablePublicTableCell : UITableViewCell

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)JXCablePublicCellView *leftView;//左边

//@property (nonatomic,strong)JXCablePublicCellView *rightView;//右边

@property (nonatomic,copy)JXCablePublicTableCellBlock cellBlock;


@property (nonatomic,strong)NSMutableDictionary *dataDic;

- (void)JXCableHomeUpDate;//更新数据

@end
