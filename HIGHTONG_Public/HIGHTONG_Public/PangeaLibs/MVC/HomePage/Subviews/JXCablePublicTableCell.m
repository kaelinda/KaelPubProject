//
//  JXCablePublicTableCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JXCablePublicTableCell.h"

@implementation JXCablePublicTableCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
//        self.dateArray = [NSMutableArray array];
        
        self.dataDic = [NSMutableDictionary dictionary];
        
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    WS(wself);
    
    UIView *superView = self.contentView;
    
    self.leftView = [JXCablePublicCellView new];
    [superView addSubview:self.leftView];
    
//    self.rightView = [JXCablePublicCellView new];
//    [superView addSubview:self.rightView];
    
    
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(7*kRateSize);//10
        make.top.equalTo(superView.mas_top);//.offset(5*kRateSize);
        make.right.mas_equalTo(superView.mas_right).offset(-7*kRateSize);//-10
        make.height.mas_equalTo(172*kRateSize);
        
    }];
    [self.leftView refreshUI];
    self.leftView.backgroundColor = [UIColor clearColor];
    self.leftView.ButtonBlock = ^(UIButton_Block*btn,NSString *url)
    {
        if (wself.cellBlock) {
            NSLog(@"左视图直达cell选了第几个");
            wself.cellBlock(btn,url);
        }
    };
    
//    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wself.leftView.mas_right).offset(5*kRateSize);
//        make.top.equalTo(superView.mas_top);//.offset(5*kRateSize);
//        make.width.mas_equalTo(148*kRateSize);
//        make.height.mas_equalTo(87*kRateSize);
//    }];
//    [self.rightView refreshUI];
//    self.rightView.ButtonBlock = ^(UIButton_Block*btn,NSString *url)
//    {
//        if (wself.cellBlock) {
//            NSLog(@"右视图直达cell选了第几个");
//            wself.cellBlock(btn,url);
//        }
//    };
}

- (void)JXCableHomeUpDate
{
    
    self.leftView.dataDic = self.dataDic;
    
    NSLog(@"%@",self.leftView.dataDic);
    
    [self.leftView refreshUI];
 
    
//    if (self.dateArray.count >= 1) {
//        NSDictionary *dic = self.dateArray[0];
//        
//        if ([dic isKindOfClass:[NSDictionary class]]) {
//            self.leftView.dataDic = dic;
//            self.rightView.dataDic = nil;
//            
//            [self.leftView refreshUI];
//            [self.rightView refreshUI];
//        }
//    }
//    if (self.dateArray.count >= 2) {
//        
//        NSDictionary *dic = self.dateArray[0];
//        NSDictionary *dic1 = self.dateArray[1];
//        //        NSLog(@"%d: %@",1 ,dic );
//        if ([dic isKindOfClass:[NSDictionary class]]) {
//            
//            self.leftView.dataDic = dic;
//            
//            self.rightView.dataDic = dic1;
//            
//            
//            
//            [self.leftView refreshUI];
//            [self.rightView refreshUI];
//        }
//        
//    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
