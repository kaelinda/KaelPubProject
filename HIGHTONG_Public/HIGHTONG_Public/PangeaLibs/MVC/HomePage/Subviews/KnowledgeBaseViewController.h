//
//  KnowledgeBaseViewController.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>


@protocol JavaScriptInteractDelegate <JSExport>

- (void)finish;

@end


@interface KnowledgeBaseViewController : HT_FatherViewController

@property (nonatomic, strong)NSString *naviTitle;

@end

