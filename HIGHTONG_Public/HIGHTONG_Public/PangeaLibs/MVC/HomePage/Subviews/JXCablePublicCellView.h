//
//  JXCablePublicCellView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void(^JXCablePublicCellView_Button_Down)(UIButton_Block *,NSString *url);

@interface JXCablePublicCellView : UIView

@property (nonatomic, strong)UIImageView *postImg;

@property (nonatomic, strong)NSDictionary *dataDic;

@property (nonatomic, copy)JXCablePublicCellView_Button_Down ButtonBlock;

- (void)refreshUI;

@end



