//
//  KnowledgeBaseViewController.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "KnowledgeBaseViewController.h"
#import "AlertLabel.h"

#define K_USER   (IsLogin ? KMobileNum : [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"])


@interface KnowledgeBaseViewController ()<UIWebViewDelegate>

@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *context;


@end


@interface ActivityFunc : NSObject<JavaScriptInteractDelegate>

@end


@implementation ActivityFunc

- (void)finish
{
    NSLog(@"我要回去，放我回去！！");
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [[NSNotificationCenter defaultCenter] postNotificationName:@"GO_BACK_RIGHT_NOW" object:nil];
    });
}

@end



@implementation KnowledgeBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    if (_naviTitle.length) {
        
        [self setNaviTitle:_naviTitle];
    }else{
        [self hideNaviBar:YES];
    }
    
    [self.view setBackgroundColor:App_white_color];
    
    [self interactWithJS];
}

- (void)interactWithJS
{
    WS(wss);

    _webView = [[UIWebView alloc] init];
    [self.view addSubview:_webView];
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top);//.offset(20);
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.bottom.equalTo(wss.view.mas_bottom);
    }];
    [_webView setBackgroundColor:App_white_color];
    _webView.scalesPageToFit = YES;
    //    _webView.scrollView.bounces = NO;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.delegate = self;
    [_webView setBackgroundColor:CLEARCOLOR];
    

    NSString *urlStr = [NSString stringWithFormat:@"%@/shphone/pages/kt/?user=%@&uuid=%@",YJ_Shphone,K_USER,dev_ID];
    
//    NSString *urlStr = [NSString stringWithFormat:@"http://192.168.3.27/shphone/pages/kt/?user=%@&uuid=%@",userStr,dev_ID];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSLog(@"dev_ID:%@,K_USER:%@",[NSString stringWithFormat:@"%@",dev_ID],[NSString stringWithFormat:@"%@",K_USER]);
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    [_webView loadRequest:request];

    _context = [[JSContext alloc] init];
    _context = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackRightNow) name:@"GO_BACK_RIGHT_NOW" object:nil];
    
    ActivityFunc *js = [[ActivityFunc alloc] init];

    _context[@"ActivityFunc"] = js;

}


- (void)goBackRightNow//:(NSNotification *)notification
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (error) {
        
        [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self goBackRightNow];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GO_BACK_RIGHT_NOW" object:nil];
}

@end

