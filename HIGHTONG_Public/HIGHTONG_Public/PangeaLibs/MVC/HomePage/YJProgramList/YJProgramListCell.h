//
//  YJProgranListCell.h
//  HIGHTONG
//
//  Created by testteam on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YJProgramView.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class YJProgramListCell;
typedef void (^YJProgramCellBlock) (UIButton_Block*);

@interface YJProgramListCell : UITableViewCell

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)YJProgramCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)YJProgramView *leftView;//左边

@property (nonatomic,strong)YJProgramView *rightView;//右边



- (void)update;//跟新数据

@end
