//
//  OCAndJSInterfaceViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/18.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "OCAndJSInterfaceViewController.h"
#import "AlertLabel.h"


typedef void(^ActivityJSFuncBlock)();

@interface ActivityJSFunc : NSObject<OCAndJSInterfaceViewControllerDelegate>

@property (nonatomic, copy)ActivityJSFuncBlock finishBlock;

@end


@implementation ActivityJSFunc

- (void)finish
{
    NSLog(@"带请求头的JS");
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"GO_BACK_RIGHT_NOW" object:nil];
//    });
    
    if (_finishBlock) {
        _finishBlock();
    }
}

@end


@interface OCAndJSInterfaceViewController ()<UIWebViewDelegate>

@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *context;
@property (nonatomic, strong)ActivityJSFunc *js;


@end

@implementation OCAndJSInterfaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self hideNaviBar:YES];
    
    [self.view setBackgroundColor:App_white_color];
    
    [self interactWithJS];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackRightNow) name:@"GO_BACK_RIGHT_NOW" object:nil];

}


- (void)interactWithJS
{
    WS(wss);
    
    _webView = [[UIWebView alloc] init];
    [self.view addSubview:_webView];
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top);//.offset(20);
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.bottom.equalTo(wss.view.mas_bottom);
    }];
    [_webView setBackgroundColor:App_white_color];
    _webView.scalesPageToFit = YES;
    _webView.scrollView.bounces = NO;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.delegate = self;
    [_webView setBackgroundColor:CLEARCOLOR];
    
    
    NSLog(@"%@",_urlStr);
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlStr]];
//    [_webView loadRequest:request];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [_webView loadRequest:request];

    //    _context = [[JSContext alloc] init];
    //    _context = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
}

- (void)goBackRightNow
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PLAYPLAYER" object:nil];

}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showCustomeHUD];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hiddenCustomHUD];
    
    _context = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    _js = [[ActivityJSFunc alloc] init];
    _context[@"ActivityFunc"] = _js;
    
    WS(wSelf);
    _js.finishBlock = ^(){
    
        [wSelf goBackRightNow];
    };
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hiddenCustomHUD];
    
    if (error) {
        if ([error code]==-999) {
            return;
        }
//        [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            [self goBackRightNow];
//        });
        
        ZSToast(@"加载失败");
        [self goBackRightNow];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    
    NSDictionary *headers = [request allHTTPHeaderFields];
    NSString *regionCode = [headers objectForKey:@"regionCode"];
    //判断是否包含请求头
    if (regionCode.length == 0) {
        //不包含请求头
        [mutableRequest addValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [mutableRequest addValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        request = [mutableRequest copy];
        [self.webView loadRequest:request];
        NSLog(@"webView request_Header%@",request.allHTTPHeaderFields);
        
        return NO;
        
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GO_BACK_RIGHT_NOW" object:nil];
}


@end
