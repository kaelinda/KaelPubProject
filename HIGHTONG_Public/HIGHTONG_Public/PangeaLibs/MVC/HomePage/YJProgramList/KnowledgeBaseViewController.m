//
//  KnowledgeBaseViewController.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "KnowledgeBaseViewController.h"
#import "AlertLabel.h"
#import "ShareViewVertical.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import <UMSocialCore/UMSocialCore.h>
#import "UserLoginViewController.h"


typedef void(^JavaScriptInteractBlock)(NSString *UmengContent,NSString *UmengImageStr,BOOL boolean,NSString *appLink);

typedef void (^JavaScriptInteractEXBlock)(NSString *UmengContentEX,NSString *UmengImageStrEX,BOOL booleanEX,NSString *appLinkEX,NSString*UmengIDEX);

typedef void(^JavaScriptLoginBlock)();

typedef void(^JSCommitTitleBlock)(NSString *title);

@interface ActivityFunc : NSObject<JavaScriptInteractDelegate>

@property (nonatomic, copy)JavaScriptInteractBlock javaScriptInteractBlock;

@property (nonatomic, copy)JavaScriptInteractEXBlock javaScriptInteractEXBlock;

@property (nonatomic, copy)JavaScriptLoginBlock javaScriptLoginBlock;

@property (nonatomic, copy)JavaScriptLoginBlock dismissLoadingBlock;

@property (nonatomic, copy)JSCommitTitleBlock getTitleBlock;

@property (nonatomic, copy)JavaScriptLoginBlock finishBlock;

@property (nonatomic, copy)JavaScriptLoginBlock shareBlock;

@property (nonatomic, copy)JSCommitTitleBlock naviShareBlock;

@end


@implementation ActivityFunc

- (void)finish
{
    NSLog(@"我要回去，放我回去！！");
    
    if (_finishBlock) {
        _finishBlock();
    }
}


- (void)share :(NSString *)UmengContent :(NSString *)UmengImageStr :(BOOL)boolean :(NSString *)appLink
{
    if (_javaScriptInteractBlock) {
        _javaScriptInteractBlock(UmengContent,UmengImageStr,boolean,appLink);
    }
    
    NSLog(@"UmengContent--%@,UmengImageStr--%@,boolean--%id,appLink--%@",UmengContent,UmengImageStr,boolean,appLink);
}


- (void)shareEx :(NSString *)UmengContent :(NSString *)UmengImageStr :(BOOL)boolean :(NSString *)appLink  :(NSString*)UmengID
{

    if (_javaScriptInteractEXBlock) {
        _javaScriptInteractEXBlock(UmengContent,UmengImageStr,boolean,appLink,UmengID);
    }
    
    
    NSLog(@"UmengContent--%@,UmengImageStr--%@,boolean--%id,appLink--%@,%@",UmengContent,UmengImageStr,boolean,appLink,UmengID);

}


//判断是否登录了
- (NSString *)isLogin
{
    NSString *returnStr;
    if (IsLogin) {
        
        returnStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    }else{
        returnStr = @"";
    }
    
    return returnStr;
}

//调用登陆接口
- (void)login
{    
    if (_javaScriptLoginBlock) {
        _javaScriptLoginBlock();
    }
}

//刷新token
- (void)refreshToken
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
}

//取消loading的方法
- (void)dismissLoading
{
    NSLog(@"取消loading");
    if (_dismissLoadingBlock) {
        _dismissLoadingBlock();
    }
    
    NSLog(@"哈哈  消失了~");
}

//刷新title字符串
- (void)refreshTitle:(NSString *)string
{
    NSLog(@"web的导航title——————%@",string);
    //lailibo修改关于title为null的问题
    NSString *Mstring = @"";
    
    Mstring = isEmptyStringOrNilOrNull(string)?@"":string;

    if (_getTitleBlock) {
        _getTitleBlock(Mstring);
    }
    //end
}

- (void)goBackIs:(BOOL)flag
{
    
    [[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"webGoBack"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"goback--------%d",flag);
}

- (void)showShareBtn:(BOOL)isShow
{
    if (isShow) {
        if (_naviShareBlock) {
            _naviShareBlock(@"1");
        }
    }else{
        if (_naviShareBlock) {
            _naviShareBlock(@"0");
        }
    }
}

- (void)feedbackSuccess{
    NSLog(@"反馈——哈哈哈哈哈反馈成功");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"COME_FROM_FEEDBACK_VIEW" object:@"1"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Go_Back" object:nil];
    });
}

@end


@interface KnowledgeBaseViewController ()<UIWebViewDelegate>

@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *context;
@property (nonatomic, strong)ActivityFunc *js;
@property (nonatomic, strong)ShareViewVertical *shareViewVertical;
@property (nonatomic, strong)NSString *umengContent;
@property (nonatomic, strong)NSString *umengImageStr;
@property (nonatomic, strong)NSString *appLink;
@property (nonatomic, strong)NSString *umengID;


@end


/**
 *  视频处理程序的虚拟URL方案
 */
static NSString *const VideoHandlerScheme = @"videohandler";

@implementation KnowledgeBaseViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_webView) {
        [_webView stopLoading];
    }
    [_context evaluateScript:@"clearVideo()"];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_webView) {
        [_webView stopLoading];
    }
    [_context evaluateScript:@"clearVideo()"];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    progressTime = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlayer) name:@"MSG_STOPPLAYER" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(JSGoBack) name:@"Go_Back" object:nil];
    
    if (_existNaviBar) {        
        [self hideNaviBar:NO];
    }else{
    
        [self hideNaviBar:YES];
    }
    
    
    _returnBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(JSGoBack)];
    [self setNaviBarLeftBtn:_returnBtn];
    
    
    _closeBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_close_web" imgHighlight:@"btn_back_click" target:self action:@selector(closeBtnDown)];
    [self setNaviBarRightBtn:_closeBtn];
    
    _shareBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_navbar_share" imgHighlight:@"btn_back_click" target:self action:@selector(shareBtnSenderDown)];
    [self setNaviBarRRightBtn:_shareBtn];
    _shareBtn.hidden = YES;
    
    
    [self.view setBackgroundColor:App_white_color];
    
    [self interactWithJS];
    
    [self setupShareView];

    NSString *htmlString = [[NSBundle mainBundle] myVideoPageHTMLString];
    
    NSLog(@"video URL:%@",htmlString);

}

//- (void)healthPreservingRecommendAlert
//{
//    
//}

- (void)stopVideoPlayer
{
    [_context evaluateScript:@"onBackground()"];
    
    [self closeBtnDown];
    
    NSLog(@"网页推送过来了");
}

- (void)shareBtnSenderDown
{
    NSLog(@"分享按钮被调用了");
    
    [_context evaluateScript:@"webShare()"];
}

- (void)closeBtnDown
{
    NSLog(@"我要关闭了");
        
    [self hiddenCustomHUDFromView:_webView];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)JSGoBack{
    
    if (_webView.canGoBack) {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"webGoBack"]) {//可控
        
            [_context evaluateScript:@"goBack()"];
            
            NSLog(@"可控的的goback");
        }else{
            
            [_webView goBack];
            
            NSLog(@"不可控的的goback");
        }
    }else{
        
        [_context evaluateScript:@"goBack()"];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"webGoBack"]) {//可控
//        
//        [_context evaluateScript:@"goBack()"];
//        
//        NSLog(@"可控的的goback");
//    }else{//不可控
//        
//        if (_webView.canGoBack) {
//         
//            [_webView goBack];
//        }else{
//        
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//        NSLog(@"不可控的的goback");
//    }
}

-(void)WillResignActive:(NSNotification*)notification
{
//    ZSToast(@"进入后台了");
    [_context evaluateScript:@"onBackground()"];
}

-(void)DidBecomeActive:(NSNotification*)notification
{
//    ZSToast(@"进入前台了");
    [_context evaluateScript:@"onForeground()"];
}

- (void)setupShareView
{
    
    WS(wself);
    _shareViewVertical = [[ShareViewVertical alloc]init];
    _shareViewVertical.hidden = YES;
    [_shareViewVertical setInstalledQQ:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]];
    [_shareViewVertical setInstalledWX:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]];
    [wself.view addSubview:_shareViewVertical];
    
    [_shareViewVertical mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.view);
    }];
    
    _shareViewVertical.returnBlock = ^(NSInteger tag){
        
        NSString *tempLink;
        
        if (!isEmptyStringOrNilOrNull(wself.appLink)) {
            tempLink = wself.appLink;
        }else{
            tempLink = @"";
        }
        
        switch (tag) {
            case 0:
            {
                
//                [wself WechatSessionShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringIMGFormat(wself.umengImageStr) shareType:Wechat currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.existNaviBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        if (IS_SHAREEX) {

                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengID,@"shareID",wself.umengContent,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"1",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }

                    }
                };
                
            }
                break;
            case 1:
            {
                
//                [wself wechatTimelineShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringIMGFormat(wself.umengImageStr) shareType:WechatZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.existNaviBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        if (IS_SHAREEX) {

                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengID,@"shareID",wself.umengContent,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"1",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }
                    }
                };
            }
                break;
            case 2:
                
            {
                
//                [wself tencetQQSahreAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringIMGFormat(wself.umengImageStr) shareType:QQ currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.existNaviBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        
                        if (IS_SHAREEX) {

                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengID,@"shareID",wself.umengContent,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"1",@"shartType",@"0",@"shartModel", nil];
                        wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                        [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
                        }
                    }
                };
                
            }
                break;
            case 3:
                
            {
                
//                [wself QQZoneShareAction];
                
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:wself.umengContent shareImage:NSStringIMGFormat(wself.umengImageStr) shareType:QQZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess){
                    
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        
                        if (wself.existNaviBar) {
                            [wself hideNaviBar:NO];
                        }else{
                            
                            [wself hideNaviBar:YES];
                        }
                        if (IS_SHAREEX) {
                            
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:wself.umengID,@"shareID",wself.umengContent,@"shareTitle",[SystermTimeControl getNowServiceTimeString],@"shareTime",@"1",@"shartType",@"0",@"shartModel", nil];
                            wself.shareUmengModel = [ShareCollectionModel shareCollectionWithDict:dictionary];
                            [SAIInformationManager shareUMengInformation:wself.shareUmengModel];
  
                        }
                        
                        
                    }
                };
                
            }
                break;
            default:
                break;
        }
        
    };
}

- (void)interactWithJS
{
    WS(wss);

    _webView = [[UIWebView alloc] init];
    [self.view addSubview:_webView];
    
    if (_existNaviBar) {
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(64+kTopSlimSafeSpace);//44
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom).offset(-kBottomSafeSpace);
        }];
    }else{
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top);//.offset(20);
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];
    }
    
    [_webView setBackgroundColor:App_white_color];
    _webView.scalesPageToFit = YES;
    _webView.scrollView.bounces = NO;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.mediaPlaybackRequiresUserAction = false;
    _webView.delegate = self;
    _webView.allowsInlineMediaPlayback = YES;
    [_webView setBackgroundColor:CLEARCOLOR];
    
    NSLog(@"被抓去的URL————%@",_urlStr);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlStr]];
    [_webView loadRequest:request];
    
    _emptyView = [[HTEmptyView alloc]initWithEmptyFatherView:self.webView andEmptyImageString:@"bg_retry_gesture" andEmptyLabelText:nil emptyViewClick:^{
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlStr]];
        
        [_webView loadRequest:request];
        
        NSLog(@"webview的再次点击被点击了");
        
        _emptyView.hidden = YES;
    }];
    _emptyView.hidden = YES;
    
    NSLog(@"我要的推送URL2%@",_urlStr);
    [self showCustomeHUDAddTo:_webView];

}

- (void)setUpLoginView
{
    
    UserLoginViewController *user = [[UserLoginViewController alloc] init];
    [self.view addSubview:user.view];
    user.backBlock = ^()
    {
    
    };
    user.loginBlock = ^()
    {
        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
    };

}

- (void)goBackRightNow
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{//这个代理会走很多次~~
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if (webView.isLoading) {
        return;
    }
    
    WS(wSelf);
    
    NSLog(@"我来过了！！！！！");
    dispatch_async(dispatch_get_main_queue(), ^{
        _emptyView.hidden = YES;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    });
   
    
    _context = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    _js = [[ActivityFunc alloc] init];
    _context[@"ActivityFunc"] = _js;
    
    if (_existNaviBar) {//针对挂号的取消loading框
        
        _js.dismissLoadingBlock = ^(){
            dispatch_async(dispatch_get_main_queue(), ^{
                [wSelf hiddenCustomHUDFromView:webView];
            });
            
        };
    }else{//其他部分的web页面
    
        [wSelf hiddenCustomHUDFromView:webView];
    }
    
    _js.naviShareBlock = ^(NSString *isHidden){
        dispatch_async(dispatch_get_main_queue(), ^{
            wSelf.shareBtn.hidden = [isHidden integerValue] > 0 ? NO : YES;
        });
    };
    
    
    
       _js.javaScriptInteractEXBlock = ^(NSString *UmengContentEX, NSString *UmengImageStrEX, BOOL booleanEX, NSString *appLinkEX, NSString *UmengIDEX) {
           
           _umengContent = UmengContentEX;
           _umengImageStr = UmengImageStrEX;
           _appLink = appLinkEX;
           _umengID = UmengIDEX;
           dispatch_async(dispatch_get_main_queue(), ^{
               wSelf.shareViewVertical.hidden = NO;
           });
 
       };
        
   
    
    _js.javaScriptInteractBlock = ^(NSString *UmengContent,NSString *UmengImageStr,BOOL boolean,NSString *appLink){
        
        _umengContent = UmengContent;
        _umengImageStr = UmengImageStr;
        _appLink = appLink;
        dispatch_async(dispatch_get_main_queue(), ^{
            wSelf.shareViewVertical.hidden = NO;
        });
        
        
    };
    
    _js.javaScriptLoginBlock = ^(){
        dispatch_async(dispatch_get_main_queue(), ^{
            [wSelf setUpLoginView];
        });
        
    };
    
    
    _js.getTitleBlock = ^(NSString *title){
        //lailibo修改关于title为null的问题

        NSString *titleStr = @"";
        titleStr = isEmptyStringOrNilOrNull(title)?@"":title;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (titleStr.length<=11) {
                
                [wSelf setNaviBarTitle:titleStr];
            }else{
                
                NSString *tempStr = [titleStr substringToIndex:10];
                
                [wSelf setNaviBarTitle:[NSString stringWithFormat:@"%@%@",tempStr,@"..."]];
            }

        });

        
    };
    
    _js.finishBlock = ^(){
        dispatch_async(dispatch_get_main_queue(), ^{
            [wSelf goBackRightNow];
        });
        
    };
    
    
    //JS钩子
    //play end事件的处理
    NSString *videoPlayEndHandlerString = [[NSBundle mainBundle] myVideoPlayEndHandlerJavaScriptString];
    //webkitbeginfullscreen webkitendfullscreen事件的处理
    NSString *videoFullScreenHandlerString = [[NSBundle mainBundle] myVideoFullScreenHandlerJavaScriptString];
    
    if (videoPlayEndHandlerString) {
        [webView stringByEvaluatingJavaScriptFromString:videoPlayEndHandlerString];
    }
    if (videoFullScreenHandlerString) {
        [webView stringByEvaluatingJavaScriptFromString:videoFullScreenHandlerString];
    }

}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if ([request.URL.scheme isEqualToString:VideoHandlerScheme]) {
        NSLog(@"%@", request.URL);
        return NO;
    }
    return YES;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

    [self hiddenCustomHUDFromView:webView];
        
    _emptyView.hidden = NO;
    
    if (error) {
        if ([error code]==-999) {
            return;
        }
        
        ZSToast(@"加载失败");
        [self goBackRightNow];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MSG_STOPPLAYER" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Go_Back" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    NSMutableURLRequest *mutableRequest = [request mutableCopy];
//    
//    NSDictionary *headers = [request allHTTPHeaderFields];
//    NSString *regionCode = [headers objectForKey:@"regionCode"];
//    //判断是否包含请求头
//    if (regionCode.length == 0) {
//        //不包含请求头
//        [mutableRequest addValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
//        [mutableRequest addValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
//        request = [mutableRequest copy];
//        [self.webView loadRequest:request];
//        NSLog(@"webView request_Header%@",request.allHTTPHeaderFields);
//        
//        return NO;
//        
//    }
//    return YES;
//}




@end

