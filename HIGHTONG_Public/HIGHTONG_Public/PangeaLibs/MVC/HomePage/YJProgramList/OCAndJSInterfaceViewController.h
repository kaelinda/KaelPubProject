//
//  OCAndJSInterfaceViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/18.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>


@protocol OCAndJSInterfaceViewControllerDelegate <JSExport>

- (void)finish;

@end

@interface OCAndJSInterfaceViewController : HT_FatherViewController

@property (nonatomic, copy)NSString *urlStr;

@end
