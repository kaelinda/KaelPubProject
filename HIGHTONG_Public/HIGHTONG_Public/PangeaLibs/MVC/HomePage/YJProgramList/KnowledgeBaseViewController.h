//
//  KnowledgeBaseViewController.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "HTEmptyView.h"
#import <UIKit/UIKit.h>
#import "NSBundle+MyStrings.h"
#import "ShareCollectionModel.h"
#import "SAIInformationManager.h"
@protocol JavaScriptInteractDelegate <JSExport>


- (void)finish;

- (void)share :(NSString *)UmengContent :(NSString *)UmengImageStr :(BOOL)boolean :(NSString *)appLink;

- (void)shareEx :(NSString *)UmengContent :(NSString *)UmengImageStr :(BOOL)boolean :(NSString *)appLink  :(NSString*)UmengID;

- (NSString *)isLogin;

- (void)login;

- (void)refreshToken;

- (void)dismissLoading;

- (void)refreshTitle:(NSString *)string;

- (void)goBackIs:(BOOL)flag;

- (void)showShareBtn:(BOOL)isShow;

- (void)feedbackSuccess;

@end


@interface KnowledgeBaseViewController : HT_FatherViewController
{
    NSInteger progressTime;
}

@property (nonatomic, copy) NSString *urlStr;

@property (nonatomic, assign)BOOL existNaviBar;//是否存在导航条

@property (nonatomic, strong)UIButton *returnBtn;

@property (nonatomic, strong)UIButton *closeBtn;

@property (nonatomic, strong)UIButton *shareBtn;

@property (nonatomic, strong)HTEmptyView *emptyView;

@property (nonatomic, strong)ShareCollectionModel *shareUmengModel;

//@property (nonatomic, assign)BOOL isHealthPreservingKnowledge;

@end

