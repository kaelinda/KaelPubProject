//
//  PubChannelCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PubChannelView.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class PubChannelCell;
typedef void (^PubChannelCellBlock) (UIButton_Block*);

@interface PubChannelCell : UITableViewCell

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)PubChannelCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)PubChannelView *leftView;//左边

@property (nonatomic,strong)PubChannelView *rightView;//右边


- (void)update;//跟新数据

@end
