//
//  PubChannelView.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubChannelView.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton_Block.h"


@interface PubChannelView ()
{
    
    UIButton_Block * _btn;
    UIImageView *BGview;
    UIImageView * Badge ;
    
}
@end


@implementation PubChannelView

- (instancetype)init
{
    if (self = [super init]) {

        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
    
    self.photo = [UIImageView new];
    [superView addSubview:self.photo];
    
    
    
    self.subTitle = [UILabel new];
    [superView addSubview:self.subTitle];
    
    [self.photo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        //        make.bottom.equalTo(superView.mas_bottom).offset((-30)*kRateSize);
        make.bottom.equalTo(superView.mas_bottom).offset((-35)*kDeviceRate);
    }];
    
    //    self.photo.backgroundColor = [UIColor redColor];
    //    self.photo.contentMode = UIViewContentModeScaleAspectFit;
    self.photo.image =[UIImage imageNamed:@"poster"];
    
    self.photo.backgroundColor = UIColorFromRGB(0xe3e2e3);
    self.photo.layer.cornerRadius = 3;
    self.photo.layer.masksToBounds = YES;
    
    [self.subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        //        make.top.equalTo(self.photo.mas_bottom).offset(5*kRateSize);
        make.top.equalTo(self.photo.mas_bottom).offset(7*kDeviceRate);
        make.bottom.equalTo(superView.mas_bottom).offset(-15*kDeviceRate);
    }];
    self.subTitle.text = @"当前没有数据";
    
    
    UIFont *font  = HT_FONT_THIRD;
    
    self.subTitle.font = font;
    self.subTitle.textColor = HT_COLOR_FONT_FIRST;
    
    
    
    _btn  = [UIButton_Block new];
    [self addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.photo);
    }];
    
    WS(wself);
    _btn.serviceIDName = @"ID";
    _btn.eventName = self.title.text;
    [_btn setImage:[UIImage imageNamed:@"bg_common_vod_pressed"] forState:UIControlStateHighlighted];
    _btn.Click = ^(UIButton_Block*btn,NSString * name)
    {
        if (wself.block!=nil) {
            NSLog(@"cell知道点击了那个view");
            wself.block(btn);
        }
    };
    
    
    //    self.backgroundColor = [UIColor orangeColor];
}
- (void)updata
{
    
    WS(wself);
    
    NSDictionary *dic = self.dic;
    if (dic) {
        self.hidden = NO;
        
        if ([dic objectForKey:@"name"]) {
            self.subTitle.text = [dic objectForKey:@"name"];
        }
        
        if ([dic objectForKey:@"imageUrl"]) {
            
            [self tranAnimationWith:self.photo andImageLink:[dic objectForKey:@"imageUrl"]];
        }
        
        if ([dic objectForKey:@"programId"]) {
            _btn.serviceIDName = [dic objectForKey:@"programId"];
        }
        
        
        _btn.polyMedicDic = [NSMutableDictionary dictionaryWithDictionary:dic];
        
        if ([dic objectForKey:@"programId"]) {
            _btn.serviceIDName = [dic objectForKey:@"programId"];
        }

        _btn.eventName = self.subTitle.text;
        _btn.Click = ^(UIButton_Block*btn,NSString * name)
        {
            if (wself.block) {
                NSLog(@"cell知道点击了那个view");
                wself.block(btn);
            }
        };
        
    }else
    {
        self.hidden = YES;
        self.photo.image =[UIImage imageNamed:@"poster"] ;
        self.title.text = @"";
        self.subTitle.text = @"";
        self.timeTitle.text = @"";
    }
    
}

-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink{
    
    CATransition *animation = [CATransition animation];
    //动画时间
    animation.duration = 1.0f;
    //display mode, slow at beginning and end
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //在动画执行完时是否被移除
    animation.removedOnCompletion = YES;
    //过渡效果
    animation.type = kCATransitionFade;
    //    animation.type = @"rippleEffect";
    
    //过渡方向
    animation.subtype = kCATransitionFromTop;
    //    //暂时不知,感觉与Progress一起用的,如果不加,Progress好像没有效果
    //    animation.fillMode = kCAFillModeForwards;
    //    //动画停止(在整体动画的百分比).
    //    animation.endProgress = 1;
    [imageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageLink)]  placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [imageView.layer addAnimation:animation forKey:nil];
    }];

}


@end
