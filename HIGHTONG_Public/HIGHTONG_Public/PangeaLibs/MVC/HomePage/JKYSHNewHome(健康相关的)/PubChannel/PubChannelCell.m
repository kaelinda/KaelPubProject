//
//  PubChannelCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PubChannelCell.h"

#import "Masonry.h"
#import "UIButton_Block.h"
@implementation PubChannelCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.dateArray = [NSMutableArray array];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    WS(wself);
    UIView *superView = self.contentView;
    self.leftView = [PubChannelView new];
    [superView addSubview:self.leftView];
    
    self.rightView = [PubChannelView new];
    [superView addSubview:self.rightView];
    
    
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(superView.mas_top);
        make.right.equalTo(superView.mas_centerX).offset(-3*kDeviceRate);
        make.height.mas_equalTo(141*kDeviceRate);
        
    }];
    [self.leftView updata];
    self.leftView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"zou视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-6*kDeviceRate);
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(superView.mas_centerX).offset(3*kDeviceRate);
        make.height.mas_equalTo(141*kDeviceRate);
    }];
    [self.rightView updata];
    self.rightView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"you视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
}
- (void)update
{
    
    if (self.dateArray.count >= 1) {
        NSDictionary *dic = self.dateArray[0];
        
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.leftView.dic = dic;
            self.rightView.dic = nil;
            
            self.leftView.isMovie = self.isMovie;
            
            [self.leftView updata];
            [self.rightView updata];
        }
    }
    if (self.dateArray.count >= 2) {
        NSDictionary *dic = self.dateArray[1];
        
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.rightView.dic = dic;
            
            self.rightView.isMovie = self.isMovie;
            
            [self.leftView updata];
            [self.rightView updata];
        }
    }
    
}
@end
