//
//  PubChannelView.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^PubChannelView_TO_cell_With_btn)(UIButton_Block*);
@interface PubChannelView : UIView

@property(nonatomic,copy)PubChannelView_TO_cell_With_btn block;

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property(nonatomic,strong)UIImageView *photo;//图片

@property(nonatomic,strong)UILabel *title;//标题

@property(nonatomic,strong)UILabel *subTitle;//副标题

@property(nonatomic,strong)UILabel *timeTitle;//更新至


@property(nonatomic,strong)NSDictionary *dic;

- (void)updata;

@end



