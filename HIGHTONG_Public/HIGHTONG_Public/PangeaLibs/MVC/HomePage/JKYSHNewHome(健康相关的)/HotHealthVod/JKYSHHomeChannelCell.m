//
//  JKYSHHomeChannelCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/9/1.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "JKYSHHomeChannelCell.h"
#import "Masonry.h"

@interface JKYSHHomeChannelCell ()

@property(nonatomic,strong)UIImageView *img;

@property(nonatomic,strong)UIButton *btn;

@end


@implementation JKYSHHomeChannelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.dateDic = [NSMutableDictionary dictionary];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _img = [[UIImageView alloc]init];
    
    [self.contentView addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [_btn addSubview:_img];
    
    [_img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_btn);
    }];
    
    
    
    [_btn addTarget:self action:@selector(channelBtnTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)update
{
    
    if ([_dateDic objectForKey:@"postersUrl"]) {

        [self tranAnimationWith:_img andImageLink:[_dateDic objectForKey:@"postersUrl"]];
        
    }
    else
    {
        
        _img.image = [UIImage imageNamed:@"bg_home_poster_onloading_h220"];
        
    }

    
}

-(void)channelBtnTouchUpInside:(UIButton *)button{
    NSLog(@"filter 按钮抬起");
    
    if (self.ChanenelBtnSelected) {
        self.ChanenelBtnSelected(_dateDic);
    }
    
}

-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink{
    
    CATransition *animation = [CATransition animation];
    //动画时间
    animation.duration = 1.0f;
    //display mode, slow at beginning and end
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //在动画执行完时是否被移除
    animation.removedOnCompletion = YES;
    //过渡效果
    animation.type = kCATransitionFade;
    //    animation.type = @"rippleEffect";
    
    //过渡方向
    animation.subtype = kCATransitionFromTop;
    //    //暂时不知,感觉与Progress一起用的,如果不加,Progress好像没有效果
    //    animation.fillMode = kCAFillModeForwards;
    //    //动画停止(在整体动画的百分比).
    //    animation.endProgress = 1;
    [imageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageLink)]  placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading_h220"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [imageView.layer addAnimation:animation forKey:nil];
    }];
    
    
    
    
}

@end
