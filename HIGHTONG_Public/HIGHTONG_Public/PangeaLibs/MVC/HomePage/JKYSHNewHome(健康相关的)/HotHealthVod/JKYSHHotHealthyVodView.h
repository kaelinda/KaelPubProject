//
//  JKYSHHotHealthyVodView.h
//  HIGHTONG
//
//  Created by testteam on 16/9/5.
//  Copyright (c) 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"



typedef void (^JKYSHHotHealthyVodView_TO_cell_With_btn)(UIButton_Block*);
@interface JKYSHHotHealthyVodView : UIView

@property(nonatomic,copy)JKYSHHotHealthyVodView_TO_cell_With_btn block;

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property(nonatomic,strong)UIImageView *photo;//图片

@property(nonatomic,strong)UILabel *title;//标题

@property(nonatomic,strong)UILabel *subTitle;//副标题

@property(nonatomic,strong)UILabel *timeTitle;//更新至

//@property(nonatomic,strong)programModel *model;

@property(nonatomic,strong)NSDictionary *dic;

- (void)updata;

@end
