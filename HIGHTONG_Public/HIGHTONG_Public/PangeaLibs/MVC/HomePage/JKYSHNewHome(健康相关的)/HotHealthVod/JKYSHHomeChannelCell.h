//
//  JKYSHHomeChannelCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/9/1.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKYSHHomeChannelCell.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class JKYSHHomeChannelCell;

@interface JKYSHHomeChannelCell : UITableViewCell

@property (nonatomic,strong)NSMutableDictionary *dateDic; //存放字典数组

/**
 *  @author Kael
 *
 *  @brief 监控选择器按钮的选中状态
 */
@property (nonatomic,copy) void (^ ChanenelBtnSelected)(NSDictionary *dic);


- (void)update;

@end

