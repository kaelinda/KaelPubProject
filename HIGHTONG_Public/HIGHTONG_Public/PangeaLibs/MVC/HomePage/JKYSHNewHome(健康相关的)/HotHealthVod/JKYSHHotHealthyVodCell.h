//
//  JKYSHHotHealthyVodCell.h
//  HIGHTONG
//
//  Created by testteam on 16/9/5.
//  Copyright (c) 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JKYSHHotHealthyVodView.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class JKYSHHotHealthyVodCell;
typedef void (^JKYSHHotHealthyVodCelllBlock) (UIButton_Block*);

@interface JKYSHHotHealthyVodCell : UITableViewCell

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)JKYSHHotHealthyVodCelllBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)JKYSHHotHealthyVodView *leftView;//左边

@property (nonatomic,strong)JKYSHHotHealthyVodView *rightView;//右边


- (void)update;//跟新数据

@end
