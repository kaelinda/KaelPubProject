//
//  HotLiveTableCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HotLiveTableCell.h"
#import "GeneralButton.h"

#import "HotLiveItemBtn.h"

#define btnWidth 93*kDeviceRate//79.5*kRateSize//*kDeviceRate
#define btnHeight 70*kDeviceRate//60*kRateSize//*kDeviceRate

@interface HotLiveTableCell ()
{
//    GeneralButton *firstBtn;
//    GeneralButton *secondBtn;
//    GeneralButton *thirdBtn;
//    GeneralButton *fourthBtn;
    
    HotLiveItemBtn *firstBtn;
    HotLiveItemBtn *secondBtn;
    HotLiveItemBtn *thirdBtn;
    HotLiveItemBtn *fourthBtn;
    
    UIImageView  *firstImg;
    UIImageView *secondImg;
    UIImageView *thirdImg;
    UIImageView *fourImg;
}
@end

@implementation HotLiveTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.liveArr = [[NSMutableArray alloc] initWithCapacity:0];
        [self setUpView];
    }
    return self;
}


- (void)setUpView{
    UIView *superView = self.contentView;
    
    WS(wself);
    
    firstBtn = [[HotLiveItemBtn alloc] init];
    secondBtn = [[HotLiveItemBtn alloc] init];
    thirdBtn = [[HotLiveItemBtn alloc] init];
    fourthBtn = [[HotLiveItemBtn alloc] init];
    
    //UIImageView *
    firstImg = [UIImageView new];
    //UIImageView *
    secondImg = [UIImageView new];
    //UIImageView *
    thirdImg = [UIImageView new];
    
    fourImg = [UIImageView new];
    
    [superView addSubview:firstBtn];
    [superView addSubview:secondBtn];
    [superView addSubview:thirdBtn];
    [superView addSubview:fourthBtn];
    
    [superView addSubview:firstImg];
    [superView addSubview:secondImg];
    [superView addSubview:thirdImg];
    [superView addSubview:fourImg];
    
    [firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(superView.mas_left);//.offset(gap);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [firstImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(firstBtn.mas_right);//.offset(gap);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(firstBtn.mas_height);
        make.centerY.mas_equalTo(firstBtn.mas_centerY);
    }];
    
    
    [secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(firstImg.mas_right);//.offset(gap);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [secondImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(secondBtn.mas_right);//.offset(gap);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(secondBtn.mas_height);
        make.centerY.mas_equalTo(secondBtn.mas_centerY);
    }];
    
    
    [thirdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(secondImg.mas_right);//.offset(gap);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [thirdImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(thirdBtn.mas_right);//.offset(gap);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(thirdBtn.mas_height);
        make.centerY.mas_equalTo(thirdBtn.mas_centerY);
    }];
    
    
    [fourthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(thirdImg.mas_right);//.offset(gap);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [fourImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(fourthBtn.mas_right);//.offset(gap);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(fourthBtn.mas_height);
        make.centerY.mas_equalTo(fourthBtn.mas_centerY);
    }];
    
    
    [firstImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [secondImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [thirdImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [fourImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    
    firstBtn.buttonBlock = ^(HotLiveItemBtn *btn, NSString *LiveID, NSString *serviceName){
        if (wself.liveBlock) {

            wself.liveBlock(btn, LiveID, serviceName);
        }
        
        NSLog(@"热门直播1");
    };
    
    secondBtn.buttonBlock = ^(HotLiveItemBtn *btn, NSString *LiveID, NSString *serviceName){
        if (wself.liveBlock) {

            wself.liveBlock(btn, LiveID, serviceName);
        }
        
        NSLog(@"热门直播2");
    };
    
    thirdBtn.buttonBlock = ^(HotLiveItemBtn *btn, NSString *LiveID, NSString *serviceName){
        if (wself.liveBlock) {

            wself.liveBlock(btn, LiveID, serviceName);
        }
        
        NSLog(@"热门直播3");
    };
    
    fourthBtn.buttonBlock = ^(HotLiveItemBtn *btn, NSString *LiveID,NSString *serviceName){
        if (wself.liveBlock) {

            wself.liveBlock(btn, LiveID, serviceName);
        }
        
        NSLog(@"热门直播4");
    };
}


//- (void)setUpView{
//    UIView *superView = self.contentView;
//    
//    WS(wself);
//    
//    firstBtn = [[GeneralButton alloc] init];
//    secondBtn = [[GeneralButton alloc] init];
//    thirdBtn = [[GeneralButton alloc] init];
//    fourthBtn = [[GeneralButton alloc] init];
//    
//    //UIImageView *
//    firstImg = [UIImageView new];
//    //UIImageView *
//    secondImg = [UIImageView new];
//    //UIImageView *
//    thirdImg = [UIImageView new];
//    
//    fourImg = [UIImageView new];
//    
//    [superView addSubview:firstBtn];
//    [superView addSubview:secondBtn];
//    [superView addSubview:thirdBtn];
//    [superView addSubview:fourthBtn];
//    
//    [superView addSubview:firstImg];
//    [superView addSubview:secondImg];
//    [superView addSubview:thirdImg];
//    [superView addSubview:fourImg];
//    
//    [firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(btnWidth);
//        make.height.mas_equalTo(btnHeight);
//        make.left.mas_equalTo(superView.mas_left);//.offset(gap);
//        make.top.mas_equalTo(superView.mas_top);
//    }];
//    
//    [firstImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(firstBtn.mas_right);//.offset(gap);
//        make.width.mas_equalTo(0.5*kRateSize);
//        make.height.mas_equalTo(firstBtn.mas_height);
//        make.centerY.mas_equalTo(firstBtn.mas_centerY);
//    }];
//    
//    
//    [secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(btnWidth);
//        make.height.mas_equalTo(btnHeight);
//        make.left.mas_equalTo(firstImg.mas_right);//.offset(gap);
//        make.top.mas_equalTo(superView.mas_top);
//    }];
//    
//    [secondImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(secondBtn.mas_right);//.offset(gap);
//        make.width.mas_equalTo(0.5*kRateSize);
//        make.height.mas_equalTo(secondBtn.mas_height);
//        make.centerY.mas_equalTo(secondBtn.mas_centerY);
//    }];
//    
//    
//    [thirdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(btnWidth);
//        make.height.mas_equalTo(btnHeight);
//        make.left.mas_equalTo(secondImg.mas_right);//.offset(gap);
//        make.top.mas_equalTo(superView.mas_top);
//    }];
//    
//    [thirdImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(thirdBtn.mas_right);//.offset(gap);
//        make.width.mas_equalTo(0.5*kRateSize);
//        make.height.mas_equalTo(thirdBtn.mas_height);
//        make.centerY.mas_equalTo(thirdBtn.mas_centerY);
//    }];
//    
//    
//    [fourthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(btnWidth);
//        make.height.mas_equalTo(btnHeight);
//        make.left.mas_equalTo(thirdImg.mas_right);//.offset(gap);
//        make.top.mas_equalTo(superView.mas_top);
//    }];
//    
//    [fourImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(fourthBtn.mas_right);//.offset(gap);
//        make.width.mas_equalTo(0.5*kRateSize);
//        make.height.mas_equalTo(fourthBtn.mas_height);
//        make.centerY.mas_equalTo(fourthBtn.mas_centerY);
//    }];
//    
////    [firstImg setBackgroundColor:[UIColor blackColor]];
////    [secondImg setBackgroundColor:[UIColor blackColor]];
////    [thirdImg setBackgroundColor:[UIColor blackColor]];
////    [fourImg setBackgroundColor:[UIColor blackColor]];
//    
//    [firstImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
//    [secondImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
//    [thirdImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
//    [fourImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
//    
//    firstBtn.buttonBlock = ^(UIButton *btn, NSString *LiveID){
//        if (wself.liveBlock) {
//            firstBtn.backgroundColor = App_background_color;
//            wself.liveBlock(btn, LiveID);
//        }else{
//            firstBtn.backgroundColor = CLEARCOLOR;
//        }
//        
//        NSLog(@"热门直播1");
//    };
//    
//    secondBtn.buttonBlock = ^(UIButton *btn, NSString *LiveID){
//        if (wself.liveBlock) {
//            secondBtn.backgroundColor = App_background_color;
//            wself.liveBlock(btn, LiveID);
//        }else{
//            firstBtn.backgroundColor = CLEARCOLOR;
//        }
//        
//        NSLog(@"热门直播2");
//    };
//    
//    thirdBtn.buttonBlock = ^(UIButton *btn, NSString *LiveID){
//        if (wself.liveBlock) {
//            thirdBtn.backgroundColor = App_background_color;
//            wself.liveBlock(btn, LiveID);
//        }else{
//            firstBtn.backgroundColor = CLEARCOLOR;
//        }
//        
//        NSLog(@"热门直播3");
//    };
//    
//    fourthBtn.buttonBlock = ^(UIButton *btn, NSString *LiveID){
//        if (wself.liveBlock) {
//            fourthBtn.backgroundColor = App_background_color;
//            wself.liveBlock(btn, LiveID);
//        }else{
//            firstBtn.backgroundColor = CLEARCOLOR;
//        }
//        
//        NSLog(@"热门直播4");
//    };
//}

- (void)refreshHotLiveView
{
    if (self.liveArr.count>=1) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.liveArr[0];
        
        if (self.liveArr.count == 1) {
            firstImg.hidden = YES;
            secondImg.hidden = YES;
            thirdImg.hidden = YES;
            fourImg.hidden = YES;
            
            
            secondBtn.hidden = YES;
            thirdBtn.hidden = YES;
            fourthBtn.hidden = YES;
        }else{
            firstImg.hidden = NO;
            secondImg.hidden = NO;
            thirdImg.hidden = NO;
            fourImg.hidden = NO;
            
            
            secondBtn.hidden = NO;
            thirdBtn.hidden = NO;
            fourthBtn.hidden = NO;
        }
        
        [firstBtn refreshGeneralButtonWithDic:dic];
    }
    
    if (self.liveArr.count>=2) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.liveArr[1];
        
        if (self.liveArr.count == 2) {
            secondImg.hidden = YES;
            thirdImg.hidden = YES;
            fourImg.hidden = YES;
            
            
            thirdBtn.hidden = YES;
            fourthBtn.hidden = YES;
        }else{
            secondImg.hidden = NO;
            thirdImg.hidden = NO;
            fourImg.hidden = NO;
            
            
            thirdBtn.hidden = NO;
            fourthBtn.hidden = NO;
        }
        
        [secondBtn refreshGeneralButtonWithDic:dic];
    }
    
    if (self.liveArr.count>=3) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.liveArr[2];
        
        if (self.liveArr.count == 3) {
            thirdImg.hidden = YES;
            fourImg.hidden = YES;
            
            
            fourthBtn.hidden = YES;
        }else{
            thirdImg.hidden = NO;
            fourImg.hidden = NO;
            
            
            fourthBtn.hidden = NO;
        }
        
        [thirdBtn refreshGeneralButtonWithDic:dic];
    }
    
    if (self.liveArr.count>=4) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.liveArr[3];
        
        if (self.liveArr.count == 4) {
            fourImg.hidden = YES;
        }else{
            fourImg.hidden = NO;
        }
        
        [fourthBtn refreshGeneralButtonWithDic:dic];
    }
}

- (void)setValueForButton:(GeneralButton *)btn withDic:(NSDictionary *)dic
{
    
    [btn refreshGeneralButtonWithDic:dic];
}

@end
