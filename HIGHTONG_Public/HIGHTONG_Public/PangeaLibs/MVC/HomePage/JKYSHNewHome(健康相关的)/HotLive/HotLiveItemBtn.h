//
//  HotLiveItemBtn.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/13.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotLiveItemBtn;

typedef void(^HotLiveItemBtnBlock)(HotLiveItemBtn *btn, NSString *channelID, NSString *serviceName);

@interface HotLiveItemBtn : UIButton

@property (nonatomic, copy)HotLiveItemBtnBlock buttonBlock;

@property (nonatomic, strong)UIImageView *posterImg;

@property (nonatomic, strong)UILabel *nameLab;

@property (nonatomic, strong)NSString *channelID;

@property (nonatomic, strong)NSString *serviceName;

- (void)refreshGeneralButtonWithDic:(NSDictionary *)dic;

@end
