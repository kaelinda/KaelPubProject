//
//  HotLiveTableCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HotLiveTableCellBlock)(UIButton *btn, NSString *liveID, NSString *serviceName);
@interface HotLiveTableCell : UITableViewCell

@property (nonatomic, copy)HotLiveTableCellBlock liveBlock;

@property (nonatomic, strong)NSMutableArray *liveArr;

- (void)refreshHotLiveView;

@end
