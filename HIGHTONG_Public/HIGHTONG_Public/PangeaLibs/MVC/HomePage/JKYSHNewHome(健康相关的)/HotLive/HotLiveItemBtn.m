//
//  HotLiveItemBtn.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/13.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HotLiveItemBtn.h"

@implementation HotLiveItemBtn

- (instancetype)init
{
    self = [super init];
    if (self) {

        self.adjustsImageWhenHighlighted = NO;
        
        _posterImg = [[UIImageView alloc] init];
        [self addSubview:_posterImg];
        
        _nameLab = [[UILabel alloc] init];
        [self addSubview:_nameLab];
        
        
        [self setImage:[UIImage imageNamed:@"hot_live_img_pressed"] forState:UIControlStateHighlighted];
        [self addTarget:self action:@selector(clickTheButtonDown:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)clickTheButtonDown:(HotLiveItemBtn *)btn
{
    if (_buttonBlock) {
        NSLog(@"点击了单个热门直播");
        _buttonBlock(btn,_channelID,_serviceName);
    }else
    {
        NSLog(@"没点击单个热门直播");
    }
}

- (void)refreshGeneralButtonWithDic:(NSDictionary *)dic
{
    _posterImg.frame = CGRectMake(24*kDeviceRate, 0, 45*kDeviceRate, 39*kDeviceRate);
    
//    _imageBtn.frame = CGRectMake(17*kRateSize, 0, 45*kRateSize, 39*kRateSize);
    
    _nameLab.frame = CGRectMake(0, 41*kDeviceRate, 93*kDeviceRate, 19*kDeviceRate);
    
    //    [_imageBtn setImage:[UIImage imageNamed:@"hot_live_img_pressed"] forState:UIControlStateHighlighted];
    
    if ([[dic objectForKey:@"imageLink"] length]) {
        
        [_posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@""]];
    }
    
    if ([[dic objectForKey:@"serviceName"] length]) {
        _nameLab.text = [dic objectForKey:@"serviceName"];
    }
    
    _nameLab.font = HT_FONT_THIRD;
    _nameLab.textColor = HT_COLOR_FONT_FIRST;
    _nameLab.textAlignment = NSTextAlignmentCenter;
    
    if ([[dic objectForKey:@"serviceID"] length]) {
        _channelID = [dic objectForKey:@"serviceID"];
    }
    
    
    if (([[dic objectForKey:@"serviceName"] length])) {
        
        _serviceName = [dic objectForKey:@"serviceName"];
    }
}


@end
