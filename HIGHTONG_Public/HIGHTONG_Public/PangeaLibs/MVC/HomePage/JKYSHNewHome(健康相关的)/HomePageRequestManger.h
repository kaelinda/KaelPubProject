//
//  HomePageRequestManger.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/9/8.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol HomePageRequestDelegate <NSObject>

/**
 *  首页header模块的数据准备成功 回调
 *
 *  @param cycleImageResult 轮播图数据
 *  @param modulesResult    功能模块数据
 *  @param homeNewsResult   新闻模块数据
 */
-(void)homeHeaderDataGetSuccessWith:(NSMutableArray *)cycleImageResult andModules:(NSMutableArray *)modulesResult andHomeNews:(NSMutableArray *)homeNewsResult andHomeNewsUrl:(NSString *)url;

/**
 *  健康精选数据回调
 *
 *  @param result 健康精选数据
 */
-(void)greatHealthChannelDataGetSuccessWith:(NSMutableArray *)result;
/**
 *  热门直播数据回调
 *
 *  @param result 热门直播数据
 */
-(void)hotChannelDataGetSuccessWith:(NSMutableArray *)result;
/**
 *  点播数据回调
 *
 *  @param result 点播数据
 */
-(void)homeVODDataGetSuccessWith:(NSMutableArray *)result;
/**
 *  健康视频的回调
 *
 *  @param result 健康视频的数据
 */
-(void)homeHealthVideoGetSuccessWith:(NSMutableArray *)result;

/**
 *  轻松一刻的回调
 *
 *  @param result 轻松一刻的数据
 */
-(void)homePubChannelGetSuccessWith:(NSMutableDictionary *)result;

/**
 *  首页底部图的回调
 *
 *  @param result 首页底部图的数据
 */
-(void)homeBottomImageGetSuccessWith:(NSMutableDictionary *)result;


@end

@interface HomePageRequestManger : NSObject




@property (nonatomic,assign) id<HomePageRequestDelegate> delegate;


-(id)initWithDelegate:(id <HomePageRequestDelegate>)delegate;

    //**************  刷新各分支 模块 的数据
-(void)reloadHeaderData;

-(void)reloadGreatHealthData;

-(void)reloadHotChannelData;

-(void)reloadPubChannelData;

-(void)reloadVODData;

-(void)reloadhealthVideoData;

-(void)reloadFooterData;
    //**************


@end
