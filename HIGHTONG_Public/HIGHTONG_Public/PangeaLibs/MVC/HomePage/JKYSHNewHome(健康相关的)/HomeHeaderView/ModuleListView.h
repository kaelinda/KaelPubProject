//
//  ModuleListView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GeneralButton.h"

#define Animation_duration 0.75
@interface ModuleListView : UIView

/**
 *  检测功能模块视图的frame变化
 */
@property (nonatomic,copy) void (^sizeChangedBlock)(CGRect newFrame);
/**
 *  选中某个模块的回调方法
 */
@property (nonatomic,copy) void (^didSelectedModule)(NSInteger index,NSString *typeStr,NSString *itemName);
/**
 *  模块数组
 */
@property (nonatomic,strong) NSMutableArray *moduleArr;

/**
 *  用来测试的方法 添加几个子视图
 *
 *  @param viewCount 个数
 */
-(void)reloadWith:(NSInteger)viewCount;

/**
 *  重载视图
 *
 *  @param moduleList 功能模块数组
 */
-(void)reloadViewWith:(NSArray *)moduleList;



@end
