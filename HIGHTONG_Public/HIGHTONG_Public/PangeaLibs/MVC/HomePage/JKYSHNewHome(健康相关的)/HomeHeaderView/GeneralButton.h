//
//  GeneralButton.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GeneralButtonBlock)(UIButton *btn, NSString *channelID);

@interface GeneralButton : UIView
{
    UIButton *_imageBtn;
    UILabel *_nameLab;
    UIImageView *_posterImg;
}

@property (nonatomic, strong)UIImageView *posterImg;

@property (nonatomic, strong)UILabel *nameLab;

@property (nonatomic, strong)UIButton *imageBtn;

@property (nonatomic, copy)GeneralButtonBlock buttonBlock;

@property (nonatomic, assign)NSString *normalImgUrlStr;//常态图

@property (nonatomic, assign)NSString *backImgUrlStr;//背景占位图

@property (nonatomic, assign)NSString *highLightImgUrlStr;//高亮选中图

@property (nonatomic, assign)NSString *titleStr;//按钮下方title

@property (nonatomic)CGFloat fontSize;//title字号

@property (nonatomic,strong) UIFont *labFont;

@property (nonatomic, strong)UIColor *color;//title颜色

@property (nonatomic)CGSize btnSize;//按钮的size

@property (nonatomic, assign)NSString *channelID;


- (void)refreshGeneralButtonUI;

- (void)refreshGeneralButtonWithDic:(NSDictionary *)dic;

@end
