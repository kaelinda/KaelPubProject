//
//  HomeNewsView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDCycleScrollView.h"

#define News_Scroll_Time 3
#define News_text_left_space 5

@interface HomeNewsView : UIView <SDCycleScrollViewDelegate>

/**
 *  左侧icon
 */
@property (nonatomic,strong) UIImageView *leftNewsImageView;

@property (nonatomic,strong) UIImageView *leftLine;// 分割线

@property (nonatomic,strong) SDCycleScrollView *topNewsScrollView;

@property (nonatomic,strong) SDCycleScrollView *bottomNewsScrollView;

@property (nonatomic,strong) NSMutableArray *newsArr;

@property (nonatomic,strong) NSMutableArray *topNewsArr;

@property (nonatomic,strong) NSMutableArray *bottomNewsArr;

@property (nonatomic,copy) void(^itemClickedBlock)(NSInteger index,NSString *news,NSString *newsID,NSString *newsUrl);

@property (nonatomic,copy) void(^ clickActionBlock)(NSString *str);

@property (nonatomic,assign) CGSize leftImageSize;
/**
 *  重载
 *
 *  @param newsArr 新闻条目
 */
-(void)reloadNewsWith:(NSArray *)newsArr;



@end
