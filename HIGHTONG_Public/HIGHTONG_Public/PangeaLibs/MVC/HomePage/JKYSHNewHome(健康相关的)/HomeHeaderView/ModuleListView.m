//
//  ModuleListView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "ModuleListView.h"

@interface ModuleListView(){
    
    CGFloat kWidth;
    
}
@end

@implementation ModuleListView


-(instancetype)init{
    if (self == [super init]) {
        self.clipsToBounds = YES;
    }
    return self;
}

-(void)reloadViewWith:(NSArray *)moduleList{

    [_moduleArr removeAllObjects];
    _moduleArr = [moduleList mutableCopy];
    
    if (_moduleArr.count>0) {
        [self reloadWith:moduleList.count];
    }
}

-(void)reloadWith:(NSInteger)viewCount{
    [self removeAllSubviews];

    WS(wself);
    CGSize btnSize = CGSizeMake(KDeviceHeight > kDeviceWidth ? kDeviceWidth/4 : KDeviceHeight/4, 70*kRateSize);
    CGFloat spaceHeight = 0*kDeviceRate;
    
    for (int i = 0; i < viewCount; i++) {
        NSDictionary *itemDic = [_moduleArr objectAtIndex:i];
        NSString *image = [itemDic objectForKey:@"moduleLogo"];
        NSString *moduleLogo = NSStringPM_IMGFormat(image);
        NSString *moduleName = [itemDic objectForKey:@"moduleName"];
        
        GeneralButton *button = [[GeneralButton alloc] init];
        button.tag = i;

        NSInteger lineNum = i/4 + 1;

        button.frame = CGRectMake((i%4) * (btnSize.width), 10+(i/4) * (btnSize.height-5) + lineNum*spaceHeight, btnSize.width, btnSize.height);
        
        NSLog(@"^^^^^^^^^^%f---%f",button.frame.origin.x,button.frame.origin.y);
        button.btnSize = CGSizeMake(50*kDeviceRate, 50*kDeviceRate);
        button.backImgUrlStr = [NSString stringWithFormat:@"icon%d",i+1];
        button.labFont = HT_FONT_FIFTH;
        button.nameLab.textColor = APP_Six_color;
        [button setBackgroundColor:App_white_color];
        [self addSubview:button];
        
        if (NotEmptyStringAndNilAndNull(moduleLogo)) {
            button.normalImgUrlStr = moduleLogo;
        }
        if (NotEmptyStringAndNilAndNull(moduleName)) {
            button.titleStr = moduleName;
        }
        
        [button refreshGeneralButtonUI];
        
        button.buttonBlock = ^(UIButton *btn,NSString *liveID){
        
            NSInteger index = i;
            if (wself.didSelectedModule && wself.moduleArr.count>index) {
                NSDictionary *itemDic = [wself.moduleArr objectAtIndex:index];
                NSString *type = [itemDic objectForKey:@"moduleType"];
                NSString *info = [itemDic objectForKey:@"moduleInfo"];
                wself.didSelectedModule(index,type,info);
            }
            
        };

    }
    
    CGRect orFrame = self.frame;
    NSInteger line = viewCount/4 + ((viewCount%4 == 0) ? 0 : 1);
    CGRect newFrame = CGRectMake(orFrame.origin.x, orFrame.origin.y, orFrame.size.width, line * (btnSize.height-5) + line*spaceHeight + (line>0 ? 10 : 1) );

    if (_sizeChangedBlock) {
        _sizeChangedBlock(newFrame);
    }
    [UIView animateWithDuration:Animation_duration animations:^{
        self.frame = newFrame;
    } completion:^(BOOL finished) {
       
    }];
    
}

-(void)didSeletedIn:(NSInteger)index{

    if (_didSelectedModule) {
        _didSelectedModule(index,@"itemType",@"itemName");
    }
}

//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
//{
//    
//    // 如果当前处于横屏显示状态
//    if(UIInterfaceOrientationIsLandscape(interfaceOrientation))
//    {
//        
//        kWidth = KDeviceHeight/4;
//        
//    }else
//    {
//        
//        kWidth = kDeviceWidth/4;
//
//    }
//    
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
