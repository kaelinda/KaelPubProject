//
//  GeneralButton.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/30.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "GeneralButton.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

@implementation GeneralButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        
//        [self setBackgroundColor:[UIColor purpleColor]];
        [self initializationControlView];
        
    }
    
    return self;
}

- (void)initializationControlView
{
    _posterImg = [[UIImageView alloc] init];
    [self addSubview:_posterImg];
    
    _imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_imageBtn addTarget:self action:@selector(imageBtnClickedDown:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_imageBtn];

    
    _nameLab = [[UILabel alloc] init];
    [self addSubview:_nameLab];
}

- (void)refreshGeneralButtonUI
{
    _posterImg.frame = CGRectMake((self.frame.size.width-_btnSize.width)/2, 0, _btnSize.width, _btnSize.height);
    
    _imageBtn.frame = CGRectMake((self.frame.size.width-_btnSize.width)/2, 0, _btnSize.width, _btnSize.height);
    
    _nameLab.frame = CGRectMake(0, CGRectGetMaxY(_imageBtn.frame)-5, self.frame.size.width, (self.frame.size.height-_imageBtn.frame.size.height));
    
    
    _nameLab.backgroundColor = [UIColor clearColor];
    
//    [_imageBtn setBackgroundImage:[UIImage imageNamed:_backImgUrlStr.length?_backImgUrlStr:@""] forState:UIControlStateNormal];
//    [_imageBtn setImage:[UIImage imageNamed:_normalImgUrlStr.length?_normalImgUrlStr:@""] forState:UIControlStateNormal];
    
    [_posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(_normalImgUrlStr.length?_normalImgUrlStr:@"")] placeholderImage:[UIImage imageNamed:_backImgUrlStr.length?_backImgUrlStr:@""]];
    [_imageBtn setImage:[UIImage imageNamed:_highLightImgUrlStr.length?_highLightImgUrlStr:@""] forState:UIControlStateHighlighted];
    _nameLab.text = _titleStr;
    _nameLab.font = _labFont ? _labFont : [UIFont systemFontOfSize:_fontSize];
    _nameLab.textColor = _color;
    _nameLab.textAlignment = NSTextAlignmentCenter;
}


- (void)refreshGeneralButtonWithDic:(NSDictionary *)dic
{
    _posterImg.frame = CGRectMake(17*kRateSize, 0, 45*kRateSize, 39*kRateSize);
    
    _imageBtn.frame = CGRectMake(17*kRateSize, 0, 45*kRateSize, 39*kRateSize);
    
    _nameLab.frame = CGRectMake(0, 39*kRateSize, 79*kRateSize, 11*kRateSize);
    
    if ([[dic objectForKey:@"imageLink"] length]) {
        
         [_posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@""]];
    }
    
    [_imageBtn setImage:[UIImage imageNamed:_highLightImgUrlStr.length?_highLightImgUrlStr:@""] forState:UIControlStateHighlighted];

    if ([[dic objectForKey:@"serviceName"] length]) {
        _nameLab.text = [dic objectForKey:@"serviceName"];
    }

    _nameLab.font = _labFont?_labFont:App_font(11);
    _nameLab.textColor = UIColorFromRGB(0x333333);
    _nameLab.textAlignment = NSTextAlignmentCenter;
    
    if ([[dic objectForKey:@"serviceID"] length]) {
        _channelID = [dic objectForKey:@"serviceID"];
    }

}


- (void)imageBtnClickedDown:(UIButton *)btn
{
    WS(wself);
    if (wself.buttonBlock) {
        wself.buttonBlock(btn, wself.channelID);
    }
}

@end
