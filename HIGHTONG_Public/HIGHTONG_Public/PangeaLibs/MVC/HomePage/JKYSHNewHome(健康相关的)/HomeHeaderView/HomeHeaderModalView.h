//
//  HomeHeaderModalView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "SDCycleScrollView.h"
#import "HomeNewsView.h"
#import "ModuleListView.h"

#define Header_animation_duration 0.3

typedef void(^HomeHeaderReturnBlcok)(NSInteger index,NSString *itemType,NSString *itemID);

typedef void(^HomeHeaderNewsReturnBlcok)(NSInteger index,NSString *itemType,NSString *itemID,NSString *itemUrl);

@interface HomeHeaderModalView : UIView<SDCycleScrollViewDelegate>

/**
 *  @brief 首页轮播图
 */
@property (nonatomic,strong) SDCycleScrollView *homeCirecleImage;

@property (nonatomic,strong) NSMutableArray *headResultArr;
/**
 *  轮播图 标题数组
 */
@property (nonatomic,strong) NSMutableArray *cycleTitlesArr;
/**
 *  轮播图 图片数组
 */
@property (nonatomic,strong) NSMutableArray *cycleImagesArr;

/**
 *  轮播图的业务类型  广告？图片？视频？
 */
@property (nonatomic,strong) NSMutableArray *cycleTypesArr;
/**
 *  图片对应的  节目ID  或者是 htm的网页链接
 */
@property (nonatomic,strong) NSMutableArray *cycleItemIDArr;
/**
 *  轮播图响应事件
 */
@property (nonatomic,copy) HomeHeaderReturnBlcok cycleScrollViewBlock;

/**
 *  重载轮播图
 *
 *  @param cycleScrollArr 轮播图数据
 */
-(void)reloadHomeCycleScrollViewWith:(NSArray *)cycleScrollArr;


/**
 *  @brief -------------------首页功能模块视图（动态数量的）
 */
@property (nonatomic,strong) ModuleListView *homeModuleListView;

/**
 *  每个模块选中后的回调
 */
@property (nonatomic,copy) HomeHeaderReturnBlcok moduleListViewBlock;

/**
 *  功能模块数组
 */
@property (nonatomic,strong) NSMutableArray *moduleArr;
/**
 *  重载 功能模视图
 *
 *  @param modulListArr 功能模块 视图的数据源
 */
-(void)reloadModuleListViewWith:(NSArray *)modulListArr;

/**
 *  @brief----------------- 首页新闻模块
 */
@property (nonatomic,strong) HomeNewsView *homeNewsView;
/**
 *  新闻模块点击事件
 */
@property (nonatomic,copy) HomeHeaderNewsReturnBlcok newsViewBlock;

/**
 *  新闻条目数组
 */
@property (nonatomic,strong) NSMutableArray *newsArr;

/**
 *  重新加载新闻
 *
 *  @param newsArr 新闻条目数组
 */
-(void)reloadNewsViewWith:(NSArray *)newsArr;



@property (nonatomic,copy) void(^sizeChangedBlock)(CGRect newFrame);


@end
