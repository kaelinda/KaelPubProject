//
//  HomeNewsView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HomeNewsView.h"

@interface HomeNewsView(){

    CGFloat kWidth;
    CGFloat kHeight;
    NSInteger topNewsInteger;
    NSInteger bottomNewsInteger;

}

@end

@implementation HomeNewsView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        
        kWidth = frame.size.width;
        kHeight = frame.size.height;
        
        
        [self initData];
        [self initUI];
    }
    return self;
}

-(instancetype)init{
    NSAssert(NO, @"请使用--->>：initWithFrame:(CGRect)frame");
    if (self == [super init]) {
        [self initData];
        [self initUI];
    }
    return self;
}

-(void)initData{

    _newsArr = [[NSMutableArray alloc] init];
    _topNewsArr = [[NSMutableArray alloc] init];
    _bottomNewsArr = [[NSMutableArray alloc] init];
    _leftImageSize = CGSizeMake(75*kDeviceRate, 52.5*kDeviceRate);

}
-(void)setNewsArr:(NSMutableArray *)newsArr{
    if (_newsArr.count>0) {
        [_newsArr removeAllObjects];
    }
    if (_topNewsArr.count>0) {
        [_topNewsArr removeAllObjects];
    }
    if (_bottomNewsArr.count>0) {
        [_bottomNewsArr removeAllObjects];
    }

        //**********
    [newsArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *newsDic = (NSDictionary *)obj;
        NSString *newsString = [newsDic objectForKey:@"title"];
        NSLog(@"健康咨询：%@",newsString);
        if (idx%2 == 0) {
            [_topNewsArr addObject:newsString];
        }else{
            [_bottomNewsArr addObject:newsString];
        }
    }];
        //**********
    _topNewsScrollView.titlesGroup = [_topNewsArr copy];
    _bottomNewsScrollView.titlesGroup = [_bottomNewsArr copy];
        //**********
    _newsArr = [NSMutableArray arrayWithArray:newsArr];

}

-(void)initUI{
    
    WS(wself);
        //********** icon
    _leftNewsImageView = [[UIImageView alloc] init];
//    _leftNewsImageView.layer.contentsGravity = kCAGravityCenter;
    _leftNewsImageView.contentMode = UIViewContentModeCenter;
    _leftNewsImageView.image = [UIImage imageNamed:@"home_healthy_news_img"];
    [_leftNewsImageView setFrame:CGRectMake(0, 0, _leftImageSize.width - 1*kDeviceRate, _leftImageSize.height)];
    [self addSubview:_leftNewsImageView];
    
    _leftLine = [[UIImageView alloc]init];
    _leftLine.contentMode = UIViewContentModeScaleToFill;
    _leftLine.image = [UIImage imageNamed:@"home_cell_parting_line"];
    [_leftLine setFrame:CGRectMake(_leftImageSize.width - 1*kDeviceRate, 0, 1*kDeviceRate, _leftImageSize.height)];
    [self addSubview:_leftLine];
    
    
    
        //********** top News View
    _topNewsScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(_leftImageSize.width + News_text_left_space, 0, (kDeviceWidth-_leftImageSize.width) - News_text_left_space, 26*kDeviceRate) delegate:self placeholderImage:nil];
    [self initBasePropertyWith:_topNewsScrollView];
    
    [self addSubview:_topNewsScrollView];
    
    UIView *topView = [[UIView alloc]init];
    
    [_topNewsScrollView addSubview:topView];
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topNewsScrollView.mas_top);
        make.bottom.equalTo(_topNewsScrollView.mas_bottom);
        make.left.equalTo(_topNewsScrollView.mas_left);
        make.right.equalTo(_topNewsScrollView.mas_right);
    }];
    
    topView.backgroundColor = [UIColor clearColor];
//    topView.userInteractionEnabled = NO;
    
    _topNewsScrollView.clickItemOperationBlock = ^(NSInteger index){
        if (wself.itemClickedBlock) {
            NSDictionary *itemDic = [wself.newsArr objectAtIndex:(index * 2)];
            wself.itemClickedBlock(index * 2,[itemDic objectForKey:@"title"],[itemDic objectForKey:@"id"],[itemDic objectForKey:@"url"]);
        }
    };
    
    
    
        //********** bottom News View
    _bottomNewsScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(_leftImageSize.width + News_text_left_space, CGRectGetMaxY(_topNewsScrollView.frame) - 3*kDeviceRate, (kDeviceWidth-_leftImageSize.width) - News_text_left_space, 26*kDeviceRate) delegate:self placeholderImage:nil];
    [self initBasePropertyWith:_bottomNewsScrollView];
    
    [self addSubview:_bottomNewsScrollView];
    
    UIView *bottomView = [[UIView alloc]init];
    
    [_bottomNewsScrollView addSubview:bottomView];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bottomNewsScrollView.mas_top).offset(3*kDeviceRate);
        make.bottom.equalTo(_bottomNewsScrollView.mas_bottom).offset(3*kDeviceRate);
        make.left.equalTo(_bottomNewsScrollView.mas_left);
        make.right.equalTo(_bottomNewsScrollView.mas_right);
    }];
    
    bottomView.backgroundColor = [UIColor clearColor];
    
//    _bottomNewsScrollView.clickItemOperationBlock = ^(NSInteger index){
//        if (wself.itemClickedBlock) {
//            NSDictionary *itemDic = [wself.newsArr objectAtIndex:(index * 2 + 1)];
//            wself.itemClickedBlock(index * 2 + 1,[itemDic objectForKey:@"title"],[itemDic objectForKey:@"id"]);
//        }
//    };
    
    UITapGestureRecognizer *topTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedTopNewsView:)];
    topTap.numberOfTapsRequired = 1;
    [topView addGestureRecognizer:topTap];
    
    UITapGestureRecognizer *bottomTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedBottomNewsView:)];
    bottomTap.numberOfTapsRequired = 1;
    [bottomView addGestureRecognizer:bottomTap];
    
    
    
//        //********** mask View 添加手势
//    UIView *maskView = [UIView new];
//    maskView.frame = CGRectMake(0, 0, kWidth, kHeight);
//    maskView.backgroundColor = [UIColor clearColor];
//    [self addSubview:maskView];
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedMaskView:)];
//    tap.numberOfTapsRequired = 1;
//    [maskView addGestureRecognizer:tap];
//    [self reloadNewsWith:[NSArray new]];
}

-(void)reloadNewsWith:(NSArray *)newsArr{
    self.newsArr = [NSMutableArray arrayWithArray:newsArr];
}
-(void)clickedMaskView:(UITapGestureRecognizer *)tap{
    if (_clickActionBlock) {
        _clickActionBlock(@"");
    }

}

-(void)clickedTopNewsView:(UITapGestureRecognizer *)tap{
    
    WS(wself);
    
    if (wself.itemClickedBlock) {
        NSDictionary *itemDic = [wself.newsArr objectAtIndex:(topNewsInteger * 2)];
        wself.itemClickedBlock(topNewsInteger * 2,[itemDic objectForKey:@"title"],[itemDic objectForKey:@"id"],[itemDic objectForKey:@"url"]);
    }

}

-(void)clickedBottomNewsView:(UITapGestureRecognizer *)tap{
    
    WS(wself);
    
    if (wself.itemClickedBlock) {
        NSDictionary *itemDic = [wself.newsArr objectAtIndex:(bottomNewsInteger * 2 + 1)];
        wself.itemClickedBlock(bottomNewsInteger * 2 + 1,[itemDic objectForKey:@"title"],[itemDic objectForKey:@"id"],[itemDic objectForKey:@"url"]);
    }
    
}

-(void)setLeftImageSize:(CGSize)leftImageSize{
    _leftImageSize = leftImageSize;

}

-(void)initBasePropertyWith:(SDCycleScrollView *)sdScrollView{

    sdScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    sdScrollView.onlyDisplayText = YES;
    sdScrollView.titleLabelTextFont = HT_FONT_FOURTH;
    sdScrollView.autoScrollTimeInterval = News_Scroll_Time;
    sdScrollView.titleLabelTextColor = HT_COLOR_FONT_SECOND;
    sdScrollView.titleLabelBackgroundColor = [UIColor clearColor];
}


-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{

    if ([cycleScrollView isEqual:_bottomNewsScrollView]) {
        
        bottomNewsInteger = index;
        
    }
    else if ([cycleScrollView isEqual:_topNewsScrollView])
    {
        topNewsInteger = index;
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
