//
//  HomeHeaderModalView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/8/31.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HomeHeaderModalView.h"

@implementation HomeHeaderModalView


-(instancetype)init{
    if (self == [super init]) {
            //初始化基本数据
        [self initData];
            //初始化基本UI
        [self initUI];
    }
    return self;
}
#pragma mark ------:初始化数据
-(void)initData{
    _cycleTypesArr = [NSMutableArray array];
    _cycleImagesArr = [NSMutableArray array];
    _cycleItemIDArr = [NSMutableArray array];
    _cycleTitlesArr = [NSMutableArray array];
 
}
#pragma mark ------:初始化UI
-(void)initUI{
    
//**********轮播图视图
    [self initCycleScrollView];
//**********模块列表视图
    [self initModuleListView];
//********** 滚动新闻视图
    [self initNewsView];
    
}

-(void)initCycleScrollView{
    WS(wself);
    CGRect cycleVIewRect = CGRectMake(0, 0, kDeviceWidth, 135*kDeviceRate);
    _homeCirecleImage = [SDCycleScrollView cycleScrollViewWithFrame:cycleVIewRect delegate:self placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading_h220"]];
    _homeCirecleImage.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _homeCirecleImage.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    _homeCirecleImage.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    _homeCirecleImage.pageDotImage = [UIImage imageNamed:@"ic_home_poster_hui"];
    _homeCirecleImage.currentPageDotImage = [UIImage imageNamed:@"ic_home_poster_bai"]; // 自定义分页控件小圆标颜色
    _homeCirecleImage.autoScrollTimeInterval = PAGESCROLLTIME;
    [self addSubview:_homeCirecleImage];
    
    _homeCirecleImage.clickItemOperationBlock = ^(NSInteger index){
        if (wself.cycleScrollViewBlock) {
            
            NSString *type = (wself.cycleTypesArr.count > index)?[wself.cycleTypesArr objectAtIndex:index] : @"0";
            NSString *itemID = wself.cycleItemIDArr.count > index ? [wself.cycleItemIDArr objectAtIndex:index] : @"";
            wself.cycleScrollViewBlock(index,type,itemID);
        }
        NSLog(@">>>>>> %ld",(long)index);
        
    };

}

-(void)initModuleListView{
    WS(wself);
    _homeModuleListView = [[ModuleListView alloc] init];
    _homeModuleListView.backgroundColor = App_white_color;
    _homeModuleListView.frame = CGRectMake(0, _homeCirecleImage.frame.origin.y + _homeCirecleImage.frame.size.height, kDeviceWidth, 0);
    [self addSubview:_homeModuleListView];
    
    _homeModuleListView.didSelectedModule = ^(NSInteger index,NSString *itemType,NSString *itemID){
        if (wself.moduleListViewBlock) {
            wself.moduleListViewBlock(index,itemType,itemID);
        }
    };
    _homeModuleListView.sizeChangedBlock = ^(CGRect newFrame){
        
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect newsRect = CGRectMake(0, newFrame.origin.y + newFrame.size.height + 1, kDeviceWidth, 40*kRateSize);
            wself.homeNewsView.frame = newsRect;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:Header_animation_duration animations:^{
                if (wself.sizeChangedBlock) {
                    wself.sizeChangedBlock([wself calculateNewFrame]);
                }  
            }];
        }];
    };
    [_homeModuleListView reloadWith:0];
}

-(void)initNewsView{
    WS(wself);

    CGRect newsRect = CGRectMake(0, _homeModuleListView.frame.origin.y + _homeModuleListView.frame.size.height, kDeviceWidth, 52.5*kDeviceRate);
    _homeNewsView = [[HomeNewsView alloc] initWithFrame:newsRect];
    _homeNewsView.backgroundColor = App_white_color;
    [self addSubview:_homeNewsView];
    _homeNewsView.clickActionBlock = ^(NSString *newsStr){
        if (wself.newsViewBlock) {
            wself.newsViewBlock(0,@"",@"",@"");
        }
        
    };
    _homeNewsView.itemClickedBlock = ^(NSInteger index,NSString *news,NSString *newsID,NSString *newsUrl){
        if (wself.newsViewBlock) {
            wself.newsViewBlock(0,news,newsID,newsUrl);
        }
    };
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reloadNewsViewWith:[NSArray new]];
    });

    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self reloadNewsViewWith:@[@"1111111111111111111111111111111111",@"222222222222222222222222",@"3333333333333333",@"4444444444444444"]];
//    });
    
}


#pragma mark ------>>>: 重载视图
-(void)reloadHeaderResult:(NSDictionary *)hesderResult{

    

}
#pragma mark ------  重载轮播图
-(void)reloadHomeCycleScrollViewWith:(NSArray *)cycleScrollArr{
    
    _headResultArr = [cycleScrollArr mutableCopy];
    _cycleTypesArr = [cycleScrollArr mutableCopy];
    [_cycleTypesArr removeAllObjects];
    [_cycleImagesArr removeAllObjects];
    [_cycleItemIDArr removeAllObjects];
    [_cycleTitlesArr removeAllObjects];
    
    if (cycleScrollArr.count>0) {
        
        for (NSDictionary *itemDic in cycleScrollArr) {
            //图片链接
            NSString *imageH220 = [itemDic objectForKey:@"imageLinkH220"];
            imageH220 = imageH220 ? imageH220 :[itemDic objectForKey:@"imageLink"];
            if (imageH220.length ==0) {
                imageH220 = @"";
            }
                //类型
            NSString *type = [itemDic objectForKey:@"type"];
                //名称
            NSString *name = [itemDic objectForKey:@"name"];
            name = name ? name : @"";
                //节目ID
            NSString *itemID = [itemDic objectForKey:@"id"];
            itemID = itemID ? itemID : @"-1";
                //跳转链接
            NSString *urlstring = [itemDic objectForKey:@"urlLink"];
            urlstring = urlstring ? urlstring : @"";
            
            [_cycleImagesArr addObject:NSStringPM_IMGFormat(imageH220)];
            [_cycleTypesArr addObject:type];
            [_cycleItemIDArr addObject:itemID];
            [_cycleTitlesArr addObject:name];
        }
        _homeCirecleImage.imageURLStringsGroup = _cycleImagesArr;
    }
    
}

#pragma mark ------  重载功能模块视图
-(void)reloadModuleListViewWith:(NSArray *)modulListArr{
    [_homeModuleListView reloadViewWith:modulListArr];
}

#pragma mark ------  重载新闻视图
-(void)reloadNewsViewWith:(NSArray *)newsArr{
    [_homeNewsView reloadNewsWith:newsArr];
    
    [UIView animateWithDuration:Header_animation_duration animations:^{
        if (newsArr.count == 0) {
            _homeNewsView.frame = CGRectMake(CGRectGetMinX(_homeNewsView.frame), CGRectGetMinY(_homeNewsView.frame), CGRectGetWidth(_homeNewsView.frame), 0);
//            _homeNewsView.hidden = YES;
            _homeNewsView.alpha = 0;
        }else{
            _homeNewsView.frame = CGRectMake(CGRectGetMinX(_homeNewsView.frame), CGRectGetMinY(_homeNewsView.frame), CGRectGetWidth(_homeNewsView.frame), 52.5*kDeviceRate);
//            _homeNewsView.hidden = NO;
            _homeNewsView.alpha = 1;
        }
    }];
    
    WS(wself);
    [UIView animateWithDuration:Header_animation_duration animations:^{
          if (wself.sizeChangedBlock) {
        wself.sizeChangedBlock([wself calculateNewFrame]);
    }  
    }];

}


-(CGRect)calculateNewFrame{
    
    CGFloat newHeight = CGRectGetHeight(_homeCirecleImage.frame) + CGRectGetHeight(_homeModuleListView.frame) + CGRectGetHeight(_homeNewsView.frame);
    CGRect newReact = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), newHeight);
    
    return newReact;
}


#pragma  mark - 轮播图代理方法
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{

}
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
