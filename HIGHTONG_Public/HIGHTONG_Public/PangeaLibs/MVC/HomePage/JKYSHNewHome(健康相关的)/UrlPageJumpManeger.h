//
//  UrlPageJumpManeger.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/2/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrlPageJumpManeger : NSObject

//友谊的小船链接跳转
+ (NSString *)extranetPromptPageJump;

//首页更多wifi链接跳转
+ (NSString *)homePageWifiBtnPageJump;

//首页健康资讯列表链接跳转
+ (NSString *)homePageHealthNewsPageJumpWithItemId:(NSString *)itemId;

//首页轻松一刻节目链接跳转
+ (NSString *)homePageRelaxProgramPageJumpWithPubChannelId:(NSString *)pubChannelId andProgramId:(NSString *)programId;

//首页轻松一刻频道链接跳转
+ (NSString *)homePageRelaxChannelPageJump;

//首页广告网页链接跳转(包括底部广告)
+ (NSString *)homePageADPageJumpWithADUrlLink:(NSString *)urlLink;

//首页模块链接跳转
+ (NSString *)homePageModuleConfigJumpWithModuleInfo:(NSString *)moduleInfo andRecFlg:(NSString *)recFlg andIsYSHBK:(BOOL)isYSHBK;

//首页挂号链接跳转（山西挂号）
+ (NSString *)homePageSXHospitalJumpWithModuleInfo:(NSString *)moduleInfo;

//首页公司业务链接跳转（爱上TV）
+ (NSString *)homePageComPanyOperationJumpWithModuleInfo:(NSString *)url;

//我的页面关于我们链接跳转
+ (NSString *)minePageAboutUSJump;

//登陆页面协议跳转
+ (NSString *)loginPageServiceVCJump;

////aboutus界面参数拼接
+ (NSString *)aboutUsPageJump;

//feedback界面参数拼接
+ (NSString *)feedBackPageJump;

//百度推送参数拼接
+ (NSString *)bPushJumpWithExtraInfo:(NSString *)extraInfo andExtraId:(NSString *)extraId;

//UGC直播协议参数拼接
+ (NSString *)liveProtocolPageJump;

//UGC上传参数拼接
+ (NSString *)upProtocolPageJump;

//轻松一刻分享链接拼接
+ (NSString *)relaxShareLinkWithChannelID:(NSString *)channelID andChannelName:(NSString *)channelName;

//健康电视分享链接拼接
+ (NSString *)healthyShareLinkWithChannelID:(NSString *)channelID;

@end
