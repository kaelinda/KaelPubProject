//
//  HomePageRequestManger.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/9/8.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HomePageRequestManger.h"
#import "HTRequest.h"

#define HaveTuijian  2
@interface HomePageRequestManger ()<HTRequestDelegate>
{
    NSInteger  _reqCount;
    NSString  *_homeNewsUrl;
}

@property (strong ,nonatomic) UITableView   *tableView; //首页选项卡

@property (strong,nonatomic) NSMutableArray *slideArray;   //轮播图数据源
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页选项卡接收数据数据
@property (strong,nonatomic) NSMutableArray *newsListArray;   //健康咨询数据数据源
@property (strong,nonatomic) NSMutableArray *hotHealthChannelArray;   //健康精选数据源
@property (strong,nonatomic) NSMutableArray *hotEpgArray;   //热门直播数据源
@property (strong,nonatomic) NSMutableArray *tempHotVodArray;   //热门电影过度数据源
@property (strong,nonatomic) NSMutableArray *hotVodArray;   //热门电影数据源
@property (strong,nonatomic) NSMutableArray *hotHealthVodArray;   //热门健康频道及节目
@property (strong,nonatomic) NSMutableDictionary *pubChannelDic;   //热门健康频道及节目
@property (strong,nonatomic) NSMutableDictionary *bottomImageDic;   //首页底图数据

@property (strong,nonatomic) NSMutableArray *homeDateArray;   //首页tableview数据源

@end


@implementation HomePageRequestManger

-(instancetype)init{
    NSAssert(NO, @"请使用initWithDelegate方法");
    return self;
}

-(id)initWithDelegate:(id<HomePageRequestDelegate>)delegate{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        
        self.slideArray = [NSMutableArray array];
        self.dataArray = [NSMutableArray array];
        self.newsListArray = [NSMutableArray array];
    }
    return self;
}

-(void)reloadHeaderData
{
    
    _reqCount = 0;
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst CNCommonModuleConfig];
    
    //获取广告
    if (is_Separate_API) {
        [requst CNCommonSlideshowList];
        
    }else{
        [requst HomeADSLiderPagess];
    }
    
    [requst YJHealthCareNewsListWithPlatType:@"1"];//健康资讯
    
    
}

-(void)reloadGreatHealthData
{
     HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst YJHealthyGetSelectedChannel];//获取健康精选
    
}

-(void)reloadHotChannelData
{
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst HotChannelListWithTimeout:5];//热门直播
    
    [requst CNCommonBottomImage];

}

-(void)reloadPubChannelData
{
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst PubCommonHomePageChannelListWithCategory:@"0"];//轻松一刻
    
}

-(void)reloadVODData
{
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst VODHomeConfigListWithTimeOut:5];;//请求首页配置信息（VOD）

}

-(void)reloadhealthVideoData
{
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst YJHealthyVodGetHotChannel];//获取健康养生视频列表
}

-(void)reloadFooterData
{
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst CNCommonBottomImage];//获取首页底图
    
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    //广告数据
    if ([type isEqualToString:HomeADSLiderPage] ||[type isEqualToString:CN_COMMON_SLIDESHOWLIST]) {
        
        _reqCount++;
        
        self.slideArray = [NSMutableArray array];
        
        self.slideArray = [result objectForKey:@"slideshowList"];
        
        if (self.slideArray.count>1) {
            self.slideArray = [KaelTool increaseArrayWithArray:self.slideArray andKey:@"seq"];
            NSDictionary *firstDic = [self.slideArray firstObject];
            [self.slideArray removeObjectAtIndex:0];
            [self.slideArray addObject:firstDic];
        }
        
        NSLog(@"广告的数据是%@",self.slideArray);
        
        [self pushDataToHomePage];

    }
    
    //首页配置
    if ([type isEqualToString:CN_COMMON_MODULECONFIG]) {
        
        _reqCount++;
        NSLog(@"CN_COMMON_MODULECONFIG返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] ) {
            
            self.dataArray = [NSMutableArray array];
            
            NSMutableArray *tempArr = [NSMutableArray array];
            tempArr = [result objectForKey:@"moduleList"];
            
            NSLog(@"tempArr返回数据:%@",tempArr);
            
            self.dataArray = [KaelTool increaseArrayWithArray:tempArr andKey:@"seq"];
            
            NSLog(@"self.dataArray返回数据:%@",self.dataArray);

        }
        
        [self pushDataToHomePage];
    }
    
    //请求健康咨询
    if ([type isEqualToString:YJ_HEALTHCARE_NEWSLIST]) {
        _reqCount++;
        NSLog(@"YJ_HEALTHCARE_NEWSLIST返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            self.newsListArray = [NSMutableArray array];
            
            self.newsListArray = [result objectForKey:@"list"];
            
            _homeNewsUrl = [[NSString alloc]init];
            
            _homeNewsUrl = [result objectForKey:@"url"];
        }
        
        [self pushDataToHomePage];
    }
    
    //健康精选请求数据
    if ([type isEqualToString:YJ_HEALTHYVOD_GETSELECTEDCHANNEL]) {
        NSLog(@"YJ_HEALTHYVOD_GETSELECTEDCHANNEL返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            self.hotHealthChannelArray = [NSMutableArray array];
            
            self.hotHealthChannelArray = [result objectForKey:@"list"];

        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(greatHealthChannelDataGetSuccessWith:)]) {
            [_delegate greatHealthChannelDataGetSuccessWith:self.hotHealthChannelArray];
        }
    }
    
    //直播推荐请求数据
    if ([type isEqualToString:EPG_HOTCHANNELLIST]) {
        NSLog(@"EPG_HOTCHANNELLIST返回数据: %ld result:%@  type:%@",status,result,type );
        
        if (status == 0) {
            
            self.hotEpgArray = [NSMutableArray array];
            
            self.hotEpgArray = [result objectForKey:@"list"];
            
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(hotChannelDataGetSuccessWith:)]) {
            [_delegate hotChannelDataGetSuccessWith:self.hotEpgArray];
        }
        
    }
    
    if ([type isEqualToString:VOD_HomeConfigList]) {
        NSLog(@"VOD_HomeConfigList返回数据: %ld result:%@  type:%@",(long)status,result,type );
        
        if (status == 0) {
            
            if (![[result objectForKey:@"count"]isEqualToString:@"0"]) {
                
                self.tempHotVodArray = [NSMutableArray array];
                
                self.tempHotVodArray = [result objectForKey:@"configList"];
                
                NSInteger indexNum = 0 + HaveTuijian;
                for (NSMutableDictionary *dic in self.tempHotVodArray) {

                    NSString *seq = [dic objectForKey:@"seq"];
                    NSString *num = [dic objectForKey:@"num"];
                    NSString *code = [dic objectForKey:@"code"];

                    NSString *index = [NSString stringWithFormat:@"%ld",[seq integerValue]+HaveTuijian-1];
                    
                    HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                    [_requst VODTopicProgramListWithTopicCode:code andStartSeq:1 andCount:[num integerValue] andTimeout:10 andIndex:[seq integerValue]+HaveTuijian-1];//12.10首页配置信息的修改
                    
                    [dic setObject:index forKey:@"index"];
                    
                    indexNum++;
                }
                
                NSLog(@"%@",self.tempHotVodArray);
                
                _reqCount = indexNum;

                
            }else
            {
                
                if (_delegate && [_delegate respondsToSelector:@selector(homeVODDataGetSuccessWith:)]) {
                    [_delegate homeVODDataGetSuccessWith:self.hotVodArray];
                }
                
            }
        }
        else
        {
            if (_delegate && [_delegate respondsToSelector:@selector(homeVODDataGetSuccessWith:)]) {
                [_delegate homeVODDataGetSuccessWith:self.hotVodArray];
            }
        }
    }
    
    
    //推荐页面的VOD_TOPIC_PROGRAM_LIST
    if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
        NSLog(@"推荐的专题节目列表: %ld result:%@  type:%@",status,result,type );
        NSArray *array = [result objectForKey:@"vodList"];
        
        _reqCount--;
        
        NSString *index = [result objectForKey:@"index"];
        
        [self.tempHotVodArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSMutableDictionary *dic, NSUInteger idx, BOOL *stop) {
            
            if ([[result objectForKey:@"count"]integerValue] <= 0) {
                if ([[dic objectForKey:@"index"]isEqualToString:index]) {
                    [self.tempHotVodArray removeObject:dic];
                }
                
            }
            else
            {
                if ([[dic objectForKey:@"index"] isEqualToString:index])
                {
                    if (!IsArrEmpty(array)) {
                        [dic setObject:array forKey:@"vodList"];
                    }
                    
                }
            }
        }];
        
        
        if (_reqCount == HaveTuijian) {
            
            self.hotVodArray = [NSMutableArray array];
            
            self.hotVodArray = self.tempHotVodArray;
            
            NSLog(@"热门电影整理后数据%@",self.hotVodArray);
            
            if (_delegate && [_delegate respondsToSelector:@selector(homeVODDataGetSuccessWith:)]) {
                [_delegate homeVODDataGetSuccessWith:self.hotVodArray];
            }
        }
        
    }
    
    if ([type isEqualToString:PUB_COMMON_HOMEPAGECHANNELLIST]) {//首页推荐频道列表
        NSLog(@"PUB_COMMON_HOMEPAGECHANNELLIST首页推荐频道列表---返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"])
        {
            self.pubChannelDic = [NSMutableDictionary dictionary];
            
            self.pubChannelDic = (NSMutableDictionary*)result;
            
            [self.pubChannelDic removeObjectForKey:@"ret"];
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(homePubChannelGetSuccessWith:)]) {
            [_delegate homePubChannelGetSuccessWith:self.pubChannelDic];
        }
    }
    
    //健康养生热门频道及节目列表
    if ([type isEqualToString:YJ_HEALTHYVOD_GETHOTCHANNEL]) {
        NSLog(@"YJ_HEALTHYVOD_GETHOTCHANNEL返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            self.hotHealthVodArray = [NSMutableArray array];
            
            self.hotHealthVodArray = [result objectForKey:@"list"];

        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(homeHealthVideoGetSuccessWith:)]) {
            [_delegate homeHealthVideoGetSuccessWith:self.hotHealthVodArray];
        }
    }
    
    //首页底部图
    if ([type isEqualToString:CN_COMMON_BOTTOMIMAGE]) {
        
        NSLog(@"CN_COMMON_BOTTOMIMAGE返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] ) {
            
            self.bottomImageDic = [NSMutableDictionary dictionary];
            
            self.bottomImageDic = [[NSMutableDictionary dictionaryWithDictionary:result] objectForKey:@"bottomImage"];
            
            [self.bottomImageDic removeObjectForKey:@"ret"];
            
            if (_delegate && [_delegate respondsToSelector:@selector(homeBottomImageGetSuccessWith:)]) {
                [_delegate homeBottomImageGetSuccessWith:self.bottomImageDic];
            }
            
        }
    }
    
}

- (void)pushDataToHomePage
{
    if (_reqCount == 3) {
        if (_delegate && [_delegate respondsToSelector:@selector(homeHeaderDataGetSuccessWith:andModules:andHomeNews:andHomeNewsUrl:)]) {
            
            NSString *lastItem = [[self.slideArray lastObject] copy];
            [self.slideArray removeLastObject];
            [self.slideArray insertObject:lastItem atIndex:0];
            
            [_delegate homeHeaderDataGetSuccessWith:self.slideArray andModules:self.dataArray andHomeNews:self.newsListArray andHomeNewsUrl:_homeNewsUrl];
            _reqCount = 0;
        }
    }
}


@end
