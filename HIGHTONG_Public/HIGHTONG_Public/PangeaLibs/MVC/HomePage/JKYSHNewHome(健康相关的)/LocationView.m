
    //
//  LocationView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/9/2.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "LocationView.h"


#define kIcon_space 10*kDeviceRate

@implementation LocationView
#pragma mark - 这是自动布局

-(instancetype)initLocationView{
    self = [self init];
    if (self) {
        //采用自动布局的方式 进行 布局 子视图
        [self autoLayoutSubViews];
    }
    
    return self;
}

-(void)autoLayoutSubViews{


        
}


#pragma mark - 下面的方法是使用frame 布局
-(instancetype)initWithFrame:(CGRect)frame{

    if (self == [super initWithFrame:frame]) {
        [self setKSize:frame.size];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tap];
    }
    return self;
}

-(void)setKSize:(CGSize)kSize{
    _kSize = kSize;
    self.frame = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), kSize.width, kSize.height);
    [self setupSubViews];
}

-(void)setKCenter:(CGPoint)kCenter{
    _kCenter = kCenter;
    self.frame = CGRectMake(kCenter.x - _kSize.width/2, kCenter.y - _kSize.height/2, _kSize.width, _kSize.height);
}


-(void)setupSubViews{

    CGSize leftSize = CGSizeMake(8, 11);
    CGSize rightSize = CGSizeMake(8, 5);
    if (_leftImageView == nil) {
        
        _leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_home_location_position"]];
        _leftImageView.frame = CGRectMake(kIcon_space, (_kSize.height - leftSize.height)/2, leftSize.width, leftSize.height);
        [self addSubview:_leftImageView];
    }else{
        _leftImageView.frame = CGRectMake(kIcon_space, (_kSize.height - leftSize.height)/2, leftSize.width, leftSize.height);
    }

    if (_rightImageView == nil) {
        _rightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_home_location_triangle"]];
        _rightImageView.frame = CGRectMake(_kSize.width - kIcon_space - rightSize.width, (_kSize.height - rightSize.height)/2, rightSize.width, rightSize.height);
        _rightImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:_rightImageView];
    }else{
        _rightImageView.frame = CGRectMake(_kSize.width - kIcon_space - rightSize.width, (_kSize.height - rightSize.height)/2, rightSize.width, rightSize.height);
    }
    
    if (_midLabel == nil) {
        _midLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:HT_COLOR_FONT_INVERSE andFont:HT_FONT_THIRD];
        _midLabel.frame = CGRectMake(leftSize.width + kIcon_space + 5*kDeviceRate, 0, _kSize.width - leftSize.width - rightSize.width - 2*5*kDeviceRate - 2*kIcon_space, 25*kDeviceRate);
        _midLabel.text = @"欢迎来到健康电视";
        [self addSubview:_midLabel];
    }else{
        _midLabel.frame = CGRectMake(leftSize.width + kIcon_space + 5*kDeviceRate, 0, _kSize.width - leftSize.width - rightSize.width - 2*5*kDeviceRate - 2*kIcon_space, 25*kDeviceRate);
    }
    
}


-(void)tapAction:(UITapGestureRecognizer *)tap{
    if (_actionBlock) {
        _actionBlock();
    }

}
-(void)setLocationString:(NSString *)location{
    if (_midLabel) {
        if (isEmptyStringOrNilOrNull(location)) {
            _midLabel.text = @"";
        }else{
            NSMutableString *locationStr = [NSMutableString stringWithFormat:@"%@", location];
            _midLabel.text = location;
            
            if (locationStr.length>9) {
                [self resetFrameWith:[locationStr substringToIndex:9]];
            }else{
                [self resetFrameWith:location];
            }
        }
    }
}

-(void)resetFrameWith:(NSString *)location{
    
    CGSize textSize;
    
//    //第一种方法
//    textSize = [location sizeWithAttributes:attributes];
//    //第二种计算方法 得到的都是整数
//    textSize = [location sizeWithFont:HT_FONT_THIRD];
//    //第三种
//    CGRect labelFrame = [location boundingRectWithSize:CGSizeMake(320, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    textSize = labelFrame.size;
    //第四种自制
    textSize = CGSizeMake(15*kDeviceRate*location.length, 20);
//    textSize = CGSizeMake(15*kDeviceRate*9, 20);//以前是根据字数来自适应，最大为10个字；2017年6月6日改为固定大小，以1个字来设计
    
    CGPoint center = self.center;
    CGSize leftSize = CGSizeMake(8, 11);
    CGSize rightSize = CGSizeMake(8, 5);
    
    CGSize size = CGSizeMake(2*kIcon_space + 2*5*kDeviceRate+ leftSize.width + rightSize.width + textSize.width, self.frame.size.height);
    
    CGRect leftFrame = _leftImageView.frame;
    CGRect rightFrame = CGRectMake(center.x - size.width/2, (size.height - rightSize.height)/2, rightSize.width, rightSize.height);
    
    
    [UIView animateWithDuration:5 animations:^{
        
//        self.frame = CGRectMake(0, 0, size.width, size.height);
//        self.frame = CGRectMake(0, 0, midFrame.size.width, midFrame.size.height);
//
//        self.center = center;
//        _leftImageView.frame = leftFrame;
//        _midLabel.frame = midFrame;
//        _rightImageView.frame = rightFrame;
//        _midLabel.text = location;
        WS(wself);
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(size);
        }];
        
        [_leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftFrame.origin.x);
            make.top.mas_equalTo(leftFrame.origin.y);
            make.size.mas_equalTo(leftFrame.size);
        }];
        [_rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-kIcon_space);
            make.top.mas_equalTo(rightFrame.origin.y);
            make.size.mas_equalTo(rightSize);
        }];
        [_midLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(wself);
            make.size.mas_equalTo(textSize);
        }];

        
    } completion:^(BOOL finished) {
        
    }];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
