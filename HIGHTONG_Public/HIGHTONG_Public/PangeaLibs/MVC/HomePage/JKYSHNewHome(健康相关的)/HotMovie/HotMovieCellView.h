//
//  HotMovieCellView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^HotMovieCellViewBlock)(UIButton *btn, NSString *movieID, NSString *movieName);

@interface HotMovieCellView : UIView

@property (nonatomic, strong)UIImageView *posterImg;

@property (nonatomic, strong)UILabel *movieNameLab;

@property (nonatomic, copy)HotMovieCellViewBlock clickBlock;

@property (nonatomic, assign)NSString *imageUrl;//海报链接

@property (nonatomic, assign)NSString *movieName;//影片名字

@property (nonatomic, assign)NSString *movieID;//影片ID


@property (nonatomic, assign)NSString *highLightImgUrl;//高亮图

@property (nonatomic, assign)NSString *placeholderImgUrl;//占位图



- (void)refreshView;

- (void)refreshViewWithDictionary:(NSDictionary *)dic;

@end
