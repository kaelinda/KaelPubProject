//
//  HotMovieTableCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HotMovieTableCell.h"


#define cellWidth 117.5*kDeviceRate//100*kRateSize
#define cellHeight 192*kDeviceRate//163.5*kRateSize
#define gapWidth 6*kDeviceRate//5*kRateSize


@implementation HotMovieTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _dataArr = [[NSMutableArray alloc] initWithCapacity:0];
        
        [self setUpView];
    }
    
    return self;
}

- (void)setUpView
{
    self.leftView = [[HotMovieCellView alloc] init];
    self.middleView = [[HotMovieCellView alloc] init];
    self.rightView = [[HotMovieCellView alloc] init];
    
    [self.contentView addSubview:self.leftView];
    [self.contentView addSubview:self.middleView];
    [self.contentView addSubview:self.rightView];
    
    WS(wself);
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.contentView.mas_left).offset(gapWidth);
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.bottom.mas_equalTo(wself.contentView.mas_bottom);
        make.width.mas_equalTo(cellWidth);
    }];
    
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.leftView.mas_right).offset(gapWidth);
        make.top.mas_equalTo(wself.leftView.mas_top);
        make.bottom.mas_equalTo(wself.leftView.mas_bottom);
        make.width.mas_equalTo(wself.leftView.mas_width);
    }];
    
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.middleView.mas_right).offset(gapWidth);
        make.right.mas_equalTo(wself.contentView.mas_right).offset(-gapWidth);
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.bottom.mas_equalTo(wself.contentView.mas_bottom);
    }];
    
    self.leftView.clickBlock = ^(UIButton *btn, NSString *movieID, NSString *name){
        if (wself.cellBlock) {
            wself.cellBlock(btn, movieID, name);
        }
    };
    
    self.middleView.clickBlock = ^(UIButton *btn, NSString *movieID, NSString *name){
        if (wself.cellBlock) {
            wself.cellBlock(btn, movieID, name);
        }
    };
    
    self.rightView.clickBlock = ^(UIButton *btn, NSString *movieID, NSString *name){
        if (wself.cellBlock) {
            wself.cellBlock(btn, movieID, name);
        }
    };
}

- (void)refreshView
{
    
    if (self.dataArr.count >= 1) {
        
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.dataArr[0];
        [self.leftView refreshViewWithDictionary:dic];
        
        if (self.dataArr.count == 1) {
            self.middleView.hidden = YES;
            self.rightView.hidden = YES;
        }else{
            
            self.middleView.hidden = NO;
            self.rightView.hidden = NO;
        }
    }
    if (self.dataArr.count >= 2) {
        
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.dataArr[1];
        [self.middleView refreshViewWithDictionary:dic];
        
        if (self.dataArr.count == 2) {
//            self.leftView.hidden = YES;
            self.rightView.hidden = YES;
        }else{
        
//            self.leftView.hidden = NO;
            self.rightView.hidden = NO;
        }
    }
    if (self.dataArr.count >= 3) {
        
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.dataArr[2];
        [self.rightView refreshViewWithDictionary:dic];
        
//        if (self.dataArr.count == 2) {
//            self.leftView.hidden = YES;
//            self.middleView.hidden = YES;
//        }else{
//            
//            self.leftView.hidden = NO;
//            self.middleView.hidden = NO;
//        }
    }
    
//    self.leftView.highLightImgUrl = self.highLightUrl;
//    self.middleView.highLightImgUrl = self.highLightUrl;
//    self.rightView.highLightImgUrl = self.highLightUrl;
//    
//    self.leftView.placeholderImgUrl = self.placeholderUrl;
//    self.middleView.placeholderImgUrl = self.placeholderUrl;
//    self.rightView.placeholderImgUrl = self.placeholderUrl;
}

@end
