//
//  HotMovieTableCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotMovieCellView.h"

typedef void(^HotMovieTableCellBlock)(UIButton *btn, NSString *movieID, NSString *movieName);

@interface HotMovieTableCell : UITableViewCell

@property (nonatomic, copy)HotMovieTableCellBlock cellBlock;

@property (nonatomic, strong)HotMovieCellView *leftView;

@property (nonatomic, strong)HotMovieCellView *middleView;

@property (nonatomic, strong)HotMovieCellView *rightView;

@property (nonatomic, strong)NSMutableArray *dataArr;

@property (nonatomic, assign)NSString *highLightUrl;//高亮图

@property (nonatomic, assign)NSString *placeholderUrl;//占位图

- (void)refreshView;

@end
