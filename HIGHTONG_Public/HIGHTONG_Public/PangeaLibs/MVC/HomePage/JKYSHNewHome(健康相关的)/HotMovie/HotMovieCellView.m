//
//  HotMovieCellView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HotMovieCellView.h"


#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define posterWidth 117.5*kDeviceRate//100*kRateSize
#define posterHeight 157*kDeviceRate//133.5*kRateSize
#define gapWidth 7*kDeviceRate//5*kRateSize
#define labHeight 18*kDeviceRate//15*kRateSize


@implementation HotMovieCellView

{
    UIButton *_clickBtn;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setUpView];
    }
    
    return self;
}

- (void)setUpView
{
    WS(wself);
    
    self.posterImg = [[UIImageView alloc] init];
    self.movieNameLab = [[UILabel alloc] init];
    
    [self addSubview:self.posterImg];
    [self addSubview:self.movieNameLab];
    
    self.movieNameLab.font = HT_FONT_THIRD;
    self.movieNameLab.textColor = HT_COLOR_FONT_FIRST;
    self.movieNameLab.textAlignment = NSTextAlignmentLeft;
    
    [self.posterImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(posterWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(wself.mas_left);
        make.top.mas_equalTo(wself.mas_top);
    }];
    
    [self.movieNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.posterImg.mas_left);
        make.top.mas_equalTo(wself.posterImg.mas_bottom).offset(gapWidth);
        make.right.mas_equalTo(wself.posterImg.mas_right);
        make.height.mas_equalTo(labHeight);
    }];
    
    _clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:_clickBtn];
    
    [_clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.posterImg);
    }];
    
    [_clickBtn setImage:[UIImage imageNamed:@"bg_common_vod_pressed"] forState:UIControlStateHighlighted];

    [_clickBtn addTarget:self action:@selector(clickButtonSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)refreshView
{
    [self.posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(self.imageUrl.length?self.imageUrl:@"")] placeholderImage:[UIImage imageNamed:self.placeholderImgUrl.length?self.placeholderImgUrl:@""]];
    
    self.movieNameLab.text = self.movieName.length?self.movieName:@"";

    [_clickBtn setImage:[UIImage imageNamed:self.highLightImgUrl] forState:UIControlStateHighlighted];

    [_clickBtn addTarget:self action:@selector(clickButtonSenderDown:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)refreshViewWithDictionary:(NSDictionary *)dic
{
    if (dic) {
        self.hidden = NO;
        
//        [self.posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([[dic objectForKey:@"imageLink"] length]?[dic objectForKey:@"imageLink"]:@"")] placeholderImage:[UIImage imageNamed:self.placeholderImgUrl.length?self.placeholderImgUrl:@""]];
        
        [self.posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([[dic objectForKey:@"imageLink"] length]?[dic objectForKey:@"imageLink"]:@"")] placeholderImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];

        self.movieNameLab.text = [[dic objectForKey:@"name"] length]?[dic objectForKey:@"name"]:@"";
        
        self.movieName = [[dic objectForKey:@"name"] length]?[dic objectForKey:@"name"]:@"";
        
        self.movieID = [[dic objectForKey:@"contentID"] length]?[dic objectForKey:@"contentID"]:@"";
        
//        [_clickBtn setImage:[UIImage imageNamed:self.highLightImgUrl] forState:UIControlStateHighlighted];
        
//        [_clickBtn addTarget:self action:@selector(clickButtonSenderDown:) forControlEvents:UIControlEventTouchUpInside];

    }else{
        self.hidden = YES;
        
        self.posterImg.image = [UIImage imageNamed:@"poster"];
        self.movieNameLab.text = @"";
    }
}

- (void)clickButtonSenderDown:(UIButton *)btn
{
    if (self.clickBlock) {
        self.clickBlock(btn, self.movieID, self.movieName);
    }
}

@end
