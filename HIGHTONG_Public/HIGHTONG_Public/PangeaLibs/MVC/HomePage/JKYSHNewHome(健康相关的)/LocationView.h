//
//  LocationView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/9/2.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationView : UIView



/**
 *  左侧视图
 */
@property (nonatomic,strong) UIImageView *leftImageView;

/**
 *  右侧视图
 */
@property (nonatomic,strong) UIImageView *rightImageView;


@property (nonatomic,strong) UILabel *midLabel;

/**
 *  尺寸
 */
@property (nonatomic,assign) CGSize kSize;

/**
 *  中心位置
 */
@property (nonatomic,assign) CGPoint kCenter;


/**
 重置显示字符

 @param location 位置名称
 */
-(void)setLocationString:(NSString *)location;


/**
 点击之后的响应事件
 */
@property (nonatomic,copy) void(^actionBlock)();


@end
