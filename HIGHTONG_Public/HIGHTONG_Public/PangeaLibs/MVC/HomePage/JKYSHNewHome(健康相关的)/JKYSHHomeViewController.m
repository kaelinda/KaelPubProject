//
//  JKYSHHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/9/2.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "JKYSHHomeViewController.h"
#import "UIButton_Block.h"
#import "HTRequest.h"
#import "SearchViewController.h"
#import "NewHomeTableViewCell.h"
#import "Tool.h"
#import "LiveHomeViewController.h"
#import "LiveNewHomeViewController.h"
#import "ScrollDemandPageViewController.h"
#import "DemandVideoPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandChooseHospitalControl.h"
#import "MineRecorderViewController.h"
#import "WIFIAreaSelectView.h"
#import "KnowledgeBaseViewController.h"
#import "ExtranetPromptViewController.h"
#import "KaelTool.h"
#import "SystermTimeControl.h"
#import "JKTVHomeCollectionModel.h"
#import "SAIInformationManager.h"

#import "HomeHeaderModalView.h"
#import "JKYSHHotHealthyVodCell.h"
#import "JKYSHHomeChannelCell.h"
#import "HTVideoAVPlayerVC.h"
#import "JKChannelDetailViewController.h"
#import "YJHealthVideoSecondViewcontrol.h"
#import "LocationView.h"
#import "HotHealthyChannelTableCell.h"
#import "HotMovieTableCell.h"
#import "HotLiveTableCell.h"
#import "ChooseHospitalViewController.h"
#import "HealthVodViewController.h"
#import "KDIYRefreshBackFooter.h"
#import "KDIYRefreshHeader.h"
#import "HomePageRequestManger.h"
#import "DemandMenuViewController.h"
#import "UserLoginViewController.h"
#import "PubChannelCell.h"
#import "CarouselPageModel.h"
#import "UrlPageJumpManeger.h"

#import "HTUGCUpLoadInterface.h"

#import "PubVideoHomeViewController.h"

#define WeiHospital 40

#define kSection_Header_Height (40*kDeviceRate)
#define kSection_Footer_Height (35*kDeviceRate)
#define kSection_GrayLine_Height (5*kDeviceRate)
#define kSection_Footer_MaxNum (4)
#define HaveTuijian  2

#define K_USER   (IsLogin ? KMobileNum : [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"])

@interface JKYSHHomeViewController ()<HomePageRequestDelegate,HTRequestDelegate>
{
    NSInteger  _reqCount;
    
    UIView *_Headview; // tableview的头
    
    UILabel *_seachTextView;//搜索热词
    BOOL _IS_SUPPORT_VOD;//是否支持点播

    WIFIAreaSelectView *_wifiView;
    
    HomeHeaderModalView *_headerView;//tableview的透视图
    
    BOOL firstLoad;//第一次加载
    
    UIView *_footerView;
    
    UIImageView *_footImg;
    
    UIButton *_footBtn;
    
    LocationView *_lcView;//定位窗口
    
    NSString *_areaID;//城市ID
    
    NSString *_healthyNewsUrl;//推荐url
    
    NSString *_recFlg;
    
    NSInteger  healthIndex;

}
@property (nonatomic, strong) UIButton *wifiBtn;
@property (nonatomic, strong) UIButton *historyBtn;
@property (strong ,nonatomic) UITableView    * tableView; //首页选项卡

@property (strong,nonatomic) NSMutableArray *slideArray;   //轮播图数据源
@property (strong,nonatomic) NSMutableArray *slideHealthVodArray;   //轮播图中健康视频list
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页选项卡接收数据数据
@property (strong,nonatomic) NSMutableArray *newsListArray;   //健康咨询数据数据源
@property (strong,nonatomic) NSMutableArray *hotHealthChannelArray;   //健康精选数据源
@property (strong,nonatomic) NSMutableDictionary *pubChannelDic;   //轻松一刻数据源
@property (strong,nonatomic) NSMutableArray *hotEpgArray;   //热门直播数据源
@property (strong,nonatomic) NSMutableArray *tempHotVodArray;   //热门电影过度数据源
@property (strong,nonatomic) NSMutableArray *hotVodArray;   //热门电影数据源
@property (strong,nonatomic) NSMutableArray *hotHealthVodArray;   //热门健康频道及节目
@property (strong,nonatomic) NSMutableDictionary *bottomImageDic;   //首页底部图数据源

@property (strong,nonatomic) NSMutableArray *homeDateArray;   //首页tableview数据源
@property (strong,nonatomic) CarouselPageModel *carouselModel;
/**
 *  首页类型
 */
@property (nonatomic,assign) HomePageType pagetype;


@end


@implementation JKYSHHomeViewController
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.tableView.mj_header endRefreshing];
};

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.homeDateArray.count>0) {
        
        NSLog(@"我可是刷新了");
        
        [self.tableView reloadData];
    }
    
    [self.view bringSubviewToFront:_wifiView];
    
    [_wifiView setHidden:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    firstLoad = YES;
        
    self.pagetype = kApp_HomePageType;
    
    _recFlg = [[NSString alloc]init];
    
    _recFlg = @"1";
    
    [self chooseAppType];
    
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];

    //广告点击
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    //登录成功后发出的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestJKYSHomeData) name:@"RefreshHome" object:nil];
    //登陆失败后发出的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestJKYSHomeData) name:@"GuestLoginFailed" object:nil];
    //定位成功后发出的通知，用来更新医院空间
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationViewReload) name:@"GetNearByHospitalSuccess" object:nil];
    
    self.homeDateArray = [NSMutableArray array];
    
    [self setSeachBar];
    
    [self setUpHead];
    
    WS(wself);
    
    _wifiView = [[WIFIAreaSelectView alloc] init];
    _wifiView.hidden = YES;
    [[[UIApplication sharedApplication].delegate window] addSubview:_wifiView];
    __weak UIButton *wWIFIBtn = _wifiBtn;
    _wifiView.moreWIFIActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");
        
        wWIFIBtn.selected = NO;
        

        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        
        know.urlStr = [UrlPageJumpManeger homePageWifiBtnPageJump];
        
        know.existNaviBar = YES;
        
        [wself.navigationController pushViewController:know animated:YES];
        
    };
    _wifiView.hiddenBlcok = ^(BOOL isHidden){
        
        wWIFIBtn.selected = !isHidden;
        
    };
    
    UIEdgeInsets wifiEdg = UIEdgeInsetsMake(64+kTopSlimSafeSpace, 0, 0, 0);
    
    [_wifiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wifiEdg);
    }];
    
}

- (void)setUpHead{
    WS(wself);
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_selected_color;
    [self.view addSubview:view];
    
    //去除自动适应scrollview
    //初始化tableView

    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStyleGrouped];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    image.frame = self.tableView.frame;
    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    self.tableView.allowsSelection = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.separatorColor = UIColorFromRGB(0xeeeeee);
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
    self.tableView.tableFooterView = [[UIView alloc] init];
    //头View...
    
    CGRect headerReact = CGRectMake(0, 64, kDeviceWidth, 135*kDeviceRate);

    _headerView = [[HomeHeaderModalView alloc]init];
    
    _headerView.frame = headerReact;
    
    
    __weak typeof(&*_headerView)weakeHeader = _headerView;
    _headerView.sizeChangedBlock = ^(CGRect frame){
        NSLog(@"---->> 001");
        weakeHeader.frame = frame;
        wself.tableView.tableHeaderView = weakeHeader;
        
    };
    
    _headerView.cycleScrollViewBlock = ^(NSInteger index,NSString *itemType,NSString *itemID){
        
        [wself AdClicked:index];
        
        NSLog(@"我是轮播图，你想去哪");
        
    };
    
    _headerView.moduleListViewBlock = ^(NSInteger index,NSString *itemType,NSString *itemID){
        
        NSLog(@"我是新模块，你想去哪");
        
        NSLog(@"%ld,%@,%@",(long)index,itemID,itemType);
        
        NSDictionary *dic = [wself.dataArray objectAtIndex:index];
        
        [wself didSelectWith:dic];
        
    };
    
    _headerView.newsViewBlock = ^(NSInteger index,NSString *itemType,NSString *itemID,NSString *itemUrl){
        
        NSLog(@"我是新闻资讯，你想去哪index = %ld;itemType = %@;itemID = %@",(long)index,itemType,itemID);

        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        
        know.existNaviBar = YES;
        
        know.urlStr = [UrlPageJumpManeger homePageHealthNewsPageJumpWithItemId:itemID];
        
        NSLog(@"xxxxxxxx-%@",know.urlStr);
    
        [wself.navigationController pushViewController:know animated:YES];
        
    };


        self.tableView.tableHeaderView = _headerView;
        
        //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
        self.tableView.delaysContentTouches = YES;
    
    _footerView = [[UIView alloc]initWithFrame:CGRectZero];
    
    
    _footBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _footImg = [[UIImageView alloc]init];
    
   
    [_footerView addSubview:_footBtn];

    [_footerView addSubview:_footImg];
    
    
    [_footImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_footerView);
    }];
    [_footBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_footerView);
    }];
    
   
    
    [_footImg setImage:[UIImage imageNamed:@"bg_home_poster_onloading_h220"]];
    
    [_footBtn addTarget:self action:@selector(channelBtnTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView = _footerView;
    
    KDIYRefreshHeader *diyHeader = [KDIYRefreshHeader headerWithRefreshingBlock:^{
        
        [self reRequestJKYSHomeData];
        
    }];
    
    self.tableView.mj_header = diyHeader;

    
    KDIYRefreshBackFooter *freshFooter = [KDIYRefreshBackFooter footerWithRefreshingBlock:^{
        
        [self.tableView.mj_footer endRefreshing];
        
    }];
    
    self.tableView.mj_footer = freshFooter;
    
}

- (void)chooseAppType
{
    
    switch (self.pagetype) {
        case kHomePageType_JXCable_v1_0:
        {
            
        }
            
            break;
            
        case kHomePageType_JKTV_v1_1:
            
        {
            
        }
            
            break;
            
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        {
            
            healthIndex = 1;
            
        }
            
            break;
        case kHomePageType_JKTV_V1_5:{
            
            healthIndex = 2;

        }
            break;
        default:
            break;
    }

}
- (void)locationViewReload
{

    NSString *hospitalName = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
    
    if (hospitalName.length != 0) {
        
        //更新定位信息
        [_lcView setLocationString:[NSString stringWithFormat:@"%@",hospitalName]];
        
    }else
    {
        
        //请求城市列表
        HTRequest *cityRequest = [[HTRequest alloc] initWithDelegate:self];
        [cityRequest YJIntroduceAreaList];
        
    }
    
}


- (void)reRequestJKYSHomeData{
    
    [self locationViewReload];
    
    HomePageRequestManger *manger = [[HomePageRequestManger alloc] initWithDelegate:self];
    
    [manger reloadHeaderData];//通过管理类请求首页所有数据：因为要求模块数据顺序执行，所以请求头数据后顺序请求

    [manger reloadFooterData];
}

- (void)reloadHomeDate
{
    self.homeDateArray = [NSMutableArray array];
    
    if (self.hotHealthChannelArray.count > 0) {
        NSLog(@"%@",self.hotHealthChannelArray);
        NSLog(@"整理健康精选数据进入总数据源");
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.hotHealthChannelArray,@"list",@"健康精选",@"name",@"HomeChannelPolyMedic",@"homeChannelType", nil];
        [self.homeDateArray addObject:dic];
    }
    
    if (self.pubChannelDic.count > 0) {
        NSLog(@"%@",self.pubChannelDic);
        NSLog(@"整理轻松一刻数据进入总数据源");
        
        NSMutableDictionary *tempDic = [self.pubChannelDic objectForKey:@"channelInfo"];
        
        NSMutableArray *tempArr = [self.pubChannelDic objectForKey:@"programInfoList"];
        
        [tempDic setObject:tempArr forKey:@"programInfoList"];
        
        [tempDic setObject:@"HomePubChannel" forKey:@"homeChannelType"];

        [self.homeDateArray addObject:tempDic];
    }

    
    if (self.hotEpgArray.count > 0) {
        NSLog(@"%@",self.hotEpgArray);
        NSLog(@"整理热门直播数据进入总数据源");
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.hotEpgArray,@"list",@"热门直播",@"name",@"HomeChannelHotEpg",@"homeChannelType", nil];
        [self.homeDateArray addObject:dic];
    }
    
    if (self.hotVodArray.count > 0) {
        NSLog(@"%@",self.hotVodArray);
        NSLog(@"整理热门电影数据进入总数据源");
        for (NSMutableDictionary *dic in self.hotVodArray) {
            
            [dic setObject:@"HomeChannelHotVod" forKey:@"homeChannelType"];
            
            [self.homeDateArray addObject:dic];
            
        }
    }
    
    if (self.hotHealthVodArray.count > 0) {
        NSLog(@"%@",self.hotHealthVodArray);
        NSLog(@"整理健康频道和节目进入总数据源");
        
        for (NSMutableDictionary *dic in self.hotHealthVodArray) {
            
            [dic setObject:@"HomeChannelHotPolyMedic" forKey:@"homeChannelType"];
            
            [self.homeDateArray addObject:dic];
            
        }
    }
    
    NSLog(@"刷新时的数据源------%@",self.homeDateArray);
    
    [self.tableView reloadData];
    
}

#pragma mark - tableView 代理方法
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    NSArray *itemArr = [NSArray array];
    
    if ([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"]){
        if ([homeChannelType isEqualToString:@"HomeChannelHotPolyMedic"])
        {
            itemArr = [dic objectForKey:@"programList"];
            
            if (itemArr.count<=0) {
                return 0;
            }
            
            if (itemArr.count > 4) {
                return 2;
            }
            return (itemArr.count-1)/2 +1;
        }
        else
        {
            itemArr = [dic objectForKey:@"vodList"];
            
            if (itemArr.count<=0) {
                return 0;
            }
            
            if (itemArr.count > 3) {
                return 1;
            }
            return (itemArr.count-1)/3 +1;
        }
        
    }
    else if ([homeChannelType isEqualToString:@"HomePubChannel"])
    {
        
        itemArr = [dic objectForKey:@"programInfoList"];
        
        if (itemArr.count<=0) {
            return 0;
        }
        
        if (itemArr.count > 4) {
            return 2;
        }
        return (itemArr.count-1)/2 +1;

    }
    else
    {
        return 1;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return self.homeDateArray.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:indexPath.section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    if ([homeChannelType isEqualToString:@"HomeChannelPolyMedic"])
    {
        return 50*kDeviceRate;
    }
    
    if ([homeChannelType isEqualToString:@"HomeChannelHotEpg"]){
        return 75*kDeviceRate;
    }
    
    if ([homeChannelType isEqualToString:@"HomePubChannel"]){
        return 141*kDeviceRate;
    }
    
    if ([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])
    {
        if ([homeChannelType isEqualToString:@"HomeChannelHotPolyMedic"])
        {
            return 141*kDeviceRate;
        }
        else
        {
            return 192*kDeviceRate;
        }
        
    }
    else
    {
        return 105*kDeviceRate;
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:indexPath.section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    //健康精选
    if ([homeChannelType isEqualToString:@"HomeChannelPolyMedic"]){
        
        HotHealthyChannelTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"healthChannelCell"];
        
        if (cell == nil) {
            cell = [[HotHealthyChannelTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"healthChannelCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        cell.healthyDataArr.array = [dic objectForKey:@"list"] ;
       
        [cell refreshHealthyView];
        
        cell.healthyBlock = ^(UIButton_Block *btn){
            
            NSLog(@"选中了啥？------>> %@",btn.polyMedicDic);
            
            [self firstLevelChannelWithChannelID:[btn.polyMedicDic objectForKey:@"id"] andChannelName:[btn.polyMedicDic objectForKey:@"name"]];
            
        };
        
        return cell;
    }
    
    //轻松一刻
    if ([homeChannelType isEqualToString:@"HomePubChannel"]){
        
        PubChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pubChannelCell"];
        
        if (cell == nil) {
            cell = [[PubChannelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pubChannelCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        
        cell.dateArray.array = [self arrayFOR_ALLArray:[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"programInfoList"] withBeginnumb:indexPath.row withType:homeChannelType];
        
        NSString *pubChannelId = [[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"channelId"];
        
        NSString *pubChannelName = [[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"name"];
        
        
        [cell update];
        
        cell.leftView.subTitle.font = HT_FONT_THIRD;
        cell.rightView.subTitle.font = HT_FONT_THIRD;
        cell.leftView.subTitle.textColor = HT_COLOR_FONT_FIRST;
        cell.rightView.subTitle.textColor = HT_COLOR_FONT_FIRST;
        
        cell.block = ^(UIButton_Block*btn)
        {
            
            PubVideoHomeViewController *pub = [[PubVideoHomeViewController alloc]init];
            
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",pubChannelId];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"首页,%@",pubChannelName];
            
            //入口类型
            NSString *EnType = [NSString stringWithFormat:@"0"];
            
            [self.navigationController pushViewController:pub animated:YES];
            
            [pub setPubChannelId:pubChannelId andPubProgramId:[btn.polyMedicDic objectForKey:@"programId"] andTrackId:TrackId andTrackName:TrackName andEnType:EnType];
            
        };
        
        return cell;
    }
    
    //热门直播
    if ([homeChannelType isEqualToString:@"HomeChannelHotEpg"]){
        
        HotLiveTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotepgCell"];
        
        if (cell == nil) {
            cell = [[HotLiveTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hotepgCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        cell.liveArr.array = [dic objectForKey:@"list"] ;
        
        [cell refreshHotLiveView];
        
        cell.liveBlock = ^(UIButton *btn, NSString *liveID, NSString *serviceName){
            
            NSLog(@"首页打开直播%@",liveID);
            
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",@"_"];
            epg.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",@"热门直播"];
            epg.EN = @"0";
            epg.currentPlayType = CurrentPlayLive;
            epg.serviceID = liveID;
            epg.serviceName = serviceName;
            [self.navigationController pushViewController:epg animated:YES];
            
        };
        
        return cell;
    }
    
    if ([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])
    {
        //健康养生视频列表
         if ([homeChannelType isEqualToString:@"HomeChannelHotPolyMedic"]){
             JKYSHHotHealthyVodCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
             
             if (cell == nil) {
                 cell = [[JKYSHHotHealthyVodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
                 cell.selectionStyle = UITableViewCellSelectionStyleNone;
                 
             }
             
             
             cell.dateArray.array = [self arrayFOR_ALLArray:[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"programList"] withBeginnumb:indexPath.row withType:homeChannelType];
             
             
             [cell update];
             
             cell.leftView.subTitle.font = HT_FONT_THIRD;
             cell.rightView.subTitle.font = HT_FONT_THIRD;
             cell.leftView.subTitle.textColor = HT_COLOR_FONT_FIRST;
             cell.rightView.subTitle.textColor = HT_COLOR_FONT_FIRST;
             
             cell.block = ^(UIButton_Block*btn)
             {
                  [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                 NSLog(@"健康养生视频播放------>> %@",btn.polyMedicDic);
                 
                 NSMutableArray *tempHealthVodList = [NSMutableArray array];
                 
                 tempHealthVodList = [[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"programList"];
                 
                 NSString *channelId = [[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"id"];
                 
                 NSString *channelName = [[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"name"];
                 
                 HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
                 
                 HTVC.recommendArray = tempHealthVodList;
                 
                 HTVC.polyMedicDic = btn.polyMedicDic;
                 NSLog(@"ggggggtttt－－－%@",btn.polyMedicDic);

                 HTVC.titleStr = btn.eventName;
                 
                 NSString *ENtype = [NSString stringWithFormat:@"0"];
                 
                 NSString *TrackId = [[NSString alloc]init];
                 
                 NSString *TrackName = [[NSString alloc]init];
                 
                 //轨道ID
                 TrackId = [NSString stringWithFormat:@"0,%@",channelId];
                 
                 //轨道名称
                 TrackName = [NSString stringWithFormat:@"首页,%@",channelName];
                 
                 NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btn.serviceIDName,@"itemId",btn.eventName,@"name",ENtype,@"ENtype",TrackId,@"TrackId",TrackName,@"TrackName", nil];
//
                 NSLog(@"节目model－－－－－－－%@",paramDic);
                 
                 HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
                 [HTVC setPolyModel:healthVodModel];
                 
                [self.navigationController pushViewController:HTVC animated:NO];
                 
             };
             
             return cell;
        }
        else
        {
            //热门电影视频列表
            HotMovieTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotVodCell"];
            
            if (cell == nil) {
                cell = [[HotMovieTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hotVodCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            
            cell.dataArr.array = [self arrayFOR_ALLArray:[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"vodList"] withBeginnumb:indexPath.row withType:homeChannelType];
            [cell refreshView];
            
            cell.cellBlock = ^(UIButton *btn, NSString *movieID, NSString *movieName)
            {
                NSLog(@"它首页点击的section%@====%@",[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"name"],[NSString stringWithFormat:@"%@,%@",@"0",[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"code"]]);
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"code"]];
                demandVC.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",[[_homeDateArray objectAtIndex:indexPath.section] objectForKey:@"name"]];
                demandVC.EN = [NSString stringWithFormat:@"%@",@"0"];
                demandVC.contentID = movieID;
                demandVC.contentTitleName = movieName;
                
                [self.navigationController pushViewController:demandVC animated:YES];
            };
            
            return cell;
            
        }
        
    }
    else
    {
        //健康养生海报
        JKYSHHomeChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell"];
        
        if (cell == nil) {
            cell = [[JKYSHHomeChannelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"imageCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        cell.dateDic = [_homeDateArray objectAtIndex:indexPath.section];
        
        [cell update];
        
        cell.ChanenelBtnSelected = ^(NSDictionary *dic){

            NSString *channelID;
            NSString *channelName;
            
            channelID= [cell.dateDic objectForKey:@"id"];
            
            channelName= [cell.dateDic objectForKey:@"name"];

            NSString *kparentId = [dic objectForKey:@"parentId"];
            
            NSLog(@"name :%@   id : %@  parentId : %@",channelName,channelID,kparentId);
            
            if (kparentId.length == 0) {
                NSLog(@"我有子频道 ");
                
                [self firstLevelChannelWithChannelID:channelID andChannelName:channelName];
                
            }else{
                NSLog(@"俺没有子频道 ");
                
                YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
                yj.categoryID = channelID;
                yj.name = channelName;
                
                //轨道ID
                NSString *TrackId  = [NSString stringWithFormat:@"0,%@",channelID];
                
                //轨道名称
                NSString *TrackName = [NSString stringWithFormat:@"首页,%@",channelName];
                
                yj.TrackName = TrackName;
                
                yj.TrackId = TrackId;
                
                [self.navigationController pushViewController:yj  animated:YES];
            }
        };
        
        return cell;
    }
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    if ([homeChannelType isEqualToString:@"HomePubChannel"]||[homeChannelType isEqualToString:@"HomeChannelPolyMedic"]||[homeChannelType isEqualToString:@"HomeChannelHotEpg"]||([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])){
        
        return kSection_Header_Height;
    }
    else
    {
        return kSection_GrayLine_Height;
    }
    
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    if ([homeChannelType isEqualToString:@"HomePubChannel"]||[homeChannelType isEqualToString:@"HomeChannelPolyMedic"]||[homeChannelType isEqualToString:@"HomeChannelHotEpg"]||([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])) {
        //整个header
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Header_Height);
        [sectionHeaderView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        
        
        //白色部分的 header
        UIView *headerContentView = [UIView new];
        headerContentView.frame = CGRectMake(0, kSection_GrayLine_Height, kDeviceWidth, kSection_Header_Height-kSection_GrayLine_Height);
        headerContentView.backgroundColor = HT_COLOR_FONT_INVERSE;
        [sectionHeaderView addSubview:headerContentView];
        
        
        //段头标题视图
        UILabel *sectionTitleLabel = [[UILabel alloc] init];
        sectionTitleLabel.frame = CGRectMake(12*kDeviceRate, 0, kDeviceWidth/2, kSection_Header_Height-kSection_GrayLine_Height);
        if (_homeDateArray.count > 0) {
            sectionTitleLabel.text = [[_homeDateArray objectAtIndex:section] objectForKey:@"name"];
        }
        
        sectionTitleLabel.font = HT_FONT_SECOND;
        sectionTitleLabel.textColor = HT_COLOR_FONT_FIRST;
        [headerContentView addSubview:sectionTitleLabel];

        //竖线
        CGSize lineSize = CGSizeMake(1*kDeviceRate, 16*kDeviceRate);
        UIView *line = [UIView new];
        line.frame = CGRectMake(6*kDeviceRate,(headerContentView.frame.size.height-lineSize.height)/2, lineSize.width, lineSize.height);
        line.backgroundColor = App_selected_color;
        [headerContentView addSubview:line];
        line.center = CGPointMake(sectionTitleLabel.frame.origin.x-8, sectionTitleLabel.center.y);
        
        
        //更多按钮
        CGSize moreBtnSize = CGSizeMake(100, headerContentView.frame.size.height);
        UIButton_Block *moreBtn = [[UIButton_Block alloc] init];
        moreBtn.frame = CGRectMake(kDeviceWidth-80*kDeviceRate, 0, moreBtnSize.width, 30*kDeviceRate);
        moreBtn.hidden = YES;
        [moreBtn setTitleColor:HT_COLOR_FONT_ASSIST forState:UIControlStateNormal];
        moreBtn.titleLabel.font = HT_FONT_THIRD;
        [headerContentView addSubview:moreBtn];
        
        [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(headerContentView.mas_right).offset(-12*kDeviceRate);
            make.top.equalTo(headerContentView.mas_top);
            make.bottom.equalTo(headerContentView.mas_bottom);
        }];
        
        moreBtn.Click = ^(UIButton_Block *btn,NSString *hasChild){
            
            if ([homeChannelType isEqualToString:@"HomeChannelHotEpg"])
            {
                if (!IS_EPG_CONFIGURED) {
                    ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                    extranetVC.ExtranetPromptTitle = @"看电视";
                    [self.navigationController pushViewController:extranetVC animated:YES];
                }else{
                    
                    LiveNewHomeViewController *LIVE = [[LiveNewHomeViewController alloc]init];
                    LIVE.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",@"_"];
                    LIVE.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",@"热门直播"];
                    LIVE.EN = @"0";
                    [self.navigationController pushViewController:LIVE animated:YES];
                }
            }
            else if([homeChannelType isEqualToString:@"HomeChannelHotVod"])
            {

                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",@"_"];
                demandVC.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",[[_homeDateArray objectAtIndex:section] objectForKey:@"name"]];
                demandVC.EN = [NSString stringWithFormat:@"%@",@"0"];
                demandVC.contentID = [[_homeDateArray objectAtIndex:section] objectForKey:@"contentID"];
                demandVC.contentTitleName = [[_homeDateArray objectAtIndex:section] objectForKey:@"programName"];
                
                [self.navigationController pushViewController:demandVC animated:YES];

            }

        
        };
        
        if ([homeChannelType isEqualToString:@"HomeChannelHotEpg"])
        {
            [moreBtn setTitle:@"更多 >" forState:UIControlStateNormal];
            
            moreBtn.hidden = NO;
        }
        else if([homeChannelType isEqualToString:@"HomeChannelHotVod"]&&[dic objectForKey:@"programName"])
        {

            NSString *str = [dic objectForKey:@"programName"];

            if (str.length > 0) {
                
                [moreBtn setTitle:[NSString stringWithFormat:@"%@ >",str] forState:UIControlStateNormal];
                
                moreBtn.hidden = NO;
            }

        }
        
        
        return sectionHeaderView;
    }
    else
    {
        
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_GrayLine_Height);
        [sectionHeaderView setBackgroundColor:App_background_color];
        return sectionHeaderView;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    if (([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])||[homeChannelType isEqualToString:@"HomePubChannel"]) {
        
            return kSection_Footer_Height;

    }
    return 0.000001f;//这里不能直接设置为0
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    NSDictionary *dic = [NSDictionary dictionary];
    
    dic = [_homeDateArray objectAtIndex:section];
    
    NSString *homeChannelType = [dic objectForKey:@"homeChannelType"];
    
    if (([[dic objectForKey:@"style"]integerValue] == 0 && [dic objectForKey:@"style"])||[homeChannelType isEqualToString:@"HomePubChannel"]) {
        if (section>=_homeDateArray.count) {
            return nil;
        }

        UIView *footerView = [UIView new];
        footerView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Footer_Height);
        footerView.backgroundColor = HT_COLOR_FONT_INVERSE;
        
        //更多按钮
        CGSize moreBtnSize = CGSizeMake(kDeviceWidth, footerView.frame.size.height);
        UIButton_Block *moreBtn = [[UIButton_Block alloc] init];
        moreBtn.frame = CGRectMake(0, 0, moreBtnSize.width, moreBtnSize.height);
        moreBtn.center = footerView.center;
        [moreBtn setTitle:@"点此查看更多"forState:UIControlStateNormal];
        
        moreBtn.titleLabel.font = HT_FONT_THIRD;
        
        [moreBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        
        [footerView addSubview:moreBtn];
        
        moreBtn.Click = ^(UIButton_Block *btn,NSString *channel){
            
            NSLog(@"点击查看更多");
            
            if ([homeChannelType isEqualToString:@"HomeChannelHotPolyMedic"])
            {
                NSString *channelName = [[_homeDateArray objectAtIndex:section] objectForKey:@"name"];
                NSString *channelID = [[_homeDateArray objectAtIndex:section] objectForKey:@"id"];
                NSString *kparentId = [[_homeDateArray objectAtIndex:section] objectForKey:@"parentId"];
                
                NSLog(@"name :%@   id : %@  parentId : %@",channelName,channelID,kparentId);
                
                if (kparentId.length == 0) {
                    NSLog(@"我有子频道 ");
                    
                    [self firstLevelChannelWithChannelID:channelID andChannelName:channelName];
                
                }else{
                    NSLog(@"俺没有子频道 ");
                    
                    YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
                    yj.categoryID = channelID;
                    yj.name = channelName;
                    
                    //轨道ID
                    NSString *TrackId  = [NSString stringWithFormat:@"0,%@",channelID];
                    
                    //轨道名称
                    NSString *TrackName = [NSString stringWithFormat:@"首页,%@",channelName];
                    
                    yj.TrackName = TrackName;
                    
                    yj.TrackId = TrackId;
                    
                    [self.navigationController pushViewController:yj  animated:YES];

                }
            }
            else if ([homeChannelType isEqualToString:@"HomePubChannel"])
            {
                
                NSString *channelName = [[_homeDateArray objectAtIndex:section] objectForKey:@"name"];
                NSString *channelID = [[_homeDateArray objectAtIndex:section] objectForKey:@"channelId"];
                
                PubVideoHomeViewController *pub = [[PubVideoHomeViewController alloc]init];
                
                //轨道ID
                NSString *TrackId  = [NSString stringWithFormat:@"0,%@",channelID];
                
                //轨道名称
                NSString *TrackName = [NSString stringWithFormat:@"首页,%@",channelName];
                
                //入口类型
                NSString *EnType = [NSString stringWithFormat:@"0"];
                
                [self.navigationController pushViewController:pub animated:YES];
                
                [pub setPubChannelId:[dic objectForKey:@"channelId"] andPubProgramId:@"" andTrackId:TrackId andTrackName:TrackName andEnType:EnType];
                
            }
            else
            {

                if (!IS_EPG_CONFIGURED) {
                    ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                    extranetVC.ExtranetPromptTitle = @"看电影";
                    [self.navigationController pushViewController:extranetVC animated:YES];
                    return;
                }else{
                    DemandMenuViewController *demandMenu = [[DemandMenuViewController alloc]init];
//                    ScrollDemandPageViewController *VOD = [[ScrollDemandPageViewController alloc]init];
                    demandMenu.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",[[_homeDateArray objectAtIndex:section] objectForKey:@"code"]];
                    demandMenu.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",[[_homeDateArray objectAtIndex:section] objectForKey:@"name"]];
                    demandMenu.EN = [NSString stringWithFormat:@"%@",@"0"];
                    [self.navigationController pushViewController:demandMenu animated:YES];
                }
            }
        };
        
        //    UIButton_Block *
        UIView *lineView = [UIView new];
        lineView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        lineView.frame = CGRectMake(0, 0, kDeviceWidth, 0.5);
        [footerView addSubview:lineView];
        
        
        return footerView;
    }
    return nil;
}

//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index withType:(NSString *)type
{
    NSMutableArray *anser = [NSMutableArray array];
    
    if ([type isEqualToString:@"HomeChannelHotPolyMedic"]) {
        if (array.count > index*2) {
            [anser addObject:array[index*2]];
        }
        if (array.count > index*2+1) {
            [anser addObject:array[index*2+1]];
        }
    }
    else
    {
        if (array.count > index*3) {
            [anser addObject:array[index*3]];
        }
        if (array.count > index*3+1) {
            [anser addObject:array[index*3+1]];
        }
        if (array.count > index*3+2) {
            [anser addObject:array[index*3+2]];
        }
    }
    
    return anser;
}

- (void)setSeachBar
{
    
    WS(wself);
    
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(12*kDeviceRate, 25+kTopSlimSafeSpace, 24*kDeviceRate, 24*kDeviceRate)];
    logo.image = [UIImage imageNamed:@"ic_launcher"];
    [self.m_viewNaviBar addSubview:logo];
    
    UIImageView *logoName = [[UIImageView alloc]initWithFrame:CGRectMake(43*kDeviceRate, 25+kTopSlimSafeSpace, 21.5*kDeviceRate, 21.5*kDeviceRate)];
    logoName.image = [UIImage imageNamed:@"ic_appName"];
    [self.m_viewNaviBar addSubview:logoName];
    
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.m_viewNaviBar.mas_bottom).offset(-10*kDeviceRate);
        make.left.mas_equalTo(wself.m_viewNaviBar.mas_left).offset(12*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(24*kDeviceRate, 24*kDeviceRate));
    }];
    
    [logoName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.m_viewNaviBar.mas_bottom).offset(-11*kDeviceRate);
        make.left.mas_equalTo(logo.mas_right).offset(7*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(21.5*kDeviceRate, 21.5*kDeviceRate));
    }];
    
    _historyBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_recoder" imgHighlight:@"btn_home_recoder" imgSelected:@"btn_home_recoder" target:self action:@selector(gotoRecorderView)];
    [self setNaviBarRRightBtn:_historyBtn];
    
    _wifiBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_area_wifi" imgHighlight:@"btn_home_area_wifi" imgSelected:@"btn_home_area_wifi_close" target:self action:@selector(openWifiInformation)];
    [self setNaviBarRightBtn:_wifiBtn];
    
    if (!_lcView) {
        _lcView = [[LocationView alloc] initWithFrame:CGRectMake(0, 0, 180*kDeviceRate, 25*kDeviceRate)];
        _lcView.backgroundColor = [UIColor clearColor];
        _lcView.layer.cornerRadius = 12.5*kDeviceRate;
        _lcView.actionBlock = ^(){
            
            NSLog(@"sdfsdfw");
            
            ChooseHospitalViewController *hospitalVC = [[ChooseHospitalViewController alloc] init];
            
            hospitalVC.refreshHome = ^(){
                
                [wself locationViewReload];
                
            };
            
            [wself.navigationController pushViewController:hospitalVC animated:YES];
            
        };
        
        [self.m_viewNaviBar addSubview:_lcView];
        
        [_lcView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(wself.m_viewNaviBar.mas_bottom).offset(-9.5*kDeviceRate);
            make.centerX.mas_equalTo(wself.m_viewNaviBar.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(180*kDeviceRate, 25*kDeviceRate));
        }];
    }
    
    
    
    
    NSString *hospitalName = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
    
    if (hospitalName.length != 0) {
        
        [_lcView setLocationString:[NSString stringWithFormat:@"%@",hospitalName]];
        
    }
}

- (void)gotoRecorderView
{
    
    _wifiView.hidden = YES;
    _wifiBtn.selected = NO;
    
    if (IsLogin) {
        
        if (!IS_EPG_CONFIGURED) {
            ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
            extranet.ExtranetPromptTitle = @"播放记录";
            [self.navigationController pushViewController:extranet animated:YES];
            
            return;
        }
        MineRecorderViewController *vc = [[MineRecorderViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        if (isAuthCodeLogin) {
            
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:user];
            [self presentViewController:nav animated:YES completion:nil];
        }else{
            
            [AlertLabel AlertInfoWithText:@"请先登录！" andWith:self.view withLocationTag:0];
            
        }
        
    }
    
}

- (void)openWifiInformation
{
    NSLog(@"展示现在wifi信息");
    _wifiView.hidden = !_wifiView.hidden;
    
    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
        if ([netname isEqualToString:@"当前无可用网络"]) {
            [_wifiView resetNetWorkingStatusWith:0 andNetWorkingName:netname];
            
        }else if([netname isEqualToString:@"您正在使用移动网络"]){
            [_wifiView resetNetWorkingStatusWith:1 andNetWorkingName:netname];
            
        }else{
            [_wifiView resetNetWorkingStatusWith:2 andNetWorkingName:netname];
            
        }
        
    }];
}

#pragma  mark - 广告点击
- (void)AdClicked:(NSInteger)index
{

    NSDictionary *object = [self.slideArray objectAtIndex:index];
        
    NSString *type = [object objectForKey:@"type"];
    [self homeADJumpWithType:type andDiction:object];
    NSDictionary *carouselDic = [NSDictionary dictionaryWithObjectsAndKeys:(isEmptyStringOrNilOrNull([object objectForKey:@"seq"])?@"":[object objectForKey:@"seq"]),@"carouselSEQ",(isEmptyStringOrNilOrNull([object objectForKey:@"id"])?@"":[object objectForKey:@"id"]),@"carouselID",(isEmptyStringOrNilOrNull([object objectForKey:@"name"])?@"":[object objectForKey:@"name"]),@"carouselName",(isEmptyStringOrNilOrNull([object objectForKey:@"type"])?@"":[object objectForKey:@"type"]),@"carouselType",[SystermTimeControl getNowServiceTimeString],@"carouselBT", nil];
    //轮播图的信息的采集
    [SAIInformationManager carouselInfomation:[CarouselPageModel carouselCollectionWithDict:carouselDic]];
}

- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                
                if (!IS_EPG_CONFIGURED) {
                    ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                    extranetVC.ExtranetPromptTitle = @"看电影";
                    [self.navigationController pushViewController:extranetVC animated:YES];
                }
                else
                {
                    NSString *name = [dic objectForKey:@"name"];
                    NSString *contentID = [dic objectForKey:@"id"];
                    DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                    demandVC.contentID = contentID;
                    demandVC.contentTitleName = name;
                    [self.navigationController pushViewController:demandVC animated:YES];
                    
                }

            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
            
        }
            
            break;
            
        case 5:
        {            
            if (!IS_EPG_CONFIGURED) {
                ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                extranetVC.ExtranetPromptTitle = @"看电视";
                [self.navigationController pushViewController:extranetVC animated:YES];
            }
            else
            {
                EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
                epg.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",@"_"];
                epg.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",@"热门直播"];
                epg.EN = @"0";
                epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
                epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
                epg.currentPlayType = CurrentPlayLive;
                epg.IsFirstChannelist = YES;
                
                [self.navigationController pushViewController:epg animated:YES];
                
            }

        }
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = url;
                [self presentViewController:demandDetail animated:YES completion:nil];
            }
        }
            break;

        case 8:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                

                
                KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
                
                know.urlStr = [UrlPageJumpManeger homePageADPageJumpWithADUrlLink:url];
                
                know.existNaviBar = YES;
                
                [self.navigationController pushViewController:know animated:YES];
                
            }
        }
            
            break;
        case 10://健康视频
        {
            NSLog(@"要跳转的健康视频是%@",dic);
            
            NSString *channelId = [dic objectForKey:@"id"];
            NSString *channelName = [dic objectForKey:@"name"];
            
            HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
            
            HTVC.recommendArray = self.slideHealthVodArray;

            HTVC.contentID = channelId;
            HTVC.titleStr = channelName;
            
            NSString *ENtype = [NSString stringWithFormat:@"4"];
            
            NSString *TrackId = [[NSString alloc]init];
            
            NSString *TrackName = [[NSString alloc]init];
            
            //轨道ID
            TrackId = [NSString stringWithFormat:@"_"];
            
            //轨道名称
            TrackName = [NSString stringWithFormat:@"_"];
            
            NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:HTVC.contentID,@"itemId",HTVC.titleStr,@"name",ENtype,@"ENtype",TrackId,@"TrackId",TrackName,@"TrackName", nil];
            //
            NSLog(@"首页轮播图--健康视频model－－－－－－－%@",paramDic);
            
            HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
            [HTVC setPolyModel:healthVodModel];
            
            
            
            [self.navigationController pushViewController:HTVC animated:NO];

        }
            
            break;
        case 11://健康养生一级频道
        {
            NSLog(@"要跳转的健康养生一级频道是%@",dic);
            
            [self firstLevelChannelWithChannelID:[dic objectForKey:@"id"] andChannelName:[dic objectForKey:@"name"]];

        }
            
            break;
        case 12://健康养生二级频道
        {
            NSLog(@"要跳转的健康养生二级频道是%@",dic);
            
            YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
            yj.categoryID = [dic objectForKey:@"id"];
            yj.name = [dic objectForKey:@"name"];
            
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",yj.categoryID];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"首页,%@",yj.name];
            
            yj.TrackName = TrackName;
            
            yj.TrackId = TrackId;
            
            [self.navigationController pushViewController:yj  animated:YES];

        }
            
            break;

        default:
        {
            
        }
            break;
    }
    
}

#pragma  mark - 首页模块点击
-(void)firstLevelChannelWithChannelID:(NSString *)channelID andChannelName:(NSString *)channelName
{
    HT_NavigationController *htnav = (HT_NavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    UITabBarController *tabVC = (UITabBarController *)htnav.topViewController;
    
    [tabVC setSelectedIndex:healthIndex];
            
    HealthVodViewController *hVC = (HealthVodViewController *)[tabVC viewControllers][healthIndex];
            
    //轨道ID
    NSString *TrackId  = [NSString stringWithFormat:@"0,%@",channelID];
            
    //轨道名称
    NSString *TrackName = [NSString stringWithFormat:@"首页,%@",channelName];
            
    [hVC setHealthId:channelID andTrackId:TrackId andTrackName:TrackName];
   
}

- (void)didSelectWith:(NSDictionary*)dic{
    
    NSString *moduleType = [dic objectForKey:@"moduleType"];
    NSString *moduleInfo = [dic objectForKey:@"moduleInfo"];
    NSString *moduleName = [dic objectForKey:@"moduleName"];
    NSString *moduleCode = [dic objectForKey:@"moduleCode"];
    
    HTUGCUpLoadInterface *ugc = [HTUGCUpLoadInterface shareInstance];
    
    [ugc cancelOperations];
    
    
    //信息采集
    NSString *systemTime = [SystermTimeControl getNowServiceTimeString];
    
    //        NSLog(@"系统时间hahahahah－－－－－%@",systemTime);
    
    [dic setValue:systemTime forKey:@"systemTime"];
    
    JKTVHomeCollectionModel *homeCollection = [JKTVHomeCollectionModel homeWithDict:dic];
    
    //        NSLog(@"%@,%@,%@",homeCollection.moduleName,homeCollection.systemTime,homeCollection.seq);
    
    if (USER_ACTION_COLLECTION) {
        [SAIInformationManager JKTVHomeInformation:homeCollection];
    }
    
    
    if ([moduleType isEqualToString:@"1"]) {
        
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        
        BOOL isYSHBK = NO;
        
        NSString *tempRecFlg = [[NSString alloc]init];
        
        if ([moduleCode isEqualToString:@"4"]) {
            
            isYSHBK = YES;
            
            tempRecFlg = _recFlg;
            
            if ([_recFlg isEqualToString:@"1"]) {
                _recFlg = @"0";
            }

        }else
        {
            
            isYSHBK = NO;

        }
        
        know.urlStr = [UrlPageJumpManeger homePageModuleConfigJumpWithModuleInfo:moduleInfo andRecFlg:tempRecFlg andIsYSHBK:isYSHBK];
        
        know.existNaviBar = YES;
            
        [self.navigationController pushViewController:know animated:YES];
        
    }
    else if([moduleType isEqualToString:@"3"]) {//挂号
        
        if (IsLogin) {
            
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
            
            know.urlStr = [UrlPageJumpManeger homePageSXHospitalJumpWithModuleInfo:moduleInfo];
            
            know.existNaviBar = YES;
            
            [self.navigationController pushViewController:know animated:YES];
            
        }
        else
        {
            WS(wself);
            UserLoginViewController *login = [[UserLoginViewController alloc] init];
            login.isAccountBindingFlag = YES;
            
            login.loginType = kPopSelfLogin;
            
            [self.navigationController pushViewController:login animated:YES];
            
            login.refreshHomeBlock = ^(){//登录成功后的回调
  
                KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
                
                know.urlStr = [UrlPageJumpManeger homePageSXHospitalJumpWithModuleInfo:moduleInfo];
                
                NSLog(@"derk-------%@",know.urlStr);
                
                know.existNaviBar = YES;
                
                [wself.navigationController pushViewController:know animated:YES];
            };
        }
    }
    else
    {
        if ([moduleInfo isEqualToString:@"tv"]) {
            
            if (!IS_EPG_CONFIGURED) {
                ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                extranetVC.ExtranetPromptTitle = moduleName;
                [self.navigationController pushViewController:extranetVC animated:YES];
            }else{
                
                LiveNewHomeViewController *LIVE = [[LiveNewHomeViewController alloc]init];
                LIVE.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",@"_"];
                LIVE.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",@"热门直播"];
                LIVE.EN = @"0";
                [self.navigationController pushViewController:LIVE animated:YES];
            }
            
        }
        else if([moduleInfo isEqualToString:@"vod"])
        {
            
            if (!IS_EPG_CONFIGURED) {
                ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                extranetVC.ExtranetPromptTitle = moduleName;
                [self.navigationController pushViewController:extranetVC animated:YES];
                return;
            }else{
                
                if (IS_VODHOMEPage_NewVersion) {
                    //我要修改的地方
                DemandMenuViewController *demandMenu = [[DemandMenuViewController alloc]init];
                    demandMenu.TRACKID = [NSString stringWithFormat:@"%@,%@",@"_",moduleCode];
                    demandMenu.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",moduleName];
                    demandMenu.EN = @"0";
                [self.navigationController pushViewController:demandMenu animated:YES];
  
                }else
                {
                ScrollDemandPageViewController *VOD = [[ScrollDemandPageViewController alloc]init];
                [self.navigationController pushViewController:VOD animated:YES];
                }
            }
            
        }
        
    }
    
}


/**
 *  显示loading框
 */
- (void)show{
    
    [self showCustomeHUD];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}
/**
 *  隐藏loading框
 */
- (void)hide{
    
    //    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self hiddenCustomHUD];
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:YJ_INTRODUCE_AREALIST]) {//请求城市列表
        NSLog(@"城市列表---返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] && (status != 0))
        {
            NSMutableArray *cityArr = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];

            
            _areaID = [[cityArr objectAtIndex:0] objectForKey:@"id"];

            HTRequest *hospitalRequest = [[HTRequest alloc] initWithDelegate:self];
            [hospitalRequest YJIntroduceListWithAreaId:_areaID withPageNo:@"1" withPageSize:@"1"];
        

        }
    }
    
    if ([type isEqualToString:YJ_INTRODUCE_LIST]) {//请求医院
        NSLog(@"医院列表---返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"])
        {
            
            NSDictionary *resDIC = result;
            
            NSMutableArray *hospitalTotalArr = [NSMutableArray arrayWithArray:[resDIC objectForKey:@"list"]];
            
            NSString *hospitalName = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
            
            //再次比对是否有默认医院
            if (hospitalName.length != 0) {
                
                //更新定位信息
                [_lcView setLocationString:[NSString stringWithFormat:@"%@",hospitalName]];
                
            }else
            {
                
                hospitalName = [[hospitalTotalArr objectAtIndex:0]objectForKey:@"name"];
                
                //更新定位信息
                [_lcView setLocationString:[NSString stringWithFormat:@"%@",hospitalName]];
                
                NSString *code = [[hospitalTotalArr objectAtIndex:0] objectForKey:@"instanceCode"];


                [[NSUserDefaults standardUserDefaults] setObject:code.length?code:@"" forKey:@"InstanceCode"];
                [[NSUserDefaults standardUserDefaults] setObject:hospitalName.length?hospitalName:@"" forKey:@"hospitalName"];
                [[NSUserDefaults standardUserDefaults] setObject:_areaID.length?_areaID:@"" forKey:@"areaID"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            
        }
    }
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)homeHeaderDataGetSuccessWith:(NSMutableArray *)cycleImageResult andModules:(NSMutableArray *)modulesResult andHomeNews:(NSMutableArray *)homeNewsResult andHomeNewsUrl:(NSString *)url
{
    HomePageRequestManger *manger = [[HomePageRequestManger alloc]initWithDelegate:self];
    
    [manger reloadGreatHealthData];

    NSLog(@"manger返回的头数据：%@,%@,%@,%@",cycleImageResult,modulesResult,homeNewsResult,url);
    
    self.slideArray = [NSMutableArray array];
    
    self.slideHealthVodArray = [NSMutableArray array];

    if (cycleImageResult.count > 0) {
        
        self.slideArray = cycleImageResult;
    
        for (NSMutableDictionary *dic in self.slideArray) {
            
            if ([[dic objectForKey:@"type"]isEqualToString:@"10"]) {
                
                [self.slideHealthVodArray addObject:dic];
                
            }
        }
        
    }

    NSLog(@"manger广告整理后数据%@",self.slideArray);
    
    self.dataArray = [NSMutableArray array];
    
    [_headerView reloadHomeCycleScrollViewWith:self.slideArray];

    if (modulesResult.count > 0) {
  
        self.dataArray = modulesResult;
        
    }
    
    NSLog(@"manger首页模块整理后数据:%@",self.dataArray);
    
    [_headerView reloadModuleListViewWith:self.dataArray];
    
    self.newsListArray = [NSMutableArray array];
    
    _healthyNewsUrl = [[NSString alloc]init];
    
    if (homeNewsResult.count > 0) {
      
        self.newsListArray = homeNewsResult;
   
        _healthyNewsUrl = url;
        
    }
    
    NSLog(@"manger健康咨询整理后数据%@,%@",self.newsListArray ,_healthyNewsUrl);

    [_headerView reloadNewsViewWith:self.newsListArray];
    
    
    
}


-(void)greatHealthChannelDataGetSuccessWith:(NSMutableArray *)result
{
    HomePageRequestManger *manger = [[HomePageRequestManger alloc]initWithDelegate:self];
    
    [manger reloadPubChannelData];
    
//    NSLog(@"manger返回的健康精选数据%@",result);
    
    self.hotHealthChannelArray = [NSMutableArray array];
    
    if (result.count > 0) {
        
        self.hotHealthChannelArray = result;
    }
    
    NSLog(@"manger健康精选整理后数据%@",self.hotHealthChannelArray);

    
    [self reloadHomeDate];
    
}

-(void)homePubChannelGetSuccessWith:(NSMutableDictionary *)result
{
    
    HomePageRequestManger *manger = [[HomePageRequestManger alloc]initWithDelegate:self];
    
    [manger reloadHotChannelData];
    
    self.pubChannelDic = [NSMutableDictionary dictionary];
    
    if (result.count > 0) {
        
        self.pubChannelDic = result;
        
    }

    [self reloadHomeDate];
    
}



-(void)hotChannelDataGetSuccessWith:(NSMutableArray *)result
{
    HomePageRequestManger *manger = [[HomePageRequestManger alloc]initWithDelegate:self];
    
    [manger reloadVODData];
    
//    NSLog(@"manger返回的热门直播数据%@",result);
    
    self.hotEpgArray = [NSMutableArray array];
    
    if (result.count > 0) {
        
        self.hotEpgArray = result;
    }
    
    NSLog(@"manger热门直播整理后数据%@",self.hotEpgArray);
    
    [self reloadHomeDate];
    
}

-(void)homeVODDataGetSuccessWith:(NSMutableArray *)result
{
    HomePageRequestManger *manger = [[HomePageRequestManger alloc]initWithDelegate:self];
    
    [manger reloadhealthVideoData];
    
//    NSLog(@"manger返回的热门电影数据%@",result);
    
    self.hotVodArray = [NSMutableArray array];
    
    if (result.count > 0) {
 
        self.hotVodArray = result;
    }
    
    NSLog(@"manger热门电影整理后数据%@",self.hotVodArray);
    
    [self reloadHomeDate];
    
    [self.tableView.mj_header endRefreshing];
}

-(void)homeHealthVideoGetSuccessWith:(NSMutableArray *)result
{
//    NSLog(@"manger返回的热门健康数据%@",result);
    
    self.hotHealthVodArray = [NSMutableArray array];
    
    if (result.count > 0) {

        self.hotHealthVodArray = result;
    }
    
    NSLog(@"manger健康频道整理后数据%@",self.hotHealthVodArray);

    [self reloadHomeDate];
}

-(void)homeBottomImageGetSuccessWith:(NSMutableDictionary *)result
{

    self.bottomImageDic = [NSMutableDictionary dictionary];
    
    if (result.count > 0) {

        self.bottomImageDic = result;
    }
    _footerView.frame = CGRectMake(0, 0, kDeviceWidth, 105*kDeviceRate);
    _tableView.tableFooterView = _footerView;
    NSLog(@"manger首页底部图整理后数据%@",self.bottomImageDic);
    
    NSString *footIMGURL =NSStringIMGFormat([[_bottomImageDic objectForKey:@"imageLink"] length]?[_bottomImageDic objectForKey:@"imageLink"]:@"");
    [_footImg sd_setImageWithURL:[NSURL URLWithString:footIMGURL] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading_h220"]];
    

}

-(void)channelBtnTouchUpInside:(UIButton*)sender
{
    
    if (self.bottomImageDic) {
        
        NSLog(@"要跳转的是%@",self.bottomImageDic);
        NSString *url = [self.bottomImageDic objectForKey:@"urlLink"];
        if (url.length > 0 ) {
            
            NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
            if (token.length == 0) {
                
                token = @"";
                
            }
            
            NSString *type = [self.bottomImageDic objectForKey:@"type"];
            NSString *itemID = [self.bottomImageDic objectForKey:@"id"];
            NSString *itemName = [self.bottomImageDic objectForKey:@"name"];
            
            if (type.length > 0) {
                switch ([type integerValue]) {
                    case 7:
                    {
                        NSLog(@"bottomImage --- 宣传图片");
                        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
                        
                        know.urlStr = [UrlPageJumpManeger homePageADPageJumpWithADUrlLink:url];
                        
                        know.existNaviBar = YES;
                        
                        [self.navigationController pushViewController:know animated:YES];
                    }
                        break;
                    case 8:
                    {
                        NSLog(@"bottomImage --- 推荐模块");

                        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
                        know.urlStr = [UrlPageJumpManeger homePageADPageJumpWithADUrlLink:url];
                        
                        know.existNaviBar = YES;
                        
                        [self.navigationController pushViewController:know animated:YES];
                    }
                        break;
                    case 4:
                    {
                        NSLog(@"bottomImage --- 广电点播");
                        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                        demandVC.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",itemID];
                        demandVC.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",itemName];
                        demandVC.EN = [NSString stringWithFormat:@"%@",@"0"];
                        demandVC.contentID = itemID;
                        demandVC.contentTitleName = itemName;
                        
                        [self.navigationController pushViewController:demandVC animated:YES];
                    }
                        break;
                    case 5:
                    {
                        NSLog(@"bottomImage --- 广电直播");
                        EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
                        epg.TRACKID = [NSString stringWithFormat:@"%@,%@",@"0",itemID];
                        epg.TRACKNAME = [NSString stringWithFormat:@"%@,%@",@"首页",itemName];
                        epg.EN = @"0";
                        epg.currentPlayType = CurrentPlayLive;
                        epg.serviceID = itemID;
                        epg.serviceName = itemName;
                        
                        [self.navigationController pushViewController:epg animated:YES];
                    }
                        break;
                    case 10:
                    {
                        NSLog(@"bottomImage --- 健康频道");
                        HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
                        HTVC.contentID = itemID;
                        HTVC.titleStr = itemName;
                        NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:itemID,@"itemId",itemName,@"name",@"0",@"ENtype",[NSString stringWithFormat:@"%@,%@",@"0",itemID],@"TrackId",[NSString stringWithFormat:@"%@,%@",@"首页",itemName],@"TrackName", nil];
                        
                        HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
                        [HTVC setPolyModel:healthVodModel];

                        [self.navigationController pushViewController:HTVC animated:NO];


                    }
                        break;

                    case 11:
                    {
                        NSLog(@"bottomImage --- 健康一级频道");
//                        JKChannelDetailViewController *jk = [[JKChannelDetailViewController alloc]init];
//                        jk.detailTitle = itemName;
//                        jk.pageChannelID = itemID;
//                        jk.isNeedTabBar = NO;
//                        
//                        [self.navigationController pushViewController:jk animated:YES];
                        
                        [self firstLevelChannelWithChannelID:itemID andChannelName:itemName];
                        
                    }
                        break;
                    case 12:
                    {
                        NSLog(@"bottomImage --- 健康二级频道");
                        YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
                        yj.categoryID = itemID;
                        yj.name = itemName;
                        //轨道ID
                        NSString *TrackId  = [NSString stringWithFormat:@"0,%@",yj.categoryID];
                        
                        //轨道名称
                        NSString *TrackName = [NSString stringWithFormat:@"首页,%@",yj.name];
                        
                        yj.TrackName = TrackName;
                        
                        yj.TrackId = TrackId;
                        yj.ENtype = @"0";
                        [self.navigationController pushViewController:yj  animated:YES];

                    }
                        break;
                        
                    default:
                        break;
                }
                
               
            }
            
           
            
        }

        
    }
    
}

@end
