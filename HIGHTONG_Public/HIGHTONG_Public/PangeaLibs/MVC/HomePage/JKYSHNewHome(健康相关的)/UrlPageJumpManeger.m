//
//  UrlPageJumpManeger.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/2/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UrlPageJumpManeger.h"

@implementation UrlPageJumpManeger

+ (NSString *)extranetPromptPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    if ([APP_ID isEqualToString: @"562172033"]) {
        
        NSString *out_Net = [NSString stringWithFormat:@"%@/shphone/pages/jxnet/out/index.html",YJ_Shphone];
        urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }else{
        
        NSString *out_Net = web_OutNet_More_Detail;
        urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    }
    
    return urlStr;
    
}

+ (NSString *)homePageWifiBtnPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *fullUrlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"moreWifiLink"];
    
    urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&ch=3&devID=%@&reqType=NH",fullUrlStr,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    
    return urlStr;
    
    
}

+ (NSString *)homePageHealthNewsPageJumpWithItemId:(NSString *)itemId
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *shportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"shportal"];
    
    if (shportal.length) {
        
        urlStr = [NSString stringWithFormat:@"%@/healthEnc/index.html?regionCode=%@&u=%@&devType=%@&devID=%@&instanceCode=%@&ch=3&reqType=NH/#/hcdetail/%@",shportal,KREGION_CODE,U_KEY,TERMINAL_TYPE,ICS_deviceID,KInstanceCode,itemId];
        
    }else
    {
        
        urlStr = [NSString stringWithFormat:@"%@/shphone/pages/healthcare/index.html?regionCode=%@&u=%@&devType=%@&devID=%@&instanceCode=%@&ch=3&reqType=NH/#/hcdetail/%@",YJ_Shphone,KREGION_CODE,U_KEY,TERMINAL_TYPE,ICS_deviceID,KInstanceCode,itemId];

    }

    return urlStr;
}

+ (NSString *)homePageRelaxProgramPageJumpWithPubChannelId:(NSString *)pubChannelId andProgramId:(NSString *)programId
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *shportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"shportal"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length == 0) {
        
        token = @"";
        
    }
    
    if (shportal.length) {
        
        urlStr = [NSString stringWithFormat:@"%@/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@&u=%@&devType=%@&reqType=NH&ch=3&devID=%@#/relax/%@/%@",shportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID,pubChannelId,programId];
        
    }else
    {
        
        NSString *pubportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"pubportal"];
        
        urlStr = [NSString stringWithFormat:@"%@/pages/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@&u=%@&devType=%@&reqType=NH&ch=3&devID=%@#/relax/%@/%@",pubportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID,pubChannelId,programId];
    }
    
    
    
    return urlStr;
    
}

+ (NSString *)homePageRelaxChannelPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *shportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"shportal"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length == 0) {
        
        token = @"";
        
    }
    
    if (shportal.length) {
        
        urlStr = [NSString stringWithFormat:@"%@/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@@&u=%@&devType=%@&ch=3&reqType=NH&devID=%@",shportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }else
    {
        
        NSString *pubportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"pubportal"];
        
        urlStr = [NSString stringWithFormat:@"%@/pages/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@&u=%@&devType=%@&reqType=NH&ch=3&devID=%@",pubportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }
    
    return urlStr;
}

+ (NSString *)homePageADPageJumpWithADUrlLink:(NSString *)urlLink
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length == 0) {
        
        token = @"";
        
    }
    
    urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&u=%@&devType=%@&ch=3&devID=%@&reqType=NH&appFlg=1&token=%@",urlLink,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID,token];
    
    return urlStr;
    
}

+ (NSString *)homePageModuleConfigJumpWithModuleInfo:(NSString *)moduleInfo andRecFlg:(NSString *)recFlg andIsYSHBK:(BOOL)isYSHBK;
{
    
    NSString *urlStr = [[NSString alloc]init];

    if (isYSHBK) {
        
        urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&u=%@&ch=3&devType=%@&reqType=NH&devID=%@&recFlg=%@",moduleInfo,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID,recFlg];
        
    }else
    {
        
        urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&u=%@&ch=3&devType=%@&reqType=NH&devID=%@",moduleInfo,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }
    
    return urlStr;
    
}

+ (NSString *)homePageSXHospitalJumpWithModuleInfo:(NSString *)moduleInfo
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    urlStr =  [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&ch=3&u=%@&devType=%@&devID=%@&token=%@&phone=%@&reqType=NH",moduleInfo,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID,token,KMobileNum];
    
    return urlStr;
    
}

+ (NSString *)homePageComPanyOperationJumpWithModuleInfo:(NSString *)url
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    urlStr = [NSString stringWithFormat:@"%@?tsytle=%@&u=%@&ch=3&devType=%@&devID=%@&reqType=NH",url,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    
    return urlStr;
}

+ (NSString *)minePageAboutUSJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    if (YES || [APP_ID isEqualToString:@"1099034502"]) {
        NSString *aboutUsStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"aboutusurl"];
        urlStr = [NSString stringWithFormat:@"%@&tstyle=%@&u=%@&ch=3&devType=%@&devID=%@&reqType=NH",aboutUsStr,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    }
//    else
//    {
//        urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",web_About_Us,KREGION_CODE,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
//    }
    
    return urlStr;
}

+ (NSString *)loginPageServiceVCJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    if (YES||[APP_ID isEqualToString:@"1099034502"]) {
        
        NSString *agreementStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"agreementurl"];
        
        urlStr = [NSString stringWithFormat:@"%@?tsytle=%@&u=%@&ch=3&devType=%@&devID=%@&reqType=NH",agreementStr,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }
//    else
//    {
//        urlStr = [NSString stringWithFormat:@"%@/shphone/pages/agreement/index.html?regionCode=%@&tsytle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",YJ_Shphone,KREGION_CODE,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
//        ;
//
//    }
    
    return urlStr;
    
}

//+ (NSString *)aboutUsPageJump
//{
//    
//    NSString *urlStr = [[NSString alloc]init];
//    
//    urlStr = [NSString stringWithFormat:@"%@/shphone/pages/aboutus/index.html?regionCode=%@&ch=3&reqType=NH",YJ_Shphone,KREGION_CODE];
//    
//    return urlStr;
//}

+ (NSString *)feedBackPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    if ([App_Theme_Color_num isEqualToString:@"3ab7f5"]) {
        
        urlStr = [NSString stringWithFormat:@"%@/shphone/pages/feedback/index.html?regionCode=%@&instanceCode=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",YJ_Shphone,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }else{
        
        urlStr = [NSString stringWithFormat:@"%@/shphone/pages/feedback/index.html?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",YJ_Shphone,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
    }
    return urlStr;
}

+ (NSString *)bPushJumpWithExtraInfo:(NSString *)extraInfo andExtraId:(NSString *)extraId
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    if ([extraInfo rangeOfString:@"htt"].location != NSNotFound) {
        urlStr = [NSString stringWithFormat:@"%@",extraInfo];
    }else
    {
        
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        
        NSString *shportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"shportal"];
        
        if (shportal.length) {
            
            urlStr = [NSString stringWithFormat:@"%@/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@&u=%@&devType=%@&ch=3&reqType=NH&devID=%@%@",shportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID,extraInfo];
            
        }else
        {
            
            NSString *pubportal = [[NSUserDefaults standardUserDefaults] objectForKey:@"pubportal"];
            
            urlStr = [NSString stringWithFormat:@"%@/pages/relax/index.html?regionCode=%@&instanceCode=%@&appFlg=1&token=%@&u=%@&devType=%@&ch=3&reqType=NH&devID=%@%@",pubportal,KREGION_CODE,KInstanceCode,token,U_KEY,TERMINAL_TYPE,ICS_deviceID,extraInfo];
            
        }
        
        NSLog(@"我要的推送URL1%@",urlStr);
        
    }
    
    return urlStr;
}

//UGC直播协议参数拼接
+ (NSString *)liveProtocolPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    
    NSString *out_Net = [NSString stringWithFormat:@"%@/shphone/pages/liveProtocol/index.html",YJ_Shphone];
    
    urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    
    return urlStr;
}

//UGC上传参数拼接
+ (NSString *)upProtocolPageJump
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    
    NSString *out_Net = [NSString stringWithFormat:@"%@/shphone/pages/upProtocol/index.html",YJ_Shphone];
    
    urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3&reqType=NH",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
    
    return urlStr;
    
}

//轻松一刻分享链接拼接
+ (NSString *)relaxShareLinkWithChannelID:(NSString *)channelID andChannelName:(NSString *)channelName
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    NSString *out_Net = [NSString stringWithFormat:@"%@/share/index.html",YJ_Shportal];
    
    NSString *tempChannelName = [channelName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    urlStr = [NSString stringWithFormat:@"%@?shareFlg=1&regionCode=%@&instanceCode=%@&u=%@&devType=%@&devID=%@&id=%@&title=%@#/relax/share",out_Net,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID,channelID,tempChannelName];
    
    return urlStr;
    
}

//健康电视分享链接拼接
+ (NSString *)healthyShareLinkWithChannelID:(NSString *)channelID
{
    
    NSString *urlStr = [[NSString alloc]init];
    
    
    NSString *out_Net = [NSString stringWithFormat:@"%@/share/index.html",YJ_Shportal];
    
    urlStr = [NSString stringWithFormat:@"%@?shareFlg=1&regionCode=%@&instanceCode=%@&u=%@&devType=%@&devID=%@&id=%@#/healthy/share",out_Net,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID,channelID];
    
    return urlStr;
    
}

@end
