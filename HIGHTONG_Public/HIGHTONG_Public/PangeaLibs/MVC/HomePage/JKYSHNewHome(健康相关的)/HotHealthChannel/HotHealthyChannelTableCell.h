//
//  HotHealthyChannelTableCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

//typedef void(^HotHealthyChannelTableCellBlock)(UIButton *btn, NSString *name, NSString *channelID);

typedef void(^HotHealthyChannelTableCellBlock)(UIButton_Block *btn);


@interface HotHealthyChannelTableCell : UITableViewCell

@property (nonatomic, copy)HotHealthyChannelTableCellBlock healthyBlock;

//@property (nonatomic, strong)UIButton *channelBtn;

@property (nonatomic, strong)UIImageView *healthyPosterImg;

@property (nonatomic, strong)UIButton_Block *channelBtn;

@property (nonatomic, strong)UIImageView *partingLine;

@property (nonatomic, strong)NSMutableArray *healthyDataArr;

@property (nonatomic, assign)NSString *channelName;

@property (nonatomic, assign)NSString *channelID;

- (void)refreshHealthyView;







@end
