//
//  HotHealthyChannelTableCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HotHealthyChannelTableCell.h"

#define btnWidth 74*kDeviceRate//74*kDeviceRate//64*kRateSize
#define btnHeight 50*kDeviceRate//50*kDeviceRate//42.5*kRateSize
#define posterHeight 40*kDeviceRate//40*kDeviceRate//32.5*kRateSize

@interface HotHealthyChannelTableCell ()
{
    UIButton_Block *oneBtn;
    UIButton_Block *twoBtn;
    UIButton_Block *threeBtn;
    UIButton_Block *fourBtn;
    UIButton_Block *fiveBtn;
    
    UIImageView *posterOne;
    UIImageView *posterTwo;
    UIImageView *posterThree;
    UIImageView *posterFour;
    UIImageView *posterFive;
    
    UIImageView *oneImg;
    UIImageView *twoImg;
    UIImageView *threeImg;
    UIImageView *fourImg;
    UIImageView *fiveImg;
}
@end

@implementation HotHealthyChannelTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.healthyDataArr = [[NSMutableArray alloc] initWithCapacity:0];
//        [self setUpView];
        
        [self makeUI];
    }
    
    return self;
}


//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withArray:(NSMutableArray *)dataArr
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        self.healthyDataArr = [[NSMutableArray alloc] initWithCapacity:0];
//        if (dataArr.count) {
//            self.healthyDataArr = dataArr;
//        }
//        
//        [self setUpView];
//    }
//    
//    return self;
//}

- (void)makeUI
{
    WS(wself);
    
    UIView *superView = self.contentView;
    
    oneBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    twoBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    threeBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    fourBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    fiveBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
    
    oneBtn.adjustsImageWhenHighlighted = NO;
    twoBtn.adjustsImageWhenHighlighted = NO;
    threeBtn.adjustsImageWhenHighlighted = NO;
    fourBtn.adjustsImageWhenHighlighted = NO;
    fiveBtn.adjustsImageWhenHighlighted = NO;

    
    posterOne = [UIImageView new];
    posterTwo = [UIImageView new];
    posterThree = [UIImageView new];
    posterFour = [UIImageView new];
    posterFive = [UIImageView new];
    
    oneImg = [[UIImageView alloc] init];
    twoImg = [[UIImageView alloc] init];
    threeImg = [[UIImageView alloc] init];
    fourImg = [[UIImageView alloc] init];
    fiveImg = [[UIImageView alloc] init];
    
    [superView addSubview:oneBtn];
    [superView addSubview:twoBtn];
    [superView addSubview:threeBtn];
    [superView addSubview:fourBtn];
    [superView addSubview:fiveBtn];
    
    [superView addSubview:posterOne];
    [superView addSubview:posterTwo];
    [superView addSubview:posterThree];
    [superView addSubview:posterFour];
    [superView addSubview:posterFive];
    
    [superView addSubview:oneImg];
    [superView addSubview:twoImg];
    [superView addSubview:threeImg];
    [superView addSubview:fourImg];
    [superView addSubview:fiveImg];
    
    
    [posterOne mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(superView.mas_left);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [oneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(superView.mas_left);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [oneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(posterOne.mas_right);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(posterOne.mas_height);
        make.centerY.mas_equalTo(posterOne.mas_centerY);
    }];
    
    
    
    [posterTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(oneImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [twoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(oneImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];

    [twoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(posterTwo.mas_right);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(posterTwo.mas_height);
        make.centerY.mas_equalTo(posterTwo.mas_centerY);
    }];
    
    
    
    [posterThree mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(twoImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [threeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(twoImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    
    
    [threeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(posterThree.mas_right);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(posterThree.mas_height);
        make.centerY.mas_equalTo(posterThree.mas_centerY);
    }];
    
    
    
    [posterFour mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(threeImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [fourBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(threeImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];

    [fourImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(posterFour.mas_right);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(posterFour.mas_height);
        make.centerY.mas_equalTo(posterFour.mas_centerY);
    }];
    
    
    
    [posterFive mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(posterHeight);
        make.left.mas_equalTo(fourImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];
    
    [fiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(btnHeight);
        make.left.mas_equalTo(fourImg.mas_right);
        make.top.mas_equalTo(superView.mas_top);
    }];

    [fiveImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(posterFive.mas_right);
        make.width.mas_equalTo(0.5*kDeviceRate);
        make.height.mas_equalTo(posterFive.mas_height);
        make.centerY.mas_equalTo(posterFive.mas_centerY);
    }];

    
    [oneImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [twoImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [threeImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [fourImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    [fiveImg setImage:[UIImage imageNamed:@"home_cell_parting_line"]];
    
    oneBtn.Click = ^(UIButton_Block *btn, NSString *name)
    {
        NSLog(@"tttttttt---%@",btn.polyMedicDic);
        
        if (wself.healthyBlock) {
            wself.healthyBlock(btn);
        }
        
        NSLog(@"111111");
    };
    
    twoBtn.Click = ^(UIButton_Block *btn, NSString *name)
    {
        if (wself.healthyBlock) {
            wself.healthyBlock(btn);
        }
        
        NSLog(@"222222");
    };
    
    threeBtn.Click = ^(UIButton_Block *btn, NSString *name)
    {
        if (wself.healthyBlock) {
            wself.healthyBlock(btn);
        }
        NSLog(@"333333");
    };
    
    fourBtn.Click = ^(UIButton_Block *btn, NSString *name)
    {
        if (wself.healthyBlock) {
            wself.healthyBlock(btn);
        }
        
        NSLog(@"444444");
    };
    
    fiveBtn.Click = ^(UIButton_Block *btn, NSString *name)
    {
        if (wself.healthyBlock) {
            wself.healthyBlock(btn);
        }
        
        NSLog(@"555555");
    };
}

- (void)setUpView
{
    WS(wself);
    
    UIView *superView = self.contentView;
    
    UIButton *tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImageView *tempLine = [[UIImageView alloc] init];
    
    for (int i = 0; i<self.healthyDataArr.count; i++) {
        
//        self.channelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
//        self.channelBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
        
        self.channelBtn = [UIButton_Block new];
        
        self.partingLine = [[UIImageView alloc] init];
        
        [superView addSubview:self.channelBtn];
        [superView addSubview:self.partingLine];
        
//        tempBtn = self.channelBtn;
//        tempLine = self.partingLine;
        
        [self.channelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(btnWidth);
            make.height.mas_equalTo(btnHeight);
            make.left.mas_equalTo(i==0?superView.mas_left:wself.partingLine.mas_right);
            make.top.mas_equalTo(superView.mas_top);
        }];
        
        
        if (i<self.healthyDataArr.count-1) {
            
            [self.partingLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0.5*kDeviceRate);
                make.height.mas_equalTo(wself.channelBtn.mas_height);
                make.left.mas_equalTo(wself.channelBtn.mas_right);
                make.top.mas_equalTo(wself.channelBtn.mas_top);
            }];
        }
        
        tempBtn = self.channelBtn;
        tempLine = self.partingLine;
        
        [self.channelBtn setBackgroundImage:[UIImage imageNamed:self.healthyDataArr[i]] forState:UIControlStateNormal];
        [self.channelBtn setImage:[UIImage imageNamed:self.healthyDataArr[i]] forState:UIControlStateNormal];
        [self.channelBtn setImage:[UIImage imageNamed:self.healthyDataArr[i]] forState:UIControlStateHighlighted];
        
        self.channelBtn.tag = i+444;
        
//        self.channelBtn.eventName = [[self.healthyDataArr objectAtIndex:i] objectForKey:@"hahahaha"];
//        self.channelBtn.channelID = [[self.healthyDataArr objectAtIndex:i] objectForKey:@""];
        self.channelBtn.Click = ^(UIButton_Block *btn, NSString *name){
            if (wself.healthyBlock) {
                wself.healthyBlock(btn);
            }
        };
        
//        [self.channelBtn addTarget:self action:@selector(healthyChannelButtonClickDown:) forControlEvents:UIControlEventTouchUpInside];
        [self.partingLine setImage:[UIImage imageNamed:@""]];
        [self.partingLine setBackgroundColor:[UIColor blackColor]];
        
    }
}

//- (void)healthyChannelButtonClickDown:(UIButton *)btn
//{
//    UIButton *tempBtn = [self.contentView viewWithTag:btn.tag];
//    if (self.healthyBlock) {
//        self.healthyBlock(tempBtn, self.channelName, self.channelID);
//    }
//}

- (void)refreshHealthyView
{
    if (self.healthyDataArr.count>=1) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.healthyDataArr[0];

        if (self.healthyDataArr.count == 1) {
            oneImg.hidden = YES;
            twoImg.hidden = YES;
            threeImg.hidden = YES;
            fourImg.hidden = YES;
            fiveImg.hidden = YES;
            
            
            twoBtn.hidden = YES;
            threeBtn.hidden = YES;
            fourBtn.hidden = YES;
            fiveBtn.hidden = YES;
            
        }else{
            oneImg.hidden = NO;
            
            
            twoBtn.hidden = NO;
            threeBtn.hidden = NO;
            fourBtn.hidden = NO;
            fiveBtn.hidden = NO;
        }

        [self setValueForPosterImg:posterOne withBtn:oneBtn withDic:dic];
    }
    
    if (self.healthyDataArr.count>=2) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.healthyDataArr[1];
        
        if (self.healthyDataArr.count == 2) {
            twoImg.hidden = YES;
            threeImg.hidden = YES;
            fourImg.hidden = YES;
            fiveImg.hidden = YES;
            
            
            threeBtn.hidden = YES;
            fourBtn.hidden = YES;
            fiveBtn.hidden = YES;
        }else{
            twoImg.hidden = NO;
            
            threeBtn.hidden = NO;
            fourBtn.hidden = NO;
            fiveBtn.hidden = NO;
        }
        
        [self setValueForPosterImg:posterTwo withBtn:twoBtn withDic:dic];
    }
    
    if (self.healthyDataArr.count>=3) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.healthyDataArr[2];

        if (self.healthyDataArr.count == 3) {
            threeImg.hidden = YES;
            fourImg.hidden = YES;
            fiveImg.hidden = YES;
            
            
            fourBtn.hidden = YES;
            fiveBtn.hidden = YES;
        }else{
            threeImg.hidden = NO;
            
            fourBtn.hidden = NO;
            fiveBtn.hidden = NO;
        }
        
        [self setValueForPosterImg:posterThree withBtn:threeBtn withDic:dic];
    }
    
    if (self.healthyDataArr.count>=4) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.healthyDataArr[3];

        if (self.healthyDataArr.count == 4) {
            fourImg.hidden = YES;
            fiveImg.hidden = YES;
            
            
            fiveBtn.hidden = YES;
        }else{
            fourImg.hidden = NO;
            
            fiveBtn.hidden = NO;
        }
        
        [self setValueForPosterImg:posterFour withBtn:fourBtn withDic:dic];
    }
    
    if (self.healthyDataArr.count>=5) {
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.healthyDataArr[4];

        if (self.healthyDataArr.count == 5) {
            fiveImg.hidden = YES;
        }else{
            fiveImg.hidden = NO;
        }
        
        [self setValueForPosterImg:posterFive withBtn:fiveBtn withDic:dic];
    }
}

- (void)setValueForPosterImg:(UIImageView *)posterImg withBtn:(UIButton_Block *)btn withDic:(NSDictionary *)dic
{
    if (dic.count) {
        
        [btn setImage:[UIImage imageNamed:@"hot_healthy_img_pressed"] forState:UIControlStateHighlighted];
        
        if ([[dic objectForKey:@"imageUrl"] length]) {
            
            [posterImg sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([dic objectForKey:@"imageUrl"])] placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"]];
        }
        
        btn.polyMedicDic = [NSMutableDictionary dictionaryWithDictionary:dic];
    }
}


- (void)setValueForButton:(UIButton_Block *)btn withDic:(NSDictionary *)dic
{
    [btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [btn setImage:[UIImage imageNamed:[dic objectForKey:@"postersUrl"]] forState:UIControlStateNormal];
    
    [btn.imageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([dic objectForKey:@"imageUrl"])] placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"]];
    
    [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    
    btn.eventName = [dic objectForKey:@"name"];
    btn.channelID = [dic objectForKey:@""];
    btn.polyMedicDic = dic;
}

@end
