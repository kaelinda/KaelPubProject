//
//  HospitalTableViewCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void(^HospitalTableViewCellBlock)(UIButton *btn, NSString *name, NSString *hospitalID, NSString *areaID);


@interface HospitalTableViewCell : UITableViewCell

@property (nonatomic, strong)UIButton *senderBtn;

@property (nonatomic, strong)UILabel *hospitalLab;

@property (nonatomic, strong)UIImageView *partingLineImg;

@property (nonatomic,copy)HospitalTableViewCellBlock hospitalBlock;

@property (nonatomic, assign)NSString *name;

@property (nonatomic, assign)NSString *hospitalID;

@property (nonatomic, assign)NSString *areaID;

- (void)refreshHospitalCellWithDic:(NSDictionary *)dic;

@end
