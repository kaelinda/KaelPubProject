//
//  HospitalCenter.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/6/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HospitalCenter : NSObject

@property (nonatomic, strong)NSMutableArray *hospitalArr;

@property (nonatomic, strong)NSMutableArray *cityArr;


+ (id)defaultCenter;

//存储医院列表
- (void)pushAreaHospitalListInCenterWithAreaID:(NSString *)areaID andAreaHospitalListArr:(NSArray *)hospitalArr;

//根据areaID获取区域医院列表
- (NSArray *)getHospitalListWithAreaID:(NSString *)areaID;

//清除本地医院列表
- (void)cleanHospitalCenter;


- (NSArray *)getAreaCityListArr;

- (void)saveAreaCityWith:(NSArray *)cityArr;


@end
