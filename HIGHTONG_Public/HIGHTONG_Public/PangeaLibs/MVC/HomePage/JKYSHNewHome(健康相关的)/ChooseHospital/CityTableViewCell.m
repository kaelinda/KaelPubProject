//
//  CityTableViewCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "CityTableViewCell.h"

#define cellWidth 105*kRateSize
#define cellHeight 40*kRateSize


@implementation CityTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    UIView *superView = self.contentView;
    
    _cityLab = [[UILabel alloc] init];
    [superView addSubview:_cityLab];
    [_cityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(superView.mas_right);
        make.height.mas_equalTo(cellHeight);
        make.left.mas_equalTo(superView.mas_left);//.offset(35*kRateSize);
        make.top.mas_equalTo(superView.mas_top);
    }];
    _cityLab.font = App_font(15);
    _cityLab.textAlignment = NSTextAlignmentCenter;
    _cityLab.textColor = UIColorFromRGB(0x666666);
    self.backgroundColor = [UIColor purpleColor];
    
//    _cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [superView addSubview:_cityBtn];
//    [_cityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(cellWidth);
//        make.height.mas_equalTo(cellHeight);
//        make.left.mas_equalTo(superView.mas_left);
//        make.top.mas_equalTo(superView.mas_top);
//    }];
//    [_cityBtn addTarget:self action:@selector(cityBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
//    _cityBtn.selected = !_cityBtn.selected;
}

- (void)cityBtnSenderDown:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected) {
        
//        btn.selected = !btn.selected;
        _cityLab.textColor = UIColorFromRGB(0x666666);
        self.backgroundColor = UIColorFromRGB(0xffffff);
    }else{
        
//        btn.selected = !btn.selected;
        _cityLab.textColor = UIColorFromRGB(0x3ab7f5);
        self.backgroundColor = [UIColor purpleColor];//UIColorFromRGB(0x333333);
    }
    

    if (self.cityBlock) {
        self.cityBlock(btn, _btnTitle, _cityID);
    }
}

- (void)refreshCityCellWithDic:(NSDictionary *)dic
{
    if (dic.count) {
        
        _btnTitle = [dic objectForKey:@"name"];
        _cityID = [dic objectForKey:@"id"];
        _cityLab.text = _btnTitle;
    }
}

@end
