//
//  ChooseHospitalViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

typedef void(^ChooseHospitalViewControllerBlock)();

@interface ChooseHospitalViewController : HT_FatherViewController

@property (nonatomic, strong)UITableView *cityTable;
@property (nonatomic, strong)UITableView *hospitalTable;

@property (nonatomic, strong)ChooseHospitalViewControllerBlock refreshHome;


@end
