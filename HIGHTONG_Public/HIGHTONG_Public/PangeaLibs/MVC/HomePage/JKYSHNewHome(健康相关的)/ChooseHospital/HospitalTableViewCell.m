//
//  HospitalTableViewCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HospitalTableViewCell.h"


#define cellWidth 215*kRateSize
#define cellHeight 40*kRateSize

@implementation HospitalTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    UIView *superView = self.contentView;
    WS(wself);
    
    _hospitalLab = [[UILabel alloc] init];
    [superView addSubview:_hospitalLab];
    _hospitalLab.font = App_font(15);
    _hospitalLab.textAlignment = NSTextAlignmentLeft;
    [_hospitalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(superView.mas_right);
        make.height.mas_equalTo(cellHeight);
        make.left.mas_equalTo(superView.mas_left).offset(15*kRateSize);
        make.top.mas_equalTo(superView.mas_top);
    }];

    
    _partingLineImg = [[UIImageView alloc] init];
    [superView addSubview:_partingLineImg];
    [_partingLineImg setImage:[UIImage imageNamed:@""]];
    _partingLineImg.backgroundColor = App_line_color;
    [_partingLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.hospitalLab.mas_bottom);
        make.height.mas_equalTo(0.5*kRateSize);
        make.width.mas_equalTo(superView.mas_width);
        make.centerX.mas_equalTo(wself.hospitalLab.centerX);
    }];
    

}

- (void)hospitalBtnSenderDown:(UIButton *)btn
{
    if (self.hospitalBlock) {
        self.hospitalBlock(btn, self.name, self.hospitalID, self.areaID);
    }
}

- (void)refreshHospitalCellWithDic:(NSDictionary *)dic
{
    if (dic.count) {
        
        self.name = [dic objectForKey:@"name"];
        _hospitalLab.text = self.name;
        
        self.hospitalID = [dic objectForKey:@"instanceCode"];
        
        self.areaID = [dic objectForKey:@"areaID"];
    }
}

@end
