//
//  CityTableViewCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UIButton_Block.h"

typedef void(^CityTableViewCellBlock)(UIButton *btn, NSString *btnTitle, NSString *cityID);

@interface CityTableViewCell : UITableViewCell

@property (nonatomic, copy)CityTableViewCellBlock cityBlock;

@property (nonatomic, strong)UILabel *cityLab;

@property (nonatomic, strong)UIButton *cityBtn;

@property (nonatomic, assign)NSString *btnTitle;

@property (nonatomic, assign)NSString *cityID;

- (void)refreshCityCellWithDic:(NSDictionary *)dic;

@end
