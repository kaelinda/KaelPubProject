//
//  HospitalCenter.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/6/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HospitalCenter.h"

static HospitalCenter *center = nil;

@implementation HospitalCenter

+ (id)defaultCenter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        center = [[[self class] alloc] init];
    });
    
    return center;
}


- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
        _hospitalArr = [[NSMutableArray alloc] init];
        
        _cityArr = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)pushAreaHospitalListInCenterWithAreaID:(NSString *)areaID andAreaHospitalListArr:(NSArray *)hospitalArr
{
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
    
//    if (_hospitalArr.count>0) {
//        
//        [_hospitalArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * stop) {
//            
//            if ([[dic objectForKey:@"areaId"] isEqualToString:areaID]) {
//                
//                return ;
//                
//                *stop = YES;
//                
//                if (*stop == YES) {
//                    [_hospitalArr removeObjectAtIndex:idx];
//                }
//            }
//            
//        }];
//        
//    }else{
    
#warning 17.12.14 wuwei
//        [tempDic setObject:areaID forKey:@"areaId"];
//        [tempDic setObject:hospitalArr forKey:@"list"];
    
//    }

    if (hospitalArr.count) {
        [tempDic setObject:hospitalArr forKey:@"list"];
    }
    if (areaID.length) {
        [tempDic setObject:areaID forKey:@"areaId"];
    }
    
    [_hospitalArr addObject:tempDic];
    
    NSLog(@"添加进来的个体字典\n%@",tempDic);
    NSLog(@"添加后组成的新数组\n%@",_hospitalArr);
    NSLog(@"本地医院数组的长度\n%lu",(unsigned long)_hospitalArr.count);
}

- (NSArray *)getHospitalListWithAreaID:(NSString *)areaID
{
    NSArray *resultArr;
    
    for (int i = 0; i < _hospitalArr.count; i++) {
        
        if ([areaID isEqualToString:[[_hospitalArr objectAtIndex:i] objectForKey:@"areaId"]]) {
            
//            [resultArr addObject:[[_hospitalArr objectAtIndex:i] objectForKey:@"list"]];
            
            resultArr = [[_hospitalArr objectAtIndex:i] objectForKey:@"list"];
        }
    };
    
    return resultArr;
    
    NSLog(@"根据ID取得的数组/n%@",resultArr);
}

- (void)saveAreaCityWith:(NSArray *)cityArr
{
    [_cityArr removeAllObjects];
    
    _cityArr = [NSMutableArray arrayWithArray:cityArr];
}

- (NSArray *)getAreaCityListArr
{
    return [NSArray arrayWithArray:_cityArr];
}

- (void)cleanHospitalCenter
{
    [_hospitalArr removeAllObjects];
    
    [_cityArr removeAllObjects];
}

@end
