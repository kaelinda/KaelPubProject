//
//  ChooseHospitalViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/9/6.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "ChooseHospitalViewController.h"
#import "CityTableViewCell.h"
#import "HospitalTableViewCell.h"
#import "BMKSingleTon.h"
#import "HospitalCenter.h"

#define cellHeight 40*kRateSize
#define cityTableWidth 105*kRateSize
#define hospitalTableWidth 215*kRateSize


@interface ChooseHospitalViewController ()<UITableViewDelegate,UITableViewDataSource,HTRequestDelegate>
{
    UIButton *_backBtn;
    UIView *_headView;
    UILabel *_currentHospitalLab;
    
    NSString *_currentHospitalName;
    NSString *_currentHospitalID;
    NSString *_areaID;
    
    BOOL _isFirstCity;
}
@property (nonatomic, strong)NSMutableArray *cityArr;//城市列表
@property (nonatomic, strong)NSMutableArray *hospitalArr;//单个区域医院列表
@property (nonatomic, strong)NSMutableArray *hospitalTotalArr;//全部医院列表

@end

@implementation ChooseHospitalViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadData];
    [self getLocationInformationAndReloadTable];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cityArr = [[NSMutableArray alloc] initWithCapacity:0];
    self.hospitalArr = [[NSMutableArray alloc] initWithCapacity:0];
    self.hospitalTotalArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    [self setUpView];
    
//    [HospitalCenter defaultCenter];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLocationInformationAndReloadTable) name:@"RefreshHome" object:nil];
}

- (void)setUpView
{
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"请选择医院"];
    
    [self setNaviBarRightBtn:nil];
    
    self.view.backgroundColor = UIColorFromRGB(0xffffff);//HT_COLOR_BACKGROUND_APP;
    
    WS(wself);
    _headView = [[UIView alloc] init];
    [self.view addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kDeviceWidth);
        make.height.mas_equalTo(cellHeight);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(wself.view.mas_top).offset(64+kTopSlimSafeSpace);
    }];
    
    _headView.backgroundColor = App_white_color;
    
    _currentHospitalLab = [[UILabel alloc] init];
    [_headView addSubview:_currentHospitalLab];
    [_currentHospitalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_headView.mas_left).offset(35*kRateSize);
        make.right.mas_equalTo(_headView.mas_right);
        make.top.mas_equalTo(_headView.mas_top);
        make.bottom.mas_equalTo(_headView.mas_bottom).offset(-1*kRateSize);
    }];
    _currentHospitalLab.textAlignment = NSTextAlignmentLeft;
    _currentHospitalLab.textColor = UIColorFromRGB(0x666666);
    _currentHospitalLab.font = App_font(13);
    _currentHospitalLab.text = [NSString stringWithFormat:@"当前选择%@",_currentHospitalName];
    
    UIImageView *honLine = [[UIImageView alloc] init];
    [_headView addSubview:honLine];
    honLine.backgroundColor = App_line_color;
    [honLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(_headView);
        make.top.mas_equalTo(_currentHospitalLab.mas_bottom);
        make.bottom.mas_equalTo(_headView.mas_bottom);
    }];
    
    
    _cityTable = [self customInitTableView];
    [_cityTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(cityTableWidth);
        make.left.mas_equalTo(wself.view.mas_left);
        make.top.mas_equalTo(_headView.mas_bottom);
        make.bottom.mas_equalTo(wself.view.mas_bottom).offset(-kBottomSafeSpace);
    }];
    
    _cityTable.backgroundColor = UIColorFromRGB(0xf7f7f7);
    
    _hospitalTable = [self customInitTableView];
    [_hospitalTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(hospitalTableWidth);
        make.left.mas_equalTo(wself.cityTable.mas_right);
        make.top.mas_equalTo(wself.cityTable.mas_top);
        make.bottom.mas_equalTo(wself.view.mas_bottom).offset(-kBottomSafeSpace);
    }];
    _hospitalTable.backgroundColor = UIColorFromRGB(0xffffff);
    
}

- (void)getLocationInformationAndReloadTable
{
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[[BMKSingleTon shareInstance] getLocationCityAndHospital]];
    if (dic.count) {
        
        _currentHospitalName = [dic objectForKey:@"hospitalName"];
        _currentHospitalID = [dic objectForKey:@"InstanceCode"];
        _areaID = [dic objectForKey:@"areaID"];
        _currentHospitalLab.text = [NSString stringWithFormat:@"当前选择%@",_currentHospitalName];
        
        [_cityTable reloadData];
        
        //请求相应城市对应医院
//        HTRequest *hospitalRequest = [[HTRequest alloc] initWithDelegate:self];
//        [hospitalRequest YJIntroduceListWithAreaId:_areaID withPageNo:@"1" withPageSize:@"100"];
        
        [self getDataListWithAreaID:_areaID];
        
    }else{//定位失败且无历史记录，列表展示默认城市和医院（第一个城市的第一个医院）
        _currentHospitalID = nil;
        _areaID = nil;
        _currentHospitalName = @"定位失败，请手动选择";
        _currentHospitalLab.text = _currentHospitalName;
    }
}

- (void)getDataListWithAreaID:(NSString *)areaID
{
    if (areaID.length>0) {
        NSArray *tempArr = [[HospitalCenter defaultCenter] getHospitalListWithAreaID:areaID];
        
        if (tempArr.count>0) {
            
            _hospitalArr = [NSMutableArray arrayWithArray:tempArr];
            
            if (_hospitalArr.count>0) {
                
                if ([_currentHospitalName isEqualToString:@"定位失败，请手动选择"] && !_currentHospitalID.length && _isFirstCity) {
                    
                    _currentHospitalID = [[_hospitalArr objectAtIndex:0] objectForKey:@"instanceCode"];
                }
            }
            
            _isFirstCity = NO;
            
            [_hospitalTable reloadData];

        }else{
            
            //请求相应城市对应医院
            HTRequest *hospitalRequest = [[HTRequest alloc] initWithDelegate:self];
            [hospitalRequest YJIntroduceListWithAreaId:_areaID withPageNo:@"1" withPageSize:@"100"];
        }
    }else{
        return;
    }
}

- (void)loadData
{
    //请求城市列表
//    HTRequest *cityRequest = [[HTRequest alloc] initWithDelegate:self];
//    [cityRequest YJIntroduceAreaList];
    
    
    NSArray *tempArr = [[HospitalCenter defaultCenter] getAreaCityListArr];
    if (tempArr.count>0) {
        
        _cityArr = [NSMutableArray arrayWithArray:tempArr];
        
        NSLog(@"单例中的城市列表\n%@",_cityArr);
       
        if (!_areaID.length) {
            
            _isFirstCity = YES;
            
            _areaID = [[_cityArr objectAtIndex:0] objectForKey:@"id"];
            
            [self getDataListWithAreaID:_areaID];
        }
        
        [_cityTable reloadData];
    }else{
        HTRequest *cityRequest = [[HTRequest alloc] initWithDelegate:self];
        [cityRequest YJIntroduceAreaList];
    }
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:YJ_INTRODUCE_AREALIST]) {//请求城市列表
        NSLog(@"城市列表---返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"])
        {
            _cityArr = [NSMutableArray arrayWithArray:[result objectForKey:@"list"]];
            if (_cityArr.count == 0) {
                return;
            }
            
            [[HospitalCenter defaultCenter] saveAreaCityWith:[result objectForKey:@"list"]];
            
            if (!_areaID.length) {
                
                _isFirstCity = YES;
                
                _areaID = [[_cityArr objectAtIndex:0] objectForKey:@"id"];
                
                //请求相应城市对应医院
//                HTRequest *hospitalRequest = [[HTRequest alloc] initWithDelegate:self];
//                [hospitalRequest YJIntroduceListWithAreaId:_areaID withPageNo:@"1" withPageSize:@"100"];
                [self getDataListWithAreaID:_areaID];
            }
            
            [_cityTable reloadData];
        }
    }
    
    if ([type isEqualToString:YJ_INTRODUCE_LIST]) {//请求医院
        NSLog(@"医院列表---返回数据: %ld result:%@  type:%@",status,result,type );
        
        NSDictionary *resDIC = result;

        _hospitalArr = [NSMutableArray arrayWithArray:[resDIC objectForKey:@"list"]];
        
        [[HospitalCenter defaultCenter] pushAreaHospitalListInCenterWithAreaID:_areaID andAreaHospitalListArr:[resDIC objectForKey:@"list"]];
        
        if (_hospitalArr.count>0) {
            
            if ([_currentHospitalName isEqualToString:@"定位失败，请手动选择"] && !_currentHospitalID.length && _isFirstCity) {
                
                _currentHospitalID = [[_hospitalArr objectAtIndex:0] objectForKey:@"instanceCode"];
            }
        }
        
        _isFirstCity = NO;
        
        [_hospitalTable reloadData];
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _cityTable) {

        static NSString *ident = @"ident";
        
        CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
        if (!cell) {
            cell = [[CityTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ident];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        
        [cell refreshCityCellWithDic:[_cityArr objectAtIndex:indexPath.row]];
        
        if (_cityArr.count > indexPath.row) {
            
            if ([_areaID isEqualToString:[[_cityArr objectAtIndex:indexPath.row] objectForKey:@"id"]]) {
                
                cell.cityLab.textColor = UIColorFromRGB(0x3ab7f5);
                
                cell.backgroundColor = UIColorFromRGB(0xffffff);
            }else{
                cell.cityLab.textColor = UIColorFromRGB(0x666666);
                
                cell.backgroundColor = UIColorFromRGB(0xf7f7f7);
            }
        }

        return cell;
    }else{
        
        static NSString *hosident = @"hosident";
        
        HospitalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:hosident];
        if (!cell) {
            cell = [[HospitalTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:hosident];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
                
//        [cell refreshHospitalCellWithDic:[_hospitalArr objectAtIndex:indexPath.row]];
        
        if (_hospitalArr.count > indexPath.row) {
            
            [cell refreshHospitalCellWithDic:[_hospitalArr objectAtIndex:indexPath.row]];
            
            if ([_currentHospitalID isEqualToString:[[_hospitalArr objectAtIndex:indexPath.row] objectForKey:@"instanceCode"]]) {
                
                cell.hospitalLab.textColor = UIColorFromRGB(0x3ab7f5);
            }else
            {
                cell.hospitalLab.textColor = UIColorFromRGB(0x666666);
            }
        }
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _cityTable) {
        NSLog(@"城市点你点你点你");
        
        _areaID = [[_cityArr objectAtIndex:indexPath.row] objectForKey:@"id"];
        [_cityTable reloadData];
        
//        HTRequest *hospitalRequest = [[HTRequest alloc] initWithDelegate:self];
//        [hospitalRequest YJIntroduceListWithAreaId:_areaID withPageNo:@"1" withPageSize:@"100"];
        
        [self getDataListWithAreaID:_areaID];
    }else{
    
        NSLog(@"医院点点点点");
        
        HospitalTableViewCell *cell = (HospitalTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if (cell.selected) {
            cell.backgroundColor = UIColorFromRGB(0xf7f7f7);
        }else
        {
            cell.backgroundColor = UIColorFromRGB(0xffffff);
            
        }
        
        NSDictionary *itemDic = [_hospitalArr objectAtIndex:indexPath.row];
        NSString *code = [itemDic objectForKey:@"instanceCode"];
        NSString *name = [itemDic objectForKey:@"name"];
        NSString *areaId = [itemDic objectForKey:@"areaId"];
        [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"InstanceCode"];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"hospitalName"];
        [[NSUserDefaults standardUserDefaults] setObject:areaId forKey:@"areaID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _currentHospitalID = [code length]?code:@"";
        _currentHospitalName = [name length]?name:@"";
        
        if (_refreshHome) {
            _refreshHome();
        }

        _currentHospitalLab.text = [NSString stringWithFormat:@"当前选择%@",_currentHospitalName];
        [_hospitalTable reloadData];
        
        [self goBack];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _cityTable) {
        
        return cellHeight;
    }
    return cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _cityTable) {
        
        return _cityArr.count;
    }
    
    return _hospitalArr.count;
}

- (NSMutableArray *)getAreaHospitalFromTotalHospitalListWithAreaID:(NSString *)areaID
{
    NSMutableArray *hospitalArr = [NSMutableArray array];
    
    for (int i = 0; i<_hospitalTotalArr.count; i++) {
        
        NSDictionary *tempDic = [_hospitalTotalArr objectAtIndex:i];
        
        if ([areaID isEqualToString:[tempDic objectForKey:@"id"]]) {
            
            [hospitalArr addObject:tempDic];
        }
    }
    
    return hospitalArr;
}

- (UITableView *)customInitTableView
{
    UITableView *tableview = [[UITableView alloc] init];
    
    [self.view addSubview:tableview];
    
    tableview.delegate = self;
    
    tableview.dataSource = self;
    
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableview.showsVerticalScrollIndicator = NO;
    
    tableview.showsHorizontalScrollIndicator = NO;
    
    return tableview;
}

- (void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshHome" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
