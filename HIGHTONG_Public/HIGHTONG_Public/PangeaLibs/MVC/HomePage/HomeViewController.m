//
//  HomeViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HomeViewController.h"
#import "CustomAlertView.h"
#import "UserCollectCenter.h"
//#import "LiveViewControl.h"
#import "DetailViewcontrol.h"
#import "PosterViewofTB.h"
#import "UIButton_Block.h"
#import "DataPlist.h"
#import "BufferLivewController.h"

#import "HTRequest.h"
#import "ProgramListCell.h"

#import "DemandVideoPlayerVC.h"


#import "SearchViewController.h"

#import "DemandSecondViewcontrol.h"

#import "MineCollectViewController.h"
#import "DemandADDetailViewController.h"

//首页上面的 AD  播放记录 放在tableview的headerview上/

//#import "SearchViewController.h"  //搜索页面

#import "LiveTuiJianCell.h"

//#import "FavoriteChannelCell.h"

#import "MineRecorderViewController.h"

#import "DataPlist.h"

#import "LiveTuiJianViewCell.h"


#import "MBProgressHUD.h"


#import "FavoriteChannelViewCell.h"

#import "EpgSmallVideoPlayerVC.h"

#import "Tool.h"


#import "NoFavoriteChannelViewCell.h"
#import "HospitalViewController.h"
#import "DemandWIFIViewController.h"
#import "KnowledgeBaseViewController.h"

#define HaveTuijian  2


#define TVCable_CompanyInfo YES//有线电视 公司业务信息开关

#define WeiHospital  (TVCable_CompanyInfo==YES ? 70 : 44)


//#define URL_company_info     @"http://101.200.148.37/shphone/pages/jxnet/mp/index.html"
//#define URL_operation_intro  @"http://101.200.148.37/shphone/pages/jxnet/bi/index.html"
//#define URL_busyness_net     @"http://101.200.148.37/shphone/pages/jxnet/bnq/index.html"
//#define URL_operation_manage @"http://101.200.148.37/shphone/pages/jxnet/faq/index.html"

#define URL_company_info     [NSString stringWithFormat:@"%@/shphone/pages/jxnet/mp/index.html",YJ_Shphone]
#define URL_operation_intro  [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html",YJ_Shphone]
#define URL_busyness_net     [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bnq/index.html",YJ_Shphone]
#define URL_operation_manage [NSString stringWithFormat:@"%@/shphone/pages/jxnet/faq/index.html",YJ_Shphone]
//这些链接是需要拼接一下的



#define Name_company_info     @"公司概况"
#define Name_operation_intro  @"业务介绍"
#define Name_busyness_net     @"营业网点"
#define Name_operation_manage @"常见问题"





#import "HospitalViewController.h"
#import "ComPanyOperationView.h"
@interface HomeViewController ()<CustomAlertViewDelegate,HTRequestDelegate,MBProgressHUDDelegate>//HTTabBarDelegate,暂时没加入
{
    NSInteger  _reqCount;//请求的个数 综艺  6：
    BOOL _reqFinish;//请求完成
    NSDate *_requsetDate;//请求的时间
    
    BOOL diyici;
    
    BOOL  _first;
    
    MBProgressHUD *_hub;//加载等待
    
    UIView *_Headview; // tableview的头
    UIImageView *_RecordView;//播放记录
    UILabel *_seachTextView;//搜索热词
    
    BOOL havaFavoriteList;//是否有喜爱记录列表
    
    BOOL LiveHave;//有无直播
    
//    HTRequest *_requst;//请求类i
    
    NSString *_MYplaytime;//播放记录时间
    BOOL isEPGRecord;//是直播记录
    
    NSString *_channelID;//直播id
    NSString *_endDate;//回看播放结束时间
    
    NSMutableDictionary *_homeConfigDiction;//首页配置字典
    
    
    BOOL _finishRequest;//请求完成了
    
    NSString *  episodeSeq;//
    NSString *  breakPointRecord;//
    
    BOOL HaveRecord;//有播放记录
    
    UIView *_WeiYY;//微医院的视图
    ComPanyOperationView *_operationView;//公司业务视图 四个按钮
    
    NSDictionary *_hospitalDiction;//医院配置文件
    
    BOOL _IS_SUPPORT_VOD;//是否支持点播
   
}

@property (nonatomic,strong) UIButton *goBtn;
@property (strong, nonatomic) PosterViewofTB *  posterView;           //海报view
@property (strong ,nonatomic) UITableView    * tableView;            //首页热门节目列表
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页直播推荐  影视精选 等 数据
@property (strong,nonatomic) UILabel *XUKanLabel;         //续看标签
@property (strong,nonatomic) UIButton_Block *XUKanBtn;         //续看按钮

@property (nonatomic,strong)NSMutableArray *Livearray ;//直播推荐数组

@property (nonatomic,strong)NSMutableArray *BOOLArray  ;//数据源中的某一组数据是否显示

@end

@implementation HomeViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CANBCAKHOME"];
    //    [self makeTabBarHidden:YES];
    
    //    [self.tabBar reSetSelectedIndex:0];
//    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    
    NSLog(@"首页即将出现");
    [_tableView reloadData];
    
    [self reRequestHomeData];
    
    
    
    [MobCountTool pushInWithPageView:@"HomePage"];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"HomePage"];
}


-(void)viewDidAppear:(BOOL)animated{
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    
    [super viewDidAppear:animated];
    [self makeTabBarHidden:NO];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNaviBarTitle:@"首页"];
    [UIApplication sharedApplication].statusBarHidden = NO;
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
//    CustomAlertView *alert = [[CustomAlertView alloc] initWithMessage:@"sjfvuvfjwfvwhfbwh" andButtonNum:2 withDelegate:self];
//    [alert show];
    _finishRequest = YES;
     _homeConfigDiction = [[NSMutableDictionary alloc]init];
    HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
    _requst = [[HTRequest alloc]initWithDelegate:self];
  
//    HaveRecord = YES;
    
    diyici = YES;
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestHomeDataBA1:) name:@"loginSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestHomeDataBA) name:@"RefreshHome" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:@"GETPLAYRECORDERSUCCESS" object:nil];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:REFRESH_THE_RE_WATCH_LIST object:nil];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:@"REFRESHMINEVCTABLECELL" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadFavoriteChannel) name:@"reloadFavoriteChannel" object:nil];
    
    [self setSeachBar];
    
    _requsetDate = [NSDate date];
    _first = YES;
    
   
    
    _reqFinish = YES;//请求完成，第一次的时候要，设置请求刷新，
    
    
    
    
    /*********************************/
    //创建tableView 整体放倒tableView上
    self.dataArray  = [NSMutableArray array];
    self.BOOLArray = [NSMutableArray array];
    
    
    [self initbaseTableView];
    
    
    //adjust the UI for iOS 7
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
#endif
    
    
    
    
    // 假数据刷进去
    {
        //直播推荐
        NSMutableArray *array = [NSMutableArray arrayWithObjects:@"111",@"222",@"333", nil];
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"直播推荐",@"title",array,@"Array" ,nil];
        
        
        //喜爱频道
        NSMutableDictionary *zidian = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"http://192.168.3.23/taibiao/phone/s/CCTV-1.png",@"imageLink",@"中央",@"title", nil];
        NSMutableDictionary *zidian2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"http://192.168.3.60:8080/taibiao/phone/s/cctv_phone_s.png",@"imageLink",@"爱哪哪",@"title", nil];
        NSMutableDictionary *zidian3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"http://192.168.3.60:8080/taibiao/phone/s/31.png",@"imageLink",@"湖南",@"title", nil];
        NSMutableDictionary *zidian4 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"http://192.168.3.60:8080/taibiao/phone/s/31.png",@"imageLink",@"湖南",@"title", nil];
        NSMutableDictionary *zidian5 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"http://192.168.3.60:8080/taibiao/phone/s/31.png",@"imageLink",@"湖南",@"title", nil];
        
        NSMutableArray *array1 = [NSMutableArray arrayWithObjects:zidian,zidian2,zidian3,zidian4,zidian5, nil];
        array1.array = [NSMutableArray array];
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"喜爱频道",@"title",array1,@"Array" ,nil];
        
        
        
        
        [self.dataArray  addObject:dic1];
        [self.dataArray  addObject:dic];
        
        [self showLoading];
        
    }
    
}
- (void)reloadHEADERBA
{
    [self reloadHEADER];
}
- (void)reRequestHomeDataBA1:(NSNotification*)notif
{
    NSLog(@"登陆成功了额  啊%@",notif);
    [self reRequestHomeDataBA];
    
}
- (void)reRequestHomeDataBA
{
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    _requsetDate = [NSDate date];
    
    {
        if (self.dataArray.count >= 4) {
            NSDictionary *dic =  self.dataArray[0];
            NSDictionary *dic1 =  self.dataArray[1];
            
            
            [self.dataArray removeAllObjects];
            [self.dataArray addObject:dic];
            [self.dataArray addObject:dic1];
            
            
            
        }
    }
    
    
    NSLog(@"调用刷新");//_reqFinish&&
    if (_finishRequest&&1) {//一次请求完成后才会刷新下一次
        NSLog(@"2222222调刷新pinbi");
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _finishRequest = YES;
        });
        //*****1.请求直播推荐的数据
        HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
        [requst EPGGetHomepageHotChannel];
        if (_IS_SUPPORT_VOD) {
            //请求首页配置信息
            [requst VODHomeConfigListWithTimeOut:10];
        }
        
        
        //有无喜爱频道
        [self haveFavirateList];
        
        
        if (0) {
            //*******3请求广告页
            [requst VODGetHomepageADImage];
        }else
        {
//            [requst getHomePageSlideshowList];
            [requst HomeADSLiderPagess];
        }
        
        
        
        
        
        //*******4请求观看记录
        if (IsLogin) {
            
            
            if (_IS_SUPPORT_VOD) {
                //            [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
                [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
            }else
            {
                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
                
            }
            
            
        }else{
            [self setHighForheadIsOrNo:NO];
        }
        if(_IS_SUPPORT_VOD)
        {
            //*****5请求搜索热门字第一天
            [requst VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
        }else{
            [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
        }

        //*******6请求推荐专题的节目列表
        //            [requst VODTopicProgramListWithTopicCode:@"ZT0002" andStartSeq:1 andCount:6 andTimeout:10 andIndex:99999999];
        //            [requst VODTopicProgramListWithTopicCode:@"ZT0003" andStartSeq:1 andCount:6 andTimeout:10 andIndex:count+1];
        
        if([self RequstEnabledWithFirstTime:_requsetDate andSecondData:[NSDate date]])
        {
//            _hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            _hub.delegate = self;
            [self showLoading];
            
        }
        
        [self reloadHEADER];
        
        if (WeiHospital) {
//            [requst VODHospitalInfoWithTimeout:10];
            [requst EPGGetRegionInfoWithRegionCode:REGION_CODE andTimeout:5];
        }
        
        
    }
}
- (void)showLoading
{
    
    [self showCustomeHUD];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}
- (void)hideLoading
{
    [self hiddenCustomHUD];
    [MBProgressHUD hideAllHUDsForView:self.view  animated:YES];
}

#pragma mark -
- (void)initbaseTableView
{
    
    WS(wself);
    
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    [self.view addSubview:self.tableView];
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_background_color;
    [self.view addSubview:view];
    
    //去除自动适应scrollview
    
    
    //初始化tableView
//    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    image.frame = self.tableView.frame;
    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
    self.tableView.backgroundColor = App_background_color;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    self.view.backgroundColor = App_background_color;
//        self.tableView.backgroundColor =[UIColor yellowColor];
    //[self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
    
    
    
    //  头View  tableView的HeaderView  上  AD广告页 + 续看 View   ...
    {

        if (TVCable_CompanyInfo) {
            //初始化公司业务视图
            _operationView = [self getCompanyOperationView];
            _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital)*kRateSize)];
            [_Headview addSubview:_operationView];


        }else{
            _WeiYY = [self WeiYY];
            if (IsLogin) {
                //头View...
                
                _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital)*kRateSize)];
                [_Headview addSubview:_WeiYY];
                
            }else
            {
                _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital)*kRateSize)];
                [_Headview addSubview:_WeiYY];
                
            }
        }
        
       
        _Headview.backgroundColor = App_background_color;
        //AD广告页
        self.posterView = [[PosterViewofTB alloc]init];
        [_Headview addSubview:self.posterView];
        [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_Headview.mas_top );
            make.left.equalTo(_Headview.mas_left);
            make.right.equalTo(_Headview.mas_right);
            make.height.equalTo(@((154+kheigh)*kRateSize));
        }];
        self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
        self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];

        //续看View...   节目Label + 播放记录Button
        {
            
            _RecordView = [[UIImageView alloc]init];
//            _RecordView.backgroundColor = App_selected_color;
//            _RecordView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
            _RecordView.backgroundColor = UIColorFromRGB(0xffffff);
//            _RecordView.image = [UIImage imageNamed:@"bg_user_add_fav_channel"];
            [_Headview addSubview:_RecordView];
            
            [_RecordView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
                make.left.equalTo(_Headview.mas_left);
                make.width.equalTo(_Headview.mas_width);
                make.height.equalTo(@(40*kRateSize));
            }];
            _RecordView.userInteractionEnabled =  YES;
            
            
            if (TVCable_CompanyInfo) {
                [_operationView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_RecordView.mas_bottom ).offset(0*kRateSize);
                    make.left.equalTo(_Headview.mas_left);
                    make.width.equalTo(_Headview.mas_width);
                    make.height.equalTo(@(WeiHospital*kRateSize));
                }];
            }else{
                [_WeiYY mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_RecordView.mas_bottom ).offset(0*kRateSize);
                    make.left.equalTo(_Headview.mas_left);
                    make.width.equalTo(_Headview.mas_width);
                    make.height.equalTo(@(WeiHospital*kRateSize));
                }];
            }

            //节目Label
            self.XUKanLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5*kRateSize, 200*kRateSize, 30*kRateSize)];
            
            self.XUKanBtn = [UIButton_Block new];
            [self.XUKanLabel addSubview:self.XUKanBtn];
            self.XUKanBtn.backgroundColor = UIColorFromRGB(0xffffff);
            [self.XUKanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.XUKanLabel);
            }];
            //                            self.XUKanBtn.backgroundColor = [UIColor redColor];
            
//            self.XUKanLabel.backgroundColor = App_selected_color;
            self.XUKanLabel.text = @"续看                     ";
//            self.XUKanLabel.textColor = [UIColor whiteColor];
//            self.XUKanLabel.textColor = [UIColor colorWithRed:0.63 green:0.63 blue:0.63 alpha:1];
            
            self.XUKanLabel.userInteractionEnabled = YES;
            self.XUKanLabel.numberOfLines = 0;
            
            __weak NSString *weakchannelID = _channelID;
            self.XUKanBtn.Click = ^(UIButton_Block*btn,NSString *name)
            {
//                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
//                demandVC.contentID = btn.serviceIDName;
//                demandVC.contentTitleName = btn.eventName;
                _channelID =  btn.serviceIDName;
                
                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:wself];
                [_requst EventPlayWithEventID:weakchannelID andTimeout:10];
            };
            
            
            
            //            UIFont *font  =  [UIFont fontWithName:@"Arial" size:18];
            UIFont *font  =  [UIFont systemFontOfSize:14*kRateSize];
            self.XUKanLabel.font = font;

            //            NSLog(@"%f %f ",labelsize.width,labelsize.height);
            //            [self.XUKanLabel setFrame:CGRectMake(10*kRateSize,5*kRateSize, labelsize.width*kRateSize, 30*kRateSize)];
            [_RecordView addSubview:self.XUKanLabel];
            //播放记录Button
            UIButton_Block *btn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(FRAME_W(self.view) - 105*kRateSize, 5*kRateSize, 100*kRateSize, 30*kRateSize);
            UIImageView* inImageView = [UIImageView new];
            [btn addSubview:inImageView];
            [inImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                //            make.top.equalTo(btn.mas_top).offset(7*kRateSize);
                make.centerY.mas_equalTo(btn.mas_centerY);
                make.right.equalTo(btn.mas_right).offset(-9*kRateSize);
                //                make.bottom.equalTo(btn.mas_bottom).offset(-7*kRateSize);
                make.width.equalTo(@(6*kRateSize));
                make.height.equalTo(@(8*kRateSize));
            }];
            
            
            inImageView.image = [UIImage imageNamed:@"img_user_protocol_arrow"];//公版改进
            btn.titleLabel.font = [UIFont systemFontOfSize:14*kRateSize];
            [btn setTitle:@"播放记录" forState:UIControlStateNormal];
//            [btn setTitleColor:[UIColor colorWithRed:0.63 green:0.63 blue:0.63 alpha:1] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] forState:UIControlStateNormal];
            btn.userInteractionEnabled = YES;
            btn.serviceIDName = @"selina";
//            [btn setBackgroundColor:[UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1]];
            btn.backgroundColor = UIColorFromRGB(0xffffff);
            
            
            [btn setClick:^(UIButton_Block*btn,NSString* name){
                
                NSLog(@"%@ name:%@",[btn currentTitle],name);
                
                MineRecorderViewController *mine = [[MineRecorderViewController alloc]init];
                
                if (!isEPGRecord) {
                    
                    mine.fromHome = YES;
                }
                [self.navigationController pushViewController:mine animated:YES];

            }];
            
            
            
            [_RecordView addSubview:btn];
            UIView *recordVerLine = [UIView new];
            recordVerLine.backgroundColor = UIColorFromRGB(0xeeeeee);
            [_RecordView addSubview:recordVerLine];
            [recordVerLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_RecordView.mas_left);
                make.right.equalTo(_RecordView.mas_right);
                make.height.equalTo(@(1));
                make.bottom.equalTo(_RecordView.mas_bottom).offset(-1);
                
            }];
            
            
            
            self.tableView.tableHeaderView = _Headview;
            _RecordView.hidden= YES;
            [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
                make.left.equalTo(_Headview.mas_left);
                make.width.equalTo(_Headview.mas_width);
                make.height.equalTo(@(0*kRateSize));
            }];
            //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
            self.tableView.delaysContentTouches = YES;
            
            
        }
        
    }
    
    
    //更新广告数据
    {
        
        
        
        NSMutableArray *adArray = [NSMutableArray array];
       
        [self.posterView updateThePosterData:adArray];
    }
    
}

-(UIView *)getCompanyOperationView{
    ComPanyOperationView *operationView = [[ComPanyOperationView alloc] init];
    operationView.frame = CGRectMake(0, 0, kDeviceWidth, 90*kRateSize);
    [operationView initUI];
    operationView.actionBlock = ^(TitleButtonType actiontype){
    
        switch (actiontype) {
            case Btn_Company_Info:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                webVC.urlStr = [NSString stringWithFormat:@"%@?tsytle=%@",URL_company_info,App_Theme_Color_num];
                [self.navigationController pushViewController:webVC animated:YES];
                
//                DemandWIFIViewController *adDetail = [[DemandWIFIViewController alloc] init];
//                adDetail.isVertical = YES;
//                adDetail.pageName = Name_company_info;
//                adDetail.detailADURL = URL_company_info;
                
//                [self.navigationController pushViewController:adDetail animated:YES];
//                [self presentViewController:webVC animated:YES completion:nil];
                
                break;
            }
            case Btn_Operation_Intro:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                webVC.urlStr = [NSString stringWithFormat:@"%@?tsytle=%@",URL_operation_intro,App_Theme_Color_num];
                [self.navigationController pushViewController:webVC animated:YES];

//                DemandWIFIViewController *adDetail = [[DemandWIFIViewController alloc]init];
//                adDetail.isVertical = YES;
//                adDetail.pageName = Name_operation_intro;
//                adDetail.detailADURL = URL_operation_intro;
//                [self presentViewController:webVC animated:YES completion:nil];

                break;
            }
            case Btn_Busyness_Net:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                webVC.urlStr = [NSString stringWithFormat:@"%@?tsytle=%@",URL_busyness_net,App_Theme_Color_num];
                [self.navigationController pushViewController:webVC animated:YES];

//                DemandWIFIViewController *adDetail = [[DemandWIFIViewController alloc]init];
//                adDetail.isVertical = YES;
//                adDetail.pageName = Name_busyness_net;
//                adDetail.detailADURL = URL_busyness_net;
//                [self presentViewController:webVC animated:YES completion:nil];
                break;
            }
            case Btn_Operation_Manage:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                webVC.urlStr = [NSString stringWithFormat:@"%@?tsytle=%@",URL_operation_manage,App_Theme_Color_num];
                [self.navigationController pushViewController:webVC animated:YES];

//                DemandWIFIViewController *adDetail = [[DemandWIFIViewController alloc]init];
//                adDetail.isVertical = YES;
//                adDetail.pageName = Name_operation_manage;
//                adDetail.detailADURL = URL_operation_manage;
//                [self presentViewController:webVC animated:YES completion:nil];
                break;
            }
            default:
                break;
        }
        
    };
    return operationView;
}

- (UIView *)WeiYY{
    
    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, WeiHospital*kRateSize)];
//    view.image = [UIImage imageNamed:@"BGBGBG"];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *title = [[UIImageView alloc]initWithFrame:CGRectMake(5, 3, kDeviceWidth/2-7.5, 40*kRateSize-2)];
//    title.image = [UIImage imageNamed:@"WYY"];
    [view addSubview:title];
    
    UIButton_Block *WYY =  [[UIButton_Block alloc]initWithFrame:title.frame];
    
    UIImageView *title1 = [[UIImageView alloc]initWithFrame:CGRectMake(kDeviceWidth/2+2.5, 3, kDeviceWidth/2-7.5, 40*kRateSize-2)];
//    title1.image = [UIImage imageNamed:@"WSP"];
    [view addSubview:title1];

    UIButton_Block *WSP =  [[UIButton_Block alloc]initWithFrame:title1.frame];
    
    [view addSubview:WSP];
    [view addSubview:WYY];
    
    [WSP setImage:[UIImage imageNamed:@"ic_pm_hospital_video"] forState:UIControlStateNormal];
    [WSP setImage:[UIImage imageNamed:@"ic_pm_hospital_video_press"] forState:UIControlStateHighlighted];
    [WYY setImage:[UIImage imageNamed:@"ic_pm_micro_hospital"] forState:UIControlStateNormal];
    [WYY setImage:[UIImage imageNamed:@"ic_pm_micro_hospital_press"] forState:UIControlStateHighlighted];
    view.userInteractionEnabled = YES;
    title1.userInteractionEnabled = YES;
    title.userInteractionEnabled = YES;
    
    WYY.Click = ^(UIButton_Block*btn,NSString *name){
        NSLog(@"微医院");
        HospitalViewController *hospi = [[HospitalViewController alloc]init];
        hospi.HospitalType = HospitalTypeYiYuan;
        [self.navigationController pushViewController:hospi animated:YES];
//        NSDictionary *hospital = [_hospitalDiction objectForKey:@"hospital"] ;
//        NSString *url = [hospital objectForKey:@"urlLink"];
//        if (url.length) {
//            AboutUSViewController *YY = [[AboutUSViewController alloc]init];
//            YY.Ktitle = @"微医院";
//            YY.urlLink = url;
//            [self.navigationController pushViewController:YY animated:YES];
//        }else{
//           [AlertLabel AlertInfoWithText:@"暂时不可跳转" andWith:self.view withLocationTag:1];
//        }
        
    };
    WSP.Click = ^(UIButton_Block*btn,NSString *name){
        NSLog(@"医视频");
        HospitalViewController *hospi = [[HospitalViewController alloc]init];
        hospi.HospitalType = HospitalTypeYiVideo;
        [self.navigationController pushViewController:hospi animated:YES];
        
//        NSDictionary *video = [_hospitalDiction objectForKey:@"video"] ;
//        NSString *code = [video objectForKey:@"code"];
//        NSString *type = [video objectForKey:@"type"];
//        
//        DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
//        demandSecond.hidenChoose = YES;
//        demandSecond.isMovie = YES;
//        if ([type isEqualToString:@"1"]) {
//            demandSecond.operationCode = code;
//            demandSecond.isTopic = NO;
//        }if ([type isEqualToString:@"2"]) {
//            demandSecond.TopicCode = code;
//            demandSecond.isTopic = YES;
//        }
//        
//        demandSecond.name = @"医视频";
////        [self setUPWhenMovieOrTV:demandSecond andtitle:code];
//        if ((demandSecond.isTopic&& demandSecond.TopicCode) ||(demandSecond.operationCode)  ){
//            [self.navigationController pushViewController:demandSecond animated:YES];
//        }else
//        {
//            [AlertLabel AlertInfoWithText:@"暂时不可跳转该专题" andWith:self.view withLocationTag:1];
//        }

    };
    
    return view;
}
#pragma mark -  广告点击 按钮按下
- (void)BtnClick:(UIButton*)btn
{
    NSLog(@"播放 %@",btn);
}
- (void)logo
{
    NSLog(@"logo  logo   logo ");
}
- (void)AdClicked:(NSNotification*)notif
{
    NSLog(@"%@",notif);
    if (0) {
        NSDictionary *object = notif.object;
        NSString *name = [object objectForKey:@"name"];
        NSString *contentID = [object objectForKey:@"contentID"];
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = contentID;
        demandVC.contentTitleName = name;
        
        
        [self.navigationController pushViewController:demandVC animated:YES];
    }else
    {
         NSDictionary *object = notif.object;
        
        NSString *type = [object objectForKey:@"type"];
        [self homeADJumpWithType:type andDiction:object];
    }
   
    
    
}
- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                NSString *name = [dic objectForKey:@"name"];
                NSString *contentID = [dic objectForKey:@"id"];
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = contentID;
                demandVC.contentTitleName = name;
                [self.navigationController pushViewController:demandVC animated:YES];
            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
           
        }
            
            break;
            
        case 5:
        {
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            
            [self.navigationController pushViewController:epg animated:YES];

        }
            
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = [dic objectForKey:@"urlLink"];
                [self presentViewController:demandDetail animated:YES completion:nil];
            }
            
            
            
        }
            
            break;
        default:
        {
            
        }
            break;
    }
    
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if (TVCable_CompanyInfo) {
            return 0;
        }
        if(section == 0){
            NSDictionary *dic = [self.dataArray firstObject];
            id obj = [dic objectForKey:@"Array"];
            if([obj isKindOfClass:[NSArray class]])
            {
                if ([obj count] == 1) {
                    obj = obj[0];
                    if ([obj isKindOfClass:[NSString class]]) {
                        if ([obj isEqualToString:@"暂无喜爱记录"]) {
                            return 0;
                        }
                        
                    }
                    
                }
                
            }
            
        }
        return 1;
        
    }else if (section == 1)//[self.BOOLArray[1] isEqualToString:@"YES"]&&
    {
        NSInteger numOfRow = [self numberOfCellRowsWithArray:self.Livearray withRowNum:2];
        if(numOfRow == 0)
        {
            if (numOfRow == 1) {
                id string = [self.Livearray firstObject];;
                if ([string isKindOfClass:[NSString class]]) {
                    if ([string isEqualToString:@"waitingwaiting"]) {
                        numOfRow = 0;
                    }
                }
            }

            return 1;
        }else
        {
            if (numOfRow>2) {
                return 2;
            }else
            {
                return numOfRow;
            }
            
        }

    }
    else
    {
        NSDictionary *dic = [self.dataArray objectAtIndex:section];
        NSArray *array =  ((NSArray*)[dic objectForKey:@"Array"]);
//        return  [self numberOfCellRowsWithArray:array];
      
        NSInteger numOfRow = array.count;
            if (numOfRow == 1) {
                id string = [array firstObject];;
                if ([string isKindOfClass:[NSString class]]) {
                    if ([string isEqualToString:@"waitingwaiting"]) {
                        numOfRow = 0;
                        return numOfRow;
                    }
                }
            }else
            {
                numOfRow = [self numberOfCellRowsWithArray:array];
            }
            
            return numOfRow;
        

    }
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1) {
        //        NSLog(@"第一层  直播推荐");
        
        if(LiveHave)
        {
            static NSString *Liveidentifier = @"LiveTuiJianViewCellidentifier";
            LiveTuiJianViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Liveidentifier];
            if (!cell) {
                cell = [[LiveTuiJianViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Liveidentifier];
            }
            cell.LiveMessageArray = [self arrayFOR_ALLArray:self.Livearray withBeginnumb:indexPath.row withRowNum:2];;
            [cell reloadCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.BLOCK = ^(UIButton_Block*btn,NSString *name)
            {
                NSLog(@"点击%@ \n %@",name,btn.Diction);
                //进入直播界面
                EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
                epg.serviceID = [NSString stringWithFormat:@"%@",[btn.Diction objectForKey:@"serviceID"]];
                epg.serviceName = [NSString stringWithFormat:@"%@",[btn.Diction objectForKey:@"serviceName"]];
                epg.currentPlayType = CurrentPlayLive;
                epg.IsFirstChannelist = YES;
            
                
                
                
                
                [self.navigationController pushViewController:epg animated:YES];
            };
            
            
            return cell;
        }else
        {
            static NSString *LiveNO = @"LiveNO";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LiveNO];
            if(!cell)
            {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LiveNO];
                cell.textLabel.text = @"暂无节目信息";
            }
            return cell;
        }
        
        //之前的直播推荐类型
        {
            
            //
            //        static NSString *Liveidentifier = @"Liveidentifier";
            //        LiveTuiJianCell *cell = [tableView dequeueReusableCellWithIdentifier:Liveidentifier];
            //        if (!cell) {
            //            cell = [[LiveTuiJianCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Liveidentifier];
            //        }
            //
            //
            //        if (self.Livearray.count) {
            //            cell.array = self.Livearray;
            //            [cell reloadCell];
            //            //***********直播推荐跳转
            //            cell.btn1.Click =  ^(UIButton_Block * btn ,NSString* name)
            //            {
            //
            //
            //
            //                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            //
            //                [dic setValue:btn.serviceIDName forKey:@"LiveServiceID"];
            //                [dic setValue:btn.eventName forKey:@"eventName"];
            //
            //
            //                [DataPlist WriteToPlistDictionary:dic ToFile:LIVEINFOPLIST];
            //
            //                HT_FatherBarViewcontroller *fatherbar = [[HT_FatherBarViewcontroller alloc]init];
            //
            //                [fatherbar HTTabBar:nil didSelectedButtonfrom:0 to:1];
            //
            //
            //
            //                //********跳转到新的Live界面
            //                NSLog(@"跳转的名字:%@",name );
            //            };
            //            cell.btn2.Click =  ^(UIButton_Block * btn ,NSString* name)
            //            {
            //                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            //
            //                [dic setValue:btn.serviceIDName forKey:@"LiveServiceID"];
            //                [dic setValue:btn.eventName forKey:@"eventName"];
            //
            //
            //                [DataPlist WriteToPlistDictionary:dic ToFile:LIVEINFOPLIST];
            //
            //                HT_FatherBarViewcontroller *fatherbar = [[HT_FatherBarViewcontroller alloc]init];
            //
            //                [fatherbar HTTabBar:nil didSelectedButtonfrom:0 to:1];
            //
            //                //********跳转到新的Live界面
            //                NSLog(@"跳转的名字:%@",name );
            //            };
            //
            //        }
            //        else
            //        {
            ////            cell.contentView.backgroundColor = [UIColor lightGrayColor];
            //        }
            //        cell.contentView.backgroundColor = [UIColor whiteColor];
            //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //        return cell;
            
        }
    }
    else if (indexPath.section == 0 )//&& [self.BOOLArray[1] isEqualToString:@"YES"]
    {
        //        NSLog(@"第0层  喜爱频道");
        if (self.dataArray.count) {
            NSDictionary *dic = [self.dataArray firstObject];
            id obj = [dic objectForKey:@"Array"];
            if([obj isKindOfClass:[NSArray class]])
            {
                if ([obj count] == 1) {
                    obj = obj[0];
                    if ([obj isKindOfClass:[NSString class]]) {
                        if ([obj isEqualToString:@"暂无喜爱记录"]) {
                            
                            
                            static NSString *FavoriteNO = @"FavoriteNO";
                            NoFavoriteChannelViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FavoriteNO];
                            if(!cell)
                            {
                                cell = [[NoFavoriteChannelViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FavoriteNO];
//                                cell.textLabel.text = @"您还没有喜爱频道喔！";
                            }
//                            cell.textLabel.text = @"您还没有喜爱频道喔！";
                            return cell;
                        }

                    }
                    
                }
                
            }
            
            
        }
        
        
        static NSString *favoriteidentifier = @"favoriteidentifier";
        FavoriteChannelViewCell*cell = [tableView dequeueReusableCellWithIdentifier:favoriteidentifier];
        if (!cell) {
            cell = [[FavoriteChannelViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:favoriteidentifier];
        }
        NSDictionary *dic = self.dataArray[indexPath.section];
        NSArray *array = [dic objectForKeyedSubscript:@"Array"];
        cell.favoriteArray.array = array;
        [cell updataInfo];
        cell.BLOCK = ^(NSDictionary*dic)
        {
            NSLog(@"%@\n%@",[dic objectForKey:@"name"],dic);
            
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            [self.navigationController pushViewController:epg animated:YES];
        };
        
        return cell;
        
        //之前的喜爱展示类型
        {
            //        static NSString *favoriteidentifier = @"favoriteidentifier";
            //        FavoriteChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:favoriteidentifier];
            //        if (!cell) {
            //            cell = [[FavoriteChannelCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:favoriteidentifier];
            //        }
            //        NSDictionary *dic = self.dataArray[indexPath.section];
            //        NSArray *array = [dic objectForKeyedSubscript:@"Array"];
            //        cell.favoriteArray.array = array;
            //        [cell update];
            //        cell.favorite_Block = ^(id Diction)
            //        {
            //            NSLog(@"%@",Diction);
            //            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            //
            //
            //
            //            [dic setValue:[Diction objectForKey:@"id"] forKey:@"LiveServiceID"];
            //            [dic setValue:[Diction objectForKey:@"name"] forKey:@"eventName"];
            //
            //            [DataPlist WriteToPlistDictionary:dic ToFile:LIVEINFOPLIST];
            //
            //            LiveViewControl *live = [[LiveViewControl alloc] init];
            //            /*这里需要给直播界面传值*/
            //            [self.navigationController pushViewController:live animated:NO];
            //        };
            //        cell.selectionStyle =UITableViewCellSelectionStyleNone;
            //        cell.backgroundColor = [UIColor whiteColor];
            //        cell.contentView.backgroundColor = [UIColor whiteColor];
            //        return cell;
        }
        
    }
    else
    {
        
        
        
        //        NSLog(@"第三层  点播分类、电影、综艺、电视剧");
        static NSString *Progranidentifier = @"Progranidentifier";
        ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
        if (!cell) {
            cell = [[ProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        
        //22222222*******yes转换数据
//        NSInteger countYES  = [self realINdexYESORNO:self.BOOLArray andindex:indexPath.section];
//
        NSDictionary *dic = [NSDictionary dictionary];
        if (self.dataArray.count>indexPath.section) {
            dic = [self.dataArray objectAtIndex:indexPath.section];
        }

        NSArray * array = [dic objectForKey:@"Array"];
        if (indexPath.section == 2) {
            cell.isMovie = NO;
        }else
        {
            cell.isMovie = NO;
        }
        
        if (array.count) {
            id objc = array[0];
            if ([objc isKindOfClass:[NSString class]] && [objc isEqualToString:@"waitingwaiting"]) {
                cell.textLabel.text = @"\t\t\t暂无节目信息";
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            }else
            {
                cell.textLabel.text = @"";
            }
        }else
        {
            cell.textLabel.text = @"";
        }
        
        
        cell.dateArray.array = [self arrayFOR_ALLArray:array withBeginnumb:indexPath.row];
        NSLog(@"0时间%@",[NSDate date]);
        [cell update];
        self.tableView.delaysContentTouches = YES;
        NSLog(@"时间%@",[NSDate date]);
        cell.block = ^(UIButton_Block*btn)
        {
            NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
            
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
            demandVC.contentID = btn.serviceIDName;
            demandVC.contentTitleName = btn.eventName;
            
            
            [self.navigationController pushViewController:demandVC animated:YES];
        };
        NSLog(@"1时间%@\n\n\n\n\n\n\n",[NSDate date]);
        cell.contentView.backgroundColor = [UIColor whiteColor];
//                cell.backgroundColor = [UIColor grayColor];
        return cell;
        
    }
}
#pragma  mark - 分组headerView
//每个分组的头标题
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGFloat HighOfSectionBlow = 5;
    //header承载式图
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), 40*kRateSize)];
    headerView.backgroundColor = App_background_color;
    //白色背景图
    UIImageView *background = [UIImageView new];
    [headerView addSubview:background];
    [background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_top).offset((5+HighOfSectionBlow)*kRateSize);
        make.leading.equalTo(headerView.mas_leading);
        make.right.equalTo(headerView.mas_right).offset(0*kRateSize);
        make.bottom.equalTo(headerView.mas_bottom).offset(0*kRateSize);
    }];
    background.image = [UIImage imageNamed:@"bg_home_section_header"];
    
    //左边竖线段
    UIImageView *Leftline = [[UIImageView alloc]initWithFrame:CGRectMake(5*kRateSize, (11+HighOfSectionBlow)*kRateSize, 3*kRateSize, 18*kRateSize)];
    Leftline.image = [UIImage imageNamed:@"ic_vertical_yellow_line"];
    Leftline.layer.cornerRadius = 2.5;
    Leftline.clipsToBounds =YES;
    [headerView addSubview:Leftline];
    
    //section的title
    NSString *sectionTitle = @"";
    if (self.dataArray.count>section) {
        sectionTitle  =[(NSDictionary*) self.dataArray[section] objectForKey:@"title"];
    }
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13*kRateSize, (5+HighOfSectionBlow)*kRateSize, 100*kRateSize, 30*kRateSize)];
    label.text = sectionTitle;
    label.size = CGSizeMake([self MYString:label.text StringSizeOfFont:[UIFont systemFontOfSize:18]].width,30*kRateSize);
    label.textColor = [UIColor blackColor];
    [headerView addSubview:label];

    //不需要更多按钮的时候 下面这代码不走
    if (section >= 2) {
        //求出了section中的个数 以此判断 是否该显示更多按钮
        NSInteger numOfRow=0;
        {
            NSDictionary *dic= [NSDictionary dictionary];
            if (section<self.dataArray.count) {
                dic = [self.dataArray objectAtIndex:section];
            }

            NSArray *array =  ((NSArray*)[dic objectForKey:@"Array"]);
            //        return  [self numberOfCellRowsWithArray:array];
            
            numOfRow = array.count;
            if (numOfRow == 1) {
                id string = [array firstObject];;
                if ([string isKindOfClass:[NSString class]]) {
                    if ([string isEqualToString:@"waitingwaiting"]) {
                        numOfRow = 0;
                        
                    }
                }
            }else
            {
                numOfRow = [self numberOfCellRowsWithArray:array];
            }
            
            
        }
        if(0)
        {
        //是否需要更多按钮啊？
        UIButton_Block *btn = [UIButton_Block new];
        btn.frame =   CGRectMake(FRAME_W(self.view) - (60-7)*kRateSize, 5*kRateSize, 60*kRateSize, 30*kRateSize);
        btn.titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
//        btn.titleLabel.textColor = UIColorFromRGB(0x57b1f4);
        [btn setTitle:@"更多" forState:UIControlStateNormal];
        [btn setTitleColor:UIColorFromRGB(0x57b1f4) forState:UIControlStateNormal];
        //    btn.backgroundColor =[UIColor grayColor];
        btn.serviceIDName = label.text;
//        if (section == 2 ) {//&& [self.BOOLArray[2] isEqualToString:@"YES"]
        
            [btn setClick:^(UIButton_Block*btn,NSString* name){
                NSDictionary *dic = (NSDictionary*)self.dataArray[section];
                NSString *title = [dic objectForKey:@"title"];
                NSString * categoryID = [dic objectForKey:@"categoryID"];
                NSLog(@"%@ 频道:%@",[btn currentTitle],title);
                
                DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
                demandSecond.hidenChoose = YES;
                demandSecond.isMovie = YES;
                demandSecond.categoryID = categoryID;
                demandSecond.name = title;
                [self setUPWhenMovieOrTV:demandSecond andtitle:title];
                if (demandSecond.isTopic&& ![demandSecond.TopicCode isEqualToString:@"NO"]){
                    [self.navigationController pushViewController:demandSecond animated:YES];
                }else
                {
                    [AlertLabel AlertInfoWithText:@"暂时不可跳转该专题" andWith:self.view withLocationTag:1];
                }
                
            }];
            
        
        {
//        }else if(section == 3)// && [self.BOOLArray[3] isEqualToString:@"YES"
//        {
//            
//            
//            [btn setClick:^(UIButton_Block*btn,NSString* name){
////                NSDictionary *dic = (NSDictionary*)self.dataArray[countyes];
////                NSString *title = [dic objectForKey:@"title"];
////                NSString * categoryID = [dic objectForKey:@"categoryID"];
////                NSLog(@"%@ 频道:%@",[btn currentTitle],title);
//                
////                DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
////                demandSecond.hidenChoose = YES;
////                demandSecond.isMovie = YES;
////                demandSecond.categoryID = categoryID;
////                demandSecond.name = title;
////                [self setUPWhenMovieOrTV:demandSecond andtitle:title];
////                [self.navigationController pushViewController:demandSecond animated:YES];
//            }];
//            
//        }
        }
        
        
        UIImageView *Rightline = [[UIImageView alloc]initWithFrame:CGRectMake(12*kRateSize, 8*kRateSize, 1*kRateSize, 14*kRateSize)];
        Rightline.backgroundColor = UIColorFromRGB(0x57b1f4);
        Rightline.layer.cornerRadius = 2.5;
        Rightline.clipsToBounds =YES;
        [btn addSubview:Rightline];
        
        [headerView addSubview:btn];
        
        if (numOfRow==0) {
            btn.hidden = YES;
        }else
        {
            btn.hidden = NO;
        }
            
        }
        
    }
    
    UIView *BlackLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (5+HighOfSectionBlow))];
    BlackLine.backgroundColor =App_background_color;
    [headerView addSubview:BlackLine];
    
    if (TVCable_CompanyInfo && section == 0) {
        UIView *nilView = [[UIView alloc] init];
        nilView.frame = CGRectMake(0, 0, kDeviceWidth, 5*kRateSize);
        nilView.backgroundColor = CLEARCOLOR;
        return nilView;
    }
    return headerView;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"2时间%@",[NSDate date]);;
}
//设置分组header的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section>1) {
        NSDictionary *dic = [self.dataArray objectAtIndex:section];
        NSArray *array = [dic objectForKey:@"Array"];
        if (array.count == 0) {
            return 0;
        }
        if (self.dataArray.count>section) {
            NSDictionary *dic = [self.dataArray objectAtIndex:section];
            NSArray *array = [dic objectForKey:@"Array"];
            if ((array.count == 1) && [array[0] isKindOfClass:[NSString class]] &&[array[0] isEqualToString:@"waitingwaiting"]) {
                return 0;
            }
            return (35+5)*kRateSize;
        }
        
    }else if(section == 0){
        NSDictionary *dic = [self.dataArray firstObject];
        id obj = [dic objectForKey:@"Array"];
        if([obj isKindOfClass:[NSArray class]])
        {
            if ([obj count] == 1) {
                obj = obj[0];
                if ([obj isKindOfClass:[NSString class]]) {
                    if ([obj isEqualToString:@"暂无喜爱记录"]) {
                        return 0;
                    }
                    
                }
                
            }
            
        }
        
        if (TVCable_CompanyInfo && section == 0) {
            return 5*kRateSize;
        }
    }
    
    return (35+5)*kRateSize;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if(LiveHave)
        {
            return 105*kRateSize;
        }else
        {
            return 60*kRateSize;
        }
    }
    if (indexPath.section == 0 ) {//&& [self.BOOLArray[1] isEqualToString:@"YES"]
        return 78*kRateSize;
    }
    return 170*kRateSize;
}

#pragma mark - HT请求回调方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"结: %ld result:%@  type:%@",status,result,type );
    
    WS(wself);
    
    switch (status) {
        case 0:
        {
            if ([type isEqualToString:HomeADSLiderPage] || [type isEqualToString:CN_COMMON_SLIDESHOWLIST]) {
                NSMutableArray *array = [result objectForKey:@"slideshowList"];
                
                if (array.count>1) {
                    array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
                    NSDictionary *firstDic = [array firstObject];
                    [array removeObjectAtIndex:0];
                    [array addObject:firstDic];
                }
                [self.posterView updateThePosterData:array];
                

            }
            
            if ([type isEqualToString:VOD_HospitalInfo]) {
                _hospitalDiction = result;
            }
            if ([type isEqualToString:EPG_REGION_INFO]) {
                _hospitalDiction = result;
            }
            if ([type isEqualToString:@"topSearchWords"]) {
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];

                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时搜索热词记录");
                    _seachTextView.text = @"";
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSLog(@"名字是：： %@",name);
                        if (kDeviceWidth == 320) {
                            
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                            
                        }else if (kDeviceWidth == 375)
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        else
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        
                        
                        
                    }
                }

            }
            
            //直播推荐请求数据
            if ([type isEqualToString:EPG_HOTCHANNELLIST]) {
                //                NSLog(@"直播推荐请求成功结果: %ld result:%@  type:%@",status,result,type );
                NSDictionary *dic  = (NSDictionary*)result;
                self.Livearray = [dic objectForKey:@"list"];
                if(self.Livearray.count)
                {
                    LiveHave = YES;
                }else
                {
                    LiveHave = NO;
                }
                //                {//第一次的时候没有直播推荐，table里为空，要添加一个title和数组刷出直播推荐
                NSMutableDictionary *LiveDiction = [NSMutableDictionary dictionary];
                [LiveDiction setObject:@"热点直播" forKey:@"title"];
                [LiveDiction setObject:[NSArray arrayWithObject:@"livewaiting"] forKey:@"Array"];
                
                [self.dataArray removeObjectAtIndex:1];
                [self.dataArray insertObject:LiveDiction atIndex:1];
                
                _hub.hidden = YES;
                [self hideLoading];
                
                [self.tableView reloadData];
            }
            
            
            if ([type isEqualToString:VOD_HomeConfigList]) {
                NSLog(@"节目配置单%@",result);
                
                NSArray *configList = [result objectForKey:@"configList"];
                
                NSInteger indexNum = 0 + HaveTuijian;
                for (NSDictionary *dic in configList) {
                    
                    NSString *code = [dic objectForKey:@"code"];
                    NSString *name = [dic objectForKey:@"name"];
                    NSString *seq = [dic objectForKey:@"seq"];
                    NSString *num = [dic objectForKey:@"num"];

                    if (code.length&&name.length) {
                        [_homeConfigDiction setObject:code forKey:name];
                    }

                    
                    NSMutableDictionary *titleDIC = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSArray arrayWithObject:@"waitingwaiting"],@"Array",code,@"code",name,@"title",nil];
                    [self.dataArray addObject:titleDIC];
                    
                        HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                     [_requst VODTopicProgramListWithTopicCode:code andStartSeq:1 andCount:[num integerValue] andTimeout:10 andIndex:[seq integerValue]+HaveTuijian-1];//12.10首页配置信息的修改
                    indexNum++;
                }
                _reqCount = indexNum;
                
                if (_reqCount == 2) {
                    //请求完成
                }
                
            }
            
            
            //推荐页面的VOD_TOPIC_PROGRAM_LIST
            if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
                NSLog(@"推荐的专题节目列表: %ld result:%@  type:%@",status,result,type );
                NSArray *array = [result objectForKey:@"vodList"];
                
                _reqCount--;
                if (_reqCount == 2) {
                    _finishRequest = YES;
//                    [AlertLabel AlertInfoWithText:@"数据分类下请求完成" andWith:self.view withLocationTag:1];
                }
                
                
                NSInteger index = [[result objectForKey:@"index"] integerValue];
                if ([result objectForKey:@"count"]) {
                    
                    if (index<self.dataArray.count) {
//                        NSString *string = [self.BOOLArray objectAtIndex:index];
//                        [self.BOOLArray replaceObjectAtIndex:index withObject:@"YES"];
                        NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:index];
                        if (array) {
                            [zidian setObject:array forKey:@"Array"];
                        }else
                        {
//                            [self.BOOLArray replaceObjectAtIndex:index withObject:@"YES!"];
                        }
                        
//                        [self.tableView reloadData];
                    }
                }
                
            }
            
            //广告页
            if ([type isEqualToString:@"ADVOD_TOPIC_PROGRAM_LIST"]) {
                NSLog(@"结果广告: %ld result:%@  type:%@",status,result,type );
                NSArray *array = [result objectForKey:@"vodList"];
                [self.posterView updateThePosterData:array];

            }
            
            if ([type isEqualToString:@"slideshowList"]) {
                NSLog(@"首页的广告啊%@",result);
                NSLog(@"结果广告: %ld result:%@  type:%@",status,result,type );
                NSMutableArray *array = [result objectForKey:@"slideshowList"];
                if (array.count>1) {
                    array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
                    NSDictionary *firstDic = [array firstObject];
                    [array removeObjectAtIndex:0];
                    [array addObject:firstDic];
                }
                [self.posterView updateThePosterData:array];
            }
            
            
            
            //喜爱记录
            if ([type isEqualToString:EPG_TVFAVORATELIST]) {
                NSLog(@"喜爱记录: %ld result:%@  type:%@",status,result,type );
                
                NSDictionary *dic = result;
                NSString *ret = [dic objectForKey:@"ret"];
                NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:0];
                if ([ret integerValue] == -1 ) {
                    NSLog(@"暂时没有喜爱记录");
                   NSMutableDictionary *dic =  [self.dataArray firstObject];
                    [dic setObject:@[@"暂无喜爱记录"] forKey:@"Array"];
                    
//                    [self.BOOLArray replaceObjectAtIndex:1 withObject:@"YES"];
                }else if ([ret integerValue] == -2 ) {
                    NSLog(@"暂时没有喜爱记录");
                    NSMutableDictionary *dic =  [self.dataArray firstObject];
                    [dic setObject:@[@"暂无喜爱记录"] forKey:@"Array"];
                    
                    //                    [self.BOOLArray replaceObjectAtIndex:1 withObject:@"YES"];
                }
                
                else if ([ret integerValue] == 0 )
                {
                    NSArray *array = [dic objectForKey:@"servicelist"];
                    if (array.count) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTI_UPDATE _FAVORATE_CHANELL_LIST" object:array];
                        [zidian setObject:array forKey:@"Array"];
//                        [self.BOOLArray replaceObjectAtIndex:1 withObject:@"YES"];
                    }else
                    {
                        //喜爱界面一定有
//                        [self.BOOLArray replaceObjectAtIndex:1 withObject:@"YES"];
                    }
                }
                
                //                [self.tableView reloadData];
                
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                
            }
            
            //点播观看记录
            if ([type isEqualToString:VOD_PLAY_LIST]) {
                NSLog(@"观看记录: %ld result:%@  type:%@",status,result,type );
                    HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
                
                NSDictionary *dic = result;
                NSString *ret = [dic objectForKey:@"ret"];
                
                if ([ret integerValue] == -1 ) {
                    NSLog(@"暂时没有播放记录");
                    HaveRecord = NO;
                    
                    [self setHighForheadIsOrNo:HaveRecord];
                   
                    
                }else if ([ret integerValue] == 0 )
                {
                    NSArray *array = [dic objectForKey:@"vodList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSString *breakPoint = [record objectForKey:@"breakPoint"];
                        _MYplaytime = [record objectForKey:@"playTime"];
                        if(breakPoint.integerValue == 0)
                        {
                            NSArray *list  = [record objectForKey:@"seriesList"] ;
                            if (list.count) {
                                NSDictionary *dic = list[0];
                                breakPoint = [dic objectForKey:@"breakPoint"];
                            }
                        }
                        NSString *contentID = [record objectForKey:@"contentID"];
                        NSString *string = [NSString stringWithFormat:@"续看 %@ %@",[self shortStringWithString:name],[self secendtoTime:[breakPoint integerValue]]];
                       
                        HaveRecord = YES;
                        
                        [self setHighForheadIsOrNo:HaveRecord];
                        
                       
                        
                        
                        self.XUKanLabel.text = string;
                        _RecordView.userInteractionEnabled = YES;
                        self.XUKanBtn.eventName = name;
                        
                        self.XUKanBtn.serviceIDName = contentID;
                        
                        __block BOOL weakisEPGRecord = isEPGRecord;
                        
                        __weak NSString *weakchannelID = _channelID;
                        
                        self.XUKanBtn.Click = ^(UIButton_Block *btn,NSString *name)
                        {
                            if (weakisEPGRecord) {
                                //进入直播界面
                                _channelID =  btn.serviceIDName;
                                
                                    HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:wself];
                                [_requst EventPlayWithEventID:weakchannelID andTimeout:10];

                            }else
                            {
                                //*******要续看的一些操作
                                NSLog(@"续看的名字%@ ID:%@ ",btn.eventName,btn.serviceIDName);
                                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                                demandVC.contentID = btn.serviceIDName;
                                demandVC.contentTitleName = btn.eventName;
                                
                                [wself.navigationController pushViewController:demandVC animated:YES];
                                
                            }
                        };
                        
                        isEPGRecord = NO;
                        
                    }else
                    {
                        HaveRecord = NO;
                       
                           [self setHighForheadIsOrNo:HaveRecord];
                        
                    }
                }
                
                
                
            }
            
            if ([type isEqualToString:VOD_NewPlayRecordList]) {
                NSLog(@"观看记录: %ld result:%@  type:%@",status,result,type );
                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
                
                NSDictionary *dic = result;
                NSString *ret = [dic objectForKey:@"count"];
                
                if ([ret integerValue] <=0 ) {
                    NSLog(@"暂时没有播放记录");
                    HaveRecord = NO;
                    [self setHighForheadIsOrNo:HaveRecord];
                   
                    
                }else if ([ret integerValue] > 0 )
                {
                    NSArray *array = [dic objectForKey:@"vodList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSString *breakPoint = [record objectForKey:@"breakPoint"];
                        
                        _MYplaytime = [record objectForKey:@"playTime"];
                        episodeSeq = [record objectForKey:@"lastPlayEpisode"];
                        breakPointRecord =[record objectForKey:@"breakPoint"];
                        NSString *contentID = [record objectForKey:@"contentID"];
                        
                        NSString *string = [NSString stringWithFormat:@"续看 %@ %@",[self shortStringWithString:name],[self secendtoTime:[breakPoint integerValue]]];
                       HaveRecord = YES;
                        [self setHighForheadIsOrNo:HaveRecord];
                        
                        self.XUKanLabel.text = string;
                        _RecordView.userInteractionEnabled = YES;
                        self.XUKanBtn.eventName = name;
                        
                        self.XUKanBtn.serviceIDName = contentID;
                        
                        __block BOOL weakisEPGRecord = isEPGRecord;
                        
                        __weak NSString *weakchannelID = _channelID;
                        
                        __weak NSString *weakepisodeSeq= episodeSeq;
                        
                        __weak NSString *weakbreakPointRecord= breakPointRecord;
                        
                        self.XUKanBtn.Click = ^(UIButton_Block *btn,NSString *name)
                        {
                            if (weakisEPGRecord) {
                                //进入直播界面
                                
                                
                                _channelID =  btn.serviceIDName;
                                
                                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:wself];
                                [_requst EventPlayWithEventID:weakchannelID andTimeout:10];
                       
                            }else
                            {
                                //*******要续看的一些操作
                                NSLog(@"续看的名字%@ ID:%@ ",btn.eventName,btn.serviceIDName);
                                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                                demandVC.contentID = btn.serviceIDName;
                                demandVC.contentTitleName = btn.eventName;
                                demandVC.episodeSeq = weakepisodeSeq;
                                demandVC.breakPointRecord = weakbreakPointRecord;
                                
                                episodeSeq = @"";
                                breakPointRecord = @"";
                                [wself.navigationController pushViewController:demandVC animated:YES];
                                
                            }
                        };
                        
                        isEPGRecord = NO;
                        
                    }else
                    {
                        HaveRecord = NO;
                        [self setHighForheadIsOrNo:HaveRecord];
                    }
                }
                

            }
            
            
            
            //EPG_TVPLAYLIST
            if ([type isEqualToString:EPG_EVENTPLAYLIST]) {
                
                
                NSLog(@"直播结果%@",result);
                NSArray *array = [result objectForKey:@"eventlist"];
               
//                if (HaveRecord) {
//                    NSString *ret = [result objectForKey:@"ret"];
//                    if ([ret integerValue] == 0) {
//                       [self setHighForheadIsOrNo:HaveRecord];
//                    }
//                }else{
//                   [self setHighForheadIsOrNo:HaveRecord];
//                }
                NSString *start = @"";
                NSString *name = @"";
                NSString *contentID = @"";
                NSString *breakPoint = @"";
                if (array.count) {
                    NSDictionary *dic = [array firstObject];
                    start = [dic objectForKey:@"playTime"];
                    name = [dic objectForKey:@"eventName"];
                    contentID = [dic objectForKey:@"eventID"];
                    breakPoint = [dic objectForKey:@"breakPoint"];
                    HaveRecord = YES;
                    NSDictionary * endDiction = array[0];
                    self.XUKanBtn.Diction = endDiction;
                   
                    NSString *dayStr = [endDiction objectForKey:@"endTime"];
                    _endDate = [Tool YearMouthDayWithString:dayStr];
                    
                }
                if (!_IS_SUPPORT_VOD) {
                    if (array.count) {
                        HaveRecord = YES;
                    }else{
                        HaveRecord = NO;
                    }
                    
                }
                
                NSLog(@"点播播 %@放的时间 %@",start,_MYplaytime);
                breakPoint = [self secendtoTime:[breakPoint integerValue]];
                name = [NSString stringWithFormat:@"%@ %@",name,breakPoint];
                             
                if ([Tool compareTime:start endTime:_MYplaytime] == 1) {
                    NSLog(@"展示直播记录");
                    isEPGRecord = YES;
                    NSString *string = [NSString stringWithFormat:@"续看 %@",name];

                    self.XUKanBtn.eventName = name;
                    self.XUKanBtn.serviceIDName = contentID;
                    
                    self.XUKanLabel.text = string;
                    {
                       
                        
                    [self setHighForheadIsOrNo:HaveRecord];
                     
                        
                    }

                }else
                {
                    
                    [self setHighForheadIsOrNo:HaveRecord];
                }
                
            }
            
            //            EPG_EVENTPLAY
            if ([type isEqualToString:EPG_EVENTPLAY]){
                
                
                if (result.count > 0) {
                    
                    switch ([[result objectForKey:@"ret"] integerValue]) {
                        case 0:{
                            NSLog(@"成功");
                            
                            //NSDictionary *dataDic = [NSDictionary dictionary];
                            
                            NSDictionary *dataDic = [result objectForKey:@"serviceinfo"];
                            
                            NSLog(@"%@",dataDic);
                            
                            
                            EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                            live.currentPlayType = CurrentPlayEvent;
                            live.serviceID = [dataDic objectForKey:@"serviceID"];
                            live.serviceName = [dataDic objectForKey:@"name"];
                            live.eventURL =[NSURL URLWithString:[dataDic objectForKey:@"playLink"]];
                            live.eventURLID = _channelID;
                            live.currentDateStr = _endDate;
                            [self.navigationController pushViewController:live animated:YES];
                            
                            
                            break;
                        }
                        case -1:{
                            
                            NSLog(@"当前节目不存在或者不提供回看");
                            
                            [AlertLabel AlertInfoWithText:@"当前节目不存在或者不提供回看" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -2:{
                            
                            NSLog(@"用户未登录或者登录超时");
                            
                            [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -3:{
                            
                            NSLog(@"鉴权失败");
                            
                            [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -5:{
                            
                            NSLog(@"频道对应此终端无播放链接");
                            
                            [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -6:{
                            
                            NSLog(@"该频道不提供回看功能");
                            
                            [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -9:{
                            NSLog(@"失败（连接3A系统失败或者参数错误）");
                             [AlertLabel AlertInfoWithText:@"失败（连接3A系统失败或者参数错误）" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
            
            //顶部的搜索文字
            if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
                NSLog(@"热门记录: %ld result:%@  type:%@",status,result,type );
                
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                
                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时搜索热词记录");
                    _seachTextView.text = @"";
                    HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
                     [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSLog(@"名字是：： %@",name);
                        if (kDeviceWidth == 320) {
                            
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                            
                        }else if (kDeviceWidth == 375)
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        else
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        
                        
                        
                    }
                }
                
            }
            
            
        }
            break;
            
        default:
        {
             _finishRequest = YES;
            NSLog(@"请求失败码%ld %@",status,type);
        }
            break;
    }
    
}


- (NSInteger)realCountYESORNO:(NSArray*)array
{
    NSInteger count =0 ;
    for (NSString *string in array) {
        if ([string isEqualToString:@"YES"]) {
            count ++;
        }
    }
    return count;
}

- (NSInteger)realINdexYESORNO:(NSArray*)array andindex:(NSInteger)index
{
    NSInteger count =0 ;
    index++;
    for (NSInteger i = 0; i<array.count; i++) {
        NSString *string = array[i];
        if ([string isEqualToString:@"YES"]) {
            count ++;
            if (index == count) {
                return i;
            }
        }
    }
    
    return -1;
}
#pragma  mark - HT Navigation
-(void)gotoLivePage:(UIButton *)btn{
    
    
//    SearchViewController *search = [[SearchViewController alloc]init];
//    [self.navigationController pushViewController:search animated:YES];
    
}


- (BOOL)shouldAutorotate {
    
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    return (toInterfaceOrientation == UIInterfaceOrientationMaskPortrait);
}



#pragma  mark -header跟随tabelView动
//header跟随tabelView动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView)
    {
        //YOUR_HEIGHT 为最高的那个headerView的高度
        CGFloat sectionHeaderHeight = 40;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

#pragma  mark -首页刷新
- (void)reRequestHomeData
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length==0||[token isEqualToString:@"(null)"]) {
        return;
    }
    
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
    if (string.length == 0 ) {
        return;
    }
    
    [self performSelector:@selector(finishRequest) withObject:self afterDelay:5];
    NSLog(@"12312调用刷");
    if ([self RequstEnabledWithFirstTime:_requsetDate andSecondData:[NSDate date]]&[self RequstEnabledWithFirstTime1111:_requsetDate andSecondData:[NSDate date]] || _first ) {
        _first = NO;
        _requsetDate = [NSDate date];
        
        {
            if (self.dataArray.count >= 4) {
                NSDictionary *dic =  self.dataArray[0];
                NSDictionary *dic1 =  self.dataArray[1];

                
                [self.dataArray removeAllObjects];
                [self.dataArray addObject:dic];
                [self.dataArray addObject:dic1];

                
                
            }
        }
        
        
        NSLog(@"调用刷新");//_reqFinish&&
        if (_finishRequest) {//一次请求完成后才会刷新下一次
            NSLog(@"2222222调刷新pinbi");
            
            _finishRequest = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _finishRequest = YES;
            });
            //*****1.请求直播推荐的数据
            HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
            [requst EPGGetHomepageHotChannel];
            if (_IS_SUPPORT_VOD) {
                //请求首页配置信息
                [requst VODHomeConfigListWithTimeOut:10];
            }
            
            
            //有无喜爱频道
            [self haveFavirateList];

            
            if (0) {
                //*******3请求广告页
                [requst VODGetHomepageADImage];
            }else
            {
//                [requst getHomePageSlideshowList];
//                [requst HomeADSLiderPagess];
//                [requst commAPI_HomeSliderpages];
                [requst CNCommonSlideshowList];

                
            }
            
            

            
            
            //*******4请求观看记录
            if (IsLogin) {
                
            
                if (_IS_SUPPORT_VOD) {
                    //            [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
                    [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
                }else
                {
                    HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
                    [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];

                }

            
            }else{
                [self setHighForheadIsOrNo:NO];
            }
            if(_IS_SUPPORT_VOD)
            {
            //*****5请求搜索热门字第一天
                [requst VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
            }else{
                [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
            }
            
            if (WeiHospital) {
//                [requst VODHospitalInfoWithTimeout:10];
                [requst EPGGetRegionInfoWithRegionCode:REGION_CODE andTimeout:5];

            }
            
            //*******6请求推荐专题的节目列表
//            [requst VODTopicProgramListWithTopicCode:@"ZT0002" andStartSeq:1 andCount:6 andTimeout:10 andIndex:99999999];
//            [requst VODTopicProgramListWithTopicCode:@"ZT0003" andStartSeq:1 andCount:6 andTimeout:10 andIndex:count+1];
            
            if([self RequstEnabledWithFirstTime:_requsetDate andSecondData:[NSDate date]])
            {
//                _hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                _hub.delegate = self;
                [self showLoading];
                
            }
            
            [self reloadHEADER];
            
            
        }
        
    }
    
}
- (void)finishRequest
{
    _finishRequest = YES;
}

#pragma mark - 刷新喜爱频道
- (void)reloadFavoriteChannel
{
    [self haveFavirateList];
    
//    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
}




#pragma mark- gdddd


- (void)setSeachBar
{
    
//    UIButton *logo = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    logo = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"ic_launcher" imgHighlight:@"ic_launcher"  target:self action:@selector(logo)];
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(14*kRateSize, 25, 34, 34)];
    logo.image = [UIImage imageNamed:@"ic_launcher"];
    [self.m_viewNaviBar addSubview:logo];
    
    //设置搜索textField
    
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0*kRateSize, 0, size.width, size.height)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = size.height/2;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(40 ,1,size.width - 33*kRateSize/kRateSize , 26)];
//            _seachTextView.backgroundColor = [UIColor redColor];
    //    seachTextView.delegate = self;
    _seachTextView.text = @"";
    //        _seachTextView.backgroundColor = [UIColor redColor];
    _seachTextView.font = [UIFont systemFontOfSize:16];
    _seachTextView.textColor = UIColorFromRGB(0xaaaaaa);
    
    //    seachTextView.preservesSuperviewLayoutMargins = YES;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    //    UIButton *btn = [seachTextView valueForKey:@"_clearButton"];
    //    [btn setImage:[UIImage imageNamed:@"sl_search_del_item_pressed"] forState:UIControlStateNormal];
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake((_seachTextView.frame.origin.x-30)*kRateSize,(_seachTextView.frame.origin.y+1)*kRateSize, 24, 24)];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.alpha = 0.8;
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
        SearchViewController *search1 = [[SearchViewController alloc]init];

//        search1.searchText = [array lastObject];;
        [self.navigationController pushViewController:search1 animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    
    
    
}


- (void)haveFavirateList
{
    //有没有喜爱频道
    if (IsLogin) {
        if (!TVCable_CompanyInfo) {
            //请求是否含有喜爱记录
            HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
            [requst TVFavorateListWithTimeout:10];
        }

    }else
    {
        NSArray *array = [DataPlist ReadToFileArr:GUESTVODFAVORITEPLIST];
        if (array.count) {
            NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:0];
            [zidian setObject:array forKey:@"Array"];
//            [self.BOOLArray replaceObjectAtIndex:0 withObject:@"YES"];
        }else
        {
//            [self.BOOLArray replaceObjectAtIndex:0 withObject:@"YES"];
            NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:0];
            [zidian setObject:[NSArray arrayWithObject:@"暂无喜爱记录"] forKey:@"Array"];
         
        }
        
        [self.tableView reloadData];
        
    }
}
- (void)havePlayRecordList
{

    //有没有喜爱频道
    if (IsLogin) {
        //请求是否含有喜爱记录
        HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
//        [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
        if (_IS_SUPPORT_VOD) {
            //            [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
            [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
        }else
        {
            HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
            [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
            
        }

      
    }else
    {
        NSArray *array = [DataPlist ReadToFileArr:GUESTPLAYRECORDERPLIST];
        if (array.count) {
            
        }else
        {
            
        }
        
    }
}


#pragma mark - 工具计算
- (void)setHighForheadIsOrNo:(BOOL)havePlayRecorder
{
    if (havePlayRecorder) {
        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+40+WeiHospital)*kRateSize);
        _RecordView.hidden = NO;
        [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(40*kRateSize));
        }];
        
        self.tableView.tableHeaderView = nil;
        self.tableView.tableHeaderView = _Headview;
    }else{
        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital)*kRateSize);
        _RecordView.hidden = YES;
        [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(0*kRateSize));
        }];
        
        self.tableView.tableHeaderView = nil;
        self.tableView.tableHeaderView = _Headview;
    }
   
}


- (void)setUPWhenMovieOrTV:(DemandSecondViewcontrol*)demand andtitle:(NSString*)title
{
    //配置节目信息跳转的专题的code
    
    NSString *code = [_homeConfigDiction objectForKey:title];
    if (code.length) {
        demand.isTopic = YES;
        demand.TopicCode = code;
    }else
    {
        demand.isTopic = YES;
        demand.TopicCode = @"NO";
    }
}
//    if ([title rangeOfString:@"热门电影"].length) {
//        demand.isTopic = YES;
//        demand.TopicCode = @"ZT0002";
//    }
//    if ([title rangeOfString:@"热门电视"].length) {
//        demand.isTopic = YES;
//        demand.TopicCode = @"ZT0003";
//    }
//}



//看请求是不是都已完成
- (BOOL)requstFinishWithArray:(NSArray*)array
{
    BOOL finish = YES;
    if (array.count) {
        array = array[0];
    }
    for (NSDictionary *dic in array) {
        if ([[dic objectForKey:@"Array"] count] == 1 ) {
            id obj =  [[dic objectForKey:@"Array"] objectAtIndex:0];
            if ([obj isKindOfClass:[NSString class] ]) {
                if ([obj isEqualToString:@"vodFinish"]) {
                    finish = YES;
                }else
                {
                    return NO;
                }
            }
        }else
        {
            finish = NO;
            return finish;
        }
    }
    return finish;
}

//可以放在工具类中，开一个时间是不是超时了
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    
    return _finishRequest;//
//    if ([date1 timeIntervalSinceDate:date] >= 10) {
//        NSLog(@"超时了啊");
//        return YES;
//    }
//    return NO;
}

- (BOOL)RequstEnabledWithFirstTime1111:(NSDate*)date andSecondData:(NSDate*)date1
{
    
//    return _finishRequest;//
        if ([date1 timeIntervalSinceDate:date] >= 10) {
            NSLog(@"超时了啊");
            return YES;
        }
        return NO;
}
//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    return [self numberOfCellRowsWithArray:array withRowNum:3];
}
//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array withRowNum:(NSInteger)num
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        return numb % num == 0?  numb/num : numb/num+1;
    }
}
//这个是取出相应行cell 中的那2个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index withRowNum:(NSInteger)num
{
    NSMutableArray *anser = [NSMutableArray array];
    
    for(NSInteger i = 0 ;i < num; i++)
    {
        if (array.count > index*num + i  ) {
            [anser addObject:array[index*num + i]];
        }
    }
    {
        //    if (array.count > index*3) {
        //        [anser addObject:array[index*3]];
        //    }
        //    if (array.count > index*3+1) {
        //        [anser addObject:array[index*3+1]];
        //    }
        //    if (array.count > index*3+2) {
        //        [anser addObject:array[index*3+2]];
        //    }
    }
    return anser;
}

//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    return  [self arrayFOR_ALLArray:array withBeginnumb:index withRowNum:3];
    //    NSMutableArray *anser = [NSMutableArray array];
    //    if (array.count > index*3) {
    //        [anser addObject:array[index*3]];
    //    }
    //    if (array.count > index*3+1) {
    //        [anser addObject:array[index*3+1]];
    //    }
    //    if (array.count > index*3+2) {
    //        [anser addObject:array[index*3+2]];
    //    }
    //
    //    return anser;
}

- (void)reloadHEADER
{
    //*******4请求观看记录
    //    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    //    [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
    _MYplaytime = @"1999-01-01 00:00:00";
    if (!IsLogin) {
        
        
    }else
    {
        //*******4请求观看记录
        HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
//        [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
        if (_IS_SUPPORT_VOD) {
            //            [requst VODPlayListWithContentID:nil andStarSeq:1 andCount:1 andTimeout:10];
            [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
        }else
        {
            HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
            [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
            
        }

        
    }
    
    //    _Headview.backgroundColor = [UIColor redColor];
    
    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = _Headview;
    //    [self.tableView reloadData];
    
    
    
}


- (CGSize)MYString:(NSString*) string StringSizeOfFont:(UIFont*)font
{
    CGSize size;
    if (isAfterIOS6) {
        size =  [string sizeWithFont:font forWidth:99999 lineBreakMode:NSLineBreakByCharWrapping];
        size.width += 50*kRateSize;
    }else
    {
        NSDictionary *attrs = @{NSFontAttributeName : font};
        size = [string boundingRectWithSize:CGSizeMake(30, 9999999) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    }
    
    return size;
}
- (NSString*)shortStringWithString:(NSString*)string
{
    NSString *anser;
    if (string.length>6) {
        anser = [NSString stringWithFormat:@"%@...",[string substringToIndex:6]];
    }else
    {
        anser = string;
    }
    
    return anser;
    
}
- (NSString*)secendtoTime:(NSInteger)secend
{
    NSString *string ;
    if (secend < 60) {
        string = [NSString stringWithFormat:@"00:00:%.2ld",(long)secend];
    }else
    {
        if (secend < 3600) {
            NSInteger min = (int)(secend / 60);
            NSInteger sec = secend % 60;
            {
                string = [NSString stringWithFormat:@"00:%.2ld:%.2ld",min,(long)sec];
            }
            
        }else
        {
            NSInteger hour = (int)(secend / 3600);
            NSInteger min = (int)((secend%3600) / 60);
            NSInteger sec = secend % 60;
            {
                string = [NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld",hour,min,(long)sec];
            }
        }
    }
    return string;
}
#pragma mark -
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end





















////111点播分类
//if ([type isEqualToString:VOD_CATEGORY_LIST]) {
//    //                NSLog(@"点播分类请求成功结果: %ld result:%@  type:%@",status,result,type );
//    NSInteger count  = [[result objectForKey:@"count"] integerValue];;
//    NSDictionary *dic = (NSDictionary*)result;
//    
//    //点播分类有数据的话   会请求分类下的数据
//    if (count) {
//        NSArray *array = [dic objectForKey:@"typeList"];
//        NSInteger indexNum = 4;
//        for (NSDictionary *list in array) {
//            
//            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//            [dic setObject:[list objectForKey:@"categoryID"] forKey:@"categoryID"];
//            [dic setObject:[list objectForKey:@"id"] forKey:@"id"];
//            [dic setObject:[list objectForKey:@"name"] forKey:@"title"];
//            [dic setObject:[NSArray arrayWithObject:@"vodwaiting"] forKey:@"Array"];
//            
//            [self.dataArray addObject:dic];
//            [self.tableView reloadData];
//            
//            //********这里会有好多个分类的。如果直接发出无数个请求会使无法判断哪一个回来的数据
//            //故采用懒加载的方式，变为改变了那个 ，接口的形式
//            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//            [request VODProgramListWithOperationCode:nil andRootCategpryID:[list objectForKey:@"categoryID"] andStartSeq:1 andCount:6 andTimeout:10 andIndex:indexNum];
//            
//            
//            //1.********这个是第几个请求，如果一共六个组，发送6个小请求，每一个用一个index标记
//            indexNum ++ ;
//            
//            [self.BOOLArray addObject:@"YES"];
//            
//        }
//        //2.********标记一共有多少个请求，记录总数，用于后面判断，首页一次大请求 是否完成
//        _reqCount = indexNum - 4;
//        
//    }
//}











////22222分类下的数据电视电影点播分类运营组
//if ([type isEqualToString:VOD_PROGRAM_LIST]) {
//    
//    
//    JIHAO++;
//    
//    NSInteger count  = [[result objectForKey:@"count"] integerValue];;
//    
//    NSDictionary *dic = (NSDictionary*)result;
//    NSInteger index = [[dic objectForKey:@"index"]integerValue];
//    
//    
//    if (index < self.dataArray.count) {
//        
//        NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:index];
//        
//        if (count > 0) {
//            NSArray *array = [dic objectForKey:@"vodList"];
//            if (array) {
//                [zidian setObject:array forKey:@"Array"];
//                
//            }
//            for (NSDictionary *list in array) {
//                NSLog(@"-------|| %ld %@列*表 \n%@ \n%@ \n%@\n",index,[zidian objectForKey:@"title"],[list objectForKey:@"seq"],[list objectForKey:@"name"],[list objectForKey:@"lastEpisode"]);
//            }
//        }else
//        {
//            [zidian setObject:[NSArray arrayWithObject:@"vodFinish"] forKey:@"Array"];
//        }
//        
//    }
//    
//    [self.tableView reloadData];
//    
//    
//    //3.********这是一个分请求，记录总数了，在这里判断，首页一次大请求 是否完成
//    //                if ([self requstFinishWithArray:[NSArray arrayWithObject:[self.dataArray subarrayWithRange:NSMakeRange(2,_reqCount-2)]]])
//    //                {
//    //                     NSLog(@"此时的总数2组：：：：：：：,self.dataArray");
//    //                    _reqFinish = YES;
//    //                }
//    NSLog(@"--||----%ld %ld",_reqCount,JIHAO);
//    if (_reqCount == JIHAO) {
//        NSLog(@"请求完成了鹅鹅鹅鹅鹅鹅饿鹅鹅鹅 ");
//        JIHAO = 0;
//        _reqFinish = YES;
//    }else
//    {
//        _reqFinish = NO;
//    }
//    
//}
//NSLog(@"%@",type);
