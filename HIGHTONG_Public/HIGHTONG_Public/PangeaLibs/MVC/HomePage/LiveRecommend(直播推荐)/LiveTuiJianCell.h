//
//  LiveTuiJianCell.h
//  HIGHTONG
//
//  Created by testteam on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
@interface LiveTuiJianCell : UITableViewCell

@property(nonatomic,strong)NSMutableArray *array ;//存放两个直播信息的数组

@property(nonatomic,strong)UIButton_Block *btn1;//进入观看1

@property(nonatomic,strong)UIButton_Block *btn2;//进入观看2

- (void)setUpView;

- (void)reloadCell;
@end
