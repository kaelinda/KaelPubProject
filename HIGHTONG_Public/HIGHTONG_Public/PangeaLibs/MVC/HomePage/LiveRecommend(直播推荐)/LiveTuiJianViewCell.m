//
//  LiveTuiJianViewCell.m
//  HIGHTONG
//
//  Created by testteam on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//



#import "LiveTuiJianViewCell.h"
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton_Block.h"
@interface LiveTuiJianViewCell()
{
    UIImageView *FirstBaseView;//第一个背景视图
    UIImageView *FirstTVLogo;//第一个TVloge
    UILabel *FirstTVName;//第一个TV名字
    UILabel *FirstLiveingTitle;//第一个的直播标题
    UILabel *FirstNextTitle;//第一个的后续节目标题
    UIImageView *isLivingFirst;//第一个正在播放按钮
    UIButton_Block *FirstBlock;//第一个的点击按钮
    UIImageView *firstLineView;//第一个分割图片线
    
    UIImageView *SecondBaseView;//第二个背景视图
    UIImageView *SecondTVLogo;//第二个TVloge
    UILabel *SecondTVName;//第二个TV名字
    UILabel *SecondLiveingTitle;//第二个的直播标题
    UILabel *SecondNextTitle;//第二个的后续节目标题
    UIImageView *isLivingSecond;//第一个正在播放按钮
    UIButton_Block *SecondBlock;//第二个的点击按钮
    UIImageView *SecondLineView;//第一个分割图片线
}
@end


@implementation LiveTuiJianViewCell

- (instancetype)init
{
    if([super init])
    {
        self.LiveMessageArray = [NSMutableArray array];
        [self setup];
    }
    return self;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.LiveMessageArray = [NSMutableArray array];
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setup
{
    UIView *View = self.contentView;
    
    FirstBaseView = [UIImageView new];
    [View addSubview:FirstBaseView];
    [FirstBaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(View.mas_left).offset(10*kRateSize);
        make.right.equalTo(View.mas_centerX).offset(-5*kRateSize);
        make.top.equalTo(View.mas_top).offset(2*kRateSize);
        make.bottom.equalTo(View.mas_bottom).offset(-3*kRateSize);
    }];
    
//    FirstBaseView.layer.borderWidth = 2;
//    FirstBaseView.layer.borderColor = UIColorFromRGB(0xf5f5f5).CGColor;
//    FirstBaseView.backgroundColor = [UIColor yellowColor];
    //TVName
    FirstTVName = [UILabel new];
    [FirstBaseView addSubview:FirstTVName];
    [FirstTVName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(FirstBaseView.mas_left);
        make.right.equalTo(FirstBaseView.mas_right);
        make.centerY.equalTo(FirstBaseView.mas_centerY);
        make.height.equalTo(@(20*kRateSize));
    }];
    FirstTVName.textAlignment = NSTextAlignmentCenter;
    //TVLogo
    FirstTVLogo = [UIImageView new];
    [FirstBaseView addSubview:FirstTVLogo];
    [FirstTVLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(FirstBaseView.mas_top);
        make.centerX.equalTo(FirstBaseView.mas_centerX);
        make.bottom.equalTo(FirstTVName.mas_top);
        make.left.equalTo(FirstBaseView.mas_left).offset(50*kRateSize);
        make.right.equalTo(FirstBaseView.mas_right).offset(-50*kRateSize);
    }];
    
    //FirstLiving
    FirstLiveingTitle = [UILabel new];
    [FirstBaseView addSubview:FirstLiveingTitle];
    [FirstLiveingTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(FirstTVName.mas_bottom).offset(2*kRateSize);
        make.left.equalTo(FirstBaseView.mas_left).offset((15+15)*kRateSize);
        make.right.equalTo(FirstBaseView.mas_right).offset(-15*kRateSize);
        make.height.equalTo(@(18*kRateSize));
    }];
    
    isLivingFirst = [UIImageView new];
    [FirstBaseView addSubview:isLivingFirst];
    [isLivingFirst mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(FirstBaseView.mas_left).offset(10*kRateSize);
        //        make.top.equalTo(FirstTVName.mas_bottom).offset(8*kRateSize);
        make.centerY.equalTo(FirstLiveingTitle.mas_centerY);
        make.width.equalTo(@(12*kRateSize));
        make.height.equalTo(@(12*kRateSize));
    }];
    isLivingFirst.image = [UIImage imageNamed:@"ic_home_hotlive_play_label"];//icon_BF公版改进
    
    firstLineView = [UIImageView new];
    [FirstBaseView addSubview:firstLineView];
    [firstLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(isLivingFirst.mas_left);
        make.right.equalTo(FirstLiveingTitle.mas_right);
        make.bottom.equalTo(FirstLiveingTitle.mas_top).offset(-2*kRateSize);
        make.height.equalTo(@(1));
    }];
    
    //FirstNext
    FirstNextTitle = [UILabel new];
    [FirstBaseView addSubview:FirstNextTitle];
    [FirstNextTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(FirstLiveingTitle.mas_bottom).offset(-2*kRateSize);
        make.left.equalTo(FirstBaseView.mas_left).offset((15+15)*kRateSize);
        make.right.equalTo(FirstBaseView.mas_right).offset(-15*kRateSize);
        make.height.equalTo(@(18*kRateSize));
    }];
    FirstBaseView.userInteractionEnabled = YES;
    FirstBlock = [UIButton_Block new];
    [FirstBaseView addSubview:FirstBlock];
    [FirstBlock mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(FirstBaseView);
    }];
    
    
    
    
    SecondBaseView = [UIImageView new];
    [View addSubview:SecondBaseView];
    [SecondBaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(FirstBaseView.mas_right).offset(5*kRateSize);
        make.right.equalTo(View.mas_right).offset(-10*kRateSize);
        make.top.equalTo(View.mas_top).offset(2*kRateSize);
        make.bottom.equalTo(View.mas_bottom).offset(-3*kRateSize);
    }];
//    SecondBaseView.layer.borderWidth = 2;
//    SecondBaseView.layer.borderColor = UIColorFromRGB(0xf5f5f5).CGColor;
//    SecondBaseView.backgroundColor = [UIColor yellowColor];
    //TVName
    SecondTVName = [UILabel new];
    [SecondBaseView addSubview:SecondTVName];
    [SecondTVName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SecondBaseView.mas_left);
        make.right.equalTo(SecondBaseView.mas_right);
        make.centerY.equalTo(SecondBaseView.mas_centerY);
        make.height.equalTo(@(22*kRateSize));
    }];
    SecondTVName.textAlignment = NSTextAlignmentCenter;
    //TVLogo
    SecondTVLogo = [UIImageView new];
    [SecondBaseView addSubview:SecondTVLogo];
    [SecondTVLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(SecondBaseView.mas_top);
        make.centerX.equalTo(SecondBaseView.mas_centerX);
        make.bottom.equalTo(SecondTVName.mas_top);
        make.left.equalTo(SecondBaseView.mas_left).offset(50*kRateSize);
        make.right.equalTo(SecondBaseView.mas_right).offset(-50*kRateSize);
    }];
    
    //SecondLiving
    SecondLiveingTitle = [UILabel new];
    [SecondBaseView addSubview:SecondLiveingTitle];
    [SecondLiveingTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(SecondTVName.mas_bottom).offset(2*kRateSize);
        make.left.equalTo(SecondBaseView.mas_left).offset((15+15)*kRateSize);
        make.right.equalTo(SecondBaseView.mas_right).offset(-15*kRateSize);
        make.height.equalTo(@(18*kRateSize));
    }];
    
    isLivingSecond = [UIImageView new];
    [SecondBaseView addSubview:isLivingSecond];
    [isLivingSecond mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SecondBaseView.mas_left).offset(10*kRateSize);
//        make.top.equalTo(SecondTVName.mas_bottom).offset(8*kRateSize);
        make.centerY.equalTo(SecondLiveingTitle.mas_centerY);
        make.width.equalTo(@(12*kRateSize));
        make.height.equalTo(@(12*kRateSize));
    }];
    isLivingSecond.image = [UIImage imageNamed:@"ic_home_hotlive_play_label"];
    
    SecondLineView = [UIImageView new];
    [SecondBaseView addSubview:SecondLineView];
    [SecondLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(isLivingSecond.mas_left);
        make.right.equalTo(SecondLiveingTitle.mas_right);
        make.bottom.equalTo(SecondLiveingTitle.mas_top).offset(-2*kRateSize);
        make.height.equalTo(@(1));
    }];
    UIImageView *VerticalLine = [UIImageView new];
    [View addSubview:VerticalLine];
    VerticalLine.image = [UIImage imageNamed:@"line_hotepg"];
//    VerticalLine.backgroundColor = UIColorFromRGB(0xf7f7f7);
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(FirstBaseView.mas_right).offset(3*kRateSize);
//        make.right.equalTo(View.mas_right).offset(-10*kRateSize);
        make.width.equalTo(@(1));
//        make.top.equalTo(View.mas_top).offset(12*kRateSize);
        make.height.equalTo(@(60*kRateSize));
        make.bottom.equalTo(View.mas_bottom).offset(-2*kRateSize);

    }];
    
    
    //SecondNext
    SecondNextTitle = [UILabel new];
    [SecondBaseView addSubview:SecondNextTitle];
    [SecondNextTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(SecondLiveingTitle.mas_bottom).offset(-2*kRateSize);
        make.left.equalTo(SecondBaseView.mas_left).offset((15+15)*kRateSize);
        make.right.equalTo(SecondBaseView.mas_right).offset(-15*kRateSize);
        make.height.equalTo(@(18*kRateSize));
    }];
    SecondBaseView.userInteractionEnabled = YES;
    SecondBlock = [UIButton_Block new];
    [SecondBaseView addSubview:SecondBlock];
    [SecondBlock mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(SecondBaseView);
    }];
    
    
    
    [FirstBlock setImage:[UIImage imageNamed:@"sl_home_gridview_pressed"] forState:UIControlStateHighlighted];
    [SecondBlock setImage:[UIImage imageNamed:@"sl_home_gridview_pressed"] forState:UIControlStateHighlighted];

    {
        FirstTVName.font = [UIFont systemFontOfSize:11];
        FirstLiveingTitle.font = [UIFont systemFontOfSize:13];
        FirstNextTitle.font = [UIFont systemFontOfSize:13];
        FirstBaseView.image = [UIImage imageNamed:@"加载底图"];
        FirstTVLogo.image =  [UIImage imageNamed:@"bg_home_hotchannel_onloading"];
        isLivingFirst.hidden = YES;
        firstLineView.hidden = YES;
//        FirstLiveingTitle.textColor =App_selected_color;
        FirstLiveingTitle.textColor = UIColorFromRGB(0x000000);;
        FirstNextTitle.textColor = UIColorFromRGB(0x999999);
        FirstTVLogo.contentMode = UIViewContentModeScaleAspectFit;
        SecondTVLogo.contentMode = UIViewContentModeScaleAspectFit;
        
        
        SecondTVName.font = [UIFont systemFontOfSize:11];
        SecondLiveingTitle.font = [UIFont systemFontOfSize:13];
        SecondNextTitle.font = [UIFont systemFontOfSize:13];
        SecondTVLogo.image =  [UIImage imageNamed:@"bg_home_hotchannel_onloading"];
        SecondBaseView.image = [UIImage imageNamed:@"加载底图"];
        isLivingSecond.hidden = YES;
        SecondLineView.hidden = YES;
//        SecondLiveingTitle.textColor =App_selected_color;
        SecondLiveingTitle.textColor = UIColorFromRGB(0x000000);;
        SecondNextTitle.textColor =UIColorFromRGB(0x999999);
    }
    //调试
    { 
//        FirstBaseView.backgroundColor = [UIColor lightGrayColor];
//        FirstTVName.backgroundColor = [UIColor redColor];
//        FirstTVName.text = @"中央一套11";
//        FirstLiveingTitle.textColor = [UIColor blackColor];
//        FirstNextTitle.backgroundColor = [UIColor grayColor];
//        FirstLiveingTitle.backgroundColor = [UIColor yellowColor];
//        FirstNextTitle.backgroundColor = [UIColor grayColor];
//        FirstTVLogo.backgroundColor = [UIColor blueColor];
//
//        
//        SecondBaseView.backgroundColor = [UIColor yellowColor];
//        SecondTVName.backgroundColor = [UIColor whiteColor];
//        SecondTVName.text = @"北京卫视";
//        SecondLiveingTitle.textColor = [UIColor blackColor];
//        SecondNextTitle.textColor = [UIColor yellowColor];
//        SecondLiveingTitle.backgroundColor = [UIColor blackColor];
//        SecondNextTitle.backgroundColor = [UIColor grayColor];
//        SecondTVLogo.backgroundColor = [UIColor blueColor];
        
    }
    
}

//.刷新cell
- (void)reloadCell
{
    WS(wself);

    NSLog(@"--%@",self.LiveMessageArray);
    FirstLiveingTitle.text = @"暂无节目信息";
    FirstNextTitle.text = @"暂无节目信息";
    
    SecondLiveingTitle.text = @"暂无节目信息";
    SecondNextTitle.text = @"暂无节目信息";
    
    FirstLiveingTitle.hidden = YES;
    FirstNextTitle.hidden =  YES;
    SecondNextTitle.hidden = YES;
    SecondLiveingTitle.hidden = YES;
    
    isLivingFirst.hidden = YES;
    isLivingSecond.hidden = YES;
    if(self.LiveMessageArray.count >= 1)
    {
        FirstLiveingTitle.hidden = NO;
        FirstNextTitle.hidden =  NO;
//        FirstBaseView.image = [UIImage imageNamed:@"bg_RDZB"];//公版改版
        FirstBaseView.hidden = NO;
        NSLog(@"%@",self.LiveMessageArray);
        NSDictionary *firstDiction = [self.LiveMessageArray objectAtIndex:0];
        NSArray *epginfo = [firstDiction objectForKey:@"epginfo"];
        NSString *imageLink = [firstDiction objectForKey:@"imageLink"];
        NSString *serviceName = [firstDiction objectForKey:@"serviceName"];
        
        FirstBlock.Diction = firstDiction;
        FirstBlock.Click = ^(UIButton_Block*btn,NSString *string)
        {
            if(wself.BLOCK)
            {
                wself.BLOCK(btn,serviceName);
            }else
            {
                NSLog(@"推荐cell没有实现block");
            }
        };
        
        
       
        
        FirstBlock.hidden = NO;
    
        firstLineView.hidden = NO;
        [FirstTVLogo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)] placeholderImage:[UIImage imageNamed:@"bg_home_hotchannel_onloading"]];
        FirstTVName.text = serviceName;
        if(epginfo.count >= 1)
        {
            NSDictionary *epginfoFirst = epginfo[0];
            NSString *eventName = [epginfoFirst objectForKey:@"eventName"];
            NSString *startTime = [epginfoFirst objectForKey:@"startTime"];
            eventName = [NSString stringWithFormat:@"%@  %@",startTime,eventName];
            FirstLiveingTitle.text = eventName;
            if (serviceName.length) {
                isLivingFirst.hidden = NO;
            }
        
        }else
        {
//            FirstBlock.hidden = YES;
            isLivingFirst.hidden = YES;
//            FirstLiveingTitle.text = @"";
        }
        if(epginfo.count >= 2)
        {
            NSDictionary *epginfoSecond = epginfo[1];
            NSString *eventName = [epginfoSecond objectForKey:@"eventName"];
            NSString *startTime = [epginfoSecond objectForKey:@"startTime"];
            eventName = [NSString stringWithFormat:@"%@  %@",startTime,eventName];
            FirstNextTitle.text = eventName;

        }else
        {
//            FirstBlock.hidden = YES;
            isLivingSecond.hidden = YES;
//            FirstNextTitle.text = @"";
//            FirstBaseView.image = [UIImage imageNamed:@"sl_home_gridview_pressed"];
        }
    }else
    {
         FirstBlock.hidden = YES;
        FirstBaseView.image = nil;
        FirstBaseView.hidden = YES;
    }
    
    
    if(self.LiveMessageArray.count >= 2)
    {
        SecondNextTitle.hidden = NO;
        SecondLiveingTitle.hidden = NO;
//        SecondBaseView.image = [UIImage imageNamed:@"bg_RDZB"];//公版改版
        SecondBaseView.hidden = NO;
        NSLog(@"%@",self.LiveMessageArray);
        NSDictionary *firstDiction = [self.LiveMessageArray objectAtIndex:1];
        NSArray *epginfo = [firstDiction objectForKey:@"epginfo"];
        NSString *imageLink = [firstDiction objectForKey:@"imageLink"];
        NSString *serviceName = [firstDiction objectForKey:@"serviceName"];
        
        SecondBlock.Diction = firstDiction;
        SecondBlock.Click = ^(UIButton_Block*btn,NSString *string)
        {
            if(wself.BLOCK)
            {
                wself.BLOCK(btn,serviceName);
            }else
            {
                NSLog(@"推荐cell没有实现block");
            }
        };
        
        

        SecondBlock.hidden = NO;
        SecondLineView.hidden = NO;
        [SecondTVLogo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)]placeholderImage:[UIImage imageNamed:@"bg_home_hotchannel_onloading"]];
        SecondTVName.text = serviceName;
        if(epginfo.count >= 1)
        {
            NSDictionary *epginfoFirst = epginfo[0];
            NSString *eventName = [epginfoFirst objectForKey:@"eventName"];
            NSString *startTime = [epginfoFirst objectForKey:@"startTime"];
            eventName = [NSString stringWithFormat:@"%@  %@",startTime,eventName];
            SecondLiveingTitle.text = eventName;
            if (serviceName.length) {
                isLivingSecond.hidden = NO;
            }
        }else
        {
//            SecondBlock.hidden = YES;
            isLivingSecond.hidden = YES;
//            SecondLiveingTitle.text = @"";
//            SecondBaseView.image = [UIImage imageNamed:@"sl_home_gridview_pressed"];
        }
        if(epginfo.count >= 2)
        {
            NSDictionary *epginfoSecond = epginfo[1];
            NSString *eventName = [epginfoSecond objectForKey:@"eventName"];
            NSString *startTime = [epginfoSecond objectForKey:@"startTime"];
            eventName = [NSString stringWithFormat:@"%@  %@",startTime,eventName];
            SecondNextTitle.text = eventName;
            
        }else
        {
//            SecondBlock.hidden = YES;
//            SecondNextTitle.text = @"";
        }
    }else
    {
        SecondBlock.hidden = YES;
        SecondBaseView.image = nil;
          SecondBaseView.hidden = YES;
    }
}

@end
