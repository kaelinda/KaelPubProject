//
//  LiveTuiJianViewCell.h
//  HIGHTONG
//
//  Created by testteam on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UIButton_Block;
typedef  void (^TuijianBlock) (UIButton_Block*,NSString*);
@interface LiveTuiJianViewCell : UITableViewCell

@property (nonatomic,strong)NSMutableArray* LiveMessageArray;//直播cell的数组

@property (nonatomic,copy)TuijianBlock BLOCK;//直播cell的block

@property (nonatomic,strong)NSDictionary* LiveFirstDiction;//直播cell的第一个频道
@property (nonatomic,strong)NSDictionary* LiveSecondDiction;//直播cell的第二个频道

- (void)reloadCell;

@end
