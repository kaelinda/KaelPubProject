//
//  FavoriteView.m
//  HIGHTONG
//
//  Created by testteam on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FavoriteView.h"
#import "UIImageView+AFNetworking.h"
@interface FavoriteView()
{

    NSDictionary *diction;
}
@end
#define kImageH (48*kRateSize)
#define kImageW (55*kRateSize)

@implementation FavoriteView

- (id)initWithDition:(NSDictionary *)dic
{
    if(self = [super init])
    {
        diction = dic;
        [self setup];
    }
    
    return self;
}
- (void)setup
{
    UIImageView *imagev = [UIImageView new];
    [self addSubview:imagev];
    [imagev mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5*kRateSize);
        make.right.equalTo(self.mas_right).offset(-5*kRateSize);
        make.top.equalTo(self.mas_top).offset(5*kRateSize);
        make.height.equalTo(@(kImageH));
    }];
//    imagev.backgroundColor = [UIColor redColor];
    imagev.contentMode = UIViewContentModeScaleAspectFit;
    UILabel *label = [UILabel new];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(2);
        make.right.equalTo(self.mas_right).offset(-2);
        make.top.equalTo(imagev.mas_bottom).offset(0*kRateSize);
        make.bottom.equalTo(self.mas_bottom).offset(0*kRateSize);
    }];
//    label.backgroundColor = [UIColor grayColor];
    if ([diction isKindOfClass:[NSDictionary class]]) {
    
   
    
    NSString *imageLink = [diction objectForKey:@"imageLink"];
    NSString *name = [diction objectForKey:@"name"];
//    NSString *levelImageLink = [diction objectForKey:@"levelImageLink"];
    
    [imagev sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];//公版改版
    label.backgroundColor = [UIColor whiteColor];
    label.text = name;
    label.font = [UIFont systemFontOfSize:12];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor =[ UIColor clearColor];
        
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
