//
//  FavoriteChannelViewCell.h
//  HIGHTONG
//
//  Created by testteam on 15/9/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^FAVORITEBLOCK) (NSDictionary *);
@interface FavoriteChannelViewCell : UITableViewCell
/**
 *  实现接收喜爱cell点击的某一个视图内容,(NSDictionary* dic)
 */
@property (nonatomic,copy)FAVORITEBLOCK BLOCK;

/**
 *  存放喜爱频道数组
 */
@property (nonatomic,strong)NSMutableArray *favoriteArray;//喜爱频道数组

/**
 *  已经设置完喜爱频道数组，更新数据
 */
- (void)updataInfo;

@end
