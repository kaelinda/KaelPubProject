//
//  FavoriteView.h
//  HIGHTONG
//
//  Created by testteam on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteView : UIImageView

- (instancetype)initWithDition:(NSDictionary*)dic;

@end
