//
//  NoFavoriteChannelViewCell.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/10/14.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "NoFavoriteChannelViewCell.h"

@implementation NoFavoriteChannelViewCell



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self setupView];
    }
    return self;
}
- (void)setupView
{
    UIView *contentView = self.contentView;
    UILabel *NoFavorText = [[UILabel alloc]initWithFrame:CGRectMake((kDeviceWidth/4)*kRateSize, 23*kRateSize, kDeviceWidth/2, 40*kRateSize)];
    NoFavorText.text =  @"您还没有喜爱频道哦!";
    
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((kDeviceWidth/4-30)*kRateSize, 30*kRateSize, 24*kRateSize, 24*kRateSize)];
    imageView.image = [UIImage imageNamed:@"ic_user_favchannel_label"];
    
    [contentView addSubview:imageView];
    [contentView addSubview:NoFavorText];
    contentView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
    
}


- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
