//
//  FavoriteChannelViewCell.m
//  HIGHTONG
//
//  Created by testteam on 15/9/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FavoriteChannelViewCell.h"
#import "UIButton_Block.h"
#import "FavoriteView.h"
#define kkWidth (70*kRateSize)

@interface FavoriteChannelViewCell()
{
    
}

@property (nonatomic,strong)UIScrollView * scrollView;
@end
@implementation FavoriteChannelViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if([super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        _favoriteArray = [NSMutableArray array];
    }
    return self;
}


//更新数据
- (void)updataInfo
{
    UIView *Baseview = self.contentView;
    [Baseview removeAllSubviews];
    self.scrollView = [UIScrollView new];
    [Baseview addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(Baseview);
    }];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    UIView *contentView = [UIView new];
    UIView *BigView = contentView;
    [self.scrollView addSubview:BigView];
    [BigView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.height.equalTo(self.scrollView.mas_height);
    }];
    
    
//    BigView.backgroundColor = [UIColor lightGrayColor];
    UIButton_Block *lastView;
    for(NSInteger i=0; i<self.favoriteArray.count;i++)
    {
        
        NSDictionary *dic = self.favoriteArray[i];
        FavoriteView *fav = [[FavoriteView alloc]initWithDition:dic];
        [BigView addSubview:fav];
        [fav mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastView? lastView.mas_right : BigView.mas_left).offset(lastView? 4*kRateSize : 6*kRateSize);
            make.top.equalTo(BigView.mas_top).offset(3);
            make.bottom.equalTo(BigView.mas_bottom).offset(-7);
            make.width.equalTo(@kkWidth);
        }];
//        fav.layer.borderColor = UIColorFromRGB(0xf5f5f5).CGColor;
//        fav.layer.borderWidth = 2;
//        fav.image = [UIImage imageNamed:@"bg_XAPD"];//公版改版
        
        fav.userInteractionEnabled =YES;
        UIButton_Block *btn = [UIButton_Block new];
        [fav addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(fav);
        }];
//        fav.backgroundColor = [UIColor redColor];
        
        [btn setImage:[UIImage imageNamed:@"sl_home_gridview_pressed"] forState:UIControlStateHighlighted];
        btn.serviceIDName = [NSString stringWithFormat:@"第%ld个",i];
        if (i > 0) {
            UIImageView *VerticalLine = [[UIImageView alloc]init];
            [BigView addSubview:VerticalLine];
            VerticalLine.image = [UIImage imageNamed:@"line_hotepg"];
//            VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
            [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastView? lastView.mas_right : BigView.mas_left).offset(lastView? (3)*kRateSize : (5)*kRateSize);
                make.top.equalTo(BigView.mas_top).offset(9);
                make.bottom.equalTo(BigView.mas_bottom).offset(-9);
                make.width.equalTo(@(1));
            }];
        }
        
        
        lastView  = btn;
//        btn.backgroundColor = [UIColor grayColor];
        btn.Click = ^(UIButton_Block*btn,NSString*name)
        {
            NSLog(@"zhujiang %@",btn.serviceIDName);
            if(_BLOCK)
            {
                NSDictionary *dic = self.favoriteArray[i];
                _BLOCK(dic);
            }else
            {
                NSLog(@"没有实现喜爱频道点击接收");
            }
        };
    }
    
    if(self.favoriteArray.count)
    {
        [BigView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(lastView.mas_right).offset(4*kRateSize);
        }];
    }
    
    
    
    
}

@end
