//
//  ProgranListCell.m
//  HIGHTONG
//
//  Created by testteam on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ProgramListCell.h"
#import "Masonry.h"
#import "UIButton_Block.h"
@implementation ProgramListCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.dateArray = [NSMutableArray array];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    WS(wself);
    
    UIView *superView = self.contentView;
    self.leftView = [ProgramView new];
    [superView addSubview:self.leftView];
    
    self.middleView = [ProgramView new];
    [superView addSubview:self.middleView];
    
    self.rightView = [ProgramView new];
    [superView addSubview:self.rightView];
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(superView.mas_left).offset(4*kRateSize);
//        make.top.equalTo(superView.mas_top).offset(5*kRateSize);
//        make.bottom.equalTo(superView.mas_bottom).offset(-0*kRateSize);
//        make.width.equalTo(self.middleView);
//        make.width.equalTo(self.rightView);
        make.left.equalTo(superView.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(superView.mas_top).offset(5*kDeviceRate);
        make.bottom.equalTo(superView.mas_bottom).offset(-0*kRateSize);
        make.width.equalTo(self.middleView);
        make.width.equalTo(self.rightView);
    }];
    [self.leftView updata];
    self.leftView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"zou视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftView.mas_right).offset(3*kDeviceRate);
        make.top.equalTo(superView.mas_top).offset(5*kDeviceRate);
        make.bottom.equalTo(superView.mas_bottom).offset(-0*kRateSize);
        make.width.equalTo(self.rightView);
        make.width.equalTo(self.leftView);
    }];
    [self.middleView updata];
    self.middleView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"zhong视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.middleView.mas_right).offset(3*kDeviceRate);
        make.right.equalTo(superView.mas_right).offset(-6*kDeviceRate);
        make.top.equalTo(superView.mas_top).offset(5*kDeviceRate);
        make.bottom.equalTo(superView.mas_bottom).offset(-0*kRateSize);
        make.width.equalTo(self.rightView);
        make.width.equalTo(self.middleView);
    }];
    [self.rightView updata];
    self.rightView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"you视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
//    self.leftView.backgroundColor = [UIColor redColor];
    
//    superView.backgroundColor = [UIColor grayColor];
}
- (void)update
{
    
//    NSLog(@"0.5时间%@",[NSDate date]);
    if (self.dateArray.count >= 1) {
        NSDictionary *dic = self.dateArray[0];
//        NSLog(@"%d: %@",0 ,dic );
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.leftView.dic = dic;
            self.middleView.dic = nil;
            self.rightView.dic = nil;
            
            self.leftView.isMovie = self.isMovie;
           
            [self.leftView updata];
            [self.middleView updata];
            [self.rightView updata];
        }
    }
    if (self.dateArray.count >= 2) {
        NSDictionary *dic = self.dateArray[1];
//        NSLog(@"%d: %@",1 ,dic );
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.middleView.dic = dic;
            self.rightView.dic = nil;
            
            self.middleView.isMovie = self.isMovie;
            
            [self.leftView updata];
            [self.middleView updata];
            [self.rightView updata];
        }
    }
    if (self.dateArray.count >= 3) {
        NSDictionary *dic = self.dateArray[2];
//        NSLog(@"%d: %@",2,dic );
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.rightView.dic = dic;
            self.rightView.isMovie = self.isMovie;
            [self.leftView updata];
            [self.middleView updata];
            [self.rightView updata];
        }
    }
    
}
@end
