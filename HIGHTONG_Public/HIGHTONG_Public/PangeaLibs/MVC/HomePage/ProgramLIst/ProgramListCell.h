//
//  ProgranListCell.h
//  HIGHTONG
//
//  Created by testteam on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProgramView.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class ProgramListCell;
typedef void (^ProgramCellBlock) (UIButton_Block*);

@interface ProgramListCell : UITableViewCell

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)ProgramCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)ProgramView *leftView;//左边

@property (nonatomic,strong)ProgramView *middleView;//中间

@property (nonatomic,strong)ProgramView *rightView;//右边

//@property (assign,nonatomic)CGFloat interval;//间隙默认

- (void)update;//跟新数据

@end
