//
//  UIButton_Block.m
//  HT辅助test
//
//  Created by testteam on 15/8/7.
//  Copyright (c) 2015年 cap. All rights reserved.
//

#import "UIButton_Block.h"


@implementation UIButton_Block
@synthesize serviceIDName = _serviceIDName;
- (id)init{
    if (self = [super init]) {
        [self addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)click:(UIButton_Block*)btn
{
    
    if (_Click) {
        NSLog(@"已经实现Block");
        _Click(btn,_serviceIDName);
        
    }else
    {
        NSLog(@"没有实现Block");
    }
    
}
@end
