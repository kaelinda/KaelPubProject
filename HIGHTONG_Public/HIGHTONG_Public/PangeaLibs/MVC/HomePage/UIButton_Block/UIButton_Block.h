//
//  UIButton_Block.h
//  HT辅助test
//
//  Created by testteam on 15/8/7.
//  Copyright (c) 2015年 cap. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIButton_Block;

typedef void (^BlockBtn)(UIButton_Block*,NSString*);

@interface UIButton_Block : UIButton

/**
 *  void (^BlockBtn) (UIButton_Block*,NSString*);
 */
@property (nonatomic,copy)BlockBtn Click;

@property (strong,nonatomic)NSString *dateStr;

@property (nonatomic,strong)NSDictionary* Diction;

@property (nonatomic,copy)NSString* serviceIDName;

@property (nonatomic,copy)NSString *eventName;

@property (nonatomic,assign)BOOL *isVarity;

@property (nonatomic,copy)NSString *channelID;//频道ID  zzs新加

@property(nonatomic,assign)BOOL isYuYue;//是否预约节目

@property(nonatomic,assign)BOOL isLive;//是否直播节目

@property (nonatomic,copy)NSString *polyMedicType;//医视频的TYPE

@property (nonatomic,copy)NSMutableDictionary *polyMedicDic;
@end
