//
//  YJHealthVideoSecondViewcontrol.m
//  HIGHTONG
//
//  Created by HTYunjiang on 16/5/12.
//  Copyright (c) 2016年 创维海通. All rights reserved.
//

#import "YJHealthVideoSecondViewcontrol.h"
#import "LoginViewController.h"
#import "MyGesture.h"
#import "CalculateTextWidth.h"
#import "MineVodTableViewCell.h"
#import "CustomAlertView.h"
#import "DataPlist.h"
#import "DemandVideoPlayerVC.h"
#import "MBProgressHUD.h"
#import "SelectAndDeleteView.h"
#import "MJRefresh.h"
#import "HTVideoAVPlayerVC.h"
#import "YJProgramListCell.h"
#import "TMPDataManager.h"
#import "HealthVodCollectionModel.h"

@interface YJHealthVideoSecondViewcontrol ()<UIScrollViewDelegate,HTRequestDelegate,UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    // 跳到  小屏幕 所需数据
    NSString *_channelID;     // 频道编号
    NSString *_channelName;   // 频道名称
    NSString *_eventId;   // 节目 id
    NSString *_eventName;   //节目名称
    NSString *_channelImageLink;//频道图标链接
    BOOL firstLoad;//第一次加载
    NSInteger pageNo;//第一次加载
    
    BOOL toDemandPlayer;//防止多次点击点播cell，多次push
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    BOOL _LastPage;//是否支持点播
}

@property (nonatomic,strong)UIButton *backBtn;//返回按钮
@property (nonatomic,strong)UIButton *requestYJVideoBtn;//重新请求按钮
@property (nonatomic,strong)UILabel *noDatetipLb;//无数据提醒


@end
#define requestCount @"12"
@implementation YJHealthVideoSecondViewcontrol
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSMutableArray *)vodArr
{
    if (!_vodArr) {
        _vodArr = [NSMutableArray array];
    }
    
    return _vodArr;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self showLoading];
    
    if (firstLoad) {
        [self loadData];
    }
    firstLoad = YES;


    [MobCountTool pushInWithPageView:@"MineCollectPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineCollectPage"];
    
}


- (void)refreshYJVideoTableCell
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    if (isUsePolymedicine_2_0) {
        if (self.itemID.length == 0) {
            [request YJHealthyVodProgramListWithChannelId:self.categoryID withPageNo:[NSString stringWithFormat: @"%ld", (long)pageNo] withPageSize:@"20" withKeyword:nil withFilter:nil];
        }else if (self.selectID.length == 0)
        {
            NSMutableArray *filter = [[NSMutableArray alloc]init];
            
            NSMutableArray *tempArr = [[NSMutableArray alloc]init];
            
            [tempArr addObject:_itemID];
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.categoryID,@"id",tempArr,@"items", nil];
            
            [filter addObject:paramDic];
            
            [request YJHealthyVodProgramListWithChannelId:self.categoryID withPageNo:[NSString stringWithFormat: @"%ld", (long)pageNo] withPageSize:@"20" withKeyword:nil withFilter:filter];

        }
        else
        {
            NSMutableArray *filter = [[NSMutableArray alloc]init];
            
            NSMutableArray *tempArr = [[NSMutableArray alloc]init];
            
            [tempArr addObject:_itemID];
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.selectID,@"id",tempArr,@"items", nil];
            
            [filter addObject:paramDic];
            
            [request YJHealthyVodProgramListWithChannelId:self.categoryID withPageNo:[NSString stringWithFormat: @"%ld", (long)pageNo] withPageSize:@"20" withKeyword:nil withFilter:filter];
        }
    }
    else
    {
            [request YJHealthyHealthyVideoListWithGlobal:@"1" withInstanceCode:KInstanceCode withPageNo:[NSString stringWithFormat: @"%ld", (long)pageNo] withPageSize:@"20" withTypeId:self.categoryID withOrder:@"" withYear:@"" withTagId:@""];
    }
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageNo = 1;
    
    _LastPage = YES;
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    _selAllBtnIsClick = NO;
    
    __vodIsEditing = NO;
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    
    [self setNaviBarTitle:self.name];
    
    self.view.backgroundColor = App_background_color;


    [self makeTheMainPartUI];
    
    [self refreshUISetup];
    
    [self refreshYJVideoTableCell];
}

- (void)refreshUISetup
{
    _vodTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
}


- (void)showLoading
{
    [self showCustomeHUD];
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}


- (void)hideLoading
{
    if (_vodArr.count>0) {
        
        _noDatetipLb.hidden = YES;
    }else{
        if (_requestYJVideoBtn.hidden) {
            _noDatetipLb.hidden = NO;
        }
    }
    [self hiddenCustomHUD];
}


- (void)makeTheMainPartUI
{
    _noVODDataLabel = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight) andImgName:@"bg_user_collection_metadata" andTitle:@"暂无收藏记录"];
    _noVODDataLabel.hidden = YES;
    [self.view addSubview:_noVODDataLabel];
    
    
    //点播table
    _vodTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace)style:UITableViewStyleGrouped];
    _vodTable.delegate = self;
    _vodTable.dataSource = self;
    _vodTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _vodTable.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_vodTable];
    _vodTable.showsHorizontalScrollIndicator = NO;
    _vodTable.showsVerticalScrollIndicator = NO;
    
    _requestYJVideoBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace)];
    [_requestYJVideoBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
    [_requestYJVideoBtn setBackgroundColor:App_background_color];
    [self.view addSubview:_requestYJVideoBtn];
    [_requestYJVideoBtn addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    _requestYJVideoBtn.hidden = YES;

    //空数据页提示信息
    _noDatetipLb = [[UILabel alloc]init];
    _noDatetipLb.text = @"暂无数据";
    _noDatetipLb.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:_noDatetipLb];
    
    [_noDatetipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    _noDatetipLb.hidden = YES;
}


#pragma mark 请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    switch (status) {
        case 0:
            if ([type isEqualToString:YJ_HEALTHY_HEALTHYVIDEOLIST]){//医聚医视频专题列表
                
                NSLog(@"healthy/healthyVideolist结果: %ld result:%@  type:%@",status,result,type );
                if ([[result objectForKey:@"ret"] integerValue] == 0){
                    
                    pageNo++;//如果请求成功，则页码自增
                    
                    NSDictionary *resDIC = result;
                    
                    [self.vodArr addObjectsFromArray:[resDIC objectForKey:@"list"]];
                    NSLog(@"%@",self.vodArr);
                    
                    if (self.vodArr.count) {
                        [_vodTable reloadData];
                    }
                    else
                    {
                        _noDatetipLb.hidden = NO;
                    }
                }
                else
                {
                    
                    if (self.vodArr.count == 0) {
                        _requestYJVideoBtn.hidden = NO;
                    }
   
                }
                [self hideLoading];
            }
            if ([type isEqualToString:YJ_HEALTHYVOD_PROGRAMLIST]){//医聚医视频专题列表
                
                NSLog(@"YJ_HEALTHYVOD_PROGRAMLIST结果: %ld result:%@  type:%@",status,result,type );
                if ([[result objectForKey:@"ret"] integerValue] == 0){
                    
                    if ([[result objectForKey:@"lastPage"]integerValue] == 0) {
                        pageNo++;//如果请求成功，则页码自增
                        
                        NSDictionary *resDIC = result;
                        
                        [self.vodArr addObjectsFromArray:[resDIC objectForKey:@"list"]];
                    }
                    else if(_LastPage)
                    {
                        NSDictionary *resDIC = result;
                        
                        _LastPage = NO;
                        
                        [self.vodArr addObjectsFromArray:[resDIC objectForKey:@"list"]];
                    }
                    

                    NSLog(@"%@",self.vodArr);
                    
                    if (self.vodArr.count) {
                        [_vodTable reloadData];
                    }
                    else
                    {
                        _noDatetipLb.hidden = NO;
                    }
                }
                else
                {
                    
                    if (self.vodArr.count == 0) {
                        _requestYJVideoBtn.hidden = NO;
                    }
                    
                }
                [self hideLoading];
            }
            break;
            
        default:
            if ([type isEqualToString:YJ_HEALTHY_HEALTHYVIDEOLIST] && [type isEqualToString:YJ_HEALTHYVOD_PROGRAMLIST]){
                if (self.vodArr.count == 0) {
                    _requestYJVideoBtn.hidden = NO;
                }
                
                [self hideLoading];
            }
            break;
    }
}


#pragma mark tableview delegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5*kDeviceRate;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //整个header
    UIView *sectionHeaderView = [UIView new];
    sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, 5*kDeviceRate);
    [sectionHeaderView setBackgroundColor:App_white_color];
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _mediaHight = 141*kDeviceRate;

    return _mediaHight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    
    NSLog(@"eeeeeeeeee-%ld",(long)[self numberOfCellRowsWithArray:self.vodArr]);
    
    return [self numberOfCellRowsWithArray:self.vodArr];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *YJProgranidentifier = @"YJProgranidentifier";
    YJProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:YJProgranidentifier];
    if (!cell) {
        cell = [[YJProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:YJProgranidentifier];
    }
    NSArray * array = self.vodArr;
    
    NSLog(@"rrrrrrrrrrrrrr---%@",array);
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;

    
    cell.dateArray.array = [self arrayFOR_ALLArray:array withBeginnumb:indexPath.row];

    [cell update];
    
    cell.block = ^(UIButton_Block*btn)
    {
        NSLog(@"ttttttttttt－－－%@ %@",btn.serviceIDName,btn.eventName);
        
        __block NSMutableArray *tempHealthVodList = [NSMutableArray array];
        
        [self.vodArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            
            dic = obj;
            
            if ([btn.serviceIDName isEqualToString:[dic objectForKey:@"id"]]) {
                
                if (self.vodArr.count - idx - 1 >= 10) {
                    
                    if (idx <= 9) {
                        
                        tempHealthVodList = [self.vodArr subarrayWithRange:NSMakeRange(0, idx+10 + 1)].copy;
                        
                    }else
                    {
                        
                        tempHealthVodList = [self.vodArr subarrayWithRange:NSMakeRange(idx-10, 21)].copy;
                        
                    }
                
                }else
                {
                    
                    if (idx <= 9) {
                        
                        tempHealthVodList = self.vodArr.copy;
                        
                    }else
                    {
                        
                        tempHealthVodList = [self.vodArr subarrayWithRange:NSMakeRange(idx-10, self.vodArr.count-idx+10)].copy;
                        
                    }
                    
                }
                
            }
        }];

        HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
        
        HTVC.recommendArray = tempHealthVodList;
        
        HTVC.polyMedicDic = btn.polyMedicDic;
        NSLog(@"gggggg－－－%@",btn.polyMedicDic);
        HTVC.titleStr = btn.eventName;
        if (!isUsePolymedicine_2_0) {
            HTVC.polyMedicType = btn.polyMedicType;
        }
        
        NSString *ENtype = [NSString stringWithFormat:@"0"];
        
        NSString *TrackId = [NSString stringWithFormat:@"%@",self.TrackId];
        
        NSString *TrackName = [NSString stringWithFormat:@"%@",self.TrackName];
        
        NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btn.serviceIDName,@"itemId",btn.eventName,@"name",ENtype,@"ENtype",TrackId,@"TrackId",TrackName,@"TrackName", nil];
        
        NSLog(@"节目model－－－－－－－%@",paramDic);
        
        HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
        [HTVC setPolyModel:healthVodModel];
        [self.navigationController pushViewController:HTVC animated:NO];
    };
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_vodArr.count) {
        
        _noVODDataLabel.hidden = YES;
        
    }
}

//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        return numb % 2 == 0?  numb/2 : numb/2+1;
    }
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];

    if (array.count > index*2) {
        [anser addObject:array[index*2]];
    }
    if (array.count > index*2+1) {
        [anser addObject:array[index*2+1]];
    }
    
    return anser;
}

#pragma mark 点播cell点击事件 多选---进入点播播放器
- (void)goToVODTV:(VODDeleteButton *)ges
{
  
    HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
    HTVC.polyMedicDic = _vodArr[ges.tag];
    HTVC.contentID = [_vodArr[ges.tag] objectForKey:@"videoId"];
    HTVC.titleStr = [_vodArr[ges.tag] objectForKey:@"name"];
    HTVC.polyMedicType = [_vodArr[ges.tag] objectForKey:@"type"];
    [self.navigationController pushViewController:HTVC animated:YES];
}


//登录按钮触发方法
- (void)loginBtnSender
{
    [self.navigationController pushViewController:[[LoginViewController alloc] init]animated:YES];
}


- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadData
{
    [_vodTable.mj_footer endRefreshing];
    
    _requestYJVideoBtn.hidden = YES;
    
    [self refreshYJVideoTableCell];
    
}


@end
