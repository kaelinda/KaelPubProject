//
//  YJHealthVideoSecondViewcontrol.h
//  HIGHTONG
//
//  Created by HTYunjiang on 16/5/12.
//  Copyright (c) 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralPromptView.h"

@interface YJHealthVideoSecondViewcontrol : HT_FatherViewController
{
    NSInteger _mediaCount;
    CGFloat  _mediaHight;
    
    UIView *_noView;//  ***** 暂无图片的 View *****
        
    BOOL _selAllBtnIsClick;//标记全选按钮是否被点击
}

@property (assign, nonatomic) BOOL _vodIsEditing;//点播垃圾桶按钮是否是编辑状态

@property (strong, nonatomic) NSMutableArray *vodArr;

@property (strong, nonatomic) GeneralPromptView *noVODDataLabel;//暂无数据

@property (strong, nonatomic) UITableView *vodTable;//点播视图

@property (nonatomic,copy)NSString *categoryID;//频道ID

@property (nonatomic,copy)NSString *name;//频道名称

@property (nonatomic,copy)NSString *selectID;//选择大类ID

@property (nonatomic,copy)NSString *itemID;//选择小类ID

@property (nonatomic,copy)NSString *TrackId;//节目路径

@property (nonatomic,copy)NSString *ENtype;

@property (nonatomic,copy)NSString *TrackName;//节目名称路径

@end
