//
//  HospitalViewController.h
//  HIGHTONG_Public
//
//  Created by testteam on 15/12/30.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
typedef NS_ENUM(NSInteger, HospitalType) {

    HospitalTypeYiYuan,//微医院
    
    HospitalTypeYiVideo,//微视频
    
    HospitalTypeYiZhuanJia,//出诊专家
    
    HospitalTypeYiZhuShou,//助手
    
    HospitalTypeMoreWiFi//更多wifi
};

@interface HospitalViewController : HT_FatherViewController
@property (nonatomic,assign)BOOL isTopic;//是主题节目列表
@property (nonatomic,copy)NSString *TopicCode;

@property (nonatomic,copy)NSString *YiYuanTitle;

@property (nonatomic,copy)NSString *categoryID;

@property(nonatomic,assign)HospitalType HospitalType;

@end
