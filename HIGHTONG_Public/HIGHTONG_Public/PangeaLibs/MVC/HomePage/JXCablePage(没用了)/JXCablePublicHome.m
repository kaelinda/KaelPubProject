//
//  JXCablePublicHome.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "JXCablePublicHome.h"
#import "PosterViewofTB.h"
#import "UIButton_Block.h"
#import "DataPlist.h"
#import "DemandVideoPlayerVC.h"
#import "SearchViewController.h"
#import "DemandADDetailViewController.h"
#import "MineRecorderViewController.h"
#import "EpgSmallVideoPlayerVC.h"
#import "Tool.h"
#import "KnowledgeBaseViewController.h"
#import "JXCablePublicTableCell.h"
#import "ComPanyOperationView.h"
#import "UserToolClass.h"
#import "UserBindingVC.h"
#import "UserBindingInfoVC.h"
#import "UrlPageJumpManeger.h"

#define HaveTuijian  2

#define TVCable_CompanyInfo YES//有线电视 公司业务信息开关

#define WeiHospital  (TVCable_CompanyInfo==YES ? 94 : 44)

#define URL_company_info     [NSString stringWithFormat:@"%@/shphone/pages/jxnet/mp/index.html",YJ_Shphone]
#define URL_operation_intro  [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html",YJ_Shphone]
#define URL_busyness_net     [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bnq/index.html",YJ_Shphone]
#define URL_operation_manage [NSString stringWithFormat:@"%@/shphone/pages/jxnet/faq/index.html",YJ_Shphone]


#define Name_company_info     @"公司概况"
#define Name_operation_intro  @"业务介绍"
#define Name_busyness_net     @"营业网点"
#define Name_operation_manage @"业务办理"


/* 特色专区 有线宽带 新添加的 */
#define URL_combo_tariff [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true#/ptnav",YJ_Shphone,KREGION_CODE,KInstanceCode]//资费套餐

#define URL_wired_broadband [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true#/numberInfo/2",YJ_Shphone,KREGION_CODE,KInstanceCode]//有线宽带

#define URL_interact_tv [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true#/itv",YJ_Shphone,KREGION_CODE,KInstanceCode]//互动电视

#define URL_specialZone [NSString stringWithFormat:@"%@/shphone/pages/jxnet/fet/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true",YJ_Shphone,KREGION_CODE,KInstanceCode]//特色专区

#define URL_hdtv [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true#/hdtvInfo/5",YJ_Shphone,KREGION_CODE,KInstanceCode]//高清电视

#define URL_dtv [NSString stringWithFormat:@"%@/shphone/pages/jxnet/bi/index.html?regionCode=%@&instanceCode=%@&ch=3&backToApp=true#/numberInfo/6",YJ_Shphone,KREGION_CODE,KInstanceCode]//数字电视


@interface JXCablePublicHome ()<HTRequestDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIView *_Headview; // tableview的头
    UIImageView *_RecordView;//播放记录
    
    NSString *_MYplaytime;//播放记录时间
    BOOL isEPGRecord;//是直播记录
    
    NSString *_channelID;//直播id
    NSString *_endDate;//回看播放结束时间
    
    NSString *episodeSeq;//
    NSString *breakPointRecord;//
    
    BOOL HaveRecord;//有播放记录
    
    ComPanyOperationView *_operationView;//公司业务视图 四个按钮
    
    BOOL _IS_SUPPORT_VOD;//是否支持点播
       
    BOOL firstLoad;//第一次加载
    BOOL requestYES;//请求完成了
    NSDate *_requestDate;//加载时间
    
    BOOL isJumpToBinding;
}
@property (strong,nonatomic) PosterViewofTB *posterView;//海报view
@property (strong,nonatomic) UITableView    *tableView;//首页热门节目列表
@property (strong,nonatomic) NSMutableArray *dataArray;//首页直播推荐  影视精选 等 数据
@property (strong,nonatomic) UILabel        *XUKanLabel;//续看标签
@property (strong,nonatomic) UIButton_Block *XUKanBtn;//续看按钮
@property (strong,nonatomic) NSMutableArray *jxCableArray;//江西有线图片数组

@end

@implementation JXCablePublicHome

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSLog(@"首页即将出现");
    
    if (firstLoad) {
        if ([self RequstEnabledWithFirstTime:_requestDate andSecondData:[NSDate date]] || requestYES) {
            
            [self refreshHomeInViewWillAppear];
        }
    }
    firstLoad = YES;
    
    isJumpToBinding = NO;
    
    [MobCountTool pushInWithPageView:@"JXCableHomePage"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setStatusBarStyle:UIStatusBarStyleDefault];
    });

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"JXCableHomePage"];
}

-(void)viewDidAppear:(BOOL)animated{
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];

    [super viewDidAppear:animated];

}

#pragma  mark -首页刷新
- (void)refreshHomeInViewWillAppear
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length==0||[token isEqualToString:@"(null)"]) {
        return;
    }
    
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
    if (string.length == 0 ) {
        return;
    }

    requestYES = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        requestYES = YES;
    });

    [self showCustomeHUD];
    
    
    //*****轮播图列表
    HTRequest *requstSlideList = [[HTRequest alloc]initWithDelegate:self];
    [requstSlideList CNCommonSlideshowList];
    
    
    [self setHighForheadIsOrNo:NO];
    
    
    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = _Headview;
    
    [_tableView reloadData];

    _requestDate = [NSDate date];
}

//上一次请求时间和现在时间差值
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    if ([date1 timeIntervalSinceDate:date] >= 10) {
        NSLog(@"两次请求时间差值够了，请求去吧");
        return YES;
    }
    return NO;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = App_background_color;
    

    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoginRefreshHome:) name:@"loginSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(generalRefreshHome) name:@"RefreshHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:@"REFRESHMINEVCTABLECELL" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHEADERBA) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    
    //REFRESH_THE_PLAY_RECORDER_LIST
    
    
    //初始化数据源
    self.dataArray  = [NSMutableArray array];
    self.jxCableArray = [NSMutableArray array];

    /*********************************/
    //创建tableView 整体放到tableView上    
    [self initbaseTableView];

    //请求时间
    _requestDate = [NSDate date];
    
    //adjust the UI for iOS 7
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
#endif
    [self setWhiteNavBar];
}


-(void)setWhiteNavBar{
    [self.m_viewNaviBar setBackgroundColor:[UIColor whiteColor]];
    [self.m_viewNaviBar.m_imgViewBg setImage:nil];
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(10*kRateSize, 27, 83, 30)];
    logo.image = [UIImage imageNamed:@"ic_launcher2"];
    [self.m_viewNaviBar addSubview:logo];
//    [self setNaviBarTitle:kAPP_NAME];
    
    
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_home_search"]];
    searchImage.userInteractionEnabled = YES;
    searchImage.size = CGSizeMake(211*kDeviceRate, 28);
    searchImage.center = CGPointMake(self.m_viewNaviBar.center.x+32, logo.center.y);
    [self.m_viewNaviBar addSubview:searchImage];
    
    //-----
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoDemandVC)];
    tap.numberOfTapsRequired = 1;
    [searchImage addGestureRecognizer:tap];
    
    UIButton *recorderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [recorderBtn setImage:[UIImage imageNamed:@"btn_home_palyrecorder"] forState:UIControlStateNormal];
    [recorderBtn addTarget:self action:@selector(gotoRecorderVC) forControlEvents:UIControlEventTouchUpInside];
    [self.m_viewNaviBar setRightBtn:recorderBtn];

    
    

}
-(void)gotoRecorderVC{
    if (IsLogin) {
       MineRecorderViewController *recorderVC = [[MineRecorderViewController alloc] init];
        [self.navigationController pushViewController:recorderVC animated:YES];
    }else{
        UserLoginViewController *loginVC= [[UserLoginViewController alloc] init];
//        [self.navigationController pushViewController:loginVC animated:YES];
        [self presentViewController:loginVC animated:YES completion:^{
            [self setStatusBarStyle:UIStatusBarStyleDefault];
        }];
    }
    
}

-(void)gotoDemandVC{
    SearchViewController *searchV = [[SearchViewController alloc] init];
    [self.navigationController pushViewController:searchV animated:YES];
//    ZSToast(@"我要跳到点播搜索界面🔍")
    
}
//刷新是否用户是否有播放记录
- (void)reloadHEADERBA
{
    //*******请求观看记录
    _MYplaytime = @"1999-01-01 00:00:00";
    if (!IsLogin) {
        
        
    }else
    {
        //*******请求观看记录
        if (_IS_SUPPORT_VOD) {
            
            HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
            [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
        }else
        {
            
            HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:self];
            [_requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
        }
    }
    
    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = _Headview;
}

- (void)userLoginRefreshHome:(NSNotification*)notif
{
    NSLog(@"登陆成功了额  啊%@",notif);
    
    [self generalRefreshHome];
}

- (void)generalRefreshHome
{
    [self showCustomeHUD];

    {//江西有线数据
        
        [self.jxCableArray removeAllObjects];
        
        
        NSMutableDictionary *jxzidian1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_cambo_tariff",@"imageUrl",URL_combo_tariff,@"jxURL", nil];//套餐资费
        
        NSMutableDictionary *jxzidian2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_wireBand",@"imageUrl",URL_wired_broadband,@"jxURL", nil];//有线宽带
        
        NSMutableDictionary *jxzidian3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_interact_tv",@"imageUrl",URL_interact_tv,@"jxURL", nil];//互动电视
        
        NSMutableDictionary *jxzidian4 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_specialZone",@"imageUrl",URL_specialZone,@"jxURL", nil];//特色专区
        
        NSMutableDictionary *jxzidian5 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_hdtv",@"imageUrl",URL_hdtv,@"jxURL", nil];//高清电视
        
        NSMutableDictionary *jxzidian6 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"ic_home_dtv",@"imageUrl",URL_dtv,@"jxURL", nil];//数字电视

        //--------------- 新添加的两项假数据



        
        
        [self.jxCableArray addObject:jxzidian1];
        [self.jxCableArray addObject:jxzidian2];
        [self.jxCableArray addObject:jxzidian3];
        [self.jxCableArray addObject:jxzidian4];
        [self.jxCableArray addObject:jxzidian5];
        [self.jxCableArray addObject:jxzidian6];
        
        [self.tableView reloadData];
    }
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    NSLog(@"调用刷新");
    
    //请求产品配置
    HTRequest *requstConfig = [[HTRequest alloc]initWithDelegate:self];
    [requstConfig CNCommonAppConfig];

    
    //*****轮播图列表
    HTRequest *requstSlideList = [[HTRequest alloc]initWithDelegate:self];
    [requstSlideList CNCommonSlideshowList];
    
    [self setHighForheadIsOrNo:NO];
    
    
    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = _Headview;
    
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
    if (isEmptyStringOrNilOrNull(string)) {
        
        [self hiddenCustomHUD];
//        return;
    }
}

#pragma mark -
- (void)initbaseTableView
{
    WS(wself);
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_background_color;
    [self.view addSubview:view];
    
    //初始化tableView
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    [self.tableView setShowsVerticalScrollIndicator:NO];
//    UIImageView *image = [[UIImageView alloc]init];
//    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
//    image.frame = self.tableView.frame;
//    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    

    //  头View  tableView的HeaderView  上  AD广告页 + 续看 View   ...
    {
        if (TVCable_CompanyInfo) {
            //初始化公司业务视图
            _operationView = (ComPanyOperationView *)[self getCompanyOperationView];
            
            _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (173+kheigh+WeiHospital)*kRateSize)];
            
            [_Headview addSubview:_operationView];

        }else{
        }
        
       
        _Headview.backgroundColor = App_background_color;
        //AD广告页
        self.posterView = [[PosterViewofTB alloc]init];
        [_Headview addSubview:self.posterView];
        [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_Headview.mas_top );
            make.left.equalTo(_Headview.mas_left);
            make.right.equalTo(_Headview.mas_right);
            make.height.equalTo(@((173+kheigh)*kRateSize));
        }];
        self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
        self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];

        //续看View...   节目Label + 播放记录Button
        {
            _RecordView = [[UIImageView alloc]init];
            _RecordView.backgroundColor = UIColorFromRGB(0xffffff);
            [_Headview addSubview:_RecordView];
            
            [_RecordView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
                make.left.equalTo(_Headview.mas_left);
                make.width.equalTo(_Headview.mas_width);
                make.height.equalTo(@(40*kRateSize));
            }];
            _RecordView.userInteractionEnabled =  YES;
            
        
            
            if (TVCable_CompanyInfo) {
                [_operationView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_RecordView.mas_bottom ).offset(0*kRateSize);
                    make.left.equalTo(_Headview.mas_left);
                    make.width.equalTo(_Headview.mas_width);
                    make.height.equalTo(@(WeiHospital*kRateSize));
                }];
            }
            
            
            //节目Label
            self.XUKanLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5*kRateSize, 200*kRateSize, 30*kRateSize)];
            
            self.XUKanBtn = [UIButton_Block new];
            [self.XUKanLabel addSubview:self.XUKanBtn];
            self.XUKanBtn.backgroundColor = UIColorFromRGB(0xffffff);
            [self.XUKanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.XUKanLabel);
            }];
            self.XUKanLabel.text = @"续看                     ";

            
            self.XUKanLabel.userInteractionEnabled = YES;
            self.XUKanLabel.numberOfLines = 0;
            
            __weak NSString *weakchannelId = _channelID;
            
            self.XUKanBtn.Click = ^(UIButton_Block*btn,NSString *name)
            {
                _channelID =  btn.serviceIDName;
                
                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:wself];
                [_requst EventPlayWithEventID:weakchannelId andTimeout:10];
                
            };
            
            
            UIFont *font  =  [UIFont systemFontOfSize:14*kRateSize];
            self.XUKanLabel.font = font;
            [_RecordView addSubview:self.XUKanLabel];
            //播放记录Button
            UIButton_Block *btn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(FRAME_W(self.view) - 105*kRateSize, 5*kRateSize, 100*kRateSize, 30*kRateSize);
            UIImageView* inImageView = [UIImageView new];
            [btn addSubview:inImageView];
            [inImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(btn.mas_centerY);
                make.right.equalTo(btn.mas_right).offset(-9*kRateSize);
                make.width.equalTo(@(6*kRateSize));
                make.height.equalTo(@(8*kRateSize));
            }];
            
            inImageView.image = [UIImage imageNamed:@"img_user_protocol_arrow"];//公版改进
            btn.titleLabel.font = [UIFont systemFontOfSize:14*kRateSize];
            [btn setTitle:@"播放记录" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] forState:UIControlStateNormal];
            btn.userInteractionEnabled = YES;
            btn.serviceIDName = @"selina";
            btn.backgroundColor = UIColorFromRGB(0xffffff);
            
            
            [btn setClick:^(UIButton_Block*btn,NSString* name){
                
                NSLog(@"%@ name:%@",[btn currentTitle],name);
                
                MineRecorderViewController *mine = [[MineRecorderViewController alloc]init];
                
                if (!isEPGRecord) {
                    
                    mine.fromHome = YES;
                }
                [self.navigationController pushViewController:mine animated:YES];

            }];
            
            
            [_RecordView addSubview:btn];
            UIView *recordVerLine = [UIView new];
            recordVerLine.backgroundColor = UIColorFromRGB(0xeeeeee);
            [_RecordView addSubview:recordVerLine];
            [recordVerLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_RecordView.mas_left);
                make.right.equalTo(_RecordView.mas_right);
                make.height.equalTo(@(1));
                make.bottom.equalTo(_RecordView.mas_bottom).offset(-1);
                
            }];
            
            self.tableView.tableHeaderView = _Headview;
            _RecordView.hidden= YES;
            [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
                make.left.equalTo(_Headview.mas_left);
                make.width.equalTo(_Headview.mas_width);
                make.height.equalTo(@(0*kRateSize));
            }];
            //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
            self.tableView.delaysContentTouches = YES;
            
        }
    }
    
    //更新广告数据
    {
        NSMutableArray *adArray = [NSMutableArray array];
       
        [self.posterView updateThePosterData:adArray];
    }
}

//公司业务四个模块
-(UIView *)getCompanyOperationView{
    ComPanyOperationView *operationView = [[ComPanyOperationView alloc] init];
    operationView.frame = CGRectMake(0, 0, kDeviceWidth, 90*kRateSize);
    [operationView initUI];
    operationView.actionBlock = ^(TitleButtonType actiontype){

        switch (actiontype) {
            case Btn_Company_Info:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                
                webVC.urlStr = [UrlPageJumpManeger homePageComPanyOperationJumpWithModuleInfo:URL_company_info];

                [self.navigationController pushViewController:webVC animated:YES];
                
                break;
            }
            case Btn_Operation_Intro:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                
                webVC.urlStr = [UrlPageJumpManeger homePageComPanyOperationJumpWithModuleInfo:URL_operation_intro];
                
                [self.navigationController pushViewController:webVC animated:YES];

                break;
            }
            case Btn_Busyness_Net:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                
                webVC.urlStr = [UrlPageJumpManeger homePageComPanyOperationJumpWithModuleInfo:URL_busyness_net];
               
                [self.navigationController pushViewController:webVC animated:YES];
                
                break;
            }
            case Btn_Operation_Manage:
            {
                NSLog(@"button类型 %lu",(unsigned long)actiontype);
                KnowledgeBaseViewController *webVC = [[KnowledgeBaseViewController alloc] init];
                
                webVC.urlStr = [UrlPageJumpManeger homePageComPanyOperationJumpWithModuleInfo:URL_operation_manage];
                
                [self.navigationController pushViewController:webVC animated:YES];
                
                break;
            }
            case Btn_Account_Binding:{
                
                if (!isJumpToBinding) {
                    
                    isJumpToBinding = YES;
                    if (IsLogin) {
                        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                        [request UPORTALUserBindStatusJxTV];
                    }else{
                        
                        UserLoginViewController *login = [[UserLoginViewController alloc] init];
                        login.isAccountBindingFlag = YES;
                        [self.navigationController pushViewController:login animated:YES];
                        
                        login.loginType = kPopSelfLogin;
                        login.refreshHomeBlock = ^(){//登录成功后的回调
                        
                            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                            [request UPORTALUserBindStatusJxTV];
                        };
                    }
                }else{
                    NSLog(@"账号绑定按钮点击的次数太多啦");
                    break;
                }
            }
            default:
                break;
        }
        //睛彩江西 网页 状态条要变成黑色的
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setStatusBarStyle:UIStatusBarStyleDefault];
        });
        
    };
    return operationView;
}

#pragma mark -  广告点击 按钮按下
- (void)AdClicked:(NSNotification*)notif
{
    NSLog(@"广告业被点到了吗---%@",notif);
    
    NSDictionary *object = notif.object;
    NSString *type = [object objectForKey:@"type"];
    [self homeADJumpWithType:type andDiction:object];
}

- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSLog(@"广告带的字典--%@",dic);
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                NSString *name = [dic objectForKey:@"name"];
                NSString *contentID = [dic objectForKey:@"id"];
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = contentID;
                demandVC.contentTitleName = name;
                [self.navigationController pushViewController:demandVC animated:YES];
            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
        }
            break;
            
        case 5:
        {
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            
            [self.navigationController pushViewController:epg animated:YES];
        }
            
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = [dic objectForKey:@"urlLink"];
                [self presentViewController:demandDetail animated:YES completion:nil];
                ZSToast(@"广告请求成功");
            }
        }
            
            break;
        default:
        {
            
        }
            break;
    }
    
}


#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.jxCableArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *jxCableIdentifier = @"jxCableIdentifier";
    JXCablePublicTableCell *cell = [tableView dequeueReusableCellWithIdentifier:jxCableIdentifier];
    if (!cell) {
        
        cell = [[JXCablePublicTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:jxCableIdentifier];
        
        cell.leftView.postImg.layer.cornerRadius = 20;
        cell.backgroundColor = UIColorFromRGB(0xdadada);
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    if (self.jxCableArray.count >indexPath.row) {
        
        cell.dataDic = [self.jxCableArray objectAtIndex:indexPath.row];
        [cell JXCableHomeUpDate];
        
        WS(wself);
        
        cell.cellBlock = ^(UIButton_Block *btn,NSString *url)
        {
            KnowledgeBaseViewController *knowledge = [[KnowledgeBaseViewController alloc] init];
            NSLog(@"江西有线首页%@",url);
            knowledge.urlStr = url;
            [wself.navigationController pushViewController:knowledge animated:YES];
            //睛彩江西 网页 状态条要变成黑色的
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self setStatusBarStyle:UIStatusBarStyleDefault];
            });
        };
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 172*kRateSize;
}

#pragma mark - HT请求回调方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"结: %ld result:%@  type:%@",status,result,type );
    
    switch (status) {
        case 0:
        {
            if ([type isEqualToString:HomeADSLiderPage] || [type isEqualToString:CN_COMMON_SLIDESHOWLIST]) {

                [self hiddenCustomHUD];

                NSMutableArray *array = [result objectForKey:@"slideshowList"];
                NSLog(@"广告的数据是%@",array);
                if (array.count>1) {
                    array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
                    NSDictionary *firstDic = [array firstObject];
                    if (array.count>0) {
                        [array removeObjectAtIndex:0];
                    }
                    [array addObject:firstDic];
                }
                [self.posterView updateThePosterData:array];
            }
            
            //鉴权回看
            if ([type isEqualToString:EPG_EVENTPLAY]){
                
                if (result.count > 0) {
                    
                    switch ([[result objectForKey:@"ret"] integerValue]) {
                        case 0:{
                            NSLog(@"成功");
                            
                            NSDictionary *dataDic = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"serviceinfo"]];
                            
                            NSLog(@"%@",dataDic);
                            
                            EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                            live.currentPlayType = CurrentPlayEvent;
                            live.serviceID = [dataDic objectForKey:@"serviceID"];
                            live.serviceName = [dataDic objectForKey:@"name"];
                            live.eventURL =[NSURL URLWithString:[dataDic objectForKey:@"playLink"]];
                            live.eventURLID = _channelID;
                            live.currentDateStr = _endDate;
                            [self.navigationController pushViewController:live animated:YES];
                            
                            break;
                        }
                        case -1:{
                            
                            NSLog(@"当前节目不存在或者不提供回看");
                            
                            [AlertLabel AlertInfoWithText:@"当前节目不存在或者不提供回看" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -2:{
                            
                            NSLog(@"用户未登录或者登录超时");
                            
                            [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -3:{
                            
                            NSLog(@"鉴权失败");
                            
                            [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -5:{
                            
                            NSLog(@"频道对应此终端无播放链接");
                            
                            [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -6:{
                            
                            NSLog(@"该频道不提供回看功能");
                            
                            [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        case -9:{
                            NSLog(@"失败（连接3A系统失败或者参数错误）");
                             [AlertLabel AlertInfoWithText:@"失败（连接3A系统失败或者参数错误）" andWith:self.view withLocationTag:1];
                            
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
            //账号绑定
            if ([type isEqualToString:userBindStatusJxTV]) {
                
                 NSLog(@"账号绑定: %ld result:%@  type:%@",status,result,type );
                if (result.count>0) {
                    
                    if ([[result objectForKey:@"ret"] integerValue] == 0) {//已绑定
                        
                        UserBindingInfoVC *bindingInfo = [[UserBindingInfoVC alloc] init];
                        bindingInfo.isComeFromHome = YES;
                        [self.navigationController pushViewController:bindingInfo animated:YES];
                        
                    }else if ([[result objectForKey:@"ret"] integerValue] == -1 || [[result objectForKey:@"ret"] integerValue] == -9){//未绑定
                    
                        UserBindingVC *binding = [[UserBindingVC alloc] init];
                        [self.navigationController pushViewController:binding animated:YES];
                    }else{
                        
                        ZSToast(@"服务器开小差啦");
                        isJumpToBinding = NO;
                        break;
                    }
                }
            }
        }
            break;
            
        default:
        {
            NSLog(@"请求失败码%ld %@",status,type);
            isJumpToBinding = NO;
        }
            break;
    }
}

#pragma mark - 是否显示续看条目的方法
- (void)setHighForheadIsOrNo:(BOOL)havePlayRecorder
{
    if (havePlayRecorder) {

    }else{

        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (173+kheigh+WeiHospital)*kRateSize);
        _RecordView.hidden = YES;
        [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(0*kRateSize));
        }];
        
        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (173+kheigh+WeiHospital)*kRateSize);
        self.tableView.tableHeaderView = nil;
        self.tableView.tableHeaderView = _Headview;
    }
    
    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = _Headview;
}

//截取播放记录节目名字的方法
- (NSString*)shortStringWithString:(NSString*)string
{
    NSString *anser;
    if (string.length>6) {
        anser = [NSString stringWithFormat:@"%@...",[string substringToIndex:6]];
    }else
    {
        anser = string;
    }
    
    return anser;
}

#pragma  mark -header跟随tabelView动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView)
    {
        //YOUR_HEIGHT 为最高的那个headerView的高度
        CGFloat sectionHeaderHeight = 40;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}


#pragma mark -
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end


















