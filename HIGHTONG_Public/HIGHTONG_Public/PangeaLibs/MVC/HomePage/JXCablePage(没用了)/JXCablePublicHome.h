//
//  JXCablePublicHome.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HT_FatherViewController.h"

#import "SearchViewController.h"
#import "MineRecorderViewController.h"
#import "NewLoginViewController.h"
#import "UserLoginViewController.h"

@interface JXCablePublicHome : HT_FatherViewController

@end
