//
//  UserBindingInfoVC.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UserBindingInfoVC.h"
#import "BindingInfoTableViewCell.h"
#import "CustomAlertView.h"
#import "UserBindingVC.h"
#import "JXCablePublicHome.h"
#import "HTRequest.h"

#define cellHeight 45*kDeviceRate

@interface UserBindingInfoVC ()<UITableViewDelegate,UITableViewDataSource,CustomAlertViewDelegate,HTRequestDelegate>

@property (nonatomic, strong)UIButton *backBtn;//返回按钮
@property (nonatomic, strong)UIView *userInfoView;
@property (nonatomic, strong)UITableView *tableview;
@property (nonatomic, strong)UIButton *cancelBindingBtn;

@property (nonatomic, strong)NSMutableArray *dataArr;

@end

@implementation UserBindingInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO; 

    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"账号绑定"];
    
    self.view.backgroundColor = App_background_color;
    
    _dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request UPORTALUserBindInfoJxTV];
    
//    [self setupUI];
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if (status == 0 && result.count>0) {
        if ([type isEqualToString:userBindInfoJxTV]) {
            
            NSLog(@"绑定信息 返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSDictionary *dic = [result objectForKey:@"bindInfo"];
                    NSString *typeStr;
                    switch ([[dic objectForKey:@"type"] integerValue]) {
                        case 1:
                        {//数字
                            typeStr = @"数字用户";
                            break;
                        }
                        case 2:
                        {//互动
                            typeStr = @"互动用户";
                            break;
                        }
                        default:
                            break;
                    }
                    
                    NSDictionary *itemDic1 = @{@"title":@"用户类型",@"info":typeStr};
                    NSDictionary *itemDic2 = @{@"title":@"开户姓名",@"info":[dic objectForKey:@"realname"]};
                    
                    NSString *idStr;
                    if ([[dic objectForKey:@"identitynum"] length]>15) {
                        idStr = [[dic objectForKey:@"identitynum"] stringByReplacingCharactersInRange:NSMakeRange(10, 4) withString:@"****"];
                    }else{
                    
                        idStr = [dic objectForKey:@"identitynum"];
                    }
//                    NSString *idStr = [[dic objectForKey:@"identitynum"] stringByReplacingCharactersInRange:NSMakeRange(10, 4) withString:@"****"];//10
                    
                    NSDictionary *itemDic3 = @{@"title":@"证件号",@"info":idStr};
                    NSDictionary *itemDic4 = @{@"title":@"智能卡号",@"info":[dic objectForKey:@"idcard"]};
                    
                    [_dataArr addObject:itemDic1];
                    [_dataArr addObject:itemDic2];
                    [_dataArr addObject:itemDic3];
                    [_dataArr addObject:itemDic4];
                    
                    [self setupUI];
                    
                    break;
                }
                case -1:
                {
                
                    [self setupUI];
                    break;
                }
                default:
                    break;
            }
        }
        if ([type isEqualToString:unBindJxTV]) {
            
            NSLog(@"解除绑定 返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);

            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"解除绑定成功");
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                    
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IsBinding"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    break;
                }
                case -1:
                {
                    NSLog(@"解除绑定失败");
                    break;
                }
                default:
                    break;
            }
        }
    }
}

- (UIView *)setView
{
    UIView *view = [[UIView alloc] init];
    [self.view addSubview:view];
    view.backgroundColor = UIColorFromRGB(0xe8e8e8);
    return view;
}

- (UILabel *)setLab
{
    UILabel *lab = [[UILabel alloc] init];
    [_userInfoView addSubview:lab];
    lab.font = [UIFont fontWithName:@"PingFangSC" size:15*kDeviceRate];
    lab.textAlignment = NSTextAlignmentLeft;
    lab.textColor = UIColorFromRGB(0x000000);
    
    return lab;
}

- (void)setupUI
{
    WS(wself);
    
    UIView *navlineView = [self setView];
    [navlineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(wself.view.mas_top).offset(64);
        make.height.mas_equalTo(0.5*kDeviceRate);
    }];
    
    
    UIView *headLine = [self setView];
    [headLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(navlineView.mas_bottom).offset(10*kDeviceRate);
        make.height.mas_equalTo(0.5*kDeviceRate);
    }];

    
    _userInfoView = [[UIView alloc] init];
    [self.view addSubview:_userInfoView];
    _userInfoView.backgroundColor = UIColorFromRGB(0xffffff);
    [_userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wself.view);
        make.height.mas_equalTo(90*kDeviceRate);
        make.top.mas_equalTo(headLine.mas_bottom);
    }];
    
    
    UIImageView *avatarImg = [[UIImageView alloc] init];
    [_userInfoView addSubview:avatarImg];
    [avatarImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(65*kDeviceRate);
        make.centerY.mas_equalTo(wself.userInfoView.mas_centerY);
        make.left.mas_equalTo(wself.userInfoView.mas_left).offset(20*kDeviceRate);
    }];
    avatarImg.layer.masksToBounds = YES;
    [avatarImg.layer setCornerRadius:(65*kDeviceRate/2)];

    NSString *photoStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];
    [avatarImg sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[UIImage imageNamed:@"img_user_avatar_def"]];
    
    UILabel *nicknameLab = [self setLab];
    [nicknameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.userInfoView.mas_right);
        make.top.mas_equalTo(avatarImg.mas_top).offset(7*kDeviceRate);
        make.left.mas_equalTo(avatarImg.mas_right).offset(20*kDeviceRate);
    }];
    nicknameLab.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
    
    
    UILabel *phoneLab = [self setLab];
    [phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.userInfoView.mas_right);
        make.bottom.mas_equalTo(avatarImg.mas_bottom).offset(-7*kDeviceRate);
        make.left.mas_equalTo(avatarImg.mas_right).offset(20*kDeviceRate);
    }];
    phoneLab.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    
    
    UIView *neckLine = [self setView];
    [neckLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(wself.userInfoView.mas_bottom);
        make.height.mas_equalTo(0.5*kDeviceRate);
    }];
    
    _tableview = [[UITableView alloc] init];
    [self.view addSubview:_tableview];
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(neckLine.mas_bottom).offset(15*kDeviceRate);
        make.centerX.width.mas_equalTo(wself.view);
        make.height.mas_equalTo(wself.dataArr.count*cellHeight);
    }];
    _tableview.scrollEnabled = NO;
    
    
    _cancelBindingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:_cancelBindingBtn];
    [_cancelBindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45*kDeviceRate);
        make.top.mas_equalTo(wself.tableview.mas_bottom).offset(15*kDeviceRate);
        make.width.centerX.mas_equalTo(wself.view);
    }];
    [_cancelBindingBtn setTitle:@"解除绑定" forState:UIControlStateNormal];
    _cancelBindingBtn.titleLabel.font = [UIFont systemFontOfSize:15*kDeviceRate];
    _cancelBindingBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_cancelBindingBtn setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    _cancelBindingBtn.backgroundColor = [UIColor whiteColor];
    [_cancelBindingBtn addTarget:self action:@selector(cancelBindingBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *iden = @"identi";
 
    BindingInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:iden];
    if (!cell) {
        cell = [[BindingInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
        cell.userInteractionEnabled = NO;
    }
    
    cell.titleLab.text = [[_dataArr objectAtIndex:indexPath.row] objectForKey:@"title"];
    cell.infoLab.text = [[_dataArr objectAtIndex:indexPath.row] objectForKey:@"info"];
    
    return cell;
}

- (void)cancelBindingBtnSenderDown:(UIButton *)btn
{
    CustomAlertView *alert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"提示" andDescribeString:@"解除绑定可能导致某些节目观看受限\n您确定要解除吗？" andButtonNum:2 andCancelButtonMessage:@"解除" andOKButtonMessage:@"再看看" withDelegate:self];
    [alert show];
}

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){//再看看
        NSLog(@"再看看");
        
    }else{//解除
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request UPORTALUNBindJxTV];
        
        if (_isComeFromHome) {//从home页直接进入到绑定信息之后，解除绑定的操作，需要跳进新页面绑定介绍页
            
            UserBindingVC *binding = [[UserBindingVC alloc] init];
            [self.navigationController pushViewController:binding animated:YES];
        }else{
        
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[UserBindingVC class]]) {
                    
                    [self.navigationController popToViewController:controller animated:YES];
                }
            }
        }
    }
}

- (void)goBack
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
