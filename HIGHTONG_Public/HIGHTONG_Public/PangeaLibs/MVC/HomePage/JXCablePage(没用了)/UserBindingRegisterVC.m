//
//  UserBindingRegisterVC.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UserBindingRegisterVC.h"
#import "GeneralInputBlock.h"
#import "CustomAlertView.h"
#import "UserBindingInfoVC.h"
#import "HTRequest.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandVideoPlayerVC.h"

@interface UserBindingRegisterVC ()<UITextFieldDelegate,HTRequestDelegate>
{
    NSString *_chipCardTitle;
    NSString *_chipCardPlaceHolder;
    NSInteger _requestType;
    BOOL isClickBindingBtn;
}
@property (nonatomic, strong)UIButton *backBtn;//返回按钮
@property (nonatomic, strong)GeneralInputBlock *userName;
@property (nonatomic, strong)GeneralInputBlock *identityCard;
@property (nonatomic, strong)GeneralInputBlock *chipCard;
@property (nonatomic, strong)UIButton *bindingBtn;

@end

@implementation UserBindingRegisterVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    isClickBindingBtn = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:_titleStr];
    
    self.view.backgroundColor = App_background_color;
    
    switch (_bindingType) {
        case 0:
        {//数字
            _chipCardTitle = @"智能卡号";
            _chipCardPlaceHolder = @"请输入机顶盒CA卡卡号";
            _requestType = 1;
            break;
        }
        case 1:
        {//互动
            _chipCardTitle = @"智能卡号";
            _chipCardPlaceHolder = @"请输入机顶盒CA卡卡号";
            _requestType = 2;
            break;
        }
    }
    [self setUpUI];
}

- (UIView *)setView
{
    UIView *view = [[UIView alloc] init];
    [self.view addSubview:view];
    view.backgroundColor = UIColorFromRGB(0xe8e8e8);
    return view;
}

- (void)setUpUI
{
    WS(wself);
    
    UIView *navlineView = [self setView];
    [navlineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(wself.view.mas_top).offset(64);
        make.height.mas_equalTo(0.5*kDeviceRate);
    }];
    
    
    UIView *headLine = [self setView];
    [headLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(navlineView.mas_bottom).offset(10*kDeviceRate);
        make.height.mas_equalTo(0.5*kDeviceRate);
    }];
    
    _userName = [[GeneralInputBlock alloc] initAccountBindingWithTitle:@"开户姓名" andPlaceholderText:@"请输入开户人姓名"];
    _identityCard = [[GeneralInputBlock alloc] initAccountBindingWithTitle:@"证件号    " andPlaceholderText:@"请输入您开户时所用的证件号"];
    _chipCard = [[GeneralInputBlock alloc] initAccountBindingWithTitle:_chipCardTitle andPlaceholderText:_chipCardPlaceHolder];
    
    [self.view addSubview:_userName];
    [self.view addSubview:_identityCard];
    [self.view addSubview:_chipCard];
    
    _userName.inputBlock.delegate = self;
    _identityCard.inputBlock.delegate = self;
    _chipCard.inputBlock.delegate = self;
    
    _userName.userInteractionEnabled = YES;
    _identityCard.userInteractionEnabled = YES;
    _chipCard.userInteractionEnabled = YES;
    
    _userName.lineView.hidden = NO;
    _identityCard.lineView.hidden = NO;
    _chipCard.lineView.hidden = NO;
    
    [_userName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headLine.mas_bottom);
        make.height.mas_equalTo(45*kDeviceRate);
        make.width.centerX.mas_equalTo(wself.view);
    }];
    
    [_identityCard mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wself.userName);
        make.top.mas_equalTo(wself.userName.mas_bottom);
        make.centerX.mas_equalTo(wself.userName.mas_centerX);
    }];
    
    [_chipCard mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wself.userName);
        make.top.mas_equalTo(wself.identityCard.mas_bottom);
        make.centerX.mas_equalTo(wself.identityCard.mas_centerX);
    }];
    
    _bindingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:_bindingBtn];
    [_bindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wself.userName);
        make.top.mas_equalTo(wself.chipCard.mas_bottom).offset(15*kDeviceRate);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
    }];
    [_bindingBtn setTitle:@"验证并绑定" forState:UIControlStateNormal];
    _bindingBtn.titleLabel.font = [UIFont fontWithName:@"PingFangSC" size:15*kDeviceRate];
    _bindingBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_bindingBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    _bindingBtn.backgroundColor = [UIColor whiteColor];
    [_bindingBtn addTarget:self action:@selector(bindingBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)bindingBtnSenderDown:(UIButton *)btn
{
    if (_userName.inputBlock.text.length<=0||_identityCard.inputBlock.text.length<=0||_chipCard.inputBlock.text.length<=0) {
        
        CustomAlertView *alert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"提示" andDescribeString:@"请您输入完整的个人信息" andButtonNum:1 andCancelButtonMessage:@"确定" andOKButtonMessage:@"" withDelegate:self];
        alert.tag = 10001;
        [alert show];
        
        return;
    }
    
    if (!isClickBindingBtn) {
        
        isClickBindingBtn = YES;

        //发送请求
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request UPORTALBindJxTVWithIdCard:_chipCard.inputBlock.text withIdentityNum:_identityCard.inputBlock.text withPhoneNo:@"" withRealName:_userName.inputBlock.text withType:_requestType];
    }else{
    
        NSLog(@"验证绑定按钮点击次数太多啦");
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if (status == 0 && result.count>0 && [type isEqualToString:bindJxTV]) {
        NSLog(@"账号开始绑定 返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {//成功
//                NSArray *vcArr = self.navigationController.viewControllers;
//                NSInteger vcCount = vcArr.count;
//                UIViewController *infoVC = vcArr[vcCount-3];//绑定账号详细信息页
//                if ([infoVC isKindOfClass:[UserBindingInfoVC class]]) {//首页直接进入绑定详情页操作解除绑定后，又再次绑定新账号后的操作
//            
//                    [self.navigationController popToViewController:infoVC animated:YES];
//                }else{
//                }
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsBinding"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if (_isFroming) {//从播放器进来的部分
                    
                    NSArray *vcArr = self.navigationController.viewControllers;
                    NSInteger vcCount = vcArr.count;
                    UIViewController *infoVC = vcArr[vcCount-3];
                    if ([infoVC isKindOfClass:[EpgSmallVideoPlayerVC class]] || [infoVC isKindOfClass:[DemandVideoPlayerVC class]]) {

                        [self.navigationController popToViewController:infoVC animated:YES];
                    }
                }else{//从首页进来的
                
                    UserBindingInfoVC *info = [[UserBindingInfoVC alloc] init];
                    info.isComeFromHome = NO;
                    [self.navigationController pushViewController:info animated:YES];
                }
                
                break;
            }
            case -1:
            {//失败
                CustomAlertView *alert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"提示" andDescribeString:@"您输入的信息有误，请核对后重试" andButtonNum:1 andCancelButtonMessage:@"确定" andOKButtonMessage:@"" withDelegate:self];
                alert.tag = 10002;
                [alert show];
                
                isClickBindingBtn = NO;
                break;
            }
            default:
            {
                ZSToast(@"服务器开小差啦");
                isClickBindingBtn = NO;
                break;
            }
        }
    }
}

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 10001:
        {
            if(buttonIndex==0){//确定
                NSLog(@"确定");
            }
            break;
        }
        case 10002:
        {
            if(buttonIndex==0){//确定
                NSLog(@"确定");
            }
            break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_userName.inputBlock resignFirstResponder];
    [_identityCard.inputBlock resignFirstResponder];
    [_chipCard.inputBlock resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [_userName.inputBlock resignFirstResponder];
    [_identityCard.inputBlock resignFirstResponder];
    [_chipCard.inputBlock resignFirstResponder];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
