//
//  UserBindingVC.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UserBindingVC.h"
#import "UserBindingRegisterVC.h"
#import "CustomAlertView.h"

@interface UserBindingVC ()<CustomAlertViewDelegate>

@property (nonatomic, strong)UIButton *backBtn;//返回按钮
@property (nonatomic, strong)UIImageView *logoImg;
@property (nonatomic, strong)UIButton *unBindingBtn;
@property (nonatomic, strong)UILabel *tipsLab;
@property (nonatomic, strong)UIView *bindingView;
@property (nonatomic, strong)NSArray *bindingCategoryArr;

@end

@implementation UserBindingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];

    [self setNaviBarTitle:@"账号绑定"];
    self.view.backgroundColor = UIColorFromRGB(0xffffff);
    
    _bindingCategoryArr = @[@"数字电视用户绑定",@"互动电视用户绑定"];
    [self setUpUI];
}

- (void)setUpUI
{
    WS(wself);
    
    UIView *lineView = [[UIView alloc] init];
    [self.view addSubview:lineView];
    [lineView setBackgroundColor:UIColorFromRGB(0xe8e8e8)];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(wself.view.mas_width);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(wself.view.mas_top).offset(64);
        make.height.mas_equalTo(1*kDeviceRate);
    }];
    
    _logoImg = [[UIImageView alloc] init];
    [self.view addSubview:_logoImg];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(234*kDeviceRate);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.top.mas_equalTo(lineView.mas_bottom).offset(34*kDeviceRate);
        make.height.mas_equalTo(232*kDeviceRate);
    }];
    [_logoImg setImage:[UIImage imageNamed:@"account_binding_logo"]];
    
    _tipsLab = [[UILabel alloc] init];
    [self.view addSubview:_tipsLab];
    [_tipsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.logoImg.mas_bottom).offset(44*kDeviceRate);
        make.height.mas_equalTo(60*kDeviceRate);//50
        make.right.left.mas_equalTo(wself.view);
    }];
    _tipsLab.attributedText = [self getAttributedStringWithString:@"关联您的APP账号与机顶盒智能卡号\n以便获得更多观看权限" lineSpace:5*kDeviceRate];
    _tipsLab.textAlignment = NSTextAlignmentCenter;
    _tipsLab.textColor = UIColorFromRGB(0x000000);
    _tipsLab.font = [UIFont fontWithName:@"PingFangSC" size:16];
    _tipsLab.numberOfLines = 0;
    
    
    _bindingView = [[UIView alloc] init];
    [self.view addSubview:_bindingView];
    [_bindingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(74*kDeviceRate);
        make.top.mas_equalTo(wself.tipsLab.mas_bottom).offset(33*kDeviceRate);
        make.left.right.mas_equalTo(wself.view);
    }];
    
    
    NSArray *imgArr = @[@"btn_digital_binding",@"btn_interact_binding"];
    for (int i = 0; i<_bindingCategoryArr.count; i++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 10000+i;
        [_bindingView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(74*kDeviceRate);
            make.top.mas_equalTo(wself.bindingView.mas_top);
            make.centerX.mas_equalTo(wself.bindingView.mas_centerX).offset(-60.5*kDeviceRate+(i*121*kDeviceRate));
        }];
        [btn setImage:[UIImage imageNamed:imgArr[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(bindingBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    _unBindingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:_unBindingBtn];
    [_unBindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90*kDeviceRate);
        make.top.mas_equalTo(wself.bindingView.mas_bottom).offset(49*kDeviceRate);
        make.height.mas_equalTo(20*kDeviceRate);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
    }];
    if (KDeviceHeight==480) {
        _unBindingBtn.hidden = YES;
    }
    [_unBindingBtn setTitle:@"暂不绑定" forState:UIControlStateNormal];
    [_unBindingBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
    _unBindingBtn.titleLabel.font = [UIFont fontWithName:@"PingFangSC" size:14*kDeviceRate];
    _unBindingBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_unBindingBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){//继续绑定
        NSLog(@"继续绑定");
        
    }else{//退出
        
        if (_isFroming) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
        
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}

- (void)bindingBtnSenderDown:(UIButton *)btn
{
    UserBindingRegisterVC *toBinding = [[UserBindingRegisterVC alloc] init];
    toBinding.titleStr = [_bindingCategoryArr objectAtIndex:(btn.tag-10000)];
    toBinding.bindingType = btn.tag-10000;
    toBinding.isFroming = _isFroming;
    [self.navigationController pushViewController:toBinding animated:YES];
}

- (void)goBack
{
    CustomAlertView *alert = [[CustomAlertView alloc] initWithAccountBindingTitle:@"提示" andDescribeString:@"不绑定可能导致某些节目观看受限\n您确定要退出吗？" andButtonNum:2 andCancelButtonMessage:@"退出" andOKButtonMessage:@"继续绑定" withDelegate:self];
    [alert show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
