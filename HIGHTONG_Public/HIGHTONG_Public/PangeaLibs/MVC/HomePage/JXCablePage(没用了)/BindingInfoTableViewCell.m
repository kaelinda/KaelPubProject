//
//  BindingInfoTableViewCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/2/14.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "BindingInfoTableViewCell.h"

@implementation BindingInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        WS(wself);
        UIView *upView = [self setView];
        [upView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.top.centerX.mas_equalTo(wself.contentView);
            make.height.mas_equalTo(0.5*kDeviceRate);
        }];
        
        
        UIView *underView = [self setView];
        [underView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.bottom.centerX.mas_equalTo(wself.contentView);
            make.height.mas_equalTo(0.5*kDeviceRate);
        }];
        
        
        _titleLab = [self setLable];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wself.contentView.mas_left).offset(20*kDeviceRate);
            make.top.mas_equalTo(upView.mas_bottom);
            make.width.mas_equalTo(65*kDeviceRate);
            make.bottom.mas_equalTo(underView.mas_top);
        }];
        _titleLab.textColor = UIColorFromRGB(0x333333);
        
        
        _infoLab = [self setLable];
        [_infoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wself.titleLab.mas_right).offset(20*kDeviceRate);
            make.right.mas_equalTo(wself.contentView.mas_right);
            make.top.mas_equalTo(upView.mas_bottom);
            make.bottom.mas_equalTo(underView.mas_top);
        }];
        _infoLab.textColor = UIColorFromRGB(0x000000);
    }
    
    return self;
}

- (UIView *)setView
{
    UIView *myView = [[UIView alloc] init];
    myView.backgroundColor = UIColorFromRGB(0xe8e8e8);
    [self.contentView addSubview:myView];
    
    return myView;
}

- (UILabel *)setLable
{
    UILabel *lab = [[UILabel alloc] init];
    [self.contentView addSubview:lab];
    lab.font = [UIFont systemFontOfSize:15*kDeviceRate];
//    lab.textColor = UIColorFromRGB(0x333333);
    lab.textAlignment = NSTextAlignmentLeft;

    return lab;
}

@end
