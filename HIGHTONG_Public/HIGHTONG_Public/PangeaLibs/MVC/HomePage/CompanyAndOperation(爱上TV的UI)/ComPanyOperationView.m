//
//  ComPanyOperationView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/5/12.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ComPanyOperationView.h"

@implementation ComPanyOperationView


-(instancetype)init{
    self = [super init];
    if (self) {
//        [self initUI];
        [self setBackgroundColor:App_white_color];
    }
    return self;
}


-(void) initUI{
    WS(wself);
    for (int i=0; i<5; i++) {
        UIButton *btnItem = [KaelTool setButtonPropertyWithButtonType:UIButtonTypeCustom andBackGroundColor:CLEARCOLOR andCornerRadius:0 andNormalImage:nil andHighlightedImage:nil];
        
        btnItem.tag = i+100;
        [btnItem addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnItem];
        
        
        switch (btnItem.tag) {
            case 100:
            {
               
                [btnItem setImage:[UIImage imageNamed:@"ic_home_company_infomation"] forState:UIControlStateNormal];
                break;
            }
            case 101:
            {
                
                [btnItem setImage:[UIImage imageNamed:@"ic_home_operation_intro"] forState:UIControlStateNormal];

                break;
            }
            case 102:
            {
                
                [btnItem setImage:[UIImage imageNamed:@"ic_home_bussyness_net"] forState:UIControlStateNormal];

                break;
            }
            case 103:
            {
                [btnItem setImage:[UIImage imageNamed:@"ic_home_operation_manage"] forState:UIControlStateNormal];

                break;
            }
            case 104:
            {
                [btnItem setImage:[UIImage imageNamed:@"ic_home_account_binding"] forState:UIControlStateNormal];
            }
            default:
                break;
        }
        
        
        CGSize btnSize = CGSizeMake(48*kRateSize, 64*kRateSize);

        CGFloat bounarySpace = 22*kDeviceRate;
        
        
        CGFloat space = (self.frame.size.width-5*btnSize.width-2*bounarySpace)/4;
        
        

        
        [btnItem mas_makeConstraints:^(MASConstraintMaker *make) {

//            make.left.mas_equalTo(self.mas_left).offset(space/2+(space+btnSize.width)*i);
            make.left.mas_equalTo(self.mas_left).offset(bounarySpace+(space+btnSize.width)*i);
            make.size.mas_equalTo(btnSize);
            make.centerY.mas_equalTo(wself.mas_centerY);
            
        }];
        
        
    }

}


-(void)btnAction:(UIButton *)btn{

    switch (btn.tag) {
        case 100:
        {
            if (_actionBlock) {
                _actionBlock(Btn_Company_Info);
            }else{
                NSLog(@"这个地方你还没实现呢");
            }
            
            break;
        }
        case 101:
        {
            if (_actionBlock) {
                _actionBlock(Btn_Operation_Intro);
            }else{
                NSLog(@"这个地方你还没实现呢");
            }
            
            break;
        }
        case 102:
        {
            if (_actionBlock) {
                _actionBlock(Btn_Busyness_Net);
            }else{
                NSLog(@"这个地方你还没实现呢");
            }
            
            break;
        }
        case 103:
        {
            if (_actionBlock) {
                _actionBlock(Btn_Operation_Manage);
            }else{
                NSLog(@"这个地方你还没实现呢");
            }
            break;
        }
        case 104:
        {
            if (_actionBlock) {
                _actionBlock(Btn_Account_Binding);
            }else{
                NSLog(@"这个地方你还没实现呢");
            }
            break;
        }
        default:
            break;
    }

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
