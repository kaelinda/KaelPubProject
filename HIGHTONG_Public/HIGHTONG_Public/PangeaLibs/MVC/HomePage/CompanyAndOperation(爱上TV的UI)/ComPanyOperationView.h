//
//  ComPanyOperationView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/5/12.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TitleButtonType) {
    Btn_Company_Info,//公司概况
    Btn_Operation_Intro,//业务介绍
    Btn_Busyness_Net,//营业网点
    Btn_Operation_Manage,//业务办理
    Btn_Account_Binding,//账号绑定
};

typedef void(^SelectActionBlock)(TitleButtonType BtnType);



@interface ComPanyOperationView : UIView

@property (nonatomic,copy) SelectActionBlock actionBlock;
-(void) initUI;

@end
