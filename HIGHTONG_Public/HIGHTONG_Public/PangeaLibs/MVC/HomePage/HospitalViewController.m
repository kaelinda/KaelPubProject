//
//  HospitalViewController.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/12/30.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "HospitalViewController.h"
#import "HTRequest.h"
#import "ProgramListCell.h"
#import "YJProgramListCell.h"
#import "HotSearchTableViewCell.h"
#import "DemandVideoPlayerVC.h"
#import "MJRefresh.h"
#import "YJHealthVideoSecondViewcontrol.h"
#import "HTVideoAVPlayerVC.h"
#define requestCount 9
@interface HospitalViewController ()<HTRequestDelegate,UIWebViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIWebView *_webview;//
    NSDictionary *_hospital;//医院配置信息
    
    NSInteger _realCountOfResault;//实际请求的个数
    NSInteger _allCountOfResault;//实际请求的个数
    
    BOOL _isNOresult;//搜不到东西
    BOOL Statefresh;//筛选状态
    BOOL firstLoad;//第一次加载
    
    UIImageView *_HTImg;//
    UIView *_HTVw;//


}
@property (nonatomic,strong)UITableView *tableView_base;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,strong)UIButton *requestYJVideoBtn;//重新请求按钮
@property (nonatomic,strong)UILabel *noDatetipLb;//无数据提醒

@end

@implementation HospitalViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (firstLoad && self.HospitalType == HospitalTypeYiVideo && YJVideoNewDisplayTypeOn) {
        [self loadData];
    }
    firstLoad = YES;
    
    
    [MobCountTool pushInWithPageView:@"MineCollectPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineCollectPage"];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //医院信息
    
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    if (regionCode.length==0) {
        regionCode = REGION_CODE;
    }
    HTRequest *_request = [[HTRequest alloc]initWithDelegate:self];
    
    if (YJVideoNewDisplayTypeOn && self.HospitalType == HospitalTypeYiVideo) {
//         [_request YJHealthyTypeListVideoWithVideoCount:@"4"];
        [_request YJHealthyTypeListVideoWithGlobal:@"1" withInstanceCode:KInstanceCode WithVideoCount:@"4"];
    }
    else
    {
        [_request EPGGetRegionInfoWithRegionCode:regionCode andTimeout:5];
    }
    
    self.view.backgroundColor = App_background_color;
    
    if (self.HospitalType == HospitalTypeYiYuan) {
        [self HospitalTypeYiYuan];
    }else if (self.HospitalType == HospitalTypeYiVideo){
        [self HospitalTypeYiVideo];
    }else if (self.HospitalType == HospitalTypeYiZhuShou){
        [self HospitalTypeYiZhuShou];
    }else if (self.HospitalType == HospitalTypeYiZhuanJia){
        [self HospitalTypeYiZhuanJia];
    }else if (self.HospitalType == HospitalTypeMoreWiFi){
        [self moreWiFi];
    }
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
}
- (void)moreWiFi{
    [self setNaviBarTitle:self.YiYuanTitle];
    _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, kDeviceWidth, KDeviceHeight-44)];
    _webview.delegate = self;
    _webview.backgroundColor = App_background_color;
    [self.view addSubview:_webview];
    
    [self loadViewSetup];
    
    [self showLoading];
    
    NSString *url = Home_more_Wifi_Link;
    
    if (url.length>6) {
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }else{
        //弹出提示框
        [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
        [self hideLoading];
    }
}
- (void)HospitalTypeYiZhuanJia{

    if (self.YiYuanTitle.length) {
        [self setNaviBarTitle:self.YiYuanTitle];
    }
    _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, kDeviceWidth, KDeviceHeight-44)];
    _webview.delegate = self;
    _webview.backgroundColor = [UIColor redColor];
    [self.view addSubview:_webview];
    
    [self loadViewSetup];

    
    [self showLoading];
}


- (void)HospitalTypeYiZhuShou{
    if (self.YiYuanTitle.length) {
        [self setNaviBarTitle:self.YiYuanTitle];
    }
    _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, kDeviceWidth, KDeviceHeight-44)];
    _webview.delegate = self;
    _webview.backgroundColor = App_background_color;
    [self.view addSubview:_webview];
    
    [self loadViewSetup];
    
    [self showLoading];
}

- (void)HospitalTypeYiYuan{
    
    if (self.YiYuanTitle.length) {
         [self setNaviBarTitle:self.YiYuanTitle];
    }
    _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, kDeviceWidth, KDeviceHeight-44)];
    _webview.delegate = self;
    _webview.backgroundColor = App_background_color;
    [self.view addSubview:_webview];
    

    [self loadViewSetup];
    [self showLoading];
    
}
- (void)HospitalTypeYiVideo{
    


    [self showLoading];
    if (self.YiYuanTitle.length) {
        [self setNaviBarTitle:self.YiYuanTitle];
    }
    if (self.tableView_base) {
        return;
    }
    UIView *spaceView = [UIView new];
    spaceView.frame = CGRectZero;
    [self.view addSubview:spaceView];
    self.tableView_base = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, KDeviceHeight - 64)];
    self.tableView_base.delegate  = self;
    self.tableView_base.dataSource = self;
    [self.tableView_base setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView_base.scrollsToTop = YES;
    self.dataArray = [[NSMutableArray alloc]init];
    [self.view addSubview:self.tableView_base];
    self.tableView_base.backgroundColor = App_background_color;
    self.view.backgroundColor = App_background_color;
    _realCountOfResault = 1;
    
    _requestYJVideoBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, KDeviceHeight-64)];
    [_requestYJVideoBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
    [_requestYJVideoBtn setBackgroundColor:App_background_color];
    [self.view addSubview:_requestYJVideoBtn];
    [_requestYJVideoBtn addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    _requestYJVideoBtn.hidden = YES;
    
    //空数据页提示信息
    _noDatetipLb = [[UILabel alloc]init];
    _noDatetipLb.text = @"暂无数据";
    _noDatetipLb.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:_noDatetipLb];
    
    [_noDatetipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    _noDatetipLb.hidden = YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"加载完成消失");
    
    
    [self hiddenCustomHUDAfterDelay:0.3];
}

- (void)loadViewSetup
{
    
    _HTVw =[[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-44)];
    _HTVw.backgroundColor = App_background_color;
    [_webview addSubview:_HTVw];
    
    _HTImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];
    [_HTVw addSubview:_HTImg];
    
    [_HTImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
        make.height.equalTo(@(240*kRateSize));
        make.width.equalTo(@(180*kRateSize));
        
    }];
}

#pragma mark - 请求代理
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"VOD_HospitalInfo: %ld result:%@  type:%@",status,result,type );
    switch (status) {
        case 0:
            if ([type isEqualToString:EPG_REGION_INFO]) {

                _hospital = result;
                NSLog(@"%@",_hospital);
                
                if ([[result objectForKey:@"ret"] integerValue] == 0){
                    [self loadingHospitalURLWithDic:_hospital];
                    
                    if (self.HospitalType == HospitalTypeYiYuan) {
                        //            NSString *url = [[_hospital objectForKey:@"region"] objectForKey:@"urlLink"];
                        NSString *url = kMicroHospital;
                        
                        if (url.length>6) {
                            [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
                        }else{
                            [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
                            [self hideLoading];
                        }
                        
                    }else if (self.HospitalType == HospitalTypeYiVideo && !YJVideoNewDisplayTypeOn){
                        NSDictionary *video = [_hospital objectForKey:@"video"] ;
                        NSString *code = [video objectForKey:@"code"];
                        NSString *type = [video objectForKey:@"type"];
                        
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        if ([type isEqualToString:@"1"]) {
                            
                            [request VODProgramListWithOperationCode:code andRootCategpryID:@"" andStartSeq:1 andCount:requestCount andTimeout:10];
                            self.isTopic = NO;
                            self.categoryID = code;
                            
                        }if ([type isEqualToString:@"2"]) {
                            self.isTopic = YES;
                            [request VODTopicProgramListWithTopicCode:code andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
                            self.TopicCode = code;
                            NSLog(@"%@",self.TopicCode);
                        }
                        
                    }

                }else{
                    _noDatetipLb.hidden = NO;
                }
                
            }
            
            if ([type isEqualToString:VOD_HospitalInfo]) {
                
                _hospital = result;
                NSLog(@"%@",_hospital);
                if (self.HospitalType == HospitalTypeYiYuan) {
                    NSString *url = [[_hospital objectForKey:@"hospital"] objectForKey:@"urlLink"];
                    if (url.length>6) {
                        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
                    }
                    
                }else if (self.HospitalType == HospitalTypeYiVideo && !YJVideoNewDisplayTypeOn){
                    NSDictionary *video = [_hospital objectForKey:@"video"] ;
                    NSString *code = [video objectForKey:@"code"];
                    NSString *type = [video objectForKey:@"type"];
                    
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    if ([type isEqualToString:@"1"]) {
                        
                        [request VODProgramListWithOperationCode:code andRootCategpryID:@"" andStartSeq:1 andCount:requestCount andTimeout:10];
                        self.isTopic = NO;
                        self.categoryID = code;
                        
                    }if ([type isEqualToString:@"2"]) {
                        self.isTopic = YES;
                        [request VODTopicProgramListWithTopicCode:code andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
                        self.TopicCode = code;
                        NSLog(@"%@",self.TopicCode);
                    }
                    
                }
                
            }
            
            
            
            //推荐页面的
            if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
                NSLog(@"VOD_TOPIC_PROGRAM_LIST推荐广告: %ld result:%@  type:%@",status,result,type );
                NSArray *vodList = [result objectForKey:@"vodList"];
                NSLog(@"VOD_TOPIC_PROGRAM_LIST返回数据:%@",vodList );
                NSString *count = [result objectForKey:@"count"];
                NSInteger allcout = [count integerValue];
                if ([[result objectForKey:@"count"] integerValue] >= 1) {
                    
                    
                    if (self.dataArray.count &&Statefresh) {
                        [self.dataArray addObjectsFromArray:vodList];
                    }else
                    {
                        self.dataArray.array = vodList;
                    }
                    Statefresh = NO;
                    
                    [self.tableView_base reloadData];
                    
                    
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    if (([vodList count] <= allcout) && ([vodList count] >= requestCount) ) {
                        
                        if (!self.tableView_base.mj_footer) {
                            self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(MyloadMoreData)];
                        }
                        
                        [self.tableView_base.mj_footer endRefreshing];
                    }else
                    {
                        
                        [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                        _realCountOfResault += [vodList count];
                        _allCountOfResault = allcout;
                        //self.tableView_base.footer = nil
                    }
                    
                    
                    
                }else{
//                    [AlertLabel AlertInfoWithText:@"暂无健康讲座" andWith:self.view withLocationTag:1];
                    _noDatetipLb.hidden = NO;
                    
                }
                
                
                [self hideLoading];
                
            }
            
            if ([type isEqualToString:VOD_PROGRAM_LIST]) {
                NSLog(@"VOD_PROGRAM_LIST返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDIC = result;
                NSString *count = [resDIC objectForKey:@"count"];
                NSInteger allcout = [count integerValue];
                NSArray *vodList = [resDIC objectForKey:@"vodList"];
                NSLog(@"VOD_PROGRAM_LIST返回数据:%@",vodList );
                if ([vodList count]) {
                    
                    //                    NSLog(@"数据源：%@",vodList);
                    if (self.dataArray.count && Statefresh) {
                        [self.dataArray addObjectsFromArray:vodList];
                    }else
                    {
                        self.dataArray.array = vodList;
                    }
                    Statefresh = NO;
                    
                    [self.tableView_base reloadData];
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    if (([vodList count] <= allcout) && ([vodList count] >= requestCount) ) {
                        
                        if (!self.tableView_base.mj_footer) {
                            self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(MyloadMoreData)];
                        }
                        
                        [self.tableView_base.mj_footer endRefreshing];
                    }else
                    {
                        
                        [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                        _realCountOfResault += [vodList count];
                        _allCountOfResault = allcout;
                        //self.tableView_base.footer = nil
                    }
                    
                    [self hideLoading];
                }else
                {
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    //self.tableView_base.footer = nil
                }
            }
            
            if ([type isEqualToString:YJ_HEALTHY_TYPELISTVIDEO]) {
                NSLog(@"YJ_HEALTHY_TYPELISTVIDEO返回数据: %ld result:%@  type:%@",status,result,type );
                
                if ([[result objectForKey:@"ret"] integerValue] == 0){
                    NSDictionary *resDIC = result;
                    self.dataArray = [resDIC objectForKey:@"list"];
                    NSLog(@"%@",self.dataArray);
                    
                    if (self.dataArray.count) {
                        [self.tableView_base reloadData];
                    }
                    else
                    {
                        _noDatetipLb.hidden = NO;
                    }
                }
                else
                {
                    _requestYJVideoBtn.hidden = NO;
                }
                
                [self hideLoading];
                
            }

            break;
            
        default:
            
            if ([type isEqualToString:YJ_HEALTHY_TYPELISTVIDEO]) {
                NSLog(@"YJ_HEALTHY_TYPELISTVIDEO返回数据: %ld result:%@  type:%@",status,result,type );
                _requestYJVideoBtn.hidden = NO;
                
            }
            break;
    }
    

}

- (void)MyloadMoreData
{
    
    NSLog(@"_realCountOfResault-----%ld",_realCountOfResault);
    
    NSLog(@"下拉刷新了");
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        if (self.isTopic) {
            [request VODTopicProgramListWithTopicCode:self.TopicCode andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
        }else
        {
            [request VODProgramListWithOperationCode:@"" andRootCategpryID:self.categoryID andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10];
        }
        
//    }
    
    Statefresh =  YES;

}

#pragma mark - tabelView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (YJVideoNewDisplayTypeOn) {
        NSDictionary * categoryMutableReqestDiction = [self.dataArray objectAtIndex:section];
        NSArray *programList = [categoryMutableReqestDiction objectForKey:@"videoList"];
        
        return [self numberOfCellRowsWithArray:programList];

    }
    else
    {
        
        return [self numberOfCellRowsWithArray:self.dataArray];
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (YJVideoNewDisplayTypeOn) {
        return 130*kRateSize;
    }else
    {
        return 170*kRateSize;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (YJVideoNewDisplayTypeOn) {
 
        static NSString *YJProgranidentifier = @"YJProgranidentifier";
        YJProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:YJProgranidentifier];
        if (!cell) {
            cell = [[YJProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:YJProgranidentifier];
        }
        NSArray * array = self.dataArray;
        
        NSLog(@"rrrrrrrrrrrrrr---%@",array);
        
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        NSDictionary * categoryMutableReqestDiction = [array objectAtIndex:indexPath.section];
        
        NSArray *tempArray = [categoryMutableReqestDiction objectForKey:@"videoList"];
        
        NSLog(@"rrrrrrrrrrrrrr---%@",tempArray);
        
        cell.dateArray.array = [self arrayFOR_ALLArray:tempArray withBeginnumb:indexPath.row];
        //        cell.isMovie = self.isMovie;
        [cell update];
        cell.block = ^(UIButton_Block*btn)
        {
            NSLog(@"ttttttttttt－－－%@ %@",btn.serviceIDName,btn.eventName);
            
//            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
//            demandVC.contentID = btn.serviceIDName;
//            demandVC.contentTitleName = btn.eventName;
//            [self.navigationController pushViewController:demandVC animated:YES];
            HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
            HTVC.polyMedicDic = btn.polyMedicDic;
            HTVC.contentID = btn.serviceIDName;
            HTVC.titleStr = btn.eventName;
            HTVC.polyMedicType = btn.polyMedicType;
            [self.navigationController pushViewController:HTVC animated:NO];
        };
        return cell;
    }else
    {
        static NSString *Progranidentifier = @"Progranidentifier";
        ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
        if (!cell) {
            cell = [[ProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
        }
        NSArray * array = self.dataArray;
        
        NSLog(@"111111123123132131---%@",array);
        
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.dateArray.array = [self arrayFOR_ALLArray:array withBeginnumb:indexPath.row];
        //        cell.isMovie = self.isMovie;
        [cell update];
        cell.block = ^(UIButton_Block*btn)
        {
            NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
            
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
            demandVC.contentID = btn.serviceIDName;
            demandVC.contentTitleName = btn.eventName;
            [self.navigationController pushViewController:demandVC animated:YES];
            
            
//            HTVideoAVPlayerVC *HTVC = [[HTVideoAVPlayerVC alloc]init];
//            HTVC.polyMedicDic = btn.polyMedicDic;
//            HTVC.contentID = btn.serviceIDName;
//            HTVC.titleStr = btn.eventName;
//            HTVC.polyMedicType = btn.polyMedicType;
//            [self.navigationController pushViewController:HTVC animated:YES];
            
            
        };
        return cell;
        
    }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    if (YJVideoNewDisplayTypeOn) {
        return self.dataArray.count;
    }
    else{
        return 1;
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (YJVideoNewDisplayTypeOn) {
        NSDictionary * categoryMutableReqestDiction = [self.dataArray objectAtIndex:section];
        
        //    NSDictionary * categoryMutableReqestDiction = [souceArray objectAtIndex:section];
        NSArray *programList = [categoryMutableReqestDiction objectForKey:@"Array"];
        if ((programList.count == 1)) {
            id objc = [programList objectAtIndex:0];
            if ([objc isKindOfClass:[NSString class]]) {
                return 0;
            }
        }
        if (section == 0) {
            return 30;
        }else{
        return 40;
        }
    }
    
    return 0;
}

//每个分段的头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (YJVideoNewDisplayTypeOn) {
        CGFloat sectionHeigh = section == 0 ? 30 : 40;
        
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), sectionHeigh)];
        headerView.userInteractionEnabled = YES;
        UIImageView *background = [UIImageView new];
        background.userInteractionEnabled = YES;
        [headerView addSubview:background];
        
        CGFloat HighOfSectionBlow = 8;
        CGFloat space = 5;
        
        UIView *BlackLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (5+HighOfSectionBlow)*kRateSize)];
        BlackLine.backgroundColor =App_background_color;
        if (section == 0) {
            BlackLine.backgroundColor =[UIColor whiteColor];
            BlackLine.frame = CGRectMake(0, 0, kDeviceWidth, 0);
        }
        [headerView addSubview:BlackLine];
        
        
        if (section == 0) {
            space = 0;
            HighOfSectionBlow = 2;
            
        }else{
            
            
        }
        
        [background mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headerView.mas_top).offset((HighOfSectionBlow)*kRateSize);
            make.leading.equalTo(headerView.mas_leading);
            make.right.equalTo(headerView.mas_right);
            make.bottom.equalTo(headerView.mas_bottom);
        }];
        
        background.image = [UIImage imageNamed:@"bg_home_section_header@2x"];
        
        //****************下面的视图都是background 的子视图
        //左边线段
        UIImageView *Leftline = [[UIImageView alloc]initWithFrame:CGRectMake(5*kRateSize, (5+HighOfSectionBlow), 3, 18)];
        Leftline.image = [UIImage imageNamed:@"ic_vertical_yellow_line"];
        //    Leftline.backgroundColor = [UIColor grayColor];
        Leftline.layer.cornerRadius = 2.5;
        Leftline.clipsToBounds =YES;
        [background addSubview:Leftline];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13*kRateSize, (HighOfSectionBlow-1), 100*kRateSize, 30)];
        //********数据中的头标题的名称
        //        NSArray *categoryArray = [self currentDataSource];
        NSDictionary *sectionDiction = [self.dataArray objectAtIndex:section];
        NSString *str =[sectionDiction objectForKey:@"name"];
        label.text = str;
        
        label.textColor = [UIColor blackColor];
        
        
        NSArray *array = [sectionDiction objectForKey:@"videoList"];

        if(array.count >= 4)
        {
            UIButton_Block *btn = [UIButton_Block new];
            
            btn.frame =   CGRectMake(FRAME_W(self.view) - (60-7)*kRateSize, (0+HighOfSectionBlow)*kRateSize, 60*kRateSize, 30);
            
            btn.titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
            [btn setTitle:@"更多" forState:UIControlStateNormal];
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
            //                    btn.backgroundColor =[UIColor grayColor];
            btn.serviceIDName = label.text;
            [btn setClick:^(UIButton_Block*btn,NSString* name){
                
                NSString *title = [sectionDiction objectForKey:@"name"];
                NSString *categoryID =[sectionDiction objectForKey:@"id"];
                NSLog(@"%@ 频道:%@",categoryID,title);
                
                YJHealthVideoSecondViewcontrol *healthVideoSecond = [[YJHealthVideoSecondViewcontrol alloc]init];
                //                demandSecond.isMovie = [self isMovieOR:btn.serviceIDName];
                healthVideoSecond.categoryID = categoryID;
                //                healthVideoSecond.operationCode = _currentCode;
                healthVideoSecond.name = title;
                [self.navigationController pushViewController:healthVideoSecond animated:YES];
                
                
            }];
            
            UIImageView *Rightline = [[UIImageView alloc]initWithFrame:CGRectMake(12*kRateSize, 8, 1*kRateSize, 14)];
            Rightline.backgroundColor = App_selected_color;
            Rightline.layer.cornerRadius = 2.5;
            Rightline.clipsToBounds =YES;
            [btn addSubview:Rightline];
            
            [background addSubview:btn];
            
        }
        [background addSubview:label];
        
        
        //            UIView *BlackLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (5+HighOfSectionBlow)*kRateSize)];
        //            BlackLine.backgroundColor =App_background_color;
        //            if (section == 0) {
        //                BlackLine.backgroundColor =[UIColor whiteColor];
        //                BlackLine.frame = CGRectMake(0, 0, kDeviceWidth, 0);
        //            }
        //            [headerView addSubview:BlackLine];
        
        return headerView;
    }
    else
    {
        return nil;
    }

    
}

#pragma  mark - navigation上的按钮
- (void)Back
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"返回点播首页");
}
#pragma mark- 工具类

- (void)loadData{
    
    [self showLoading];
    
    _requestYJVideoBtn.hidden = YES;
    
//    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//    if (regionCode.length==0) {
//        regionCode = REGION_CODE;
//    }
    
    HTRequest *_request = [[HTRequest alloc]initWithDelegate:self];
    [_request YJHealthyTypeListVideoWithGlobal:@"1" withInstanceCode:KInstanceCode WithVideoCount:@"4"];
    
//    if (YJVideoNewDisplayTypeOn && self.HospitalType == HospitalTypeYiVideo) {
//        [_request YJHealthyTypeListVideoWithVideoCount:@"4"];
//    }
//    else
//    {
//        [_request EPGGetRegionInfoWithRegionCode:regionCode andTimeout:5];
//    }

}

- (void)showLoading
{
    [self showCustomeHUD];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}
- (void)hideLoading
{
    _HTVw.hidden = YES;
    
    [self hiddenCustomHUD];
    NSLog(@"完成消失");
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    });
//    [MBProgressHUD hideAllHUDsForView:self.view  animated:YES];
}
//可以放在工具类中，开一个时间是不是超时了
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1 andTime:(NSInteger) time
{
    if ([date1 timeIntervalSinceDate:date] >= time) {
        NSLog(@"超时了啊");
        return YES;
    }
    return NO;
}
//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else if(YJVideoNewDisplayTypeOn)
    {
        return numb % 2 == 0?  numb/2 : numb/2+1;
    }else
    {
        return numb % 3 == 0?  numb/3 : numb/3+1;
    }
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    if(YJVideoNewDisplayTypeOn){
        if (array.count > index*2) {
            [anser addObject:array[index*2]];
        }
        if (array.count > index*2+1) {
            [anser addObject:array[index*2+1]];
        }
    }else{
        if (array.count > index*3) {
            [anser addObject:array[index*3]];
        }
        if (array.count > index*3+1) {
            [anser addObject:array[index*3+1]];
        }
        if (array.count > index*3+2) {
            [anser addObject:array[index*3+2]];
        }
    }
    
    return anser;
}

-(void)loadingHospitalURLWithDic:(NSDictionary *)hospitalDic{
    NSMutableString *allUrlLink = [NSMutableString stringWithFormat:@"%@",[[hospitalDic objectForKey:@"region"] objectForKey:@"urlLink"]];
    
    
    NSArray *urlArr = [[NSArray alloc] initWithArray:[allUrlLink componentsSeparatedByString:@"|"]];
    NSString *microHospital = @"";
    NSString *chuzhenStr = @"";
    NSString *jiuzhenStr = @"";
    
    for (int i=0; i<urlArr.count; i++) {
        switch (i) {
            case 0:
            {
                microHospital = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                break;
            }
            case 1:
            {
                chuzhenStr = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                
                break;
            }
            case 2:
            {
                jiuzhenStr = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                
                break;
            }
            default:
                break;
        }
    }

   
    
    [[NSUserDefaults standardUserDefaults] setObject:microHospital forKey:@"microHospital"];
    [[NSUserDefaults standardUserDefaults] setObject:chuzhenStr forKey:@"chuzhenStr"];
    [[NSUserDefaults standardUserDefaults] setObject:jiuzhenStr forKey:@"jiuzhenStr"];


    
    if (self.HospitalType == HospitalTypeYiZhuShou) {
        //            NSString *url = [[_hospital objectForKey:@"region"] objectForKey:@"urlLink"];
        NSString *url = Home_JIUZHENZHUSHOU;
        
        if (url.length > 6) {
            [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        }else{
            //弹出提示框
            [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
            [self hideLoading];
        }
        
    }else if (self.HospitalType == HospitalTypeYiZhuanJia){
        NSString *url = Home_ZHUANJIACHUZHEN;
        
        if (url.length>6) {
            [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        }else{
            //弹出提示框
            [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
            [self hideLoading];
        }
        
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//section‘headView follow scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (YJVideoNewDisplayTypeOn) {
        float MaxHighOfSectionHeadView = (35+5)*kRateSize;;
        if (scrollView.contentOffset.y < MaxHighOfSectionHeadView && scrollView.contentOffset.y > 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }else if (scrollView.contentOffset.y >= MaxHighOfSectionHeadView){
            scrollView.contentInset = UIEdgeInsetsMake(-MaxHighOfSectionHeadView, 0, 0, 0);
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
