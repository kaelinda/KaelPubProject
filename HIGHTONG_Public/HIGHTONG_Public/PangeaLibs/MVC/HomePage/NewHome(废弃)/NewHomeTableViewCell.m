//
//  NewHomeTableViewCell.m
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//


#import "NewHomeTableViewCell.h"
#import "Masonry.h"
#import "ProgramView.h"
#import "NewHomeView.h"
@implementation NewHomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
        self.dateArray = [NSMutableArray array];
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView
{
    
    WS(wself);
    
    UIView *superView = self.contentView;
    superView.backgroundColor = [UIColor whiteColor];
    self.leftView = [NewHomeView new];
    [superView addSubview:self.leftView];
    
    
    
    self.rightView = [NewHomeView new];
    [superView addSubview:self.rightView];
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(0*kRateSize);
        make.top.equalTo(superView.mas_top).offset(0*kRateSize);
        make.bottom.equalTo(superView.mas_bottom).offset(0*kRateSize);
        make.width.equalTo(self.rightView);
        
    }];
    
    
    self.leftView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"zou视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
    
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftView.mas_right).offset(0*kRateSize);
        make.right.equalTo(superView.mas_right).offset(0*kRateSize);
        make.top.equalTo(superView.mas_top).offset(0*kRateSize);
        make.bottom.equalTo(superView.mas_bottom).offset(0*kRateSize);
        make.width.equalTo(self.rightView);
        make.width.equalTo(self.leftView);
    }];
    
    UIView *VerticalLine = [UIView new];
    [self.leftView addSubview:VerticalLine];
    VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftView.mas_left).offset(0);
        make.width.equalTo(self.leftView.mas_width);
        make.height.equalTo(@(1));
        make.top.equalTo(self.leftView.mas_bottom).offset(-1);
    }];
    
    UIView *VerticalLineRight = [UIView new];
    [self.rightView addSubview:VerticalLineRight];
    VerticalLineRight.backgroundColor = UIColorFromRGB(0xeeeeee);
    [VerticalLineRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.rightView.mas_left).offset(0);
        make.width.equalTo(self.rightView.mas_width);
        make.height.equalTo(@(1));
        make.top.equalTo(self.rightView.mas_bottom).offset(-1);
    }];
    
    UIView *HirezantalLine = [UIView new];
    [superView addSubview:HirezantalLine];
    HirezantalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [HirezantalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftView.mas_right).offset(-1);
        make.width.equalTo(@(1));
        make.height.equalTo(self.leftView.mas_height);
        make.top.equalTo(self.leftView.mas_top);
    }];
    
    self.rightView.block = ^(UIButton_Block*btn)
    {
        if (wself.block) {
            NSLog(@"you视图直达cell选了第几个");
            wself.block(btn);
        }
    };
    
    
    
}
- (void)update
{
    
    //    NSLog(@"0.5时间%@",[NSDate date]);
    if (self.dateArray.count == 1) {
        NSDictionary *dic = self.dateArray[0];
        //        NSLog(@"%d: %@",0 ,dic );
        if ([dic isKindOfClass:[NSDictionary class]]) {
            self.leftView.dic = dic;
            
            self.rightView.dic = nil;
            
            self.leftView.isMovie = self.isMovie;
            
            [self.leftView updata];
            
            [self.rightView updata];
        }
    }
    if (self.dateArray.count == 2) {
        NSDictionary *dic = self.dateArray[0];
        NSDictionary *dic1 = self.dateArray[1];
        //        NSLog(@"%d: %@",1 ,dic );
        if ([dic isKindOfClass:[NSDictionary class]]) {
            
            self.leftView.dic = dic;
            
            self.rightView.dic = dic1;
            
            
            
            [self.leftView updata];
            
            [self.rightView updata];
        }
    }
    
    
}
@end

