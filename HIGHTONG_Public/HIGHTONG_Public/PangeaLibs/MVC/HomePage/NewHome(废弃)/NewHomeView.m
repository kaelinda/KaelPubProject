//
//  NewHomeView.m
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewHomeView.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton_Block.h"


@interface NewHomeView ()
{
    
//    UIButton_Block * _btn;
    UIImageView *BGview;
    UIImageView * Badge ;
    
}
@end


@implementation NewHomeView

- (instancetype)init
{
    if (self = [super init]) {
        //        self.backgroundColor = [UIColor lightGrayColor];
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
//    superView.backgroundColor = UIColorFromRGB(0xf7f7f7);
//    UIView *ColorView = [UIView new];
//    [superView addSubview:ColorView];
//    [ColorView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left);
//        make.right.equalTo(self.mas_right).offset(-0*kRateSize);;
//        make.top.equalTo(self.mas_top);
//        make.bottom.equalTo(self.mas_bottom).offset(-0*kRateSize);
//    }];
//    ColorView.backgroundColor = [UIColor whiteColor];
//    superView = ColorView;
    
    self.photo = [UIImageView new];
    self.photo.contentMode = UIViewContentModeScaleAspectFill;
//    self.photo.contentMode = UIViewContentModeScaleToFill;
    [superView addSubview:self.photo];
    
    
    
//    self.subTitle = [UILabel new];
//    [superView addSubview:self.subTitle];
//    
    
//    self.timeTitle = [UILabel new];
//    [superView addSubview:self.timeTitle];
    self.photo.backgroundColor = UIColorFromRGB(0xffffff);
    [self.photo mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.edges.equalTo(superView);
        make.centerY.equalTo(superView.mas_centerY);
        make.centerX.equalTo(superView.mas_centerX);
//        make.width.equalTo(@(132));
//        make.height.equalTo(@(66));
        make.width.equalTo(@(150*kRateSize));
        make.height.equalTo(@(78*kRateSize));
//        //        make.top.equalTo(superView.mas_top).offset(3*kRateSize);
//        make.left.equalTo(superView.mas_left).offset(3*kRateSize);
//        make.width.equalTo(@(51*kRateSize));
//        make.height.equalTo(@(68*kRateSize));
        //         make.bottom.equalTo(superView.mas_bottom).offset((-20)*kRateSize);
    }];
    
    //    self.photo.backgroundColor = [UIColor redColor];
    //    //    self.photo.contentMode = UIViewContentModeScaleAspectFit;
//    self.photo.image =[UIImage imageNamed:@"poster"] ;
    //
    //    self.photo.backgroundColor = UIColorFromRGB(0xe3e2e3);
    //    self.photo.layer.cornerRadius = 5;
    //    self.photo.layer.masksToBounds = YES;
    //
//    Badge = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_common_special_net"]];
//    [self.photo addSubview:Badge];
//    [Badge mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.photo.mas_top);
//        make.right.equalTo(self.photo.mas_right);
//        make.width.equalTo(@(21*kRateSize));
//        make.height.equalTo(@(21*kRateSize));
//        
//    }];
//    
//    
//    self.title = [UILabel new];
//    self.title.textColor = [UIColor blackColor];
//    [superView addSubview:self.title];
//    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.photo.mas_right).offset(4*kRateSize);
//        make.right.equalTo(superView.mas_right);
//        make.bottom.equalTo(self.photo.mas_bottom).offset(0*kRateSize);
//        make.height.equalTo(@(20*kRateSize));
//    }];
//    
//    
    _btn  = [UIButton_Block new];
    [superView addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(superView);
    }];
    
//    UIImage *HImage = [UIImage imagewit]
    
//    [_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    
    [_btn addTarget:self action:@selector(highLightColor) forControlEvents:UIControlEventTouchDown];
    [_btn addTarget:self action:@selector(NOhighLightColor) forControlEvents:UIControlEventTouchUpOutside];
    [_btn addTarget:self action:@selector(NOhighLightColor) forControlEvents:UIControlEventTouchCancel];



    
//
//    [self.subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.photo.mas_right).offset(4*kRateSize);
//        make.right.equalTo(superView.mas_right).offset(-10*kRateSize);
//        make.top.equalTo(self.photo.mas_top).offset(0*kRateSize);
//        make.height.equalTo(@(40*kRateSize));
//        
//    }];
//    self.subTitle.numberOfLines = 2;
//    self.subTitle.text = @"当前没有数据";
//    
//    UIFont *font  = [UIFont systemFontOfSize:13];
//    //    self.title.font = font;
//    
//    
//    self.subTitle.adjustsFontSizeToFitWidth = YES;
//    
////    self.subTitle.font = font;
////    self.subTitle.textAlignment = NSTextAlignmentLeft;
//    
////    font =  [UIFont systemFontOfSize:12];
////    self.title.font = font;
//    self.title.adjustsFontSizeToFitWidth = YES;
////    self.subTitle.backgroundColor = [UIColor redColor];
    
}
- (void)highLightColor{
    NSLog(@"higt light");
    self.backgroundColor = UIColorFromRGB(0xf1f8ff);
    self.photo.backgroundColor = UIColorFromRGB(0xf1f8ff);
}
- (void)NOhighLightColor{
    NSLog(@"no higt light");
    self.backgroundColor = UIColorFromRGB(0xffffff);
    self.photo.backgroundColor = UIColorFromRGB(0xffffff);
}
- (void)updata
{
    
    WS(wself);
    
    //    contentID = 8e0e314d1cb24bd9b60b74e1a;
    //    imageLink = "http://192.168.3.23//CMS/20150814/2015081410400391451_450_645.jpg?id=652&crc=1484819196";
    //    name = "\U7834\U574f\U8005";
    //    seq = 3;
    NSDictionary *dic = self.dic;
    if (dic) {
//        self.hidden = NO;
//        
//        if ([APP_ID isEqualToString:@"1099034502"]) {
        
            if ([dic objectForKey:@"moduleLogo"]) {
//                [self.photo sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat([dic objectForKey:@"moduleLogo"])] placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"]];
                [self tranAnimationWith:self.photo andImageLink:[dic objectForKey:@"moduleLogo"]];
            }
            
            
            _btn.eventName = self.subTitle.text;
            _btn.Diction = dic;
            _btn.Click = ^(UIButton_Block*btn,NSString * name)
            {
                if (wself.block) {
                    NSLog(@"back light");
                    [wself NOhighLightColor];
                    NSLog(@"cell知道点击了那个view");
                    wself.block(btn);
                }
            };
            
//        }
//        else
//        {
//            //再赋值之前要判断   电影，  电视剧  综艺   或者其他， 以显示不同的效果
//            if ([dic objectForKey:@"amount"] &&  [dic objectForKey:@"lastEpisode"]) {//电视剧应为有总集数
//                NSString *amout = [dic objectForKey:@"amount"];
//                NSString *lastEpisode = [dic objectForKey:@"lastEpisode"];
//                NSString *name = [dic objectForKey:@"name"];
//                NSString *lastEpisodeTitle = [dic objectForKey:@"lastEpisodeTitle"];
//                //            if ([lastEpisode integerValue] ==  [amout integerValue]) {
//                //                self.title.text = [NSString stringWithFormat:@"%@集全",amout];
//                //            }else
//                //            {
//                //                self.title.text = [NSString stringWithFormat:@"更新至%@集",lastEpisode];
//                //            }
//                if (lastEpisodeTitle.length) {
//                    self.title.text = [NSString stringWithFormat:@"%@",lastEpisodeTitle];
//                }
//                
//            }else
//            {
//                self.title.text = @"";
//            }
//            
//            
//            if (self.isMovie) {
//                self.title.hidden = YES;
//                BGview.hidden = YES;
//            }else
//            {
//                self.title.hidden = NO;
//                BGview.hidden = NO;
//            }
//            
//            if([[dic objectForKey:@"flagPlayOuter"] isEqualToString:@"1"])
//            {
//                Badge.hidden = NO;
//            }else{
//                Badge.hidden = YES;
//            }
//            
//            if ([dic objectForKey:@"name"]) {
//                self.subTitle.text = [dic objectForKey:@"name"];
//            }
//            
//            if ([dic objectForKey:@"imageLink"]) {
//                [self.photo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"poster"]];
//            }
//            if ([dic objectForKey:@"image"]) {
//                self.photo.image = [UIImage imageNamed:[dic objectForKey:@"image"]];
//            }
//            
//            if ([dic objectForKey:@"contentID"]) {
//                _btn.serviceIDName = [dic objectForKey:@"contentID"];
//            }
//            _btn.eventName = self.subTitle.text;
//            _btn.Diction = dic;
//            _btn.Click = ^(UIButton_Block*btn,NSString * name)
//            {
//                if (_block) {
//                    NSLog(@"back light");
//                    [self NOhighLightColor];
//                    NSLog(@"cell知道点击了那个view");
//                    _block(btn);
//                }
//            };
//        }

        
    }else
    {
        self.hidden = YES;
        self.photo.image =[UIImage imageNamed:@"poster"] ;
        self.title.text = @"";
        self.subTitle.text = @"";
        self.timeTitle.text = @"";
    }
    
}


-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink{

    CATransition *animation = [CATransition animation];
    //动画时间
    animation.duration = 1.0f;
    //display mode, slow at beginning and end
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //在动画执行完时是否被移除
    animation.removedOnCompletion = YES;
    //过渡效果
    animation.type = kCATransitionFade;
//    animation.type = @"rippleEffect";

    //过渡方向
    animation.subtype = kCATransitionFromTop;
    //    //暂时不知,感觉与Progress一起用的,如果不加,Progress好像没有效果
    //    animation.fillMode = kCAFillModeForwards;
    //    //动画停止(在整体动画的百分比).
    //    animation.endProgress = 1;
    [imageView sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(imageLink)]  placeholderImage:[UIImage imageNamed:@"img_home_medical_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [imageView.layer addAnimation:animation forKey:nil];
    }];
    

    

}



@end

