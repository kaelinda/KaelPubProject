//
//  WIFIAreaSelectView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"


#define UP_WIFI_INTROL @"连接健康电视指定场所的免费WiFi"
#define DOWN_WIFI_INTROL @"可享受更多服务哟！"
#define FOUND_MORE_WIFI_AREA @"查看更多免费WiFi场所"

typedef NS_ENUM(NSUInteger, NetWorkingType) {
    NoNetWorking,
    MoveCellularWorking,
    WiFiWorking,
};

typedef void(^IsHiddenBlock)(BOOL);

@interface WIFIAreaSelectView : UIView

/**
 *  半透明黑色背景
 */
@property (nonatomic,strong) UIImageView *BGImageView;
/**
 *  主要视图的承载图 这里有背景图片 所以用了Iamge
 */
@property (nonatomic,strong) UIImageView *contentImageView;
/**
 *  当前网络名称
 */
@property (nonatomic,strong) UILabel *networkingNameLabel;
/**
 *  免费WIFI场所介绍字样上半部分
 *  连接健康电视指定场所的免费WIFI可享受更多服务哟！
 *  连接健康电视指定场所的免费WIFI
 *  这里还是分开来写的好字体居中显示
 */
@property (nonatomic,strong) UILabel *upWIFIAreaIntrolLabel;
/**
 *  免费WIFI场所介绍字样下半部分
 *  连接健康电视指定场所的免费WIFI可享受更多服务哟！
 *  可享受更多服务哟！
 *  这里还是分开来写的好字体居中显示
 */
@property (nonatomic,strong) UILabel *downWIFIAreaIntrolLabel;

/**
 *  更多WIFI按钮
 */
@property (nonatomic,strong) UIButton_Block *moreWIFIAreaBtn;

//block  最好是用strong 或者 copy  retain的时候是会有问题的
/**
 *  更多WIFI按钮的响应事件
 */
@property (nonatomic,strong) void (^moreWIFIActionBlock)();
/**
 *  是否调用过hidden方法
 */
@property (nonatomic,copy) IsHiddenBlock hiddenBlcok;




//*******************下面是接口了
-(void)resetNetWorkingStatusWith:(NetWorkingType)netWorkingType andNetWorkingName:(NSString *)netWorkingaNme;





@end
