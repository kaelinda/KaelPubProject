//
//  WIFIAreaSelectView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "WIFIAreaSelectView.h"

@implementation WIFIAreaSelectView


-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self initSubViews];
        
    }
    return self;
}


-(void) initSubViews{
    WS(wself);
    _BGImageView = [[UIImageView alloc] init];
    [_BGImageView setBackgroundColor:[UIColor blackColor]];
    _BGImageView.alpha = 0.5;
    _BGImageView.userInteractionEnabled = YES;
    
    [self addSubview:_BGImageView];
    
    
    [_BGImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself);
    }];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelAction)];
    tap.numberOfTapsRequired = 1;
    [_BGImageView addGestureRecognizer:tap];
    
    _contentImageView = [[UIImageView alloc] init];
    _contentImageView.image = [UIImage imageNamed:@"bg_home_area_wifi"];
    _contentImageView.userInteractionEnabled = YES;
    _contentImageView.contentMode = UIViewContentModeScaleToFill;
    [_contentImageView setClipsToBounds:YES];
    [self addSubview:_contentImageView];
    
    [_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.mas_top).offset(-6*kRateSize);
        make.left.mas_equalTo(wself);
        make.right.mas_equalTo(wself);
        make.height.mas_equalTo(170*kRateSize);
    }];
    
    //***********当前网络名称
    _networkingNameLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:App_selected_color andFont:App_font(16)];
    [_networkingNameLabel setText:@"当前无可用网络"];
    [_contentImageView addSubview:_networkingNameLabel];
    
    [_networkingNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself).offset(15*kRateSize);
        make.width.mas_equalTo(wself);
        make.left.mas_equalTo(wself);
        make.height.mas_equalTo(@(30*kRateSize));
    }];
    
    //**********免费WiFi场所 上部分介绍
    _upWIFIAreaIntrolLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:UIColorFromRGB(0x333333) andFont:App_font(17)];
    [_upWIFIAreaIntrolLabel setText:UP_WIFI_INTROL];
    [_contentImageView addSubview:_upWIFIAreaIntrolLabel];
    
    [_upWIFIAreaIntrolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_networkingNameLabel.mas_bottom).offset(5*kRateSize);
        make.left.mas_equalTo(wself);
        make.width.mas_equalTo(wself);
        make.height.mas_equalTo(30);
    }];
    
    //**********免费WiFi场所 下半部分介绍
    _downWIFIAreaIntrolLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:[UIColor blackColor] andFont:App_font(17)];
    [_downWIFIAreaIntrolLabel setText:DOWN_WIFI_INTROL];
    [_contentImageView addSubview:_downWIFIAreaIntrolLabel];
    
    [_downWIFIAreaIntrolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_upWIFIAreaIntrolLabel.mas_bottom);
        make.left.mas_equalTo(wself);
        make.width.mas_equalTo(wself);
        make.height.mas_equalTo(30);
    }];
    

    //*************更多免费WiFi按钮
//    __weak void (^wBlock)()  = _moreWIFIActionBlock;
    _moreWIFIAreaBtn = [[UIButton_Block alloc] init];
    _moreWIFIAreaBtn.Click = ^(UIButton *btn, NSString *str){
        [wself setHidden:YES];

        if (wself.moreWIFIActionBlock) {
            wself.moreWIFIActionBlock();
        }else{
            NSLog(@"你还没实现查看更多免费WiFi场所的 Block 呢 亲~");
        }
        
    };
    [_moreWIFIAreaBtn setTitle:FOUND_MORE_WIFI_AREA forState:UIControlStateNormal];
    [_moreWIFIAreaBtn setTitleColor:App_white_color forState:UIControlStateNormal];
    [_moreWIFIAreaBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [_moreWIFIAreaBtn.titleLabel setFont:App_font(17)];
    
//    [_moreWIFIAreaBtn setImage:[UIImage imageNamed:@"btn_chakanwifi"] forState:UIControlStateNormal];
    [_moreWIFIAreaBtn setBackgroundColor:App_selected_color];
    [_contentImageView addSubview:_moreWIFIAreaBtn];
    
    
    CGSize btnsize = CGSizeMake(230*kRateSize, 40*kRateSize);
    [_moreWIFIAreaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnsize);
        make.top.mas_equalTo(_downWIFIAreaIntrolLabel.mas_bottom).offset(kRateSize>1?10:0);
        make.centerX.mas_equalTo(wself);
    }];
    
    
    
    


}
-(void)cancelAction{
    self.hidden = YES;
}

-(void)setHidden:(BOOL)hidden{
    
    [super setHidden:hidden];
    
    
    if (_hiddenBlcok) {
        _hiddenBlcok(hidden);
        
    }else{
        NSLog(@"WiFi场所视图隐藏了 你是不是该实现一下逼得行为操作呢？");
    }
   
    
    return;
    WS(wself);
    if (hidden) {
        
        [_contentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(wself.mas_top).offset(-6*kRateSize);
            make.left.mas_equalTo(wself);
            make.right.mas_equalTo(wself);
            make.height.mas_equalTo(0*kRateSize);
        }];
        
    }else{
        [super setHidden:hidden];

        [_contentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(wself.mas_top).offset(-6*kRateSize);
            make.left.mas_equalTo(wself);
            make.right.mas_equalTo(wself);
            make.height.mas_equalTo(170*kRateSize);
        }];
    }
    
    

    // tell constraints they need updating
    [self setNeedsUpdateConstraints];
    
    // update constraints now so we can animate the change
    [self updateConstraintsIfNeeded];
    
    
    [UIView animateWithDuration:0.15 animations:^{
        [self layoutIfNeeded];
        if (hidden) {
            _BGImageView.alpha = 0;
        }else{
            _BGImageView.alpha = 0.6;
        }
    } completion:^(BOOL finished) {
        [super setHidden:hidden];
        
    }];
}

-(void)resetNetWorkingStatusWith:(NetWorkingType)netWorkingType andNetWorkingName:(NSString *)netWorkingaNme{

    switch (netWorkingType) {
        case NoNetWorking:
        {
            _networkingNameLabel.text = @"当前无可用网络";
            break;
        }
        case MoveCellularWorking:
        {
            _networkingNameLabel.text = @"您正在使用移动网络";
            break;
        }
        case WiFiWorking:
        {
            _networkingNameLabel.text = [NSString stringWithFormat:@"您已连接到:%@",netWorkingaNme];
            break;
        }
        default:
            break;
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
