//
//  YJHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "YJHomeViewController.h"
#import "UIButton_Block.h"
#import "PosterViewofTB.h"
#import "SearchViewController.h"
#import "NewHomeTableViewCell.h"
#import "NewHomeView.h"
#import "Tool.h"
#import "HospitalViewController.h"
#import "LiveHomeViewController.h"
#import "ScrollDemandPageViewController.h"
#import "HomeWebViewViewController.h"
#import "MineRecorderViewController.h"
#import "DemandVideoPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandChooseHospitalControl.h"
#import "MineRecorderViewController.h"
#import "DemandChooseHospitalControl.h"
#import "WIFIAreaSelectView.h"
#import "KnowledgeBaseViewController.h"
#import "ExtranetPromptViewController.h"

#define WeiHospital 40

#define K_USER   (IsLogin ? KMobileNum : [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"])

@interface YJHomeViewController ()<HTRequestDelegate>
{
    
    UIView *_Headview; // tableview的头
    
    UILabel *_seachTextView;//搜索热词
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    UIImageView *_RecordView;//播放记录
    
    NSString *_continueToReadDate;//续看时间
    
    BOOL _haveRecord;//有播放记录
    
    NSString *_channelID;
    NSString *_endDate;
    
    NSInteger _requestCount;//请求失败次数
    
    NSDictionary *_VODdictionDic;//点播
    NSDictionary *_EPGdictionDic;//点播
    WIFIAreaSelectView *_wifiView;

    
}
@property (nonatomic, strong) UIButton *wifiBtn;
@property (nonatomic, strong) UIButton *historyBtn;
@property (strong ,nonatomic) UITableView    * tableView; //首页热门节目列表
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页直播推荐  影视精选 等 数据
@property (strong,nonatomic) UILabel *XUKanLabel;         //续看标签
@property (strong,nonatomic) UIButton_Block *XUKanBtn;         //续看按钮
@property (strong,nonatomic) UIButton_Block *moreHospitalBtn;  //更多医院按钮
@end


@implementation YJHomeViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    if (regionCode.length == 0) {
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request YJIntroduceListWithAreaId:@"" withPageNo:@"0" withPageSize:@"2"];
        
//        [request EPGGetRegionListWithTimeout:10];
    }
    else
    {
        [self reqestHome];
    }

    [self.view bringSubviewToFront:_wifiView];
    [_wifiView setHidden:YES];

    [MobCountTool pushInWithPageView:@"YJHomePage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobCountTool popOutWithPageView:@"YJHomePage"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
//    [request EPGGetRegionListWithTimeout:10];
    [request YJIntroduceListWithAreaId:@"" withPageNo:@"0" withPageSize:@"2"];
  
    // Do any additional setup after loading the view.
    [self setNaviBarTitle:kAPP_NAME];
    [UIApplication sharedApplication].statusBarHidden = NO;
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    _continueToReadDate = @"1997-12-01 00:00:00";
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestHome) name:@"loginSuccess" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestRecord) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestYJHomeData) name:@"RefreshHome" object:nil];
    
    [self setSeachBar];
    
    [self setUpHead];
    
//    [self reqestHomeDate];
    
    WS(wself);
    
    _wifiView = [[WIFIAreaSelectView alloc] init];
    _wifiView.hidden = YES;
    [[[UIApplication sharedApplication].delegate window] addSubview:_wifiView];
    __weak UIButton *wWIFIBtn = _wifiBtn;
    _wifiView.moreWIFIActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");

        wWIFIBtn.selected = NO;
//        HospitalViewController *hospi = [[HospitalViewController alloc]init];
//        hospi.HospitalType = HospitalTypeMoreWiFi;
//        hospi.YiYuanTitle = @"更多免费WiFi场所";
//        [wself.navigationController pushViewController:hospi animated:YES];
        
        
        NSString *fullUrlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"moreWifiLink"];
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@",fullUrlStr,KREGION_CODE,KInstanceCode,App_Theme_Color_num];
        [wself.navigationController pushViewController:know animated:YES];
     
    };
    _wifiView.hiddenBlcok = ^(BOOL isHidden){

        wWIFIBtn.selected = !isHidden;

    };
    
    UIEdgeInsets wifiEdg = UIEdgeInsetsMake(64, 0, 0, 0);

    [_wifiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wifiEdg);
    }];
    
    
}

- (void)reRequestYJHomeData
{
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    if (regionCode.length == 0) {
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
//        [request EPGGetRegionListWithTimeout:10];
        [request YJIntroduceListWithAreaId:@"" withPageNo:@"0" withPageSize:@"2"];
        
    }
    
     [self reqestHomeDate];
}



- (void)setUpHead{
    
    WS(wself);
    
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    [self.view addSubview:self.tableView];
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_background_color;
    [self.view addSubview:view];
    
    //去除自动适应scrollview
    //初始化tableView
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    image.frame = self.tableView.frame;
    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = App_background_color;
    
    //    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.separatorColor = UIColorFromRGB(0xeeeeee);
//    self.tableView.separatorColor = [UIColor redColor];
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
    self.tableView.tableFooterView = [[UIView alloc] init];
        //头View...
        

    _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+ WeiHospital - 3)*kRateSize)];
//  (154+kheigh+WeiHospital*1)*kRateSize-2.5*kRateSize
    
    _Headview.backgroundColor = UIColorFromRGB(0xffffff);
    //AD广告页
    self.posterView = [[PosterViewofTB alloc]init];
    [_Headview addSubview:self.posterView];
    [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_Headview.mas_top );
        make.left.equalTo(_Headview.mas_left);
        make.right.equalTo(_Headview.mas_right);
        make.height.equalTo(@((154 + kheigh)*kRateSize));
    }];
    self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
    self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
    
    {
        
        _RecordView = [[UIImageView alloc]init];
        _RecordView.backgroundColor = [UIColor whiteColor];
        
        
        [_Headview addSubview:_RecordView];
        
        [_RecordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(40*kRateSize));
        }];
//        _RecordView.hidden = YES;
        _RecordView.userInteractionEnabled =  YES;
        
        //节目Label
        self.XUKanLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 5*kRateSize, 200*kRateSize, 30*kRateSize)];

        self.XUKanLabel.userInteractionEnabled = YES;
        self.XUKanLabel.numberOfLines = 1;


        //            UIFont *font  =  [UIFont fontWithName:@"Arial" size:18];
        UIFont *font  =  [UIFont systemFontOfSize:16];
        self.XUKanLabel.font = font;
        self.XUKanLabel.tintColor = UIColorFromRGB(0x333333);

        [_RecordView addSubview:self.XUKanLabel];
        //播放记录Button
        _moreHospitalBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
        _moreHospitalBtn.frame = CGRectMake(FRAME_W(self.view) - 105*kRateSize, 5*kRateSize, 80*kRateSize, 30*kRateSize);
        UIImageView* inImageView = [UIImageView new];
        [_moreHospitalBtn addSubview:inImageView];
        [inImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_moreHospitalBtn.mas_centerY);
            make.left.equalTo(_moreHospitalBtn.mas_right);
            make.width.equalTo(@(25*kRateSize));
            make.height.equalTo(@(40*kRateSize));
        }];
        _moreHospitalBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _moreHospitalBtn.hidden = YES;
        
        inImageView.image = [UIImage imageNamed:@"ic_arrow_right_blue"];//公版改进
        _moreHospitalBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_moreHospitalBtn setTitle:@"更换医院" forState:UIControlStateNormal];
        [_moreHospitalBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        _moreHospitalBtn.userInteractionEnabled = NO;
        _moreHospitalBtn.serviceIDName = @"selina";
        _moreHospitalBtn.backgroundColor = UIColorFromRGB(0xffffff);
        
        [_moreHospitalBtn setClick:^(UIButton_Block*btn,NSString* name){
            
            NSLog(@"%@ name:%@",[btn currentTitle],name);
            
            DemandChooseHospitalControl *vc = [[DemandChooseHospitalControl alloc]init];

            vc.YiYuanTitle = @"更换医院";
            
            [wself.navigationController pushViewController:vc animated:YES];
            
        }];
        
        
        
        [_RecordView addSubview:_moreHospitalBtn];
        
        UIView *recordVerLine = [UIView new];
        recordVerLine.backgroundColor = UIColorFromRGB(0xeeeeee);
        [_RecordView addSubview:recordVerLine];
        [recordVerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_RecordView.mas_left);
            make.right.equalTo(_RecordView.mas_right);
            make.height.equalTo(@(1));
            make.bottom.equalTo(_RecordView.mas_bottom).offset(-1);
            
        }];
        
        
        self.tableView.tableHeaderView = _Headview;
        
        //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
        self.tableView.delaysContentTouches = YES;
        
    }
   
    self.dataArray = [NSMutableArray array];
    //6个模块的图片名字和跳转title
    if ([APP_ID isEqualToString:@"1071241443"] || [APP_ID isEqualToString:@"1107480482"]) {
        NSDictionary *dic1 = @{@"name":@"医院资讯",@"image":@"img_home_medical_icon1"};;
        NSDictionary *dic2 = @{@"name":@"健康讲座",@"image":@"img_home_medical_icon6"};
        NSDictionary *dic3 = @{@"name":@"太原有线",@"image":@"img_home_medical_icon3"};
        NSDictionary *dic4 = @{@"name":@"看电影",@"image":@"img_home_medical_icon4"};
        NSDictionary *dic5 = @{@"name":@"就诊导航",@"image":@"img_home_medical_icon5"};
        NSDictionary *dic6 = @{@"name":@"出诊专家",@"image":@"img_home_medical_icon2"};
        NSDictionary *dic7 = @{@"name":@"知识库",@"image":@"img_home_medical_icon7"};
        [self.dataArray addObject:dic1];
        //    [self.dataArray addObject:dic2];
        [self.dataArray addObject:dic6];//新调整
        [self.dataArray addObject:dic3];
        if (isKnowledgeBase) {
            [self.dataArray addObject:dic7];//新调整
        }
        [self.dataArray addObject:dic4];
        [self.dataArray addObject:dic5];
        [self.dataArray addObject:dic2];//新调整
   
    }
    else
    {
        
        NSDictionary *dic1 = @{@"name":@"医院特色",@"image":@"img_home_medical_icon1"};;
        NSDictionary *dic2 = @{@"name":@"健康讲座",@"image":@"img_home_medical_icon6"};
        NSDictionary *dic3 = @{@"name":@"太原有线",@"image":@"img_home_medical_icon3"};
        NSDictionary *dic4 = @{@"name":@"看电影",@"image":@"img_home_medical_icon4"};
        NSDictionary *dic5 = @{@"name":@"就诊助手",@"image":@"img_home_medical_icon5"};
        NSDictionary *dic6 = @{@"name":@"出诊专家",@"image":@"img_home_medical_icon2"};;
        NSDictionary *dic7 = @{@"name":@"知识库",@"image":@"img_home_medical_icon7"};

        [self.dataArray addObject:dic1];
        //    [self.dataArray addObject:dic2];
        [self.dataArray addObject:dic6];//新调整
        [self.dataArray addObject:dic3];
        if (isKnowledgeBase) {
            [self.dataArray addObject:dic7];//新调整
        }
        [self.dataArray addObject:dic4];
        [self.dataArray addObject:dic2];//新调整
        [self.dataArray addObject:dic5];
    }

}

- (void)reqestHome{
    [self showCustomeHUD];
    [self reqestHomeDate];
    [self reqestRecord];
    
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//    if (regionCode.length ) {
//        regionCode = REGION_CODE;
//    }
    
    NSString *microH = kMicroHospital;
    NSString *homeJIUZHEM = Home_JIUZHENZHUSHOU;
    NSString *homeCHUZHEM = Home_ZHUANJIACHUZHEN;
    if (microH.length || homeCHUZHEM.length || homeJIUZHEM.length) {
        return;
    }
    
    //如果没有regionCode，不用取regioninfo
    if (regionCode.length != 0) {
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

        [request EPGGetRegionInfoWithRegionCode:regionCode andTimeout:5];
    }

    
}
- (void)reqestHomeDate{
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst CNCommonAppConfig];//请求产品配置
    
    [self reqestRecord];
    

    if (is_Separate_API) {
        [requst CNCommonSlideshowList];

    }else{
        [requst HomeADSLiderPagess];
    }
    
    
}
- (void)reqestRecord{
    
    [self setHighForheadIsOrNo:YES];
    
}

#pragma mark - 代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [Tool numberOfCellRowsWithArray:self.dataArray withRowNum:2];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    if (!cell) {
        cell = [[NewHomeTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"identifier"];
        if (indexPath.row == (self.dataArray.count/2) - 1) {
            cell.separatorInset = UIEdgeInsetsMake(0, 2 * cell.bounds.size.width, 0, 0);
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *arr = [Tool arrayFOR_ALLArray:self.dataArray withBeginnumb:indexPath.row withRowNum:2];
    cell.dateArray.array = arr;
    [cell update];
    cell.block = ^(UIButton_Block*btn){
        NSLog(@"123  btn%@",btn.Diction);
        [self didSelectWith:btn.Diction];
    };
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 83*kRateSize;
}


- (void)didSelectWith:(NSDictionary*)dic{
    NSString *string = [dic objectForKey:@"image"];
    NSString *title = [dic objectForKey:@"name"];
    if ([string isEqualToString:@"img_home_medical_icon1"]) {//医院资讯
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiYuan;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
//            HospitalViewController *hospi = [[HospitalViewController alloc]init];
//            hospi.HospitalType = HospitalTypeYiYuan;
//            hospi.YiYuanTitle = title;
//            [self.navigationController pushViewController:hospi animated:YES];
            
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
            know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&ch=3",Home_Hospital_News,KREGION_CODE,KInstanceCode,App_Theme_Color_num];
            [self.navigationController pushViewController:know animated:YES];
        }
    }
    else if ([string isEqualToString:@"img_home_medical_icon6"]){//健康讲座
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiVideo;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
            HospitalViewController *hospi = [[HospitalViewController alloc]init];
            hospi.HospitalType = HospitalTypeYiVideo;
            hospi.YiYuanTitle = title;
            [self.navigationController pushViewController:hospi animated:YES];
            
        }
        
    }
    else if ([string isEqualToString:@"img_home_medical_icon3"]){//太原有线
        
        if (IS_PUBLIC_ENV) {
            ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
            extranetVC.ExtranetPromptTitle = title;
            [self.navigationController pushViewController:extranetVC animated:YES];
        }else{
            LiveHomeViewController *LIVE = [[LiveHomeViewController alloc]init];
            [self.navigationController pushViewController:LIVE animated:YES];
        }
        
    }
    else if ([string isEqualToString:@"img_home_medical_icon4"]){//看电影
        if (IS_PUBLIC_ENV) {
            ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
            extranetVC.ExtranetPromptTitle = title;
            [self.navigationController pushViewController:extranetVC animated:YES];
            return;
        }else{
            ScrollDemandPageViewController *VOD = [[ScrollDemandPageViewController alloc]init];
            [self.navigationController pushViewController:VOD animated:YES];
        }

        
    }
    else if ([string isEqualToString:@"img_home_medical_icon5"]){//就诊导航
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiZhuShou;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
//            HospitalViewController *hospi = [[HospitalViewController alloc]init];
//            hospi.HospitalType = HospitalTypeYiZhuShou;
//            hospi.YiYuanTitle = title;
//            [self.navigationController pushViewController:hospi animated:YES];
            
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
            know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&ch=3",Home_Doctors_Guide,KREGION_CODE,KInstanceCode,App_Theme_Color_num];
            [self.navigationController pushViewController:know animated:YES];
        }
        
        //        HomeWebViewViewController *homeWeb = [[HomeWebViewViewController alloc]init];
        //        homeWeb.TitleName = @"就诊助手";
        //        homeWeb.urlLink = Home_JIUZHENZHUSHOU;
        //        if (homeWeb.urlLink.length>3) {
        //            [self.navigationController pushViewController:homeWeb animated:YES];
        //        }else{
        //            [AlertLabel AlertInfoWithText:@"敬请期待！" andWith:self.view withLocationTag:0];
        //        }
        
    }
    else if ([string isEqualToString:@"img_home_medical_icon2"]){//出诊专家
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiZhuanJia;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
//            HospitalViewController *hospi = [[HospitalViewController alloc]init];
//            hospi.HospitalType = HospitalTypeYiZhuanJia;
//            hospi.YiYuanTitle = title;
//            [self.navigationController pushViewController:hospi animated:YES];
            
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
            know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&ch=3",Home_Doctors_Order,KREGION_CODE,KInstanceCode,App_Theme_Color_num];
            [self.navigationController pushViewController:know animated:YES];
        }
        
        //        HomeWebViewViewController *homeWeb = [[HomeWebViewViewController alloc]init];
        //        homeWeb.TitleName = @"专家出诊";
        //        homeWeb.urlLink = Home_ZHUANJIACHUZHEN;
        //        if (homeWeb.urlLink.length>3) {
        //            [self.navigationController pushViewController:homeWeb animated:YES];
        //        }else{
        //            [AlertLabel AlertInfoWithText:@"敬请期待！" andWith:self.view withLocationTag:0];
        //        }
        
    }
    else if ([string isEqualToString:@"img_home_medical_icon7"]){//知识库
        
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [NSString stringWithFormat:@"%@/shphone/pages/kt/?user=%@&uuid=%@&tstyle=%@&ch=3",YJ_Shphone,K_USER,dev_ID,App_Theme_Color_num];
//        know.urlStr = @"http://192.168.3.49:8020/shphone01/pages/kt/index.html#/";

        [self.navigationController pushViewController:know animated:YES];
        
//        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//        if (code.length == 0) {
//            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
//            demandChoss.HospitalType = HospitalTypeYiZhuanJia;
//            demandChoss.YiYuanTitle = title;
//            [self.navigationController pushViewController:demandChoss animated:YES];
//            
//        }else{
//            HospitalViewController *hospi = [[HospitalViewController alloc]init];
//            hospi.HospitalType = HospitalTypeYiZhuanJia;
//            hospi.YiYuanTitle = title;
//            [self.navigationController pushViewController:hospi animated:YES];
//        }
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    if ([type isEqualToString:@"topSearchWords"]) {
        NSDictionary *dic = result;
        NSString *count = [dic objectForKey:@"count"];
        
        if ([count integerValue] == -1 ) {
            NSLog(@"暂时搜索热词记录");
            _seachTextView.text = @"";
        }else if ([count integerValue] == 1 )
        {
            NSArray *array = [dic objectForKey:@"keyList"];
            if (array.count) {
                NSDictionary *record = array[0];
                _VODdictionDic = [[NSDictionary alloc]initWithDictionary:record];
                NSString *name = [record objectForKey:@"name"];
                NSLog(@"名字是：： %@",name);
                if (kDeviceWidth == 320) {
                    
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                    
                }else if (kDeviceWidth == 375)
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
                else
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
            }
        }
    }
    
    if ([type isEqualToString:HomeADSLiderPage] || [type isEqualToString:CN_COMMON_SLIDESHOWLIST]) {
        NSMutableArray *array = [result objectForKey:@"slideshowList"];
        NSLog(@"广告的数据是%@",array);
        [self hiddenCustomHUD];
        if (array.count>1) {
            array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
            NSDictionary *firstDic = [array firstObject];
            [array removeObjectAtIndex:0];
            [array addObject:firstDic];
        }
            [self.posterView updateThePosterData:array];
    }
    
    if ([type isEqualToString:EPG_REGION_INFO]) {
        
        NSLog(@"EPG_REGION_INFO－－－－%@",result);
        [self loadingHospitalURLWithDic:result];
        
    }
    
    if ([type isEqualToString:YJ_INTRODUCE_LIST]) {
        NSLog(@"YJ_INTRODUCE_LIST返回数据: %ld result:%@  type:%@",status,result,type );
        NSDictionary *resDIC = result;
//        NSLog(@"%@",resDIC);
        NSArray *resArray = [resDIC objectForKey:@"list"];
        NSLog(@"resArray---%@",resArray);
        
        if (resArray.count<=0) {
            
            return;
        }
        
        NSDictionary *tempDIC = [resArray objectAtIndex:0];
        
        NSLog(@"tempDIC----%@",tempDIC);
        NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        
        if (regionCode.length == 0) {//如果有regioncode，就不进行存储
        
        NSString *code = [tempDIC objectForKey:@"instanceCode"];
        NSString *name = [tempDIC objectForKey:@"name"];
        [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"InstanceCode"];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"hospitalName"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self reqestHome];
        
        }
        
        if (resArray.count > 1) {//如果医院列表请求只有一家医院，则不显示更换医院按钮
            _moreHospitalBtn.hidden = NO;
            _moreHospitalBtn.userInteractionEnabled = YES;
        }
        
        [self setHighForheadIsOrNo:YES];//更新所到医院展示框
    }
}

- (void)setSeachBar
{
    
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(14*kRateSize, 25, 34, 34)];
    logo.image = [UIImage imageNamed:@"ic_launcher"];
    [self.m_viewNaviBar addSubview:logo];

    _historyBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_recoder" imgHighlight:@"btn_home_recoder" imgSelected:@"btn_home_recoder" target:self action:@selector(gotoRecorderView)];
    [self setNaviBarRRightBtn:_historyBtn];
    

    _wifiBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_area_wifi" imgHighlight:@"btn_home_area_wifi" imgSelected:@"btn_home_area_wifi_close" target:self action:@selector(openWifiInformation)];
    [self setNaviBarRightBtn:_wifiBtn];
}

- (void)gotoRecorderView
{
    
    _wifiView.hidden = YES;
    _wifiBtn.selected = NO;

    if (IsLogin) {
        
        if (IS_PUBLIC_ENV) {
            ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
            extranet.ExtranetPromptTitle = @"播放记录";
            [self.navigationController pushViewController:extranet animated:YES];
            
            return;
        }
        MineRecorderViewController *vc = [[MineRecorderViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [AlertLabel AlertInfoWithText:@"请先登录！" andWith:self.view withLocationTag:0];
    }
    
}

- (void)openWifiInformation
{
    NSLog(@"展示现在wifi信息");
    _wifiView.hidden = !_wifiView.hidden;

    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
        if ([netname isEqualToString:@"当前无可用网络"]) {
            [_wifiView resetNetWorkingStatusWith:0 andNetWorkingName:netname];

        }else if([netname isEqualToString:@"您正在使用移动网络"]){
            [_wifiView resetNetWorkingStatusWith:1 andNetWorkingName:netname];

        }else{
            [_wifiView resetNetWorkingStatusWith:2 andNetWorkingName:netname];

        }
        
    }];
}


#pragma  mark - 广告点击
- (void)AdClicked:(NSNotification*)notif
{
    NSLog(@"%@",notif);
    if (0) {
        NSDictionary *object = notif.object;
        NSString *name = [object objectForKey:@"name"];
        NSString *contentID = [object objectForKey:@"contentID"];
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = contentID;
        demandVC.contentTitleName = name;
        
        
        [self.navigationController pushViewController:demandVC animated:YES];
    }else
    {
        NSDictionary *object = notif.object;
        
        NSString *type = [object objectForKey:@"type"];
        [self homeADJumpWithType:type andDiction:object];
    }
}

- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                NSString *name = [dic objectForKey:@"name"];
                NSString *contentID = [dic objectForKey:@"id"];
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = contentID;
                demandVC.contentTitleName = name;
                [self.navigationController pushViewController:demandVC animated:YES];
            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
            
        }
            
            break;
            
        case 5:
        {
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            
            [self.navigationController pushViewController:epg animated:YES];
            
        }
            
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = [dic objectForKey:@"urlLink"];
                [self presentViewController:demandDetail animated:YES completion:nil];
            }
            
            
            
        }
            
            break;
        default:
        {
            
        }
            break;
    }
    
}


-(void)loadingHospitalURLWithDic:(NSDictionary *)hospitalDic{
    NSMutableString *allUrlLink = [NSMutableString stringWithFormat:@"%@",[[hospitalDic objectForKey:@"region"] objectForKey:@"urlLink"]];
    
    NSArray *urlArr = [[NSArray alloc] initWithArray:[allUrlLink componentsSeparatedByString:@"|"]];
    NSString *microHospital = @"";
    NSString *chuzhenStr = @"";
    NSString *jiuzhenStr = @"";
    
    for (int i=0; i<urlArr.count; i++) {
        switch (i) {
            case 0:
            {
                
                microHospital = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                break;
            }
            case 1:
            {
                chuzhenStr = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                
                break;
            }
            case 2:
            {
                jiuzhenStr = [KaelTool adjustUIWebViewURLWith:[urlArr objectAtIndex:i]];
                
                break;
            }
            default:
                break;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:microHospital forKey:@"microHospital"];
    [[NSUserDefaults standardUserDefaults] setObject:chuzhenStr forKey:@"chuzhenStr"];
    [[NSUserDefaults standardUserDefaults] setObject:jiuzhenStr forKey:@"jiuzhenStr"];
    
}


- (void)setHighForheadIsOrNo:(BOOL)havePlayRecorder
{


    NSString *hospitalName = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
    if (hospitalName.length == 0) {
        NSString *tempStr = kAPP_NAME;
        self.XUKanLabel.text = [NSString stringWithFormat:@"欢迎使用%@",tempStr];
    }
    else
    {
        self.XUKanLabel.text = [NSString stringWithFormat:@"您已来到%@",hospitalName];
    }

}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

