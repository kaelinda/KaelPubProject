//
//  HomeWebViewViewController.h
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/25.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface HomeWebViewViewController : HT_FatherViewController
@property (nonatomic,copy)NSString *TitleName;
@property (nonatomic,copy)NSString *urlLink;
@property (nonatomic,copy)NSString *Ktitle;
@end
