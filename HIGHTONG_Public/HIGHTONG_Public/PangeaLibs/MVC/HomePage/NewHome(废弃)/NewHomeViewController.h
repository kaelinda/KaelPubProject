//
//  NewHomeViewController.h
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HT_FatherViewController.h"
#import "PosterViewofTB.h"
@interface NewHomeViewController : HT_FatherViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) PosterViewofTB *  posterView;           //海报view

@end


