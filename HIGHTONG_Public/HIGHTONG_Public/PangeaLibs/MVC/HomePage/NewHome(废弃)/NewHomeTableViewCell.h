//
//  NewHomeTableViewCell.h
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "NewHomeView.h"
#import "UIButton_Block.h"
@interface NewHomeTableViewCell : UITableViewCell
typedef void (^VODShowTableViewCellBlock) (UIButton_Block*);

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)VODShowTableViewCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)NewHomeView *leftView;//左边

@property (nonatomic,strong)NewHomeView *rightView;//右边

//@property (assign,nonatomic)CGFloat interval;//间隙默认

- (void)update;//跟新数据
@end