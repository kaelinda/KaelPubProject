//
//  YJHomeViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HT_FatherViewController.h"
#import "PosterViewofTB.h"
#import "NetWorkNotification_shared.h"

@interface YJHomeViewController : HT_FatherViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) PosterViewofTB *  posterView;           //海报view

@end
