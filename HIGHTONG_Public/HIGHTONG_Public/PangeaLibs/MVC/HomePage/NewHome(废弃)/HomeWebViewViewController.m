//
//  HomeWebViewViewController.m
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/25.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HomeWebViewViewController.h"

@interface HomeWebViewViewController ()<UIWebViewDelegate>
{
    
    MBProgressHUD *_HUD;
    
}
@property (nonatomic,strong)UIButton *backBtn;

@end

@implementation HomeWebViewViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    [MobCountTool pushInWithPageView:@"HomeWebPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"HomeWebPage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.urlLink.length<1 && _Ktitle.length==0) {

         [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:1];
    }else{
        
        
        WS(wss);
        UIWebView *webView = [UIWebView new];
        [self.view addSubview:webView];
        [self.view setBackgroundColor:App_selected_color];//Kael_change
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(20);//Kael_change
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];
        
        webView.scalesPageToFit = YES;
        
        [(UIScrollView *)[[webView subviews] objectAtIndex:0] setBounces:NO];
        
        webView.delegate = self;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlLink]];
        
        [webView loadRequest:request];
   
        [self showCustomeHUD];
        
            [self setNaviBarTitle:self.TitleName];
            [webView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(wss.m_viewNaviBar.mas_bottom).offset(-20);
                make.width.equalTo(wss.view.mas_width);
                make.centerX.equalTo(wss.view.mas_centerX);
                make.bottom.equalTo(wss.view.mas_bottom);
            }];
            _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBackVC)];
            [self setNaviBarLeftBtn:_backBtn];
            [self setNaviBarRightBtn:nil];
        }

}

- (void)goBackVC
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"加载完毕");
    //    if (_HUD) {
    //        [_HUD hide:YES];
    //    }
    [self hiddenCustomHUD];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSURL * url = [request URL];
    if ([[url scheme] isEqualToString:@"firstclick"]) {
        NSArray *params =[url.query componentsSeparatedByString:@"&"];
        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
        for (NSString *paramStr in params) {
            NSArray *dicArray = [paramStr componentsSeparatedByString:@"="];
            if (dicArray.count > 1) {
                NSString *decodeValue = [dicArray[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; [tempDic setObject:decodeValue forKey:dicArray[0]];
            }
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"方式一" message:@"这是OC原生的弹出窗" delegate:self cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alertView show];
        NSLog(@"tempDic:%@",tempDic); return NO;
    }
    return YES;
}

@end
