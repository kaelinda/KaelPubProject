//
//  JktvHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/6/20.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "JktvHomeViewController.h"
#import "UIButton_Block.h"
#import "PosterViewofTB.h"
#import "SearchViewController.h"
#import "NewHomeTableViewCell.h"
#import "Tool.h"
#import "HospitalViewController.h"
#import "LiveHomeViewController.h"
#import "ScrollDemandPageViewController.h"
#import "MineRecorderViewController.h"
#import "DemandVideoPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandChooseHospitalControl.h"
#import "MineRecorderViewController.h"
#import "WIFIAreaSelectView.h"
#import "KnowledgeBaseViewController.h"
#import "ExtranetPromptViewController.h"
#import "KaelTool.h"
#import "HealthVodViewController.h"
#import "SystermTimeControl.h"
#import "JKTVHomeCollectionModel.h"
#import "SAIInformationManager.h"
#define WeiHospital 40

#define K_USER   (IsLogin ? KMobileNum : [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"])

@interface JktvHomeViewController ()<HTRequestDelegate>
{
    
    UIView *_Headview; // tableview的头
    
    UILabel *_seachTextView;//搜索热词
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    UIImageView *_HospitalView;//欢迎医院
    
    NSDictionary *_VODdictionDic;//点播
    
    WIFIAreaSelectView *_wifiView;
    
    
}
@property (nonatomic, strong) UIButton *wifiBtn;
@property (nonatomic, strong) UIButton *historyBtn;
@property (strong ,nonatomic) UITableView    * tableView; //首页选项卡
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页选项卡接收数据数据
@property (strong,nonatomic) UILabel *HospitalLabel;         //医院欢迎条
@property (strong,nonatomic) UIButton_Block *moreHospitalBtn;  //更多医院按钮
@end


@implementation JktvHomeViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    if (regionCode.length != 0) {
        
        [self reqestHome];
        
    }
    
    [self.view bringSubviewToFront:_wifiView];
    [_wifiView setHidden:YES];
    
//    [self show];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
//    
////    [request YJIntroduceListWithPageNo:@"0" withPageSize:@"2"];
//    
//    [request YJHealthyVodGetHotChannel];
//    
//    [request YJHealthyVodHotChannelProgramList];
    
    // Do any additional setup after loading the view.
    [self setNaviBarTitle:kAPP_NAME];
    [UIApplication sharedApplication].statusBarHidden = NO;
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];

    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestHome) name:@"loginSuccess" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestRecord) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestYJHomeData) name:@"RefreshHome" object:nil];
    
    [self setSeachBar];
    
    [self setUpHead];
        
    WS(wself);
    
    _wifiView = [[WIFIAreaSelectView alloc] init];
    _wifiView.hidden = YES;
    [[[UIApplication sharedApplication].delegate window] addSubview:_wifiView];
    __weak UIButton *wWIFIBtn = _wifiBtn;
    _wifiView.moreWIFIActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");
        
        wWIFIBtn.selected = NO;
        
        NSString *fullUrlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"moreWifiLink"];
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@",fullUrlStr,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        [wself.navigationController pushViewController:know animated:YES];
        
    };
    _wifiView.hiddenBlcok = ^(BOOL isHidden){
        
        wWIFIBtn.selected = !isHidden;
        
    };
    
    UIEdgeInsets wifiEdg = UIEdgeInsetsMake(64, 0, 0, 0);
    
    [_wifiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wifiEdg);
    }];
    
    
    
    
    
    
    
}

- (void)reRequestYJHomeData
{
//    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//    
//    if (regionCode.length == 0) {
    
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
    [request YJIntroduceListWithAreaId:@"" withPageNo:@"0" withPageSize:@"2"];
    
//        [request YJHealthyVodGetHotChannel];
//    
//        [request YJHealthyVodHotChannelProgramList];
    
//    }
    
    [self reqestHomeDate];
}



- (void)setUpHead{
    
    WS(wself);
    
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    [self.view addSubview:self.tableView];
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_background_color;
    [self.view addSubview:view];
    
    //去除自动适应scrollview
    //初始化tableView
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    image.frame = self.tableView.frame;
    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = App_background_color;
    
    self.tableView.separatorColor = UIColorFromRGB(0xeeeeee);
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
    self.tableView.tableFooterView = [[UIView alloc] init];
    //头View...
    
    
    _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+ WeiHospital - 3)*kRateSize)];
    
    _Headview.backgroundColor = UIColorFromRGB(0xffffff);
    //AD广告页
    self.posterView = [[PosterViewofTB alloc]init];
    [_Headview addSubview:self.posterView];
    [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_Headview.mas_top );
        make.left.equalTo(_Headview.mas_left);
        make.right.equalTo(_Headview.mas_right);
        make.height.equalTo(@((154 + kheigh)*kRateSize));
    }];
    self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
    self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
    
    NSArray *array = [NSArray array];
    
    [self.posterView updateThePosterData:array];
    
    {
        
        _HospitalView = [[UIImageView alloc]init];
        _HospitalView.backgroundColor = [UIColor whiteColor];
        
        
        [_Headview addSubview:_HospitalView];
        
        [_HospitalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(40*kRateSize));
        }];
        //        _RecordView.hidden = YES;
        _HospitalView.userInteractionEnabled =  YES;
        
        //节目Label
        self.HospitalLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 5*kRateSize, 200*kRateSize, 30*kRateSize)];
        
        self.HospitalLabel.userInteractionEnabled = YES;
        self.HospitalLabel.numberOfLines = 1;
        
        UIFont *font  =  [UIFont systemFontOfSize:16];
        self.HospitalLabel.font = font;
        self.HospitalLabel.tintColor = UIColorFromRGB(0x333333);

        [_HospitalView addSubview:self.HospitalLabel];
        
        //播放记录Button
        _moreHospitalBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
        _moreHospitalBtn.frame = CGRectMake(FRAME_W(self.view) - 105*kRateSize, 5*kRateSize, 80*kRateSize, 30*kRateSize);
        UIImageView* inImageView = [UIImageView new];
        [_moreHospitalBtn addSubview:inImageView];
        [inImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_moreHospitalBtn.mas_centerY);
            make.left.equalTo(_moreHospitalBtn.mas_right);
            make.width.equalTo(@(25*kRateSize));
            make.height.equalTo(@(40*kRateSize));
        }];
        _moreHospitalBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _moreHospitalBtn.hidden = YES;
        
        inImageView.image = [UIImage imageNamed:@"ic_arrow_right_blue"];//公版改进
        _moreHospitalBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_moreHospitalBtn setTitle:@"更换医院" forState:UIControlStateNormal];
        [_moreHospitalBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        _moreHospitalBtn.userInteractionEnabled = NO;
        _moreHospitalBtn.serviceIDName = @"selina";
        _moreHospitalBtn.backgroundColor = UIColorFromRGB(0xffffff);
        
        [_moreHospitalBtn setClick:^(UIButton_Block*btn,NSString* name){
            
            NSLog(@"%@ name:%@",[btn currentTitle],name);
            
            DemandChooseHospitalControl *vc = [[DemandChooseHospitalControl alloc]init];
            
            vc.YiYuanTitle = @"更换医院";
            
            [wself.navigationController pushViewController:vc animated:YES];
            
        }];
   
        [_HospitalView addSubview:_moreHospitalBtn];
        
        UIView *recordVerLine = [UIView new];
        recordVerLine.backgroundColor = UIColorFromRGB(0xeeeeee);
        [_HospitalView addSubview:recordVerLine];
        [recordVerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_HospitalView.mas_left);
            make.right.equalTo(_HospitalView.mas_right);
            make.height.equalTo(@(1));
            make.bottom.equalTo(_HospitalView.mas_bottom).offset(-1);
            
        }];
        
        self.tableView.tableHeaderView = _Headview;
        
        //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
        self.tableView.delaysContentTouches = YES;
        
    }

}

- (void)reqestHome{
    
//    [self showCustomeHUD];
    
    
    [self reqestHomeDate];
    
    [self reqestRecord];
    
}
- (void)reqestHomeDate{
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    [requst CNCommonAppConfig];//请求产品配置
    
    [requst CNCommonModuleConfig];
    
    if (is_Separate_API) {
        [requst CNCommonSlideshowList];
        
    }else{
        [requst HomeADSLiderPagess];
    }
    
}
- (void)reqestRecord{
    
    [self setHighForheadIsOrNo:YES];
    
}

#pragma mark - 代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [Tool numberOfCellRowsWithArray:self.dataArray withRowNum:2];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    if (!cell) {
        cell = [[NewHomeTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"identifier"];
        if (indexPath.row == (self.dataArray.count/2) - 1) {
            cell.separatorInset = UIEdgeInsetsMake(0, 2 * cell.bounds.size.width, 0, 0);
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *arr = [Tool arrayFOR_ALLArray:self.dataArray withBeginnumb:indexPath.row withRowNum:2];
    cell.dateArray.array = arr;
    [cell update];
    cell.block = ^(UIButton_Block*btn){
        NSLog(@"123  btn%@",btn.Diction);
        [self didSelectWith:btn.Diction];
    };
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 83*kRateSize;
}


- (void)didSelectWith:(NSDictionary*)dic{
    
    NSString *moduleType = [dic objectForKey:@"moduleType"];
    NSString *moduleInfo = [dic objectForKey:@"moduleInfo"];
    NSString *moduleName = [dic objectForKey:@"moduleName"];
    
    if ([moduleType isEqualToString:@"1"]) {
        
        KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
        know.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&u=%@&devType=%@&devID=%@",moduleInfo,KREGION_CODE,KInstanceCode,U_KEY,TERMINAL_TYPE,ICS_deviceID];
        
        NSString *systemTime = [SystermTimeControl getNowServiceTimeString];
        
//        NSLog(@"系统时间hahahahah－－－－－%@",systemTime);
        
        [dic setValue:systemTime forKey:@"systemTime"];
        
        JKTVHomeCollectionModel *homeCollection = [JKTVHomeCollectionModel homeWithDict:dic];
        
//        NSLog(@"%@,%@,%@",homeCollection.moduleName,homeCollection.systemTime,homeCollection.seq);
        
        
        if (USER_ACTION_COLLECTION) {
            [SAIInformationManager JKTVHomeInformation:homeCollection];
        }
        
        

        [self.navigationController pushViewController:know animated:YES];

    }
    else
    {
        if ([moduleInfo isEqualToString:@"tv"]) {
            
            if (!IS_EPG_CONFIGURED) {
                ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                extranetVC.ExtranetPromptTitle = moduleName;
                [self.navigationController pushViewController:extranetVC animated:YES];
            }else{
                
                LiveHomeViewController *LIVE = [[LiveHomeViewController alloc]init];
                [self.navigationController pushViewController:LIVE animated:YES];
            }
            
        }
        else if([moduleInfo isEqualToString:@"vod"])
        {
            
            if (!IS_EPG_CONFIGURED) {
                ExtranetPromptViewController *extranetVC = [[ExtranetPromptViewController alloc] init];
                extranetVC.ExtranetPromptTitle = moduleName;
                [self.navigationController pushViewController:extranetVC animated:YES];
                return;
            }else{
                ScrollDemandPageViewController *VOD = [[ScrollDemandPageViewController alloc]init];
                [self.navigationController pushViewController:VOD animated:YES];
            }
            
        }
        else if([moduleInfo isEqualToString:@"healthvod"])
        {
            
//            if (isUsePolymedicine_2_0) {
//                HealthVodViewController *healthVod = [[HealthVodViewController alloc]init];
//                healthVod.HealthVodViewTitle = moduleName;
//                [self.navigationController pushViewController:healthVod animated:YES];
//            }
//            else
//            {
                HospitalViewController *hospi = [[HospitalViewController alloc]init];
                hospi.HospitalType = HospitalTypeYiVideo;
                hospi.YiYuanTitle = moduleName;
                [self.navigationController pushViewController:hospi animated:YES];
//            }

            
        }
        else if([moduleInfo isEqualToString:@"healthvod2"])
        {
            
//            if (isUsePolymedicine_2_0) {
                HealthVodViewController *healthVod = [[HealthVodViewController alloc]init];
                healthVod.HealthVodViewTitle = moduleName;
                [self.navigationController pushViewController:healthVod animated:YES];
//            }
//            else
//            {
//                HospitalViewController *hospi = [[HospitalViewController alloc]init];
//                hospi.HospitalType = HospitalTypeYiVideo;
//                hospi.YiYuanTitle = moduleName;
//                [self.navigationController pushViewController:hospi animated:YES];
//            }
    
        }
    }

}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    if ([type isEqualToString:HomeADSLiderPage] ||[type isEqualToString:CN_COMMON_SLIDESHOWLIST]) {
        NSMutableArray *array = [result objectForKey:@"slideshowList"];
        NSLog(@"广告的数据是%@",array);
        [self hiddenCustomHUD];
        if (array.count>1) {
            array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
            NSDictionary *firstDic = [array firstObject];
            [array removeObjectAtIndex:0];
            [array addObject:firstDic];
        }
        [self.posterView updateThePosterData:array];
    }
    
    if ([type isEqualToString:YJ_INTRODUCE_LIST]) {
        NSLog(@"YJ_INTRODUCE_LIST返回数据: %ld result:%@  type:%@",status,result,type );
        NSDictionary *resDIC = result;
        //        NSLog(@"%@",resDIC);
        NSArray *resArray = [resDIC objectForKey:@"list"];
        NSLog(@"resArray---%@",resArray);
        
        if (resArray.count<=0) {
            
            [self reqestHome];
            
            return;
        }
        
        NSDictionary *tempDIC = [resArray objectAtIndex:0];
        
        NSLog(@"tempDIC----%@",tempDIC);
        NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        
        if (regionCode.length == 0) {//如果有regioncode，就不进行存储
            
            NSString *code = [tempDIC objectForKey:@"instanceCode"];
            NSString *name = [tempDIC objectForKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"InstanceCode"];
            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"hospitalName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self reqestHome];
            
        }
        
        if (resArray.count > 1) {//如果医院列表请求只有一家医院，则不显示更换医院按钮
            _moreHospitalBtn.hidden = NO;
            _moreHospitalBtn.userInteractionEnabled = YES;
        }
        
        [self setHighForheadIsOrNo:YES];//更新所到医院展示框
    }
    
    if ([type isEqualToString:CN_COMMON_MODULECONFIG]) {
       NSLog(@"CN_COMMON_MODULECONFIG返回数据: %ld result:%@  type:%@",status,result,type );
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] ) {
            
            self.dataArray = [NSMutableArray array];
            NSMutableArray *tempArr = [NSMutableArray array];
            tempArr = [result objectForKey:@"moduleList"];
            
            NSLog(@"tempArr返回数据:%@",tempArr);
            
            
            self.dataArray = [KaelTool increaseArrayWithArray:tempArr andKey:@"seq"];

            NSLog(@"self.dataArray返回数据:%@",self.dataArray);
            
            [self.tableView reloadData];
            
            for (NSDictionary *dic in self.dataArray) {
                if ([[dic objectForKey:@"moduleInfo"]isEqualToString:@"healthvod"]) {
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"HEALTHVOD"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else if ([[dic objectForKey:@"moduleInfo"]isEqualToString:@"healthvod2"])
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HEALTHVOD"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            
//            [self hide];
        }
    }

}

- (void)setSeachBar
{
    
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(14*kRateSize, 25, 34, 34)];
    logo.image = [UIImage imageNamed:@"ic_launcher"];
    [self.m_viewNaviBar addSubview:logo];
    
    _historyBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_recoder" imgHighlight:@"btn_home_recoder" imgSelected:@"btn_home_recoder" target:self action:@selector(gotoRecorderView)];
    [self setNaviBarRRightBtn:_historyBtn];
    
    _wifiBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_home_area_wifi" imgHighlight:@"btn_home_area_wifi" imgSelected:@"btn_home_area_wifi_close" target:self action:@selector(openWifiInformation)];
    [self setNaviBarRightBtn:_wifiBtn];
}

- (void)gotoRecorderView
{
    
    _wifiView.hidden = YES;
    _wifiBtn.selected = NO;
    
    if (IsLogin) {
        
        if (!IS_EPG_CONFIGURED) {
            ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
            extranet.ExtranetPromptTitle = @"播放记录";
            [self.navigationController pushViewController:extranet animated:YES];
            
            return;
        }
        MineRecorderViewController *vc = [[MineRecorderViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [AlertLabel AlertInfoWithText:@"请先登录！" andWith:self.view withLocationTag:0];
    }
    
}

- (void)openWifiInformation
{
    NSLog(@"展示现在wifi信息");
    _wifiView.hidden = !_wifiView.hidden;
    
    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
        if ([netname isEqualToString:@"当前无可用网络"]) {
            [_wifiView resetNetWorkingStatusWith:0 andNetWorkingName:netname];
            
        }else if([netname isEqualToString:@"您正在使用移动网络"]){
            [_wifiView resetNetWorkingStatusWith:1 andNetWorkingName:netname];
            
        }else{
            [_wifiView resetNetWorkingStatusWith:2 andNetWorkingName:netname];
            
        }
        
    }];
}


#pragma  mark - 广告点击
- (void)AdClicked:(NSNotification*)notif
{
    NSLog(@"%@",notif);
    if (0) {
//        NSDictionary *object = notif.object;
//        NSString *name = [object objectForKey:@"name"];
//        NSString *contentID = [object objectForKey:@"contentID"];
//        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
//        demandVC.contentID = contentID;
//        demandVC.contentTitleName = name;
//        
//        
//        [self.navigationController pushViewController:demandVC animated:YES];
    }else
    {
        NSDictionary *object = notif.object;
        
        NSString *type = [object objectForKey:@"type"];
        [self homeADJumpWithType:type andDiction:object];
    }
}

- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                NSString *name = [dic objectForKey:@"name"];
                NSString *contentID = [dic objectForKey:@"id"];
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = contentID;
                demandVC.contentTitleName = name;
                [self.navigationController pushViewController:demandVC animated:YES];
            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
            
        }
            
            break;
            
        case 5:
        {
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            [self.navigationController pushViewController:epg animated:YES];
            
        }
            
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = url;
                [self presentViewController:demandDetail animated:YES completion:nil];
            }
        }
            
            break;
        default:
        {
            
        }
            break;
    }
    
}

- (void)setHighForheadIsOrNo:(BOOL)havePlayRecorder
{
    
    NSString *hospitalName = [[NSUserDefaults standardUserDefaults] objectForKey:@"hospitalName"];
    if (hospitalName.length == 0) {
        NSString *tempStr = kAPP_NAME;
        self.HospitalLabel.text = [NSString stringWithFormat:@"欢迎使用%@",tempStr];
    }
    else
    {
        self.HospitalLabel.text = [NSString stringWithFormat:@"您已来到%@",hospitalName];
    }
    
}

/**
 *  显示loading框
 */
- (void)show{
    
    [self showCustomeHUD];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}
/**
 *  隐藏loading框
 */
- (void)hide{

    //    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self hiddenCustomHUD];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
