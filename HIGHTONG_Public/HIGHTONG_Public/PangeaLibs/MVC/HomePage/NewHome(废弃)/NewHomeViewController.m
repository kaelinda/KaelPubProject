//
//  NewHomeViewController.m
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewHomeViewController.h"
#import "UIButton_Block.h"
#import "PosterViewofTB.h"
#import "SearchViewController.h"
#import "NewHomeTableViewCell.h"
#import "NewHomeView.h"
#import "Tool.h"
#import "HospitalViewController.h"
#import "LiveHomeViewController.h"
#import "ScrollDemandPageViewController.h"
#import "HomeWebViewViewController.h"
#import "MineRecorderViewController.h"
#import "DemandVideoPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandChooseHospitalControl.h"
#define WeiHospital 40

@interface NewHomeViewController ()<HTRequestDelegate>
{
    
    UIView *_Headview; // tableview的头
    
    UILabel *_seachTextView;//搜索热词
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    UIImageView *_RecordView;//播放记录
    
    NSString *_continueToReadDate;//续看时间
    
    BOOL _haveRecord;//有播放记录
    
    NSString *_channelID;
    NSString *_endDate;
    
    NSInteger _requestCount;//请求失败次数
    
    NSDictionary *_VODdictionDic;//点播
    NSDictionary *_EPGdictionDic;//点播
    
    
}
@property (strong ,nonatomic) UITableView    * tableView; //首页热门节目列表
@property (strong,nonatomic) NSMutableArray *dataArray;   //首页直播推荐  影视精选 等 数据
@property (strong,nonatomic) UILabel *XUKanLabel;         //续看标签
@property (strong,nonatomic) UIButton_Block *XUKanBtn;         //续看按钮
@end




@implementation NewHomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reqestRecord];
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    if(_IS_SUPPORT_VOD)
    {
        //*****5请求搜索热门字第一天
        [requst VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    }else{
        [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }
    
    [self reqestRecord];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNaviBarTitle:@"首页"];
    [UIApplication sharedApplication].statusBarHidden = NO;
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    _continueToReadDate = @"1997-12-01 00:00:00";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestHome) name:@"loginSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqestRecord) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdClicked:) name:@"GOTODETAILOFVOD" object:nil];
    
    [self setSeachBar];
    
    [self setUpHead];
    
    [self reqestHomeDate];
    
    
}
- (void)setUpHead{
    
    WS(wSelf);
    
    //******竟然是这种奇葩的方式啊，tableview的偏移就没有问题了
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    [self.view addSubview:self.tableView];
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = App_background_color;
    [self.view addSubview:view];
    
    //去除自动适应scrollview
    //初始化tableView
    //    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height -64-44-5) style:UITableViewStylePlain];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    image.frame = self.tableView.frame;
    [self.view addSubview:image];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = App_background_color;
    
    //    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.separatorColor = UIColorFromRGB(0xeeeeee);
    
    
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);;
    if (IsLogin) {
        //头View...
        
        _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital*0)*kRateSize)];
        _Headview.backgroundColor = UIColorFromRGB(0xffffff);
        //AD广告页
        self.posterView = [[PosterViewofTB alloc]init];
        [_Headview addSubview:self.posterView];
        [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_Headview.mas_top );
            make.left.equalTo(_Headview.mas_left);
            make.right.equalTo(_Headview.mas_right);
            make.height.equalTo(@((154+kheigh)*kRateSize));
        }];
        self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
        self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
        
        
    }else
    {
        _Headview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital*0)*kRateSize)];
        //        _Headview.backgroundColor = [UIColor redColor];
        _Headview.backgroundColor = UIColorFromRGB(0xffffff);
        //AD广告页
        self.posterView = [[PosterViewofTB alloc]init];
        [_Headview addSubview:self.posterView];
        [self.posterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_Headview.mas_top );
            make.left.equalTo(_Headview.mas_left);
            make.right.equalTo(_Headview.mas_right);
            make.height.equalTo(@((154+kheigh)*kRateSize));
        }];
        self.posterView.posterPageControl.currentPageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"];
        self.posterView.posterPageControl.pageIndicatorImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
        
    }
    
    
    {
        
        _RecordView = [[UIImageView alloc]init];
        _RecordView.backgroundColor = [UIColor whiteColor];
        
        
        [_Headview addSubview:_RecordView];
        
        [_RecordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(35*kRateSize));
        }];
        _RecordView.hidden = YES;
        _RecordView.userInteractionEnabled =  YES;
        
        //节目Label
        self.XUKanLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5*kRateSize, 200*kRateSize, 30*kRateSize)];
        
        self.XUKanBtn = [UIButton_Block new];
        [self.XUKanLabel addSubview:self.XUKanBtn];
        self.XUKanBtn.backgroundColor = UIColorFromRGB(0xffffff);
        
        [self.XUKanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.XUKanLabel);
        }];
        
        self.XUKanLabel.text = @"续看                     ";
        
        
        self.XUKanLabel.userInteractionEnabled = YES;
        self.XUKanLabel.adjustsFontSizeToFitWidth = YES;
        self.XUKanLabel.numberOfLines = 1;
        self.XUKanBtn.Click = ^(UIButton_Block*btn,NSString *name){
            NSLog(@"%@",btn.Diction);
            NSString *type = [btn.Diction objectForKey:@"TYPE"];
            if ([type isEqualToString:@"VOD"]) {
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                
                demandVC.contentID = [btn.Diction objectForKey:@"contentID"];
                demandVC.contentTitleName = [btn.Diction objectForKey:@"name"];
                demandVC.episodeSeq = [btn.Diction objectForKey:@"lastPlayEpisode"];;
                demandVC.breakPointRecord = [btn.Diction objectForKey:@"breakPoint"];
                
                
                [wSelf.navigationController pushViewController:demandVC animated:YES];
            }
            
            if ([type isEqualToString:@"EPG"]) {
                NSString *eventID = [btn.Diction objectForKey:@"eventID"];
                _channelID = eventID;
                _endDate = [btn.Diction objectForKey:@"endTime"];
                HTRequest *_requst  =[[HTRequest alloc]initWithDelegate:wSelf];
                [_requst EventPlayWithEventID:eventID andTimeout:10];
            }
        };
       

        
        
        //            UIFont *font  =  [UIFont fontWithName:@"Arial" size:18];
        UIFont *font  =  [UIFont systemFontOfSize:14];
        self.XUKanLabel.font = font;

        [_RecordView addSubview:self.XUKanLabel];
        //播放记录Button
        UIButton_Block *btn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(FRAME_W(self.view) - 105*kRateSize, 5*kRateSize, 100*kRateSize, 30*kRateSize);
        UIImageView* inImageView = [UIImageView new];
        [btn addSubview:inImageView];
        [inImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.top.equalTo(btn.mas_top).offset(7*kRateSize);
            make.centerY.mas_equalTo(btn.mas_centerY);
            make.right.equalTo(btn.mas_right).offset(-9*kRateSize);
            //                make.bottom.equalTo(btn.mas_bottom).offset(-7*kRateSize);
            make.width.equalTo(@(6*kRateSize));
            make.height.equalTo(@(8*kRateSize));
        }];
        
        
        inImageView.image = [UIImage imageNamed:@"img_user_protocol_arrow"];//公版改进
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitle:@"播放记录" forState:UIControlStateNormal];
        //            [btn setTitleColor:[UIColor colorWithRed:0.63 green:0.63 blue:0.63 alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
        btn.serviceIDName = @"selina";
        //            [btn setBackgroundColor:[UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1]];
        btn.backgroundColor = UIColorFromRGB(0xffffff);
        
        [btn setClick:^(UIButton_Block*btn,NSString* name){
            
            NSLog(@"%@ name:%@",[btn currentTitle],name);
            
            MineRecorderViewController *mine = [[MineRecorderViewController alloc]init];
            
            //            if (!isEPGRecord) {
            //
            //                mine.fromHome = YES;
            //            }
            [self.navigationController pushViewController:mine animated:YES];
            
        }];
        
        [_RecordView addSubview:btn];
        
        UIView *recordVerLine = [UIView new];
        recordVerLine.backgroundColor = UIColorFromRGB(0xeeeeee);
        [_RecordView addSubview:recordVerLine];
        [recordVerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_RecordView.mas_left);
            make.right.equalTo(_RecordView.mas_right);
            make.height.equalTo(@(1));
            make.bottom.equalTo(_RecordView.mas_bottom).offset(-1);
            
        }];
        
        
        self.tableView.tableHeaderView = _Headview;
        
        //...tabelview 上的按钮会delay相应事件，需要关闭这个事件 cap
        self.tableView.delaysContentTouches = YES;
        
        //        self.XUKanLabel.backgroundColor = [UIColor redColor];
        //        self.XUKanBtn.backgroundColor = [UIColor redColor];
        
    }
    
    
    
    
    self.dataArray = [NSMutableArray array];

    //6个模块的图片名字和跳转title

    if ([APP_ID isEqualToString:@"1071241443"]) {
        NSDictionary *dic1 = @{@"name":@"山西省中西医结合医院",@"image":@"img_home_medical_icon1"};;
        NSDictionary *dic2 = @{@"name":@"健康讲座",@"image":@"img_home_medical_icon6"};
        NSDictionary *dic3 = @{@"name":@"太原有线",@"image":@"img_home_medical_icon3"};
        NSDictionary *dic4 = @{@"name":@"看电影",@"image":@"img_home_medical_icon4"};
        NSDictionary *dic5 = @{@"name":@"就诊助手",@"image":@"img_home_medical_icon5"};
        NSDictionary *dic6 = @{@"name":@"专家出诊",@"image":@"img_home_medical_icon2"};;
        [self.dataArray addObject:dic1];
        //    [self.dataArray addObject:dic2];
        [self.dataArray addObject:dic6];//新调整
        [self.dataArray addObject:dic3];
        [self.dataArray addObject:dic4];
        [self.dataArray addObject:dic5];
        [self.dataArray addObject:dic2];//新调整
    }
    else
    {

        NSDictionary *dic1 = @{@"name":@"医院特色",@"image":@"img_home_medical_icon1"};;
        NSDictionary *dic2 = @{@"name":@"健康讲座",@"image":@"img_home_medical_icon6"};
        NSDictionary *dic3 = @{@"name":@"太原有线",@"image":@"img_home_medical_icon3"};
        NSDictionary *dic4 = @{@"name":@"看电影",@"image":@"img_home_medical_icon4"};
        NSDictionary *dic5 = @{@"name":@"就诊助手",@"image":@"img_home_medical_icon5"};
        NSDictionary *dic6 = @{@"name":@"出诊专家",@"image":@"img_home_medical_icon2"};;
        [self.dataArray addObject:dic1];
        //    [self.dataArray addObject:dic2];
        [self.dataArray addObject:dic6];//新调整
        [self.dataArray addObject:dic3];
        [self.dataArray addObject:dic4];
        [self.dataArray addObject:dic5];
        [self.dataArray addObject:dic2];//新调整
    }
    
    
    
//    NSMutableDictionary *tempDict1 = [[NSMutableDictionary alloc]init];
//    if ([APP_ID isEqualToString:@"1077216776"]) {
//        [tempDict1 setValue:@"特色医院" forKey:@"name"];
//        [tempDict1 setValue:@"img_home_medical_icon1" forKey:@"image"];
//    }
//    else
//    {
//        [tempDict1 setValue:@"山西省中西医结合医院" forKey:@"name"];
//        [tempDict1 setValue:@"img_home_medical_icon1" forKey:@"image"];
//    }
//    
//    NSMutableDictionary *tempDict6 = [[NSMutableDictionary alloc]init];
//    if ([APP_ID isEqualToString:@"1077216776"]) {
//        [tempDict6 setValue:@"出诊专家" forKey:@"name"];
//        [tempDict6 setValue:@"img_home_medical_icon2" forKey:@"image"];
//    }
//    else
//    {
//        [tempDict6 setValue:@"专家出诊" forKey:@"name"];
//        [tempDict6 setValue:@"img_home_medical_icon2" forKey:@"image"];
//    }
//    
//    NSDictionary *dic1 = tempDict1;
//    NSDictionary *dic2 = @{@"name":@"健康讲座",@"image":@"img_home_medical_icon6"};
//    NSDictionary *dic3 = @{@"name":@"太原有线",@"image":@"img_home_medical_icon3"};
//    NSDictionary *dic4 = @{@"name":@"看电影",@"image":@"img_home_medical_icon4"};
//    NSDictionary *dic5 = @{@"name":@"就诊助手",@"image":@"img_home_medical_icon5"};
//    NSDictionary *dic6 = tempDict6;
//    [self.dataArray addObject:dic1];
//    //    [self.dataArray addObject:dic2];
//    [self.dataArray addObject:dic6];//新调整
//    [self.dataArray addObject:dic3];
//    [self.dataArray addObject:dic4];
//    [self.dataArray addObject:dic5];
//    [self.dataArray addObject:dic2];//新调整
    
}

- (void)reqestHome{
    [self showCustomeHUD];
    
    if (!is_Separate_API) {
        
        [self reqestHomeDate];
    }
    
    if (!IS_PUBLIC_ENV) {
        
        [self reqestRecord];
    }
    
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    if (regionCode.length==0) {
        regionCode = REGION_CODE;
    }
    
    NSString *microH = kMicroHospital;
    NSString *homeJIUZHEM = Home_JIUZHENZHUSHOU;
    NSString *homeCHUZHEM = Home_ZHUANJIACHUZHEN;
    if (microH.length || homeCHUZHEM.length || homeJIUZHEM.length) {
        return;
    }
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    //    [_request VODHospitalInfoWithTimeout:10];
    [request EPGGetRegionInfoWithRegionCode:regionCode andTimeout:5];
    
}
- (void)reqestHomeDate{
    
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    if(_IS_SUPPORT_VOD)
    {
        //*****5请求搜索热门字第一天
        [requst VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    }else{
        [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }
    
    [requst HomeADSLiderPagess];
    
}
- (void)reqestRecord{
    //*******4请求观看记录
    HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
    
    if (IsLogin) {
        
        
        
        
        [requst VODNewPlayRecordListWithStartSeq:1 andCount:1 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
        
        
        [requst EventPlayListWithEventID:@"" andCount:1 andTimeout:10];
        _requestCount = 0;
        _VODdictionDic = [[NSDictionary alloc]init];;
        _EPGdictionDic = [[NSDictionary alloc]init];
        
        
        
    }else{
        [self setHighForheadIsOrNo:NO];
        
    }
    
}

#pragma mark - 代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [Tool numberOfCellRowsWithArray:self.dataArray withRowNum:2];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    if (!cell) {
        cell = [[NewHomeTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"identifier"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *arr = [Tool arrayFOR_ALLArray:self.dataArray withBeginnumb:indexPath.row withRowNum:2];
    cell.dateArray.array = arr;
    [cell update];
    cell.block = ^(UIButton_Block*btn){
        NSLog(@"123  btn%@",btn.Diction);
        [self didSelectWith:btn.Diction];
    };
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 83*kRateSize;
}
- (void)didSelectWith:(NSDictionary*)dic{
    NSString *string = [dic objectForKey:@"image"];
    NSString *title = [dic objectForKey:@"name"];
    if ([string isEqualToString:@"img_home_medical_icon1"]) {
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiYuan;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
            HospitalViewController *hospi = [[HospitalViewController alloc]init];
            hospi.HospitalType = HospitalTypeYiYuan;
            hospi.YiYuanTitle = title;
            [self.navigationController pushViewController:hospi animated:YES];
        }
        
        
        
    }else if ([string isEqualToString:@"img_home_medical_icon6"]){
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiVideo;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
            HospitalViewController *hospi = [[HospitalViewController alloc]init];
            hospi.HospitalType = HospitalTypeYiVideo;
            hospi.YiYuanTitle = title;
            [self.navigationController pushViewController:hospi animated:YES];
            
        }
        
        
        
    }else if ([string isEqualToString:@"img_home_medical_icon3"]){
        
        LiveHomeViewController *LIVE = [[LiveHomeViewController alloc]init];
        [self.navigationController pushViewController:LIVE animated:YES];
        
    }else if ([string isEqualToString:@"img_home_medical_icon4"]){
        
        ScrollDemandPageViewController *VOD = [[ScrollDemandPageViewController alloc]init];
        [self.navigationController pushViewController:VOD animated:YES];
        
    }else if ([string isEqualToString:@"img_home_medical_icon5"]){
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiZhuShou;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
            HospitalViewController *hospi = [[HospitalViewController alloc]init];
            hospi.HospitalType = HospitalTypeYiZhuShou;
            hospi.YiYuanTitle = title;
            [self.navigationController pushViewController:hospi animated:YES];
        }
        
        //        HomeWebViewViewController *homeWeb = [[HomeWebViewViewController alloc]init];
        //        homeWeb.TitleName = @"就诊助手";
        //        homeWeb.urlLink = Home_JIUZHENZHUSHOU;
        //        if (homeWeb.urlLink.length>3) {
        //            [self.navigationController pushViewController:homeWeb animated:YES];
        //        }else{
        //            [AlertLabel AlertInfoWithText:@"敬请期待！" andWith:self.view withLocationTag:0];
        //        }
        
    }else if ([string isEqualToString:@"img_home_medical_icon2"]){
        
        NSString *code =  [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
        if (code.length == 0) {
            DemandChooseHospitalControl *demandChoss = [[DemandChooseHospitalControl alloc]init];
            demandChoss.HospitalType = HospitalTypeYiZhuanJia;
            demandChoss.YiYuanTitle = title;
            [self.navigationController pushViewController:demandChoss animated:YES];
            
        }else{
            HospitalViewController *hospi = [[HospitalViewController alloc]init];
            hospi.HospitalType = HospitalTypeYiZhuanJia;
            hospi.YiYuanTitle = title;
            [self.navigationController pushViewController:hospi animated:YES];
        }
        
        //        HomeWebViewViewController *homeWeb = [[HomeWebViewViewController alloc]init];
        //        homeWeb.TitleName = @"专家出诊";
        //        homeWeb.urlLink = Home_ZHUANJIACHUZHEN;
        //        if (homeWeb.urlLink.length>3) {
        //            [self.navigationController pushViewController:homeWeb animated:YES];
        //        }else{
        //            [AlertLabel AlertInfoWithText:@"敬请期待！" andWith:self.view withLocationTag:0];
        //        }
        
    }
    
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    if ([type isEqualToString:@"topSearchWords"]) {
        NSDictionary *dic = result;
        NSString *count = [dic objectForKey:@"count"];
        
        if ([count integerValue] == -1 ) {
            NSLog(@"暂时搜索热词记录");
            _seachTextView.text = @"";
        }else if ([count integerValue] == 1 )
        {
            NSArray *array = [dic objectForKey:@"keyList"];
            if (array.count) {
                NSDictionary *record = array[0];
                _VODdictionDic = [[NSDictionary alloc]initWithDictionary:record];
                NSString *name = [record objectForKey:@"name"];
                NSLog(@"名字是：： %@",name);
                if (kDeviceWidth == 320) {
                    
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                    
                }else if (kDeviceWidth == 375)
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
                else
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
                
                
                
            }
        }
        
    }
    
    if ([type isEqualToString:HomeADSLiderPage]) {
        NSMutableArray *array = [result objectForKey:@"slideshowList"];
        NSLog(@"广告的数据是%@",array);
        [self hiddenCustomHUD];
        if (array.count>1) {
            array = [KaelTool increaseArrayWithArray:array andKey:@"seq"];
            NSDictionary *firstDic = [array firstObject];
            [array removeObjectAtIndex:0];
            [array addObject:firstDic];
        }
        if(array.count){
            [self.posterView updateThePosterData:array];
        }
        
        
        
    }
    
    if ([type isEqualToString:VOD_NewPlayRecordList]) {
        NSLog(@"观看记录: %ld result:%@  type:%@",status,result,type );
        [self hiddenCustomHUD];
        _requestCount ++;
        
        NSDictionary *dic = result;
        NSString *ret = [dic objectForKey:@"count"];
        
        if ([ret integerValue] <=0 ) {
            NSLog(@"暂时没有播放记录");
            
      
            
            
        }else if ([ret integerValue] > 0 )
        {
            [self hiddenCustomHUD];
            NSArray *array = [dic objectForKey:@"vodList"];
            if (array.count) {
                NSDictionary *record = array[0];
                _VODdictionDic = [[NSDictionary alloc]initWithDictionary:record];
                NSLog(@"点播记录%@",_VODdictionDic);
                
            }else
            {
                
            }
            
            
        }
        
        
        if (_requestCount == 3) {
            NSLog(@"请求完成了%@ %@",_VODdictionDic,_EPGdictionDic);
            [self setUPRecordWithDic:_VODdictionDic andEPG:_EPGdictionDic];
            if ((_VODdictionDic.count >0) &&(_EPGdictionDic.count>0) ) {
                [self setHighForheadIsOrNo:YES];
            }else if((_VODdictionDic.count == 0) &&(_EPGdictionDic.count == 0)){
                [self setHighForheadIsOrNo:NO];
            }else{
                [self setHighForheadIsOrNo:YES];
            }
            
        }
        
        
    }
    
    //
    //EPG_TVPLAYLIST
    if ([type isEqualToString:EPG_EVENTPLAYLIST]) {
        _requestCount ++;
        
        NSLog(@"直播结果%@",result);
        NSString *ret = [result objectForKey:@"ret"];
        NSArray *array = [result objectForKey:@"eventlist"];
        if ([ret integerValue] < 0 ) {
            NSLog(@"暂时没有播放记录");
         
        }else{
            if (array.count) {
                
                NSDictionary *dic = [array firstObject];
                NSLog(@"回看记录%@",dic);
                _EPGdictionDic = [[NSDictionary alloc]initWithDictionary:dic];
                ;
              
                
            }else{
               
            }
        }
        
        if (_requestCount == 3) {
            NSLog(@"请求完成s了%@ %@",_VODdictionDic,_EPGdictionDic);
            [self setUPRecordWithDic:_VODdictionDic andEPG:_EPGdictionDic];
            if ((_VODdictionDic.count >0) &&(_EPGdictionDic.count>0) ) {
                [self setHighForheadIsOrNo:YES];
            }else if((_VODdictionDic.count == 0) &&(_EPGdictionDic.count == 0)){
                [self setHighForheadIsOrNo:NO];
            }else{
                [self setHighForheadIsOrNo:YES];
            }
            
        }
        
        
    }
    
    
    
    //            EPG_EVENTPLAY
    if ([type isEqualToString:EPG_EVENTPLAY]){
        
        
        if (result.count > 0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"成功");
                    
                    //NSDictionary *dataDic = [NSDictionary dictionary];
                    
                    NSDictionary *dataDic = [result objectForKey:@"serviceinfo"];
                    
                    NSLog(@"%@",dataDic);
                    
                    
                    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                    live.currentPlayType = CurrentPlayEvent;
                    live.serviceID = [dataDic objectForKey:@"serviceID"];
                    live.serviceName = [dataDic objectForKey:@"name"];
                    live.eventURL =[NSURL URLWithString:[dataDic objectForKey:@"playLink"]];
                    live.eventURLID = _channelID;
                    live.currentDateStr = _endDate;
                    [self.navigationController pushViewController:live animated:YES];
                    
                    
                    break;
                }
                case -1:{
                    
                    NSLog(@"当前节目不存在或者不提供回看");
                    
                    [AlertLabel AlertInfoWithText:@"当前节目不存在或者不提供回看" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                case -2:{
                    
                    NSLog(@"用户未登录或者登录超时");
                    
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                case -3:{
                    
                    NSLog(@"鉴权失败");
                    
                    [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                case -5:{
                    
                    NSLog(@"频道对应此终端无播放链接");
                    
                    [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                case -6:{
                    
                    NSLog(@"该频道不提供回看功能");
                    
                    [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接3A系统失败或者参数错误）");
                    [AlertLabel AlertInfoWithText:@"失败（连接3A系统失败或者参数错误）" andWith:self.view withLocationTag:1];
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
    
    
    //顶部的搜索文字
    if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
        NSLog(@"热门记录: %ld result:%@  type:%@",status,result,type );
        
        NSDictionary *dic = result;
        NSString *count = [dic objectForKey:@"count"];
        
        if ([count integerValue] == -1 ) {
            NSLog(@"暂时搜索热词记录");
            _seachTextView.text = @"";
            HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
            [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
        }else if ([count integerValue] == 1 )
        {
            NSArray *array = [dic objectForKey:@"keyList"];
            if (array.count) {
                NSDictionary *record = array[0];
                NSString *name = [record objectForKey:@"name"];
                NSLog(@"名字是：： %@",name);
                if (kDeviceWidth == 320) {
                    
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                    
                }else if (kDeviceWidth == 375)
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
                else
                {
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                }
                
                
                
            }
        }
        
    }
    
    
    if ([type isEqualToString:EPG_REGION_INFO]) {
        
        NSLog(@"%@",result);
        [self loadingHospitalURLWithDic:result];
        
    }
}

- (void)setSeachBar
{
    
    //    UIButton *logo = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    logo = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"ic_launcher" imgHighlight:@"ic_launcher"  target:self action:@selector(logo)];
    [self setNaviBarLeftBtn:nil];
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(14*kRateSize, 25, 34, 34)];
    logo.image = [UIImage imageNamed:@"ic_launcher"];
    [self.m_viewNaviBar addSubview:logo];
    //设置搜索textField
    
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0*kRateSize, 0, size.width, size.height)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = size.height/2;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(40 ,1,size.width - 33*kRateSize/kRateSize , 26)];
    //            _seachTextView.backgroundColor = [UIColor redColor];
    //    seachTextView.delegate = self;
    _seachTextView.text = @"";
    //        _seachTextView.backgroundColor = [UIColor redColor];
    _seachTextView.font = [UIFont systemFontOfSize:16];
    _seachTextView.textColor = UIColorFromRGB(0xaaaaaa);
    
    //    seachTextView.preservesSuperviewLayoutMargins = YES;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    //    UIButton *btn = [seachTextView valueForKey:@"_clearButton"];
    //    [btn setImage:[UIImage imageNamed:@"sl_search_del_item_pressed"] forState:UIControlStateNormal];
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake((_seachTextView.frame.origin.x-30)*kRateSize,( _seachTextView.frame.origin.y+1)*kRateSize, 24, 24)];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.alpha = 0.8;
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        //
        SearchViewController *search1 = [[SearchViewController alloc]init];

        //        search1.searchText = [array lastObject];;
        [self.navigationController pushViewController:search1 animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    
    
    
}
#pragma  mark - 广告点击
- (void)AdClicked:(NSNotification*)notif
{
    NSLog(@"%@",notif);
    if (0) {
        NSDictionary *object = notif.object;
        NSString *name = [object objectForKey:@"name"];
        NSString *contentID = [object objectForKey:@"contentID"];
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = contentID;
        demandVC.contentTitleName = name;
        
        
        [self.navigationController pushViewController:demandVC animated:YES];
    }else
    {
        NSDictionary *object = notif.object;
        
        NSString *type = [object objectForKey:@"type"];
        [self homeADJumpWithType:type andDiction:object];
    }
    
    
    
}
- (void)homeADJumpWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    
    NSInteger typeInt = [type integerValue];
    switch (typeInt) {
        case 4:
        {
            if (_IS_SUPPORT_VOD) {
                NSString *name = [dic objectForKey:@"name"];
                NSString *contentID = [dic objectForKey:@"id"];
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = contentID;
                demandVC.contentTitleName = name;
                [self.navigationController pushViewController:demandVC animated:YES];
            }else
            {
                [AlertLabel AlertInfoWithText:@"点播功能敬请期待！" andWith:self.view withLocationTag:0];
            }
            
        }
            
            break;
            
        case 5:
        {
            EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
            epg.serviceID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
            epg.serviceName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
            epg.currentPlayType = CurrentPlayLive;
            epg.IsFirstChannelist = YES;
            
            
            [self.navigationController pushViewController:epg animated:YES];
            
        }
            
            break;
        case 7:
        {
            NSLog(@"要跳转的是%@",dic);
            NSString *url = [dic objectForKey:@"urlLink"];
            if (url.length > 0 ) {
                DemandADDetailViewController *demandDetail = [[DemandADDetailViewController alloc]init];
                demandDetail.detailADURL = [dic objectForKey:@"urlLink"];
                [self presentViewController:demandDetail animated:YES completion:nil];
            }
            
            
            
        }
            
            break;
        default:
        {
            
        }
            break;
    }
    
}


-(void)loadingHospitalURLWithDic:(NSDictionary *)hospitalDic{
    NSMutableString *allUrlLink = [NSMutableString stringWithFormat:@"%@",[[hospitalDic objectForKey:@"region"] objectForKey:@"urlLink"]];
    
    NSArray *urlArr = [[NSArray alloc] initWithArray:[allUrlLink componentsSeparatedByString:@"|"]];
    NSString *microHospital = @"";
    NSString *chuzhenStr = @"";
    NSString *jiuzhenStr = @"";
    
    for (int i=0; i<urlArr.count; i++) {
        switch (i) {
            case 0:
            {
                microHospital = [urlArr objectAtIndex:i];
                break;
            }
            case 1:
            {
                chuzhenStr = [urlArr objectAtIndex:i];
                
                break;
            }
            case 2:
            {
                jiuzhenStr = [urlArr objectAtIndex:i];
                
                break;
            }
            default:
                break;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:microHospital forKey:@"microHospital"];
    [[NSUserDefaults standardUserDefaults] setObject:chuzhenStr forKey:@"chuzhenStr"];
    [[NSUserDefaults standardUserDefaults] setObject:jiuzhenStr forKey:@"jiuzhenStr"];
    
}
#pragma mark - 工具计算

- (void)setUPRecordWithDic:(NSDictionary*)vod andEPG:(NSDictionary*)EPG{
    NSMutableDictionary *voddiction = [[NSMutableDictionary alloc]initWithDictionary:vod];
    NSMutableDictionary *epgdiction = [[NSMutableDictionary alloc]initWithDictionary:EPG];
    
    NSString *vodplayTime = [voddiction objectForKey:@"playTime"];
    NSString *vodduration = [voddiction objectForKey:@"breakPoint"];
    NSString *vodname = [voddiction objectForKey:@"name"];
    
    NSString *EPGplayTime = [epgdiction objectForKey:@"playTime"];
    NSString *EPGduration = [epgdiction objectForKey:@"breakPoint"];
    NSString *EPGeventName = [epgdiction objectForKey:@"eventName"];
    
    
    if (vodplayTime.length && EPGplayTime.length) {
        if ([Tool compareTime:vodplayTime endTime:EPGplayTime] == NSOrderedAscending) {
            NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",EPGeventName,[Tool secendtoTime:[EPGduration integerValue]]];
            self.XUKanLabel.text = showString;
            [epgdiction setObject:@"EPG" forKey:@"TYPE"];
            self.XUKanBtn.Diction = epgdiction;
        }
        if ([Tool compareTime:vodplayTime endTime:EPGplayTime] == NSOrderedDescending) {
            NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",vodname,[Tool secendtoTime:[vodduration integerValue]]];
            self.XUKanLabel.text = showString;
            [voddiction setObject:@"VOD" forKey:@"TYPE"];
            self.XUKanBtn.Diction = voddiction;
        }
    }else if (vodplayTime.length){
        NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",vodname,[Tool secendtoTime:[vodduration integerValue]]];
        self.XUKanLabel.text = showString;
        [voddiction setObject:@"VOD" forKey:@"TYPE"];
        self.XUKanBtn.Diction = voddiction;
        
    }else if (EPGplayTime.length){
        NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",EPGeventName,[Tool secendtoTime:[EPGduration integerValue]]];
        self.XUKanLabel.text = showString;
        [epgdiction setObject:@"EPG" forKey:@"TYPE"];
        self.XUKanBtn.Diction = epgdiction;
    }else{
        
    }
    
    
}

- (void)updateRecordWithType:(NSString*)type andDiction:(NSDictionary*)dic{
    NSMutableDictionary *diction = [[NSMutableDictionary alloc]initWithDictionary:dic];
    
    
    if ([type isEqualToString:@"VOD"]) {
        //        NSString *contenID = [dic objectForKey:@"contentID"];
        NSString *playTime = [dic objectForKey:@"playTime"];
        NSString *duration = [dic objectForKey:@"duration"];
        NSString *name = [dic objectForKey:@"name"];
        
        
        if (playTime.length &&[Tool compareTime:_continueToReadDate endTime:playTime] != NSOrderedDescending){
            _continueToReadDate = playTime;
            NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",name,[Tool secendtoTime:[duration integerValue]]];
            self.XUKanLabel.text = showString;
            _haveRecord =YES;
            [diction setObject:@"VOD" forKey:@"TYPE"];
            self.XUKanBtn.Diction = diction;
        }else if ([Tool compareTime:_continueToReadDate endTime:playTime] == NSOrderedDescending){
            _haveRecord =NO;
        }
        
        
    }else{

        NSString *playTime = [dic objectForKey:@"playTime"];
        NSString *duration = [dic objectForKey:@"breakPoint"];
        NSString *eventName = [dic objectForKey:@"eventName"];
        
        if (_IS_SUPPORT_VOD) {
            if (playTime.length && ([Tool compareTime:_continueToReadDate endTime:playTime] == NSOrderedAscending)) {
                _continueToReadDate = playTime;
                NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",eventName,[Tool secendtoTime:[duration integerValue]]];
                self.XUKanLabel.text = showString;
                [diction setObject:@"EPG" forKey:@"TYPE"];
                self.XUKanBtn.Diction = diction;
                _haveRecord = YES;
            }else if ([Tool compareTime:_continueToReadDate endTime:playTime] == NSOrderedDescending){
                _haveRecord =YES;
            }
            
        }else{
            _continueToReadDate = playTime;
            NSString *showString = [NSString stringWithFormat:@"续看 %@ %@",eventName,[Tool secendtoTime:[duration integerValue]]];
            self.XUKanLabel.text = showString;
            [self setHighForheadIsOrNo:YES];
            [diction setObject:@"EPG" forKey:@"TYPE"];
            self.XUKanBtn.Diction = diction;
            _haveRecord = YES;
        }
        
    }
    
    if (_haveRecord) {
        [self setHighForheadIsOrNo:YES];
    }else{
        [self setHighForheadIsOrNo:NO];
    }
    
    
    
}


- (void)setHighForheadIsOrNo:(BOOL)havePlayRecorder
{
    
    //<<<<<<< .mine
    
    //    if ((havePlayRecorder&&_RecordView.hidden==NO) || (havePlayRecorder==NO && _RecordView.hidden==YES)) {
    //        return;
    //    }
    
    
    //=======
    
    if ((havePlayRecorder&&_RecordView.hidden==NO) || (havePlayRecorder==NO && _RecordView.hidden==YES)) {
        return;
    }
    
    
    //>>>>>>> .r43
    if (havePlayRecorder) {
        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital*1)*kRateSize-2.5*kRateSize);
        _RecordView.hidden = NO;
        [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(40*kRateSize));
        }];
        
        self.tableView.tableHeaderView = nil;
        self.tableView.tableHeaderView = _Headview;
    }else{
        _Headview.frame = CGRectMake(0, 0, FRAME_W(self.view), (154+kheigh+WeiHospital*0)*kRateSize-2.5*kRateSize);
        _RecordView.hidden = YES;
        [_RecordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.posterView.mas_bottom ).offset(0*kRateSize);
            make.left.equalTo(_Headview.mas_left);
            make.width.equalTo(_Headview.mas_width);
            make.height.equalTo(@(0*kRateSize));
        }];
        
        self.tableView.tableHeaderView = nil;
        self.tableView.tableHeaderView = _Headview;
    }
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
