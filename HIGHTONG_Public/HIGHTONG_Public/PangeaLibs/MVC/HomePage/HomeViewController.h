//
//  HomeViewController.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HT_FatherViewController.h"
@interface HomeViewController : HT_FatherViewController<UITableViewDataSource,UITableViewDelegate>

@end
