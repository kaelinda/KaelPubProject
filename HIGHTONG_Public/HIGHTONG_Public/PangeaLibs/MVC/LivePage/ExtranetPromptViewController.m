//
//  ExtranetPromptViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ExtranetPromptViewController.h"
#import "ExtranetPromptView.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

@interface ExtranetPromptViewController ()

@end

@implementation ExtranetPromptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = App_background_color;
    
    [self setNaviBarTitle:self.ExtranetPromptTitle];
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
    [self setupExtranetPrompt];
    
}

- (void)setupExtranetPrompt
{
    ExtranetPromptView *promptView = [[ExtranetPromptView alloc]init];
    
    [self.view addSubview:promptView];
    
    WS(wself);
    
    [promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(162*kRateSize);
        make.right.mas_equalTo(wself.view.mas_right);
        make.left.mas_equalTo(wself.view.mas_left);
        make.height.mas_equalTo(@(300*kRateSize));
    }];
    
    promptView.moreInformationActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");
        KnowledgeBaseViewController *knowledgeVC = [[KnowledgeBaseViewController alloc]init];
        
//        ：http://101.200.148.37/shphone/pages/jxnet/out/index.html
        
        knowledgeVC.urlStr = [UrlPageJumpManeger extranetPromptPageJump];
        
        knowledgeVC.existNaviBar = YES;
        
//        if ([APP_ID isEqualToString: @"562172033"]) {
//            
//            NSString *out_Net = [NSString stringWithFormat:@"%@/shphone/pages/jxnet/out/index.html",YJ_Shphone];
//            knowledgeVC.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
//
//        }else{
//        
//            NSString *out_Net = web_OutNet_More_Detail;
//            knowledgeVC.urlStr = [NSString stringWithFormat:@"%@?regionCode=%@&instanceCode=%@&tstyle=%@&u=%@&devType=%@&devID=%@&ch=3",out_Net,KREGION_CODE,KInstanceCode,App_Theme_Color_num,U_KEY,TERMINAL_TYPE,ICS_deviceID];
//        }
        
        [self.navigationController pushViewController:knowledgeVC animated:YES];
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark - navigation上的按钮
- (void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"返回上一级菜单");
}

@end
