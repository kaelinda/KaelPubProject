//
//  ExtranetPromptView.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ExtranetPromptView.h"
#import "UIButton_Block.h"

@interface ExtranetPromptView ()
{
    
    UILabel *_EPLb;//提示文字
    UIImageView *_EPImg;//提示图像
    UIButton_Block *_EPBtn;//了解更多按钮
    
}
@end

@implementation ExtranetPromptView

- (instancetype)init{
    self = [super init];
    if (self) {
        
        [self initSubViews];
        
    }
    return self;
}

-(void) initSubViews{
    
    self.backgroundColor = App_background_color;
    
    _EPImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_learn_more_metadata"]];
    
    WS(wself);
    
    [self addSubview:_EPImg];
    
    [_EPImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself).offset(8*kRateSize);
        make.centerX.mas_equalTo(wself);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
    }];
    
    
    
    //***********当前网络名称
    _EPLb = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:App_selected_color andFont:App_font(15)];
    _EPLb.textColor = UIColorFromRGB(0x999999);
    [_EPLb setText:@" 抱歉，该内容需要      连接广电网络才可观看"];
    _EPLb.numberOfLines = 2;
    _EPLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_EPLb];
    
    [_EPLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_EPImg.mas_bottom).offset(10*kRateSize);
        make.right.mas_equalTo(wself).offset(-80*kRateSize);
        make.left.mas_equalTo(wself).offset(80*kRateSize);
//        make.height.mas_equalTo(@(30*kRateSize));
    }];
    
    _EPBtn = [[UIButton_Block alloc] init];
    _EPBtn.Click = ^(UIButton *btn, NSString *str){

        
        if (wself.moreInformationActionBlock) {
            wself.moreInformationActionBlock();
        }else{
            NSLog(@"你还没实现查看更多免费WiFi场所的 Block 呢 亲~");
        }
        
    };

//    [_EPBtn setImage:[UIImage imageNamed:@"btn_learn_more_normal"] forState:UIControlStateNormal];
    
    [_EPBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_EPBtn setTitle:@"了解更多"forState:UIControlStateNormal];
    _EPBtn.titleLabel.font = [UIFont systemFontOfSize:17*kRateSize];
    _EPBtn.layer.cornerRadius = 5;
    _EPBtn.layer.masksToBounds = YES;
    
    
    _EPBtn.backgroundColor = App_selected_color;
    
    [self addSubview:_EPBtn];
    
    
    CGSize btnsize = CGSizeMake(290*kRateSize, 40*kRateSize);
    [_EPBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnsize);
        make.top.mas_equalTo(_EPLb.mas_bottom).offset(15*kRateSize);
        make.centerX.mas_equalTo(wself);
    }];
}

@end
