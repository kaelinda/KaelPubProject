//
//  ExtranetPromptView.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtranetPromptView : UIView


@property (nonatomic,strong) void (^moreInformationActionBlock)();//了解更多按钮响应时间

@end
