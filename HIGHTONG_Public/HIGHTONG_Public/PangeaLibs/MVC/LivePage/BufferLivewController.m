//
//  BufferLivewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "BufferLivewController.h"
#import "EpgSmallVideoPlayerVC.h"

@interface BufferLivewController ()

@end

@implementation BufferLivewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"直播"];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    
    [button setBackgroundColor:[UIColor redColor]];
    
    [self.view addSubview:button];
    
    
    
    [button addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}



-(void)clickButton
{
    EpgSmallVideoPlayerVC *epgVC = [[EpgSmallVideoPlayerVC alloc]init];
    
    [self.navigationController pushViewController:epgVC animated:YES];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
