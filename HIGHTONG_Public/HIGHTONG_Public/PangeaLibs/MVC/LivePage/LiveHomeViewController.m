//
//  LiveHomeViewController.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "LiveHomeViewController.h"
#import "headScroll.h"
#import "UIButton_Block.h"
#import "DemandHomeView.h"
#import "Tool.h"
#import "LiveHomeTableViewCell.h"
#import "SearchViewController.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "EpgSmallVideoPlayerVC.h"
#import "ExtranetPromptView.h"
#import "KnowledgeBaseViewController.h"
#import "CustomSearchBarView.h"
#import "UrlPageJumpManeger.h"

@interface LiveHomeViewController ()<DemandHomeViewDelegate, HTRequestDelegate,UITableViewDelegate,UITableViewDataSource>
{
    headScroll *  _head;//头标题头
    
    DemandHomeView *_ScrollTable;//滑动的tableView
    NSMutableArray *_ScollArray;//滑动的数据源
    NSMutableArray *channelList;//所有节目列表
    
    NSMutableArray * _headArray ;//头的数组
    NSMutableDictionary *_scrollDic;
    
    NSInteger _currentIndex;//当前页数
    
//    UILabel *_seachTextView;//搜索热词
//    
//    UIView *_topSearchView;//搜索View
    
    CGFloat MBRectX;//..
    
    BOOL firstLoad;//第一次加载
    
    UIView *_HTVw;
    UILabel *_HTLb;
    UIImageView *_HTImg;
    
    UIView *_QGVw;//全国版本运营商view
    UILabel *_QGoperatorName;//全国版本运营商名称
    UIImageView *_QGoperatorLogo;//全国版本运营商logo
    
    ExtranetPromptView *_promptView;//了解更多view
    
    
    UIView *_VerticalLine;//头视图分割线

    NSDate *_requestDate;//加载时间
    
    NSDictionary *dic ;
    
    BOOL requestYES;//请求完成了

    UIImageView *Guide ;//引导图
    
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    
    UIButton *_requestEPGBtn;//epg再次请求按钮
    UIButton *_requestChannelBtn;//channel再次请求按钮
    BOOL loadDateResult;//网络加载成功标志符
}

@property (nonatomic, copy)NSMutableArray *epgChannelList;//频道列表
@property (nonatomic, strong)CustomSearchBarView *searchBarView;

@end

@implementation LiveHomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!IS_EPG_CONFIGURED) {
        
        _head.hidden = YES;
        
        _VerticalLine.hidden = YES;
        
        _ScrollTable.hidden = YES;
        
        _promptView.hidden = NO;
        
//        _topSearchView.hidden = YES;
        
        _searchBarView.hidden = YES;
        
        return;
        
    }
    else
    {
        _VerticalLine.hidden = NO;
        
        _head.hidden = NO;
        
        _ScrollTable.hidden = NO;
        
        _promptView.hidden = YES;
        
//        _topSearchView.hidden = NO;
        
        _searchBarView.hidden = NO;
    }
    
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    if (firstLoad) {
        if ([self RequstEnabledWithFirstTime:_requestDate andSecondData:[NSDate date]] || requestYES) {
            [self loadData];
        }
    }
    firstLoad = YES;

    _head.contentOffset = CGPointMake(0, 0);
    _head.showsHorizontalScrollIndicator = YES;
    _head.showsVerticalScrollIndicator = YES;
    
    
    [MobCountTool pushInWithPageView:@"LiveHomePage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"LiveHomePage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
        NSLog(@"dfafasfsfadsfsfsf");
    }
#endif
    // Do any additional setup after loading the view.
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    if(HomeNewDisplayTypeOn){
        UIButton *Back = [[UIButton alloc]initWithFrame:CGRectMake(2, 18, 44, 44)];
        Back.showsTouchWhenHighlighted = YES;
        [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
        [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateHighlighted];
        [Back addTarget:self action:@selector(goBackPage) forControlEvents:UIControlEventTouchUpInside];
        [self.m_viewNaviBar addSubview:Back];
    }
    
    _headArray = [NSMutableArray array];
    _ScollArray = [NSMutableArray array];
    _scrollDic = [NSMutableDictionary dictionary];
    channelList = [NSMutableArray array];
     HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
    _request = [[HTRequest alloc]initWithDelegate:self];

    _currentIndex = 0;

    //请求时间
    _requestDate = [NSDate date];
    
    if(_IS_SUPPORT_VOD)
    {
        //*****3请求搜索热门字第一天
        [_request VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    }else{
        [_request TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }

    [self setSeachBar];
    
    [self setupOPERATIONHeader];
    
    self.view.backgroundColor = App_background_color;
    
    
    [self.view addSubview:[UIView new]];
    
    [self setupScrollTableView];
    
    [self setupExtranetPrompt];

}

- (void)setupScrollTableView
{
    if (!_ScrollTable) {
        
        if ([APP_ID isEqualToString:@"562172033"]) {
            
            _ScrollTable = [[DemandHomeView alloc]initWithFrame:CGRectMake(0, 64+40*kDeviceRate+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-85*kDeviceRate-kTopSlimSafeSpace-kBottomSafeSpace)];
            
        }
        else
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            NSString *operatorCode = [userDefaults objectForKey:@"operatorCode"];
            
            if (operatorCode.length == 0) {
                _ScrollTable = [[DemandHomeView alloc]initWithFrame:CGRectMake(0, 64+40*kDeviceRate+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-40*kDeviceRate-kTopSlimSafeSpace-kBottomSafeSpace)];
            }
            else
            {
                
                _ScrollTable = [[DemandHomeView alloc]initWithFrame:CGRectMake(0, 64+40*kDeviceRate+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-75*kDeviceRate-kTopSlimSafeSpace-kBottomSafeSpace)];
                
                [self setupOperatorUI];
                
            }
            
        }
        
        _ScrollTable.addHigh = -4;
        [self.view addSubview:_ScrollTable];
        
    }
}

- (void)setupExtranetPrompt
{
    
    _promptView = [[ExtranetPromptView alloc]init];
    
    [self.view addSubview:_promptView];
    
    _promptView.hidden = YES;
    
    WS(wself);
    
    [_promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(162*kDeviceRate);
        make.right.mas_equalTo(wself.view.mas_right);
        make.left.mas_equalTo(wself.view.mas_left);
        make.height.mas_equalTo(@(400*kDeviceRate));
    }];
    
    _promptView.moreInformationActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");
        KnowledgeBaseViewController *knowledgeVC = [[KnowledgeBaseViewController alloc]init];
        
        knowledgeVC.urlStr = [UrlPageJumpManeger extranetPromptPageJump];
        
        knowledgeVC.existNaviBar = YES;
                
        [wself.navigationController pushViewController:knowledgeVC animated:YES];
    };
    
}


#pragma mark- 状态栏按钮响应
//返回按钮操作
- (void)goBackPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData
{
    
    //移除重新请求按钮
    if (_requestEPGBtn) {
        
        _requestEPGBtn.hidden = YES;
        
    }
    
    if (_requestChannelBtn) {
        
        _requestChannelBtn.hidden = YES;
        
    }
    
    
    requestYES = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        requestYES = YES;
    });
    
     HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    [_request TypeListWithTimeout:10];
    
    //发送了请求了
    NSLog(@"开始请求直播页的首页信息");
   
    [_request EPGGetLivePageChannelInfoAndisType:_currentIndex + 1];
    
    [_request VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    
    if(_IS_SUPPORT_VOD)
    {
        //*****3请求搜索热门字第一天
        [_request VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    }else{
        [_request TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }
    
    //请求时间
    _requestDate = [NSDate date];
}

- (void)setupOPERATIONHeader
{
    WS(wself);

     HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    [_request TypeListWithTimeout:10];
    
    _head = [[headScroll alloc]initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, 40*kDeviceRate)];
    _head.KEY = @"name";
    
    __weak headScroll *weakhead = _head;
    
    __weak UIButton *weakrequestEPGBtn = _requestEPGBtn;
    
    __weak UIButton *weakrequestChannelBtn = _requestChannelBtn;
    
    __weak DemandHomeView *weakScrollTable = _ScrollTable;
    
    _head.block = ^(NSInteger num,UIButton_Block*btn)
    {
        weakhead.MubiaoNum = num;
        weakhead.isSelectedNum = YES;
        weakhead.dianjizhuangtai = YES;
        NSLog(@"%ld  %@",num,btn.serviceIDName);
        
        NSLog(@"好似魔鬼的步伐%ld",[[[wself.epgChannelList objectAtIndex:num]objectForKey:@"type"]integerValue]);
        
        
        NSInteger type = [[[wself.epgChannelList objectAtIndex:num]objectForKey:@"type"]integerValue];

        if (weakrequestEPGBtn) {
            
            weakrequestEPGBtn.hidden = YES;
            
        }
        
        
        if (weakrequestChannelBtn) {
            
            weakrequestChannelBtn.hidden = YES;
            
        }
        
        HTRequest *_request  =[[HTRequest alloc]initWithDelegate:wself];
        [_request EPGGetLivePageChannelInfoAndisType:type];
        [wself show];
        
        [weakhead setSelectedNum:num];
        [weakScrollTable ChangePage:num+1];
        _currentIndex = num;
        //点击的哪一个头

        
    };
    [self.view addSubview:[UIView new]];
    [self.view addSubview:_head];
    
    _VerticalLine = [[UIView alloc]init];
    [self.view addSubview:_VerticalLine];
    _VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [_VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_head.mas_bottom).offset(-2);
        make.left.equalTo(_head.mas_left);
        make.right.equalTo(_head.mas_right);
        make.height.equalTo(@(2));
    }];
    
}

- (void)setupOperatorUI
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *operatorName = [userDefaults objectForKey:@"operatorName"];
    
    NSString *operatorLogo = [userDefaults objectForKey:@"operatorLogo"];
    
    _QGVw = [[UIView alloc]initWithFrame:CGRectMake(0, KDeviceHeight - 35*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 35*kDeviceRate)];
    [self.view addSubview:_QGVw];
    _QGVw.backgroundColor = UIColorFromRGB(0xffffff);
    
    UIView *VerticalLine = [[UIView alloc]init];
    [_QGVw addSubview:VerticalLine];
    VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_QGVw.mas_top).offset(1);
        make.left.equalTo(_QGVw.mas_left);
        make.right.equalTo(_QGVw.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _QGoperatorLogo = [[UIImageView alloc]init];
    [_QGVw addSubview:_QGoperatorLogo];
    
    [_QGoperatorLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_QGVw.mas_top).offset(2*kDeviceRate);
        make.left.equalTo(_QGVw.mas_left).offset(107*kDeviceRate);
        make.height.equalTo(@(30*kDeviceRate));
        make.width.equalTo(@(30*kDeviceRate));
    }];
    
    _QGoperatorName = [[UILabel alloc]init];
    [_QGVw addSubview:_QGoperatorName];
    
    [_QGoperatorName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_QGVw.mas_centerY);
        make.left.equalTo(_QGoperatorLogo.mas_right).offset(5*kDeviceRate);
    }];
    
    if (operatorName.length == 0 && operatorLogo.length == 0) {
        _QGoperatorLogo.image = [UIImage imageNamed:@"ic_launcher"];
        
        _QGoperatorName.text = kAPP_NAME;
    }
    else
    {
        [_QGoperatorLogo sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(operatorLogo)] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];
        
        _QGoperatorName.text = operatorName;
    }
    
    _QGoperatorName.font = App_font(18);
    
}


#pragma mark-   DemandHomeViewDelegate 滑动的Tableview  代理
//滚动结束后，调用数据
- (void)didChangePageWithPages:(NSInteger)page
{
    [_head setSelectedNum:page];
    _currentIndex = page-1;
    _head.dianjizhuangtai = NO;
    
    NSLog(@"-=当前页数%ld",(long)_currentIndex);
    
    NSMutableArray *tempArray = [NSMutableArray array];
    tempArray = [[_ScollArray objectAtIndex:_currentIndex]valueForKey:@"List"];
    NSLog(@"%lu", (unsigned long)tempArray.count);
    NSDictionary *dic = [_head.titledataArray objectAtIndex:_currentIndex];
    NSString *type = [dic objectForKey:@"type"];
    
    if ((unsigned long)tempArray.count <= 0) {
        HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
        [_request EPGGetLivePageChannelInfoAndisType:[type integerValue]];
        [self show];
    }
    else
    {
        UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
        if (Tab) {
            [Tab reloadData];
        }
    }
}

- (void)didRefreshDateWithTag:(NSInteger)tag
{
    NSLog(@"%ld目前的下拉中的东西啊",tag);
    
    if (_headArray.count > _currentIndex) {
        NSDictionary *dic = [_headArray objectAtIndex:_currentIndex];
        NSString *type = [dic objectForKey:@"type"];
        
        NSLog(@"%ld时机当前页",_currentIndex);
         HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
        [_request EPGGetLivePageChannelInfoAndisType:[type integerValue]];
    }
}
#pragma mark-工具块
- (void)logo
{
    NSLog(@"点击了logo");
}

/**
 *  显示loading框
 */
- (void)show{
    
    _HTImg.hidden = NO;
    [self showCustomeHUD];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}
/**
 *  隐藏loading框
 */
- (void)hide{
    _HTVw.hidden = NO;
    _HTImg.hidden = YES;
    [self hiddenCustomHUD];
}
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    if ([date1 timeIntervalSinceDate:date] >= 10) {
        NSLog(@"超时了啊");
        return YES;
    }
    return NO;
}

#pragma mark - 代理方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"live------%ld,%@,%@",(long)status,result,type);
    
    switch (status) {
        case 0:
        {

            if ([type isEqualToString:@"topSearchWords"]) {
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                
                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时搜索热词记录");
                    
                    _searchBarView.placeholderLab.text = @"";
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSLog(@"名字是：： %@",name);

                        _searchBarView.placeholderLab.text = [NSString stringWithFormat:@"%@",name];

                    }
                }
            }
            

            //得到分类的epg
            if ([type isEqualToString:EPG_TYPELIST]) {
                                NSLog(@"EPG_TYPELIST----%@",result);
                
                self.epgChannelList = [NSMutableArray array];
                
                self.epgChannelList = [result objectForKey:@"typeList"];
                
                HTRequest *_request = [[HTRequest alloc]initWithDelegate:self];
                
                [_request EPGGetLivePageChannelInfoAndisType:[[[self.epgChannelList objectAtIndex:_currentIndex] objectForKey:@"type"] integerValue]];
                
                NSLog(@"%ld",[[[self.epgChannelList objectAtIndex:_currentIndex] objectForKey:@"type"] integerValue]);

                _head.titledataArray.array  = self.epgChannelList;
                _head.MubiaoNum = _currentIndex;
                _head.dianjizhuangtai = NO;
                [_head updataTitle];

                //直播页的引导
                NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGGUIDE"];
                if (string.length == 0 && IS_EPG_CONFIGURED) {
                    
                    Guide = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
                    UIImage *image = [UIImage imageNamed:@"bg_first_geture"];
                    
                    Guide.image =  image;
                    Guide.userInteractionEnabled = YES;
                    
                    UIImageView *guideTap = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+4*kRateSize, kDeviceWidth, 110*kRateSize)];
                    UIImage *imageTap = [UIImage imageNamed:@"ic_first_tap_gesture"];
                    
                    guideTap.image = imageTap;
                    
                    [Guide addSubview:guideTap];
                    
                    UIButton_Block *block = [[UIButton_Block alloc]initWithFrame:Guide.frame];
                    [Guide addSubview:block];
                    block.Click = ^(UIButton_Block *btn,NSString *name){
                        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"EPGGUIDE"];
                        [Guide removeFromSuperview];
                    };
                    
                    if (HomeNewDisplayTypeOn){
                        [self.view  addSubview:Guide];
                    }
                    else
                    {
                        [self.tabBarController.view  addSubview:Guide];
                    }
                    
                }
                
                
                _ScrollTable.DataSource = self.epgChannelList;
                _headArray.array = self.epgChannelList;
                _ScrollTable.delegate = self;
                
                [_ScrollTable setupView];
                [_ScrollTable ChangePage:_currentIndex+1];
                
                
                [self show];
                
                
            }
            
            
            //所有直播Epg信息
            if ([type isEqualToString:@"allCategoryChannelList"]) {
                                NSLog(@"%@",result);
                channelList.array = [result objectForKey:@"channelList"];
                for (NSDictionary * diction  in _headArray ) {
                    NSString *type = [diction objectForKey:@"type"];

                    NSMutableArray *array = [self MyEpgGetSingleCategory:channelList andType:type];

                    
                    NSMutableDictionary  *dic = [NSMutableDictionary dictionary];
                    
                    [_scrollDic setObject:array forKey:type];
                    [dic setObject:array forKey:@"List"];
                    [_ScollArray addObject:dic];
                    [self hide];
                }
                
                
                _ScrollTable.ALLCategoryDiction = _scrollDic;
                UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                if (Tab) {
                    
                    [Tab reloadData];
                    
                }
                
                [_ScrollTable ChangePage:_currentIndex+1];
                
            }
            
            
            
            
            if ([type isEqualToString:@"typeCategoryChannelList"]) {
                if (!_ScollArray.count) {
                    channelList.array = [result objectForKey:@"channelList"];
                    for (NSDictionary * diction  in _headArray ) {
                        NSString *type = [diction objectForKey:@"type"];

                        NSMutableArray *array = [self MyEpgGetSingleCategory:channelList andType:type];

                        
                        NSMutableDictionary  *dic = [NSMutableDictionary dictionary];
                        
                        [_scrollDic setObject:array forKey:type];
                        [dic setObject:array forKey:@"List"];
                        [_ScollArray addObject:dic];
                        [self hide];
                    }
                    
                    NSLog(@"allCategoryChannelList后的数组--------%@",_ScollArray);
                    _ScrollTable.ALLCategoryDiction = _scrollDic;
                    UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                    if (Tab) {
                        
                        [Tab reloadData];
                        
                    }
                    
                    [_ScrollTable ChangePage:_currentIndex+1];
                }else
                {
                    channelList.array = [result objectForKey:@"channelList"];
                    
                    NSDictionary *dic = [_headArray objectAtIndex: _currentIndex];
                    NSString *leixing = [dic objectForKey:@"type"];
                    NSMutableArray *array = [self MyEpgGetSingleCategory:channelList andType:leixing];

                    NSMutableDictionary  *dicccc = [NSMutableDictionary dictionary];
                    
                    
                    
                    [_scrollDic setObject:array forKey:type];

                    [dicccc setObject:array forKey:@"List"];
                    
                    
                    if (_ScollArray.count >= _currentIndex && _ScollArray.count != 0) {
                        [_ScollArray replaceObjectAtIndex:_currentIndex withObject:dicccc];
                    }
                    NSLog(@"typeCategoryChannelList--打印array－－－－－－%@",_ScollArray);
                    [self hide];
                    UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                    if (Tab) {
                        [Tab.mj_header endRefreshing];
                        [Tab reloadData];
                    }
                    [_ScrollTable ChangePage:_currentIndex+1];
                }
            }
            
            
            
            
            /**
             *  直播搜索结果划分成时间段的格式，周一周二
             */
            if ([type isEqualToString:EPG_EVENTSEARCH]) {
                NSLog(@"搜索结果%@",result);
                NSArray *array =  [Tool dateTypeWith:result];
                NSLog(@"%@",array);
            }
            
            
            
            //顶部的搜索文字
            if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
                NSLog(@"热门记录: %ld result:%@  type:%@",status,result,type );
                
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                
                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时搜索热词记录");

                    _searchBarView.placeholderLab.text = @"";

                    HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
                    [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSLog(@"名字是：： %@",name);
                        
                        _searchBarView.placeholderLab.text = [NSString stringWithFormat:@"%@",name];
                        
                    }
                }
                
            }
            
        }
            break;
            
        default:
        {
            
            if (IS_EPG_CONFIGURED == YES) {
                loadDateResult = NO;
                
                NSLog(@"-=_currentIndex当前页数%ld",(long)_currentIndex);
                
                if ([type isEqualToString:EPG_TYPELIST] && _head.titledataArray.count == 0) {
                    
                    if (!_requestChannelBtn.hidden) {
                        
                        _requestChannelBtn.hidden = YES;
                        
                    }
                    
                    if (!_requestEPGBtn && !_requestEPGBtn.hidden) {
                        _requestEPGBtn = [[UIButton alloc]init];
                        [_requestEPGBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
                        [_requestEPGBtn setBackgroundColor:App_background_color];
                        [self.view addSubview:_requestEPGBtn];
                        [_requestEPGBtn addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
                        [_requestEPGBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.view.mas_left);
                            make.right.equalTo(self.view.mas_right);
//                            make.top.equalTo(_seachTextView.mas_bottom).offset(7*kDeviceRate);
                            
                            make.top.equalTo(_searchBarView.mas_bottom).offset(7*kDeviceRate);
                            make.bottom.equalTo(self.view.mas_bottom);
                        }];
                    }
                    else
                    {
                        _requestEPGBtn.hidden = NO;
                    }
                    
                    
                }
                else if(([type isEqualToString:EPG_CHANNELLIST]||[type isEqualToString:EPG_CURRENTEVENTLIST]) && (_requestEPGBtn.hidden || !_requestEPGBtn)){
                    
                    NSMutableArray *tempArray = [NSMutableArray array];
                    if (_ScollArray.count != 0) {
                        tempArray = [[_ScollArray objectAtIndex:_currentIndex]valueForKey:@"List"];
                    }
                    
                    NSLog(@"%lu", (unsigned long)tempArray.count);
                    if ((unsigned long)tempArray.count <= 0 && !loadDateResult){
                        
                        if (!_requestChannelBtn) {
                            _requestChannelBtn = [[UIButton alloc]init];
                            [_requestChannelBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
                            [_requestChannelBtn setBackgroundColor:App_background_color];
                            [self.view addSubview:_requestChannelBtn];
                            [_requestChannelBtn addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
                            [_requestChannelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                                make.left.equalTo(self.view.mas_left);
                                make.right.equalTo(self.view.mas_right);
                                make.top.equalTo(_head.mas_bottom).offset(-2*kRateSize);
                                make.bottom.equalTo(self.view.mas_bottom);
                            }];
                        }
                        else
                        {
                            _requestChannelBtn.hidden = NO;
                            [self.view bringSubviewToFront:_requestChannelBtn];
                        }
                    }
                    
                    UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                    if (Tab) {
                        [Tab.mj_header endRefreshing];
                    }
                    
                }
                else
                {
                    UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                    if (Tab) {
                        [Tab.mj_header endRefreshing];
                        [Tab reloadData];
                    }
                }
            }

        }
        break;
    }
    
    if ([type isEqualToString:@"typeCategoryChannelList"]) {
        UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
        if (Tab) {
            [Tab.mj_header endRefreshing];
            [Tab reloadData];
        }
    }
}
#pragma mark - 滚动视图的代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if (_ScollArray.count > _currentIndex) {
        NSDictionary *dic = [_ScollArray objectAtIndex:_currentIndex];
        NSArray *array = [dic objectForKey:@"List"];
        count = array.count;
    }
    
    return count;
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *scollIDENT  = @"scrollidentifier";
    LiveHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:scollIDENT];
    if (!cell) {
        cell = [[LiveHomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:scollIDENT];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    NSArray *array;//数据源数组
    if (_ScollArray.count > _currentIndex) {
        NSDictionary *dic = [_ScollArray objectAtIndex:_currentIndex];
        array = [dic objectForKey:@"List"];
    }
    if (array.count > indexPath.row) {
        NSDictionary *jiemuXunXi = [array objectAtIndex:indexPath.row];
        //    cell.textLabel.text =[NSString stringWithFormat:@"----%d",tableView.tag];
        
        [cell updateCellWithDiction:jiemuXunXi];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = UIColorFromRGB(0xffffff);
        cell.Block = ^(UIButton_Block* btn)
    {
        NSLog(@"点击的%@",btn.Diction);
        
        
        NSDictionary *diction = btn.Diction;
        EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
        epg.currentPlayType = CurrentPlayLive;
        epg.serviceID = [diction objectForKey:@"categoryServiceID"];
        epg.serviceName = [diction objectForKey:@"categoryServiceName"];
        epg.currentPlayType = CurrentPlayLive;
        epg.clickCategoryTag = _currentIndex+2;
        [self.navigationController pushViewController:epg animated:YES];
        NSLog(@"=====");
        
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70*kDeviceRate;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //空数据提示页
    
    _HTVw =[[UIView alloc]init];
    _HTVw.backgroundColor = App_background_color;
    
    //空数据页提示信息
    _HTLb = [[UILabel alloc]init];
    _HTLb.text = @"暂无数据";
    _HTLb.textAlignment = NSTextAlignmentCenter;
    
    [_HTVw addSubview:_HTLb];
    
    [_HTLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
    }];
    
    _HTImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];
    [_HTVw addSubview:_HTImg];
    
    [_HTImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
        make.height.equalTo(@(240*kDeviceRate));
        make.width.equalTo(@(180*kDeviceRate));
        
    }];
    
    _HTImg.hidden = NO;
    _HTLb.hidden = NO;
    
    NSInteger count = 0;
    
    if (_ScollArray.count > _currentIndex) {
        NSDictionary *dic = [_ScollArray objectAtIndex:_currentIndex];
        NSArray *array = [dic objectForKey:@"List"];
        count = array.count;
    }
    
    if (count) {
        return nil;
    }
    
    return _HTVw;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if (_ScollArray.count > _currentIndex) {
        NSDictionary *dic = [_ScollArray objectAtIndex:_currentIndex];
        NSArray *array = [dic objectForKey:@"List"];
        count = array.count;
    }
    
    if (count) {
        return 0;
    }
    
    return (KDeviceHeight - 138*kDeviceRate);
}



//获取所有分类中的某个频道匪类下的节目        所有节目信息[_request EPGGetLivePageChannelInfo];                     高清类别
-(NSMutableArray *)EpgGetSingleCategory:(NSMutableArray *)categoryArr andType:(NSString*)type
{
    NSMutableArray *lastArr = [[NSMutableArray alloc]init];
    
    
    for (int i = 0; i<categoryArr.count; i++) {
        
        if ([type isEqualToString:[[categoryArr objectAtIndex:i] valueForKey:@"type"]]) {
            
            for (int j = 0; j<[[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"] count]; j++) {
                
                NSMutableDictionary *lastDic = [[NSMutableDictionary alloc]init];
                
                NSDictionary *dic = [[NSDictionary alloc]init];
                dic =  [[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"] objectAtIndex:j];
                
                [lastDic setValue:[dic valueForKey:@"name"] forKey:@"categoryServiceName"];
                [lastDic setValue:[dic valueForKey:@"id"] forKey:@"categoryServiceID"];
                [lastDic setValue:[dic valueForKey:@"imageLink"] forKey:@"categoryLevelImage"];
                [lastDic setValue:[[[[dic valueForKey:@"epglist"] valueForKey:@"epginfo"] objectAtIndex:0] valueForKey:@"eventName"] forKey:@"categoryServiceEventName"];
                
                
                [lastArr addObject:lastDic];
                
            }
            
            
        }
        
        
    }
    
    
    return lastArr;
    
}

-(NSMutableArray *)MyEpgGetSingleCategory:(NSMutableArray *)categoryArr andType:(NSString*)type
{
    NSMutableArray *lastArr = [[NSMutableArray alloc]init];
    
    
    for (int i = 0; i<categoryArr.count; i++) {
        NSString *Datatype = [[categoryArr objectAtIndex:i] valueForKey:@"type"];
        if ([type isEqualToString:Datatype]) {
            
            NSDictionary *ProgramDic = [categoryArr objectAtIndex:i];
            NSArray *DataserviceinfoArray = [ProgramDic valueForKey:@"serviceinfo"];
            
            for (int j = 0; j<[DataserviceinfoArray count]; j++) {
                
                NSMutableDictionary *lastDic = [[NSMutableDictionary alloc]init];
                
                NSDictionary *dic =  [DataserviceinfoArray objectAtIndex:j];
                
                [lastDic setValue:[dic valueForKey:@"name"] forKey:@"categoryServiceName"];
                [lastDic setValue:[dic valueForKey:@"id"] forKey:@"categoryServiceID"];
                [lastDic setValue:[dic valueForKey:@"imageLink"] forKey:@"categoryLevelImage"];
                [lastDic setValue:[[[[dic valueForKey:@"epglist"] valueForKey:@"epginfo"] objectAtIndex:0] valueForKey:@"eventName"] forKey:@"categoryServiceEventName"];
                [lastDic setValue:[[dic objectForKey:@"epglist"] objectForKey:@"epginfo"] forKey:@"epginfo"];
                
                [lastArr addObject:lastDic];
                
            }
            
            
        }
        
        
    }
    
    
    return lastArr;
    
}




/**
 *  获得到周一周二周三这个类型数据，从搜索结果中转化
 *
 *  @param array 搜索结果页
 *
 *  @return 格式化好的结果
 */
- (NSMutableDictionary*)dateTypeWithArray:(NSArray*)array
{
    NSMutableDictionary *ServiceIDiction = [NSMutableDictionary dictionary];
    NSMutableDictionary *anserDiction = [NSMutableDictionary dictionary];
    
    for (NSDictionary *epgdic in array) {
        
        NSString *serviceID = [epgdic objectForKey:@"serviceID"];
        
        NSArray *epginfo = [epgdic objectForKey:@"epginfo"];
        
        [ServiceIDiction setObject:@"" forKey:serviceID];
        
        for (NSDictionary *ProgramOfEPG in epginfo) {
            
            NSString *startTime = [ProgramOfEPG objectForKey:@"startTime"];
            
            startTime = [self YearMouthDayWithString:startTime];
            
            if ([anserDiction objectForKey:startTime]) {
                NSMutableDictionary *JiemuXinxi = [anserDiction objectForKey:startTime];
                if ([JiemuXinxi objectForKey:serviceID]) {
                    NSMutableArray *timeSerIDEPGArray = [JiemuXinxi objectForKey:serviceID];
                    [timeSerIDEPGArray addObject:ProgramOfEPG];
                }else
                {
                    NSMutableArray *timeSerIDEPGArrayNew = [NSMutableArray array];
                    [timeSerIDEPGArrayNew addObject:ProgramOfEPG];
                    [JiemuXinxi setObject:timeSerIDEPGArrayNew forKey:serviceID];
                    
                }
            }else
            {
                NSMutableDictionary *ProgramDic = [NSMutableDictionary dictionary];
                NSMutableArray *TimeServiceIDArrray = [NSMutableArray array];
                [TimeServiceIDArrray addObject:ProgramOfEPG];
                [ProgramDic setObject:TimeServiceIDArrray forKey:serviceID];
                [anserDiction setObject:ProgramDic forKey:startTime];
                
            }
            
            
        }
        
    }
    
    [anserDiction setObject:ServiceIDiction forKey:@"ALLServiceID"];
    return anserDiction;
    
}
//时间转化展示方式
- (NSString*)YearMouthDayWithString:(NSString*)string
{
    NSDateFormatter *fm = [[NSDateFormatter alloc]init];
    fm.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    NSDate *time = [fm dateFromString:string];
    fm.dateFormat = @"YYYY-MM-dd";
    NSString *StyleDate = [fm stringFromDate:time];
    return StyleDate;
}




- (void)setSeachBar
{
    WS(wself);
    _searchBarView = [[CustomSearchBarView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth-90, 28)];
    _searchBarView.center = CGPointMake(self.m_viewNaviBar.centerX, self.m_viewNaviBar.centerY + 15 + kTopSlimSafeSpace/2);
    [self.m_viewNaviBar addSubview:_searchBarView];
    _searchBarView.searchBlock = ^()
    {
        SearchViewController *search = [[SearchViewController alloc]init];
        [wself.navigationController pushViewController:search animated:YES];
    };
    
    if (!HomeNewDisplayTypeOn) {
        [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 27, kDeviceWidth-30*2*kRateSize, 28)];
    }else{
        [self setNaviBarSearchViewFrame:CGRectMake(40*kRateSize, 27, kDeviceWidth-30*2*kRateSize-10*kRateSize, 28)];
    }
}

- (void)publicSetup:(BOOL)public{
    if (public) {
        
        _head.hidden = YES;
        
        _VerticalLine.hidden = YES;
        
        _ScrollTable.hidden = YES;
        
        _promptView.hidden = NO;
        
//        _topSearchView.hidden = YES;
        
        _searchBarView.hidden = YES;
        return;
        
    }
    else
    {
        _VerticalLine.hidden = NO;
        
        _head.hidden = NO;
        
        _ScrollTable.hidden = NO;
        
        _promptView.hidden = YES;
        
//        _topSearchView.hidden = NO;
        
        _searchBarView.hidden = NO;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
