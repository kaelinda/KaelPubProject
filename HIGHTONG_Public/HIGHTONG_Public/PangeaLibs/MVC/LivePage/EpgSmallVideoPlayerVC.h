//
//  EpgSmallVideoPlayerVC.h
//  HIGHTONG_Public
//
//  Created by 赖利波 on 15/9/22.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "VideoInfo.h"
#import "HTRequest.h"
#import "EPGDateSelectedView.h"
#import "EPGListTableViewCell.h"
#import "RemoteControlView.h"
#import "ShareView.h"
//#import "UMSocial.h"
#import <UMSocialCore/UMSocialCore.h>
#import "MyGesture.h"
#import "MBProgressHUD.h"
#import "DateTools.h"
#import "CustomAlertView.h"
#import "SAIInformationManager.h"
#import <objc/runtime.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "PlayerMaskView.h"
#import "ErrorMaskView.h"
#import <AVFoundation/AVFoundation.h>
#import "TVADMaskView.h"
#import "DemandADDetailViewController.h"
#import "PolyMedicineLoadingView.h"
#import "BlackInstallView.h"
#import "HTTimerManager.h"
#import "EPGCollectionModel.h"
#import "SystermTimeControl.h"
#import "LocalVideoModel.h"
#import "LocalCollectAndPlayRecorder.h"
@interface EpgSmallVideoPlayerVC : HT_FatherViewController<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,UIGestureRecognizerDelegate,epgdateSelectedViewDelegate,EPGDateBtnDelegate,EPGListCellDelegate,RemoteControlViewDelegate,shareViewDelegate,MBProgressHUDDelegate,CustomAlertViewDelegate,HTTimerManagerDelegate>
{
//    HTRequest *request;
    BOOL OnMove;

    BOOL _isTapbtn_play;//必须点击了，才能选装才能播放
    BOOL _ischangDate;
    BOOL _isEPGPlayerEnter;
    BOOL _isForwardBackground;
//    BOOL _isLocationOrder;
    NSString *_allCodeRateLink;//记录多有码率的所有链接  每次鉴权时候清空
//    VideoInfo *_theVideoInfo;
    NSInteger PlayTVType;
    NSString *_playingEventID;
    BOOL _isPlayFirstVideo;         // 是否播放的是当天的第一个视频
    BOOL _isPlayLastVideo;          // 是否播放的是当天的最后一个视频
    NSInteger _liveTVPosition;      // 直播视频的位置
    
    NSInteger _videoPlayPosition;   // 记录当前播放的视频的位置
    BOOL _isPrepareToPlay;
    NSInteger _channelSelectIndex;
    NSInteger _videolistSelectIndex;
    UIButton *_clearButton;         // 清晰度按钮
    
//    NSString *_clickCategoryTag;//点击分类的哪一个
    NSString *_currentServiceEventStr;

    NSMutableArray *clearTitles;
    BOOL _isClickedClear;           // 是否点击过清晰度的按钮

    NSString *_currentPlayID;//当前播放的节目ID
    NSString *_currentPlayServiceID;
    NSMutableArray *_videoListArray;  // 记录当天视频信息
    NSMutableArray *_resultTBVideoListArr;//专门用来更新resuletableview的数组，为了区分_videoListArray
    NSMutableArray *_allCategoryEPGServiceEventArr;
    NSMutableArray *_dicArr;
    NSMutableArray *_favourChannelArr;
     // 清晰度按钮
    
    
    BOOL _isScreenLocked;//判断屏幕是否枷锁的标记。

    UILabel *_currentTimeL;         // 当前时间
    UILabel *_slantlineL;
    
    UILabel *_totalTimeL;           // 总时间
    
    UILabel *_dragLabel;
    
    CGFloat offset_x;
    CGFloat offset_y;
    
    NSString *liveStartTime;
    NSString *liveEndTime;
    
    CGFloat liveTimeInterVal;
    
    NSString *nowTimeString;
    
    NSTimer *_livePlayerTimer;
    
    UIView *remoteControlBackV;
    UIView *pushBackV;
    UIView *pullBackV;
    
    UIImageView *remoteLeftV;
    UIImageView *pushLeftV;
    UIImageView *pullLeftV;
    
    UILabel *remoteLabel;
    UILabel *pushLabel;
    UILabel *pullLabel;
    
    UIImageView *remoteDownLine;
    UIImageView *pushDownLine;
    UIImageView *pullDownLine;

    
    
    
    UIButton *_RemoteButton;
    UIButton *_TVButton;
    UIButton *_MTButton;
    
    CGFloat ZTTimeIntervalLive;
    
    BOOL _isPlayEnd;
    
    UIView *_unFavourBV;
    UILabel *_noFavorTextLabel;
    UIImageView *_noFavorImageView;
    BOOL _isLookPlayBackEvent;
    
    PlayerMaskView *_playerLoginView;
    ErrorMaskView *_errorMaskView;
    PlayerMaskView *_installMaskView;
    
}
@property (nonatomic,strong)BlackInstallView *blackInstallView;
@property (nonatomic,strong)PolyMedicineLoadingView *polyLoadingView;
@property (nonatomic,assign)NSInteger clickCategoryTag;
@property (nonatomic,strong)NSString *clickCategoryString;
@property (nonatomic,assign)float isSlineceValue;
@property (nonatomic,assign)BOOL isVolumeSlinece;
@property (nonatomic,strong)TVADMaskView *adPlayerView;
@property (nonatomic,assign)TVADMaskViewType ADPlayType;
@property (nonatomic,assign)BOOL isHaveAD;
@property (assign, atomic) NSInteger  adImageHiddenSecend;
@property (nonatomic,strong)NSString *adImageURL;
@property (nonatomic,strong)NSString *adVideoURL;
@property (nonatomic,strong)NSString *adImageDetailURL;
@property (nonatomic,strong)NSString *adVideoDetailURL;
@property (nonatomic,assign)BOOL isADDetailEnter;
@property (assign, nonatomic) NSInteger  adVideoHiddenSecend;

@property (nonatomic,assign)NSInteger JumTimeMoveStart;
@property (nonatomic,assign)BOOL IsFirstChannelist;
@property (nonatomic,assign)BOOL isVODEX;
@property (nonatomic,assign)BOOL isLocationOrder;
@property (nonatomic,assign)BOOL isEnterClick;
@property (nonatomic,assign)BOOL ISOrderPlay;
@property (nonatomic,strong)NSString *LocalServiceID;
@property (nonatomic,assign)BOOL isLocationOrderVOD;
@property (strong,nonatomic)  NSString      *nowDateString;//采集  获取时间的字符串
@property (retain,nonatomic)  NSDate        * nowDate;//采集  获取服务器现在的时间
@property (retain,nonatomic)  NSString      *epgDateStr;   //采集   覆盖EPG区间
@property (retain,nonatomic)  NSString      * skimDuration;//采集   浏览的时长
@property (nonatomic,strong)  NSString      *EpgCollectionDuration;
@property (strong,nonatomic)  NSDate *EPGNowDate;//采集  获取时移的服务器的现在的时刻
@property (strong,nonatomic)  NSString *EPGDateString;//采集  获取时间的字符串
@property (strong,nonatomic)  NSString *epgCollectionModel;
@property (nonatomic,copy)VideoInfo *theVideoInfo;
@property (nonatomic,copy)NSURL *eventURL;//3.0新增属性，进小屏开始都不播放，用这个来区分过来的时候是点的event还是tv，这俩个有一个必需是nil；
@property (nonatomic,copy)NSString *eventURLID;//3.0新增属性，进小屏开始都不播放，用这个来区分过来的时候是点的event还是tv，这俩个有一个必需是nil；
@property (nonatomic,strong)NSString *eventName;
@property (nonatomic,copy)NSString *eventStartTime;//节目开始时间
@property (nonatomic,copy)NSString *eventEndTime;//节目结束时间


@property (nonatomic,copy)NSString *eventBreakPoint;//3.0新增属性，进小屏开始都不播放，用这个来区分过来的时候是点的event还是tv，这俩个有一个必需是nil；
@property (nonatomic,assign) NSInteger MoveTimeInteger;
@property (strong,nonatomic) UIImageView *IPBackIV;//ip背景图
@property (strong,nonatomic) UITableView *IPTableView;//推拉IP的tableView
@property (strong,nonatomic) UIImageView *IPSeperateLineView;//推拉IP的取消按钮的上边的那条线
@property (strong,nonatomic) UIButton *IPButton;//推拉IP的取消按钮
@property (strong,nonatomic) UIImageView *backEventTagIV;//回看标识的那个IV

@property (strong,nonatomic) NSMutableArray *IPArray;//存放IP数组的

@property (nonatomic,strong)UIImageView *screenLockedIV;//屏幕枷锁view

@property (assign,nonatomic) CurrentPLAYType currentPlayType;
@property (strong,nonatomic) NSMutableArray *categoryArr;
@property (strong,nonatomic) NSMutableArray * weekdays;


@property (strong,nonatomic) UILabel *serviceTitleLabel;
@property (strong,nonatomic) UILabel *currentTitleLabel;

@property (strong,nonatomic) NSString *serviceTitleStr;


@property (strong,nonatomic) NSString *currentLiveVideoTitleStr;

@property (strong,nonatomic) NSString *nextLiveVideoTitleStr;


@property (strong,nonatomic) NSString *currentDateStr;

@property (copy ,nonatomic)NSString * fullStartTimeStr;//年月日。不带时分秒

//选台TableView
@property (strong,nonatomic) UITableView *videoListTV;

//节目单TableView
@property (strong,nonatomic) UITableView *channelListTV;

@property (strong,nonatomic) NSMutableArray *videoListArr;

@property (strong,nonatomic) NSMutableArray *channelListArr;
@property (strong,nonatomic)NSMutableArray *netScheduledListArr;//网络获取的预约数据
@property (strong,nonatomic)MyGesture *SelectdscheduleD;//登陆用户，点击预定按钮以后要在回调中做按钮状态以及数据源的改变。用这个btn。tag记录数组中位置，用于清除
@property (nonatomic,strong)NSString *serviceID;
@property (nonatomic,strong)NSString *serviceName;
@property (nonatomic,strong)UIView *backgroundView;
@property (nonatomic,strong)UIImageView *topBarIV;
@property (nonatomic,strong)UIImageView *bottomBarIV;
@property (nonatomic,strong)UIButton*returnBtn;
@property (nonatomic,strong)UISlider*videoProgressV;
@property (nonatomic,strong)UIImageView*returnBtnIV;
@property (nonatomic,strong)UIButton*returnMainBtn;
@property (strong,nonatomic)  NSTimer *changeVideoNameTimer;//直播时候更改当前视频信息并再次获取epg_show

@property (strong, nonatomic) MPMoviePlayerController * moviePlayer;
@property (strong,nonatomic)MPMusicPlayerController * musicPlayerController;
@property (strong,nonatomic) UIView *contentView;
@property (strong,nonatomic) UIView *tapView;
@property (strong, nonatomic) NSTimer * autoHiddenTimer;
@property (assign, atomic) int  autoHiddenSecend;
@property (nonatomic,assign)GestureType gestureType;
@property (nonatomic,strong)UIProgressView *brightnessProgress;
@property (nonatomic,assign)CGPoint originalLocation;
@property (nonatomic,assign)CGFloat systemBrightness;
@property (nonatomic,strong)UIProgressView *volumProgress;
@property (nonatomic,strong)UIImageView *brightnessView;
@property (nonatomic,strong)UIImageView *progressTimeView;
@property (nonatomic,strong)UIImageView *progressDirectionIV;
@property (nonatomic,strong)UILabel *progressTimeLable_top;
@property (nonatomic,strong)UILabel *progressTimeLable_bottom;
@property (nonatomic,assign)CGFloat ProgressBeginToMove;
@property (nonatomic,assign)NSInteger JumMoveStart;
@property(nonatomic,strong)UIPanGestureRecognizer   * panGesture;//增加手势
@property(nonatomic,assign)PanDirection               panDirection; // 定义一个实例变量，保存枚举值


@property(nonatomic,assign)BOOL isVerticalDirection;
@property (retain,nonatomic)  NSTimer *myNewTimer;//新的url更新进度定时器
@property (strong,nonatomic) UIImageView *playBackgroundIV;
@property (nonatomic,strong) UIButton *playBackgroundIVBtn;//小屏幕上面初始化下的中间大播放按钮。
@property (nonatomic,strong) UIButton *playPauseBigBtn;//大屏幕上面暂停中间大播放按钮。
@property (nonatomic, copy) NSString *currentURL;//极为重要的属性，传个大屏播放器用
@property (strong,nonatomic)  NSTimer *getPlayBackRecordListRepitTimer;//回看时重置断点时间
@property (assign, nonatomic) BOOL isPlay;

@property (nonatomic,assign) EPG_DateType dateType;
//是否是爱上TV的逻辑，比如登录和绑定

@property (nonatomic,assign) APPMaskType *appMaskType;
@property (nonatomic,assign) BOOL  isEnterBind;

/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;

/**
 新增的一个记录本地Video的Model
 */
@property (nonatomic,copy) LocalVideoModel *localVideoModel;

@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;
-(void)setSmallScreenPlayer;
-(void)setBigScreenPlayer;


@end
