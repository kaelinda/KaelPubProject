//
//  LiveNewPageController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/10/17.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "LiveNewPageController.h"
#import "LiveHomeTableViewCell.h"
#import "EpgSmallVideoPlayerVC.h"

#import "KDIYRefreshHeader.h"

@interface LiveNewPageController ()
{
    
    UIView *_HTVw;
    
    UILabel *_HTLb;
    
    UIImageView *_HTImg;
    
    UIButton *_requestChannelBtn;//channel再次请求按钮
 
}
@end

@implementation LiveNewPageController

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self show];
    
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        _isNeedNavBar = NO;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideNaviBar:!_isNeedNavBar];

    [self setupTableView];
    
    [self reloadChannelDataWith:self.livePageID];
    
    // Do any additional setup after loading the view.
}

- (void)setupTableView
{
    if (!_contentTableView) {
        
        if ([APP_ID isEqualToString:@"562172033"]) {
            
            _contentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 40*kDeviceRate - 64-kTopSlimSafeSpace-kBottomSafeSpace)];
            
        }
        else
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            NSString *operatorCode = [userDefaults objectForKey:@"operatorCode"];
            
            if (operatorCode.length == 0) {
                _contentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 40*kDeviceRate - 64-kTopSlimSafeSpace-kBottomSafeSpace)];
            }
            else
            {
                
                _contentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 80*kDeviceRate - 64-kTopSlimSafeSpace-kBottomSafeSpace)];
                
            }
            
        }
        
        _contentTableView.dataSource = self;
        _contentTableView.delegate = self;
        _contentTableView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        
        KDIYRefreshHeader *diyHeader = [KDIYRefreshHeader headerWithRefreshingBlock:^{
            
            [self reloadChannelDataWith:self.livePageID];
            
        }];
        
        _contentTableView.mj_header = diyHeader;

        [self.view addSubview:_contentTableView];
        
    }
}

#pragma mark - 滚动视图的代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _childrenArray.count;
    
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *scollIDENT  = @"scrollidentifier";
    LiveHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:scollIDENT];
    if (!cell) {
        cell = [[LiveHomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:scollIDENT];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    

    if (_childrenArray.count > indexPath.row) {
        NSDictionary *jiemuXunXi = [_childrenArray objectAtIndex:indexPath.row];
        //    cell.textLabel.text =[NSString stringWithFormat:@"----%d",tableView.tag];

        [cell updateCellWithDiction:jiemuXunXi];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = UIColorFromRGB(0xffffff);
    cell.Block = ^(UIButton_Block* btn)
    {
        NSLog(@"点击的%@",btn.Diction);
        
        
        NSDictionary *diction = btn.Diction;
        EpgSmallVideoPlayerVC *epg = [[EpgSmallVideoPlayerVC alloc]init];
        epg.TRACKID = self.TRACKID;
        epg.TRACKNAME = self.TRACKNAME;
        epg.EN = self.EN;
        epg.currentPlayType = CurrentPlayLive;
        epg.serviceID = [diction objectForKey:@"categoryServiceID"];
        epg.serviceName = [diction objectForKey:@"categoryServiceName"];
        epg.currentPlayType = CurrentPlayLive;
        epg.clickCategoryTag = _currentIndex+2;
        
        [self.navigationController pushViewController:epg animated:YES];
        NSLog(@"=====");
        
    };
    
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70*kDeviceRate;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //空数据提示页
    
    _HTVw =[[UIView alloc]init];
    _HTVw.backgroundColor = App_background_color;
    
    //空数据页提示信息
    _HTLb = [[UILabel alloc]init];
    _HTLb.text = @"暂无数据";
    _HTLb.textAlignment = NSTextAlignmentCenter;
    
    [_HTVw addSubview:_HTLb];
    
    [_HTLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
    }];
    
    _HTImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    _HTImg.backgroundColor = App_background_color;
    [_HTVw addSubview:_HTImg];
    
    [_HTImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
        make.height.equalTo(@(240*kDeviceRate));
        make.width.equalTo(@(180*kDeviceRate));
        
    }];
    
    _HTImg.hidden = NO;
    _HTLb.hidden = NO;
    
    NSInteger count = 0;
    
    count = _childrenArray.count;
    
    if (count) {
        return nil;
    }
    
    return _HTVw;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    count = _childrenArray.count;
    
    if (count) {
        return 0;
    }
    
    return (KDeviceHeight - 138*kDeviceRate);
}

#pragma mark - 代理方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    switch (status) {
        case 0:
            
            //直播Epg信息
            if ([type isEqualToString:@"typeCategoryChannelList"]) {
                
                NSMutableArray *tempArr = [result objectForKey:@"channelList"];
                
                //        NSLog(@"tempArr--------%@",tempArr);
                
                NSString *type = [[tempArr objectAtIndex:0] objectForKey:@"type"];
                
                _childrenArray = [self MyEpgGetSingleCategory:tempArr andType:type];
                
                
                //        NSLog(@"_childrenArray-------%@",_childrenArray);
                
                [self.contentTableView reloadData];
                
                [_contentTableView.mj_header endRefreshing];
                
                //如果 父控制器 想获取界面的数据 可以通过这个方法获取
                if (self.pageCahnnelInfo) {
                    self.pageCahnnelInfo(_childrenArray,self.livePageID);
                }
                
            }
            
            break;
            
        default:
            
            if([type isEqualToString:@"typeCategoryChannelList"]){
                
               
                if (_childrenArray.count <= 0){
                    
                    if (!_requestChannelBtn) {
                        _requestChannelBtn = [[UIButton alloc]init];
                        [_requestChannelBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
                        [_requestChannelBtn setBackgroundColor:App_background_color];
                        [self.view addSubview:_requestChannelBtn];
                        [_requestChannelBtn addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
                        [_requestChannelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.view.mas_left);
                            make.right.equalTo(self.view.mas_right);
                            make.top.equalTo(self.view.mas_top).offset(-2*kRateSize);
                            make.bottom.equalTo(self.view.mas_bottom);
                        }];
                    }
                    else
                    {
                        
                        _requestChannelBtn.hidden = NO;
                
                    }
                }
            }
            
            break;
    }
    
    [self hide];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadChannelDataWith:(NSString *)ChannelID{

    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
     [request EPGGetLivePageChannelInfoAndisType:[ChannelID integerValue]];
}

-(void)reloadContentViewDataWithChannelList:(NSArray *)ChannelList{
    
    _childrenArray = ChannelList.copy;
    
    [_contentTableView reloadData];
    
    
}

- (void)loadData
{
    
    //移除重新请求按钮
    if (_requestChannelBtn) {
        
        _requestChannelBtn.hidden = YES;
        
    }
    
    [self reloadChannelDataWith:self.livePageID];
    
}

-(NSMutableArray *)MyEpgGetSingleCategory:(NSMutableArray *)categoryArr andType:(NSString*)type
{
    NSMutableArray *lastArr = [[NSMutableArray alloc]init];
    
    
    for (int i = 0; i<categoryArr.count; i++) {
        NSString *Datatype = [[categoryArr objectAtIndex:i] valueForKey:@"type"];
        if ([type isEqualToString:Datatype]) {
            
            NSDictionary *ProgramDic = [categoryArr objectAtIndex:i];
            NSArray *DataserviceinfoArray = [ProgramDic valueForKey:@"serviceinfo"];
            
            for (int j = 0; j<[DataserviceinfoArray count]; j++) {
                
                NSMutableDictionary *lastDic = [[NSMutableDictionary alloc]init];
                
                NSDictionary *dic =  [DataserviceinfoArray objectAtIndex:j];
                
                [lastDic setValue:[dic valueForKey:@"name"] forKey:@"categoryServiceName"];
                [lastDic setValue:[dic valueForKey:@"id"] forKey:@"categoryServiceID"];
                [lastDic setValue:[dic valueForKey:@"imageLink"] forKey:@"categoryLevelImage"];
                [lastDic setValue:[[[[dic valueForKey:@"epglist"] valueForKey:@"epginfo"] objectAtIndex:0] valueForKey:@"eventName"] forKey:@"categoryServiceEventName"];
                [lastDic setValue:[[dic objectForKey:@"epglist"] objectForKey:@"epginfo"] forKey:@"epginfo"];
                
                [lastArr addObject:lastDic];
                
            }
            
        }
        
    }
    
    return lastArr;
    
}

#pragma mark - VTMagicReuseProtocol
- (void)vtm_prepareForReuse
{
    
    NSLog(@"clear old data if needed:%@", self.livePageID);
    
    _childrenArray = nil;
    
    [_contentTableView reloadData];
    
}


/**
 *  显示loading框
 */
- (void)show{
    
    _HTImg.hidden = NO;
    
    [self showCustomeHUD];
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hide) userInfo:nil repeats:NO];
   
}
/**
 *  隐藏loading框
 */
- (void)hide{

    _HTVw.hidden = NO;
    _HTImg.hidden = YES;
    [self hiddenCustomHUD];
}

@end
