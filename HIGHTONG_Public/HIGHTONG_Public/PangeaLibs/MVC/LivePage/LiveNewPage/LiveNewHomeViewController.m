//
//  LiveNewHomeViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/10/17.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "LiveNewHomeViewController.h"
#import "LiveNewPageController.h"
#import "Masonry.h"
#import "HTRequest.h"
#import "UIButton_Block.h"
#import "SearchViewController.h"
#import "NewSearchViewController.h"


@interface LiveNewHomeViewController ()<VTMagicViewDataSource, VTMagicViewDelegate,HTRequestDelegate>
{
    UILabel *_seachTextView;//搜索热词
    
    UIView *_topSearchView;//搜索View
    
    UIView *_QGVw;//全国版本运营商view
    
    UILabel *_QGoperatorName;//全国版本运营商名称
    
    UIImageView *_QGoperatorLogo;//全国版本运营商logo
    
    UIImageView *Guide ;//引导图
    
    UIButton *_requestEPGBtn;//epg再次请求按钮
    
    NSString *defaultSearchStr;
}

@property (nonatomic,copy)NSMutableArray *epgChannelList;//频道列表

@property (nonatomic, strong) NSMutableArray *menuNameList;//频道列表

@property (nonatomic, strong) NSMutableArray *cacheList;//缓存列表

@end

@implementation LiveNewHomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(HomeNewDisplayTypeOn){
        UIButton *Back = [[UIButton alloc]initWithFrame:CGRectMake(2, 18+kTopSlimSafeSpace, 44, 44)];
        Back.showsTouchWhenHighlighted = YES;
        [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
        [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateHighlighted];
        [Back addTarget:self action:@selector(Back) forControlEvents:UIControlEventTouchUpInside];
        [self.m_viewNaviBar addSubview:Back];
    }
    
    //    [self setNaviBarTitle:self.HealthVodViewTitle];
    [self setNaviBarTitle:@"健康"];
    
    defaultSearchStr = [[NSString alloc]init];
    
    _menuNameList = [NSMutableArray array];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.magicView.navigationColor = HT_COLOR_FONT_INVERSE;
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDefault;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 3*kDeviceRate;
    self.magicView.itemSpacing = 25*kDeviceRate;
    self.magicView.itemScale = 1.1;
    
    [self setSeachBar];//设置搜索栏
    
//    [self integrateComponents];//设置更多按钮
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *operatorCode = [userDefaults objectForKey:@"operatorCode"];
    
    if (![APP_ID isEqualToString:@"562172033"] && operatorCode.length != 0) {
        
        [self setupOperatorUI];//设置运营商展示
        
    }
    
    [self epgGuide];//直播引导页
    
    [self epgRequest];//请求频道列表
    
    [self requestVodRankByWeek];
    
}

- (void)requestVodRankByWeek
{
    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
    [_request PubCommonVodRankByWeekWithWeekly:@""];//请求热搜词
    
}

- (void)epgRequest
{
    
    //移除重新请求按钮
    if (_requestEPGBtn) {
        
        _requestEPGBtn.hidden = YES;
        
    }
    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
    [_request TypeListWithTimeout:10];//请求频道列表
    
}

- (void)epgGuide
{
    //直播页的引导
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGGUIDE"];
    if (string.length == 0 && IS_EPG_CONFIGURED) {
        
        Guide = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
        UIImage *image = [UIImage imageNamed:@"bg_first_geture"];
        
        Guide.image =  image;
        Guide.userInteractionEnabled = YES;
        
        UIImageView *guideTap = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+4*kRateSize+kTopSlimSafeSpace, kDeviceWidth, 110*kRateSize)];
        UIImage *imageTap = [UIImage imageNamed:@"ic_first_tap_gesture"];
        
        guideTap.image = imageTap;
        
        [Guide addSubview:guideTap];
        
        UIButton_Block *block = [[UIButton_Block alloc]initWithFrame:Guide.frame];
        [Guide addSubview:block];
        block.Click = ^(UIButton_Block *btn,NSString *name){
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"EPGGUIDE"];
            [Guide removeFromSuperview];
        };
        
        [self.view  addSubview:Guide];
        
    }
}


- (void)setSeachBar
{
    
    //设置搜索textField
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    _topSearchView =  [[UIView alloc ]initWithFrame:CGRectMake(0, kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    _topSearchView.backgroundColor = viewBgC;
    _topSearchView.layer.cornerRadius = 5*kDeviceRate;
    _topSearchView.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(32*kDeviceRate,6*kDeviceRate,260*kDeviceRate, 17*kDeviceRate)];
    
    _seachTextView.text = @"请输入您想要查找的关键字";
    _seachTextView.font = HT_FONT_THIRD ;
    _seachTextView.textColor = UIColorFromRGB(0x999999);
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    [_topSearchView addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]init];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [_topSearchView addSubview:searchview];
    
    [searchview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_topSearchView.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(_topSearchView.mas_top).offset(6*kDeviceRate);
        make.bottom.equalTo(_topSearchView.mas_bottom).offset(-6*kDeviceRate);
        make.width.equalTo(@(17*kDeviceRate));
    }];
    
    UIButton_Block *btn = [UIButton_Block new];
    [_topSearchView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_topSearchView);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
//        SearchViewController *search = [[SearchViewController alloc]init];
        NewSearchViewController *search = [[NewSearchViewController alloc]init];

        //        search.searchText = [array lastObject];;
        [self.navigationController pushViewController:search animated:YES];
        
    };
    
    _topSearchView.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:_topSearchView];
    if (!HomeNewDisplayTypeOn) {
        [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 25+kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    }else{
        [self setNaviBarSearchViewFrame:CGRectMake(40*kRateSize, 25+kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status------%ld,result------%@,type-----%@",(long)status,result,type);
    
    
    switch (status) {
        case 0:
            //得到分类的epg
            if ([type isEqualToString:EPG_TYPELIST]) {
                NSLog(@"EPG_TYPELIST----%@",result);
                
                self.cacheList = [NSMutableArray array];
                
                self.epgChannelList = [NSMutableArray array];
                
                self.epgChannelList = [result objectForKey:@"typeList"];
                
                NSMutableArray *tempArray = [NSMutableArray array];
                
                
                for (NSDictionary *dic in self.epgChannelList) {
                    
                    if ([dic objectForKey:@"name"]) {
                        
                        [_menuNameList addObject:[dic objectForKey:@"name"]];
                        
                    }
                    
                    if ([dic objectForKey:@"id"]) {
                        
                        [tempArray addObject:[dic objectForKey:@"id"]];
                        
                    }
                    
                }
                
                [self.magicView reloadData];
                
                for(id obj in tempArray){
                    
                    NSLog(@"%@",obj);
                    
                    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:obj,@"id", nil];
                    
                    [_cacheList addObject:paramDic];
                    
                }
                
                NSLog(@"_cacheList---------%@",_cacheList);
                
            }
            
            //得到热搜词
            if ([type isEqualToString:PUB_COMMON_VODRANKBYWEEK]) {
                NSLog(@"PUB_COMMON_VODRANKBYWEEK----%@",result);
                
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"dataList"];
                NSInteger numofList = [List count];
                
               
                if(numofList)
                {
                    
                    NSArray *useArr = [self dateSequenceWithsortArr:List andKeyName:@"nums"];
                    
                    defaultSearchStr = [[useArr objectAtIndex:0]objectForKey:@"groupName"];
                    
                    if (defaultSearchStr.length) {
                        _seachTextView.text = defaultSearchStr;
                    }
                }
                
            }

            break;
            
        default:
            if([type isEqualToString:EPG_TYPELIST] && _menuNameList.count == 0)
            {                
                
                if (!_requestEPGBtn && !_requestEPGBtn.hidden) {
                    _requestEPGBtn = [[UIButton alloc]init];
                    [_requestEPGBtn setImage:[UIImage imageNamed:@"bg_retry_gesture"] forState:UIControlStateNormal];
                    [_requestEPGBtn setBackgroundColor:App_background_color];
                    [self.view addSubview:_requestEPGBtn];
                    [_requestEPGBtn addTarget:self action:@selector(epgRequest) forControlEvents:UIControlEventTouchUpInside];
                    [_requestEPGBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.view.mas_left);
                        make.right.equalTo(self.view.mas_right);
                        make.top.equalTo(_seachTextView.mas_bottom).offset(7*kDeviceRate);
                        make.bottom.equalTo(self.view.mas_bottom);
                    }];
                }
                else
                {
                    _requestEPGBtn.hidden = NO;
                }
                
            }
        
            break;
    }
    
    
}



#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    return _menuNameList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        menuItem.titleLabel.font = HT_FONT_SECOND;
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    
    static NSString *gridIdd = @"grid.Identifier";
    LiveNewPageController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
    if (!viewController) {
        viewController = [[LiveNewPageController alloc] init];
        
        viewController.pageCahnnelInfo = ^(NSMutableArray *childrenArray,NSString *pageChannelID){
            
  
            NSLog(@"pageChannelID--->>:%@",pageChannelID);
            
            for (NSMutableDictionary *dic in _cacheList) {
                if ([[dic objectForKey:@"id"]isEqualToString:pageChannelID]) {
                    
                    [dic setObject:childrenArray forKey:@"channelInfoDic"];
                }
            }
        };
    }
    
    viewController.isNeedNavBar = NO;
    viewController.livePageID = [[self.epgChannelList objectAtIndex:pageIndex] objectForKey:@"id"];
    viewController.livePageName = [[self.epgChannelList objectAtIndex:pageIndex] objectForKey:@"name"];
    viewController.currentIndex = [[[self.epgChannelList objectAtIndex:pageIndex] objectForKey:@"id"] integerValue];
    viewController.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,viewController.livePageID];
    viewController.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,viewController.livePageName];
    viewController.EN = [NSString stringWithFormat:@"%@",self.EN];
    return viewController;
    
    
}

#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(LiveNewPageController *)viewController atPage:(NSUInteger)pageIndex
{

    
    viewController.livePageID = [[self.epgChannelList  objectAtIndex:pageIndex] objectForKey:@"id"];
    viewController.currentIndex = pageIndex;
    
    NSLog(@"index:%ld viewDidAppeare:%@", (long)pageIndex, viewController.livePageID);
    
    for (NSDictionary *dic in _cacheList) {
        
        NSLog(@"eeeeeeeeeee---%@",dic);
        
        if ([[dic objectForKey:@"id"]isEqualToString:viewController.livePageID]) {
            if (![dic objectForKey:@"channelInfoDic"]) {
                [viewController reloadChannelDataWith:viewController.livePageID];
            }
            else
            {
                
                NSArray *channelArr = [dic objectForKey:@"channelInfoDic"];
                
                NSLog(@"eeeeeeeeeeeggg---%@",channelArr);
                
                if (channelArr.count == 0) {
                    
                    [viewController reloadChannelDataWith:viewController.livePageID];
                    
                }
                else
                {
                    
                    [viewController reloadContentViewDataWithChannelList:channelArr];
                    
                }

            }
        }
    }
    
    
    
    
}

- (void)magicView:(VTMagicView *)magicView viewDidDisappeare:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    //    NSLog(@"index:%ld viewDidDisappeare:%@", (long)pageIndex, viewController.view);
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex
{
    //    NSLog(@"didSelectItemAtIndex:%ld", (long)itemIndex);
}

#pragma mark - actions
- (void)gotoMore
{
    NSLog(@"打开更多频道接界面");
    
}

#pragma  mark - navigation上的按钮
/**
 *  界面返回按钮方法
 */
- (void)Back {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"返回首页");
    
}


- (void)integrateComponents
{
    CGSize btnSize = CGSizeMake(48, 39);
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(self.view.frame.size.width-btnSize.width, 0, btnSize.width, btnSize.height);
    [rightButton addTarget:self action:@selector(gotoMore) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_default"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"home_icon_more_press"] forState:UIControlStateHighlighted];
    rightButton.backgroundColor = CLEARCOLOR;
    //    rightButton.center = CGPointMake(kDeviceWidth - btnSize.width/2, 64+btnSize.height/2);
    self.magicView.rightNavigatoinItem = rightButton;
    //    [self.view addSubview:rightButton];
    
    NSLog(@"%@",self.magicView.rightNavigatoinItem.backgroundColor);
}

- (void)setupOperatorUI
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *operatorName = [userDefaults objectForKey:@"operatorName"];
    
    NSString *operatorLogo = [userDefaults objectForKey:@"operatorLogo"];
    
    _QGVw = [[UIView alloc]initWithFrame:CGRectMake(0, KDeviceHeight - 40*kDeviceRate - kBottomSafeSpace, kDeviceWidth, 40*kDeviceRate+kBottomSafeSpace)];
    [self.view addSubview:_QGVw];
    _QGVw.backgroundColor = UIColorFromRGB(0xffffff);
    
    UIView *VerticalLine = [[UIView alloc]init];
    [_QGVw addSubview:VerticalLine];
    VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_QGVw.mas_top).offset(1);
        make.left.equalTo(_QGVw.mas_left);
        make.right.equalTo(_QGVw.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _QGoperatorLogo = [[UIImageView alloc]init];
    [_QGVw addSubview:_QGoperatorLogo];
    
    [_QGoperatorLogo mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(_QGVw.mas_centerY);
        make.top.equalTo(VerticalLine.mas_bottom).offset(10*kDeviceRate);
        make.left.equalTo(_QGVw.mas_left).offset(135*kDeviceRate);
        make.height.equalTo(@(19*kDeviceRate));
        make.width.equalTo(@(19*kDeviceRate));
    }];
    
    _QGoperatorName = [[UILabel alloc]init];
    [_QGVw addSubview:_QGoperatorName];
    
    [_QGoperatorName mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(_QGVw.mas_centerY);
        make.centerY.equalTo(_QGoperatorLogo.mas_centerY);
        make.left.equalTo(_QGoperatorLogo.mas_right).offset(3*kDeviceRate);
    }];
    
    if (operatorName.length == 0 && operatorLogo.length == 0) {
        _QGoperatorLogo.image = [UIImage imageNamed:@"ic_launcher"];
        
        _QGoperatorName.text = kAPP_NAME;
    }
    else
    {
        [_QGoperatorLogo sd_setImageWithURL:[NSURL URLWithString:NSStringPM_IMGFormat(operatorLogo)] placeholderImage:[UIImage imageNamed:@"bg_common_exit_dialog_ad_onloading"]];
        
        _QGoperatorName.text = operatorName;
    }
    
    _QGoperatorName.textColor = HT_COLOR_FONT_FIRST;
    _QGoperatorName.font = HT_FONT_SECOND;
    
}

/**
 *  排列数据按照日期先后进行排序
 *
 *  @param arr 原始数组
 *
 *  @return 排序后数组
 */
- (NSArray *)dateSequenceWithsortArr:(NSArray *)arr andKeyName:(NSString *)keyName
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:keyName ascending:YES];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}

@end
