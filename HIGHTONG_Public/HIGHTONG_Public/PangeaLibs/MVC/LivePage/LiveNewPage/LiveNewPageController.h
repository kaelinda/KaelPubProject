//
//  LiveNewPageController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/10/17.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface LiveNewPageController : HT_FatherViewController<UITableViewDelegate,UITableViewDataSource>



/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;


/**
 *  @author derk
 *
 *  @brief 频道详情界面的标题
 */
@property (nonatomic,strong) NSString *detailTitle;

/**
 *  @author derk
 *
 *  @brief 当前页数
 */
@property (nonatomic,assign) NSInteger currentIndex;

/**
 *  @author derk
 *
 *  @brief 该页面频道的 ID
 */
@property (nonatomic,strong) NSString *livePageID;


/**
 @author lailibo
 
 @bried  增加一个livePageName
 */
@property (nonatomic,strong) NSString *livePageName;


/**
 *  @author derk
 *
 *  @brief 主tableView
 */
@property (nonatomic,strong) UITableView *contentTableView;

/**
 *  @author derk
 *
 *  @brief 子频道类型的数据源
 */
@property (nonatomic,strong) NSMutableArray *childrenArray;

/**
 *  @author derk
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar;

/**
 *  @author derk
 *
 *  @brief 节目信息请求结束后 回调该方法 夫控制器可用这个方法来获取每个自控制器的展示数据
 */
@property (nonatomic,copy) void (^ pageCahnnelInfo)(NSMutableArray *childrenArray,NSString *pageChannelID);


/**
 *  @author derk
 *
 *  @brief 频道详情信息字典 用来返回给父控制器的
 */
@property (nonatomic,strong) NSMutableDictionary *channelInfoDic;

/**
 *  @author derk
 *
 *  @brief 重新请求数据
 *  @param ChannelID 频道ID
 */
-(void)reloadChannelDataWith:(NSString *)ChannelID;

/**
 *  @author derk
 *
 *  @brief 直接加载现有数据
 *  @param chaildList  频道节目列表
 */
-(void)reloadContentViewDataWithChannelList:(NSArray *)ChannelList;

@end
