//
//  LiveNewHomeViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/10/17.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "VTMagicController.h"

@interface LiveNewHomeViewController : VTMagicController

@property (nonatomic,copy)NSString *LiveHomeViewTitle;//界面名称
/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;
@end
