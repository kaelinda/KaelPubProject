//
//  EpgSmallVideoPlayerVC.m
//  HIGHTONG_Public
//
//  Created by 赖利波 on 15/9/22.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "EpgSmallVideoPlayerVC.h"
#import "BONavigaitonView.h"
#import "AlertLabel.h"
#import "VideoListTableViewCell.h"
#import "ChannelListTableViewCell.h"
#import "TimeButton.h"
#import "UserCollectCenter.h"
#import "UserPlayRecorderCenter.h"
#import "CustomAlertView.h"
#import "EPGDateSelectedView.h"
#import "EPGListTableViewCell.h"
#import "LinkDeviceTableViewCell.h"
#import "LinkButton.h"
#import "UserScheduleCenter.h"
#import "MBProgressHUD.h"
#import "LoadingView.h"
#import "DateTools.h"
#import "LoginViewController.h"
#import "DataPlist.h"
#import "ADLiveVodProgramView.h"
#import "NewLoginViewController.h"
#import "HTTPRequestHelper.h"
#import "EPGScrollView.h"
#import "NetWorkNotification_shared.h"
#import "UserLoginViewController.h"
#import "PlayerErrorManager.h"
#import "UserBindingVC.h"
#import "HTVolumeUtil.h"
#import "PlayerHeader.h"
static RemoteControlView *remote;;
static NSString * const kMASCellReuseIdentifier = @"kMASCellReuseIdentifier";
static NSString * const kMMSCellReuseIdentifier = @"kMASCellReuseIdentifier";
static NSString * const kMNSCellReuseIdentifier = @"kMASCellReuseIdentifier";
#define ISIntall (IS_PUBLIC_ENV && [APP_ID isEqualToString:@"562172033"])
@interface EpgSmallVideoPlayerVC ()
@property (nonatomic,strong)PlayerErrorManager *errorManager;
@property (nonatomic,strong)ShareView *shareView;
@end

@implementation EpgSmallVideoPlayerVC

{
    NSDateFormatter *formatterdate;
    UIImageView *_gestureGuidView;  //zzsc
    UITableView *_EPGListTableView;
    EPGDateSelectedView *dateView;
    UIView *downView;
    BOOL isremote;
    BOOL _isBackPlayAfterZero;
    BOOL _isBackPlayBeforeZero;
    BOOL _isLastPlayLive;
    BOOL _isCollectionInfo;
    //    BOOL _IsFirstChannelist;
    BOOL _isLiveHasAD;
    BONavigaitonView  *boNavi;
    UIView *waitAndLoadLabel2;
    UIView *waitAndLoadLabel1;
    
    
    UIView *_noDataView;
    UILabel *_noDataLabel;
    UILabel *_noEPGListViewLabel;
    
    HTTimerManager *_timeEPGManager;
    
    //新增一个时刻记录的断点的一个变量
    NSInteger recordTime;//12.12日
    NSString *orderID;   //预约的当前的ID
    NSString *orderServiceName;
    NSString *orderChannelImage;
    NSString *orderEventName;
    NSString *orderStartTime;
    NSString *orderEndTime;
    NSString *orderFullStartTime;
    
    
    
    LoadingView *ac;
    UIActivityIndicatorView *ac1;
    
    UILabel *loading;
    
    
    CGFloat statusHeight;
    
    NSInteger JumpStartTime;
    
    
    //暂停播放按钮
    
    BOOL _isPaused;                 // 播放按钮状态, 假正在播放, 真暂停状态
    
    
    UIButton *_playButton;
    //时间按钮
    
    UIButton *_timeButton;
    
    //锁屏按钮
    UIButton *lockedBtn;
    
    NSDateComponents * dayComponent;
    
    
    //更多按钮
    
    UIButton *_moreButton;
    
    //分享按钮
    
    UIButton * _interactButton;
    UIImageView *_interactView;
    
    
    UIButton *_shareButton;
    UIButton *_shareRightLabel;
    
    
    //Bar上的小按钮
    UIButton *_favourButton;
    UIButton *_favourLabel;
    
    NSMutableArray *typelistARR;
    
    //剧集
    UIButton *_channelButton;
    
    UIImageView *_channelListView;
    ADLiveVodProgramView * _ADViewBase;//频道节目点击时广告    cap
    ADLiveVodProgramView * _ADViewPauseBase;
    
    UIButton *_videoListButton;
    
    UIImageView *_videoListView;
    EPGScrollView *_epgChannelSV;
    EPGScrollView *_epgVideoListSV;
    UIImageView *_moreBackView;
    
    
    
    //全屏按钮
    UIButton *_fullWindowbutton;
    
    
    
    //正在播放的节目   下一个播放的节目
    
    UILabel *_currntPlayLabel;
    
    UILabel *_nextPlayLabel;
    
    UILabel *_currentPlayBackLabel;
    
    
    
    
    UIImageView *_selectClearIV;    // 选择清晰度界面
    
    NSInteger _clickPosition;       // 节目单点击的位置
    
    
    
    NSDate *dateLiveLeft;
    NSDate *dateLiveMiddle;
    NSDate *dateLiveRight;
    
    
    
    NSTimeInterval aTimer;
    NSTimeInterval bTimer;
    
    
    //采集时间的变量
    
    NSDate *timeAgoDate;
    
    NSString *netWork;
    
    PlayerMaskView *_loadingMaskView;
    
    NSOperationQueue *opQueue;
    
    PlayerMaskView *_jxPlayerLoginView;
    PlayerMaskView *_jxPlayerBindView;
    
}


- (id)init
{
    self = [super init];
    if (self) {

        opQueue = [[NSOperationQueue alloc]init];
        
        
        if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
            
            _dateType = EPG_Date_TenDays;
        }else
        {
        _dateType = EPG_Date_week;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];
       
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(volumeChangedNotification:)
                                                     name:@"AVSystemController_SystemVolumeDidChangeNotification"
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                     name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadStateDidChange:)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackStateDidChange:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:_moviePlayer];
        
       
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocationOrder:) name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PausePlayer) name:@"MSG_PAUSEPLAYER" object:nil];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PlayPlayer) name:@"MSG_PLAYPLAYER" object:nil];
       
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_StopPlayer) name:@"MSG_STOPPLAYER" object:nil];

        
        [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
            
            [_EPGListTableView reloadData];
            [_channelListTV reloadData];
        }];
        
        //接收拉的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playPullTV:) name:@"RECEVEPULLTVINFO" object:nil];//接收到拉平消息
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveBoxIPNoti:) name:@"RECEIVE_BOXIP" object:nil];//接收到 盒子IP 消息  更新列表
//        [self StartNewTimer];
        
        
        _theVideoInfo = [[VideoInfo alloc]init];
        //        request = [[HTRequest alloc]initWithDelegate:self];
        clearTitles = [[NSMutableArray alloc]init];
        _SelectdscheduleD = [[MyGesture alloc]init];
        _favourChannelArr = [[NSMutableArray alloc]init];
        _isLiveHasAD = YES;
        if (_timeEPGManager) {
            _timeEPGManager = nil;
        }
            _timeEPGManager = [[HTTimerManager alloc]init];
            _timeEPGManager.delegate = self;
        
        if (_EpgCollectionDuration) {
            _EpgCollectionDuration = nil;
        }
        
        if (_errorManager) {
            _errorManager = nil;
        }
        _errorManager = [[PlayerErrorManager alloc]init];
        
        _localVideoModel = [[LocalVideoModel alloc]init];
        
        
    }
    return self;
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    
    [MobCountTool popOutWithPageView:@"EpgSmallVideoPage"];
    
}

#pragma mark----------播放暂停
//播放暂停
-(void)receiveMsg_PausePlayer
{
    [self actionPauseForVideo];
}

#pragma mark----------恢复播放

-(void)receiveMsg_PlayPlayer
{
    [self actionPlayForVideo];
}


-(void)receiveMsg_StopPlayer
{
//    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];

    if (USER_ACTION_COLLECTION == YES) {
        
        
        
        if (_errorManager.isAVLoading == YES) {
            
            _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
            [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_serviceName andOPSCode:PlayerLoadingExitError andOPSST:[SystermTimeControl getNowServiceTimeString]];
        }
        
        
        [_errorManager resetPlayerStatusInfo];
        
        
        
        //采集EPG 退出小屏界面
        
        self.skimDuration = [self EPGtimeLengthForCurrent];
        
        [SAIInformationManager EPGinformationForChanelFenlei:_serviceID andChanelName:_serviceName andChanelInterval:_epgDateStr andSkimDuration:self.skimDuration andSkimTime:self.nowDateString];
        
        
        //end
        
        
        
        if (_isCollectionInfo) {
            
            
            if (_currentPlayType == CurrentPlayEvent) {
                
                NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
                
                
                
                
                
            }
            if (_currentPlayType == CurrentPlayLive) {
                
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];
                
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",isEmptyStringOrNilOrNull(_dragLabel.text)?@"":_dragLabel.text,@"timeMoveTWT",nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
            }
            
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
        }
        
    }
    
    //本地的播放记录
    if (_currentPlayType == CurrentPlayEvent) {
        
        _localVideoModel.lvID = _serviceID;
        _localVideoModel.lvEventID = _playingEventID;
        _localVideoModel.lvServiceName = _theVideoInfo.channelName;
        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
        _localVideoModel.lvEventName = _theVideoInfo.videoName;
        _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
        
        [_localListCenter addReWatchPlayRecordWithModel:_localVideoModel andUserTye:IsLogin];
        [_localVideoModel resetLocalVideoModel];
        
    }
    
    
    if (_currentPlayType == CurrentPlayEvent) {
        if (IsLogin) {
            [self recordPlaybackTime];
        }else{
            //                [self recordLocalPlaybackTime];
        }
    }
    
    [self removeMoviePlayerNotification];
}



-(void)removeMoviePlayerNotification
{
        if (self.getPlayBackRecordListRepitTimer) {
        [self.getPlayBackRecordListRepitTimer invalidate];
        _getPlayBackRecordListRepitTimer = nil;
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CAN_HIDDEN_TOAST" object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RECEVEPULLTVINFO" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LOCALORDERNOTI_TO_LIVE" object:nil];

    [_adPlayerView.CountTimer invalidate];
    _adPlayerView.CountTimer = nil;
    [_moviePlayer stop];
    [_moviePlayer setContentURL:nil];
    [self.myNewTimer invalidate];
    _moviePlayer = nil;
    [[NSNotificationCenter defaultCenter]postNotificationName:@"REFRESHRECORDERTABLECELL" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REFRESHMINEVCTABLECELL" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETPLAYRECORDERSUCCESS" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    //精简
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveTokenSuccessed" object:nil];

    [_videoListView removeObserver:self forKeyPath:@"hidden"];
    
    [_channelListView removeObserver:self forKeyPath:@"hidden"];
    
    [_polyLoadingView removeObserver:self forKeyPath:@"hidden"];
    [_installMaskView removeObserver:self forKeyPath:@"hidden"];
    [_blackInstallView removeObserver:self forKeyPath:@"hidden"];
    
    [_jxPlayerLoginView removeObserver:self forKeyPath:@"hidden"];
    
    [_playerLoginView removeObserver:self forKeyPath:@"hidden"];
    
    [_loadingMaskView removeObserver:self forKeyPath:@"hidden"];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //    [_seachTextView becomeFirstResponder];
    //    _isSearchHistory = NO;
        [self hideNaviBar:YES];
//       if (_isADDetailEnter == YES) {
//        _adPlayerView.isCanContinu = YES;
////        _isHaveAD = NO;
//           _returnBtnIV.hidden = YES;
//
//        [_moviePlayer play];
//    }

    
    
    
    [self hiddenPanlwithBool:NO];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
//    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//    }else{
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    if (_isVerticalDirection == YES) {
        
        [self setSmallScreenPlayer];
    }
    
    
    if (_isADDetailEnter == YES) {
        _isADDetailEnter = NO;
        _adPlayerView.isCanContinu = YES;
        //        _isHaveAD = NO;
        _returnMainBtn.hidden = YES;
        _topBarIV.hidden = YES;
        _bottomBarIV.hidden = YES;
        _returnBtnIV.hidden = YES;
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        [_moviePlayer play];
        
        
    }

    

    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CAN_SHOW_TOAST" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [MobCountTool pushInWithPageView:@"EpgSmallVideoPage"];
    [[self.view viewWithTag:4040] setHidden:YES];
    [AVAudioSession sharedInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:nil];
    
   
        
        [self afterRefreshBindAuthority];

    
    
    
    
    
}


-(void)afterRefreshBindAuthority
{
    if (_isEnterBind == YES) {
        
        
        _isEnterBind = NO;
        
        if (IsBinding) {
            
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(afterRefreshBind:) name:@"SaveTokenSuccessed" object:nil];
        }

        
    }

}

-(void)afterRefreshBind:(NSNotification*)noti
{
    if (_isVerticalDirection == NO) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
    }
    
    
    NSDictionary *liveDIC = [[NSDictionary alloc]init];
    liveDIC =  [DataPlist ReadToFileDic:EPGINFOPLIST];
    
    _serviceID = [liveDIC objectForKey:@"serviceID"];
    _currentDateStr = [liveDIC objectForKey:@"currentDateStr"];
    _playingEventID = [liveDIC objectForKey:@"eventID"];
    _eventName = [liveDIC objectForKey:@"eventName"];
    _serviceName = [liveDIC objectForKey:@"serviceName"];
    
    if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"0"]) {
        _currentPlayType = CurrentPlayLive;
        HTRequest *requesst = [[HTRequest alloc]initWithDelegate:self];
        if (ISIntall) {
            _installMaskView.hidden = NO;
        }else
        {
            //                        [requesst TVPlayWithServiceID:[liveDIC objectForKey:@"serviceID"] andTimeout:kTIME_TEN];
            [requesst TVPlayWithServiceID:[liveDIC objectForKey:@"serviceID"] andServiceName:_serviceName andTimeout:KTimeout];
            
        }
    }
    
    if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"1"]) {
        
        _currentPlayType = CurrentPlayEvent;
        HTRequest *requesster = [[HTRequest alloc]initWithDelegate:self];
        
        //                    [requesster EventPlayWithEventID:_playingEventID andTimeout:kTIME_TEN];
        if (ISIntall) {
            
            _installMaskView.hidden = NO;
        }else
        {
            
            [requesster EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:KTimeout];
        }
    }
    
    
    if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"2"]) {
        
        _currentPlayType = CurrentPlayTimeMove;
        HTRequest *requesster = [[HTRequest alloc]initWithDelegate:self];
        if (ISIntall) {
            _installMaskView.hidden = NO;
        }else
        {
            [requesster PauseTVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
        }
    }

}




-(void)loginAfterEnterVideo
{
    if (IS_PERMISSION_GROUP) {
        
        
        if (_isEPGPlayerEnter == YES) {
            if (IsLogin) {
                
                [self loginAfterRefreshFavor];
                NSDictionary *liveDIC = [[NSDictionary alloc]init];
                liveDIC =  [DataPlist ReadToFileDic:EPGINFOPLIST];
                
                _serviceID = [liveDIC objectForKey:@"serviceID"];
                _currentDateStr = [liveDIC objectForKey:@"currentDateStr"];
                _playingEventID = [liveDIC objectForKey:@"eventID"];
                _eventName = [liveDIC objectForKey:@"eventName"];
                _serviceName = [liveDIC objectForKey:@"serviceName"];
                
                if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"0"]) {
                    _currentPlayType = CurrentPlayLive;
                    HTRequest *requesst = [[HTRequest alloc]initWithDelegate:self];
                    if (ISIntall) {
                        _installMaskView.hidden = NO;
                    }else
                    {
//                        [requesst TVPlayWithServiceID:[liveDIC objectForKey:@"serviceID"] andTimeout:kTIME_TEN];
                        [requesst TVPlayWithServiceID:[liveDIC objectForKey:@"serviceID"] andServiceName:_serviceName andTimeout:KTimeout];

                    }
                }
                
                if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"1"]) {
                    
                    _currentPlayType = CurrentPlayEvent;
                    HTRequest *requesster = [[HTRequest alloc]initWithDelegate:self];
                    
                    //                    [requesster EventPlayWithEventID:_playingEventID andTimeout:kTIME_TEN];
                    if (ISIntall) {
                        
                        _installMaskView.hidden = NO;
                    }else
                    {
                        
                        [requesster EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:KTimeout];
                    }
                }
                
                
                if ([[liveDIC objectForKey:@"currentPlayType"]isEqualToString:@"2"]) {
                    
                    _currentPlayType = CurrentPlayTimeMove;
                    HTRequest *requesster = [[HTRequest alloc]initWithDelegate:self];
                    if (ISIntall) {
                        _installMaskView.hidden = NO;
                    }else
                    {
                        [requesster PauseTVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    }
                }
                
                
                
                
            }else
            {
                return;
            }
            
            
            
        }else
        {
            return;
        }
        
        
    }

}



- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            NSLog(@"Headphone/Line plugged in");
            
            [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            _isPaused = NO;//改变暂停标记
            _playPauseBigBtn.hidden = YES;
            [_moviePlayer play];//暂停视频

            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            
            _isPaused = YES;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            _playPauseBigBtn.hidden = NO;
            [_moviePlayer pause];
//            [self.myNewTimer invalidate];
            
            
            NSLog(@"Headphone/Line was pulled. Stopping player....");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
     [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self hideNaviBar:YES];

    
    
}

-(void)setExclusiveTouchForButtons:(UIView *)myView
{
    for (UIView * v in [myView subviews]) {
        if([v isKindOfClass:[UIButton class]])
            [((UIButton *)v) setExclusiveTouch:YES];
        else if ([v isKindOfClass:[UIView class]]){
            [self setExclusiveTouchForButtons:v];
        }
    }
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _isEnterClick = YES;
    [self setExclusiveTouchForButtons:self.view];
}




-(void)viewDidLoad{
    [super viewDidLoad];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
     WS(wself);
    [self hideNaviBar:YES];
    _IsFirstChannelist = YES;
    _isVerticalDirection = NO;
    _isPlayEnd =NO;
    
    
    //zzsc 取消侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    NSLog(@"时间时间*********%@",[SAIInformationManager serviceTimeTransformStr]);
    statusHeight = IsIOS6?0:20;
    netWork =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];
    //    netWork = @"3G";
    _isVerticalDirection = YES;
    
    _backgroundView = [[UIView alloc]init];
    [_backgroundView setBackgroundColor:[UIColor blackColor]];
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"PlayTVType"]) {
        PlayTVType = 4;
    }else
    {
        PlayTVType = [[NSUserDefaults standardUserDefaults] integerForKey:@"PlayTVType"];
    }
    
    
    _allCategoryEPGServiceEventArr = [[NSMutableArray alloc]init];
    
    _dicArr = [[NSMutableArray alloc]init];
    
    
//    _netScheduledListArr = [[NSMutableArray alloc]init];
//    UserScheduleCenter *center = [UserScheduleCenter defaultCenter];
//    self.netScheduledListArr = center.didScheduledArr;
    self.netScheduledListArr = [NSMutableArray arrayWithArray:[_localListCenter getScheduledList]];
    
    
    
    
    //采集时移的初始时间   初始化服务器的时间
    self.EPGNowDate = [SystermTimeControl GetSystemTimeWith];
    NSDateFormatter *mformatter =[[NSDateFormatter alloc]init] ;
    [mformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.EPGDateString = [mformatter stringFromDate:self.EPGNowDate];
    
    //end
    
    //小播放器图界面
//    boNavi = [[BONavigaitonView alloc]init];
//    
//    [_backgroundView addSubview:boNavi];
    
    
    _shareView = [[ShareView alloc]init];
    _shareView.delegate = self;
    _shareView.hidden = YES;
    MyGesture *gestureRemote = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gestureRemote.numberOfTapsRequired = 1;
    gestureRemote.tag = SHAREVIEWTAG;
    [_shareView addGestureRecognizer:gestureRemote];
    
    //    if (_moviePlayer != nil) {
    //        //        [player release];
    //        _moviePlayer = nil; //i prefer to do both, just to be sure.
    //    }
    if (!_moviePlayer) {
        _moviePlayer = [[MPMoviePlayerController alloc] init];
    }
    //    if (_moviePlayer) {
    
    _moviePlayer.allowsAirPlay = YES;
    //    [_backgroundView addSubview:_moviePlayer.view];
    _moviePlayer.view.tag = 328;
    //    _moviePlayer.initialPlaybackTime=0;
    [_moviePlayer setControlStyle:MPMovieControlStyleNone];
    [_moviePlayer setScalingMode:MPMovieScalingModeFill];

    _moviePlayer.view.backgroundColor = [UIColor clearColor];
    //    }
    
    
    
    _backEventTagIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_player_lookback_flag"]];
    _backEventTagIV.hidden = YES;
    
    
    
    _contentView = [[UIView alloc]init];
    [_contentView setBackgroundColor:[UIColor clearColor]];
    
    
    
    
   
    
    
    
    //锁屏
    
    _screenLockedIV = [[UIImageView alloc]init];
    
    _screenLockedIV.hidden = YES;
    _screenLockedIV.userInteractionEnabled = YES;
    //    _screenLockedIV.image = [UIImage imageNamed:@"smallSC_locked_bg"];
    lockedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [lockedBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_unlock"] forState:UIControlStateNormal];
    
    [lockedBtn addTarget:self action:@selector(changeTheLockedState:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    //上下Bar
    
    
    _bottomBarIV = [[UIImageView alloc] init];
    _bottomBarIV.hidden = YES;
//    [_bottomBarIV setBackgroundColor:[UIColor clearColor]];
//    [_bottomBarIV setImage:nil];
    [_bottomBarIV setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
    _topBarIV = [[UIImageView alloc] init];
    [_topBarIV setImage:[UIImage imageNamed:@"bg_player_playbill_listview_epg"]];
    //    [_topBarIV setBackgroundColor:[UIColor clearColor]];
    _topBarIV.hidden = YES;
    _topBarIV.userInteractionEnabled = YES;
    _bottomBarIV.userInteractionEnabled = YES;
    
    //开始时刻的大播放按钮和背景View
    
    _playBackgroundIV = [[UIImageView alloc]init];
    _playBackgroundIV.image = [UIImage imageNamed:@""];
    _playBackgroundIV.userInteractionEnabled = YES;
    _playBackgroundIVBtn.showsTouchWhenHighlighted = YES;
    _playBackgroundIVBtn  = [[UIButton alloc]init];
    //    _playBackgroundIVBtn.frame = CGRectMake(0, 0, 57, 57);
    //    _playBackgroundIVBtn.center = _smallContentView.center;
    [_playBackgroundIVBtn setBackgroundColor:[UIColor blackColor]];
    //    _isTapbtn_play = NO;
    
    _isTapbtn_play = YES;
    _playBackgroundIV.hidden = YES;
    
    [_playBackgroundIVBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_pause_normal"] forState:UIControlStateNormal];
    [_playBackgroundIVBtn addTarget:self action:@selector(TapSmallScreenbtn_playNoButton) forControlEvents:UIControlEventTouchUpInside];
    
    _playPauseBigBtn  = [[UIButton alloc]init];
    //    _playBackgroundIVBtn.frame = CGRectMake(0, 0, 57, 57);
    //    _playBackgroundIVBtn.center = _smallContentView.center;
    [_playPauseBigBtn setBackgroundColor:[UIColor clearColor]];
    
    [_playPauseBigBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_pause_normal"] forState:UIControlStateNormal];
    _playPauseBigBtn.hidden = YES;
    _playPauseBigBtn.showsTouchWhenHighlighted = YES;
    [_playPauseBigBtn addTarget:self action:@selector(playPauseBigBtnPlay:) forControlEvents:UIControlEventTouchUpInside];
    

    
    
    
    
    
    _videoProgressV = [[UISlider alloc] init];
    _videoProgressV.hidden = YES;
    _videoProgressV.userInteractionEnabled = YES;
    [_videoProgressV setThumbImage:[UIImage imageNamed:@"ic_player_seekbar_progress_cursor.png"] forState:UIControlStateNormal];
    
    //    -(UIImage*) OriginImage:(UIImage*)image scaleToSize:(CGSize)size
    //    [self OriginImage:[UIImage imageNamed:@"ic_player_seekbar_progress_cursor"] scaleToSize:CGSizeMake(80, 80)];
    
    //    [_videoProgressV setThumbImage:[self OriginImage:[UIImage imageNamed:@"ic_player_seekbar_progress_cursor"] scaleToSize:CGSizeMake(60, 60)] forState:UIControlStateNormal];
    
    
    [_videoProgressV setMaximumTrackImage:[UIImage imageNamed:@"bg_player_progress_normal"] forState:UIControlStateNormal];
    [_videoProgressV setMinimumTrackImage:[UIImage imageNamed:@"bg_player_progress_pressed"] forState:UIControlStateNormal];
    
    
    [_videoProgressV setBackgroundColor:[UIColor clearColor]];
    _videoProgressV.hidden = YES;
    
    [_videoProgressV setMaximumValue:1000.00];
    [_videoProgressV setMinimumValue:0.00];
    
    
    
    [_videoProgressV addTarget:self action:@selector(ProgressValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_videoProgressV addTarget:self action:@selector(ProgressValueConfirm:) forControlEvents:UIControlEventTouchUpInside];
    [_videoProgressV addTarget:self action:@selector(ProgressValueConfirm:) forControlEvents:UIControlEventTouchUpOutside];
    
    
    
    _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _playButton.hidden = YES;
    _playButton.tag = PLAY;
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
    //    [_playButton setTitle:@"播放" forState:UIControlStateNormal];
    _playButton.backgroundColor = [UIColor clearColor];
    _playButton.showsTouchWhenHighlighted = YES;
    [_playButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    _currentPlayBackLabel = [[UILabel alloc]init];
//    [_currentPlayBackLabel setBackgroundColor:[UIColor clearColor]];
//    [_currentPlayBackLabel setFont:[UIFont systemFontOfSize:14]];
//    _currentPlayBackLabel.textColor = [UIColor whiteColor];
//    _currentPlayBackLabel.hidden = YES;
//    
    
    //开始时间、、、、、、
    
    _currentTimeL = [[UILabel alloc]init];
    _currentTimeL.backgroundColor = [UIColor clearColor];
    _currentTimeL.text = @"00:00:00";
    _currentTimeL.textColor = COLOR_FROM_RGB(0xeeeeee);
    _currentTimeL.font = [UIFont systemFontOfSize:11*kRateSize];
    //_currentTimeL.textAlignment = NSTextAlignmentRight;
    _currentTimeL.hidden = YES;
    _currentTimeL.textAlignment = NSTextAlignmentLeft;
    
    
    
    
    //滑动的Label
    
    _dragLabel = [[UILabel alloc]init];
    
    [_dragLabel setTextAlignment:NSTextAlignmentCenter];
    [_dragLabel setTextColor:[UIColor whiteColor]];
    [_dragLabel setBackgroundColor:[UIColor redColor]];
    
    _dragLabel.size = CGSizeMake(100, 20);
    
    
    
    
    
    //结束时间、、、、、
    
    _totalTimeL = [[UILabel alloc]init];
    _totalTimeL.backgroundColor = [UIColor clearColor];
    _totalTimeL.text = @"00:00:00";
    _totalTimeL.font = [UIFont systemFontOfSize:11*kRateSize];
    _totalTimeL.hidden = YES;
    _totalTimeL.textColor = COLOR_FROM_RGB(0xeeeeee);
    //_totalTimeL.textAlignment = NSTextAlignmentLeft;
    _totalTimeL.textAlignment = NSTextAlignmentRight;
    
    
    
    
//    _currntPlayLabel = [[UILabel alloc]init];
//    [_currntPlayLabel setBackgroundColor:[UIColor clearColor]];
//    [_currntPlayLabel setFont:[UIFont systemFontOfSize:14]];
//    _currntPlayLabel.textColor = [UIColor whiteColor];
//    _currntPlayLabel.hidden = YES;
//    
//    
//    
//    _nextPlayLabel = [[UILabel alloc]init];
//    [_nextPlayLabel setBackgroundColor:[UIColor clearColor]];
//    [_nextPlayLabel setFont:[UIFont systemFontOfSize:14]];
//    _nextPlayLabel.textColor = [UIColor whiteColor];
//    _nextPlayLabel.hidden = YES;
    
    
    
    
    
    _tapView = [[UIView alloc] init];
    [_tapView setBackgroundColor:[UIColor clearColor]];
    
    
    UITapGestureRecognizer * tapGestuer = [[UITapGestureRecognizer alloc] init];
    [tapGestuer addTarget:self action:@selector(singleTapAction:)];
    [_tapView addGestureRecognizer:tapGestuer];
    
    _adPlayerView = [[TVADMaskView alloc]init];
    _adPlayerView.hidden = YES;
    _adPlayerView.backBtn.tag = 0;
    _adPlayerView.backBlcok = ^(BOOL isBack)
    {
        if (_adPlayerView.backBtn.tag == 0) {
            [wself removeMoviePlayerNotification];
            
            if (_isLocationOrder == YES|| _isLocationOrderVOD) {
                
                [wself.navigationController popToRootViewControllerAnimated:NO];
                
            }else
            {
                
                
                [wself.navigationController popViewControllerAnimated:NO];
                
            }
            
            if (wself.adPlayerView.CountTimer) {
                [wself.adPlayerView.CountTimer invalidate];
                wself.adPlayerView.CountTimer = nil;
            }
            
         
            
        }else{
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        }
        
        
        
    };
    
    
    _adPlayerView.ADDetailBlock = ^(NSString *str){
        
        DemandADDetailViewController *adDetail = [[DemandADDetailViewController alloc]init];
        if (_adPlayerView.maskViewType == ImageAD) {
            adDetail.detailADURL = [[[wself.theVideoInfo.adList objectAtIndex:0] valueForKey:@"pictureRes"] objectForKey:@"urlLink"];
        }else if (_adPlayerView.maskViewType == VideoAD)
        {
            adDetail.detailADURL = wself.adVideoDetailURL;
        }
       
        if (adDetail.detailADURL.length >0) {
            
            _isADDetailEnter = YES;
            if (_isADDetailEnter == YES) {
                wself.adPlayerView.isCanContinu = NO;
                [wself.moviePlayer pause];
                
                _isPlay = NO;
                
            }
            [wself presentViewController:adDetail animated:YES completion:^{
                
            }];
        }else{
            return ;
        }
        
        
        
        
        
    };
    
    _adPlayerView.volumeBlock = ^(BOOL  isSilence){
        if (isSilence == NO) {
            NSLog(@"jkjkj%.2f===***%.2f",wself.volumProgress.progress,wself.isSlineceValue);
         [HTVolumeUtil shareInstance].volumeValue = wself.isSlineceValue;
            _isVolumeSlinece = NO;

        }else
        {
            _isSlineceValue = [HTVolumeUtil shareInstance].volumeValue;
            NSLog(@"jkjkjXXXX%.2f====%.2f",wself.volumProgress.progress,[HTVolumeUtil shareInstance].volumeValue);
            _isVolumeSlinece = YES;

           
            NSLog(@"没有静音了啊");
            [HTVolumeUtil shareInstance].volumeValue =  wself.volumProgress.progress;
            
        }
        
    };
    _adPlayerView.rotateBtn.tag = 0;
    _adPlayerView.rotateBlock = ^(BOOL isFullWindown){
        if (_adPlayerView.rotateBtn.tag == 0) {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                //这里是1.0f后 你要做的事情
                [AlertLabel AlertInfoWithText:@"已切换至全屏模式" andWith:wself.contentView withLocationTag:1];
                
            });
            
            
        }else if (_adPlayerView.rotateBtn.tag == 1)
        {
            return ;
//            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                
//                //这里是1.0f后 你要做的事情
//                [AlertLabel AlertInfoWithText:@"已切换至小屏模式" andWith:wself.contentView withLocationTag:1];
//                
//            });
        }
    };
    
    _adPlayerView.ADEndBlock = ^(BOOL isEnd){
        
        _isHaveAD = NO;
        //        _adPlayerView.backImageView.hidden = YES;
        [wself clearMoviePlayerStop];
        if (wself.adPlayerView.CountTimer) {
            [wself.adPlayerView.CountTimer invalidate];
            _adPlayerView = nil;
        }
        wself.returnMainBtn.alpha = 1;
        [wself allcodeRateRequest:[NSURL URLWithString:wself.currentURL]];
        if (_adPlayerView.maskViewType == VideoAD) {
            if (_isVolumeSlinece == YES) {
                [HTVolumeUtil shareInstance].volumeValue = wself.isSlineceValue;
                
            }else
            {
                return ;
            }
            
        }

//        if (_isVerticalDirection == NO) {
//            [self setBigScreenPlayer];
//            [self showGestureGuidView];
//
//        }else
//        {
//            [self setSmallScreenPlayer];
//        }
        NSLog(@"广告的时间结束");
    };
    
    _ADViewBase = [[ADLiveVodProgramView alloc]init]; // cap
    _ADViewBase.hidden = YES;
    [_ADViewBase UpDateViewHW:ADPLAYER_MENULive];
    _ADViewBase.alpha = 1;
    
    _ADViewPauseBase = [[ADLiveVodProgramView alloc]init]; // cap
    _ADViewPauseBase.hidden = YES;
    [_ADViewPauseBase UpDateViewHW:ADPLAYER_MENUPause];
    _ADViewPauseBase.alpha = 1;
    
    _serviceTitleLabel = [[UILabel alloc]init];
    [_serviceTitleLabel setBackgroundColor:[UIColor clearColor]];
    _serviceTitleLabel.font = [UIFont systemFontOfSize:18];
    if (_isVerticalDirection == YES) {
        _serviceTitleLabel.text = _serviceName;
    }
//    _serviceTitleLabel.text = _serviceName;
    _serviceTitleLabel.textColor = [UIColor whiteColor];
    
    _currentTitleLabel = [[UILabel alloc]init];
    [_currentTitleLabel setBackgroundColor:[UIColor clearColor]];
    _currentTitleLabel.font = [UIFont systemFontOfSize:18*kRateSize];
    _currentTitleLabel.text = _serviceName;
    _currentTitleLabel.hidden = YES;
    _currentTitleLabel.textColor = [UIColor whiteColor];
    
    
    
    
    
    
    _returnBtnIV = [[UIImageView alloc]init];
    _returnBtnIV.userInteractionEnabled = YES;
    
    _returnBtnIV.image = ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER])?[UIImage imageNamed:@"btn_player_common_returnMain_back_normal"]:[UIImage imageNamed:@"btn_common_title_back_normal"];
    _returnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_returnBtn setBackgroundColor:[UIColor clearColor]];
//    [_returnBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
//    [_returnBtn setImage:[UIImage imageNamed:@"btn_back_click"] forState:UIControlStateHighlighted];
    _returnBtn.showsTouchWhenHighlighted = YES;
    
    _returnBtn.hidden = YES;
    _returnBtnIV.hidden = NO;
    
    _returnMainBtn= [UIButton buttonWithType:UIButtonTypeCustom];
//    [_returnMainBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
//    [_returnMainBtn setImage:[UIImage imageNamed:@"btn_back_click"] forState:UIControlStateHighlighted];
    [_returnMainBtn setBackgroundColor:[UIColor clearColor]];
    _returnMainBtn.showsTouchWhenHighlighted = YES;
    
    
    
    
    
    //互动按钮
    
    _interactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _interactButton.tag = INTERACT;
    _interactButton.backgroundColor = [UIColor clearColor];
    [_interactButton setImage:[UIImage imageNamed:@"btn_player_interaction_normal"] forState:UIControlStateNormal];
    [_interactButton setImage:[UIImage imageNamed:@"btn_player_interaction_pressed"] forState:UIControlStateHighlighted];
//    [_interactButton setTitle:@"互动" forState:UIControlStateNormal];
//    
//    
//    _interactButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    _interactButton.showsTouchWhenHighlighted = YES;
    [_interactButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _interactView = [[UIImageView alloc]init];
    //    [_interactView setBackgroundColor:[UIColor redColor]];
    _interactView.hidden = YES;
    _interactView.userInteractionEnabled = YES;
    [_interactView setImage:[UIImage imageNamed:@"bg_player_interaction_item"]];
    //    [_interactView setBackgroundColor:[UIColor redColor]];
    
    
    
    //三个小背景
    remoteControlBackV = [[UIView alloc]init];
    [remoteControlBackV setBackgroundColor:[UIColor clearColor]];
    remoteControlBackV.userInteractionEnabled = YES;
    
    pushBackV = [[UIView alloc]init];
    [pushBackV setBackgroundColor:[UIColor clearColor]];
    pushBackV.userInteractionEnabled = YES;
    pullBackV = [[UIView alloc]init];
    [pullBackV setBackgroundColor:[UIColor clearColor]];
    pushBackV.userInteractionEnabled = YES;
    
    
    //三个小背景加按钮
    
    
    
    
    remoteLeftV  = [[UIImageView alloc]init];
    [remoteLeftV setImage:[UIImage imageNamed:@"remoteSmall.png"]];
    remoteLeftV.userInteractionEnabled = YES;
    
    pushLeftV  = [[UIImageView alloc]init];
    [pushLeftV setImage:[UIImage imageNamed:@"btn_player_full_interact_push.png"]];
    pullLeftV.userInteractionEnabled = YES;
    
    
    pullLeftV  = [[UIImageView alloc]init];
    [pullLeftV setImage:[UIImage imageNamed:@"btn_player_full_interact_pull.png"]];
    pullLeftV.userInteractionEnabled = YES;
    
    
    
    remoteLabel = [[UILabel alloc]init];
    remoteLabel.userInteractionEnabled = YES;
    [remoteLabel setBackgroundColor:[UIColor clearColor]];
    [remoteLabel setText:@"智能遥控器"];
    [remoteLabel setTextColor:UIColorFromRGB(0xffffff)];
    [remoteLabel setFont:[UIFont systemFontOfSize:15]];
    remoteLabel.textAlignment = NSTextAlignmentLeft;
    
    pushLabel = [[UILabel alloc]init];
    pushLabel.userInteractionEnabled = YES;
    [pushLabel setBackgroundColor:[UIColor clearColor]];
    [pushLabel setText:@"推屏"];
    [pushLabel setTextColor:UIColorFromRGB(0xffffff)];
    [pushLabel setFont:[UIFont systemFontOfSize:15]];
    pullLabel.textAlignment = NSTextAlignmentLeft;
    
    pullLabel = [[UILabel alloc]init];
    [pullLabel setBackgroundColor:[UIColor clearColor]];
    pullLabel.userInteractionEnabled = YES;
    [pullLabel setText:@"拉屏"];
    [pullLabel setFont:[UIFont systemFontOfSize:15]];
    pullLabel.textAlignment = NSTextAlignmentLeft;
    [pullLabel setTextColor:UIColorFromRGB(0xffffff)];
    
    
    
    remoteDownLine = [[UIImageView alloc]init];
    [remoteDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    pushDownLine = [[UIImageView alloc]init];
    [pushDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    pullDownLine = [[UIImageView alloc]init];
    [pullDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    
    
    
    
    //分享按钮
    
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.tag= SHARE;
    //    [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
    _shareButton.hidden = YES;
    //    _shareButton.backgroundColor = [UIColor clearColor];
//    [_shareButton setImage:[UIImage imageNamed:@"btn_share"] forState:UIControlStateNormal];
    
    if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession] && ![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ])
    {
        [_shareButton setImage:[UIImage imageNamed:@"btn_more_share_not"] forState:UIControlStateNormal];
        _shareButton.userInteractionEnabled = NO;
    }else
    {
    
    
    [_shareButton setImage:[UIImage imageNamed:@"btn_player_share_normal"] forState:UIControlStateNormal];
    [_shareButton setImage:[UIImage imageNamed:@"btn_player_share_pressed"] forState:UIControlStateHighlighted];
    }
    [_shareButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    _shareRightLabel = [[UIButton alloc]init];
//    //    _shareRightLabel.text = @"分享";
//    _shareRightLabel.hidden = YES;
//    _shareRightLabel.tag = SHARE;
//    [_shareRightLabel setTitle:@"分享" forState:UIControlStateNormal];
//    //    _shareRightLabel.textColor = [UIColor redColor];
//    [_shareRightLabel setBackgroundColor:[UIColor clearColor]];
//    [_shareRightLabel addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    _clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _clearButton.tag= CLEAR;
    //    [_clearButton setImage:[UIImage imageNamed:@"cleartitle"] forState:UIControlStateNormal];
    _clearButton.titleLabel.font = [UIFont systemFontOfSize:11*kRateSize];
    _clearButton.showsTouchWhenHighlighted = YES;
    
    //    BtnTag
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SUPER_CLEAR) {
        [_clearButton setTitle:@"超清" forState:UIControlStateNormal];
    }
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == HIGH_DEFINE) {
        [_clearButton setTitle:@"高清" forState:UIControlStateNormal];
    }
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == ORIGINAL_PAINT) {
        [_clearButton setTitle:@"标清" forState:UIControlStateNormal];
    }
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SMOOTH) {
        [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
    }
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == 0) {
        [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
    }
    
    _clearButton.titleLabel.textColor = UIColorFromRGB(0x999999);
    _clearButton.backgroundColor = [UIColor clearColor];
    [_clearButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //******************清晰度按钮end**********************************
    
    
    //清晰度选择界面
    
    _selectClearIV = [[UIImageView alloc]init];
    
    [_selectClearIV setImage:[UIImage imageNamed:@"bg_coderate_btn"]];
//    _selectClearIV.alpha = 0.8;
    _selectClearIV.userInteractionEnabled = YES;
    
    _selectClearIV.hidden = YES;
    
    //    [_contentView addSubview:_selectClearIV];
    
    
    
    
    
    
    
    
    //*************初始化分享按钮及初始化下方的View*******end***************************
    
    
    
    //****************初始化剧集按钮及初始化下方的View**********************************
    
    
    //节目单按钮
    
    _videoListButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _videoListButton.tag = VIDEO_LIST;
    _videoListButton.hidden = YES;
//    [_videoListButton setTitle:@"节目单" forState:UIControlStateNormal];
//    _videoListButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    _videoListButton.showsTouchWhenHighlighted = YES;
    
    [_videoListButton setImage:[UIImage imageNamed:@"btn_player_playbill_normal"] forState:UIControlStateNormal];
    [_videoListButton setImage:[UIImage imageNamed:@"btn_player_playbill_pressed"] forState:UIControlStateHighlighted];
//    _videoListButton.backgroundColor = [UIColor clearColor];
    [_videoListButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _videoListView = [[UIImageView alloc]init];
    [_videoListView setBackgroundColor:[UIColor clearColor]];
    _videoListView.userInteractionEnabled = YES;
    _videoListView.hidden = YES;
    _videoListView.userInteractionEnabled = YES;
    [_videoListView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_videoListView"];
    
    
    //节目单列表
    
    _videoListTV = [[UITableView alloc]init];
    _videoListTV.delegate = self;
    _videoListTV.dataSource = self;
    _videoListTV.userInteractionEnabled = YES;
    [_videoListTV setBackgroundColor:[UIColor clearColor]];
    [_videoListTV setBackgroundView:[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];
//    [_videoListTV setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];
    //
    //    _videoListView.userInteractionEnabled = YES;
    //    [_videoListView setImage:[UIImage imageNamed:@"bg_player_fullscreen_menu.png"]];
    _videoListTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_videoListTV registerClass:[VideoListTableViewCell class] forCellReuseIdentifier:kMASCellReuseIdentifier];
    
    _videoListArr = [[NSMutableArray alloc]init];
    
    
    
    
    
    
    //频道
    
    _channelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _channelButton.tag = CHANNEL_SELECT;
    //    [_interactButton setImage:[UIImage imageNamed:@"epg_second_smallwindow.png"] forState:UIControlStateNormal];
//    [_channelButton setTitle:@"选台" forState:UIControlStateNormal];
//    _channelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    _channelButton.showsTouchWhenHighlighted = YES;
    [_channelButton setImage:[UIImage imageNamed:@"btn_player_selchannel_normal"] forState:UIControlStateNormal];
    [_channelButton setImage:[UIImage imageNamed:@"btn_player_selchannel_pressed"] forState:UIControlStateHighlighted];
//    _channelButton.backgroundColor = [UIColor clearColor];
    [_channelButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _channelListView = [[UIImageView alloc]init];
    [_channelListView setBackgroundColor:[UIColor clearColor]];
    _channelListView.hidden = YES;
    _channelListView.userInteractionEnabled = YES;
    [_channelListView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_channelListView"];
    
    
   
    
    
    
    
    _unFavourBV = [[UIView alloc]init];
    [_unFavourBV setBackgroundColor:[UIColor clearColor]];
    
    
    _noFavorImageView = [[UIImageView alloc]init];
    [_noFavorImageView setImage:[UIImage imageNamed:@"ic_player_playbill_no_collection_label"]];
    
    _noFavorTextLabel = [[UILabel alloc]init];
    _noFavorTextLabel.text = @"您还没有喜爱频道哦!";
    _noFavorTextLabel.font = [UIFont systemFontOfSize:13*kRateSize];
    [_noFavorTextLabel setBackgroundColor:[UIColor clearColor]];
    [_noFavorTextLabel setTextColor:UIColorFromRGB(0x999999)];
    
    
    _channelListTV = [[UITableView alloc]init];
    _channelListTV.delegate = self;
    _channelListTV.dataSource = self;
    
    _channelListTV.userInteractionEnabled = YES;
    //    [_channelListTV setBackgroundColor:[UIColor blackColor]];
    [_channelListTV setBackgroundColor:[UIColor clearColor]];
    
    
//    _channelListTV.alpha = 0.8;
//    
//    [_channelListTV setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];
    //
    
    
    [_channelListTV setBackgroundView:[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];

    _channelListTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_channelListTV registerClass:[ChannelListTableViewCell class] forCellReuseIdentifier:kMMSCellReuseIdentifier];
    
    
    //    NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    
    
    
    
    _channelListArr = [[NSMutableArray alloc]init];
    
    
    
//    _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _moreButton.tag = MORE;
//    _moreButton.hidden = YES;
//    _moreButton.showsTouchWhenHighlighted = YES;
//    
//    [_moreButton setImage:[UIImage imageNamed:@"btn_more"] forState:UIControlStateNormal];
//    
//    
//    
//    //    _videoListButton.backgroundColor = [UIColor clearColor];
//    [_moreButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    _moreBackView = [[UIImageView alloc]init];
    _moreBackView.hidden = YES;
    _moreBackView.userInteractionEnabled = YES;
    [_moreBackView setImage:[UIImage imageNamed:@"btn_player_more"]];
    
    
    
    
    
    
    //****************初始化剧集按钮及初始化下方的View*******end***********************
    
    
    
    
    //****************初始化时间按钮**********************************
    
    
//    _timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _timeButton.backgroundColor = [UIColor clearColor];
    
    
    
//   _livePlayerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timeSystemStr) userInfo:nil repeats:YES];
    
    
    
    
    
    //收藏按钮
    _favourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _favourButton.tag = FAVOUR;
    
    [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
    
    //    [_favourButton setBackgroundImage:[UIImage imageNamed:@"btn_player_collect_normal.png"] forState:UIControlStateNormal];
    //    [_favourButton setBackgroundColor:[UIColor redColor]];
    
    [_favourButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    _favourLabel = [[UIButton alloc]init];
//    //    _favourLabel.text = @"添加";
//    [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
////    [self KFavorateBtnisFaved:NO];
//    //    _favourLabel.textColor = [UIColor whiteColor];
//    _favourLabel.hidden = YES;
//    _favourLabel.tag = FAVOUR;
//    //     _favourLabel.showsTouchWhenHighlighted = YES;
//    //    [_favourLabel setBackgroundColor:[UIColor clearColor]];
//    [_favourLabel addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
//    
    [self loginAfterRefreshFavor];
    
    
    
    
    
    
    //    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _shareButton.tag= SHARE;
    //    //    [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
    //    _shareButton.hidden = YES;
    //    //    _shareButton.backgroundColor = [UIColor clearColor];
    //    [_shareButton setImage:[UIImage imageNamed:@"btn_share"] forState:UIControlStateNormal];
    //    [_shareButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    _shareRightLabel = [[UILabel alloc]init];
    //    _shareRightLabel.text = @"分享";
    //    _shareRightLabel.hidden = YES;
    //    _shareRightLabel.textColor = [UIColor whiteColor];
    //    [_shareRightLabel setBackgroundColor:[UIColor clearColor]];
    
    
    _IPArray = [[NSMutableArray alloc]init];//初始化数据源
    
    
    
    _IPBackIV = [[UIImageView alloc]init];
    [_IPBackIV setBackgroundColor:[UIColor blackColor]];
    _IPBackIV.alpha = 0.6;
    
    _IPBackIV.userInteractionEnabled = YES;
    
    
    
    
    
    _IPTableView = [[UITableView alloc]init];
    _IPTableView.delegate = self;
    _IPTableView.dataSource = self;
    //        [_IPTableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_player_interaction_item"]]];
    [_IPTableView setBackgroundColor:[UIColor clearColor]];
    _IPTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _IPTableView.alpha = 0.5;
    _IPTableView.hidden = YES;
    
    
    
    _IPSeperateLineView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_player_full_menuitem_seperate_line"]];
    
    _IPButton = [[UIButton alloc]init];
    [_IPButton setTitle:@"收起" forState:UIControlStateNormal];
    [_IPButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [_IPButton addTarget:self action:@selector(ipButtonCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _RemoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _RemoteButton.hidden = YES;
    _RemoteButton.enabled = NO;
    _RemoteButton.tag = REMOTECONTROL;
    [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_normal.png"] forState:UIControlStateNormal];
    [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_pressed"] forState:UIControlStateHighlighted];

    [_RemoteButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    _TVButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _TVButton.hidden = YES;
    _TVButton.tag = TVTAG;
    [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_normal.png"] forState:UIControlStateNormal];
    [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_pressed"] forState:UIControlStateHighlighted];
    [_TVButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _MTButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _MTButton.hidden = YES;
    _MTButton.tag = MTTAG;
    [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_normal.png"] forState:UIControlStateNormal];
    [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_pressed"] forState:UIControlStateHighlighted];
    [_MTButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //***************初始化快进快退View，亮度等View**********************************
    
    
    _brightnessView = [[UIImageView alloc]init];
    _brightnessView.image = [UIImage imageNamed:@"bg_player_brightness.png"];
    
    _brightnessProgress = [[UIProgressView alloc]init];
    _brightnessProgress.trackImage = [UIImage imageNamed:@"video_num_bg.png"];
    _brightnessProgress.progressImage = [UIImage imageNamed:@"video_num_front.png"];
    _brightnessProgress.progress = [UIScreen mainScreen].brightness;
    _brightnessView.hidden= YES;
    
    
    _volumProgress = [[UIProgressView alloc]init];
    _volumProgress.hidden = YES;
    [_volumProgress addObserver:self forKeyPath:@"progress" options:NSKeyValueObservingOptionNew context:@"_volumProgress"];
    _progressTimeView = [[UIImageView alloc]init];
    _progressTimeView.hidden = YES;
    [_progressTimeView setImage:[UIImage imageNamed:@"bg_player_full_fast_action"]];
//    _progressTimeView.alpha = 0.5;
    
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    _progressTimeLable_top = [[UILabel alloc]init];
    _progressTimeLable_top.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_top.textColor = App_selected_color;
    _progressTimeLable_top.backgroundColor = [UIColor clearColor];
    _progressTimeLable_top.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_top.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_top.shadowOffset = CGSizeMake(1.0, 1.0);
    
        _progressTimeLable_bottom = [[UILabel alloc]init];
        _progressTimeLable_bottom.textAlignment = NSTextAlignmentCenter;
        _progressTimeLable_bottom.textColor = UIColorFromRGB(0xffffff);
        _progressTimeLable_bottom.backgroundColor = [UIColor clearColor];
        _progressTimeLable_bottom.font = [UIFont systemFontOfSize:13*kRateSize];
        _progressTimeLable_bottom.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        _progressTimeLable_bottom.hidden = NO;
        _progressTimeLable_bottom.shadowOffset = CGSizeMake(1.0, 1.0);
    
    
    
//    _timeButton.hidden = YES;
    _shareButton.hidden = YES;
    
    _interactButton.hidden = YES;
    
    _channelButton.hidden = YES;
    
    _videoListButton.hidden = YES;
    
    
    _shareButton.hidden = YES;
    _dragLabel.hidden = YES;
    
    
    _fullWindowbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal.png"] forState:UIControlStateNormal];
    [_fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_pressed"] forState:UIControlStateHighlighted];
//    _fullWindowbutton.backgroundColor = [UIColor clearColor];
    _fullWindowbutton.tag = 0;
    [_fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.view addSubview:_backgroundView];
    [_backgroundView addSubview:_moviePlayer.view];
    [_moviePlayer.view addSubview:_backEventTagIV];
    [_moviePlayer.view addSubview:_contentView];
    [_contentView addSubview:_tapView];
    [_contentView addSubview:_adPlayerView];
    [_moviePlayer.view addSubview:_screenLockedIV];
    [_moviePlayer.view addSubview:_TVButton];
    [_moviePlayer.view addSubview:_RemoteButton];
    [_moviePlayer.view addSubview:_MTButton];
    [_screenLockedIV addSubview:lockedBtn];
    [_playBackgroundIV addSubview:_playBackgroundIVBtn];
    [_tapView addSubview:_playPauseBigBtn];

    [_contentView addSubview:_ADViewPauseBase];

    [_tapView addSubview:_playBackgroundIV];
    [_contentView addSubview:_topBarIV];
    [_contentView addSubview:_bottomBarIV];
    
    
    [_contentView addSubview:_IPBackIV];
    [_IPBackIV addSubview:_IPTableView];
    [_IPBackIV addSubview:_IPSeperateLineView];
    [_IPBackIV addSubview:_IPButton];
    [_bottomBarIV addSubview:_videoProgressV];
    [_topBarIV addSubview:_serviceTitleLabel];
    [_topBarIV addSubview:_currentTitleLabel];
    [_contentView addSubview:_returnBtnIV];
    [_contentView addSubview:_returnBtn];
    [_contentView addSubview:_returnMainBtn];
    [_topBarIV addSubview:_interactButton];
    [_topBarIV addSubview:_videoListButton];
//    [_topBarIV addSubview:_timeButton];
//    [_topBarIV addSubview:_moreButton];
    [_topBarIV addSubview:_channelButton];
    [_bottomBarIV addSubview:_fullWindowbutton];
    [_bottomBarIV addSubview:_currentTimeL];
    [_bottomBarIV addSubview:_dragLabel];
    [_bottomBarIV addSubview:_totalTimeL];
//    [_bottomBarIV addSubview:_currntPlayLabel];
//    [_bottomBarIV addSubview:_nextPlayLabel];
    [_bottomBarIV addSubview:_playButton];
//    [_bottomBarIV addSubview:_currentPlayBackLabel];
    [_contentView addSubview:_interactView];
    [_interactView addSubview:remoteControlBackV];
    [_interactView addSubview:pullBackV];
    [_interactView addSubview:pushBackV];
    [remoteControlBackV addSubview:remoteLeftV];
    [pushBackV addSubview:pushLeftV];
    [pullBackV addSubview:pullLeftV];
    
    
    [remoteControlBackV addSubview:remoteLabel];
    [pushBackV addSubview:pushLabel];
    [pullBackV addSubview:pullLabel];
    [remoteControlBackV addSubview:remoteDownLine];
    [pushBackV addSubview:pushDownLine];
    [pullBackV addSubview:pullDownLine];
    
    
    
    
    
    [_contentView addSubview:_moreBackView];
    [_topBarIV addSubview:_shareButton];
//    [_moreBackView addSubview:_shareRightLabel];
    [_topBarIV addSubview:_favourButton];
//    [_moreBackView addSubview:_favourLabel];
    
    
    [_contentView addSubview:_channelListView];
    [_channelListView addSubview:_channelListTV];
    [_tapView addSubview:_ADViewBase];//cap
    
    [_contentView addSubview:_videoListView];
    [_videoListView addSubview:_videoListTV];
    [_videoListView addSubview:_unFavourBV];
    [_unFavourBV addSubview:_noFavorImageView];
    [_unFavourBV addSubview:_noFavorTextLabel];
    
    [_bottomBarIV addSubview:_clearButton];
    [_contentView addSubview:_selectClearIV];
    
    
    [_contentView addSubview:_brightnessView];
    [_brightnessView addSubview:_brightnessProgress];
    [_contentView addSubview:_volumProgress];
    [_contentView addSubview:_progressTimeView];
    [_progressTimeView addSubview:_progressDirectionIV];
    [_progressTimeView addSubview:_progressTimeLable_bottom];
    [_progressTimeView addSubview:_progressTimeLable_top];
    [_contentView     addGestureRecognizer:self.panGesture];

    
    if ([self.view viewWithTag:8080] == nil) {
        
        _noDataView = [[UIView alloc]init];
        _noDataView.tag = 8080;
        [_noDataView setBackgroundColor:[UIColor blackColor]];
        _noDataView.alpha = 0.9;
        [_contentView addSubview:_noDataView];
        
        _noDataLabel = [[UILabel alloc]init];
        [_noDataLabel setBackgroundColor:[UIColor clearColor]];
        _noDataLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [_noDataLabel setTextColor:UIColorFromRGB(0x999999)];
        _noDataLabel.text = @"暂无相关数据";
        _noDataLabel.textAlignment = NSTextAlignmentCenter;
//        _noDataLabel.center = _noDataView.center;
        [_noDataView addSubview:_noDataLabel];
        
        
        
    }
    
    
    [_noDataView mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.centerY.mas_equalTo(wself.contentView);
        //        make.size.mas_equalTo(CGSizeMake(212*kRateSize, 40*kRateSize));
        //        make.left.mas_equalTo(_channelListView.mas_height/2);
        make.right.mas_equalTo(wself.contentView.mas_right);
        make.width.mas_equalTo(212*kRateSize);
        make.top.mas_equalTo(wself.topBarIV);
        make.bottom.mas_equalTo(wself.bottomBarIV);
    }];
    
    
    
    
    [_noDataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_noDataView.mas_left).offset(40);
//        make.top.mas_equalTo(_noDataView.mas_top).offset(10);
        make.centerX.mas_equalTo(_noDataView.centerX);
        make.centerY.mas_equalTo(_noDataView.centerY);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    
    
    
    
    
    
    
//    [[self.view viewWithTag:8080]setHidden:YES];
    _polyLoadingView = [[PolyMedicineLoadingView alloc]init];
    [_polyLoadingView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"polyLoadingView"];

    if (ISIntall) {
        _polyLoadingView.hidden = YES;
    }else
    {
    _polyLoadingView.hidden = NO;
    }
    [_tapView addSubview:_polyLoadingView];
    [_polyLoadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 60*kRateSize));
        make.centerX.mas_equalTo(wself.tapView.centerX).offset(30*kRateSize);
        make.centerY.mas_equalTo(wself.tapView.centerY);
        
    }];

    
    
    if ([self.view viewWithTag:4040]== nil) {
        
        
        _loadingMaskView = [[PlayerMaskView alloc] init];
        _loadingMaskView.backgroundColor = [UIColor clearColor];
        _loadingMaskView.programType = LoadingViewType;
        _loadingMaskView.hidden = YES;
        _loadingMaskView.tag = 4040;
        _loadingMaskView.maskViewReturnBlock = ^(){
            
            
        };
        [_tapView addSubview:_loadingMaskView];
        
    }
    [_loadingMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];
    
   
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        //        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.left.mas_equalTo(wself.view.mas_left);
        make.height.mas_equalTo(194*kRateSize);
        make.width.equalTo(wself.view.mas_width);
        
    }];
    
    
//    [boNavi mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top);
//        make.left.mas_equalTo(self.view.mas_left);
//        make.width.mas_equalTo(self.view.mas_width);
//        make.height.mas_equalTo(50*kRateSize);
//    }];
    
    [_moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.backgroundView);
        //        make.centerY.mas_equalTo(wself.backgroundView);
        make.top.mas_equalTo(wself.backgroundView.mas_top);
        make.left.mas_equalTo(wself.backgroundView.mas_left);
        make.height.mas_equalTo(wself.backgroundView.mas_height);
        make.width.mas_equalTo(wself.backgroundView.mas_width);
        
        
    }];
    
    
    [_backEventTagIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(60*kRateSize);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(-10*kRateSize);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize, 18*kRateSize));
    }];
    
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.moviePlayer.view);
        //        make.centerY.mas_equalTo(wself.moviePlayer.view);
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.width.mas_equalTo(wself.moviePlayer.view.mas_width);
        
    }];
    
    
    
    
    [_tapView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.contentView.mas_centerX);
        //        make.centerY.mas_equalTo(wself.contentView);
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
    }];
    
    
    [_adPlayerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.contentView);
    }];
    
    //大按钮
    
    [_playBackgroundIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wself.tapView.mas_centerX);
        make.centerY.mas_equalTo(wself.tapView.mas_centerY);
        make.height.mas_equalTo(wself.tapView.mas_height);
        make.width.mas_equalTo(wself.tapView.mas_width);
    }];
    
    
    
    [_playBackgroundIVBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_playBackgroundIV.mas_centerX);
        make.centerY.mas_equalTo(_playBackgroundIV.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];
    
    
    
    
    [_topBarIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(0);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
        
        
    }];
    
    
    
    
    [_bottomBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.height.mas_equalTo(60*kRateSize);
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    
    
    [_fullWindowbutton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(0);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(0);
        make.width.mas_equalTo(43*kRateSize);
        make.height.mas_equalTo(30*kRateSize);
        
    }];
    
    
    
//    _returnBtn.tag = RETURNBIG;
//    [_returnBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _returnMainBtn.tag = RETURNSMALL;
    [_returnMainBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
        [_returnBtnIV mas_makeConstraints:^(MASConstraintMaker *make) {
    
            make.top.mas_equalTo(self.topBarIV.mas_top).offset(20*kDeviceRate);
            make.left.mas_equalTo(self.topBarIV.mas_left).offset(12*kDeviceRate);
            make.width.mas_equalTo(28*kDeviceRate);
            make.height.mas_equalTo(28*kDeviceRate);
    
        }];
    //
    //
    //    [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(self.topBarIV.mas_top).offset(0);
    //        make.left.mas_equalTo(self.topBarIV.mas_left).offset(0);
    //        make.width.mas_equalTo(55*kRateSize);
    //        make.height.mas_equalTo(55*kRateSize);
    //    }];
    
    [_returnMainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(wself.contentView.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(50*kRateSize);
    }];
    
    
    
    
    
    
    
    [_serviceTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(40*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kDeviceRate);
        make.width.mas_equalTo(120*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        
        
    }];
    
    
    
    //    remote = [[RemoteControlView alloc]init];
    //    remote.backgroundColor = [UIColor cyanColor];
    //    remote.delagete = self;
    //    remote.hidden = YES;
    //    isremote = NO;
    //    [self.view addSubview:remote];
    
    
    //亮度视图尺寸****************
    
    [_brightnessView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessProgress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
    
    
    [_backgroundView addSubview:_shareView];
    
    
    
    
    //小屏播放器界面的详情界面
    downView = [[UIView alloc]init];
    [downView setBackgroundColor:App_background_color];
    
    [self.view addSubview:downView];
    
    [downView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.backgroundView.mas_bottom);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        
        make.width.equalTo(wself.view.mas_width);
        make.height.equalTo(wself.view.mas_height).multipliedBy(0.97);
        //        make.bottom.equalTo(wself.view.mas_bottom);
        
    }];
    
    
    dateView = [[EPGDateSelectedView alloc] initWithDateType:_dateType];
    dateView.EPGDelegate = self;
//    [dateView setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
    [dateView setBackgroundColor:App_background_color];
    [downView addSubview:dateView];
    
    [dateView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(downView.mas_top);
        make.left.mas_equalTo(downView.mas_left);
        make.right.mas_equalTo(downView.mas_right);
        //        make.bottom.mas_equalTo(downView.mas_bottom);
        make.height.mas_equalTo(32*kRateSize);
        
        
    }];
    
    _EPGListTableView = [[UITableView alloc] init];
//    [_EPGListTableView setBackgroundColor:[UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1]];
    [_EPGListTableView setBackgroundColor:App_background_color];
    _EPGListTableView.delegate = self;
    _EPGListTableView.dataSource = self;
    _EPGListTableView.contentOffset = CGPointMake(0, 0);
    _EPGListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [downView addSubview:_EPGListTableView];
    
    [_EPGListTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(dateView.mas_bottom).offset(1);
        make.left.mas_equalTo(downView.mas_left);
        //        make.height.mas_equalTo(150*kRateSize);
        make.width.mas_equalTo(kDeviceWidth);
        make.bottom.mas_equalTo(wself.view.mas_bottom);
        
    }];
    
    
    _noEPGListViewLabel = [[UILabel alloc]init];
    [_noEPGListViewLabel setBackgroundColor:[UIColor clearColor]];
    _noEPGListViewLabel.font = [UIFont systemFontOfSize:13*kRateSize];
    [_noEPGListViewLabel setBackgroundColor:[UIColor clearColor]];
    [_noEPGListViewLabel setTextColor:UIColorFromRGB(0x999999)];
    _noEPGListViewLabel.text = @"暂无相关数据";
    
    [downView addSubview:_noEPGListViewLabel];
    _noEPGListViewLabel.hidden = YES;
    [_noEPGListViewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerY.mas_equalTo(downView);
        //        _noDataLabel.center = CGPointMake(80, 80);
        //        make.height.mas_equalTo(150*kRateSize);
        make.top.mas_equalTo(100*kRateSize);
        make.left.mas_equalTo(120*kRateSize);
        make.size.mas_equalTo(CGSizeMake(200, 100));
        
    }];
    
  //报装的View信息
    _installMaskView = [[PlayerMaskView alloc]init];
    _installMaskView.programType = InstallType;
    _installMaskView.hidden = YES;
    [_installMaskView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"installMaskView"];
    
    _blackInstallView = [[BlackInstallView alloc]init];
    _blackInstallView.userInteractionEnabled = YES;
    [_blackInstallView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
//    _blackInstallView.alpha = 0.8;
    [_blackInstallView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"blackInstallView"];
    _blackInstallView.hidden = YES;
    [self.view  addSubview:_blackInstallView];
    [_blackInstallView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.view.mas_left);
        make.top.mas_equalTo(wself.view.mas_top);
        make.bottom.mas_equalTo(wself.view.mas_bottom);
        make.right.mas_equalTo(wself.view.mas_right);
    }];
    
    
    _blackInstallView.cancelButtonClick = ^(BOOL isSelect)
    {
        wself.blackInstallView.hidden = YES;
        
    };
    
    _blackInstallView.confirmButtonClick = ^(NSString *nameStr,NSString *addressStr,NSString *teleStr,NSString *remarkStr,BOOL isSelect)
    {
        HTRequest *htrequest = [[HTRequest alloc]initWithDelegate:wself];
        [htrequest JXBaseBusinessInstallInfoWithName:nameStr withTel:teleStr withAddress:addressStr withDescription:remarkStr];
        
    };
    
    
    
    _installMaskView.maskViewReturnBlock = ^(){
        wself.blackInstallView.hidden = NO;
        
       
        
    };
    [_tapView addSubview:_installMaskView];
    
    [_installMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];

    
    //PlayMaskView
    
    
    
    
     if ([APP_ID isEqualToString:JXCABLE_SERIAL_NUMBER])
     {
         _jxPlayerLoginView = [[PlayerMaskView alloc]initWithAppType:JXCABLE_ASTV];
         _jxPlayerLoginView.backgroundColor = [UIColor clearColor];
         [_jxPlayerLoginView setJxProgramType:JXLoginActionType];
         _jxPlayerLoginView.hidden = YES;
         [_tapView addSubview:_jxPlayerLoginView];
         [_jxPlayerLoginView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"jxplayerLoginView"];
         [_jxPlayerLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.edges.mas_equalTo(_tapView);
         }];
         
         _jxPlayerLoginView.maskViewReturnBlock = ^()
         {
             NSLog(@"我已经实现了江西的登录按钮的点击事件");
             if (_isEnterClick == YES) {
                 
                 _isEnterClick = NO;
                 //            if (_isVerticalDirection == NO) {
                 //
                 //                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                 //
                 //            }
                 
                 if (isAuthCodeLogin) {
                     
                     //                NewLoginViewController *loginController = [[NewLoginViewController alloc]init];
                     //                loginController.isPlayerEnter = YES;
                     //                [self.navigationController pushViewController:loginController animated:YES];
                     _isPaused = YES;
                     
                     [wself actionPauseForVideo];
                     [wself changeLoginVerticalDirection];
                     UserLoginViewController *user = [[UserLoginViewController alloc] init];
                     user.loginType = kVideoLogin;
                     _isEnterClick = YES;
                     [wself.view addSubview:user.view];
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                     user.backBlock = ^()
                     {
                         [[UIApplication sharedApplication]setStatusBarHidden:NO];
                         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                         [wself actionPlayForVideo];
                     };
                     
                     user.tokenBlock = ^(NSString *token)
                     {
                         [[UIApplication sharedApplication]setStatusBarHidden:NO];
                         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                         [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                         [wself loginAfterEnterVideo];
                         [wself actionPlayForVideo];
                     };
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     return;
                     
                     
                     
                 }
                 else
                 {
                     [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.view withLocationTag:1];
                     //            LoginViewController *loginController = [[LoginViewController alloc]init];
                     //            loginController.isPlayerEnter = YES;
                     //                [self.navigationController pushViewController:loginController animated:YES];
                     
                 }
                 
                 
             }
             NSLog(@"这里写上 订购询价的方法");
             
         };
         
         
         _jxPlayerBindView = [[PlayerMaskView alloc]initWithAppType:JXCABLE_ASTV];
         _jxPlayerBindView.backgroundColor = [UIColor redColor];
         [_jxPlayerBindView setJxProgramType:JXBindingActionType];
         _jxPlayerBindView.hidden = YES;
         [_tapView addSubview:_jxPlayerBindView];
         [_jxPlayerBindView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.edges.mas_equalTo(_tapView);
         }];
         
         _jxPlayerBindView.maskViewReturnBlock = ^()
         {
             
             if (_isVerticalDirection == NO) {
                 
                 [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                 
             }
             
             _isEnterBind = YES;
             UserBindingVC *bindVC = [[UserBindingVC alloc]init];
             bindVC.isFroming = YES;
             [wself.navigationController pushViewController:bindVC animated:YES];
         };
         
  
     }else
     {
         _playerLoginView = [[PlayerMaskView alloc] init];
         _playerLoginView.backgroundColor = [UIColor clearColor];
         _playerLoginView.programType = LoginActionType;
         _playerLoginView.hidden = YES;
         _jxPlayerLoginView.hidden = YES;
         [_playerLoginView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"playerLoginView"];
         _playerLoginView.maskViewReturnBlock = ^(){
             
             
             if (_isEnterClick == YES) {
                 
                 _isEnterClick = NO;
                 //            if (_isVerticalDirection == NO) {
                 //
                 //                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                 //
                 //            }
                 
                 if (isAuthCodeLogin) {
                     
                     //                NewLoginViewController *loginController = [[NewLoginViewController alloc]init];
                     //                loginController.isPlayerEnter = YES;
                     //                [self.navigationController pushViewController:loginController animated:YES];
                     _isPaused = YES;
                     
                     [wself actionPauseForVideo];
                     [wself changeLoginVerticalDirection];
                     UserLoginViewController *user = [[UserLoginViewController alloc] init];
                     user.loginType = kVideoLogin;
                     _isEnterClick = YES;
                     [wself.view addSubview:user.view];
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                     user.backBlock = ^()
                     {
                         [[UIApplication sharedApplication]setStatusBarHidden:NO];
                         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                         [wself actionPlayForVideo];
                     };
                     
                     user.tokenBlock = ^(NSString *token)
                     {
                         [[UIApplication sharedApplication]setStatusBarHidden:NO];
                         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                         [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                         [wself loginAfterEnterVideo];
                         [wself actionPlayForVideo];
                     };
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     return;
                     
                     
                     
                 }
                 else
                 {
                     [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.view withLocationTag:1];
                     //            LoginViewController *loginController = [[LoginViewController alloc]init];
                     //            loginController.isPlayerEnter = YES;
                     //                [self.navigationController pushViewController:loginController animated:YES];
                     
                 }
                 
                 
             }
             NSLog(@"这里写上 订购询价的方法");
             
         };
         [_tapView addSubview:_playerLoginView];
         
         [_playerLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
             
             make.edges.mas_equalTo(_tapView);
             
         }];

     }
     
    _errorMaskView = [[ErrorMaskView alloc] init];
    
    __weak HTTimerManager *weaktimeEPGManager = _timeEPGManager;
    
    __block NSInteger weakPlayTVType = PlayTVType;
    
    _errorMaskView.retryAction = ^(){
        [weaktimeEPGManager start];
        [wself playBackChannelVideoModoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",wself.currentURL]]  andPlayTVType:weakPlayTVType];
        NSLog(@"ok ~");
    };
    _errorMaskView.hidden = YES;
    [_tapView addSubview:_errorMaskView];
    
    [_errorMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.tapView);
    }];
   
    
    
    
    [_playPauseBigBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_contentView.mas_centerX);
        make.centerY.mas_equalTo(_contentView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];

    
    
    
    
    [self createEPGController];
    
    [_loadingMaskView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_loadingMaskView"];
//    [self addShareView];
}



-(void)loginAfterRefreshFavor
{
    if (IsLogin) {
        
//         [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];
        
        
//        [[UserCollectCenter defaultCenter] refreshEPGFavorateChannelListWith:^(BOOL isSuccess, NSArray *resultList) {
//            if (isSuccess) {
//                if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                    
//                    //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                    //            [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
//                    [self KFavorateBtnisFaved:YES];
//                    
//                    _favourButton.selected = YES;
//                    //            _favourLabel.selected = YES;
//                    
//                    
//                }else
//                {
//                    
//                    //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                    //            [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
//                    [self KFavorateBtnisFaved:NO];
//                    
//                    _favourButton.selected = NO;
//                    //            _favourLabel.selected = NO;
//                }
//            }
//            else{
//            
//            }
//        }];

        //ww...
        [_localListCenter refreshEPGCollectListWithUserType:IsLogin andBlock:^(BOOL isExist) {
            
            if (isExist) {
                
                if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:IsLogin]) {
                    
                    [self KFavorateBtnisFaved:YES];
                    _favourButton.selected = YES;
                }else{
                    [self KFavorateBtnisFaved:NO];
                    _favourButton.selected = NO;
                }
            }else{
            
            
            }
        }];
        
        
    }else
    {
        
        //        [LocalCollectAndPlayRecorder  che]
        
        
    }

}



-(void)addShareView{
    ShareView *KShareView = [[ShareView alloc] init];
//    KShareView.programType = LoadingViewType;
//    KShareView.hidden = NO;
    [_backgroundView addSubview:KShareView];
    
    [KShareView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_backgroundView.mas_top).offset(isAfterIOS6A?0: 0);
        make.left.mas_equalTo(_backgroundView.mas_left);
        make.width.mas_equalTo(_backgroundView);
        make.height.mas_equalTo(_backgroundView);
        make.bottom.mas_equalTo(_backgroundView.mas_bottom);
        
        
    }];
    

}

- (void)volumeChangedNotification:(NSNotification *)notification
{
    
    float volume =
    [[[notification userInfo]
      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
     floatValue];
    
    if (volume == 0) {
        [_adPlayerView resetvolumBtn:YES];
    }else{
        [_adPlayerView resetvolumBtn:NO];
    }
    
    
    
    
    
    //    DDLogVerbose(@"current volume = %f", volume);
}


#pragma mark - KVO
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    PlayerMaskView *maskview =(PlayerMaskView *)[self.view viewWithTag:4040];
    if (object == maskview) {
        //        _loadingMaskView.hidden = YES;
        NSLog(@"KVO -- %@",change);
        if ([[change objectForKey:@"new"] integerValue]== 1) {
//            _loadingMaskView.hidden = YES;
            [maskview clearProgressWithStatus:NO];
        }else{
            [maskview clearProgressWithStatus:YES];
//            _loadingMaskView.hidden = NO;
        }
        return;
    }
    
    UIView *listVIew = (UIView *)object;
    if (_videoListView == listVIew && [@"hidden" isEqualToString:keyPath]) {

        if (_videoListView.hidden == YES) {
        
        _ADViewBase.hidden = YES;
        }
    }
    
    if (_channelListView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_channelListView.hidden == YES) {
            
            _ADViewBase.hidden = YES;
        }
    }
    
    
    if (_polyLoadingView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_polyLoadingView.hidden == YES) {
            
            _playButton.enabled = YES;
        }else
        {
            _playButton.enabled = NO;
        }
    }

    if (_playerLoginView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_playerLoginView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;
            _playButton.enabled = YES;
            
        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _polyLoadingView.hidden = YES;
            _installMaskView.hidden = YES;
            _playButton.enabled = NO;
        }
    }
    
    

    if (_jxPlayerLoginView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_jxPlayerLoginView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;
            
            _playButton.enabled = YES;
        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _polyLoadingView.hidden = YES;
            _installMaskView.hidden = YES;
            _playButton.enabled = NO;
        }
    }

    
    
    

    if (_installMaskView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        if (_installMaskView.hidden == NO) {
            _polyLoadingView.hidden = YES;
            _playerLoginView.hidden = YES;
            _errorMaskView.hidden = YES;
            _playerLoginView.hidden = YES;
            _jxPlayerLoginView.hidden = YES;

        }
    }
    
    if (_blackInstallView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        if (_blackInstallView.hidden == NO) {
            
            _isScreenLocked = YES;
            
        }else
        {
            _isScreenLocked = NO;
            
        }
    }

    
    
    
}






-(CGRect)trackRectForBounds:(CGRect)bounds
{
    bounds.origin.x=1;
    bounds.origin.y=3;
    bounds.size.height=bounds.size.height*30;
    bounds.size.width=bounds.size.width*30;
    return bounds;
}




#pragma mark - 创建 拖动手势
- (UIPanGestureRecognizer*)panGesture
{
    if (!_panGesture) {
        _panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panAction:)];
        
    }
    return _panGesture;
    
}


#pragma mark - pan水平移动的方法
-(void)panAction:(UIPanGestureRecognizer*)pan
{
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:_contentView];
    CGPoint translatePoint = [pan locationInView:_contentView];
    // 判断是垂直移动还是水平移动
    
    
    CGRect frame = [UIScreen mainScreen].bounds;
    
    //判断屏幕得
    
    if([[[UIDevice currentDevice] valueForKey:@"orientation"] integerValue] == 1 ){
        
        frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width);
        
    }else{
        
        if (IsIOS8) {
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width);
        }else
        {
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
        }
        
    }
    
    
    
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{
            // 开始移动
            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            
            
            if (_isVerticalDirection == NO && _isHaveAD == NO) {
                
                
                if (x > y) { // 水平移动
                    _panDirection = PanDirectionHorizontalMoved;
                    // 取消隐藏
                    
                    _dragLabel.hidden = YES;
                    _progressTimeView.hidden = YES;
                    
                    
                    if (_currentPlayType == CurrentPlayLive) {
                        
                        if (veloctyPoint.x>0) {
                            [AlertLabel AlertInfoWithText:@"正在直播，不能快进噢~" andWith:_contentView withLocationTag:2];
                            return;
                        }else
                        {
                            
                            if (IS_SUPPORT_PAUSEPLAY == YES) {
                                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                                [liveDic setObject:_serviceID forKey:@"serviceID"];
                                [liveDic setObject:@"" forKey:@"eventID"];
                                [liveDic setObject:@"2" forKey:@"currentPlayType"];
                                if (NotNilAndNull(_serviceName)) {
                                    [liveDic setObject:_serviceName forKey:@"serviceName"];
                                }else
                                {
                                    [liveDic setObject:@"" forKey:@"serviceName"];
                                }

                                [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                                
                                _currentPlayType = CurrentPlayTimeMove;
                                _allCodeRateLink = @"";
                                _videoProgressV.value = 1000;
                                _ProgressBeginToMove = 0;
                                
                                
                            }
                        }
                       
                    }
                    
                    
                    if (_currentPlayType == CurrentPlayEvent) {
                        _JumMoveStart = [self secondsFrom:_currentTimeL.text];
                        
                        _ProgressBeginToMove = _videoProgressV.value;
                        
                    }
                    
                    if (_currentPlayType == CurrentPlayTimeMove) {
                        //                        _dragLabel.hidden = YES;
                        _JumTimeMoveStart = [self secondsFrom:_dragLabel.text];
                        
                        _ProgressBeginToMove = _videoProgressV.value;
                        
                    }
                    
                    
                    
                    
                }
                else if (x < y){ // 垂直移动
                    
                    
                    if ((translatePoint.x > frame.size.height*0.5)){
                        
                        _panDirection = PanDirectionVerticalVolumeMoved;
                        
                    }else if ((translatePoint.x < frame.size.height*0.5)){
                        _brightnessView.hidden = NO;
                        
                        _panDirection = PanDirectionVerticalBrighteMoved;
                    }
                    
                    
                    
                }
            }else
            {
                return;
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{ // 正在移动
            if (_isVerticalDirection == NO && _isHaveAD == NO) {
                
                switch (_panDirection) {
                    case PanDirectionHorizontalMoved:{
                        
                        JumpStartTime = 0;
                        OnMove = YES;
                        [self StartNewTimer];
                        if (_currentPlayType == CurrentPlayLive) {
                            
                            
                            if (_videoProgressV.value == 1000) {
                               [AlertLabel AlertInfoWithText:@"即将切换到直播" andWith:_contentView withLocationTag:2];
                                _polyLoadingView.hidden = NO;
                            }else
                            {
                            
                            [AlertLabel AlertInfoWithText:@"正在直播，不能快进噢~" andWith:_contentView withLocationTag:2];
                            }
                            return;
                        }
//                        else
//                        {
//                            JumpStartTime = 0;
//                            OnMove = YES;
//                            [self StartNewTimer];
//                        }
                        _progressTimeView.hidden = NO;
                        [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
                        
                        break;
                    }
                    case PanDirectionVerticalVolumeMoved:{
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                    case PanDirectionVerticalBrighteMoved:{
                        _brightnessView.hidden = NO;
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                        
                    default:
                        break;
                }
            }else
            {
                return;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:{ // 移动停止
            // 移动结束也需要判断垂直或者平移
            // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
            switch (_panDirection) {
                case PanDirectionHorizontalMoved:{
                    _progressTimeView.hidden = YES;
                    
                    if (_isVerticalDirection == NO && _isHaveAD == NO)
                    {
                        OnMove = NO;
                        [self updateProfressTimeLable];
                        _dragLabel.hidden = YES;
                        if (_currentPlayType == CurrentPlayLive) {
                            
                        }else
                        {
                            _videoProgressV.value = _ProgressBeginToMove;
 
                        }
//                        _ProgressBeginToMove = 0;
                        _JumMoveStart = 0;
                        _JumTimeMoveStart = 0;
                        
                    }
                    else
                    {
                        return;
                    }
                    
                    break;
                }
                case PanDirectionVerticalVolumeMoved:{
                    
                   
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                    _brightnessView.hidden = YES;
                   
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark - pan垂直移动的方法
- (void)verticalMoved:(CGFloat)value
{
    
    NSInteger index = (NSInteger)value;
    
    switch (_panDirection) {
        case PanDirectionVerticalVolumeMoved:
            
        {
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素声音减一格
                    
                    [self volumeAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素声音增加一格
                    [self volumeAdd:+0.05];
                }
            }
            
            
        }
            
            break;
        case PanDirectionVerticalBrighteMoved:
        {
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素亮度减一格
                    [self brightnessAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素亮度增加一格
                    
                    [self brightnessAdd:0.05];
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
}

#pragma mark - pan水平移动的方法
- (void)horizontalMoved:(CGFloat)value
{
    _progressTimeView.hidden = NO;
    // 每次滑动需要叠加时间
    
    
    if (value >0) {
        
        [_progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_forward"]];
    }else
    {
        [_progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_backward"]];
    }
    
    
    
    if (_currentPlayType == CurrentPlayEvent) {
        
        _ProgressBeginToMove += value / 150;
        
        if (_ProgressBeginToMove > _moviePlayer.duration) {
            _videoProgressV.value = _videoProgressV.value + 200;
        }else if (_ProgressBeginToMove < 0){
            _ProgressBeginToMove = 0;
        }
        _videoProgressV.value = _ProgressBeginToMove;
        
        NSInteger mmmInterger = _moviePlayer.duration * (_videoProgressV.value/1000);
        NSInteger Jumphour = (mmmInterger/3600);
        NSInteger JumpMinute = (mmmInterger - (Jumphour*3600)) / 60 ;
        NSInteger JumpSecond = (mmmInterger -  Jumphour*3600 - JumpMinute*60);
        NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
        [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
        
        
        [_progressTimeLable_top setText:[NSString stringWithFormat:@"%@",TimeString]];
        
        
        
        NSInteger nInterger = _moviePlayer.duration;
        NSInteger NJumphour = (nInterger/3600);
        NSInteger NJumpMinute = (nInterger - (NJumphour*3600)) / 60 ;
        NSInteger NJumpSecond = (nInterger -  NJumphour*3600 - NJumpMinute*60);
        NSString *NTimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",NJumphour,NJumpMinute,NJumpSecond];
        
        [_progressTimeLable_bottom setText:[NSString stringWithFormat:@"/%@",NTimeString]];

        
        
        
    }
    
    if (_currentPlayType == CurrentPlayLive) {
        _progressTimeView.hidden = YES;
        return;
    }
    
    
    //滑动时移的时候的左侧进度条的改变  和漂浮进度条的移动
    
    if (_currentPlayType == CurrentPlayTimeMove) {
        _playPauseBigBtn.hidden = YES;//kael
        _dragLabel.hidden = NO;
        
        if (_ProgressBeginToMove == 1000) {
            _polyLoadingView.hidden = NO;
            [AlertLabel AlertInfoWithText:@"进入时移状态" andWith:_contentView withLocationTag:2];
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
            [request PauseTVPlayWithServiceID:_currentPlayServiceID andTimeout:ktotalTime];
            }

            
        }
        
        
        _ProgressBeginToMove += value /150;
        _videoProgressV.value = _ProgressBeginToMove;
        
        NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
        [Mcalendar setTimeZone: timeZone];
        NSDateComponents * dayComponentA = [Mcalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond | kCFCalendarUnitWeek |kCFCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:[NSDate dateWithTimeInterval:0 sinceDate:[SystermTimeControl GetSystemTimeWith]]];
        
        UIImageView *imageView = [_videoProgressV.subviews objectAtIndex:2];
        
        
        CGRect theRect = [_bottomBarIV convertRect:imageView.frame fromView:imageView.superview];
        [_dragLabel setFrame:CGRectMake(theRect.origin.x-27, theRect.origin.y-58, _dragLabel.frame.size.width, _videoProgressV.frame.size.height+30)];
        [_dragLabel setBackgroundColor:[UIColor blackColor]];
        
        
        
        NSInteger v =(1000- _ProgressBeginToMove)*14.4;
        
        
        _MoveTimeInteger = v;
        
        NSTimeInterval vseconds = v;
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //
        [formatter setDateFormat:@"HH:mm:ss"];
        
        NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
        
        NSString *MideleTime = [formatter stringFromDate:Mideledate];
        
        
        
        //label的显示数字。为滑动条移动后的位置的value
        [_dragLabel setText:[NSString stringWithFormat:@"%@",MideleTime]];
        
        _currentTimeL.text =  [NSString stringWithFormat:@"%02ld:%02ld:%02ld",dayComponentA.hour-4,dayComponentA.minute,dayComponentA.second];
        
        
//        NSString *dragLabelStr = [NSString stringWithFormat:@"%@ %@",_currentDateStr,MideleTimeHM];
        _totalTimeL.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",dayComponentA.hour,dayComponentA.minute,dayComponentA.second];
               _progressTimeLable_top.text = [NSString stringWithFormat:@"%@",MideleTime];
        _progressTimeLable_bottom.text = [NSString stringWithFormat:@"/%@",[self getHourMinuteAndSecond:[SystermTimeControl GetSystemTimeWith]]];
        
        
        
        
        if (_videoProgressV.value == 1000) {
            _dragLabel.hidden = YES;
            _polyLoadingView.hidden = NO;
            [AlertLabel AlertInfoWithText:@"即将切换到直播" andWith:_contentView withLocationTag:2];
            [[self.view viewWithTag:8080]setHidden:YES];
            
            _currentPlayType = CurrentPlayLive;
            
            
        }
        
    }
  
    
}


- (void)updateProfressTimeLable{
    
    if (_currentPlayType == CurrentPlayTimeMove) {
        
        if (_videoProgressV.value >= 990) {
            
            return;
        }else
        {
        
        NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
        [Mcalendar setTimeZone: timeZone];
        
        UIImageView *imageView = [_videoProgressV.subviews objectAtIndex:2];
        
        
        CGRect theRect = [_bottomBarIV convertRect:imageView.frame fromView:imageView.superview];
        [_dragLabel setFrame:CGRectMake(theRect.origin.x-27, theRect.origin.y-58, _dragLabel.frame.size.width, _videoProgressV.frame.size.height+30)];
        [_dragLabel setBackgroundColor:[UIColor blackColor]];
        
        
        
        NSInteger v =(1000- _ProgressBeginToMove)*14.4;
        
        
        _MoveTimeInteger = v;
        
        NSTimeInterval vseconds = v;
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //
        [formatter setDateFormat:@"HH:mm:ss"];
        
        NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
        
        NSString *MideleTime = [formatter stringFromDate:Mideledate];
        [self timeMoveUpdateServiceTitleStringMoveTimeInteger:_MoveTimeInteger];
        //label的显示数字。为滑动条移动后的位置的value
        [_dragLabel setText:[NSString stringWithFormat:@"%@",MideleTime]];
        
//        NSString *timeMoveString = [NSString stringWithFormat:@"%@?npt=-%ld-",_theVideoInfo.videoPauseLink,_MoveTimeInteger];
        _allCodeRateLink = @"";
        _dragLabel.hidden = NO;
//        [self PauseTVPlayModeWithURL:[NSURL URLWithString:timeMoveString] andPlayTVType:PlayTVType];
            
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
            [request PauseTVPlayWithServiceID:_currentPlayServiceID andTimeout:KTimeout];
            }
            
            
        }
    }
    
    //    }
    
    
    if (_currentPlayType == CurrentPlayEvent) {
        
        if (_moviePlayer.duration ==0) {
            
            _videoProgressV.value = 0;
        }else
        {
            JumpStartTime = _moviePlayer.duration * ([_videoProgressV value]/1000);
            
            NSInteger Jumphour = (JumpStartTime/3600);
            NSInteger JumpMinute = (JumpStartTime - (Jumphour*3600)) / 60 ;
            NSInteger JumpSecond = (JumpStartTime -  Jumphour*3600 - JumpMinute*60);
            NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
            [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
            
            if (IsLogin) {
                [self recordPlaybackTime];
            }else{
                //        [self recordLocalPlaybackTime];
            }
            _moviePlayer.currentPlaybackTime = JumpStartTime;
            //        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
            
            if (_moviePlayer.duration == JumpStartTime) {
                // 用户拉到最后
                [self ChangeCurTimeForNewPort];
                return;
            }

        }
        
        
        
        
        
    }
    
    
    
    if (_currentPlayType == CurrentPlayLive)
    {
        
        if (_videoProgressV.value==1000) {
            [AlertLabel AlertInfoWithText:@"即将切换到直播" andWith:_contentView withLocationTag:2];
            _polyLoadingView.hidden = NO;
            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
//            [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

            }
        }else
        {
            
            [[self.view viewWithTag:4040] setHidden:YES];
            
            return;
        }
        
    }
    
    
    
    
}





#pragma 时移滑动更新节目单


-(NSString *)timeMoveForSliderToChangeServiceTitle:(NSMutableArray*)serviceArr andDragTitle:(NSString*)dragTitle
{
//    NSString *timeMoveStr;
    for (int i = 0; i<serviceArr.count; i++) {
        
        NSDictionary *serviceDic = [[NSDictionary alloc]init];
        
        serviceDic = [serviceArr objectAtIndex:i];
        
        
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
 
        //全部统一规定时间
       
        NSString *fullEndTimeStr = [NSString stringWithFormat:@"%@",[self changeCalender:[serviceDic objectForKey:@"endTime"]]];
//        NSString *fullTimeMoveTimeStr = [NSString stringWithFormat:@"%@ %@",fullTimeStr,dragTitle];
        NSString *fullTimeMoveTimeStr = [NSString stringWithFormat:@"%@",[self changeCalender:dragTitle]];
      NSDate *endDate =  [NSDate dateWithString:fullEndTimeStr formatString:@"yyyy-MM-dd HH:mm" timeZone:nil];
        NSDate *timeMoveDate = [NSDate dateWithString:fullTimeMoveTimeStr formatString:@"yyyy-MM-dd HH:mm" timeZone:nil];
        NSDate *startDate = [NSDate dateWithString:[NSString stringWithFormat:@"%@",[self changeCalender:[serviceDic objectForKey:@"startTime"]]] formatString:@"yyyy-MM-dd HH:mm" timeZone:nil];
        
        
        if ([timeMoveDate isLaterThan:startDate] && [timeMoveDate isEarlierThan:endDate]) {
            
            NSLog(@"时移的节目名称%@",[serviceDic objectForKey:@"eventName"]);
            
            return  [serviceDic objectForKey:@"eventName"];
            
        }
        
        
        
    }

    return nil;
//    return timeMoveStr;
    
    
}


-(NSString*)changeCalender:(NSString*)hourAndMiniteStr
    {
        NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
        [Mcalendar setTimeZone: timeZone];
        NSDateComponents * dayComponentA = [Mcalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond | kCFCalendarUnitWeek |kCFCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:[NSDate dateWithTimeInterval:0 sinceDate:[SystermTimeControl GetSystemTimeWith]]];
        
        NSString *currentYearStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld %@",dayComponentA.year,dayComponentA.month,dayComponentA.day,hourAndMiniteStr];
        
        
        
        return currentYearStr;
        
        
    }



-(BOOL)checkPlayBackForCurrentListForServiceID:(NSString *)serviceID andCurrentList:(NSMutableArray*)currentList
{
    
    //    BOOL lookbackFlag = false;
    for (int i = 0; i<currentList.count; i++) {
        
        NSMutableArray *itemsMArr = [[NSMutableArray alloc] initWithArray:[[currentList objectAtIndex:i] valueForKey:@"serviceinfo"]];
        
        
        for (int j = 0; j< [itemsMArr count]; j++) {

            NSDictionary *dic = [[NSDictionary alloc]init];
            
            dic =  [[[currentList objectAtIndex:i] valueForKey:@"serviceinfo"] objectAtIndex:j];
            
            
            if ([serviceID isEqualToString:[dic valueForKey:@"id"]]) {
                
                
                if ([[dic objectForKey:@"lookbackFlag"] isEqualToString:@"0"]) {
                    return NO;
                }
                if ([[dic objectForKey:@"lookbackFlag"] isEqualToString:@"1"]) {
                    return YES;
                }
                
                break;
                
            }
            
            
        }
        
        
    }
    
    
    return YES;
}



-(void)createEPGController
{
    
    NSMutableArray *currentArr = [[NSMutableArray alloc]init];
    
    currentArr = [[NSUserDefaults standardUserDefaults] objectForKey:EPG_CHANNELLIST];
    
    //    _isLookPlayBackEvent =  [self checkPlayBackForCurrentList:currentArr];
    
    _isLookPlayBackEvent = [self checkPlayBackForCurrentListForServiceID:_serviceID andCurrentList:currentArr];
    
    
//    NSLog(@"jkjkjkjkjkjddddd%@",_isLookPlayBackEvent);
    
    
    
    NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [Mcalendar setTimeZone: timeZone];
    dayComponent = [Mcalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond | kCFCalendarUnitWeek |kCFCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:[NSDate dateWithTimeInterval:0 sinceDate:[SystermTimeControl GetSystemTimeWith]]];
    
    
    if (!_currentDateStr) {
        _currentDateStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",dayComponent.year,dayComponent.month,dayComponent.day];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //这里是1.0f后 你要做的事情
        NSString *bufferStr = [[NSString alloc] initWithFormat:@"%@",_currentDateStr];
        [dateView DateBtnSelectedAtDate:bufferStr];
    });
    
    
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    //    /数据接口
    [request TypeListWithTimeout:kTIME_TEN];
    
    
    [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
//        [request EPGGetLivePageChannelInfo];
        [request EPGGetLivePageChannelInfoAndisType:-1];
        [request TVFavorateListWithTimeout:kTIME_TEN];
        
        
    });
    
    
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocationOrder:) name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EPGTableVIewAndChannelTableView) name:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil];//响应事件中 刷新TableView 列表
    //    //接收拉的通知
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playPullTV:) name:@"RECEVEPULLTVINFO" object:nil];//接收到拉平消息
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveBoxIPNoti:) name:@"RECEIVE_BOXIP" object:nil];//接收到 盒子IP 消息  更新列表
    [self StartNewTimer];
    
    
    
    
    
}




-(void)receiveBoxIPNoti:(NSNotification *)noti{
    
    
    //    [_IPArray removeAllObjects];
    NSDictionary *IPDic = [[NSDictionary alloc] initWithDictionary:[noti object]];
    
    NSLog(@" 接收到盒子IP信息 %@",IPDic);
    NSString *IPStr = [[IPDic objectForKey:@"param"] objectForKey:@"IPAddress"];
    
    if (IPStr.length==0) {
        IPStr = @"";
        //        [AlertLabel AlertInfoWithText:@"未搜索到盒子,稍后请重试" andWith:_contentView withLocationTag:2];
    }
    
    BOOL isExist = NO;//判断新获取的IP是否已经存在
    for (NSString *ip in _IPArray) {
        if ([ip isEqualToString:IPStr]) {
            isExist = YES;
        }
    }
    
    if (!isExist) {
        [_IPArray addObject:IPStr];
        
//        _IPTableView.hidden = NO;
//        _IPBackIV.hidden = NO;

    }
    
    
//    [_IPTableView reloadData];
    
    
    
    
}



-(void)playPullTV:(NSNotification *)not
{
    //    _isNowPlayLiveTV = NO;
    
    NSDictionary *pullDic = [[NSDictionary alloc] initWithDictionary:[not userInfo]];
    NSString *type = [pullDic objectForKey:@"@command"];
    NSDictionary *param = [[NSDictionary alloc] initWithDictionary:[pullDic objectForKey:@"param"]];
    
    NSLog(@"拉屏8888888%@",param);
    
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    if ([type isEqualToString:@"DLNAPLAY_VOD"]) {
        //点播节目
        //        [self.titleLabel setText:[param objectForKey:@"name"]];
        if ([[param objectForKey:@"name"] length]>0) {
            [_serviceTitleLabel setText:[NSString stringWithFormat:@"%@",[param objectForKey:@"name"]]];
        }else
        {
            [_serviceTitleLabel setText:@"暂无:"];
        }
        
        //由于拉屏和点播全都走这个地，所以拉过来的点播的断点暂时用负值加以区分
        
        JumpStartTime = [[param objectForKey:@"seekPos"] integerValue];
        
        NSString *MType =[param objectForKey:@"movie_type"];
        if ([MType integerValue]==0) {
            MType = @"1";
        }
        
        NSString *McontenID = [param objectForKey:@"contentID"];
        
        
        if (McontenID.length >0) {
            
            _theVideoInfo.contentIDP = McontenID;
            [request VODPlayWithContentID:[param objectForKey:@"contentID"] andWithMovie_type:MType andTimeout:kTIME_TEN];
        }else
        {
            [AlertLabel AlertInfoWithText:@"拉屏操作暂无播放节目" andWith:_contentView withLocationTag:2];
            
            return;
        }
        
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_LIVE"]){
        //直播节目
        
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            [_serviceTitleLabel setText:[NSString stringWithFormat:@"%@",[param objectForKey:@"channelName"]]];
        }else
        {
            [_serviceTitleLabel setText:@"暂无:"];
        }
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            [request TVPlayEXWithTVName:[param objectForKey:@"channelName"] andTimeout:kTIME_TEN];
            
        }else
        {
            
            [AlertLabel AlertInfoWithText:@"拉屏操作无频道名称" andWith:_contentView withLocationTag:2];
            
            
            return;
        }
        
        
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_EVENT"]){
        //回看节目
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            [_serviceTitleLabel setText:[NSString stringWithFormat:@"%@:",[param objectForKey:@"channelName"]]];
            
        }else
        {
            [_serviceTitleLabel setText:@"暂无:"];
        }
        
        if ([[param objectForKey:@"seekPos"] integerValue]>0) {
            JumpStartTime = [[param objectForKey:@"seekPos"] integerValue];
        }else
        {
            JumpStartTime = 0;
        }
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            
            _theVideoInfo.fullStartTimeP = [param objectForKey:@"startDate"];
            _theVideoInfo.endTimeP = [param objectForKey:@"endDate"];
            
            [request EventPlayEXWithTVName:[param objectForKey:@"channelName"] andStarTime:[param objectForKey:@"startDate"] andEndTime:[param objectForKey:@"endDate"] andTimeout:kTIME_TEN];
        }else
        {
            
            [AlertLabel AlertInfoWithText:@"拉屏获取频道名称为空" andWith:_contentView withLocationTag:2];
            
            
            return;
        }
        
        
        
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_PAUSE"]){
        //时移节目 增强版时移鉴权
        [self.serviceTitleLabel setText:[param objectForKey:@"channelName"]];
        //        [data exchangeScreenTimeMovePlayWithTVName:[param objectForKey:@"channelName"] andTimeout:10];
        [request PauseTVPlayEXWithTVName:[param objectForKey:@"channelName"] andTimeout:kTIME_TEN];
        JumpStartTime = [[param objectForKey:@"seekPos"] integerValue]+10;
        return;
        
    }else{
        
        [AlertLabel AlertInfoWithText:@"非EPG节目，暂无法播放" andWith:self.view withLocationTag:0];
        
        
    }
}

#pragma mark touch event 手势响应



//声音增加
- (void)volumeAdd:(CGFloat)step{
    
    [HTVolumeUtil shareInstance].volumeValue += step;;
    
    _volumProgress.progress = [HTVolumeUtil shareInstance].volumeValue;
}
//亮度增加
- (void)brightnessAdd:(CGFloat)step{
    [UIScreen mainScreen].brightness += step;
    _brightnessProgress.progress = [UIScreen mainScreen].brightness;
}



- (void)updateVolumView
{
    if (_volumProgress.alpha) {
        [UIView animateWithDuration:3 animations:^{
            _volumProgress.alpha = 0;
        }];
    }
}













-(UIImage*) OriginImage:(UIImage*)image scaleToSize:(CGSize)size

{
    
    UIGraphicsBeginImageContext(size);//size为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0,0, size.width, size.height)];
    
    UIImage* scaledImage =UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
    
}



-(void)ProgressValueChanged:(UISlider*)sender
{
        [self.myNewTimer invalidate];
    if (_currentPlayType == CurrentPlayLive) {
        
        
        [_currentTimeL setText:[NSString stringWithFormat:@"%@", liveStartTime]];
        [_totalTimeL setText:[NSString stringWithFormat:@"%@", liveEndTime]];
        
        return;
        
    }
    else if (_currentPlayType == CurrentPlayEvent)
    {
        JumpStartTime = 0;
        OnMove = YES;
        
        NSInteger JumpTime = _moviePlayer.duration * (([_videoProgressV value])/1000);
        
        NSInteger Jumphour = (JumpTime/3600);
        NSInteger JumpMinute = (JumpTime - (Jumphour*3600)) / 60 ;
        NSInteger JumpSecond = (JumpTime -  Jumphour*3600 - JumpMinute *60);
        
        NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
        [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
        self.autoHiddenSecend = 5;
        
    }else
    {
        //        _videoProgressV.enabled = YES;
        _dragLabel.hidden = NO;
        
        NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
        [Mcalendar setTimeZone: timeZone];
        NSDateComponents * dayComponentA = [Mcalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond | kCFCalendarUnitWeek |kCFCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:[NSDate dateWithTimeInterval:0 sinceDate:[SystermTimeControl GetSystemTimeWith]]];
        
        
        //        if (_videoProgressV.value ==0) {
        //            _videoProgressV.value = 1000;
        //
        //        }
        
        
        //        _videoProgressV.value = 1000;
        UIImageView *imageView = [_videoProgressV.subviews objectAtIndex:2];
        
        
        CGRect theRect = [_bottomBarIV convertRect:imageView.frame fromView:imageView.superview];
        [_dragLabel setFrame:CGRectMake(theRect.origin.x-27, theRect.origin.y-58, _dragLabel.frame.size.width, _videoProgressV.frame.size.height+30)];
        [_dragLabel setBackgroundColor:[UIColor blackColor]];
        
        
        _currentTimeL.text =  [NSString stringWithFormat:@"%02ld:%02ld:%02ld",dayComponentA.hour-4,dayComponentA.minute,dayComponentA.second];
        
        _totalTimeL.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",dayComponentA.hour,dayComponentA.minute,dayComponentA.second];
        
        
        
        
        int v =(1000- _videoProgressV.value)*14.4;
        
        
        _MoveTimeInteger = v;

        
        NSTimeInterval vseconds = v;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //
        [formatter setDateFormat:@"HH:mm:ss"];
        
        NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
        
        NSString *MideleTime = [formatter stringFromDate:Mideledate];
        
        //label的显示数字。为滑动条移动后的位置的value
        [_dragLabel setText:[NSString stringWithFormat:@"%@",MideleTime]];
        //    [formatter release];
        
        
        NSLog(@"dragggg%@",MideleTime);
        
        
        //动画效果
        [UIView animateWithDuration:0.5
                         animations:^
         {
             
             [_dragLabel setAlpha:1.f];
         }
                         completion:^(BOOL finished)
         {
             // 动画结束时的处理
         }];
        
 
    }
    
    
}





-(void)ProgressValueConfirm:(UISlider*)slider
{
    [[self.view viewWithTag:4040] setHidden:YES];
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    if (_currentPlayType == CurrentPlayTimeMove) {
        _playPauseBigBtn.hidden = YES;//kael

        [_dragLabel setAlpha:0.f];
        
        
        NSInteger timemoveValue = (1000-slider.value)*14.4;
//        NSString *timeMoveString = [NSString stringWithFormat:@"%@?npt=-%d-",_theVideoInfo.videoPauseLink,(int)timemoveValue];
         _MoveTimeInteger = (int)timemoveValue;
        _ProgressBeginToMove = slider.value;
        [[self.view viewWithTag:4040] setHidden:YES];
//        _videoProgressV.value = slider.value;

        
        [self timeMoveUpdateServiceTitleStringMoveTimeInteger:timemoveValue];
        
        
        
        if (timemoveValue == 0) {
            _polyLoadingView.hidden = NO;
            [AlertLabel AlertInfoWithText:@"即将切换到直播" andWith:_contentView withLocationTag:2];
            _dragLabel.hidden = YES;
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
//            [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

            }
        }else
        {
            _allCodeRateLink = @"";
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
            [request PauseTVPlayWithServiceID:_currentPlayServiceID andTimeout:KTimeout];
            }
//            [self PauseTVPlayModeWithURL:[NSURL URLWithString:timeMoveString] andPlayTVType:PlayTVType];
        }
        
        
        
        
        
    }
    
    
    
    if (_currentPlayType == CurrentPlayEvent)
    {
        [[self.view viewWithTag:4040] setHidden:YES];
        if (_moviePlayer.duration <= 0) {
            _videoProgressV.value = 0;
            
        }else{
        OnMove = NO;
        JumpStartTime = _moviePlayer.duration * ([_videoProgressV value]/1000);
        NSInteger Jumphour = (JumpStartTime/3600);
        NSInteger JumpMinute = (JumpStartTime - (Jumphour*3600)) / 60 ;
        NSInteger JumpSecond = (JumpStartTime -  Jumphour*3600 - JumpMinute*60);
        NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
        [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
        
        if (IsLogin) {
            [self recordPlaybackTime];
        }else{
            //        [self recordLocalPlaybackTime];
        }
        _moviePlayer.currentPlaybackTime = JumpStartTime;
//        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
        
        if (_moviePlayer.duration == JumpStartTime) {
            // 用户拉到最后
            [self ChangeCurTimeForNewPort];
            return;
        }
        self.autoHiddenSecend = 5;
        }
    }
    
    
    if (_currentPlayType == CurrentPlayLive)
    {
        if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle] <=0 && (liveStartTime.length<=0|| liveEndTime.length<=0)) {
            return;
        }
        
        if (_videoProgressV.value < (aTimer/bTimer)*1000) {
            _polyLoadingView.hidden = NO;
            [AlertLabel AlertInfoWithText:@"进入时移状态" andWith:_contentView withLocationTag:2];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
            [request PauseTVPlayWithServiceID:_currentPlayServiceID andTimeout:kTIME_TEN];
            }
            //             [AlertLabel AlertInfoWithText:@"即将切换进入时移状态" andWith:_contentView withLocationTag:2];
            //            _videoProgressV.value = 1000;
            //
            //            _currentPlayType = CurrentPlayTimeMove;
            
            
        }else
        {
            [AlertLabel AlertInfoWithText:@"正在直播，不能快进噢~" andWith:_contentView withLocationTag:2];

            if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle] <=0) {
                
                _videoProgressV.value = 0;
            }else
            {
                [[self.view viewWithTag:4040] setHidden:YES];
                [self StartNewTimer];
                return;
            }
//            _videoProgressV.value = 0;
           
        }
        
    }
    
    
}


-(void)timeMoveUpdateServiceTitleStringMoveTimeInteger:(NSInteger)moveTimeInteger
{
//    NSInteger v =(1000- _ProgressBeginToMove)*14.4;
    
    _MoveTimeInteger = moveTimeInteger;
    
    NSTimeInterval vseconds = moveTimeInteger;
    
    NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
    
    NSDateFormatter *formatterHM = [[NSDateFormatter alloc]init];
    //
    [formatterHM setDateFormat:@"HH:mm"];
    
    NSString *MideleTimeHM = [formatterHM stringFromDate:Mideledate];
    
    if (_currentServiceEventStr) {
        
        _currentServiceEventStr = @"";
        if ([self timeMoveForSliderToChangeServiceTitle:_channelListArr andDragTitle:MideleTimeHM].length>0) {
            _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,[self timeMoveForSliderToChangeServiceTitle:_channelListArr andDragTitle:MideleTimeHM]];
        }else
        {
            _currentServiceEventStr = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
        }
    }
    
    
    NSLog(@"=====serviceName==15%@===%@",_serviceName,_currentServiceEventStr);

    
    if (_isVerticalDirection == NO && _isHaveAD == NO) {
        
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",_currentServiceEventStr];
    }else
    {
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",_serviceName];
    }
    

  
}



- (void)changeTheLockedState:(UIButton *)btn
{//点击屏幕上的小锁按钮
    _isScreenLocked = !_isScreenLocked;
    if (_isScreenLocked) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"screenLocked"];
        
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_lock"] forState:UIControlStateNormal];
        _contentView.userInteractionEnabled = NO;
        _TVButton.alpha = 0;
        _MTButton.alpha = 0;
        _RemoteButton.alpha = 0;
        _selectClearIV.hidden = YES;
        _interactView.hidden = YES;
        _moreBackView.hidden = YES;
        _channelListView.hidden = YES;
//        _ADViewBase.hidden = YES;//cap
        _videoListView.hidden = YES;
        _returnBtnIV.hidden = YES;
        //        if (_contentView.hidden != YES) {
        //            [[UIApplication sharedApplication] setStatusBarHidden:YES];
        //        }
        
        [_topBarIV setHidden:YES];
        [_bottomBarIV setHidden:YES];
        //        _selectShareIV.hidden = YES;
        [AlertLabel AlertInfoWithText:@"锁定屏幕旋转" andWith:_contentView withLocationTag:1];
        
        
    }else{
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"screenLocked"];
        
        _contentView.userInteractionEnabled = YES;
        //        self.smallContentView.userInteractionEnabled = YES;
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_unlock"] forState:UIControlStateNormal];
        [AlertLabel AlertInfoWithText:@"开启屏幕旋转" andWith:_contentView withLocationTag:1];
        
    }
    
}




-(void)timeSystemStr:(NSTimer*)timer
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"HH:mm:ss"];
    
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    
    [formatter2 setDateFormat:@"HH:mm"];
    
    dateLiveLeft = [formatter dateFromString:liveStartTime];
    
    dateLiveMiddle = [formatter dateFromString:[self getHourMinuteAndSecond:[SystermTimeControl GetSystemTimeWith]]];
    
    dateLiveRight = [formatter dateFromString:liveEndTime];
    
    aTimer = [dateLiveMiddle timeIntervalSinceDate:dateLiveLeft];
    bTimer = [dateLiveRight timeIntervalSinceDate:dateLiveLeft];
    
    
    
   
    
    if ([timer.userInfo integerValue]!=0) {
        [self.myNewTimer invalidate];
        //            [self playTheNextVideo];
        
        [_livePlayerTimer invalidate];
        _currentTimeL.text = @"00:00:00";
        _totalTimeL.text = @"00:00:00";
        
        
        [_videoProgressV setValue:0 animated:YES];
        return;
    }else
    {
        
    
    if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle] <=0 && (liveStartTime.length<=0|| liveEndTime.length<=0)) {
        
        [self.myNewTimer invalidate];
        //            [self playTheNextVideo];
        
        [_livePlayerTimer invalidate];
        _currentTimeL.text = @"00:00:00";
        _totalTimeL.text = @"00:00:00";
    
        
        [_videoProgressV setValue:0 animated:YES];

        
//        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//        [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
        
        return;
        
        }else
        {
             liveTimeInterVal = aTimer/bTimer;
            
        }
    }

    
}


-(void)playPauseBigBtnPlay:(UIButton*)sender
{
    _playPauseBigBtn.hidden = YES;
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
    //----上面是改变按钮图标
    _isPaused = NO;//更换暂停标记
    [_moviePlayer play];
}

-(void)returnBackBtnAction:(UIButton*)sender
{
    
    switch (sender.tag) {
        case RETURNBIG:
            
        {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            
        }
            break;
        case RETURNSMALL:
        {
            
            if (USER_ACTION_COLLECTION == YES) {
                
                
                
                if (_errorManager.isAVLoading == YES) {
                    
                    _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
                    [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_serviceName andOPSCode:PlayerLoadingExitError andOPSST:[SystermTimeControl getNowServiceTimeString]];
                }
                
                
                [_errorManager resetPlayerStatusInfo];

                
                
                //采集EPG 退出小屏界面
                
                self.skimDuration = [self EPGtimeLengthForCurrent];
                
                [SAIInformationManager EPGinformationForChanelFenlei:_serviceID andChanelName:_serviceName andChanelInterval:_epgDateStr andSkimDuration:self.skimDuration andSkimTime:self.nowDateString];
                
                
                //end
                
                
                
                if (_isCollectionInfo) {
               
                
                if (_currentPlayType == CurrentPlayEvent) {
                    
                    NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                    
                    EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                    
                    [SAIInformationManager epgBackEventInformation:eventModel];
                    
                    
                    
                    
                    
                }
                if (_currentPlayType == CurrentPlayLive) {
                    
                    NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                    
                    EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                    
                    [SAIInformationManager epgLiveInformation:liveModel];
                    
                    }
                
                if (_currentPlayType == CurrentPlayTimeMove) {
                    NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",isEmptyStringOrNilOrNull(_dragLabel.text)?@"":_dragLabel.text,@"timeMoveTWT",nil];
                    
                    EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                    
                    [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
                }
                    
                    [_timeEPGManager reset];
                    _EpgCollectionDuration = nil;
                }
                
            }

            //本地的播放记录
            if (_currentPlayType == CurrentPlayEvent) {
                
                _localVideoModel.lvID = _serviceID;
                _localVideoModel.lvEventID = _playingEventID;
                _localVideoModel.lvServiceName = _theVideoInfo.channelName;
                _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                _localVideoModel.lvEventName = _theVideoInfo.videoName;
                _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
                
                [_localListCenter addReWatchPlayRecordWithModel:_localVideoModel andUserTye:IsLogin];
                [_localVideoModel resetLocalVideoModel];

            }
            
           
            if (_currentPlayType == CurrentPlayEvent) {
                if (IsLogin) {
                    [self recordPlaybackTime];
                }else{
                    //                [self recordLocalPlaybackTime];
                }
            }

            [self removeMoviePlayerNotification];

            if (_isLocationOrder == YES|| _isLocationOrderVOD) {
                
                [self.navigationController popToRootViewControllerAnimated:NO];
                
            }else
            {
                
                
                
                [self.navigationController popViewControllerAnimated:NO];
                
                
            }
        }
            
            break;
        default:
            break;
    }
    
    
    
}

-(void)changeLoginVerticalDirection
{
    if (_isVerticalDirection == NO) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
    }
}



#pragma mark --
#pragma mark fullWindowbutton
-(void)jumpToFullWindowToPlay:(UIButton*)sender
{
 
    if (sender.tag == 0) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [AlertLabel AlertInfoWithText:@"已切换至全屏模式" andWith:_contentView withLocationTag:1];
            
        });
    }else
    {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [AlertLabel AlertInfoWithText:@"已切换至小屏模式" andWith:_contentView withLocationTag:1];
            
        });
    }
    
    
    
    
    
}


-(NSString *)typeListArrForTitleString:(NSInteger)clickCategoryTag andWithTypeListArr:(NSMutableArray*)listArr
{
    NSString *titleCategory = @"";
    
   titleCategory =  [[listArr objectAtIndex:clickCategoryTag] objectForKey:@"shortName"];

    return titleCategory;
}





NSDate *startDateLive;

NSDate *endDateLive;
DTTimePeriod *firstPeriodLive;
-(void)buttonClickAction:(UIButton*)aButton
{
       WS(wself);
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    switch (aButton.tag) {
            
            
        case INTERACT:
        {
            _isClickedClear = NO;
            [self reStartHiddenTimer];
            [self hiddenPanlwithBool:YES];
            NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            if (remberIP.length<=0) {
                _interactView.hidden=!_interactView.hidden;
            }else
            {
                BOOL isExist = NO;//判断新获取的IP是否已经存在
                for (NSString *ip in _IPArray) {
                    if ([ip isEqualToString:remberIP]) {
                        isExist = YES;
                    }
                }
                
                if (!isExist) {
                    [_IPArray addObject:remberIP];
                    
                    //                    if (_IPArray.count <=0) {
                    //
                    //                        //            [AlertLabel AlertInfoWithText:@"未搜索到盒子,稍后请重试" andWith:_contentView withLocationTag:2];
                    //
                    //                    }
                    //
                    //
                    //                    //        _IPTableView.hidden = NO;
                    //                    _IPBackIV.hidden = NO;
                    
                }

                _IPBackIV.hidden = !_IPBackIV.hidden;
                [self reloadIPTableView];
            }
            _channelListView.hidden = YES;
//            _ADViewBase.hidden = YES;//cap
            _videoListView.hidden = YES;
            _selectClearIV.hidden = YES;
            _moreBackView.hidden = YES;
            
        }
            break;
        case CHANNEL_SELECT:
        {
            [self reStartHiddenTimer];
            [[UIApplication sharedApplication]setStatusBarHidden:YES];
            _clearButton.hidden = YES;
            _selectClearIV.hidden = YES;
            _moreBackView.hidden = YES;
            _isClickedClear = NO;
            _videoListView.hidden=!_videoListView.hidden;
            _screenLockedIV.hidden = YES;
//            _ADViewBase.hidden = !_ADViewBase.hidden;//cap
            //            _ADViewBase.hidden = NO;//cap
            
            //            NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            //            if (remberIP.length>2) {
            
            if (_videoListView.hidden == NO) {
                
                
                _MTButton.alpha = 0;
                _TVButton.alpha = 0;
                _RemoteButton.alpha = 0;
                
            }else
            {
                _MTButton.alpha = 1;
                _TVButton.alpha = 1;
                _RemoteButton.alpha = 1;
            }
            
            
            
            //            }
            
            
            
            _interactView.hidden = YES;
            _channelListView.hidden = YES;
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            
            _topBarIV.hidden = YES;
            _bottomBarIV.hidden = YES;
            //            _videoListArr = [self getFavourEPG:_allCategoryEPGServiceEventArr];
            //            [self.videoListTV reloadData];
            
//            _clickCategoryTag =[NSString stringWithFormat:@"%ld",_currentCatergoryType +2];
            
            
            if (_clickCategoryTag<=0) {
                if (IsLogin) {
                    
                    _clickCategoryTag = 0;
                }else
                {
//                    UserCollectCenter *collectCenter = [UserCollectCenter defaultCenter];
//                    if ( [collectCenter checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                        _clickCategoryTag = 0;
//                        
//                    }else
//                    {
//                        
//                        _clickCategoryTag = 1;
//                    }
                    
                    //ww...
                    if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES]) {
                        
                        _clickCategoryTag = 0;
                    }else{
                    
                        _clickCategoryTag = 1;
                    }
                }
                
            }
            
            
            
            if (_clickCategoryTag  == 0) {
                
                _videoListArr = [self getFavourEPG:_allCategoryEPGServiceEventArr];
                
                
            }else if (_clickCategoryTag  == 1)
            {
                _unFavourBV.hidden = YES;
                _videoListArr = [self EPGGetAllCategory:_dicArr];
                
            }
            else
            {
                _unFavourBV.hidden = YES;
                _videoListArr = [self EpgGetSingleCategory:_dicArr andType:[NSString stringWithFormat:@"%ld",_clickCategoryTag -1]];
            }
            [_videoListTV reloadData];
            
            [_epgChannelSV setClickCategoryTag:_clickCategoryTag];
            
            _clickCategoryString = [self typeListArrForTitleString:_clickCategoryTag andWithTypeListArr:typelistARR];
            NSLog(@"889988%@",[self typeListArrForTitleString:_clickCategoryTag andWithTypeListArr:typelistARR]);
//            NSLog(@"scroll点击的是什么分类%@",)
//            _epgChannelSV.clickCategoryTitle

//            [self categoryBtnSelectedCurrentCategoryTpe:_clickCategoryTag];
            
//            if (_videoListArr.count>0) {
//                
//                for (int i = 0 ; i<_videoListArr.count; i++) {
//                    
//                    NSDictionary *dic = [[NSDictionary alloc]init];
//                    
//                    dic = [_videoListArr objectAtIndex:i];
//                    
//                    NSLog(@"&&&&&&&%@",[dic objectForKey:@"categoryServiceID"]);
//                    if (!_currentPlayServiceID) {
//                        _currentPlayServiceID = _serviceID;
//                    }
//                    
//                    if ( [_currentPlayServiceID isEqualToString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryServiceID"]]]) {
//                        //                            _videoPlayPosition = i;
//                        _videolistSelectIndex = i;
//                        _theVideoInfo.channelImage = [NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryLevelImage"]];
//                        if (_videoListArr.count >_videolistSelectIndex)
//                        {
//                            [_videoListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
//                        }
//                    }
//                    
//                }
//                
//            }
            
            
            if (SUPPORT_AD) {
                
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request ADInfoWithADCode:PANGEA_PHONE_LIVE_PLAYER_MENU andType:AD_LIVETYPE andAssetID:_serviceID andAssetName:_serviceName andGroup:AD_GroupID andTimeout:KTimeout andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {
                _ADViewBase.hidden = NO;
                _ADViewBase.adplayerMenuType = ADPLAYER_MENULive;
                [_ADViewBase setUpWithDiction:result WithIsHidden:NO];
                
            }];
            
            
            }
            
        }
            break;
            
            
        case FAVOUR:
            
        {
//            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
            if (aButton.selected) {
                
                if (IsLogin) {
                    
                    
//                    if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID] == YES) {
//                        
////                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
////                        [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
////                        
////                        aButton.selected = YES;
//                        
//                        [request AddTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
//                        
//                    }else
//                    {
////                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
////                        [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
////                        
////                        
////                        aButton.selected = NO;
//                        
//                        [request DelTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
//                        
//                    }
                    
                    //ww...
                    if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES] == YES) {
                        
                        [request AddTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    }else{
                        [request DelTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    }
                    
                    
                   aButton.selected = YES;
                    
                }else
                {
                    if (isAuthCodeLogin) {
                        _isPaused = YES;

                        [self actionPauseForVideo];
                        [self changeLoginVerticalDirection];
                        UserLoginViewController *user = [[UserLoginViewController alloc] init];
                        user.loginType = kVideoLogin;
                        [self.view addSubview:user.view];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                        user.backBlock = ^()
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self actionPlayForVideo];
                        };
                        
                        user.tokenBlock = ^(NSString *token)
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self loginAfterEnterVideo];
                            [self actionPlayForVideo];
                        };
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        return;
                    }else
                    {
                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    }
                }
                
            }else
            {
                if (IsLogin) {
                    //已收藏
                    
                    
//                    if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID] == YES) {
////                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
////                        [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
////                        
////                        
////                        aButton.selected = NO;
//                        
//                        [request DelTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
//                        
//                    }else
//                    {
////                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
////                        [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
////                        
////                        aButton.selected = YES;
//                        
//                        [request AddTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
//                    }
                    
                    //ww...
                    if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES] == YES) {
                        
                        [request DelTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    }else{
                        [request AddTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    }
                    
                    aButton.selected = NO;
                    
                }else
                {
                    if (isAuthCodeLogin) {
                        _isPaused = YES;

                        [self actionPauseForVideo];
                        [self changeLoginVerticalDirection];
                        UserLoginViewController *user = [[UserLoginViewController alloc] init];
                        user.loginType = kVideoLogin;
                        [self.view addSubview:user.view];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                        user.backBlock = ^()
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self actionPlayForVideo];
                        };
                        
                        user.tokenBlock = ^(NSString *token)
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self loginAfterEnterVideo];
                            [self actionPlayForVideo];
                        };
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        return;
                    }else
                    {
                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    }
                }
                
                
            }
            
            
            
            
        }
            
            break;
            
        case VIDEO_LIST:
        {
            _clearButton.hidden = YES;
            [self reStartHiddenTimer];
            [[UIApplication sharedApplication]setStatusBarHidden:YES];
            _moreBackView.hidden = YES;
            _channelListView.hidden=!_channelListView.hidden;
            _screenLockedIV.hidden = YES;
         
            
            if (_dateType == EPG_Date_week) {
                
                [self setWeekdays:[NSMutableArray arrayWithObjects:@"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil]];
            }else
            {
            
            [self setWeekdays:[NSMutableArray arrayWithObjects:@"周四", @"周五", @"周六",@"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil]];
            }

//            _ADViewBase.hidden = !_ADViewBase;//cap

            //            if (remberIP.length>2) {
//            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
////            [request EPGSHOWWithServiceID:_serviceID andDate:_currentDateStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
            if (_channelListView.hidden == NO) {
                
                
                _MTButton.alpha = 0;
                _TVButton.alpha = 0;
                _RemoteButton.alpha = 0;
                
            }else
            {
                _MTButton.alpha = 1;
                _TVButton.alpha = 1;
                _RemoteButton.alpha = 1;
            }
            
            
//            if (_channelListArr.count>0) {
//                [[self.view viewWithTag:8080]setHidden:YES];
//                
//            }else
//            {
////                [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?YES:NO];
////                [[self.view viewWithTag:8080]setHidden:NO];
//                if (_isVerticalDirection == NO) {
//                    [[self.view viewWithTag:8080]setHidden:NO];
//
//                }else
//                {
//                    [[self.view viewWithTag:8080]setHidden:YES];
//                }
//            }
            
            
            
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
//            if (_channelListView.hidden) {//cap
//                _ADViewBase.hidden = YES;
//            }else{
//                _ADViewBase.hidden = NO;
//            }
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _selectClearIV.hidden = YES;
            _isClickedClear = NO;
            _topBarIV.hidden = YES;
            _bottomBarIV.hidden = YES;
            
            
            
//            if (_currentPlayType == CurrentPlayLive) {
            
                if (_channelListArr.count>0) {
                    
                    for (int i = 0 ; i<_channelListArr.count; i++) {
                        
                        NSDictionary *dic = [[NSDictionary alloc]init];
                        
                        dic = [_channelListArr objectAtIndex:i];
                        
                        NSLog(@"&&&&&&&%@",[dic objectForKey:@"id"]);
                        
                        if ( [_currentPlayID isEqualToString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]]]) {
                            //                            _videoPlayPosition = i;
                            _channelSelectIndex = i;
                            if (_channelListArr.count >_videoPlayPosition)
                            {
                                [_channelListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                                [_EPGListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                            }
                        }
                        
                    }
                    
                }
                
                
            [_EPGListTableView reloadData];
            [_channelListTV reloadData];
            
//            }
            
            
            if (SUPPORT_AD == YES) {
             
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request ADInfoWithADCode:PANGEA_PHONE_LIVE_PLAYER_MENU andType:AD_LIVETYPE andAssetID:_serviceID andAssetName:_serviceName andGroup:AD_GroupID andTimeout:KTimeout andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {
                if (status == 0) {
                    
                    _ADViewBase.hidden = NO;
                    //                    - (void)setUpWithDiction:(NSDictionary *)diction;
                    _ADViewBase.adplayerMenuType = ADPLAYER_MENULive;

                    [_ADViewBase setUpWithDiction:result];
                }else
                {
                    _ADViewBase.hidden = YES;
                }

                
            }];

            }
            
        }
            break;
            
            
        case MORE:
        {
            [self reStartHiddenTimer];
            _isClickedClear = NO;
            _moreBackView.hidden = !_moreBackView.hidden;
            _videoListView.hidden = YES;
//            if (_channelListView.hidden) {//cap
//                _ADViewBase.hidden = YES;
//            }else{
//                _ADViewBase.hidden = NO;
//            }
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _interactView.hidden = YES;
            
            _selectClearIV.hidden = YES;
            
            if (IsLogin) {
                
//                if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                    
//                    //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                    //            [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
//                    [self KFavorateBtnisFaved:YES];
//                    
//                    _favourButton.selected = YES;
////                    _favourLabel.selected = YES;
//                    
//                    
//                }else
//                {
//                    
//                    //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                    //            [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
//                    [self KFavorateBtnisFaved:NO];
//                    
//                    _favourButton.selected = NO;
////                    _favourLabel.selected = NO;
//                }
                
                
                //ww...
                if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES] == YES) {
                    
                    [self KFavorateBtnisFaved:YES];
                    
                    _favourButton.selected = YES;
                }else{
                    [self KFavorateBtnisFaved:NO];
                    
                    _favourButton.selected = NO;
                }

                
            }else
            {
                
                //        [LocalCollectAndPlayRecorder  che]
                
                
            }

            
            
            
            
        }
            
            break;
            
        case CLEAR:
        {
            [self reStartHiddenTimer];
            
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
//            if (_channelListView.hidden) {//cap
//                _ADViewBase.hidden = YES;
//            }else{
//                _ADViewBase.hidden = NO;
//            }

            _moreBackView.hidden = YES;
            
            if (_isClickedClear == NO) {
                _selectClearIV.hidden = NO;
                _isClickedClear = YES;
            }else{
                _selectClearIV.hidden = YES;
                
                _isClickedClear = NO;
                
            }
            
            clearTitles = [self getCodeRateCategoryWithLivType:1];
            
            
            
            if (clearTitles.count>0) {
                
                
                
                [_selectClearIV mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-40*kRateSize);
                    make.width.mas_equalTo(50*kRateSize);
                    make.height.mas_equalTo(clearTitles.count*30*kRateSize);
                    make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-15*kRateSize);
                }];
                
                
                [_selectClearIV removeAllSubviews];
                for (int i = 0; i<clearTitles.count; i++) {
                    
                    
                    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    playButton.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
                
                    [playButton setBackgroundColor:[UIColor clearColor]];
                     [playButton setTitleColor:UIColorFromRGB(0xeeeeee) forState:UIControlStateNormal];
                    playButton.tag = [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] intValue];
                    playButton.showsTouchWhenHighlighted = YES;
                    [playButton setTitle:[NSString stringWithFormat:@"%@",[[clearTitles objectAtIndex:i] valueForKey:@"type"]] forState:UIControlStateNormal];
                    
                    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"]== [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] integerValue]) {
                        [playButton setTitleColor:App_white_color forState:UIControlStateNormal];
                        [playButton setBackgroundImage:[UIImage imageNamed:@"bg_coderate_btn_click"] forState:UIControlStateNormal];
                        playButton.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
                    }
                    
                    
            
                    [playButton addTarget:self action:@selector(clearCodeRateAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [_selectClearIV addSubview:playButton];
                    
                    [playButton mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_selectClearIV.mas_top).offset(30*(kRateSize*i));
                        //            make.left.mas_equalTo(@0);
                        make.left.mas_equalTo(_selectClearIV.mas_left);
                        make.right.mas_equalTo(_selectClearIV.mas_right);
                        make.height.mas_equalTo(30*kRateSize);
//                        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 30*kRateSize));
                        
                        
                    }];
                    
                    
                    
                }
                
                
            }else
            {
                _clearButton.hidden = YES;
                [AlertLabel AlertSSIDInfoWithText:@"暂无多码率" andWith:self.view withLocationTag:2];
                return;
            }
            
            
        }
            
            break;
            
            
            
        case SHARE:
        {
            [self reStartHiddenTimer];
            [self hiddenPanlwithBool:YES];
            NSString *url = [KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:_currentPlayType];

            if (url.length <=0) {
                
                [AlertLabel AlertInfoWithText:@"服务器正在打盹，请稍后再试" andWith:self.view withLocationTag:2];
                return;
            }
            _shareView.hidden = !_shareView.hidden;
            _shareView.installedWX = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
            _shareView.installedQQ = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ];
            
            _shareView.returnBlock = ^(NSInteger tag) {
                [[wself.view viewWithTag:4040] setHidden:YES];

                NSString *tempLink;
                
                if (!isEmptyStringOrNilOrNull([KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:wself.currentPlayType])) {
                    tempLink = [KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:wself.currentPlayType];
                }else{
                    tempLink = @"";
                }
                
                NSString* thumbURL = NSStringPM_IMGFormat(wself.theVideoInfo.channelImage);

                
                NSString *name;
                
                if (_theVideoInfo.channelName.length>0)
                {
                    name = _theVideoInfo.channelName;
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"获取节目失败，暂无法分享" andWith:wself.contentView withLocationTag:2];
                }
                
                NSString *EPGName;
                if (wself.theVideoInfo.videoName) {
                    EPGName = wself.theVideoInfo.videoName;
                }else{
                    EPGName = @"";
                }
                
                NSString *contentString = [NSString stringWithFormat:@"我正在看%@播放的%@，太精彩了！安装#%@#手机客户端，电视直播回看免费看啦！快快安装一起看吧。",name,EPGName,kAPP_NAME];
                
                
                switch (tag) {
                    case 0:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechat currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];
                            }
                        };
   
                    }
                        break;
                    case 1:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechatZone currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };
                    }
                        break;
                    case 2:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQ currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };
                    }
                        break;
                    case 3:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQZone currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };
                    }
                        break;
                        
                    default:
                        break;
                }
            };
            
            _IPBackIV.hidden = YES;

            _interactView.hidden = YES;
            _videoListView.hidden = YES;
//            if (_channelListView.hidden) {//cap
//                _ADViewBase.hidden = YES;
//            }else{
//                _ADViewBase.hidden = NO;
//            }
            _selectClearIV.hidden = YES;
            
            
           
            
            
            
            
            
        }
            break;
            
        case PLAY:
        {
            //            // 创建集合.
            //            collectionLive = [DTTimePeriodCollection collection];
            //
            //            // 创建时间段
            //            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            //            [dateFormatter setDateFormat: @"YYYY-MM-dd HH:mm:ss"];
            
            _isClickedClear = NO;
            
            if (_isPaused) {
                //----改变按钮图标
                [aButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
                //----上面是改变按钮图标
                _isPaused = NO;//更换暂停标记
                _playPauseBigBtn.hidden = YES;
                //                endDateLive = [SystermTimeControl GetSystemTimeWith];
                [_moviePlayer play];
                
            }else
            {
                [aButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
//#warning 暂时这么写
    
                if (self.moviePlayer.duration<=0) {
                    
                    return;
                }else
                {
                    _isPaused = YES;//改变暂停标记
                    [_moviePlayer pause];//暂停视频
                    _playPauseBigBtn.hidden = NO;
                }
                
                //                startDateLive = [SystermTimeControl GetSystemTimeWith];
                [self hiddenPanlwithBool:YES];
                if (_currentPlayType == CurrentPlayEvent) {
                    if (IsLogin) {
                        [self recordPlaybackTime];
                    }else{
                        //                [self recordLocalPlaybackTime];
                    }
                }

                if (_currentPlayType != CurrentPlayTimeMove) {
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    [request ADInfoWithADCode:PANGEA_PHONE_SOTV_PAUSE andType:AD_LIVETYPE andAssetID:_serviceID andAssetName:_serviceName andGroup:AD_GroupID andTimeout:KTimeout andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {
                        if (status == 0) {
                            
                            if (_isVerticalDirection == YES) {
                                _ADViewPauseBase.hidden = YES;
                            }else
                            {
                                _ADViewPauseBase.hidden = NO;
                                _ADViewPauseBase.adplayerMenuType = ADPLAYER_MENUPause;
                            }
                            [_ADViewPauseBase setUpWithDiction:result WithIsHidden:NO];
                            
                            
                        }else
                        {
                            _ADViewPauseBase.hidden = YES;
                        }
                        
                    }];
  
                }
                
                
            }
            //            firstPeriodLive = [DTTimePeriod timePeriodWithStartDate:startDateLive endDate:endDateLive];
            //            // 把时间段添加到集合中.
            //            [collectionLive addTimePeriod:firstPeriodLive];
            //
            //            ZTTimeIntervalLive +=[collectionLive durationInSeconds];
            
            
            
            
          
            
            
            
        }
            break;
            
        case TVTAG:
        {
            _isClickedClear = NO;
            
            //            [aButton setImage:[UIImage imageNamed:@"btn_player_push_screen_pressed"] forState:UIControlStateNormal];
            //
            NSString * nStr =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"pointToPointIP"]];
            
            if (nStr.length<7) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                
                return;
            }
            
            [AlertLabel AlertInfoWithText:@"推屏啦~" andWith:self.view withLocationTag:0];
            //IP不为空的时候
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            NSString *command ;
            NSDictionary *video ;
            video = [_channelListArr objectAtIndex:_videoPlayPosition];
            
            
            if (_theVideoInfo.contentIDP.length >0) {
                
                NSString * contentID = _theVideoInfo.contentIDP;
                
                
                NSString * name = [NSString stringWithFormat:@"%@",_serviceTitleLabel.text];
                NSString *breakpoint = [NSString stringWithFormat:@"%ld",[self recordPlaybackTime]];
                dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)breakpoint],@"seekPos",name,@"name", nil];
                command = @"DLNAPLAY_VOD";
                
            }
            
            
            if (_currentPlayType == CurrentPlayLive) {
                
                dic = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",_serviceTitleLabel.text] forKey:@"channelName"];
                command = @"DLNAPLAY_LIVE";
                
            }
            
            if (_currentPlayType == CurrentPlayEvent)
                
            {
                
                command = @"DLNAPLAY_EVENT";
                
                NSString *timeV;
                
                
                
                timeV = [[[video objectForKey:@"fullStartTime"] componentsSeparatedByString:@" "] objectAtIndex:0];
                
                
                NSString *breakpoint = [NSString stringWithFormat:@"%ld",[self recordPlaybackTime]];//这里可以用进度条的值value/maxValue来计算 这样会避免崩掉的现象
                
                NSString *startDate;
                NSString *endDate;
                
                if ([startDate isEqualToString:@""]) {
                    
                    startDate = [NSString stringWithFormat:@"%@",_theVideoInfo.fullStartTimeP];
                }else
                {
                    startDate = [NSString stringWithFormat:@"%@",[video objectForKey:@"fullStartTime"]];
                }
                
                if ([endDate isEqualToString:@""]) {
                    
                    endDate = [NSString stringWithFormat:@"%@",_theVideoInfo.endTimeP];
                }else
                {
                    endDate =  [NSString stringWithFormat:@"%@ %@",timeV,[video objectForKey:@"endTime"]];
                }
                
                
                
                
                //                NSString *startDate = [NSString stringWithFormat:@"%@",[video objectForKey:@"fullStartTime"]];
                //                NSString *endDate = [NSString stringWithFormat:@"%@ %@",timeV,[video objectForKey:@"endTime"]];
                
                //         NSString *startDate = [NSString stringWithFormat:@"%@",_theVideoInfo.videoTime];
                //         NSString *endDate = [NSString stringWithFormat:@"%@",_theVideoInfo.videoEndTime];
                
                
                dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",_serviceTitleLabel.text],@"channelName",startDate,@"startDate",endDate,@"endDate",breakpoint,@"seekPos", nil];
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                
                command = @"DLNAPLAY_PAUSE";
                NSString *channalName = _theVideoInfo.channelName;
                NSString *seekPos = [NSString stringWithFormat:@"%ld",(long)_MoveTimeInteger];
                NSString *UTCTime = [NSString stringWithFormat:@"%lld",(long long) [[SystermTimeControl GetSystemTimeWith] timeIntervalSince1970]*1000];
                
                dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:channalName,@"channelName",seekPos,@"seekPos",UTCTime,@"UTCTime", nil];
                
            }
            
            NSDictionary *resultDic=[NSDictionary dictionaryWithObjectsAndKeys:command,@"@command",dic,@"param", nil];
            NSLog(@"甩屏信息 ： %@",resultDic);
            if (nStr.length>0) {
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:resultDic];
                
            }else if (nStr.length < 6)
            {
                
                [AlertLabel AlertInfoWithText:@"暂无可连接设备" andWith:self.view withLocationTag:0];
                _IPTableView.hidden = NO;
                _IPBackIV.hidden = NO;

                
            }
            
        }
            break;
            
        case MTTAG:
        {
            
            //            [aButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_pressed"] forState:UIControlStateNormal];
            _isClickedClear = NO;
            
            NSDictionary *daramDic;
            daramDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_REQUEST",@"@command",nil];
            
            
            NSString * nStr =  [[NSUserDefaults standardUserDefaults]objectForKey:@"pointToPointIP"];
            if (nStr.length>2&&nStr!=nil) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:daramDic];
                [AlertLabel AlertInfoWithText:@"拉屏啦~" andWith:self.view withLocationTag:0];
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                
                
            }else if (nStr.length == 0)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                
                //                _IPTableView.hidden = NO;
                return;
                
            }
            
            
            
            
        }
            break;
            
        case REMOTECONTROL:
        {
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length < 10) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [self showSearchDeviceLoadingView];
                
                break;
            }else{
                
                [self reloadIPTableView];
                
                break;
            }
        }
            
            break;
            
            
            
        default:
            break;
    }
    
    
    
    
}




-(NSString *)getChannelIMage:(NSMutableArray*)videoListArr
{
    NSString *ChannelImage;
    
    if (videoListArr.count>0) {
        
        for (int i = 0 ; i<videoListArr.count; i++) {
            
            NSDictionary *dic = [[NSDictionary alloc]init];
            
            dic = [videoListArr objectAtIndex:i];
            //            if (!_currentPlayServiceID) {
            //                _currentPlayServiceID = _serviceID;
            //            }
            
            if ( [_serviceID isEqualToString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryServiceID"]]]) {
                //                            _videoPlayPosition = i;
                
                ChannelImage = [NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryLevelImage"]];
                
                break;
            }
            
        }
        
    }
    
    return ChannelImage;
}


-(NSString *)getChannelName:(NSMutableArray*)videoListArr
{
    NSString *ChannelName;
    
    if (videoListArr.count>0) {
        
        for (int i = 0 ; i<videoListArr.count; i++) {
            
            NSDictionary *dic = [[NSDictionary alloc]init];
            
            dic = [videoListArr objectAtIndex:i];
            //            if (!_currentPlayServiceID) {
            //                _currentPlayServiceID = _serviceID;
            //            }
            
            if ( [_serviceID isEqualToString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryServiceID"]]]) {
                //                            _videoPlayPosition = i;
                
                ChannelName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"categoryServiceName"]];
                
                break;
            }
            
        }
        
    }
    
    return ChannelName;
}




-(void)clearCodeRateAction:(UIButton*)sender
{
    [[self.view viewWithTag:4040] setHidden:NO];
    [self hiddenPanlwithBool:YES];
    if ((NSInteger)_moviePlayer.currentPlaybackTime >0) {
        JumpStartTime = (NSInteger)(_moviePlayer.currentPlaybackTime);
    }
    
    switch (sender.tag) {
        case SUPER_CLEAR:
        {
            PlayTVType = 1;
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:SUPER_CLEAR forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_clearButton setTitle:@"超清" forState:UIControlStateNormal];
            
            //            JumpStartTime = _moviePlayer.currentPlaybackTime;
            //            [_moviePlayer stop];
            //            _allCodeRateLink = @"";
            
            
            switch (_currentPlayType) {
                    
                case 0:
                {
                    
                    [self clearMoviePlayerStop];
                    [self playVideoModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:1];
                }
                    break;
                case 1:
                {
                    
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:1];
                }
                    break;
                case 2:
                {//精简
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:1];
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
            
            
            
            
        }
            break;
        case HIGH_DEFINE:
        {
            PlayTVType = 2;
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:HIGH_DEFINE forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"高清" forState:UIControlStateNormal];
            
            //            JumpStartTime = _moviePlayer.currentPlaybackTime;
            //            [_moviePlayer stop];
            //            _allCodeRateLink = @"";
            switch (_currentPlayType) {
                case 0:
                {//精简
                    
                    [self clearMoviePlayerStop];
                    [self playVideoModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:2];
                    
                }
                    break;
                case 1:
                {
                    
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:2];
                    
                }
                    break;
                case 2:
                {//精简
                   
                    [self clearMoviePlayerStop];
                    [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:2];
                    
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case ORIGINAL_PAINT:
        {
            PlayTVType = 3;
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:ORIGINAL_PAINT forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"标清" forState:UIControlStateNormal];
            //            NSString *breakpoint =   [[UserPlayRecorderCenter defaultCenter]checkTheVODListWithContentID:_demandModel.contentID];
            //            JumpStartTime = [breakpoint integerValue];
            //            JumpStartTime = _moviePlayer.currentPlaybackTime;
            
            //            [_moviePlayer stop];
            //            _allCodeRateLink = @"";
            switch (_currentPlayType) {
                case 0:
                {
                   
                    [self clearMoviePlayerStop];
                    [self playVideoModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:3];
                    
                }
                    break;
                case 1:
                {
                   
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:3];
                }
                    break;
                case 2:
                {
                    
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:3];
                    
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case SMOOTH:
        {
            PlayTVType = 4;
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:SMOOTH forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
            
            //            NSString *breakpoint =   [[UserPlayRecorderCenter defaultCenter]checkTheVODListWithContentID:_demandModel.contentID];
            //            JumpStartTime = [breakpoint integerValue];
            //            JumpStartTime = _moviePlayer.currentPlaybackTime;
            //            [_moviePlayer stop];
            //            _allCodeRateLink = @"";
            switch (_currentPlayType) {
                case 0:
                {
                                        [self clearMoviePlayerStop];
                    [self playVideoModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:4];
                }
                    break;
                case 1:
                {
                    
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:4];
                }
                    break;
                case 2:
                {
                   
                    [self clearMoviePlayerStop];
                    _allCodeRateLink = @"";
                    [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:4];
                    
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }        }
            break;
            
            
        default:
            break;
    }
}



/**
 *  TableViewDelegate
 *
 *  @param
 */


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _EPGListTableView) {
        return _channelListArr.count;
    } else if (tableView == _videoListTV) {
        return  _videoListArr.count;
    }else if (tableView == _channelListTV){
        
        return _channelListArr.count;
    }else
    {
        return _IPArray.count;
    }
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _EPGListTableView) {
        return 47*kRateSize;
    }
    else if (tableView == _videoListTV) {
        return 47*kRateSize;
    }else if (tableView == _channelListTV)
    {
        return 47*kRateSize;
        
    }else if (tableView == _IPTableView)
    {
        return 30*kRateSize;
    }else
    {
        return 0;
    }
    
}


//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//
//
//    if (tableView == _IPTableView) {
//
//        if (section == 0)
//
//        {
//
//
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
//            [button setTitle:@"连接设备列表" forState:UIControlStateNormal];
//
//            [button setTitleColor:[UIColor grayColor]forState:UIControlStateNormal];
//
//            return button;
//
//
//
//        }
//
//
//    }
//
//
//    return nil;
//
//
//}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self reStartHiddenTimer];
    
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    _polyLoadingView.hidden= NO;
    if (tableView == _EPGListTableView) {
        EPGListTableViewCell *Ecell = [tableView dequeueReusableCellWithIdentifier:@"Ecell"];
        if (Ecell==nil) {
            Ecell = [[EPGListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Ecell"];
        }
        Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
       
        
        
//        if (indexPath.row == _channelSelectIndex) {
//            //            Ecell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];
//            
//            [Ecell setSelectedContentViewBacogrundColor];
//            Ecell.startTime.textColor = App_selected_color;
//            Ecell.eventName.textColor = App_selected_color;
//            
//            
//        }else
//        {
//            
//            [Ecell setUnselectedContentViewbackgrundColor];
//            Ecell.startTime.textColor = UIColorFromRGB(0x333333);
//            Ecell.eventName.textColor = UIColorFromRGB(0x333333);
//            //            Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
//        }
//
        
        Ecell.startTime.textColor =APP_TirdBlack_color;
        Ecell.eventName.textColor = APP_TirdBlack_color;

        
        Ecell.backgroundColor = [UIColor clearColor];
        [Ecell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //        [_channelListTV scrollToRowAtIndexPath:[NSIndexPath
        
        NSDictionary *dic = [[NSDictionary alloc]init];
        dic = [_channelListArr objectAtIndex:indexPath.row];
        
        
        //        _theVideoInfo.videoName = [dic valueForKey:@"eventName"];
        _theVideoInfo.PlayBackTag = [dic valueForKey:@"playType"];
        _theVideoInfo.videoID = [dic valueForKey:@"id"];
        
        Ecell.startTime.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"startTime"]];
        Ecell.eventName.text =[NSString stringWithFormat:@"%@",[dic valueForKey:@"eventName"]];
        
        //        cell.playBtn.tag=indexPath.row;//用tag值来记录行数

        //        Ecell.playBtn = indexPath.row;
        Ecell.playBtn.Click = ^(UIButton *btn,NSString *name){
            //这里执行相关 点击操作
            //            [weakCell setPlayingBackImage];
            //下面是 播放操作
            
            [self tableView:_EPGListTableView didSelectRowAtIndexPath:indexPath];
            
        };
        
        
        NSLog(@"^&^&^*^%@",_playingEventID);
        //播放传值
        if ([self.fullStartTimeStr isEqualToString:[self getTodayStringWithDate]]) {
            NSInteger mark=[self isPlayingWith:[dic objectForKey:@"startTime"] endTime:[dic objectForKey:@"endTime"]];
            if (mark==-1) {
                
                if (_isLookPlayBackEvent == YES) {
                    //回看
                    if ([_theVideoInfo.PlayBackTag integerValue]==0) {
                        Ecell.playBtn.hidden=YES;
                    }else{
                        Ecell.playBtn.hidden=NO;
                    }
                    
                    if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                        [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_backing"] forState:UIControlStateNormal];
                        
                        Ecell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];
                        Ecell.startTime.textColor = App_selected_color;
                        Ecell.eventName.textColor = App_selected_color;
                    }else{
                        [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_back"] forState:UIControlStateNormal];
                        
                        Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                        Ecell.startTime.textColor = APP_TirdBlack_color;
                        Ecell.eventName.textColor = APP_TirdBlack_color;
                    }
                    
                }else
                {
                    Ecell.playBtn.hidden=YES;
                    
                }
                //点击事件中判定是否可以回看
                
            }else if (mark==0){
                //直播
                Ecell.playBtn.hidden=NO;
                
                if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_live_playing"] forState:UIControlStateNormal];
                    Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    Ecell.startTime.textColor =App_selected_color;
                    Ecell.eventName.textColor = App_selected_color;
                    
                    
                }else{
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_live_play"] forState:UIControlStateNormal];
                    Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    Ecell.startTime.textColor =APP_TirdBlack_color;
                    Ecell.eventName.textColor = APP_TirdBlack_color;
                    
                    
                }
            }else if (mark==1){
                Ecell.playBtn.hidden=NO;
//                BOOL didBooked =[[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:[dic objectForKey:@"id"]];
                BOOL didBooked = [_localListCenter checkTheScheduledListwithID:[dic objectForKey:@"id"]];

                if (didBooked) {
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateNormal];
                    
                }else{
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
                    
                }
                
            }
            
            
        }else if ([self.fullStartTimeStr compare:[self getTodayStringWithDate]]<0){
            //如果是今天之前执行下面操作
            //昨天
            
            if (_isLookPlayBackEvent == YES) {
                if ([_theVideoInfo.PlayBackTag integerValue]==0) {
                    Ecell.playBtn.hidden=YES;
                }else{
                    Ecell.playBtn.hidden=NO;
                }
                
                if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_backing"] forState:UIControlStateNormal];
                    
                    Ecell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];
                    Ecell.startTime.textColor = App_selected_color;
                    Ecell.eventName.textColor = App_selected_color;
                }else{
                    [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_back"] forState:UIControlStateNormal];
                    
                    Ecell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    Ecell.startTime.textColor = APP_TirdBlack_color;
                    Ecell.eventName.textColor = APP_TirdBlack_color;
                }
                
            }else
            {
                Ecell.playBtn.hidden = YES;
                
            }
            
            
            
            
        }else{
            //如果是今天之后执行
            Ecell.playBtn.hidden=NO;
//            BOOL didBooked =[[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:[dic objectForKey:@"id"]];
            BOOL didBooked = [_localListCenter checkTheScheduledListwithID:[dic objectForKey:@"id"]];
            
            if (didBooked) {
                [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateNormal];
                
            }else{
                [Ecell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
                
            }
            
        }
        
        return Ecell;
        
    }
    else if (tableView == _videoListTV) {
        VideoListTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:kMASCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[VideoListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMASCellReuseIdentifier];
            
        }
        
                cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        NSMutableArray *videoDic = [[NSMutableArray alloc]init];
        
        
        videoDic = [_videoListArr objectAtIndex:indexPath.row];
        
        
        if ([_currentPlayServiceID isEqualToString:[videoDic valueForKey:@"categoryServiceID"]]) {
            
            cell.channelNameLabel.textColor = App_selected_color;
            cell.channelTitleLabel.textColor = App_selected_color;
            
            
        }else
        {
            cell.channelNameLabel.textColor = UIColorFromRGB(0x999999);
            cell.channelTitleLabel.textColor = UIColorFromRGB(0x999999);
        }
        
        
        
        NSLog(@"nnn======%@",[videoDic valueForKey:@"categoryServiceName"]);
        
        //        cell.backgroundColor = [UIColor clearColor];
        //        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        //        [cell.backButton addTarget:self action:@selector(mmmCLick) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[videoDic valueForKey:@"categoryServiceName"] length]>0) {
            cell.channelNameLabel.text = [NSString stringWithFormat:@"%@",[videoDic valueForKey:@"categoryServiceName"]];
            
        }else
        {
            cell.channelNameLabel.text = @"暂无节目信息";
        }
        
        
        if ([[videoDic valueForKey:@"categoryServiceEventName"] length]>0) {
            cell.channelTitleLabel.text = [NSString stringWithFormat:@"%@",[videoDic valueForKey:@"categoryServiceEventName"]];
        }else
        {
            cell.channelTitleLabel.text = @"暂无节目信息";
        }
        
        
//        //        [cell.channelLogo setImage:[UIImage imageNamed:@"channelLogoPlacehoder"]];
//        NSBlockOperation *downLoadImage = [NSBlockOperation blockOperationWithBlock:^{
//            NSLog(@"正在下载**********");
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[videoDic valueForKey:@"categoryLevelImage"]]]];
//            UIImage *channelImage = [UIImage imageWithData:imageData];
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                //                [cell.channelLogo setImage:channelImage];
//                cell.channelLogo.image = channelImage;
//            }];
//            
//            
//        }];
//        
//        [opQueue addOperation:downLoadImage];
        
//        [cell.channelLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[videoDic valueForKey:@"categoryLevelImage"]]] placeholderImage:[UIImage imageNamed:@"bg_home_hotchannel_onloading"]];
//       [cell.channelLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGPORTAL,[videoDic valueForKey:@"categoryLevelImage"]]] placeholderImage:[UIImage imageNamed:@"bg_home_hotchannel_onloading"]];
       [cell.channelLogo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([videoDic valueForKey:@"categoryLevelImage"])] placeholderImage:[UIImage imageNamed:@"bg_home_hotchannel_onloading"]];
        
        return cell;
    }else if (tableView == _channelListTV)
    {
        
        ChannelListTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:kMMSCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[ChannelListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMMSCellReuseIdentifier];
            //            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            
        }
        //
        cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
        cell.timeLabel.textColor =UIColorFromRGB(0x999999);
        cell.videoTitleLabel.textColor = UIColorFromRGB(0x999999);
        
        
        cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //        [_channelListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        NSDictionary *dic = [[NSDictionary alloc]init];
        
        dic = [_channelListArr objectAtIndex:indexPath.row];
        
        
        //        _theVideoInfo = [[VideoInfo alloc]init];
        
        //        _theVideoInfo.videoName = [dic valueForKey:@"eventName"];
        _theVideoInfo.PlayBackTag = [dic valueForKey:@"playType"];
        _theVideoInfo.videoID = [dic valueForKey:@"id"];
        
        cell.timeLabel.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"startTime"]];
        cell.videoTitleLabel.text =[NSString stringWithFormat:@"%@",[dic valueForKey:@"eventName"]];

        cell.playBtn.tag=indexPath.row;//用tag值来记录行数
        cell.playBtn.Click = ^(UIButton *btn,NSString *name){
            //这里执行相关 点击操作
            //            [weakCell setPlayingBackImage];
            //下面是 播放操作
            
            [self tableView:_channelListTV didSelectRowAtIndexPath:indexPath];
            
        };
        
        
        
        NSLog(@"^&^&^*^%@",_playingEventID);
        //播放传值
        if ([self.fullStartTimeStr isEqualToString:[self getTodayStringWithDate]]) {
            NSInteger mark=[self isPlayingWith:[dic objectForKey:@"startTime"] endTime:[dic objectForKey:@"endTime"]];
            if (mark==-1) {
                //回看
                
                if (_isLookPlayBackEvent == YES) {
                    
                    if ([_theVideoInfo.PlayBackTag integerValue]==0) {
                        cell.playBtn.hidden=YES;
                    }else{
                        cell.playBtn.hidden=NO;
                    }
                    
                    if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                        [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_backing"] forState:UIControlStateNormal];
                        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];;
                        cell.timeLabel.textColor =App_selected_color;
                        cell.videoTitleLabel.textColor = App_selected_color;
                        
                        
                    }else{
                        [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_back"] forState:UIControlStateNormal];
                        cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                        cell.timeLabel.textColor =UIColorFromRGB(0x999999);
                        cell.videoTitleLabel.textColor = UIColorFromRGB(0x999999);
                        
                        
                    }
                    //点击事件中判定是否可以回看
                    
                }else
                {
                    cell.playBtn.hidden = YES;
                }
                
                
                
            }
            else if (mark==0){
                //直播
                cell.playBtn.hidden=NO;
                
                
                if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_live_playing"] forState:UIControlStateNormal];
                    cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    cell.timeLabel.textColor =App_selected_color;
                    cell.videoTitleLabel.textColor = App_selected_color;
                    
                    
                }else{
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_live_play"] forState:UIControlStateNormal];
                    cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    cell.timeLabel.textColor =UIColorFromRGB(0x999999);
                    cell.videoTitleLabel.textColor = UIColorFromRGB(0x999999);
                    
                    
                }
                
                
            }
            else if (mark==1){
                cell.playBtn.hidden=NO;
//                BOOL didBooked =[[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:[dic objectForKey:@"id"]];
                BOOL didBooked = [_localListCenter checkTheScheduledListwithID:[dic objectForKey:@"id"]];

                if (didBooked == YES) {
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateNormal];
                    
                }else{
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
                    
                }
                
                
                
            }
            
        }else if ([self.fullStartTimeStr compare:[self getTodayStringWithDate]]<0){
            //如果是今天之前执行下面操作
            //昨天
            
            
            if (_isLookPlayBackEvent == YES) {
                if ([_theVideoInfo.PlayBackTag integerValue]==0) {
                    cell.playBtn.hidden=YES;
                }else{
                    cell.playBtn.hidden=NO;
                }
                
                if ([_playingEventID isEqualToString:[dic objectForKey:@"id"]]) {
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_backing"] forState:UIControlStateNormal];
                    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];;
                    cell.timeLabel.textColor =App_selected_color;
                    cell.videoTitleLabel.textColor = App_selected_color;
                    
                    
                }else{
                    [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_look_back"] forState:UIControlStateNormal];
                    cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
                    cell.timeLabel.textColor =UIColorFromRGB(0x999999);
                    cell.videoTitleLabel.textColor = UIColorFromRGB(0x999999);
                    
                    
                }
                
            }else
            {
                cell.playBtn.hidden = YES;
            }
            
            
            
            
        }else{
            //如果是今天之后执行
            cell.playBtn.hidden=NO;
            //            [cell.playBtn setImage:[UIImage imageNamed:@"btn_booking_42"] forState:UIControlStateNormal];
            
            //            BOOL didBooked =[self  checkTheScheduledArrwithID:[dic objectForKey:@"@id"]];
//            BOOL didBooked =[[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:[dic objectForKey:@"id"]];
            BOOL didBooked = [_localListCenter checkTheScheduledListwithID:[dic objectForKey:@"id"]];
            
            if (didBooked) {
                [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateNormal];
                
            }else{
                [cell.playBtn setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
                
            }
        }
        //            [cell.backLookBtn addTarget:self action:@selector(TVBook:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //        [cell.playBtn addTarget:self action:@selector(PlaySmallVideoPlayer:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        
        
        return cell;
        
        
        
    }
    else if (tableView == _IPTableView){
        
        
        LinkDeviceTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:kMNSCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[LinkDeviceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMNSCellReuseIdentifier];
            cell.backgroundColor = [UIColor clearColor];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
        }
        
        
        cell.IPLabel.text = [NSString stringWithFormat:@"%@",[_IPArray objectAtIndex:indexPath.row]];
        cell.linkBtn.linkStr = [NSString stringWithFormat:@"%@",[_IPArray objectAtIndex:indexPath.row]];
        cell.linkBtn.tag = indexPath.row;
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length>2) {
//#warning 没有连接先不显示
//            _MTButton.hidden = NO;
//            _TVButton.hidden = NO;
//            _RemoteButton.hidden = NO;
        }
        
        if ([cell.IPLabel.text isEqualToString:remberIP] ) {
            cell.linkBtn.selected = YES;
            cell.IPLabel.textColor = App_selected_color;
            [cell.linkBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
            
        }else{
            cell.linkBtn.selected = NO;
            cell.IPLabel.textColor = App_background_color;
            [cell.linkBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_connect@2x"] forState:UIControlStateNormal];
            
        }
        
        
        [cell.linkBtn addTarget:self action:@selector(linkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        return cell;
    }
    else
    {
        return nil;
    }
    
    
    
}

-(void)linkBtnClick:(LinkButton*)sender
{
    
    //    [[NSUserDefaults standardUserDefaults] setObject:sender.linkStr forKey:@"pointToPointIP"];
    //    [[NSUserDefaults standardUserDefaults]synchronize];
    [self reStartHiddenTimer];
    [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
    
    if (sender.selected) {
        
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"pointToPointIP"];
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_connect@2x"] forState:UIControlStateNormal];
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length==0) {
            _MTButton.hidden = YES;
            _TVButton.hidden = YES;
            _RemoteButton.hidden = YES;
            [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_normal"] forState:UIControlStateNormal];
            [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_normal"] forState:UIControlStateNormal];
            [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_normal"] forState:UIControlStateNormal];
            
            
        }
        //        sender.selected = !sender.selected;
        sender.selected = NO;
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"islink"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        

        
        
        //这个地方 可能会发通知 让手机连接
    }else
    {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:sender.linkStr forKey:@"pointToPointIP"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"islink"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
        sender.selected = YES;
        
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length>2) {
            _MTButton.hidden = NO;
            _TVButton.hidden = NO;
            _RemoteButton.hidden = NO;
        }
        
        
    }
    
    
    [_IPTableView reloadData];
    
    
    
    
    
    
}



-(NSString *)seperateServiceTitle:(NSString*)serviceString
{
    NSString *serviceStr ;
    
    if (serviceString.length>0) {
        
        
        if([serviceString rangeOfString:@":"].location !=NSNotFound)//_roaldSearchText
        {
            NSLog(@"yes");
            
            NSArray *arr = [serviceString componentsSeparatedByString:@":"];
            if (arr.count>0) {
                
              serviceStr = [arr objectAtIndex:0];
            }
            
        }
        else
        {
            NSLog(@"no");
            serviceStr = serviceString;
        }
        
    }
    
    return serviceStr;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    _channelSelectIndex = indexPath.row;
    
    firstPeriodLive = nil;
    startDateLive = [SystermTimeControl GetSystemTimeWith];
    ZTTimeIntervalLive=0;
    
    _videoPlayPosition = indexPath.row;
    
    _videolistSelectIndex = indexPath.row;
    
    
    
    
    
    if (tableView == _videoListTV) {
        NSLog(@"点击了_videoListTV  %ld",indexPath.row);
        NSLog(@"你瞅瞅传的%@,",_clickCategoryString);
        
        
        if (USER_ACTION_COLLECTION == YES) {
            
            if (_isCollectionInfo) {
                
           
            
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?[NSString stringWithFormat:@"%@,%ld",@"_",_clickCategoryTag]: _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?[NSString stringWithFormat:@"%@,%@",@"选台",_clickCategoryString]:_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
            }
            if (_currentPlayType == CurrentPlayLive) {
                
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?[NSString stringWithFormat:@"%@,%ld",@"_",_clickCategoryTag]: _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?[NSString stringWithFormat:@"%@,%@",@"选台",_clickCategoryString]:_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];

                
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength", isEmptyStringOrNilOrNull(_TRACKID)?[NSString stringWithFormat:@"%@,%ld",@"_",_clickCategoryTag]: _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?[NSString stringWithFormat:@"%@,%@",@"选台",_clickCategoryString]:_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
   
            }
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
                _TRACKID = nil;
                _TRACKNAME = nil;
                _EN = nil;
            }
        }
        
        
        //本地的播放记录
        if (_currentPlayType == CurrentPlayEvent) {
            
            _localVideoModel.lvID = _serviceID;
            _localVideoModel.lvEventID = _playingEventID;
            _localVideoModel.lvServiceName = _theVideoInfo.channelName;
            _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
            _localVideoModel.lvEventName = _theVideoInfo.videoName;
            _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
            
            [_localListCenter addReWatchPlayRecordWithModel:_localVideoModel andUserTye:IsLogin];
            [_localVideoModel resetLocalVideoModel];

        }
        
        
        
        NSDictionary *video = [_videoListArr objectAtIndex:indexPath.row];
        
        NSString *KServiceID = [video valueForKey:@"categoryServiceID"];
        
//#warning 先修改一下这里****************************************************************
        //增加新功能这里是需要，直接就赋值，不是之前鉴权后再赋值
        if (IS_PERMISSION_GROUP) {
            _serviceID = KServiceID;
            NSString *eventNameStr = [video objectForKey:@"categoryServiceName"];
            
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:_serviceID forKey:@"serviceID"];
            if (eventNameStr== nil||eventNameStr.length<=0||[eventNameStr isEqualToString:@"null"]) {
                eventNameStr = @"";
            }
            NSString *serviceNameStr = [video objectForKey:@"categoryServiceName"];
            if (IsStrEmpty(serviceNameStr)) {
                serviceNameStr = @"";
            }
            [liveDic setObject:eventNameStr forKey:@"eventName"];
            [liveDic setObject:@"" forKey:@"eventID"];
            [liveDic setObject:@"0" forKey:@"currentPlayType"];
            [liveDic setObject:serviceNameStr forKey:@"serviceName"];

            [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
            
          
            

        }
//        else
//        {
////            _currentPlayServiceID = KServiceID;
//            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
//            
//            [request TVPlayWithServiceID:KServiceID andTimeout:kTIME_TEN];
//
//        }[0]	(null)	@"categoryServiceName" : @"CCTV5+体育赛事"
        
        _serviceName = [video objectForKey:@"categoryServiceName"];

        _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull([video objectForKey:@"categoryServiceName"])?@"暂无节目名称":[video objectForKey:@"categoryServiceName"],isEmptyStringOrNilOrNull([video objectForKey:@"categoryServiceEventName"])?@"暂无节目名称":[video objectForKey:@"categoryServiceEventName"]];
        NSLog(@"=====serviceName==1%@===%@",_serviceName,_currentServiceEventStr);
        if (_isVerticalDirection == YES) {
            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[video objectForKey:@"categoryServiceName"]];
        }else
        {
            _serviceTitleLabel.text = _currentServiceEventStr;
        }
        
        _serviceID = KServiceID;
        _currentPlayServiceID = _serviceID;
        
        
        if (IsLogin) {
            
//            if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:YES];
//                
//                _favourButton.selected = YES;
//            }else
//            {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:NO];
//                _favourButton.selected = NO;
//            }
            
            //ww...
            if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES] == YES) {
                
                [self KFavorateBtnisFaved:YES];
                
                _favourButton.selected = YES;
            }else{
                [self KFavorateBtnisFaved:NO];
                
                _favourButton.selected = NO;
            }

        }
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self TVPLayModelService:KServiceID andCurrentDateStr:_currentDateStr];
        
                });

        [dateView DateBtnSelectedAtDate:[[[self getTodayStringWithDate] componentsSeparatedByString:@" "] objectAtIndex:0]];

        
        _IsFirstChannelist = YES;
        
        
        _videoListView.hidden = YES;
//        if (_channelListView.hidden) {//cap
//            _ADViewBase.hidden = YES;
//        }else{
//            _ADViewBase.hidden = NO;
//        }
        
        
        NSMutableArray *currentArr = [[NSMutableArray alloc]init];
        
        currentArr = [[NSUserDefaults standardUserDefaults] objectForKey:EPG_CHANNELLIST];
        if (currentArr.count>0) {
            
            _isLookPlayBackEvent = [self checkPlayBackForCurrentListForServiceID:KServiceID andCurrentList:currentArr];
        }

        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
        _currentPlayType = CurrentPlayLive;
        if (ISIntall) {
            _installMaskView.hidden = NO;
        }else
        {
//        [request TVPlayWithServiceID:KServiceID andTimeout:kTIME_TEN];
            [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

        }
        [request EPGPFWithServiceID:KServiceID andType:-1 andCount:2 andTimeout:kTIME_TEN];

        
        
    }else if (tableView == _channelListTV)
    {
        NSLog(@"点击了%ld",indexPath.row);
        
        if (self.changeVideoNameTimer) {
            [self.changeVideoNameTimer invalidate];
            self.changeVideoNameTimer = nil;
        }
        
        NSLog(@"点击了%ld",indexPath.row);
        
        //        _channelSelectIndex = indexPath.row;
        //
        //
        //        _videoPlayPosition = indexPath.row;
        
        
        //        [request EPGPFWithServiceID:_serviceID andType:-1 andCount:50 andTimeout:kTIME_TEN];
        
        //******
        //    所有的indexPath都要减一 才能对应上正常的row
        //        if (indexPath.row == 0) {
        //            return;
        //        }
        //        _isDownload = NO;
        
        if (USER_ACTION_COLLECTION == YES) {
            
            if (_isCollectionInfo) {
               
            
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"节目单":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
            }
            if (_currentPlayType == CurrentPlayLive) {
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"节目单":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength",isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"节目单":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN", nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
    
            }
            
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
                _TRACKID = nil;
                _TRACKNAME = nil;
                _EN = nil;
            }
        }
        
        
        //本地的播放记录
        if (_currentPlayType == CurrentPlayEvent) {
            
            _localVideoModel.lvID = _serviceID;
            _localVideoModel.lvEventID = _playingEventID;
            _localVideoModel.lvServiceName = _serviceName;
            _localVideoModel.lvEventName = _theVideoInfo.videoName;
            _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
            _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
            [_localVideoModel resetLocalVideoModel];
            
        }

        

        
        
        NSDictionary *video = [_channelListArr objectAtIndex:indexPath.row];
        if ([[video objectForKey:@"ifCanPlay"] isEqualToString:@"1"]) {
            
            // 点击的视频可以播放
            //            _isSureCanPlay = YES;
            _clickPosition = indexPath.row;
            // 记录下当前的播放时间，上传给服务器
            
            // 更换视频信息
            _theVideoInfo.videoTime = [NSString  stringWithFormat:@"%@ %@",self.fullStartTimeStr,[video objectForKey:@"startTime"]];
            _theVideoInfo.videoID = [video objectForKey:@"id"];
            _theVideoInfo.videoName = [video objectForKey:@"eventName"];
            _theVideoInfo.videoEndTime = [video objectForKey:@"endTime"];
            if ([_playingEventID isEqualToString:_theVideoInfo.videoID]) {
                
                [AlertLabel AlertInfoWithText:@"您选择的节目正在播放" andWith:_contentView withLocationTag:2];
                return;
                
            }else{
                if ([[video objectForKey:@"playType"] isEqualToString:@"0"]) {
                    // 直播
                    _channelListView.hidden = YES;
                    _playingEventID = [video objectForKey:@"id"];
                    _currentPlayID = _playingEventID;
                    _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                    liveEndTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"endTime"]] ;
                    liveStartTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"startTime"]];
                    _playBackgroundIV.hidden = YES;
                    _isTapbtn_play = YES;
                    _currentPlayType = CurrentPlayLive;
                    
                    [self StartNewTimer];
                    
                    
                    if (IS_PERMISSION_GROUP) {
                        NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                        [liveDic setObject:_serviceID forKey:@"serviceID"];
                        
                        if (_theVideoInfo.videoName== nil||[_theVideoInfo.videoName length]<=0||[_theVideoInfo.videoName isEqualToString:@"null"]) {
                            _theVideoInfo.videoName = @"";
                        }
                        
                        
                        [liveDic setObject: _theVideoInfo.videoName forKey:@"eventName"];
                        if (NotNilAndNull(_serviceName)) {
                            [liveDic setObject:_serviceName forKey:@"serviceName"];
                        }else
                        {
                            [liveDic setObject:@"" forKey:@"serviceName"];
                        }
                        if (NotNilAndNull(_serviceName)) {
                            [liveDic setObject:_serviceName forKey:@"serviceName"];
                        }else
                        {
                            [liveDic setObject:@"" forKey:@"serviceName"];
                        }
                        if (NotNilAndNull(_playingEventID)) {
                            [liveDic setObject:_playingEventID forKey:@"eventID"];
                        }else
                        {
                            [liveDic setObject:@"" forKey:@"eventID"];
                        }
                        [liveDic setObject:@"0" forKey:@"currentPlayType"];
                        [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                    }
                    
                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                    if (ISIntall) {
                        _installMaskView.hidden = NO;
                    }else
                    {
//                    [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                        [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                    }
                    
                    
                }
                
                
                if (_isLookPlayBackEvent == YES) {
                    
                    
                    if ([[video objectForKey:@"playType"] isEqualToString:@"1"]){
                        
                        
                        // 回看
                        _channelListView.hidden = YES;
                        _playingEventID = [video objectForKey:@"id"];
                        _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                        _currentPlayID = _playingEventID;
                        _playBackgroundIV.hidden = YES;
                        _isTapbtn_play = YES;
                        _currentPlayType = CurrentPlayEvent;
                        _currentTimeL.text = @"00:00:00";
                        _totalTimeL.text = @"00:00:00";
                        _currentServiceEventStr =[NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(_theVideoInfo.videoName)?@"暂无节目":_theVideoInfo.videoName];
                        NSLog(@"=====serviceName==2%@===%@",_serviceName,_currentServiceEventStr);

                        if (_isVerticalDirection == YES) {
                            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                            
                        }else
                        {
                            _serviceTitleLabel.text = _currentServiceEventStr;
                        }

                        
                        
                        //                _currentPlayType = CurrentPlayEvent;
                        //                [_moviePlayer pause];
                        
                        if (IS_PERMISSION_GROUP) {
                            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                            [liveDic setObject:_serviceID forKey:@"serviceID"];
                            if (_theVideoInfo.videoName== nil||[_theVideoInfo.videoName length]<=0||[_theVideoInfo.videoName isEqualToString:@"null"]) {
                                _theVideoInfo.videoName = @"";
                            }
                            [liveDic setObject: _theVideoInfo.videoName forKey:@"eventName"];
                            if (NotNilAndNull(_playingEventID)) {
                                [liveDic setObject:_playingEventID forKey:@"eventID"];
                            }else
                            {
                                [liveDic setObject:@"" forKey:@"eventID"];
                            }
                            [liveDic setObject:@"1" forKey:@"currentPlayType"];
                            if (NotNilAndNull(_serviceName)) {
                                [liveDic setObject:_serviceName forKey:@"serviceName"];
                            }else
                            {
                                [liveDic setObject:@"" forKey:@"serviceName"];
                            }
                            [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                        }
                        
                        
                        
                        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                        
//                        [request EventPlayWithEventID:_playingEventID andTimeout:kTIME_TEN];
                        if (ISIntall) {
                            
                            _installMaskView.hidden = NO;
                        }else
                        {

                        [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:kTIME_TEN];
                        }
                        
                        [self StartNewTimer];

                    }
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"该节目不支持回看功能" andWith:_contentView withLocationTag:2];
                    return;
                }
  
            }
            
            
            
        }
        else if ([[video objectForKey:@"playType"] isEqualToString:@"2"]){
            
//            NSString *orderID = [video objectForKey:@"id"];
            
            orderID = [video objectForKey:@"id"];
            orderEventName = [video objectForKey:@"eventName"];
            orderStartTime = [video objectForKey:@"startTime"];
            orderEndTime = [video objectForKey:@"endTime"];
            orderFullStartTime = [video objectForKey:@"fullStartTime"];

            
            
            [self reStartHiddenTimer];

            
            if (IsLogin) {
                //处理登陆的接口。20号弄。
                if ([_localListCenter checkTheScheduledListwithID:orderID] == YES) {//[[UserScheduleCenter defaultCenter]checkTheScheduledListwithID:orderID] == YES
                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                    
                    [request DelEventReserveWithEventID:orderID andTimeout:kTIME_TEN];
                    self.SelectdscheduleD.tag = indexPath.row;
                    _SelectdscheduleD.selectedEventID = orderID;
                }else{
                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                    
                    [request AddEventReserveWithEventID:orderID andTimeout:kTIME_TEN];
                    self.SelectdscheduleD.tag = indexPath.row;
                    _SelectdscheduleD.selectedEventID = orderID;
                }
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                }
                return;
                
            }
        }
        
        if (IsLogin) {
            
//            if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:YES];
//                
//                _favourButton.selected = YES;
//            }else
//            {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:NO];
//                _favourButton.selected = NO;
//            }
            
            //ww...
            if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES]) {
                
                [self KFavorateBtnisFaved:YES];
                
                _favourButton.selected = YES;
            }else{
                [self KFavorateBtnisFaved:NO];
                
                _favourButton.selected = NO;
            }

        }

        
//        _channelListView.hidden = YES;
//        _ADViewBase.hidden = YES;//cap
    }
    
    else if (tableView == _EPGListTableView)
    {
        
        
        if (self.changeVideoNameTimer) {
            [self.changeVideoNameTimer invalidate];
            self.changeVideoNameTimer = nil;
        }
        
        NSLog(@"点击了%ld",indexPath.row);
        if (USER_ACTION_COLLECTION == YES) {
            
            if (_isCollectionInfo) {
                
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
            }
            if (_currentPlayType == CurrentPlayLive) {
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength", isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
  
            }
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
                _TRACKID = nil;
                _TRACKNAME = nil;
                _EN = nil;
            }
        }
        
        //本地的播放记录
        if (_currentPlayType == CurrentPlayEvent) {
            
            _localVideoModel.lvID = _serviceID;
            _localVideoModel.lvEventID = _playingEventID;
            _localVideoModel.lvServiceName = _serviceName;
            _localVideoModel.lvEventName = _theVideoInfo.videoName;
            _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
            _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
            
            [_localListCenter addReWatchPlayRecordWithModel:_localVideoModel andUserTye:IsLogin];
            [_localVideoModel resetLocalVideoModel];
            
        }

        
        
        
        

        //******
        //    所有的indexPath都要减一 才能对应上正常的row
        //        if (indexPath.row == 0) {
        //            return;
        //        }
        //        _isDownload = NO;
        NSDictionary *video = [_channelListArr objectAtIndex:indexPath.row];
        if ([[video objectForKey:@"ifCanPlay"] isEqualToString:@"1"]) {
            // 点击的视频可以播放
            //            _isSureCanPlay = YES;
            
            
            
            //            _clickPosition = indexPath.row;
            // 记录下当前的播放时间，上传给服务器
            if (_currentPlayType!=0 || _currentPlayType!=2) {
                if (IsLogin) {
                    [self recordPlaybackTime];
                }else{
                    //                    [self recordLocalPlaybackTime];
                }
            }
            
            // 更换视频信息
            _theVideoInfo.videoTime = [NSString  stringWithFormat:@"%@ %@",self.fullStartTimeStr,[video objectForKey:@"startTime"]];
            _theVideoInfo.videoID = [video objectForKey:@"id"];
            _currentPlayID = _theVideoInfo.videoID;
            _theVideoInfo.videoName = [video objectForKey:@"eventName"];
            _theVideoInfo.videoEndTime = [video objectForKey:@"endTime"];
           
            
            if ([_playingEventID isEqualToString:_theVideoInfo.videoID]) {
                 [AlertLabel AlertInfoWithText:@"您选择的节目正在播放" andWith:_contentView withLocationTag:2];
                return;
            }else
            {
            if ([[video objectForKey:@"playType"] isEqualToString:@"0"]) {
                // 直播
                _playBackgroundIV.hidden = YES;
                _currentPlayType = CurrentPlayLive;
                
                _isTapbtn_play = YES;
                //                _currentPlayType = CurrentPlayLive;
                
                _playingEventID = [video objectForKey:@"id"];
                _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                liveEndTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"endTime"]] ;
                liveStartTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"startTime"]];
                
                if (IS_PERMISSION_GROUP) {
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:_serviceID forKey:@"serviceID"];
                    if (_theVideoInfo.videoName== nil||[_theVideoInfo.videoName length]<=0||[_theVideoInfo.videoName isEqualToString:@"null"]) {
                        _theVideoInfo.videoName = @"";
                    }
                    [liveDic setObject: _theVideoInfo.videoName forKey:@"eventName"];
                    if (NotNilAndNull(_playingEventID)) {
                        [liveDic setObject:_playingEventID forKey:@"eventID"];
                    }else
                    {
                        [liveDic setObject:@"" forKey:@"eventID"];
                    }
                    [liveDic setObject:@"0" forKey:@"currentPlayType"];
                    if (NotNilAndNull(_serviceName)) {
                        [liveDic setObject:_serviceName forKey:@"serviceName"];
                    }else
                    {
                        [liveDic setObject:@"" forKey:@"serviceName"];
                    }
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                    
                }
                
                
                HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                if (ISIntall) {
                    _installMaskView.hidden = NO;
                }else
                {
//                [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];
                }
                
                [self StartNewTimer];

                
            }
            
            
            if (_isLookPlayBackEvent == YES) {
                
            if ([[video objectForKey:@"playType"] isEqualToString:@"1"]){
                
                
                        // 回看
                        _playBackgroundIV.hidden = YES;
                        //                [_moviePlayer pause];
                        
                        _currentPlayType = CurrentPlayEvent;
                


                
                        
                        if (IS_PERMISSION_GROUP) {
                            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                            [liveDic setObject:_serviceID forKey:@"serviceID"];
                            if (_theVideoInfo.videoName== nil||[_theVideoInfo.videoName length]<=0||[_theVideoInfo.videoName isEqualToString:@"null"]) {
                                _theVideoInfo.videoName = @"";
                            }
                            [liveDic setObject: _theVideoInfo.videoName forKey:@"eventName"];
                            if (NotNilAndNull(_playingEventID)) {
                                [liveDic setObject:_playingEventID forKey:@"eventID"];
                            }else
                            {
                                [liveDic setObject:@"" forKey:@"eventID"];
                            }
                            [liveDic setObject:@"1" forKey:@"currentPlayType"];
                            if (NotNilAndNull(_serviceName)) {
                                [liveDic setObject:_serviceName forKey:@"serviceName"];
                            }else
                            {
                                [liveDic setObject:@"" forKey:@"serviceName"];
                            }

                            [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                        }
                
                _currentServiceEventStr =[NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(_theVideoInfo.videoName)?@"暂无节目":_theVideoInfo.videoName];
                NSLog(@"=====serviceName==3%@===%@",_serviceName,_currentServiceEventStr);

                if (_isVerticalDirection == YES) {
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                    
                }else
                {
                    _serviceTitleLabel.text = _currentServiceEventStr;
                }

                
                        _isTapbtn_play = YES;
                        _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                        _currentPlayType = CurrentPlayEvent;
                        _playingEventID = [video objectForKey:@"id"];
                _currentTimeL.text = @"00:00:00";
                _totalTimeL.text = @"00:00:00";
                        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                        
//                        [request EventPlayWithEventID:_theVideoInfo.videoID andTimeout:kTIME_TEN];
                if (ISIntall) {
                    
                    _installMaskView.hidden = NO;
                }else
                {
                         [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:kTIME_TEN];
                }
                        [self StartNewTimer];
                        
                        
                    }

                
            }else
            {
                
                [AlertLabel AlertInfoWithText:@"该节目不支持回看功能" andWith:_contentView withLocationTag:2];
                
                return;
            }
            }
        }
        else if ([[video objectForKey:@"playType"] isEqualToString:@"2"]){
            
            orderID = [video objectForKey:@"id"];
            orderEventName = [video objectForKey:@"eventName"];

            orderStartTime = [video objectForKey:@"startTime"];
            orderEndTime = [video objectForKey:@"endTime"];
            orderFullStartTime = [video objectForKey:@"fullStartTime"];
//            [self reStartHiddenTimer];
            if (IsLogin) {
                //处理登陆的接口。20号弄。
                if ([_localListCenter checkTheScheduledListwithID:orderID] == YES) {//[[UserScheduleCenter defaultCenter]checkTheScheduledListwithID:orderID] == YES
                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                    
                    [request DelEventReserveWithEventID:orderID andTimeout:kTIME_TEN];
                    
                    self.SelectdscheduleD.tag = indexPath.row;
                    _SelectdscheduleD.selectedEventID = orderID;
                }else{
                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                    
                    [request AddEventReserveWithEventID:orderID andTimeout:kTIME_TEN];
                    self.SelectdscheduleD.tag = indexPath.row;
                    _SelectdscheduleD.selectedEventID = orderID;
                }
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;

                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                         [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                }
                return;
                
            }
        }
        
        if (IsLogin) {
            
//            if ([[UserCollectCenter defaultCenter]checkTheFavoriteChannelListWithServiceID:_serviceID]) {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:YES];
//                
//                _favourButton.selected = YES;
//            }else
//            {
//                
//                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                [self KFavorateBtnisFaved:NO];
//                _favourButton.selected = NO;
//            }
            
            //ww...
            if ([_localListCenter checkTheFavoriteChannelListWithServiceID:_serviceID andUserType:YES]) {
                
                [self KFavorateBtnisFaved:YES];
                
                _favourButton.selected = YES;
            }else{
                [self KFavorateBtnisFaved:NO];
                
                _favourButton.selected = NO;
            }

        }
  
        
    }
    
    
    if (_isVerticalDirection == NO) {
        [self setBigScreenPlayer];
    }
    [_EPGListTableView reloadData];
    [_channelListTV reloadData];
    
    
    
}

- (void)changeVideoName
{
    
    if (_currentPlayType == CurrentPlayLive) {
        // 正在播放的是直播视频，修改直播视频的名称
        _videoPlayPosition += 1;
        _channelSelectIndex = _videoPlayPosition;
        
        if (_videoPlayPosition<_channelListArr.count-1) {
            
            NSDictionary *video = [_channelListArr objectAtIndex:_videoPlayPosition];
            _theVideoInfo.videoID = [video objectForKey:@"id"];
            _theVideoInfo.videoName = [video objectForKey:@"eventName"];
            _theVideoInfo.videoTime = [video objectForKey:@"fullStartTime"];
            _theVideoInfo.videoEndTime = [video objectForKey:@"endTime"];
            self.serviceTitleLabel.text = [video objectForKey:@"eventName"];
            liveEndTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"endTime"]] ;
            liveStartTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"startTime"]];
            _currentDateStr = [[[self getTodayStringWithDate] componentsSeparatedByString:@" "] objectAtIndex:0];
            _playingEventID = _theVideoInfo.videoID;
            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
            
            [request EPGSHOWWithServiceID:_serviceID andDate:_currentDateStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
            [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
//            [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

            }
            
            self.fullStartTimeStr = [[[self getTodayStringWithDate] componentsSeparatedByString:@" "] objectAtIndex:0];
            
        }
        
        if (_videoPlayPosition ==_channelListArr.count-1) {
            NSDictionary *dic = [[NSDictionary alloc]init];
            dic = [_channelListArr objectAtIndex:_videoPlayPosition];
            
            
            _theVideoInfo.videoName = [dic valueForKey:@"eventName"];
            _theVideoInfo.PlayBackTag = [dic valueForKey:@"playType"];
            if (_theVideoInfo.PlayBackTag == 0) {
                HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                
                [request EPGSHOWWithServiceID:_serviceID andDate:[self getTheNextDay] andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
                _videoPlayPosition = 0;
                self.fullStartTimeStr = [[[self getTheNextDay] componentsSeparatedByString:@" "] objectAtIndex:0];
                
                [dateView DateBtnSelectedAtDate:self.fullStartTimeStr];
            }
            
            if ([_theVideoInfo.PlayBackTag intValue]== 1) {
                HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                
                [request EPGSHOWWithServiceID:_serviceID andDate:[self getTheNextDay] andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
                
                
                
                
                _videoPlayPosition = 0;
                self.fullStartTimeStr = [[[self getTheNextDay] componentsSeparatedByString:@" "] objectAtIndex:0];
                
                [dateView DateBtnSelectedAtDate:self.fullStartTimeStr];
            }
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
        
        
        
    }
    
    //下载当天的EPG信息
    //    self.pan.userInteractionEnabled = NO;
    //    DataInterface *da = [[DataInterface alloc]initWithDelegate:self];
    //    [request EPGShowWithServiceID:_theVideoInfo.channelID andDate:[[[self getNowServiceTime] componentsSeparatedByString:@" "] objectAtIndex:0] andTimeout:10];
    
    //    [request EPGSHOWWithServiceID:_theVideoInfo.channelID andDate:<#(NSString *)#> andStartDate:<#(NSString *)#> andEndDate:<#(NSString *)#> andTimeout:<#(NSInteger)#>]
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}


// 获取上一天的时间
- (NSString *)getThePrevDay
{
    NSString *dateString = [[_theVideoInfo.videoTime componentsSeparatedByString:@" "] objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSTimeInterval oneDay = 0 - 24 * 60 * 60;
    NSDate *prevDate = [date dateByAddingTimeInterval:oneDay];
    return [dateFormatter stringFromDate:prevDate];
}





// 获取下一天的日期
- (NSString *)getTheNextDay
{
    NSString *dateString = [[_theVideoInfo.videoTime componentsSeparatedByString:@" "] objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSTimeInterval oneDay = 24 * 60 * 60;
    NSDate *nextDate = [date dateByAddingTimeInterval:oneDay];
    return [dateFormatter stringFromDate:nextDate];
}


-(NSString *)getTodayStringWithDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:[SystermTimeControl GetSystemTimeWith]];
    return strDate;
}


-(NSString*)getHourMinuteAndSecond:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}



-(CGFloat)getIntWithTime:(NSString *)time{
    NSArray *arr=[time componentsSeparatedByString:@":"];
    NSString *hour=[arr objectAtIndex:0];
    NSString *min=[arr objectAtIndex:1];
    float hhh=[hour floatValue];
    float mmm=[min floatValue];
    CGFloat number=(CGFloat)(hhh*3600+mmm*60);
    return number;
}



#pragma  mark - NSDate的转化
-(NSInteger)isPlayingWith:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:[SystermTimeControl GetSystemTimeWith]];
    NSArray *arr=[strDate componentsSeparatedByString:@" "];
    float now=[self getIntWithTime:[arr lastObject]];
    float startInt=[self getIntWithTime:startTime];
    float endInt=[self getIntWithTime:endTime];
    
    if (endInt<startInt)
    {//跨天
        if (now<startInt&& now
            >endInt) {
            return 1;
        }else if (now>endInt && now<=startInt)
        {
            return -1;
        }
        return 0;
        
    }else
    {
        if (now>=endInt) {
            return -1;
        }else if (now<startInt){
            return 1;
        }
        return 0;
    }
    
    //返回-1 为回看  返回0为直播 返回1为预定
}




#pragma  mark - 建造数组，数组的转化

-(NSMutableArray *)currentEventListMutableArr:(NSDictionary *)dic
{
    
    
    NSMutableArray *backArr;
    
    if ([[dic valueForKey:@"servicelist"] count] >0) {
        NSDictionary *onedic = [[NSDictionary alloc]init];
        onedic = [[dic valueForKey:@"servicelist"] objectAtIndex:0];
        
        NSArray *secondArr = [[NSArray alloc]init];
        
        secondArr = [onedic valueForKey:@"serviceinfo"];
        
        if (secondArr.count>0) {
            NSMutableArray *thirdArr = [[NSMutableArray alloc]init];
            thirdArr = [[secondArr objectAtIndex:0] valueForKey:@"epginfo"];
            
            backArr = thirdArr;
            
        }else
        {
            return nil;
        }
    }
    
    return backArr;
    
}





-(void)StartNewTimer
{
    if (self.myNewTimer)
    {
        [self.myNewTimer invalidate];
    }
    self.myNewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ChangeCurTimeForNewPort) userInfo:nil repeats:YES];
}

NSInteger progressValue;
-(void)ChangeCurTimeForNewPort
{
    if (_isHaveAD == YES) {
        
        
        
        return;
    }else{
    
    if (_currentPlayType == CurrentPlayLive)
    {
        
        
        if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle]>0) {
            
            _currentTimeL.text = liveStartTime;
            _totalTimeL.text = liveEndTime;
            
            
            
            progressValue = 1000* (liveTimeInterVal);
            
            [_videoProgressV setValue:progressValue animated:YES];

        }
        
//        else
//        {
//            return;
//        }
        
        
        
//        
//        if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle] <=0) {
//            
////            [self.myNewTimer invalidate];
//////            [self playTheNextVideo];
////
////            [_livePlayerTimer invalidate];
////            
////            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
////            [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
//            
//            return;
//            
//        }

        
        
    }
    
   
    if (_currentPlayType == CurrentPlayEvent)
    {
        
        
        NSInteger movieDuration =  _moviePlayer.duration;
        NSInteger currentPlaybackTime;

//        NSInteger KcurrentPlaybackTime = _moviePlayer.currentPlaybackTime;
        //        NSLog(@"kkkkkk======%ld",KcurrentPlaybackTime);
        if (movieDuration) {
            
            
            if (_moviePlayer.currentPlaybackTime <0) {
                currentPlaybackTime = 0;
            }else
            {
                
                
                currentPlaybackTime = _moviePlayer.currentPlaybackTime;
            }
            if (currentPlaybackTime>= movieDuration) {
                currentPlaybackTime = movieDuration;
            }
            
            NSInteger total_hours = (movieDuration/3600);
            NSInteger total_minutes = (movieDuration - (total_hours*3600)) / 60 ;
            NSInteger total_seconds =(movieDuration -  total_hours*3600 - total_minutes*60);
            
            [_totalTimeL setText:[NSString stringWithFormat:@"%02ld:%02ld:%02ld",total_hours,total_minutes,total_seconds]];
            
            NSInteger currentPlayback_hours = (currentPlaybackTime / 3600);
            NSInteger currentPlayback_minutes = (currentPlaybackTime - (currentPlayback_hours*3600)) / 60 ;
            
            NSInteger currentPlayback_seconds =(currentPlaybackTime - currentPlayback_hours*3600 - currentPlayback_minutes*60);
          
          
            
            
            if ( currentPlaybackTime ==  movieDuration-10) {
                
                
                
                //判断下一个节目是回看还是直播节目
                
                

                
                //                if (_currentPlayType == CurrentPlayEvent) {
                
                [AlertLabel AlertInfoWithText:[NSString stringWithFormat:@"%@",[self judgmentNextEPGIsEventORLive:_videoPlayPosition+1]] andWith:_contentView withLocationTag:2];
                
                //                }
            }
            
            
            if (movieDuration <= (currentPlaybackTime+1) ||  _isPlayEnd == YES)
            {
                _isPlayEnd = NO;
//                [self clearMoviePlayerStop];
                //                [self.myNewTimer invalidate];
                
                if (_currentPlayType == CurrentPlayEvent) {
                    if (IsLogin) {
                        [self recordPlaybackTime];
                    }else{
                        //                [self recordLocalPlaybackTime];
                    }
                }
                //                [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
                
                [self playTheNextVideo];
            }
            
            if (!OnMove) {
                
                [_currentTimeL setText:[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currentPlayback_hours,currentPlayback_minutes,currentPlayback_seconds]];
                progressValue = (1000 * currentPlaybackTime / movieDuration);
                [_videoProgressV setValue:progressValue animated:NO];
            }
            
            
            
            
        }
        
    }
    
    
    if (_currentPlayType == CurrentPlayTimeMove)
    {
        
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:[SystermTimeControl GetSystemTimeWith]];
        NSInteger seconds = [dateComponents second];
        NSInteger minutes = [dateComponents minute];
        NSInteger hours = [dateComponents hour];
        
        
        
        _currentTimeL.text =  [NSString stringWithFormat:@"%02ld:%02ld:%02ld",hours-4,minutes,seconds];
        
        _totalTimeL.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",hours,minutes,seconds];
        
        progressValue = _ProgressBeginToMove;
        
        _MoveTimeInteger =1000 - progressValue-10;
        
        //新增加的时移00000
        NSInteger v =(1000- _ProgressBeginToMove)*14.4;
        
        NSTimeInterval vseconds = v;
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //
        [formatter setDateFormat:@"HH:mm:ss"];
        
        NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
        
        NSString *MideleTime = [formatter stringFromDate:Mideledate];
        
        
        
        //label的显示数字。为滑动条移动后的位置的value
        [_dragLabel setText:[NSString stringWithFormat:@"%@",MideleTime]];
        
        NSLog(@"时移drag%@",_dragLabel.text);
        
        [_videoProgressV setValue:progressValue animated:NO];
        
        
    }
    
    }
    
    //    [_videoProgressV setValue:progressValue animated:NO];
    
    
    
}


-(NSString *)judgmentNextEPGIsEventORLive:(NSInteger)videoPosition
{
    
    NSString *str;
    
    if (_channelListArr.count >0) {
        for (int i = 0; i<_channelListArr.count; i++) {
            
            NSDictionary *dictionary = [[NSDictionary alloc]init];
            
            dictionary = [_channelListArr objectAtIndex:videoPosition];
            
            
            if ([[dictionary objectForKey:@"ifCanPlay"] isEqualToString:@"1"]) {
                
                if ([[dictionary objectForKey:@"playType"] isEqualToString:@"1"]) {
                    
                    str  = @"即将播放下一节目";
                    
                }else if ([[dictionary objectForKey:@"playType"] isEqualToString:@"0"])
                {
                    str = @"即将切换到直播";
                }
                
                
            }
            
            
        }
 
    }else
    {
        str = @"";
    }
    
    
    
    
    
    
    return str;

}







-(void)playTheNextVideo
{
    /*
     //A.先判断是否是最后一个节目  如果是最后一个节目 并且 这最后一个节目是回看 那么 跳转到给频道的直播节目（土司提示：今天节目已播放完毕）
     B.最后一个节目 并且 是直播 继续播直播 OK。（土司提示：节目播放完毕）；
     
     CCCC：更换节目信息---》》
     如果最后一个节目 是回看 跳转到下一天你
     最后一个节目 如果是直播 并且 时间过了00：00 切换节目信息
     
     
     */
    //B.
    
    
    
    //本地的播放记录
    if (_currentPlayType == CurrentPlayEvent) {
        
        _localVideoModel.lvID = _serviceID;
        _localVideoModel.lvEventID = _playingEventID;
        _localVideoModel.lvServiceName = _theVideoInfo.channelName;
        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
        _localVideoModel.lvEventName = _theVideoInfo.videoName;
        _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
        
        [_localListCenter addReWatchPlayRecordWithModel:_localVideoModel andUserTye:IsLogin];
        [_localVideoModel resetLocalVideoModel];

    }
    

    
    
    
    
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    //-------------不是最后一个节目的时候走下面的方法
    [[self.view viewWithTag:4040] setHidden:YES];
    //精简
    
    [self clearMoviePlayerStop];
    [self StartNewTimer];
    
    
    
    _videoPlayPosition +=1;
    
    _channelSelectIndex = _videoPlayPosition;
    
    
    
    if (USER_ACTION_COLLECTION == YES) {
        
        if (_isCollectionInfo) {
            
            
            
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"_":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
            }
            if (_currentPlayType == CurrentPlayLive) {
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"_":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];
                
                
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength", isEmptyStringOrNilOrNull(_TRACKID)?@"_": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"_":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",isEmptyStringOrNilOrNull(_dragLabel.text)?@"":_dragLabel.text,@"timeMoveTWT",nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
                
            }
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
            _TRACKID = nil;
            _TRACKNAME = nil;
            _EN = nil;
        }
    }
    
    
    
    
    if (_channelListArr.count>0) {
        
        if (_videoPlayPosition<_channelListArr.count-1) {
            
            
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *video = [_channelListArr objectAtIndex:_videoPlayPosition];
                
                if ([[video objectForKey:@"ifCanPlay"] isEqualToString:@"1"])
                {
                    _theVideoInfo.videoTime = [NSString  stringWithFormat:@"%@ %@",self.fullStartTimeStr,[video objectForKey:@"startTime"]];
                    _theVideoInfo.videoID = [video objectForKey:@"id"];
                    _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                    _theVideoInfo.videoEndTime = [video objectForKey:@"endTime"];
                    _playingEventID = [video objectForKey:@"id"];
                    //                UILabel *videoNameLabel = (UILabel *)[self.view viewWithTag:5051];
                    //                videoNameLabel.text = [NSString stringWithFormat:@"即将播放 %@", _theVideoInfo.videoName];
                    
                    //3.0添加的自动播放功能，是否加入暂停呢？
                    if ( [[video objectForKey:@"playType"] isEqualToString:@"0"]) {
                        //                    _currentPlayType = CurrentPlayLive;
                        [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
                        //                    _currntPlayLabel.text = [NSString stringWithFormat:@"正在播放：%@",_currentLiveVideoTitleStr];
                        //                    _nextPlayLabel.text = [NSString stringWithFormat:@"下一节目：%@",_nextLiveVideoTitleStr];
                        liveEndTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"endTime"]] ;
                        liveStartTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"startTime"]];
                        if (ISIntall) {
                            _installMaskView.hidden = NO;
                        }else
                        {
//                        [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                            [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                        }
                        
                    }
                    
                    if ([[video objectForKey:@"playType"] isEqualToString:@"1"])
                    {
                        //                    _currentPlayType = CurrentPlayEvent;
                        _currentPlayBackLabel.text = [NSString stringWithFormat:@"正在回看：%@",_theVideoInfo.videoName];
                        //                    [request EventPlayWithEventID:_theVideoInfo.videoID andTimeout:kTIME_TEN];
                        if (ISIntall) {
                            
                            _installMaskView.hidden = NO;
                        }else
                        {

                        [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_theVideoInfo.videoID andTimeout:KTimeout];
                        }
                    }
                    
                    
                }else
                {
                    // 下一个视频不能播放, 退出播放器界面
                    [self performSelectorOnMainThread:@selector(displayError:) withObject:nil waitUntilDone:NO];
                }
                
                
                [_channelListTV reloadData];
                [_EPGListTableView reloadData];
            }
            
            if (_currentPlayType == CurrentPlayLive) {
                
                
                NSDictionary *video = [_channelListArr objectAtIndex:_videoPlayPosition];
                
                //            NSLog(@"%@",video);
                
                _playingEventID = [video objectForKey:@"id"];
                
                NSLog(@"*%@?????****%@",video, _playingEventID);
                
                liveEndTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"endTime"]] ;
                liveStartTime = [NSString stringWithFormat:@"%@:00",[video objectForKey:@"startTime"]];
                if (ISIntall) {
                    _installMaskView.hidden = NO;
                }else
                {
//                [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                    [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                }
                [request EPGSHOWWithServiceID:_serviceID andDate:_currentDateStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
            }
            
            
            
            
        }
        //已经检查到最后一个
        if (_videoPlayPosition == _channelListArr.count) {
            
            
            if (_currentPlayType == CurrentPlayEvent) {
                NSDictionary *dic = [[NSDictionary alloc]init];
                dic = [_channelListArr objectAtIndex:_videoPlayPosition-1];
                
                
                _theVideoInfo.videoName = [dic valueForKey:@"eventName"];
                _theVideoInfo.PlayBackTag = [dic valueForKey:@"playType"];
                _theVideoInfo.videoID = [dic valueForKey:@"id"];
                _theVideoInfo.videoTime = [dic valueForKey:@"fullStartTime"];
                
                
                NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                
                NSString *endTimeDate = [[_theVideoInfo.videoTime componentsSeparatedByString:@" "] objectAtIndex:0];
                NSString *fullEndTime = [NSString stringWithFormat:@"%@ %@",endTimeDate,[dic valueForKey:@"endTime"]];
                
                
                NSDate *NewendTime   = [formatter dateFromString:[NSString stringWithFormat:@"%@",fullEndTime]];
                
                
                if ([NewendTime isLaterThanOrEqualTo:[SystermTimeControl GetSystemTimeWith]] == NO) {
                    
                    
                    [request EPGSHOWWithServiceID:_serviceID andDate:[self getTheNextDay] andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
                    
                    _isBackPlayBeforeZero = YES;
                    
                    
                }else
                {
                    
                    [request EPGSHOWWithServiceID:_serviceID andDate:[self getTheNextDay] andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
                    _isBackPlayAfterZero= YES;
                    
                }
                _currentDateStr = [[[self getTheNextDay] componentsSeparatedByString:@" "] objectAtIndex:0];
                [dateView DateBtnSelectedAtDate:_currentDateStr];
                
                
            }
            
            if (_currentPlayType == CurrentPlayLive) {
                
                NSDictionary *dic = [[NSDictionary alloc]init];
                dic = [_channelListArr objectAtIndex:_videoPlayPosition-1];
                _theVideoInfo.videoTime = [dic valueForKey:@"fullStartTime"];
                
                [request EPGSHOWWithServiceID:_serviceID andDate:[self getTheNextDay] andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
                _currentDateStr = [[[self getTheNextDay] componentsSeparatedByString:@" "] objectAtIndex:0];
                [dateView DateBtnSelectedAtDate:_currentDateStr];
                
                _isLastPlayLive = YES;
                
                
            }
            
            
            
        }
        
    }
    
    
    
    
}


#pragma tap点击事件


-(void)TapSmallScreenbtn_playNoButton
{
    if (self.changeVideoNameTimer) {
        [self.changeVideoNameTimer invalidate];
        self.changeVideoNameTimer = nil;
    }
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    firstPeriodLive = nil;
    startDateLive = [SystermTimeControl GetSystemTimeWith];
    ZTTimeIntervalLive=0;
    
    _isTapbtn_play = YES;
    
    _playBackgroundIV.hidden = YES;
    [self StartNewTimer];
    
    _allCodeRateLink = @"";
    
    //采集回看直播采集初始时间   初始化服务器的时间
    //采集回看直播采集初始时间   初始化服务器的时间
    self.nowDate = [SystermTimeControl GetSystemTimeWith];
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init] ;
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.nowDateString = [formatter stringFromDate:self.nowDate];
    
    
    //    [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    
    
    if (_currentPlayType == CurrentPlayEvent) {
        
        
        if (_channelListArr.count >0) {
            for (NSInteger i = 0; i<_channelListArr.count; i++) {
                
                if ([[[_channelListArr objectAtIndex:i] objectForKey:@"id"] isEqualToString:self.eventURLID]) {
                    
                    NSDictionary *eventDic = [[NSDictionary alloc]init];
                    
                    eventDic = [_channelListArr objectAtIndex:i];
                    _playingEventID = [eventDic objectForKey:@"id"];
                    _videoPlayPosition = i;
                    _channelSelectIndex = _videoPlayPosition;
                    _theVideoInfo.videoName = [NSString stringWithFormat:@"%@",[eventDic objectForKey:@"eventName"]];
                    
                    
                    break;
                    
                }
                else
                {
                    if (_eventName.length>0) {
                        _theVideoInfo.videoName = _eventName;

                    }else
                    {
                        _theVideoInfo.videoName = @"暂无节目";

                    }
                }
                
                
            }
            
        }else
        {
            if (self.eventName.length<=0 ||[self.eventName isEqual:@"(null)"]) {
                _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",@"暂无节目"];
                _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,@"暂无节目"];
                NSLog(@"=====serviceName==4%@===%@",_serviceName,_currentServiceEventStr);

                if (_isVerticalDirection == YES) {
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                    
                }else
                {
                    _serviceTitleLabel.text = _currentServiceEventStr;
                }

                
            }else
            {
                _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",self.eventName];
                _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(self.eventName)?@"暂无节目":self.eventName];
                NSLog(@"=====serviceName==5%@===%@",_serviceName,_currentServiceEventStr);

                if (_isVerticalDirection == YES) {
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                    
                }else
                {
                    _serviceTitleLabel.text = _currentServiceEventStr;
                }

            }
            
        }
        
        
        if (!_theVideoInfo.videoName) {
            
            if (!self.eventName) {
                _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",@"暂无节目"];
                _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,@"暂无节目"];
                NSLog(@"=====serviceName==6%@===%@",_serviceName,_currentServiceEventStr);

                if (_isVerticalDirection == YES) {
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                    
                }else
                {
                    _serviceTitleLabel.text = _currentServiceEventStr;
                }

            }else
            {
                
                _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",self.eventName];
                _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(self.eventName)?@"暂无节目":self.eventName];
                NSLog(@"=====serviceName==7%@===%@",_serviceName,_currentServiceEventStr);

                if (_isVerticalDirection == YES) {
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                    
                }else
                {
                    _serviceTitleLabel.text = _currentServiceEventStr;
                }

            }
            
        }else
        {
            _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",_theVideoInfo.videoName];
            _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(_theVideoInfo.videoName)?@"暂无节目":_theVideoInfo.videoName];
            NSLog(@"=====serviceName==8%@===%@",_serviceName,_currentServiceEventStr);

            if (_isVerticalDirection == YES) {
                _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                
            }else
            {
                _serviceTitleLabel.text = _currentServiceEventStr;
            }

        }
        
        
        if (IS_PERMISSION_GROUP) {
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:_serviceID forKey:@"serviceID"];
            if (_currentDateStr== nil||_currentDateStr.length<=0||[_currentDateStr isEqualToString:@"null"]) {
                _currentDateStr = @"";
            }
            if (_playingEventID == nil|| _playingEventID.length<=0||[_playingEventID isEqualToString:@"null"]) {
                _playingEventID = @"";
            }
            
            [liveDic setObject:_currentDateStr forKey:@"currentDateStr"];
            if (NotNilAndNull(_playingEventID)) {
                [liveDic setObject:_playingEventID forKey:@"eventID"];
            }else
            {
                [liveDic setObject:@"" forKey:@"eventID"];
            }
            [liveDic setObject:@"1" forKey:@"currentPlayType"];
            if (NotNilAndNull(_serviceName)) {
                [liveDic setObject:_serviceName forKey:@"serviceName"];
            }else
            {
            [liveDic setObject:@"" forKey:@"serviceName"];
            }
            [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
        }
        
        
//        NSString *breakpoint = [[UserPlayRecorderCenter defaultCenter]checkTheReWatchListWithEventID:_eventURLID];
        NSString *breakpoint = [_localListCenter checkTheReWatchListWithEventID:_eventURLID andUserType:IsLogin];//ww...
        JumpStartTime =   [breakpoint integerValue];
        _currentPlayType = CurrentPlayEvent;
//        [request EventPlayWithEventID:_eventURLID andTimeout:kTIME_TEN];
        if (ISIntall) {
            
            _installMaskView.hidden = NO;
        }else
        {

        [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_eventURLID andTimeout:KTimeout];
        }
        
    }else
    {
        
        
        if ([self.fullStartTimeStr isEqualToString:[self getTodayStringWithDate]])
        {
            
            
            for (int i = 0; i<_channelListArr.count; i++) {
                NSDictionary *dic = [[NSDictionary alloc]init];
                dic= [_channelListArr objectAtIndex:i];
                NSInteger mark=[self isPlayingWith:[dic objectForKey:@"startTime"] endTime:[dic objectForKey:@"endTime"]];
                
                if (mark == 0) {
                    _playingEventID = [dic objectForKey:@"id"];
                    NSMutableDictionary *dicChannel = [[NSMutableDictionary alloc]init];
                    dicChannel = [_channelListArr objectAtIndex:i];
                    _currentPlayID = [dicChannel objectForKey:@"id"];
                    _theVideoInfo.videoName = [dicChannel objectForKey:@"eventName"];
                    _videoPlayPosition = i;
                    _channelSelectIndex = _videoPlayPosition;
                    _currentLiveVideoTitleStr = [NSString stringWithFormat:@"%@",[dicChannel objectForKey:@"eventName"]];
                    liveEndTime = [NSString stringWithFormat:@"%@:00",[dicChannel objectForKey:@"endTime"]] ;
                    liveStartTime = [NSString stringWithFormat:@"%@:00",[dicChannel objectForKey:@"startTime"]];
                    _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,_currentLiveVideoTitleStr];
                    
                    NSLog(@"=====serviceName==9%@===%@",_serviceName,_currentServiceEventStr);

                    break;
                    
                }
                
                
            }
            
            
            if (_channelSelectIndex == -1) {
                return;
            }
            
            if (IS_PERMISSION_GROUP) {
                
                
                
                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                [liveDic setObject:_serviceID forKey:@"serviceID"];
                if (_currentDateStr== nil||_currentDateStr.length<=0||[_currentDateStr isEqualToString:@"null"]) {
                    _currentDateStr = @"";
                }
                if (_playingEventID == nil|| _playingEventID.length<=0||[_playingEventID isEqualToString:@"null"]) {
                    _playingEventID = @"";
                }
                [liveDic setObject:_currentDateStr forKey:@"currentDateStr"];
                if (NotNilAndNull(_playingEventID)) {
                    [liveDic setObject:_playingEventID forKey:@"eventID"];
                }else
                {
                    [liveDic setObject:@"" forKey:@"eventID"];
                }
                [liveDic setObject:@"0" forKey:@"currentPlayType"];
                if (NotNilAndNull(_serviceName)) {
                    [liveDic setObject:_serviceName forKey:@"serviceName"];
                }else
                {
                    [liveDic setObject:@"" forKey:@"serviceName"];
                }

                [DataPlist WriteToPlistDictionary:liveDic ToFile:EPGINFOPLIST];
                
            }
            
            _currentPlayType = CurrentPlayLive;
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
//                [request TVPlayWithServiceID:self.serviceID andTimeout:kTIME_TEN];
                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

            }
            
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    [_EPGListTableView reloadData];
    [_channelListTV reloadData];

    if (_channelListArr.count >_videoPlayPosition)
    {
        
        
        [_EPGListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
         [_channelListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
    }
    
   
    
    
    
    
    
}




-(void)TapSmallScreenbtn_play:(UIButton*)sender
{
    if (self.changeVideoNameTimer) {
        [self.changeVideoNameTimer invalidate];
        self.changeVideoNameTimer = nil;
    }
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    firstPeriodLive = nil;
    startDateLive = [SystermTimeControl GetSystemTimeWith];
    ZTTimeIntervalLive=0;
    [_moviePlayer pause];
    
    _isTapbtn_play = YES;
    
    _playBackgroundIV.hidden = YES;
    [self StartNewTimer];
    
    _allCodeRateLink = @"";
    
    //采集回看直播采集初始时间   初始化服务器的时间
    //采集回看直播采集初始时间   初始化服务器的时间
    self.nowDate = [SystermTimeControl GetSystemTimeWith];
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init] ;
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.nowDateString = [formatter stringFromDate:self.nowDate];
    
    
    
    
    [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    if (_currentPlayType == CurrentPlayEvent) {
        
        
        if (_channelListArr.count >0) {
            for (NSInteger i = 0; i<_channelListArr.count; i++) {
                
                if ([[[_channelListArr objectAtIndex:i] objectForKey:@"id"] isEqualToString:self.eventURLID]) {
                    
                    _playingEventID = [[_channelListArr objectAtIndex:i] objectForKey:@"id"];
                    _videoPlayPosition = i;
                    
                }
                
                
            }
//            NSString *breakpoint =   [[UserPlayRecorderCenter defaultCenter]checkTheReWatchListWithEventID:_eventURLID];
            NSString *breakpoint = [_localListCenter checkTheReWatchListWithEventID:_eventURLID andUserType:IsLogin];//ww...
            JumpStartTime =   [breakpoint integerValue];
            //        [self playBackChannelVideoModoWithURL:self.eventURL andPlayTVType:PlayTVType];
//            [request EventPlayWithEventID:_eventURLID andTimeout:kTIME_TEN];
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
            [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_eventURLID andTimeout:KTimeout];
            }
        }
        
    }else
    {
        
        
        if ([self.fullStartTimeStr isEqualToString:[self getTodayStringWithDate]])
        {
            
            
            for (NSDictionary *dic in _channelListArr) {
                NSInteger mark=[self isPlayingWith:[dic objectForKey:@"startTime"] endTime:[dic objectForKey:@"endTime"]];
                if (mark == 0) {
                    //0是直播
                    _playingEventID = [dic objectForKey:@"id"];
                    break;
                }
            }
            
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }else
            {
//                 [request TVPlayWithServiceID:self.serviceID andTimeout:kTIME_TEN];
                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

            }
           
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    [_EPGListTableView reloadData];
    if (_channelListArr.count >_videoPlayPosition)
    {
        [_EPGListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
    }
    
    
    
}


#pragma mark --
#pragma mark  采集的时间代码



//采集  时间开始
-(NSString*)eventON
{
    NSString *eventONTime;
    
    if (_channelListArr.count>0) {
        if (_channelListArr.count < _videoPlayPosition) {
            return @"";
        }
        if ([[_channelListArr objectAtIndex:_videoPlayPosition] count]>0 ) {
            
            eventONTime =  [[_channelListArr objectAtIndex:_videoPlayPosition]valueForKey:@"fullStartTime"];
        }else
        {
            eventONTime = @"";
        }
    }else
    {
        eventONTime = @"";
    }
    
    return eventONTime;
    
    
}
//over


//采集  时间结束
-(NSString*)eventOFF
{
    NSString *eventOFFTime;
    if (_channelListArr.count>0) {
        
        if (_channelListArr.count < _videoPlayPosition) {
            return @"";
        }
        
        if ([[_channelListArr objectAtIndex:_videoPlayPosition] count]>0) {
            
            
            NSString *timeV = [[[[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"fullStartTime"] componentsSeparatedByString:@" "] objectAtIndex:0];
            
            NSString *endDate = [NSString stringWithFormat:@"%@ %@",timeV,[[_channelListArr objectAtIndex:_videoPlayPosition] valueForKey:@"endTime"]];
            
            eventOFFTime = endDate;
        }else
        {
            eventOFFTime = @"";
        }
        
    }else
    {
        eventOFFTime = @"";
    }
    
    return eventOFFTime;
    
    
}

//over


//事件的时间
-(NSString*)eventTime
{
    NSString *eventTimeStr;
    
    if (_channelListArr.count >0) {
        
        if (_channelListArr.count < _videoPlayPosition) {
            return @"";
        }
        if ([[_channelListArr objectAtIndex:_videoPlayPosition] count]>0) {
            
            
            eventTimeStr = [NSString stringWithFormat:@"%@",[[_channelListArr objectAtIndex:_videoPlayPosition] valueForKey:@"fullStartTime"]];
            
        }else
        {
            eventTimeStr = @"";
        }
        
    }else{
        eventTimeStr = @"";
    }
    
    return eventTimeStr;
    
}

//over

//采集  回看频道EVENTID
-(NSString*)BackLookChannelEventID
{
    NSString* bEventID;
    
    if (_channelListArr.count>0) {
        if (_channelListArr.count < _videoPlayPosition) {
            return @"";
        }
        if ([[_channelListArr objectAtIndex:_videoPlayPosition] count]>0) {
            
            
            
            bEventID =  [[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"id"];
            
            
        }else
        {
            bEventID = @"";
        }
    }else
    {
        bEventID = @"";
    }
    
    
    
    
    return bEventID;
    
    
}

//OVER



//采集    计算节目的时长 （例如新闻联播30分钟），开始时间
-(NSString *)eventTimePlayTime
{
    NSString *stringPlayTime;
//    return nil;
    if (_channelListArr.count>0) {
        if (_channelListArr.count < _videoPlayPosition) {
            return @"";
        }
        if ([[_channelListArr objectAtIndex:_videoPlayPosition] count]>0) {
            
            if (_videoPlayPosition <_channelListArr.count-1) {
//                [[_channelListArr objectAtIndex:_videoPlayPosition] valueForKey:@"fullStartTime"];
                stringPlayTime = [[_channelListArr objectAtIndex:_videoPlayPosition] valueForKey:@"fullStartTime"];
            }
            
            
        }else
        {
            stringPlayTime = @"";
        }
        
    }else
    {
        stringPlayTime = @"";
    }
    
    
    return stringPlayTime;
}






//采集    计算节目的时长 （例如新闻联播30分钟），开始时间与结束时间的时长
-(NSString *)EventLength:(NSString *)eventON andeventOFF:(NSString*)eventOFF
{
    NSString * timeinterval = @"";
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate* inputON = [inputFormatter dateFromString:eventON];
    
    
    NSDate* inputOFF = [inputFormatter dateFromString:eventOFF];
    
    NSTimeInterval time=[inputOFF timeIntervalSinceDate:inputON];
    if (time>0) {
        
        timeinterval = [NSString stringWithFormat:@"%.0f",time];
 
    }else
    {
        timeinterval = @"0";
    }
//    NSString * timeinterval = [NSString stringWithFormat:@"%.0f",time];
    
    
    return timeinterval;
    
    
    
}

//over

- (NSString*)formattedStringForDuration:(NSTimeInterval)duration
{
    //    NSInteger minutes = floor(duration/60);
    //    NSInteger seconds = round(duration - minutes * 60);
    //    return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%ld",(long)seconds];
    
    
}



//采集  观看时长,每回调一次方法，就可计算出时长
-(NSString *)EPGtimeLengthForCurrent
{
    
    // 获取时长的方法
    
    // 获取时长的方法
    
    
    
    NSString *BOString;
    
    NSString *nowString = [NSString stringWithFormat:@"%.0f",[[SystermTimeControl GetSystemTimeWith] timeIntervalSinceDate:self.EPGNowDate]];
    
    
    if (ZTTimeIntervalLive>0) {
        
        BOString = [NSString stringWithFormat:@"%.0d",[nowString intValue] - [[NSString stringWithFormat:@"%.0f",ZTTimeIntervalLive] intValue] ];
        
    }else
    {
        BOString = [NSString stringWithFormat:@"%.0d",[nowString intValue]];
        
    }
    
    
    self.EPGNowDate = [SystermTimeControl GetSystemTimeWith];
    
    
    NSLog(@"BOString+++++++%@===%.f",BOString,ZTTimeIntervalLive);
    
    
    return BOString;
    
    
    //    NSString *nowString = [NSString stringWithFormat:@"%.0f",[[SystermTimeControl GetSystemTimeWith] timeIntervalSinceDate:self.EPGNowDate]];
    //    self.EPGNowDate = [SystermTimeControl GetSystemTimeWith];
    //    return nowString;
    
}


//采集时移的节目名称
-(NSString *)timeMoveCurrentEventName
{
    NSString *timeMoveEventStr;
    NSInteger v =(1000- _ProgressBeginToMove)*14.4;
    
    _MoveTimeInteger = v;
    
    NSTimeInterval vseconds = v;
    
    NSDate *Mideledate = [[SystermTimeControl GetSystemTimeWith] dateByAddingTimeInterval:-vseconds];
    
    NSDateFormatter *formatterHM = [[NSDateFormatter alloc]init];
    //
    [formatterHM setDateFormat:@"HH:mm"];
    
    NSString *MideleTimeHM = [formatterHM stringFromDate:Mideledate];
    
    
        if ([self timeMoveForSliderToChangeServiceTitle:_channelListArr andDragTitle:MideleTimeHM].length>0) {
            timeMoveEventStr = [NSString stringWithFormat:@"%@",[self timeMoveForSliderToChangeServiceTitle:_channelListArr andDragTitle:MideleTimeHM]];
        }else
        {
            timeMoveEventStr = [NSString stringWithFormat:@"%@",@""];
        }
    
    return timeMoveEventStr;
}


//采集  观看时长,每回调一次方法，就可计算出时长
-(NSString *)timeLengthForCurrent
{
    
    // 获取时长的方法
    
    
    
    NSString *BOBOString;
    
    NSString *nowString = [NSString stringWithFormat:@"%.0f",[[SystermTimeControl GetSystemTimeWith] timeIntervalSinceDate:self.nowDate]];
    
    
    if (ZTTimeIntervalLive>0) {
        
        BOBOString = [NSString stringWithFormat:@"%.0d",[nowString intValue] - [[NSString stringWithFormat:@"%.0f",ZTTimeIntervalLive] intValue] ];
        
    }else
    {
        BOBOString = [NSString stringWithFormat:@"%.0d",[nowString intValue]];
        
    }
    self.nowDate = [[NSDate alloc]init];
    self.nowDate = [SystermTimeControl GetSystemTimeWith];
    
    NSLog(@"BOBOString+++++++%@ == %.f",BOBOString,ZTTimeIntervalLive);
    
    return BOBOString;
    
    
}

//over



//over


-(void)allcodeRateRequest:(NSURL *)url
{
    _errorManager.startErrorDate = [SystermTimeControl getNowServiceTimeDate];
    _adPlayerView.timeLabel.text = @"";
    [_errorManager resetPlayerStatusInfo];
    [self clearMoviePlayerStop];
    if (_isHaveAD == NO) {
//        if (_adPlayerView.isCanContinu == NO) {
//            _isLiveHasAD = NO;
//
//        }else
//        {
//            _isLiveHasAD = YES;
//        }
        
        if (_currentPlayType == CurrentPlayLive) {
            _isLiveHasAD = NO;
        }
        
        
    NSString *playLink = [url absoluteString];
    NSString *formLink = [[playLink componentsSeparatedByString:@"?"] firstObject];
    NSString *type;
    if ([playLink rangeOfString:@"?"].location == NSNotFound) {
        type= [[playLink componentsSeparatedByString:@"."] lastObject];
    }else{
        type= [[formLink componentsSeparatedByString:@"."] lastObject];
    }
    if (![type isEqualToString:@"m3u8"]) {
        
         [clearTitles removeAllObjects];
        [self allCodeClearButtonTitle];
        _moviePlayer.movieSourceType = MPMovieSourceTypeFile;
        [_moviePlayer setContentURL:url];
        
//        _playPauseBigBtn.hidden = YES;//kael_hidden
        [_moviePlayer play];
        _isPrepareToPlay = YES;
        self.isPlay = YES;
        [self actionPlayBackRecorderListRepitTime];
        return;
        
    }
    if (_allCodeRateLink.length>0) {
        
        NSMutableString *URLStr;
        if (_allCodeRateLink.length>15) {
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:0]];
            [self allCodeClearButtonTitle];

            URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:PlayTVType andLiveType:0]];
            
        }else{
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            [self allCodeClearButtonTitle];

            [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self.view withLocationTag:1];
            return ;
        }
        //                    URLStr = [[URLStr componentsSeparatedByString:@"?"] objectAtIndex:0];
        if (URLStr.length<10 ) {
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            [self allCodeClearButtonTitle];

            [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self.view withLocationTag:1];
            
        }else{
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:0]];
            [self allCodeClearButtonTitle];

            [URLStr deleteCharactersInRange:NSMakeRange(URLStr.length-3, 2)];
            _theVideoInfo.playLink = URLStr;
            _moviePlayer.movieSourceType = MPMovieSourceTypeFile;
            [_moviePlayer setContentURL:[self getCanPlayURLWithString:URLStr]];
            // [_moviePlayer setContentURL:[NSURL URLWithString:URLStr ]];
            [_moviePlayer prepareToPlay];
            [_moviePlayer play];
//            _playPauseBigBtn.hidden = YES;//kael_hidden
            _isPrepareToPlay = YES;
            _isPlay = YES;
            
            [self actionPlayBackRecorderListRepitTime];

        }
        
        
        return;
    }
    
    NSString *mUrl = [NSString stringWithFormat:@"%@",url];
//        mUrl = [mUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:mUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSString *text = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                        _allCodeRateLink = text;
                        if (clearTitles.count>0) {
                            [clearTitles removeAllObjects];
                        }
                        clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:1]];
                        [self allCodeClearButtonTitle];
            
                        NSMutableString *URLStr;
                        if (text.length>15) {
                            if (PlayTVType == 5) {
                                //----自动 默认选择
                                URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:text andTVCodeType:4 andLiveType:0]];
            
                            }else{
            
                                URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:text andTVCodeType:PlayTVType andLiveType:0]];
            
                            }
            
                        }else{
            
                            [AlertLabel AlertInfoWithText:@"该码率暂无法播放" andWith:self.view withLocationTag:1];
                            return ;
                        }
            
                        //                    URLStr = [[URLStr componentsSeparatedByString:@"?"] objectAtIndex:0];
                        if (URLStr.length<22 ) {
                            [AlertLabel AlertInfoWithText:@"该码率暂无法播放" andWith:self.view withLocationTag:1];
            
                        }else{
                            URLStr = [self deleteString:@" " fromString:URLStr];
            
                            //         [URLStr deleteCharactersInRange:NSMakeRange(URLStr.length-2, 2)];
            
                            NSLog(@"当前码率 ： %@",URLStr);
            
                            _theVideoInfo.playLink = URLStr;
                            //            _moviePlayer.movieSourceType = MPMovieSourceTypeFile;
            
//                                       [_moviePlayer setContentURL:[self getCanPlayURLWithString:URLStr]];
                            [_moviePlayer setContentURL:[NSURL URLWithString:[URLStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                            
                            [_moviePlayer prepareToPlay];
                            [_moviePlayer play];
                            //            _playPauseBigBtn.hidden = YES;//kael_hidden
                            
                            _isPrepareToPlay = YES;
                            _isPlay = YES;
                            [self actionPlayBackRecorderListRepitTime];

                        }

                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                        [self clearMoviePlayerStop];
                        [_moviePlayer setContentURL:url];
            
                        _adPlayerView.timeLabel.text = @"";
                        _isPrepareToPlay = YES;
                        self.isPlay = YES;
            
                        [_moviePlayer prepareToPlay];
                        [_moviePlayer play];
                        [self actionPlayBackRecorderListRepitTime];

//            [self clearMoviePlayerStop];
//            [_moviePlayer setContentURL:url];
//
//            _adPlayerView.timeLabel.text = @"";
//            _isPrepareToPlay = YES;
//            self.isPlay = YES;
//            
//            [_moviePlayer prepareToPlay];
//            [_moviePlayer play];

        }];
        
        [clearTitles removeAllObjects];
        [self allCodeClearButtonTitle];

        if (JumpStartTime <0) {
            JumpStartTime = 0;
            
        }
        if (IsLogin) {
            NSDictionary *dic = [[NSDictionary alloc]init];
            
//            dic = [[UserPlayRecorderCenter defaultCenter] getAReWatchItemWithEventID:_playingEventID];
            dic = [_localListCenter getAReWatchItemWithEventID:_playingEventID andUserType:YES];//ww...
            _theVideoInfo.fullStartTimeP = @"";
            JumpStartTime = [[dic valueForKey:@"breakPoint"] integerValue];
        }else
        {
            JumpStartTime = 0;
        }
        
        NSInteger break_hours = (JumpStartTime/3600);
        NSInteger break_minutes = (JumpStartTime - (break_hours*3600)) / 60 ;
        NSInteger break_seconds =(JumpStartTime -  break_hours*3600 - break_minutes*60);
        
        
        if (JumpStartTime>0) {
            [AlertLabel AlertInfoWithText:[NSString stringWithFormat:@"您上次观看至%02ld:%02ld:%02ld,即将为您续播",(long)break_hours,(long)break_minutes,(long)break_seconds] andWith:_contentView withLocationTag:2];
        }

//    [self StartNewTimer];
    }else
    {
        _adPlayerView.isCanContinu = YES;
        if (_adPlayerView.maskViewType == ImageAD) {
            
            [_adPlayerView resetImageADViewWithData:self.theVideoInfo.adList];
        }else if (_adPlayerView.maskViewType == VideoAD)
        {
            [self clearMoviePlayerStop];
            _adPlayerView.timeLabel.text = @"";
            [_moviePlayer setContentURL:[NSURL URLWithString:_adVideoURL]];
            _isPrepareToPlay = YES;
            self.isPlay = YES;

            [_moviePlayer prepareToPlay];
            [_moviePlayer play];
            
          
        }
        

    }
    
    
}

-(NSURL *)getCanPlayURLWithString:(NSString *)urlStr{
    
    NSMutableString *mCanplay = [NSMutableString stringWithFormat:@"%@",urlStr];
    [mCanplay deleteCharactersInRange:NSMakeRange(mCanplay.length-2, 2)];
    
    NSURL *canPlayURL = [NSURL URLWithString:mCanplay];
    return canPlayURL;
}


#pragma mark --
#pragma mark  精简小方法


-(void)resetHiddenEPGDataAndNoDataLabel:(BOOL)isHaveData
{
    [self reStartHiddenTimer];
    if (isHaveData == NO) {
        _noEPGListViewLabel.hidden = NO;
        _EPGListTableView.hidden = YES;
        _channelListTV.hidden = YES;
        if (_isVerticalDirection == NO) {
            [[self.view viewWithTag:8080]setHidden:NO];
            
        }else
        {
            [[self.view viewWithTag:8080]setHidden:YES];
        }

//        [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?NO:YES];
//        [[self.view viewWithTag:8080]setHidden:NO];
    }else
    {
        _noEPGListViewLabel.hidden = YES;
        _EPGListTableView.hidden = NO;
        _channelListTV.hidden = NO;
//        [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?YES:NO];
//        [[self.view viewWithTag:8080]setHidden:YES];
////        if (_isVerticalDirection == NO) {
//            [[self.view viewWithTag:8080]setHidden:NO];
//            
//        }else
//        {
            [[self.view viewWithTag:8080]setHidden:YES];
//        }

        
    }
}















#pragma mark --
#pragma mark  自动旋转

//PauseTVPlayWithServiceID
- (void)PauseTVPlayModeWithURL:(NSURL *) url andPlayTVType:(NSInteger)PlayTVType
{
    
//    _currntPlayLabel.hidden = NO;
//    _nextPlayLabel.hidden = NO;
//    _currentPlayBackLabel.hidden = YES;
    _playButton.hidden = NO;
    _backEventTagIV.hidden = YES;
//    [self clearMoviePlayerStop];
    
    if (_isVerticalDirection == NO) {
        [self setBigScreenPlayer];
    }
    
    _theVideoInfo.playLink = [NSString stringWithFormat:@"%@",url];
    
    if (!JumpStartTime) {
        JumpStartTime = 0;
    }
    
    
   
        if ([netWork isEqualToString:@"WIFI"]) {
            
            [self allcodeRateRequest:url];
            
        }else if ([netWork isEqualToString:@"无网络"])
        {
            return;
        }else if([netWork isEqualToString:@"2G"]||[netWork isEqualToString:@"3G"]||[netWork isEqualToString:@"4G"])
        {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
                
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 2222;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                }else
                {
                    [self allcodeRateRequest:url];
                    
                }
                
                
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
                
                
                
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 333333;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
               
                }else
                {
                    [self allcodeRateRequest:url];
                }
            
            }
            
  
        }

}







- (void) playVideoModeWithURL:(NSURL *) url andPlayTVType:(NSInteger)PlayTVType
{
    //    if ([[url description] length] == 0) {
    //        [self displayError:nil];
    //        return;
    //    }
    

    _currentPlayType = CurrentPlayLive;
    _backEventTagIV.hidden = YES;
    _theVideoInfo.playLink = [NSString stringWithFormat:@"%@",url];
//    _currentPlayBackLabel.hidden = YES;
//    if (_isVerticalDirection == YES) {
//        _currntPlayLabel.hidden = YES;
//        _nextPlayLabel.hidden = YES;
//    }else
//    {
//        _currntPlayLabel.hidden = NO;
//        _nextPlayLabel.hidden = NO;
//    }
    
    //    _currntPlayLabel.hidden = NO;
    //    _nextPlayLabel.hidden = NO;
    _playButton.hidden = YES;
    _errorMaskView.hidden = YES;
    _topBarIV.hidden = YES;
    _bottomBarIV.hidden = YES;
    _polyLoadingView.hidden = NO;


    if (_isVerticalDirection == NO) {
        [self setBigScreenPlayer];
    }
    [self clearMoviePlayerStop];
    //精简
    
    
   
        if ([netWork isEqualToString:@"WIFI"]) {
            
            [self allcodeRateRequest:url];
            
        }else if ([netWork isEqualToString:@"无网络"])
        {
            return;
        }else if([netWork isEqualToString:@"2G"]||[netWork isEqualToString:@"3G"]||[netWork isEqualToString:@"4G"])
        {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 2222;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
                
                
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
                
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 333333;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
        
            
        }

    }
    
    
    
    
   
    
//
//    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
//
//        if ([netname isEqualToString:@"当前无可用网络"]) {
//            return ;
//        }else if([netname isEqualToString:@"您正在使用移动网络"]){
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 2222;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//            }
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
//                
//                //            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 333333;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//                
//                
//                
//                
//                
//            }
//            
//            
//            
//            
//            
//            
//        }else{
//            [self allcodeRateRequest:url];
//            
//        }
//        
//    }];
    
    
    
}



-(void)displayError:(NSError *)theError
{
    if (theError)
    {
        
        //        [_forwardButton setImage:[UIImage imageNamed:@"btn_lastI_none"] forState:UIControlStateNormal];
        //        [_forwardButton setEnabled:NO];
        //        [_backwardButton setImage:[UIImage imageNamed:@"btn_player_movenext_normal"] forState:UIControlStateNormal];
        //        [_backwardButton setEnabled:NO];
        
        [self performSelector:@selector(returnBackBtnAction:) withObject:nil afterDelay:2];
    }
}


//回看

- (void) playBackChannelVideoModoWithURL:(NSURL *)url andPlayTVType:(NSInteger)PlayTVType
{
    
    _polyLoadingView.hidden = NO;

    _backEventTagIV.hidden = NO;
    _errorMaskView.hidden = YES;
    _bottomBarIV.hidden = YES;

//    if ([[url description] length] == 0) {
//        [self displayError:nil];
//        return;
//    }
    
    //    _moviePlayer = nil;
    [self clearMoviePlayerStop];
    
    
    
    
    _videoProgressV.value = 0;
    _isPaused = NO;
    
    
    
    [_currentTimeL setText:@"00:00:00"];
    [_totalTimeL setText:@"00:00:00"];
    
    
    
    _currentPlayType = CurrentPlayEvent;
    
    
  
        if ([netWork isEqualToString:@"WIFI"]) {
            
            [self allcodeRateRequest:url];
            
        }else if ([netWork isEqualToString:@"无网络"])
        {
            return;
        }else if([netWork isEqualToString:@"2G"]||[netWork isEqualToString:@"3G"]||[netWork isEqualToString:@"4G"])
        {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 2222;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
                
                
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
                
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 333333;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
                
                
                
                
            }
            
    }
    
    
    //    if (!JumpStartTime) {
    //        JumpStartTime = 0;
    //    }
    
    //    _isPrepareToPlay = YES;
    
//    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
//        if ([netname isEqualToString:@"当前无可用网络"]) {
//            return ;
//        }else if([netname isEqualToString:@"您正在使用移动网络"]){
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 2222;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//            }
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
//                
//                //            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 333333;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//                
//          
//                
//            }
//            
//            
//        }else{
//            [self allcodeRateRequest:url];
//            
//        }
//        
//    }];
    

    //    self.isPlay = YES;
    
    
   
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"REFRESHRECORDERTABLECELL" object:nil];
}




-(void)actionPlayBackRecorderListRepitTime
{
    if (self.getPlayBackRecordListRepitTimer) {
        [self.getPlayBackRecordListRepitTimer invalidate];
        self.getPlayBackRecordListRepitTimer = nil;
    }
    self.getPlayBackRecordListRepitTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(recordPlaybackTime) userInfo:nil repeats:YES];
    
}




-(NSInteger)recordPlaybackTime
{
    recordTime = 0;
    
    if (_isHaveAD == NO) {
        recordTime = (NSInteger)(_moviePlayer.currentPlaybackTime);
        //    JumpStartTime = recordTime;
        
        if (recordTime >=0) {
            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
            
            [request EventStopWithEventID:_playingEventID andBreakPoint:recordTime andTimeout:kTIME_TEN];
        }
        
        
        NSLog(@"******?断点%ld",recordTime);
        return recordTime;

    }else
    {
        return 0;

    }
    
    
    
    
}




#pragma mark - 多码率相关
//多码率
-(NSMutableString *)deleteString:(NSString *)DStr fromString:(NSMutableString *)FStr{
    NSMutableString *mFstr = [[NSMutableString alloc] initWithFormat:@"%@",FStr];
    NSInteger location = [FStr rangeOfString:DStr].location;
    if ([FStr rangeOfString:DStr].location == NSNotFound) {
        return FStr;
    }else{
        [mFstr deleteCharactersInRange:NSMakeRange(location, 2)];
        [self deleteString:DStr fromString:mFstr];
    }
    
    return FStr;
    
}
//根据 直播方式LiveType 和 清晰度TVCodeType
-(NSString *)getHIGHDefintionUrlStringWithM3U8String:(NSString *)m3u8 andTVCodeType:(NSInteger)TVCodeType andLiveType:(NSInteger)LiveType{
    
    //-----------type    1：超清  2：高清  3：标清  4：流畅
    if (m3u8.length<20) {
        //如果M3U8字符本来就是错误的那么就不用解析了 直接返回空播放链接
        return @"";
    }
    NSMutableString *m3u8Str = [[NSMutableString alloc] initWithString:m3u8];
    [m3u8Str deleteCharactersInRange:NSMakeRange(0, 7)];
    NSArray *m3u8Arr = [m3u8Str componentsSeparatedByString:@"?"];
    NSMutableArray *IPArr = [[NSMutableArray alloc] init];
    NSMutableArray *bandWidthArr = [[NSMutableArray alloc] init];
    
    //------分割 拼接 播放链接数组 和 带宽（码率）数组
    for (int i=0; i<m3u8Arr.count; i++) {
        NSString *str = [m3u8Arr objectAtIndex:i];
        NSString *IPStr = [[str componentsSeparatedByString:@"http"] lastObject];
        NSString *bandStr = [[[[str componentsSeparatedByString:@"http"] objectAtIndex:0] componentsSeparatedByString:@"BANDWIDTH="] lastObject];
        if ([bandStr rangeOfString:@","].location!=NSNotFound) {
            //带宽（码率）字符串后面还有可能 拼接上其他的字符 是以","字符分割的
            bandStr = [[bandStr componentsSeparatedByString:@","] firstObject];
        }
        [IPArr addObject:[NSString stringWithFormat:@"http%@",IPStr]];
        [bandWidthArr addObject:bandStr];
    }
    [bandWidthArr removeLastObject];//过滤掉非数值的 假的 带宽元素
    
    NSArray *truthArr = [m3u8Str componentsSeparatedByString:@"#"];
    NSMutableArray *mutTruthArr = [[NSMutableArray alloc] initWithArray:truthArr];
    //zzsc
    for (int i=0; i<mutTruthArr.count; i++) {
        NSString *link = [mutTruthArr objectAtIndex:i];
        if (link.length<22) {
            [mutTruthArr removeObjectAtIndex:i];
        }
    }
    
    NSMutableArray *longIPArr = [[NSMutableArray alloc] init];//这个数组用来接收 拼接上 http 字符的的真正的全连接
    for (int i=0; i<mutTruthArr.count; i++) {
        
        NSArray *itemArr = [[mutTruthArr objectAtIndex:i] componentsSeparatedByString:@"http"];
        [longIPArr addObject:[NSString stringWithFormat:@"http%@",[itemArr lastObject]]];
        
    }
    //------码率 、连接 分割结束
    
    if (longIPArr.count>0) {
        
        NSString *firstLink = [longIPArr objectAtIndex:0];
        if ([firstLink hasPrefix:@"httpEXT"]) {
            [longIPArr removeObjectAtIndex:0];//就点播的需要加 因为点播的比直播多了一行
            
        }
        NSString *lastLink = [longIPArr lastObject];
        if ([lastLink hasPrefix:@"httpEXT"]) {
            [longIPArr removeLastObject];
        }
        
    }else{
        return @"";
    }
    //-----------数组排序
    if (longIPArr.count==bandWidthArr.count) {
        //只有IP数组和带宽数组长度一致才允许进入这里
        
        for (int i=0; i<bandWidthArr.count; i++) {
            
            for (int j=1; j<bandWidthArr.count; j++) {
                //码率（带宽）按照从高到底排列
                if ([bandWidthArr objectAtIndex:i]<[bandWidthArr objectAtIndex:j]) {
                    [bandWidthArr exchangeObjectAtIndex:i withObjectAtIndex:j];
                    [longIPArr exchangeObjectAtIndex:i withObjectAtIndex:j];
                }
                
            }
            
        }
        
        
    }else{
        
        longIPArr = (NSMutableArray *)[[longIPArr reverseObjectEnumerator] allObjects];
    }
    //--------数组排序结束

    if (TVCodeType<0 || TVCodeType>4) {
        
        return [longIPArr lastObject];//默认 播放流畅的
    }
    
    //zzs码率策略修改   下面的是
    if (longIPArr.count>(4-TVCodeType)) {
        if (TVCodeType<=0) {
            return [longIPArr firstObject];
        }
        return [longIPArr objectAtIndex:(4-TVCodeType)];
        
    }else{
        return @"";
        
    }
    //zzs码率策略修改  上面的是
    switch (TVCodeType) {
        case 1:{
            for (int i=0; i<longIPArr.count; i++) {
                
                if (bandWidthArr.count<=i) {
                    return @"";
                }
                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==1) {
                    
                    return [longIPArr objectAtIndex:i];
                }
            }
            
            break;
        }
        case 2:{
            for (int i=0; i<longIPArr.count; i++) {
                if (bandWidthArr.count<=i) {
                    return @"";
                }
                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==2) {
                    
                    
                    return [longIPArr objectAtIndex:i];
                }
            }
        }
        case 3:{
            for (int i=0; i<longIPArr.count; i++) {
                if (bandWidthArr.count<=i) {
                    return @"";
                }
                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==3) {
                    
                    return [longIPArr objectAtIndex:i];
                }
            }
        }
        case 4:{
            for (int i=0; i<longIPArr.count; i++) {
                if (bandWidthArr.count<=i) {
                    return @"";
                }
                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==4) {
                    
                    return [longIPArr objectAtIndex:i];
                }
            }
        }
    }
    
    return @"";
}



//--------------根据码率  区分高清还是标清
-(NSInteger)getTheTypeWithTVRate:(CGFloat)rate andTVType:(NSInteger)type{
    if (type==0) {
        if (rate< 524288) {// 0.5M 524288 0.8M  819200
            return 4;//流畅
        }
        if (rate>524288&&rate<1048576) {
            return 3;//标清
        }
        if (rate>1048576&&rate<2097152) {
            return 2;//高清
        }
        if (rate>2097152&&rate<4194304) {
            return 1;//超清
        }
        
    }else{
        
        if (rate<1048576) {
            return 4;//流畅
        }
        if (rate>1048576&&rate<2097152) {
            return 3;//标清
        }
        if (rate>2097152&&rate<4194304) {
            return 2;//高清
        }
        if (rate>4194304&&rate<8388608) {
            return 1;//超清
        }
        
    }
    return 0;//返回类型 3：超清 2：高清 1：标清 0：流畅
    //   type:  0-直播 1-回看
    /*
     0.5M - 524288
     1M - 1024     1048576
     2M - 2048     2097152
     4M - 4096     4194304
     8M - 8192     8388608
     -------直播---------
     超清：2M--4M
     高清：1M--2M
     标清：0.5M--1M
     流畅：<0.5M
     --------回看---------
     超清：4M--8M
     高清：2M--4M
     标清：1M--2M
     流畅：<1M
     */
}




-(NSMutableArray *)getCodeRateCategoryWithLivType:(NSInteger)liveType{
    
    NSMutableArray *mCodeArr = [[NSMutableArray alloc] init];
    
    NSString *superRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:1 andLiveType:0];
    NSString *highRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:2 andLiveType:0];
    NSString *biaoRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:3 andLiveType:0];
    NSString *liuRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:4 andLiveType:0];
    
    if (superRateURL.length>0) {
        //        [mCodeDic setObject:@"超清" forKey:@"superRateURL"];
        NSMutableDictionary *superdic = [[NSMutableDictionary alloc]init];
        [superdic setValue:@"超清" forKey:@"type"];
        [superdic setValue:[NSString stringWithFormat:@"%d",SUPER_CLEAR] forKey:@"tag"];
        [mCodeArr addObject:superdic];
    }
    
    if (highRateURL.length>0) {
        NSMutableDictionary *highdic = [[NSMutableDictionary alloc]init];
        [highdic setValue:@"高清" forKey:@"type"];
        [highdic setValue:[NSString stringWithFormat:@"%d",HIGH_DEFINE] forKey:@"tag"];
        [mCodeArr addObject:highdic];
    }
    
    
    if (biaoRateURL.length>0) {
        NSMutableDictionary *origindic = [[NSMutableDictionary alloc]init];
        [origindic setValue:@"标清" forKey:@"type"];
        [origindic setValue:[NSString stringWithFormat:@"%d",ORIGINAL_PAINT] forKey:@"tag"];
        [mCodeArr addObject:origindic];
        
    }
    
    
    if (liuRateURL.length>0) {
        //        [mCodeDic setObject:@"流畅" forKey:@"liuRateURL"];
        NSMutableDictionary *smoothdic = [[NSMutableDictionary alloc]init];
        [smoothdic setValue:@"流畅" forKey:@"type"];
        [smoothdic setValue:[NSString stringWithFormat:@"%d",SMOOTH] forKey:@"tag"];
        [mCodeArr addObject:smoothdic];
        
    }
    
    
    return mCodeArr;
}


-(NSMutableArray *)eventListCombine:(NSMutableArray*)EPGEventList andFullDateFormatter:(NSDateFormatter *)formatter
{
    __block NSMutableArray *itemArr = [NSMutableArray array];
    
    
    [EPGEventList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:obj];
        NSDate *NewstartTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@",self.fullStartTimeStr,[dic valueForKey:@"startTime"]]];
        _theVideoInfo.videoTime =  [NSString  stringWithFormat:@"%@ %@",self.fullStartTimeStr,[dic objectForKey:@"startTime"]];
        NSDate *NewendTime   = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@",self.fullStartTimeStr,[dic valueForKey:@"endTime"]]];
        //                        NSString *nowDateStr = [formatter stringFromDate:[NSDate date]];
        //                        NSDate *nowDate = [formatter dateFromString:nowDateStr];
        //                                NSDate *nowDate = [NSDate date];
        NSDate *nowDate = [SystermTimeControl GetSystemTimeWith];
        //时间的问题，再为dic添加一个fullStarttime属性，带日期的
        [dic setValue:[NSString stringWithFormat:@"%@ %@",self.fullStartTimeStr,[dic valueForKey:@"startTime"]] forKey:@"fullStartTime"];
        if ([nowDate compare:NewstartTime]<0) {
            //playType 0播放, 1回看, 2预约, 3播放失效, 4回看失效
            //ifCanPlay 1 可以 0 不可以
            [dic setValue:@"2" forKey:@"playType"];
            [dic setValue:@"0" forKey:@"ifCanPlay"];
        }else{
            if ([NewendTime compare:nowDate]>0) {
                [dic setValue:@"0" forKey:@"playType"];
                if ([_theVideoInfo.PlayTag isEqualToString:@"0"]) {
                    [dic setValue:@"0" forKey:@"ifCanPlay"];
                }else{
                    [dic setValue:@"1" forKey:@"ifCanPlay"];
                }
            }else{
                [dic setValue:@"1" forKey:@"playType"];
                if ([_theVideoInfo.PlayBackTag isEqualToString:@"0"]) {
                    [dic setValue:@"0" forKey:@"ifCanPlay"];
                }else{
                    [dic setValue:@"1" forKey:@"ifCanPlay"];
                }
            }
        }
        
        [itemArr addObject:dic];
    }];
    
    
    return itemArr;
    
    
}




-(void)NoneEPGDataAndPlayServiceID
{
    if (_IsFirstChannelist == YES && _serviceID) {
        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
        if (ISIntall) {
            _installMaskView.hidden = NO;
        }else
        {
        
//        [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
            [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

        }
        _IsFirstChannelist = NO;
        [self resetHiddenEPGDataAndNoDataLabel:NO];
        return;
        
    }
    
}





- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"结果广告: %ld result:%@  type:%@",status,result,type );
    switch (status) {
        case 0:
        {
            
            if ([type isEqualToString:JXTV_BUSINESS_INSTALLINFO]) {
                NSInteger ret = [[result objectForKey:@"ret"] integerValue];
                _blackInstallView.hidden = YES;
                if (ret == 0) {
                    
                    ZSToast(@"报装成功啦 小晴会尽快与您联系^_^")
                
                }else
                {
                    ZSToast(@"提交信息失败啦 再来一次吧 加油^_^");
                }
                
            }
        

            if ([type isEqualToString:EPG_TYPELIST]) {
                NSLog(@"结果: %ld result:%@  type:%@",status,result,type );
                
                
                NSDictionary *dicOne = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"id",@"",@"name",@"",@"type",@"喜爱",@"shortName", nil];
                
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"id",@"",@"name",@"",@"type",@"全部",@"shortName", nil];
                
                
                typelistARR = [NSMutableArray arrayWithObjects:dicOne,dic ,nil];
                
                
                //                [typelistARR addObject:dicOne];
                //                [typelistARR addObject:dic];
                [typelistARR addObjectsFromArray:[result valueForKey:@"typeList"]];
                [self setCategoryArr:typelistARR];
//                _categoryArr = typelistARR;
                NSLog(@"*******MMM%@",typelistARR);
//                if (_categoryArr.count >0) {
//                    [self updateVideoListUI];
//                }else
//                {
//                    return;
//                }
                
                
                
            }
            if ([type isEqualToString:@"allCategoryChannelList"] || [type isEqualToString:EPG_CHANNELLIST]) {
                
                int ret = [[result objectForKey:@"ret"] intValue];
                if (ret!=0) {
                    return;
                }
                
                if (ret == 0) {
                    //                    NSLog(@"YYYYYY%@",result);
                    NSDictionary *Dic = [[NSDictionary alloc] initWithDictionary:result];
                    //                    NSLog(@"******&&&&*****%@",[Dic valueForKey:@"channelList"]);
                    
                    //                    [_dicArr addObject:_favourChannelArr];
                    
                    _dicArr = [Dic valueForKey:@"channelList"];
                    
                    
                    _allCategoryEPGServiceEventArr =  [self EPGGetAllCategory:_dicArr];
                    
                    NSLog(@"_allCategoryEPGServiceEventArr%@",_allCategoryEPGServiceEventArr);
                    
                    
                    _videoListArr = [self getFavourEPG:_allCategoryEPGServiceEventArr];
                    
                    //只要获取了值，那么久赋值channelImage
                    
                    _theVideoInfo.channelImage = [self getChannelIMage:_allCategoryEPGServiceEventArr];
                    
                    
                    _theVideoInfo.channelName = [self getChannelName:_allCategoryEPGServiceEventArr];
                    
                    
                    
                    [_videoListTV reloadData];
                    
                }
            }
            if ([type isEqualToString:EPG_EVENTLIST]) {
                
                //                NSLog(@"&&&&&&&&%@",[result valueForKey:@"epginfo"]);
                
                int ret = [[result objectForKey:@"ret"] intValue];
                if (ret != 0 ) {
                    
                    [self NoneEPGDataAndPlayServiceID];
                   
                }
                
                
                switch (ret) {
                    case 0:
                    {
                        if ([[result valueForKey:@"epginfo"] count] >0) {
                            
                            NSMutableArray *muArr = [result valueForKey:@"epginfo"];
                            
                            //        NSLog(@"添加播放状态前的数组muArr = %@",muArr);
                            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
                            [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                            
                            self.fullStartTimeStr = _currentDateStr;
                            
                            _channelListArr = [self eventListCombine:muArr andFullDateFormatter:formatter];
                            if (_channelListArr.count >0) {
                                
                                if (_isBackPlayBeforeZero == YES) {
                                    if ([[[_channelListArr objectAtIndex:0] valueForKey:@"playType"] intValue]== 1) {
                                        _videoPlayPosition = 0;
                                        _playingEventID = [[_channelListArr objectAtIndex:0] valueForKey:@"id"];
                                        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                                        
                                        //                                        [request EventPlayWithEventID:_playingEventID andTimeout:kTIME_TEN];
                                        if (ISIntall) {
                                            _installMaskView.hidden = NO;
                                        }else
                                        {
                                        [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:KTimeout];
                                        }
                                    }
                                    if ([[[_channelListArr objectAtIndex:0] valueForKey:@"playType"] intValue]== 0) {
                                        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                                        if (ISIntall) {
                                            _installMaskView.hidden = NO;
                                        }else
                                        {
//                                        [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                                            [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                                        }
                                    }
                                    
                                    
                                    _isBackPlayBeforeZero = NO;
                                }
                                
                                if (_isBackPlayAfterZero == YES) {
                                    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                                    if (ISIntall) {
                                        _installMaskView.hidden = NO;
                                    }else
                                    {
//                                    [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
                                        [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                                    }
                                    _isBackPlayAfterZero =NO;
                                }
                                
                                
                                
                                if (_IsFirstChannelist == YES) {
                                    [self TapSmallScreenbtn_playNoButton];
                                    
                                    _IsFirstChannelist = NO;
                                    
                                }
                                
                                if (_isLocationOrder == YES) {
                                    _isLocationOrder = NO;
                                    
                                    for (NSInteger i = 0; i<_channelListArr.count; i++) {
                                        
                                        if ([[[_channelListArr objectAtIndex:i] objectForKey:@"id"] isEqualToString:self.eventURLID]) {
                                            
                                            NSDictionary *eventDic = [[NSDictionary alloc]init];
                                            
                                            eventDic = [_channelListArr objectAtIndex:i];
                                            _playingEventID = [eventDic objectForKey:@"id"];
                                            _videoPlayPosition = i;
                                            _channelSelectIndex = _videoPlayPosition;
                                            
                                            
                                            break;
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                // 如果播放的视频是今天的，就隔一段时间请求次EPG信息，否则不再请求
                                NSLog(@"--==--------");
                                if ([[[_theVideoInfo.videoTime componentsSeparatedByString:@" "] objectAtIndex:0] isEqualToString:[[[self getTodayStringWithDate] componentsSeparatedByString:@" "] objectAtIndex:0]]) {
                                    if (_currentPlayType == CurrentPlayLive) {
                                        
                                        if (_isLastPlayLive == YES) {
                                            HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
                                            if (ISIntall) {
                                                _installMaskView.hidden = NO;
                                            }else
                                            {
//                                            [request TVPlayWithServiceID:_serviceID andTimeout:YES];
                                                [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

                                            }
                                            _isLastPlayLive = NO;
                                        }
                                        
                                        
                                        // 当当前的直播节目播放完一个段落后，更新视频名称
                                        NSString *yearEndTime = [[self.fullStartTimeStr componentsSeparatedByString:@" "] objectAtIndex:0];
                                        NSString *videoPositionString;
                                        if (_channelListArr.count>0) {
                                            if (_videoPlayPosition<_channelListArr.count-1) {
                                                
                                                if ([_channelListArr objectAtIndex:_videoPlayPosition]) {
                                                    videoPositionString =  [[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"endTime"];
                                                    liveEndTime = [NSString stringWithFormat:@"%@:00",[[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"endTime"]] ;
                                                    liveStartTime = [NSString stringWithFormat:@"%@:00",[[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"startTime"]];
                                                    
                                                    //                                                    _playingEventID = [[_channelListArr objectAtIndex:_videoPlayPosition] objectForKey:@"id"];
                                                    //                                                    _channelSelectIndex = _videoPlayPosition;
                                                    
                                                    
                                                }else
                                                {
                                                    return;
                                                }
                                            }else
                                            {
                                                return;
                                            }
                                        }
                                        
                                        
                                        
                                        NSString *endTime = [NSString stringWithFormat:@"%@ %@:00",yearEndTime, videoPositionString];
                                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                        //                [dateFormatter setDateFormat:@"HH:mm:ss"];
                                        NSDate *endDate = [dateFormatter dateFromString:endTime];
                                        
                                        
                                        
                                        if (self.changeVideoNameTimer) {
                                            [self.changeVideoNameTimer invalidate];
                                            self.changeVideoNameTimer = nil;
                                        }
                                        
                                        
                                        NSDateFormatter *MdateFormatter = [[NSDateFormatter alloc] init];
                                        [MdateFormatter setDateFormat:@"yyyy-MM-dd"];
                                        NSDate *showDate = [MdateFormatter dateFromString:_currentDateStr];
                                        
                                        //这里是1.0f后 你要做的事情
                                        //                                        NSTimeInterval delay = [endDate timeIntervalSinceDate:[SystermTimeControl GetSystemTimeWith]];
                                        NSTimeInterval delay = [endDate timeIntervalSinceDate:showDate];
                                        
                                        
                                        NSLog(@"延迟时间%2lf",delay);
                                        
                                        self.changeVideoNameTimer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(playTheNextVideo) userInfo:nil repeats:NO];
                                        _isLastPlayLive = NO;
                                        
                                    }
                                    
                                }
                                
                                [self resetHiddenEPGDataAndNoDataLabel:YES];
                                
                                [_channelListTV reloadData];
                                [_EPGListTableView reloadData];
//                                if (_channelListArr.count >_videoPlayPosition) {
//                                    [_EPGListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
//                                    [_channelListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
//                                    
//                                    
//                                }
                                
                                
                                
                            }else
                            {
                                [self NoneEPGDataAndPlayServiceID];
                            }
                            
                            
                        }else
                        {
                            [self NoneEPGDataAndPlayServiceID];
                            
                        }
                        
                        
                    }
                        break;
                    case -1:
                    {
                        //                        [AlertLabel AlertInfoWithText:@"获取数据失败（如：频道不存在，当天无电视节目等）" andWith:_contentView withLocationTag:2];
                        [self resetHiddenEPGDataAndNoDataLabel:NO];
                        
                        return;
                    }
                        break;
                    case 1:
                    {
                        [self resetHiddenEPGDataAndNoDataLabel:NO];
                        //                        [AlertLabel AlertInfoWithText:@"没有新数据" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                    default:
                        break;
                }
                
                
                
                
            }
            
            if ([type isEqualToString:EPG_TVPLAY]) {
                NSInteger ret = [[[result objectForKey:@"playDic"] objectForKey:@"ret"] integerValue];
                NSLog(@"EPG_TVPLAY会看的数据是%@",result);
                [clearTitles removeAllObjects];
                _theVideoInfo.videoLink = @"";
                if (IS_PERMISSION_GROUP) {
                    if (ret!=-3) {
                        [self clearMoviePlayerStop];
                        _playerLoginView.hidden = YES;
                        _isEPGPlayerEnter = NO;
                        _polyLoadingView.hidden = YES;
                        _jxPlayerLoginView.hidden = YES;
                        _jxPlayerBindView.hidden = YES;
                        
                    }
                }
                
                if (_livePlayerTimer) {
                    [_livePlayerTimer invalidate];
                    _livePlayerTimer = nil;
                }
                
                _livePlayerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timeSystemStr:) userInfo:[result objectForKey:@"ret"] repeats:YES];
                
                
                if (ret !=0) {
                    if (USER_ACTION_COLLECTION == YES) {
                        
                        _polyLoadingView.hidden = YES;
                        _isCollectionInfo = NO;
                        
                      
                    }
                    
                    if (_isLocationOrder == YES) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [self TVPLayModelService:_serviceID andCurrentDateStr:_currentDateStr];
                            
                            
                        });
                        
                    }else{
                        //暂时先不return
                        //                        return;
                    }
                    
                    //                    if (_isLocationOrder == YES) {
                    //
                    //                        //                        [self LocalOrderButError];
                    //                        [_moviePlayer play];
                    //
                    //                        _isLocationOrder = NO;
                    //
                    //                        return;
                    //                        //
                    //
                    //                    }
                }
                
                switch (ret ) {
                    case 0:
                    {
                        
                        if ([[[result objectForKey:@"adDic"] objectForKey:@"count"] integerValue]>0) {
                            _adPlayerView.timeLabel.text = @"";
                            
                            if ([[[result objectForKey:@"adDic"] objectForKey:@"adList"] count]>0) {
                                
                                
                                _theVideoInfo.adList = [[result objectForKey:@"adDic"] objectForKey:@"adList"];
                                NSDictionary *ADdic = [NSDictionary dictionaryWithDictionary:[_theVideoInfo.adList objectAtIndex:0]] ;
                                
                                if ([[ADdic objectForKey:@"pictureRes"] allValues]>0) {
                                    _adPlayerView.timeLabel.text = @"";
                                    
                                    [_adPlayerView setMaskViewType:ImageAD];;
                                    _isHaveAD = YES;
                                    _adPlayerView.hidden = NO;
                                    _adPlayerView.rotateBtn.hidden = NO;
                                    
                                }else if ([[ADdic objectForKey:@"videoRes"] allValues]>0)
                                {
                                    _adPlayerView.timeLabel.text = @"";
                                    [_adPlayerView setMaskViewType:VideoAD];
                                    
                                    _adVideoDetailURL = [[[self.theVideoInfo.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"urlLink"];
                                    
                                    self.adVideoURL = [[[self.theVideoInfo.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"videoLink"];
                                    if (self.adVideoURL.length>0) {
                                        NSLog(@"是有广告视频链接的");
                                        _isHaveAD = YES;
                                        _adPlayerView.hidden = NO;
                                        _adPlayerView.rotateBtn.hidden = NO;
                                        
                                    }else
                                    {
                                        _isHaveAD = NO;
                                        _adPlayerView.hidden = YES;
                                        _adPlayerView.rotateBtn.hidden = YES;
                                        NSLog(@"是没有广告视频链接的");
                                    }
                                }
                                else
                                {
                                    _isHaveAD = NO;
                                    _adPlayerView.hidden = YES;
                                    _adPlayerView.rotateBtn.hidden = YES;
                                    
                                    
                                }
                                
                                
                                
                            }else
                            {
                                _isHaveAD = NO;
                                _adPlayerView.hidden = YES;
                                _adPlayerView.rotateBtn.hidden = YES;
                                
                            }
                            
                            
                        }else
                        {
                            _isHaveAD = NO;
                            
                            _adPlayerView.hidden = YES;
                            
                            _adPlayerView.rotateBtn.hidden = YES;
                            
                        }
                        
                        
                        
                        if (_isHaveAD == YES ) {
                            if (_isLiveHasAD == NO) {
                                
                                _isHaveAD = NO;
                                _returnMainBtn.alpha = 1;
                                _returnBtnIV.hidden = NO;
                                _adPlayerView.hidden = YES;
                                _adPlayerView.rotateBtn.hidden = YES;

                            }else
                            {
                            _returnMainBtn.alpha = 0;
                            _returnBtnIV.hidden = YES;
                            }
                            
                        }else{
                            _returnMainBtn.alpha = 1;
                            _returnBtnIV.hidden = NO;
                        }

//                        if (_isHaveAD == YES) {
//                            _returnMainBtn.alpha = 0;
//                            _returnBtnIV.hidden = YES;
//                            
//                        }else{
//                            _returnMainBtn.alpha = 1;
//                            _returnBtnIV.hidden = NO;
//                        }

                        _allCodeRateLink = @"";
//                        _isHaveAD = NO;
//                        _adPlayerView.hidden = YES;
//                        _returnMainBtn.alpha = 1;
//                        _adPlayerView.rotateBtn.hidden = YES;

                        
                        NSLog(@"HHJH***********%@",result);
                        _theVideoInfo.videoLink = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        NSLog(@"timemove str = %@",_theVideoInfo.videoLink);
                        _theVideoInfo.videoID = [[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"] objectForKey:@"id"];
                        _theVideoInfo.channelName = [[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"]objectForKey:@"name"];
                        _theVideoInfo.fullStartTimeP = @"";
                        _theVideoInfo.endTimeP = @"";
                        _theVideoInfo.contentIDP = @"";
//                        _serviceName = [[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"]objectForKey:@"name"];
                        //                    _isNowPlayLiveTV = YES;
                        _currentPlayType = CurrentPlayLive;
                        
                        _currentURL = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle]>0)
                        {
                            _currentTimeL.text = liveStartTime;
                            _totalTimeL.text = liveEndTime;
                            
                        }else
                        {
                            _currentTimeL.text = @"00:00:00";
                            _totalTimeL.text  = @"00:00:00";
                        }
                        
                        if (_isVerticalDirection == YES) {
                            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                        }
                        
                        
                        _currentPlayServiceID = _theVideoInfo.videoID;
                        _serviceID = _theVideoInfo.videoID;
                        //                        [self TVPLayModelService:_serviceID andCurrentDateStr:_currentDateStr];
                        [self playVideoModeWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:PlayTVType];
                        
                        // 先注释掉
                        
                        if (_isLocationOrder == YES) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                [self TVPLayModelService:_serviceID andCurrentDateStr:_currentDateStr];
                                
                                
                            });
                            
                        }else{
                            return;
                        }
                        
                        
                        [_channelListTV reloadData];
                        [_EPGListTableView reloadData];
                        
                        
                        
                    }
                        
                        
                        break;
                    case -1:
                    {
                        
                        [AlertLabel AlertInfoWithText:@"当前直播频道不存在 " andWith:_contentView withLocationTag:2];
                        return;
                    }
                        break;
                    case -2:
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
                        
                        // [AlertLabel AlertInfoWithText:@"当前直播频道用户未登录或者登录超时 " andWith:_contentView withLocationTag:2];
                        //return;
                        
                    }
                        break;
                    case -3:
                    {
                        
                        if (IS_PERMISSION_GROUP) {
                            
                            if (IsLogin) {
                                
                                [self clearMoviePlayerStop];
                                _playerLoginView.hidden = YES;
                                _jxPlayerLoginView.hidden = YES;

                                _isEPGPlayerEnter = NO;
                                _polyLoadingView.hidden = YES;
                                _jxPlayerBindView.hidden = NO;
                                
                                if (IsBinding) {
                                    
                                [AlertLabel AlertInfoWithText:@"该视频暂不能播放" andWith:_contentView withLocationTag:2];
 
                                }else
                                {
                                    _jxPlayerBindView.hidden = NO;
                                }
                                
                            }else
                            {
                                [self clearMoviePlayerStop];
                                _playerLoginView.hidden = NO;
                                _jxPlayerLoginView.hidden = NO;

                                _isEPGPlayerEnter = YES;
                            }
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"直播鉴权失败" andWith:_contentView withLocationTag:2];
                        }
                        
                        return;
                    }
                        break;
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"直播频道对应此终端无播放链接 " andWith:_contentView withLocationTag:2];
                        return;
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                        return;
                    }
                        break;
                        
                        
                    default:
                        break;
                }
                
                
            }
            
          
            
            if ([type isEqualToString:EPG_PAUSETVPLAY])
            {
                NSInteger ret = [[result objectForKey:@"ret"] integerValue];
                [clearTitles removeAllObjects];

                _theVideoInfo.videoPauseLink = @"";
                if (ret !=0) {
                    {
                        [[self.view viewWithTag:4040] setHidden:YES];
                        _isCollectionInfo = NO;
                    }
                }
                
                
                
                if (IS_PERMISSION_GROUP) {
                    if (ret!=-3) {
                        [self clearMoviePlayerStop];
                        _playerLoginView.hidden = YES;
                        _jxPlayerLoginView.hidden = YES;
                        _jxPlayerBindView.hidden = YES;
                        _isEPGPlayerEnter = NO;
                        _polyLoadingView.hidden = YES;
                        
                    }
                }
                
                
                
                switch (ret) {
                    case 0:
                    {
                        _allCodeRateLink = @"";
                        
//                        [AlertLabel AlertInfoWithText:@"即将切换进入时移状态" andWith:_contentView withLocationTag:2];
//                        _videoProgressV.value = 1000;
                        _playButton.hidden = NO;
                        _currentPlayType = CurrentPlayTimeMove;
                        
                        if (_MoveTimeInteger<=0) {
                            _MoveTimeInteger = 10;
                        }
                        
                        JumpStartTime = _MoveTimeInteger;
//                        
                        if ([[UIDevice currentDevice] orientation]== !UIInterfaceOrientationPortrait)
                        {
                            [self showGestureGuidView];
                            
                        }
                        
                        _theVideoInfo.videoPauseLink = [NSString stringWithFormat:@"%@",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        
                        _theVideoInfo.videoID = [[result objectForKey:@"serviceinfo"] objectForKey:@"id"];
                        _theVideoInfo.channelName = [[result objectForKey:@"serviceinfo"]objectForKey:@"name"];
                        
                        _theVideoInfo.fullStartTimeP = @"";
                        _theVideoInfo.endTimeP = @"";
                        _theVideoInfo.contentIDP = @"";
                        //                    _isNowPlayLiveTV = YES;
                        _currentPlayType = CurrentPlayTimeMove;
                        //                _videoProgressV.value = 1000;
                        
                        
                        
//                        _currentURL = [NSString stringWithFormat:@"%@?npt=-%ld-",_theVideoInfo.videoPauseLink,(long)JumpStartTime];
                        _currentURL = [NSString stringWithFormat:@"%@%@",_theVideoInfo.videoPauseLink,[self timeMoveChangeIsJXCable:JXCABLE_SERIAL_NUMBER andJumpTimeInterger:JumpStartTime]];
                        NSLog(@"*********timeMoveBOBo%@",_currentURL);
                        /**
                         *  BOBO修改******************
                         */
                        //                        _currentURL = [NSString stringWithFormat:@"%@",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        _currentTimeL.text = liveStartTime;
                        _totalTimeL.text = liveEndTime;
                        [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL]  andPlayTVType:PlayTVType];
//                        _playingEventID = _theVideoInfo.videoID;
//                        [self StartNewTimer];
                        
                        
                    }
                        break;
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前频道暂无法播放" andWith:_contentView withLocationTag:2];
                            return;
                    }
                        break;
                    case -2:
                    {
                        
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
  
                        
                                return;
                        
                    }
                        break;
                    case -3:
                    {
                        if (IS_PERMISSION_GROUP) {
                            
                            if (IsLogin) {
                                if (IsBinding) {
                                    
                                    [AlertLabel AlertInfoWithText:@"当前频道暂无法播放" andWith:_contentView withLocationTag:2];
 
                                }else
                                {
                                    _jxPlayerBindView.hidden = NO;
                                }
                                
                            }else
                            {
                                [self clearMoviePlayerStop];
                                _playerLoginView.hidden = NO;
                                _jxPlayerLoginView.hidden = NO;

                                _isEPGPlayerEnter = YES;
                            }
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"当前频道暂无法播放" andWith:_contentView withLocationTag:2];
                        }
                        
                        return;
                        
                    }
                        break;
                        
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"当前频道暂无法播放" andWith:_contentView withLocationTag:2];
                      
                        return;
                        
                    }
                        break;
                    case -7:
                    {
                        [AlertLabel AlertInfoWithText:@"当前频道暂无法播放" andWith:_contentView withLocationTag:2];
                        return;
                        
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                        return;
                        
                    }
                        break;
                        
                    default:
                        break;
                }
                [_channelListTV reloadData];
                [_EPGListTableView reloadData];
                
            }
            
            if ([type isEqualToString:EPG_EVENTPLAY]) {
                
                NSInteger ret = [[[result objectForKey:@"playDic"] objectForKey:@"ret"] integerValue];
                
                NSLog(@"EPG_EVENTPLAY会看的数据是%@",result);
                [clearTitles removeAllObjects];
                _isHaveAD = NO;
                _adPlayerView.timeLabel.text = @"";
                
                if (ret !=0 ) {
                    _isCollectionInfo = NO;
                }
                
                if (IS_PERMISSION_GROUP) {
                    if (ret!=-3) {
                        [self clearMoviePlayerStop];
                        _playerLoginView.hidden = YES;
                        _jxPlayerLoginView.hidden = YES;
                        _jxPlayerBindView.hidden = YES;
                        _isEPGPlayerEnter = NO;
                        _polyLoadingView.hidden = YES;
                        
                    }
                }

                
                switch (ret) {
                    case 0:
                    {
                        
                        if ([[[result objectForKey:@"adDic"] objectForKey:@"count"] integerValue]>0) {
                            _adPlayerView.timeLabel.text = @"";
                            
                            if ([[[result objectForKey:@"adDic"] objectForKey:@"adList"] count]>0) {
                                

                            _theVideoInfo.adList = [[result objectForKey:@"adDic"] objectForKey:@"adList"];
                                NSDictionary *ADdic = [NSDictionary dictionaryWithDictionary:[_theVideoInfo.adList objectAtIndex:0]] ;
                                
                                if ([[ADdic objectForKey:@"pictureRes"] allValues]>0) {
                                    _adPlayerView.timeLabel.text = @"";
                                    
                                    [_adPlayerView setMaskViewType:ImageAD];;
                                    _isHaveAD = YES;
                                    _adPlayerView.hidden = NO;
                                    _adPlayerView.rotateBtn.hidden = NO;
                                
                                }else if ([[ADdic objectForKey:@"videoRes"] allValues]>0)
                                {
                                    _adPlayerView.timeLabel.text = @"";
                                    [_adPlayerView setMaskViewType:VideoAD];
                                    
                                    _adVideoDetailURL = [[[self.theVideoInfo.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"urlLink"];
                                    
                                                               self.adVideoURL = [[[self.theVideoInfo.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"videoLink"];
                                    if (self.adVideoURL.length>0) {
                                        NSLog(@"是有广告视频链接的");
                                        _isHaveAD = YES;
                                        _adPlayerView.hidden = NO;
                                        _adPlayerView.rotateBtn.hidden = NO;
                                        
                                    }else
                                    {
                                        _isHaveAD = NO;
                                        _adPlayerView.hidden = YES;
                                        _adPlayerView.rotateBtn.hidden = YES;
                                        NSLog(@"是没有广告视频链接的");
                                    }
                                }
                                else
                                {
                                    _isHaveAD = NO;
                                    _adPlayerView.hidden = YES;
                                    _adPlayerView.rotateBtn.hidden = YES;

                                    
                                }

                             
                                
                        }else
                        {
                            _isHaveAD = NO;
                            _adPlayerView.hidden = YES;
                            _adPlayerView.rotateBtn.hidden = YES;

                        }
                            
                            
                    }else{
                        _isHaveAD = NO;
                        
                        _adPlayerView.hidden = YES;
 
                    _adPlayerView.rotateBtn.hidden = YES;
                    }
                        
                        if (_isHaveAD == YES) {
                            _returnMainBtn.alpha = 0;
                            _returnBtnIV.hidden = YES;
                            
                        }else{
                            _returnMainBtn.alpha = 1;
                            _returnBtnIV.hidden = NO;
                        }
 
                        
                        
                        
                        _allCodeRateLink = @"";
                        //                    NSLog(@"HHJH***********%@",result);
//                        NSString *channelURLStr = [NSString stringWithFormat:@"%@",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                         NSString *channelURLStr = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        
                       
                        _playPauseBigBtn.hidden = YES;
                        _isPaused = NO;
                        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
                        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
                        
                        
                        
                        _theVideoInfo.endTimeP = @"";
                        _theVideoInfo.contentIDP = @"";
                        _currentPlayType = CurrentPlayEvent;
                        //                    JumpStartTime =   [breakpoint integerValue];
                        
                        
                        if ([[UIDevice currentDevice] orientation]== !UIInterfaceOrientationPortrait)
                        {
                            [self showGestureGuidView];
                            
                        }
                        _currentURL = [NSString stringWithFormat:@"%@",[[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                        _theVideoInfo.channelName = [[[result objectForKey:@"playDic"] objectForKey:@"serviceinfo"]objectForKey:@"name"];
                        
                        //
                        
//                        [self clearMoviePlayerStop];
                        _currentPlayType = CurrentPlayEvent;
                        [self playBackChannelVideoModoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",channelURLStr]]  andPlayTVType:PlayTVType];
                        
                        _currentTitleLabel.text = [NSString stringWithFormat:@"正在回看:%@",_theVideoInfo.videoName];
                        
                        _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(_theVideoInfo.videoName)?@"暂无节目":_theVideoInfo.videoName];
                        NSLog(@"=====serviceName==10%@===%@",_serviceName,_currentServiceEventStr);

                        if (_isVerticalDirection == YES) {
                            [self setSmallScreenPlayer];

                            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                            
                        }else
                        {
                            [self setBigScreenPlayer];

                            _serviceTitleLabel.text = _currentServiceEventStr;
                        }

                        //                    _playingEventID = _theVideoInfo.videoID;
                        [_channelListTV reloadData];
                        [_EPGListTableView reloadData];
                    }
                        break;
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前节目不存在或者不提供回看" andWith:_contentView withLocationTag:2];
                        if (IS_PERMISSION_GROUP) {
                            [self clearMoviePlayerStop];
                            _playerLoginView.hidden = YES;
                            _jxPlayerLoginView.hidden = YES;

                            _isEPGPlayerEnter = NO;
                            _polyLoadingView.hidden = YES;
                        }
                        return;
                    }
                        break;
                    case -2:
                    {
                        
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                            
                            if (IS_PERMISSION_GROUP) {
                                [self clearMoviePlayerStop];
                                _playerLoginView.hidden = YES;
                                _jxPlayerLoginView.hidden = YES;

                                _isEPGPlayerEnter = NO;
                                _polyLoadingView.hidden = YES;
                                
                            }
                        }

                        
                        return;
                        
                    }
                        break;
                    case -3:
                    {
                        
                        
                        if (IS_PERMISSION_GROUP) {
                            
                            if (IsLogin) {
                                
                                if (IsBinding) {
                                    
                                    [AlertLabel AlertInfoWithText:@"该视频暂不能播放" andWith:_contentView withLocationTag:2];
  
                                }else
                                {
                                    _jxPlayerBindView.hidden = NO;

                                }
                                
                            }else
                            {
                                [self clearMoviePlayerStop];
                                _playerLoginView.hidden = NO;
                                _jxPlayerLoginView.hidden = NO;

                                _isEPGPlayerEnter = YES;
                                _polyLoadingView.hidden = YES;

                            }
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"回看鉴权失败" andWith:_contentView withLocationTag:2];
                        }
                        
                        
                        
                        
                      //                        if (IS_PERMISSION_GROUP) {
//                            [self clearMoviePlayerStop];
//                            _playerLoginView.hidden = YES;
//                            _polyLoadingView.hidden = YES;
//                            _isEPGPlayerEnter = NO;
//                        }
                        return;
                        
                    }
                        break;
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"回看频道对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                            if (IS_PERMISSION_GROUP) {
                            [self clearMoviePlayerStop];
                            _playerLoginView.hidden = YES;
                                _jxPlayerLoginView.hidden = YES;

                            _isEPGPlayerEnter = NO;
                            _polyLoadingView.hidden = YES;

                        }
                        return;
                    }
                        break;
                    case -6:
                    {
                        [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:_contentView withLocationTag:2];
                            if (IS_PERMISSION_GROUP) {
                            [self clearMoviePlayerStop];
                            _playerLoginView.hidden = YES;
                                _jxPlayerLoginView.hidden = YES;

                            _isEPGPlayerEnter = NO;
                            _polyLoadingView.hidden = YES;

                        }
                        return;
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试 " andWith:_contentView withLocationTag:2];
                        if (IS_PERMISSION_GROUP) {
                            [self clearMoviePlayerStop];
                            _playerLoginView.hidden = YES;
                            _jxPlayerLoginView.hidden = YES;

                            _isEPGPlayerEnter = NO;
                            _polyLoadingView.hidden = YES;

                        }
                        return;
                    }
                        break;
                        
                    default:
                        break;
                }
                
               
                
            }
            
            
            
            if ([type isEqualToString:EPG_CURRENTEVENTLIST]) {
                
                NSInteger ret = [[result objectForKey:@"ret"] integerValue];
                
                
                if (ret == 0) {
                    
                    NSMutableArray *multableArr = [[NSMutableArray alloc]init];
                    
                    multableArr = [self currentEventListMutableArr:result];
                    
                    if (multableArr.count>0) {
                        _currentLiveVideoTitleStr = [[multableArr objectAtIndex:0] valueForKey:@"eventName"];
                         _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,isEmptyStringOrNilOrNull(_currentLiveVideoTitleStr)?@"暂无节目":_currentLiveVideoTitleStr];
                        NSLog(@"=====serviceName==11%@===%@",_serviceName,_currentServiceEventStr);

                        _currentTitleLabel.text = _currentLiveVideoTitleStr;
                        if (_isVerticalDirection == YES) {
                            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                            
                        }else
                        {
                            _serviceTitleLabel.text = _currentServiceEventStr;
                        }

                        //zzs
                        if ([multableArr count]>1) {
                            _nextLiveVideoTitleStr = [[multableArr objectAtIndex:1] valueForKey:@"eventName"];
                        }else{
                            _nextLiveVideoTitleStr = @"";
                        }
                        
//                        _currntPlayLabel.text = [NSString stringWithFormat:@"正在播放：%@",_currentLiveVideoTitleStr];
//                        _nextPlayLabel.text = [NSString stringWithFormat:@"下一节目：%@",_nextLiveVideoTitleStr];
                        
                        
                        
                        
                    }else
                    {
                        _currentLiveVideoTitleStr = @"暂无节目名称";
                        _nextLiveVideoTitleStr = @"暂无节目名称";
                        _currentTitleLabel.text = _currentLiveVideoTitleStr;
                         _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,_currentLiveVideoTitleStr];
                        NSLog(@"=====serviceName==12%@===%@",_serviceName,_currentServiceEventStr);

                        if (_isVerticalDirection == YES) {
                            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                            
                        }else
                        {
                            _serviceTitleLabel.text = _currentServiceEventStr;
                        }

//                        _currntPlayLabel.text = [NSString stringWithFormat:@"正在播放：%@",_currentLiveVideoTitleStr];
//                        _nextPlayLabel.text = [NSString stringWithFormat:@"下一节目：%@",_nextLiveVideoTitleStr];
                        
                        return;
                    }
                    
                    
                    
                }
                
                else if (ret == -1)
                {
                    if (_currentPlayType == CurrentPlayLive) {
                        
                        _currentServiceEventStr =[NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,@"暂无节目"];
                        
                        NSLog(@"=====serviceName==13%@===%@",_serviceName,_currentServiceEventStr);

                        if (NotEmptyStringAndNilAndNull(_currentServiceEventStr)) {
                            
                            if (_isVerticalDirection == YES) {
                                _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                                
                            }else
                            {
                                _serviceTitleLabel.text = _currentServiceEventStr;
                            }
                        }else
                        {
                            
                        }
                    }else
                    {
                        if (isEmptyStringOrNilOrNull(_currentLiveVideoTitleStr)) {
                            _currentLiveVideoTitleStr = @"暂无节目名称";
                            _nextLiveVideoTitleStr = @"暂无节目名称";
                            _currentTitleLabel.text = _currentLiveVideoTitleStr;
                            _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,_currentLiveVideoTitleStr];
                            NSLog(@"=====serviceName==14%@===%@",_serviceName,_currentServiceEventStr);

                            if (_isVerticalDirection == YES) {
                                _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName];
                                
                            }else
                            {
                                _serviceTitleLabel.text = _currentServiceEventStr;
                            }
                        }
                            
                        }
                        
                        
                    
//                    _currntPlayLabel.text = [NSString stringWithFormat:@"正在播放：%@",_currentLiveVideoTitleStr];
//                    _nextPlayLabel.text = [NSString stringWithFormat:@"下一节目：%@",_nextLiveVideoTitleStr];
                    

                    //                    return;
                }else
                {
                    return;
                }
                
            }
            
            
            
            
            if ([type isEqualToString:EPG_ADDTVFAVORATE]) {
                
                int ret = [[result objectForKey:@"ret"] intValue];
                if (ret == 0) {
                    
                    if (IsLogin) {
                        [AlertLabel AlertInfoWithText:@"成功添加喜爱频道" andWith:self.view withLocationTag:1];
                        
//                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//                        [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
                        [self KFavorateBtnisFaved:YES];
                        
                        _favourButton.selected = YES;
                        
                        //添加收藏
                        _localVideoModel.lvID = _serviceID;
                        _localVideoModel.lvServiceName = _serviceName;
                        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                        _localVideoModel.lvEventName = _eventName;
                        
//                        [LocalCollectAndPlayRecorder addLiveFavoriteWithSeq:@"" andID:_serviceID andName:_serviceName andIntro:@"" andImageLink:_theVideoInfo.channelImage andLevelImageLink:@"" andUserType:IsLogin andKey:@"serviceID"];
                        
//                        [LocalCollectAndPlayRecorder addLiveFavoriteWithModel:_localVideoModel andUserType:IsLogin];
                        [_localListCenter addLiveFavoriteWithModel:_localVideoModel andUserType:IsLogin];
                        [_localVideoModel resetLocalVideoModel];
                        
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];
                    }else
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];
                }
                
                else if (ret == 1){
                    if (IsLogin) {
                        HTRequest *htrequest = [[HTRequest alloc]initWithDelegate:self];
                        [htrequest DelTVFavorateWithServiceID:_serviceID andTimeout:kTIME_TEN];

                    }else
                    {
                        
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }

                    }
                    
                    
                }else
                {
                    
                    if (IsLogin) {
                        [AlertLabel AlertInfoWithText:@"添加收藏失败 请重试" andWith:self.view withLocationTag:1];

                    }else
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
                        
                    }
                }
                
                
                
            }
            
            
            if ([type isEqualToString:EPG_DELTVFAVORATE])
            {
                int ret = [[result objectForKey:@"ret"] intValue];
                if (ret == 0) {
                    
                    if (IsLogin) {
                        [AlertLabel AlertInfoWithText:@"成功移除喜爱频道" andWith:self.view withLocationTag:1];
                        
//                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//                        [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
                        
                        [self KFavorateBtnisFaved:NO];
                        
                        _favourButton.selected = NO;
                        
                        //取消收藏
                        _localVideoModel.lvID = _serviceID;
                        _localVideoModel.lvServiceName = _serviceName;
                        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                        
//                        [LocalCollectAndPlayRecorder singleDeleteLiveFavoriteWithDeleteID:_serviceID andUserType:IsLogin andKey:@"serviceID"];
                        [_localListCenter singleDeleteLiveFavoriteWithDeleteID:_serviceID andUserType:IsLogin andKey:@"id"];

                        [_localVideoModel resetLocalVideoModel];
                        
                        
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];
                    }else
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                            
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
                    }
                    
                }
                else if (ret == 1){
                    if (IsLogin) {
                        HTRequest *htrequest = [[HTRequest alloc]initWithDelegate:self];
                        [htrequest AddTVFavorateWithServiceID:self.serviceID andTimeout:kTIME_TEN];
   
                    }else
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        }
                    }
                
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"收藏节目失败，请重试" andWith:self.view withLocationTag:1];
                    
                }
            }
            
            if ([type isEqualToString:EPG_EVENTSTOP]) {
                
                int ret = [[result objectForKey:@"ret"] intValue];
                
                switch (ret) {
                    case 0:
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_RE_WATCH_LIST object:nil];
                        
                    }
                        break;
                    case -1:
                    {
                        //                        [AlertLabel AlertInfoWithText:@"当前节目不存在" andWith:self.view withLocationTag:1];
                        
                    }
                        break;
                    case -2:
                    {
                        //                        [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
                        
                    }
                        break;
                    case -9:
                    {
                        //                        [AlertLabel AlertInfoWithText:@"失败（连接个人中心失败，或者个人中心返回失败，或者断点位置超出节目时长范围）" andWith:self.view withLocationTag:1];
                        
                    }
                        break;
                        
                    default:
                        break;
                }
                
            }
            
            
            if ([type isEqual:EPG_DELEVENTRESERVE]) {
                NSInteger ret = [[result objectForKey:@"ret"] intValue];
                
                switch (ret) {
                    case 0://0,1成功，-2未登录，-9失败
                    {
                        for (int i = 0; i<self.netScheduledListArr.count; i++) {
                            NSDictionary *dic = [self.netScheduledListArr objectAtIndex:i];
                            if ([[dic objectForKey:@"eventID"] isEqualToString:_SelectdscheduleD.selectedEventID]) {
                                [self.netScheduledListArr removeObjectAtIndex:i];
                                
                                
                                break;
                            }
                        }
                        
                       //本地的预约
                        _localVideoModel.lvID = _serviceID;
                        _localVideoModel.lvServiceName = _serviceName;
                        _localVideoModel.lvEventID = orderID;
                        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                        _localVideoModel.lvStartTime = orderStartTime;
                        _localVideoModel.lvEndTime = orderEndTime;
                        _localVideoModel.lvEventName = orderEventName;
                        _localVideoModel.lvFullStartTime = orderFullStartTime;
                        
//                        [LocalCollectAndPlayRecorder singleDeletScheduledListWith:orderID andKey:@"eventID"];
                        [_localListCenter singleDeletScheduledListWith:orderID andKey:@"eventID"];

                        [_localVideoModel resetLocalVideoModel];
                        
                        [AlertLabel AlertInfoWithText:@"已取消预约" andWith:_contentView withLocationTag:2];
                        [self.channelListTV reloadData];
                        [_EPGListTableView reloadData];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                    }
                        break;
                    case 1:
                    {
                        for (int i = 0; i<self.netScheduledListArr.count; i++) {
                            NSDictionary *dic = [self.netScheduledListArr objectAtIndex:i];
                            if ([[dic objectForKey:@"eventID"] isEqualToString:_SelectdscheduleD.selectedEventID]) {
                                [self.netScheduledListArr removeObjectAtIndex:i];
                                break;
                            }
                        }
                        
                        //本地的预约
                        _localVideoModel.lvID = _serviceID;
                        _localVideoModel.lvServiceName = _serviceName;
                        _localVideoModel.lvEventID = orderID;
                        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                        _localVideoModel.lvStartTime = orderStartTime;
                        _localVideoModel.lvEndTime = orderEndTime;
                        _localVideoModel.lvEventName = orderEventName;
                        _localVideoModel.lvFullStartTime = orderFullStartTime;
                        
//                        [LocalCollectAndPlayRecorder singleDeletScheduledListWith:orderID andKey:@"eventID"];
                        [_localListCenter singleDeletScheduledListWith:orderID andKey:@"eventID"];

                        [_localVideoModel resetLocalVideoModel];

                        
                        [AlertLabel AlertInfoWithText:@"已取消预约" andWith:_contentView withLocationTag:2];
                        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                        
                    }
                        break;
                    case -2:
                    {
                        CustomAlertView *alert = [[CustomAlertView alloc] initWithMessage:@"本功能需要登录后才可使用" andButtonNum:2 withDelegate:self];
                        alert.tag = -2;
                        [alert show];
                    }
                        break;
                    case -9:
                        //失败
                        
                        
                        [AlertLabel AlertInfoWithText:@"取消预约失败 请重试！" andWith:_contentView withLocationTag:2];
                        break;
                        
                    default:
                        break;
                }
                
                
                [self.channelListTV reloadData];
                [_EPGListTableView reloadData];
                
            }
            
            
            
            
            if ([type isEqualToString:EPG_ADDEVENTRESERVE])
            {//添加预约
                NSInteger ret = [[result objectForKey:@"ret"] intValue];
                
                
                switch (ret) {
                    case 0://0,1成功，-2未登录，-9失败
                    {
                        
                        //                         [request EventRevserveWithTimeou:10];
                        //                        [[UserScheduleCenter defaultCenter] postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                        //
                        
                        //本地的预约
                        _localVideoModel.lvID = _serviceID;
                        _localVideoModel.lvServiceName = _serviceName;
                        _localVideoModel.lvEventID = orderID;
                        _localVideoModel.lvChannelImage = _theVideoInfo.channelImage;
                        _localVideoModel.lvStartTime = orderStartTime;
                        _localVideoModel.lvEndTime = orderEndTime;
                        _localVideoModel.lvEventName = orderEventName;
                        _localVideoModel.lvFullStartTime = orderFullStartTime;
//                        [LocalCollectAndPlayRecorder addScheduledListWith:_serviceID andEventName:_serviceName andEventID:orderID andEventName:orderEventName andStartTime:orderStartTime andEndTime:orderEndTime andImageLink:_theVideoInfo.channelImage andLevelImageLink:@"" andKey:@"id"];
//                        [LocalCollectAndPlayRecorder addScheduledListWithModel:_localVideoModel];
                        [_localListCenter addScheduledListWithModel:_localVideoModel];

                        [_localVideoModel resetLocalVideoModel];
                        
                        
                        
                        [AlertLabel AlertInfoWithText:@"预约成功！" andWith:_contentView withLocationTag:2];
                        [self.channelListTV reloadData];
                        [_EPGListTableView reloadData];
//                        [[NSNotificationCenter defaultCenter]postNotificationName:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
                        
                        
                        
                    }
                        break;
                        
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"预约失败 请重试！" andWith:_contentView withLocationTag:2];
                    }
                        
                    default:
                        break;
                        
                        
                }
                
                [_channelListTV reloadData];
                [_EPGListTableView reloadData];
                
            }
            
            
//            if ([type isEqualToString:EPG_EVENTRESERVELIST])
//            {
//                NSInteger ret = [[result objectForKey:@"ret"] intValue];
//                if (ret == 0) {
//                    [self.netScheduledListArr removeAllObjects];
//                    self.netScheduledListArr = [result objectForKey:@"eventlist"];
//                    
//                    [_EPGListTableView reloadData];
//                    [_channelListTV reloadData];
//                }
//            }
            
            
            
            
            
            //***********************拖拽过来的TV数据******************************
            
            if ([type isEqualToString:EPG_TVPLAYEX]) {
                int ret = [[result objectForKey:@"ret"] intValue];
                switch (ret) {
                        
                        
                    case 0:
                    {
                        _allCodeRateLink = @"";
                        _currentPlayType = CurrentPlayLive;
                        //                        _isNowPlayLiveTV = YES;
                        _currentURL = [[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"];
                        _theVideoInfo.fullStartTimeP = @"";
                        _theVideoInfo.endTimeP = @"";
                        _theVideoInfo.contentIDP = @"";
                        
                        [self playVideoModeWithURL:[NSURL URLWithString:[[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                        self.serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[[result valueForKey:@"serviceinfo"] valueForKey:@"tvName"]];
                    }
                        break;
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前频道不存在" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -2:
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                          
                        }

                        
                        
                      //  [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -3:
                    {
                        [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -8:
                    {
                        [AlertLabel AlertInfoWithText:@"鉴权成功待付费" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
                
                
                
            }else if ([type isEqualToString:EPG_EVENTPLAYEX])
            {
                int ret = [[result objectForKey:@"ret"] intValue];
                
                switch (ret) {
                    case 0:
                    {
                        _allCodeRateLink = @"";
                        _theVideoInfo.contentIDP = @"";
                        _currentPlayType = CurrentPlayEvent;
                        //BOBO清理记录直播和时移的布尔值
                        //                        _isNowPlayLiveTV = NO;
                        //                        _isTimeMove = NO;
                        
                        //end
                        //                        _currentURL =  [[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"];
                        [self playBackChannelVideoModoWithURL:[NSURL URLWithString:[[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                        self.serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[[result valueForKey:@"serviceinfo"] valueForKey:@"tvName"]];
                        
                    }
                        break;
                        
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前拉屏频道不存在" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -2:
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                            
                          
                        }

                        //[AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -3:
                    {
                        [AlertLabel AlertInfoWithText:@"拉屏节目鉴权失败" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"拉屏频道对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -8:
                    {
                        [AlertLabel AlertInfoWithText:@"鉴权成功待付费" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
                
                
                
            }
            else if ([type isEqualToString:EPG_PAUSETVPLAYEX])
            {
                NSInteger ret = [[result objectForKey:@"ret"] integerValue];
                
                if (ret !=0) {
                    {
                        [[self.view viewWithTag:4040] setHidden:YES];
                        
                    }
                }
                switch (ret) {
                    case 0:
                    {
                        _allCodeRateLink = @"";
                        
                        //                        [AlertLabel AlertInfoWithText:@"即将切换进入时移状态" andWith:_contentView withLocationTag:2];
                        _videoProgressV.value = 1000;
                        _playButton.hidden = NO;
                        _currentPlayType = CurrentPlayTimeMove;
                        [self showGestureGuidView];
                        _theVideoInfo.videoID = [[result objectForKey:@"serviceinfo"] objectForKey:@"id"];
                        _theVideoInfo.channelName = [[result objectForKey:@"serviceinfo"]objectForKey:@"name"];
                        
                        _theVideoInfo.fullStartTimeP = @"";
                        _theVideoInfo.endTimeP = @"";
                        _theVideoInfo.contentIDP = @"";
                        //                    _isNowPlayLiveTV = YES;
                        _currentPlayType = CurrentPlayTimeMove;
                        //                _videoProgressV.value = 1000;
                        
                        _currentURL = [NSString stringWithFormat:@"%@?npt=-%ld-",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"],(long)JumpStartTime];
                        _currentTimeL.text = liveStartTime;
                        _totalTimeL.text = liveEndTime;
                        
                        [self PauseTVPlayModeWithURL:[NSURL URLWithString:_currentURL]  andPlayTVType:PlayTVType];
                        _playingEventID = _theVideoInfo.videoID;
                        [self StartNewTimer];
                        
                        
                    }
                        break;
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前拉屏时移频道不存在" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -2:
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                            
                            }

                       // [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -3:
                    {
                        [AlertLabel AlertInfoWithText:@"当前拉屏时移频道鉴权失败" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -7:
                    {
                        [AlertLabel AlertInfoWithText:@"该频道不提供时移功能" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                    default:
                        break;
                }
                
            }
            else if ([type isEqualToString:VOD_PLAY])
            {
                int ret = [[result objectForKey:@"ret"] intValue];
                
                switch (ret) {
                    case 0:
                    {
                        _allCodeRateLink = @"";
                        
                        _theVideoInfo.fullStartTimeP = @"";
                        _theVideoInfo.endTimeP = @"";
                        
                        _currentPlayType = CurrentPlayVod;
                        //BOBO清理修改直播和时移的布尔值
                        //end
                        if (_isVODEX == YES) {
                            
                            _isVODEX = NO;
                        }
                        [self playBackChannelVideoModoWithURL:[NSURL URLWithString:[result valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                        
                    }
                        break;
                    case -1:
                    {
                        [AlertLabel AlertInfoWithText:@"当前拉屏点播频道不存在" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -2:
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;

                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;

                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                 [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                            
                            }

                       // [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -3:
                    {
                        [AlertLabel AlertInfoWithText:@"拉屏点播鉴权失败" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -5:
                    {
                        [AlertLabel AlertInfoWithText:@"拉屏节目对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -8:
                    {
                        [AlertLabel AlertInfoWithText:@"鉴权成功待付费" andWith:_contentView withLocationTag:2];
                    }
                        break;
                    case -9:
                    {
                        [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
                
                
                
            }
            
        }
            break;
            
            
            
        default:
            break;
    }
    
    
    
    
    
}



-(NSString *)timeMoveChangeIsJXCable:(NSString *)JXCableTimeMove andJumpTimeInterger:(NSInteger)jumpTime
{
    NSString *timeMoveStr = @"";
    
    if ([APP_ID isEqualToString:JXCableTimeMove]) {
        
        timeMoveStr = [NSString stringWithFormat:@"&playseek=%ld-",(long)[[NSDate dateWithTimeInterval:-jumpTime sinceDate:[SystermTimeControl GetSystemTimeWith]] timeIntervalSince1970]];
        
        ;
        
    }else
    {
        timeMoveStr = [NSString stringWithFormat:@"?npt=-%ld-",jumpTime];
        
    }
    
    
    
    
    return timeMoveStr;
    
    
}


-(void)TVPLayModelService:(NSString*)serviceID andCurrentDateStr:(NSString *)currentDateStr
{
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    [request EPGSHOWWithServiceID:serviceID andDate:currentDateStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
    //
    [request EPGPFWithServiceID:serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    
   
}

-(void)LocalOrderButError
{
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    
    //还保持之前的
    [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    if (_currentPlayType == CurrentPlayEvent) {
        
//        [request EventPlayWithEventID:_playingEventID andTimeout:kTIME_TEN];
        
        
        if (ISIntall) {
            _installMaskView.hidden = NO;
            
        }else{
        [request EPGPlayWithServiceID:_serviceID andServiceName:_serviceName andEventID:_playingEventID andTimeout:KTimeout];
        }
        
       
        for (NSInteger i = 0; i<_channelListArr.count; i++) {
            
            if ([[[_channelListArr objectAtIndex:i] objectForKey:@"id"] isEqualToString:_playingEventID]) {
                
                NSDictionary *eventDic = [[NSDictionary alloc]init];
                
                eventDic = [_channelListArr objectAtIndex:i];
                //                                        _playingEventID = [eventDic objectForKey:@"id"];
                _videoPlayPosition = i;
                _channelSelectIndex = _videoPlayPosition;
                //                                        _theVideoInfo.videoName = [NSString stringWithFormat:@"%@",[eventDic objectForKey:@"eventName"]];
                
                
                break;
                
            }
            
            
        }
        
        
        
        
        
        
    }
    if (_currentPlayType == CurrentPlayLive) {
        if (ISIntall) {
            _installMaskView.hidden = NO;
        }else
        {
//        [request TVPlayWithServiceID:_serviceID andTimeout:kTIME_TEN];
            [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

        }
        for (int i = 0; i<_channelListArr.count; i++) {
            NSDictionary *dic = [[NSDictionary alloc]init];
            dic= [_channelListArr objectAtIndex:i];
            NSInteger mark=[self isPlayingWith:[dic objectForKey:@"startTime"] endTime:[dic objectForKey:@"endTime"]];
            
            if (mark == 0) {
                //                _playingEventID = [dic objectForKey:@"id"];
                NSMutableDictionary *dicChannel = [[NSMutableDictionary alloc]init];
                dicChannel = [_channelListArr objectAtIndex:i];
                //                _currentPlayID = [dicChannel objectForKey:@"id"];
                _videoPlayPosition = i;
                _channelSelectIndex = _videoPlayPosition;
                //                _currentLiveVideoTitleStr = [NSString stringWithFormat:@"%@",[dicChannel objectForKey:@"eventName"]];
                //                liveEndTime = [NSString stringWithFormat:@"%@:00",[dicChannel objectForKey:@"endTime"]] ;
                //                liveStartTime = [NSString stringWithFormat:@"%@:00",[dicChannel objectForKey:@"startTime"]];
                
                
                break;
                
            }
            
            
        }
        
        
        
    }
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EPGTableVIewAndChannelTableView) name:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil];
    
    [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
        
        [_EPGListTableView reloadData];
        [_channelListTV reloadData];
    }];
}




#pragma mark --
#pragma mark  取分类数组


-(NSMutableArray *)EpgGetSingleCategory:(NSMutableArray *)categoryArr andType:(NSString*)type
{
    NSMutableArray *lastArr = [[NSMutableArray alloc]init];
    
    
    for (int i = 0; i<categoryArr.count; i++) {
        
        if ([type isEqualToString:[[categoryArr objectAtIndex:i] valueForKey:@"type"]]) {
            
            for (int j = 0; j<[[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"] count]; j++) {
                
                NSMutableDictionary *lastDic = [[NSMutableDictionary alloc]init];
                
                NSDictionary *dic = [[NSDictionary alloc]init];
                dic =  [[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"] objectAtIndex:j];
                
                [lastDic setValue:[dic valueForKey:@"name"] forKey:@"categoryServiceName"];
                [lastDic setValue:[dic valueForKey:@"id"] forKey:@"categoryServiceID"];
                [lastDic setValue:[dic valueForKey:@"imageLink"] forKey:@"categoryLevelImage"];
                [lastDic setValue:[[[[dic valueForKey:@"epglist"] valueForKey:@"epginfo"] objectAtIndex:0] valueForKey:@"eventName"] forKey:@"categoryServiceEventName"];
                
                
                [lastArr addObject:lastDic];
                
            }
            
            
        }
        
        
    }
    
    if (lastArr.count>0) {
        
        [[self.view viewWithTag:8080]setHidden:YES];
    }else
    {
//        [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?YES:NO];
//        [[self.view viewWithTag:8080]setHidden:NO];
        
        if (_isVerticalDirection == NO) {
            [[self.view viewWithTag:8080]setHidden:NO];
            
        }else
        {
            [[self.view viewWithTag:8080]setHidden:YES];
        }

    }
    
    return lastArr;
    
}









-(NSMutableArray *)EPGGetAllCategory:(NSMutableArray *)categoryArr
{
    
    NSMutableArray *lastArr = [[NSMutableArray alloc]init];
    for (int i = 0; i<categoryArr.count; i++) {
        
        NSMutableArray *itemsMArr = [[NSMutableArray alloc] initWithArray:[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"]];
        
        for (int j = 0; j< [itemsMArr count]; j++) {
            
            NSMutableDictionary *lastDic = [[NSMutableDictionary alloc]init];
            NSDictionary *dic = [[NSDictionary alloc]init];
            
            dic =  [[[categoryArr objectAtIndex:i] valueForKey:@"serviceinfo"] objectAtIndex:j];
            
            [lastDic setValue:[dic valueForKey:@"name"] forKey:@"categoryServiceName"];
            [lastDic setValue:[dic valueForKey:@"id"] forKey:@"categoryServiceID"];
            [lastDic setValue:[dic valueForKey:@"imageLink"] forKey:@"categoryLevelImage"];
            [lastDic setValue:[[[[dic valueForKey:@"epglist"] valueForKey:@"epginfo"] objectAtIndex:0] valueForKey:@"eventName"] forKey:@"categoryServiceEventName"];
            
            [lastArr addObject:lastDic];
            
        }
        
        
    }
    
    
    
    if (lastArr.count>0) {
        
        [[self.view viewWithTag:8080]setHidden:YES];
    }else
    {
//        [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?YES:NO];
//        [[self.view viewWithTag:8080]setHidden:NO];
        
        if (_isVerticalDirection == NO) {
            [[self.view viewWithTag:8080]setHidden:NO];
            
        }else
        {
            [[self.view viewWithTag:8080]setHidden:YES];
        }

    }
    
    
    NSLog(@"lastArr*****%@",lastArr);
    
    
    
    
    
    return lastArr;
    
}






#pragma mark --
#pragma mark  自动旋转
- (BOOL)shouldAutorotate {
    
    
    
    BOOL canRotation = [[NSUserDefaults standardUserDefaults]boolForKey:@"CAN_ROTATION"];
    
    
    //    return _isTapbtn_play&&!_isScreenLocked&&canRotation&&YES;
    
    
    
    return  (_isTapbtn_play&&!_isScreenLocked&&canRotation)?YES:NO;
}
- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskAllButUpsideDown;
}





- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    
    // 如果当前处于横屏显示状态
    if(UIInterfaceOrientationIsLandscape(interfaceOrientation))
    {
        //        if (isAfterIOS7) {
        //            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        //
        //        }
        _isVerticalDirection = NO;
        if (_isHaveAD == NO) {
            [self showGestureGuidView];
        }
        [self hideNaviBar:YES];
        _isClickedClear = NO;
        [self setBigScreenPlayer];
        [[UIApplication sharedApplication]setStatusBarHidden:YES];
//        [[UIApplication sharedApplication]setStatusBarHidden:YES];//隐藏状态条
        _serviceTitleLabel.text = _currentServiceEventStr;
        
    }else
    {
        
        _isVerticalDirection = YES;
        [self hideNaviBar:YES];
        [self hiddenGestureGuid];//zzsc
        //        if (isAfterIOS7) {
        //            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        //
        //        }
//        if (!IsIOS6) {
//            
//            [[UIApplication sharedApplication]setStatusBarHidden:NO];//显示状态条
//        }else{
//            
//            [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationUnknown;
//            
//        }
        [dateView DateBtnSelectedAtDate:_currentDateStr];
        _serviceTitleLabel.text =isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName;
        [self setSmallScreenPlayer];
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
        
    }
    
    
    
}



//zzsc
-(void)showGestureGuidView{
    
    if (_currentPlayType == CurrentPlayLive)
    {
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLive"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLive"];
            NSLog(@"第一次启动");
            _gestureGuidView = [[UIImageView alloc] init];
            _gestureGuidView.image = [UIImage imageNamed:@"guide_player_live_guesture"];//直播左划 进入时移功能图片
            _gestureGuidView.hidden = NO;
            _gestureGuidView.userInteractionEnabled = YES;
            [_contentView addSubview:_gestureGuidView];
            
            MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(hiddenGestureGuid)];
            tap.numberOfTapsRequired = 1;
            [_gestureGuidView addGestureRecognizer:tap];
            
            WS(wself);
            [_gestureGuidView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(wself.view);
            }];
            
        }
    }
    if (_currentPlayType == CurrentPlayEvent)
    {
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstPlaybackLive"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstPlaybackLive"];
            NSLog(@"第一次启动");
            _gestureGuidView = [[UIImageView alloc] init];
            _gestureGuidView.image = [UIImage imageNamed:@"guide_player_other_guesture"];
            _gestureGuidView.hidden = NO;
            _gestureGuidView.userInteractionEnabled = YES;
            [_contentView addSubview:_gestureGuidView];
            
            MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(hiddenGestureGuid)];
            tap.numberOfTapsRequired = 1;
            [_gestureGuidView addGestureRecognizer:tap];
            
            WS(wself);
            [_gestureGuidView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(wself.view);
            }];
            
        }
        
    }
    if (_currentPlayType == CurrentPlayTimeMove)
    {
        
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstPlayMoveTime"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstPlayMoveTime"];
            NSLog(@"第一次启动");
            _gestureGuidView = [[UIImageView alloc] init];
            _gestureGuidView.image = [UIImage imageNamed:@"guide_player_other_guesture"];
            _gestureGuidView.hidden = NO;
            _gestureGuidView.userInteractionEnabled = YES;
            [_contentView addSubview:_gestureGuidView];
            
            MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(hiddenGestureGuid)];
            tap.numberOfTapsRequired = 1;
            [_gestureGuidView addGestureRecognizer:tap];
            
            WS(wself);
            [_gestureGuidView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(wself.view);
            }];
            
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}
-(void)hiddenGestureGuid{
    _gestureGuidView.hidden = YES;
    [_gestureGuidView removeFromSuperview];
    
}
//进入到小屏播放器

-(void)setSmallScreenPlayer
{
    [self reStartHiddenTimer];
    [[self.view viewWithTag:8080]setHidden:YES];
    _currentTitleLabel.hidden = YES;
    _ADViewPauseBase.hidden = YES;
    _ADViewPauseBase.adplayerMenuType = -1;
    _ADViewBase.hidden = YES;
    if (_isHaveAD == YES) {
        _returnMainBtn.alpha = 0;
        _adPlayerView.rotateBtn.hidden = NO;
        _topBarIV.hidden = YES;
        _bottomBarIV.hidden = YES;
        _fullWindowbutton.hidden = YES;
        _returnBtnIV.hidden = YES;
    }else{
        _returnMainBtn.alpha = 1;
        _adPlayerView.rotateBtn.hidden = YES;
        _topBarIV.hidden = NO;
        _bottomBarIV.hidden = NO;
        _fullWindowbutton.hidden = NO;
        _returnBtnIV.hidden = NO;

    }
    _adPlayerView.backBtn.tag = 0;
    [_adPlayerView.rotateBtn setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal"] forState:UIControlStateNormal];
    _adPlayerView.rotateBtn.tag = 0;
    _videoProgressV.hidden = YES;
    _progressTimeLable_top.alpha = 0;
    
    _moreBackView.hidden = YES;
    _screenLockedIV.hidden = YES;
    _screenLockedIV.alpha = 0;
    _shareView.hidden = YES;
    _dragLabel.hidden = YES;
//    boNavi.hidden = NO;
//    _topBarIV.hidden = NO;
    
//    if (_isHaveAD == NO) {
//       _topBarIV.hidden = NO;
//        _fullWindowbutton.hidden = NO;
//    }else
//    {
//        _topBarIV.hidden = YES;
//        _fullWindowbutton.hidden = YES;
//    }
    
    remote.hidden = YES;
    _playButton.hidden = YES;
    _returnBtn.hidden = YES;
//    _returnBtnIV.hidden = NO;
    _returnMainBtn.hidden = NO;
    _returnMainBtn.tag = RETURNSMALL;
    [_returnMainBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    _bottomBarIV.hidden = NO;
    _videoProgressV.hidden = YES;
//    _timeButton.hidden = YES;
    _shareButton.hidden = YES;
    _totalTimeL.hidden = YES;
    _currentTimeL.hidden = YES;
    _interactButton.hidden = YES;
//    _currntPlayLabel.hidden = YES;
//    _nextPlayLabel.hidden = YES;
//    _currentPlayBackLabel.hidden = YES;
    _playButton.hidden = YES;
    _channelButton.hidden = YES;
    
    _videoListButton.hidden = YES;
    _videoListView.hidden = YES;
    _channelListView.hidden = YES;
//    _ADViewBase.hidden = YES;//cap
    _selectClearIV.hidden = YES;
    _interactView.hidden = YES;
    
//    _shareButton.hidden = YES;
//    _shareRightLabel.hidden = YES;
    _favourButton.hidden = YES;
//    _favourLabel.hidden = YES;
//    _IPTableView.hidden = YES;
    _IPBackIV.hidden = YES;

    
    _TVButton.hidden = YES;
    _MTButton.hidden = YES;
    _RemoteButton.hidden = YES;
    _clearButton.hidden = YES;
//    [_bottomBarIV setBackgroundColor:[UIColor clearColor]];
//    [_bottomBarIV setImage:nil];
    _fullWindowbutton.tag = 0;
    [_fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal.png"] forState:UIControlStateNormal];
    [_fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_pressed"] forState:UIControlStateHighlighted];
    _fullWindowbutton.backgroundColor = [UIColor clearColor];
    [_fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    [_topBarIV setImage:[UIImage imageNamed:@"bg_player_playbill_listview_epg"]];

//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    WS(wself);
    //背景视图尺寸****************
    
    [_backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.height.mas_equalTo(194*kRateSize);
        make.width.mas_equalTo(wself.view.mas_width);
        
    }];
    
    
    //播放器界面图视图尺寸****************
    
    
    [_moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.backgroundView);
        //        make.centerY.mas_equalTo(wself.backgroundView);
        make.top.mas_equalTo(wself.backgroundView.mas_top);
        make.left.mas_equalTo(wself.backgroundView.mas_left);
        make.height.mas_equalTo(wself.backgroundView.mas_height);
        make.width.mas_equalTo(wself.backgroundView.mas_width);
        
        
    }];
    
    
    //内容图，放topBar bottomBar视图尺寸****************
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.moviePlayer.view);
        //        make.centerY.mas_equalTo(wself.moviePlayer.view);
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.width.mas_equalTo(wself.moviePlayer.view.mas_width);
        
    }];
    
    
    [_tapView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    
    
    [_topBarIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(0);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize);
        make.width.mas_equalTo(wself.contentView);
        
        
        
    }];
    
    
    //    [_clearButton mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(wself.videoProgressV.mas_bottom).offset(12*kRateSize);//zzsc
    //        //        make.left.mas_equalTo(_nextPlayLabel.mas_right).offset(0*kRateSize);
    //        make.right.mas_equalTo(_bottomBarIV.right).offset(-50*kRateSize);//zzsc
    //        make.width.mas_equalTo(0*kRateSize);
    //        make.height.mas_equalTo(0*kRateSize);
    //
    //    }];
    //
    
    
    [_returnBtnIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.topBarIV.mas_top).offset(20*kDeviceRate);
        make.left.mas_equalTo(self.topBarIV.mas_left).offset(12*kDeviceRate);
        make.width.mas_equalTo(28*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
        
    }];
    
    
    [_returnMainBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(wself.contentView.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(50*kRateSize);
    }];
    
    
    
    
    [_bottomBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.height.mas_equalTo(60*kRateSize);
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    
    [_fullWindowbutton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(0);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(0);
        make.width.mas_equalTo(43*kRateSize);
        make.height.mas_equalTo(30*kRateSize);
        
    }];
    
    //旋转小屏界面亮度视图尺寸****************
    
    [_brightnessView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessProgress mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
    
    [_returnBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(0);
        make.width.mas_equalTo(55*kRateSize);
        make.height.mas_equalTo(55*kRateSize);
    }];
    
    
    
    
    
    [_serviceTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(40*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kDeviceRate);
        make.width.mas_equalTo(120*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        
        
    }];
    
    
    [_playPauseBigBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_tapView.mas_centerX);
        make.centerY.mas_equalTo(_tapView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];

    
    if (_isPaused == YES) {
        _playPauseBigBtn.hidden = NO;
    }else
    {_playPauseBigBtn.hidden = YES;
    }
    
    //    [_currentPlayBackLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(wself.videoProgressV.mas_bottom).offset(10*kRateSize);
    //        make.left.mas_equalTo(_playButton.mas_right).offset(15*kRateSize);
    //
    //        make.width.mas_equalTo(0*kRateSize);
    //        make.height.mas_equalTo(0*kRateSize);
    //
    //    }];
    
    
    
    
}
UIView *channellastView;
UIView* channelcontentView;
//进入到大屏播放器
-(void)setBigScreenPlayer
{
    [self reStartHiddenTimer];
    if (_isPaused == YES) {
        _playPauseBigBtn.hidden = NO;
    }else
    {_playPauseBigBtn.hidden = YES;
    }
    _currentTitleLabel.hidden = YES;
    //判定是否是遥控
    if (isremote== YES) {
        remote.hidden = YES;
    }else{
        remote.hidden = YES;
    }
    _shareView.hidden = YES;
    
    _videoProgressV.hidden = NO;
    _progressTimeLable_top.alpha = 1;
    if (_isHaveAD == NO) {
        _adPlayerView.rotateBtn.hidden = YES;
        if (_ADViewPauseBase.adplayerMenuType == ADPLAYER_MENUPause || _ADViewBase.adplayerMenuType == ADPLAYER_MENULive) {
            [_topBarIV setHidden:YES];
            [_bottomBarIV setHidden:YES];
            _screenLockedIV.hidden = YES;
            _screenLockedIV.alpha = 0;
        }else
        {
            [_topBarIV setHidden:NO];
            [_bottomBarIV setHidden:NO];
            _screenLockedIV.hidden = NO;
            _screenLockedIV.alpha = 1;
        }
        if (_isVerticalDirection == NO) {
            _fullWindowbutton.hidden = YES;
        }else{
            _fullWindowbutton.hidden = NO;
        }
        

        //        [_topBarIV setHidden:NO];
        //        [_bottomBarIV setHidden:NO];
    }else
    {
        [_topBarIV setHidden:YES];
        [_bottomBarIV setHidden:YES];
        _screenLockedIV.hidden = YES;
        _screenLockedIV.alpha = 0;
        _adPlayerView.rotateBtn.hidden = NO;
        _fullWindowbutton.hidden = YES;

    }
    [_adPlayerView.rotateBtn setImage:nil forState:UIControlStateNormal];
//    //        [_adPlayerView.rotateBtn setImage:[UIImage imageNamed:@"toSmallScreen_click"] forState:UIControlStateHighlighted];
        _adPlayerView.rotateBtn.tag = 1;
    _adPlayerView.backBtn.tag = 1;
//    _screenLockedIV.hidden = NO;
//    _screenLockedIV.alpha = 1;
    _dragLabel.hidden = YES;
    _moreBackView.hidden = YES;
//    _shareButton.hidden = NO;
//    _shareRightLabel.hidden = NO;
    _favourButton.hidden = NO;
//    _favourLabel.hidden = NO;
//    _topBarIV.hidden = NO;
//    _bottomBarIV.hidden = NO;
//    boNavi.hidden = YES;
//    _timeButton.hidden = NO;
    _shareButton.hidden = NO;
    _videoProgressV.hidden = NO;
    _currentTimeL.hidden = NO;
    _totalTimeL.hidden =NO;
    //    _interactButton.hidden = NO;
    _interactButton.hidden = IS_SUPPORT_MULTI_SCREEN_INERACT?NO:YES;
    _clearButton.hidden = NO;
    
    
    _channelButton.hidden = NO;
    _selectClearIV.hidden = YES;
    _videoListButton.hidden = NO;
    _returnBtn.hidden = NO;
    _returnBtnIV.hidden = YES ;
    _returnMainBtn.hidden = YES;
    _returnBtn.tag = RETURNBIG;
    [_returnBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _interactView.hidden = YES;
//    _shareButton.hidden = NO;
//    _IPTableView.hidden = YES;
    _IPBackIV.hidden = YES;

    //    _TVButton.hidden = NO;
    //    _MTButton.hidden = NO;
//    [_bottomBarIV setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
//    _fullWindowbutton.tag = 1;
//    [_fullWindowbutton setImage:[UIImage imageNamed:@"toSmallScreen.png"] forState:UIControlStateNormal];
//    [_fullWindowbutton setImage:[UIImage imageNamed:@"toSmallScreen_click"] forState:UIControlStateHighlighted];
//    [_fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    WS(wself);
    
    //旋转大屏界面背景底图视图尺寸******************
    
    [_backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //xiaoxiugai
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        //        make.centerX.mas_equalTo(wself.view.mas_centerX);
        //        make.centerY.mas_equalTo(wself.view.mas_centerY);
        make.left.mas_equalTo(wself.view.mas_left);
        make.width.mas_equalTo(kDeviceWidth);
        make.height.mas_equalTo(KDeviceHeight);
//        make.bottom.mas_equalTo(wself.view.mas_bottom);
        
    }];
    
    
    [_shareView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //xiaoxiugai
        make.top.mas_equalTo(_backgroundView.mas_top).offset(isAfterIOS6A?0: 0);
        make.left.mas_equalTo(_backgroundView.mas_left);
        make.width.mas_equalTo(_backgroundView);
        make.height.mas_equalTo(_backgroundView);
        make.bottom.mas_equalTo(_backgroundView.mas_bottom);
        
        
    }];
    
    
    //旋转大屏界面播放器视图尺寸******************
    //播放器界面图
    
    [_moviePlayer.view mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerX.mas_equalTo(wself.backgroundView.mas_centerX);
        //        make.centerY.mas_equalTo(wself.backgroundView);
        make.top.mas_equalTo (wself.backgroundView.mas_top).offset(isAfterIOS6A ? (0):0);
        make.height.mas_equalTo(wself.backgroundView.mas_height);
        make.width.mas_equalTo(wself.backgroundView.mas_width);
        make.left.mas_equalTo(wself.backgroundView.mas_left);
        _moviePlayer.view.alpha = 0.8;
        
    }];
    
    
    [_screenLockedIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.centerY.mas_equalTo(wself.moviePlayer.view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize, 30*kRateSize));
    }];
    
    [lockedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7.5*kRateSize);
        make.left.mas_equalTo(7.5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(20*kRateSize, 20*kRateSize));
    }];
    
    
    
    
    
    //旋转大屏界面内容图，放topBar bottomBar视图尺寸******************
    //内容图，放topBar bottomBar
    
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.width.mas_equalTo(wself.moviePlayer.view.mas_width);
        
    }];
    
    
    //旋转大屏界面tapView视图尺寸******************
    
    [_tapView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
    }];
    
    
    
    [_topBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(-15*kRateSize);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize);
        make.width.mas_equalTo(wself.contentView);
        
        
        
    }];
    
    //旋转大屏界面底部BottomBar视图尺寸******************
    
    [_bottomBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(0);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(60*kRateSize);
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    //左上返回按钮
    
    
    [_moreBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.topBarIV.mas_bottom).offset(0*kRateSize);
        make.right.mas_equalTo(wself.topBarIV.mas_right).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(100*kRateSize, 70*kRateSize));
        
        
    }];
    
    
    
    
    [_playPauseBigBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_tapView.mas_centerX);
        make.centerY.mas_equalTo(_tapView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];
    
    
//    [_currentPlayBackLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(wself.videoProgressV.mas_bottom).offset(10*kRateSize);
//        make.left.mas_equalTo(_playButton.mas_right).offset(15*kRateSize);
//        
//        make.width.mas_equalTo(209*kRateSize);
//        make.height.mas_equalTo(30*kRateSize);
//        
//    }];
    
    
    
    
    
    
    
    
    //清晰度按钮****************
    
    [_clearButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);//zzsc
        //        make.left.mas_equalTo(_nextPlayLabel.mas_right).offset(0*kRateSize);
        make.right.mas_equalTo(wself.bottomBarIV.right).offset(-15*kRateSize);//zzsc
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
        
    }];
    
    
    if (clearTitles.count >0) {
        _clearButton.hidden = NO;
    }else
    {
        _clearButton.hidden = YES;
    }
    
    
    [self allCodeClearButtonTitle];
    
    
    
    
    [_playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerX.mas_equalTo(wself.bottomBarIV);
        make.left.mas_equalTo(0*kRateSize);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-5*kRateSize);
        make.width.mas_equalTo(39*kRateSize);
        make.height.mas_equalTo(39*kRateSize);
        
    }];

    [_selectClearIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-40*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(clearTitles.count*30*kRateSize);
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-15*kRateSize);
    }];
    
    NSLog(@"kkkkkk%ld",_currentPlayType);
    
    
    
    if (_currentPlayType == CurrentPlayLive) {
        
        _playButton.hidden = YES;
        [_currentTimeL mas_updateConstraints:^(MASConstraintMaker *make) {
            
            //        make.centerY.mas_equalTo(wself.bottomBarIV).offset(-15);
            make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);
            make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(11*kRateSize);
            make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
            
        }];
        
        
        
        
        [_videoProgressV mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-22*kRateSize
                                                                        );
            make.left.mas_equalTo(_currentTimeL.mas_right).offset(-8*kRateSize);
            //        make.centerX.mas_equalTo(wself.bottomBarIV).offset(-10);
            make.height.mas_equalTo(6*kRateSize);
            make.right.mas_equalTo(_totalTimeL.mas_left).offset(8*kRateSize);
            //        make.width.mas_equalTo(@375);
        }];
    }
    
    if (_currentPlayType == CurrentPlayEvent || _currentPlayType == CurrentPlayTimeMove)
    {
        
        _playButton.hidden = NO;
        [_currentTimeL mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            //        make.centerY.mas_equalTo(wself.bottomBarIV).offset(-15);
            make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);
            make.left.mas_equalTo(_playButton.mas_right).offset(10*kRateSize);
            make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
            
        }];
        
        
        [_videoProgressV mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-22*kRateSize
                                                                        );
            make.left.mas_equalTo(_currentTimeL.mas_right).offset(-8*kRateSize);
            //        make.centerX.mas_equalTo(wself.bottomBarIV).offset(-10);
            make.height.mas_equalTo(6*kRateSize);
            make.right.mas_equalTo(_totalTimeL.mas_left).offset(8*kRateSize);
//            make.width.mas_equalTo(kDeviceWidth-(30+180)*kRateSize);
            //        make.width.mas_equalTo(@375);
        }];
  
    }
    
    
    [_totalTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);
        //make.top.mas_equalTo(wself.bottomBarIV.mas_top).offset(10);
        //        make.right.mas_equalTo(fullWindowbutton.mas_left);
        make.right.mas_equalTo(_clearButton.mas_left).offset(-15*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];

    
    
    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_contentView.mas_centerX);
        make.centerY.mas_equalTo(_contentView.mas_centerY);
        make.height.mas_equalTo(70*kRateSize);
        make.width.mas_equalTo(130*kRateSize);
      
        
    }];
    
    
    
    [_progressDirectionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_progressTimeView.mas_centerY).offset(-15*kRateSize);
        make.centerX.mas_equalTo(_progressTimeView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
    }];
    
    
    
    [_progressTimeLable_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.left.mas_equalTo(wself.progressTimeView.mas_left).offset(5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_progressTimeLable_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.right.mas_equalTo(wself.progressTimeView.mas_right).offset(-5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
        
    }];
    
    
    
    
   
    
    
    //    [_bigtmDragLabel setFrame:CGRectMake(_bigtimeMoveSlider.frame.origin.x-8, _bigtimeMoveSlider.frame.origin.y-30, 100, 20)];
    
    [_dragLabel setFrame:CGRectMake(_videoProgressV.frame.origin.x-8, _videoProgressV.frame.origin.y-30, 100*kRateSize, 20)];
    //
    
    //    frame.origin.x = CGRectGetMaxX(self.videoProgressV.frame)+10;
    //    frame.origin.y = CGRectGetMinY(self.videoProgressV.frame);
    
    
    //    [_dragLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.center.mas_equalTo(wself.videoProgressV.center);
    //         make.size.mas_equalTo(CGSizeMake(100, 20));
    //
    //
    //    }];
    
    
    
    
    
   
    
    
//    [_currntPlayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(wself.videoProgressV.mas_bottom).offset(10*kRateSize);
//        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(39*kRateSize);
//        
//        make.width.mas_equalTo(isAfterIOS6A?170*kRateSize:180*kRateSize);
//        make.height.mas_equalTo(30*kRateSize);
//        
//        
//    }];
//    
//    
//    
//    //    _nextPlayLabel.text = @"正在播放：中国达人秀";
//    
//    [_nextPlayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(wself.videoProgressV.mas_bottom).offset(10*kRateSize);
//        make.left.mas_equalTo(_currntPlayLabel.mas_right).offset(isAfterIOS6A?0*kRateSize:20*kRateSize);
//        
//        make.width.mas_equalTo(isAfterIOS6A?170*kRateSize:180*kRateSize);
//        make.height.mas_equalTo(30*kRateSize);
//        
//        
//    }];
    
    
    
    //收藏按钮********************
    
//    [_favourButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_moreBackView.mas_left).offset(0);
//        make.top.mas_equalTo(_moreBackView.mas_top).offset(0*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 35*kRateSize));
//        
//    }];
    
    
    
//    [_favourLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_favourButton.mas_right).offset(0);
//        make.top.mas_equalTo(_moreBackView.mas_top).offset(0*kRateSize);
//        //        make.size.mas_equalTo(CGSizeMake(42*kRateSize, 35*kRateSize));
//        make.height.mas_equalTo(35*kRateSize);
//        make.right.mas_equalTo(_moreBackView.mas_right);
//        
//    }];
    
    
    
//    [_shareButton mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_moreBackView.mas_left).offset(0);
//        make.top.mas_equalTo(_favourButton.mas_bottom).offset(0*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 35*kRateSize));
//        
//    }];
    
    
//    [_shareRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_shareButton.mas_right).offset(0);
//        make.top.mas_equalTo(_favourLabel.mas_bottom).offset(0*kRateSize);
//        //        make.size.mas_equalTo(CGSizeMake(42*kRateSize, 35*kRateSize));
//        make.right.mas_equalTo(_moreBackView.mas_right);
//        make.height.mas_equalTo(35*kRateSize);
//        
//    }];
    
    
    //end**************
    
    [_RemoteButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        
        make.bottom.mas_equalTo(_TVButton.mas_top).offset(0);
        
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
    }];
    
    
    
    
    
    //TVButton
    [_TVButton mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.right.mas_equalTo(wself.contentView.right).offset(0);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(60);
        //        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
        
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        //        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(60);
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
        make.centerY.mas_equalTo(wself.moviePlayer.view.mas_centerY).offset(-20*kRateSize);
        
        
        
    }];
    
    //_MTButton
    
    [_MTButton mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.right.mas_equalTo(wself.contentView.right).offset(0);
        //        make.top.mas_equalTo(_TVButton.mas_bottom).offset(0);
        //
        //        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
        
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        
        make.top.mas_equalTo(_TVButton.mas_bottom).offset(0);
        
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
        
        
        
    }];
    
    
    
   
    
    
    
    
    
//    [_fullWindowbutton mas_updateConstraints:^(MASConstraintMaker *make) {
//        
//        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-10*kRateSize);
//        //        make.centerY.mas_equalTo(wself.bottomBarIV);
//        make.top.mas_equalTo(wself.bottomBarIV.mas_top).offset(25*kRateSize);
//        make.width.mas_equalTo(22.5*kRateSize);
//        make.height.mas_equalTo(23.5*kRateSize);
//        
//    }];
    
    
    
    
    [_returnBtnIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(42*kDeviceRate);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(12*kDeviceRate);
        make.width.mas_equalTo(28*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
        
    }];
    
    
    [_returnBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(wself.topBarIV);
        
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kRateSize);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(44*kRateSize);
        make.height.mas_equalTo(wself.topBarIV.mas_height);
    }];
    
    
    
    
    
    [_serviceTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(50*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(42*kDeviceRate);        make.width.mas_equalTo(220*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        
        
    }];
    
    
    
    [_currentTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.serviceTitleLabel.mas_right).offset(-0*kRateSize);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(30*kRateSize);
        make.width.mas_equalTo(120*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
    }];
    
    
    
    
    [_shareButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.topBarIV.mas_right).offset(0*kRateSize);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(42*kRateSize, 45*kRateSize));
        
        
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(12*kDeviceRate);
//        make.width.mas_equalTo(28*kDeviceRate);
//        make.height.mas_equalTo(28*kDeviceRate);

        
        
        
    }];
    
    [_favourButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_shareButton.mas_left).offset(0*kRateSize);
//        make.centerY.mas_equalTo(wself.topBarIV);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(42*kRateSize, 45*kRateSize));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];
    
    [_channelButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_favourButton.mas_left);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize, 45*kRateSize));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];
    
    [_videoListButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_channelButton.mas_left);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize, 45*kRateSize));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];
    
    [_interactButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_videoListButton.mas_left);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize, 45*kRateSize));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];
    
    
    
    //互动下方View的背景图************
    [_interactView mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.top.mas_equalTo(wself.topBarIV.mas_bottom).offset(10);
        //        make.left.mas_equalTo(_interactButton.mas_left).offset(-20);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(140*kRateSize, 120*kRateSize));
        
        
    }];
    
    
    [remoteControlBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_interactView.mas_top);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gestureRemote = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gestureRemote.numberOfTapsRequired = 1;
    gestureRemote.tag = REMOTECONTROL;
    [remoteControlBackV addGestureRecognizer:gestureRemote];
    
    
    
    
    [remoteLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remoteControlBackV.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(remoteControlBackV.mas_left).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 40*kRateSize));
        
    }];
    
    
    [remoteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(remoteLeftV.mas_right).offset(0);
        make.top.mas_equalTo(remoteControlBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize);
        make.height.mas_equalTo(remoteControlBackV.mas_height);
        
    }];
    
    
    [remoteDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(remoteControlBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize);
        make.height.mas_equalTo(1*kRateSize);
        make.bottom.mas_equalTo(remoteControlBackV.mas_bottom);
        
    }];
    
    
    
    
    
    [pushBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(remoteControlBackV.mas_bottom).offset(0);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gesturePush = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gesturePush.numberOfTapsRequired = 1;
    gesturePush.tag = PUSH;
    gesturePush.cancelsTouchesInView = YES;
    [pushBackV addGestureRecognizer:gesturePush];
    
    
    
    
    
    [pushLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pushBackV.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(pushBackV.mas_left).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 40*kRateSize));
        
    }];
    
    [pushLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pushLeftV.mas_right).offset(0);
        make.top.mas_equalTo(pushBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize);
        make.height.mas_equalTo(pushBackV.mas_height);
        
    }];
    
    [pushDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pushBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize);
        make.height.mas_equalTo(1*kRateSize);
        make.bottom.mas_equalTo(pushBackV.mas_bottom);
        
    }];
    
    
    
    [pullBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(pushBackV.mas_bottom).offset(0);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gesturePull = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gesturePull.numberOfTapsRequired = 1;
    gesturePull.tag = PULL;
    [pullBackV addGestureRecognizer:gesturePull];
    
    [pullLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pullBackV.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(pullBackV.mas_left).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 40*kRateSize));
        
    }];
    
    [pullLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pullLeftV.mas_right).offset(0);
        make.top.mas_equalTo(pullBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize);
        make.height.mas_equalTo(pullBackV.mas_height);
        
    }];
    
    [pullDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pullBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize);
        make.height.mas_equalTo(1*kRateSize);
        make.bottom.mas_equalTo(pullBackV.mas_bottom);
        
    }];
    
    //互动下方View的互动手势View************end***************
    
    
    //互动下方View的背景图************
    [_videoListView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV).offset(10*kRateSize);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right);
        //        make.size.mas_equalTo(CGSizeMake(150.5*kRateSize, 181*kRateSize));
        
        make.width.mas_equalTo(264*kRateSize);
//        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        
        
        make.bottom.mas_equalTo(wself.bottomBarIV);
    }];
    
    
    //    [remote mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.width.mas_equalTo(wself.view.mas_width);
    //        make.height.mas_equalTo(wself.view.mas_height);
    //        make.top.mas_equalTo(wself.view.mas_top);
    //        make.right.mas_equalTo(wself.view.mas_right);
    //    }];
    
    
    [_IPBackIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 165*kRateSize));
    }];
    
    
    
    
    [_IPTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.mas_equalTo(_IPBackIV.mas_left);
        make.right.mas_equalTo(_IPBackIV.mas_right);
        make.top.mas_equalTo(_IPBackIV.mas_top);
        make.height.mas_equalTo(135*kRateSize);
        
        
        
    }];
    
    
    [_IPSeperateLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_IPTableView.mas_bottom);
        make.left.mas_equalTo(_IPTableView.mas_left);
        make.width.mas_equalTo(200*kRateSize);
        make.height.mas_equalTo(1*kRateSize);
    }];
    
    [_IPButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_IPSeperateLineView.mas_bottom);
        make.centerX.mas_equalTo(_IPBackIV.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 30*kRateSize));
    }];
    
    
    
    //节目单的数据列表
    
    [_videoListTV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerX.equalTo(_videoListView);
        //        make.centerY.mas_equalTo(_videoListView);
        make.top.mas_equalTo(_videoListView.mas_top);
        make.right.mas_equalTo(_videoListView.mas_right);
        make.height.mas_equalTo(_videoListView.mas_height);
        make.width.mas_equalTo(212*kRateSize);
    }];
    
    
    
    [_channelListView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(10*kRateSize);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right);
        //        make.size.mas_equalTo(CGSizeMake(120.5*kRateSize, 81*kRateSize));
        make.width.mas_equalTo(264*kRateSize);
//        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.bottom.mas_equalTo(wself.bottomBarIV);
    }];
    //cap
    [_ADViewBase mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV.mas_top);
        make.right.mas_equalTo(_channelListView.mas_left).offset(-20*kRateSize);
        //        make.size.mas_equalTo(CGSizeMake(120.5*kRateSize, 81*kRateSize));
        //        make.width.mas_equalTo((kDeviceWidth-264)*kRateSize);
        make.left.mas_equalTo(wself.tapView.mas_left).offset(20*kRateSize);
        //        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        //         make.height.mas_equalTo(100);
        make.bottom.mas_equalTo(wself.tapView.mas_bottom);
        
    }];
    
    
    [_ADViewPauseBase mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_contentView);
    }];

    
    [_unFavourBV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_channelListView);
        //        make.size.mas_equalTo(CGSizeMake(212*kRateSize, 40*kRateSize));
        //        make.left.mas_equalTo(_channelListView.mas_height/2);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right);
        make.width.mas_equalTo(212*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
        
    }];
    
    
    [_noFavorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_unFavourBV.mas_centerY);
        //        make.size.mas_equalTo(CGSizeMake(212*kRateSize, 40*kRateSize));
        make.left.mas_equalTo(_unFavourBV.mas_left).offset(30*kRateSize);
        //        make.top.mas_equalTo(_unFavourBV.mas_top);
        make.width.mas_equalTo(17*kRateSize);
        make.height.mas_equalTo(15*kRateSize);
        
    }];
    
    [_noFavorTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_unFavourBV.mas_centerY);
        //        make.size.mas_equalTo(CGSizeMake(212*kRateSize, 40*kRateSize));
        make.left.mas_equalTo(_noFavorImageView.mas_right);
        //        make.top.mas_equalTo(_unFavourBV.mas_top);
        make.width.mas_equalTo(200*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
        
    }];
    
    
    
    
    //频道的数据列表
    
    
    [_channelListTV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerX.equalTo(_videoListView);
        //        make.centerY.mas_equalTo(_videoListView);
        make.top.mas_equalTo(_channelListView.mas_top);
        make.right.mas_equalTo(_channelListView.mas_right);
        make.height.mas_equalTo(_channelListView.mas_height);
        make.width.mas_equalTo(212*kRateSize);
    }];
    
    
    
    
    //亮度进度条中间View****************
    
    
    [_brightnessView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessProgress mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
}




-(void)setWeekdays:(NSMutableArray *)weekdays
{
    WS(wself);
    
    _weekdays = weekdays;
    
    if (_epgVideoListSV) {
        [_epgVideoListSV removeFromSuperview];
        _epgVideoListSV = nil;
    }
    
//    _epgVideoListSV = [[EPGScrollView alloc]initEpgScrollViewWithScrollViewType:EPGScrollViewVideoList andScrollViewArr:weekdays];
    
    _epgVideoListSV = [[EPGScrollView alloc]initEpgScrollViewWithScrollViewType:EPGScrollViewVideoList andScrollViewArr:weekdays isVideoListWithCurrentDateStr:_currentDateStr andEPGDateType:_dateType];

    
    _epgVideoListSV.currentDateStr = _currentDateStr;
    [_channelListView addSubview:_epgVideoListSV];
    
    __weak EPGDateSelectedView *weakdateView = dateView;
    __weak UITableView *weakEPGListTableView = _EPGListTableView;
    
    _epgVideoListSV.scrollSelectVideoListBtnReturn = ^(UIButton_Block *btn)
    {
        NSArray *array = btn.superview.subviews;
        NSLog(@"%ld  === ",(long)btn.tag);
        
        for (UIButton *button in array) {
            
            if(button.tag != btn.tag )
            {
                [button setTitleColor:App_unselected0range_color forState:UIControlStateNormal];
            }
            else if([button isKindOfClass:[UIButton class]])
            {
                
                NSLog(@"%f......%f......%f......%f",btn.frame.origin.x,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height);
                [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
                
                
            }
            
            
        }
        
        
        
        
        _channelSelectIndex = -1;
        firstPeriodLive = nil;
        startDateLive = [SystermTimeControl GetSystemTimeWith];
        ZTTimeIntervalLive=0;
        if (USER_ACTION_COLLECTION == YES) {
            
            
            //采集   点击更换时间，进行计算浏览的时长以及浏览的用户信息的存储
            wself.skimDuration = [wself EPGtimeLengthForCurrent];
            //采集  存储EPG
            [SAIInformationManager EPGinformationForChanelFenlei:wself.serviceID andChanelName:wself.serviceName andChanelInterval:wself.epgDateStr andSkimDuration:wself.skimDuration andSkimTime:wself.nowDateString];
            
            //over
            
        }
        
        //    if (btn.tag==2) {
        //        [self resetHiddenEPGDataAndNoDataLabel:NO];
        //        return;
        //    }
        
        NSString *DATEStr = [NSString stringWithFormat:@"%@",btn.dateStr];
        
        _currentDateStr = DATEStr;
        
        HTRequest *request =  [[HTRequest alloc]initWithDelegate:wself];
        [request EPGSHOWWithServiceID:wself.serviceID andDate:DATEStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
        _epgDateStr =[[NSString alloc] initWithFormat:@"%@ 00:00~%@ 23:59",_currentDateStr,_currentDateStr];
        
        [weakdateView DateBtnSelectedAtDate:wself.currentDateStr];
        
        [wself.channelListTV reloadData];
        [weakEPGListTableView reloadData];
        
        
        
    };
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
        HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
        [request EPGSHOWWithServiceID:_serviceID andDate:_currentDateStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];

        //这里是1.0f后 你要做的事情
        [_epgVideoListSV MMMMDateBtnSelectedAtDate:_currentDateStr];
        
    });
    
    
    
    [_epgVideoListSV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_channelListView.mas_left);
        make.top.mas_equalTo(_channelListView.mas_top);
        make.width.mas_equalTo(51*kRateSize);
        make.height.mas_equalTo(_channelListView.mas_height);
    }];
    
    
    
}




-(void)setCategoryArr:(NSMutableArray *)categoryArr
{
    WS(wself);
    _categoryArr = categoryArr;
    
    if (_categoryArr.count>0) {
        
    if (_epgChannelSV) {
        [_epgChannelSV removeFromSuperview];
        _epgChannelSV = nil;
    }
    
    
    _epgChannelSV = [[EPGScrollView alloc]initEpgScrollViewWithScrollViewType:EPGScrollViewChannel andScrollViewArr:categoryArr];
    _epgChannelSV.clickCategoryTag = _clickCategoryTag;
    [_videoListView addSubview:_epgChannelSV];
        
    __weak UIView *weakunFavourBV = _unFavourBV;
        
    __weak NSString *weakcurrentPlayServiceID = _currentPlayServiceID;
        
    __block NSInteger weakvideoPlayPosition = _videoPlayPosition;
        
    _epgChannelSV.scrollSelectChannelBtnReturn = ^(UIButton_Block *buttonBlock)
    {
        NSLog(@"******%ld",buttonBlock.tag);
        
        
        [wself reStartHiddenTimer];
        
        
        _clickCategoryTag = buttonBlock.tag;
        //    [request EPGGetLivePageChannelInfo];
        
        
        
        if (_clickCategoryTag == 0) {
            
            _videoListArr = [self getFavourEPG:_allCategoryEPGServiceEventArr];
            
            
            
        }
        else if (_clickCategoryTag  == 1)
        {
            weakunFavourBV.hidden = YES;
            _videoListArr = [self EPGGetAllCategory:_dicArr];
            
        }
        else
        {
            weakunFavourBV.hidden = YES;
            _videoListArr = [self EpgGetSingleCategory:_dicArr andType:[NSString stringWithFormat:@"%ld",_clickCategoryTag -1] ];
        }
        
       _clickCategoryString =  [self typeListArrForTitleString:_clickCategoryTag andWithTypeListArr:typelistARR];
        
        [wself.videoListTV reloadData];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (_videoListArr.count>0) {
                
                for (int i = 0; i<_videoListArr.count; i++) {
                    NSDictionary *videoDic = [[NSDictionary alloc]init];
                    
                    videoDic = [_videoListArr objectAtIndex:i];
                    if ([[videoDic objectForKey:@"categoryServiceID"] isEqualToString:weakcurrentPlayServiceID]) {
                        _videoPlayPosition = i;
                    }
                    
                }
                
                if (_videoListArray.count >_videoPlayPosition) {
                    [wself.videoListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakvideoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                }
                
                
            }
            
        });
        
        
        
        
        
        
        
        
    };
    [_epgChannelSV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_videoListView.mas_left);
        make.top.mas_equalTo(_videoListView.mas_top);
        make.width.mas_equalTo(51*kRateSize);
        make.height.mas_equalTo(_videoListView.mas_height);
    }];
        
    }else
    {
        return;
    }
    
    
}




/**
 *  点击的mygesture事件
 *
 *  @param
 */



/**
 *  点击的mygesture事件
 *
 *  @param
 */

- (void)goToGesture:(MyGesture *)gesture
{
    
    
    switch (gesture.tag) {
        case REMOTECONTROL:
        {
            NSLog(@"remoteControl");
            //测试
            
            
//            for (int i = 0; i<3; i++) {
//                
//                [_IPArray addObject:[NSString stringWithFormat:@"192.168.1.%d",arc4random()%123]];
//            }
//            
            
//            [_IPArray addObject:@"192.168.1.1"];
            remote.hidden = YES;
            isremote = YES;
            
            _interactView.hidden = !_interactView.hidden;
            [self reStartHiddenTimer];
            
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length < 7) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                _IPBackIV.hidden = YES;
//                _IPTableView.hidden = YES;
                
                [self showSearchDeviceLoadingView];
                
                break;
            }else{
                //
                
                [self reloadIPTableView];
                
                break;
            }
            
            
            //            [self.contentView removeGestureRecognizer:tapGestuerImfo];
            //                RemoteControlView *remote = [[RemoteControlView alloc] initWithFrame:CGRectMake(0, frame.size.height/2, self.view.bounds.size.width , self.view.bounds.size.height/2) ];
            
            
            
            
        }
            break;
        case PUSH:
        {
            NSLog(@"push");
            
            _interactView.hidden = !_interactView.hidden;
            
            NSString * nStr =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"pointToPointIP"]];
            
            if (nStr.length<7) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                //                [AlertLabel AlertInfoWithText:@"发现设备ing~" andWith:self.view withLocationTag:0];
                //
                
                [self showSearchDeviceLoadingView];
                //                _IPTableView.hidden = NO;
                
                return;
            }
            
            [AlertLabel AlertInfoWithText:@"推屏啦~" andWith:self.view withLocationTag:0];
            //IP不为空的时候
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            NSString *command ;
            NSDictionary *video ;
            if (_channelListArr.count>0) {
                video  = [NSDictionary dictionaryWithDictionary:[_channelListArr objectAtIndex:_videoPlayPosition]];
            }
            
            
            if (_theVideoInfo.contentIDP.length >0) {
                
                NSString * contentID = _theVideoInfo.contentIDP;
                
                
                NSString * name = [NSString stringWithFormat:@"%@",[self seperateServiceTitle:_serviceTitleLabel.text]];
                NSString *breakpoint = [NSString stringWithFormat:@"%ld",[self recordPlaybackTime]];
                dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)breakpoint],@"seekPos",name,@"name", nil];
                command = @"DLNAPLAY_VOD";
                
            }
            
            
            if (_currentPlayType == CurrentPlayLive) {
                
                dic = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",[self seperateServiceTitle:_serviceTitleLabel.text]] forKey:@"channelName"];
                command = @"DLNAPLAY_LIVE";
                
            }
            
            if (_currentPlayType == CurrentPlayEvent)
            {
                command = @"DLNAPLAY_EVENT";
                NSString *timeV = [[[video objectForKey:@"fullStartTime"] componentsSeparatedByString:@" "] objectAtIndex:0];
                //                NSString *breakpoint = [NSString stringWithFormat:@"%d",[self getRecordTime]];//这里可以用进度条的值value/maxValue来计算 这样会避免崩掉的现象
                
                NSString *breakpoint = [NSString stringWithFormat:@"%2ld",[self recordPlaybackTime]];//这里可以用进度条的值value/maxValue来计算 这样会避免崩掉的现象
                
                NSString *startDate;
                NSString *endDate;
                
                if ([startDate isEqualToString:@""]) {
                    
                    startDate = [NSString stringWithFormat:@"%@",_theVideoInfo.fullStartTimeP];
                }else
                {
                    startDate = [NSString stringWithFormat:@"%@",[video objectForKey:@"fullStartTime"]];
                }
                
                if ([endDate isEqualToString:@""]) {
                    
                    endDate = [NSString stringWithFormat:@"%@",_theVideoInfo.endTimeP];
                }else
                {
                    endDate =  [NSString stringWithFormat:@"%@ %@",timeV,[video objectForKey:@"endTime"]];
                }
                
                
                
                
                //                NSString *startDate = [NSString stringWithFormat:@"%@",[video objectForKey:@"fullStartTime"]];
                //                NSString *endDate = [NSString stringWithFormat:@"%@ %@",timeV,[video objectForKey:@"endTime"]];
                
                //                _theVideoInfo.videoID = [video objectForKey:@"id"];
                //                _theVideoInfo.videoName = [video objectForKey:@"eventName"];
                //                _theVideoInfo.videoEndTime = [video objectForKey:@"endTime"];
                //         NSString *startDate = [NSString stringWithFormat:@"%@",_theVideoInfo.videoTime];
                //         NSString *endDate = [NSString stringWithFormat:@"%@",_theVideoInfo.videoEndTime];
                
                
                dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[self seperateServiceTitle:_serviceTitleLabel.text]],@"channelName",startDate,@"startDate",endDate,@"endDate",breakpoint,@"seekPos", nil];
            }
            
            
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                
                command = @"DLNAPLAY_PAUSE";
                NSString *channalName = _theVideoInfo.channelName;
                NSString *seekPos = [NSString stringWithFormat:@"%ld",(long)_MoveTimeInteger];
                NSString *UTCTime = [NSString stringWithFormat:@"%lld",(long long) [[SystermTimeControl GetSystemTimeWith] timeIntervalSince1970]*1000];
                
                dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:channalName,@"channelName",seekPos,@"seekPos",UTCTime,@"UTCTime", nil];
                
            }
            
            
            NSDictionary *resultDic=[NSDictionary dictionaryWithObjectsAndKeys:command,@"@command",dic,@"param", nil];
            NSLog(@"甩屏信息 ： %@",resultDic);
            if (nStr.length>0) {
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:resultDic];
                
            }else if (nStr.length < 6)
            {
                
                [AlertLabel AlertInfoWithText:@"暂无可连接设备" andWith:self.view withLocationTag:0];
//                _IPTableView.hidden = NO;
                _IPBackIV.hidden = NO;

                
            }
            
            
            
            
        }
            break;
        case PULL:
        {
            NSLog(@"pull");
            
            _interactView.hidden = !_interactView.hidden;
            
            NSDictionary *daramDic;
            daramDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_REQUEST",@"@command",nil];
            
            
            NSString * nStr =  [[NSUserDefaults standardUserDefaults]objectForKey:@"pointToPointIP"];
            if (nStr.length>2&&nStr!=nil) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:daramDic];
                //                [AlertLabel AlertInfoWithText:@"拉屏啦~" andWith:self.view withLocationTag:0];
                [AlertLabel AlertInfoWithText:@"拉屏啦~" andWith:self.view withLocationTag:0];
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                
            }else if (nStr.length < 7)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                
                //                _IPTableView.hidden = NO;
                return;
                
            }
            
            
        }
            break;
            
            
        case SHAREVIEWTAG:
        {
            _shareView.hidden = YES;
            
        }
            break;
            
            
            
            
        default:
            break;
    }
}







-(void)MYVideoGesture:(UIButton*)btn
{
    NSArray *array = btn.superview.subviews;
    NSLog(@"%ld  === ",(long)btn.tag);
    
    for (UIButton *button in array) {
        
        if(button.tag != btn.tag )
        {
            [button setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
        }
        else if([button isKindOfClass:[UIButton class]])
        {
            
            NSLog(@"%f......%f......%f......%f",btn.frame.origin.x,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height);
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            
        }
        
        
    }
    
    
    
    [self reStartHiddenTimer];
    
    
    _clickCategoryTag = btn.tag;
    //    [request EPGGetLivePageChannelInfo];
    
    
    
    if (_clickCategoryTag == 0) {
        
        _videoListArr = [self getFavourEPG:_allCategoryEPGServiceEventArr];
        
        
        
    }
    else if (_clickCategoryTag  == 1)
    {
        _unFavourBV.hidden = YES;
        _videoListArr = [self EPGGetAllCategory:_dicArr];
        
    }
    else
    {
        _unFavourBV.hidden = YES;
        _videoListArr = [self EpgGetSingleCategory:_dicArr andType:[NSString stringWithFormat:@"%ld",_clickCategoryTag -1] ];
    }
    
        
    
    [_videoListTV reloadData];
    
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (_videoListArr.count>0) {
            
            for (int i = 0; i<_videoListArr.count; i++) {
                NSDictionary *videoDic = [[NSDictionary alloc]init];
                
                videoDic = [_videoListArr objectAtIndex:i];
                if ([[videoDic objectForKey:@"categoryServiceID"] isEqualToString:_currentPlayServiceID]) {
                    _videoPlayPosition = i;
                }
                
            }
            
            if (_videoListArray.count >_videoPlayPosition) {
                [_videoListTV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_videoPlayPosition inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            }
            
            
        }

    });
    
    
    
    
   
    
    
    
    
    
    
    
    
}




-(NSMutableArray *)getFavourEPG:(NSMutableArray *)arr
{
//    UserCollectCenter *collectCenter = [UserCollectCenter defaultCenter];
//    _favourChannelArr = collectCenter.favoriteChannelArr;
    
    //ww...
    _favourChannelArr = [NSMutableArray arrayWithArray:[_localListCenter getLiveFavoriteListWithUserType:YES]];
    
    if (_favourChannelArr.count>0) {
        _unFavourBV.hidden = YES;
    }else
    {
        _unFavourBV.hidden = NO;
    }
    
    
    NSMutableArray *FavourArr = [[NSMutableArray alloc]init];
    if (_favourChannelArr.count) {
        
    for (int i = 0; i<arr.count; i++) {
        
        NSDictionary *arrDic = [arr objectAtIndex:i];
        
        if (_favourChannelArr.count>0) {
            
            
            for (int j = 0; j<_favourChannelArr.count; j++) {
                
                NSDictionary *favdic = [[NSDictionary alloc]init];
                favdic = [_favourChannelArr objectAtIndex:j];
                if ([[arrDic valueForKey:@"categoryServiceID"]isEqualToString:[favdic valueForKey:@"id"]])
                {
                    
                    [FavourArr addObject:arrDic];
                    
                }
                
                
                
            }
        }
    }
    }
    return FavourArr;
    
    
    
    
}





-(void)channelgesture:(TimeButton*)btn
{
    
    NSArray *array = btn.superview.subviews;
    NSLog(@"%ld  === ",(long)btn.tag);
    
    for (UIButton *button in array) {
        
        if(button.tag != btn.tag )
        {
            [button setTitleColor:App_unselected0range_color forState:UIControlStateNormal];
        }
        else if([button isKindOfClass:[UIButton class]])
        {
            
            NSLog(@"%f......%f......%f......%f",btn.frame.origin.x,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height);
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            
        }
        
        
    }
    
    
    
    
    _channelSelectIndex = -1;
    firstPeriodLive = nil;
    startDateLive = [SystermTimeControl GetSystemTimeWith];
    ZTTimeIntervalLive=0;
    if (USER_ACTION_COLLECTION == YES) {
        
        
        //采集   点击更换时间，进行计算浏览的时长以及浏览的用户信息的存储
        self.skimDuration = [self EPGtimeLengthForCurrent];
        //采集  存储EPG
        [SAIInformationManager EPGinformationForChanelFenlei:_serviceID andChanelName:_serviceName andChanelInterval:_epgDateStr andSkimDuration:self.skimDuration andSkimTime:self.nowDateString];
        
        //over
        
    }
    
    //    if (btn.tag==2) {
    //        [self resetHiddenEPGDataAndNoDataLabel:NO];
    //        return;
    //    }
    
    NSString *DATEStr = [NSString stringWithFormat:@"%@",btn.dateStr];
    
    _currentDateStr = DATEStr;
    
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    [request EPGSHOWWithServiceID:_serviceID andDate:DATEStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
    _epgDateStr =[[NSString alloc] initWithFormat:@"%@ 00:00~%@ 23:59",_currentDateStr,_currentDateStr];
    
    [dateView DateBtnSelectedAtDate:DATEStr];
    
    
    [_channelListTV reloadData];
    [_EPGListTableView reloadData];
    
    
    
    
}







/**
 *  点击的tap事件
 *
 *  @param
 */

- (void)singleTapAction:(UITapGestureRecognizer *)sender
{
    [[self.view viewWithTag:8080]setHidden:YES];
    
    
    if (![_topBarIV isHidden]) {
        [self hiddenPanlwithBool:YES];
        
    } else {
        [self hiddenPanlwithBool:NO];
    }
    
    
    
}



- (void) hiddenPanlwithBool:(BOOL) bol
{
    
    if (bol) {
        [UIView animateWithDuration:0.1 animations:^{
            if (_isHaveAD == YES) {
                return ;
            }else
            {
            [_topBarIV setHidden:YES];
            [_bottomBarIV setHidden:YES];
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _channelListView.hidden = YES;
            _ADViewBase.hidden = YES;//cap
            _moreBackView.hidden = YES;
            _bottomBarIV.userInteractionEnabled = YES;
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _MTButton.alpha = 0;
            _TVButton.alpha = 0;
            _RemoteButton.alpha = 0;
            [[self.view viewWithTag:8080]setHidden:YES];
            
            //            //zzs
            //            _BigView.hidden = YES;
            //            _backHistoryView.hidden = YES;
            //            _bottomBarIV.userInteractionEnabled = YES;
            //
            if (!_isScreenLocked) {
                _screenLockedIV.hidden = YES;
                _screenLockedIV.alpha = 0;

            }
            
            if (_isVerticalDirection == YES) {
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
                _returnBtnIV.hidden = NO;
                _returnMainBtn.hidden = NO;
 
            }else
            {
                         [[UIApplication sharedApplication] setStatusBarHidden:YES];
                _returnBtnIV.hidden = YES;
                _returnBtn.hidden = NO;
            }
            //
            //            [self.smallBottomBarIV setHidden:YES];
            //            [_leftIV setHidden:YES];
            //            [_videoNameL setHidden:YES];
            //            //403 用
            //
            //            if (self.contentView.hidden != YES) {
            //                [[UIApplication sharedApplication] setStatusBarHidden:YES];
            //            }
            
            //            _selectVolumeIV.hidden = YES;
            //            _isShowSelectVolumeIV = NO;
            [_selectClearIV setHidden:YES];
            //            //            [_videoListTV setHidden:YES];
            //            //            [_selectShareIV setHidden:YES];
            _isClickedClear = YES;
            if (clearTitles.count >0) {
                _clearButton.hidden = YES;
            }else
            {
                _clearButton.hidden = YES;
            }
        }//            _isClickedVideoList = NO;
        }];
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    } else {
        
        if (_isHaveAD == YES) {
            
            return;
        }else
        {
        [[self.view viewWithTag:8080]setHidden:YES];
         [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [_topBarIV setHidden:NO];
            
            
            [_bottomBarIV setHidden:NO];

        //        _clearButton.hidden = NO;
        _isClickedClear = NO;
        [self allCodeClearButtonTitle];
        _interactView.hidden = YES;
        _videoListView.hidden = YES;
        _channelListView.hidden = YES;
//        _ADViewBase.hidden = YES;//cap
//        _IPTableView.hidden = YES;
        _IPBackIV.hidden = YES;

        _moreBackView.hidden = YES;
        _MTButton.alpha = 1;
        _TVButton.alpha = 1;
        _RemoteButton.alpha = 1;
        if (_isVerticalDirection == YES) {
            _screenLockedIV.hidden = YES;
            _screenLockedIV.alpha = 0;
            _fullWindowbutton.hidden = NO;
            _returnBtnIV.hidden = NO;
            _returnMainBtn.hidden = NO;
        }else
        {
            _returnBtnIV.hidden = NO;
            _returnBtn.hidden = NO;
            if (!_isScreenLocked) {
                _screenLockedIV.hidden = NO;
                _screenLockedIV.alpha = 1;
            }
            _fullWindowbutton.hidden = YES;
        }

        //        [self.smallBottomBarIV setHidden:NO];
        //        [_leftIV setHidden:NO];
        
        //        if (self.contentView.hidden != YES) {
        //            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        //            ////            [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:YES];
        //        }
        //
        //        [_videoNameL setHidden:NO];
        [_selectClearIV setHidden:YES];
        //        [_videoListTV setHidden:YES];
        [self reStartHiddenTimer];
        }
    }
}


-(void)reStartHiddenTimer
{
    self.autoHiddenSecend = 5;
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
    self.autoHiddenTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(autoHiddenPanel:) userInfo:nil repeats:YES];
}



#pragma mark - alertView delegate

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    _isEnterClick = YES;
    switch (alertView.tag) {
            
        case 2222:
        {
            
            if (buttonIndex==0) {
                //取消，不付费
                [self allcodeRateRequest:alertView.URLStr];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
                
            }else{
                NSLog(@"点击的是取消");
                
                
                [_moviePlayer pause];
                
//                _isTapbtn_play = NO;
                
                _playBackgroundIV.hidden = NO;
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
                return;
            }
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
            
            break;
        case 333333:
        {//这个是 是否选择付费
            
            if (buttonIndex==0) {
                //取消，不付费
                
                [self allcodeRateRequest:alertView.URLStr];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }else{
                NSLog(@"点击的是取消");
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                //精简

                [self clearMoviePlayerStop];
                
//                _isTapbtn_play = NO;
                
                _playBackgroundIV.hidden = NO;
                return;
            }
            
            
            
        }
            
            break;
            
        default:
            break;
    }
    
}


#pragma mark --
#pragma mark 清晰度按钮的字体

-(void)allCodeClearButtonTitle
{
    if (clearTitles.count >0) {
        
        _clearButton.hidden = _isVerticalDirection?YES:NO;
        
        for (int i = 0; i<clearTitles.count; i++) {
            
            if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"]== [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] integerValue]) {
                
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SUPER_CLEAR) {
                    PlayTVType = 1;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == HIGH_DEFINE) {
                    PlayTVType = 2;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == ORIGINAL_PAINT) {
                    PlayTVType = 3;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SMOOTH) {
                    PlayTVType = 4;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                
                [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                break;
                
            }
            else
            {
                
                if (i==clearTitles.count-1) {
                    [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
                    [[NSUserDefaults standardUserDefaults] setInteger:SMOOTH forKey:@"BtnTag"];
                    PlayTVType = 4;
                    [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
                
                continue;
            }
        }
    }else
    {
        _clearButton.hidden = YES;
    }
}

#pragma mark --
#pragma mark 自动隐藏





- (void) autoHiddenPanel:(NSTimer *)sender
{
    if (self.autoHiddenSecend == 0) {
        [self hiddenPanlwithBool:YES];
    } else {
        self.autoHiddenSecend --;
    }
}





-(void)orientationChanged:(NSNotification *)notification
{
    NSInteger orientation = [[UIDevice currentDevice] orientation];
    
    NSString *str;
    switch (orientation) {
            
        case 1://home bottom
        {
            
            _isVerticalDirection = YES;
            
        }
            
            break;
        case 3://home right
        {
            str=@"right";
            _isVerticalDirection = NO;
            
        }
            
            break;
        case 4://home left
        {
            str=@"left";
            _isVerticalDirection = NO;
            
            // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
            
            //[self makeRotationWithAngel:0];
            
        }
            
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"orientChange" object:str];
    
}

#pragma mark -收藏按钮 添加文字一起改动
-(void)KFavorateBtnisFaved:(BOOL)faved{
    if (faved) {
        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
//        [_favourLabel setTitle:@"已添加" forState:UIControlStateNormal];
    }else{
        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//        [_favourLabel setTitle:@"添加" forState:UIControlStateNormal];
    }

}

#pragma mark -

#pragma mark ======   MPMoviePlayerPlaybackStateDidChangeNotification ====



// 从后台进入前台后，恢复视频的状态
- (void)replayTheVideo
{
    if (_isPaused) {
        [_moviePlayer pause];
    }else{
        [_moviePlayer play];
    }
}

#pragma 停掉播放器，清空播放器

-(void)clearMoviePlayerStop
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                   name:MPMoviePlayerPlaybackDidFinishNotification
    //                                                 object:nil];
    //
    [_adPlayerView.CountTimer invalidate];
    _adPlayerView.CountTimer = nil;
    [_moviePlayer stop];
    [_moviePlayer setContentURL:nil];
    [self.myNewTimer invalidate];
    _playPauseBigBtn.hidden = YES;
    
    
    
}



-(void)actionPauseForVideo
{
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
        _playPauseBigBtn.hidden = NO;
        _adPlayerView.isCanContinu = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
        //        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        _adPlayerView.isCanContinu = NO;
        
        [_moviePlayer pause];//暂停视频
        
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
        //        self.isPlay = NO;//改变暂停标记
        
        //        _playPauseBigBtn.hidden = NO;
    }

}


-(void)actionPlayForVideo
{
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
        _adPlayerView.isCanContinu = NO;
        
        _playPauseBigBtn.hidden = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
        //        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        if (_isADDetailEnter == NO) {
            _adPlayerView.isCanContinu = YES;
            
            //        _isPaused = YES;//改变暂停标记
            [_moviePlayer play];
        }
        
        //        _adPlayerView.isCanContinu = YES;
        
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
        //        _isPaused = YES;//改变暂停标记
        //        [_moviePlayer play];//暂停视频
                _playPauseBigBtn.hidden = YES;
    }
    
 
}






-(void)WillResignActive:(NSNotification*)notification
{
    
    
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
        _playPauseBigBtn.hidden = NO;
        _adPlayerView.isCanContinu = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
        //        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        _adPlayerView.isCanContinu = NO;

        [_moviePlayer pause];//暂停视频
        
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
        //        self.isPlay = NO;//改变暂停标记
        
        //        _playPauseBigBtn.hidden = NO;
    }
    
    
    
    
}


-(void)DidBecomeActive:(NSNotification*)notification
{
    if (IsLogin) {
        if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
            self.isPlay = NO;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
            _adPlayerView.isCanContinu = NO;
            
            _playPauseBigBtn.hidden = NO;
            [_moviePlayer pause];
            [self.myNewTimer invalidate];
            //        324 俩个的播放按钮都要变化 smallBottomBarIV
            
        }else
        {
            if (_isADDetailEnter == NO) {
                _adPlayerView.isCanContinu = YES;
                
                //        _isPaused = YES;//改变暂停标记
                [_moviePlayer play];
            }
            
            //        _adPlayerView.isCanContinu = YES;
            
            [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
            //        _isPaused = YES;//改变暂停标记
            //        [_moviePlayer play];//暂停视频
            //        _playPauseBigBtn.hidden = NO;
        }
        
 
    }else
    {
        return;
    }
    
    
    
    
}









#pragma mark ======   MPMediaPlaybackIsPreparedToPlayDidChangeNotification ====
- (void) mediaIsPreparedToPlayDidChange:(NSNotification*)notification
{
    NSLog(@"Now Begin to play!");
    //    [((UIActivityIndicatorView *)[self.view viewWithTag:5000]) stopAnimating];
    //    [[self.view viewWithTag:5050] setHidden:YES];
//    [[self.view viewWithTag:4040] setHidden:YES];
//    _adPlayerView.isCanContinu = YES;

    
    self.isPlay = YES;
//    _adPlayerView.timeLabel.text = @"";

    if (_isHaveAD == YES && _adPlayerView.maskViewType == VideoAD) {
        
        
        if ([[[_adPlayerView.timeLabel.text componentsSeparatedByString:@" "] lastObject] integerValue]<=0) {
            //            if (self.adImageHiddenSecend) {
            //                self.adImageHiddenSecend = 0;
            //                _adPlayerView.timeLabel.text = @"";
            //            }
            _adPlayerView.timeLabel.text = @"";
            _adVideoHiddenSecend = (NSInteger)_moviePlayer.duration;
            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
        }else
        {
            
            return;
        }
        
        
        
        
    }else if (_isHaveAD == YES && _adPlayerView.maskViewType == ImageAD) {
         if ([[[_adPlayerView.timeLabel.text componentsSeparatedByString:@" "] lastObject] integerValue]<=0)
         {
//             _isLiveHasAD = NO;
         }else
         {
             return;
         }
    }
    else
    {
    
    
    
    if (_isPrepareToPlay) {
        
        if (((NSInteger)_moviePlayer.duration - JumpStartTime )<=10) {
            JumpStartTime = 0;
        }
        
        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
//        [self StartNewTimer];

//        OnMove = NO;
//        [self ChangeCurTimeForNewPort];
        _isPrepareToPlay = NO;
    }else
    {
        _isPrepareToPlay = YES;
        //        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
        
    }
    
    
       [self StartNewTimer];

    }
}

#pragma mark ======   MPMoviePlayerLoadStateDidChangeNotification ====
- (void) loadStateDidChange:(NSNotification*)notification
{
    MPMoviePlayerController *player = notification.object;
    MPMovieLoadState loadState = player.loadState;
    
    /* The load state is not known at this time. */
    if (loadState & MPMovieLoadStateUnknown)
    {
        NSLog(@"MovieLoadStateUnknown");
        [[self.view viewWithTag:4040] setHidden:NO];
        _polyLoadingView.hidden = YES;
        _errorManager.isAVLoading = YES;
//        _adPlayerView.isCanContinu = NO;


    }
    /* The buffer has enough data that playback can begin, but it
     may run out of data before playback finishes. */
    if (loadState & MPMovieLoadStatePlayable)
    {
        NSLog(@"MovieLoadStatePlayable");
        _polyLoadingView.hidden = YES;

        _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
        
        [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_serviceName andOPSCode:PlayerOutTimeError andOPSST:[SystermTimeControl getNowServiceTimeString]];
        if (_isHaveAD == YES && _adPlayerView.maskViewType == VideoAD) {
            
            
            //            if (self.adImageHiddenSecend) {
            //                self.adImageHiddenSecend = 0;
            //                _adPlayerView.timeLabel.text = @"";
            //            }
            _adPlayerView.timeLabel.text = @"";
            _adVideoHiddenSecend = (NSInteger)_moviePlayer.duration;
            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
            
            
            
            
            
        }
        //        [((UIActivityIndicatorView *)[self.view viewWithTag:5000]) startAnimating];
        //        [[self.view viewWithTag:5050] setHidden:YES];
        [[self.view viewWithTag:4040] setHidden:NO];
//        _adPlayerView.isCanContinu = NO;
        _errorManager.isAVLoading = YES;
    }
    
    /* Enough data has been buffered for playback to continue uninterrupted. */
    if (loadState & MPMovieLoadStatePlaythroughOK)
    {
        NSLog(@"Movie is start!/resumed!");
        //        [((UIActivityIndicatorView *)[self.view viewWithTag:5000]) stopAnimating];
        _errorMaskView.hidden = YES;
        [[self.view viewWithTag:4040] setHidden:YES];
        _polyLoadingView.hidden = YES;
        _errorManager.isAVLoading = NO;
//        _adPlayerView.isCanContinu = YES;

    }
    
    /* The buffering of data has stalled. */
    if (loadState & MPMovieLoadStateStalled)
    {
        
        NSLog(@"Movie is Stalled!");
        //        [((UIActivityIndicatorView *)[self.view viewWithTag:5000]) startAnimating];
        //        [[self.view viewWithTag:5050] setHidden:YES];
        if (self.myNewTimer) {
            [self.myNewTimer invalidate];
        }
        [[self.view viewWithTag:4040] setHidden:NO];
        _polyLoadingView.hidden = YES;
        _errorManager.isAVLoading = YES;
//        _adPlayerView.isCanContinu = NO;

    }
    
}

-(NSInteger)secondsFrom:(NSString*)timeLabelText
{
    NSInteger currentTimeSeconds;
    NSArray *timeARR;
    
    if (timeLabelText.length >0) {
        timeARR = [timeLabelText  componentsSeparatedByString:@":"];
        
        if (timeARR.count>0) {
            currentTimeSeconds = [[timeARR objectAtIndex:0] integerValue] * 3600+[[timeARR objectAtIndex:1] integerValue] * 60 + [[timeARR objectAtIndex:2] integerValue];
        }
    }
    
    return currentTimeSeconds;
    
}


#pragma mark ======   MPMoviePlayerNowPlayingMovieDidChangeNotification ====
- (void) MPMoviePlayerPlayingNotification:(NSObject *)object
{
    NSLog(@"MovieNotificationCenter === %@",@"hello Movie");
}
/*  Notification called when the movie finished playing. */

NSMutableArray *current;
NSMutableArray *total ;
NSInteger kcurrentTime;
NSInteger ktotalTime;
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    NSLog(@"Moviel===DidFinish");
    
    NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if (_currentTimeL.text.length <=0) {
        _currentTimeL.text = @"00:00:00";
    }
    if (_totalTimeL.text.length <=0) {
        _totalTimeL.text = @"00:00:00";
    }
    current = [NSMutableArray arrayWithArray:[_currentTimeL.text  componentsSeparatedByString:@":"]];
    if (current.count>0) {
        
        if ([[current objectAtIndex:0] length]>0 && [[current objectAtIndex:1] length]>0 && [[current objectAtIndex:2] length]>0) {
            kcurrentTime = [[current objectAtIndex:0] integerValue] * 3600+[[current objectAtIndex:1] integerValue] * 60 + [[current objectAtIndex:2] integerValue];
        }
    }
    total = [NSMutableArray arrayWithArray:[_totalTimeL.text componentsSeparatedByString:@":"]];
    
    if (total.count>0) {
        if ([[total objectAtIndex:0] length]>0 && [[total objectAtIndex:1] length]>0 && [[total objectAtIndex:2] length]>0)
        {
            ktotalTime = [[total objectAtIndex:0] integerValue] * 3600+[[total objectAtIndex:1] integerValue] * 60 + [[total objectAtIndex:2] integerValue];
        }
    }
    
    switch ([reason integerValue])
    {
            /* The end of the movie was reached. */
        case MPMovieFinishReasonPlaybackEnded:
            /*
             Add your code here to handle MPMovieFinishReasonPlaybackEnded.
             */
        {
            
            if (_isHaveAD == YES) {
                
                return;
            }else
            {
                if ([dateLiveRight timeIntervalSinceDate:dateLiveMiddle]<=0) {
                    return;
                }else
                {
            
            
            if (kcurrentTime != ktotalTime) {
                if (_moviePlayer.loadState == 0) {
                    [_moviePlayer prepareToPlay];
                    [_moviePlayer play];
                    _isPlayEnd = NO;

                    
                    return;
                    //                    _isPlayEnd = NO;
                }else{
                    _isPlayEnd = YES;
                    [_moviePlayer setCurrentPlaybackTime:ktotalTime];
                }
            }else
            {
                if (kcurrentTime == 0 && ktotalTime == 0) {
                    _isPlayEnd = NO;
                }
                

            }
                }
            }
            
        }
            
            break;
            /* An error was encountered during playback. */
        case MPMovieFinishReasonPlaybackError:
            NSLog(@"An error was encountered during playback");
            _isPlayEnd = NO;
            if (_timeEPGManager.counting) {
                [_timeEPGManager pause];
            }
            _errorMaskView.hidden = NO;
            if (_currentPlayType == CurrentPlayEvent) {
                if (IsLogin) {
                    [self recordPlaybackTime];
                }
            }
            
            
            
//            if (kcurrentTime != ktotalTime) {
//                //                [self performSelectorOnMainThread:@selector(displayError:) withObject:[[notification userInfo] objectForKey:@"error"] waitUntilDone:NO];
//            }
            
            break;
            
            /* The user stopped playback. */
        case MPMovieFinishReasonUserExited:
            break;
            
        default:
            break;
    }
    
    //
    //    [UIView animateWithDuration:1
    //                          delay: 0.0
    //                        options: UIViewAnimationOptionCurveEaseIn
    //                     animations:^{
    //                         // one second to fade out the view
    ////                         _moviePlayer.view.alpha = 0.0;
    //                     }
    //                     completion:^(BOOL finished){
    //                         _moviePlayer = nil;
    ////                         [self initanothermovieplayerandplay];
    //                     }];
}

- (void) moviePlayBackStateDidChange:(NSNotification*)notification
{
    
    MPMoviePlayerController *player = notification.object;
    
    /* Playback is currently stopped. */
    if (player.playbackState == MPMoviePlaybackStateStopped)
    {
//        [[self.view viewWithTag:4040] setHidden:NO];
        
        NSLog(@"stopped");
        //        [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self.view withLocationTag:1];
        //        self.isPlay = NO;
        //        [_moviePlayer stop];
        //        [_moviePlayer setContentURL:nil];
        [self clearMoviePlayerStop];
        //        [AlertLabel AlertInfoWithText:@"该视频暂无、、、、法播放" andWith:self.view withLocationTag:1];
        
    }
    /*  Playback is currently under way. */
    else if (player.playbackState == MPMoviePlaybackStatePlaying)
    {
        if (_isHaveAD == YES) {
            _adPlayerView.isCanContinu = YES;

            //            _adPlayerView.isCanContinu = YES;
            //            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            //            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
            return;
        }else
        {
//        [[self.view viewWithTag:4040] setHidden:YES];

        NSLog(@"playing");
            
        [self StartNewTimer];
        self.isPlay = YES;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_pressed"] forState:UIControlStateHighlighted];
        _isPaused = NO;
        endDateLive = [SystermTimeControl GetSystemTimeWith];
        
        firstPeriodLive = [DTTimePeriod timePeriodWithStartDate:startDateLive endDate:endDateLive];
        
        ZTTimeIntervalLive +=firstPeriodLive.durationInSeconds;
            
            
            [_timeEPGManager start];
            _isCollectionInfo = YES;

            
        }
        
        [self hiddenPanlwithBool:NO];
    }
    /* Playback is currently paused. */
    else if (player.playbackState == MPMoviePlaybackStatePaused)
    {
        
        
        // 创建时间段
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat: @"YYYY-MM-dd HH:mm:ss"];
        
        NSLog(@"paused");
        
        if (_isHaveAD == YES) {
            
            //            _adPlayerView.isCanContinu = NO;
            //            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            //            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
            
            return;
        }else
        {
        if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
            self.isPlay = NO;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_pressed"] forState:UIControlStateHighlighted];
            _playPauseBigBtn.hidden = NO;
            //324 俩个的播放按钮都要变化 smallBottomBarIV
            startDateLive = [SystermTimeControl GetSystemTimeWith];
            
            if (_timeEPGManager.counting) {
                [_timeEPGManager pause];
            }
            
        }
        else{
            NSLog(@"kjkljkj%ld,=====%ld",kcurrentTime,ktotalTime);
            //            [[self.view viewWithTag:4040] setHidden:NO];
            if (kcurrentTime != ktotalTime) {
                if (_moviePlayer.loadState == 0  ) {
                    [_moviePlayer play];
                    _isPlayEnd = NO;
                    
                }else if (_moviePlayer.loadState == 3)
                {
                    [_moviePlayer pause];
                    _isPlayEnd = NO;
                    
                }else{
                    _isPlayEnd = YES;
                    [_moviePlayer setCurrentPlaybackTime:ktotalTime];
                }
            }
            else
            {
//                if (kcurrentTime == 0&& ktotalTime == 0) {
//                    _isPlayEnd = NO;
//                }
                
                NSLog(@"*****UUUUUU%ld",_moviePlayer.loadState)  ;
                
                
                //                if (_moviePlayer.loadState == 3) {
                //
                //                    _isPlayEnd = NO;
                //                    _isPaused = NO;
                //
                //
                //                    self.playPauseBigBtn.hidden = YES;
                //                    [_moviePlayer pause];
                //                }else
                //                {
                ////                    _isPlayEnd = YES;
                ////
                ////                    [_moviePlayer play];
                ////                    self.playPauseBigBtn.hidden = YES;
                //
                //                }
                
                
                //                [[self.view viewWithTag:4040] setHidden:NO];
            }
//#warning 这里要检查下，那中自动暂停发生时候，播放器到底是不是暂停的，如果是那么play，如果不是那么不需要play
            //            [_moviePlayer play];
             [self StartNewTimer];
        }
            
           
    }
        
    }
    /* Playback is temporarily interrupted, perhaps because the buffer
     ran out of content. */
    else if (player.playbackState == MPMoviePlaybackStateInterrupted)
    {
        [[self.view viewWithTag:4040] setHidden:NO];
        
        NSLog(@"interrupted");
        _adPlayerView.hidden = YES;
        // 当网络状态不好，请求不到视频切片时
        [AlertLabel AlertInfoWithText:@"网络状况不好，切换清晰度试试！" andWith:_contentView withLocationTag:2];
        [_moviePlayer prepareToPlay];
        
    }
}


-(void)remoteControlViewSelectedWith:(NSString *)selectedBtn
{
    if ([selectedBtn isEqualToString:@"hiden"]) {
        
        remote.hidden = YES;
        isremote = NO;
        
    }
    
    
    
}

#pragma mark - IP取消按钮

-(void)ipButtonCancelClick:(UIButton*)sender
{
    _IPBackIV.hidden = YES;
}


#pragma mark - 本地预约通知

-(void)receiveLocationOrder:(NSNotification*)notifi
{
    
    
    if (USER_ACTION_COLLECTION == YES) {
        
        if (_isCollectionInfo) {
            
            if (_currentPlayType == CurrentPlayEvent) {
            NSDictionary *eventDic = [NSDictionary dictionaryWithObjectsAndKeys:_theVideoInfo.videoName,@"eventVideoName",[self BackLookChannelEventID],@"eventEventID",_serviceID,@"eventServiceID",_theVideoInfo.channelName,@"eventServiceName",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"eventEventLength",[self eventTime],@"eventEventTime",_EpgCollectionDuration,@"eventWatchDuration",[SystermTimeControl getNowServiceTimeString],@"eventWatchTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"eventTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"eventTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"eventEN", nil];
                
                EPGCollectionModel *eventModel = [EPGCollectionModel eventCollectionWithDict:eventDic];
                
                [SAIInformationManager epgBackEventInformation:eventModel];
            }
            if (_currentPlayType == CurrentPlayLive) {
                NSDictionary *liveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"liveServiceID",_serviceName,@"liveServiceName",_theVideoInfo.videoName,@"liveViedoName",[SystermTimeControl getNowServiceTimeString],@"liveWatchTime",_EpgCollectionDuration,@"liveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"liveEventLength",[self eventTimePlayTime],@"liveLiveTime",isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"liveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"liveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"liveEN", nil];
                
                EPGCollectionModel *liveModel = [EPGCollectionModel liveCollectionWithDict:liveDic];
                
                [SAIInformationManager epgLiveInformation:liveModel];
            }
            
            if (_currentPlayType == CurrentPlayTimeMove) {
                NSDictionary *timeMoveDic = [NSDictionary dictionaryWithObjectsAndKeys:_serviceID,@"timeMoveServiceID",_serviceName,@"timeMoveServiceName",[self timeMoveCurrentEventName],@"timeMoveViedoName",[SystermTimeControl getNowServiceTimeString],@"timeMoveWatchTime",_EpgCollectionDuration,@"timeMoveWatchDuration",[self EventLength:[self eventON] andeventOFF:[self eventOFF]],@"timeMoveEventLength", isEmptyStringOrNilOrNull(_TRACKID)?@"": _TRACKID,@"timeMoveTRACKID",isEmptyStringOrNilOrNull(_TRACKNAME)?@"":_TRACKNAME,@"timeMoveTRACKNAME",isEmptyStringOrNilOrNull(_EN)?@"9":_EN,@"timeMoveEN",isEmptyStringOrNilOrNull(_dragLabel.text)?@"":_dragLabel.text,@"timeMoveTWT",nil];
                
                EPGCollectionModel *timeMoveModel = [EPGCollectionModel timeMoveCollectionWithDict:timeMoveDic];
                
                [SAIInformationManager epgTimeMoveInformation:timeMoveModel];
                
            }
            [_timeEPGManager reset];
            _EpgCollectionDuration = nil;
            _TRACKID = nil;
            _TRACKNAME = nil;
            _EN = nil;
        }
    }

    //    NSMutableDictionary *notiItem = [[NSMutableDictionary alloc] initWithDictionary:notifi.object];
    NSLog(@"11111 :%@ 22222:%@",notifi.userInfo,notifi.object);
    
    
    /*
     
     {
     endTime = "2015-11-06 22:21:00";
     eventID = 842533;
     eventName = "\U5e7f\U5ba3\U65f6\U6bb5";
     imageLink = "http://192.168.3.60:8080/taibiao/phone/s/btv.png";
     levelImageLink = "http://192.168.3.60:8080/taibiao/phone/l/21.png";
     serviceID = 21;
     serviceName = "BTV\U5317\U4eac\U536b\U89c6";
     startTime = "2015-11-06 22:17:00";
     }
     
     [[[_theVideoInfo.videoTime componentsSeparatedByString:@" "] objectAtIndex:0] isEqualToString:[[[self getTodayStringWithDate] componentsSeparatedByString:@" "] objectAtIndex:0]]
     
     */
    
    //    _serviceID = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"serviceID"]];
    
    if (self.changeVideoNameTimer) {
        [self.changeVideoNameTimer invalidate];
        self.changeVideoNameTimer = nil;
    }
    
    if (_eventURLID) {
        _eventURLID = nil;
    }
    
    _eventURLID = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"eventID"]];
    _serviceName = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"serviceName"]];
    _currentServiceEventStr = [NSString stringWithFormat:@"%@:%@",isEmptyStringOrNilOrNull(_serviceName)?@"":_serviceName,[notifi.userInfo valueForKey:@"eventName"]];

    NSString *MtimeDateString = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"startTime"]];
    
    if ([MtimeDateString componentsSeparatedByString:@" "].count>0) {
        
        if (_currentDateStr) {
            _currentDateStr = nil;
        }
        _currentDateStr =   [[MtimeDateString componentsSeparatedByString:@" "] objectAtIndex:0];
    }
    
    
    _isLocationOrder = YES;
    
    //    [request EPGPFWithServiceID:_serviceID andType:-1 andCount:2 andTimeout:kTIME_TEN];
    
    
    
    //    [_moviePlayer pause];
    
    [self LocationOrderController:[NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"serviceID"]]];
    
    
    //    _IsFirstChannelist = YES;
    
    
    
    
    
    
    
}




-(void)LocationOrderController:(NSString *)localServiceID
{
    
    //    [self clearMoviePlayerStop];
    
    NSMutableArray *currentArr = [[NSMutableArray alloc]init];
    
    currentArr = [[NSUserDefaults standardUserDefaults] objectForKey:EPG_CHANNELLIST];
    if (currentArr.count>0) {
        
    _isLookPlayBackEvent = [self checkPlayBackForCurrentListForServiceID:localServiceID andCurrentList:currentArr];
    }
    _serviceID = localServiceID;
    
//    [_moviePlayer pause];
    [self clearMoviePlayerStop];
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    if (ISIntall) {
        _installMaskView.hidden = NO;
    }else
    {
//    [request TVPlayWithServiceID:_serviceID andTimeout:YES];
        [request TVPlayWithServiceID:_serviceID andServiceName:_serviceName andTimeout:KTimeout];

    }
    _IsFirstChannelist = NO;
    
    [self StartNewTimer];
    
}






#pragma mark - epgDateVIewdelegate

-(void)epgDateSelectedViewAtIndex:(EPGVideoDateBtn *)btn{
    
    NSLog(@"你所点击的按钮 %@",btn.dateBOStr);
    if (USER_ACTION_COLLECTION == YES) {
        
        
        //采集   点击更换时间，进行计算浏览的时长以及浏览的用户信息的存储
        self.skimDuration = [self EPGtimeLengthForCurrent];
        //采集  存储EPG
        [SAIInformationManager EPGinformationForChanelFenlei:_serviceID andChanelName:_serviceName andChanelInterval:_epgDateStr andSkimDuration:self.skimDuration andSkimTime:self.nowDateString];
        
        //over
        
    }
    
   
    
    NSString *DATEStr = [NSString stringWithFormat:@"%@",btn.dateBOStr];
    _currentDateStr = DATEStr;
    
    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
    [request EPGSHOWWithServiceID:_serviceID andDate:DATEStr andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
    
    _channelSelectIndex = -1;
    
    _epgDateStr =[[NSString alloc] initWithFormat:@"%@ 00:00~%@ 23:59",_currentDateStr,_currentDateStr];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EPGTableVIewAndChannelTableView) name:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil];
    [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
        
        [_EPGListTableView reloadData];
        [_channelListTV reloadData];
    }];
}


-(void)EPGDateBtnSelectedAtIndex:(UIButton *)Datebtn
{
    
}


-(void)EPGListCellPlayBtn:(UIButton *)btn
{
    
}





-(void)EPGTableVIewAndChannelTableView
{
    [_EPGListTableView reloadData];
    [_channelListTV reloadData];
    
}


-(void)showSearchDeviceLoadingView{
    
    MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:_contentView animated:YES];
    HUD.customView.backgroundColor = [UIColor whiteColor];
    HUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.animationType = MBProgressHUDAnimationFade;
    HUD.delegate = self;
    HUD.color = [UIColor clearColor];//这儿表示无背景
    HUD.labelText = @"正在搜索设备...";
    
    [HUD show:YES];
    
    [HUD hide:YES afterDelay:3];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //这里是1.0f后 你要做的事情
        [self reloadIPTableView];
    });
    
    
}


-(void)reloadIPTableView
{
    
    if (_IPArray.count<=0) {
        
        [AlertLabel AlertInfoWithText:@"暂无可连接设备！" andWith:_contentView withLocationTag:2];
//        _IPTableView.hidden = YES;
        _IPBackIV.hidden = YES;

        _MTButton.hidden = YES;
        _TVButton.hidden = YES;
        _RemoteButton.hidden = YES;
        
    }else{
        _IPTableView.hidden = NO;
        _IPBackIV.hidden = NO;
//***********************************************************************#warning 没有连接，所以不显示-----------------------------------------------------
//        _MTButton.hidden = NO;
//        _TVButton.hidden = NO;
//        _RemoteButton.hidden = NO;
        [_IPTableView reloadData];
        NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (pointToPointIP.length>0) {
            _MTButton.hidden = NO;
            _TVButton.hidden = NO;
            _RemoteButton.hidden = NO;
        }

    }
    
    
}


#pragma mark timeManager
-(void)countingTo:(NSTimeInterval)time timeToShowString:(NSString*)timeToShowString
{
    _EpgCollectionDuration = [NSString stringWithFormat:@"%.0f",time];
}



@end
