//
//  LiveHomeTableViewCell.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/23.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "LiveHomeTableViewCell.h"
#import "Masonry.h"
#import "UIButton_Block.h"

@interface LiveHomeTableViewCell()
{
    UIImageView *_taiBiaoView;//台标
    UILabel *_Titlelabel;//台名
    
    UILabel *_currentLabel;//当前节目
    UILabel *_nextLabel;//后续节目
    
    UILabel *_currentTimeLabel;//当前节目时间
    UILabel *_nextTimeLabel;//后续节目时间
    
    UIButton_Block * _SeeBtn;///观看按钮
    UIImageView *_seeView;//观看按钮
    
    UIView *_BaseView;
    UIView *_taiBiaoBase;
    
}

@end

@implementation LiveHomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
    }
    return self;
}
- (void)setUpView
{
    

    
    _BaseView = [self contentView];
    [_BaseView removeAllSubviews];
    
    

    _taiBiaoBase = [UIView new];
    [_BaseView addSubview:_taiBiaoBase];
    [_taiBiaoBase mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BaseView.mas_left);
        make.width.equalTo(@(96*kDeviceRate));
        make.top.equalTo(_BaseView.mas_top);
        make.bottom.equalTo(_BaseView.mas_bottom).offset(-1*kDeviceRate);
    }];
   
    
    _taiBiaoView = [UIImageView new];
    [_taiBiaoBase addSubview:_taiBiaoView];
    [_taiBiaoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BaseView.mas_left).offset(28*kDeviceRate);
        make.top.equalTo(_BaseView.mas_top).offset(8*kDeviceRate);
        make.right.equalTo(_taiBiaoBase.mas_right).offset(-28*kDeviceRate);
        make.height.equalTo(@(35*kDeviceRate));
    }];
    
    _taiBiaoView.contentMode = UIViewContentModeScaleAspectFit;
    
//    _taiBiaoView.backgroundColor = [UIColor redColor];
    
    _Titlelabel = [UILabel new];
    _Titlelabel.numberOfLines= 2;
    _Titlelabel.textAlignment = NSTextAlignmentCenter;
    [_taiBiaoBase addSubview:_Titlelabel];
    [_Titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_taiBiaoBase.mas_left).offset(5*kDeviceRate);
        make.right.equalTo(_taiBiaoBase.mas_right).offset(-5*kDeviceRate);
        make.height.equalTo(@(12*kDeviceRate));
        make.top.equalTo(_taiBiaoView.mas_bottom).offset(5*kDeviceRate);
    }];
    _Titlelabel.font = HT_FONT_FOURTH;
    _Titlelabel.numberOfLines = 1;
    
//    //台名下的横线
//    UIView *lineHorizontal = [UIView new];
//    [taiBiaoBase addSubview:lineHorizontal];
//    [lineHorizontal mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_Titlelabel.mas_left);
//        make.right.equalTo(_Titlelabel.mas_right).offset(-6*kRateSize);
//        make.top.equalTo(_Titlelabel.mas_bottom).offset(-2*kRateSize);
//        make.height.equalTo(@(1));
//    }];
//    lineHorizontal.backgroundColor = UIColorFromRGB((0x999999));
    
    
    //台名右侧的横线
//    UIView *rightlineHorizontal = [UIView new];
//    [BaseView addSubview:rightlineHorizontal];
//    [rightlineHorizontal mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(taiBiaoBase.mas_right).offset(10*kRateSize);
//        make.width.equalTo(@(1));
//        make.top.equalTo(taiBiaoBase.mas_top).offset(8*kRateSize);
//        make.bottom.equalTo(taiBiaoBase.mas_bottom).offset(-8*kRateSize);
//    }];
//    rightlineHorizontal.backgroundColor = UIColorFromRGB((0x999999));
    
    //台名右侧右侧的横线
    UIView *RightrightlineHorizontal = [UIView new];
    [_BaseView addSubview:RightrightlineHorizontal];
    [RightrightlineHorizontal mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(taiBiaoBase.mas_right).offset(18*kRateSize);
//        make.height.equalTo(@(1));
//        make.right.equalTo(BaseView.mas_right).offset(0*kRateSize);
//        make.bottom.equalTo(BaseView.mas_bottom).offset(-0*kRateSize);
        make.left.equalTo(_BaseView.mas_left).offset(0*kDeviceRate);
        make.height.equalTo(@(1));
        make.right.equalTo(_BaseView.mas_right).offset(0*kDeviceRate);
        make.bottom.equalTo(_BaseView.mas_bottom).offset(0*kDeviceRate);
    }];
    RightrightlineHorizontal.backgroundColor = UIColorFromRGB((0xeeeeee));
    
    
    _currentTimeLabel = [UILabel new];
    [_BaseView addSubview:_currentTimeLabel];
//    [_currentTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
//        make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
//        make.height.equalTo(@(15*kDeviceRate));
//        make.width.equalTo(@(44*kDeviceRate));
//    }];
    [_currentTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_taiBiaoBase.mas_right).offset(10*kDeviceRate);
        make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
        make.height.equalTo(@(15*kDeviceRate));
        make.width.equalTo(@(46*kDeviceRate));
    }];
    
    
    _nextTimeLabel = [UILabel new];
    [_BaseView addSubview:_nextTimeLabel];
//    [_nextTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
//        make.top.equalTo(_currentTimeLabel.mas_bottom).offset(10*kDeviceRate);
//        make.height.equalTo(@(14*kDeviceRate));
//        make.width.equalTo(@(44*kDeviceRate));
//    }];
    [_nextTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_taiBiaoBase.mas_right).offset(10*kDeviceRate);
        make.top.equalTo(_currentTimeLabel.mas_bottom).offset(10*kDeviceRate);
        make.height.equalTo(@(14*kDeviceRate));
        make.width.equalTo(@(46*kDeviceRate));
    }];
    
    _currentLabel = [UILabel new];
    [_BaseView addSubview:_currentLabel];
    [_currentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_currentTimeLabel.mas_right).offset(28*kDeviceRate);
        make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
        make.height.equalTo(@(15*kDeviceRate));
        make.width.equalTo(@((kDeviceWidth-(100+100)*kDeviceRate)));
    }];
    
    _nextLabel = [UILabel new];
    [_BaseView addSubview:_nextLabel];
    [_nextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nextTimeLabel.mas_right).offset(28*kDeviceRate);
        make.top.equalTo(_currentLabel.mas_bottom).offset(10*kDeviceRate);
        make.height.equalTo(@(14*kDeviceRate));
        make.width.equalTo(@((kDeviceWidth-(100+100)*kDeviceRate)));

    }];
    _currentLabel.font = HT_FONT_SECOND;
    _nextLabel.font = HT_FONT_THIRD;
    _currentTimeLabel.font = HT_FONT_SECOND;
    _nextTimeLabel.font = HT_FONT_THIRD;
    
//    _currentLabel.textAlignment = NSTextAlignmentLeft;
//    _nextLabel.textAlignment = NSTextAlignmentLeft;
    
//    BaseView.backgroundColor = [UIColor lightGrayColor];
    
    {
//    _seeView = [UIImageView new];
//    [BaseView addSubview:_seeView];
//    [_seeView mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.top.equalTo(BaseView.mas_top).offset(18*kRateSize);
//        make.centerY.equalTo(BaseView.mas_centerY);
//        make.right.equalTo(BaseView.mas_right).offset(-15*kRateSize);
//        make.width.equalTo(@(45*kRateSize));
//        make.height.equalTo(@(24*kRateSize));
//    }];
////    _seeView.backgroundColor = [UIColor grayColor];
//    _seeView.image = [UIImage imageNamed:@"btn_guankan"];
//    _seeView.userInteractionEnabled = YES;
    }
    
    _SeeBtn = [UIButton_Block new];
    [_BaseView addSubview:_SeeBtn];
    [_SeeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(BaseView.mas_top).offset(18*kRateSize);
//        make.right.equalTo(BaseView.mas_right).offset(-15*kRateSize);
//        make.width.equalTo(@(45*kRateSize));
//        make.height.equalTo(@(24*kRateSize));
        make.edges.equalTo(_BaseView);
    }];
    [_SeeBtn setBackgroundImage:[UIImage imageNamed:@"bg_live_page_cell_press"] forState:UIControlStateHighlighted];
    
    _SeeBtn.alpha = 0.5;
    {
//    _SeeBtn.backgroundColor = [UIColor yellowColor];
    
//    UIView *lineHorizontal = [UIView new];
//    [_SeeBtn addSubview:lineHorizontal];
//    [lineHorizontal mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_SeeBtn.mas_left);
//        make.right.equalTo(_SeeBtn.mas_right);
//        make.top.equalTo(_SeeBtn.mas_bottom).offset(-2*kRateSize);
//        make.height.equalTo(@(3*kRateSize));
//    }];
//    lineHorizontal.backgroundColor = UIColorFromRGB((0xe2e2e2));//[UIColor redColor];//
    
//    taiBiaoBase.backgroundColor = [UIColor grayColor];
        
    }
    _BaseView.backgroundColor = HT_COLOR_FONT_INVERSE;
}

- (void)updateCellWithDiction:(NSDictionary *)diction
{
    
    WS(wself);
    
    _Titlelabel.text = @"暂无";
    _currentLabel.text = @"暂无节目信息";
    _nextLabel.text = @"暂无节目信息";
    _seeView.hidden = YES;
    NSString *url = [diction objectForKey:@"categoryLevelImage"];
    [_taiBiaoView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(url)] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
    _SeeBtn.Diction = diction;
    
    _SeeBtn.Click = ^(UIButton_Block*btn,NSString*name)
    {
        if (wself.Block) {
            wself.Block(btn);
        }else
        {
            NSLog(@"直播一级cell外没有实现这个block");
        }
    };
    
    _Titlelabel.text = [diction objectForKey:@"categoryServiceName"];
    
//    _taiBiaoView.image = [UIImage imageNamed:@"icon-touxiang"];
 
    NSArray *epginfo = [diction objectForKey:@"epginfo"];
    if (epginfo.count >= 2) {
        
//        [_currentTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
//            make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
//            make.height.equalTo(@(15*kDeviceRate));
//            make.width.equalTo(@(44*kDeviceRate));
//            
//        }];
//        
//        [_nextTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
//            make.top.equalTo(_currentTimeLabel.mas_bottom).offset(10*kDeviceRate);
//            make.height.equalTo(@(14*kDeviceRate));
//            make.width.equalTo(@(44*kDeviceRate));
//            
//        }];
        
        [_currentTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_taiBiaoBase.mas_right).offset(10*kDeviceRate);
            make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
            make.height.equalTo(@(15*kDeviceRate));
            make.width.equalTo(@(46*kDeviceRate));
            
        }];
        
        [_nextTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_taiBiaoBase.mas_right).offset(10*kDeviceRate);
            make.top.equalTo(_currentTimeLabel.mas_bottom).offset(10*kDeviceRate);
            make.height.equalTo(@(14*kDeviceRate));
            make.width.equalTo(@(46*kDeviceRate));
        }];
        
        NSDictionary *dic = epginfo[0];
        NSString *startTime = [dic objectForKey:@"startTime"];
        NSString *eventName = [dic objectForKey:@"eventName"];
        _currentLabel.text = [NSString stringWithFormat:@"%@",eventName];
        _currentTimeLabel.text = [NSString stringWithFormat:@"%@",startTime];
        
        _seeView.hidden = NO;
        NSDictionary *dic1 = epginfo[1];
        NSString *startTime1 = [dic1 objectForKey:@"startTime"];
        NSString *eventName1 = [dic1 objectForKey:@"eventName"];
        _nextLabel.text = [NSString stringWithFormat:@"%@",eventName1];
        _nextTimeLabel.text = [NSString stringWithFormat:@"%@",startTime1];
    }
    else
    {
        
        [_currentTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
            make.top.equalTo(_BaseView.mas_top).offset(16*kDeviceRate);
            make.height.equalTo(@(15*kDeviceRate));
            make.width.equalTo(@(0*kDeviceRate));
            
        }];
        
        [_nextTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_taiBiaoBase.mas_right).offset(12*kDeviceRate);
            make.top.equalTo(_currentTimeLabel.mas_bottom).offset(10*kDeviceRate);
            make.height.equalTo(@(14*kDeviceRate));
            make.width.equalTo(@(0*kDeviceRate));
            
        }];
    }

    
    {
//        _taiBiaoView.backgroundColor = [UIColor lightGrayColor];
        _nextLabel.textColor  = HT_COLOR_FONT_SECOND;
        _Titlelabel.textColor = HT_COLOR_FONT_FIRST;
        _currentLabel.textColor = HT_COLOR_FONT_FIRST;
        _nextTimeLabel.textColor  = HT_COLOR_FONT_SECOND;
        _currentTimeLabel.textColor = HT_COLOR_FONT_FIRST;
        
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
