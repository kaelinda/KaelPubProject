//
//  EPGListTableViewCell.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButton_Block.h"

@protocol EPGListCellDelegate <NSObject>

-(void)EPGListCellPlayBtn:(UIButton *)btn;

@end

@interface EPGListTableViewCell : UITableViewCell

@property (nonatomic,assign) id<EPGListCellDelegate>cellDelegate;

@property (nonatomic,strong) UILabel *startTime;//开始时间
@property (nonatomic,strong) UILabel *eventName;//节目名称
@property (nonatomic,strong) UIButton_Block *playBtn;//播放按钮



-(void)setbackImage;
-(void)setPlayingBackImage;

-(void)setLiveImage;
-(void)setLiveingImage;

-(void)setOrderImage;
-(void)setOrderedImage;
-(void)setSelectedContentViewBacogrundColor;
-(void)setUnselectedContentViewbackgrundColor;


@end
