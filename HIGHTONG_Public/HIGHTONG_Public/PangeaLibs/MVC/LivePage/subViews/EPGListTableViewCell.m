//
//  EPGListTableViewCell.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EPGListTableViewCell.h"

@implementation EPGListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubViews];

    }
    return self;
}
-(instancetype)init{
    self = [super init];
    if (self) {
//        NSInteger kkkk = ((KDeviceHeight>kDeviceWidth)?kDeviceWidth:KDeviceHeight);
    }

    return self;
}

-(void)setupSubViews{
    WS(wself);
    [self.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    
    _startTime = [[UILabel alloc] init];
    [_startTime setFont:[UIFont fontWithName:HEITI_Light_FONT size:14.0f]];
    [_startTime setTextColor:UIColorFromRGB(0x333333)];
    [_startTime setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_startTime];
    
    [_startTime mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(wself.contentView).offset(15*kRateSize);
        make.width.mas_equalTo(110*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);

    }];
    
    _eventName = [[UILabel alloc] init];
    [_eventName setFont:[UIFont fontWithName:HEITI_Light_FONT size:14.0f]];
    [_eventName setTextColor:UIColorFromRGB(0x333333)];
    [_eventName setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_eventName];
    
    [_eventName mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(wself.contentView).offset(65*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.width.mas_equalTo(190*kRateSize);
        
    }];

    
    _playBtn = [[UIButton_Block alloc] init];
    [_playBtn addTarget:self action:@selector(playBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_playBtn];
    CGSize btnSize = CGSizeMake(45*kRateSize, 24*kRateSize);
    [_playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.right.mas_equalTo(wself.contentView.mas_right).offset(-15*kRateSize);
        make.size.mas_equalTo(btnSize);
        
    }];
    
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:[UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1]];
    [self.contentView addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.top.mas_equalTo(wself.contentView.mas_bottom).offset(-1);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.right.mas_equalTo(wself.contentView.mas_right);
        make.height.mas_equalTo(1);
        
    }];
    
    
    

}


-(void)playBtnAction:(UIButton *)btn{
    if ([self.cellDelegate respondsToSelector:@selector(EPGListCellPlayBtn:)]) {
        [self.cellDelegate EPGListCellPlayBtn:btn];
    }

}


-(void)setbackImage{

    [self.playBtn setImage:[UIImage imageNamed:@"btn_see_37"] forState:UIControlStateNormal];

}
-(void)setPlayingBackImage{

    [self.playBtn setImage:[UIImage imageNamed:@"btn_see_338"] forState:UIControlStateNormal];
    
}

-(void)setLiveImage{

    [self.playBtn setImage:[UIImage imageNamed:@"btn_play_40"] forState:UIControlStateNormal];
}
-(void)setLiveingImage{

    [self.playBtn setImage:[UIImage imageNamed:@"btn_play_39"] forState:UIControlStateNormal];
}
-(void)setOrderImage{
    [self.playBtn setImage:[UIImage imageNamed:@"btn_booking_41"] forState:UIControlStateNormal];
    
}
-(void)setOrderedImage{

    [self.playBtn setImage:[UIImage imageNamed:@"btn_booking_42"] forState:UIControlStateNormal];
}

-(void)setSelectedContentViewBacogrundColor{

    [self.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [self.startTime setTextColor:App_selected_color];
    [self.eventName setTextColor:App_selected_color];
}
-(void)setUnselectedContentViewbackgrundColor{
    [self.contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
//    [self.startTime setTextColor:UIColorFromRGB(0x333333)];
//    [self.eventName setTextColor:UIColorFromRGB(0x333333)];
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
