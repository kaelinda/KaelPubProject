//
//  VideoListTableViewCell.m
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VideoListTableViewCell.h"

@implementation VideoListTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        WS(wsel);
        
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        
        //        UIImageView *channelLogo;
        //        UILabel *serialLabel;
        //        UILabel *channelNameLabel;
        //        UILabel *channelTitleLabel;
        _channelLogo = [[UIImageView alloc]init];
        _channelLogo.contentMode = UIViewContentModeScaleAspectFit;
        [_channelLogo setBackgroundColor:[UIColor clearColor]];
        
        
        
        //        _serialLabel = [[UILabel alloc]init];
        //
        //        _serialLabel.text = @"1211";
        //        _serialLabel.textColor = [UIColor whiteColor];
        //        _serialLabel.font = [UIFont systemFontOfSize:11];
        //        [_serialLabel setBackgroundColor:[UIColor clearColor]];
        
        
        
        _channelNameLabel = [[UILabel alloc]init];
//        _channelNameLabel.text = @"江苏卫视";
//        _channelNameLabel.textColor = [UIColor whiteColor];
        _channelNameLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [_channelNameLabel setBackgroundColor:[UIColor clearColor]];
        
        
        _channelTitleLabel = [[UILabel alloc]init];
//        _channelTitleLabel.text = @"非诚勿扰（9）";
//        _channelTitleLabel.textColor = [UIColor whiteColor];
        _channelTitleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [_channelTitleLabel setBackgroundColor:[UIColor clearColor]];
        
        _seperateLine = [[UIImageView alloc]init];
        [_seperateLine setImage:[UIImage imageNamed:@"ic_player_full_menuitem_seperate_line"]];
        _seperateLine.hidden = YES;
        
        
        [self.contentView addSubview:_channelLogo];
        [self.contentView addSubview:_serialLabel];
        [self.contentView addSubview:_channelNameLabel];
        [self.contentView addSubview:_channelTitleLabel];
        [self.contentView addSubview:_seperateLine];
        
        
        [_channelLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(wsel.mas_left).offset(-16*kRateSize);
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(10*kRateSize);
            make.width.mas_equalTo(86*kRateSize);
            make.height.mas_equalTo(32*kRateSize);
            
        }];
        
        
        
        [_veticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wsel.channelLogo.mas_right);
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(17*kRateSize);
            make.width.mas_equalTo(1*kRateSize);
            make.height.mas_equalTo(32*kRateSize);
            
        }];
        
        
        //        [_serialLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //
        //            make.left.mas_equalTo(wsel.contentView.mas_left).offset(97*BORateSize);
        //            make.top.mas_equalTo(wsel.contentView.mas_top).offset(8.5*BORateSize);
        //            make.height.mas_equalTo(10*BORateSize);
        //            make.width.mas_equalTo(35*BORateSize);
        //
        //        }];
        
        [_channelNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(_channelLogo.mas_right).offset(0*kRateSize);
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(6.5*kRateSize);
            make.width.mas_equalTo(162*kRateSize);
            make.height.mas_equalTo(15*kRateSize);
            
        }];
        
        [_channelTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(_channelLogo.mas_right).offset(0*kRateSize);
            make.top.mas_equalTo(_channelNameLabel.mas_bottom).offset(4*kRateSize);
            make.width.mas_equalTo(145*kRateSize);
            make.height.mas_equalTo(15*kRateSize);
            
            
        }];
        
        
        [_seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(wsel.contentView.mas_bottom).offset(2*kRateSize);
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(8.5);
            make.width.mas_equalTo(195*kRateSize);
            make.height.mas_equalTo(1*kRateSize);
            
            
        }];
        
        
        
        
        
        
        
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
