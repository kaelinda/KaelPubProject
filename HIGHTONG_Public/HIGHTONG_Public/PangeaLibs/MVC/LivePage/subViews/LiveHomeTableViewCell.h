//
//  LiveHomeTableViewCell.h
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/23.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"


typedef void (^LiveHomeCellBlock)(UIButton_Block*btn);

@interface LiveHomeTableViewCell : UITableViewCell

/**
 *  (^LiveHomeCellBlock)(UIButton_Block*btn)
 */
@property (nonatomic,copy)LiveHomeCellBlock Block;//点击事件的block

/**
 *  更新cell 的row 用字典
 *
 *  @param diction 更新的信息字典
 */

- (void)updateCellWithDiction:(NSDictionary*)diction;

@end
