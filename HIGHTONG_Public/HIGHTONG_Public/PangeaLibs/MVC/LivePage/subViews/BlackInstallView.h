//
//  BlackInstallView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralInputBlock.h"
#import "MyGesture.h"
typedef void(^BICancelButtonClick)(BOOL isSelect);
typedef void(^BIConfirmButtonClick)(NSString *nameStr,NSString *addressStr,NSString *teleStr,NSString *remarkStr, BOOL isSelect);

@interface BlackInstallView : UIView
@property (nonatomic,strong)UIImageView *BZView;
@property (nonatomic,strong)GeneralInputBlock *nameField;//用户名
@property (nonatomic,strong)GeneralInputBlock *addressField;//密码
@property (nonatomic,strong)GeneralInputBlock *teleField;//验证码
@property (nonatomic,strong)GeneralInputBlock *remarkField;//验证码
@property (nonatomic,strong)UIButton *cancelBtn;
@property (nonatomic,copy)BICancelButtonClick cancelButtonClick;
@property (nonatomic,copy)BIConfirmButtonClick confirmButtonClick;
@property (nonatomic,assign)MyGesture *mygesture;
@property (nonatomic,strong)UIButton *confirmBtn;
@property (nonatomic,strong)UITextView *remarkContent;
@end
