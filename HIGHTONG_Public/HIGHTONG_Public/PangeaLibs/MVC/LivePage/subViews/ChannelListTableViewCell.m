//
//  ChannelListTableViewCell.m
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ChannelListTableViewCell.h"

@implementation ChannelListTableViewCell






- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        WS(wsel);
        
        
        
        
        
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        
        _timeLabel = [[UILabel alloc]init];
        //        _timeLabel.text = @"15:51";
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [_timeLabel setBackgroundColor:[UIColor clearColor]];
        
        _videoTitleLabel = [[UILabel alloc]init];
        
        _videoTitleLabel.textColor = [UIColor whiteColor];
        _videoTitleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        
        
        [_videoTitleLabel setBackgroundColor:[UIColor clearColor]];
        
        
        _seperateLine = [[UIImageView alloc]init];
        [_seperateLine setImage:[UIImage imageNamed:@"ic_player_full_menuitem_seperate_line.png"]];
        _seperateLine.hidden = YES;
        
        
//        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
       
        _playBtn = [[UIButton_Block alloc]init];
//        [_backButton setBackgroundColor:[UIColor redColor]];
        
        
        
        [self.contentView addSubview:_timeLabel];
        [self.contentView addSubview:_videoTitleLabel];
        [self.contentView addSubview:_seperateLine];
        [self.contentView addSubview:_playBtn];
       
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(8*kRateSize
);
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(12*kRateSize
);
            make.width.mas_equalTo(40*kRateSize
);
            make.height.mas_equalTo(15*kRateSize
);
        }];
        
        
        [_videoTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(wsel.mas_bottom).offset(-6*kRateSize
);
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(11*kRateSize
);
            make.width.mas_equalTo(142*kRateSize
);
            make.height.mas_equalTo(15*kRateSize
);
        }];
        
        
        [_playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(14*kRateSize
);
            make.right.mas_equalTo(wsel.contentView.mas_right).offset(-9*kRateSize
);
            make.width.mas_equalTo(45*kRateSize
);
            make.height.mas_equalTo(24*kRateSize
);
            
           
        }];
        
        
        
        [_seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_videoTitleLabel.mas_bottom).offset(4*kRateSize
);
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(8);
            make.width.mas_equalTo(195*kRateSize
);
            make.height.mas_equalTo(1*kRateSize
);
            
            
        }];
        
        
        
        
    }
    
    return self;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
