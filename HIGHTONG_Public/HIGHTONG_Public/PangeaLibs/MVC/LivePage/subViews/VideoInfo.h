//
//  VideoInfo.h
//  HIGHTONG
//
//  Created by HTYunjiang on 14-9-1.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoInfo : NSObject


@property (nonatomic, copy)   NSMutableArray *adList;
@property (nonatomic, copy)   NSString *channelID;     // 频道编号
@property (nonatomic, copy)   NSString *channelName;   // 频道名称
@property (nonatomic, copy)   NSString *channelImage;  // 台标的链接地址
@property (nonatomic, copy)   NSString *PlayTag;       //是否可直播
@property (nonatomic, copy)   NSString *PlayBackTag;   //是否可回看

@property (nonatomic, copy)   NSString *videoID;       // 节目编号
@property (nonatomic, copy)   NSString *videoName;     // 节目名称
@property (nonatomic, copy)   NSString *videoTime;     // 视频播放的初始时间2014-10-10 12:20
@property (nonatomic, assign) NSInteger breadPoint;  //视频断点信息
@property (nonatomic, copy)   NSString *videoEndTime;     //节目结束时间

@property (nonatomic, copy)  NSString *fullStartTimeP;
@property (nonatomic, copy)  NSString *endTimeP;
@property (nonatomic,copy) NSString *playLink;
@property (nonatomic,copy) NSString *videoPauseLink;

@property (nonatomic, copy)  NSString *contentIDP;




//下载相关
@property (nonatomic, copy)   NSString *videoLink;     // 视频的下载链接
@property (nonatomic, assign) CGFloat downloadPercent; // 下载的百分比
@property (nonatomic, assign) BOOL    videoIsPaused;   // 视频下载是否暂停
@property (nonatomic, assign) NSInteger itemNumber;    // 要下载第几个片段

@end
