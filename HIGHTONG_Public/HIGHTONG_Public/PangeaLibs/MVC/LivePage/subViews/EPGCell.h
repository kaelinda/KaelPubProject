//
//  EPGCell.h
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
@interface EPGCell : UITableViewCell

@property(nonatomic,strong)NSMutableArray *array;//每个cell上的数据

@property(nonatomic,strong)UIButton_Block *btn1;//进入观看1

@property(nonatomic,strong)UIButton_Block *btn2;//进入观看2

- (void)setUpView;

- (void)reloadCell;
@end
