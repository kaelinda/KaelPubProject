//
//  EPGDateSelectedView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EPGDateSelectedView.h"
#import "DateTools.h"
@implementation EPGDateSelectedView

-(instancetype)initWithDateType:(EPG_DateType)type{
    self = [super init];
    if (self) {
        _dateType = type;
        [self setupSubviews];
    }
    return self;
}

-(instancetype)init{
    
    self = [super init];
    if (self) {
        [self initBaseData];
        [self setupSubviews];
    }
    return self;
}
-(void)initBaseData{
    _dateType =EPG_Date_TenDays; //EPG_Date_week;
}
-(void)setupSubviews{
    WS(wself);
    //初始化按钮  then  初始化 tableview
    
    _dateScrollView = [[UIScrollView alloc] init];
    _dateScrollView.showsVerticalScrollIndicator = NO;
    _dateScrollView.showsHorizontalScrollIndicator = NO;
    _dateScrollView.bounces = NO;
    _dateScrollView.delegate =self;
    [_dateScrollView setBackgroundColor:App_white_color];
    [self addSubview:_dateScrollView];
    
    [_dateScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(2*kRateSize);
        make.height.mas_equalTo(32*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-2*kRateSize);
        
    }];
    [self fillScrollView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //这里是1.5f后 你要做的事情
        [_dateScrollView setContentOffset:CGPointMake(123*kRateSize, 0)];
        
    });
    
    
}

-(void)fillScrollView{
    
    _dateContentView = [[UIView alloc] init];
    [_dateContentView setBackgroundColor:[UIColor clearColor]];
    [_dateScrollView addSubview:_dateContentView];
    
    [_dateContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_dateScrollView);
        make.height.mas_equalTo(_dateScrollView);
        
    }];
    
    NSInteger btnNum = 7;
    NSInteger todayNum = 3;
    if (_dateType == EPG_Date_TenDays) {
        btnNum = 10;
        todayNum = 6;
    }
    
    //--------------
    EPGVideoDateBtn *lastBtn = [[EPGVideoDateBtn alloc] init];
    
    for (int i=0; i<btnNum; i++) {
        
        NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents * dayComponent = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit |NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit | NSWeekCalendarUnit |NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit fromDate:[NSDate dateWithTimeInterval:(i*24*60*60)- (todayNum*24*60*60) sinceDate:[SystermTimeControl GetSystemTimeWith]]];
        
        EPGVideoDateBtn *btn = [[EPGVideoDateBtn alloc] init];
        btn.tag = i;
        btn.EPGBtnDelegate = self;
        if (i == todayNum) {
            [btn setBtnWithTitle:@"今天" andLineColor:App_selected_color];
            [btn.lineIV setBackgroundColor:App_selected_color];
            [btn.dateBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
            self.selectedBtn = btn;
            self.selectedBtn.dateBOStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)dayComponent.year,(long)dayComponent.month,dayComponent.day];;
            
            
        }else{
            [btn setBtnWithTitle:[NSString stringWithFormat:@"%02ld月%02ld日",(long)dayComponent.month,(long)dayComponent.day] andLineColor:CLEARCOLOR];
            [btn.lineIV setBackgroundColor:CLEARCOLOR];
            [btn.dateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.dateBOStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)dayComponent.year,dayComponent.month,dayComponent.day];
        }
        
//        [btn setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        [btn setBackgroundColor:App_white_color];
        [_dateContentView addSubview:btn];
        WS(wself);
        //------------添加限制
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_dateScrollView.mas_top).offset(5*kRateSize);
            //            make.size.mas_equalTo(CGSizeMake(80*kRateSize, 40*kRateSize));o
            make.width.mas_equalTo(80*kRateSize);
            make.bottom.mas_equalTo(wself.mas_bottom).offset(-0.2*kRateSize);
            make.left.mas_equalTo(82*kRateSize*i);
            
        }];
        //新版本不需要分割线
//        if (i<7) {
//            UIView *VLineLabel = [[UIView alloc] init];
//            [VLineLabel setBackgroundColor:[UIColor lightGrayColor]];
//            [_dateContentView addSubview:VLineLabel];
//            
//            [VLineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//                
//                make.left.mas_equalTo(btn.mas_right).offset(1);
//                make.width.mas_equalTo(0.5);
//                make.height.mas_equalTo(14*kRateSize);
//                make.centerY.mas_equalTo(_dateContentView.mas_centerY);
//                
//            }];
//        }
        
        lastBtn = btn;
    }
    
    //---------------
    [_dateContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastBtn.mas_right);
        
    }];
    
    
}

-(void)DateBtnSelectedAtDate:(NSString *)selectedDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *showDate = [dateFormatter dateFromString:selectedDate];
    
    
    NSDate *nextDatee = [SystermTimeControl GetSystemTimeWith];
    NSDateFormatter *mformatter =[[NSDateFormatter alloc]init] ;
    [mformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *netDateStr = [mformatter stringFromDate:nextDatee];
    
    
    
    NSDate *nextDate = [dateFormatter dateFromString:netDateStr];
    
    NSInteger index = [showDate daysFrom:nextDate];
    
    //-------------
    if (_dateType == EPG_Date_TenDays && (index > 3 || index < -6)) {
        [self DateBtnSelectedAtIndex:6];
        return;
    }
    //------------
    if (_dateType == EPG_Date_week && (index>3||index<-3)) {
        [self DateBtnSelectedAtIndex:3];
        return;
    }
    

    index = index+ ((_dateType == EPG_Date_week) ? 3 : 6);
    
    [self DateBtnSelectedAtIndex:index];
    
}


-(void)DateBtnSelectedAtIndex:(NSInteger)index{
    
    NSInteger minIndex = 2;//最小下标数量
    NSInteger maxIndex = (_dateType == EPG_Date_week) ? 4 : 7;
    
    NSArray *subViews = [_dateContentView subviews];
    
    for (UIView *view in subViews) {
        if ([view isKindOfClass:[EPGVideoDateBtn class]]) {
            EPGVideoDateBtn *ddddBtn = (EPGVideoDateBtn *)view;
            [ddddBtn.lineIV setBackgroundColor:CLEARCOLOR];
            [ddddBtn.dateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            //-----------------
            if (ddddBtn.tag==index)
            {
                self.selectedBtn = ddddBtn;
                [ddddBtn.lineIV setBackgroundColor:App_selected_color];
                [ddddBtn.dateBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
                if ([self.EPGDelegate respondsToSelector:@selector(epgDateSelectedViewAtIndex:)])
                {
                    if (index>=minIndex&&index<=maxIndex) {
                        
                        [_dateScrollView setContentOffset:CGPointMake(123*kRateSize+(index-3)*80*kRateSize, 0)];
                        
                    }else if (index<minIndex){
                        
                        [_dateScrollView setContentOffset:CGPointMake(0, 0)];
                    }
                    else{
                        
                        [_dateScrollView setContentOffset:CGPointMake(((_dateType == EPG_Date_week)? 2 : 4)*124*kRateSize, 0)];
                    }

//                    [_dateScrollView scrollRectToVisible:ddddBtn.frame animated:YES];
                    [self.EPGDelegate epgDateSelectedViewAtIndex:self.selectedBtn];
                }
            }
            //-----------------
        }
    }
    
}
-(void)EPGDateBtnSelectedAtIndex:(EPGVideoDateBtn *)Datebtn{
    NSInteger minIndex = 2;//最小下标数量
    NSInteger maxIndex = (_dateType == EPG_Date_week) ? 4 : 7;
    
    NSInteger index = Datebtn.tag;
    [UIView animateWithDuration:0.4 animations:^{
        if (index>=minIndex && index<=maxIndex) {
            
            [_dateScrollView setContentOffset:CGPointMake(123*kRateSize+(index-3)*80*kRateSize, 0)];
            
        }else if (index<minIndex){
            
            [_dateScrollView setContentOffset:CGPointMake(0, 0)];
        }
        else{
            
//            [_dateScrollView setContentOffset:CGPointMake(2*123*kRateSize, 0)];
            [_dateScrollView setContentOffset:CGPointMake(((_dateType == EPG_Date_week)? 2 : 4)*124*kRateSize, 0)];

        }
        
        
    }];
    
    
    self.selectedBtn = Datebtn;
    
    NSArray *subViews = [_dateContentView subviews];
    
    for (UIView *view in subViews) {
        if ([view isKindOfClass:[EPGVideoDateBtn class]]) {
            EPGVideoDateBtn *ddddBtn = (EPGVideoDateBtn *)view;
            [ddddBtn.lineIV setBackgroundColor:CLEARCOLOR];
            [ddddBtn.dateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
    
    [Datebtn.lineIV setBackgroundColor:App_selected_color];
    [Datebtn.dateBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    
    
    
    if ([self.EPGDelegate respondsToSelector:@selector(epgDateSelectedViewAtIndex:)]) {
        [self.EPGDelegate epgDateSelectedViewAtIndex:self.selectedBtn];
    }
    
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
