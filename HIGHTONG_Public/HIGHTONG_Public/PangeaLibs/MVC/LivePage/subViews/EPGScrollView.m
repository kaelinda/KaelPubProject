//
//  EPGScrollView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/20.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "EPGScrollView.h"

@implementation EPGScrollView

-(instancetype)initEpgScrollViewWithScrollViewType:(EPGScrollViewType)epgScrollViewType andScrollViewArr:(NSMutableArray *)scrollViewArr isVideoListWithCurrentDateStr:(NSString *)currentDateStr andEPGDateType:(EPG_DateType)epgDateType
{
    if (self) {
        self = [super init];
    }
    
    _dateType = epgDateType;

    if (epgScrollViewType == EPGScrollViewChannel) {
        
        [self setupChannelSubViews:scrollViewArr];
        
        
    }
    
    if (epgScrollViewType == EPGScrollViewVideoList) {
        [self setupVideoListSubViews:scrollViewArr withCurrentDateStr:currentDateStr];
    }
    
    
    
    
    
    return self;
}


-(instancetype)initEpgScrollViewWithScrollViewType:(EPGScrollViewType)epgScrollViewType andScrollViewArr:(NSMutableArray *)scrollViewArr
{
    if (self) {
        self = [super init];
    }

    if (epgScrollViewType == EPGScrollViewChannel) {
        
        [self setupChannelSubViews:scrollViewArr];
        
        
    }
    
    if (epgScrollViewType == EPGScrollViewVideoList) {
        [self setupVideoListSubViews:scrollViewArr withCurrentDateStr:_currentDateStr];
    }
    
    
    
    
    
    return self;
}


-(void)setupChannelSubViews:(NSMutableArray*)scrollViewArr
{
     WS(wself);
    
    
        _epgScrollView = [[UIScrollView alloc]init];
        [_epgScrollView setBackgroundColor:[UIColor clearColor]];
        _epgScrollView.showsVerticalScrollIndicator = YES;
        [wself addSubview:_epgScrollView];
    
        [_epgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wself.mas_left);
            make.top.mas_equalTo(wself.mas_top);
            make.width.mas_equalTo(51*kDeviceRate);
            make.height.mas_equalTo(wself.mas_height);
        }];
    
    
    _epgContentView = UIView.new;
    [_epgContentView setBackgroundColor:[UIColor clearColor]];
    [_epgScrollView addSubview:_epgContentView];
    [_epgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_epgScrollView);
        make.width.mas_equalTo(_epgScrollView.mas_width);
    }];
    
    UIView *lastView = UIView.new;

    if (scrollViewArr.count>0) {
        
        for (int i = 0; i < scrollViewArr.count; i++) {
            NSDictionary *dic = [[NSDictionary alloc]init];
            
            dic = [scrollViewArr objectAtIndex:i];
            
            
            NSLog(@"我要的%@",[dic valueForKey:@"shortName"]);
//            _actionBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];
            _actionBtn = [[UIButton_Block alloc]init];
            [_actionBtn setBackgroundColor:[UIColor clearColor]];
            _actionBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16*kDeviceRate];
            
            NSString *string1 = [NSString stringWithFormat:@"%@",[dic valueForKey:@"shortName"]];
            NSString *string2 = [string1 substringWithRange:NSMakeRange(0, 2)];
            _actionBtn.tag = i;
            [_actionBtn setTitle:[NSString stringWithFormat:@"%@",string2] forState:UIControlStateNormal];
            [_actionBtn setBackgroundImage:[UIImage imageNamed:@"bg_player_playbill_date"] forState:UIControlStateNormal];
            _actionBtn.alpha = 0.8;
            
            
            _actionBtn.Click = ^(UIButton_Block *btn,NSString *name){
                ;
                
                NSArray *array = btn.superview.subviews;

                for (UIButton *button in array) {
                    
                    if(button.tag != btn.tag )
                    {
                        [button setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
                    }
                    else if([button isKindOfClass:[UIButton class]])
                    {
                        
                        NSLog(@"%f......%f......%f......%f",btn.frame.origin.x,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height);
                        [btn setTitleColor:App_selected_color forState:UIControlStateNormal];

                        
                    }
                    
                    
                }

                
                
                if (wself.scrollSelectChannelBtnReturn) {
                    wself.scrollSelectChannelBtnReturn(btn);
                }else{
                    
                }
                
            };
            
            [_epgContentView addSubview:_actionBtn];
            
            
            if (i == 0) {
                
                [_actionBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
                
            }else
            {
                [_actionBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            }
            
            
            
            [_actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_epgContentView.mas_top).offset(52*(kDeviceRate*i));
                make.left.mas_equalTo(wself.epgScrollView.mas_left);
                make.size.mas_equalTo(CGSizeMake(51*kDeviceRate, 50*kDeviceRate));
                
            }];
            
            lastView = _actionBtn;
        }
    }
    
    [_epgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(lastView.mas_bottom);
        
    }];
    
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //这里是1.0f后 你要做的事情
        [self categoryBtnSelectedCurrentCategoryTpe:_clickCategoryTag];
        
    });
    
}

-(void)setClickCategoryTag:(NSInteger)clickCategoryTag
{
    _clickCategoryTag = clickCategoryTag;
    [self categoryBtnSelectedCurrentCategoryTpe:_clickCategoryTag];

}


-(void)setClickCategoryTitle:(NSString *)clickCategoryTitle
{
    _clickCategoryTitle = clickCategoryTitle;
}





-(void)categoryBtnSelectedCurrentCategoryTpe:(NSInteger)currentCategoryType
{
    NSArray *array = _epgContentView.subviews;
    
    for (UIButton *button in array) {
        
        if(button.tag != currentCategoryType )
        {
            [button setTitleColor:App_unselected0range_color forState:UIControlStateNormal];
        }
        else if([button isKindOfClass:[UIButton class]])
        {
            
            
            [button setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            
        }
        
        
    }
    
}






-(void)setupVideoListSubViews:(NSMutableArray*)scrollViewArr withCurrentDateStr:(NSString *)currentDateStr
{
    WS(wself);
    _epgScrollView = [[UIScrollView alloc]init];
    [_epgScrollView setBackgroundColor:[UIColor clearColor]];
    _epgScrollView.userInteractionEnabled = YES;
    _epgScrollView.delegate = self;
    _epgScrollView.bounces = NO;
    _epgScrollView.showsVerticalScrollIndicator = YES;
    [wself addSubview:_epgScrollView];
    
    [_epgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.mas_left);
        make.top.mas_equalTo(wself.mas_top);
        make.width.mas_equalTo(51*kDeviceRate);
        make.height.mas_equalTo(wself.mas_height);
    }];
    
    
    NSInteger btnNum = 7;
    NSInteger todayNum = 3;
    if (_dateType == EPG_Date_TenDays) {
        btnNum = 10;
        todayNum = 6;
    }

    
    _epgContentView = UIView.new;
    [_epgContentView setBackgroundColor:[UIColor clearColor]];
    [_epgScrollView addSubview:_epgContentView];
    [_epgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_epgScrollView);
        make.width.mas_equalTo(_epgScrollView.mas_width);
    }];
    
    UIView *lastView = UIView.new;
    
    if (scrollViewArr.count>0) {
        
        for (int i = 0; i < scrollViewArr.count; i++) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents * dayComponenter = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit |NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit | NSWeekCalendarUnit |NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit fromDate:[NSDate dateWithTimeInterval:(i*24*60*60)- (todayNum*24*60*60) sinceDate:[SystermTimeControl GetSystemTimeWith]]];
            NSInteger week = dayComponenter.weekday;
            
            
            _actionBtn = [UIButton_Block buttonWithType:UIButtonTypeCustom];;
            _actionBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16*kDeviceRate];
            
            _actionBtn.dateStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)dayComponenter.year,dayComponenter.month,dayComponenter.day];
            
            
            _actionBtn.tag = i;
            _actionBtn.titleLabel.numberOfLines=0;
            [_actionBtn setTitle:[NSString stringWithFormat:@" %ld\n%@",(long)dayComponenter.day,[scrollViewArr objectAtIndex:week-1]] forState:UIControlStateNormal];
            [_actionBtn setBackgroundImage:[UIImage imageNamed:@"bg_player_playbill_date"] forState:UIControlStateNormal];
            _actionBtn.alpha = 0.8;
            _actionBtn.Click = ^(UIButton_Block *btn,NSString *name){
                if (wself.scrollSelectVideoListBtnReturn) {
                    
                    [wself sliderScrollew:btn.tag];
                    wself.scrollSelectVideoListBtnReturn(btn);
                }else{
                    
                }
                
            };

            [_epgContentView addSubview:_actionBtn];
            
            
            
            
            
            if (i == todayNum) {
                
                [_actionBtn setTitle:[NSString stringWithFormat:@" %ld\n%@",(long)dayComponenter.day,@"今天"] forState:UIControlStateNormal];
                
                [_actionBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
                
            }else
            {
                [_actionBtn setTitleColor:App_unselected0range_color forState:UIControlStateNormal];
            }
            
            [_actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_epgContentView.mas_top).offset( 52*(i*kDeviceRate));
                //            make.left.mas_equalTo(1);
                make.left.mas_equalTo(wself.epgScrollView.mas_left);
                make.size.mas_equalTo(CGSizeMake(51*kDeviceRate, 50*kDeviceRate));
                
            }];
            
            lastView = _actionBtn;
        }
    }
    
    
    [_epgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(lastView.mas_bottom);
        
    }];
    

 
}




-(void)MMMMDateBtnSelectedAtDate:(NSString *)selectedDate
{
//    HTRequest *request =  [[HTRequest alloc]initWithDelegate:self];
//    [request EPGSHOWWithServiceID:_serviceID andDate:selectedDate andStartDate:@"" andEndDate:@"" andTimeout:kTIME_TEN];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *showDate = [dateFormatter dateFromString:selectedDate];
    
    
    NSDate *nextDatee = [SystermTimeControl GetSystemTimeWith];
    NSDateFormatter *mformatter =[[NSDateFormatter alloc]init] ;
    [mformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *netDateStr = [mformatter stringFromDate:nextDatee];
    
    
    
    NSDate *nextDate = [dateFormatter dateFromString:netDateStr];
    
    NSInteger index = [showDate daysFrom:nextDate];
    
    if (_dateType == EPG_Date_week && (index>3||index<-3)) {
        
        
        [self TimeButtonSelectedAtIndex:3];
        return;
    }
    
    
    if (_dateType == EPG_Date_TenDays && (index>3||index<-6))
    {
        
        [self TimeButtonSelectedAtIndex:6];
        return;
    }
    
    
    
    
    
    index = index+((_dateType == EPG_Date_week) ? 3 : 6);
    
    [self TimeButtonSelectedAtIndex:index];
}


-(void)TimeButtonSelectedAtIndex:(NSInteger)index
{
    
    
    [self sliderScrollew:index];
    
    
    NSArray *array = _epgContentView.subviews;
    ;
    
    for (UIButton *button in array) {
        
        if(button.tag != index )
        {
            [button setTitleColor:App_unselected0range_color forState:UIControlStateNormal];
        }
        else if([button isKindOfClass:[UIButton class]])
        {
            
            
            [button setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            
        }
        
        
    }
    
    
}




-(void)sliderScrollew:(NSInteger)scrollDateInterger
{
    NSInteger minIndex = 2;//最小下标数量
    NSInteger maxIndex = (_dateType == EPG_Date_week) ? 4 : 7;
    
    NSInteger index = scrollDateInterger;
    [UIView animateWithDuration:0.4 animations:^{
        if (index>=minIndex && index<=maxIndex) {
            
            [_epgScrollView setContentOffset:CGPointMake(0,(_dateType == EPG_Date_week)? 0:38*kDeviceRate+(index-3)*28*kDeviceRate)];
            
        }else if (index<minIndex){
            
            [_epgScrollView setContentOffset:CGPointMake(0, 0)];
        }
        else{
            
            //            [_dateScrollView setContentOffset:CGPointMake(2*123*kDeviceRate, 0)];
            [_epgScrollView setContentOffset:CGPointMake(0,((_dateType == EPG_Date_week)? 0 : 4)*38*kDeviceRate)];
            
        }
        
        
    }];
}



@end
