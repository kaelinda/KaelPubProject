//
//  EPGCell.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EPGCell.h"
#import "Tool.h"

#import "UserScheduleCenter.h"
#import "LocalCollectAndPlayRecorder.h"

@interface EPGCell ()
{
    UIImageView * _taibiao;
    UILabel  * _titleLable;
    UILabel *_nextLabel;
    UILabel *_CurrentLabel;
    
    UIImageView * _taibiao_2;
    UILabel  * _titleLable_2;
    UILabel *_nextLabel_2;
    UILabel *_CurrentLabel_2;
}
@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;
@end

@implementation EPGCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        _array = [NSMutableArray array];
        _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    }
    return self;
}

- (void)setUpView
{
//
        
        UIView *SuperView = self.contentView;
        
        //    self.backgroundColor = [UIColor yellowColor];
        [SuperView removeAllSubviews];
        
        
        //    SuperView.backgroundColor = [UIColor redColor];
        
        UIView *baseView = [UIView new];
        [SuperView addSubview:baseView];
        //    baseView.backgroundColor = [UIColor cyanColor];
        
        UIView *SecendView = [UIView new];
        //    SecendView.backgroundColor = [UIColor blackColor];
        [SuperView addSubview:SecendView];
        
        //******************第一块直播位置
        [baseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(SuperView.mas_top);
            make.left.equalTo(SuperView.mas_left);
            make.right.equalTo(SuperView.mas_right);
            
            make.height.equalTo(SecendView.mas_height);
            
            make.bottom.equalTo(SecendView.mas_top);
            
        }];
        
        {
            _taibiao = [UIImageView new];
            [baseView addSubview:_taibiao];
            
            [_taibiao mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(baseView.mas_top).offset(38*kRateSize);
                make.left.equalTo(baseView.mas_left).offset(25*1*kRateSize);
                make.width.equalTo(@(51*1*kRateSize));
                make.height.equalTo(@(44*1*kRateSize));
            }];
            
            
            _titleLable = [UILabel new];
            _titleLable.numberOfLines = 0;
            _titleLable.textAlignment = NSTextAlignmentCenter;
            _titleLable.font = [UIFont fontWithName:HEITI_Light_FONT size:15.0f];
            [baseView addSubview:_titleLable];
            [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_taibiao.mas_bottom).offset(3*1*kRateSize);
                make.centerX.equalTo(_taibiao.mas_centerX).offset(0);
                make.width.equalTo(@(80*1*kRateSize));
                make.height.equalTo(@(40*1*kRateSize));
            }];
            
            
            _taibiao.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
            _titleLable.text = @"";
            
            UIImageView *line = [UIImageView new];
            [baseView addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(baseView.mas_top).offset(15*1*kRateSize);
                make.left.equalTo(_taibiao.mas_right).offset(18*1*kRateSize);
                make.width.equalTo(@(1*1*kRateSize));
                make.height.equalTo(_taibiao.mas_height).offset(10*1*kRateSize);
            }];
            line.image = [UIImage imageNamed:@"line_03"];
            
            
            
            self.btn1 = [UIButton_Block new];
            [baseView addSubview:self.btn1];
            [self.btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(baseView.mas_top).offset(28*1*kRateSize);
                make.right.equalTo(baseView.mas_right).offset(-8*1*kRateSize);
                make.width.equalTo(@(70*1*kRateSize));
                make.height.equalTo(@(35*1*kRateSize));
            }];
            _btn1.layer.cornerRadius = 2;
            _btn1.serviceIDName = @"";
            _btn1.eventName =@"";
//            [self.btn1 setBackgroundImage:[UIImage imageNamed:@"进入观看"] forState:UIControlStateNormal];
            
            
            _nextLabel = [UILabel new];
            [baseView addSubview:_nextLabel];
            [_nextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(baseView.mas_top).offset(10*1*kRateSize);
                make.left.equalTo(line.mas_right).offset(8*1*kRateSize);
                make.right.equalTo(self.btn1.mas_left).offset(-6*1*kRateSize);
                make.bottom.equalTo(self.btn1.mas_top).offset(12.5*1*kRateSize);
            }];
            _nextLabel.text = @"";
            
            
            _CurrentLabel = [UILabel new];
            [baseView addSubview:_CurrentLabel];
            [_CurrentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_nextLabel.mas_bottom).offset(0*1*kRateSize);
                make.left.equalTo(line.mas_right).offset(8*1*kRateSize);
                make.right.equalTo(self.btn1.mas_left).offset(-6);
                make.height.equalTo(_nextLabel.mas_height).offset(0*1*kRateSize);
            }];
            _CurrentLabel.text = @"";
            
        }
        
        
        
        
        //******************第二块直播位置
        
        [SecendView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(baseView.mas_bottom);
            make.left.equalTo(SuperView.mas_left);
            make.right.equalTo(SuperView.mas_right);
            
            make.height.equalTo(baseView.mas_height);
            
            make.bottom.equalTo(SuperView.mas_bottom);
        }];
        
        {
            _taibiao_2 = [UIImageView new];
            [SecendView addSubview:_taibiao_2];
            [_taibiao_2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(SecendView.mas_top).offset(9*kRateSize);
                make.left.equalTo(SecendView.mas_left).offset(25*1*kRateSize);
                make.width.equalTo(@(51*1*kRateSize));
                make.height.equalTo(@(0));

//                make.height.equalTo(@(44*1*kRateSize));
            }];
            
            _taibiao_2.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
            
            
            _titleLable_2 = [UILabel new];
            [SecendView addSubview:_titleLable_2];
            [_titleLable_2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_taibiao_2.mas_bottom).offset(3*1*kRateSize);
                make.centerX.equalTo(_taibiao_2.mas_centerX).offset(0);
                make.width.equalTo(@(80*1*kRateSize));
                make.height.equalTo(@(20*1*kRateSize));
            }];
            _titleLable_2.text = @"";
            
            
            
            UIImageView *line1 = [UIImageView new];
            [SecendView addSubview:line1];
            [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(SecendView.mas_top).offset(15*1*kRateSize);
                make.left.equalTo(_taibiao_2.mas_right).offset(18*1*kRateSize);
                make.width.equalTo(@(1*1*kRateSize));
                make.height.equalTo(_taibiao_2.mas_height).offset(10*1*kRateSize);
            }];
            
            line1.image = [UIImage imageNamed:@"line_03"];
            
            
            
            //修改了bug,这个用了父类方法去创建了子类，没有重写方法，所以创建出了，uibutton 而不是uibuton_block
            self.btn2 = [UIButton_Block new];
            [SecendView addSubview:self.btn2];
            [self.btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(SecendView.mas_top).offset(28*1*kRateSize);
                make.right.equalTo(SecendView.mas_right).offset(-8*1*kRateSize);
                make.width.equalTo(@(70*1*kRateSize));
                make.height.equalTo(@(35*1*kRateSize));
            }];
            
            self.btn2.layer.cornerRadius = 2;
            self.btn2.clipsToBounds = YES;
            self.btn2.serviceIDName = @"";
            self.btn2.eventName =@"";
            
//            [self.btn2 setBackgroundImage:[UIImage imageNamed:@"进入观看"] forState:UIControlStateNormal];
            
            
            _nextLabel_2 = [UILabel new];
            
            [SecendView addSubview:_nextLabel_2];
            [_nextLabel_2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(SecendView.mas_top).offset(10*1*kRateSize);
                make.left.equalTo(line1.mas_right).offset(8*1*kRateSize);
                make.right.equalTo(self.btn2.mas_left).offset(-6);
                make.bottom.equalTo(self.btn2.mas_top).offset(12.5*1*kRateSize);
            }];
            _nextLabel_2.text = @"";
            
            
            _CurrentLabel_2 = [UILabel new];
            
            [SecendView addSubview:_CurrentLabel_2];
            [_CurrentLabel_2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_nextLabel_2.mas_bottom).offset(0*1*kRateSize);
                make.left.equalTo(line1.mas_right).offset(8*1*kRateSize);
                make.right.equalTo(self.btn2.mas_left).offset(-6);
                make.height.equalTo(_nextLabel_2.mas_height).offset(0*1*kRateSize);
            }];
            
        }
        
        
        {//测试  颜色设置 字体
            _CurrentLabel_2.text =@"";
            UIFont *font = [UIFont systemFontOfSize:13*kRateSize];
            _nextLabel.font = font;
            _nextLabel_2.font = font;
            
            font = [UIFont systemFontOfSize:15*kRateSize];
            _CurrentLabel.font = font;
            _CurrentLabel_2.font = font;
            
            font = [UIFont systemFontOfSize:13*kRateSize];
            _titleLable.font = font;
            _titleLable_2.font =font;
            
            
            UIColor *color = UIColorFromRGB(0x666666);
            
            _titleLable.textColor = color;
            _titleLable_2.textColor = color;
            _CurrentLabel.textColor = color;
            _CurrentLabel_2.textColor = color;
            
            _CurrentLabel.font = font;
            _CurrentLabel_2.font = font;
            
            
            
            //测试颜色
            //
            //        color = [UIColor redColor];
            //        _titleLable.backgroundColor = color;
            //
            //
            //        _nextLabel.backgroundColor = color;
            //        _nextLabel_2.backgroundColor = color;
            //        _titleLable_2.backgroundColor = color;
            //
            //        color = [UIColor yellowColor];
            //
            //        _CurrentLabel.backgroundColor = color;
            //        _taibiao.backgroundColor = color;
            //
            //        _CurrentLabel_2.backgroundColor = color;
            //        _taibiao_2.backgroundColor = color;
            
        }
    
}




- (void)reloadCell
{
    _titleLable.text = @"";
    _nextLabel.text = @"";
    _CurrentLabel.text = @"";
    _titleLable_2.text = @"";
    _nextLabel_2.text = @"";
    _CurrentLabel_2.text = @"";
    
    self.btn1.selected = NO;
    self.btn2.selected = NO;
    
    [self.btn1 setImage:nil forState:UIControlStateNormal];
    [self.btn1 setImage:nil forState:UIControlStateSelected];
    
    [self.btn2 setImage:nil forState:UIControlStateNormal];
    [self.btn2 setImage:nil forState:UIControlStateSelected];
    
    if (self.array.count >= 1) {
        NSDictionary *dic1 = self.array[0];
        [_taibiao sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic1 objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        
        _titleLable.text = [dic1 objectForKey:@"channelName"];
        
        self.btn1.channelID = [dic1 objectForKey:@"channelID"];
        self.btn1.serviceIDName = [dic1 objectForKey:@"id"];
        self.btn1.eventName = [dic1 objectForKey:@"eventName"];
        self.btn1.Diction = dic1;
        
        NSString *startTime = [dic1 objectForKey:@"startTime"];
        NSString *endTime = [dic1 objectForKey:@"endTime"];

        if ([Tool typePlayStartTime:startTime andEndTime:endTime] == 1) {
            //****************搜索栏中的预约、已预约图片**************************************
//            BOOL finishYuYue = [[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:self.btn1.serviceIDName];;
            
            //ww...
            BOOL finishYuYue = [_localListCenter checkTheScheduledListwithID:self.btn1.serviceIDName];
            
            [self.btn1 setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateSelected];
            [self.btn1 setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
            if (finishYuYue) {
                self.btn1.selected = YES;
            }else
            {
                self.btn1.selected = NO;
            }
            self.btn1.isYuYue = YES;
            self.btn1.isLive = NO;

        }else if ([Tool typePlayStartTime:startTime andEndTime:endTime] == 0)
        {
            [self.btn1 setImage:nil forState:UIControlStateSelected];
            [self.btn1 setImage:[UIImage imageNamed:@"enterWatch"] forState:UIControlStateNormal];
            self.btn1.isYuYue = NO;
            self.btn1.isLive = YES;
        }
        else
        {
            [self.btn1 setImage:nil forState:UIControlStateSelected];
            [self.btn1 setImage:[UIImage imageNamed:@"enterWatch"] forState:UIControlStateNormal];
            self.btn1.isYuYue = NO;
            self.btn1.isLive = NO;
        }
        
        NSString *start = [dic1 objectForKey:@"startTime"];
        NSString *end = [dic1 objectForKey:@"endTime"];
        start = [Tool MMddssToyyMMddStringFromDate:start andFormaterString:@"hh:mm"];
        end = [Tool MMddssToyyMMddStringFromDate:end andFormaterString:@"hh:mm"];
        NSString *time = [NSString stringWithFormat:@"%@-%@",start,end];
        
        _CurrentLabel.text = time;
        _nextLabel.text = [dic1 objectForKey:@"eventName"];
    }
    
    
    
    if (self.array.count >= 2) {
        //**********************下半部分的Live推荐》》》》》》
        self.btn2.hidden = NO;

        NSDictionary *dic2 = self.array[1];
        [_taibiao_2 sd_setImageWithURL:[NSURL URLWithString:[dic2 objectForKey:@"imageLink"]] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        
        
        if ([dic2 objectForKey:@"serviceName"]) {
            
            _titleLable_2.text = [dic2 objectForKey:@"eventName"];
            
        }
        
        self.btn2.channelID = [dic2 objectForKey:@"channelID"];
        self.btn2.serviceIDName = [dic2 objectForKey:@"id"];
        self.btn2.eventName = [dic2 objectForKey:@"eventName"];
        
        NSString *startTime = [dic2 objectForKey:@"startTime"];
        NSString *endTime = [dic2 objectForKey:@"endTime"];

        
        if ([Tool typePlayStartTime:startTime andEndTime:endTime] == 1) {
            //****************搜索栏中的预约、已预约图片**************************************
          
//            BOOL finishYuYue = [[UserScheduleCenter defaultCenter] checkTheScheduledListwithID:self.btn2.serviceIDName];
            //ww...
            BOOL finishYuYue = [_localListCenter checkTheScheduledListwithID:self.btn2.serviceIDName];
            [self.btn2 setImage:[UIImage imageNamed:@"btn_live_foxpor_item_ordered"] forState:UIControlStateSelected];
            [self.btn2 setImage:[UIImage imageNamed:@"btn_live_foxpor_item_order"] forState:UIControlStateNormal];
           
            if (finishYuYue) {
                self.btn2.selected = YES;
            }else
            {
                self.btn2.selected = NO;
            }
           
            self.btn2.isLive = NO;
            self.btn2.isYuYue = YES;

        }else if ([Tool typePlayStartTime:startTime andEndTime:endTime] == 0)
        {
            [self.btn2 setImage:nil forState:UIControlStateSelected];
            [self.btn2 setImage:[UIImage imageNamed:@"enterWatch"] forState:UIControlStateNormal];
            self.btn2.isYuYue = NO;
            self.btn2.isLive = YES;
        }
        else
        {
            [self.btn2 setImage:nil forState:UIControlStateSelected];
            [self.btn2 setImage:[UIImage imageNamed:@"enterWatch"] forState:UIControlStateNormal];
            self.btn2.isYuYue = NO;
            self.btn2.isLive = NO;
        }
        
        
        NSString *start = [dic2 objectForKey:@"startTime"];
        NSString *end = [dic2 objectForKey:@"endTime"];
        start = [Tool MMddssToyyMMddStringFromDate:start andFormaterString:@"hh:mm"];
        end = [Tool MMddssToyyMMddStringFromDate:end andFormaterString:@"hh:mm"];
        NSString *time = [NSString stringWithFormat:@"%@-%@",start,end];
        
        _CurrentLabel_2.text = time;
        _nextLabel_2.text = [dic2 objectForKey:@"eventName"];
        
        
    }else{
            self.btn2.hidden = YES;
        
    }
    
}



- (CGSize)String:(NSString*) string StringSizeOfFont:(UIFont*)font
{
    CGSize size;
    if (isAfterIOS6) {
        size =  [string sizeWithFont:font forWidth:99999 lineBreakMode:NSLineBreakByCharWrapping];
        size.width += 30*kRateSize;
    }else
    {
        NSDictionary *attrs = @{NSFontAttributeName : font};
        size = [string boundingRectWithSize:CGSizeMake(30, 9999999) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    }
    
    return size;
}
- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end



