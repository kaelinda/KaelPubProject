//
//  EPGDateSelectedView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGVideoDateBtn.h"


@protocol epgdateSelectedViewDelegate <NSObject>

-(void)epgDateSelectedViewAtIndex:(EPGVideoDateBtn *)btn;

@end


@interface EPGDateSelectedView : UIView<EPGDateBtnDelegate,UIScrollViewDelegate>

@property (nonatomic,assign)id<epgdateSelectedViewDelegate>EPGDelegate;

@property (nonatomic,strong) UIScrollView *dateScrollView;//日期选择区域
@property (nonatomic,strong) UIView *dateContentView;

@property (nonatomic,strong) EPGVideoDateBtn *selectedBtn;

@property (nonatomic,assign) EPG_DateType dateType;


/**
 初始化 新加参数

 @param type 日期格式参数
 @return 视图自身
 */
-(instancetype)initWithDateType:(EPG_DateType)type;

-(void)DateBtnSelectedAtIndex:(NSInteger )index;

-(void)DateBtnSelectedAtDate:(NSString *)selectedDate;


@end
