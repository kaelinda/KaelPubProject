//
//  EPGVideoDateBtn.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EPGVideoDateBtn.h"

@implementation EPGVideoDateBtn

-(instancetype)init{
    self = [super init];
    [self setupSubViews];
    
    return self;
}
-(void)setupSubViews{
    WS(wself);
    _dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _dateBtn.backgroundColor = [UIColor clearColor];
    _dateBtn.titleLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:15.0f];
    [_dateBtn addTarget:self action:@selector(dateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_dateBtn];
    
//    CGSize btnSize = CGSizeMake(88*kRateSize, 23*kRateSize);
    [_dateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself);
        make.left.mas_equalTo(wself.mas_left);
        make.bottom.mas_equalTo(wself.bottom).offset(-2);
        make.width.mas_equalTo(85*kRateSize);
        
    }];
    
    _lineIV = [[UIImageView alloc] init];
    [self addSubview:_lineIV];
    
//    CGSize lineSize = CGSizeMake(88*kRateSize, 2);
    [_lineIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.mas_bottom).offset(1);
        make.left.mas_equalTo(_dateBtn.mas_left).offset(4);
        make.right.mas_equalTo(_dateBtn.mas_right).offset(-8);
        make.height.mas_equalTo(1);
        
    }];
    


}


-(void)setBtnWithTitle:(NSString *)title andLineColor:(UIColor *)color{

    [_dateBtn setTitle:title forState:UIControlStateNormal];
    [_lineIV setBackgroundColor:color];

    _dateBtn.tag = self.tag;

}
-(void)setLineColorWith:(UIColor *)color{
    [_lineIV setBackgroundColor:color];

}
-(void)dateBtnAction:(EPGVideoDateBtn *)btn{
    WS(wself);
    if ([self.EPGBtnDelegate respondsToSelector:@selector(EPGDateBtnSelectedAtIndex:)]) {
        [self.EPGBtnDelegate EPGDateBtnSelectedAtIndex:(UIButton *)wself];
    }

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
