//
//  EPGVideoDateBtn.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EPGDateBtnDelegate <NSObject>

-(void)EPGDateBtnSelectedAtIndex:(UIButton *)Datebtn;

@end

@interface EPGVideoDateBtn : UIView

@property (nonatomic,assign) id<EPGDateBtnDelegate>EPGBtnDelegate;
@property (nonatomic,strong) UIButton *dateBtn;
@property (nonatomic,strong) NSString *dateBOStr;
@property (nonatomic,strong) UIImageView *lineIV;
-(void)setLineColorWith:(UIColor *)color;
-(void)setBtnWithTitle:(NSString *)title andLineColor:(UIColor *)color;
@end
