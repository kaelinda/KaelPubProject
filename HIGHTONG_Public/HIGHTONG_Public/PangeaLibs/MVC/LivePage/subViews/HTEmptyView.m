//
//  HTEmptyView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/21.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTEmptyView.h"
@interface HTEmptyView ()
@property (copy, nonatomic) EmptyViewClicked emptyViewClick;

@end

@implementation HTEmptyView

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}


-(id)initWithEmptyImageString:(NSString*)imageString andEmptyLabelText:(NSString*)text emptyViewClick:(EmptyViewClicked)emptyViewClick
{
    self = [super init];
    if (self) {

        [self addEmptyImageString:imageString andEmptyLabelText:text emptyViewClick:emptyViewClick];
        
        
        }
        return self;
}


-(id)initWithEmptyFatherView:(UIView*)fatherView  andEmptyImageString:(NSString*)imageString andEmptyLabelText:(NSString*)text emptyViewClick:(EmptyViewClicked)emptyViewClick
{
    self = [super init];
    if (self) {
       
        [fatherView addSubview:self];
        [self setBackgroundColor:[UIColor clearColor]];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(fatherView.mas_top).offset(100);
            make.centerX.mas_equalTo(fatherView.mas_centerX);
            make.centerY.mas_equalTo(fatherView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(300*kDeviceRate, 300*kDeviceRate));
        }];
        
        
        [self addEmptyImageString:imageString andEmptyLabelText:text emptyViewClick:emptyViewClick];
        
        
    }
    return self;

}





-(void)addEmptyImageString:(NSString*)imageString andEmptyLabelText:(NSString*)text emptyViewClick:(EmptyViewClicked)emptyViewClick
{
    
    _emptyIV = [[UIImageView alloc]init];
    _emptyIV.userInteractionEnabled = YES;
    _emptyViewClick = emptyViewClick;
    _emptyIV.contentMode = UIViewContentModeScaleAspectFit;
    [_emptyIV addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emptyIVClick)]];
    [self addSubview:_emptyIV];
    [_emptyIV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.mas_top).offset(10*kDeviceRate);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.centerY.mas_equalTo(self.mas_centerY).offset(40*kDeviceRate);
        make.size.mas_equalTo(CGSizeMake(160*kDeviceRate, 80*kDeviceRate));
    }];
    
    _emptyIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imageString]];
    
    if (!isEmptyStringOrNilOrNull(text)) {
        
        _emptyLabel = [[UILabel alloc]init];
        
        [self addSubview:_emptyLabel];
        [_emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_emptyIV.mas_bottom).offset(10*kDeviceRate);
            make.centerX.mas_equalTo(self.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(100*kDeviceRate, 30*kDeviceRate));
        }];
        
        [_emptyLabel setText:[NSString stringWithFormat:@"%@",text]];
        
  
    }
    
    
    
}

//-(void)setHidden:(BOOL)hidden
//{
//    if (hidden) {
//        [self setHidden:YES];
//    }else
//    {
//        [self setHidden:NO];
//    }
//    
//    [super setHidden:hidden];
//
//}

-(void)showSuperView:(UIView*)frontView andImageString:(NSString*)imageString andTextString:(NSString*)textString
{
    if (!frontView) {
        
        NSException *excp = [NSException exceptionWithName:@"EmptyViewException" reason:@"未设置父视图。" userInfo:nil];
        [excp raise];
        return;
    };
    
    
    self.backgroundColor = [UIColor redColor];
//    [frontView insertSubview:self atIndex:0];
    
    
}


-(void)getOutView
{
    !self ?:[self removeFromSuperview];
}



//-(UIImageView *)emptyIV
//{
//    if (!_emptyIV) {
//        _emptyIV = [[UIImageView alloc]init];
//        [_emptyIV addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emptyIVClick)]];
//        [self addSubview:_emptyIV];
//    }
//    
//    return _emptyIV;
//}

-(void)emptyIVClick
{
    if (_emptyViewClick) {
        [HTEmptyView animationPopupWith:_emptyIV duration:0.25];
        _emptyViewClick();
    }else
    {
        return;
    }
}

//图片点击动画
+(void)animationPopupWith:(UIView*)aview duration:(CGFloat)duration{
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = duration ? duration : 0.5f;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.6, 0.6, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aview.layer addAnimation:animation forKey:nil];
    
}


//-(UILabel *)emptyLabel
//{
//    if (!_emptyLabel) {
//        _emptyLabel = [[UILabel alloc]init];
//        
//        [self addSubview:_emptyLabel];
//    }
//    
//    return _emptyLabel;
//}




@end
