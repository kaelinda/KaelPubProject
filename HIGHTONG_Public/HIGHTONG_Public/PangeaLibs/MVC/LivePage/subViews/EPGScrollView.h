//
//  EPGScrollView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/20.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
typedef void(^ScrollSelectChannelBtnReturn)(UIButton_Block*buttonBlock);
typedef void(^ScrollSelectVideoListBtnReturn)(UIButton_Block*buttonBlock);


@interface EPGScrollView : UIView<UIScrollViewDelegate>
@property (nonatomic,strong)NSString *scrollViewType;//scrollView的类型，channel则为频道分类、videoList则为节目单的类型
@property (nonatomic,strong)UIScrollView *epgScrollView;//scrollView承载的滑动视图
@property (nonatomic,strong)UIView *epgContentView;//承载滑动视图的内容的视图
@property (nonatomic,strong)NSString *currentDateStr;//
@property (nonatomic,assign)NSInteger clickCategoryTag;
@property (nonatomic,strong)NSString *clickCategoryTitle;
@property (nonatomic,strong) UIButton_Block *actionBtn;//点击ScrollViewButton的按钮
@property (nonatomic,copy) ScrollSelectChannelBtnReturn scrollSelectChannelBtnReturn;
@property (nonatomic,copy) ScrollSelectVideoListBtnReturn scrollSelectVideoListBtnReturn;
@property (nonatomic,assign) EPG_DateType dateType;


-(instancetype)initEpgScrollViewWithScrollViewType:(EPGScrollViewType)epgScrollViewType andScrollViewArr:(NSMutableArray *)scrollViewArr isVideoListWithCurrentDateStr:(NSString *)currentDateStr andEPGDateType:(EPG_DateType)epgDateType;
-(instancetype)initEpgScrollViewWithScrollViewType:(EPGScrollViewType)epgScrollViewType andScrollViewArr:(NSMutableArray *)scrollViewArr;


-(void)MMMMDateBtnSelectedAtDate:(NSString *)selectedDate;

-(void)sliderScrollew:(NSInteger)scrollDateInterger;

@end
