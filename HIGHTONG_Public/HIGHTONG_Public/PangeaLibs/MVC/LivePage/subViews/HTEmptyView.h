//
//  HTEmptyView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/21.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^EmptyViewClicked)();
@interface HTEmptyView : UIView
@property (copy, nonatomic) UIImageView *emptyIV;
@property (copy, nonatomic) UILabel *emptyLabel;

-(id)initWithEmptyImageString:(NSString*)imageString andEmptyLabelText:(NSString*)text emptyViewClick:(EmptyViewClicked)emptyViewClick;



-(id)initWithEmptyFatherView:(UIView*)fatherView  andEmptyImageString:(NSString*)imageString andEmptyLabelText:(NSString*)text emptyViewClick:(EmptyViewClicked)emptyViewClick;


@end
