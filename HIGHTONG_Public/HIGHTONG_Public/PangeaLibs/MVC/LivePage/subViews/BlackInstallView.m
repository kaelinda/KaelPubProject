//
//  BlackInstallView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/5.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "BlackInstallView.h"
#import "CHECKFORMAT.h"
#define kGrayLine  0.5*kRateSize

@interface BlackInstallView ()<UITextFieldDelegate>
{
    BOOL inRule;
    
    BOOL lengthOk;
    
    BOOL noEMOJI;
}
@end
@implementation BlackInstallView
-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

-(void)setupSubViews
{
    
    WS(wself);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeContentViewPosition:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeContentViewPosition:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    _BZView = [[UIImageView alloc]init];
    _BZView.userInteractionEnabled = YES;
    _BZView.layer.cornerRadius = 8;
    _BZView.layer.masksToBounds = YES;
    [_BZView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:_BZView];
    
    [_BZView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(300*kRateSize, 280*kRateSize));
//        make.left.mas_equalTo(wself.mas_left).offset(10*kRateSize);
//        make.right.mas_equalTo(wself.mas_right).offset(-10*kRateSize);
    }];
    
    
    _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelBtn setBackgroundImage:[UIImage imageNamed:@"install_dialog_close"] forState:UIControlStateNormal];
    [self addSubview:_cancelBtn];
    
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25*kRateSize, 25*kRateSize));
        make.right.mas_equalTo(_BZView.mas_right).offset(5*kRateSize);
        make.top.mas_equalTo(_BZView.mas_top).offset(-10*kRateSize);
    }];
    
    
    [_cancelBtn addTarget:self action:@selector(cancelButton:) forControlEvents:UIControlEventTouchUpInside];
        
    _nameField = [[GeneralInputBlock alloc] initWithTitle:@"姓名：    " andPlaceholderText:@"请填写您的名字"];
    _nameField.userInteractionEnabled = YES;
    _nameField.inputBlock.delegate = self;
    [_BZView addSubview:_nameField];
    [_nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BZView.mas_left);
        make.top.equalTo(_BZView.mas_top).offset(5*kRateSize);
        make.height.equalTo(@(40*kRateSize));
        make.right.equalTo(_BZView.mas_right);
    }];
    
    UIImageView *firstSideLine = [[UIImageView alloc] init];
    [firstSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [_BZView addSubview:firstSideLine];
    [firstSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@(280*kRateSize));
        make.height.mas_equalTo(kGrayLine);
        make.centerX.equalTo(_BZView.mas_centerX);
        make.top.equalTo(wself.nameField.mas_bottom);
    }];

    
    _addressField = [[GeneralInputBlock alloc] initWithTitle:@"地址：    " andPlaceholderText:@"请填写详细地址"];
    _addressField.userInteractionEnabled = YES;
    _addressField.inputBlock.delegate = self;
    [_BZView addSubview:_addressField];
    [_addressField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BZView.mas_left);
        make.top.equalTo(_nameField.mas_bottom).offset(5*kRateSize);
        make.height.equalTo(@(40*kRateSize));
        make.right.equalTo(_BZView.mas_right);
    }];

    
    UIImageView *secondSideLine = [[UIImageView alloc] init];
    [secondSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [_BZView addSubview:secondSideLine];
    [secondSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@(280*kRateSize));
        make.height.mas_equalTo(kGrayLine);
        make.centerX.equalTo(_BZView.mas_centerX);
        make.top.equalTo(wself.addressField.mas_bottom);
    }];
    
    
    _teleField = [[GeneralInputBlock alloc] initWithTitle:@"联系电话：    " andPlaceholderText:@"请填写您的电话号码"];
    _teleField.userInteractionEnabled = YES;
    _teleField.inputBlock.delegate = self;
    [_BZView addSubview:_teleField];
    [_teleField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BZView.mas_left);
        make.top.equalTo(_addressField.mas_bottom).offset(5*kRateSize);
        make.height.equalTo(@(40*kRateSize));
        make.right.equalTo(_BZView.mas_right);
    }];
    
    UIImageView *thirdSideLine = [[UIImageView alloc] init];
    [thirdSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [_BZView addSubview:thirdSideLine];
    [thirdSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@(280*kRateSize));
        make.height.mas_equalTo(kGrayLine);
        make.centerX.equalTo(_BZView.mas_centerX);
        make.top.equalTo(wself.teleField.mas_bottom);
    }];
    
    
    _remarkField = [[GeneralInputBlock alloc] initWithTitle:@"备注(选填)：    " andPlaceholderText:@""];
    _remarkField.userInteractionEnabled = NO;
    _remarkField.inputBlock.delegate = self;
    [_BZView addSubview:_remarkField];
    [_remarkField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BZView.mas_left);
        make.top.equalTo(_teleField.mas_bottom).offset(5*kRateSize);
        make.height.equalTo(@(40*kRateSize));
        make.right.equalTo(_BZView.mas_right);
    }];
    
    
    
    _remarkContent = [[UITextView alloc]init];
//    [remarkContent setBackgroundColor:[UIColor whiteColor]];
    _remarkContent.layer.cornerRadius = 8;
    _remarkContent.layer.masksToBounds = YES;
    [_remarkContent.layer setBorderColor:UIColorFromRGB(0xeeeeee).CGColor];
    [_remarkContent.layer setBorderWidth:1];
    [_BZView addSubview:_remarkContent];
    
    [_remarkContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BZView.mas_left).offset(20*kRateSize);
        make.top.equalTo(_remarkField.mas_bottom).offset(5*kRateSize);
        make.height.equalTo(@(50*kRateSize));
        make.right.equalTo(_BZView.mas_right).offset(-20*kRateSize);
        
    }];
    
    
    
    _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];// buttonWithType:UIButtonTypeCustom];
    _confirmBtn.layer.borderWidth = 1;
    _confirmBtn.layer.cornerRadius = 8;
    _confirmBtn.layer.masksToBounds = YES;
    _confirmBtn.layer.borderColor = App_selected_color.CGColor;
    
    //    [_confirmButton setImage:[UIImage imageNamed:@"btn_sure_line-frame"] forState:UIControlStateNormal];
    [_confirmBtn setTitle:@"提交" forState:UIControlStateNormal];
    _confirmBtn.adjustsImageWhenHighlighted = NO;
    [_confirmBtn setBackgroundImage:[self imageWithColor:App_white_color] forState:UIControlStateNormal];
    [_confirmBtn setBackgroundImage:[self imageWithColor:App_selected_color] forState:UIControlStateHighlighted];
    [_confirmBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    [_confirmBtn setTitleColor:App_white_color  forState:UIControlStateHighlighted];
    [_BZView addSubview:_confirmBtn];
    
    
    [_confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_remarkContent.mas_bottom).offset(5*kRateSize);
        make.centerX.mas_equalTo(_BZView.mas_centerX);
//        make.size.mas_equalTo(CGSizeMake(generalBtnWidth-50*kRateSize, 40*kRateSize));
         make.size.mas_equalTo(CGSizeMake(150*kRateSize, 30*kRateSize));
    }];
    
    [_confirmBtn addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
    

}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}





-(void)cancelButton:(UIButton*)sender
{
    sender.selected = !sender.selected;
    if (self.cancelButtonClick) {
        self.cancelButtonClick(sender.selected);
       
    }
    
    [_nameField.inputBlock resignFirstResponder];
    [_addressField.inputBlock resignFirstResponder];
    [_teleField.inputBlock resignFirstResponder];
    [_remarkField.inputBlock resignFirstResponder];
    [_remarkContent resignFirstResponder];
}


-(void)confirmClick:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    if (isEmptyStringOrNilOrNull(_nameField.inputBlock.text)) {
        
      [AlertLabel AlertInfoWithText:@"姓名不能为空" andWith:self withLocationTag:0];
    }else if (isEmptyStringOrNilOrNull(_addressField.inputBlock.text)) {
        
        [AlertLabel AlertInfoWithText:@"地址不能为空" andWith:self withLocationTag:0];
    }else if (isEmptyStringOrNilOrNull(_teleField.inputBlock.text)) {
         [AlertLabel AlertInfoWithText:@"电话号码不能为空" andWith:self withLocationTag:0];
    }else if ( NotEmptyStringAndNilAndNull(_addressField.inputBlock.text) && NotEmptyStringAndNilAndNull(_teleField.inputBlock.text)) {
        
//        if ([CHECKFORMAT checkChineseName:_nameField.inputBlock.text]) {
//             
//            inRule = YES;
        
        
            if (self.confirmButtonClick) {
                self.confirmButtonClick(_nameField.inputBlock.text,_addressField.inputBlock.text,_teleField.inputBlock.text,_remarkContent.text,sender.selected);
                
            }
//        }else
//        {
//            [AlertLabel AlertInfoWithText:@"姓名不符合标准~" andWith:self withLocationTag:0];
//        }
        
        
    }
    
    [_nameField.inputBlock resignFirstResponder];
    [_addressField.inputBlock resignFirstResponder];
    [_teleField.inputBlock resignFirstResponder];
    [_remarkField.inputBlock resignFirstResponder];
    [_remarkContent resignFirstResponder];

}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_nameField.inputBlock resignFirstResponder];
    [_addressField.inputBlock resignFirstResponder];
    [_teleField.inputBlock resignFirstResponder];
    [_remarkField.inputBlock resignFirstResponder];
    [_remarkContent resignFirstResponder];


    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_nameField.inputBlock resignFirstResponder];
    [_addressField.inputBlock resignFirstResponder];
    [_teleField.inputBlock resignFirstResponder];
    [_remarkField.inputBlock resignFirstResponder];
    [_remarkContent resignFirstResponder];

    return YES;
}


- (void) changeContentViewPosition:(NSNotification *)notification{
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY = value.CGRectValue.origin.y;
    
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    if ([notification.name isEqualToString:@"UIKeyboardWillShowNotification"]) {
        [UIView animateWithDuration:duration.doubleValue animations:^{
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationCurve:[curve intValue]];
            // CGPointMake(_BZView.center.x, keyBoardEndY - STATUS_BAR_HEIGHT - self.view.bounds.size.height/2.0);
            
            
            [_BZView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.mas_centerX);
                make.centerY.mas_equalTo(keyBoardEndY - 144*kRateSize - KDeviceHeight/2.0);
                make.size.mas_equalTo(CGSizeMake(300*kRateSize, 280*kRateSize));
            }];
            
        }];

    }else
    {
        
        
        [UIView animateWithDuration:duration.doubleValue animations:^{
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationCurve:[curve intValue]];
            
            [_BZView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self.mas_centerY);
                make.centerX.mas_equalTo(self.mas_centerX);
                make.size.mas_equalTo(CGSizeMake(300*kRateSize, 280*kRateSize));
            }];
        }];

        

    }
    
    
    
    
    
    
}



@end
