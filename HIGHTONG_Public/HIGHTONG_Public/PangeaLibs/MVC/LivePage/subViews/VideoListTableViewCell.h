//
//  VideoListTableViewCell.h
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoListTableViewCell : UITableViewCell

@property (strong,nonatomic) UIImageView *channelLogo;
@property (strong,nonatomic) UILabel *serialLabel;
@property (strong,nonatomic) UILabel *channelNameLabel;
@property (strong,nonatomic) UILabel *channelTitleLabel;
@property (strong,nonatomic) UIImageView *seperateLine;
@property (strong,nonatomic) UIImageView *veticalLine;
@end
