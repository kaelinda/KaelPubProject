//
//  ChannelListTableViewCell.h
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
@interface ChannelListTableViewCell : UITableViewCell


@property (strong,nonatomic) UILabel *timeLabel;
@property (strong,nonatomic) UILabel *videoTitleLabel;
@property (strong,nonatomic) UIImageView *seperateLine;
@property(strong,nonatomic) UIButton_Block *playBtn;


@end
