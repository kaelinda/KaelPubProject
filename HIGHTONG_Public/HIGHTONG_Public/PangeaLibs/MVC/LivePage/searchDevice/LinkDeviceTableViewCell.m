//
//  LinkDeviceTableViewCell.m
//  HIGHTONG
//
//  Created by apple on 15/3/12.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "LinkDeviceTableViewCell.h"

@implementation LinkDeviceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(wself);
        
        
        [self.contentView setBackgroundColor:[UIColor clearColor]];

        
        
        
        _IPLabel=[[UILabel alloc]init];
//        _IPLabel.frame=CGRectMake(10, 5, 130, 45);
            _IPLabel.textColor=UIColorFromRGB(0xffffff);
            _IPLabel.font=[UIFont systemFontOfSize:12];
        _IPLabel.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:self.IPLabel];
        
        _linkBtn = [[LinkButton alloc]init];
//        _linkBtn.frame = CGRectMake((kDeviceWidth-76), 15, 66, 28);
        [self.contentView addSubview:self.linkBtn];
        
        
        
        _seperateLine = [[UIImageView alloc]init];
        [_seperateLine setImage:[UIImage imageNamed:@"ic_player_ip_seperate_line"]];
        [self.contentView addSubview:_seperateLine];
        
        
        
        [_IPLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(wself.mas_top).offset(-5*kRateSize
);
            make.size.mas_equalTo(CGSizeMake(93*kRateSize
, 40*kRateSize
));
            make.left.mas_equalTo(wself.mas_left).offset(10);
            
        }];
        
        
        
        [_seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
            
            
            make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(2*kRateSize
);
            make.left.mas_equalTo(wself.contentView.mas_left);
            make.right.mas_equalTo(wself.contentView.mas_right
);
            make.height.mas_equalTo(1*kRateSize
);
            
            
            
        }];
        
        
        [_linkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(wself.contentView.mas_right).offset(-10*kRateSize);
            make.size.mas_equalTo(CGSizeMake(41*kRateSize
, 19*kRateSize
));
            make.top.mas_equalTo(wself.mas_top).offset(5*kRateSize
);
        }];
        
        
        
        
        
        
        
        
        
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    
}

@end
