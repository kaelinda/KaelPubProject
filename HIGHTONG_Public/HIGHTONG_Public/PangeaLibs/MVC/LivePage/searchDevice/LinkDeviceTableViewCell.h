//
//  LinkDeviceTableViewCell.h
//  HIGHTONG
//
//  Created by apple on 15/3/12.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LinkButton.h"
@interface LinkDeviceTableViewCell : UITableViewCell



@property (strong,nonatomic)UILabel *IPLabel;
@property (strong,nonatomic)LinkButton *linkBtn;
@property (strong,nonatomic) UIImageView *seperateLine;

@end
