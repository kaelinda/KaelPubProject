//
//  ExtranetPromptViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface ExtranetPromptViewController : HT_FatherViewController

@property (nonatomic,copy)NSString *ExtranetPromptTitle;

@end
