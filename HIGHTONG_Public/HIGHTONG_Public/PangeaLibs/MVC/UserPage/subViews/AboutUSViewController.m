//
//  AboutUSViewController.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/18.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "AboutUSViewController.h"
#import "UrlPageJumpManeger.h"

@interface AboutUSViewController ()<UIWebViewDelegate>
{

    MBProgressHUD *_HUD;

}
@property (nonatomic,strong)UIButton *backBtn;

@end

@implementation AboutUSViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarHidden = NO;

    [MobCountTool pushInWithPageView:@"aboutUSPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"aboutUSPage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.urlLink.length<1 && _Ktitle.length==0) {
        [self setNaviBarTitle:@"关于我们"];
        _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBackVC)];
        [self setNaviBarLeftBtn:_backBtn];
        [self setNaviBarRightBtn:nil];
        
        WS(wss);
        
        UIWebView *webView = [UIWebView new];
        [self.view addSubview:webView];
        
        
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(44);
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];
        
        
        
        [(UIScrollView *)[[webView subviews] objectAtIndex:0] setBounces:NO];
        
        
//        NSString *htmlPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"about_us2.html"];
//        NSString *htmlString = [[NSString alloc] initWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
//        [webView loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
       
        NSString *urlStr = [UrlPageJumpManeger aboutUsPageJump];
        
//        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
//        [webView loadRequest:request];
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
        [webView loadRequest:request];
        
    }else{
        

        WS(wss);
        
        UIWebView *webView = [UIWebView new];
        [self.view addSubview:webView];
        [self.view setBackgroundColor:App_selected_color];//Kael_change
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(20);//Kael_change
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];

        
        [(UIScrollView *)[[webView subviews] objectAtIndex:0] setBounces:NO];
        
        webView.delegate = self;
//        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlLink]];
//        [webView loadRequest:request];
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlLink] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
        [webView loadRequest:request];
        
//        _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        //显示的文字
//        _HUD.labelText = @"加载中...";
//        //是否有庶罩
//        _HUD.dimBackground = NO;
//        [_HUD show:YES];
        [self showCustomeHUD];
    
        if (_Ktitle.length==0) {
            [self hideNaviBar:YES];
            _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [_backBtn setImage:[UIImage imageNamed:@"btn_ad_black_back"] forState:UIControlStateNormal];
            [_backBtn addTarget:self action:@selector(goBackVC) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:_backBtn];
            
            [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(CGSizeMake(30, 30));
                make.top.mas_equalTo(wss.view).offset(40);
                make.left.mas_equalTo(wss.view).offset(20);
                
            }];

        }else{
            [self setNaviBarTitle:@"微医院"];
            [webView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(wss.m_viewNaviBar.mas_bottom).offset(-20);
                make.width.equalTo(wss.view.mas_width);
                make.centerX.equalTo(wss.view.mas_centerX);
                make.bottom.equalTo(wss.view.mas_bottom);
            }];
            _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBackVC)];
            [self setNaviBarLeftBtn:_backBtn];
            [self setNaviBarRightBtn:nil];
        }
        
    }
   
}

- (void)goBackVC
{
    [self hiddenCustomHUD];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"加载完毕");
//    if (_HUD) {
//        [_HUD hide:YES];
//    }
    [self hiddenCustomHUD];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSURL * url = [request URL];
    if ([[url scheme] isEqualToString:@"firstclick"]) {
        NSArray *params =[url.query componentsSeparatedByString:@"&"];
        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
        for (NSString *paramStr in params) {
            NSArray *dicArray = [paramStr componentsSeparatedByString:@"="];
            if (dicArray.count > 1) {
                NSString *decodeValue = [dicArray[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; [tempDic setObject:decodeValue forKey:dicArray[0]];
            }
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"方式一" message:@"这是OC原生的弹出窗" delegate:self cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alertView show];
        NSLog(@"tempDic:%@",tempDic); return NO;
    }
    return YES;
}

@end
