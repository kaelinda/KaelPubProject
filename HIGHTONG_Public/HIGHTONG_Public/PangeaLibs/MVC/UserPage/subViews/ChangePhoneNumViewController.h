//
//  ChangePhoneNumViewController.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "VerityAlertView.h"
#import "ErrorAlertView.h"

@interface ChangePhoneNumViewController : HT_FatherViewController

@property (nonatomic,strong)GeneralInputBlock *phoneNumField;
@property (nonatomic,strong)GeneralInputBlock *recentPhoneNum;
@property (nonatomic,strong)GeneralInputBlock *pwdNum;
@property (nonatomic,strong)GeneralInputBlock *verityNum;
@property (nonatomic,strong)UIButton          *verityBtn;//获取验证码按钮
@property (nonatomic,strong)VerityAlertView   *verityPost;
@property (nonatomic,strong)ErrorAlertView    *errorView;


@property (nonatomic, strong)UIView           *backView;
@property (nonatomic, strong)UILabel          *currentNumLab;
@property (nonatomic, strong)UILabel          *tipsLab;

@end
