//
//  ForgetPwdViewController.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "VerityAlertView.h"
#import "ErrorAlertView.h"

@interface ForgetPwdViewController : HT_FatherViewController

@property (nonatomic, strong)UIButton *backBtn;
@property (nonatomic, strong)GeneralInputBlock *phoneField;
@property (nonatomic, strong)GeneralInputBlock *verityField;
@property (nonatomic, strong)GeneralInputBlock *PwdField;
@property (nonatomic, strong)UIButton *verityBtn;
@property (nonatomic, strong)UILabel *verityLab;
@property (nonatomic, strong)VerityAlertView *verityPost;
@property (nonatomic, strong)ErrorAlertView *errorView;


@end
