//
//  MineCollectViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineCollectViewController.h"
#import "LoginViewController.h"
#import "MyGesture.h"
#import "CalculateTextWidth.h"
#import "MineVodTableViewCell.h"
#import "CustomAlertView.h"
#import "DataPlist.h"
#import "DemandVideoPlayerVC.h"
#import "MyGesture.h"
#import "MBProgressHUD.h"
#import "SelectAndDeleteView.h"
#import "HTVideoAVPlayerVC.h"
#import "LocalCollectAndPlayRecorder.h"


#define TV_ID @"contentID"

@interface MineCollectViewController ()<UIScrollViewDelegate,HTRequestDelegate,UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    // 跳到  小屏幕 所需数据
    NSString *_channelID;     // 频道编号
    NSString *_channelName;   // 频道名称
    NSString *_PlayTag;       //是否可直播
    NSString *_PlayBackTag;   //是否可回看
    NSString *_eventId;   // 节目 id
    NSString *_eventName;   //节目名称
    NSString *_channelImageLink;//频道图标链接
    NSString *_videoTime;// "2014-09-22 11:50";
    
    
    BOOL toDemandPlayer;//防止多次点击点播cell，多次push
    BOOL _IS_SUPPORT_VOD;//是否支持点播
}

@property (nonatomic,strong)UIButton *backBtn;//返回按钮
@property (nonatomic,strong)UIButton *cleanBtn;//垃圾桶按钮
@property (nonatomic,strong)UIImageView *deleteView;//删除-清空背景view
@property (nonatomic,strong)UILabel *removeAllLab;//清空按钮
@property (nonatomic,strong)SelectAndDeleteView *selectAndDeleteView;
@property (nonatomic,strong)NSMutableArray *deleteArr;
@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation MineCollectViewController
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSMutableArray *)vodArr
{
    if (!_vodArr) {
        _vodArr = [NSMutableArray array];
    }
    
    return _vodArr;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    
    [self refreshCollectTableCell];

    [MobCountTool pushInWithPageView:@"MineCollectPage"];
    
    toDemandPlayer = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineCollectPage"];
}


- (void)refreshCollectTableCell
{
    _vodArr = [NSMutableArray arrayWithArray:[_localListCenter getVODFavoriteListWithUserType:IsLogin]];

    [self hiddenCustomHUD];
    [self hideLoading];
    
    [_vodTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];

    _deleteArr = [NSMutableArray array];
    
    _selAllBtnIsClick = NO;
    
    __vodIsEditing = NO;
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    
    [self setNaviBarTitle:_controllerTitleStr];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    _cleanBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_del_normal" imgHighlight:@"btn_user_cancel" imgSelected:@"btn_user_cancel" target:self action:@selector(dustbinBtnSenderDown:)];
    
    [self setNaviBarRightBtn:_cleanBtn];
    
    _cleanBtn.hidden = YES;

    [self makeTheMainPartUI];
 
    [_vodTable reloadData];

    [self showCustomeHUD];
}

- (void)hideLoading
{
    if (_vodArr.count>0) {
        
        _noVODDataLabel.hidden = YES;
        
        _cleanBtn.hidden = NO;
    }else{
        
        _noVODDataLabel.hidden = NO;
        
        _cleanBtn.hidden = YES;
    }
}

- (void)makeTheMainPartUI
{
    NSString *noRecorderStr;
    NSString *noRecorderImg;
    
    noRecorderStr = @"暂无收藏记录";
    noRecorderImg = @"bg_user_collection_metadata";
    
    _noVODDataLabel = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 200*kDeviceRate+64+kTopSlimSafeSpace, kDeviceWidth, 88*kDeviceRate) andImgName:noRecorderImg andTitle:noRecorderStr];
    _noVODDataLabel.hidden = YES;
    [self.view addSubview:_noVODDataLabel];
    
    
    //点播table
    _vodTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace)];
    _vodTable.delegate = self;
    _vodTable.dataSource = self;
    _vodTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _vodTable.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_vodTable];
    _vodTable.showsHorizontalScrollIndicator = NO;
    _vodTable.showsVerticalScrollIndicator = NO;
    
    
    //点击垃圾桶按钮后出现的view
    _selectAndDeleteView = [[SelectAndDeleteView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-60*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 60*kDeviceRate+kBottomSafeSpace)];
    [_selectAndDeleteView setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:_selectAndDeleteView];
    _selectAndDeleteView.hidden = YES;
    _selectAndDeleteView.userInteractionEnabled = YES;
    
    [_selectAndDeleteView.selectBtn addTarget:self action:@selector(selectAllItemsButSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    [_selectAndDeleteView.deleteBtn addTarget:self action:@selector(deleteBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark 全选按钮触发方法
- (void)selectAllItemsButSenderDown:(VODDeleteButton *)btn
{
    NSLog(@"我是全选按钮");
    
    btn.selected = !btn.selected;
    
    if (btn.selected) {//全选
        
        [_deleteArr removeAllObjects];
        
        [_deleteArr addObjectsFromArray:_vodArr];
        
        NSLog(@"%@",_vodArr);
        _selAllBtnIsClick = YES;
        
        [_vodTable reloadData];
        
        [self cellCheckDataListSelected];
        
        _selectAndDeleteView.selectLab.text = @"取消全选";
        
    }else{//取消全选
        
        [self.deleteArr removeAllObjects];
        
        _selAllBtnIsClick = NO;
        
        [_vodTable reloadData];
        
        [self cellCheckDataListSelected];
        
        _selectAndDeleteView.selectLab.text = @"全选";
    }
}

#pragma 删除按钮触发方法
- (void)deleteBtnSenderDown:(VODDeleteButton *)btn
{
    NSLog(@"我是删除按钮");
    
    btn.selected = !btn.selected;
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    if (_selAllBtnIsClick) {//全选的删除
        
        [request DELVODFavoryteWithContentID:@"" andTimeout:10];

    }else{//正常的选择删除
        
        if (_deleteArr.count==0) {
            
            return;
        }
        
        NSMutableArray *eventIDArr = [NSMutableArray array];
        
        for (NSDictionary *dic in _deleteArr ) {
            
            if ([dic objectForKey:TV_ID]) {
                
                [eventIDArr addObject:[dic objectForKey:TV_ID]];
            }
        }
        
        NSString *eventIDStr = [NSString string];
        for (int i = 0; i<eventIDArr.count; i++) {
            
            if ([eventIDStr isEqualToString:@""]) {
                
                eventIDStr = [NSString stringWithFormat:@"%@",eventIDArr[i]];
            }else{
                
                eventIDStr = [NSString stringWithFormat:@"%@,%@",eventIDStr,eventIDArr[i]];
            }
        }
        
        [eventIDArr removeAllObjects];
        
        [request DELVODFavoryteWithContentID:eventIDStr andTimeout:10];

    }
}

#pragma mark 请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    [self hiddenCustomHUD];

    if ([type isEqualToString:VOD_NewFavorateList]){//点播收藏列表
        
        NSLog(@"点播收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (_IS_SUPPORT_VOD) {//有点播的时候  YSE
            
            if (result.count>0) {
                
                if ([[result objectForKey:@"count"] integerValue] > 0) {//++++成功
                    
                    self.vodArr = [NSMutableArray arrayWithArray:[result objectForKey:@"vodList"]];
                    
                    [self hiddenCustomHUD];
                    [self hideLoading];
                    
                    [_vodTable reloadData];
                    
                }else if ([[result objectForKey:@"count"] integerValue]==0){
                    
                    NSLog(@"查询结果为空");
                    
                    [self hiddenCustomHUD];
                    [self hideLoading];
                    
                    return;
                    
                }else if ([[result objectForKey:@"count"] integerValue]==-2){
                    
                    NSLog(@"用户未登录或者登录超时");
                    
                    [self hiddenCustomHUD];
                    [self hideLoading];
                    
                    return;
                    
                }else if ([[result objectForKey:@"count"] integerValue]==-9){
                    
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    [self hiddenCustomHUD];
                    [self hideLoading];
                    
                    return;
                    
                }else{
                    
                    [self hiddenCustomHUD];
                    [self hideLoading];
                    
                    return;
                }
            }

        }else{//无点播模块的时候  ---NO
            
            NSLog(@"查询结果为空");
            
            [self hiddenCustomHUD];
            [self hideLoading];
            
            return;
            
        }
        
    }
    else if ([type isEqualToString:DEL_VOD_FAVORITE]){//删除点播收藏
        
        NSLog(@"点播播放记录列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 1:{
                    NSLog(@"未收藏该节目或者已经取消收藏");
                    break;
                }
                case 0:{
                    NSLog(@"成功");
                    
                    if (_selAllBtnIsClick) {//清空点播收藏列表
                        
                        [_vodArr removeAllObjects];
                                                
                        _selAllBtnIsClick = NO;
                        
                        [_localListCenter multiDeleteVODFavoriteWithDeleteArr:[NSArray arrayWithArray:_deleteArr] andUserType:IsLogin andKey:@"contentID"];
                        
                        [_deleteArr removeAllObjects];
                        
                        [self cellCheckDataListSelected];
                        
                        _vodArr = [NSMutableArray arrayWithArray:[_localListCenter getVODFavoriteListWithUserType:IsLogin]];
                    }
                    
                    if ([_vodArr count] > 0) {
                        
                        [_noVODDataLabel setHidden:YES];
                        
                    }else{
                        
                        [_noVODDataLabel setHidden:NO];
                        
                        _cleanBtn.hidden = YES;
                        

                        _selectAndDeleteView.hidden = YES;
                        
                        _cleanBtn.selected = !_cleanBtn.selected;
                        
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_DEMAND_LIST object:nil];
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误");
                    break;
                }
                    
                default:
                    break;
            }
        }

        [_vodTable reloadData];
    }
    else if ([type isEqualToString:YJ_CDN_VIDEO_LISTFAVORITE]){//我的医视频
        
        NSLog(@"我的医视频请求成功结果:%ld result:%@  type:%@",(long)status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {//++++成功
                
                [self.vodArr removeAllObjects];
                
                self.vodArr = [result objectForKey:@"list"];
                
                [self hiddenCustomHUD];
                [self hideLoading];
                
                [_vodTable reloadData];
                
            }else{
                
                [self hiddenCustomHUD];
                [self hideLoading];
                
                return;
            }
        }
    }
    else if ([type isEqualToString:YJ_CDN_VIDEO_CLEANFAVORITE]){//3.8	清空收藏夹
        NSLog(@"清空医视频收藏夹请求成功结果: %ld result:%@  type:%@",(long)status,result,type );
        
        if (result.count>0) {
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                NSLog(@"成功");

                [_vodArr removeAllObjects];
                
                _selAllBtnIsClick = NO;
                
                [_noVODDataLabel setHidden:NO];
                
                _cleanBtn.hidden = YES;
                
                _selectAndDeleteView.hidden = YES;
                
                _cleanBtn.selected = !_cleanBtn.selected;
                
                [_vodTable reloadData];

            }
        }
    }
    else if ([type isEqualToString:YJ_CDN_VIDEO_DELFAVORITE]){//3.7	删除收藏夹中的记录
    
        NSLog(@"删除医视频收藏夹请求成功结果: %ld result:%@  type:%@",(long)status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                 NSLog(@"成功");
                
                WS(wself);
                
                for (NSDictionary *deleDic in _deleteArr ) {
                    
                    [_vodArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        
                        if ([[obj objectForKey:@"videoId"] isEqualToString:[deleDic objectForKey:@"videoId"]]) {
                            *stop = YES;
                        }
                        
                        if (*stop == YES) {
                            
                            [wself.vodArr removeObject:obj];
                        }
                    }];
                }
                
                [_deleteArr removeAllObjects];
                
                [self cellCheckDataListSelected];
                
                if ([_vodArr count] > 0) {
                    
                    [_noVODDataLabel setHidden:YES];
                    
                }else{
                    
                    [_noVODDataLabel setHidden:NO];
                    
                    _cleanBtn.hidden = YES;
                    
                    _selectAndDeleteView.hidden = YES;
                    
                    _cleanBtn.selected = !_cleanBtn.selected;
                    
                }
                
                [_vodTable reloadData];

            }
        }
    }
    else{
        
    }
}


#pragma mark tableview delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _mediaHight = 115*kDeviceRate;

    return _mediaHight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_vodArr count]%2 == 0) {
        
        _mediaCount = [self.vodArr count]/2;
    }
    else
    {
        _mediaCount = [self.vodArr count]/2+1;
    }
    
    return _mediaCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *vim = @"vim";
    
    MineVodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:vim];
    
    if (!cell) {
        
        cell = [[MineVodTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:vim];
        
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
    if (_vodArr.count>=(1+indexPath.row*2))
    {
        
        NSDictionary *dicleft = [NSDictionary dictionaryWithDictionary:[self.vodArr objectAtIndex:2*indexPath.row]];
        
        [cell.leftView.bgV setBackgroundColor:App_white_color];
        
        if ([dicleft valueForKey:@"name"]) {
            
            cell.leftView.contentNameL.text = [dicleft valueForKey:@"name"];
        }else{
            
            cell.leftView.contentNameL.text = @"";
        }
        
        if ([dicleft valueForKey:@"imageLink"]) {
            
            [cell.leftView.imageV sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dicleft valueForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
            
            cell.leftView.highLigthBtn.hidden = NO;
        }else{
            
            cell.leftView.highLigthBtn.hidden = YES;
        }
        
        
        if ([dicleft valueForKey:@"type"]) {
            
            NSString *typeStr = [dicleft valueForKey:@"type"];
            
            if ([typeStr isEqualToString:@"9"]) {//多集
                
                if ([dicleft valueForKey:@"lastEpisodeTitle"]) {
                    
                    cell.leftView.timeLabel.text = [dicleft valueForKey:@"lastEpisodeTitle"];
                }
            }
            else if ([typeStr isEqualToString:@"0"]){//单集
                
                cell.leftView.timeLabel.text = @"";
            }else{
                
                cell.leftView.timeLabel.text = @"";
            }
        }
        
        
        if (__vodIsEditing == YES) {

            cell.leftView.deleteImg.hidden = NO;
            
        }else{
            cell.leftView.deleteImg.hidden = YES;
        }
        
        
        if (_selAllBtnIsClick) {
            
            [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        }else{
        
            [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        }
        
        
        if (_deleteArr.count > 0) {
            
            NSString *selectID = [dicleft objectForKey:TV_ID];
            
            for (NSDictionary *selectTempDic in self.deleteArr) {
                
                if ([selectID isEqualToString:[selectTempDic objectForKey:TV_ID]]) {
                    
                    [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                }
            }
        }

        
        cell.leftView.highLigthBtn.tag = 2*indexPath.row;
        [cell.leftView.highLigthBtn addTarget:self action:@selector(goToVODTV:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        
        [cell.leftView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        [cell.leftView.bgV setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        [cell.leftView.imageV setBackgroundColor:[UIColor clearColor]];
        cell.leftView.contentNameL.text = @"";
        cell.leftView.timeLabel.text = @"";
        [cell.leftView.contentNameL setBackgroundColor:[UIColor clearColor]];
        cell.leftView.imageV.image = nil;
        [cell.leftView.timeLabel setBackgroundColor:[UIColor clearColor]];
        cell.leftView.deleteBtn.hidden = YES;
        cell.leftView.timeBackImg.hidden = YES;
        cell.leftView.highLigthBtn.hidden = YES;
        cell.leftView.timeLabel.text = @"";
        cell.leftView.deleteImg.image = nil;
    }
    
    if (_vodArr.count>=(2+indexPath.row*2)) {
        
        NSDictionary *dicright = [NSDictionary dictionaryWithDictionary:[self.vodArr objectAtIndex:2*indexPath.row+1]];
        [cell.rightView.bgV setBackgroundColor:App_white_color];
        
        if ([dicright valueForKey:@"imageLink"]) {
            
            [cell.rightView.imageV sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dicright valueForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
            
            cell.rightView.highLigthBtn.hidden = NO;
        }else{
            
            cell.rightView.highLigthBtn.hidden = YES;
        }
        
        
        if ([dicright valueForKey:@"name"]) {
            
            cell.rightView.contentNameL.text = [dicright valueForKey:@"name"];
        }else{
            cell.rightView.contentNameL.text = @"";
        }
        
        
        if ([dicright valueForKey:@"type"]) {
            
            NSString *typeStr = [dicright valueForKey:@"type"];
            
            if ([typeStr isEqualToString:@"9"]) {//多集
                
                if ([dicright valueForKey:@"lastEpisodeTitle"]) {
                    
                    cell.rightView.timeLabel.text = [dicright valueForKey:@"lastEpisodeTitle"];
                }
            }else if ([typeStr isEqualToString:@"0"]){//单集
                
                cell.rightView.timeLabel.text = @"";
            }else{
                
                cell.rightView.timeLabel.text = @"";
            }
        }
        
        if (__vodIsEditing == YES) {
            cell.rightView.deleteImg.hidden = NO;

        }else{
            
            cell.rightView.deleteImg.hidden = YES;
        }
        
        
        if (_selAllBtnIsClick) {
            [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        }else{
            
            [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        }
        
        if (self.deleteArr.count > 0) {
            
            NSString *selectID = [dicright objectForKey:TV_ID];
            
            for (NSDictionary *selectTempDic in self.deleteArr) {
                
                if ([selectID isEqualToString:[selectTempDic objectForKey:TV_ID]]) {
                    
                    [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                }
            }
            
        }
        
        cell.rightView.highLigthBtn.tag = 2*indexPath.row+1;
        [cell.rightView.highLigthBtn addTarget:self action:@selector(goToVODTV:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        
        [cell.rightView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        [cell.rightView.bgV setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        [cell.rightView.imageV setBackgroundColor:[UIColor clearColor]];
        cell.rightView.timeLabel.text = @"";
        cell.rightView.contentNameL.text = @"";
        cell.rightView.imageV.image = nil;
        [cell.rightView.contentNameL setBackgroundColor:[UIColor clearColor]];
        [cell.rightView.timeLabel setBackgroundColor:[UIColor clearColor]];
        cell.rightView.deleteBtn.hidden = YES;
        cell.rightView.timeBackImg.hidden = YES;
        cell.rightView.highLigthBtn.hidden = YES;
        cell.rightView.deleteImg.image = nil;
    }
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_vodArr.count) {
        
        _noVODDataLabel.hidden = YES;
        
    }
}

#pragma mark 点播cell点击事件 多选删除---进入点播播放器
- (void)goToVODTV:(VODDeleteButton *)ges
{
    if (__vodIsEditing == YES) {
        
        WS(wself);
        ges.selected = !ges.selected;
        
        NSDictionary *selDic = [_vodArr objectAtIndex:ges.tag];
        
        if (ges.selected) {
            
            if (_deleteArr.count==0) {
                
                [_deleteArr addObject:selDic];
            }else{
                
                __block BOOL isDel;
                
                isDel = NO;
                
                [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    if ([[obj objectForKey:TV_ID] isEqualToString:[selDic objectForKey:TV_ID]]) {
                        
                        *stop = YES;
                        
                        if (*stop == YES) {
                            
                            [wself.deleteArr removeObject:obj];
                            
                            ges.selected = NO;
                            
                            isDel = YES;
                        }
                    }
                    
                }];
                
                if (!isDel) {
                    [_deleteArr addObject:selDic];
                }

            }
            
        }else{
            
            [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                if ([[obj objectForKey:TV_ID] isEqualToString:[selDic objectForKey:TV_ID]]) {
                    *stop = YES;
                    
                    if (*stop) {
                        
                        [wself.deleteArr removeObject:obj];
                    }
                }
                
            }];
        }
        
        
        [self cellCheckDataListSelected];
        
        if (_vodArr.count == _deleteArr.count) {
            
            _selectAndDeleteView.selectBtn.selected = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
            
            _selAllBtnIsClick = YES;
            
        }else{
            
            _selectAndDeleteView.selectBtn.selected = NO;
            
            _selectAndDeleteView.selectLab.text = @"全选";
            
            _selAllBtnIsClick = NO;
        }

        [_vodTable reloadData];
        
    }else{
        
        if (toDemandPlayer) {
            
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc] init];
            
            demandVC.contentID = [_vodArr[ges.tag] objectForKey:@"contentID"];
            
            demandVC.contentTitleName = [_vodArr[ges.tag] objectForKey:@"name"];
            demandVC.TRACKID = @"2";
            demandVC.TRACKNAME = @"我的";
            demandVC.EN = @"1";
            [self.navigationController pushViewController:demandVC animated:YES];
            
            toDemandPlayer = NO;
        }
    }
}


#pragma mark 根据deleteArr中的数据修改删除按钮状态
- (void)cellCheckDataListSelected
{
    if (self.deleteArr.count >0) {

        _selectAndDeleteView.deleteBtn.enabled = YES;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"sl_user_playrecord_del_item_normal"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = [NSString stringWithFormat:@"删除( %ld )",_deleteArr.count];
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_REMIND;//UIColorFromRGB(0xff9c00);
        
    } else {

        _selectAndDeleteView.deleteBtn.enabled = NO;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"btn_user_disable_click"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = @"删除";
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_FONT_WARNING;//UIColorFromRGB(0xcccccc);
    }
}


#pragma mark 导航条上垃圾桶按钮点击事件
- (void)dustbinBtnSenderDown:(UIButton *)sender
{
    
    sender.selected = !sender.selected;
    
    NSLog(@"+++++++++++++++++++清空啦");
    
    WS(wself);
    
    if (sender.selected) {//垃圾桶
        _selectAndDeleteView.hidden = NO;
        
        __vodIsEditing = YES;
        
        [_vodTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wself.view.mas_width);
            make.centerX.equalTo(wself.view.mas_centerX);
            make.top.equalTo(wself.view.mas_top).offset(64+kTopSlimSafeSpace);
            make.bottom.equalTo(wself.view.mas_bottom).offset(-60*kDeviceRate-kBottomSafeSpace);
        }];

    }else{//取消
        _selectAndDeleteView.hidden = YES;
        _selectAndDeleteView.selectLab.text = @"全选";

        if (_selectAndDeleteView.selectBtn.selected) {
            
            _selectAndDeleteView.selectBtn.selected = NO;
        }
        
        __vodIsEditing = NO;
        
        _selAllBtnIsClick = NO;
        
        [_deleteArr removeAllObjects];
        
        [self cellCheckDataListSelected];
        
        [_vodTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wself.view.mas_width);
            make.centerX.equalTo(wself.view.mas_centerX);
            make.top.equalTo(wself.view.mas_top).offset(64+kTopSlimSafeSpace);
            make.bottom.equalTo(wself.view.mas_bottom).offset(-kBottomSafeSpace);
        }];
    }
    
    [_vodTable reloadData];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
