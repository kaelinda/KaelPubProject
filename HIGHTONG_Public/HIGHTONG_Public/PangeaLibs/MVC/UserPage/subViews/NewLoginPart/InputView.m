//
//  InputView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "InputView.h"
#import "UserTime.h"
#import "CHECKFORMAT.h"
#import "CustomAlertView.h"
#import "HTRequest.h"

@interface InputView ()<CustomAlertViewDelegate,HTRequestDelegate>//UITextFieldDelegate,

@property (nonatomic, strong)CustomAlertView *customView;

@end

@implementation InputView

- (instancetype)initWithIsHasAuthCode:(BOOL)isHasAuthCode withPlaceholderStr:(NSString *)holderStr withLeftImageStr:(NSString *)leftStr withBackImageStr:(NSString *)backStr
{
    self = [super init];
    
    if (self) {
        
        _isHasAuthCode = isHasAuthCode;
        
        _placeholderStr = holderStr;
        _leftImageStr = leftStr;
        _backImageStr = backStr;
        
        [self setUpUI];
        
        _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
    }
    return self;
}

- (void)sendAuthCodeWithUserPhoneNum:(NSString *)phoneNum
{
    _userPhoneNum = phoneNum;
    
    if ([_userPhoneNum isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];

        return;
    }
    
    NSLog(@"+++++++++++++++++++++++发送验证码");
    
    if ([CHECKFORMAT checkPhoneNumber:_userPhoneNum])
    {
//        _phonenumberOK = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _authCodeBtn.enabled = NO;
        });
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"4" andMobilePhone:_userPhoneNum andTimeout:KTimeout];
        
    }else{
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];

    }
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//       
//        _authCodeBtn.enabled = YES;
//    });
    

}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        
        
        NSLog(@"动态口令登录返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    [UserTime changBtn:self.authCodeBtn withLab:self.authCodeLab withSleepSecond:60];
                    
                    ZSToast(@"已发送至手机，请注意查收")
                    
                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");

                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");

                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");

                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    

                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");

                    _customView.messageStr = @"该手机号已注册";
                    
                    [_customView show];

                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");

                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];
                    

                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
            ZSToast(@"请检查网络设置")
        }
        
        if ([[result objectForKey:@"ret"] integerValue] != 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _authCodeBtn.enabled = YES;
            });
        }
    }
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        
//        [_customView resignKeyWindow];
//        _customView = nil;
        
        _customView.hidden = YES;
    }
    return;
}



- (void)setUpInputViewFrame
{
    WS(wSelf);
    
    [_backImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(285);
        make.height.mas_equalTo(42);
        make.center.mas_equalTo(wSelf);
    }];
    
    [_leftLogoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(25*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        make.left.mas_equalTo(wSelf.backImage.mas_left).offset(17*kDeviceRate);
        make.centerY.mas_equalTo(wSelf.backImage.mas_centerY);
    }];
    
    [_authCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.mas_equalTo(wSelf.backImage);
        make.width.mas_equalTo(102);
        make.right.mas_equalTo(wSelf.backImage.mas_right).offset(-1);
    }];
    
    [_authCodeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wSelf.authCodeBtn);
    }];
    
    if (_isHasAuthCode) {
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wSelf.leftLogoImage.mas_right).offset(8*kDeviceRate);
            make.height.centerY.mas_equalTo(wSelf.backImage);
            make.right.mas_equalTo(wSelf.authCodeBtn.mas_left);
        }];
    }else{
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wSelf.leftLogoImage.mas_right).offset(8*kDeviceRate);
            make.height.centerY.mas_equalTo(wSelf.backImage);
            make.right.mas_equalTo(wSelf.backImage.mas_right);
        }];
    }
    
    
    [_placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wSelf.textField);
    }];
    
    _authCodeBtn.titleLabel.text = @"获取验证码";

}

- (void)authCodeBtnSenderDown:(UIButton *)btn
{
    NSLog(@"点击了验证码验证码");
    if (self.authCodeBtnBlock) {
        _authCodeBtnBlock(btn);
    }
}

- (void)setUpUI
{
    self.backImage = [[UIImageView alloc] init];
    self.textField = [[UITextField alloc] init];
    self.leftLogoImage = [[UIImageView alloc] init];
    self.authCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.authCodeLab = [[UILabel alloc] init];
    self.placeholderLab = [[UILabel alloc] init];
    
    
    [self addSubview:self.backImage];
    [self.backImage addSubview:self.leftLogoImage];
    [self.backImage addSubview:self.textField];
    [self.textField addSubview:self.placeholderLab];
    [self.backImage addSubview:self.authCodeBtn];
    [self.authCodeBtn addSubview:self.authCodeLab];
    
    _backImage.userInteractionEnabled = YES;
    
    
//    _textField.delegate = self;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _textField.textAlignment = NSTextAlignmentLeft;
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.keyboardType = UIKeyboardTypeNumberPad;//UIKeyboardTypePhonePad;
    _textField.textColor = HT_COLOR_FONT_FIRST;
    _textField.font = HT_FONT_SECOND;
    [_textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    _placeholderLab.font = HT_FONT_FOURTH;
    _placeholderLab.textColor = HT_COLOR_FONT_SECOND;
    _placeholderLab.textAlignment = NSTextAlignmentLeft;
    
    
    _authCodeBtn.adjustsImageWhenHighlighted = NO;
    [_authCodeBtn setImage:[UIImage imageNamed:@"btn_login_dynamic_tv_auth_code_normal"] forState:UIControlStateNormal];
    [_authCodeBtn addTarget:self action:@selector(authCodeBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _authCodeLab.font = HT_FONT_FOURTH;
    _authCodeLab.textAlignment = NSTextAlignmentCenter;
    _authCodeLab.textColor = HT_COLOR_FONT_INVERSE;
    _authCodeLab.text = @"获取验证码";
    
//    _authCodeBtn.titleLabel.font = HT_FONT_FOURTH;
//    _authCodeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    _authCodeBtn.titleLabel.textColor = HT_COLOR_FONT_INVERSE;
//    _authCodeBtn.backgroundColor = App_selected_color;

    
    if (_isHasAuthCode) {
        _authCodeBtn.hidden = NO;
    }else{
        _authCodeBtn.hidden = YES;
    }
    
    [_backImage setImage:[UIImage imageNamed:_backImageStr]];
    [_leftLogoImage setImage:[UIImage imageNamed:_leftImageStr]];
    _placeholderLab.text = _placeholderStr;

}

- (void)inputBlockValueChange:(UITextField *)textField
{
    if (textField.text.length>0) {
        
        self.placeholderLab.hidden = YES;
        
    }else{
        self.placeholderLab.hidden = NO;
    }

}

//#pragma mark textfield代理
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [_textField resignFirstResponder];
//
//    return YES;
//}


@end
