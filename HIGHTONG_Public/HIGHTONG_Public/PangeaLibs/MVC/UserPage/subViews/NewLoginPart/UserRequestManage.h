//
//  UserRequestManage.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/28.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^UserRequestManageBlock)(BOOL flag);

typedef void(^UserManageTokenBlock)(NSString *token);


@interface UserRequestManage : NSObject


@property (nonatomic, assign) BOOL flag;

@property (nonatomic, copy)UserRequestManageBlock requestReturnBlock;//动态登陆接口调用成功后的回调（显示吐司“正在登陆 请稍后”内容的）

@property (nonatomic, copy)UserManageTokenBlock tokenBlock;//用户真正登录成功的回调（获取鉴权token成功后的）

@property (nonatomic, copy)UserRequestManageBlock isDocBlock;

- (void)authCodeLoginRequestManagerWithPhoneNum:(NSString *)phoneNum withAuthCode:(NSString *)authCode withProtocal:(NSString *)protocal;

@end
