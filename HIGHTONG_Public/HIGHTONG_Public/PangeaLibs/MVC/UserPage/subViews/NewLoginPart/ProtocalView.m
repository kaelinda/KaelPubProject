//
//  ProtocalView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "ProtocalView.h"
#import "MyGesture.h"

@interface ProtocalView ()
{
    NSString *_isSelect;
}
@end

@implementation ProtocalView

- (id)init
{
    self = [super init];
    if (self) {
        WS(wSelf);
        
        _protocalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_protocalBtn setImage:[UIImage imageNamed:@"btn_user_radio_normal_2"] forState:UIControlStateNormal];//此处应该放默认图片(未选中)
        [_protocalBtn setImage:[UIImage imageNamed:@"btn_user_radio_pressed_2"] forState:UIControlStateSelected];//选中时
        [_protocalBtn addTarget:self action:@selector(ifAgreement:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_protocalBtn];
        
        [_protocalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(14*kDeviceRate);
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
        }];

        // 默认
        _protocalBtn.selected = YES;
        _isSelect = @"1";
        
        _protocalLab = [[UILabel alloc] init];
        [self addSubview:_protocalLab];
        _protocalLab.text = @"我已同意该应用的服务协议";
        _protocalLab.textAlignment = NSTextAlignmentLeft;
        _protocalLab.textColor = HT_COLOR_FONT_SECOND;
        _protocalLab.font = HT_FONT_FOURTH;
        _protocalLab.userInteractionEnabled = YES;
        [_protocalLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wSelf.protocalBtn.mas_right).offset(5*kDeviceRate);
            make.centerY.mas_equalTo(wSelf.protocalBtn.mas_centerY);
            make.height.mas_equalTo(wSelf.protocalBtn.mas_height);
            make.width.mas_equalTo(150*kDeviceRate);
        }];
        
        MyGesture *serviceGes = [[MyGesture alloc] initWithTarget:self action:@selector(jumpToProtocalVC)];
        serviceGes.numberOfTapsRequired = 1;
        [_protocalLab addGestureRecognizer:serviceGes];

    }
    
    return self;
}

//- (void)setUpViewFrame
//{
//    WS(wSelf);
//
//    [_protocalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(30*kDeviceRate);
//        make.left.mas_equalTo(wSelf);
//    }];
//}

- (void)ifAgreement:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    if(sender.selected){
        _isSelect = @"1";
        NSLog(@"yeyeyeeyeyyeyeyeyeyeyey");
    
    }else{
        _isSelect = @"2";
        NSLog(@"nonononononononononononono");
    }
    
    NSLog(@"%@", _isSelect);
    
    if (self.protocalBlock) {
        
        _protocalBlock(sender, _isSelect);
    }
}

- (void)jumpToProtocalVC
{
    if (self.protocalPushBlock) {
        
        _protocalPushBlock(_protocalBtn, _isSelect);
    }
}

@end
