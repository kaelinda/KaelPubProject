//
//  UserRequestManage.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/28.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UserRequestManage.h"
#import "CHECKFORMAT.h"
#import "CustomAlertView.h"
#import "HTRequest.h"
#import "HTYJUserCenter.h"

@interface UserRequestManage ()<CustomAlertViewDelegate,HTRequestDelegate>
{
    BOOL _phonenumberOK;//手机号规格合格
    BOOL _authCodeOK;//验证码规格合格
    BOOL _ifReadRule;//是否同意并阅读条例
    
    NSString *_phoneNumber;
}

@property (nonatomic, strong)CustomAlertView *customView;

@end

@implementation UserRequestManage

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
        
    }
    return self;
}


- (void)authCodeLoginRequestManagerWithPhoneNum:(NSString *)phoneNum withAuthCode:(NSString *)authCode withProtocal:(NSString *)protocal
{
    //手机号检验
    if ([CHECKFORMAT checkPhoneNumber:phoneNum] && (phoneNum.length>0))
    {
        _phonenumberOK = YES;
        
    }else{
        _customView.messageStr = @"账号输入不正确";
        [_customView show];
        
        return;
    }
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:authCode]) {
        
        _authCodeOK = YES;
    }else{
        
        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    if ([protocal isEqualToString:@"1"]) {
        
        _ifReadRule = YES;
    }else{
        _ifReadRule = NO;
        
        return;
    }
    
    
    if (_phonenumberOK & _authCodeOK & _ifReadRule) {
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request authCodeLoginWithMobilePhone:phoneNum andAuthCode:authCode andTimeout:KTimeout];
                
        _phoneNumber = phoneNum;
        
    }else{
        
        ZSToast(@"请检查网络设置")
        
        return;
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:authCodeLogin]){//动态口令登录
        NSLog(@"动态口令登录返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
        
        if (status == 0 && result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    NSLog(@"动态口令登录成功");
                    
                    [[HTYJUserCenter userCenter] saveAuthCodeLoginDataWithDic:result andPhoneNum:_phoneNumber];

                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                    [request UPORTALGetTokenWithTimeout:10];
                    
                    if (_requestReturnBlock) {
                        _requestReturnBlock(_flag);
                    }
                    
                    break;
                }
                case -1:{
                    NSLog(@"失败");
                    
                    _customView.messageStr = @"失败";
                    [_customView show];
                    
                    break;
                }
                case -3:{
                    NSLog(@"验证码错误");
                    
                    _customView.messageStr = @"验证码错误";
                    [_customView show];
                    
                    break;
                }
                case -7:{
                    NSLog(@"验证码超时效");
                    
                    _customView.messageStr = @"验证码超时效";
                    [_customView show];
                    
                    break;
                }
                case -9:
                {
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"登录超时 请重试！";
                    [_customView show];
                    
                    break;
                    
                }
                default:
                    
                    break;
            }
        }
    }else if ([type isEqualToString:UGetToken]){//根据identityToken获取鉴权token接口
        
        if (result.count >0) {
            if ([[result objectForKey:@"ret"] integerValue]==0) {//获取成功
                
                //登录状态
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsLogin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //发送是否合法主播请求
                HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                [request UGCCommonIsValidUser];
                
                
                //原用户、动态口令、游客三种状态的相关操作
                [[HTYJUserCenter userCenter] relativeOperationAfterGetAuthenticTokenSuccessWithResultDic:result];
                
                [[HT_UsercollectionService shareUserCollectionservice] changeUser:YES];//获取上传信息采集的配置文件
                                
                if (_tokenBlock) {//用户真正登录成功的回调（获取鉴权token成功后的）
                    
                    _tokenBlock([result objectForKey:@"token"]);
                }
            }
        }
    }else if ([type isEqualToString:UGC_COMMON_ISVALIDUSER]) {////2.1    判断是否合法主播
        
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {//合法主播
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                //1、存认证成功的标记
                [userDefaults setBool:YES forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(YES);
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UGC_ISVALIDUSER_SUCCESS" object:nil];
                
                //2、下面的key统一存储认证状态
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                
                //3、存相应数据
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[result objectForKey:@"channelId"] length]>0?[result objectForKey:@"channelId"]:@"",@"channelId",[[result objectForKey:@"name"] length]>0?[result objectForKey:@"name"]:@"",@"name",[[result objectForKey:@"hospital"] length]>0?[result objectForKey:@"hospital"]:@"",@"hospital",[[result objectForKey:@"department"] length]>0?[result objectForKey:@"department"]:@"",@"department",[[result objectForKey:@"academicTitle"] length]>0?[result objectForKey:@"academicTitle"]:@"",@"academicTitle", nil];
                
                
                [userDefaults setObject:dic forKey:@"AuthenticSuccessedData"];
                [userDefaults synchronize];
                
                break;
            }
            case 1:
            {//未认证  去认证
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(NO);
                }
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                break;
            }
            case 2:
            {//认证中
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(NO);
                }
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                break;
            }
            case 3:
            {//审核不通过   去认证
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(NO);
                }
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
              
                break;
            }
            case -2:
            {//身份认证有问题
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(NO);
                }
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                break;
            }
            case -9:
            {//其它异常（连接数据库失败或者参数错误等）  去认证
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                if (_isDocBlock) {
                    _isDocBlock(NO);
                }
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                break;
            }
        }
    }
    else{
    
        NSLog(@"其他异常");
        
        _customView.messageStr = @"登录超时 请重试！";
        [_customView show];
        
    }
}

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        
        //        [_customView resignKeyWindow];
        //        _customView = nil;
        
        _customView.hidden = YES;
    }
    
    return;
}

@end
