//
//  UserLoginViewController.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UserLoginViewController.h"
#import "InputView.h"
#import "ProtocalView.h"
#import "KnowledgeBaseViewController.h"
#import "UserRequestManage.h"
#import "UrlPageJumpManeger.h"
#import "DejalActivityView.h"

@interface UserLoginViewController ()<UITextFieldDelegate>
{
    NSString *_isSelectProtocal;
}
@property (nonatomic, strong)UIView *backView;
@property (nonatomic, strong)UIButton *returnBtn;
@property (nonatomic, strong)UIImageView *headImage;
@property (nonatomic, strong)InputView *phoneNumView;
@property (nonatomic, strong)InputView *authCodeView;
@property (nonatomic, strong)UIButton *loginBtn;
@property (nonatomic, strong)ProtocalView *protocalView;

@end

@implementation UserLoginViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if (![APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    if (_dismissBlock) {
        _dismissBlock();
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didClickKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didKboardDisappear:) name:UIKeyboardWillHideNotification object:nil];
    
    [self setUpUI];
    
    _isSelectProtocal = @"1";//默认选中
}


- (void)setUpUI
{
    WS(wSelf);
    self.view.backgroundColor = [UIColor whiteColor];
    _backView = [[UIView alloc] init];
    [self.view addSubview:_backView];
    _backView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wSelf.view);
    }];
    
    
    //返回按钮
    _returnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_returnBtn];
    [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40*kDeviceRate);
        make.top.mas_equalTo(wSelf.view.mas_top).offset(30*kDeviceRate+kTopSlimSafeSpace);
        make.left.mas_equalTo(wSelf.view.mas_left).offset(6*kDeviceRate);
    }];
    [_returnBtn addTarget:self action:@selector(returnBtnSenderDown) forControlEvents:UIControlEventTouchUpInside];
    [_returnBtn setImage:[UIImage imageNamed:@"btn_login_title_back_normal"] forState:UIControlStateNormal];
    
    
    //头像logo
    _headImage = [[UIImageView alloc] init];
    [_backView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(128*kDeviceRate);
        make.top.mas_equalTo(wSelf.backView.mas_top).offset(89*kDeviceRate+kTopSlimSafeSpace);
        make.centerX.mas_equalTo(wSelf.backView.mas_centerX);
    }];
    _headImage.image = [UIImage imageNamed:@"btn_login_label_normal"];
    _headImage.contentMode = UIViewContentModeScaleToFill;
    
    
    //手机号输入框
    _phoneNumView = [[InputView alloc] initWithIsHasAuthCode:NO withPlaceholderStr:@"请输入11位手机号" withLeftImageStr:@"btn_login_laccount_label_norma" withBackImageStr:@"bg_login_label_normal"];
    [_backView addSubview:_phoneNumView];
    [_phoneNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wSelf.headImage.mas_bottom).offset(105*kDeviceRate);
        make.width.centerX.mas_equalTo(wSelf.backView);
        make.height.mas_equalTo(42);
    }];
    [_phoneNumView setUpInputViewFrame];
    _phoneNumView.textField.delegate = self;
    [_phoneNumView.textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    //验证码输入框
    _authCodeView = [[InputView alloc] initWithIsHasAuthCode:YES withPlaceholderStr:@"请输入验证码" withLeftImageStr:@"btn_login_code_label_normal" withBackImageStr:@"bg_login_label_normal"];
    [_backView addSubview:_authCodeView];
    [_authCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wSelf.phoneNumView);
        make.top.mas_equalTo(wSelf.phoneNumView.mas_bottom).offset(30*kDeviceRate);
        make.centerX.mas_equalTo(wSelf.phoneNumView.mas_centerX);
    }];
    [_authCodeView setUpInputViewFrame];
    _authCodeView.authCodeBtnBlock = ^(UIButton *btn){//验证码按钮触发事件
        [wSelf.phoneNumView resignFirstResponder];
        [wSelf.authCodeView resignFirstResponder];
        [wSelf.authCodeView sendAuthCodeWithUserPhoneNum:wSelf.phoneNumView.textField.text];
        btn.enabled = YES;
    };
    _authCodeView.textField.delegate = self;
    [_authCodeView.textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
    
    //登陆按钮
    _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backView addSubview:_loginBtn];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wSelf.authCodeView);
        make.top.mas_equalTo(wSelf.authCodeView.mas_bottom).offset(40*kDeviceRate);
        make.centerX.mas_equalTo(wSelf.backView.mas_centerX);
    }];
    [_loginBtn setImage:[UIImage imageNamed:@"btn_user_login_normal_2"] forState:UIControlStateNormal];
    [_loginBtn setImage:[UIImage imageNamed:@"btn_user_login_normal_2"] forState:UIControlStateHighlighted];
    [_loginBtn addTarget:self action:@selector(loginBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //用户协议
    _protocalView = [[ProtocalView alloc] init];
    [_backView addSubview:_protocalView];
    [_protocalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wSelf.loginBtn.mas_bottom).offset(15*kDeviceRate);
        make.right.mas_equalTo(wSelf.backView.mas_right);
        make.height.mas_equalTo(15*kDeviceRate);
        make.left.mas_equalTo(wSelf.backView.mas_left).offset(108*kDeviceRate);
    }];
    
#pragma mark  是否选择同意用户协议按钮
    _protocalView.protocalBlock = ^(UIButton *btn, NSString *str){
        _isSelectProtocal = str;
        if ([str isEqualToString:@"1"]) {//选择
         
            [wSelf.loginBtn setImage:[UIImage imageNamed:@"btn_user_login_normal_2"] forState:UIControlStateNormal];
            wSelf.loginBtn.enabled = YES;

        }else if ([str isEqualToString:@"2"]){//未选
        
            [wSelf.loginBtn setImage:[UIImage imageNamed:@"btn_user_login_disable_2"] forState:UIControlStateNormal];
            wSelf.loginBtn.enabled = NO;
        }
    };
    
#pragma mark  跳转到用户协议界面
    _protocalView.protocalPushBlock = ^(UIButton *btn, NSString *str){
        
        if (YES||[APP_ID isEqualToString:@"1099034502"]) {
            
            KnowledgeBaseViewController *service = [[KnowledgeBaseViewController alloc] init];
            
            service.urlStr = [UrlPageJumpManeger loginPageServiceVCJump];
            
            service.existNaviBar = YES;
            
            [wSelf.navigationController pushViewController:service animated:YES];
        }
    };
}

- (void)inputBlockValueChange:(UITextField *)textField
{
    if (textField == _phoneNumView.textField) {
        if (_phoneNumView.textField.text.length>=11) {
            //            [_phoneNumView.textField resignFirstResponder];
            [_authCodeView.textField becomeFirstResponder];
        }else{
            
        }
    }
    
    if (textField == _authCodeView.textField) {
        
    }
}

- (void)loginBtnSenderDown:(UIButton *)btn
{
    
    btn.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
    });
    
    [_phoneNumView.textField resignFirstResponder];
    [_authCodeView.textField resignFirstResponder];
    
    NSLog(@"登陆按钮触发了");
    UserRequestManage *manager = [[UserRequestManage alloc] init];
    manager.flag = _isAccountBindingFlag;
    [manager authCodeLoginRequestManagerWithPhoneNum:_phoneNumView.textField.text withAuthCode:_authCodeView.textField.text withProtocal:_isSelectProtocal];
    
    manager.requestReturnBlock = ^(BOOL flag){
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在登录 请稍后"];
    };
    
    WS(wself);
    
    manager.tokenBlock = ^(NSString *token){
        
        NSLog(@"我得到的====%@",token);
        
        [DejalBezelActivityView removeView];
            
        switch (_loginType) {
            case kNormalLogin:
            {
                [wself loginBtnSenderDismiss];
                break;
            }
            case kVideoLogin:
            {
                if (wself.tokenBlock) {
                 
                    [self.view removeFromSuperview];
                    wself.tokenBlock(token);
                }
                break;
            }
            case kPopSelfLogin:
            {
                if (wself.refreshHomeBlock) {
                    wself.refreshHomeBlock();
                }
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    NSMutableArray *marr = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];
                    for (UIViewController *vc in marr) {
                        if ([vc isKindOfClass:[UserLoginViewController class]]) {
                            [marr removeObject:vc];
                            break;
                        }
                    }
                    self.navigationController.viewControllers = marr;
                });
                
                break;
            }
        }
    };
    
    manager.isDocBlock = ^(BOOL isDoc){
        
        if (wself.isLawDocBlock) {
            wself.isLawDocBlock(isDoc);
        }
    };
}


#pragma mark -      键盘即将跳出
//-(void)didClickKeyboard:(NSNotification *)sender{
//
//    CGFloat durition = [sender.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
//
//    CGRect keyboardRect = [sender.userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
//
//    CGFloat keyboardHeight = keyboardRect.size.height-89*kDeviceRate-25*kDeviceRate;
//
//    [UIView animateWithDuration:durition animations:^{
//
//        self.backView.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight);
//
//    }];
//}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _phoneNumView.textField) {
    }
    
    if (textField == _authCodeView.textField) {
        
    }
    
    [_protocalView layoutIfNeeded];
    
    CGFloat upMoveHeight = [self getMoveHeightWithFrame:_protocalView.frame];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backView.transform = CGAffineTransformMakeTranslation(0, -upMoveHeight);
    }];
    
    return YES;
}

-(CGFloat)getMoveHeightWithFrame:(CGRect)frame{
    //loginBtn 是最底部视图
    CGSize btnSize = frame.size;
    CGPoint btnPoint = frame.origin;
    // 理论上的移动高度计算方式
    // 移动高度 = (最底部视图距离底部高度 - 键盘高度 > 0) ? 0 : (键盘高度 - 最底部视图距离底部高度)
    CGFloat bottomMargin = KDeviceHeight - (btnPoint.y + btnSize.height);
    CGFloat keyboardHeight = kNumKeyboardHeight + 50;//键盘高度 预留10像素 间隙
    CGFloat moveHeight = (bottomMargin > keyboardHeight) ? 0 : (keyboardHeight - bottomMargin);
    return moveHeight;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_phoneNumView.textField resignFirstResponder];
    [_authCodeView.textField resignFirstResponder];
    return YES;
}

#pragma mark -      当键盘即将消失
-(void)didKboardDisappear:(NSNotification *)sender{
    
    CGFloat duration = [sender.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        
        self.backView.transform = CGAffineTransformIdentity;
        
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_phoneNumView.textField resignFirstResponder];
    [_authCodeView.textField resignFirstResponder];
}

- (void)returnBtnSenderDown
{
    [_phoneNumView.textField resignFirstResponder];
    [_authCodeView.textField resignFirstResponder];

    if (_backBlock) {
        [self.view removeFromSuperview];

        _backBlock();
    }
    else
    {
        if (self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}


- (void)loginBtnSenderDismiss
{
    [_phoneNumView.textField resignFirstResponder];
    [_authCodeView.textField resignFirstResponder];
    if (_loginBlock) {
        [self.view removeFromSuperview];
        
        _loginBlock();
    }else
    {
        if (self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}

- (BOOL)shouldAutorotate{
    
    return NO;
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0

- (NSUInteger)supportedInterfaceOrientations

#else

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

#endif
{
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    //viewController初始显示的方向
    return UIInterfaceOrientationPortrait;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LGOIN_SUCCESS_JUMP_TO_FATHER_VIEW" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
