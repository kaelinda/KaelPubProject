//
//  InputView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^InputViewBlock)(UIButton *btn);

//typedef void(^InputViewBlock)(UIButton *btn, UILabel *lab);

@interface InputView : UIView

@property (nonatomic, strong)UIImageView *backImage;

@property (nonatomic, strong)UITextField *textField;

@property (nonatomic, strong)UIImageView *leftLogoImage;

@property (nonatomic, strong)UIButton *authCodeBtn;

@property (nonatomic, strong)UILabel *authCodeLab;

@property (nonatomic, strong)UILabel *placeholderLab;

@property (nonatomic, copy)InputViewBlock authCodeBtnBlock;



@property (nonatomic, assign)BOOL isHasAuthCode;
@property (nonatomic, assign)NSString *placeholderStr;
@property (nonatomic, assign)NSString *leftImageStr;
@property (nonatomic, assign)NSString *backImageStr;
@property (nonatomic, assign)NSString *authCodeStr;//验证码上的文字需单独添加


@property (nonatomic, assign)NSString *userPhoneNum;


- (instancetype)initWithIsHasAuthCode:(BOOL)isHasAuthCode withPlaceholderStr:(NSString *)holderStr withLeftImageStr:(NSString *)leftStr withBackImageStr:(NSString *)backStr;

- (void)setUpInputViewFrame;

- (void)sendAuthCodeWithUserPhoneNum:(NSString *)phoneNum;

@end
