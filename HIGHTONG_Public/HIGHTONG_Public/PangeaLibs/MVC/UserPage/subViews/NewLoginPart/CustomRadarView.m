//
//  CustomRadarView.m
//
//  Code generated using QuartzCode 1.39.10 on 2016/11/9.
//  www.quartzcodeapp.com
//

#import "CustomRadarView.h"
#import "QCMethod.h"

@interface CustomRadarView ()

@property (nonatomic, assign) BOOL  updateLayerValueForCompletedAnimation;
@property (nonatomic, assign) BOOL  animationAdded;
@property (nonatomic, strong) NSMapTable * completionBlocks;
@property (nonatomic, strong) NSMutableDictionary * layers;


@end

@implementation CustomRadarView

#pragma mark - Life Cycle

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self setupProperties];
		[self setupLayers];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self) {
		[self setupProperties];
		[self setupLayers];
	}
	return self;
}

- (void)setRadarAnimProgress:(CGFloat)radarAnimProgress{
	if(!self.animationAdded){
		[self removeAllAnimations];
		[self addRadarAnimation];
		self.animationAdded = YES;
		self.layer.speed = 0;
		self.layer.timeOffset = 0;
	}
	else{
		CGFloat totalDuration = 7.98;
		CGFloat offset = radarAnimProgress * totalDuration;
		self.layer.timeOffset = offset;
	}
}

- (void)setFrame:(CGRect)frame{
	[super setFrame:frame];
	[self setupLayerFrames];
}

- (void)setBounds:(CGRect)bounds{
	[super setBounds:bounds];
	[self setupLayerFrames];
}

- (void)setupProperties{
	self.completionBlocks = [NSMapTable mapTableWithKeyOptions:NSPointerFunctionsOpaqueMemory valueOptions:NSPointerFunctionsStrongMemory];;
	self.layers = [NSMutableDictionary dictionary];
	self.updateLayerValueForCompletedAnimation = YES;
	self.color = [UIColor colorWithRed:0.0445 green: 1 blue:0.76 alpha:1];
}

- (void)setupLayers{
	CAShapeLayer * oval = [CAShapeLayer layer];
	[self.layer addSublayer:oval];
	self.layers[@"oval"] = oval;
	
	CAShapeLayer * oval2 = [CAShapeLayer layer];
	[self.layer addSublayer:oval2];
	self.layers[@"oval2"] = oval2;
	
	CAShapeLayer * oval3 = [CAShapeLayer layer];
	[self.layer addSublayer:oval3];
	self.layers[@"oval3"] = oval3;
	
	CAShapeLayer * oval4 = [CAShapeLayer layer];
	[self.layer addSublayer:oval4];
	self.layers[@"oval4"] = oval4;
	
	CAShapeLayer * oval5 = [CAShapeLayer layer];
	[self.layer addSublayer:oval5];
	self.layers[@"oval5"] = oval5;
	
	CAShapeLayer * oval6 = [CAShapeLayer layer];
	[self.layer addSublayer:oval6];
	self.layers[@"oval6"] = oval6;
	
	CAShapeLayer * oval7 = [CAShapeLayer layer];
	[self.layer addSublayer:oval7];
	self.layers[@"oval7"] = oval7;
	
	[self resetLayerPropertiesForLayerIdentifiers:nil];
	[self setupLayerFrames];
}

- (void)resetLayerPropertiesForLayerIdentifiers:(NSArray *)layerIds{
	[CATransaction begin];
	[CATransaction setDisableActions:YES];
	
	if(!layerIds || [layerIds containsObject:@"oval"]){
		CAShapeLayer * oval = self.layers[@"oval"];
		oval.opacity   = 0;
		oval.fillColor = _color.CGColor;
		oval.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval2"]){
		CAShapeLayer * oval2 = self.layers[@"oval2"];
		oval2.opacity   = 0;
		oval2.fillColor = _color.CGColor;
		oval2.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval3"]){
		CAShapeLayer * oval3 = self.layers[@"oval3"];
		oval3.opacity   = 0;
		oval3.fillColor = _color.CGColor;
		oval3.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval4"]){
		CAShapeLayer * oval4 = self.layers[@"oval4"];
		oval4.opacity   = 0;
		oval4.fillColor = _color.CGColor;
		oval4.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval5"]){
		CAShapeLayer * oval5 = self.layers[@"oval5"];
		oval5.opacity   = 0;
		oval5.fillColor = _color.CGColor;
		oval5.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval6"]){
		CAShapeLayer * oval6 = self.layers[@"oval6"];
		oval6.opacity   = 0;
		oval6.fillColor = _color.CGColor;
		oval6.lineWidth = 0;
	}
	if(!layerIds || [layerIds containsObject:@"oval7"]){
		CAShapeLayer * oval7 = self.layers[@"oval7"];
		oval7.opacity   = 0;
		oval7.fillColor = _color.CGColor;
		oval7.lineWidth = 0;
	}
	
	[CATransaction commit];
}

- (void)setupLayerFrames{
	[CATransaction begin];
	[CATransaction setDisableActions:YES];
	
	CAShapeLayer * oval  = self.layers[@"oval"];
	oval.frame           = CGRectMake(0.25 * CGRectGetWidth(oval.superlayer.bounds), 0.25 * CGRectGetHeight(oval.superlayer.bounds), 0.5 * CGRectGetWidth(oval.superlayer.bounds), 0.5 * CGRectGetHeight(oval.superlayer.bounds));
	oval.path            = [self ovalPathWithBounds:[self.layers[@"oval"] bounds]].CGPath;
	
	CAShapeLayer * oval2 = self.layers[@"oval2"];
	oval2.frame          = CGRectMake(0.25 * CGRectGetWidth(oval2.superlayer.bounds), 0.25 * CGRectGetHeight(oval2.superlayer.bounds), 0.5 * CGRectGetWidth(oval2.superlayer.bounds), 0.5 * CGRectGetHeight(oval2.superlayer.bounds));
	oval2.path           = [self oval2PathWithBounds:[self.layers[@"oval2"] bounds]].CGPath;
	
	CAShapeLayer * oval3 = self.layers[@"oval3"];
	oval3.frame          = CGRectMake(0.25 * CGRectGetWidth(oval3.superlayer.bounds), 0.25 * CGRectGetHeight(oval3.superlayer.bounds), 0.5 * CGRectGetWidth(oval3.superlayer.bounds), 0.5 * CGRectGetHeight(oval3.superlayer.bounds));
	oval3.path           = [self oval3PathWithBounds:[self.layers[@"oval3"] bounds]].CGPath;
	
	CAShapeLayer * oval4 = self.layers[@"oval4"];
	oval4.frame          = CGRectMake(0.25 * CGRectGetWidth(oval4.superlayer.bounds), 0.25 * CGRectGetHeight(oval4.superlayer.bounds), 0.5 * CGRectGetWidth(oval4.superlayer.bounds), 0.5 * CGRectGetHeight(oval4.superlayer.bounds));
	oval4.path           = [self oval4PathWithBounds:[self.layers[@"oval4"] bounds]].CGPath;
	
	CAShapeLayer * oval5 = self.layers[@"oval5"];
	oval5.frame          = CGRectMake(0.25 * CGRectGetWidth(oval5.superlayer.bounds), 0.25 * CGRectGetHeight(oval5.superlayer.bounds), 0.5 * CGRectGetWidth(oval5.superlayer.bounds), 0.5 * CGRectGetHeight(oval5.superlayer.bounds));
	oval5.path           = [self oval5PathWithBounds:[self.layers[@"oval5"] bounds]].CGPath;
	
	CAShapeLayer * oval6 = self.layers[@"oval6"];
	oval6.frame          = CGRectMake(0.25 * CGRectGetWidth(oval6.superlayer.bounds), 0.25 * CGRectGetHeight(oval6.superlayer.bounds), 0.5 * CGRectGetWidth(oval6.superlayer.bounds), 0.5 * CGRectGetHeight(oval6.superlayer.bounds));
	oval6.path           = [self oval6PathWithBounds:[self.layers[@"oval6"] bounds]].CGPath;
	
	CAShapeLayer * oval7 = self.layers[@"oval7"];
	oval7.frame          = CGRectMake(0.25 * CGRectGetWidth(oval7.superlayer.bounds), 0.25 * CGRectGetHeight(oval7.superlayer.bounds), 0.5 * CGRectGetWidth(oval7.superlayer.bounds), 0.5 * CGRectGetHeight(oval7.superlayer.bounds));
	oval7.path           = [self oval7PathWithBounds:[self.layers[@"oval7"] bounds]].CGPath;
	
	[CATransaction commit];
}

#pragma mark - Animation Setup

- (void)addRadarAnimation{
	[self resetLayerPropertiesForLayerIdentifiers:@[@"oval", @"oval2", @"oval3", @"oval4", @"oval5", @"oval6", @"oval7"]];
	
	self.layer.speed = 1;
	self.animationAdded = NO;
	
	NSString * fillMode = kCAFillModeForwards;
	
	////An infinity animation
	
	CAShapeLayer * oval = self.layers[@"oval"];
	
	////Oval animation
	CAKeyframeAnimation * ovalTransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	ovalTransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	ovalTransformAnim.keyTimes    = @[@0, @1];
	ovalTransformAnim.duration    = 3.98;
	ovalTransformAnim.repeatCount = INFINITY;
	
	CAKeyframeAnimation * ovalOpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	ovalOpacityAnim.values                = @[@1, @0];
	ovalOpacityAnim.keyTimes              = @[@0, @1];
	ovalOpacityAnim.duration              = 3.98;
	ovalOpacityAnim.repeatCount           = INFINITY;
	
	CAAnimationGroup * ovalRadarAnim = [QCMethod groupAnimations:@[ovalTransformAnim, ovalOpacityAnim] fillMode:fillMode];
	[oval addAnimation:ovalRadarAnim forKey:@"ovalRadarAnim"];
	
	////Oval2 animation
	CAKeyframeAnimation * oval2OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval2OpacityAnim.values                = @[@1, @0];
	oval2OpacityAnim.keyTimes              = @[@0, @1];
	oval2OpacityAnim.duration              = 3.98;
	oval2OpacityAnim.beginTime             = 0.667;
	oval2OpacityAnim.repeatCount           = INFINITY;
	
	CAShapeLayer * oval2 = self.layers[@"oval2"];
	
	CAKeyframeAnimation * oval2TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval2TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval2TransformAnim.keyTimes    = @[@0, @1];
	oval2TransformAnim.duration    = 3.98;
	oval2TransformAnim.beginTime   = 0.667;
	oval2TransformAnim.repeatCount = INFINITY;
	
	CAAnimationGroup * oval2RadarAnim = [QCMethod groupAnimations:@[oval2OpacityAnim, oval2TransformAnim] fillMode:fillMode];
	[oval2 addAnimation:oval2RadarAnim forKey:@"oval2RadarAnim"];
	
	////Oval3 animation
	CAKeyframeAnimation * oval3OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval3OpacityAnim.values                = @[@1, @0];
	oval3OpacityAnim.keyTimes              = @[@0, @1];
	oval3OpacityAnim.duration              = 3.98;
	oval3OpacityAnim.beginTime             = 1.33;
	oval3OpacityAnim.repeatCount           = INFINITY;
	
	CAShapeLayer * oval3 = self.layers[@"oval3"];
	
	CAKeyframeAnimation * oval3TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval3TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval3TransformAnim.keyTimes    = @[@0, @1];
	oval3TransformAnim.duration    = 3.98;
	oval3TransformAnim.beginTime   = 1.33;
	oval3TransformAnim.repeatCount = INFINITY;
	
	CAAnimationGroup * oval3RadarAnim = [QCMethod groupAnimations:@[oval3OpacityAnim, oval3TransformAnim] fillMode:fillMode];
	[oval3 addAnimation:oval3RadarAnim forKey:@"oval3RadarAnim"];
	
	////Oval4 animation
	CAKeyframeAnimation * oval4OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval4OpacityAnim.values                = @[@1, @0];
	oval4OpacityAnim.keyTimes              = @[@0, @1];
	oval4OpacityAnim.duration              = 3.98;
	oval4OpacityAnim.beginTime             = 2;
	oval4OpacityAnim.repeatCount           = INFINITY;
	
	CAShapeLayer * oval4 = self.layers[@"oval4"];
	
	CAKeyframeAnimation * oval4TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval4TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval4TransformAnim.keyTimes    = @[@0, @1];
	oval4TransformAnim.duration    = 3.98;
	oval4TransformAnim.beginTime   = 2;
	oval4TransformAnim.repeatCount = INFINITY;
	
	CAAnimationGroup * oval4RadarAnim = [QCMethod groupAnimations:@[oval4OpacityAnim, oval4TransformAnim] fillMode:fillMode];
	[oval4 addAnimation:oval4RadarAnim forKey:@"oval4RadarAnim"];
	
	////Oval5 animation
	CAKeyframeAnimation * oval5OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval5OpacityAnim.values                = @[@1, @0];
	oval5OpacityAnim.keyTimes              = @[@0, @1];
	oval5OpacityAnim.duration              = 3.98;
	oval5OpacityAnim.beginTime             = 2.67;
	oval5OpacityAnim.repeatCount           = INFINITY;
	
	CAShapeLayer * oval5 = self.layers[@"oval5"];
	
	CAKeyframeAnimation * oval5TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval5TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval5TransformAnim.keyTimes    = @[@0, @1];
	oval5TransformAnim.duration    = 3.98;
	oval5TransformAnim.beginTime   = 2.67;
	oval5TransformAnim.repeatCount = INFINITY;
	
	CAAnimationGroup * oval5RadarAnim = [QCMethod groupAnimations:@[oval5OpacityAnim, oval5TransformAnim] fillMode:fillMode];
	[oval5 addAnimation:oval5RadarAnim forKey:@"oval5RadarAnim"];
	
	CAShapeLayer * oval6 = self.layers[@"oval6"];
	
	////Oval6 animation
	CAKeyframeAnimation * oval6TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval6TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval6TransformAnim.keyTimes    = @[@0, @1];
	oval6TransformAnim.duration    = 3.98;
	oval6TransformAnim.beginTime   = 3.33;
	oval6TransformAnim.repeatCount = INFINITY;
	
	CAKeyframeAnimation * oval6OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval6OpacityAnim.values                = @[@1, @0];
	oval6OpacityAnim.keyTimes              = @[@0, @1];
	oval6OpacityAnim.duration              = 3.98;
	oval6OpacityAnim.beginTime             = 3.33;
	oval6OpacityAnim.repeatCount           = INFINITY;
	
	CAAnimationGroup * oval6RadarAnim = [QCMethod groupAnimations:@[oval6TransformAnim, oval6OpacityAnim] fillMode:fillMode];
	[oval6 addAnimation:oval6RadarAnim forKey:@"oval6RadarAnim"];
	
	CAShapeLayer * oval7 = self.layers[@"oval7"];
	
	////Oval7 animation
	CAKeyframeAnimation * oval7TransformAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	oval7TransformAnim.values      = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)], 
		 [NSValue valueWithCATransform3D:CATransform3DMakeScale(2, 2, 1)]];
	oval7TransformAnim.keyTimes    = @[@0, @1];
	oval7TransformAnim.duration    = 3.98;
	oval7TransformAnim.beginTime   = 4;
	oval7TransformAnim.repeatCount = INFINITY;
	
	CAKeyframeAnimation * oval7OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
	oval7OpacityAnim.values                = @[@1, @0];
	oval7OpacityAnim.keyTimes              = @[@0, @1];
	oval7OpacityAnim.duration              = 3.98;
	oval7OpacityAnim.beginTime             = 4;
	oval7OpacityAnim.repeatCount           = INFINITY;
	
	CAAnimationGroup * oval7RadarAnim = [QCMethod groupAnimations:@[oval7TransformAnim, oval7OpacityAnim] fillMode:fillMode];
	[oval7 addAnimation:oval7RadarAnim forKey:@"oval7RadarAnim"];
}

#pragma mark - Animation Cleanup

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
	void (^completionBlock)(BOOL) = [self.completionBlocks objectForKey:anim];;
	if (completionBlock){
		[self.completionBlocks removeObjectForKey:anim];
		if ((flag && self.updateLayerValueForCompletedAnimation) || [[anim valueForKey:@"needEndAnim"] boolValue]){
			[self updateLayerValuesForAnimationId:[anim valueForKey:@"animId"]];
			[self removeAnimationsForAnimationId:[anim valueForKey:@"animId"]];
		}
		completionBlock(flag);
	}
}

- (void)updateLayerValuesForAnimationId:(NSString *)identifier{
	if([identifier isEqualToString:@"radar"]){
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval"] animationForKey:@"ovalRadarAnim"] theLayer:self.layers[@"oval"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval2"] animationForKey:@"oval2RadarAnim"] theLayer:self.layers[@"oval2"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval3"] animationForKey:@"oval3RadarAnim"] theLayer:self.layers[@"oval3"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval4"] animationForKey:@"oval4RadarAnim"] theLayer:self.layers[@"oval4"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval5"] animationForKey:@"oval5RadarAnim"] theLayer:self.layers[@"oval5"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval6"] animationForKey:@"oval6RadarAnim"] theLayer:self.layers[@"oval6"]];
		[QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval7"] animationForKey:@"oval7RadarAnim"] theLayer:self.layers[@"oval7"]];
	}
}

- (void)removeAnimationsForAnimationId:(NSString *)identifier{
	if([identifier isEqualToString:@"radar"]){
		[self.layers[@"oval"] removeAnimationForKey:@"ovalRadarAnim"];
		[self.layers[@"oval2"] removeAnimationForKey:@"oval2RadarAnim"];
		[self.layers[@"oval3"] removeAnimationForKey:@"oval3RadarAnim"];
		[self.layers[@"oval4"] removeAnimationForKey:@"oval4RadarAnim"];
		[self.layers[@"oval5"] removeAnimationForKey:@"oval5RadarAnim"];
		[self.layers[@"oval6"] removeAnimationForKey:@"oval6RadarAnim"];
		[self.layers[@"oval7"] removeAnimationForKey:@"oval7RadarAnim"];
	}
	self.layer.speed = 1;
}

- (void)removeAllAnimations{
	[self.layers enumerateKeysAndObjectsUsingBlock:^(id key, CALayer *layer, BOOL *stop) {
		[layer removeAllAnimations];
	}];
	self.layer.speed = 1;
}

#pragma mark - Bezier Path

- (UIBezierPath*)ovalPathWithBounds:(CGRect)bound{
	UIBezierPath * ovalPath = [UIBezierPath bezierPathWithOvalInRect:bound];
	return ovalPath;
}

- (UIBezierPath*)oval2PathWithBounds:(CGRect)bound{
	UIBezierPath * oval2Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval2Path;
}

- (UIBezierPath*)oval3PathWithBounds:(CGRect)bound{
	UIBezierPath * oval3Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval3Path;
}

- (UIBezierPath*)oval4PathWithBounds:(CGRect)bound{
	UIBezierPath * oval4Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval4Path;
}

- (UIBezierPath*)oval5PathWithBounds:(CGRect)bound{
	UIBezierPath * oval5Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval5Path;
}

- (UIBezierPath*)oval6PathWithBounds:(CGRect)bound{
	UIBezierPath * oval6Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval6Path;
}

- (UIBezierPath*)oval7PathWithBounds:(CGRect)bound{
	UIBezierPath * oval7Path = [UIBezierPath bezierPathWithOvalInRect:bound];
	return oval7Path;
}


@end
