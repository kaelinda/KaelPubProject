//
//  UserLoginViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, LOGINVIEWTYPE) {
    kNormalLogin = 0,//正常的 push-pop 类型
    kVideoLogin,//播放器使用的登录
    kPopSelfLogin,//把当前界面从navigation中出栈后再push出新界面类型（账号绑定、山西挂号）
};

typedef void(^ HandleBlock)() ;

typedef void(^ TokenForVideoBlock)(NSString *token);


typedef void (^callBackBlock)(NSString *token);

@interface UserLoginViewController : UIViewController

/**
 点击返回按钮的回调 （仅供 贴图方式 使用）
 */
@property (nonatomic, copy)HandleBlock backBlock;

/**
 点击登录按钮的回调 （仅供 贴图方式 使用）
 */
@property (nonatomic, copy)HandleBlock loginBlock;

/**
 界面模式 推出动态登陆界面时，如果界面消失会调用这个方法
 */
@property (nonatomic, copy)HandleBlock dismissBlock;

@property (nonatomic, assign) BOOL isXuanzhuan;

@property (nonatomic, assign) BOOL isAccountBindingFlag;


/**
 通知首页跳转到相应界面 （山西挂号、爱上TV的账号绑定）
 */
@property (nonatomic, copy)HandleBlock refreshHomeBlock;


/**
 动态登录后调取鉴权token成功后的，真正的登录成功的回调
 */
@property (nonatomic, copy)TokenForVideoBlock tokenBlock;


@property (nonatomic, copy) void (^isLawDocBlock)(BOOL);

@property (nonatomic, assign)LOGINVIEWTYPE loginType;

- (void)returnBtnSenderDown;

/**
 暂未实现方法体

 @param lgoinType <#lgoinType description#>
 @param callBackBlock <#callBackBlock description#>
 */
//- (void)setLoginType:(LOGINVIEWTYPE)lgoinType andCallBack:(callBackBlock)callBackBlock;


@end
