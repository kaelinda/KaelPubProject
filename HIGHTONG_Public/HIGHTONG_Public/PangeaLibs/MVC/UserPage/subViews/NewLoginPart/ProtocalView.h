//
//  ProtocalView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2016/10/27.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ProtocalViewBlock)(UIButton *btn, NSString *isSelectValue);

@interface ProtocalView : UIView

@property (nonatomic, strong)UIButton *protocalBtn;

@property (nonatomic, strong)UILabel *protocalLab;

@property (nonatomic, copy)ProtocalViewBlock protocalBlock;

@property (nonatomic, copy)ProtocalViewBlock protocalPushBlock;

//- (void)setUpViewFrame;

@end
