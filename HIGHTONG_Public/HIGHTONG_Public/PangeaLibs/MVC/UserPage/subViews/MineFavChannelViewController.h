//
//  MineFavChannelViewController.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface MineFavChannelViewController : HT_FatherViewController
{
    NSString *liveFavContentID;

    NSInteger selectCellNum;
    
    NSInteger allChannelNum;//添加喜爱频道后，请求喜爱频道列表的标记
}

@property (nonatomic, strong) NSMutableArray *dataArr;//收藏直播数据
@property (nonatomic, strong) NSMutableArray *tempArr;//收藏直播历史数据

@end
