//
//  MineFavChannelViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineFavChannelViewController.h"
#import "GeneralPromptView.h"
#import "MyGesture.h"
#import "MineFvoriteChannelCell.h"
#import "CustomAlertView.h"
#import "FavChannelCollectionViewCell.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DataPlist.h"
#import "UserCollectCenter.h"
#import "AllChannelTableCell.h"
#import "MineFavChannelSelectViewController.h"
#import "MineFvoriteEditChannelCell.h"
#import "MBProgressHUD.h"
#import "LocalCollectAndPlayRecorder.h"
#import "DataPlist.h"

static CGFloat ZoomAnimationDuration = 0.20f;


@interface MineFavChannelViewController ()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    BOOL ISNoting;//清空按钮是否被点击
    BOOL isEditing;//是否显示cell右上角的圆叉按钮
    BOOL _flag;//只出现一次可以移动cell提示图的标记
    BOOL _addFlag;//只出现一次可以添加喜爱频道提示图的标记
    BOOL isMove;//标记cell是否移动了的标记
    BOOL toEpgPlayer;//防止多次点击回看cell，多次push
    BOOL firstLoad;//第一次加载
}

@property (nonatomic, strong)UIButton *backBtn;
@property (nonatomic, strong)UIButton *cleanBtn;
@property (nonatomic, strong)GeneralPromptView *promptView;
@property (nonatomic, strong)UIImageView *addView;
@property (nonatomic, strong)UIImageView *deleteView;
@property (nonatomic, strong)UIImageView *addTiShiImg;
@property (nonatomic, strong)UITableView *table;
@property (nonatomic, strong)UIButton *addBtn;
@property (nonatomic, strong)NSMutableArray *contentSelectedArr;
@property (nonatomic, strong)UIView *footView;
@property (nonatomic, strong)UIButton *editDeleteBtn;//编辑底部添加按钮
@property (nonatomic, strong)UIImageView *editDeleteView;//编辑底部添加视图
@property (nonatomic, assign)HomePageType pageType;
@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation MineFavChannelViewController

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
        
    return _dataArr;
}

- (NSMutableArray *)tempArr
{
    if (!_tempArr) {
        _tempArr = [NSMutableArray array];
    }
    
    return _tempArr;
}

- (NSMutableArray *)contentSelectedArr {
    
    if (!_contentSelectedArr) {
        _contentSelectedArr = [[NSMutableArray alloc] init];
    }
    return _contentSelectedArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (firstLoad) {
        //这里是1.0f后 你要做的事情
        [self sendHTTPRequestMethod];
    }
    [self showLoading];
    firstLoad = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendHTTPRequestMethod) name:@"REFRESHMINEVCTABLECELL" object:nil];
    
    [MobCountTool pushInWithPageView:@"MineFavChannelPage"];

    toEpgPlayer = YES;

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineFavChannelPage"];
}

- (void)sendHTTPRequestMethod
{
     [self showLoading];
    
    _dataArr = [NSMutableArray arrayWithArray:[_localListCenter getLiveFavoriteListWithUserType:YES]];
    
    if (_dataArr.count) {
        _cleanBtn.hidden = NO;
        _promptView.hidden = YES;
        
        _addView.hidden = NO;
        
        _addBtn.hidden = NO;
        
        [_table setBackgroundColor:UIColorFromRGB(0xffffff)];
        
        [_table reloadData];
        
        if (_tempArr.count != 0) {
            if (_tempArr.count < _dataArr.count) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_UPDATE_FAVORATE_CHANELL_LIST object:_dataArr];
                
                _tempArr = nil;
            }
        }
        
        [self checkBlackView];
    }else{
    
        _cleanBtn.hidden = YES;
        
        _promptView.hidden = NO;
        
        _addView.hidden = NO;
        
        _addBtn.hidden = NO;
        
        _table.backgroundColor = [UIColor clearColor];
        
        [self checkAddFavChannelView];
    }
    
    [self hideLoading];
}

- (void)changeNaviRightBtnImg
{
    if (isMove) {
        
        [_cleanBtn setImage:[UIImage imageNamed:@"btn_user_save"] forState:UIControlStateNormal];
        [_cleanBtn setImage:[UIImage imageNamed:@"title_save_click"] forState:UIControlStateHighlighted];
        
        [self.view setNeedsDisplay];

    }else{
    
        [_cleanBtn setImage:[UIImage imageNamed:@"btn_common_title_del_normal"] forState:UIControlStateNormal];
        [_cleanBtn setImage:[UIImage imageNamed:@"btn_user_cancel"] forState:UIControlStateSelected];
        
        [self.view setNeedsDisplay];
    }
}

# pragma - method begin
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    // 请求数据
    [self sendHTTPRequestMethod];
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"喜爱频道"];

    self.view.backgroundColor = UIColorFromRGB(0xffffff);
    
    _cleanBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_user_edit" imgHighlight:@"btn_user_edit" imgSelected:@"btn_user_done" target:self action:@selector(dustbinBtnSenderDown:)];
    [self setNaviBarRightBtn:_cleanBtn];
    
    _promptView = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 200*kDeviceRate+64+kTopSlimSafeSpace, kDeviceWidth, 88*kDeviceRate) andImgName:@"ic_user_channelcollect_metadata" andTitle:@"您还没有喜爱频道喔"];
    [self.view addSubview:_promptView];
    _promptView.hidden = YES;
    _promptView.userInteractionEnabled = YES;

    
    WS(wss);
    
    //长按拖动可以排序的view
    if ( [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstComeInAndNull"] == nil ) {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"FirstComeInAndNull"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    //点击我可以添加喜爱频道view
    _addTiShiImg = [UIImageView new];
    [_addTiShiImg setImage:[UIImage imageNamed:@"guide_user_favchannel_add_intro"]];
    [self.view addSubview:_addTiShiImg];
    _addTiShiImg.hidden = YES;
    _addTiShiImg.userInteractionEnabled = YES;
    
    if (KDeviceHeight == 480) {
        
        [_addTiShiImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(248*kRateSize));
            make.height.equalTo(@(198*kRateSize));
            make.centerX.equalTo(wss.view.mas_centerX);
            make.top.equalTo(wss.view.mas_top).offset(64 + 159*kRateSize);
        }];
    }else{
        [_addTiShiImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(248*kRateSize));
            make.height.equalTo(@(198*kRateSize));
            make.centerX.equalTo(wss.view.mas_centerX);
            make.top.equalTo(wss.view.mas_top).offset(64 + 239*kRateSize);
        }];
    }

    //喜爱界面table
    _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-44-60*kDeviceRate-kBottomSafeSpace)];
    _table.delegate = self;
    _table.dataSource = self;
    
    if (_dataArr.count>0) {
        
        _table.backgroundColor = UIColorFromRGB(0xf7f7f7);
    }else{
        
        _table.backgroundColor = [UIColor clearColor];
    }
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_table];
    _table.showsHorizontalScrollIndicator = NO;
    _table.showsVerticalScrollIndicator = NO;

    
    _footView = [UIView new];
    _footView.frame = CGRectMake(0, 0, kDeviceWidth, 20);
    _table.tableFooterView = _footView;

    self.pageType = kApp_HomePageType;
    
    switch (self.pageType) {
        case kHomePageType_JXCable_v1_0:
        {
            _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [_addBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_favchannel_normal"] forState:UIControlStateNormal];
             [_addBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_favchannel_pressed"] forState:UIControlStateHighlighted];
            
            [_addBtn setBackgroundColor:App_background_color];
            [self.view addSubview:_addBtn];
            [_addBtn addTarget:self action:@selector(addChannelGesture) forControlEvents:UIControlEventTouchUpInside];
            [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left);
                make.right.equalTo(self.view.mas_right);
                make.height.equalTo(@(60*kDeviceRate));
                make.bottom.equalTo(self.view.mas_bottom).offset(-kBottomSafeSpace);
            }];
        }
            break;
        case kHomePageType_JKTV_v1_1:
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        case kHomePageType_JKTV_V1_5:
        {
            //页面底端增加频道收藏条
            _addView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-60*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 60*kDeviceRate)];
            _addView.backgroundColor = HT_COLOR_FONT_INVERSE;
            [self.view addSubview:_addView];
            _addView.userInteractionEnabled = YES;
            
            UIView *VerticalLine = [[UIView alloc]init];
            [_addView addSubview:VerticalLine];
            VerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
            [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_addView.mas_top).offset(1);
                make.left.equalTo(_addView.mas_left);
                make.right.equalTo(_addView.mas_right);
                make.height.equalTo(@(1));
            }];
            
            UIImageView *addImg = [[UIImageView alloc] init];
            [addImg setImage:[UIImage imageNamed:@"btn_user_add"]];
            [_addView addSubview:addImg];
            
            [addImg mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(38*kDeviceRate));
                make.height.equalTo(@(38*kDeviceRate));
                make.centerX.equalTo(_addView.mas_centerX);
                make.centerY.equalTo(_addView.mas_centerY);
            }];
            
            MyGesture *addGes = [[MyGesture alloc] initWithTarget:self action:@selector(addChannelGesture)];
            addGes.numberOfTapsRequired = 1;
            [_addView addGestureRecognizer:addGes];
        }
            break;
            
        default:
            break;
    }
    
    //页面底端增加频道收藏条
    _editDeleteView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-60*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 60*kDeviceRate)];
    _editDeleteView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_editDeleteView];
    _editDeleteView.userInteractionEnabled = YES;
    _editDeleteView.hidden = YES;
    
     UIView *editVerticalLine = [[UIView alloc]init];
    [_editDeleteView addSubview:editVerticalLine];
    editVerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [editVerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_editDeleteView.mas_top).offset(1);
        make.left.equalTo(_editDeleteView.mas_left);
        make.right.equalTo(_editDeleteView.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _editDeleteBtn = [[UIButton alloc]init];
    [_editDeleteView addSubview:_editDeleteBtn];
    [_editDeleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_editDeleteView.mas_centerX);
        make.centerY.equalTo(_editDeleteView.mas_centerY);
    }];
    
    _editDeleteBtn.titleLabel.font = HT_FONT_FIRST;
    [_editDeleteBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_fav_channel_delete_normal_cell"] forState:UIControlStateNormal];
    [_editDeleteBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_fav_channel_delete_selected_cell"] forState:UIControlStateHighlighted];
    [_editDeleteBtn setBackgroundImage:[UIImage imageNamed:@"ic_user_common_no_delete_item"] forState:UIControlStateDisabled];
    [_editDeleteBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_editDeleteBtn setTitle:@"删    除"forState:UIControlStateDisabled];
    _editDeleteBtn.enabled = NO;
    
    [_editDeleteBtn addTarget:self action:@selector(deleteBtnSenderDown) forControlEvents:UIControlEventTouchUpInside];
    
    [self showLoading];
}


- (void)showLoading
{
    [self showCustomeHUD];
    
    [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}


- (void)hideLoading
{
    _promptView.hidden = NO;
    [self hiddenCustomHUD];
}


//可以排序提示图
- (void)checkBlackView
{
    if ( _flag && _dataArr.count >=2) {
        [self.view setNeedsDisplay];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"FirstVisiteFavChannal"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _flag = false;
    }else{
    
    }
}


//可以添加喜爱频道
- (void)checkAddFavChannelView{
    
    _addFlag = [[NSUserDefaults standardUserDefaults] boolForKey:@"FirstComeInAndNull"];
    
    NSLog(@"AAAAAAAAAAAAAAflag:%d, count:%ld", _addFlag, _dataArr.count);
    if (_addFlag && _dataArr.count == 0 && _editDeleteView.hidden) {
        
        _addTiShiImg.hidden = NO;
        
        [self.view setNeedsDisplay];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"FirstComeInAndNull"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _addFlag = false;

    }
    else{
    
        _addTiShiImg.hidden = YES;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60*kDeviceRate;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isEditing) {
        static NSString *edit = @"edit";
        
        MineFvoriteEditChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:edit];
        
        if (!cell) {
            cell = [[MineFvoriteEditChannelCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:edit];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.contentView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        }
        
        NSDictionary *tempDic = [_dataArr objectAtIndex:indexPath.row];
        
        [cell.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([tempDic objectForKey:@"imageLink"])]placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
        
        cell.linkName.text = [tempDic objectForKey:@"name"];
        
        if (isEditing == YES) {
            
            cell.choiceImg.hidden = YES;
            
            cell.UnChoiceImg.hidden = NO;
            
            //给当前table的cell赋
            NSLog(@"%ld",indexPath.row);
            NSInteger Kindex= indexPath.row;//zzs

            NSDictionary *selId = nil;
            if (Kindex < _dataArr.count) {
                selId = [_dataArr objectAtIndex:Kindex];
            }
            
            NSString *selectID = [selId objectForKey:@"id"];
            
            
            for (NSDictionary *selectTempDic in self.contentSelectedArr ) {
                
                if ([selectID isEqualToString:[selectTempDic objectForKey:@"id"]]) {
                    
                    cell.choiceImg.hidden = NO;
                    
                    cell.UnChoiceImg.hidden = YES;
                    
                }
            }
        }else{
            
            cell.choiceImg.hidden = YES;
            
            cell.UnChoiceImg.hidden = YES;
        }
        
        MyGesture *topGes = [[MyGesture alloc] initWithTarget:self action:@selector(touchSingleTopGesture:)];
        topGes.tag = indexPath.row;
        topGes.numberOfTapsRequired = 1;
        [cell.topImg addGestureRecognizer:topGes];
        
        MyGesture *selectedGes = [[MyGesture alloc] initWithTarget:self action:@selector(touchSingleSelectGesture:)];
        selectedGes.tag = indexPath.row;
        selectedGes.numberOfTapsRequired = 1;
        [cell.choiceImg addGestureRecognizer:selectedGes];
        
        MyGesture *selectGes = [[MyGesture alloc] initWithTarget:self action:@selector(touchSingleSelectGesture:)];
        selectGes.tag = indexPath.row;
        selectGes.numberOfTapsRequired = 1;
        [cell.UnChoiceImg addGestureRecognizer:selectGes];

        UIPanGestureRecognizer *longPress = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
        [cell.moveImg addGestureRecognizer:longPress];

        return cell;
    }
    else
    {
        static NSString *iden = @"iden";

        _table.backgroundColor = UIColorFromRGB(0xf7f7f7);
        
        MineFvoriteChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:iden];
        
        if (!cell) {
            cell = [[MineFvoriteChannelCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.backBtn.tag = indexPath.row;
        [cell.backBtn addTarget:self action:@selector(goToEPGPlayer:) forControlEvents:UIControlEventTouchUpInside];
        
        NSDictionary *tempDic = [_dataArr objectAtIndex:indexPath.row];
        
        [cell.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([tempDic objectForKey:@"imageLink"])]placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
        
        cell.linkName.text = [tempDic objectForKey:@"name"];
        
        if (isEditing == YES) {
            
            cell.deleteBtn.hidden = NO;
            
            cell.deleteBtnView.hidden = NO;
        }else{
            
            cell.deleteBtn.hidden = YES;
            
            cell.deleteBtnView.hidden = YES;
        }
        
        return cell;
    }
}

- (void)goToEPGPlayer:(VODDeleteButton *)sender
{
    if (isEditing == YES) {
        
        return;
    }
    
    if (toEpgPlayer) {
        
        NSDictionary *diction = [_dataArr objectAtIndex:sender.tag];
        
        EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
        live.serviceID = [diction objectForKey:@"id"];
        live.serviceName = [diction objectForKey:@"name"];
        live.TRACKID = @"2";
        live.TRACKNAME = @"我的";
        live.EN = @"1";
        [self.navigationController pushViewController:live animated:YES];
        
        toEpgPlayer = NO;
    }
    else{
        
        return;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}


// 判断选中数据
- (void)callCheckDataListSelected
{
    NSLog(@"callCheckDataListSelected-------%@",self.contentSelectedArr);
    
    if (self.contentSelectedArr.count >0) {
        _editDeleteBtn.enabled = YES;
        
        NSString *btnStr = [NSString stringWithFormat:@"删    除 ( %lu )",(unsigned long)self.contentSelectedArr.count];
        
        [_editDeleteBtn setTitle:btnStr forState:UIControlStateNormal];
        [_editDeleteBtn setTitleColor:HT_COLOR_REMIND forState:UIControlStateNormal];
        
    } else {
        _editDeleteBtn.enabled = NO;
    }
}

- (void)longPressGestureRecognized:(UILongPressGestureRecognizer *)sender
{
    if (!isEditing) {
        
        
    }else{
    
        UIGestureRecognizerState state = sender.state;
        
        CGPoint location = [sender locationInView:self.table];
        
        NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:location];
        
        static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
        static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
        
        switch (state) {
            case UIGestureRecognizerStateBegan: {
                if (indexPath) {
                    sourceIndexPath = indexPath;
                    
                    UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPath];
                    
                    // Take a snapshot of the selected row using helper method.
                    snapshot = [self customSnapshoFromView:cell];
                    
                    // Add the snapshot as subview, centered at cell's center...
                    __block CGPoint center = cell.center;
                    snapshot.center = center;
                    

                    //snapshot.alpha = 0.0;
                    
                    
                    [self.table addSubview:snapshot];
                    [UIView animateWithDuration:ZoomAnimationDuration animations:^{
                        
                        // Offset for gesture location.
                        center.y = location.y;
                        snapshot.center = center;
//                        snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                        
                        
                       //snapshot.alpha = 0.98;
                        
                        
                       // cell.alpha = 0.0;
                        
                    } completion:^(BOOL finished) {
                      
                        
                        cell.hidden = YES;
                        
                    }];
                }
                break;
            }
                
            case UIGestureRecognizerStateChanged: {
                CGPoint center = snapshot.center;
                center.y = location.y;
                snapshot.center = center;
                
                // Is destination valid and is it different from source?
                if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                    
                    isMove = YES;
                    
                    

                    _table.frame = CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-64-kTopSlimSafeSpace-kBottomSafeSpace);
                    
                    _footView.frame = CGRectMake(0, 0, kDeviceWidth, 0);

                    _addView.hidden = YES;
                    
                    _addBtn.hidden = YES;
                    
                    [self.view setNeedsDisplay];
                    
                    
                    
                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"change_RRight_BtnImg" object:nil];
                    
                    // ... update data source.
                    [self.dataArr exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                    
                    // ... move the rows.
                    [self.table moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                    
                    // ... and update source so it is in sync with UI changes.
                    sourceIndexPath = indexPath;
                    
                    NSLog(@"排序后的数组%@",self.dataArr);

                }
                break;
            }
                
            default: {// UIGestureRecognizerStateEnded
                // Clean up.
                UITableViewCell *cell = [self.table cellForRowAtIndexPath:sourceIndexPath];
                
                
                
               // cell.hidden = NO;
                
                
                
                //cell.alpha = 0.0;
                
                
                [UIView animateWithDuration:ZoomAnimationDuration animations:^{
                    
                    snapshot.center = cell.center;
                    snapshot.transform = CGAffineTransformIdentity;
                    
                    
    //                snapshot.alpha = 0.0;
    //                cell.alpha = 1.0;
                    
                } completion:^(BOOL finished) {
                    
   
                    cell.hidden = NO;

                    
                    sourceIndexPath = nil;
                    [snapshot removeFromSuperview];
                    snapshot = nil;
                    
                    
                    //NSLog(@"排序后的数组%@",self.dataArr);
                    
                }];
               
                break;
            }
        }
        
        
        
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            //return;
            
            NSLog(@"我移动了一次吗");
            _cleanBtn.userInteractionEnabled = YES;
            
            
            
        } else if (sender.state == UIGestureRecognizerStateBegan) {
            
            _cleanBtn.userInteractionEnabled = NO;
            //TODO
            
            return;
        }
    }
}


#pragma mark - Helper methods
/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView
{
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
//    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
//    snapshot.layer.shadowRadius = 5.0;
//    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:EPG_TVFAVORATELIST]) {//直播收藏列表
        
        NSLog(@"直播收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"++++成功");
                    
                    self.dataArr =[NSMutableArray arrayWithArray: [result objectForKey:@"servicelist"]];
                    
                    _cleanBtn.hidden = NO;
                    _promptView.hidden = YES;
                    
                    _addView.hidden = NO;
                    
                    _addBtn.hidden = NO;
                    
                    [_table setBackgroundColor:UIColorFromRGB(0xffffff)];
                    
                    [_table reloadData];
                    
 
                    
                    if (_tempArr.count != 0) {
                        if (_tempArr.count < _dataArr.count) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_UPDATE_FAVORATE_CHANELL_LIST object:_dataArr];
                            
                            _tempArr = nil;
                        }
                    }
                    
                    [self checkBlackView];

                    break;
                }
                case -1:{
                    NSLog(@"收藏列表为空");
                    
                    _cleanBtn.hidden = YES;
                    
                    _promptView.hidden = NO;
                    
                    _addView.hidden = NO;
                    
                    _addBtn.hidden = NO;
                    
                    _table.backgroundColor = [UIColor clearColor];
                    
                    [self checkAddFavChannelView];

                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    _cleanBtn.hidden = YES;
                    
                    _promptView.hidden = NO;
                    
                    _addView.hidden = NO;
                    
                    _addBtn.hidden = NO;
                    _table.backgroundColor = [UIColor clearColor];

                    
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    _cleanBtn.hidden = YES;
                    
                    _promptView.hidden = NO;
                    
                    
                    _addView.hidden = NO;
                    
                    _addBtn.hidden = NO;
                    _table.backgroundColor = [UIColor clearColor];
                    
                    break;
                }
                    
                default:
                    
                    _table.backgroundColor = [UIColor clearColor];
                    
                    _promptView.hidden = NO;
                    
                    break;
            }
            
            [self hideLoading];

        }
    }
    
    else if ([type isEqualToString:EPG_DELTVFAVORATE]){//删除频道收藏
        
        NSLog(@"删除频道收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"++++成功");
                    
                    if (ISNoting) {//清空频道收藏列表
                        
                        [_dataArr removeAllObjects];
                        
                        [_localListCenter cleanLiveFavoriteChannelListWithUserType:YES];
                    }else{//单个删除
                        
                    }
                    
                    [_localListCenter multiDeleteLiveFavoriteWithDeleteArr:[NSArray arrayWithArray:_contentSelectedArr] andUserType:YES andKey:@"id"];
                    
                    [self.contentSelectedArr removeAllObjects];
                    
                    [self callCheckDataListSelected];
                    
                    [_dataArr removeAllObjects];
                    
                    _dataArr = [NSMutableArray arrayWithArray:[_localListCenter getLiveFavoriteListWithUserType:YES]];
                    
                    
                    
                    if ([_dataArr count] > 0) {
                        
                        [_promptView setHidden:YES];
                        
                        _cleanBtn.hidden = NO;
                        
                        _table.backgroundColor = App_background_color;
                        
                    }else
                    {
                        [_promptView setHidden:NO];
                        
                        _addView.hidden = NO;
                        
                        _addBtn.hidden = NO;
                        
                        isEditing = NO;
                        
                        
                        _table.backgroundColor = CLEARCOLOR;
                        
                        [self checkAddFavChannelView];
                    }
                    
                    
                    //发给首页的通知
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadFavoriteChannel" object:nil];
                    //给管理者发送通知，并发送喜爱频道数据
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_UPDATE_FAVORATE_CHANELL_LIST object:_dataArr];
                    
                    
                    [self.table reloadData];

                    
                    break;
                }
                case 1:{
                    NSLog(@"未收藏该频道或者已经取消收藏");
                    
                    _table.backgroundColor = [UIColor clearColor];
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    _table.backgroundColor = [UIColor clearColor];
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    _table.backgroundColor = [UIColor clearColor];
                    
                    break;
                }
                default:
                    break;
            }
            
        }
    }
    else if ([type isEqualToString:EPG_UPDATE_TVFAVORATE]){//更新频道收藏列表
        
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"移动cell更新成功");
                    
                    isMove = NO;
                    
                    _table.frame = CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-44-36*kDeviceRate-kTopSlimSafeSpace-kBottomSafeSpace);
                    
                    
                    _footView.frame = CGRectMake(0, 0, kDeviceWidth, 20);
                    
                    
                    _addView.hidden = NO;
                    
                    _addBtn.hidden = NO;
                    
                    [self.view setNeedsDisplay];

                    break;
                }
                case -1:{
                    NSLog(@"移动cell更新失败");
                    
                    
                    break;
                }
                case -2:{
                    NSLog(@"移动cell用户未登录或者登录超时");
                    
                    
                    break;
                }
                case -9:{
                    NSLog(@"移动cell失败（连接个人中心失败或者参数错误");
                    
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
}

#pragma mark  手势 直播cell置顶
- (void)touchSingleTopGesture:(MyGesture *)sender
{
    
    NSLog(@"置顶啦");
    
    CGPoint location = [sender locationInView:self.table];
    
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:location];
    NSLog(@"%ld",(long)indexPath.row);
    
    NSLog(@"置顶之前%@",self.dataArr);
    
    if (indexPath.row == 0) {
        
        return;
    }
    
    if (self.dataArr.count>indexPath.row) {
        
        NSDictionary *_insertDic = [NSDictionary dictionaryWithDictionary:[self.dataArr objectAtIndex:indexPath.row]];
        
        if (_insertDic) {
            
            [self.dataArr insertObject:_insertDic atIndex:0];
            
            [self.dataArr removeObjectAtIndex:indexPath.row + 1];
            
            NSLog(@"置顶之后%@",self.dataArr);
            
            [self.table reloadData];
        }
    }
}

#pragma mark  手势 直播cell选中
- (void)touchSingleSelectGesture:(MyGesture *)sender
{
    NSLog(@"选中啦");
    
    CGPoint location = [sender locationInView:self.table];
    
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:location];
    
    NSLog(@"%@",indexPath);
    
    NSLog(@"%@",_dataArr);
    
    BOOL isExit = NO;
    
    for (int i=0; i<self.contentSelectedArr.count; i++) {
        
        NSString *cellID = [[self.contentSelectedArr objectAtIndex:i] objectForKey:@"id"];
        
        if ([cellID isEqualToString:[[_dataArr objectAtIndex:indexPath.row] objectForKey:@"id"]]) {
            
            [self.contentSelectedArr removeObjectAtIndex:i];
            
            isExit = YES;
            
            break;
        }
    }
    
    if (isExit) {
        
        [self.table reloadData];
        
    }else{
        
        BOOL flag = false;
        
        NSLog(@"添加前%@",self.contentSelectedArr);
        
        if (!flag) {
            
            [self.contentSelectedArr addObject:_dataArr[indexPath.row]];
        }
        
        [self.table reloadData];
        
    }
    
    NSLog(@"添加后%@",self.contentSelectedArr);
    
    [self callCheckDataListSelected];
    
}

#pragma mark 页面底端增加频道条手势事件
- (void)addChannelGesture
{
    NSLog(@"增加频道啦");
    
    if (IsLogin) {
        _tempArr = _dataArr;
        
        MineFavChannelSelectViewController *vc = [[MineFavChannelSelectViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        [AlertLabel AlertInfoWithText:@"请先登录哦！" andWith:self.view withLocationTag:0];
    }
}

#pragma mark 清空频道手势事件
- (void)cleanAllChannelGesture
{
    NSLog(@"开始大扫除啦");
    
    CustomAlertView *custom = [[CustomAlertView alloc] initWithMessage:@"您确定要清空喜爱频道吗" andButtonNum:2 withDelegate:self];
    
    custom.tag = 111;
    
    [custom show];
}


#pragma mark  提示框代理方法
- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 111) {
        
        if (buttonIndex == 0) {//确定按钮
            
            if (IsLogin) {//用户登陆的
                
                ISNoting = YES;
                
                HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                
                [request DelTVFavorateWithServiceID:@"" andTimeout:10];
                
            }else{//游客的本地的
                [_dataArr removeAllObjects];
                
                _cleanBtn.hidden = YES;
                _deleteView.hidden = YES;
                
                _promptView.hidden = NO;
            }
            
        }else if (buttonIndex == 1){//取消按钮
            
            ISNoting = NO;
            
            return;
        }

    }
}

#pragma mark 导航条上编辑按钮点击事件
- (void)dustbinBtnSenderDown:(UIButton *)sender
{

    sender.selected = !sender.selected;
        
        NSLog(@"+++++++++++++++++++清空啦");
        
        if (sender.selected) {
            
            NSLog(@"2号线");
            
            _addView.hidden = YES;
            
            _addBtn.hidden = YES;
            
            _editDeleteView.hidden = NO;
            
            isEditing = YES;
            
            UIView *editView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, 40*kDeviceRate)];
            editView.backgroundColor = UIColorFromRGB(0xffffff);
            
            
            UILabel *pindaoName = [[UILabel alloc]init];
            pindaoName.text = @"频道名称";
            pindaoName.textAlignment = NSTextAlignmentCenter;
            pindaoName.textColor = HT_COLOR_FONT_SECOND;
            pindaoName.font = HT_FONT_SECOND;
            [editView addSubview:pindaoName];
            
            [pindaoName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(editView.mas_left).offset(67*kDeviceRate);
                make.centerY.equalTo(editView.mas_centerY);
            }];
            
            UILabel *zhiding = [[UILabel alloc]init];
            zhiding.text = @"置顶";
            zhiding.textAlignment = NSTextAlignmentCenter;
            zhiding.textColor = HT_COLOR_FONT_SECOND;
            zhiding.font = HT_FONT_SECOND;
            [editView addSubview:zhiding];
            
            [zhiding mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(pindaoName.mas_right).offset(50*kDeviceRate);
                make.centerY.equalTo(editView.mas_centerY);
            }];
            
            UILabel *tuodong = [[UILabel alloc]init];
            tuodong.text = @"拖动排序";
            tuodong.textAlignment = NSTextAlignmentCenter;
            tuodong.textColor = HT_COLOR_FONT_SECOND;
            tuodong.font = HT_FONT_SECOND;
            [editView addSubview:tuodong];
            
            [tuodong mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(zhiding.mas_right).offset(20*kDeviceRate);
                make.centerY.equalTo(editView.mas_centerY);
            }];
            
            UILabel *xuanze = [[UILabel alloc]init];
            xuanze.text = @"选择";
            xuanze.textAlignment = NSTextAlignmentCenter;
            xuanze.textColor = HT_COLOR_FONT_SECOND;
            xuanze.font = HT_FONT_SECOND;
            [editView addSubview:xuanze];
            
            [xuanze mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(tuodong.mas_right).offset(20*kDeviceRate);
                make.centerY.equalTo(editView.mas_centerY);
            }];
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39*kDeviceRate, kDeviceWidth, 1*kDeviceRate)];
            lineView.backgroundColor = UIColorFromRGB(0Xeeeeee);
            [editView addSubview:lineView];
            
            [_table setTableHeaderView:editView];
            
        }else{
            
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            
            NSMutableArray *sortArr = [NSMutableArray array];
            
            for (int i = 0; i< self.dataArr.count; i++) {
                
                NSDictionary *tempDic = self.dataArr[i];
                
                NSMutableDictionary *sortDic = [NSMutableDictionary dictionary];
                
                [sortDic setValue:[NSString stringWithFormat:@"%d",i] forKey:@"seq"];
                [sortDic setValue:[tempDic objectForKey:@"id"] forKey:@"id"];
                [sortDic setValue:[tempDic objectForKey:@"name"] forKey:@"name"];
                
                [sortArr addObject:sortDic];
            }
            
            [request EPGUpdateTvFavoriteWithNewSortArr:sortArr andTimeout:KTimeout];
            
            //ww...
            [_localListCenter cleanLiveFavoriteChannelListWithUserType:YES];
            [DataPlist WriteToPlistArray:_dataArr ToFile:LIVEFAVORITEPLIST];
            
            [self.contentSelectedArr removeAllObjects];
            
            [self callCheckDataListSelected];
            
            NSLog(@"3号线");
            
            _addView.hidden = NO;
            
            _addBtn.hidden = NO;
            
            _editDeleteView.hidden = YES;
            
            isEditing = NO;
            
            [_table setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 3.75*kRateSize)]];
            
            if (self.dataArr.count == 0) {
                _cleanBtn.hidden = YES;
            }
            
            [self checkAddFavChannelView];
            
        }
        
        [self.table reloadData];
}

- (void)deleteBtnSenderDown
{
    NSLog(@"编辑界面删除频道啦");
    
    NSLog(@"deleteBtnSenderDown-----%@",self.contentSelectedArr);
    
    NSString *deleteId = @"";
    
    for (NSDictionary *selectTempDic in self.contentSelectedArr ) {
        
        NSString *tempStr = deleteId;
        
        NSString *selectId = [selectTempDic objectForKey:@"id"];
        
        if (tempStr.length) {
            deleteId = [NSString stringWithFormat:@"%@,%@", tempStr, selectId];
        }
        else{
            deleteId = selectId;
        }
    }
    
    NSLog(@"deleteId---%@",deleteId);
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request DelTVFavorateWithServiceID:deleteId andTimeout:10];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
