//
//  ForgetPwdViewController.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ForgetPwdViewController.h"
#import "UserTime.h"
#import "AlertLabel.h"
#import "CHECKFORMAT.h"
#import "MyGesture.h"
#import "CalculateTextWidth.h"
#import "CustomAlertView.h"

@interface ForgetPwdViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    BOOL phonenumberOK;//电话号码格式正确
    BOOL passwordOK;//密码格式正确
    BOOL authCodeOK;//验证码格式正确
}

@property (nonatomic, strong)CustomAlertView *customView;

@end

@implementation ForgetPwdViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"ForgetPWDPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"ForgetPWDPage"];
    
    [_customView resignKeyWindow];
    
//    _customView = nil;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"忘记密码"];

    self.view.backgroundColor = App_white_color;
    
    WS(wss);
    
    _verityPost = [[VerityAlertView alloc] init];
    [self.view addSubview:_verityPost];
    [_verityPost mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(44));
        make.top.equalTo(wss.view.mas_top).offset(20);
        make.width.equalTo(wss.view.mas_width);
        make.left.equalTo(wss.view.mas_left);
    }];
    _verityPost.hidden = YES;

    
    //账号
    _phoneField = [[GeneralInputBlock alloc] initWithTitle:@"账号：    " andPlaceholderText:@"您注册时的11位手机号"];
    _phoneField.inputBlock.secureTextEntry = NO;
    _phoneField.inputBlock.delegate = self;
    _phoneField.userInteractionEnabled = YES;
    [self.view addSubview:_phoneField];
    [_phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left);
        make.right.equalTo(wss.view.mas_right);
        make.top.equalTo(wss.verityPost.mas_bottom).offset(10*kRateSize);
        make.height.equalTo(@(44*kRateSize));
    }];


    //分割线一
    UIImageView *fSideLine = [UIImageView new];
    [fSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:fSideLine];
    [fSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(280*kRateSize));
        make.height.equalTo(@(0.5*kRateSize));
        make.centerX.equalTo(wss.phoneField.mas_centerX);
        make.top.equalTo(wss.phoneField.mas_bottom);
    }];
    
    
    //验证码
//    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码"];
    
    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码" withVerityBtnName:@"获取验证码"];
    [_verityField.verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
    
    _verityField.inputBlock.delegate = self;
//    _verityField.inputBlock.clearButtonMode = UITextFieldViewModeNever;
    _verityField.userInteractionEnabled = YES;
    [self.view addSubview:_verityField];
    [_verityField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.phoneField);
        make.top.equalTo(fSideLine.mas_bottom);
        make.left.equalTo(wss.phoneField.mas_left);
    }];
    _verityField.userInteractionEnabled = YES;
    
    
    //分割线二
    UIImageView *sSideLine = [UIImageView new];
    [sSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:sSideLine];
    [sSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(fSideLine);
        make.centerX.equalTo(wss.verityField.mas_centerX);
        make.top.equalTo(wss.verityField.mas_bottom);
    }];
    
    
//    //self.verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_verityBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
//    [_verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
////    [_verityBtn setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
//    [_verityBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
//    _verityBtn.titleLabel.font = App_font(13);
//    [_verityField addSubview:_verityBtn];
//    
//    CGSize veritySize = [CalculateTextWidth sizeWithText:_verityBtn.titleLabel.text font:App_font(13)];
//    int weightWW = veritySize.width;
//
//    [_verityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(wss.verityField.mas_centerY);
//        make.right.equalTo(wss.verityField.mas_right).offset(-20*kRateSize);
//        make.height.equalTo(wss.verityField.mas_height);
//        make.width.equalTo(@(weightWW*kRateSize));
//    }];
    
    
    //目的重新设置 textfield的frame,使自带删除属性的位置在获取验证码的左侧
//    [_verityField.inputBlock mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wss.verityField.titleLab.mas_right);
//        make.height.equalTo(wss.verityField.mas_height);
//        make.centerY.equalTo(wss.verityField.mas_centerY);
//        make.right.equalTo(wss.verityBtn.mas_left);
//    }];

    
    _PwdField = [[GeneralInputBlock alloc] initWithTitle:@"新密码：" andPlaceholderText:@"6-16位数字、字母或字符"];
    _PwdField.userInteractionEnabled = YES;
    _PwdField.inputBlock.delegate = self;
    _PwdField.inputBlock.secureTextEntry = YES;
    [self.view addSubview:_PwdField];
    [_PwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.phoneField);
        make.left.equalTo(wss.view.mas_left);
        make.top.equalTo(sSideLine.mas_bottom);
    }];
    
    
    //分割线三
    UIImageView *thSideLine = [UIImageView new];
    [thSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:thSideLine];
    [thSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(sSideLine);
        make.centerX.equalTo(wss.PwdField);
        make.top.equalTo(wss.PwdField.mas_bottom);
    }];
    
    
//    _errorView = [[ErrorAlertView alloc] init];
//    [self.view addSubview:_errorView];
//    _errorView.hidden = YES;
//    [_errorView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.top.equalTo(_PwdField.mas_bottom);
//        make.height.equalTo(@(25*kRateSize));
//    }];

    
    UIButton *complish = [UIButton buttonWithType:UIButtonTypeCustom];
    [complish setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_normal"] forState:UIControlStateNormal];
    [complish setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_pressed"] forState:UIControlStateHighlighted];
    [complish addTarget:self action:@selector(complishBtnSender) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:complish];
    [complish mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(305*kRateSize));
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(thSideLine.mas_bottom).offset(15*kRateSize);
    }];
    
    
    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
    
}


//获取验证码触发方法
- (void)getCoding:(UIButton*)sender
{
    [_phoneField.inputBlock resignFirstResponder];
    [_PwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    
    if ([_phoneField.inputBlock.text isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];
        
        return;
    }
    
   
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text])
    {
        phonenumberOK = YES;
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"2" andMobilePhone:_phoneField.inputBlock.text andTimeout:10];
        
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";

        //[AlertLabel AlertInfoWithText:@"请检查手机号码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];
    }
}

- (void)complishBtnSender
{
    
    [_phoneField.inputBlock resignFirstResponder];
    [_PwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    //手机号码比对
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text]) {
        
        phonenumberOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";
        
        //[AlertLabel AlertInfoWithText:@"请检查手机号码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];

        return;
    }
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityField.inputBlock.text]) {
        
        authCodeOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请正确填写验证码";
        
        //[AlertLabel AlertInfoWithText:@"请正确填写验证码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请正确填写验证码";//请正确填写验证码
        [_customView show];
        
        return;
    }
    
    //密码比对
    if ([CHECKFORMAT checkPassword:_PwdField.inputBlock.text]) {
        
        passwordOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请正确填写密码";

        //[AlertLabel AlertInfoWithText:@"密码不符合规范" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请正确填写密码";
        [_customView show];
        
        return;
    }
    
    if (phonenumberOK&passwordOK&authCodeOK) {
        //发起请求
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request NewresetUserPwdWithMobilePhone:_phoneField.inputBlock.text andPassWord:_PwdField.inputBlock.text andAuthCode:_verityField.inputBlock.text andTimeout:10];
        
    }else{
        //检查账号和密码格式是否正确
        
        return;
    }
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        //        [_customView resignKeyWindow];
        
        //        _customView = nil;
        
        _customView.hidden = YES;
    }
    return;
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
    
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    NSLog(@"+++++++++++++++++++++++发送验证码");
                    
//                    _errorView.hidden = YES;
                    
                    [UserTime changBtn:_verityField.verityBtn withSleepSecond:60];
                    [self.verityField.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];

//                    WS(wss);
//                    
//                    _verityPost.phoneNumLable.text = _phoneField.inputBlock.text;
//                    
//                    [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                        make.height.equalTo(@(44));
//                        make.top.equalTo(wss.view.mas_top).offset(64);
//                        make.width.equalTo(wss.view.mas_width);
//                        make.left.equalTo(wss.view.mas_left);
//                    }];
//                    _verityPost.hidden = NO;
//                    
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        WS(wss);
//                        [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                            make.height.equalTo(@(44));
//                            make.top.equalTo(wss.view.mas_top).offset(20);
//                            make.width.equalTo(wss.view.mas_width);
//                            make.left.equalTo(wss.view.mas_left);
//                            
//                            _verityPost.hidden = YES;
//                        }];
//                        
//                    });

                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"请输入正确的手机号码";
                    
                    //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    [_customView show];

                    break;
                }
                    
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"验证码已发疯 请明日再试";
                    
                    //[AlertLabel AlertInfoWithText:@"验证码已发疯 请明日再试" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    [_customView show];

                    
                    break;
                }
                case -6:{
                    NSLog(@"手机号未绑定");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"用户不存在";
                    
                    //[AlertLabel AlertInfoWithText:@"用户不存在" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"用户不存在";
                    [_customView show];

                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";
                    
                    //[AlertLabel AlertInfoWithText:@"无法访问个人中心" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    [_customView show];

                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
//            _errorView.hidden = YES;
            
            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
            
            return;
        }
    }else if ([type isEqualToString:resetUserPwd]) {
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"更改成功");
                    
//                    _errorView.hidden = YES;

                    //保存新密码，并且必须重新登录
                    
                    [[NSUserDefaults standardUserDefaults] setObject:_phoneField.inputBlock.text forKey:@"mindUserName"];//自动登录的时候记住账号
                    
                    //[[NSUserDefaults standardUserDefaults] setObject:_PwdField.inputBlock.text forKey:@"mindPWD"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];

                    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsLogin" ];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    //                    [AlertLabel AlertInfoWithText:@"密码修改成功！" andWith:self.view withLocationTag:0];
                    //
                    //                    [self performSelector:@selector(goToLogin) withObject:nil afterDelay:3];
                    
                    [self goBack];
                    
                    break;
                }
                case -3:{
                    NSLog(@"手机号错误");
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindUserName"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"请输入正确的手机号码";

                   // [AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    [_customView show];

                    break;
                }
                case -7:{
                    NSLog(@"验证码错误或者超时效");
                    
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindUserName"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];
//
//                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"验证码已过期 请重新获取";
                    
                    //[AlertLabel AlertInfoWithText:@"验证码已过期 请重新获取" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"验证码已过期 请重新获取";
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindUserName"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];
//
//                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";
                    
                    _customView.messageStr = @"无法访问个人中心";
                    [_customView show];

                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
//            _errorView.hidden = YES;

            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
            
//            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindUserName"];
//            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];
//
//            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }
    }
}


#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_phoneField.inputBlock resignFirstResponder];
    [_PwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_phoneField.inputBlock resignFirstResponder];
    [_PwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
}



- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
