//
//  NicknameViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/30.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "NicknameViewController.h"
#import "AlertLabel.h"
#import "CHECKFORMAT.h"

@interface NicknameViewController ()<UITextFieldDelegate,HTRequestDelegate>
{
    UIButton *backBtn;//返回按钮
    
    BOOL inRule;
    
    BOOL lengthOk;
    
    BOOL noEMOJI;
    
    BOOL isClickSaveBtn;
}

@property (nonatomic,strong)UIButton *saveBtn;//保存按钮
@property (nonatomic,strong)UITextField *textField;
@property (nonatomic,strong)UILabel *holdLab;

@end

@implementation NicknameViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"NickNamePage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"NickNamePage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"修改昵称"];
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goToBack)];
    [self setNaviBarLeftBtn:backBtn];
    
    _saveBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_user_save" imgHighlight:@"title_save_click" imgSelected:@"btn_user_save" target:self action:@selector(saveTheUserName)];
    
    [self setNaviBarRightBtn:_saveBtn];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, self.view.frame.size.width, 50*kDeviceRate)];
    backView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:backView];
    
    
    WS(wss);
    _textField = [UITextField new];
    _textField.clearButtonMode = UITextFieldViewModeAlways;
    _textField.delegate = self;
    _textField.backgroundColor = [UIColor clearColor];
//    [_textField setValue: UIColorFromRGB(0x999999) forKeyPath:@"_placeholderLabel.textColor"];
//    [_textField setValue:[UIFont systemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    [_textField addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
    [backView addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(backView.mas_height);
        make.left.equalTo(backView.mas_left).offset(12*kDeviceRate);
        make.right.equalTo(backView.mas_right);//offset(-20*kRateSize);
        make.centerY.equalTo(backView.mas_centerY);
    }];
    [_textField becomeFirstResponder];

    
    if (isBeforeIOS7) {
        
        _textField.placeholder = @"请输入昵称";

    }else{
    
        _holdLab = [UILabel new];
        _holdLab.text = @"请输入昵称";
        _holdLab.textColor = HT_COLOR_FONT_WARNING;//UIColorFromRGB(0x999999);
        _holdLab.font = HT_FONT_SECOND;
        _holdLab.textAlignment = NSTextAlignmentLeft;
        [_textField addSubview:_holdLab];
        [_holdLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(wss.textField);
            make.center.equalTo(wss.textField);
        }];

    }
    
    
    
    UILabel *placeholdLab = [UILabel new];
    placeholdLab.text = @"支持汉字、英文或数字，1-16个字符";
    placeholdLab.font = HT_FONT_FOURTH;
    placeholdLab.textColor = HT_COLOR_FONT_SECOND;
    //placeholdLab.backgroundColor = [UIColor clearColor];
    placeholdLab.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:placeholdLab];
    [placeholdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(wss.view.mas_right).offset(-12*kDeviceRate);
        make.left.equalTo(wss.view.mas_left);//.offset(111*kRateSize);
        make.top.equalTo(backView.mas_bottom).offset(12*kDeviceRate);
        make.height.equalTo(@(20*kDeviceRate));
    }];
}


+ (BOOL)isEmoji:(char *)codePoint
{
    
    
    return (codePoint == 0x0)
    || (codePoint == 0x9)
    || (codePoint == 0xA)
				|| (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
				|| ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
}

+ (BOOL)handleEmoji:(NSString *)sourceStr
{
    NSInteger len = sourceStr.length;
    
    for (NSInteger i = 0; i<len; i++) {
        
        NSString *template = [sourceStr substringWithRange:NSMakeRange(i, 1)];
        
        const char *itemChar = [template UTF8String];
        
        if ([self isEmoji:itemChar])
        {
            return NO;
        };
    }
    
    return YES;
}



- (void)saveTheUserName
{
    if (!isClickSaveBtn) {
        
        isClickSaveBtn = YES;
        
        [_textField resignFirstResponder];
        
        if ([CHECKFORMAT checkNickName:self.textField.text]) {
            
            inRule = YES;
            
        }else{
            
            [AlertLabel AlertInfoWithText:@"昵称长度不在范围内~" andWith:self.view withLocationTag:0];
            
            isClickSaveBtn = NO;
            
            return;
        }
        
        
        if ([NicknameViewController handleEmoji:self.textField.text]) {
            
            noEMOJI = YES;
        }else{
            
            [AlertLabel AlertInfoWithText:@"昵称只支持汉字,英文或数字" andWith:self.view withLocationTag:0];
            
            isClickSaveBtn = NO;
            
            return;
        }
        
        
        if (inRule & noEMOJI) {// & lengthOk
            
            //上传服务器
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            [request NewUpdateNickNameWithNickName:self.textField.text andTimeout:10];
            
        }
    }else{
    
        NSLog(@"慎重 保存按钮点击的次数太多了！！！");
    }
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if (status == 0) {
        
        if ([type isEqualToString:@"updateNickName"]) {
            NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
            
            if ([result count]>0) {
                switch ([[result objectForKey:@"ret"] integerValue]) {
                    case 0:{
                        
                        [[NSUserDefaults standardUserDefaults] setObject:self.textField.text forKey:@"NickName"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];//数据同步到系统文件中
                        
                        
                        [AlertLabel AlertInfoWithText:@"昵称保存成功！" andWith:self.view withLocationTag:0];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANG_ACCOUNT_IMAGE" object:nil];
                        
                        [self performSelector:@selector(goToBack) withObject:nil afterDelay:2];
                        
                        break;
                    }
                    case -2:{
                        //用户未登录或者登录超时
                        [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:0];
                        
                        isClickSaveBtn = NO;
                        break;
                    }
                    case -5:{
                        //昵称已存在
                        
                        [AlertLabel AlertInfoWithText:@"昵称已存在" andWith:self.view withLocationTag:0];
                        
                        isClickSaveBtn = NO;
                        
                        break;
                    }
                    case -9:{
                        //其它异常（如：无法访问用户中心、参数异常）
                        
                        [AlertLabel AlertInfoWithText:@"无法访问用户中心" andWith:self.view withLocationTag:0];
                        
                        isClickSaveBtn = NO;
                        
                        break;
                    }
                    default:
                        
                        break;
                }
            }
        }
        
    }else{//请求失败接口不通的时候
    
         [AlertLabel AlertInfoWithText:@"无法访问用户中心" andWith:self.view withLocationTag:0];
        
         isClickSaveBtn = NO;
    }
}

- (void)inputBlockValueChange:(UITextField *)textField{
    
    if (textField.text.length>0) {
        
        _holdLab.hidden = YES;
        
    }else{
        _holdLab.hidden = NO;
    }
}


- (void)goToBack
{
    //_textField.text = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark textfield Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil) {
//        self.view = nil;
//    }
}



@end
