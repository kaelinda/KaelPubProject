//
//  NewLoginViewController.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewLoginViewController.h"
#import "UrlPageJumpManeger.h"

@interface NewLoginViewController ()<UITextFieldDelegate,CustomAlertViewDelegate,HTRequestDelegate>

@end

@implementation NewLoginViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [MobCountTool pushInWithPageView:@"newLoginPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"newLoginPage"];
    
    [_customView resignKeyWindow];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(toMinePage)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"短信验证码登录"];
    
    self.view.backgroundColor = App_white_color;
    
    [self setUpMainView];
    
     _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
}

- (void)setUpMainView
{
    WS(wss);
    
    _phoneField = [[GeneralInputBlock alloc] initWithTitle:@"手机号：" andPlaceholderText:@"请输入11位手机号" withVerityBtnName:@"获取验证码"];
    [_phoneField.verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
    _phoneField.inputBlock.delegate = self;
    _phoneField.userInteractionEnabled = YES;
    [self.view addSubview:_phoneField];
    [_phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wss.view);
        make.height.mas_equalTo(50*kRateSize);
        make.top.mas_equalTo(wss.view.mas_top).offset(64);
    }];

    
    //第一条分割线
    UIImageView *firstSideLine = [UIImageView new];
    [firstSideLine setBackgroundColor:App_line_color];
    [self.view addSubview:firstSideLine];
    [firstSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left).offset(20*kRateSize);
        make.right.equalTo(wss.view.mas_right).offset(-20*kRateSize);
        make.height.equalTo(@(0.5*kRateSize));
        make.top.equalTo(_phoneField.mas_bottom);
    }];
    
    
    //验证码输入框
    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入短信中的验证码"];
    _verityField.inputBlock.secureTextEntry = NO;
    _verityField.inputBlock.delegate = self;
    _verityField.userInteractionEnabled = YES;
    [self.view addSubview:_verityField];
    [_verityField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(firstSideLine.mas_bottom);
        make.size.equalTo(wss.phoneField);
    }];
    
    
    
    //第二条分割线
    UIImageView *secondSideLine = [UIImageView new];
    [secondSideLine setBackgroundColor:App_line_color];
    [self.view addSubview:secondSideLine];
    [secondSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(firstSideLine);
        make.top.equalTo(wss.verityField.mas_bottom);
        make.centerX.equalTo(wss.verityField.mas_centerX);
    }];
    
    
    
    //登录按钮
    _LoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_normal"] forState:UIControlStateNormal];
    [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_pressed"] forState:UIControlStateHighlighted];
    [_LoginBtn addTarget:self action:@selector(LoginSender:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_LoginBtn];
    [_LoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(305*kRateSize));
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(secondSideLine.mas_bottom).offset(30*kRateSize);
    }];
    
    
    
    UIView *protocalView = [UIView new];
    [protocalView setBackgroundColor:CLEARCOLOR];
    [self.view addSubview:protocalView];
    [protocalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.height.equalTo(@(22*kRateSize));
        make.top.equalTo(wss.LoginBtn.mas_bottom).offset(15*kRateSize);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    //是否同意服务协议
    _ifAgreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //..............此处应该放默认图片
    [_ifAgreeBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"] forState:UIControlStateNormal];
    //... 选中时
    [_ifAgreeBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"] forState:UIControlStateSelected];
    [_ifAgreeBtn addTarget:self action:@selector(ifAgreement:) forControlEvents:UIControlEventTouchUpInside];
    [protocalView addSubview:_ifAgreeBtn];
    [_ifAgreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(protocalView.mas_left).offset(75*kRateSize);
        make.width.height.equalTo(@(22*kRateSize));
        make.top.equalTo(protocalView.mas_top);
    }];
    // 默认
    _ifAgreeBtn.selected = YES;
    _ifReadRule = YES;
    
    
    UILabel *agreeInfoLab = [UILabel new];
    agreeInfoLab.text = @"我已同意该应用的服务协议";
    agreeInfoLab.font = App_font(11);
    agreeInfoLab.textAlignment = NSTextAlignmentLeft;
    agreeInfoLab.backgroundColor = CLEARCOLOR;
    agreeInfoLab.textColor = UIColorFromRGB(0x999999);
    agreeInfoLab.adjustsFontSizeToFitWidth = YES;
    [protocalView addSubview:agreeInfoLab];
    
    CGSize agreeSize = [CalculateTextWidth sizeWithText:agreeInfoLab.text font:App_font(11)];
    int heightWW = agreeSize.height;
    
    [agreeInfoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.ifAgreeBtn.mas_right).offset(6*kRateSize);
        make.height.equalTo(@(heightWW*kRateSize));
        make.centerY.equalTo(wss.ifAgreeBtn.mas_centerY);
    }];
    agreeInfoLab.userInteractionEnabled = YES;
    
    
    UIImageView *protocolImg = [[UIImageView alloc] init];
    [protocolImg setImage:[UIImage imageNamed:@"img_user_protocol_arrow"]];
    [protocalView addSubview:protocolImg];
    [protocolImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(6*kRateSize));
        make.height.equalTo(@(8*kRateSize));
        make.centerY.equalTo(agreeInfoLab.mas_centerY);
        make.left.equalTo(agreeInfoLab.mas_right).offset(4*kRateSize);
    }];
    
    
    MyGesture *serviceGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheServiceVC)];
    serviceGes.numberOfTapsRequired = 1;
    [agreeInfoLab addGestureRecognizer:serviceGes];
    
}


#pragma mark   进入甜果服务协议
- (void)goToTheServiceVC
{

    KnowledgeBaseViewController *service = [[KnowledgeBaseViewController alloc] init];
    
    service.urlStr = [UrlPageJumpManeger loginPageServiceVCJump];
    
    service.existNaviBar = YES;
    
    [self.navigationController pushViewController:service animated:YES];
    
}


#pragma mark   是否同意服务协议按钮触发事件
- (void)ifAgreement:(UIButton *)sender
{//...此处要更换图片
    
    sender.selected = !sender.selected;
    
    if(sender.selected){
        _ifReadRule = YES;
        NSLog(@"yeyeyeeyeyyeyeyeyeyeyey");
        
        [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_normal"] forState:UIControlStateNormal];
        
        _LoginBtn.enabled = YES;
    }else{
        _ifReadRule = NO;
        NSLog(@"nonononononononononononono");
        
        [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_disable"] forState:UIControlStateNormal];
        
        _LoginBtn.enabled = NO;
        //[AlertLabel AlertInfoWithText:@"请同意应用协议" andWith:self.view withLocationTag:0];
    }
    
    NSLog(@"%d", _ifReadRule);
}


#pragma mark   获取验证码触发方法
- (void)getCoding:(UIButton*)sender
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    if ([_phoneField.inputBlock.text isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];
        
        return;
    }
    
    NSLog(@"+++++++++++++++++++++++发送验证码");
    
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text])
    {
        _phonenumberOK = YES;
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"4" andMobilePhone:_phoneField.inputBlock.text andTimeout:KTimeout];
        
    }else{
        
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];
    }
}


#pragma mark   登录按钮触发事件
- (void)LoginSender:(UIButton *)sender
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    //手机号检验
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text]&&(_phoneField.inputBlock.text.length>0))
    {
        _phonenumberOK = YES;
        
    }else{
        _customView.messageStr = @"账号输入不正确";
        [_customView show];
        
        return;
    }
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityField.inputBlock.text]) {
        
        _authCodeOK=YES;
    }else{
        
        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    
    if (_phonenumberOK & _authCodeOK & _ifReadRule) {
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request authCodeLoginWithMobilePhone:_phoneField.inputBlock.text andAuthCode:_verityField.inputBlock.text andTimeout:KTimeout];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在登录 请稍后"];
        
    }else{
        
        [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        return;
    }
}

#pragma mark   请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    [DejalBezelActivityView removeView];
    
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        
        NSLog(@"动态口令登录返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    [UserTime changBtn:self.phoneField.verityBtn withSleepSecond:60];
                    [self.phoneField.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
    
                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
                    _customView.messageStr = @"该手机号已注册";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
    }
    else if ([type isEqualToString:authCodeLogin]){//动态口令登录
        NSLog(@"动态口令登录返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
        
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    if (status == 0) {
                        
                        NSLog(@"动态口令登录成功");
                        
                        
                        if (![KMobileNum isEqualToString:[NSString stringWithFormat:@"%@",_phoneField.inputBlock.text]]) {
                            
                            NSLog(@"我做一个请求");
                        }
                        
                        
                        //登录成功后存储相关信息及操作
                        [HTYJUserCenter saveUserLoginProfileWithDic:result andUserAccount:_phoneField.inputBlock.text andUserPassword:@""];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"AUTOLOGIN" object:nil];//自动登录的通知
                                            
                        [self performSelector:@selector(toMinePage) withObject:nil afterDelay:0.5];
                        
                        
                        
                        
                        
                        

                    }
                    
                    break;
                }
                case -1:{
                    NSLog(@"失败");
                    
                    _customView.messageStr = @"失败";
                    [_customView show];
                    
                    break;
                }
                case -3:{
                    NSLog(@"验证码错误");
                    
                    _customView.messageStr = @"验证码错误";
                    [_customView show];
                    
                    break;
                }
                case -7:{
                    NSLog(@"验证码超时效");
                    
                    _customView.messageStr = @"验证码超时效";
                    [_customView show];
                    
                    break;
                }
                case -9:
                {
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"登录超时 请重试！";
                    [_customView show];
                    
                    break;
                    
                }
                default:
                    
                    [AlertLabel AlertInfoWithText:@"登录超时 请重试！" andWith:self.view withLocationTag:0];
                    
                    break;
            }
        }
    }
    
    else{
        [AlertLabel AlertInfoWithText:@"请检查网络连接" andWith:self.view withLocationTag:0];
    }
}


- (void)toMinePage
{
    if (_isPlayerEnter) {
        
        
        [self.navigationController popViewControllerAnimated:YES];
    }else
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
    
        _customView.hidden = YES;
    }
    
    return;
}


#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
