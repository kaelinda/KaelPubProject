//
//  MineRecorderViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineRecorderViewController.h"
#import "LoginViewController.h"
#import "MyGesture.h"
#import "AlertLabel.h"
#import "MineVodTableViewCell.h"
#import "DataPlist.h"
#import "DemandVideoPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "MyGesture.h"
#import "MBProgressHUD.h"
#import "CustomAlertView.h"
#import "CalculateTextWidth.h"
#import "ChannelPlayRecorderCell.h"
#import "Tool.h"
#import "UserToolClass.h"
#import "SelectAndDeleteView.h"
#import "LocalCollectAndPlayRecorder.h"


@interface MineRecorderViewController ()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,MBProgressHUDDelegate>
{
    //  跳到  小屏幕 所需数据
    NSString *_channelID;     // 频道编号
    NSString *_channelName;   // 频道名称
    NSString *_PlayTag;       //是否可直播
    NSString *_PlayBackTag;   //是否可回看
    NSString *_eventId;   // 节目 id
    NSString *_eventName;   //节目名称
    NSString *_channelImageLink;//频道图标链接
    NSString *_videoTime;// "2014-09-22 11:50";
    NSString *_endDate;
    
    BOOL toEpgPlayer;//防止多次点击回看cell，多次push
    BOOL toDemandPlayer;//防止多次点击点播cell，多次push
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    
    NSInteger _vodPlaylistLength;
    NSInteger _epgPlaylistLength;
}

@property (nonatomic, strong)SelectAndDeleteView *selectAndDeleteView;
@property (nonatomic, strong)NSMutableArray *deleteArr;
@property (nonatomic, strong)UIView *scrolContentView;
@property (nonatomic) BOOL isFirstLoading;
@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;


@end

@implementation MineRecorderViewController

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSMutableArray *)liveArr
{
    if (!_liveArr) {
        _liveArr = [NSMutableArray array];
    }
    return _liveArr;
}

- (NSMutableArray *)vodArr
{
    if (!_vodArr) {
        _vodArr = [NSMutableArray array];
    }
    return _vodArr;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"MineRecorderPage"];

    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    
    
    if (_isFirstLoading) {
        
        [self refreshRecorderTableCell];
    }
    
    _isFirstLoading = YES;
   
    if (_fromHome) {
        
        [_liveLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [_vodLab setTitleColor:App_selected_color forState:UIControlStateNormal];
        
        [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        
        _liveLab.titleLabel.font = HT_FONT_SECOND;
        _vodLab.titleLabel.font = HT_FONT_FIRST;
        
        _epgLine.hidden = YES;
        _vodLine.hidden = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [_scrollView setContentOffset:CGPointMake(kDeviceWidth, 0)];
            
            _cleanBtn.hidden = NO;

        });

    }
    
    toEpgPlayer = YES;
    toDemandPlayer = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineRecorderPage"];
    
}


- (void)refreshRecorderTableCell
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromEpgPlayer"]) {//回来记录列表进入直播播放器的返回标记
        
        _liveArr = [NSMutableArray arrayWithArray:[_localListCenter GetReWatchPlayRecordWithUserType:IsLogin]];
        
        if (_liveArr.count>0) {
            _cleanBtn.hidden= NO;
            
            [self hiddenCustomHUD];
            [self hideLoading];
            [_liveTable reloadData];
        }else{
            _cleanBtn.hidden= YES;
        }
        
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromEpgPlayer"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDemandPlayer"]) {//点播记录列表进入点播播放器的返回标记
        
        if (_scrollView.contentOffset.x == 1){
            
            _cleanBtn.hidden = YES;
        }
        _vodArr = [NSMutableArray arrayWithArray:[_localListCenter GetVODPlayRecordWithUserType:IsLogin]];
        
        if (_vodArr.count>0) {
            
            NSLog(@"点播播放记录%@",_vodArr);
            
            _noVODDataLabel.hidden = YES;
            
            if (_scrollView.contentOffset.x == 1){
                
                _cleanBtn.hidden = NO;
            }
            
            [_vodTable reloadData];
        }
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDemandPlayer"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    _isFirstLoading = NO;
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    _isLive_not_vod = YES;
    
    _deleteArr = [NSMutableArray arrayWithCapacity:0];
    
    _ifCleanVOD = NO;
    _ifCleanLive = NO;
    
    [self.view setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"播放记录"];
    
    _cleanBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_del_normal" imgHighlight:@"btn_common_title_del_normal" imgSelected:@"btn_user_cancel" target:self action:@selector(dustbinBtnSenderDown:)];
    
    [self setNaviBarRightBtn:_cleanBtn];
    
    _cleanBtn.hidden = YES;
    
    [self makeTheMainPartUI];
    
    [self showCustomeHUD];

    
    [self loadingData];
}

- (void)loadingData
{
    _cleanBtn.hidden= YES;
    
    _liveArr = [NSMutableArray arrayWithArray:[_localListCenter GetReWatchPlayRecordWithUserType:IsLogin]];
    
    NSLog(@"回看播放记录----%@",_liveArr);
    
    if (_liveArr.count>0) {
        
        _cleanBtn.hidden= NO;
    }else{
        
        _cleanBtn.hidden= YES;
    }
    [_liveTable reloadData];
    
    
    
    if (_scrollView.contentOffset.x == 1){
        
        _cleanBtn.hidden = YES;
    }
    
    _vodArr = [NSMutableArray arrayWithArray:[_localListCenter GetVODPlayRecordWithUserType:IsLogin]];

    
    NSLog(@"点播播放记录----%@",_vodArr);
    if(_vodArr.count>0){
        
        if (_scrollView.contentOffset.x == 1){
            
            _cleanBtn.hidden = NO;
        }
        
        [_vodTable reloadData];
    }
    
    [self hiddenCustomHUD];
    [self hideLoading];
}

- (void)showLoading
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self hideLoading];
    });
}


- (void)hideLoading
{
    if (_vodArr.count>0 && _liveArr.count>0) {
        _noLiveDataLabel.hidden = YES;
        
        _noVODDataLabel.hidden = YES;
        
    }else if (_liveArr.count>0 && _vodArr.count <= 0){
        
        _noVODDataLabel.hidden = NO;
        
        _noLiveDataLabel.hidden = YES;
        
    }else if (_vodArr.count>0 && _liveArr.count <= 0){
        
        _noVODDataLabel.hidden = YES;
        
        _noLiveDataLabel.hidden = NO;
    }else{
        
        _noLiveDataLabel.hidden = NO;
        
        _noVODDataLabel.hidden = NO;
    }
}


- (void)makeTheMainPartUI
{
    WS(wss);
    
    _liveLab = [UIButton buttonWithType:UIButtonTypeCustom];
    [_liveLab setTitle:@"回看" forState:UIControlStateNormal];
    [_liveLab setTitleColor:App_selected_color forState:UIControlStateNormal];
    [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
    _liveLab.titleLabel.font = HT_FONT_FIRST;
    [_liveLab addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_liveLab];
    _liveLab.frame = CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth/2, 38*kDeviceRate);
    _liveLab.tag = 1;
    
    
    _epgLine = [UIImageView new];
    [_epgLine setImage:[UIImage imageNamed:@"bg_user_playrecorder_short_line"]];
    [self.view addSubview:_epgLine];
    [_epgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(90*kDeviceRate));
        make.height.equalTo(@(1.5*kDeviceRate));
        make.bottom.equalTo(wss.liveLab.mas_bottom);
        make.centerX.equalTo(wss.liveLab.mas_centerX);
    }];
    
    
    _vodLab = [UIButton buttonWithType:UIButtonTypeCustom];
    [_vodLab setTitle:@"点播" forState:UIControlStateNormal];
    [_vodLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
    [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
    _vodLab.titleLabel.font = HT_FONT_SECOND;
    [self.view addSubview:_vodLab];
    [_vodLab addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventTouchUpInside]
    ;
    _vodLab.frame = CGRectMake(kDeviceWidth/2, 64+kTopSlimSafeSpace, kDeviceWidth/2, 38*kDeviceRate);
    _vodLab.tag = 2;
    
    
    _vodLine = [UIImageView new];
    [_vodLine setImage:[UIImage imageNamed:@"bg_user_playrecorder_short_line"]];
    [self.view addSubview:_vodLine];
    [_vodLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(90*kDeviceRate));
        make.height.equalTo(@(1.5*kDeviceRate));
        make.bottom.equalTo(wss.vodLab.mas_bottom);
        make.centerX.equalTo(wss.vodLab.mas_centerX);
    }];
    _vodLine.hidden = YES;
    
    
    _honLine = [UIImageView new];
    [_honLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [self.view addSubview:_honLine];
    _honLine.frame = CGRectMake(0, 39*kDeviceRate+64+kTopSlimSafeSpace, kDeviceWidth, 1*kDeviceRate);

    
    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top).offset(64+40*kDeviceRate+kTopSlimSafeSpace);
        make.width.equalTo(@(kDeviceWidth));
        make.bottom.equalTo(wss.view.mas_bottom).offset(-kBottomSafeSpace);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    
    _scrolContentView = [UIView new];
    [_scrolContentView setBackgroundColor:[UIColor clearColor]];
    [_scrollView addSubview:_scrolContentView];
    [_scrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wss.scrollView);
        make.height.equalTo(wss.scrollView);
    }];

    
    _noLiveDataLabel = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 200*kDeviceRate, self.view.frame.size.width, 88*kDeviceRate) andImgName:@"ic_user_playrecord_metadata" andTitle:@"暂无播放记录"];
    _noLiveDataLabel.hidden = YES;
    [_scrollView addSubview:_noLiveDataLabel];
    
    
    _noVODDataLabel = [[GeneralPromptView alloc] initWithFrame:CGRectMake(kDeviceWidth, 200*kDeviceRate, kDeviceWidth, 88*kDeviceRate) andImgName:@"ic_user_playrecord_metadata" andTitle:@"暂无播放记录"];
    _noVODDataLabel.hidden = YES;
    [_scrollView addSubview:_noVODDataLabel];
    
    
    
    //直播table
    _liveTable = [[UITableView alloc] init];
    _liveTable.delegate = self;
    _liveTable.dataSource = self;
    _liveTable.backgroundColor = [UIColor clearColor];
    _liveTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _liveTable.tag = 909090;
    [_scrolContentView addSubview:_liveTable];
    _liveTable.showsHorizontalScrollIndicator = NO;
    _liveTable.showsVerticalScrollIndicator = NO;
    [_liveTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(kDeviceWidth));
        make.height.equalTo(wss.scrollView.mas_height);
        make.centerY.equalTo(wss.scrollView.mas_centerY);
        make.left.equalTo(@(0));
    }];
    
    
    //点播table
    _vodTable = [[UITableView alloc] init];
    _vodTable.delegate = self;
    _vodTable.dataSource = self;
    _vodTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _vodTable.tag = 909091;
    [_scrolContentView addSubview:_vodTable];
    _vodTable.backgroundColor = [UIColor clearColor];
    _vodTable.showsHorizontalScrollIndicator = NO;
    _vodTable.showsVerticalScrollIndicator = NO;
    
    [_vodTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.liveTable);
        make.left.equalTo(wss.liveTable.mas_right);
        make.centerY.equalTo(wss.scrollView.mas_centerY);
    }];
    
    [_scrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(wss.vodTable.mas_right);
    }];

    
    //点击垃圾桶按钮后出现的view
    _selectAndDeleteView = [[SelectAndDeleteView alloc] init];
    [_selectAndDeleteView setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:_selectAndDeleteView];
    _selectAndDeleteView.hidden = YES;
    _selectAndDeleteView.userInteractionEnabled = YES;
    [_selectAndDeleteView.selectBtn addTarget:self action:@selector(selectAllItemsButSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    [_selectAndDeleteView.deleteBtn addTarget:self action:@selector(deleteBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];

    [_selectAndDeleteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.top.equalTo(wss.scrollView.mas_bottom);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.height.equalTo(@(60*kDeviceRate+kBottomSafeSpace));
    }];
}


#pragma mark 全选按钮触发事件
- (void)selectAllItemsButSenderDown:(VODDeleteButton *)btn
{
    NSLog(@"我是全选按钮");

    NSInteger current = _scrollView.contentOffset.x / kDeviceWidth;
    
    NSLog(@"%ld",(long)current);
    
    btn.selected = !btn.selected;
    
    if (!current) {//回看
        
        NSLog(@"--------------回看记录被全选———————————");
        
        if (btn.selected) {//全选回看
            
            [_deleteArr removeAllObjects];
            
            NSMutableArray *tempArr = nil;
            for (NSDictionary *liveDic in _liveArr) {
                
                tempArr = [liveDic objectForKey:@"playList"];
                [_deleteArr addObjectsFromArray:tempArr];
            }

            _ifCleanLive = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
        }else{//取消全选
            
            [self.deleteArr removeAllObjects];
            
            _ifCleanLive = NO;
            
            _selectAndDeleteView.selectLab.text = @"全选";
        }
        
        [_liveTable reloadData];
        
        [self cellCheckDataListSelected];
        
    }else{//点播
        
        NSLog(@"--------------点播记录被全选———————————");
        
        if (btn.selected) {//全选回看
            
            [_deleteArr removeAllObjects];
            
            NSMutableArray *tempArr = nil;
            for (NSDictionary *vodDic in _vodArr) {
                
                tempArr = [vodDic objectForKey:@"playList"];
                [_deleteArr addObjectsFromArray:tempArr];
            }
            
            _ifCleanVOD = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
        }else{//取消全选
            
            [self.deleteArr removeAllObjects];
            
            _ifCleanVOD = NO;
         
            _selectAndDeleteView.selectLab.text = @"全选";
        }

        [_vodTable reloadData];
        
        [self cellCheckDataListSelected];
        
    }
}

//根据deleteArr判断删除按钮的状态
- (void)cellCheckDataListSelected
{
    if (self.deleteArr.count >0) {
        
        _selectAndDeleteView.deleteBtn.enabled = YES;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"sl_user_playrecord_del_item_normal"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = [NSString stringWithFormat:@"删除( %ld )",_deleteArr.count];
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_REMIND;//UIColorFromRGB(0xff9c00);
        
    } else {
        
        _selectAndDeleteView.deleteBtn.enabled = NO;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"btn_user_disable_click"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = @"删除";
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_FONT_WARNING;//UIColorFromRGB(0xcccccc);
    }
}


#pragma mark 删除按钮触发事件
- (void)deleteBtnSenderDown:(VODDeleteButton *)btn
{
    NSLog(@"我是删除按钮");
    
    NSInteger current = _scrollView.contentOffset.x / kDeviceWidth;
    
    NSLog(@"%ld",(long)current);
    
    btn.selected = !btn.selected;
    
    if (!current) {//回看
        
        NSLog(@"--------------回看的删除按钮被全选———————————");
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        if (_ifCleanLive) {//全选的删除
            
            [request DelEventPlayWithEventID:@"" andTimeout:10];
        }else{//正常的选择删除
            
            if (_deleteArr.count == 0) {
                
                return;
            }
            
            NSMutableArray *eventIDArr = [NSMutableArray array];
            
            for (NSDictionary *dic in _deleteArr ) {
                
                if ([dic objectForKey:@"eventID"]) {
                    
                    [eventIDArr addObject:[dic objectForKey:@"eventID"]];
                }
            }
            
            NSString *eventIDStr = [NSString string];
            for (int i = 0; i<eventIDArr.count; i++) {
                
                if ([eventIDStr isEqualToString:@""]) {
                    
                    eventIDStr = [NSString stringWithFormat:@"%@",eventIDArr[i]];
                }else{
                    
                    eventIDStr = [NSString stringWithFormat:@"%@,%@",eventIDStr,eventIDArr[i]];
                }
            }
            
            [request DelEventPlayWithEventID:eventIDStr andTimeout:10];
        }
        
    }else{//点播
        
        NSLog(@"--------------点播的删除按钮被全选———————————");
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        if (_ifCleanVOD) {//全选的删除
            
            [request DELVODPlayHistoryWithContentID:nil andTimeout:10];
        }else{//正常的选择删除
            
            if (_deleteArr.count == 0) {
                
                return;
            }
            
            NSMutableArray *contentIDArr = [NSMutableArray array];
            
            for (NSDictionary *dic in _deleteArr ) {
                
                if ([dic objectForKey:@"contentID"]) {
                    
                    [contentIDArr addObject:[dic objectForKey:@"contentID"]];
                }
            }
            
            NSString *contentIDStr = [NSString string];
            for (int i = 0; i<contentIDArr.count; i++) {
                
                if ([contentIDStr isEqualToString:@""]) {
                    
                    contentIDStr = [NSString stringWithFormat:@"%@",contentIDArr[i]];
                }else{
                    
                    contentIDStr = [NSString stringWithFormat:@"%@,%@",contentIDStr,contentIDArr[i]];
                }
            }
            
            [request DELVODPlayHistoryWithContentID:contentIDStr andTimeout:10];
        }
    }
}


#pragma mark  直播、点播按钮触发事件
- (void)switchBtnClick:(UIButton *)sender
{
    _isLive_not_vod = !_isLive_not_vod;
    
    NSLog(@"回看、点播左右栏按钮触发事件中_isLive_not_vod的值是：%d",_isLive_not_vod);

    if (sender.tag == 1) {//直播页
        
        _liveLab.userInteractionEnabled = NO;
        _vodLab.userInteractionEnabled = YES;
        
        [_liveLab setTitleColor:App_selected_color forState:UIControlStateNormal];
        [_vodLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        
        [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        
        _liveLab.titleLabel.font = HT_FONT_FIRST;
        _vodLab.titleLabel.font = HT_FONT_SECOND;
        
        _epgLine.hidden = NO;
        _vodLine.hidden = YES;
        
        [_scrollView setContentOffset:CGPointMake(0, 0)];
        [_noLiveDataLabel setHidden:YES];
        
        if ([_liveArr count] == 0) {
            [_noLiveDataLabel setHidden:NO];
            _cleanBtn.hidden = YES;
            
        }else{
            _cleanBtn.hidden = NO;
        }
    }
    else if (sender.tag == 2)
    {//点播页
        
        _liveLab.userInteractionEnabled = YES;
        _vodLab.userInteractionEnabled = NO;

        [_liveLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [_vodLab setTitleColor:App_selected_color forState:UIControlStateNormal];
        
        [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
        
        _liveLab.titleLabel.font = HT_FONT_SECOND;
        _vodLab.titleLabel.font = HT_FONT_FIRST;
        
        _epgLine.hidden = YES;
        _vodLine.hidden = NO;

        [_scrollView setContentOffset:CGPointMake(kDeviceWidth, 0)];
        
        [_noVODDataLabel setHidden:YES];
        
        if ([_vodArr count] == 0) {
            [_noVODDataLabel setHidden:NO];
            
            _cleanBtn.hidden = YES;
        }else{
            
            _cleanBtn.hidden = NO;
        }
        
        [_vodTable reloadData];
        
    }
}


#pragma mark 请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:EPG_EVENTPLAY]){//鉴权回看
        
        if (result.count > 0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"成功");
                    
                    NSDictionary *dataDic = [result objectForKey:@"serviceinfo"];
                    
                    NSLog(@"%@",dataDic);
                    
                    
                    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                    live.serviceID = [dataDic objectForKey:@"serviceID"];
                    live.serviceName = [dataDic objectForKey:@"name"];
                    live.eventURL = [NSURL URLWithString:[dataDic objectForKey:@"playLink"]];
                    live.eventName = _eventName;
                    live.eventURLID = _channelID;
                    live.currentDateStr = _endDate;
                    live.currentPlayType = CurrentPlayEvent;
                    live.IsFirstChannelist = YES;
                    live.TRACKID = @"2";
                    live.TRACKNAME = @"我的";
                    live.EN = @"6";
                    [self.navigationController pushViewController:live animated:YES];
                    
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromEpgPlayer"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    break;
                }
                case -1:{
                
                    NSLog(@"当前节目不存在或者不提供回看");
                    
                    [AlertLabel AlertInfoWithText:@"当前节目不存在或者不提供回看" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -2:{
                    
                    NSLog(@"用户未登录或者登录超时");
                    
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:0];
                
                    break;
                }
                case -3:{
                    
                    NSLog(@"鉴权失败");
                    
                    break;
                }
                case -5:{
                    
                    NSLog(@"频道对应此终端无播放链接");

                    [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:0];
                
                    break;
                }
                case -6:{
                    
                    NSLog(@"该频道不提供回看功能");

                    [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:self.view withLocationTag:0];
                
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接3A系统失败或者参数错误）");
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
    else if ([type isEqualToString:EPG_DELEVENTPLAY]){//删除回看播放记录
        
        NSLog(@"删除回看播放记录请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    if (_ifCleanLive) {//清空回看播放记录列表
                        
                        [_liveArr removeAllObjects];
                        

                        [_localListCenter cleanReWatchPlayRecordWithUserType:IsLogin];

                        
                        _ifCleanLive = NO;
                        
                        _selectAndDeleteView.selectBtn.selected = !_selectAndDeleteView.selectBtn.selected;
                        
                        _selectAndDeleteView.selectLab.text = @"全选";
                    }else{//单个删除
                        
                        _liveArr = [_localListCenter deleteReWatchPlayRecordWithDeleteArr:[NSMutableArray arrayWithArray:_deleteArr] andUserType:IsLogin andKey:@"eventID"];
                    }
                    
                    [_deleteArr removeAllObjects];
                    [self cellCheckDataListSelected];
                    
                    if (_liveArr.count==1) {
                        NSDictionary *dic =[_liveArr firstObject];
                        NSArray *playlist =[dic objectForKey:@"playList"];
                        if (playlist.count == 0) {
                        
                            [_liveArr removeAllObjects];
                        }
                    }
                    
                    
                    if ([_liveArr count] > 0) {
                        
                        [_noLiveDataLabel setHidden:YES];
                        
                        _cleanBtn.hidden = NO;
                        
                    }else
                    {
                        [_noLiveDataLabel setHidden:NO];
                        
                        _cleanBtn.selected = !_cleanBtn.selected;
                        
                        _cleanBtn.hidden = YES;
                        
                        WS(wss);
                        
                        [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.top.equalTo(wss.view.mas_top).offset(64+40*kDeviceRate+kTopSlimSafeSpace);
                            make.width.equalTo(@(kDeviceWidth));
                            make.bottom.equalTo(wss.view.mas_bottom).offset(-kBottomSafeSpace);
                            make.centerX.equalTo(wss.view.mas_centerX);
                        }];
                        
                        
                        [_selectAndDeleteView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.width.equalTo(wss.view.mas_width);
                            make.top.equalTo(wss.scrollView.mas_bottom);
                            make.centerX.equalTo(wss.view.mas_centerX);
                            make.height.equalTo(@(60*kDeviceRate+kBottomSafeSpace));
                        }];
                        
                        _selectAndDeleteView.hidden = YES;
                        
                        _scrollView.scrollEnabled = YES;
                        
                        _vodLab.userInteractionEnabled = YES;
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_RE_WATCH_LIST object:nil];
                    
                    break;
                }
                case 1:{
                    NSLog(@"该节目不在回看播放记录");
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    break;
                }
                default:
                    break;
            }
        }
        
        [_liveTable reloadData];
    }
    else if ([type isEqualToString:DEL_VOD_PLAY]){//删除点播播放记录
        
        NSLog(@"删除点播播放记录请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (result.count>0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                    
                case 1:{
                    NSLog(@"该视频不在点播播放记录或者已经删除");
                    break;
                }
                case 0:{//成功
                    
                    if (_ifCleanVOD) {//清空点播收藏列表
                        
                        [_vodArr removeAllObjects];
                        
                        [_localListCenter cleanVODPlayRecordWithUserType:IsLogin];
                        
                        _ifCleanVOD = NO;
                        
                        _selectAndDeleteView.selectBtn.selected = !_selectAndDeleteView.selectBtn.selected;
                        
                        _selectAndDeleteView.selectLab.text = @"全选";
                    }else{//单个删除
                        
                        _vodArr = [_localListCenter deleteVODPlayRecordWithDeleteArr:[NSMutableArray arrayWithArray:_deleteArr] andUserType:IsLogin andKey:@"contentID"];
                    }
                    
                    
                    [_deleteArr removeAllObjects];
                    [self cellCheckDataListSelected];
                    
                    if (_vodArr.count==1) {
                        NSDictionary *dic =[_vodArr firstObject];
                        NSArray *playlist =[dic objectForKey:@"playList"];
                        if (playlist.count == 0) {
                            [_vodArr removeAllObjects];
                        }
                    }
                    
                    if ([_vodArr count] > 0) {
                        
                        [_noVODDataLabel setHidden:YES];
                        
                        _cleanBtn.hidden = NO;
                        
                    }else{
                        
                        [_noVODDataLabel setHidden:NO];
                        
                        _cleanBtn.selected = !_cleanBtn.selected;
                        
                        _cleanBtn.hidden = YES;
                        
                        WS(wss);
                        
                        [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.top.equalTo(wss.view.mas_top).offset(64+40*kDeviceRate+kTopSlimSafeSpace);
                            make.width.equalTo(@(kDeviceWidth));
                            make.bottom.equalTo(wss.view.mas_bottom).offset(-kBottomSafeSpace);
                            make.centerX.equalTo(wss.view.mas_centerX);
                        }];
                        
                        
                        [_selectAndDeleteView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.width.equalTo(wss.view.mas_width);
                            make.top.equalTo(wss.scrollView.mas_bottom);
                            make.centerX.equalTo(wss.view.mas_centerX);
                            make.height.equalTo(@(60*kDeviceRate+kBottomSafeSpace));
                        }];
                        
                        _selectAndDeleteView.hidden = YES;
                        
                        _liveLab.userInteractionEnabled = YES;
                        
                        _scrollView.scrollEnabled = YES;
                        
                        _vodTable.backgroundColor = [UIColor clearColor];
                    }
                    
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
                    
                    [_vodTable reloadData];

                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误");
                    break;
                }
                    
                default:
                    break;
            }
        }
        
    }else{
        
    }
}


- (NSMutableArray *)deleteArrWithArray:(NSMutableArray *)originalArr withDeleteID:(NSString *)deleteID
{
    __block NSMutableArray *itemArr = [NSMutableArray array];
    
    for (NSDictionary *delDic in _deleteArr ) {
        
        
        [originalArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
            
            itemArr = [obj objectForKey:@"playList"];
            
            for (int i = 0; i<itemArr.count; i++) {
                
                NSMutableDictionary *dic = itemArr[i];
                
                if ([[delDic objectForKey:deleteID] isEqualToString:[dic objectForKey:deleteID]]) {
                    
                    *stop = YES;
                    
                    if (*stop == YES) {
                        
                        [[[originalArr objectAtIndex:idx] objectForKey:@"playList"] removeObjectAtIndex:i];
                        
                        if ([[[originalArr objectAtIndex:idx] objectForKey:@"playList"] count] == 0) {
                            
                            [originalArr removeObjectAtIndex:idx];
                        }
                    }
                }
            }
            
        }];
    }
    
    return originalArr;
}


#pragma mark tableview delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _liveTable) {//直播
        _mediaHight = 60*kDeviceRate;
    }else
    {//点播
        _mediaHight =  115*kDeviceRate;
    }
    
    return _mediaHight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == _liveTable){//直播
        
        if (section>=_liveArr.count) {
            
            return 0;
        }
        
        return [[[_liveArr objectAtIndex:section] objectForKey:@"playList"] count];
        
    }else{//点播
        
        if (section>=[_vodArr count]) {
            
            return 0;
        }
        
        
        NSDictionary *sectionDic = [NSDictionary dictionaryWithDictionary:[_vodArr objectAtIndex:section]];
        
        NSArray *VODItemArr = [[NSArray alloc]initWithArray:[sectionDic objectForKey:@"playList"]];
        
        
        if ([VODItemArr count]%2 == 0) {
            
            _mediaCount = [VODItemArr count]/2;
            
        }else{
        
            _mediaCount = [VODItemArr count]/2+1;
        }
        
        return _mediaCount;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _liveTable) {//************************直播播放记录

        static NSString * iden = @"iden";
        
        ChannelPlayRecorderCell *cell = [tableView dequeueReusableCellWithIdentifier:iden];
        
        if (!cell) {
            
            cell = [[ChannelPlayRecorderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
            
            cell.backgroundColor = UIColorFromRGB(0xffffff);
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        cell.backBtn.tag = indexPath.row;
        cell.backBtn.sectionBtn = indexPath.section;
        [cell.backBtn addTarget:self action:@selector(goToEPGPlayer:) forControlEvents:UIControlEventTouchUpInside];
        
        NSArray *tempArr = [NSArray arrayWithArray:[[_liveArr objectAtIndex:indexPath.section] objectForKey:@"playList"]];
        
        NSDictionary *itemDic = [tempArr objectAtIndex:indexPath.row];

        [cell.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([itemDic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
        
        cell.epgName.text = [itemDic objectForKey:@"serviceName"];
        
        cell.programName.text = [itemDic objectForKey:@"eventName"];
        
        cell.hasPlayTime.text = [NSString stringWithFormat:@"观看至：%@",[UserToolClass getBreakPointWithTime:[itemDic objectForKey:@"breakPoint"]]];
        
        if (__liveIsEditing == YES) {
            
            cell.deleteImg.hidden = NO;
        }else{

            cell.deleteImg.hidden = YES;
        }
        
        if (_ifCleanLive) {
            
            [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        }else{
            
            [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        }
        
        
        NSString *idStr = [itemDic objectForKey:@"eventID"];
        if (_deleteArr.count > 0) {
            
            for (NSDictionary *dic in _deleteArr ) {
                if ([idStr isEqualToString:[dic objectForKey:@"eventID"]]) {
                    
                    [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                }
            }
        }
        
        _liveRow = indexPath.row;
        _liveSection = indexPath.section;
        
        return cell;
                
    }else{//----------------------------------------------点播播放记录
    
        static NSString *vim = @"vim";
        
        MineVodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:vim];
        
        if (!cell) {
            
            cell = [[MineVodTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:vim];
            
            cell.backgroundColor = UIColorFromRGB(0xffffff);
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        if (_vodArr.count > indexPath.section) {
        
            if ([[[_vodArr objectAtIndex:indexPath.section] objectForKey:@"playList"] count]>=(1+indexPath.row*2))
            {
                
                NSDictionary *dicleft = [NSDictionary dictionaryWithDictionary:[[[_vodArr objectAtIndex:indexPath.section] objectForKey:@"playList"] objectAtIndex:2*indexPath.row]];
                
                if ([dicleft valueForKey:@"name"]) {
                    
                    cell.leftView.contentNameL.text = [dicleft valueForKey:@"name"];
                    
                }else{
                    
                    cell.leftView.contentNameL.text = @"";
                }
                
                
                if ([dicleft valueForKey:@"imageLink"]) {
                    
                    [cell.leftView.imageV sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dicleft valueForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
                    
                    cell.leftView.highLigthBtn.hidden = NO;
                    
                }else{
                    
                    cell.leftView.highLigthBtn.hidden = YES;
                }
                
                
                if ([dicleft valueForKey:@"breakPoint"]) {
                    
                    cell.leftView.timeBackImg.hidden = NO;
                    
                    cell.leftView.timeLabel.text = [NSString stringWithFormat:@"观看至%@",[UserToolClass getBreakPointWithTime:[dicleft valueForKey:@"breakPoint"]]];
                }else{
                    
                    cell.leftView.timeBackImg.hidden = YES;
                    
                    cell.leftView.timeLabel.text = @"";
                }
                
                
                if (__vodIsEditing == YES) {
                    
                    cell.leftView.deleteImg.hidden = NO;
                }else{
                    
                    cell.leftView.deleteImg.hidden = YES;
                }
                
                
                if (_ifCleanVOD) {
                    
                    [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                }else{
                    
                    [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
                }
                
                
                NSString *idStr = [dicleft objectForKey:@"contentID"];
                if (_deleteArr.count > 0) {
                    
                    for (NSDictionary *dic in _deleteArr ) {
                        
                        if ([idStr isEqualToString:[dic objectForKey:@"contentID"]]) {
                            
                            [cell.leftView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                        }
                    }
                }
                
                [cell.leftView.highLigthBtn addTarget:self action:@selector(goToVODTV:) forControlEvents:UIControlEventTouchUpInside];
                cell.leftView.highLigthBtn.tag = 2*indexPath.row;
                cell.leftView.highLigthBtn.sectionBtn = indexPath.section;
                
                _vodRow = 2*indexPath.row;
                _vodSection = indexPath.section;
                
                [cell.leftView refreshHeightWithMovieName];
            }else{
                [cell.leftView.bgV setBackgroundColor:[UIColor clearColor]];
                [cell.leftView.imageV setBackgroundColor:[UIColor clearColor]];
                cell.leftView.timeLabel.text = @"";
                cell.leftView.contentNameL.text = @"";
                [cell.leftView.contentNameL setBackgroundColor:[UIColor clearColor]];
                cell.leftView.imageV.image = nil;
                [cell.leftView.timeLabel setBackgroundColor:[UIColor clearColor]];
                cell.leftView.deleteBtn.hidden = YES;
                cell.leftView.timeBackImg.hidden = YES;
                cell.leftView.highLigthBtn.hidden = YES;
                cell.leftView.deleteImg.hidden = YES;
            }
            
            if ([[[_vodArr objectAtIndex:indexPath.section] objectForKey:@"playList"] count]>=(2+indexPath.row*2)) {
                
                NSDictionary *dicright = [NSDictionary dictionaryWithDictionary:[[[_vodArr objectAtIndex:indexPath.section] objectForKey:@"playList"] objectAtIndex:2*indexPath.row+1]];
                
                if ([dicright valueForKey:@"name"]) {
                    
                    cell.rightView.contentNameL.text = [dicright valueForKey:@"name"];
                    
                }else{
                    
                    cell.rightView.contentNameL.text = @"";
                }
                
                
                if ([dicright valueForKey:@"imageLink"]) {
                    
                    [cell.rightView.imageV sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dicright valueForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
                    
                    cell.rightView.highLigthBtn.hidden = NO;
                    
                }else{
                    
                    cell.rightView.highLigthBtn.hidden = YES;
                }
                
                
                if ([dicright valueForKey:@"breakPoint"]) {
                    cell.rightView.timeBackImg.hidden = NO;
                    
                    cell.rightView.timeLabel.text = [NSString stringWithFormat:@"观看至%@",[UserToolClass getBreakPointWithTime:[dicright valueForKey:@"breakPoint"]]];
                }else{
                    cell.rightView.timeBackImg.hidden = YES;
                    
                    cell.rightView.timeLabel.text = @"";
                }
                
                
                if (__vodIsEditing == YES) {
                    
                    cell.rightView.deleteImg.hidden = NO;
                }else{
                    
                    cell.rightView.deleteImg.hidden = YES;
                }
                
                
                if (_ifCleanVOD) {
                    
                    [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                }else{
                
                    [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
                }
                
                
                NSString *idStr = [dicright objectForKey:@"contentID"];
                if (_deleteArr.count > 0) {
                    
                    for (NSDictionary *dic in _deleteArr ) {
                        
                        if ([idStr isEqualToString:[dic objectForKey:@"contentID"]]) {
                            
                            [cell.rightView.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
                        }
                    }
                }
                
                [cell.rightView.highLigthBtn addTarget:self action:@selector(goToVODTV:) forControlEvents:UIControlEventTouchUpInside];
                cell.rightView.highLigthBtn.sectionBtn = indexPath.section;
                cell.rightView.highLigthBtn.tag = 2*indexPath.row+1;
                
                _vodRow = 2*indexPath.row+1;
                _vodSection = indexPath.section;
                
                [cell.rightView refreshHeightWithMovieName];
            }else{
                [cell.rightView.bgV setBackgroundColor:[UIColor clearColor]];
                [cell.rightView.imageV setBackgroundColor:[UIColor clearColor]];
                cell.rightView.timeLabel.text = @"";
                cell.rightView.contentNameL.text = @"";
                cell.rightView.imageV.image = nil;
                [cell.rightView.contentNameL setBackgroundColor:[UIColor clearColor]];
                [cell.rightView.timeLabel setBackgroundColor:[UIColor clearColor]];
                cell.rightView.deleteBtn.hidden = YES;
                cell.rightView.timeBackImg.hidden = YES;
                cell.rightView.highLigthBtn.hidden = YES;
                cell.rightView.deleteImg.hidden = YES;
            }
        }else{
        
            return 0;
        }
        
        return cell;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == _liveTable) {
        
        UIView *backView = [UIView new];
        [backView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        backView.frame = CGRectMake(0, 0, kDeviceWidth, 25*kDeviceRate);
        
        UILabel *weekLab = [UILabel new];
        weekLab.textColor = HT_COLOR_FONT_SECOND;
        weekLab.font = HT_FONT_FOURTH;
        weekLab.textAlignment = NSTextAlignmentLeft;
        
        
        if (section>=[_liveArr count]) {
            
            return 0;
        }

        if ([[_liveArr objectAtIndex:section] objectForKey:@"date"]) {
            
            weekLab.text = [[_liveArr objectAtIndex:section] objectForKey:@"date"];
        }

        CGSize weekSize = [CalculateTextWidth sizeWithText:weekLab.text font:HT_FONT_FOURTH];
        CGFloat weekHeight = weekSize.height;
        
        [backView addSubview:weekLab];
        [weekLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backView.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(backView.mas_centerY);
            make.right.equalTo(backView.mas_right);
            make.height.equalTo(@(weekHeight));
        }];
        
        return backView;

    }else{
        
        UIView *backView = [UIView new];
        [backView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
        backView.frame = CGRectMake(0, 0, kDeviceWidth, 25*kDeviceRate);
        
        UILabel *weekLab = [UILabel new];
        weekLab.textColor = HT_COLOR_FONT_SECOND;
        weekLab.font = HT_FONT_FOURTH;
        weekLab.textAlignment = NSTextAlignmentLeft;
        
        
        if (section>=[_vodArr count]) {
            
            return 0;
        }
        
        if ([[_vodArr objectAtIndex:section] objectForKey:@"date"]) {
            
            weekLab.text = [[_vodArr objectAtIndex:section] objectForKey:@"date"];
        }
        
        CGSize weekSize = [CalculateTextWidth sizeWithText:weekLab.text font:HT_FONT_FOURTH];

        CGFloat weekHeight = weekSize.height;
        
        [backView addSubview:weekLab];
        [weekLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backView.mas_left).offset(12*kDeviceRate);
            make.centerY.equalTo(backView.mas_centerY);
            make.right.equalTo(backView.mas_right);
            make.height.equalTo(@(weekHeight));
        }];

        return backView;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section>=[_vodArr count]) {
        
        return 0;
    }

    
    if (tableView == _liveTable) {
        
        return [[_liveArr objectAtIndex:section] objectForKey:@"date"];

    }else{
    
        return [[_vodArr objectAtIndex:section] objectForKey:@"date"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _liveTable) {
        
        NSArray *arr = [[_liveArr objectAtIndex:section] objectForKey:@"playList"];
        
        if (arr.count) {
            
            return 25*kDeviceRate;
            
        }else{
            
            return 0;
        }

    }else{
        
        NSArray *brr = [[_vodArr objectAtIndex:section] objectForKey:@"playList"];
        
        if (brr.count) {
            
            return 25*kDeviceRate;
            
        }else{
            
            return 0;
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _vodTable) {
        return _vodArr.count;
        
    }else{
        
        return _liveArr.count;
    }
}


#pragma mark 
- (void)goToEPGPlayer:(VODDeleteButton *)sender
{
    if (__liveIsEditing == YES) {//多选删除
        
        WS(wself);
        
        sender.selected = !sender.selected;
        
        //每次选中的字典
        NSDictionary *selDic = [[[_liveArr objectAtIndex:sender.sectionBtn] objectForKey:@"playList"] objectAtIndex:sender.tag];
        
        if (sender.selected) {
            
            if (_deleteArr.count==0) {
                
                [_deleteArr addObject:selDic];
            }else{
                
                __block BOOL isDel;
                
                isDel = NO;
                
                [_deleteArr enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL * stop) {
                    
                    if ([[obj objectForKey:@"eventID"] isEqualToString:[selDic objectForKey:@"eventID"]]) {
                        
                        *stop = YES;
                        
                        if (*stop == YES) {
                            
                            [wself.deleteArr removeObject:obj];
                            
                            sender.selected = NO;
                            
                            isDel = YES;
                        }
                    }
                    
                }];
                
                if (!isDel) {
                    [_deleteArr addObject:selDic];
                }
            }
        }else{
            
            [_deleteArr enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL * stop) {
                
                if ([[obj objectForKey:@"eventID"] isEqualToString:[selDic objectForKey:@"eventID"]]) {
                    
                    *stop = YES;
                    
                    if (*stop == YES) {
                        [wself.deleteArr removeObject:obj];
                    }
                }
                
            }];
        }
        
        
        [self cellCheckDataListSelected];
        
        _epgPlaylistLength = [_localListCenter getRewatchListLengthWithUserType:IsLogin];
        
        if (_epgPlaylistLength == _deleteArr.count) {
            
            _selectAndDeleteView.selectBtn.selected = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
            
            _ifCleanLive = YES;
            
        }else{
            
            _selectAndDeleteView.selectBtn.selected = NO;
            
            _selectAndDeleteView.selectLab.text = @"全选";
            
            _ifCleanLive = NO;
        }
        
        [_liveTable reloadData];
        
    }else{//进入直播播放器
        
        if (toEpgPlayer) {
            
            NSDictionary *diction = [[[_liveArr objectAtIndex:sender.sectionBtn] objectForKey:@"playList"] objectAtIndex:sender.tag];
            
            NSLog(@"直播的 %@", [[[_liveArr objectAtIndex:sender.sectionBtn] objectForKey:@"playList"] objectAtIndex:sender.tag]);
            
            _channelID = [diction objectForKey:@"eventID"];
            
            _eventName = [diction objectForKey:@"eventName"];
            
            NSString *dayStr = [diction objectForKey:@"endTime"];
            
            _endDate = [UserToolClass YearMouthDayWithString:dayStr];
            
            NSLog(@"原始结束日期%@",dayStr);
            
            NSLog(@"年月日%@",_endDate);
            
            NSLog(@"%@",_channelID);
            
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            
            [request EventPlayWithEventID:_channelID andTimeout:10];
            
        }
        
        toEpgPlayer = NO;
        
    }
}


#pragma mark - Scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat sectionHeaderHeight = 40*kDeviceRate;
    //固定section 随着cell滚动而滚动
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        
    }
    if (_vodArr.count>0 && _liveArr.count>0) {
        _noLiveDataLabel.hidden = YES;
        
        _noVODDataLabel.hidden = YES;
        
    }else if (_liveArr.count>0 && _vodArr.count <= 0){
        
        _noVODDataLabel.hidden = NO;
        
        _noLiveDataLabel.hidden = YES;
        
    }else if (_vodArr.count>0 && _liveArr.count <= 0){
        
        _noVODDataLabel.hidden = YES;
        
        _noLiveDataLabel.hidden = NO;
    }else{
        
        _noLiveDataLabel.hidden = NO;
        
        _noVODDataLabel.hidden = NO;
        
    }
    
}


#pragma mark 点播cell点击方法 （多选删除--进入点播播放器）
- (void)goToVODTV:(VODDeleteButton *)ges
{
    if (__vodIsEditing == YES) {//多选删除
        
        WS(wself);
        
        ges.selected = !ges.selected;
        
        NSDictionary *selDic = [_vodArr[ges.sectionBtn] objectForKey:@"playList"][ges.tag];
        
        if (ges.selected) {
            
            if (_deleteArr.count==0) {
                
                [_deleteArr addObject:selDic];
            }else{
                
                __block BOOL isDel;
                
                isDel = NO;
                
                [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    if ([[obj objectForKey:@"contentID"] isEqualToString:[selDic objectForKey:@"contentID"]]) {
                        
                        *stop = YES;
                        
                        if (*stop == YES) {
                            
                            [wself.deleteArr removeObject:obj];
                            
                            ges.selected = NO;
                            
                            isDel = YES;
                        }
                    }
                    
                }];
                
                if (!isDel) {
                    [_deleteArr addObject:selDic];
                }
            }
        }else{
            
            
            [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                if ([[obj objectForKey:@"contentID"] isEqualToString:[selDic objectForKey:@"contentID"]]) {
                    
                    *stop = YES;
                    
                    if (*stop == YES) {
                        [wself.deleteArr removeObject:obj];
                    }
                }
                
            }];
        }

        
        [self cellCheckDataListSelected];
        
        _vodPlaylistLength = [_localListCenter getVODListLengthWithUserType:IsLogin];
        
        if (_vodPlaylistLength == _deleteArr.count) {
            
            _selectAndDeleteView.selectBtn.selected = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
            
            _ifCleanVOD = YES;
            
        }else{
            
            _selectAndDeleteView.selectBtn.selected = NO;
            
            _selectAndDeleteView.selectLab.text = @"全选";
            
            _ifCleanVOD = NO;
        }
        
        [_vodTable reloadData];

    }else{//进入播放器
    
        if (toDemandPlayer) {
            
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc] init];
            
            demandVC.contentID = [[_vodArr[ges.sectionBtn] objectForKey:@"playList"][ges.tag] objectForKey:@"contentID"];
            
            demandVC.contentTitleName = [[_vodArr[ges.sectionBtn] objectForKey:@"playList"][ges.tag] objectForKey:@"name"];
            
            demandVC.episodeSeq = [[_vodArr[ges.sectionBtn] objectForKey:@"playList"][ges.tag] objectForKey:@"lastPlayEpisode"];
            demandVC.breakPointRecord = [[_vodArr[ges.sectionBtn] objectForKey:@"playList"][ges.tag] objectForKey:@"breakPoint"];
            demandVC.TRACKID = @"2";
            demandVC.TRACKNAME = @"我的";
            demandVC.EN = @"6";
            NSLog(@"%@",demandVC.episodeSeq);
            
            [self.navigationController pushViewController:demandVC animated:YES];
            
            toDemandPlayer = NO;
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDemandPlayer"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }
    }
}


#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _scrollView) {
        
        _isLive_not_vod = !_isLive_not_vod;
        
        NSLog(@"scrollView代理中_isLive_not_vod的值是：%d",_isLive_not_vod);
        
        if (scrollView.contentOffset.x == 0) {
            
            _liveLab.userInteractionEnabled = NO;
            _vodLab.userInteractionEnabled = YES;
        
            [_liveLab setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_vodLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
            
            [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
            [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
            
            _liveLab.titleLabel.font = HT_FONT_FIRST;
            _vodLab.titleLabel.font = HT_FONT_SECOND;
            
            _epgLine.hidden = NO;
            _vodLine.hidden = YES;
            
            [_noLiveDataLabel setHidden:YES];
            
            
            if (_liveArr.count > 0) {
                
                _cleanBtn.hidden = NO;
            }else{
                
                _cleanBtn.hidden = YES;
                
                [_noLiveDataLabel setHidden:NO];

            }
        }
        else if (scrollView.contentOffset.x == kDeviceWidth)
        {
            
            _liveLab.userInteractionEnabled = YES;
            _vodLab.userInteractionEnabled = NO;
            
            [_liveLab setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
            [_vodLab setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            [_liveLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
            [_vodLab setBackgroundColor:HT_COLOR_FONT_INVERSE];
            
            _liveLab.titleLabel.font = HT_FONT_SECOND;
            _vodLab.titleLabel.font = HT_FONT_FIRST;
            
            _epgLine.hidden = YES;
            _vodLine.hidden = NO;
            
            [_noVODDataLabel setHidden:YES];
            
            
            if (_vodArr.count > 0) {
                
                _cleanBtn.hidden = NO;
            }else{
                
                _cleanBtn.hidden = YES;
                
                [_noVODDataLabel setHidden:NO];
            }
        }
    }
}


- (void)goBack
{
    _fromHome = NO;
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 导航条上垃圾桶按钮点击事件
- (void)dustbinBtnSenderDown:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    NSLog(@"+++++++++++++++++++清空啦");
    
    WS(wss);
    
    if (sender.selected) {//导航条右上角删除按钮
        
        NSInteger current = _scrollView.contentOffset.x / kDeviceWidth;
        
        
        [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(64+40*kDeviceRate+kTopSlimSafeSpace);
            make.width.equalTo(@(kDeviceWidth));
            make.bottom.equalTo(wss.view.mas_bottom).offset(-60*kDeviceRate-kBottomSafeSpace);
            make.centerX.equalTo(wss.view.mas_centerX);
        }];

        
        [_selectAndDeleteView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.view.mas_width);
            make.top.equalTo(wss.scrollView.mas_bottom);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.height.equalTo(@(60*kDeviceRate+kBottomSafeSpace));
        }];
        
        
        if (current == 0) {//回看页
            __liveIsEditing = YES;
            
            _selectAndDeleteView.hidden = NO;
            
            _scrollView.scrollEnabled = NO;
            
            __vodIsEditing = NO;
            
        }else{//点播页
            
            __vodIsEditing = YES;
            
            _selectAndDeleteView.hidden = NO;
            
            _scrollView.scrollEnabled = NO;
            
            __liveIsEditing = NO;
            
        }
        
        _liveLab.userInteractionEnabled = NO;
        
        _vodLab.userInteractionEnabled = NO;

    }else{//取消按钮
        
        
        [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(64+40*kDeviceRate+kTopSlimSafeSpace);
            make.width.equalTo(@(kDeviceWidth));
            make.bottom.equalTo(wss.view.mas_bottom).offset(-kBottomSafeSpace);
            make.centerX.equalTo(wss.view.mas_centerX);
        }];
        
        
        [_selectAndDeleteView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.view.mas_width);
            make.top.equalTo(wss.scrollView.mas_bottom);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.height.equalTo(@(60*kDeviceRate+kBottomSafeSpace));
        }];
        
        
        
        _selectAndDeleteView.hidden = YES;
        _selectAndDeleteView.selectLab.text = @"全选";
        if (_selectAndDeleteView.selectBtn.selected) {
            
            _selectAndDeleteView.selectBtn.selected = NO;
        }
        __vodIsEditing = NO;
        
        __liveIsEditing = NO;
        
        _ifCleanVOD = NO;
        
        _ifCleanLive = NO;
        
        _scrollView.scrollEnabled = YES;
        
        _liveLab.userInteractionEnabled = YES;
        
        _vodLab.userInteractionEnabled = YES;

        [_deleteArr removeAllObjects];
        
        [self cellCheckDataListSelected];
    }
    
    [_vodTable reloadData];
    
    [_liveTable reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
