//
//  FeedbackViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FeedbackViewController.h"
#import "AlertLabel.h"
#import "GETBaseInfo.h"
#import "KaelTool.h"
#import "UrlPageJumpManeger.h"



@interface FeedbackViewController ()<UIWebViewDelegate>
{
    NSString *_feedBackStr;
}

@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *context;


@end




@interface FeedBackFunc : NSObject<JavaScriptObjectiveCDelegate>


#ifdef DEBUG
#define JS_PORTAL             @"http://192.168.3.27:8080"//http://192.168.3.219:8080

#else
#define JS_PORTAL             DEFAULT_SERVER_PATH 
#endif

@end


@implementation FeedBackFunc


- (NSString *)getNativeInfo
{
    NSLog(@"你好");
    
    NSString *regisPhoneStr;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"] length] == 0) {
        
        regisPhoneStr = @"guest";
    }else{
        
        regisPhoneStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    }
    NSString *appVersoin = [NSString stringWithFormat:@"%@",APP_VERSION];
    NSString *iphoneTypeStr = [[GETBaseInfo alloc] Equipment_Type];
    NSString *versionStr = [[GETBaseInfo alloc] getSystemVersion];
    
    
    NSString *info = [NSString stringWithFormat:@"||%@||%@||%@||%@",regisPhoneStr,appVersoin,iphoneTypeStr,versionStr];

    
    if (HomeNewDisplayTypeOn) {
        
        return [NSString stringWithFormat:@"%@?nativeInfo=%@",YJ_Appbase,info];
    }
    
    return [NSString stringWithFormat:@"%@%@?nativeInfo=%@",JS_PORTAL,DEFAULT_USER_PATH,info];
}


- (void)feedbackSuccess{
    NSLog(@"反馈——哈哈哈哈哈反馈成功");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"COME_FROM_FEEDBACK_VIEW" object:@"1"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Go_Back" object:nil];
    });
}

- (void)reTryLogin{
    
    NSLog(@"反馈——滚去登录");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"COME_FROM_FEEDBACK_VIEW" object:@"2"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Go_Back" object:nil];

    });
}


- (void)finish
{
    NSLog(@"反馈——finish");

    dispatch_async(dispatch_get_main_queue(), ^{

        [[NSNotificationCenter defaultCenter] postNotificationName:@"Go_Back" object:nil];

    });
}

- (NSString *)getSign
{
    NSString *tokenStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *sign = [KaelTool getRquestSignValueWith:tokenStr andVersionNum:Encrypt_version];
    
    return sign;
}

@end



@implementation FeedbackViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"FeedbackPage"];
    
    [self showCustomeHUD];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"FeedbackPage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideNaviBar:YES];
    
    [self interactWithJS];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBack) name:@"Go_Back" object:nil];
}

- (void)interactWithJS
{
    WS(wss);
    
    _webView = [[UIWebView alloc] init];
    [self.view addSubview:_webView];
    
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top);
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.bottom.equalTo(wss.view.mas_bottom);
    }];
    [_webView setBackgroundColor:CLEARCOLOR];
//    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    
    _webView.scrollView.bounces = NO;
    
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
  
   
   
    NSString *kurl;
    
    kurl = [UrlPageJumpManeger feedBackPageJump];
    
    NSLog(@"帮助反馈URL——————%@",kurl);
    
    NSURL *url = [NSURL URLWithString:[kurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//    [_webView loadRequest:request];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [_webView loadRequest:request];
    
    _context = [[JSContext alloc] init];
    _context = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    FeedBackFunc *feedFunc = [[FeedBackFunc alloc] init];
    _context[@"FeedBackFunc"] = feedFunc;
    
}

//返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hiddenCustomHUD];
//    _js = [[ActivityFunc alloc] init];
//    _context[@"ActivityFunc"] = _js;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hiddenCustomHUD];

    if (error) {
        
        [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self goBack];
        });
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Go_Back" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
