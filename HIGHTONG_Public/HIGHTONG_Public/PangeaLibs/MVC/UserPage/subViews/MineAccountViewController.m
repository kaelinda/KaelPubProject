//
//  MineAccountViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineAccountViewController.h"
#import "AccountCustomView.h"
#import "MyGesture.h"
#import "NicknameViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ChangePhoneNumViewController.h"
#import "DejalActivityView.h"
#import "AlertLabel.h"
#import "SAIInformationManager.h"
#import "CameraAndAlbum.h"
#import "HTYJUserCenter.h"


@interface MineAccountViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,HTRequestDelegate>
{
    UIButton *backBtn;//返回按钮
    UIImageView *backView;//更改头像的背景view----原始版
    __strong UIImageView *headImg;//头像
}
@property (nonatomic,assign) BOOL  isConstraintLandscape;
@property (nonatomic, strong)CameraAndAlbum *changeHeadImgView;//新版相机和相册view
@property (nonatomic, strong)AccountCustomView *viewTwo;


@end

@implementation MineAccountViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _viewTwo.midLab.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"mindUserName"];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"] length]) {
        
        _viewTwo.midLab.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"NickName"];
    }
    
    [MobCountTool pushInWithPageView:@"MineAccountPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineAccountPage"];
    
    [_changeHeadImgView resignKeyWindow];
//    [_changeHeadImgView removeFromSuperview];
//    _changeHeadImgView = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.isConstraintLandscape = NO;
    [self setNaviBarTitle:@"我的账号"];
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:backBtn];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    WS(wss);
    
    UIImageView *topImg = [UIImageView new];
    topImg.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:topImg];
    [topImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top).offset(64+kTopSlimSafeSpace);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.height.equalTo(@(100*kDeviceRate));
        make.width.equalTo(wss.view);
    }];
    topImg.userInteractionEnabled = YES;
    
    
    UIImageView *lineImg = [UIImageView new];
    lineImg.backgroundColor = HT_COLOR_SPLITLINE;
    [topImg addSubview:lineImg];
    [lineImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topImg.mas_left).offset(12*kDeviceRate);
        make.right.mas_equalTo(topImg.mas_right).offset(-12*kDeviceRate);
        make.height.equalTo(@(0.5*kDeviceRate));
        make.center.equalTo(topImg);
    }];
    

    AccountCustomView *viewOne = [[AccountCustomView alloc] initWithLeftTitle:@"我的头像" andMidLabText:nil];
    viewOne.midLab.hidden = YES;
//    viewOne.rightImg.hidden = YES;
    headImg = viewOne.midImg;
    [topImg addSubview:viewOne];
    [viewOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wss.view.mas_centerX);
        make.width.equalTo(wss.view.mas_width);
        make.bottom.equalTo(lineImg.mas_top);
        make.top.equalTo(@0);
    }];

    NSString *photoStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];
    [headImg sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[UIImage imageNamed:@"img_user_avatar_def"]];
    
    MyGesture *gesOne = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    gesOne.numberOfTapsRequired = 1;
    gesOne.tag = 1;
    [viewOne addGestureRecognizer:gesOne];
    
    
    //此处放用户注册用手机号，之后变成改的昵称。。。
    _viewTwo = [[AccountCustomView alloc] initWithLeftTitle:@"我的昵称" andMidLabText:[[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"]];
    
    _viewTwo.midLab.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"] length]) {
        
        _viewTwo.midLab.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
    }
    
    _viewTwo.midImg.hidden = YES;
    [topImg addSubview:_viewTwo];
    [_viewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOne.mas_centerX);
        make.width.equalTo(viewOne.mas_width);
        make.top.equalTo(lineImg.mas_bottom);
        make.bottom.equalTo(topImg.mas_bottom);
    }];
    
    
    MyGesture *gesTwo = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    gesTwo.numberOfTapsRequired = 1;
    gesTwo.tag = 10;
    [_viewTwo addGestureRecognizer:gesTwo];
    
    
    //-------------------------------点击更换头像后显示出的view
    [self setUpViewOfChangeHeadImg];
}


//点击更换头像后显示出的view
- (void)setUpViewOfChangeHeadImg
{
    WS(wself);
    _changeHeadImgView = [[CameraAndAlbum alloc] init];
    
    _changeHeadImgView.backBtn.Click = ^(UIButton_Block *btn, NSString *name){//阴影按钮，点击view消失
        
        [UIView animateWithDuration:0.5 animations:^{
            
            wself.changeHeadImgView.alpha = 0.0;
        } completion:^(BOOL finished) {
            
//            wself.changeHeadImgView.alpha = 0.0;
            wself.changeHeadImgView.hidden = YES;
        }];
    };
    
    _changeHeadImgView.cameraBtn.Click = ^(UIButton_Block *btn, NSString *name){//相机按钮
    
        wself.changeHeadImgView.hidden = YES;
        wself.changeHeadImgView.alpha = 0.0;

        
        if (isBeforeIOS7) {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = wself;
            imagePicker.allowsEditing = YES;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [wself presentViewController:imagePicker animated:YES completion:nil];
        }
        if (isAfterIOS8) {
            AVAuthorizationStatus auth = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            switch (auth) {
                    
                    //        case AVAuthorizationStatusNotDetermined:{ // 状态检测第一次引导
                    //            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    //                if (granted) {
                    //                    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
                    //                    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
                    //                }
                    //            }];
                    //            break;
                    //        }
                    
                    //            case AVAuthorizationStatusNotDetermined:{ // 无
                    ////                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ok" message:@"无" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    ////                [alert show];
                    //                break;
                    //            }
                case AVAuthorizationStatusRestricted:{ //
                    //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ok" message:@"ok" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    //                [alert show];
                    break;
                }
                case AVAuthorizationStatusDenied:{ // 拒绝
                    //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ok" message:@"拒绝" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    //                [alert show];
                    break;
                }
                default:{ // 允许
                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = wself;
                    imagePicker.allowsEditing = YES;
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [wself presentViewController:imagePicker animated:YES completion:nil];
                    break;
                }
            }
        }
    };
    
    
    _changeHeadImgView.albumBtn.Click = ^(UIButton_Block *btn, NSString *name){//相册按钮
    
        wself.changeHeadImgView.hidden = YES;
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = wself;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [wself presentViewController:imagePicker animated:YES completion:nil];

    };
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:@"uploadHeadPhoto"]) {
        
        NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);

        if ([result count]>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    [self hiddenCustomHUD];
                    
                    [AlertLabel AlertInfoWithText:@"更换成功！" andWith:self.view withLocationTag:0];
                    
                    //登陆成功，请求用户信息
                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                    [request NewqueryUserInfoWithTimeout:KTimeout];

                    break;
                }
                case -2:{
                    [self hiddenCustomHUD];
                    break;
                }
                    
                case -9:{
                    [self hiddenCustomHUD];
                    break;
                }
                default:
                    [self hiddenCustomHUD];
                    break;
            }
        }
        
    }
    else if ([type isEqualToString:queryUserInfo]) {//登陆成功后请求用户信息
        
        NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
        
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"请求用户信息成功");
                    
                    [HTYJUserCenter saveUserDetailProfileWithDic:result];
    
                    NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];
                    [headImg sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"img_user_avatar_def"]];

                    break;
                }
                case -2:{
                    
                    break;
                }
                case -9:{
                    break;
                }
            }
        }
    }
    else{
    
    }
}


// 改变图片大小(90*90 32bit 算下来约10k) 90*90*32 /8 /1024
- (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self saveImage:image withName:@"savePhoto.png"];
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
//        UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];//原始图不可编辑
       
        //编辑图片，主要是缩放
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
        image = [self scaleFromImage:image toSize:CGSizeMake(90, 90)];
        
        NSData *data;
        if (UIImagePNGRepresentation(image) == nil)
        {
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(image);
        }
//
//        //图片保存的路径
//        //这里将图片放在沙盒的documents文件夹中
//        NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//        
//        //文件管理器
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        
//        //把刚刚图片转换的data对象拷贝至沙盒中 并保存为image.png
//        [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
//        [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/savePhoto.png"] contents:data attributes:nil];
//        
//        //得到选择后沙盒中图片的完整路径
//        NSString *filePath = [[NSString alloc]initWithFormat:@"%@%@",DocumentsPath, @"/savePhoto.png"];
//        
        //关闭相册界面
        self.isConstraintLandscape = YES;
        
        if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }else{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //关闭遮罩
//        backView.hidden = YES;
        
//        //赋值
//        [headImg setImage:[UIImage imageWithContentsOfFile:filePath]];
        
//        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"HeaderPhoto"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        [headImg setImage:image];
        
//        //账号界面赋值
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANG_ACCOUNT_IMAGE" object:nil];
        
        //上传服务器
        
        dispatch_queue_t q = dispatch_queue_create("upload", NULL);
        dispatch_async(q, ^{
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            [request NewUploadHeadPhotoWithImageData:image andWidth:45 andHeight:45 andTimeout:10];//[UIImage imageWithContentsOfFile:filePath]
        });
        
        [self showCustomeHUD];
    } 
}


- (BOOL)shouldAutorotate {
    return self.isConstraintLandscape ?YES:NO;
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0

- (NSUInteger)supportedInterfaceOrientations

#else

- (UIInterfaceOrientationMask)supportedInterfaceOrientations

#endif

{
    return  UIInterfaceOrientationMaskAllButUpsideDown;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    //viewController初始显示的方向
    return self.isConstraintLandscape ? UIInterfaceOrientationPortrait:UIInterfaceOrientationPortrait;
}


//取消相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.isConstraintLandscape = YES;
    
    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


//讲图片转成Data存入本地
- (void)saveImage:(UIImage *)tempImg withName:(NSString *)imageName
{
    //NSData *imgData = UIImagePNGRepresentation(tempImg);
    NSData *imgData = UIImageJPEGRepresentation(tempImg, 1.0);
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [path objectAtIndex:0];
    NSString *fileName = [dataPath stringByAppendingPathComponent:imageName];
    
    [imgData writeToFile:fileName atomically:YES];
}


//返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


//四个手势事件
- (void)goToTheNewVC:(MyGesture *)gesture
{
    switch (gesture.tag) {
        case 10:{//更改昵称
            [self.navigationController pushViewController:[[NicknameViewController alloc] init] animated:YES];
            
            break;
        }
        case 1:{//修改头像
            _changeHeadImgView.hidden = NO;
//            [_changeHeadImgView show];
            
            WS(wself);
            
            [UIView animateWithDuration:0.5 animations:^{
                
                wself.changeHeadImgView.alpha = 1.0;
                [wself.changeHeadImgView show];

            } completion:^(BOOL finished) {
                
//                wself.changeHeadImgView.alpha = 1.0;
            }];

            
            break;
        }

        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
