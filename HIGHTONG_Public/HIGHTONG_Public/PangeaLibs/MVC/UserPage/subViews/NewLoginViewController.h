//
//  NewLoginViewController.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/10.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "CustomAlertView.h"
#import "CHECKFORMAT.h"
#import "DejalActivityView.h"
#import "HTYJUserCenter.h"
#import "CalculateTextWidth.h"
#import "MyGesture.h"
//#import "ServiceAgreementViewController.h"
#import "UserTime.h"
#import "KnowledgeBaseViewController.h"

@interface NewLoginViewController : HT_FatherViewController
{
    BOOL _phonenumberOK;//手机号规格合格
    BOOL _authCodeOK;//验证码规格合格
    BOOL _ifReadRule;//是否同意并阅读条例
}

@property (nonatomic,strong) UIButton          *backBtn;//返回按钮
@property (nonatomic,strong) UIButton          *registerBtn;//注册按钮
@property (nonatomic,strong) GeneralInputBlock *phoneField;//账号
@property (nonatomic,strong) GeneralInputBlock *verityField;//验证码


@property (nonatomic,strong) UIView            *errerBack;
@property (nonatomic,strong) UIView            *clearBackView;//整体透明背景图
@property (nonatomic,strong) UIButton          *LoginBtn;//登陆按钮
@property (nonatomic,strong) UILabel           *lineLabel;//线
@property (nonatomic,strong) CustomAlertView   *customView;
@property (nonatomic,strong)UIButton           *ifAgreeBtn;//是否同意按钮

@property (nonatomic,assign)BOOL isPlayerEnter;

@end
