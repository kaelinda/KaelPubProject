//
//  ServiceAgreementViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/13.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ServiceAgreementViewController.h"

@interface ServiceAgreementViewController ()<UIWebViewDelegate>
{
    NSString *_urlStr;
}

@property (nonatomic,strong)UIButton *backBtn;

@end

@implementation ServiceAgreementViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"ServiceAgreementPage"];
    
    [self showCustomeHUD];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"ServiceAgreementPage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"用户协议"];
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    [self setNaviBarRightBtn:nil];

    WS(wss);
    
    UIWebView *webView = [UIWebView new];
    [self.view addSubview:webView];
    webView.delegate = self;
    
    if (isBeforeIOS7) {
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(64);
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];

    }else{
    
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wss.view.mas_top).offset(44);
            make.width.equalTo(wss.view.mas_width);
            make.centerX.equalTo(wss.view.mas_centerX);
            make.bottom.equalTo(wss.view.mas_bottom);
        }];

    }
        
    [(UIScrollView *)[[webView subviews] objectAtIndex:0] setBounces:NO];
    
    
//    NSString *htmlPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"disclaimer_w2.html"];
//    NSString *htmlString = [[NSString alloc] initWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
//    [webView loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    
    if ([APP_ID isEqualToString:@"1099034502"]) {
        _urlStr = [NSString stringWithFormat:@"%@?tsytle=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"agreementurl"],App_Theme_Color_num];
    }
    else
    {
        _urlStr = [NSString stringWithFormat:@"%@/shphone/static/user_agreement%@?tsytle=%@&ch=3",YJ_Shphone,HTML_ID,App_Theme_Color_num];
    }
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlStr]];
    [webView loadRequest:request];

}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hiddenCustomHUD];
    //    _js = [[ActivityFunc alloc] init];
    //    _context[@"ActivityFunc"] = _js;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hiddenCustomHUD];
    
    if (error) {
        
        [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self goBack];
        });
    }
}


- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
