//
//  NewChangePhoneViewController.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "KaelTool.h"
#import "GeneralInputBlock.h"
#import "UserTime.h"
#import "CustomAlertView.h"
#import "CHECKFORMAT.h"
#import "NewCPhoneSecondViewController.h"

@interface NewChangePhoneViewController : HT_FatherViewController
{
    NSString *_oldPhoneStr;
    BOOL _authCodeOK;
}

@property (nonatomic, strong) GeneralInputBlock *verityView;
@property (nonatomic, strong) CustomAlertView   *customView;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIButton *nextStepBtn;

@end
