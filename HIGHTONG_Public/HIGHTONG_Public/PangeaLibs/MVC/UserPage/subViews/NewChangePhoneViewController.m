//
//  NewChangePhoneViewController.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewChangePhoneViewController.h"

@interface NewChangePhoneViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>

@end

@implementation NewChangePhoneViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"newChangePhoneFirst"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"newChangePhoneFirst"];
    
    [_customView resignKeyWindow];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"更换手机"];
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    [self setNaviBarRightBtn:nil];

    [self.view setBackgroundColor:App_white_color];
    
    _oldPhoneStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request NewauthCodeOfPhoneWithType:@"5" andMobilePhone:_oldPhoneStr andTimeout:KTimeout];
    
     _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
    
    [self setUpMainView];
}

- (void)setUpMainView
{
    WS(wss);
    UILabel *displayLab = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:UIColorFromRGB(0x666666) andFont:App_font(13)];
    displayLab.text = @"已发送短信验证码到";
    [self.view addSubview:displayLab];
    [displayLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wss.view);
        make.top.mas_equalTo(wss.view.mas_top).offset(64+15*kRateSize);
    }];
    
    
    NSString *newPhoneStr = [_oldPhoneStr stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    
    UILabel *phoneLab = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:UIColorFromRGB(0x333333) andFont:App_font(16)];
    phoneLab.text = newPhoneStr;
    [self.view addSubview:phoneLab];
    [phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wss.view);
        make.top.mas_equalTo(displayLab.mas_bottom).offset(10*kRateSize);
    }];
    
    
    _verityView = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入短信中的验证码" withVerityBtnName:@"获取验证码"];
    [_verityView.verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
    _verityView.inputBlock.delegate = self;
    _verityView.userInteractionEnabled = YES;
    [self.view addSubview:_verityView];
    [_verityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wss.view);
        make.height.mas_equalTo(50*kRateSize);
        make.top.mas_equalTo(phoneLab.mas_bottom).offset(15*kRateSize);
    }];
    
    
    UIImageView *sepLine = [UIImageView new];
    [sepLine setBackgroundColor:App_line_color];
    [self.view addSubview:sepLine];
    [sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wss.verityView);
        make.top.mas_equalTo(wss.verityView.mas_bottom);
        make.height.mas_equalTo(0.5*kRateSize);
    }];
    
    
    _nextStepBtn = [KaelTool setButtonPropertyWithBackgroundNormalImageName:@"btn_user_next_step_normal" andBackgroundHighlightedImageName:@"btn_user_next_step_pressed" andBackgroundSelectedImageName:nil andTarget:self andSelect:@selector(nextBtnClickDown:)];
    [self.view addSubview:_nextStepBtn];
    [_nextStepBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.verityView.mas_bottom).offset(30*kRateSize);
        make.width.equalTo(@(305*kRateSize));
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
}

#pragma mark 下一步按钮触发方法
- (void)nextBtnClickDown:(UIButton *)btn
{
    [_verityView.inputBlock resignFirstResponder];
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityView.inputBlock.text]) {
        _authCodeOK=YES;
    }else{
        
        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    if (_authCodeOK) {//去到下一个页面
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];

        [request checkAuthCodeWithMobilePhone:KMobileNum andAuthCode:_verityView.inputBlock.text andTimeout:KTimeout];
       
    }else{
        //检查账号和密码格式是否正确
        return;
    }
}

#pragma mark   请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        if (result.count > 0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    [UserTime changBtn:_verityView.verityBtn withSleepSecond:60];
                    [self.verityView.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
                    _customView.messageStr = @"该手机号已注册";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
    }
    else if ([type isEqualToString:User_Check_Authcode]){
    
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    NewCPhoneSecondViewController *second = [[NewCPhoneSecondViewController alloc] init];
                    
                    second.oldPhoneAuthCodeStr = _verityView.inputBlock.text;
                    
                    [self.navigationController pushViewController:second animated:YES];
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"验证码错误");
                    
                    _customView.messageStr = @"请输入正确的验证码";
                    
                    [_customView show];
                    
                    break;
                }
                case -7:{
                    NSLog(@"验证码超时效");
                    
                    _customView.messageStr = @"验证码超时效";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"无法访问个人中心";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
    }
}

#pragma mark   获取验证码触发方法
- (void)getCoding:(UIButton*)sender
{
    [_verityView.inputBlock resignFirstResponder];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request NewauthCodeOfPhoneWithType:@"5" andMobilePhone:_oldPhoneStr andTimeout:KTimeout];
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        
        _customView.hidden = YES;
    }
    
    return;
}

#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_verityView.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_verityView.inputBlock resignFirstResponder];
}

//导航条返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
