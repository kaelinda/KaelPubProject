//
//  MineScheduleViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineScheduleViewController.h"
#import "SchedualTableCell.h"
#import "MyGesture.h"
#import "CustomAlertView.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DataPlist.h"
#import "Tool.h"
#import "UserToolClass.h"
#import "SelectAndDeleteView.h"
#import "LocalCollectAndPlayRecorder.h"

@interface MineScheduleViewController ()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate>
{
    NSString *_programID;//节目编号
    NSString *_channelID;//频道编号
    NSString *_endDateStr;
    
    BOOL toEpgPlayer;//防止多次点击回看cell，多次push
    
    SelectAndDeleteView *_selectAndDeleteView;
}
@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation MineScheduleViewController

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }

    return _dataArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    [self sendHTTPRequestMethod];
    
    [MobCountTool pushInWithPageView:@"MineSchedulePage"];
    
    toEpgPlayer = YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineSchedulePage"];
}


- (void)sendHTTPRequestMethod
{
    _dataArr = [NSMutableArray arrayWithArray:[_localListCenter getScheduledList]];

    
    if (_dataArr.count) {
        _editBtn.hidden = NO;
    }
    
    [self hiddenCustomHUD];
    
    [self hideLoading];
    
    [_table reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    self.deleteArr = [NSMutableArray array];
    
    _selAllBtnIsClick = NO;
    
    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    [self setNaviBarTitle:@"我的预约"];
    
    _editBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_del_normal" imgHighlight:@"btn_common_title_del_normal" imgSelected:@"btn_user_cancel" target:self action:@selector(editSenderDown:)];
    [self setNaviBarRightBtn:_editBtn];
    
    _editBtn.hidden = YES;
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    

    _prompt = [[GeneralPromptView alloc] initWithFrame: CGRectMake(0, 200*kDeviceRate+64+kTopSlimSafeSpace, kDeviceWidth, 88*kDeviceRate) andImgName:@"ic_user_order_metadata" andTitle:@"暂无预约记录"];
    [self.view addSubview:_prompt];
    _prompt.hidden = YES;
    
    
    _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, self.view.frame.size.width, self.view.frame.size.height-64-kTopSlimSafeSpace-kBottomSafeSpace)];
    _table.separatorStyle = UITableViewCellEditingStyleNone;
    _table.delegate = self;
    _table.dataSource = self;
    _table.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_table];
    _table.showsHorizontalScrollIndicator = NO;
    _table.showsVerticalScrollIndicator = NO;
    
    
    _selectAndDeleteView = [[SelectAndDeleteView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-60*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 60*kDeviceRate+kBottomSafeSpace)];
    [_selectAndDeleteView setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:_selectAndDeleteView];
    _selectAndDeleteView.hidden = YES;
    _selectAndDeleteView.userInteractionEnabled = YES;
    
    [_selectAndDeleteView.selectBtn addTarget:self action:@selector(selectAllItemsButSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    [_selectAndDeleteView.deleteBtn addTarget:self action:@selector(deleteBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
    [self showCustomeHUD];
}


- (void)showLoading
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self hideLoading];
    });
}


- (void)hideLoading
{
    WS(wself);
    
    if (wself.dataArr.count>0) {
        
        wself.prompt.hidden = YES;
    }else{
        wself.prompt.hidden = NO;
    }
}


#pragma mark 全选按钮触发事件
- (void)selectAllItemsButSenderDown:(VODDeleteButton *)btn
{
    btn.selected = !btn.selected;

    if (btn.selected) {
        
        [_deleteArr removeAllObjects];//先清再加
        
        [_deleteArr addObjectsFromArray:_dataArr];
        
        NSLog(@"%@",_dataArr);
        _selAllBtnIsClick = YES;
        
        [_table reloadData];
        
        [self cellCheckDataListSelected];

        _selectAndDeleteView.selectLab.text = @"取消全选";

    }else{
    
        NSLog(@"_dataArr:%@",_dataArr);
        NSLog(@"_deleteArr::%@",_deleteArr);
        
        [self.deleteArr removeAllObjects];
        
        NSLog(@"_dataArr:::%@",_dataArr);
        _selAllBtnIsClick = NO;
    
        [_table reloadData];
        
        [self cellCheckDataListSelected];
        
        _selectAndDeleteView.selectLab.text = @"全选";
    }
}

#pragma mark 删除按钮触发事件
- (void)deleteBtnSenderDown:(VODDeleteButton *)btn
{
    NSLog(@"我是删除按钮");
    
    btn.selected = !btn.selected;
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    if (_selAllBtnIsClick) {//全选的删除
        
        [request DelEventReserveWithEventID:@"" andTimeout:10];
        
    }else{//正常的选择删除
    
        if (_deleteArr.count == 0) {
            
            return;
        }
        
        NSMutableArray *eventIDArr = [NSMutableArray array];
        
        for (NSDictionary *dic in _deleteArr ) {
            
            if ([dic objectForKey:@"eventID"]) {
                
                [eventIDArr addObject:[dic objectForKey:@"eventID"]];
            }
        }
        
        NSString *eventIDStr = [NSString string];
        for (int i = 0; i<eventIDArr.count; i++) {
            
            if ([eventIDStr isEqualToString:@""]) {
                
                eventIDStr = [NSString stringWithFormat:@"%@",eventIDArr[i]];
            }else{
            
                eventIDStr = [NSString stringWithFormat:@"%@,%@",eventIDStr,eventIDArr[i]];
            }
        }
        
        [request DelEventReserveWithEventID:eventIDStr andTimeout:10];
    }
}


#pragma mark table delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60*kDeviceRate;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *iden = @"iden";
    
    SchedualTableCell *cell = [tableView dequeueReusableCellWithIdentifier:iden];
    
    if (!cell) {
        cell = [[SchedualTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
        
        cell.backgroundColor = UIColorFromRGB(0xffffff);
        
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    
    if (_dataArr.count > indexPath.row) {
        
        [cell refreshCellWithDictionary:[_dataArr objectAtIndex:indexPath.row] andIfEdit:isEditing];
    }
    
    
    if (_selAllBtnIsClick) {
        
        [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        
    }else{
        
        [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        
    }

    
    NSInteger cellNum = indexPath.row;
    
    if (_deleteArr.count > 0) {
        
        NSString *selectID = [[_dataArr objectAtIndex:cellNum] objectForKey:@"eventID"];
        
        for (NSDictionary *selectTempDic in self.deleteArr) {
            
            if ([selectID isEqualToString:[selectTempDic objectForKey:@"eventID"]]) {
                
                [cell.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
            }
        }
    }

    [cell.backBtn addTarget:self action:@selector(buttonOnCellDidSelect:) forControlEvents:UIControlEventTouchUpInside];
    cell.backBtn.tag = indexPath.row;
    
    
    return cell;
}


#pragma mark   cell的点击事件 (选中删除--进入播放器)
- (void)buttonOnCellDidSelect:(VODDeleteButton *)btn
{
    if (isEditing == YES) {//选中删除
        
        WS(wself);
        btn.selected = !btn.selected;
        
        NSDictionary *selDic = [_dataArr objectAtIndex:btn.tag];
        
        if (btn.selected) {
            
            if (_deleteArr.count==0) {
                
                [_deleteArr addObject:selDic];
            }else{
                
                __block BOOL isDel;
                
                isDel = NO;
                
                [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    if ([[obj objectForKey:@"eventID"] isEqualToString:[selDic objectForKey:@"eventID"]]) {
                        
                        *stop = YES;
                        
                        if (*stop == YES) {
                            
                            [wself.deleteArr removeObject:obj];
                            
                            btn.selected = NO;
                            
                            isDel = YES;
                        }
                    }
                    
                }];
                
                if (!isDel) {
                    [_deleteArr addObject:selDic];
                }
            }
            
        }else{
            
            [_deleteArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                if ([[obj objectForKey:@"eventID"] isEqualToString:[selDic objectForKey:@"eventID"]]) {
                    *stop = YES;
                    
                    if (*stop) {
                        
                        [wself.deleteArr removeObject:obj];
                    }
                }
                
            }];
        }
        
        
        [self cellCheckDataListSelected];
        
        if (_dataArr.count == _deleteArr.count) {

            _selectAndDeleteView.selectBtn.selected = YES;
            
            _selectAndDeleteView.selectLab.text = @"取消全选";
            
            _selAllBtnIsClick = YES;

        }else{
            
            _selectAndDeleteView.selectBtn.selected = NO;

            _selectAndDeleteView.selectLab.text = @"全选";

            _selAllBtnIsClick = NO;
        }
        
        [_table reloadData];
        
    }else{//进入播放器
        if (toEpgPlayer) {
            
            
            NSDictionary *selectedDic = _dataArr[btn.tag];
            
            if ([[selectedDic objectForKey:@"ifCanPlay"] isEqual:@"1"]) {
                
                _endDateStr = [Tool MMddssToyyMMddStringFromDate:[selectedDic objectForKey:@"endTime"]];
                
                _programID = [selectedDic objectForKey:@"eventID"];//节目ID
                _channelID = [selectedDic objectForKey:@"serviceID"];//频道ID
                
                
                if ([[selectedDic objectForKey:@"playType"] isEqualToString:@"1"]) {//回看
                    
                    //鉴权回看
                    
                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                    
                    [request EventPlayWithEventID:_programID andTimeout:10];
                    
                    
                }else if ([[selectedDic objectForKey:@"playType"] isEqualToString:@"0"]){//直播
                    
                    //鉴权直播
                    
                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                    
                    [request TVPlayWithServiceID:_channelID andTimeout:10];
                    
                }
            }
            
            toEpgPlayer = NO;
            
        }
        else{
            
            return;
        }
    }

}


// 判断选中数据(_contentTable)
- (void)cellCheckDataListSelected
{
    if (self.deleteArr.count >0) {

        _selectAndDeleteView.deleteBtn.enabled = YES;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"sl_user_playrecord_del_item_normal"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = [NSString stringWithFormat:@"删除( %ld )",_deleteArr.count];
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_REMIND;//UIColorFromRGB(0xff9c00);
        
    } else {
        
        _selectAndDeleteView.deleteBtn.enabled = NO;
        [_selectAndDeleteView.deleteBtn setImage:[UIImage imageNamed:@"btn_user_disable_click"] forState:UIControlStateNormal];
        _selectAndDeleteView.deleteLab.text = @"删除";
        _selectAndDeleteView.deleteLab.textColor = HT_COLOR_FONT_WARNING;//UIColorFromRGB(0xcccccc);
    }
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    [self hiddenCustomHUD];
    if ([type isEqualToString:EPG_EVENTRESERVELIST]) {//预约节目列表
  
        
    }
    else if ([type isEqualToString:EPG_DELEVENTRESERVE]){//取消预约
    
        NSLog(@"清空预约节目列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    if (_selAllBtnIsClick) {//清空
                        
                        [_localListCenter cleanScheduleList];
                        
                        [_dataArr removeAllObjects];
                        
                        _selAllBtnIsClick = NO;
                    }else{//单删
                        
                        [_dataArr removeAllObjects];
                        
                        [_localListCenter multiDeleteScheduledListWith:[NSArray arrayWithArray:_deleteArr] andKey:@"eventID"];
                        _dataArr = [NSMutableArray arrayWithArray:[_localListCenter getScheduledList]];
                    }
                    
                    [_deleteArr removeAllObjects];
                    [self cellCheckDataListSelected];
                    
                    if ([_dataArr count] > 0) {
                        
                        [_prompt setHidden:YES];
                        
                        [_table reloadData];
                        
                    }else{
                        [_prompt setHidden:NO];
                        
                        _editBtn.hidden = YES;
                        
                        _selectAndDeleteView.hidden = YES;
                    }
                    
                    break;
                }
                case 1:{
                    NSLog(@"未预约该节目或者已经取消预约");
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    break;
                }
                    
                default:
                    
                    break;
            }
            
            [_table reloadData];
        }
    }
    else if ([type isEqualToString:EPG_EVENTPLAY]){//鉴权回看
        
        if (result.count > 0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"成功");
                    
                    NSDictionary *dataDic = [result objectForKey:@"serviceinfo"];
                    
                    NSLog(@"%@",dataDic);
                    
                    
                    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
        
                    live.serviceID = [dataDic objectForKey:@"serviceID"];//频道编号
                    live.serviceName = [dataDic objectForKey:@"name"];//频道名称
                    live.eventURL = [NSURL URLWithString:[dataDic objectForKey:@"playLink"]];//回看链接
                    
                    live.eventURLID = _programID;//节目编号
                    live.currentDateStr = _endDateStr;//年月日
                    
                    live.currentPlayType = CurrentPlayEvent;
                    
                    live.TRACKID = @"2";
                    live.TRACKNAME = @"我的";
                    live.EN = @"7";

                    [self.navigationController pushViewController:live animated:YES];
                    
                    break;
                }
                case -1:{
                    
                    NSLog(@"当前节目不存在或者不提供回看");
                    
                    [AlertLabel AlertInfoWithText:@"当前节目不存在或不提供回看" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -2:{
                    
                    NSLog(@"用户未登录或者登录超时");
                    
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -3:{
                    
                    NSLog(@"鉴权失败");
                    
                    break;
                }
                case -5:{
                    
                    NSLog(@"频道对应此终端无播放链接");
                    
                    [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -6:{
                    
                    NSLog(@"该频道不提供回看功能");
                    
                    [AlertLabel AlertInfoWithText:@"该频道不提供回看功能" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接3A系统失败或者参数错误）");
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
    else if ([type isEqualToString:EPG_TVPLAY]){//鉴权直播
        
        if (result.count > 0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"成功");
                    
                    NSDictionary *dataDic = [result objectForKey:@"serviceinfo"];
                    
                    NSLog(@"%@",dataDic);
                    
                    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
                    
                    live.serviceID = [dataDic objectForKey:@"id"];//频道编号
                    live.serviceName = [dataDic objectForKey:@"name"];//频道名称
                    live.eventURL = [NSURL URLWithString:[dataDic objectForKey:@"playLink"]];//播放链接
                    
                    live.eventURLID = _programID;//节目编号
                    live.currentDateStr = _endDateStr;
                    
                    live.currentPlayType = CurrentPlayLive;

                    live.TRACKID = @"2";
                    live.TRACKNAME = @"我的";
                    live.EN = @"7";
                    [self.navigationController pushViewController:live animated:YES];
                    
                    break;
                }
                case -1:{
                    
                    NSLog(@"当前频道不存在");
                    
                    [AlertLabel AlertInfoWithText:@"当前频道不存在" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -2:{
                    
                    NSLog(@"用户未登录或者登录超时");
                    
                    [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -3:{
                    
                    NSLog(@"鉴权失败");
                    
                    break;
                }
                case -5:{
                    
                    NSLog(@"频道对应此终端无播放链接");
                    
                    [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接3A系统失败或者参数错误）");
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
}


#pragma mark 右上角垃圾桶按钮点击事件
- (void)editSenderDown:(UIButton *)sender
{
    WS(wself);
    
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        
        _selectAndDeleteView.hidden = NO;
        
        isEditing = YES;
        
        [_table mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wself.view.mas_width);
            make.centerX.equalTo(wself.view.mas_centerX);
            make.top.equalTo(wself.view.mas_top).offset(64+kTopSlimSafeSpace);
            make.bottom.equalTo(wself.view.mas_bottom).offset(-60*kDeviceRate-kBottomSafeSpace);
        }];
        
    }else{//取消
        
        _selectAndDeleteView.hidden = YES;
        _selectAndDeleteView.selectLab.text = @"全选";
        if (_selectAndDeleteView.selectBtn.selected) {
            
            _selectAndDeleteView.selectBtn.selected = NO;
        }
        
        isEditing = NO;

        _selAllBtnIsClick = NO;
        
        [_deleteArr removeAllObjects];

        [self cellCheckDataListSelected];
        
        [_table mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wself.view.mas_width);
            make.centerX.equalTo(wself.view.mas_centerX);
            make.top.equalTo(wself.view.mas_top).offset(64+kTopSlimSafeSpace);
            make.bottom.equalTo(wself.view.mas_bottom).offset(-kBottomSafeSpace);
        }];

    }
    
    [_table reloadData];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_dataArr.count) {
        
        _prompt.hidden = YES;
        
    }
}


- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
