//
//  MineRecorderViewController.h
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralPromptView.h"

@interface MineRecorderViewController : HT_FatherViewController
{
    NSInteger _mediaCount;
    CGFloat  _mediaHight;
    
    NSString *liveFavContentID;
    
    NSString *vodFavContentID;
    
    NSInteger _liveRow; //直播删除记录编号
    
    NSInteger _vodRow;//点播删除记录编号
    
    NSInteger _vodSection;//点播删除记录段落标记
    
    NSInteger _liveSection;
    
    UIView *_noView;//  ***** 暂无图片的 View *****
    
    BOOL _ifCleanVOD;//标记点播页面时，全选按钮是否点击
    
    BOOL _ifCleanLive;//标记回看页面时，全选按钮是否点击
    
    BOOL _isLive_not_vod;//当前的页面窗口
    
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *liveTable;//回看视图
@property (nonatomic, strong) UITableView *vodTable;//点播视图

@property (assign, nonatomic) BOOL fromHome;//是不是从首页跳进来的
@property (assign, nonatomic) BOOL _vodIsEditing;//点播垃圾桶按钮是否是编辑状态
@property (assign, nonatomic) BOOL _liveIsEditing;//直播页垃圾桶按钮是否编辑状态

@property (strong, nonatomic) NSMutableArray *liveArr;//重新分组的回看数据源
@property (strong, nonatomic) NSMutableArray *vodArr;//重新分组的点播数据源
@property (strong, nonatomic) GeneralPromptView *noVODDataLabel;
@property (strong, nonatomic) GeneralPromptView *noLiveDataLabel;


@property (nonatomic, strong)UIButton *backBtn;//返回按钮
@property (nonatomic, strong)UIButton *cleanBtn;//垃圾桶按钮
@property (nonatomic, strong)UIView *loginView;//登录后可同步背景view
@property (nonatomic, strong)UIImageView *deleteView;//删除-清空背景view
@property (nonatomic, strong)UIView *swichView;//点播、直播背景view
@property (nonatomic, strong)UILabel *removeAllLab;//清空按钮
@property (nonatomic, strong)UIButton *liveLab;//直播
@property (nonatomic, strong)UIButton *vodLab;//点播
@property (nonatomic, strong)UIImageView *honLine;//回看、点播下面的长长分割线


@property (nonatomic, strong)UIImageView *epgLine;//回看按钮下面的小线；
@property (nonatomic, strong)UIImageView *vodLine;//点播单个按钮下面的小线


@end
