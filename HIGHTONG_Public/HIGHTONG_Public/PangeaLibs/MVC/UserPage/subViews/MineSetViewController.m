//
//  MineSetViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineSetViewController.h"
#import "MyGesture.h"
#import "CustomAlertView.h"
#import "AlertLabel.h"
#import "DejalActivityView.h"
#import "CalculateTextWidth.h"
#import "UserToolClass.h"
#import "HTYJUserCenter.h"

@interface MineSetViewController ()<CustomAlertViewDelegate,HTRequestDelegate>
{
    UIButton *backBtn;//返回按钮
    
    UILabel *alwaysAlertLab;//每次提醒
    UILabel *oneAlertLab;//提醒一次
    UIImageView *alwaysAlertImg;
    UIImageView *onceAlertImg;
    
    UILabel *versionLabel;//当前版本号：V.02.0
    
    NSString *cachStr;
}
@end

@implementation MineSetViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"MineSetPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineSetPage"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:backBtn];
    
    [self setNaviBarTitle:@"系统设置"];
    
    [self setNaviBarRightBtn:nil];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    [self makeTheMainPartUI];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"]);
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {//每次提醒
        //        alwaysAlertLab.textColor = UIColorFromRGB(0x00a0e9);
        //        oneAlertLab.textColor = UIColorFromRGB(0x000000);
        
        alwaysAlertImg.hidden = NO;
        onceAlertImg.hidden = YES;
    }else{
        //        oneAlertLab.textColor = UIColorFromRGB(0x00a0e9);
        //        alwaysAlertLab.textColor = UIColorFromRGB(0x000000);
        
        alwaysAlertImg.hidden = YES;
        onceAlertImg.hidden = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBack) name:@"LOG_OUT_SUCCESS" object:nil];
}

//导航条返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)makeTheMainPartUI
{
    WS(wss);
    
    
    UIView *setView = [UIView new];
    [setView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    [self.view addSubview:setView];
    [setView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.view.mas_top).offset(64+kTopSlimSafeSpace);
        make.height.equalTo(@(25*kDeviceRate));
    }];
    
    
    UILabel *wlanLab = [UILabel new];
    wlanLab.text = @"非WiFi网络播放提醒";
    wlanLab.textAlignment = NSTextAlignmentLeft;
    wlanLab.font = HT_FONT_FOURTH;
    wlanLab.textColor = HT_COLOR_FONT_SECOND;
    [setView addSubview:wlanLab];
    [wlanLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(setView.mas_left).offset(12*kDeviceRate);
        make.centerY.equalTo(setView.mas_centerY);
        make.height.equalTo(setView.mas_height);
        make.right.equalTo(setView.mas_right);
    }];
    
    //分割线一
    UIImageView *fSideLine = [UIImageView new];
    [fSideLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [self.view addSubview:fSideLine];
    [fSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.height.equalTo(@(0.5*kDeviceRate));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(setView.mas_bottom);
    }];
    
    //上部背景图
    UIImageView *topImg = [UIImageView new];
    [topImg setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:topImg];
    [topImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wss.view.mas_centerX);
        make.width.equalTo(wss.view.mas_width);
        make.height.equalTo(@(100*kDeviceRate));
        make.top.equalTo(fSideLine.mas_bottom);
    }];
    topImg.userInteractionEnabled = YES;
    
    
    UIImageView *FirstLine = [UIImageView new];
    [FirstLine setBackgroundColor:HT_COLOR_SPLITLINE];
    //    [FirstLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
    [topImg addSubview:FirstLine];
    [FirstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topImg.mas_centerY);
        //make.width.equalTo(topImg.mas_width);
        make.left.equalTo(topImg.mas_left).offset(12*kDeviceRate);
        make.right.equalTo(topImg.mas_right).offset(-12*kDeviceRate);
        make.height.equalTo(@(0.5*kDeviceRate));
    }];
    
    
    //承载每次提醒的背景view
    UIView *viewOne = [UIView new];
    viewOne.backgroundColor = [UIColor clearColor];
    [topImg addSubview:viewOne];
    [viewOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topImg.mas_left);
        make.right.equalTo(topImg.mas_right);
        make.top.equalTo(topImg.mas_top);
        make.bottom.equalTo(FirstLine.mas_top);
    }];
    viewOne.userInteractionEnabled = YES;
    
    
    alwaysAlertLab = [UILabel new];
    alwaysAlertLab.text = @"每次提醒";
    alwaysAlertLab.textColor = HT_COLOR_FONT_FIRST;
    alwaysAlertLab.font = HT_FONT_SECOND;
    alwaysAlertLab.textAlignment = NSTextAlignmentLeft;
    alwaysAlertImg.backgroundColor = [UIColor clearColor];
    [viewOne addSubview:alwaysAlertLab];
    [alwaysAlertLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOne.mas_top);
        make.bottom.equalTo(viewOne.mas_bottom);
        make.left.equalTo(viewOne.mas_left).offset(12*kDeviceRate);
        make.width.equalTo(@(130*kDeviceRate));
    }];
    
    alwaysAlertImg = [UIImageView new];
    [alwaysAlertImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
    alwaysAlertImg.hidden = YES;
    [viewOne addSubview:alwaysAlertImg];
    [alwaysAlertImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(@(30*kDeviceRate));
        make.centerY.equalTo(alwaysAlertLab.mas_centerY);
        make.right.equalTo(viewOne.mas_right).offset(-12*kDeviceRate);
    }];
    
    
    MyGesture *alwaysAlertGes = [[MyGesture alloc] initWithTarget:self action:@selector(unWifiWarning:)];
    alwaysAlertGes.numberOfTapsRequired = 1;
    alwaysAlertGes.tag = 1;
    [viewOne addGestureRecognizer:alwaysAlertGes];
    
    
    
    //承载提醒一次的背景view
    UIView *viewTwo = [UIView new];
    viewTwo.backgroundColor = [UIColor clearColor];
    [topImg addSubview:viewTwo];
    viewTwo.userInteractionEnabled = YES;
    [viewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(viewOne);
        make.top.equalTo(FirstLine.mas_bottom);
        make.left.equalTo(topImg.mas_left);
    }];
    
    oneAlertLab = [UILabel new];
    oneAlertLab.textAlignment = NSTextAlignmentLeft;
    oneAlertLab.text = @"提醒一次";
    oneAlertLab.font = HT_FONT_SECOND;
    oneAlertLab.textColor = HT_COLOR_FONT_FIRST;
    [viewTwo addSubview:oneAlertLab];
    [oneAlertLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(alwaysAlertLab);
        make.left.equalTo(alwaysAlertLab.mas_left);
        make.centerY.equalTo(viewTwo.mas_centerY);
    }];
    
    onceAlertImg = [UIImageView new];
    [onceAlertImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
    onceAlertImg.hidden = YES;
    [viewTwo addSubview:onceAlertImg];
    [onceAlertImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(alwaysAlertImg);
        make.centerY.equalTo(viewTwo.mas_centerY);
        make.right.equalTo(alwaysAlertImg.mas_right);
    }];
    
    MyGesture *onceGes = [[MyGesture alloc] initWithTarget:self action:@selector(unWifiWarning:)];
    onceGes.numberOfTapsRequired = 1;
    onceGes.tag = 2;
    [viewTwo addGestureRecognizer:onceGes];
    
    
    //清除缓存部分
    UIView *cleanView = [UIView new];
    [cleanView setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:cleanView];
    [cleanView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.top.equalTo(viewTwo.mas_bottom).offset(5*kDeviceRate);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.height.equalTo(@(50*kDeviceRate));
    }];
    
    
    UILabel *cachesLab = [UILabel new];
    cachesLab.text = @"清除所有缓存";
    cachesLab.textColor = HT_COLOR_FONT_FIRST;
    cachesLab.font = HT_FONT_SECOND;
    cachesLab.textAlignment = NSTextAlignmentLeft;
    cachesLab.backgroundColor = [UIColor clearColor];
    [cleanView addSubview:cachesLab];
    
    CGSize cachesSize = [CalculateTextWidth sizeWithText:cachesLab.text font:HT_FONT_SECOND];
    int weightWW = cachesSize.width+30*kDeviceRate;
    
    [cachesLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cleanView.mas_left).offset(12*kDeviceRate);
        make.width.equalTo(@(weightWW));
        make.height.equalTo(cleanView.mas_height);
        make.centerY.equalTo(cleanView.mas_centerY);
    }];
    cachesLab.userInteractionEnabled = YES;
    
    
    MyGesture *cacheGesture = [[MyGesture alloc] initWithTarget:self action:@selector(cleanTheCaches)];
    cacheGesture.numberOfTapsRequired = 1;
    [cleanView addGestureRecognizer:cacheGesture];
    
    
    _cachSizeLab = [[UILabel alloc] init];
    _cachSizeLab.font = HT_FONT_THIRD;
    _cachSizeLab.textColor = HT_COLOR_FONT_SECOND;
    _cachSizeLab.textAlignment = NSTextAlignmentRight;
    _cachSizeLab.backgroundColor = [UIColor clearColor];
    
    
    //找到Documents文件所在的路径
//    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    //取得第一个Documents文件夹的路径
//    NSString *filePath = [path objectAtIndex:0];
    
    CGFloat cachFloat = [UserToolClass SDImageCacheSize];//[UserToolClass folderSizeAtPath:filePath];
        
    NSString *cachLabTextStr = cachFloat >= 1 ? [NSString stringWithFormat:@"当前缓存：%.2f MB",cachFloat]:[NSString stringWithFormat:@"当前缓存：%.2f KB",cachFloat*1024];
    
    _cachSizeLab.text = cachLabTextStr;
    
    if (cachFloat==0.00) {
        
        _cachSizeLab.text = @"0.00 KB";
    }
    
    [cleanView addSubview:_cachSizeLab];
    [_cachSizeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(cleanView.mas_right).offset(-12*kDeviceRate);
        make.height.equalTo(cleanView.mas_height);
        make.centerY.equalTo(cleanView.mas_centerY);
        make.left.equalTo(cachesLab.mas_right);
    }];
    
    
    UIView *scoreTwoBack = [UIView new];
    [scoreTwoBack setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [self.view addSubview:scoreTwoBack];
    [scoreTwoBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(cleanView);
        make.top.equalTo(cleanView.mas_bottom).offset(5*kDeviceRate);
        make.centerX.equalTo(cleanView.mas_centerX);
    }];
    
    
    UILabel *justScore = [UILabel new];
    justScore.text = @"为我评分";
    justScore.textColor = HT_COLOR_FONT_FIRST;
    justScore.font = HT_FONT_SECOND;
    justScore.textAlignment = NSTextAlignmentLeft;
    justScore.backgroundColor = [UIColor clearColor];
    [scoreTwoBack addSubview:justScore];
    
    CGSize scoreSize = [CalculateTextWidth sizeWithText:justScore.text font:HT_FONT_SECOND];
    int scoreWeight = scoreSize.width+30*kDeviceRate;
    
    [justScore mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(scoreTwoBack.mas_left).offset(12*kDeviceRate);
        make.width.equalTo(@(scoreWeight));
        make.height.equalTo(scoreTwoBack.mas_height);
        make.centerY.equalTo(scoreTwoBack.mas_centerY);
    }];
    cachesLab.userInteractionEnabled = YES;
    
    
    UIImageView *justImg = [UIImageView new];
    [justImg setImage:[UIImage imageNamed:@"ic_user_arrow"]];
    [scoreTwoBack addSubview:justImg];
    [justImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(scoreTwoBack.mas_right).offset(-12*kDeviceRate);
        make.width.equalTo(@(13*kDeviceRate));
        make.height.equalTo(@(13*kDeviceRate));
        make.centerY.equalTo(justScore.mas_centerY);
    }];
    justImg.userInteractionEnabled = YES;
    
    MyGesture *justGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToAPPStore)];
    justGes.numberOfTapsRequired = 1;
    [scoreTwoBack addGestureRecognizer:justGes];
    
    
    
    //退出登录部分
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [exitBtn setBackgroundImage:[UIImage imageNamed:@"sl_user_favchannel_add_normal"] forState:UIControlStateNormal];
    [exitBtn setImage:[UIImage imageNamed:@"sl_user_favchannel_add_normal"] forState:UIControlStateNormal];
    [exitBtn setImage:[UIImage imageNamed:@"btn_user_logout_pressed"] forState:UIControlStateHighlighted];
//    [exitBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_logout_pressed"] forState:UIControlStateHighlighted];
    [self.view addSubview:exitBtn];
    [exitBtn addTarget:self action:@selector(exitTheUserCenter) forControlEvents:UIControlEventTouchUpInside];
    exitBtn.adjustsImageWhenHighlighted = NO;
    
    if (IsLogin) {
        
        exitBtn.hidden = NO;
    }else{
        
        exitBtn.hidden = YES;
    }
    
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
            make.top.equalTo(scoreTwoBack.mas_bottom).offset(25*kDeviceRate);
            make.size.mas_equalTo(CGSizeMake(305*kDeviceRate, 40*kDeviceRate));
            make.centerX.mas_equalTo(wss.view.mas_centerX);
        }else{
            make.left.equalTo(wss.view.mas_left).offset(12*kDeviceRate);
            make.top.equalTo(scoreTwoBack.mas_bottom).offset(25*kDeviceRate);
            make.height.equalTo(@(40*kDeviceRate));
            make.right.equalTo(wss.view.mas_right).offset(-12*kDeviceRate);
        }
        
    }];

    
    if ([APP_ID isEqualToString:JKTV_SERIAL_NUMBER]) {
        UILabel *exitLab = [[UILabel alloc] init];
        [exitBtn addSubview:exitLab];
        [exitLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(exitBtn);
        }];
        exitLab.text = @"退出登录";
        exitLab.font = HT_FONT_SECOND;
        exitLab.textColor = App_selected_color;
        exitLab.textAlignment = NSTextAlignmentCenter;
    }


    versionLabel = [UILabel new];
    versionLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:11.0f];
    versionLabel.textColor = HT_COLOR_FONT_WARNING;
    versionLabel.backgroundColor = CLEARCOLOR;
    versionLabel.text = [NSString stringWithFormat:@"当前版本：V%@",APP_VERSION];
    versionLabel.textAlignment = NSTextAlignmentCenter;
   
    [self.view addSubview:versionLabel];
    
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(wss.view.mas_width);
        make.height.mas_equalTo(40*kDeviceRate);
        make.bottom.mas_equalTo(wss.view.mas_bottom).offset(-40*kDeviceRate);
        make.centerX.mas_equalTo(wss.view.mas_centerX);
    }];
    
}


//退出登录按钮事件
- (void)exitTheUserCenter
{
    CustomAlertView *customAlert = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:2 andCancelButtonMessage:@"再看看" andOtherButtonMessage:@"确定" withDelegate:self];
    
    customAlert.tag = 222;
    customAlert.messageStr = @"您确定要退出吗？";
    [customAlert show];
}

//清空缓存手势事件
- (void)cleanTheCaches
{
    CustomAlertView *custom = [[CustomAlertView alloc] initWithcacheTitle:@"" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确定" withDelegate:self];
    
    custom.tag = 100;
    [custom show];
}

- (void)goToAPPStore
{
    NSLog(@"评分思密达！");
    
    NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@",APP_ID];//, QQ_SHARE_APP_ID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];//
}


#pragma  mark customalert delegate
- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    {//清除缓存alert
        if(buttonIndex==0)
        {//确定清除
            NSLog(@"确定");
            
//            NSFileManager *fm = [NSFileManager defaultManager];
//            
//            //找到Documents文件所在的路径
//            NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            //取得第一个Documents文件夹的路径
//            NSString *filePath = [path objectAtIndex:0];


//            //把TestPlist文件加入
//            NSString *plistPath1 = [filePath stringByAppendingPathComponent:PLAYRECORDERPLIST];
//            NSString *plistPath2 = [filePath stringByAppendingPathComponent:LIVEFAVORITEPLIST];
//            NSString *plistPath3 = [filePath stringByAppendingPathComponent:VODFAVORITEPLIST];
//            NSString *plistPath5 = [filePath stringByAppendingPathComponent:GUESTLIVEFAVORITEPLIST];
//            NSString *plistPath6 = [filePath stringByAppendingPathComponent:GUESTPLAYRECORDERPLIST];
//            NSString *plistPath7 = [filePath stringByAppendingPathComponent:GUESTVODFAVORITEPLIST];
//            NSString *plistPath4 = [filePath stringByAppendingPathComponent:@"tests.plist"];
//            
//            NSString *plistPath8 = [filePath stringByAppendingPathComponent:SEARCHRECORDERPLIST];
//            NSString *plistPath9 = [filePath stringByAppendingPathComponent:GUESTSEARCHRECORDERPLIST];
//            NSString *plistPath10 = [filePath stringByAppendingPathComponent:REWATCHPLAYRECORDERPLIST];
//            NSString *plistPath11 = [filePath stringByAppendingPathComponent:GUESTREWATCHPLAYRECORDERPLIST];
//            
//            //删除文件
//            [fm removeItemAtPath:plistPath1 error:nil];
//            [fm removeItemAtPath:plistPath2 error:nil];
//            [fm removeItemAtPath:plistPath3 error:nil];
//            [fm removeItemAtPath:plistPath4 error:nil];
//            [fm removeItemAtPath:plistPath5 error:nil];
//            [fm removeItemAtPath:plistPath6 error:nil];
//            [fm removeItemAtPath:plistPath7 error:nil];
//            [fm removeItemAtPath:plistPath8 error:nil];
//            [fm removeItemAtPath:plistPath9 error:nil];
//            [fm removeItemAtPath:plistPath10 error:nil];
//            [fm removeItemAtPath:plistPath11 error:nil];
            
//            [UserToolClass clearCache:filePath];
            
            [UserToolClass clearSDImageCache];
            
            _cachSizeLab.text = @"当前缓存：0.00 KB";
        }else{
            return;
        }
    }
    else if (alertView.tag == 222) {//退出登录
        
        if (buttonIndex == 0) {//确定按钮
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在注销"];
            
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            [request logoutWithTimeout:10];
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [DejalBezelActivityView removeViewAnimated:YES];
                
                //清除用户信息--信息采集--游客登陆
                [HTYJUserCenter logoutCleanUserProfile];

            });
            
        }else if (buttonIndex == 1){//取消按钮
            
            return;
        }
    }
}


//非wifi网络提醒事件
- (void)unWifiWarning:(MyGesture *)ges
{
    if (ges.tag == 1) {
        NSLog(@"－－－－－－－－－－－－－总是提醒");
        
        //        alwaysAlertLab.textColor = UIColorFromRGB(0x00a0e9);
        //        oneAlertLab.textColor = UIColorFromRGB(0x000000);
        
        alwaysAlertImg.hidden = NO;
        onceAlertImg.hidden = YES;
        
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"NetworkTypeShowStatus"];
        // 设置提醒一次置0，提醒一次
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"EveryTimeWarnFlag"];
        
        //        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else{
        
        NSLog(@"----------------------提醒一次");
        //        oneAlertLab.textColor = UIColorFromRGB(0x00a0e9);
        //        alwaysAlertLab.textColor = UIColorFromRGB(0x000000);
        
        alwaysAlertImg.hidden = YES;
        onceAlertImg.hidden = NO;
        
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"NetworkTypeShowStatus"];
        
        //        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    // [DejalActivityView removeView];
    
    if ([type isEqualToString:@"logout"]) {
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    break;
                }
                case -1:{
                    NSLog(@"注销失败");
                    
                    break;
                }
                default:
                    break;
            }
        }else{
            
            //[AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
        
    }
    else{
        
        
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LOG_OUT_SUCCESS" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
