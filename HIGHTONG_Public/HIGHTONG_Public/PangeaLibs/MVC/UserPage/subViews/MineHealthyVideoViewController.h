//
//  MineHealthyVideoViewController.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralPromptView.h"

@interface MineHealthyVideoViewController : HT_FatherViewController
{
    NSInteger _mediaCount;
    CGFloat  _mediaHight;
    
    UIView *_noView;//  ***** 暂无图片的 View *****
    
    BOOL _selAllBtnIsClick;//标记全选按钮是否被点击
}

@property (assign, nonatomic) BOOL _vodIsEditing;//点播垃圾桶按钮是否是编辑状态

@property (strong, nonatomic) NSMutableArray *vodArr;

@property (strong, nonatomic) GeneralPromptView *noVODDataLabel;//暂无点播收藏记录数据

@property (strong, nonatomic) UITableView *vodTable;//点播视图

@property (copy, nonatomic) NSString *controllerTitleStr;

@property (copy, nonatomic) NSString *flagStr;//区分是vod的收藏、医视频收藏



@end
