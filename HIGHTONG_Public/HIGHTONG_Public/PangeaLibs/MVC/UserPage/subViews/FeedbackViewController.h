//
//  FeedbackViewController.h
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/29.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

@protocol JavaScriptObjectiveCDelegate <JSExport>

- (NSString *)getNativeInfo;

- (void)feedbackSuccess;

- (void)reTryLogin;

- (void)finish;

- (NSString *)getSign;

@end

@interface FeedbackViewController : HT_FatherViewController

@end
