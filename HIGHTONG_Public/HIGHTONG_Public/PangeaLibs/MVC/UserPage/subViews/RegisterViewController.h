//
//  RegisterViewController.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/3.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "VerityAlertView.h"
#import "ErrorAlertView.h"

@interface RegisterViewController : HT_FatherViewController

@property (nonatomic,strong)UIButton          *backBtn;//返回按钮
@property (nonatomic,strong)UIButton          *loginBtn;//注册按钮
@property (nonatomic,strong)GeneralInputBlock *nameField;//用户名
@property (nonatomic,strong)GeneralInputBlock *pwdField;//密码
@property (nonatomic,strong)GeneralInputBlock *verityField;//验证码
@property (nonatomic,strong)UIButton          *registerBtn;//注册按钮
@property (nonatomic,strong)UIButton          *ifAgreeBtn;//是否同意按钮
@property (nonatomic,strong)UIView            *showVerityBack;
@property (nonatomic,strong)UIView            *mainBackView;
@property (nonatomic,strong)UIButton          *verityBtn;//验证码按钮
@property (nonatomic,strong)VerityAlertView   *verityPost;
@property (nonatomic,strong)ErrorAlertView    *errorView;


@end
