//
//  RegisterViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/3.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "RegisterViewController.h"
#import "MyGesture.h"
#import "CHECKFORMAT.h"
//#import "ServiceAgreementViewController.h"
#import "DejalActivityView.h"
#import "UserTime.h"
#import "LoginViewController.h"
#import "AlertLabel.h"
#import "CalculateTextWidth.h"
#import "CustomAlertView.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

@interface RegisterViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    BOOL canReg;//是否可以注册
    BOOL phonenumberOK;//电话号码格式正确
    BOOL passwordOK;//密码格式正确
    BOOL authCodeOK;//验证码格式正确
    BOOL _didUp;
    BOOL ifReadRule;//是否同意并阅读条例

}

@property (nonatomic, strong)CustomAlertView *customView;

@end

@implementation RegisterViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"RegisterPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"RegisterPage"];
    
    [_customView resignKeyWindow];
    
//    _customView = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"注册"];
    
    self.view.backgroundColor = App_white_color;

    [self theMainPartUI];
    
    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        //        [_customView resignKeyWindow];
        
        //        _customView = nil;
        
        _customView.hidden = YES;
    }
    return;
}

- (void)theMainPartUI
{
    WS(wss);
    
    
    //验证码发送至手机
    _verityPost = [[VerityAlertView alloc] init];
    [self.view addSubview:_verityPost];
    [_verityPost mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(44));
        make.top.equalTo(wss.view.mas_top).offset(20);
        make.width.equalTo(wss.view.mas_width);
        make.left.equalTo(wss.view.mas_left);
    }];
    _verityPost.hidden = YES;


    //账号框
    _nameField = [[GeneralInputBlock alloc] initWithTitle:@"账号：    " andPlaceholderText:@"请输入11位手机号"];
    _nameField.userInteractionEnabled = YES;
    _nameField.inputBlock.delegate = self;
    [self.view addSubview:_nameField];
    [_nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left);
        make.top.equalTo(wss.verityPost.mas_bottom).offset(10*kRateSize);
        make.height.equalTo(@(44*kRateSize));
        make.right.equalTo(wss.view.mas_right);
    }];
    
    
    //分割线一
    UIImageView *firstSideLine = [[UIImageView alloc] init];
    [firstSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:firstSideLine];
    [firstSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(280*kRateSize));
        make.height.equalTo(@(0.5*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.nameField.mas_bottom);
    }];
    
    
    //密码框
    _pwdField = [[GeneralInputBlock alloc] initWithTitle:@"密码：    " andPlaceholderText:@"6-16位数字、字母或字符"];
    _pwdField.userInteractionEnabled = YES;
    _pwdField.inputBlock.delegate = self;
    _pwdField.inputBlock.secureTextEntry = YES;
    [self.view addSubview:_pwdField];
    [_pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.nameField);
        make.left.equalTo(wss.nameField.mas_left);
        make.top.equalTo(firstSideLine.mas_bottom);
    }];
    
    
    //分割线二
    UIImageView *secondSideLine = [[UIImageView alloc] init];
    [secondSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:secondSideLine];
    [secondSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(firstSideLine);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.pwdField.mas_bottom);
    }];
    
    
    //验证码
    
    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码" withVerityBtnName:@"获取验证码"];
    [_verityField.verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
    
//    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码"];
    _verityField.inputBlock.delegate = self;
//    _verityField.inputBlock.clearButtonMode = UITextFieldViewModeNever;
    _verityField.userInteractionEnabled = YES;
    [self.view addSubview:_verityField];
    [_verityField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.pwdField);
        make.top.equalTo(secondSideLine.mas_bottom);//.offset(fiveGap);
        make.left.equalTo(wss.pwdField.mas_left);
    }];
    
    
    //分割线三
    UIImageView *thirdSideLine = [[UIImageView alloc] init];
    [thirdSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:thirdSideLine];
    [thirdSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(secondSideLine);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.verityField.mas_bottom);
    }];
    thirdSideLine.userInteractionEnabled = YES;
    
//    //self.verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_verityBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
//    [_verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
////    [_verityBtn setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
//    [_verityBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
//    _verityBtn.titleLabel.font = App_font(13);
//    [_verityField addSubview:_verityBtn];
//    
//    CGSize veritySize = [CalculateTextWidth sizeWithText:_verityBtn.titleLabel.text font:App_font(13)];
//    int weightWW = veritySize.width;
//    
//    [_verityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(wss.verityField.mas_centerY);
//        make.right.equalTo(wss.verityField.mas_right).offset(-20*kRateSize);
//        make.height.equalTo(wss.verityField.mas_height);
//        make.width.equalTo(@(weightWW*kRateSize));
//    }];
    
    
    //目的重新设置 textfield的frame,使自带删除属性的位置在获取验证码的左侧
//    [_verityField.inputBlock mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wss.verityField.titleLab.mas_right).offset(5*kRateSize);
//        make.height.equalTo(wss.verityField.mas_height);
//        make.centerY.equalTo(wss.verityField.mas_centerY);
//        make.right.equalTo(wss.verityBtn.mas_left);
//    }];

    
//    //原来的错误提示，现在改为弹框显示
//    _errorView = [[ErrorAlertView alloc] init];
//    [self.view addSubview:_errorView];
//    _errorView.hidden = YES;
//    [_errorView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.top.equalTo(_verityBtn.mas_bottom);
//        make.height.equalTo(@(25*kRateSize));
//    }];

    
    
    //注册
    _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_registerBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_register_normal"] forState:UIControlStateNormal];
    [_registerBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_register_pressed"] forState:UIControlStateHighlighted];
    [_registerBtn addTarget:self action:@selector(RegisterSender) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_registerBtn];
    [_registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(40*kRateSize));
        make.width.equalTo(@(305*kRateSize));
        make.top.equalTo(thirdSideLine.mas_bottom).offset(15*kRateSize);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    
    UIView *protocalView = [UIView new];
    [protocalView setBackgroundColor:CLEARCOLOR];
    [self.view addSubview:protocalView];
    [protocalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.height.equalTo(@(22*kRateSize));
        make.top.equalTo(wss.registerBtn.mas_bottom).offset(15*kRateSize);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    //是否同意服务协议
    _ifAgreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //..............此处应该放默认图片
    [_ifAgreeBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"] forState:UIControlStateNormal];
    //... 选中时
    [_ifAgreeBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"] forState:UIControlStateSelected];
    [_ifAgreeBtn addTarget:self action:@selector(ifAgreement:) forControlEvents:UIControlEventTouchUpInside];
    [protocalView addSubview:_ifAgreeBtn];
    [_ifAgreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(protocalView.mas_left).offset(75*kRateSize);
        make.width.height.equalTo(@(22*kRateSize));
        make.top.equalTo(protocalView.mas_top);
    }];
    // 默认
    _ifAgreeBtn.selected = YES;
    ifReadRule = YES;
    
    
    UILabel *agreeInfoLab = [UILabel new];
    agreeInfoLab.text = @"我已同意该应用的服务协议";
    agreeInfoLab.font = App_font(11);
    agreeInfoLab.textAlignment = NSTextAlignmentLeft;
    agreeInfoLab.backgroundColor = CLEARCOLOR;
    agreeInfoLab.textColor = UIColorFromRGB(0x999999);
    agreeInfoLab.adjustsFontSizeToFitWidth = YES;
    [protocalView addSubview:agreeInfoLab];
    
    CGSize agreeSize = [CalculateTextWidth sizeWithText:agreeInfoLab.text font:App_font(11)];
    int heightWW = agreeSize.height;
    
    [agreeInfoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.ifAgreeBtn.mas_right).offset(6*kRateSize);
        make.height.equalTo(@(heightWW*kRateSize));
        make.centerY.equalTo(wss.ifAgreeBtn.mas_centerY);
    }];

    agreeInfoLab.userInteractionEnabled = YES;
    
    
    UIImageView *protocolImg = [[UIImageView alloc] init];
    [protocolImg setImage:[UIImage imageNamed:@"img_user_protocol_arrow"]];
    [protocalView addSubview:protocolImg];
    [protocolImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(6*kRateSize));
        make.height.equalTo(@(8*kRateSize));
        make.centerY.equalTo(agreeInfoLab.mas_centerY);
        make.left.equalTo(agreeInfoLab.mas_right).offset(4*kRateSize);
    }];
    
    
    MyGesture *serviceGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheServiceVC)];
    serviceGes.numberOfTapsRequired = 1;
    [agreeInfoLab addGestureRecognizer:serviceGes];

}

//进入甜果服务协议
- (void)goToTheServiceVC
{
//    [self.navigationController pushViewController:[[ServiceAgreementViewController alloc] init]animated:YES];
    

    KnowledgeBaseViewController *service = [[KnowledgeBaseViewController alloc] init];
    
    service.urlStr = [UrlPageJumpManeger loginPageServiceVCJump];
    
    service.existNaviBar = YES;
    
    [self.navigationController pushViewController:service animated:YES];

}

//是否同意服务协议按钮触发事件
- (void)ifAgreement:(UIButton *)sender
{//...此处要更换图片
    
    sender.selected = !sender.selected;
    
    if(sender.selected){
        ifReadRule = YES;
        NSLog(@"yeyeyeeyeyyeyeyeyeyeyey");
        
        [_registerBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_register_normal"] forState:UIControlStateNormal];
        
        _registerBtn.enabled = YES;
    }else{
        ifReadRule = NO;
        NSLog(@"nonononononononononononono");
        
        [_registerBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_register_disable"] forState:UIControlStateNormal];

        _registerBtn.enabled = NO;
        //[AlertLabel AlertInfoWithText:@"请同意应用协议" andWith:self.view withLocationTag:0];
    }
    
    NSLog(@"%d", ifReadRule);
}

//获取验证码触发方法
- (void)getCoding:(UIButton*)sender
{
    [_nameField.inputBlock resignFirstResponder];
    [_pwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    if ([_nameField.inputBlock.text isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];

        return;
    }
    
    NSLog(@"+++++++++++++++++++++++发送验证码");

    if ([CHECKFORMAT checkPhoneNumber:_nameField.inputBlock.text])
    {
        phonenumberOK = YES;
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"0" andMobilePhone:_nameField.inputBlock.text andTimeout:10];

    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";

        //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];
    }
}

//注册按钮触发事件
- (void)RegisterSender
{
    
    [_nameField.inputBlock resignFirstResponder];
    [_pwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];

    
    //手机号码比对
    if ([CHECKFORMAT checkPhoneNumber:_nameField.inputBlock.text]) {
        
        phonenumberOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";
       
        //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的手机号码";
        
        [_customView show];
        

        return;
    }
    
    
    //密码比对
    if ([CHECKFORMAT checkPassword:_pwdField.inputBlock.text]) {
        
        passwordOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的密码";
        
        //[AlertLabel AlertInfoWithText:@"密码不符合规范" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的密码";
        
        [_customView show];
    
        return;
    }
    
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityField.inputBlock.text]) {
        
        authCodeOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的验证码";

        //[AlertLabel AlertInfoWithText:@"请输入正确的验证码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    if (phonenumberOK&passwordOK&authCodeOK&ifReadRule) {//&ifReadRule
        //发起请求
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewmobileRegisterWithMobilePhone:_nameField.inputBlock.text andPassWord:_pwdField.inputBlock.text andAuthCode:_verityField.inputBlock.text andTimeout:10];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在注册 请稍后"];
        
    }else{
        //检查账号和密码格式是否正确
        
        return;
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
    
    [DejalActivityView removeView];

    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
//                    _errorView.hidden = YES;

                    [UserTime changBtn:self.verityField.verityBtn withSleepSecond:60];
                    [self.verityField.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];
                    
//                    WS(wss);
//
//                    _verityPost.phoneNumLable.text = _nameField.inputBlock.text;
//                    
//                    [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                        make.height.equalTo(@(44));
//                        make.top.equalTo(wss.view.mas_top).offset(64);
//                        make.width.equalTo(wss.view.mas_width);
//                        make.left.equalTo(wss.view.mas_left);
//                    }];
//                    _verityPost.hidden = NO;
//                    
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        WS(wss);
//                        [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                            make.height.equalTo(@(44));
//                            make.top.equalTo(wss.view.mas_top).offset(20);
//                            make.width.equalTo(wss.view.mas_width);
//                            make.left.equalTo(wss.view.mas_left);
//                            
//                            _verityPost.hidden = YES;
//                        }];
//                        
//                    });

                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"请输入正确的手机号码";
                    
                    //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"验证码已发疯 请明日再试";
                    
                    //[AlertLabel AlertInfoWithText:@"验证码已发疯 请明日再试" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"该手机号已注册";

                    //[AlertLabel AlertInfoWithText:@"该手机号已注册" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"该手机号已注册";
                    
                    [_customView show];
                
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";

                    //[AlertLabel AlertInfoWithText:@"无法访问个人中心" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];
            
                    break;
                }
                
                default:
                    break;
            }

        }else{

//            _errorView.hidden = YES;

            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];

        }
    }
    else if ([type isEqualToString:mobileRegister])
    {
        if (result.count>0) {
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:{
                NSLog(@"手机注册成功");
                
//                _errorView.hidden = YES;
                
                //...ww记住用户名和密码啦
                [[NSUserDefaults standardUserDefaults] setObject:_nameField.inputBlock.text forKey:@"mindUserName"];//自动登录的时候记住账号
                [[NSUserDefaults standardUserDefaults] setObject:_pwdField.inputBlock.text forKey:@"mindPWD"];//自动登录的时候记住密码
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"测试%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"]);
                NSLog(@"测试%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"]);
                
                [AlertLabel AlertInfoWithText:@"注册成功！" andWith:self.view withLocationTag:0];
                
                [self performSelector:@selector(goBack) withObject:nil afterDelay:2.5];

               // [self.navigationController popViewControllerAnimated:YES];
                
                break;
            }
            case -3:{
                NSLog(@"手机号错误");
                
//                _errorView.hidden = NO;
//                
//                _errorView.alertLab.text = @"请输入正确的手机号码";
                
                //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];

                _customView.messageStr = @"请输入正确的手机号码";
                
                [_customView show];
                
                break;
            }
            case -7:{
                NSLog(@"验证码错误或者超时效");
                
//                _errorView.hidden = NO;
//                
//                _errorView.alertLab.text = @"请输入正确的验证码";

                //[AlertLabel AlertInfoWithText:@"请输入正确的验证码" andWith:self.view withLocationTag:0];

                _customView.messageStr = @"请输入正确的验证码";
                
                [_customView show];
                
                break;
            }
            case -5:{
                NSLog(@"手机号已经注册");
                
//                _errorView.hidden = NO;
//                
//                _errorView.alertLab.text = @"该手机号已注册";
                
                //[AlertLabel AlertInfoWithText:@"该手机号已注册" andWith:self.view withLocationTag:0];
                
                _customView.messageStr = @"该手机号已注册";
                
                [_customView show];
                
                break;
            }
            case -9:{
                NSLog(@"其他异常");
                
//                _errorView.hidden = NO;
//                
//                _errorView.alertLab.text = @"无法访问个人中心";

                _customView.messageStr = @"无法访问个人中心";
                
                [_customView show];
                
                break;
            }
                
            default:
                break;
            }
        }else{
            
//            _errorView.hidden = YES;

            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
    }
}

#pragma mark - 回收键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_nameField.inputBlock resignFirstResponder];
    [_pwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    //尺寸调整动画(返回原来位置)
    
    if (_didUp) {
        [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
        [UIView setAnimationDuration:0.43f];
        if (KDeviceHeight==480) {
            self.view.frame=CGRectMake(0, 55, kDeviceWidth, KDeviceHeight);
            _didUp=NO;
        }
        
        [UIView commitAnimations];
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_nameField.inputBlock resignFirstResponder];
    [_pwdField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    //尺寸调整动画（shangsheng）
    if (_didUp) {
        [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
        [UIView setAnimationDuration:0.43f];
        if (KDeviceHeight==480) {
            self.view.frame=CGRectMake(0, 55, kDeviceWidth, KDeviceHeight);
            _didUp=NO;
        }
        
        [UIView commitAnimations];
    }
}

//返回按钮触发方法
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
