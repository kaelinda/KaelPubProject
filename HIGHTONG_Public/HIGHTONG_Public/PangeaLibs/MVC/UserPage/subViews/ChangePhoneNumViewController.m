//
//  ChangePhoneNumViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ChangePhoneNumViewController.h"
#import "CHECKFORMAT.h"
#import "CustomAlertView.h"
#import "MyGesture.h"
#import "UserTime.h"
#import "AlertLabel.h"
#import "CalculateTextWidth.h"
#import "CustomAlertView.h"

@interface ChangePhoneNumViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    UIButton *backBtn;//返回按钮
    BOOL phonenumberOK;//电话号码格式正确
    BOOL passwordOK;//密码格式正确
    BOOL authCodeOK;//验证码格式正确

}

@property (nonatomic, strong) CustomAlertView *customView;

@end

@implementation ChangePhoneNumViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"ChangePhonePage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"ChangePhonePage"];
    
    [_customView resignKeyWindow];
    
//    _customView = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNaviBarTitle:@"更换手机"];
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:backBtn];
    [self setNaviBarRightBtn:nil];

    self.view.backgroundColor = App_background_color;
    WS(wss);
    
    _verityPost = [[VerityAlertView alloc] init];
    [self.view addSubview:_verityPost];
    [_verityPost mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(44));
        make.top.equalTo(wss.view.mas_top).offset(20);
        make.width.equalTo(wss.view.mas_width);
        make.left.equalTo(wss.view.mas_left);
    }];
    _verityPost.hidden = YES;

    //当前账号
//    _phoneNumField = [[GeneralInputBlock alloc] initWithTitle:@"当前账号：" andPlaceholderText:nil];
//    _phoneNumField.titleLab.textColor = UIColorFromRGB(0x999999);
//    _phoneNumField.inputBlock.delegate = self;
//    _phoneNumField.userInteractionEnabled = NO;
//    _phoneNumField.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
//    _phoneNumField.inputBlock.textColor = UIColorFromRGB(0x999999);
//    [self.view addSubview:_phoneNumField];
//    [_phoneNumField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.height.equalTo(@(44*kRateSize));
//        make.top.equalTo(wss.view.mas_top).offset((64+10)*kRateSize);
//    }];
//    [_phoneNumField setBackgroundColor:UIColorFromRGB(0xf2f2f2)];

    
    _backView = [UIView new];
    [_backView setBackgroundColor:App_white_color];
    [self.view addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.height.equalTo(@(44*kRateSize));
        make.top.equalTo(wss.view.mas_top).offset(64+10*kRateSize);
    }];
    
    
    _currentNumLab = [UILabel new];
    _currentNumLab.font = App_font(15);
    _currentNumLab.textColor = UIColorFromRGB(0x999999);
    _currentNumLab.textAlignment = NSTextAlignmentLeft;
    [_backView addSubview:_currentNumLab];
    
    NSString *phoneStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    NSString *newPhoneStr = [phoneStr stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    _currentNumLab.text = [NSString stringWithFormat:@"当前账号：%@",newPhoneStr];
    
    CGSize currentNumSize = [CalculateTextWidth sizeWithText:_currentNumLab.text font:App_font(15)];
    int numheight = currentNumSize.height;
    
    [_currentNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.backView.mas_left).offset(20*kRateSize);
        make.top.equalTo(wss.backView.mas_top).offset(5*kRateSize);
        make.right.equalTo(wss.backView.mas_right);
        make.height.equalTo(@(numheight));
    }];
    
    _tipsLab = [UILabel new];
    _tipsLab.text = @"为保证您的账号安全，请先输入该账号登录密码";
    _tipsLab.font = App_font(10);
    _tipsLab.textColor = UIColorFromRGB(0x999999);
    _tipsLab.textAlignment = NSTextAlignmentLeft;
    [_backView addSubview:_tipsLab];
    [_tipsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.currentNumLab.mas_bottom).offset(2.5*kRateSize);
        make.bottom.equalTo(wss.backView.mas_bottom).offset(-5*kRateSize);
        make.left.equalTo(wss.backView.mas_left).offset(20*kRateSize);
        make.right.equalTo(wss.backView.mas_right);
    }];
    
    
    //分割线一
    UIImageView *fSideLine = [UIImageView new];
    [fSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:fSideLine];
    [fSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@generalWidth);
        make.height.equalTo(@(0.5*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.backView.mas_bottom);
    }];
    
    
    //密码输入框
    _pwdNum = [[GeneralInputBlock alloc] initWithTitle:@"密码：    " andPlaceholderText:@"6-16位数字、字母或字符"];
    _pwdNum.inputBlock.secureTextEntry = YES;
    _pwdNum.userInteractionEnabled = YES;
    _pwdNum.inputBlock.delegate = self;
    [self.view addSubview:_pwdNum];
    [_pwdNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left);
        make.right.equalTo(wss.view.mas_right);
        make.height.equalTo(@(44*kRateSize));
        make.top.equalTo(fSideLine.mas_bottom);
    }];
    
    
    //分割线二
    UIImageView *sSideaLine = [UIImageView new];
    [sSideaLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:sSideaLine];
    [sSideaLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(fSideLine);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.pwdNum.mas_bottom);
    }];

    
    //新号码输入框
    _recentPhoneNum = [[GeneralInputBlock alloc] initWithTitle:@"新手机：" andPlaceholderText:@"请输入11位手机号"];
    _recentPhoneNum.inputBlock.secureTextEntry = NO;
    _recentPhoneNum.userInteractionEnabled = YES;
    _recentPhoneNum.inputBlock.delegate = self;
    [self.view addSubview:_recentPhoneNum];
    [_recentPhoneNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.pwdNum);
        make.centerX.equalTo(wss.pwdNum);
        make.top.equalTo(sSideaLine.mas_bottom);
    }];
    
    
    //分割线三
    UIImageView *thSideLine = [UIImageView new];
    [thSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:thSideLine];
    [thSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(sSideaLine);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.recentPhoneNum.mas_bottom);
    }];
    
    
    //验证码输入框
//    _verityNum = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码"];
    
    _verityNum = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入验证码" withVerityBtnName:@"获取验证码"];
    [_verityNum.verityBtn addTarget:self action:@selector(verityBtnSender) forControlEvents:UIControlEventTouchUpInside];
    
    
    _verityNum.inputBlock.secureTextEntry = NO;
    _verityNum.userInteractionEnabled = YES;
    _verityNum.inputBlock.delegate = self;
//    _verityNum.inputBlock.clearButtonMode = UITextFieldViewModeNever;
    [self.view addSubview:_verityNum];
    [_verityNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.recentPhoneNum);
        make.centerX.equalTo(wss.recentPhoneNum.mas_centerX);
        make.top.equalTo(thSideLine.mas_bottom);
    }];
    
    
//    _verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_verityBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
//    [_verityBtn addTarget:self action:@selector(verityBtnSender) forControlEvents:UIControlEventTouchUpInside];
////    [_verityBtn setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
//    [_verityBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
//    _verityBtn.titleLabel.font = App_font(13);
//    [_verityNum addSubview:_verityBtn];
//    
//    CGSize veritySize = [CalculateTextWidth sizeWithText:_verityBtn.titleLabel.text font:App_font(13)];
//    int weightWW = veritySize.width;
//    
//    [_verityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(wss.verityNum.mas_centerY);
//        make.right.equalTo(wss.verityNum.mas_right).offset(-20*kRateSize);
//        make.height.equalTo(wss.verityNum.mas_height);
//        make.width.equalTo(@(weightWW*kRateSize));
//    }];

    
    //目的重新设置 textfield的frame,使自带删除属性的位置在获取验证码的左侧
//    [_verityNum.inputBlock mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wss.verityNum.titleLab.mas_right).offset(5*kRateSize);
//        make.height.equalTo(wss.verityNum.mas_height);
//        make.centerY.equalTo(wss.verityNum.mas_centerY);
//        make.right.equalTo(wss.verityBtn.mas_left);
//    }];

    
    _errorView = [[ErrorAlertView alloc] init];
    [self.view addSubview:_errorView];
    _errorView.hidden = YES;
    [_errorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.pwdNum.mas_bottom);
        make.height.equalTo(@(25*kRateSize));
    }];

    

    UIButton *complish = [UIButton buttonWithType:UIButtonTypeCustom];
    [complish setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_normal"] forState:UIControlStateNormal];
    [complish setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_pressed"] forState:UIControlStateHighlighted];
    [complish addTarget:self action:@selector(complishBtnSender) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:complish];
    [complish mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@generalBtnWidth);
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.verityNum.mas_bottom).offset(15*kRateSize);
    }];

    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        //        [_customView resignKeyWindow];
        
        //        _customView = nil;
        
        _customView.hidden = YES;
    }
    return;
}


//完成按钮点击事件
- (void)complishBtnSender
{
    
    [_recentPhoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];
    [_verityNum.inputBlock resignFirstResponder];

    
    //手机号码比对
    if ([CHECKFORMAT checkPhoneNumber:_recentPhoneNum.inputBlock.text]) {
        
        phonenumberOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";
        
        //[AlertLabel AlertInfoWithText:@"请检查手机号码" andWith:self.view withLocationTag:0];

        _customView.messageStr = @"请输入正确的手机号码";
        
        [_customView show];
        
        return;
    }
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityNum.inputBlock.text]) {
        authCodeOK=YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的验证码";
        
        //[AlertLabel AlertInfoWithText:@"验证码不符合规范" andWith:self.view withLocationTag:0];

        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    //密码比对
    if ([CHECKFORMAT checkPassword:_pwdNum.inputBlock.text]) {
        
        passwordOK=YES;
        
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的密码";
        
        //[AlertLabel AlertInfoWithText:@"密码不符合规范" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的密码";
        
        [_customView show];

        return;
    }
    

    if (phonenumberOK&passwordOK&authCodeOK) {
        NSLog(@"我要开始换号码请求了哟");
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewalterMobilePhoneWithMobilePhone:_recentPhoneNum.inputBlock.text andPassWord:_pwdNum.inputBlock.text andAuthCode:_verityNum.inputBlock.text andTimeout:10];
        
    }else{
        //检查账号和密码格式是否正确
        
        return;
    }

}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
    
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        if (result.count > 0) {
            
            NSLog(@"%@", result);
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    _errorView.hidden = YES;
                    
                    [UserTime changBtn:_verityNum.verityBtn withSleepSecond:60];
                    [self.verityNum.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];

//                    _verityPost.phoneNumLable.text = _phoneNum.inputBlock.text;
//                    
//                    WS(wss);
//                    
//                    [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                        make.height.equalTo(@(44));
//                        make.top.equalTo(wss.view.mas_top).offset(64);
//                        make.width.equalTo(wss.view.mas_width);
//                        make.left.equalTo(wss.view.mas_left);
//                    }];
//                    _verityPost.hidden = NO;
//                    
//                    
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        WS(wss);
//                        [_verityPost mas_remakeConstraints:^(MASConstraintMaker *make) {
//                            make.height.equalTo(@(44));
//                            make.top.equalTo(wss.view.mas_top).offset(20);
//                            make.width.equalTo(wss.view.mas_width);
//                            make.left.equalTo(wss.view.mas_left);
//                        }];
//                        
//                        _verityPost.hidden = YES;
//                    });

                    
                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    //提示用户不存在
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"请输入正确的手机号码";

                    //[AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"验证码已发疯 请明日再试";

                    //[AlertLabel AlertInfoWithText:@"验证码已发疯 请明日再试" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"手机号已经注册";

                    _customView.messageStr = @"手机号已经注册";
                    
                    [_customView show];
                    

                   // [AlertLabel AlertInfoWithText:@"手机号已经注册" andWith:self.view withLocationTag:0];
                    
                    //[_verityBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";
                    
                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];

                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
    }
    else if ([type isEqualToString:alterMobilePhone])
    {
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"手机号更换成功");
                    
//                    _errorView.hidden = YES;
                    
                    [[NSUserDefaults standardUserDefaults] setObject:_recentPhoneNum.inputBlock.text forKey:@"mindUserName"];//自动登录的时候记住账号
                    
                    [AlertLabel AlertInfoWithText:@"手机号更换成功！" andWith:self.view withLocationTag:0];
    
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"itIsMe"]) {
                        
                        [[NSUserDefaults standardUserDefaults] setObject:_recentPhoneNum.inputBlock.text forKey:@"userIden"];
                    }
                
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    
                    
                    [self performSelector:@selector(goBack) withObject:nil afterDelay:2.5];
                                        
                    break;
                }
                case -3:{
                    NSLog(@"手机号错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"请输入正确的手机号码";

                   // [AlertLabel AlertInfoWithText:@"请输入正确的手机号码" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                    
                case -6:{
                    NSLog(@"原密码错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"原密码错误";
                    
                    //[AlertLabel AlertInfoWithText:@"原密码错误" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"原密码错误";
                    
                    [_customView show];
                    
                    break;
                }
                case -7:{
                    NSLog(@"验证码错误或者超时效");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"验证码已过期 请重新获取";
                    
                    //[AlertLabel AlertInfoWithText:@"验证码已过期 请重新获取" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"验证码已过期 请重新获取";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已绑定");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"该手机号已绑定";

                    //[AlertLabel AlertInfoWithText:@"该手机号已绑定" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"该手机号已绑定";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";
                    
                    //[AlertLabel AlertInfoWithText:@"无法访问个人中心" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"无法访问个人中心";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
        }
        else{
            
//            _errorView.hidden = YES;

            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
            
            return;
        }
    }
}


//验证码触发事件
- (void)verityBtnSender
{
    
    [_recentPhoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];
    [_verityNum.inputBlock resignFirstResponder];
    
    
    if ([_recentPhoneNum.inputBlock.text isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];
        
        return;
    }
    
    NSLog(@"*******************验证码被点击了");
    
//    [Time changBtn:self.verityBtn withSleepSecond:60];
    
    phonenumberOK=[CHECKFORMAT checkPhoneNumber:_recentPhoneNum.inputBlock.text];
    
    if (phonenumberOK) {
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"0" andMobilePhone:_recentPhoneNum.inputBlock.text andTimeout:10];
        
        NSLog(@"发送验证码啦");

    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"请输入正确的手机号码";

        //[AlertLabel AlertInfoWithText:@"请检查手机号码" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"请输入正确的手机号码";
        
        [_customView show];
    }
}


//导航条返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_recentPhoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];
    [_verityNum.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_recentPhoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];
    [_verityNum.inputBlock resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
