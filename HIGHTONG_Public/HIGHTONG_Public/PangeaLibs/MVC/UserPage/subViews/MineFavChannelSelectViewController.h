//
//  MineFavChannelSelectViewController.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/8.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface MineFavChannelSelectViewController : HT_FatherViewController

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) NSMutableArray *classArr;

@property (nonatomic, strong) NSMutableArray *itemArr;

@property (nonatomic, strong) NSMutableArray *allChannelArr;

@end
