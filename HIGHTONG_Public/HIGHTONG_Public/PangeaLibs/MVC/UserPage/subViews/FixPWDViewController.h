//
//  FixPWDViewController.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "ErrorAlertView.h"

@interface FixPWDViewController : HT_FatherViewController

//@property (nonatomic, strong)GeneralInputBlock *phoneNumField;
@property (nonatomic, strong)GeneralInputBlock *oldPWDField;
@property (nonatomic, strong)GeneralInputBlock *currentPWDField;
@property (nonatomic, strong)GeneralInputBlock *affirmPWDField;

@property (nonatomic, strong)UIButton          *backBtn;//返回按钮
@property (nonatomic, strong)ErrorAlertView    *errorView;

@property (nonatomic, strong)UIView            *currentPhoneNumBackView;
@property (nonatomic, strong)UILabel           *phoneNumLab;

@end
