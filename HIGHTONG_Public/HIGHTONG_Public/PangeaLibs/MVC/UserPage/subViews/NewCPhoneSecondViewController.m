//
//  NewCPhoneSecondViewController.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewCPhoneSecondViewController.h"

@interface NewCPhoneSecondViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>

@end

@implementation NewCPhoneSecondViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"newChangePhoneSecond"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"newChangePhoneSecond"];
    
    [_customView resignKeyWindow];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setNaviBarTitle:@"更换手机"];
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    [self setNaviBarRightBtn:nil];
    
    [self.view setBackgroundColor:App_white_color];
    
    _oldPhoneStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    
    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
    
    [self setUpView];
}

- (void)setUpView
{
    UILabel *displayLab = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:UIColorFromRGB(0x666666) andFont:App_font(13)];
    UILabel *phoneLab = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentCenter andTextColor:UIColorFromRGB(0x666666) andFont:App_font(13)];
    
    displayLab.text = @"更换后，下次登录可使用新手机号";
    
    NSString *newPhoneStr = [_oldPhoneStr stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    phoneLab.text = [NSString stringWithFormat:@"当前手机号为%@",newPhoneStr];
    
    [self.view addSubview:displayLab];
    [self.view addSubview:phoneLab];
    
    WS(wSelf);
    
    [displayLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wSelf.view);
        make.top.mas_equalTo(wSelf.view.mas_top).offset(64+10*kRateSize);
    }];
    
    [phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wSelf.view);
        make.top.mas_equalTo(displayLab.mas_bottom).offset(5*kRateSize);
    }];
    
    
    _phoneField = [[GeneralInputBlock alloc] initWithTitle:@"手机号：" andPlaceholderText:@"请输入11位新手机号" withVerityBtnName:@"获取验证码"];
    [_phoneField.verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
    _phoneField.inputBlock.delegate = self;
    _phoneField.userInteractionEnabled = YES;
    [self.view addSubview:_phoneField];
    [_phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.mas_equalTo(wSelf.view);
        make.height.mas_equalTo(50*kRateSize);
        make.top.mas_equalTo(phoneLab.mas_bottom).offset(10*kRateSize);
    }];
    
    
    //第一条分割线
    UIImageView *firstSideLine = [UIImageView new];
    [firstSideLine setBackgroundColor:App_line_color];
    [self.view addSubview:firstSideLine];
    [firstSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wSelf.view.mas_left).offset(20*kRateSize);
        make.right.equalTo(wSelf.view.mas_right).offset(-20*kRateSize);
        make.height.equalTo(@(0.5*kRateSize));
        make.top.equalTo(_phoneField.mas_bottom);
    }];
    
    
    //验证码输入框
    _verityField = [[GeneralInputBlock alloc] initWithTitle:@"验证码：" andPlaceholderText:@"请输入短信中的验证码"];
    _verityField.inputBlock.secureTextEntry = NO;
    _verityField.inputBlock.delegate = self;
    _verityField.userInteractionEnabled = YES;
    [self.view addSubview:_verityField];
    [_verityField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wSelf.view.mas_centerX);
        make.top.equalTo(firstSideLine.mas_bottom);
        make.size.equalTo(wSelf.phoneField);
    }];
    
    
    //第二条分割线
    UIImageView *secondSideLine = [UIImageView new];
    [secondSideLine setBackgroundColor:App_line_color];
    [self.view addSubview:secondSideLine];
    [secondSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(firstSideLine);
        make.top.equalTo(wSelf.verityField.mas_bottom);
        make.centerX.equalTo(wSelf.verityField.mas_centerX);
    }];
    
    
    _confirmBtn = [KaelTool setButtonPropertyWithBackgroundNormalImageName:@"btn_user_confirm_normal" andBackgroundHighlightedImageName:@"btn_user_next_step_pressed" andBackgroundSelectedImageName:nil andTarget:self andSelect:@selector(confirmBtnSender)];
    [self.view addSubview:_confirmBtn];
    [_confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf.verityField.mas_bottom).offset(30*kRateSize);
        make.width.equalTo(@(305*kRateSize));
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wSelf.view.mas_centerX);
    }];

}


#pragma mark 确定按钮触发事件
- (void)confirmBtnSender
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    //手机号码比对
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text]) {
        
        _phonenumberOK=YES;
    }else{
        
        _customView.messageStr = @"请输入正确的手机号码";
        
        [_customView show];
        
        return;
    }
    
    //验证码比对
    if ([CHECKFORMAT checkVerifyCode:_verityField.inputBlock.text]) {
        
        _authCodeOK=YES;
    }else{
        _customView.messageStr = @"请输入正确的验证码";
        
        [_customView show];
        
        return;
    }
    
    
    if (_phonenumberOK & _authCodeOK) {
        NSLog(@"我要开始换号码请求了哟");
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request authCodeAlterMobilePhoneWithMobilePhone:_phoneField.inputBlock.text andOldMobileAauthCode:_oldPhoneAuthCodeStr andAuthCode:_verityField.inputBlock.text andTimeout:KTimeout];
        
    }else{
        //检查账号和密码格式是否正确
        
        return;
    }

}


#pragma mark   获取验证码触发方法
- (void)getCoding:(UIButton*)sender
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    if ([_phoneField.inputBlock.text isEqualToString:@""]) {
        
        _customView.messageStr = @"请输入手机号码";
        [_customView show];
        
        return;
    }
    
    NSLog(@"+++++++++++++++++++++++发送验证码");
    
    if ([CHECKFORMAT checkPhoneNumber:_phoneField.inputBlock.text])
    {
        _phonenumberOK = YES;
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        [request NewauthCodeOfPhoneWithType:@"1" andMobilePhone:_phoneField.inputBlock.text andTimeout:KTimeout];
        
    }else{
        
        _customView.messageStr = @"请输入正确的手机号码";
        [_customView show];
    }
}

#pragma mark   请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:verityCode]) {
        
        if (status != 0) {
            
            _customView.messageStr = @"短信通道拥挤，请稍候再试！";
            
            [_customView show];
            
            return;
        }
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
                    [UserTime changBtn:_phoneField.verityBtn withSleepSecond:60];
                    [self.phoneField.verityBtn setTitleColor:UIColorFromRGB(0xcccccc) forState:UIControlStateNormal];
                    
                    [AlertLabel AlertInfoWithText:@"已发送至手机，请注意查收" andWith:self.view withLocationTag:0];
                    
                    break;
                }
                case -1:
                {
                    NSLog(@"类型错误");
                    
                    break;
                }
                case -3:
                {
                    NSLog(@"手机号错误");
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -4:{
                    NSLog(@"验证码发送次数超过当日限额");
                    
                    _customView.messageStr = @"验证码已发疯 请明日再试";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
                    _customView.messageStr = @"该手机号已注册";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"短信通道拥挤，请稍候再试！";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        }
    }
    else if ([type isEqualToString:authCodeAlterMobilePhone])
    {
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"手机号更换成功");
                    
                    [[NSUserDefaults standardUserDefaults] setObject:_phoneField.inputBlock.text forKey:@"mindUserName"];//自动登录的时候记住账号
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    [AlertLabel AlertInfoWithText:@"手机号更换成功！" andWith:self.view withLocationTag:0];
                
                    [self performSelector:@selector(goBack) withObject:nil afterDelay:2.5];
                    
                    break;
                }
                case -3:{
                    NSLog(@"手机号错误");
                    
                    _customView.messageStr = @"请输入正确的手机号码";
                    
                    [_customView show];
                    
                    break;
                }
                case -5:{
                    NSLog(@"手机号已经注册");
                    
                    _customView.messageStr = @"手机号已经注册";
                    
                    [_customView show];
                }
                case -6:{
                    NSLog(@"原验证码错误或者超时效");
                    
                    _customView.messageStr = @"原验证码错误或者超时效";
                    
                    [_customView show];
                    
                    break;
                }
                case -7:{
                    NSLog(@"验证码错误或者超时效");
                    
                    _customView.messageStr = @"验证码已过期 请重新获取";
                    
                    [_customView show];
                    
                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
                    _customView.messageStr = @"无法访问个人中心";
                    
                    [_customView show];
                    
                    break;
                }
                    
                default:
                    break;
            }
        }
    }
}


#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_phoneField.inputBlock resignFirstResponder];
    [_verityField.inputBlock resignFirstResponder];
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        
        _customView.hidden = YES;
    }
    
    return;
}

//导航条返回按钮
- (void)goBack
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
