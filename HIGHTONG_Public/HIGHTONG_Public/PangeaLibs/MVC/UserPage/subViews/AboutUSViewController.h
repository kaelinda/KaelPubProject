//
//  AboutUSViewController.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/18.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface AboutUSViewController : HT_FatherViewController


@property (nonatomic,copy)NSString *urlLink;
@property (nonatomic,copy)NSString *Ktitle;

@end
