//
//  FixPWDViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/10.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FixPWDViewController.h"
#import "CHECKFORMAT.h"
#import "VerityAlertView.h"
#import "Time.h"
#import "AlertLabel.h"
#import "LoginViewController.h"
#import "CustomAlertView.h"

@interface FixPWDViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    BOOL phonenumberOK;
    BOOL oldPWDOK;
    BOOL newPWDOk;
    BOOL ifEqual;
}

@property (nonatomic, strong)CustomAlertView *customView;

@end

@implementation FixPWDViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"FixPWDPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"FixPWDPage"];
    
    [_customView resignKeyWindow];
    
//    _customView = nil;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    [self setNaviBarTitle:@"修改密码"];
    self.view.backgroundColor = App_background_color;
    
    WS(wss);
    
    
//    _phoneNumField = [[GeneralInputBlock alloc] initWithTitle:@"当前账号：" andPlaceholderText:nil];
//    _phoneNumField.titleLab.textColor = UIColorFromRGB(0x666666);
//    _phoneNumField.titleLab.font = App_font(13);
//    _phoneNumField.inputBlock.delegate = self;
//    _phoneNumField.userInteractionEnabled = NO;
//    _phoneNumField.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
//    _phoneNumField.inputBlock.textColor = UIColorFromRGB(0x666666);
//    [self.view addSubview:_phoneNumField];
//    [_phoneNumField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.height.equalTo(@(44*kRateSize));
//        make.top.equalTo(wss.view.mas_top).offset(topHigh);
//    }];
//    [_phoneNumField setBackgroundColor:UIColorFromRGB(0xffffff)];
    
    
    //显示当前账号的view
    _currentPhoneNumBackView = [UIView new];
    [_currentPhoneNumBackView setBackgroundColor:App_white_color];
    [self.view addSubview:_currentPhoneNumBackView];
    [_currentPhoneNumBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.height.equalTo(@(44*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(wss.view.mas_top).offset(topHigh);
    }];
    
    
    _phoneNumLab = [[UILabel alloc] init];
    _phoneNumLab.font = App_font(13);
    _phoneNumLab.textColor = UIColorFromRGB(0x666666);
    _phoneNumLab.textAlignment = NSTextAlignmentLeft;
    NSString *tempStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    _phoneNumLab.text = [NSString stringWithFormat:@"当前账号：%@",tempStr];
    [_currentPhoneNumBackView addSubview:_phoneNumLab];
    [_phoneNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.currentPhoneNumBackView.mas_top).offset(18*kRateSize);
        make.left.equalTo(wss.currentPhoneNumBackView.mas_left).offset(20*kRateSize);
        make.right.equalTo(wss.currentPhoneNumBackView.mas_right);
        make.bottom.equalTo(wss.currentPhoneNumBackView.mas_bottom);
    }];
    
    //原密码
    _oldPWDField = [[GeneralInputBlock alloc] initWithTitle:@"原密码：    " andPlaceholderText:@"请输入原始密码"];
    _oldPWDField.inputBlock.delegate = self;
    _oldPWDField.inputBlock.secureTextEntry = YES;
    _oldPWDField.userInteractionEnabled = YES;
    [self.view addSubview:_oldPWDField];
    [wss.oldPWDField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.currentPhoneNumBackView);
        make.top.equalTo(wss.currentPhoneNumBackView.mas_bottom);
        make.centerX.equalTo(wss.currentPhoneNumBackView.mas_centerX);
    }];
    
    
    //分割线一
    UIImageView *fSideLine = [UIImageView new];
    [fSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:fSideLine];
    [fSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@generalWidth);
        make.height.equalTo(@(0.5*kRateSize));
        make.top.equalTo(wss.oldPWDField.mas_bottom);
        make.centerX.equalTo(wss.oldPWDField.mas_centerX);
    }];
    
    
    //原来项目中承载新密码和确认密码的view
//    UIView *pwdBack = [UIView new];
//    [pwdBack setBackgroundColor:[UIColor clearColor]];
//    [self.view addSubview:pwdBack];
//    [pwdBack mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(@(89*kRateSize));
//        make.width.equalTo(_oldPWDField.mas_width);
//        make.centerX.equalTo(_oldPWDField.mas_centerX);
//        make.top.equalTo(_oldPWDField.mas_bottom).offset(fiveGap);
//    }];
    
    
    //新密码
    _currentPWDField = [[GeneralInputBlock alloc] initWithTitle:@"新密码：    " andPlaceholderText:@"6-16位数字、字母或字符"];
    _currentPWDField.inputBlock.delegate = self;
    _currentPWDField.inputBlock.secureTextEntry = YES;
    _currentPWDField.userInteractionEnabled = YES;
    [self.view addSubview:_currentPWDField];
    [_currentPWDField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fSideLine.mas_bottom);
        make.size.equalTo(wss.oldPWDField);
        make.centerX.equalTo(wss.oldPWDField.mas_centerX);
    }];
    
    
    //分割线二
    UIImageView *line = [UIImageView new];
    //    [line setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
    [line setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(fSideLine);
        make.top.equalTo(wss.currentPWDField.mas_bottom);
        make.centerX.equalTo(wss.currentPWDField.mas_centerX);
    }];

    
    //确认密码
    _affirmPWDField = [[GeneralInputBlock alloc] initWithTitle:@"确认密码：" andPlaceholderText:@"请再次输入"];
    _affirmPWDField.inputBlock.delegate = self;
    _affirmPWDField.inputBlock.secureTextEntry = YES;
    _affirmPWDField.userInteractionEnabled = YES;
    [self.view addSubview:_affirmPWDField];
    [_affirmPWDField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wss.currentPWDField);
        make.centerX.equalTo(wss.currentPWDField.mas_centerX);
        make.top.equalTo(line.mas_bottom);
    }];
    
    
//    _errorView = [[ErrorAlertView alloc] init];
//    [self.view addSubview:_errorView];
//    _errorView.hidden = YES;
//    [_errorView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.top.equalTo(_affirmPWDField.mas_bottom);
//        make.height.equalTo(@(25*kRateSize));
//    }];

    
    
    UIButton *finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [finishBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_normal"] forState:UIControlStateNormal];
    [finishBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_common_commit_pressed"] forState:UIControlStateHighlighted];
    //[finishBtn setImage:[UIImage imageNamed:@"btn_user_common_commit_normal"] forState:UIControlStateNormal];
    [finishBtn addTarget:self action:@selector(finishSenderDown) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:finishBtn];
    [finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@generalBtnWidth);
        make.height.equalTo(@(40*kRateSize));
        make.top.equalTo(wss.affirmPWDField.mas_bottom).offset(15*kRateSize);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {
        //        [_customView resignKeyWindow];
        
        //        _customView = nil;
        
        _customView.hidden = YES;
    }
    return;
}


- (void)finishSenderDown
{
    
//    [_phoneNumField.inputBlock resignFirstResponder];
    [_oldPWDField.inputBlock resignFirstResponder];
    [_currentPWDField.inputBlock resignFirstResponder];
    [_affirmPWDField.inputBlock resignFirstResponder];
    
    //手机号码比对
//    if ([CHECKFORMAT checkPhoneNumber:_phoneNumField.inputBlock.text]) {
//        
//        phonenumberOK = YES;
//    }else{
//        
//        [AlertLabel AlertInfoWithText:@"请检查手机号码" andWith:self.view withLocationTag:0];
//        
//        return;
//    }
    
    //旧密码
    if ([CHECKFORMAT checkPassword:_oldPWDField.inputBlock.text]) {
        
        oldPWDOK = YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"原密码错误";
        
        //[AlertLabel AlertInfoWithText:@"密码不符合规范" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"原密码错误";
        
        [_customView show];
        
        return;
    }
    
    //新密码
    if ([CHECKFORMAT checkPassword:_currentPWDField.inputBlock.text]) {
        
        newPWDOk = YES;
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"新密码错误";
        
        //[AlertLabel AlertInfoWithText:@"密码不符合规范" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"新密码错误";
        
        [_customView show];
        
        return;
    }
    
    //确认密码
    if ([CHECKFORMAT checkPassword:_affirmPWDField.inputBlock.text]&&[_affirmPWDField.inputBlock.text isEqualToString:_currentPWDField.inputBlock.text]) {
        ifEqual = YES;
        
    }else{
        
//        _errorView.hidden = NO;
//        
//        _errorView.alertLab.text = @"两次密码不一样 请重新输入";
        
        //[AlertLabel AlertInfoWithText:@"两次密码输入不一致" andWith:self.view withLocationTag:0];
        
        _customView.messageStr = @"两次密码不一样 请重新输入";
        
        [_customView show];

        return;
    }
    
    
    if (oldPWDOK&newPWDOk&ifEqual) {
        //发起请求
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request NewchangeUserPwdWithOldPassWord:_oldPWDField.inputBlock.text andNewPassWord:_currentPWDField.inputBlock.text andTimeout:10];
        
//        [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在注册请稍后"];
        
    }else{
        //检查账号和密码格式是否正确
        
        return;
    }
}



- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:changeUserPwd]) {
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:
                {
                    NSLog(@"获取成功");
                    
//                    _errorView.hidden = YES;
                    
//                    [[NSUserDefaults standardUserDefaults] setObject:_phoneNumField.inputBlock.text forKey:@"mindUserName"];//自动登录的时候记住账号
                    
                    //[[NSUserDefaults standardUserDefaults] setObject:_currentPWDField.inputBlock.text forKey:@"mindPWD"];//自动登录的时候记住密码
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"mindPWD"];//自动登录的时候记住密码

                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IsLogin"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [AlertLabel AlertInfoWithText:@"密码更换成功！" andWith:self.view withLocationTag:0];
    
                    
                    [self performSelector:@selector(goToLogin) withObject:nil afterDelay:2];
                    
                    break;
                }
                case -1:
                {
                    NSLog(@"-1 失败");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"修改密码失败";
                    
                }
                case -2:
                {
                    NSLog(@"用户未登录或者登录超时");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"登录超时 请重试！";

                    //[AlertLabel AlertInfoWithText:@"登录超时 请重试！" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"登录超时 请重试！";
                    
                    [_customView show];

                    break;
                }
                case -3:
                {
                    NSLog(@"原密码错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"原密码错误";
                    
                    //[AlertLabel AlertInfoWithText:@"原密码错误" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"原密码错误";
                    
                    [_customView show];
                    
                    break;
                }

                case -4:{
                    NSLog(@"新错误错误");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"新密码错误";
                    
                    //[AlertLabel AlertInfoWithText:@"新密码错误" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"新密码错误";
                    
                    [_customView show];

                    break;
                }
                case -9:{
                    NSLog(@"其他异常");
                    
//                    _errorView.hidden = NO;
//                    
//                    _errorView.alertLab.text = @"无法访问个人中心";
                    
                    //[AlertLabel AlertInfoWithText:@"无法访问个人中心" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"无法访问个人中心";
                    
                    [_customView show];

                    break;
                }
                    
                default:
                    break;
            }
            
        }else{
            
//            [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
            
            return;
        }
    }
}


#pragma mark textfield Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [_phoneNumField.inputBlock resignFirstResponder];
    [_oldPWDField.inputBlock resignFirstResponder];
    [_currentPWDField.inputBlock resignFirstResponder];
    [_affirmPWDField.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    [_phoneNumField.inputBlock resignFirstResponder];
    [_oldPWDField.inputBlock resignFirstResponder];
    [_currentPWDField.inputBlock resignFirstResponder];
    [_affirmPWDField.inputBlock resignFirstResponder];
}

- (void)goToLogin
{
    [self.navigationController pushViewController:[[LoginViewController alloc] init] animated:YES];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
