//
//  NewCPhoneSecondViewController.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralInputBlock.h"
#import "CustomAlertView.h"
#import "KaelTool.h"
#import "CHECKFORMAT.h"
#import "UserTime.h"

@interface NewCPhoneSecondViewController : HT_FatherViewController
{
    NSString *_oldPhoneStr;
    BOOL _phonenumberOK;
    BOOL _authCodeOK;
}

@property (nonatomic, strong) GeneralInputBlock *phoneField;
@property (nonatomic, strong) GeneralInputBlock *verityField;
@property (nonatomic, strong) CustomAlertView   *customView;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIButton *confirmBtn;
@property (nonatomic, copy) NSString *oldPhoneAuthCodeStr;


@end
