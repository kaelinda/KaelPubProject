//
//  MineScheduleViewController.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/15.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "GeneralPromptView.h"

@interface MineScheduleViewController : HT_FatherViewController
{
//    BOOL ISNoting;//清空按钮是否被点击
    
    BOOL isEditing;//是否显示cell右上角的取消预约按钮

    NSInteger num;
    
    NSString *cancelEventID;
    
    BOOL _selAllBtnIsClick;//标记全选按钮是否被点击
}

@property (nonatomic, strong)NSMutableArray *dataArr;//数据源

@property (nonatomic, strong)UIButton *backBtn;//返回按钮
@property (nonatomic, strong)UIButton *editBtn;//编辑按钮

@property (nonatomic, strong)GeneralPromptView *prompt;//暂无记录

@property (nonatomic, strong)UITableView *table;

@property (nonatomic, strong)NSMutableArray *deleteArr;//要删除的数组

@end
