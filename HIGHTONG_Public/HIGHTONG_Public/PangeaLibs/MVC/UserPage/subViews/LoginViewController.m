//
//  LoginViewController.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/3.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "LoginViewController.h"
#import "MyGesture.h"
#import "CHECKFORMAT.h"
#import "RegisterViewController.h"
#import "ForgetPwdViewController.h"
#import "DejalActivityView.h"
#import "UIView+Extension.h"
#import "GeneralInputBlock.h"
#import "UserCollectCenter.h"
#import "UserPlayRecorderCenter.h"
#import "AlertLabel.h"
#import "MineViewcontrol.h"
#import "CalculateTextWidth.h"
#import "UserScheduleCenter.h"
#import "MineViewcontrol.h"
#import "SAIInformationManager.h"
#import "CustomAlertView.h"
#import "HTYJUserCenter.h"

@interface LoginViewController ()<UITextFieldDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    BOOL userNameOK;//账号规格合格
    BOOL PWDOK;//密码规格合格
}

@property (nonatomic,strong) UIButton          *backBtn;//返回按钮
@property (nonatomic,strong) UIButton          *registerBtn;//注册按钮
@property (nonatomic,strong) GeneralInputBlock *phoneNum;//账号
@property (nonatomic,strong) GeneralInputBlock *pwdNum;//密码


@property (nonatomic,strong) UIView            *errerBack;
@property (nonatomic,strong) UIView            *clearBackView;//整体透明背景图
@property (nonatomic,strong) UIButton          *LoginBtn;//登陆按钮
@property (nonatomic,strong) UIImageView       *errorImage;//错误提示图片
@property (nonatomic,strong) UILabel           *errorLabel;//错误提示内容
@property (nonatomic,strong) UILabel           *forgetPWDLabel;//忘记密码 label
@property (nonatomic,strong) UILabel           *lineLabel;//线
@property (nonatomic,strong) CustomAlertView   *customView;

@end

@implementation LoginViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _phoneNum.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    _pwdNum.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"];
    
//    NSString *userString = _phoneNum.inputBlock.text;
//    
//    NSString *pwdString = _pwdNum.inputBlock.text;

    
    if ([_phoneNum.inputBlock.text isEqualToString:@""]&&[_pwdNum.inputBlock.text isEqualToString:@""]) {
        
        _phoneNum.placeholderLab.hidden = NO;
        _pwdNum.placeholderLab.hidden = NO;
    }else if (_phoneNum.inputBlock.text.length>0 && _pwdNum.inputBlock.text.length == 0){
        
        _phoneNum.placeholderLab.hidden = YES;
        _pwdNum.placeholderLab.hidden = NO;
    }else{
        
        _phoneNum.placeholderLab.hidden = YES;
        _pwdNum.placeholderLab.hidden = YES;
    }
    NSLog(@"测试1%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"]);
    NSLog(@"测试1%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"]);
    
//    NSString *str1 = _phoneNum.placeholderLab.text;
//    
//    NSString *str2 = _pwdNum.placeholderLab.text;
    
    [MobCountTool pushInWithPageView:@"LoginPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"LoginPage"];
    
    [_customView resignKeyWindow];
//    _customView = nil;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(toMinePage)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"登录"];
    
    _registerBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"注册" target:self action:@selector(goToRegister)];
    [self setNaviBarRightBtn:_registerBtn];
    
    self.view.backgroundColor = App_white_color;
    
    WS(wss);

    //账号输入框
    _phoneNum = [[GeneralInputBlock alloc] initWithTitle:@"账号：" andPlaceholderText:@"请输入手机号"];
    _phoneNum.inputBlock.secureTextEntry = NO;
    _phoneNum.inputBlock.delegate = self;
    _phoneNum.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
    _phoneNum.userInteractionEnabled = YES;
    [self.view addSubview:_phoneNum];
    [_phoneNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left);
        make.right.equalTo(wss.view.mas_right);
        make.top.equalTo(wss.view.mas_top).offset(64+10*kRateSize);
        make.height.equalTo(@(44*kRateSize));
    }];
    
    
    //第一条分割线
    UIImageView *firstSideLine = [UIImageView new];
    [firstSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:firstSideLine];
    [firstSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(280*kRateSize));
        make.height.equalTo(@(0.5*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(_phoneNum.mas_bottom);
    }];
    
    //密码输入框
//    _pwdNum = [[GeneralInputBlock alloc] initWithTitle:@"密码：" andPlaceholderText:@"6-16位数字、字母或字符"];
   
    _pwdNum = [[GeneralInputBlock alloc] initWithTitle:@"密码：" andPlaceholderText:@"6-16位数字、字母或字符" withForgetPWDBtnName:@"忘记密码？"];
    [_pwdNum.verityBtn addTarget:self action:@selector(forgetPWDBtnSenderDown:) forControlEvents:UIControlEventTouchUpInside];
    
//    _pwdNum.verityBtn.titleLabel.font = App_font(11);
    
    _pwdNum.inputBlock.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"];
    _pwdNum.inputBlock.delegate = self;
    _pwdNum.inputBlock.secureTextEntry = YES;
    _pwdNum.userInteractionEnabled = YES;
    [self.view addSubview:_pwdNum];
    [_pwdNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_phoneNum);
        make.centerX.equalTo(_phoneNum.mas_centerX);
        make.top.equalTo(firstSideLine.mas_bottom);//.offset(fiveGap);
    }];
    
//    [_pwdNum.inputBlock mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wss.pwdNum.titleLab.mas_right).offset(5*kRateSize);
//        make.height.equalTo(wss.pwdNum.mas_height);
//        make.centerY.equalTo(wss.pwdNum.mas_centerY);
//        make.right.equalTo(wss.pwdNum.mas_right).offset(-70*kRateSize);
//    }];
    
    //忘记密码
//    _forgetPWDLabel = [UILabel new];
//    _forgetPWDLabel.textAlignment = NSTextAlignmentRight;
//    _forgetPWDLabel.text = @"忘记密码？";
//    _forgetPWDLabel.font = App_font(11);
//    _forgetPWDLabel.textColor = App_selected_color;
//    _forgetPWDLabel.userInteractionEnabled = YES;
//    
//    CGSize forgetPwdSize = [CalculateTextWidth sizeWithText:_forgetPWDLabel.text font:App_font(11)];
//    int forgetWidth = forgetPwdSize.width;
//    
//    [_pwdNum addSubview:_forgetPWDLabel];
//    [_forgetPWDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(_pwdNum.mas_right).offset(-20*kRateSize);
//        make.centerY.equalTo(_pwdNum.mas_centerY);
//        make.width.equalTo(@(forgetWidth));
//        make.height.equalTo(_pwdNum.mas_height);
//    }];
//    
//    
//    MyGesture *forgetTap = [[MyGesture alloc] init];
//    forgetTap.numberOfTapsRequired = 1;
//    [forgetTap addTarget:self action:@selector(forgetPWDGesture:)];
//    [_forgetPWDLabel addGestureRecognizer:forgetTap];

    
    //第二条分割线
    UIImageView *secondSideLine = [UIImageView new];
    [secondSideLine setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    [self.view addSubview:secondSideLine];
    [secondSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(firstSideLine);
        make.top.equalTo(_pwdNum.mas_bottom);
        make.centerX.equalTo(_pwdNum.mas_centerX);
    }];
    
    //错误提示部分
//    _errerBack = [UIView new];
//    _errerBack.hidden = YES;
//    [_errerBack setBackgroundColor:[UIColor clearColor]];
//    [self.view addSubview:_errerBack];
//    [_errerBack mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(wss.view.mas_width);
//        make.centerX.equalTo(wss.view.mas_centerX);
//        make.top.equalTo(_pwdNum.mas_bottom);
//        make.height.equalTo(@(25*kRateSize));
//    }];
//    
//    
//    _errorImage = [UIImageView new];
//    _errorImage.image = [UIImage imageNamed:@"cuowu"];
//    [_errerBack addSubview:_errorImage];
//    [_errorImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@(19*kRateSize));
//        make.height.equalTo(@(17*kRateSize));
//        make.left.equalTo(_errerBack.mas_left).offset(18*kRateSize);
//        make.centerY.equalTo(_errerBack.mas_centerY);
//    }];
//    
//    UILabel *tipLab = [UILabel new];
//    tipLab.textAlignment = NSTextAlignmentLeft;
//    tipLab.font = [UIFont systemFontOfSize:12];
//    tipLab.textColor = UIColorFromRGB(0x666666);
//    tipLab.text = @"错误提示：";
//    CGSize tipSize = [CalculateTextWidth sizeWithText:tipLab.text font:[UIFont systemFontOfSize:12]];
//    CGFloat tipWidth = tipSize.width;
//    [_errerBack addSubview:tipLab];
//    [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_errorImage.mas_right).offset(10*kRateSize);
//        make.width.equalTo(@(tipWidth*kRateSize));
//        make.top.equalTo(_errorImage.mas_top);
//        make.height.equalTo(_errorImage.mas_height);
//    }];
//    
//    _errorLabel = [UILabel new];
//    _errorLabel.backgroundColor = [UIColor clearColor];
//    _errorLabel.textAlignment = NSTextAlignmentLeft;
//    _errorLabel.textColor = UIColorFromRGB(0x666666);
//    _errorLabel.font = [UIFont systemFontOfSize:12];
//    [_errerBack addSubview:_errorLabel];
//    [_errorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(tipLab.mas_right);
//        make.height.equalTo(tipLab.mas_height);
//        make.width.equalTo(@(150*kRateSize));
//        make.centerY.equalTo(tipLab.mas_centerY);
//    }];

    
    
    //登录按钮
    _LoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_normal"] forState:UIControlStateNormal];
    [_LoginBtn setBackgroundImage:[UIImage imageNamed:@"btn_user_login_pressed"] forState:UIControlStateHighlighted];
    [_LoginBtn addTarget:self action:@selector(LoginSender:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_LoginBtn];
    [_LoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(305*kRateSize));
        make.height.equalTo(@(40*kRateSize));
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(secondSideLine.mas_bottom).offset(15*kRateSize);
    }];
    
    
    _customView = [[CustomAlertView alloc] initWithTitle:@"提示" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];

}


//登录按钮触发事件
- (void)LoginSender:(UIButton *)sender
{
    
    [_phoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];

    
    //账号对比
    //if ([CHECKFORMAT checkUserName:_phoneNum.inputBlock.text]&&(_phoneNum.inputBlock.text.length>0))
    
    if ([CHECKFORMAT checkPhoneNumber:_phoneNum.inputBlock.text]&&(_phoneNum.inputBlock.text.length>0))
    {
        userNameOK = YES;
        
    }else{
//        _errerBack.hidden = NO;
//        _errorLabel.text = @"账号输入不正确";
        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            _errerBack.hidden = YES;
//        });

        _customView.messageStr = @"账号输入不正确";
        [_customView show];
        
        return;
    }
    //密码对比
    if ([CHECKFORMAT checkPassword:_pwdNum.inputBlock.text]) {
        PWDOK = YES;
        
    }else{
//        _errerBack.hidden = NO;
//        _errorLabel.text = @"密码输入不正确";
        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            _errerBack.hidden = YES;
//        });
        
        _customView.messageStr = @"密码输入不正确";
        [_customView show];
        
        return;
    }
    
    if (userNameOK&PWDOK) {
        
//        _errerBack.hidden = YES;
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        NSLog(@"登录时的 账号密码： %@  %@",_phoneNum.inputBlock.text,_pwdNum.inputBlock.text);
        
        [request mobileLoginWithMobilePhone:_phoneNum.inputBlock.text andPassWord:_pwdNum.inputBlock.text  andTimeout:10];

        [DejalBezelActivityView activityViewForView:self.view withLabel:@"正在登录 请稍后"];
        
        
    }else{

        [AlertLabel AlertInfoWithText:@"请检查网络设置" andWith:self.view withLocationTag:0];
        return;
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    //[DejalActivityView removeView];
    
    [DejalBezelActivityView removeView];
    
    if ([type isEqualToString:@"mobileLogin"]) {
        
        NSLog(@"返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);

        NSLog(@"%ld",(long)[[result objectForKey:@"ret"] integerValue]);
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    if (status == 0) {
                        
                        NSLog(@"手机-------------------登陆成功");
                        
                        //                    _errerBack.hidden = YES;
//                        [[NSUserDefaults standardUserDefaults]setObject:[SAIInformationManager serviceTimeTransformStr] forKey:@"ONLINE"];
//                        [[NSUserDefaults standardUserDefaults]synchronize];
//                        [SAIInformationManager OFFLine];
//                        
//                        double delayInSeconds = 2.0;
//                        dispatch_time_t PopTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds*NSEC_PER_SEC);
//                        dispatch_after(PopTime, dispatch_get_main_queue(), ^{
//                            
//                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//                            [dic setValue:@"login" forKey:@"poststr"];
//                            [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_POST_USER_ACTION_DATA" object:dic];//发送通知要上传数据
//                            
//                        });
//                        //end
                        
                        
                        [HTYJUserCenter saveUserLoginProfileWithDic:result andUserAccount:_phoneNum.inputBlock.text andUserPassword:_pwdNum.inputBlock.text];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"AUTOLOGIN" object:nil];//自动登录的通知
                        
                        
//                        //采集用户采集的上下线（登录之后，用户上线）
//                        [SAIInformationManager ONLine];
//                        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"resign" forKey:@"poststr"];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_POST_USER_ACTION_DATA" object:dic];//发送通知要上传数据
                        
                        
                        [self performSelector:@selector(toMinePage) withObject:nil afterDelay:0.5];

                    }
                    
                    break;
                }
                case -1:{
                    NSLog(@"手机号不存在");
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"用户不存在";
                    
//                    [AlertLabel AlertInfoWithText:@"用户不存在" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"用户不存在";
                    [_customView show];

                    break;
                }
                case -2:{
                    NSLog(@"密码不正确");
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"密码不正确";
                    
//                    [AlertLabel AlertInfoWithText:@"密码不正确" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"密码不正确";
                    [_customView show];

                    break;
                }
                case -3:{
                    NSLog(@"超过绑定设备上限");
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"超过绑定设备上限";
                    
//                    [AlertLabel AlertInfoWithText:@"超过绑定设备上限" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"超过绑定设备上限";
                    [_customView show];

                    break;
                }
                case -5:{
                    NSLog(@"挑战字不存在或者挑战字过期");
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"登录超时 请重试！";

//                    [AlertLabel AlertInfoWithText:@"登录超时 请重试！" andWith:self.view withLocationTag:0];

                    _customView.messageStr = @"登录超时 请重试！";
                    [_customView show];

                    break;
                }
                case -9:
                {
                    NSLog(@"其他异常");
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"登录超时 请重试！";
                    
//                    [AlertLabel AlertInfoWithText:@"登录超时 请重试！" andWith:self.view withLocationTag:0];
                    
                    _customView.messageStr = @"登录超时 请重试！";
                    [_customView show];

                    break;
                    
                }
                default:
                    
//                    _errerBack.hidden = NO;
//                    _errorLabel.text = @"登录超时 请重试！";

                    
                    [AlertLabel AlertInfoWithText:@"登录超时 请重试！" andWith:self.view withLocationTag:0];

                    break;
            }
            
        }
    }

    else{
        [AlertLabel AlertInfoWithText:@"请检查网络连接" andWith:self.view withLocationTag:0];
    }
}


- (void)toMinePage
{
    if (_isPlayerEnter) {
        
              
        [self.navigationController popViewControllerAnimated:YES];
    }else
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_customView) {

//        [_customView resignKeyWindow];
//        _customView = nil;
        
        _customView.hidden = YES;
    }

    return;
}


//注册按钮触发事件
- (void)goToRegister
{
    RegisterViewController *regist = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:regist animated:YES];
}


//忘记密码手势事件
//- (void)forgetPWDGesture:(MyGesture *)Sender
//{
//    [self.navigationController pushViewController:[[ForgetPwdViewController alloc] init] animated:YES];
//}

- (void)forgetPWDBtnSenderDown:(UIButton *)sender
{
    [self.navigationController pushViewController:[[ForgetPwdViewController alloc] init] animated:YES];

}


#pragma mark textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [_phoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [_phoneNum.inputBlock resignFirstResponder];
    [_pwdNum.inputBlock resignFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil) {
//        self.view = nil;
//    }
}


@end
