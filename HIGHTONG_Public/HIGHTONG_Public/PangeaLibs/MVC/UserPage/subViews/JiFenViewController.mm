//
//  JiFenViewController.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/30.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "JiFenViewController.h"
#import "CalculateTextWidth.h"

@interface JiFenViewController ()
{
    UIButton *backBtn;//返回按钮
    //UILabel *scoreLab;//用户具体的积分
}

@property (nonatomic, strong)UILabel *dayLab;
@property (nonatomic, strong)UILabel *scoreLab;


@end

@implementation JiFenViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _scoreLab.text = ([[NSUserDefaults standardUserDefaults] objectForKey:@"Userscore"] == NULL ?  @"0 积分" : [NSString stringWithFormat:@"%@积分",[[NSUserDefaults standardUserDefaults]objectForKey:@"Userscore"]]);
    
    //scoreLab.text = [NSString stringWithFormat:@"%@ 积分",[[NSUserDefaults standardUserDefaults]objectForKey:@"Userscore"]];

    [MobCountTool pushInWithPageView:@"JiFenPage"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"JiFenPage"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarTitle:@"我的积分"];
    backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:backBtn];
    [self setNaviBarRightBtn:nil];
    
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
    [self makeTheMainPartUI];
}

- (void)makeTheMainPartUI
{
    WS(wss);
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, topHigh, self.view.frame.size.width, 89*kRateSize)];
    topView.backgroundColor = UIColorFromRGB(0xffffff);
    [self.view addSubview:topView];
    
    
//    UIView *sepLine = [UIView new];
//    sepLine.backgroundColor = UIColorFromRGB(0xb3b3b3);
    
    UIImageView *sepLine = [UIImageView new];
//    [sepLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
    [sepLine setBackgroundColor:App_line_color];
    [topView addSubview:sepLine];
    [sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView.mas_left).offset(fiveGap);
        make.right.equalTo(topView.mas_right).offset(-fiveGap);
        make.centerY.equalTo(topView.mas_centerY);
        make.height.equalTo(@(1*kRateSize));
        
    }];
    
    
    UILabel *signLab = [UILabel new];
    UILabel *getLab = [UILabel new];
    signLab.text = @"已签到：";
    getLab.text = @"已领取：";
    signLab.textColor = UIColorFromRGB(0x333333);
    getLab.textColor = UIColorFromRGB(0x333333);
    signLab.font = [UIFont systemFontOfSize:15*kRateSize];
    getLab.font = [UIFont systemFontOfSize:15];
    signLab.textAlignment = NSTextAlignmentLeft;
    getLab.textAlignment = NSTextAlignmentLeft;
    [topView addSubview:signLab];
    [topView addSubview:getLab];
    
    CGSize labSize = [CalculateTextWidth sizeWithText:signLab.text font:[UIFont systemFontOfSize:15]];
    CGFloat labWidth = labSize.width;
    
    [signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView.mas_left).offset(23*kRateSize);
        make.width.equalTo(@(labWidth*kRateSize));
        make.top.equalTo(topView.mas_top).offset(14*kRateSize);
    }];
    
    [getLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(signLab);
        make.left.equalTo(signLab.mas_left);
        make.top.equalTo(sepLine.mas_bottom).offset(14*kRateSize);
    }];

    
    UIImageView *dayImg = [UIImageView new];
    [dayImg setImage:[UIImage imageNamed:@"bj-qiandao_jifen"]];
    [topView addSubview:dayImg];
    [dayImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(signLab.mas_right).offset(26*kRateSize);
        make.width.equalTo(@(80*kRateSize));
        make.height.equalTo(@(25*kRateSize));
        make.centerY.equalTo(signLab.mas_centerY);
    }];
    
    
    UIImageView *scoreImg = [UIImageView new];
    [scoreImg setImage:[UIImage imageNamed:@"bj-qiandao_jifen"]];
    [topView addSubview:scoreImg];
    [scoreImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(dayImg);
        make.centerY.equalTo(getLab.mas_centerY);
        make.left.equalTo(dayImg.mas_left);
    }];
    
    
    _dayLab = [UILabel new];
    _scoreLab = [UILabel new];

    _dayLab.text = ([[NSUserDefaults standardUserDefaults] objectForKey:@"loginTimes"] == NULL ?  @"0 天" :[NSString stringWithFormat:@"%@天",[[NSUserDefaults standardUserDefaults] objectForKey:@"loginTimes"]]);
    _scoreLab.text = ([[NSUserDefaults standardUserDefaults] objectForKey:@"Userscore"] == NULL ?  @"0 积分" : [NSString stringWithFormat:@"%@积分",[[NSUserDefaults standardUserDefaults]objectForKey:@"Userscore"]]);
    
    _dayLab.textColor = UIColorFromRGB(0xff9c00);
    _scoreLab.textColor = UIColorFromRGB(0xff9c00);
    
    _dayLab.font = [UIFont systemFontOfSize:15];
    _scoreLab.font = [UIFont systemFontOfSize:15];
    
    _dayLab.textAlignment = NSTextAlignmentCenter;
    _scoreLab.textAlignment = NSTextAlignmentCenter;
    
    [dayImg addSubview:_dayLab];
    [scoreImg addSubview:_scoreLab];
    
    [_dayLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(dayImg);
        make.center.equalTo(dayImg);
    }];
    
    [_scoreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(scoreImg);
        make.center.equalTo(scoreImg);
    }];
    
    
    
    UILabel *midLab = [UILabel new];
    midLab.text = @"如何获取积分";
    midLab.textColor = UIColorFromRGB(0x666666);
    midLab.font = [UIFont systemFontOfSize:14];
    midLab.backgroundColor = [UIColor clearColor];
    midLab.textAlignment = NSTextAlignmentCenter;

    CGSize midLabSize = [[CalculateTextWidth alloc] sizeWithText:midLab.text font:midLab.font];
    CGFloat midLabW = midLabSize.width;
    CGFloat midLabH = midLabSize.height;
    
    [self.view addSubview:midLab];
    [midLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(midLabW*kRateSize));
        make.height.equalTo(@(midLabH*kRateSize));
        make.top.equalTo(topView.mas_bottom).offset(21*kRateSize);
        make.centerX.equalTo(wss.view.mas_centerX);
    }];
    
    
    UIImageView *leftLine = [UIImageView new];
    //[leftLine setBackgroundColor:UIColorFromRGB(0xb3b3b3)];
    
    [leftLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
    [self.view addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left);
        make.right.equalTo(midLab.mas_left).offset(-11*kRateSize);
        make.centerY.equalTo(midLab.mas_centerY);
        make.height.equalTo(@(1*kRateSize));
    }];

    
    UIImageView *rightLine = [UIImageView new];
    //[rightLine setBackgroundColor:UIColorFromRGB(0xb3b3b3)];
    [rightLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
    [self.view addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(midLab.mas_right).offset(11*kRateSize);
        make.right.equalTo(wss.view.mas_right);
        make.height.equalTo(leftLine.mas_height);
        make.centerY.equalTo(leftLine.mas_centerY);
    }];
    
    
    UILabel *fiveLab = [UILabel new];
    fiveLab.text = @"1.首次注册登录 +50积分";
    fiveLab.font = [UIFont systemFontOfSize:14];
    fiveLab.textColor = UIColorFromRGB(0x999999);
    fiveLab.textAlignment = NSTextAlignmentLeft;
    fiveLab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:fiveLab];
    
    CGSize fiveSize = [[CalculateTextWidth alloc] sizeWithText:fiveLab.text font:fiveLab.font];
//    CGFloat fiveLabW = fiveSize.width+2;
    CGFloat fiveLabH = fiveSize.height;
    
    [fiveLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wss.view.mas_left).offset(36*kRateSize);
        make.top.equalTo(midLab.mas_bottom).offset(25*kRateSize);
        make.right.equalTo(wss.view.mas_right);
        make.height.equalTo(@(fiveLabH*kRateSize));
    }];
    
    
    
    UILabel *fiftyLab = [UILabel new];
    fiftyLab.text = @"2.每日签到 +5积分";
    fiftyLab.font = [UIFont systemFontOfSize:14];
    fiftyLab.textColor = UIColorFromRGB(0x999999);
    fiftyLab.textAlignment = NSTextAlignmentLeft;
    fiftyLab.backgroundColor = [UIColor clearColor];
    
    CGSize fiftySize = [[CalculateTextWidth alloc] sizeWithText:fiftyLab.text font:fiftyLab.font];
//    CGFloat fiftyLabW = fiftySize.width;
    CGFloat fiftyLabH = fiftySize.height;
    
    [self.view addSubview:fiftyLab];
    [fiftyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fiveLab.mas_left);
        make.top.equalTo(fiveLab.mas_bottom).offset(14*kRateSize);
        make.right.equalTo(wss.view.mas_right);
        make.height.equalTo(@(fiftyLabH*kRateSize));
    }];
    
    
    
    UILabel *moreLab = [UILabel new];
    moreLab.text = @"积分活动稍后上线，敬请期待！";
    moreLab.font = [UIFont systemFontOfSize:14];
    moreLab.textColor = UIColorFromRGB(0x666666);
    moreLab.textAlignment = NSTextAlignmentCenter;
    moreLab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:moreLab];
    [moreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wss.view.mas_centerX);
        make.top.equalTo(fiftyLab.mas_bottom).offset(25*kRateSize);
        make.left.equalTo(wss.view.mas_left);
        make.right.equalTo(wss.view.mas_right);
    }];
}


- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil) {
//        self.view = nil;
//    }
}


@end
