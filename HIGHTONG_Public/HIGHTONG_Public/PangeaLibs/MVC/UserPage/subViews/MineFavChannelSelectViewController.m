//
//  MineFavChannelSelectViewController.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/8.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MineFavChannelSelectViewController.h"
#import "headScroll.h"
#import "UIButton_Block.h"
#import "DemandHomeView.h"
#import "MyGesture.h"
#import "ItemChannelSelectTableCell.h"
#import "MJRefresh.h"
#import "CustomAlertView.h"
#import "LocalCollectAndPlayRecorder.h"

@interface MineFavChannelSelectViewController ()<DemandHomeViewDelegate, HTRequestDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
    NSInteger _liveRow; //...ww 点播部分的删除记录
    
    NSInteger _typeNum;
    
    NSInteger _classNum;
    
    headScroll *  _head;//头标题头
    
    DemandHomeView *_ScrollTable;//滑动的tableView
    NSMutableArray *_ScollArray;//滑动的数据源
    NSMutableArray *channelList;//所有节目列表
    
    NSMutableArray * _headArray ;//头的数组
    NSMutableDictionary *_scrollDic;
    
    NSInteger _currentIndex;//当前页数
    
    NSInteger _type;//当前页数
    
    BOOL firstLoad;//第一次加载

    NSDate *_requestDate;//加载时间
    
    NSInteger method;//加载方式
    
    NSInteger allChannelNum;//添加喜爱频道后，请求喜爱频道列表的标记
    
    UIView *_HTVw;
    UILabel *_HTLb;
    UIImageView *_HTImg;
}

@property (nonatomic, strong)UIImageView *addView;//底部添加视图

@property (nonatomic, strong)UIButton *addBtn;//底部添加按钮

@property (nonatomic, strong)UIButton *backBtn;//返回按钮

@property (nonatomic, strong)NSMutableArray *contentSelectedArr;//选中的频道列表数据

@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation MineFavChannelSelectViewController

- (NSMutableArray *)allChannelArr
{
    if (!_allChannelArr) {
        _allChannelArr = [NSMutableArray array];
    }
    
    return _allChannelArr;
}

- (NSMutableArray *)contentSelectedArr {
    
    if (!_contentSelectedArr) {
        _contentSelectedArr = [[NSMutableArray alloc] init];
    }
    return _contentSelectedArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
        NSLog(@"dfafasfsfadsfsfsf");
    }
#endif
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    
    method = 0;
    
    _headArray = [NSMutableArray array];
    _ScollArray = [NSMutableArray array];
    _scrollDic = [NSMutableDictionary dictionary];
    
    _currentIndex = 0;
    
    // 请求数据
    [self sendHTTPRequestMethod];
    
    [self setupOPERATIONHeader];

    //左侧返回按钮
    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(goBack)];
    [self setNaviBarLeftBtn:_backBtn];
    
    [self setNaviBarTitle:@"添加频道"];
    
    //页面底端增加频道收藏条
    _addView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KDeviceHeight-60*kDeviceRate-kBottomSafeSpace, kDeviceWidth, 60*kDeviceRate+kBottomSafeSpace)];
    _addView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [self.view addSubview:_addView];
    _addView.userInteractionEnabled = YES;
    
    
    UIView *VerticalLine = [[UIView alloc]init];
    [_addView addSubview:VerticalLine];
    VerticalLine.backgroundColor = HT_COLOR_SPLITLINE;
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_addView.mas_top).offset(1);
        make.left.equalTo(_addView.mas_left);
        make.right.equalTo(_addView.mas_right);
        make.height.equalTo(@(1));
    }];
    
    _addBtn = [[UIButton alloc] init];
    _addBtn.titleLabel.font = HT_FONT_FIRST;
    
    [_addBtn setBackgroundImage:[UIImage imageNamed:@"sl_user_favchannel_add_normal"] forState:UIControlStateNormal];
    [_addBtn setBackgroundImage:[UIImage imageNamed:@"sl_user_favchannel_add_pressed"] forState:UIControlStateHighlighted];
    [_addBtn setBackgroundImage:[UIImage imageNamed:@"ic_user_common_no_delete_item"] forState:UIControlStateDisabled];
    [_addBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_addBtn setTitle:@"添    加"forState:UIControlStateDisabled];
    _addBtn.enabled = NO;
    
    [_addBtn addTarget:self action:@selector(addBtnSenderDown1) forControlEvents:UIControlEventTouchUpInside];
    
    [_addView addSubview:_addBtn];
    
    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(351*kDeviceRate));
        make.height.equalTo(@(40*kDeviceRate));
        make.centerX.equalTo(_addView.mas_centerX);
        //        make.centerY.equalTo(_addView.mas_centerY);
        make.top.equalTo(VerticalLine.mas_bottom).offset(10*kDeviceRate);
    }];
    
    [_addView bringSubviewToFront:self.view];

}

- (void)setupOPERATIONHeader
{
    
    WS(wself);
    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    [_request TypeListWithTimeout:10];
    _head = [[headScroll alloc]initWithFrame:CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, 40*kDeviceRate)];
    _head.KEY = @"name";
    
    __weak headScroll *weakhead = _head;
    
    __weak DemandHomeView *weakScrollTable = _ScrollTable;
    
    _head.block = ^(NSInteger num,UIButton_Block*btn)
    {
        weakhead.MubiaoNum = num;
        weakhead.isSelectedNum = YES;
        weakhead.dianjizhuangtai = YES;
        NSLog(@"测试页数%ld  %@",num,btn.serviceIDName);

        method = 1;

        [_request EPGGetLivePageChannelInfoAndisType:-1];
        
        [wself show];

        
        [weakhead setSelectedNum:num];
        
        [_ScrollTable ChangePage:num + 1];
        
        _currentIndex = num;
                
    };
    [self show];
    [self.view addSubview:[UIView new]];
    [self.view addSubview:_head];
    
    UIView *VerticalLine = [[UIView alloc]init];
    [self.view addSubview:VerticalLine];
    VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
    [VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_head.mas_bottom).offset(-1);
        make.left.equalTo(_head.mas_left);
        make.right.equalTo(_head.mas_right);
        make.height.equalTo(@(1));
    }];
}

- (void)sendHTTPRequestMethod
{
    // 请求数据
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request EPGGetLivePageChannelInfoAndisType:-1];
    
    self.dataArr = [NSMutableArray arrayWithArray:[_localListCenter getLiveFavoriteListWithUserType:IsLogin]];
    
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    if ([type isEqualToString:EPG_ADDTVFAVORATE]){//增加频道收藏
        
        if (allChannelNum==0) {

            for (NSDictionary *dic in _contentSelectedArr) {
                [_localListCenter addLiveFavoriteWithDic:dic andUserType:IsLogin andKey:@"id"];
            }
            
            [self.contentSelectedArr removeAllObjects];
            
            _addBtn.enabled = NO;
            
            [self hiddenCustomHUD];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            
            
        }else{
            allChannelNum--;
            if (allChannelNum<=0) {
                allChannelNum = 0;
            }
            return;
        
        }
        
        NSLog(@"增加频道收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );

    }
    else if ([type isEqualToString:EPG_TVFAVORATELIST]) {//直播收藏列表
        
        NSLog(@"直播收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"++++成功");
                    
                    self.dataArr = [result objectForKey:@"servicelist"];

                    
                    break;
                }
                case -1:{
                    NSLog(@"收藏列表为空");

                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
  
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    break;
                }
                    
                default:
                    
                    break;
            }
            
        }
        
    }
    else if ([type isEqualToString:EPG_TYPELIST]) {//频道分类列表
        
        NSLog(@"频道分类列表: %ld result:%@  type:%@",status,result,type );
        switch (status) {
            case 0:
            {
                //                NSLog(@"%@",result);
                NSArray *array = [result objectForKey:@"typeList"];
                _head.titledataArray.array  = array;
                _head.MubiaoNum = _currentIndex;
                _head.dianjizhuangtai = NO;
                [_head updataTitle];
                if (!_ScrollTable) {
                    
                    _ScrollTable = [[DemandHomeView alloc]init];
                    [self.view addSubview:_ScrollTable];
                    
                    [_ScrollTable mas_makeConstraints:^(MASConstraintMaker *make) {

                        make.width.equalTo(@(kDeviceWidth));
                        make.top.equalTo(_head.mas_bottom);
                        make.bottom.equalTo(_addView.mas_top);;
                        
                    }];
                    
                }

                _ScrollTable.DataSource = array;
                _headArray.array = array;
                _ScrollTable.delegate = self;
                
                NSLog(@"_headArray-------%@",_headArray);
                
                [self show];
                
                [_ScrollTable setupView];
                [_ScrollTable ChangePage:_currentIndex + 1];
                
                break;
            }
                
                
            default:
            {
                UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                if (Tab) {
                    [Tab.mj_header endRefreshing];
                    [Tab reloadData];
                }
            }
                break;
        }
      
    }
    else if ([type isEqualToString:EPG_CHANNELLIST]){//频道列表
        
        NSLog(@"点播播放记录列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"有新数据++++成功");
                    
                    
                    _allChannelArr = [result objectForKey:@"channelList"];
                    
                    [_itemArr removeAllObjects];

                    
                    for (NSDictionary *tempDic in _allChannelArr) {

                        if (_currentIndex + 1 == [[tempDic objectForKey:@"type"] integerValue]) {
                                
                                _itemArr = [tempDic objectForKey:@"serviceinfo"];
                                
                                NSLog(@"分类列表里的内容%ld-----%@",_itemArr.count, _itemArr);
                                
                        }
                    }

                    UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
                    if (Tab) {
                        [Tab.mj_header endRefreshing];
                        [Tab reloadData];
                    }
                    
                    [self hide];
                    
                    [_ScrollTable ChangePage:_currentIndex+1];
                    
                    break;
                }
                case 1:{
                    NSLog(@"没有新数据");
                    break;
                }
                case -1:{
                    NSLog(@"aaaaaa获取数据失败");
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    break;
                }
                default:
                    break;
            }
        }
        
    }

}

//返回按钮
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 添加按钮触发方法
- (void)addBtnSenderDown1
{
    
    NSLog(@"添加了");
    HTRequest *addRequest = [[HTRequest alloc] initWithDelegate:self];
    
    allChannelNum = _contentSelectedArr.count-1;//全局记录
    
    [self showCustomeHUD];
    
    for (int i = 0; i<_contentSelectedArr.count; i++) {
        
        [addRequest AddTVFavorateWithServiceID:[[_contentSelectedArr objectAtIndex:i] objectForKey:@"id"] andTimeout:10];
        
        NSLog(@"添加第一步");
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 60*kDeviceRate;
        
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _itemArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //详细频道列表的cell
        
    static NSString *qq = @"qq";
        
    ItemChannelSelectTableCell *cell = [tableView dequeueReusableCellWithIdentifier:qq];
        
    if (!cell) {
            
        cell = [[ItemChannelSelectTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:qq];
            
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.backgroundColor = HT_COLOR_BACKGROUND_APP;
    }
        
        
    //数据源在左侧tabelcell的点击事件中
        
    cell.choiceImg.hidden = YES;
    
//    cell.UnChoiceImg.hidden = NO;
    
    cell.addedSign.hidden = YES;
    
    [cell.backgroundImg setImage:[UIImage imageNamed:@"bg_user_collection_item"]];
    
    cell.linkName.textColor = HT_COLOR_FONT_FIRST;
   

    //两个数组对比，重复的设置灰色背景，不同的白色背景
    
    NSDictionary *cdic = nil;
    if (indexPath.row < _itemArr.count) {
        cdic = [_itemArr objectAtIndex:indexPath.row];
    }
    NSString *CID = [cdic objectForKey:@"id"];
        
    //判断是否已是喜爱频道
    for (NSDictionary *tempDic in _dataArr) {
            
        if ([CID isEqualToString:[tempDic objectForKey:@"id"]]) {
                
            cell.choiceImg.hidden = YES;
            
            cell.addedSign.hidden = NO;
            
            cell.linkName.textColor = HT_COLOR_FONT_ASSIST;
            
            [cell.backgroundImg setImage:[UIImage imageNamed:@"bg_user_favchannel_hasadd"]];
            
            NSLog(@"*****yyyyyyyyyyy***%ld",indexPath.row);
                
            break;
        }
    }
        
        
    //给当前table的cell赋值
    NSLog(@"%ld",indexPath.row);
    NSInteger Kindex= indexPath.row;//zzs
        
    NSDictionary *imageLincDic = nil;
        
    if (Kindex < _itemArr.count) {
        imageLincDic = [_itemArr objectAtIndex:Kindex];
    }
        
    [cell.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([imageLincDic objectForKey:@"imageLink"])]placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
        
    cell.linkName.text = [imageLincDic objectForKey:@"name"];
        
        
    NSDictionary *selId = nil;
    if (Kindex < _itemArr.count) {
        selId = [_itemArr objectAtIndex:Kindex];
    }
        
    NSString *selectID = [selId objectForKey:@"id"];
  

    for (NSDictionary *selectTempDic in self.contentSelectedArr ) {
            
        if ([selectID isEqualToString:[selectTempDic objectForKey:@"id"]]) {
                
            cell.choiceImg.hidden = NO;
                
//            cell.UnChoiceImg.hidden = YES;
            
        }
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isExit = NO;
    
    for (int i=0; i<self.contentSelectedArr.count; i++) {
        
        NSString *cellID = [[self.contentSelectedArr objectAtIndex:i] objectForKey:@"id"];
        
        if ([cellID isEqualToString:[[_itemArr objectAtIndex:indexPath.row] objectForKey:@"id"]]) {
            
            [self.contentSelectedArr removeObjectAtIndex:i];
            
            isExit = YES;
            
            break;
        }
    }
    
    if (isExit) {
        
        UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
        if (Tab) {
            [Tab reloadData];
            
        }
    }else{
        
        BOOL flag = false;
        
        for (NSDictionary *tempDic in self.dataArr) {
            
            if ([[_itemArr[indexPath.row] objectForKey:@"id" ] isEqualToString:[tempDic objectForKey:@"id"]]) {
                
                flag = true;
            }
        }
        
        if (!flag) {
            
            [self.contentSelectedArr addObject:_itemArr[indexPath.row]];
        }
        
        UITableView *Tab  =  [_ScrollTable TableViewIndex:_currentIndex];
        if (Tab) {
            [Tab reloadData];
            
        }
    }
    
    [self callCheckDataListSelected];
  
}
// 判断选中数据
- (void)callCheckDataListSelected
{
    
    if (self.contentSelectedArr.count >0) {
        _addBtn.enabled = YES;
        
        NSString *btnStr = [NSString stringWithFormat:@"添    加 ( %lu )",(unsigned long)self.contentSelectedArr.count];
        
        [_addBtn setTitle:btnStr forState:UIControlStateNormal];
        [_addBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        
    } else {
        _addBtn.enabled = NO;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //空数据提示页
    
    _HTVw =[[UIView alloc]init];
    _HTVw.backgroundColor = App_background_color;
    
    //空数据页提示信息
    _HTLb = [[UILabel alloc]init];
    _HTLb.text = @"暂无数据";
    _HTLb.textAlignment = NSTextAlignmentCenter;
    
    [_HTVw addSubview:_HTLb];
    
    [_HTLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
    }];
    
    _HTImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];
    [_HTVw addSubview:_HTImg];
    
    [_HTImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(_HTVw.mas_centerX);
        make.centerY.equalTo(_HTVw.mas_centerY);
        make.height.equalTo(@(240*kRateSize));
        make.width.equalTo(@(180*kRateSize));
        
    }];
    
//    _HTImg.hidden = YES;
//    _HTLb.hidden = YES;
    
    NSInteger count = 0;
    
    count = _itemArr.count;
    
    if (count) {
        return nil;
    }
    
    return _HTVw;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    count = _itemArr.count;
    
    if (count) {
        return 0;
    }
    
    return (KDeviceHeight - 138*kRateSize);
}



#pragma mark-   DemandHomeViewDelegate 滑动的Tableview  代理

//滚动结束后，调用数据
- (void)didChangePageWithPages:(NSInteger)page
{
    
    NSLog(@"method-----%ld;page--------%ld",(long)method,(long)page);
    
    [_head setSelectedNum:page];
    _currentIndex = page-1;
    _head.dianjizhuangtai = NO;
    
        method = 0;
    
    NSLog(@"-=滚动结束后，调用数据当前页数%ld",(long)_currentIndex);
    
    [self show];

    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
//    [_request EPGChannelWithType:-1 andLookbackFlag:0 andTimeout:10];
    [_request EPGGetLivePageChannelInfoAndisType:-1];

}

- (void)didChangePageWithPage:(NSInteger)page
{

}

- (void)didRefreshDateWithTag:(NSInteger)tag
{
    NSLog(@"%ld目前的下拉中的东西啊",tag);
    
    if (_headArray.count > _currentIndex) {
        
    }
    
    [self show];
}

/**
 *  显示loading框
 */
- (void)show{
    _HTImg.hidden = NO;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self showCustomeHUD];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}
/**
 *  隐藏loading框
 */
- (void)hide{
    _HTVw.hidden = NO;
    _HTImg.hidden = YES;
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self hiddenCustomHUD];
}


@end
