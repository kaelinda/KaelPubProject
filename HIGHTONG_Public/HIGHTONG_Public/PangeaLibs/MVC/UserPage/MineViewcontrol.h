//
//  MineViewcontrol.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface MineViewcontrol : HT_FatherViewController

@property (nonatomic, strong)UIView *topView;//头像部分
@property (nonatomic, strong)UIView *midView;//中间播放记录部分
@property (nonatomic, strong)UIView *underView;//下面设置帮助反馈部分
@property (nonatomic, strong)UIView *loginBackView;
@property (nonatomic, strong)UIImageView *headImg;
@property (nonatomic, strong)UIButton *logRegisNameBtn;
@property (nonatomic, strong)UIButton *moreProgrammeBtn;

@property (nonatomic, strong)UIScrollView *scr;
@property (nonatomic, strong)UIView *midBackView;
@property (nonatomic, strong)UIView *underBackView;
@property (nonatomic, strong)UIView *scrolContentView;
//@property (nonatomic, strong)UIView *docAuthView;

//@property (nonatomic, strong)UIImageView *authDoctoryImg;//认证医师logo

@property (nonatomic,assign) HomePageType pagetype;

@end
