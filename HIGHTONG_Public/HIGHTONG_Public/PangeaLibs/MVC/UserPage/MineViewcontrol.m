//
//  MineViewcontrol.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineViewcontrol.h"
#import "MineCustomView.h"
#import "MyGesture.h"
#import "MineRecorderViewController.h"
#import "MineFavChannelViewController.h"
#import "MineCollectViewController.h"
#import "MineScheduleViewController.h"
#import "MineSetViewController.h"
#import "FeedbackViewController.h"
#import "MineAccountViewController.h"
#import "LoginViewController.h"
#import "UIView+Extension.h"
#import "ExtranetPromptViewController.h"
#import "KnowledgeBaseViewController.h"
#import "MineHealthyVideoViewController.h"
#import "UserLoginViewController.h"
#import "UrlPageJumpManeger.h"


#define logBtnTitleStr isAuthCodeLogin ? @"登录":@"登录/注册"

#define nickNameWidth kDeviceWidth/2

@interface MineViewcontrol ()<UIScrollViewDelegate>//,HTRequestDelegate

@property (nonatomic, assign)NSString *statusStr;//存储医生认证状态

@end

@implementation MineViewcontrol

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobCountTool pushInWithPageView:@"MineHomePage"];

    NSLog(@"宽：%f,高：%f,X:%f,Y:%f",_scr.frame.size.width,_scr.frame.size.height,_scr.origin.x,_scr.origin.y);
    
//    if (IsLogin && [[NSUserDefaults standardUserDefaults] boolForKey:@"AuthenticSuccessed"]) {
//// lailibo添加了一个标签经常出现，如果是有UGC才出现
//        if (_pagetype == kHomePageType_JKTV_V1_5) {
//
//            _authDoctoryImg.hidden = NO;
//
//        }else
//        {
//            _authDoctoryImg.hidden = YES;
//        }
////以上代码
//    }else{
//        _authDoctoryImg.hidden = YES;
//    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"MineHomePage"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideNaviBar:YES];
    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(comeFromFeedbackView:) name:@"COME_FROM_FEEDBACK_VIEW" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isCustomUserAvatar) name:@"CHANG_ACCOUNT_IMAGE" object:nil];
    
    self.pagetype = kApp_HomePageType;
    
    [self makeTheMainPartUI];
}

- (void)isCustomUserAvatar
{
    if (IsLogin) {
                
        NSString *photoStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];

        [_headImg sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[UIImage imageNamed:@"img_user_avatar_def"]];
        
        _headImg.userInteractionEnabled = NO;
        
        
        [_logRegisNameBtn setTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"] forState:UIControlStateNormal];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"] length]) {
            
            [_logRegisNameBtn setTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"] forState:UIControlStateNormal];
        }
        
        _logRegisNameBtn.userInteractionEnabled = NO;
        
        _moreProgrammeBtn.hidden = YES;
        
    }else{
        
        [_headImg setImage:[UIImage imageNamed:@"img_user_avatar"]];
        
        _headImg.userInteractionEnabled = YES;
        
        [_logRegisNameBtn setTitle:logBtnTitleStr forState:UIControlStateNormal];
        
        _logRegisNameBtn.userInteractionEnabled = YES;
        
        _moreProgrammeBtn.hidden = NO;
    }
}

- (void)comeFromFeedbackView:(NSNotification *)noti
{
    id text = noti.object;
    
    if ([text isEqualToString:@"1"]) {
        
        [AlertLabel AlertInfoWithText:@"感谢您的反馈，我们将尽快完善！" andWith:self.view withLocationTag:0];
    }else if ([text isEqualToString:@"2"]){
        
        [AlertLabel AlertInfoWithText:@"您还没有登录哦！" andWith:self.view withLocationTag:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_scr == scrollView) {
        CGFloat yOffset  = scrollView.contentOffset.y;
        if (scrollView.contentOffset.y < 0) {
            CGRect f =self.topView.frame;
            f.origin.y = yOffset;
            f.size.height = scrollView.frame.size.height-yOffset;
            self.topView.frame = f;
            
            [_loginBackView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_topView.mas_top).offset(-scrollView.contentOffset.y);
            }];
        }
        
    }
    return;
}

- (void)makeTheMainPartUI
{
    WS(wss);
    UIView *topView = [UIView new];
    topView.backgroundColor = App_selected_color;
    topView.frame = CGRectMake(0, 0, kDeviceWidth, kTopSlimSafeSpace);
    [self.view addSubview:topView];
    
    _scr = [UIScrollView new];
    [self.view addSubview:_scr];
    _scr.frame = CGRectMake(0, kTopSlimSafeSpace, kDeviceWidth, KDeviceHeight-49);
    
    _scr.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _scr.delegate = self;
    _scr.showsHorizontalScrollIndicator = NO;
    _scr.showsVerticalScrollIndicator = NO;
    

    _scrolContentView = [UIView new];
    [_scrolContentView setBackgroundColor:[UIColor clearColor]];
    [_scr addSubview:_scrolContentView];
    [_scrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wss.scr);
        make.width.equalTo(wss.scr);
    }];
    
    
    _topView = [[UIView alloc] init];
    _topView.backgroundColor = App_selected_color;
    [_scrolContentView addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.scrolContentView.mas_width);
        make.height.equalTo(@(198*kDeviceRate));
        make.top.equalTo(wss.scrolContentView.mas_top);
        make.left.equalTo(wss.scrolContentView.mas_left);
    }];
    
    
//    _docAuthView = [[UIView alloc] init];
//    [_docAuthView setBackgroundColor:HT_COLOR_FONT_INVERSE];
//    [_scrolContentView addSubview:_docAuthView];
//
//
//    if (_pagetype == kHomePageType_JKTV_V1_5) {//有UGC
//        [wss.docAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(wss.topView.mas_bottom);
//            make.centerX.mas_equalTo(wss.topView.mas_centerX);
//            make.width.mas_equalTo(wss.topView.mas_width);
//            make.height.mas_equalTo(50.5*kDeviceRate);
//        }];
//    }else{//无
//        [wss.docAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(wss.topView.mas_bottom);
//            make.centerX.mas_equalTo(wss.topView.mas_centerX);
//            make.width.mas_equalTo(wss.topView.mas_width);
//            make.height.mas_equalTo(0*kDeviceRate);
//        }];
//    }
    
    
    _midBackView = [UIView new];
    [_scrolContentView addSubview:_midBackView];
    [_midBackView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    if (YJVideoNewDisplayTypeOn) {
        [_midBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.scrolContentView.mas_width);
//            make.top.equalTo(wss.docAuthView.mas_bottom);
            make.top.equalTo(wss.topView.mas_bottom);
            make.height.equalTo(@(258*kDeviceRate));//208
            make.left.equalTo(wss.scrolContentView.mas_left);
        }];
    }else{
        [_midBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.scrolContentView.mas_width);
            make.top.equalTo(wss.topView.mas_bottom);
            make.height.equalTo(@(208*kDeviceRate));//207.5
            make.left.equalTo(wss.scrolContentView.mas_left);
        }];
    }
    
    
    _underBackView = [UIView new];
    [_scrolContentView addSubview:_underBackView];
    [_underBackView setBackgroundColor:HT_COLOR_BACKGROUND_APP];
    [_underBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.midBackView.mas_width);
        make.top.equalTo(wss.midBackView.mas_bottom);
        make.left.equalTo(wss.midBackView.mas_left);
        make.height.equalTo(@(157*kDeviceRate));
    }];

    
    [_scrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wss.underBackView.mas_bottom);
    }];

    
    MyGesture *accountGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    accountGes.numberOfTapsRequired = 1;
    accountGes.tag = 17;
    [_topView addGestureRecognizer:accountGes];
    
    _loginBackView = [[UIView alloc]init];
    [_loginBackView setBackgroundColor:[UIColor clearColor]];
    [_topView addSubview:_loginBackView];
    [_loginBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topView.mas_top);
        make.bottom.mas_equalTo(_topView.mas_bottom);
        make.centerX.mas_equalTo(_topView.mas_centerX);
        make.width.mas_equalTo(120*kDeviceRate);
    }];
    _loginBackView.userInteractionEnabled = NO;
    
    
    _headImg = [UIImageView new];
    [_loginBackView addSubview:_headImg];
    [_headImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(92*kDeviceRate));
        make.center.equalTo(wss.loginBackView); 
    }];
    _headImg.layer.masksToBounds = YES;
    [_headImg.layer setCornerRadius:(92*kDeviceRate/2)];
    _headImg.userInteractionEnabled = NO;
    
    
//    _authDoctoryImg = [[UIImageView alloc] init];
//    [_topView addSubview:_authDoctoryImg];
//    _authDoctoryImg.image = [UIImage imageNamed:@"myself_doctor_certification"];
//    [_authDoctoryImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(55*kDeviceRate, 21*kDeviceRate));
//        make.top.mas_equalTo(wss.headImg.mas_top).offset(62*kDeviceRate);
//        make.right.mas_equalTo(wss.topView.mas_right).offset(-139*kDeviceRate);
//    }];
//    _authDoctoryImg.hidden = YES;
    
    
    _logRegisNameBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _logRegisNameBtn.titleLabel.font = HT_FONT_FIRST;
    [_logRegisNameBtn setTitleColor: HT_COLOR_FONT_INVERSE forState:UIControlStateNormal];
    _logRegisNameBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_loginBackView addSubview:_logRegisNameBtn];
    [_logRegisNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.headImg.mas_bottom).offset(5*kDeviceRate);
        make.width.equalTo(@(nickNameWidth));
        make.centerX.equalTo(wss.loginBackView.mas_centerX);
        make.height.equalTo(@(20*kDeviceRate));
    }];
    _logRegisNameBtn.userInteractionEnabled = NO;
    
        
    _moreProgrammeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _moreProgrammeBtn.titleLabel.font = HT_FONT_FOURTH;
    [_moreProgrammeBtn setTitleColor:HT_COLOR_FONT_INVERSE forState:UIControlStateNormal];
    _moreProgrammeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_moreProgrammeBtn setTitle:@"登录后可观看更多节目" forState:UIControlStateNormal];
    _moreProgrammeBtn.enabled = NO;
    [_loginBackView addSubview:_moreProgrammeBtn];
    [_moreProgrammeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.logRegisNameBtn.mas_bottom).offset(2.5*kDeviceRate);
        make.width.equalTo(@(150*kDeviceRate));
        make.centerX.equalTo(wss.logRegisNameBtn);
        make.height.equalTo(@(13*kDeviceRate));
    }];
    _moreProgrammeBtn.userInteractionEnabled = NO;
    
    //头像昵称等赋值，登陆按钮的显藏等
    [self isCustomUserAvatar];
    
#pragma mark  医生认证部分
//    MineCustomView *docView = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_doctor_certification" andLabTitle:@"医生认证" andRightImgName:@"ic_user_arrow"];
//    [_docAuthView addSubview:docView];
//    [docView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.width.centerX.mas_equalTo(wss.docAuthView);
//        make.height.mas_equalTo(50*kDeviceRate);
//    }];
//
//    MyGesture *docGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
//    docGes.numberOfTapsRequired = 1;
//    docGes.tag = 19;
//    [docView addGestureRecognizer:docGes];
//
//
//    UIImageView *docLine = [[UIImageView alloc] init];
//    [docLine setBackgroundColor:HT_COLOR_SPLITLINE];
//    [_docAuthView addSubview:docLine];
//    [docLine mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(0.5*kDeviceRate);
//        make.top.mas_equalTo(docView.mas_bottom);
//        make.width.centerX.mas_equalTo(docView);
//    }];
    
#pragma mark  健康视频、播放记录、收藏、喜爱频道、预约四部分
    //一像素描边view
    UIImageView *midViewUpsideLine = [[UIImageView alloc] init];
    [midViewUpsideLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_midBackView addSubview:midViewUpsideLine];
    [midViewUpsideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.midBackView.mas_width);
        make.height.equalTo(@(0.5*kDeviceRate));
        make.top.equalTo(wss.midBackView.mas_top).offset(5*kDeviceRate);
        make.centerX.equalTo(wss.midBackView.mas_centerX);
    }];

    
    _midView = [UIView new];
    _midView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [_midBackView addSubview:_midView];
    [_midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.view.mas_width);
        make.left.equalTo(wss.view.mas_left);
        make.top.equalTo(midViewUpsideLine.mas_bottom);
        make.bottom.equalTo(wss.midBackView.mas_bottom);
    }];
    
    UIImageView *healthyLine = [UIImageView new];
    healthyLine.backgroundColor = HT_COLOR_SPLITLINE;
    [_midView addSubview:healthyLine];
    if (YJVideoNewDisplayTypeOn) {
        [healthyLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.midView.mas_left).offset(12*kDeviceRate);
            make.right.equalTo(wss.midView.mas_right).offset(-12*kDeviceRate);
            make.height.equalTo(@(0.5*kDeviceRate));
            make.top.equalTo(wss.midView.mas_top).offset(50.5*kDeviceRate);
        }];
    }else{
        [healthyLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.midView.mas_left).offset(12*kDeviceRate);
            make.right.equalTo(wss.midView.mas_right).offset(-12*kDeviceRate);
            make.height.equalTo(@(0*kDeviceRate));
            make.top.equalTo(wss.midView.mas_top);
        }];
    }
    
    
    UIImageView *firstLine = [UIImageView new];
    [firstLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_midView addSubview:firstLine];
    [firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(healthyLine);
        make.centerX.equalTo(healthyLine.mas_centerX);
        make.top.equalTo(healthyLine.mas_bottom).offset(50*kDeviceRate);
    }];
    
    
    UIImageView *secondLine = [UIImageView new];
    [secondLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_midView addSubview:secondLine];
    [secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(firstLine);
        make.centerX.equalTo(firstLine.mas_centerX);
        make.top.equalTo(firstLine.mas_bottom).offset(50*kDeviceRate);
    }];
    
    
    UIImageView *thirdLine = [UIImageView new];
    [thirdLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_midView addSubview:thirdLine];
    [thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(secondLine);
        make.centerX.equalTo(secondLine.mas_centerX);
        make.top.equalTo(secondLine.mas_bottom).offset(50*kDeviceRate);
    }];
    
    
    UIImageView *midViewDownSideLine = [[UIImageView alloc] init];
    [midViewDownSideLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_midBackView addSubview:midViewDownSideLine];
    [midViewDownSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(midViewUpsideLine);
        make.top.equalTo(wss.midView.mas_bottom);
        make.centerX.equalTo(wss.midView.mas_centerX);
    }];
    
    
    MineCustomView *medicineTV = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_polymedicine_video" andLabTitle:@"健康养生" andRightImgName:@"ic_user_arrow"];
    [_midView addSubview:medicineTV];
    [medicineTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wss.midView.mas_centerX);
        make.width.mas_equalTo(wss.midView.mas_width);
        make.bottom.mas_equalTo(healthyLine.mas_top);
        make.top.mas_equalTo(wss.midView.mas_top);
    }];
    
    
    MyGesture *medicineTvGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    medicineTvGes.numberOfTapsRequired = 1;
    medicineTvGes.tag = 18;
    [medicineTV addGestureRecognizer:medicineTvGes];
    
    
    MineCustomView *playRecorder = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_playrecord_label" andLabTitle:@"播放记录" andRightImgName:@"ic_user_arrow"];
    [_midView addSubview:playRecorder];
    [playRecorder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wss.midView.mas_centerX);
        make.width.mas_equalTo(wss.midView.mas_width);
        make.bottom.mas_equalTo(firstLine.mas_top);
        make.height.mas_equalTo(50*kDeviceRate);
    }];
    
    MyGesture *gestureOne = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    gestureOne.numberOfTapsRequired = 1;
    gestureOne.tag = 10;
    [playRecorder addGestureRecognizer:gestureOne];
    
    
    MineCustomView *favChannel = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_favchannel_label" andLabTitle:@"喜爱频道" andRightImgName:@"ic_user_arrow"];
    [_midView addSubview:favChannel];
    [favChannel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(playRecorder);
        make.top.equalTo(firstLine.mas_bottom);
        make.centerX.equalTo(wss.midView.mas_centerX);
    }];
    
    MyGesture *channelGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    channelGes.numberOfTapsRequired = 1;
    channelGes.tag = 11;
    [favChannel addGestureRecognizer:channelGes];
    
    
    MineCustomView *collect = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_collect_label" andLabTitle:@"我的收藏" andRightImgName:@"ic_user_arrow"];
    [_midView addSubview:collect];
    [collect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wss.midView.mas_centerX);
        make.top.mas_equalTo(secondLine.mas_bottom);
        make.size.mas_equalTo(favChannel);
    }];
    
    MyGesture *gestureTwo = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    gestureTwo.numberOfTapsRequired = 1;
    gestureTwo.tag = 12;
    [collect addGestureRecognizer:gestureTwo];
    
    
    MineCustomView *schedule = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_order_label" andLabTitle:@"我的预约" andRightImgName:@"ic_user_arrow"];
    [_midView addSubview:schedule];
    [schedule mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(collect);
        make.centerX.equalTo(wss.midView.mas_centerX);
        make.top.equalTo(thirdLine.mas_bottom);
    }];
    
    MyGesture *scheduleGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    scheduleGes.numberOfTapsRequired = 1;
    scheduleGes.tag = 13;
    [schedule addGestureRecognizer:scheduleGes];
    
  
#pragma mark    设置、关于我们、帮助反馈部分
    //下部分
    UIImageView *underViewUpSideLine = [UIImageView new];
    [underViewUpSideLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_underBackView addSubview:underViewUpSideLine];
    [underViewUpSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(midViewUpsideLine);
        make.top.equalTo(wss.underBackView.mas_top).offset(5*kDeviceRate);
        make.centerX.equalTo(wss.underBackView.mas_centerX);
    }];
    
    
    //下面背景图
    _underView = [UIView new];
    _underView.backgroundColor = HT_COLOR_FONT_INVERSE;
    [_underBackView addSubview:_underView];
    [_underView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wss.underBackView.mas_width);
        make.centerX.equalTo(wss.underBackView.mas_centerX);
        make.top.equalTo(underViewUpSideLine.mas_bottom);
        make.bottom.equalTo(wss.underBackView.mas_bottom);
    }];
    
    
    UIImageView *fourthLine = [UIImageView new];
    [fourthLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_underView addSubview:fourthLine];
    [fourthLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(thirdLine);
        make.centerX.equalTo(wss.underView.mas_centerX);
        make.top.equalTo(wss.underView.mas_top).offset(50*kDeviceRate);
    }];
    
    
    UIImageView *fiveLine = [UIImageView new];
    [fiveLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_underView addSubview:fiveLine];
    [fiveLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(fourthLine);
        make.centerX.equalTo(fourthLine.mas_centerX);
        make.top.equalTo(fourthLine.mas_bottom).offset(50*kDeviceRate);
    }];
    
    
    UIImageView *underViewDownSideLine = [UIImageView new];
    [underViewDownSideLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [_underBackView addSubview:underViewDownSideLine];
    [underViewDownSideLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(underViewUpSideLine);
        make.top.equalTo(wss.underView.mas_bottom);
        make.centerX.equalTo(wss.underView.mas_centerX);
    }];
    
    
    MineCustomView *Set = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_setting_label" andLabTitle:@"系统设置" andRightImgName:@"ic_user_arrow"];
    [_underView addSubview:Set];
    [Set mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(schedule);
        make.centerX.equalTo(wss.underView.mas_centerX);
        make.top.equalTo(@0);
    }];
    
    MyGesture *setGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    setGes.numberOfTapsRequired = 1;
    setGes.tag = 14;
    [Set addGestureRecognizer:setGes];
    
    
    MineCustomView *feedback = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_feedback_label" andLabTitle:@"帮助反馈" andRightImgName:@"ic_user_arrow"];
    [_underView addSubview:feedback];
    [feedback mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(Set);
        make.top.equalTo(fourthLine.mas_bottom);
        make.centerX.equalTo(wss.underView.mas_centerX);
    }];
    
    MyGesture *gestureThree = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    gestureThree.numberOfTapsRequired = 1;
    gestureThree.tag = 15;
    [feedback addGestureRecognizer:gestureThree];
    
    
    MineCustomView *aboutUs = [[MineCustomView alloc] initWithLeftImgName:@"ic_user_aboutus_label" andLabTitle:@"关于我们" andRightImgName:@"ic_user_arrow"];
    [_underView addSubview:aboutUs];
    [aboutUs mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(feedback);
        make.top.equalTo(fiveLine.mas_bottom);
        make.centerX.equalTo(feedback.mas_centerX);
    }];
    
    MyGesture *aboutGes = [[MyGesture alloc] initWithTarget:self action:@selector(goToTheNewVC:)];
    aboutGes.numberOfTapsRequired = 1;
    aboutGes.tag = 16;
    [aboutUs addGestureRecognizer:aboutGes];
    
}

- (void)goToLoginOrRegister
{
    if (isAuthCodeLogin) {
        UserLoginViewController *user = [[UserLoginViewController alloc] init];
        user.loginType = kNormalLogin;
        user.dismissBlock = ^(){
            [self isCustomUserAvatar];
        };
        
        user.isLawDocBlock = ^(BOOL isDoc){
            // lailibo添加了一个标签经常出现，如果是有UGC才出现
//
//            if (_pagetype == kHomePageType_JKTV_V1_5) {
//                
//                _authDoctoryImg.hidden = !isDoc;
//
//            }else
//            {
//                _authDoctoryImg.hidden = YES;
//            }
            //end以上
        };
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:user];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        
        LoginViewController *login = [[LoginViewController alloc] init];
        
        [self.navigationController pushViewController:login animated:YES];
    }
    
}

#pragma mark 手势触发事件
- (void)goToTheNewVC:(MyGesture *)gesture
{
    switch (gesture.tag) {
        case 10:{
            
            if (IsLogin) {
                
                if (!IS_EPG_CONFIGURED) {
                    
                    ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
                    
                    extranet.ExtranetPromptTitle = @"播放记录";
                    [self.navigationController pushViewController:extranet animated:YES];

                }else{
                
                    MineRecorderViewController *recorder = [[MineRecorderViewController alloc] init];
                    
                    [self.navigationController pushViewController:recorder animated:YES];
                }
                
            }else{
                
                [self goToLoginOrRegister];
            }
        
            break;
        }
        case 11:{
            
            if (IsLogin) {
                
                if (!IS_EPG_CONFIGURED) {
                    
                    ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
                    extranet.ExtranetPromptTitle = @"喜爱频道";
                    [self.navigationController pushViewController:extranet animated:YES];
                    
                }else{
                    
                    MineFavChannelViewController *channel = [[MineFavChannelViewController alloc] init];
                    
                    [self.navigationController pushViewController:channel animated:YES];
                }
                
            }else{
                
                [self goToLoginOrRegister];
            }

            break;
        }
        case 12:{
            
            if (IsLogin) {
                
                if (!IS_EPG_CONFIGURED) {
                    
                    ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
                    
                    extranet.ExtranetPromptTitle = @"我的收藏";
                    [self.navigationController pushViewController:extranet animated:YES];
                    
                }else{
                    
                    MineCollectViewController *collect = [[MineCollectViewController alloc] init];
                    collect.controllerTitleStr = @"我的收藏";
                    [self.navigationController pushViewController:collect animated:YES];
                }
               
            }else{

                [self goToLoginOrRegister];
            }

            break;
        }
        case 13:{
            
            if (IsLogin) {
                
                if (!IS_EPG_CONFIGURED) {
                    
                    ExtranetPromptViewController *extranet = [[ExtranetPromptViewController alloc] init];
                    extranet.ExtranetPromptTitle = @"我的预约";
                    [self.navigationController pushViewController:extranet animated:YES];
                    
                }else{
                    
                    MineScheduleViewController *schedule = [[MineScheduleViewController alloc] init];
                    
                    [self.navigationController pushViewController:schedule animated:YES];
                }
            }else{
                
                [self goToLoginOrRegister];
            }

            break;
        }
        case 14:{//设置
            MineSetViewController *set = [[MineSetViewController alloc] init];
            [self.navigationController pushViewController:set animated:YES];
            
            break;
        }
        case 15:{//帮助反馈
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc] init];
            know.existNaviBar = YES;
            know.urlStr = [UrlPageJumpManeger feedBackPageJump];
            [self.navigationController pushViewController:know animated:YES];
            
//            HTALViewController *pickerVc = [[HTALViewController alloc] initWithShowType:ALShowVideo];//单选视频
//            pickerVc.groupType = KAssetsLabraryVideo;//视频是照片的具体分类
//            pickerVc.maxCount = 1;//设置最多能选几张图片
//            pickerVc.choiceType = ALSingleChoice;
//            
//            HT_NavigationController *nav = [[HT_NavigationController alloc] initWithRootViewController:pickerVc];
//            [self presentViewController:nav animated:YES completion:nil];
            
            break;
        }
        case 16:{//关于我们
            KnowledgeBaseViewController *know = [[KnowledgeBaseViewController alloc]init];
            know.urlStr = [UrlPageJumpManeger minePageAboutUSJump];
            know.existNaviBar = YES;
            [self.navigationController pushViewController:know animated:YES];
            
            break;
        }
        case 17:{//用户中心
            
            if (IsLogin) {
                MineAccountViewController *account = [[MineAccountViewController alloc] init];
                [self.navigationController pushViewController:account animated:YES];

            }else{
                
                [self goToLoginOrRegister];
            }
        
            break;
        }
        case 18:{//健康讲座
            if (IsLogin) {
                MineHealthyVideoViewController *collect = [[MineHealthyVideoViewController alloc] init];
                collect.controllerTitleStr = @"健康养生";
                collect.flagStr = @"2";
                [self.navigationController pushViewController:collect animated:YES];

            }else{
                
                [self goToLoginOrRegister];
            }
            break;
        }
//        case 19:{//医生认证
//        
////            if (IsLogin) {
////
////                _statusStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"AuthenticStatus"];
////
////                if ([_statusStr isEqualToString:@"0"]) {//已认证
////
////                    UGCAuthStatusViewController *authView = [[UGCAuthStatusViewController alloc] init];
////
////                    authView.isStatus = YES;
////
////                    [self.navigationController pushViewController:authView animated:YES];
////
////                    return;
////                }
//////                else if ([_statusStr isEqualToString:@"1"]) {//未认证
//////
//////                    UGCAuthBasicInfoViewController *infoView = [[UGCAuthBasicInfoViewController alloc] init];
//////
//////                    [self.navigationController pushViewController:infoView animated:YES];
//////                }
////                else{//认证中及其他做请求刷新状态
////
////                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
////                    [request UGCCommonIsValidUser];
////
////                    //加loading
////                    [self showCustomeHUD];
////
////                    return;
////                }
////
////            }else{//未登录去登录
////
////                [self goToLoginOrRegister];
////            }
//    
//            break;
//        }
        default:
            break;
    }
}

//- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
//{
//    NSLog(@"认证状态界面返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
//
//    [self hiddenCustomHUD];
//
//    if (status == 0 && result.count >0) {
//
////        if ([type isEqualToString:UGC_COMMON_ISVALIDUSER]) {////2.1    判断是否合法主播
////
////            switch ([[result objectForKey:@"ret"] integerValue]) {
////                case 0:
////                {//合法主播
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////                    //1、存认证成功的标记
////                    [userDefaults setBool:YES forKey:@"AuthenticSuccessed"];
////
////                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UGC_ISVALIDUSER_SUCCESS" object:nil];
////
////                    //2、下面的key统一存储认证状态
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////
////                    //3、存相应数据
////                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[result objectForKey:@"channelId"] length]>0?[result objectForKey:@"channelId"]:@"",@"channelId",[[result objectForKey:@"name"] length]>0?[result objectForKey:@"name"]:@"",@"name",[[result objectForKey:@"hospital"] length]>0?[result objectForKey:@"hospital"]:@"",@"hospital",[[result objectForKey:@"department"] length]>0?[result objectForKey:@"department"]:@"",@"department",[[result objectForKey:@"academicTitle"] length]>0?[result objectForKey:@"academicTitle"]:@"",@"academicTitle", nil];
////
////
////                    [userDefaults setObject:dic forKey:@"AuthenticSuccessedData"];
////                    [userDefaults synchronize];
////
////
////                    UGCAuthStatusViewController *authView = [[UGCAuthStatusViewController alloc] init];
////                    authView.isStatus = YES;
////                    [self.navigationController pushViewController:authView animated:YES];
////
////
////                    break;
////                }
////                case 1:
////                {//未认证  去认证
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////
////                    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
////
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////                    [userDefaults synchronize];
////
////
////                    UGCAuthBasicInfoViewController *infoView = [[UGCAuthBasicInfoViewController alloc] init];
////                    [self.navigationController pushViewController:infoView animated:YES];
////
////
////                    break;
////                }
////                case 2:
////                {//认证中
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////
////                    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
////
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////                    [userDefaults synchronize];
////
////                    //取消loading
////                    UGCAuthStatusViewController *authView = [[UGCAuthStatusViewController alloc] init];
////                    authView.isStatus = NO;
////                    [self.navigationController pushViewController:authView animated:YES];
////
////                    break;
////                }
////                case 3:
////                {//审核不通过   去认证
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////
////                    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
////
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////                    [userDefaults synchronize];
////
////                    UGCAuthBasicInfoViewController *infoView = [[UGCAuthBasicInfoViewController alloc] init];
////                    [self.navigationController pushViewController:infoView animated:YES];
////
////                    break;
////                }
////                case -2:
////                {//身份认证有问题
////
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////
////                    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
////
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////                    [userDefaults synchronize];
////
////                    //取消loading
////                    UGCAuthStatusViewController *authView = [[UGCAuthStatusViewController alloc] init];
////                    authView.isStatus = NO;
////                    [self.navigationController pushViewController:authView animated:YES];
////
////                    break;
////                }
////                case -9:
////                {//其它异常（连接数据库失败或者参数错误等）  去认证
////
////                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
////
////                    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
////
////                    [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
////                    [userDefaults synchronize];
////
////                    UGCAuthBasicInfoViewController *infoView = [[UGCAuthBasicInfoViewController alloc] init];
////                    [self.navigationController pushViewController:infoView animated:YES];
////
////                    break;
////                }
////            }
////        }
////
//    }else{
//
//        ZSToast(@"网络不通，请稍后再试~");
//    }
//}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"COME_FROM_FEEDBACK_VIEW" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CHANG_ACCOUNT_IMAGE" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
