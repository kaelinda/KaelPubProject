//
//  MineFvoriteEditChannelCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineFvoriteEditChannelCell : UITableViewCell

@property (nonatomic, strong)UIView *channelView;//节目视图

@property (nonatomic, strong)UIImageView *backImg;//cell底图

@property (nonatomic, strong)UIImageView *topImg;//置顶按钮

@property (nonatomic, strong)UIImageView *moveImg;//拖动按钮

@property (nonatomic, strong)UIImageView *linkImg;//台标

@property (nonatomic, strong)UILabel *linkName;//台名

@property (nonatomic, strong)UIImageView *choiceImg;//选中标记

@property (nonatomic, strong)UIImageView *UnChoiceImg;//未选中标记

@end
