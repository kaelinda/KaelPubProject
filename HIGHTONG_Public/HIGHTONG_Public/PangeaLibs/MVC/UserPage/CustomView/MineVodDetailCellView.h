//
//  MineVodDetailCellView.h
//  HIGHTONG
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"


@interface MineVodDetailCellView : UIView
@property (strong, nonatomic) UIView        *bgV;//承载所有控件的背景view

@property (strong, nonatomic) UIImageView   *imageV;//海报图片

@property (strong, nonatomic) UILabel       *contentNameL;//点播视频内容名称

@property (strong, nonatomic) UILabel       *timeLabel;

@property (strong, nonatomic) UIImageView   *timeBackImg;//播放至断点背景图

@property (nonatomic, strong) VODDeleteButton *deleteBtn;

@property (nonatomic, strong) UIImageView   *deleteImg;

@property (nonatomic, strong) VODDeleteButton *highLigthBtn;

@property (nonatomic, strong) UIImageView   *honLineImg;//收藏cell横分割线


- (void)refreshHeightWithMovieName:(NSString *)movieName;

- (void)refreshHeightWithMovieName;

@end
