//
//  AccountCustomView.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/30.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "AccountCustomView.h"
#import "CalculateTextWidth.h"

@implementation AccountCustomView

-(instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

//- (id)initWithLeftTitle:(NSString *)title andMidImgName:(NSString *)imgName andMidLabText:(NSString *)labText
- (id)initWithLeftTitle:(NSString *)title andMidLabText:(NSString *)labText
//- (id)initWithLeftTitle:(NSString *)title andMidLabText:(NSString *)labText andRightImgName:(NSString *)rightName
{
    self = [super init];
    if (self) {
        
        WS(wss);
        
        _leftLab = [[UILabel alloc] init];
        _leftLab.text = title;
        _leftLab.textAlignment = NSTextAlignmentLeft;
        _leftLab.font = HT_FONT_SECOND;
        _leftLab.textColor = HT_COLOR_FONT_FIRST;
        _leftLab.backgroundColor = [UIColor clearColor];
        [self addSubview:_leftLab];
        
        CGSize labSize = [CalculateTextWidth sizeWithText:title font:HT_FONT_SECOND];
        int width = labSize.width+30*kDeviceRate;
        
        [_leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.width.equalTo(@(width));
            make.height.equalTo(wss.mas_height);
            make.left.equalTo(wss.mas_left).offset(12*kDeviceRate);
        }];
        
        
        _rightImg = [UIImageView new];
        [_rightImg setImage:[UIImage imageNamed:@"ic_user_arrow"]];
        [self addSubview:_rightImg];
        [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_leftLab.mas_centerY);
            make.width.equalTo(@(13*kDeviceRate));
            make.height.equalTo(@(13*kDeviceRate));
            make.right.equalTo(wss.mas_right).offset(-12*kDeviceRate);
        }];
        
        
        _midImg = [UIImageView new];
        _midImg.frame = CGRectMake((282*kDeviceRate), 6*kDeviceRate, 38*kDeviceRate, 38*kDeviceRate);//
        

//        if (IsLogin) {
//            
//            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"HeaderPhoto"];
//            if (data.length) {
//                
//                UIImage *image = [UIImage imageWithData:data];
//                [_midImg setImage:image];
//            }else{
//                [_midImg setImage:[UIImage imageNamed:@"img_user_small_default_avatar"]];
//            }
//            
//        }else{
//        
//            [_midImg setImage:[UIImage imageNamed:@"img_user_avatar"]];
//        }
       
        [self addSubview:_midImg];
        
        _midImg.layer.masksToBounds = YES;
        [_midImg.layer setCornerRadius:(_midImg.height / 2)];

            
        
        _midLab = [UILabel new];
        _midLab.text = labText;
        _midLab.textColor = HT_COLOR_FONT_SECOND;
        _midLab.backgroundColor = [UIColor clearColor];
        _midLab.font = HT_FONT_THIRD;
        _midLab.textAlignment = NSTextAlignmentRight;
        _midLab.adjustsFontSizeToFitWidth = YES;
        [self addSubview:_midLab];
        [_midLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_leftLab.mas_centerY);
//            make.width.equalTo(@(150*kRateSize));
            make.left.mas_equalTo(_leftLab.mas_right);
            make.height.mas_equalTo(_leftLab.mas_height);
            make.right.mas_equalTo(_rightImg.mas_left).offset(-30*kDeviceRate);
        }];
        
    }
    return self;
}

@end
