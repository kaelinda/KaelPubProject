//
//  ErrorAlertView.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/12/15.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "ErrorAlertView.h"
#import "CalculateTextWidth.h"


@implementation ErrorAlertView

- (id)init
{
    
    self = [super init];
    
    if (self) {
        
        //@(25*kRateSize
        
        WS(wss);
        
        UIImageView *errorImage = [UIImageView new];
        errorImage.image = [UIImage imageNamed:@"cuowu"];
        [self addSubview:errorImage];
        [errorImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(19*kRateSize));
            make.height.equalTo(@(17*kRateSize));
            make.left.equalTo(wss.mas_left).offset(18*kRateSize);
            make.centerY.equalTo(wss.mas_centerY);
        }];
        
        
        UILabel *tipLab = [UILabel new];
        tipLab.textAlignment = NSTextAlignmentLeft;
        tipLab.font = [UIFont systemFontOfSize:12];
        tipLab.textColor = UIColorFromRGB(0x666666);
        tipLab.text = @"错误提示：";
        CGSize tipSize = [CalculateTextWidth sizeWithText:tipLab.text font:[UIFont systemFontOfSize:12]];
        CGFloat tipWidth = tipSize.width;
        [self addSubview:tipLab];
        [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(errorImage.mas_right).offset(10*kRateSize);
            make.width.equalTo(@(tipWidth*kRateSize));
            make.top.equalTo(errorImage.mas_top);
            make.height.equalTo(errorImage.mas_height);
        }];
        
        
        self.alertLab = [UILabel new];
        self.alertLab.backgroundColor = [UIColor clearColor];
        self.alertLab.textAlignment = NSTextAlignmentLeft;
        self.alertLab.textColor = UIColorFromRGB(0x666666);
        self.alertLab.font = [UIFont systemFontOfSize:12];
        [self addSubview:self.alertLab];
        [self.alertLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(tipLab.mas_right);
            make.height.equalTo(tipLab.mas_height);
            make.width.equalTo(@(200*kRateSize));
            make.centerY.equalTo(tipLab.mas_centerY);
        }];
        
    }
    
    return self;
}


@end
