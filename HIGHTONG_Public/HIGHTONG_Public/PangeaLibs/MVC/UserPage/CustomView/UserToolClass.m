//
//  UserToolClass.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 16/3/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "UserToolClass.h"

@implementation UserToolClass

+ (NSMutableArray *)dateGroupWithArray:(NSArray *)array
{
    // global dateArr
    NSMutableArray *dateKeyArr = [NSMutableArray array];
    
    // global resArr
    NSMutableArray *resArr = [NSMutableArray array];
    
    // add keyDate to dateKeysArr
    for (NSDictionary *tempDic in array ) {
        
        NSString *dateStr = [tempDic objectForKey:@"playTime"];
        
        NSString *dateTurn = [self YearMouthDayWithString:dateStr];
        
        [dateKeyArr addObject:dateTurn];
    }
    NSSet *set = [NSSet setWithArray:dateKeyArr];
    
    // 去重排序
    NSArray *dateKeySorted = [self sortArr:[set allObjects]];
    
    // build data
    for (NSString *date in dateKeySorted) {
        
        NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
        NSMutableArray *subArr = [NSMutableArray array];
        
        for (NSDictionary *dict in array ) {
            if ([[self YearMouthDayWithString:[dict objectForKey:@"playTime"]] isEqualToString:date]) {
                [subArr addObject:dict];
            }
        }
        
        subDict[@"date"] = date;
        subDict[@"playList"] = subArr;
        
        [resArr addObject:subDict];
    }
    
    return resArr;
    
}


+ (NSMutableArray *)dependIfTheSameWeekCompareWithTodayDateGroupWithArray:(NSArray *)array
{
    // global dateArr
//    NSMutableArray *dateKeyArr = [NSMutableArray array];
    
    // global resArr
    NSMutableArray *resArr = [NSMutableArray array];
    
    // add keyDate to dateKeysArr
    
    NSMutableArray *oneWeekArr = [NSMutableArray array];
    NSMutableArray *moreThanWeekArr=  [NSMutableArray array];
    NSMutableDictionary *oneWeekDict = [NSMutableDictionary dictionary];
    NSMutableDictionary *moreThanWeekDict = [NSMutableDictionary dictionary];
    
    for (NSDictionary *tempDic in array ) {
        
        NSString *dateStr = [tempDic objectForKey:@"playTime"];
        
        NSString *dateTurn = [self YearMouthDayWithString:dateStr];

        if (([self intervalSinceNow:dateTurn] <= 7)) {//in a week
            
            [oneWeekArr addObject:tempDic];
        }else{//more than a week
        
            [moreThanWeekArr addObject:tempDic];
        }
        
    }

    oneWeekDict[@"date"] = @"一周内";
    oneWeekDict[@"playList"] = oneWeekArr;
    moreThanWeekDict[@"date"] = @"更早";
    moreThanWeekDict[@"playList"] = moreThanWeekArr;
    
    [resArr addObject:oneWeekDict];
    [resArr addObject:moreThanWeekDict];
    
    return resArr;
}


// 得到的结果为相差的天数
+ (int)intervalSinceNow:(NSString *) theDate
{
    
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    //（yyyy-MM-dd hh:mm:ss）
    [date setDateFormat:@"yyyy-MM-dd"];
    NSDate *d=[date dateFromString:theDate];
    
    NSInteger unitFlags = NSDayCalendarUnit| NSMonthCalendarUnit | NSYearCalendarUnit;
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [cal components:unitFlags fromDate:d];
    NSDate *newBegin  = [cal dateFromComponents:comps];
    
    // 当前时间
    NSCalendar *cal2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps2 = [cal2 components:unitFlags fromDate:[NSDate date]];
    NSDate *newEnd  = [cal2 dateFromComponents:comps2];
    
    
    NSTimeInterval interval = [newEnd timeIntervalSinceDate:newBegin];
    NSInteger resultDays=((NSInteger)interval)/(3600*24);
    
    return (int) resultDays;
}


+ (NSArray *)sortArr:(NSArray *)arr
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}


+ (NSString*)YearMouthDayWithString:(NSString*)string
{
    NSDateFormatter *fm = [[NSDateFormatter alloc]init];
    fm.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    NSDate *time = [fm dateFromString:string];
    fm.dateFormat = @"YYYY-MM-dd";
    NSString *StyleDate = [fm stringFromDate:time];
    return StyleDate;
}


+ (NSDate *)NsstringToNSDate:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSDate *date = [formatter dateFromString:string];
    
    return date;
}


//（当前日期的年月日）date转换成字符串
+ (NSString *)NSDateToNSString
{
    NSDate *dateNow = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSString *dateString = [formatter stringFromDate:dateNow];
    
    return dateString;
}


+ (NSString *)weekDayStringFromDate:(NSDate *)inputDate
{
    NSArray *weekdays = [NSArray arrayWithObjects:[NSNull null],@"星期日",@"星期一",@"星期二",@"星期三",@"星期四",@"星期五",@"星期六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Beijing"];
    
    [calendar setTimeZone:timeZone];
    
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}


//获取标准格式断点
+ (NSString *)getBreakPointWithTime:(NSString *)time
{
    long long breakPoint=[time longLongValue];
    if (breakPoint<60) {
        return [NSString stringWithFormat:@"00:00:%.2ld",(long)breakPoint];
    }
    
    else if (breakPoint<3600){
        return [NSString stringWithFormat:@"00:%.2lld:%.2lld",breakPoint/60,breakPoint%60];
        
    }
    
    else if (breakPoint<3600*60){
        return [NSString stringWithFormat:@"%.2lld:%.2lld:%.2lld",breakPoint/3600,(breakPoint%3600)/60,breakPoint%60];
    }
    return [NSString stringWithFormat:@"00:00:00"];
}


+ (NSString *)compareCurrentTimeWithPlayTime:(NSString *)playtime
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *dateNow = [NSDate date];
    
    NSDateFormatter *dm = [[NSDateFormatter alloc] init];
    
    [dm setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
//    NSDate *playDate = [self NsstringToNSDate:playtime];
    
    NSDate *playDate = [UserToolClass totalTimeNsstringToNSDate:playtime];
    
    long dd = (long)[playDate timeIntervalSince1970] - [dateNow timeIntervalSince1970];
    
    NSString *timeString = @"";
    
    NSInteger Htype = dd/3600;
    NSInteger Dtype =dd/86400;
    
    if (Htype<1) {
        
        long minites = dd/60+1;
        
        timeString = [NSString stringWithFormat:@"%ld分",minites];
        
        NSLog(@"/n哈哈哈时间是：%@",timeString);

    }
    
    if (Htype>=1 && Dtype<1) {
        
        long hour = dd/3600;
        
        long minites = dd%3600/60+1;
        
        if (minites == 0) {
            
            timeString = [NSString stringWithFormat:@"%ld小时",hour];
            
            NSLog(@"/n哈哈哈时间是：%@",timeString);

        }else{
            
            timeString = [NSString stringWithFormat:@"%ld小时%ld分",hour,minites];
            
            NSLog(@"/n哈哈哈时间是：%@",timeString);
        }
        
        //timeString = [NSString stringWithFormat:@"%ld小时%ld分",hour,minites];
    }
    
    if (Dtype>=1) {
        
        long day = dd/86400;
        
        NSLog(@"距播放天数%ld",day);
        
        long hour = dd%86400/3600;
        
        NSLog(@"距播放小时数%ld",hour);
        
        long minites = dd%86400/3600%3600/60+1;
        
        NSLog(@"距播放分钟数%ld",minites);
        
        //        if (minites == 0) {
        //
        //            timeString = [NSString stringWithFormat:@"%ld天%ld小时",day,hour];
        //
        //        }else
        
        
        if (hour == 0){
            
            timeString = [NSString stringWithFormat:@"%ld天",day];
            
        }else{
            
//            timeString = [NSString stringWithFormat:@"%ld天%ld小时",day,hour];
            
            timeString = [NSString stringWithFormat:@"%ld天",day];
        }
    }
    
    return timeString;
}


//字符串转日期
+ (NSDate *)totalTimeNsstringToNSDate:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];

    NSDate *date = [formatter dateFromString:string];

    return date;
}


+(float)fileSizeAtPath:(NSString *)path{
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    
    if([fileManager fileExistsAtPath:path]){
        
        long long size=[fileManager attributesOfItemAtPath:path error:nil].fileSize;
        
        return size/1024.0/1024.0;
    }
    
    return 0;
}


+(float)folderSizeAtPath:(NSString *)path{
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:path]) {
        
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        
        float folderSize = 0.00;

        for (NSString *fileName in childerFiles) {
            
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            
            folderSize +=[self fileSizeAtPath:absolutePath];
        }
        //SDWebImage框架自身计算缓存的实现
        folderSize+=[[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
        return folderSize;
    }
    return 0;
}


+(float)SDImageCacheSize
{
    float folderSize;
    
    //SDWebImage框架自身计算缓存的实现
    folderSize = [[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
    
    return folderSize;
}

+(void)clearSDImageCache
{
    [[SDImageCache sharedImageCache] clearDisk];
    
    [[SDImageCache sharedImageCache] clearMemory];
    
    //针对iOS7清不掉的处理
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

+(void)clearCache:(NSString *)path{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles) {
            //如有需要，加入条件，过滤掉不想删除的文件
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            [fileManager removeItemAtPath:absolutePath error:nil];
        }
    }
    [[SDImageCache sharedImageCache] cleanDisk];
}



@end
