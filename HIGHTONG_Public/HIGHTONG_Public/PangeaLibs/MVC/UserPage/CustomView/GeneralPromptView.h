//
//  GeneralPromptView.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extension.h"
#import "CalculateTextWidth.h"


@interface GeneralPromptView : UIView

@property (nonatomic, strong)UIImageView *showImg;

@property (nonatomic, strong)UILabel *titleLab;

- (id)initWithFrame:(CGRect)frame andImgName:(NSString *)imgName andTitle:(NSString *)title;

@end
