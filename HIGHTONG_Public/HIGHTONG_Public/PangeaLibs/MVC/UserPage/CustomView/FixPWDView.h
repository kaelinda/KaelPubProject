//
//  FixPWDView.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/1.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FixPWDView : UIView

@property (nonatomic,strong)UITextField *inputBlock;

@property (nonatomic,strong)UIButton *verityBtn;

- (id)initWithPlaceholderText:(NSString *)text;


@end
