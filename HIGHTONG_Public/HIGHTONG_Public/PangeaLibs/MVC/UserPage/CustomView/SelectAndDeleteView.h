//
//  SelectAndDeleteView.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 16/3/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
#import "VODDeleteButton.h"

@interface SelectAndDeleteView : UIView

@property (nonatomic, strong)VODDeleteButton *selectBtn;//全选按钮

@property (nonatomic, strong)VODDeleteButton *deleteBtn;//删除按钮

@property (nonatomic, strong)UILabel *selectLab;

@property (nonatomic, strong)UILabel *deleteLab;

@end
