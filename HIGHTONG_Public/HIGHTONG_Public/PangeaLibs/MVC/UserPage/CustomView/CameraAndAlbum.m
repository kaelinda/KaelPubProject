//
//  CameraAndAlbum.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/3/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "CameraAndAlbum.h"
#import "CalculateTextWidth.h"

@implementation CameraAndAlbum

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        self.frame = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        [self setUpUI];
    }
    
    return self;
}

- (void)show
{
//    [UIView animateWithDuration:0.75 animations:^{
        [self makeKeyAndVisible];
//    }];
}

- (void)setUpUI
{
    WS(wself);
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
    [self addSubview:backView];
    
    
    self.backBtn = [[UIButton_Block alloc] init];
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"bg_first_geture"] forState:UIControlStateNormal];
    [backView addSubview:self.backBtn];
    self.backBtn.frame = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-100*kDeviceRate-kBottomSafeSpace);
    
    
    UIImageView *whiteView = [UIImageView new];
    [whiteView setBackgroundColor:HT_COLOR_FONT_INVERSE];
    [backView addSubview:whiteView];
    [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(backView.mas_bottom);
        make.centerX.mas_equalTo(backView.mas_centerX);
        make.width.mas_equalTo(backView.mas_width);
        make.height.mas_equalTo(100*kDeviceRate+kBottomSafeSpace);
    }];
    whiteView.userInteractionEnabled = YES;
    
    
    UIImageView *honLine = [UIImageView new];
    [honLine setBackgroundColor:HT_COLOR_SPLITLINE];
    [whiteView addSubview:honLine];
    [honLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(whiteView.mas_width);
        make.height.mas_equalTo(@(0.5*kDeviceRate));
//        make.center.mas_equalTo(whiteView);
        make.centerX.mas_equalTo(whiteView.mas_centerX);
        make.bottom.mas_equalTo(whiteView.mas_bottom).offset(-kBottomSafeSpace-50*kDeviceRate);
    }];
    
    
    self.cameraBtn = [[UIButton_Block alloc] init];
    [whiteView addSubview:self.cameraBtn];
    [self.cameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(whiteView.mas_width);
        make.top.mas_equalTo(whiteView.mas_top);
        make.bottom.mas_equalTo(honLine.mas_top);
        make.centerX.mas_equalTo(whiteView.mas_centerX);
    }];
    
    
    self.albumBtn = [[UIButton_Block alloc] init];
    [whiteView addSubview:self.albumBtn];
    [self.albumBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(wself.cameraBtn);
        make.top.mas_equalTo(honLine.mas_bottom);
        make.centerX.mas_equalTo(whiteView.mas_centerX);
    }];
    
    
    
    UIImageView *camera = [UIImageView new];
    [camera setImage:[UIImage imageNamed:@"ic_user_camera"]];
    [self.cameraBtn addSubview:camera];
//    [camera mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@(22*kRateSize));
//        make.height.equalTo(@(17*kRateSize));
//        make.left.equalTo(whiteView.mas_left).offset(41*kRateSize);
//        make.top.equalTo(whiteView.mas_top).offset(13*kRateSize);
//    }];
    
    
    UIImageView *album = [UIImageView new];
    [album setImage:[UIImage imageNamed:@"ic_user_album"]];
    [self.albumBtn addSubview:album];
//    [album mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@(22*kRateSize));
//        make.height.equalTo(@(17*kRateSize));
//        make.top.equalTo(honLine.mas_bottom).offset(13*kRateSize);
//        make.left.equalTo(whiteView.mas_left).offset(41*kRateSize);
//    }];
    
    
    UILabel *cameraLab = [UILabel new];
    UILabel *albumLab = [UILabel new];
    
    cameraLab.text = @"拍照";
    albumLab.text = @"从相册里选择";
    
    cameraLab.textColor = HT_COLOR_FONT_FIRST;
    albumLab.textColor = HT_COLOR_FONT_FIRST;
    
    cameraLab.textAlignment = NSTextAlignmentCenter;
    albumLab.textAlignment = NSTextAlignmentCenter;
    
    cameraLab.font = HT_FONT_SECOND;
    albumLab.font = HT_FONT_SECOND;
    
    [self.cameraBtn addSubview:cameraLab];
    [self.albumBtn addSubview:albumLab];
    
    CGSize cameraLabSize = [CalculateTextWidth sizeWithText:cameraLab.text font:HT_FONT_SECOND];
    CGSize albumLabSize = [CalculateTextWidth sizeWithText:albumLab.text font:HT_FONT_SECOND];
    
    CGFloat cameraLabWidth = cameraLabSize.width;
    CGFloat albumLabWidth = albumLabSize.width;
    
    [cameraLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(wself.cameraBtn);
        make.width.mas_equalTo(cameraLabWidth+1);
        make.height.mas_equalTo(wself.cameraBtn.mas_height);
    }];
    
    [albumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(wself.albumBtn);
        make.width.mas_equalTo(albumLabWidth+1);
        make.height.mas_equalTo(wself.albumBtn.mas_height);
    }];
    
    [camera mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(22*kDeviceRate);
        make.height.mas_equalTo(16*kDeviceRate);
        make.right.mas_equalTo(cameraLab.mas_left).offset(-10*kDeviceRate);
        make.centerY.mas_equalTo(cameraLab.mas_centerY);
    }];
    

    [album mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(22*kDeviceRate);
        make.height.mas_equalTo(16*kDeviceRate);
        make.centerY.mas_equalTo(albumLab.mas_centerY);
        make.right.mas_equalTo(albumLab.mas_left).offset(-10*kDeviceRate);
    }];
}

@end
