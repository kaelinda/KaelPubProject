//
//  UserToolClass.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 16/3/2.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserToolClass : NSObject

/**
 *  根据是否是同一天对原有数组个体进行重新分组，组成新的分好组的数组
 *
 *  @param array 无区别原始数组
 *
 *  @return 返回的数组元素是由“date”和"playTime"构成的字典所组成的
 */
//+ (NSMutableArray *)dateGroupWithArray:(NSArray *)array;


/**
 *  数组中的每一个元素通过与“今天”对比是否是七天内为根据，重新排列组成新数组
 *
 *  @param array 原始数组
 *
 *  @return 七天为分界线重排好的数组
 */
+ (NSMutableArray *)dependIfTheSameWeekCompareWithTodayDateGroupWithArray:(NSArray *)array;


/**
 *  对原有数组进行降序排序
 *
 *  @param arr 未排序的原有数组
 *
 *  @return 排好序的新数组
 */
+ (NSArray *)sortArr:(NSArray *)arr;


/**
 *  去掉时分秒返回年月日的字符串
 *
 *  @param string 年月日时分秒的字符串
 *
 *  @return 年月日字符串
 */
+ (NSString*)YearMouthDayWithString:(NSString*)string;


/**
 *  stringClass changeto dateClass
 *
 *  @param string 日期字符串（年月日）
 *
 *  @return date类型
 */
+ (NSDate *)NsstringToNSDate:(NSString *)string;


/**
 *  （当前日期的年月日）date转换成字符串
 *
 *  @param NSString “今天”、date类型
 *
 *  @return ”今天“、string类型
 */
+ (NSString *)NSDateToNSString;


/**
 *  dateClass changeTo "星期xx"
 *
 *  @param inputDate weekDate
 *
 *  @return 星期xx(stringClass)
 */
+ (NSString *)weekDayStringFromDate:(NSDate *)inputDate;


/**
 *  获取标准格式断点
 *
 *  @param NSString 秒数字符串
 *
 *  @return @"00:00:00"形式的字符串
 */
+ (NSString *)getBreakPointWithTime:(NSString *)time;


/**
 *  当前时间字符串距离一个给定时间字符串的差值
 *
 *  @param playtime 给定的时间字符串
 *
 *  @return 差值字符串
 */
+ (NSString *)compareCurrentTimeWithPlayTime:(NSString *)playtime;


/**
 *  stringClass changeto dateClass
 *
 *  @param string 月日年时分秒  字符串
 *
 *  @return date
 */
+ (NSDate *)totalTimeNsstringToNSDate:(NSString *)string;


/**
 *  计算单个文件缓存大小的方法
 *
 *  @param path 文件路径
 *
 *  @return float类型的具体大小
 */
+(float)fileSizeAtPath:(NSString *)path;


/**
 *  计算整个文件夹缓存大小的方法
 *
 *  @param path 文件夹路径
 *
 *  @return float类型的具体大小
 */
+(float)folderSizeAtPath:(NSString *)path;


+(float)SDImageCacheSize;


+(void)clearSDImageCache;



/**
 *  清理缓存的方法
 *
 *  @param path 路径
 */
+(void)clearCache:(NSString *)path;


@end
