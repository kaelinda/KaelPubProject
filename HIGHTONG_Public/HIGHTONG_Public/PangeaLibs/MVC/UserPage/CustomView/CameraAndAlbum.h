//
//  CameraAndAlbum.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/3/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

@interface CameraAndAlbum : UIWindow

@property (nonatomic, strong)UIButton_Block *backBtn;

@property (nonatomic, strong)UIButton_Block *cameraBtn;

@property (nonatomic, strong)UIButton_Block *albumBtn;

- (id)init;

- (void)show;

@end
