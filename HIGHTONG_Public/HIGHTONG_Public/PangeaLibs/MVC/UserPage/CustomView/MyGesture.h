//
//  MyGesture.h
//  Mine
//
//  Created by Alaca on 14-8-25.
//  Copyright (c) 2014年 zzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGesture : UITapGestureRecognizer
@property (nonatomic,assign) NSInteger tag;

@property (nonatomic,assign) NSInteger sectionNum;

//登陆用户，点击预定按钮以后要在回调中做按钮状态以及数据源的改变。用这个btn。tag记录数组中位置，用于清除,selectedEventID用于删除预定的eventID；
@property (strong ,nonatomic)NSString *selectedEventID;

@property (strong ,nonatomic)NSString *dataStr;

@property (copy ,nonatomic) NSString *urlLink;

@end
