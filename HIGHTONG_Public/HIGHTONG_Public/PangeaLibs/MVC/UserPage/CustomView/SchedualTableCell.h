//
//  SchedualTableCell.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"
//#import "UIButton_Block.h"


//typedef void(^SCHEDULEBLOCK)(NSDictionary *);


@interface SchedualTableCell : UITableViewCell

//@property (nonatomic, copy)SCHEDULEBLOCK scheduleBlock;

@property (nonatomic, strong)VODDeleteButton *backBtn;

@property (nonatomic, strong)UIImageView *linkImg;//台标

@property (nonatomic, strong)UILabel *epgName;//电视台名称

@property (nonatomic, strong)UILabel *programName;//节目名称

@property (nonatomic, strong)UILabel *playTime;//节目播放起始时间

@property (nonatomic, strong)UILabel *sepPlayTime;//距离播放的时间

@property (nonatomic, strong)UIImageView *longLine;//长分割线

@property (nonatomic, strong)UIImageView *alarmImg;//闹钟图片

//@property (nonatomic, strong)UIButton *deleteBtn;

@property (nonatomic, strong)UIImageView *deleteImg;//删除图片

//@property (nonatomic, strong)UIButton_Block *backBtn;//为设置高亮图片需求，cell背景按钮



- (void)refreshCellWithDictionary:(NSDictionary *)diction andIfEdit:(BOOL)ifEdit;


@end
