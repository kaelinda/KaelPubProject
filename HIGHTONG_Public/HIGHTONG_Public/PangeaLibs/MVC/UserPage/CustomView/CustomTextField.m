//
//  CustomTextField.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/21.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

//...ww 控制placeholder的位置
- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGRect inset;
    inset = [super placeholderRectForBounds:bounds];
    inset = CGRectMake(bounds.origin.x+50*kRateSize, bounds.origin.y, bounds.size.width, bounds.size.height);
    return inset;
}

//...ww 控制显示文本的位置
- (CGRect)textRectForBounds:(CGRect)bounds
{
    [super textRectForBounds:bounds];
    CGRect inset = CGRectMake(bounds.origin.x+50*kRateSize, bounds.origin.y, bounds.size.width, bounds.size.height);
    return inset;
}

//...ww 控制编辑文本的位置
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    [super editingRectForBounds:bounds];
    CGRect inset = CGRectMake(bounds.origin.x+50*kRateSize, bounds.origin.y, bounds.size.width, bounds.size.height);
    return inset;
}

//- (CGRect)clearButtonRectForBounds:(CGRect)bounds
//{
//    [super clearButtonRectForBounds:bounds];
//    CGRect inset = CGRectMake(bounds.origin.x-50*kRateSize, bounds.origin.y, bounds.size.width, bounds.size.height);
//    return inset;
//    
//
//}

//...ww控制placeHolder的颜色、字体
//- (void)drawPlaceholderInRect:(CGRect)rect
//{
//    //[[UIColorFromRGB(898989)]setFill];
//    [[self placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:15]];
//}

-(void)layoutSublayersOfLayer:(CALayer *)layer{

//#if 1
     //[super layoutSublayersOfLayer:layer];
//#endif
}
//
//- (void)layoutSubviews{
//    [super layoutSubviews];
//}


@end
