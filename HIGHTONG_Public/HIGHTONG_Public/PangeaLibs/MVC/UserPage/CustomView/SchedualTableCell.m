//
//  SchedualTableCell.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "SchedualTableCell.h"
#import "CalculateTextWidth.h"
#import "Tool.h"
#import "UserToolClass.h"


@implementation SchedualTableCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //cell高度73
        
        //cell背景高亮图片
        self.backBtn = [[VODDeleteButton alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60*kDeviceRate)];
        [self.contentView addSubview:self.backBtn];
        [self.backBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_schedule_pressed"] forState:UIControlStateHighlighted];
        [self.backBtn setBackgroundColor:[UIColor clearColor]];
        
        WS(wss);
        
        
//        _backBtn = [[UIButton_Block alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 50*kRateSize)];
//        [self.contentView addSubview:_backBtn];
//        [_backBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_schedule_pressed"] forState:UIControlStateHighlighted];
//        [_backBtn setBackgroundColor:[UIColor clearColor]];
        
        
        
        //台标
        self.linkImg = [UIImageView new];
        self.linkImg.contentMode = UIViewContentModeScaleAspectFit;
        [self.backBtn addSubview:self.linkImg];
        [self.linkImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(32.5*kDeviceRate));
            make.height.equalTo(@(28*kDeviceRate));
            make.top.equalTo(wss.backBtn.mas_top).offset(10*kDeviceRate);
            make.left.equalTo(@(33.5*kDeviceRate));//@((100-32.5)/2*kRateSize)
        }];
        
        
        //电视台名称
        self.epgName = [UILabel new];
        self.epgName.font = HT_FONT_FOURTH;
        self.epgName.textColor = HT_COLOR_FONT_FIRST;
        self.epgName.textAlignment = NSTextAlignmentCenter;
//        self.epgName.centerY = self.linkImg.centerY;
//        self.epgName.frame = CGRectMake(0*kRateSize, CGRectGetMaxY(self.linkImg.frame)+1*kRateSize, 90*kRateSize, 10*kRateSize);
        [self.backBtn addSubview:self.epgName];
        [self.epgName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(wss.linkImg.mas_centerX);
            make.top.equalTo(wss.linkImg.mas_bottom);//.offset(1*kRateSize);;
            make.width.equalTo(@(100*kDeviceRate));
            make.bottom.equalTo(wss.backBtn.mas_bottom).offset(-10*kDeviceRate);
        }];
        
        
        //节目名称
        self.programName = [UILabel new];
        self.programName.font = HT_FONT_THIRD;
        self.programName.textColor = HT_COLOR_FONT_FIRST;
        self.programName.textAlignment = NSTextAlignmentLeft;
//        self.programName.frame = CGRectMake(90*kRateSize, 10.5*kRateSize, 175*kRateSize, 11*kRateSize);//左90，右47
        [self.backBtn addSubview:self.programName];
        [self.programName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wss.linkImg.mas_right).offset(33.5*kDeviceRate);
//            make.top.mas_equalTo(wss.backBtn.mas_top).offset(13*kDeviceRate);
            make.centerY.mas_equalTo(wss.linkImg.mas_centerY);
            make.width.mas_equalTo(175*kDeviceRate);
            make.height.mas_equalTo(16*kDeviceRate);
        }];
        //[self.programName setBackgroundColor:[UIColor redColor]];
        
        
        //节目播放起始时间
        self.playTime = [UILabel new];
        self.playTime.font = HT_FONT_FOURTH;
        self.playTime.textColor = HT_COLOR_FONT_SECOND;
        self.playTime.textAlignment = NSTextAlignmentLeft;
        self.playTime.adjustsFontSizeToFitWidth = YES;
        
        NSString *str = @"20:08-22:16";
        CGSize strSize = [CalculateTextWidth sizeWithText:str font:HT_FONT_FOURTH];
        int widthWW = strSize.width;
        
//        self.playTime.frame = CGRectMake(self.programName.frame.origin.x, CGRectGetMaxY(self.programName.frame)+8*kDeviceRate, widthWW, 13*kDeviceRate);
        [self.backBtn addSubview:self.playTime];
        [self.playTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wss.programName.mas_left);
//            make.top.mas_equalTo(wss.programName.mas_bottom).offset(8*kDeviceRate);
            make.centerY.mas_equalTo(wss.epgName.mas_centerY);
            make.width.mas_equalTo(widthWW);//*kDeviceRate
            make.height.mas_equalTo(14*kDeviceRate);
        }];
        
        //长的分割线
        self.longLine = [UIImageView new];
        self.longLine.frame = CGRectMake(0, self.backBtn.frame.size.height-0.5*kDeviceRate, kDeviceWidth, 0.5);
        self.longLine.backgroundColor = HT_COLOR_SPLITLINE;
//        [self.longLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
        [self.backBtn addSubview:self.longLine];
        
        
        //闹钟图片
        //新版有两种图片，需要判断
        _alarmImg = [[UIImageView alloc] init];
        [self.backBtn addSubview:_alarmImg];
        [_alarmImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.playTime.mas_right).offset(10*kDeviceRate);
            make.width.height.equalTo(@(12*kDeviceRate));
            make.centerY.equalTo(wss.playTime.mas_centerY);
        }];
        
        
        //距离播放时间
        //新版字体颜色有两种颜色了，需要判断
        self.sepPlayTime = [[UILabel alloc] init];
        self.sepPlayTime.font = HT_FONT_FOURTH;
        self.sepPlayTime.textAlignment = NSTextAlignmentLeft;
        [self.backBtn addSubview:self.sepPlayTime];
//        self.sepPlayTime.userInteractionEnabled = YES;
        
//        int timeWidth = self.backBtn.frame.size.width-CGRectGetMaxX(_alarmImg.frame)-5*kDeviceRate-22*kDeviceRate-12*kDeviceRate-10*kDeviceRate;
        int timeWidth = self.backBtn.frame.size.width-112.5*kDeviceRate-widthWW-5*kDeviceRate-12*kDeviceRate-30*kDeviceRate-10*kDeviceRate;//-12*kDeviceRate
        [self.sepPlayTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.alarmImg.mas_right).offset(2.5*kDeviceRate);
            make.centerY.equalTo(wss.alarmImg.mas_centerY);
            make.height.equalTo(@(13*kDeviceRate));
            make.width.equalTo(@(timeWidth));//*kDeviceRate
        }];
        
        
        
//        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.deleteBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"] forState:UIControlStateSelected];
//        [self.deleteBtn setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"] forState:UIControlStateNormal];
//        [self.backBtn addSubview:self.deleteBtn];
//        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.equalTo(@(22*kRateSize));
//            make.right.equalTo(wss.backBtn.mas_right).offset(-25*kRateSize);
//            make.centerY.equalTo(wss.backBtn.mas_centerY);
//        }];
        
        self.deleteImg = [UIImageView new];
        self.deleteImg.image = [UIImage imageNamed: @"sl_user_common_del_checkbox_normal"];
        [self.backBtn addSubview:self.deleteImg];
        [self.deleteImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@(30*kDeviceRate));
            make.right.equalTo(wss.backBtn.mas_right).offset(-12*kDeviceRate);
            make.centerY.equalTo(wss.backBtn.mas_centerY);
        }];
    }
    return self;
}


- (void)refreshCellWithDictionary:(NSDictionary *)diction andIfEdit:(BOOL)ifEdit
{//规范情况，在取值之前，应先判断字典里的key值是否存在，存在才赋值

    [self.linkImg sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([diction objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_tv_pangea_logo_loading"]];
    self.epgName.text = [diction objectForKey:@"serviceName"];
    self.programName.text = [diction objectForKey:@"eventName"];
    
    
    NSString *beginTime = [diction objectForKey:@"startTime"];
    NSString *overTime = [diction objectForKey:@"endTime"];
    
    NSInteger mark = [Tool typePlayStartTime:beginTime andEndTime:overTime];
    if (mark == -1) {//回看
        
        self.sepPlayTime.text = @"可以回看";
        self.sepPlayTime.textColor = HT_COLOR_FONT_WARNING;
        self.alarmImg.image = nil;
        
    }else if (mark == 0){//直播
        
        [self.alarmImg setImage:[UIImage imageNamed:@"ic_user_order_living"]];
        
        self.sepPlayTime.textColor = App_selected_color;
        self.sepPlayTime.text = @"正在直播中...";
        
    }else{//预定
        
        [self.alarmImg setImage:[UIImage imageNamed:@"ic_user_order_waiting"]];
        
        NSString *time = [UserToolClass compareCurrentTimeWithPlayTime:beginTime];
        NSLog(@"%@",time);
        
        self.sepPlayTime.textColor = HT_COLOR_FONT_WARNING;
        self.sepPlayTime.text = [NSString stringWithFormat:@"距播放还有:%@",time];
    }
    
    
    NSString *noSecondBeginTime = [Tool MMddssToyyMMddStringFromDate:beginTime andFormaterString:@"HH:mm"];
    NSString *noSecondOverTime = [Tool MMddssToyyMMddStringFromDate:overTime andFormaterString:@"HH:mm"];
    self.playTime.text = [NSString stringWithFormat:@"%@ - %@",noSecondBeginTime,noSecondOverTime];

    

    if (ifEdit) {
        
        self.deleteImg.hidden = NO;
    }else{
        
        self.deleteImg.hidden = YES;
    }
    
    
//    _backBtn.Click = ^(UIButton_Block *btn, NSString *name)
//    {
//        if (wself.scheduleBlock) {
//            
//            wself.scheduleBlock(diction);
//        }else{
//            
//            NSLog(@"没有实现预约cell的点击");
//        }
//    };
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
