//
//  MineHealthyTableViewCell.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MineHealthyTableViewCell.h"

@implementation MineHealthyTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self = [super  initWithStyle:style reuseIdentifier:reuseIdentifier ]) {
       
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{

    UIView *superView = self.contentView;
    
    self.leftView = [MineHealthyView new];
    [superView addSubview:self.leftView];
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(superView.mas_top).offset(6*kDeviceRate);
        make.width.mas_equalTo(178.5*kDeviceRate);
        make.height.mas_equalTo(140*kDeviceRate);
        
    }];
    
    
    self.rightView = [MineHealthyView new];
    [superView addSubview:self.rightView];
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-6*kDeviceRate);
        make.top.equalTo(superView.mas_top).offset(6*kDeviceRate);
        make.width.mas_equalTo(178.5*kDeviceRate);
        make.height.mas_equalTo(140*kDeviceRate);
    }];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
