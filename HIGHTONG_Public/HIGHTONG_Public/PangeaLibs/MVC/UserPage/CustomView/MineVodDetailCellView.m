//
//  MineVodDetailCellView.m
//  HIGHTONG
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "MineVodDetailCellView.h"
#import <CoreText/CoreText.h>


@implementation MineVodDetailCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self makeUpUI];
    }
    return self;
}

- (void)makeUpUI
{
    WS(wSelf);
    
    //承载所有控件的背景view
    self.bgV = [[UIView alloc] init];
    self.bgV.userInteractionEnabled = YES;
    self.bgV.backgroundColor = UIColorFromRGB(0xffffff);
    [self addSubview:self.bgV];
    [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wSelf);
    }];
    
    
    //整个cell的点击高亮图
    self.highLigthBtn = [[VODDeleteButton alloc] init];
    [self.bgV addSubview:_highLigthBtn];
    UIImage *image = [UIImage imageNamed:@"bg_vod_playbill_hold_img"];
    [_highLigthBtn setBackgroundImage:image forState:UIControlStateHighlighted];
    [self.highLigthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wSelf.bgV);
    }];
    
    
    //海报图
    self.imageV = [[UIImageView alloc] init];
    self.imageV.userInteractionEnabled = NO;
    [self.imageV setBackgroundColor:UIColorFromRGB(0xe3e2e3)];
    [self.bgV addSubview:self.imageV];
    //        self.imageV.layer.masksToBounds = YES;
    //        self.imageV.layer.cornerRadius = 4.5;
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70*kDeviceRate);
        make.height.mas_equalTo(95*kDeviceRate);
        make.left.mas_equalTo(wSelf.bgV.mas_left).offset(6*kDeviceRate);
        make.centerY.mas_equalTo(wSelf.bgV.mas_centerY);
    }];
    
    
    //影片等名字
    self.contentNameL = [[UILabel alloc] init];
    self.contentNameL.numberOfLines = 2;
    [self.contentNameL setBackgroundColor:[UIColor clearColor]];
    self.contentNameL.textColor = HT_COLOR_FONT_FIRST;
    [self.contentNameL setFont:HT_FONT_THIRD];
    self.contentNameL.textAlignment = NSTextAlignmentLeft;
    [self.bgV addSubview:self.contentNameL];
    [self.contentNameL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wSelf.imageV.mas_right).offset(5*kDeviceRate);
        make.top.mas_equalTo(wSelf.bgV.mas_top).offset(35*kDeviceRate);
        make.height.mas_equalTo(40*kDeviceRate);
        make.right.mas_equalTo(wSelf.bgV.mas_right);
    }];
    
    
    //播放至断点lab
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.textColor = HT_COLOR_FONT_SECOND;
    self.timeLabel.font = HT_FONT_FOURTH;
    [self.bgV addSubview:self.timeLabel];
    //        self.timeLabel.userInteractionEnabled = YES;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wSelf.contentNameL.mas_left);
        make.top.mas_equalTo(wSelf.contentNameL.mas_bottom).offset(15*kDeviceRate);
        make.right.mas_equalTo(wSelf.bgV.mas_right).offset(-5*kDeviceRate);
        make.height.mas_equalTo(15*kDeviceRate);
    }];
    
    
    self.deleteImg = [UIImageView new];
    [self.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
    [self.bgV addSubview:self.deleteImg];
    [self.deleteImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30*kDeviceRate);
        make.top.mas_equalTo(wSelf.bgV.mas_top);//.offset(5*kRateSize);
        make.right.mas_equalTo(wSelf.bgV.mas_right);//.offset(-5*kRateSize);
    }];
    
    
//    self.honLineImg = [UIImageView new];
//    [self.honLineImg setBackgroundColor:App_line_color];
//    [self.bgV addSubview:self.honLineImg];
//    [self.honLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(wSelf.bgV.mas_width);
//        make.height.mas_equalTo(0.5*kDeviceRate);
//        make.centerX.mas_equalTo(wSelf.bgV.mas_centerX);
//        make.bottom.mas_equalTo(wSelf.bgV.mas_bottom);
//    }];
}

//- (void)setUpUIHeight
//{
//    WS(wSelf);
//
//    //承载所有控件的背景view
//    [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(wSelf);
//    }];
//
//
//    //整个cell的点击高亮图
//    [self.highLigthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(wSelf.bgV);
//    }];
//
//
//    //海报图
//    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(70*kDeviceRate);
//        make.height.mas_equalTo(95*kDeviceRate);
//        make.left.mas_equalTo(wSelf.bgV.mas_left).offset(6*kDeviceRate);
//        make.centerY.mas_equalTo(wSelf.bgV.mas_centerY);
//    }];
//
//
//    //影片等名字
//    NSArray *lineCountArr = [self getLinesArrayOfStringInLabel:self.contentNameL];
//
//    if (lineCountArr.count==1) {
//        [self.contentNameL mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(wSelf.imageV.mas_right).offset(5*kDeviceRate);
//            make.top.mas_equalTo(wSelf.bgV.mas_top).offset(35*kDeviceRate);
//            make.height.mas_equalTo(20*kDeviceRate);
//            make.right.mas_equalTo(wSelf.bgV.mas_right);
//        }];
//    }else if (lineCountArr.count == 2) {
//        [self.contentNameL mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(wSelf.imageV.mas_right).offset(5*kDeviceRate);
//            make.top.mas_equalTo(wSelf.bgV.mas_top).offset(35*kDeviceRate);
//            make.height.mas_equalTo(40*kDeviceRate);
//            make.right.mas_equalTo(wSelf.bgV.mas_right);
//        }];
//    }else{
//
//        return;
//    }
//
//
//    //播放至断点lab
//    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(wSelf.contentNameL.mas_left);
//        make.top.mas_equalTo(wSelf.contentNameL.mas_bottom).offset(15*kDeviceRate);
//        make.right.mas_equalTo(wSelf.bgV.mas_right).offset(-5*kDeviceRate);
//        make.height.mas_equalTo(15*kDeviceRate);
//    }];
//
//
//    [self.deleteImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(30*kDeviceRate);
//        make.top.mas_equalTo(wSelf.bgV.mas_top);//.offset(5*kRateSize);
//        make.right.mas_equalTo(wSelf.bgV.mas_right);//.offset(-5*kRateSize);
//    }];
//
//
//
//    [self.honLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(wSelf.bgV.mas_width);
//        make.height.mas_equalTo(0.5*kDeviceRate);
//        make.centerX.mas_equalTo(wSelf.bgV.mas_centerX);
//        make.bottom.mas_equalTo(wSelf.bgV.mas_bottom).offset(-0.5*kDeviceRate);
//    }];
//
//}

- (NSArray *)getLinesArrayOfStringInLabel:(UILabel *)label{
    NSString *text = [label text];
    UIFont *font = [label font];
    CGRect rect = [label frame];
    
    CTFontRef myFont = CTFontCreateWithName(( CFStringRef)([font fontName]), [font pointSize], NULL);
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge  id)myFont range:NSMakeRange(0, attStr.length)];
    CFRelease(myFont);
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString(( CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    NSArray *lines = ( NSArray *)CTFrameGetLines(frame);
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    for (id line in lines) {
        CTLineRef lineRef = (__bridge  CTLineRef )line;
        CFRange lineRange = CTLineGetStringRange(lineRef);
        NSRange range = NSMakeRange(lineRange.location, lineRange.length);
        NSString *lineString = [text substringWithRange:range];
        CFAttributedStringSetAttribute((CFMutableAttributedStringRef)attStr, lineRange, kCTKernAttributeName, (CFTypeRef)([NSNumber numberWithFloat:0.0]));
        CFAttributedStringSetAttribute((CFMutableAttributedStringRef)attStr, lineRange, kCTKernAttributeName, (CFTypeRef)([NSNumber numberWithInt:0.0]));
        //NSLog(@"''''''''''''''''''%@",lineString);
        [linesArray addObject:lineString];
    }
    
    CGPathRelease(path);
    CFRelease( frame );
    CFRelease(frameSetter);
    return (NSArray *)linesArray;
}

- (void)refreshHeightWithMovieName
{
    WS(wSelf);
    
    NSArray *lineCountArr = [self getLinesArrayOfStringInLabel:self.contentNameL];
    if (lineCountArr.count==1) {
        [self.contentNameL mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wSelf.imageV.mas_right).offset(5*kDeviceRate);
            make.top.mas_equalTo(wSelf.bgV.mas_top).offset(35*kDeviceRate);
            make.height.mas_equalTo(20*kDeviceRate);
            make.right.mas_equalTo(wSelf.bgV.mas_right);
        }];
    }
    if (lineCountArr.count == 2) {
        [self.contentNameL mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wSelf.imageV.mas_right).offset(5*kDeviceRate);
            make.top.mas_equalTo(wSelf.bgV.mas_top).offset(35*kDeviceRate);
            make.height.mas_equalTo(40*kDeviceRate);
            make.right.mas_equalTo(wSelf.bgV.mas_right);
        }];
    }else{
    
        return;
    }
    
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wSelf.contentNameL.mas_left);
        make.top.mas_equalTo(wSelf.contentNameL.mas_bottom).offset(15*kDeviceRate);
        make.right.mas_equalTo(wSelf.bgV.mas_right).offset(-5*kDeviceRate);
        make.height.mas_equalTo(15*kDeviceRate);
    }];
}


@end
