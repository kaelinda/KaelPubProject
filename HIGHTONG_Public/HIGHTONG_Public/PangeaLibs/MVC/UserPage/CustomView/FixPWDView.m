//
//  FixPWDView.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/1.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FixPWDView.h"

@implementation FixPWDView

- (instancetype)init
{
    self = [super init];
    if (self) {
        //
    }
    return self;
}

- (id)initWithPlaceholderText:(NSString *)text
{
    WS(wss);
    self = [super init];
    if (self) {
        
        UIImageView *backImg = [UIImageView new];
        [backImg setImage:[UIImage imageNamed:@"mine_textfield_back"]];
        [self addSubview:backImg];
        [backImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(wss);
            make.center.equalTo(wss);
        }];
        
        self.inputBlock = [[UITextField alloc] init];
        self.inputBlock.returnKeyType = UIReturnKeyDone;
        self.inputBlock.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        if (isAfterIOS6) {
            self.inputBlock.textAlignment = NSTextAlignmentLeft;
            
        }else{
            self.inputBlock.textAlignment = NSTextAlignmentLeft;
            
        }
        self.inputBlock.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.inputBlock.borderStyle = UITextBorderStyleNone;
        self.inputBlock.placeholder = text;

        [self.inputBlock setValue:[UIFont boldSystemFontOfSize:15*kRateSize] forKeyPath:@"_placeholderLabel.font"];
        
        self.inputBlock.borderStyle =  UITextBorderStyleNone;
        
        
        if (isBeforeIOS7) {
            
        }else{
            
            self.inputBlock.tintColor = App_selected_color;
        }

        
        //self.inputBlock.delegate = self;
        [self addSubview:self.inputBlock];
        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backImg.mas_left).offset(5*kRateSize);
            make.height.equalTo(backImg.mas_height);
            make.centerY.equalTo(backImg.mas_centerY);
            make.right.equalTo(backImg.mas_right);
        }];
        
    }
    return self;
}


-(void)layoutSubviews{
    
    [super layoutSubviews];
    
}

@end
