//
//  MineFvoriteChannelCell.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "MineFvoriteChannelCell.h"
#import "CalculateTextWidth.h"

@implementation MineFvoriteChannelCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        
        _backBtn = [[VODDeleteButton alloc] initWithFrame:CGRectMake(12*kDeviceRate, 10*kDeviceRate, 351*kDeviceRate, 50*kDeviceRate)];
        [_backBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_collection_item"] forState:UIControlStateNormal];
        [_backBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_favchannel_del_unselect"] forState:UIControlStateHighlighted];
        [self.contentView addSubview:_backBtn];
  
        self.linkImg = [[UIImageView alloc] initWithFrame:CGRectMake(100*kDeviceRate, 10*kDeviceRate, 40*kDeviceRate, 30*kDeviceRate)];
        self.linkImg.contentMode = UIViewContentModeScaleAspectFit;
        [_backBtn addSubview:self.linkImg];
        
        
        self.linkName = [[UILabel alloc] init];
        self.linkName.textColor = HT_COLOR_FONT_FIRST;
        self.linkName.font = HT_FONT_SECOND;
        self.linkName.textAlignment = NSTextAlignmentLeft;

        [_backBtn addSubview:self.linkName];
        
        [self.linkName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.linkImg.mas_right).offset(5*kDeviceRate);
            make.centerY.equalTo(_backBtn.mas_centerY);
        }];
        
    }
    return self;
}

- (void)backBtnSenderDown
{
    _backBtn.selected = !_backBtn.selected;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
