//
//  MineFvoriteEditChannelCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MineFvoriteEditChannelCell.h"

@implementation MineFvoriteEditChannelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //cell按钮
        _backImg = [[UIImageView alloc] initWithFrame:CGRectMake(12*kDeviceRate, 10*kDeviceRate, 351*kDeviceRate, 50*kDeviceRate)];
        [_backImg setImage:[UIImage imageNamed:@"bg_user_favlist_cell"]];
        self.contentView.backgroundColor = HT_COLOR_BACKGROUND_APP;
        _backImg.userInteractionEnabled = YES;
        [self.contentView addSubview:_backImg];
        
        //频道视图
        _channelView = [[UIView alloc]init];
        [_backImg addSubview:_channelView];
        
        [_channelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left);
            make.top.equalTo(_backImg.mas_top);
            make.height.equalTo(@(50*kDeviceRate));
            make.width.equalTo(@(170*kDeviceRate));
            
        }];
        
        //频道图像
        _linkImg = [[UIImageView alloc]init];
        [_channelView addSubview:_linkImg];
        
        [_linkImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backImg.mas_left).offset(10*kDeviceRate);
            make.centerY.equalTo(_channelView.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(40*kDeviceRate));
            
        }];
        _linkImg.contentMode = UIViewContentModeScaleAspectFit;
        
        //频道名称
        _linkName = [[UILabel alloc]init];
        _linkName.textColor = HT_COLOR_FONT_FIRST;
        _linkName.font = HT_FONT_SECOND;
        _linkName.textAlignment = NSTextAlignmentLeft;
        [_channelView addSubview:_linkName];
        
        [_linkName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_linkImg.mas_right).offset(10*kDeviceRate);
            make.centerY.equalTo(_channelView.mas_centerY);
            make.width.equalTo(@(110*kDeviceRate));
        }];
        
        //置顶视图
        _topImg = [[UIImageView alloc]init];
        [_topImg setImage:[UIImage imageNamed:@"btn_user_favchannel_top"]];
        [_backImg addSubview:_topImg];
        _topImg.userInteractionEnabled = YES;
        
        [_topImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_channelView.mas_right);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(30*kDeviceRate));
            
        }];
        
        //拖动视图
        _moveImg = [[UIImageView alloc]init];
        [_moveImg setImage:[UIImage imageNamed:@"btn_user_favchannel_sort"]];
        _moveImg.userInteractionEnabled = YES;
        [_backImg addSubview:_moveImg];
        
        [_moveImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_topImg.mas_right).offset(36*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(30*kDeviceRate));
            
        }];
        
        //选中视图
        _choiceImg = [[UIImageView alloc]init];
        [_choiceImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        _choiceImg.userInteractionEnabled = YES;
        [_backImg addSubview:_choiceImg];

        
        [_choiceImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_moveImg.mas_right).offset(36*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(30*kDeviceRate));
            
        }];
        
        //未选中视图
        _UnChoiceImg = [[UIImageView alloc]init];
        [_UnChoiceImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        _UnChoiceImg.userInteractionEnabled = YES;
        [_backImg addSubview:_UnChoiceImg];
        
        [_UnChoiceImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_moveImg.mas_right).offset(36*kDeviceRate);
            make.centerY.equalTo(_backImg.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(30*kDeviceRate));
            
        }];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
