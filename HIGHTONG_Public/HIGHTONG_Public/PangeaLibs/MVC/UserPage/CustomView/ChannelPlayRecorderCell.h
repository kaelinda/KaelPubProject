//
//  ChannelPlayRecorderCell.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"

@interface ChannelPlayRecorderCell : UITableViewCell

@property (nonatomic, strong)VODDeleteButton *backBtn;

@property (nonatomic, strong)UIImageView *linkImg;//台标

@property (nonatomic, strong)UILabel *epgName;//电视台名称

@property (nonatomic, strong)UILabel *programName;//节目名称

@property (nonatomic, strong)UILabel *hasPlayTime;//节目播放起始时间

@property (nonatomic, strong)UIImageView *longLine;//长的分割线

//@property (nonatomic, strong)VODDeleteButton *deleteBtn;//取消预约按钮

@property (nonatomic, strong)UIImageView *deleteImg;

//@property (nonatomic, strong)UIView *deleteBtnView;


@end
