//
//  MineFvoriteChannelCell.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"

@interface MineFvoriteChannelCell : UITableViewCell

@property (nonatomic, strong) VODDeleteButton *backBtn;

@property (nonatomic, strong) UIImageView *backImg;

@property (nonatomic, strong) UIImageView *linkImg;

@property (nonatomic, strong) UILabel *linkName;

@property (nonatomic, strong) UIView *deleteBtnView;

@property (nonatomic, strong) VODDeleteButton *deleteBtn;

@end
