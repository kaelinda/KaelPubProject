//
//  MineCustomView.m
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/28.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "MineCustomView.h"

@implementation MineCustomView

-(instancetype)init{
    self = [super init];
    if (self) {
                
    }
    return self;
}


- (id)initWithLeftImgName:(NSString *)leftImgName andLabTitle:(NSString *)labTitle andRightImgName:(NSString *)rightImgName 
{
    self = [super init];
    if (self) {
        
        WS(wss);
        
        _leftImg = [UIImageView new];
        _leftImg.image = [UIImage imageNamed:leftImgName];
        [self addSubview:_leftImg];
        [_leftImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.width.equalTo(@(16*kDeviceRate));
            make.height.equalTo(@(16*kDeviceRate));
            make.left.equalTo(wss.mas_left).offset(12*kDeviceRate);
        }];
        
        _nameLab = [UILabel new];
        _nameLab.text = labTitle;
        _nameLab.textAlignment = NSTextAlignmentLeft;
        _nameLab.textColor = HT_COLOR_FONT_FIRST;
        _nameLab.backgroundColor = [UIColor clearColor];
        _nameLab.font = HT_FONT_SECOND;
        [self addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.left.equalTo(_leftImg.mas_right).offset(12*kDeviceRate);
            make.width.equalTo(@(100*kDeviceRate));
            make.height.equalTo(wss.mas_height);//@(30*kDeviceRate)
        }];
        
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:rightImgName];
//        _rightImg.alpha = 0.5;
        [self addSubview:_rightImg];
        [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.mas_right).offset(-12*kDeviceRate);
            make.width.equalTo(@(13*kDeviceRate));
            make.height.equalTo(@(13*kDeviceRate));
        }];
        
    }
    return self;
}

@end
