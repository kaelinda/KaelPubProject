//
//  VODPlayRecorderCollectionCell.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"

@interface VODPlayRecorderCollectionCell : UICollectionViewCell

@property (strong, nonatomic) UIView        *bgV;//背景图
@property (strong, nonatomic) UIImageView   *imageV;//背景图片
@property (strong, nonatomic) UILabel       *contentNameL;//点播视频内容名称
@property (strong, nonatomic) UILabel       *timeLabel;
@property (strong, nonatomic) UIImageView   *timeBackImg;
@property (nonatomic, strong) VODDeleteButton *deleteBtn;


@end
