//
//  ChannelPlayRecorderCell.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ChannelPlayRecorderCell.h"
#import "CalculateTextWidth.h"

@implementation ChannelPlayRecorderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        WS(wself);
        
        //cell高度73
        self.backBtn = [[VODDeleteButton alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60*kDeviceRate)];
        [self.contentView addSubview:self.backBtn];
        [self.backBtn setBackgroundImage:[UIImage imageNamed:@"bg_user_schedule_pressed"] forState:UIControlStateHighlighted];
        [self.backBtn setBackgroundColor:[UIColor clearColor]];
        
        
        //台标
        self.linkImg = [UIImageView new];
        [self.backBtn addSubview:self.linkImg];
        [self.linkImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(65/2*kDeviceRate));
            make.height.equalTo(@(56/2*kDeviceRate));
            make.top.equalTo(wself.backBtn.mas_top).offset(10*kDeviceRate);
            make.left.equalTo(wself.backBtn.mas_left).offset(45*kDeviceRate);//33.5*kDeviceRate
        }];
        
        //电视台名称
        self.epgName = [UILabel new];
        self.epgName.font = HT_FONT_FOURTH;
        self.epgName.textColor = HT_COLOR_FONT_FIRST;
        self.epgName.textAlignment = NSTextAlignmentCenter;
        [self.backBtn addSubview:self.epgName];
        [self.epgName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(wself.linkImg.mas_centerX);
            make.width.equalTo(@(100*kDeviceRate));
            make.top.equalTo(wself.linkImg.mas_bottom);
            make.bottom.equalTo(wself.backBtn.mas_bottom).offset(-10*kDeviceRate);
        }];
        
        
        //节目名称
        self.programName = [UILabel new];
        self.programName.font = HT_FONT_THIRD;
        self.programName.textColor = HT_COLOR_FONT_FIRST;
        self.programName.textAlignment = NSTextAlignmentLeft;
        [self.backBtn addSubview:self.programName];
        [self.programName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wself.linkImg.mas_right).offset(40*kDeviceRate);//33.5*kDeviceRate
//            make.top.equalTo(wself.backBtn.mas_top).offset(13*kDeviceRate);
            make.centerY.equalTo(wself.linkImg.mas_centerY);
            make.height.equalTo(@(15*kDeviceRate));
            make.width.equalTo(@(175*kDeviceRate));
        }];
        
        
        //节目已播放时间
        self.hasPlayTime = [UILabel new];
        self.hasPlayTime.font = HT_FONT_FOURTH;
        self.hasPlayTime.textColor = HT_COLOR_FONT_SECOND;
        self.hasPlayTime.textAlignment = NSTextAlignmentLeft;
        [self.backBtn addSubview:self.hasPlayTime];
        [self.hasPlayTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wself.programName.mas_left);
//            make.top.equalTo(wself.programName.mas_bottom).offset(8*kDeviceRate);
            make.centerY.equalTo(wself.epgName.mas_centerY);
            make.width.equalTo(@(175*kDeviceRate));
//            make.bottom.equalTo(wself.backBtn.mas_bottom).offset(-12*kDeviceRate);
            make.height.equalTo(@(13*kDeviceRate));
        }];
        
        //长的分割线
        self.longLine = [UIImageView new];
//        [self.longLine setImage:[UIImage imageNamed:@"bg_user_horizontal_parting_line"]];
        self.longLine.frame = CGRectMake(0, self.backBtn.frame.size.height-1*kDeviceRate, kDeviceWidth, 1*kDeviceRate);
        self.longLine.backgroundColor = HT_COLOR_SPLITLINE;
        [self.backBtn addSubview:self.longLine];
        
        
        self.deleteImg = [UIImageView new];
        [self.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        [self.backBtn addSubview:self.deleteImg];
//        self.deleteImg.frame = CGRectMake(kDeviceWidth-30*kDeviceRate-25*kDeviceRate, ((60-30)/2)*kDeviceRate, 30*kDeviceRate, 30*kDeviceRate);
        
        [self.deleteImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@(30*kDeviceRate));
            make.right.equalTo(wself.backBtn.mas_right).offset(-12*kDeviceRate);
            make.centerY.equalTo(wself.backBtn.mas_centerY);
        }];
        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
