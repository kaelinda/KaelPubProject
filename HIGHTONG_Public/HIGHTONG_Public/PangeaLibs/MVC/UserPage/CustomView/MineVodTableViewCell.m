//
//  MineVodTableViewCell.m
//  HIGHTONG
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "MineVodTableViewCell.h"

@implementation MineVodTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        WS(wss);
        
        self.leftView = [MineVodDetailCellView new];
        [self.contentView addSubview:self.leftView];
        [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.contentView.mas_left);
            make.top.equalTo(wss.contentView.mas_top);
            make.width.equalTo(@(kDeviceWidth/2));//@(160*kRateSize)
            make.height.equalTo(@(115*kDeviceRate));
        }];
        
        //self.leftView.backgroundColor = [UIColor cyanColor];
        
        
//        self.MiddleView = [MineVodDetailCellView new];
//        [self.contentView addSubview:self.MiddleView];
//        [self.MiddleView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.size.equalTo(self.leftView);
//            make.centerY.equalTo(self.leftView.mas_centerY);
//            make.left.equalTo(self.leftView.mas_right).offset(3*kRateSize);
//        }];
        
        //self.MiddleView.backgroundColor = [UIColor orangeColor];
        
        
        self.rightView = [MineVodDetailCellView new];
        [self.contentView addSubview:self.rightView];
        [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(self.leftView);
            make.left.equalTo(self.leftView.mas_right);//.offset(5*kRateSize);
            make.centerY.equalTo(self.leftView.mas_centerY);
        }];
        
        //self.rightView.backgroundColor = [UIColor purpleColor];
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
