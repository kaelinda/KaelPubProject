//
//  GeneralInputBlock.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "GeneralInputBlock.h"
#import "CalculateTextWidth.h"

@implementation GeneralInputBlock

- (instancetype)init
{
    self = [super init];
    if (self) {
        //
    }
    return self;
}

- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text withForgetPWDBtnName:(NSString *)btnName
{
    WS(wss);
    self = [super init];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);
        
        self.titleLab = [UILabel new];
        self.titleLab.text = title;
        self.titleLab.font = App_font(15);
        self.titleLab.textColor = UIColorFromRGB(0x333333);
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.backgroundColor = [UIColor clearColor];
        CGSize size = [[CalculateTextWidth alloc] sizeWithText:title font:self.titleLab.font];
        CGFloat titleW = size.width;
        [self addSubview:self.titleLab];
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.height.equalTo(wss.mas_height);
            make.left.equalTo(wss.mas_left).offset(20*kRateSize);
            make.width.equalTo(@(titleW*kRateSize));
        }];
        
        
        self.inputBlock = [[UITextField alloc] init];
        self.inputBlock.returnKeyType = UIReturnKeyDone;
        self.inputBlock.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.inputBlock.textAlignment = NSTextAlignmentLeft;
        self.inputBlock.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.inputBlock.borderStyle = UITextBorderStyleNone;
        // 增加textfield值改变触发事件
        [self.inputBlock addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
        
        self.inputBlock.textColor = UIColorFromRGB(0x333333);
        self.inputBlock.font = App_font(14);
        
        //        [self.inputBlock setBackgroundColor:[UIColor yellowColor]];
        
        //self.inputBlock.placeholder = text;
        
        //        CALayer *bgLayer = self.inputBlock.layer.sublayers.lastObject;
        //        bgLayer.opacity = 0.f;
        
        
        
        [self addSubview:self.inputBlock];
        //        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
        //            make.left.equalTo(self.titleLab.mas_right).offset(5*kRateSize);
        //            make.height.equalTo(wss.mas_height);
        //            make.centerY.equalTo(wss.mas_centerY);
        //            make.right.equalTo(wss.mas_right).offset(-20*kRateSize);
        //        }];
        
        if (isBeforeIOS7) {
            
            self.inputBlock.placeholder = text;
            
        }else{
            
            self.placeholderLab = [UILabel new];
            self.placeholderLab.text = text;
            self.placeholderLab.font = App_font(14);
            //            self.placeholderLab.font = [UIFont systemFontOfSize:15];
            self.placeholderLab.textColor = UIColorFromRGB(0xcccccc);
            self.placeholderLab.backgroundColor = [UIColor clearColor];
            self.placeholderLab.textAlignment = NSTextAlignmentLeft;
            self.placeholderLab.adjustsFontSizeToFitWidth = YES;
            [self.inputBlock addSubview:self.placeholderLab];
            [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(wss.inputBlock);
                make.size.equalTo(wss.inputBlock);
            }];
            
        }
        
        
        _verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_verityBtn setTitle:btnName forState:UIControlStateNormal];
        //        [_verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
        //    [_verityBtn setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
        [_verityBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        _verityBtn.titleLabel.font = App_font(11);
        [self addSubview:_verityBtn];
        _verityBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        [_verityBtn setBackgroundColor:[UIColor clearColor]];
        
        CGSize veritySize = [CalculateTextWidth sizeWithText:_verityBtn.titleLabel.text font:App_font(11)];
        int weightWW = veritySize.width;
        
        [_verityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.mas_right).offset(-20*kRateSize);
            make.height.equalTo(wss.mas_height);
            make.width.equalTo(@(weightWW*kRateSize));
        }];
        
        
        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.titleLab.mas_right).offset(5*kRateSize);
            make.height.equalTo(wss.mas_height);
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.verityBtn.mas_left);
        }];
        
        
        
    }
    return self;
}


- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text withVerityBtnName:(NSString *)btnName
{
    WS(wss);
    self = [super init];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);
        
        self.titleLab = [UILabel new];
        self.titleLab.text = title;
        self.titleLab.font = App_font(15);
        self.titleLab.textColor = UIColorFromRGB(0x333333);
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.backgroundColor = [UIColor clearColor];
        CGSize size = [[CalculateTextWidth alloc] sizeWithText:title font:self.titleLab.font];
        CGFloat titleW = size.width;
        [self addSubview:self.titleLab];
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.height.equalTo(wss.mas_height);
            make.left.equalTo(wss.mas_left).offset(20*kRateSize);
            make.width.equalTo(@(titleW*kRateSize));
        }];
        
        
        self.inputBlock = [[UITextField alloc] init];
        self.inputBlock.returnKeyType = UIReturnKeyDone;
        self.inputBlock.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.inputBlock.textAlignment = NSTextAlignmentLeft;
        self.inputBlock.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.inputBlock.borderStyle = UITextBorderStyleNone;
        // 增加textfield值改变触发事件
        [self.inputBlock addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
        
        self.inputBlock.textColor = UIColorFromRGB(0x333333);
        self.inputBlock.font = App_font(14);
        
        //        [self.inputBlock setBackgroundColor:[UIColor yellowColor]];
        
        //self.inputBlock.placeholder = text;
        
        //        CALayer *bgLayer = self.inputBlock.layer.sublayers.lastObject;
        //        bgLayer.opacity = 0.f;
        
        
        
        [self addSubview:self.inputBlock];
//        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.titleLab.mas_right).offset(5*kRateSize);
//            make.height.equalTo(wss.mas_height);
//            make.centerY.equalTo(wss.mas_centerY);
//            make.right.equalTo(wss.mas_right).offset(-20*kRateSize);
//        }];
        
        if (isBeforeIOS7) {
            
            self.inputBlock.placeholder = text;
            
        }else{
            
            self.placeholderLab = [UILabel new];
            self.placeholderLab.text = text;
            self.placeholderLab.font = App_font(14);
//            self.placeholderLab.font = [UIFont systemFontOfSize:15];
            self.placeholderLab.textColor = UIColorFromRGB(0xcccccc);
            self.placeholderLab.backgroundColor = [UIColor clearColor];
            self.placeholderLab.textAlignment = NSTextAlignmentLeft;
            self.placeholderLab.adjustsFontSizeToFitWidth = YES;
            [self.inputBlock addSubview:self.placeholderLab];
            [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(wss.inputBlock);
                make.size.equalTo(wss.inputBlock);
            }];
            
        }
        
        
        _verityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_verityBtn setTitle:btnName forState:UIControlStateNormal];
//        [_verityBtn addTarget:self action:@selector(getCoding:) forControlEvents:UIControlEventTouchUpInside];
        //    [_verityBtn setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
        [_verityBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        _verityBtn.titleLabel.font = App_font(13);
        [self addSubview:_verityBtn];
        _verityBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        
        CGSize veritySize = [CalculateTextWidth sizeWithText:_verityBtn.titleLabel.text font:App_font(13)];
        int weightWW = veritySize.width;
        
        [_verityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.mas_right).offset(-20*kRateSize);
            make.height.equalTo(wss.mas_height);
            make.width.equalTo(@(weightWW*kRateSize));
        }];

        
        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.titleLab.mas_right).offset(5*kRateSize);
            make.height.equalTo(wss.mas_height);
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.verityBtn.mas_left);
        }];
        
        
        
    }
    return self;
}


- (id)initAccountBindingWithTitle:(NSString *)title andPlaceholderText:(NSString *)text
{
    WS(wss);
    self = [super init];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);
        
        self.titleLab = [UILabel new];
        self.titleLab.text = title;
        self.titleLab.font = App_font(15);
        self.titleLab.textColor = UIColorFromRGB(0x333333);
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.backgroundColor = [UIColor clearColor];
        CGSize size = [[CalculateTextWidth alloc] sizeWithText:title font:self.titleLab.font];
        CGFloat titleW = size.width;
        [self addSubview:self.titleLab];
        
        if (kDeviceWidth<375) {
            [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(wss.mas_centerY);
                make.height.equalTo(wss.mas_height);
                make.left.equalTo(wss.mas_left).offset(18*kRateSize);
                make.width.equalTo(@((titleW+1)*kRateSize));
            }];
        }else{
        
            [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(wss.mas_centerY);
                make.height.equalTo(wss.mas_height);
                make.left.equalTo(wss.mas_left).offset(18*kRateSize);
                make.width.equalTo(@((titleW)*kRateSize));
            }];
        }
        
        
        self.lineView = [[UIView alloc] init];
        [self addSubview:self.lineView];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5*kDeviceRate);
            make.width.centerX.bottom.mas_equalTo(wss);
        }];
        self.lineView.backgroundColor = UIColorFromRGB(0xe8e8e8);
        self.lineView.hidden = YES;
        
        self.inputBlock = [[UITextField alloc] init];
        self.inputBlock.returnKeyType = UIReturnKeyDone;
        self.inputBlock.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.inputBlock.textAlignment = NSTextAlignmentLeft;
        self.inputBlock.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.inputBlock.borderStyle = UITextBorderStyleNone;
        [self.inputBlock addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
        
        self.inputBlock.textColor = UIColorFromRGB(0x333333);
        self.inputBlock.font = App_font(14);
        
        
        
        [self addSubview:self.inputBlock];
        
        if (kDeviceWidth<375) {
            [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.titleLab.mas_right).offset(18*kRateSize);
                make.height.equalTo(wss.mas_height);
                make.centerY.equalTo(wss.mas_centerY);
                make.right.equalTo(wss.mas_right);
            }];
        }else{
        
            [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.titleLab.mas_right).offset(5*kRateSize);
                make.height.equalTo(wss.mas_height);
                make.centerY.equalTo(wss.mas_centerY);
                make.right.equalTo(wss.mas_right);
            }];
        }
        
        if (isBeforeIOS7) {
            
            self.inputBlock.placeholder = text;
            
        }else{
            
            self.placeholderLab = [UILabel new];
            self.placeholderLab.text = text;
            self.placeholderLab.font = App_font(14);
            self.placeholderLab.textColor = UIColorFromRGB(0xcccccc);
            self.placeholderLab.backgroundColor = [UIColor clearColor];
            self.placeholderLab.textAlignment = NSTextAlignmentLeft;
            [self.inputBlock addSubview:self.placeholderLab];
            [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(wss.inputBlock);
                make.size.equalTo(wss.inputBlock);
            }];
            
        }
    }
    return self;
}

- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text
{
    WS(wss);
    self = [super init];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xffffff);
        
        self.titleLab = [UILabel new];
        self.titleLab.text = title;
        self.titleLab.font = App_font(15);
        self.titleLab.textColor = UIColorFromRGB(0x333333);
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.backgroundColor = [UIColor clearColor];
        CGSize size = [[CalculateTextWidth alloc] sizeWithText:title font:self.titleLab.font];
        CGFloat titleW = size.width;
        [self addSubview:self.titleLab];
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(wss.mas_centerY);
            make.height.equalTo(wss.mas_height);
            make.left.equalTo(wss.mas_left).offset(20*kRateSize);
            make.width.equalTo(@((titleW)*kRateSize));
        }];
        
        
        self.lineView = [[UIView alloc] init];
        [self addSubview:self.lineView];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5*kDeviceRate);
            make.width.centerX.bottom.mas_equalTo(wss);
        }];
        self.lineView.backgroundColor = UIColorFromRGB(0xe8e8e8);
        self.lineView.hidden = YES;
        
        self.inputBlock = [[UITextField alloc] init];
        self.inputBlock.returnKeyType = UIReturnKeyDone;
        self.inputBlock.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.inputBlock.textAlignment = NSTextAlignmentLeft;
        self.inputBlock.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.inputBlock.borderStyle = UITextBorderStyleNone;
        // 增加textfield值改变触发事件
        [self.inputBlock addTarget:self action:@selector(inputBlockValueChange:) forControlEvents:UIControlEventEditingChanged];
        
        self.inputBlock.textColor = UIColorFromRGB(0x333333);
        self.inputBlock.font = App_font(14);
        
//        [self.inputBlock setBackgroundColor:[UIColor yellowColor]];
        
        //self.inputBlock.placeholder = text;
        
//        CALayer *bgLayer = self.inputBlock.layer.sublayers.lastObject;
//        bgLayer.opacity = 0.f;
        


        [self addSubview:self.inputBlock];
        [self.inputBlock mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLab.mas_right).offset(5*kRateSize);
            make.height.equalTo(wss.mas_height);
            make.centerY.equalTo(wss.mas_centerY);
            make.right.equalTo(wss.mas_right).offset(-20*kRateSize);
        }];
        
        if (isBeforeIOS7) {
            
            self.inputBlock.placeholder = text;
            
        }else{
            
            self.placeholderLab = [UILabel new];
            self.placeholderLab.text = text;
            self.placeholderLab.font = App_font(14);
//            self.placeholderLab.font = [UIFont systemFontOfSize:13];
            self.placeholderLab.textColor = UIColorFromRGB(0xcccccc);
            self.placeholderLab.backgroundColor = [UIColor clearColor];
            self.placeholderLab.textAlignment = NSTextAlignmentLeft;
            [self.inputBlock addSubview:self.placeholderLab];
            [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(wss.inputBlock);
                make.size.equalTo(wss.inputBlock);
            }];

        }
    }
    return self;
}

- (void)inputBlockValueChange:(UITextField *)textField{
    
    if (textField.text.length>0) {
        
        self.placeholderLab.hidden = YES;
        
    }else{
        self.placeholderLab.hidden = NO;
    }
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
}

@end


