//
//  AccountCustomView.h
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/30.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCustomView : UIView

@property (nonatomic,strong)UILabel *leftLab;

@property (nonatomic,strong)UIImageView *rightImg;

@property (nonatomic,strong)UIImageView *midImg;

@property (nonatomic,strong)UILabel *midLab;


//- (id)initWithLeftTitle:(NSString *)title andMidImgName:(NSString *)imgName andMidLabText:(NSString *)labText;

- (id)initWithLeftTitle:(NSString *)title andMidLabText:(NSString *)labText;

//- (id)initWithLeftTitle:(NSString *)title andMidLabText:(NSString *)labText andRightImgName:(NSString *)rightName;



@end
