//
//  BtnGroupView.m
//  MyPage
//
//  Created by 吴伟 on 15/7/23.
//  Copyright (c) 2015年 selena. All rights reserved.
//

#import "BtnGroupView.h"
#import "UIView+Extension.h"
#import "CalculateTextWidth.h"
#import "MyGesture.h"

#define  interval 10
#define  sepImgWidth 2

@implementation BtnGroupView


- (id)initWithNameArr:(NSArray *)nameArr andTarget:(id)target andSelect:(SEL)select andViewAnimationName:(NSString *)animName andSepLineName:(NSString *)sepName
{
    _btnFrameArrs = [NSMutableArray arrayWithCapacity:0];
    
    CGFloat lastBtnX = 0; // 计算每一个按钮开始的位置
    self = [super init];
    if (self) {
        for (int i = 0; i<nameArr.count; i++) {
            _titleBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [_titleBtn setTitle:nameArr[i] forState:UIControlStateNormal];
            _titleBtn.titleLabel.textColor = [UIColor redColor];
            _titleBtn.titleLabel.font = [UIFont systemFontOfSize:20*kRateSize];
            _titleBtn.tag = i;
            [_titleBtn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
            _titleBtn.userInteractionEnabled = YES;
            
            CGSize size = [[CalculateTextWidth alloc] sizeWithText:nameArr[i] font:self.titleBtn.titleLabel.font];
            CGFloat btnX = 5 + lastBtnX;
            CGFloat btnY = (self.frame.size.height - size.height) / 2;
            CGFloat btnW = size.width;
            CGFloat btnH = size.height;
            
            lastBtnX += (btnW + 2*interval + sepImgWidth);
            _titleBtn.frame = CGRectMake(btnX, btnY, btnW, btnH);
            _titleBtn.centerY = self.centerY;
            
            [_btnFrameArrs addObject:[NSValue valueWithCGRect:_titleBtn.frame]];
            
            [self addSubview:_titleBtn];
            
            UIImageView *sepImg = [UIImageView new];
            [sepImg setImage:[UIImage imageNamed:sepName]];
            sepImg.frame = CGRectMake(btnX+btnW+interval, btnY, 2, btnH);
            [self addSubview:sepImg];
            
        }
        
        CGRect btnFrame = [_btnFrameArrs[0] CGRectValue];
        _runLineImg = [UIImageView new];
        _runLineImg.image = [UIImage imageNamed:animName];
        _runLineImg.frame = CGRectMake(btnFrame.origin.x, self.frame.size.height-btnFrame.origin.y/2, btnFrame.size.width, 2);
        [self addSubview:_runLineImg];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTheRunLineFrame) name:@"CHANGE_THE_RUN_LINE_FRAME" object:nil];
        
    }
    return self;
}

- (void)changeTheRunLineFrame
{
    NSString *tempIndex = [[NSUserDefaults standardUserDefaults]objectForKey:@"clickDown"];
    
    _index = [tempIndex intValue];
    
    CGRect frame = [_btnFrameArrs[_index] CGRectValue];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _runLineImg.frame = CGRectMake(frame.origin.x, self.frame.size.height-frame.origin.y/2, frame.size.width, 2);
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
