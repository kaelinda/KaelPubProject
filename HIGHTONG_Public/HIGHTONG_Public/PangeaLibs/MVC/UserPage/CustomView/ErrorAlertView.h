//
//  ErrorAlertView.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/12/15.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorAlertView : UIView

@property (nonatomic, strong)UILabel *alertLab;

@end
