//
//  FavChannelCollectionViewCell.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "FavChannelCollectionViewCell.h"
#import "CalculateTextWidth.h"

@implementation FavChannelCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"bg_user_favlist_cell"] forState:UIControlStateNormal];
        [_backBtn setImage:[UIImage imageNamed:@"bj_pindao_2"] forState:UIControlStateSelected];
        [_backBtn addTarget:self action:@selector(backBtnSenderDown) forControlEvents:UIControlEventTouchUpInside]
        ;
        [self.contentView addSubview:_backBtn];
        //_backBtn.frame = CGRectMake((kDeviceWidth-300*kRateSize)/2, 5*kRateSize, 300*kRateSize, 31*kRateSize);
        _backBtn.frame = self.bounds;
        
        
        
        self.linkImg = [[UIImageView alloc] initWithFrame:CGRectMake(73*kRateSize, 2.5*kRateSize, 30*kRateSize, 26*kRateSize)];
        [_backBtn addSubview:self.linkImg];
        
        
        self.linkName = [[UILabel alloc] init];
        self.linkName.textColor = UIColorFromRGB(0X666666);
        self.linkName.font = App_font(13);
        self.linkName.textAlignment = NSTextAlignmentCenter;
        //self.linkName.backgroundColor = [UIColor redColor];
        
        
//        CGSize nameSize = [CalculateTextWidth sizeWithText:@"湖南卫视" font:App_font(13)];
//        CGFloat nameWidth = nameSize.width;
//        CGFloat nameHeight = nameSize.height;
        
        
        
        self.linkName.frame = CGRectMake(CGRectGetMaxX(self.linkImg.frame)+17*kRateSize, 9*kRateSize, 60*kRateSize, 13*kRateSize);
        //self.linkName.frame = CGRectMake(CGRectGetMaxX(self.linkImg.frame)+17*kRateSize, 9*kRateSize, nameWidth*kRateSize, nameHeight*kRateSize);
        [_backBtn addSubview:self.linkName];
        
        
        self.deleteBtn = [[VODDeleteButton alloc] initWithFrame:CGRectMake(self.frame.size.width-10*kRateSize-16*kRateSize, 7.5*kRateSize, 16*kRateSize, 16*kRateSize)];
        [self.deleteBtn setImage:[UIImage imageNamed:@"mine_guanbi"] forState:UIControlStateNormal];
        [_backBtn addSubview:self.deleteBtn];

    }
    
    return self;
}

- (void)backBtnSenderDown
{
    _backBtn.selected = !_backBtn.selected;
}


@end
