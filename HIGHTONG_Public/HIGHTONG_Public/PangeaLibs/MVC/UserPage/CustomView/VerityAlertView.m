//
//  VerityAlertView.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VerityAlertView.h"
#import "CalculateTextWidth.h"

@implementation VerityAlertView

- (id)init
{
    self = [super init];
    if (self) {
        WS(wss);
        
        UILabel *alterLab = [UILabel new];
        alterLab.text = @"已发送至：";
        alterLab.font = App_font(15);
        alterLab.textColor = UIColorFromRGB(0x000000);
        alterLab.textAlignment = NSTextAlignmentLeft;
        CGSize labSize = [CalculateTextWidth sizeWithText:alterLab.text font:alterLab.font];
        CGFloat labW = labSize.width;
        
        alterLab.backgroundColor = [UIColor clearColor];
        [self addSubview:alterLab];
        [alterLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wss.mas_left).offset(15*kRateSize);
            make.top.equalTo(wss.mas_top).offset(14*kRateSize);
            make.width.equalTo(@(labW));
            make.height.equalTo(@(20*kRateSize));
        }];
        
        
        _phoneNumLable = [UILabel new];
       // _phoneNumLable.text = title;
        _phoneNumLable.textColor = App_selected_color;
        _phoneNumLable.font = App_font(15);
        _phoneNumLable.backgroundColor = [UIColor clearColor];
        _phoneNumLable.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_phoneNumLable];
        [_phoneNumLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(alterLab.mas_right);
            make.height.equalTo(alterLab.mas_height);
            make.centerY.equalTo(alterLab.mas_centerY);
            make.width.equalTo(@(200*kRateSize));
        }];
        
    }
    return self;
}

@end
