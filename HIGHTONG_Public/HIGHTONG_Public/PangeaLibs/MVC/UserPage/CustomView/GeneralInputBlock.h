//
//  GeneralInputBlock.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralInputBlock : UIImageView

@property (nonatomic, strong)UILabel *titleLab;

@property (nonatomic, strong)UILabel *placeholderLab;

@property (nonatomic, strong)UITextField *inputBlock;

@property (nonatomic, strong)UIButton *verityBtn;


@property (nonatomic, strong)UIView *lineView;


//@property (nonatomic, assign)CGRect inset;



//- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text andVerityTarget:(id)target andVeritySelect:(SEL)select andVerityImg:(NSString *)verityImg;

//- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text andVerityTarget:(id)target andVeritySelect:(SEL)select;

- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text;

- (id)initAccountBindingWithTitle:(NSString *)title andPlaceholderText:(NSString *)text;

- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text withVerityBtnName:(NSString *)btnName;

- (id)initWithTitle:(NSString *)title andPlaceholderText:(NSString *)text withForgetPWDBtnName:(NSString *)btnName;

@end
