//
//  MineCustomView.h
//  HIGHTONG
//
//  Created by HTYunjiang on 15/7/28.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineCustomView : UIView

@property (nonatomic,strong)UIImageView *leftImg;
@property (nonatomic,strong)UILabel *nameLab;
@property (nonatomic,strong)UIImageView *rightImg;

- (id)initWithLeftImgName:(NSString *)leftImgName andLabTitle:(NSString *)labTitle andRightImgName:(NSString *)rigthImgName;

@end
