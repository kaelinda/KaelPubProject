//
//  FavChannelCollectionViewCell.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"

@interface FavChannelCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) UIImageView *linkImg;

@property (nonatomic, strong) UILabel *linkName;

@property (nonatomic, strong) VODDeleteButton *deleteBtn;


@end
