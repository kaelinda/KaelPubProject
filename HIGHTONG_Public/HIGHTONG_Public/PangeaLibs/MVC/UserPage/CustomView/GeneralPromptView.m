//
//  GeneralPromptView.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "GeneralPromptView.h"
#import "UIView+Extension.h"

#define imgWidth (38*kDeviceRate)
#define imgHeight (38*kDeviceRate)

@implementation GeneralPromptView

- (id)initWithFrame:(CGRect)frame andImgName:(NSString *)imgName andTitle:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        
        UIView *backView = [UIView new];
        [backView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:backView];
        
        self.showImg = [[UIImageView alloc] init];
        [self.showImg setImage:[UIImage imageNamed:imgName]];
        
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.text = title;
        self.titleLab.font = HT_FONT_THIRD;
        self.titleLab.textColor = HT_COLOR_FONT_SECOND;
        self.titleLab.textAlignment = NSTextAlignmentCenter;
        
        CGSize titleLabSzie = [CalculateTextWidth sizeWithText:self.titleLab.text font:HT_FONT_THIRD];
//        CGFloat titleWidth = titleLabSzie.width;
        CGFloat titleHeight = titleLabSzie.height;
        
        [backView addSubview:self.showImg];
        [backView addSubview:self.titleLab];

        WS(wss);
        
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.mas_width);
            make.height.equalTo(@(imgHeight+titleHeight+10*kDeviceRate));
            make.centerX.equalTo(wss.mas_centerX);
//            make.top.equalTo(wss.mas_top).offset(200*kDeviceRate);//(480-64-(imgHeight+titleHeight+12*kRateSize))/2
            
            make.centerY.equalTo(wss.mas_centerY);
        }];
        
        
        [self.showImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(imgWidth));
            make.height.equalTo(@(imgHeight));
            make.top.equalTo(backView.mas_top);
            make.centerX.equalTo(backView.mas_centerX);
        }];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(wss.mas_width);//@(titleWidth*kDeviceRate)
            make.height.equalTo(@(titleHeight));
            make.top.equalTo(wss.showImg.mas_bottom).offset(10*kDeviceRate);
            make.centerX.equalTo(wss.mas_centerX);
        }];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
