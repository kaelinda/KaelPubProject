//
//  BtnGroupView.h
//  MyPage
//
//  Created by 吴伟 on 15/7/23.
//  Copyright (c) 2015年 selena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BtnGroupView : UIView


- (id)initWithNameArr:(NSArray *)nameArr andTarget:(id)target andSelect:(SEL)select andViewAnimationName:(NSString *)animationName andSepLineName:(NSString *)sepName;

@property (nonatomic,strong)UIImageView *runLineImg;//动画图片
@property (nonatomic,strong)UIButton *titleBtn;//选项按钮
@property (nonatomic,strong)NSMutableArray *btnFrameArrs;//存按钮frame的数组
@property (nonatomic,assign)NSInteger index;//从frame数组中取具体哪个的小标
//@property (nonatomic,weak)UIButton *selectButton;//记录当前点击的按钮

@end
