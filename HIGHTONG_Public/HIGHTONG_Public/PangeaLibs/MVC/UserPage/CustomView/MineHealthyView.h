//
//  MineHealthyView.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VODDeleteButton.h"

@interface MineHealthyView : UIView


@property (strong, nonatomic) UIImageView   *imageV;//海报图片

@property (strong, nonatomic) UILabel       *contentNameL;//点播视频内容名称

@property (nonatomic, strong) UIImageView   *deleteImg;

@property (nonatomic, strong) VODDeleteButton *highLigthBtn;

@property (nonatomic, strong) UIImageView   *honLineImg;//收藏cell横分割线

@end
