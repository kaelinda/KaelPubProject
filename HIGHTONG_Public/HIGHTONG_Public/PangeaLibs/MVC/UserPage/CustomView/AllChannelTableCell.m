//
//  AllChannelTableCell.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/28.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "AllChannelTableCell.h"

@implementation AllChannelTableCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {

        _nameLab = [[UILabel alloc] init];
        _nameLab.font = App_font(14);
        _nameLab.textAlignment = NSTextAlignmentCenter;
        _nameLab.textColor = UIColorFromRGB(0x999999);
        [self.contentView addSubview:_nameLab];
        _nameLab.frame = CGRectMake(0, 0, 80*kRateSize, 34*kRateSize);
        
    }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
