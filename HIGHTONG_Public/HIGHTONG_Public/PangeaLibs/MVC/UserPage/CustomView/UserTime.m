//
//  Time.m
//  Plist_Test
//
//  Created by WangRichard on 15/8/15.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "UserTime.h"
#import <UIKit/UIKit.h>

@implementation UserTime

+ (void)changBtn:(UIButton *)btn withSleepSecond:(NSInteger)second
{
    [[[self alloc] init] changBtn:btn withSleepSecond:second];
}


- (void)changBtn:(UIButton *)btn withSleepSecond:(NSInteger)second
{
    dispatch_queue_t queue = dispatch_queue_create("time_lost", NULL);

    dispatch_async(queue, ^{

//        btn.enabled = NO;

        for (int i = 1; i <= (second-1); i++) {
            //a 回调主线程刷新UI
            
            btn.enabled = NO;

            dispatch_async(dispatch_get_main_queue(), ^{
                 [btn setTitle:[NSString stringWithFormat:@"%lds后重发", (second - i)] forState:UIControlStateDisabled];
            });

            sleep(1);
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            btn.enabled = YES;

            [btn setTitle:@"重新获取" forState:UIControlStateNormal];
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
        });

    });
}


+ (void)changBtn:(UIButton *)btn withLab:(UILabel *)lab withSleepSecond:(NSInteger)second
{
    [[[self alloc] init] changBtn:btn withLab:lab withSleepSecond:second];
}


- (void)changBtn:(UIButton *)btn withLab:(UILabel *)lab withSleepSecond:(NSInteger)second
{
    dispatch_queue_t queue = dispatch_queue_create("time_lost", NULL);
    
    dispatch_async(queue, ^{
        
//        btn.enabled = NO;
        
        for (int i = 1; i <= (second-1); i++) {
            //a 回调主线程刷新UI
            dispatch_async(dispatch_get_main_queue(), ^{
//                [btn setTitle:[NSString stringWithFormat:@"%lds后重发", (second - i)] forState:UIControlStateDisabled];
                
                btn.enabled = NO;

                [btn setImage:[UIImage imageNamed:@"btn_login_dynamic_tv_auth_code_pressed"]  forState:UIControlStateDisabled];
                lab.text = [NSString stringWithFormat:@"%ld秒后重新获取", (second - i)];
                lab.textColor = HT_COLOR_FONT_SECOND;
            });
            
            sleep(1);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            btn.enabled = YES;
            
            [btn setImage:[UIImage imageNamed:@"btn_login_dynamic_tv_auth_code_normal"]  forState:UIControlStateDisabled];
            lab.text = @"获取验证码";
            lab.textColor = HT_COLOR_FONT_INVERSE;
        });
        
    });
}



- (void)changBtn:(UIButton *)btn{
    
}

@end
