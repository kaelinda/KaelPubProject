//
//  ItemChannelSelectTableCell.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ItemChannelSelectTableCell.h"

@implementation ItemChannelSelectTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        _backImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 60*kDeviceRate)];
        _backImg.backgroundColor = App_background_color;
        [self.contentView addSubview:_backImg];

        
        _backgroundImg = [[UIImageView alloc]initWithFrame:CGRectMake(12*kDeviceRate, 10*kDeviceRate, 351*kDeviceRate, 50*kDeviceRate)];
        [_backgroundImg setImage:[UIImage imageNamed:@"bg_user_collection_item"]];
        [_backImg addSubview:_backgroundImg];

        //台标
        self.linkImg = [[UIImageView alloc] initWithFrame:CGRectMake(100*kDeviceRate, 10*kDeviceRate, 40*kDeviceRate, 30*kDeviceRate)];
        self.linkImg.contentMode = UIViewContentModeScaleAspectFit;
        [_backgroundImg addSubview:self.linkImg];
        
        //选中图标
        self.choiceImg = [[UIImageView alloc]init];
        [self.choiceImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_checked"]];
        [_backgroundImg addSubview:self.choiceImg];
        self.choiceImg.hidden = YES;
        
        [self.choiceImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backgroundImg.mas_right).offset(-10*kDeviceRate);
            make.centerY.equalTo(_backgroundImg.mas_centerY);
            make.height.equalTo(@(30*kDeviceRate));
            make.width.equalTo(@(30*kDeviceRate));
            
        }];
        
        //未选中图标
//        self.UnChoiceImg = [[UIImageView alloc]init];
//        [self.UnChoiceImg setImage:[UIImage imageNamed:@"check"]];
//        [_backgroundImg addSubview:self.UnChoiceImg];
//        self.UnChoiceImg.hidden = NO;
//        
//        [self.UnChoiceImg mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(_backgroundImg.mas_right).offset(-17*kRateSize);
//            make.centerY.equalTo(_backgroundImg.mas_centerY);
//            make.height.equalTo(@(25*kRateSize));
//            make.width.equalTo(@(25*kRateSize));
//        }];
        
        //“已添加”按钮
        self.addedSign = [[UILabel alloc] init];
        self.addedSign.textColor = HT_COLOR_FONT_ASSIST;
        self.addedSign.font = HT_FONT_FOURTH;
        self.addedSign.textAlignment = NSTextAlignmentRight;
        self.addedSign.text = @"已添加";
        self.addedSign.hidden = YES;
  

        [_backgroundImg addSubview:self.addedSign];
        
        [self.addedSign mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_backgroundImg.mas_right).offset(-10*kDeviceRate);
            make.centerY.equalTo(_backgroundImg.mas_centerY);
        }];
        
        //台名
        self.linkName = [[UILabel alloc] init];
        self.linkName.textColor = HT_COLOR_FONT_FIRST;
        self.linkName.font = HT_FONT_SECOND;
        self.linkName.textAlignment = NSTextAlignmentLeft;
        [_backgroundImg addSubview:self.linkName];
        
        [self.linkName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.linkImg.mas_right).offset(5*kDeviceRate);
            make.centerY.equalTo(_backgroundImg.mas_centerY);
            make.right.equalTo(_backgroundImg.mas_right).offset(-50*kDeviceRate);
            
        }];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
