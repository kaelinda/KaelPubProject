//
//  AllChannelTableCell.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/28.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "VODDeleteButton.h"

@interface AllChannelTableCell : UITableViewCell

@property (nonatomic, strong)UIButton *nameBtn;

@property (nonatomic, strong)UILabel *nameLab;

@end
