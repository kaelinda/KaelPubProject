//
//  SelectAndDeleteView.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 16/3/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "SelectAndDeleteView.h"

@implementation SelectAndDeleteView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setUpView];
    }
    return self;
}


- (void)setUpView
{
    WS(wself);
    UIImageView *line = [UIImageView new];
    [line setBackgroundColor:HT_COLOR_SPLITLINE];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(wself.mas_width);
        make.height.equalTo(@(1*kDeviceRate));
        make.top.equalTo(wself.mas_top);
        make.centerX.equalTo(wself.mas_centerX);
    }];
    

    _selectBtn = [VODDeleteButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn = [VODDeleteButton buttonWithType:UIButtonTypeCustom];
    
    _selectBtn.adjustsImageWhenHighlighted = NO;//去掉Highlighted状态下的阴影
    _deleteBtn.adjustsImageWhenHighlighted = NO;
    
    [_selectBtn setImage:[UIImage imageNamed:@"sl_user_common_select_all_normal"] forState:UIControlStateNormal];
    [_selectBtn setImage:[UIImage imageNamed:@"sl_user_common_select_all_pressed"] forState:UIControlStateHighlighted];
    
    [_deleteBtn setImage:[UIImage imageNamed:@"btn_user_disable_click"] forState:UIControlStateNormal];
    [_deleteBtn setImage:[UIImage imageNamed:@"sl_user_playrecord_del_item_pressed"] forState:UIControlStateHighlighted];
    
    
    [self addSubview:_selectBtn];
    [self addSubview:_deleteBtn];
    
    _deleteBtn.enabled = NO;
    

    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(HT_WIGHT_RECORDDELEBTN*kDeviceRate));
        make.height.equalTo(@(HT_HIGHT_RECORDDELEBTN*kDeviceRate));
        make.left.equalTo(wself.mas_left).offset(12*kDeviceRate);
//        make.centerY.equalTo(wself.mas_centerY);
        make.top.mas_equalTo(line.mas_bottom).offset(10*kDeviceRate);
    }];
    
    
    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(wself.selectBtn);
//        make.centerY.equalTo(wself.selectBtn.mas_centerY);
        make.top.mas_equalTo(wself.selectBtn.mas_top);
        make.left.equalTo(wself.selectBtn.mas_right).offset(12*kDeviceRate);
    }];
    
    
    _selectLab = [[UILabel alloc] init];
    _deleteLab = [[UILabel alloc] init];
    
    _selectLab.text = @"全选";
    _deleteLab.text = @"删除";
    
    _selectLab.textColor = App_selected_color;
    _deleteLab.textColor = HT_COLOR_FONT_WARNING;
    
    _selectLab.textAlignment = NSTextAlignmentCenter;
    _deleteLab.textAlignment = NSTextAlignmentCenter;
    
    _selectLab.font = HT_FONT_FIRST;
    _deleteLab.font = HT_FONT_FIRST;
    
    [_selectBtn addSubview:_selectLab];
    [_deleteBtn addSubview:_deleteLab];
    
    [_selectLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(wself.selectBtn);
    }];
    
    [_deleteLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(wself.deleteBtn);
    }];
    
}

@end
