//
//  CalculateTextWidth.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculateTextWidth : NSObject

+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font;

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font;

@end
