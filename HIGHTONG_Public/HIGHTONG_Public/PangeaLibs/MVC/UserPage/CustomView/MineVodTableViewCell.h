//
//  MineVodTableViewCell.h
//  HIGHTONG
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineVodDetailCellView.h"
@interface MineVodTableViewCell : UITableViewCell
@property (nonatomic, strong) MineVodDetailCellView *leftView;
//@property (nonatomic, strong) MineVodDetailCellView *MiddleView;
@property (nonatomic, strong) MineVodDetailCellView *rightView;
@property (strong,nonatomic)NSMutableArray *items;
@end
