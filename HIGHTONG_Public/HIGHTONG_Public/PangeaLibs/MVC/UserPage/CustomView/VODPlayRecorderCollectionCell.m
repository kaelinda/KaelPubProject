//
//  VODPlayRecorderCollectionCell.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/24.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VODPlayRecorderCollectionCell.h"

@implementation VODPlayRecorderCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.bgV = [[UIView alloc] init];
        self.bgV.userInteractionEnabled = YES;
        self.bgV.frame = CGRectMake(3*kRateSize, 0, 102*kRateSize, 168*kRateSize);
        self.bgV.backgroundColor = UIColorFromRGB(0xffffff);
        [self.contentView addSubview:self.bgV];
        
        
        self.imageV = [[UIImageView alloc] init];
        self.imageV.userInteractionEnabled = YES;
        self.imageV.frame = CGRectMake(0, 5*kRateSize, (self.bgV.bounds.size.width),136*kRateSize);
        [self.imageV setBackgroundColor:[UIColor clearColor]];
        [self.bgV addSubview:self.imageV];
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 7.0;
        
        
        self.timeBackImg = [[UIImageView alloc] init];
        self.timeBackImg.frame = CGRectMake(0, self.imageV.frame.size.height-20*kRateSize, self.imageV.bounds.size.width, 20*kRateSize);
        [self.timeBackImg setBackgroundColor:UIColorFromRGB(0x4c4c4c)];
        self.timeBackImg.alpha = 0.8;
        [self.imageV addSubview:self.timeBackImg];
        
        self.timeLabel = [[UILabel alloc]init];
        self.timeLabel.frame = CGRectMake(4*kRateSize, 0, self.timeBackImg.bounds.size.width-4*kRateSize, 20*kRateSize);
        self.timeLabel.textColor = UIColorFromRGB(0xffffff);
        [self.timeLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10*kRateSize]];
        [self.timeBackImg addSubview:self.timeLabel];
        
        
        self.contentNameL = [[UILabel alloc] init];
        [self.contentNameL setFrame:CGRectMake(2*kRateSize, CGRectGetMaxY(self.imageV.frame), (self.bgV.bounds.size.width)-2*kRateSize, 25*kRateSize)];
        [self.contentNameL setBackgroundColor:[UIColor clearColor]];
        self.contentNameL.textColor = UIColorFromRGB(0x333333);
        [self.contentNameL setFont:[UIFont systemFontOfSize:13*kRateSize]];
        //self.contentNameL.adjustsFontSizeToFitWidth = YES;
        [self.bgV addSubview:self.contentNameL];
        
        
        self.deleteBtn = [[VODDeleteButton alloc] init];
        [self.deleteBtn setImage:[UIImage imageNamed:@"mine_guanbi"] forState:UIControlStateNormal];
        [self.imageV addSubview:self.deleteBtn];
        self.deleteBtn.frame = CGRectMake(self.imageV.frame.size.width-16*kRateSize, 0, 16*kRateSize, 16*kRateSize);
        
    }
    return self;
}


@end
