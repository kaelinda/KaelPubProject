//
//  MineHealthyTableViewCell.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineHealthyView.h"

@interface MineHealthyTableViewCell : UITableViewCell

@property (nonatomic,strong)MineHealthyView *leftView;//左边

@property (nonatomic,strong)MineHealthyView *rightView;//右边

@end
