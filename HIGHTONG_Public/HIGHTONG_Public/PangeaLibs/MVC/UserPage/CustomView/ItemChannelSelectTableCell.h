//
//  ItemChannelSelectTableCell.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemChannelSelectTableCell : UITableViewCell

@property (nonatomic, strong)UIImageView *backImg;//cell底图

@property (nonatomic, strong)UIImageView *backgroundImg;//cell背景框

//@property (nonatomic, strong)UIButton *backBtn;//cell按钮

@property (nonatomic, strong)UIImageView *linkImg;//台标

@property (nonatomic, strong)UILabel *linkName;//台名

@property (nonatomic, strong)UIImageView *choiceImg;//选中标记

//@property (nonatomic, strong)UIImageView *UnChoiceImg;//未选中标记

@property (nonatomic, strong)UILabel *addedSign;//以添加标记

@end
