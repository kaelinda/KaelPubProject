//
//  CalculateTextWidth.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/7.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "CalculateTextWidth.h"

@implementation CalculateTextWidth

+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font
{

    return [[[self alloc] init] sizeWithText:text font:font];
}

// 给定文本 字体 宽度  计算frame
- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth{
    
    if (isBeforeIOS7) {
        //[text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
        CGSize titleSize = [text sizeWithFont:font constrainedToSize:CGSizeMake(MAXFLOAT, 45*kRateSize)];
        return titleSize;
    }
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = font;
    CGSize maxSize = CGSizeMake(maxWidth, MAXFLOAT);
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
//    CGRect tmpRect = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:font, nil] context:nil];
//    return tmpRect.size;
    
}

// 给定文本 字体 计算frame
- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font{
    return [self sizeWithText:text font:font maxWidth:MAXFLOAT];
}


@end
