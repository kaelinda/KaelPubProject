//
//  Time.h
//  Plist_Test
//
//  Created by WangRichard on 15/8/15.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIButton;
@interface UserTime : NSObject
+ (void)changBtn:(UIButton *)btn withSleepSecond:(NSInteger)second;

+ (void)changBtn:(UIButton *)btn withLab:(UILabel *)lab withSleepSecond:(NSInteger)second;
@end
