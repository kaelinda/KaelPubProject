//
//  MineHealthyView.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "MineHealthyView.h"

@implementation MineHealthyView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        WS(wself);
        
        UIView *superView = self;
        
        //海报图
        self.imageV = [UIImageView new];
        self.imageV.userInteractionEnabled = YES;
//        [self.imageV setBackgroundColor:UIColorFromRGB(0xe3e2e3)];
        [superView addSubview:self.imageV];
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(superView.mas_left);
            make.right.equalTo(superView.mas_right);
            make.top.equalTo(superView.mas_top);
            make.bottom.equalTo(superView.mas_bottom).offset((-35)*kDeviceRate);
        }];
        
        //影片等名字
        self.contentNameL = [UILabel new];
        [self.contentNameL setBackgroundColor:[UIColor clearColor]];
        self.contentNameL.textColor = HT_COLOR_FONT_FIRST;
        self.contentNameL.font = HT_FONT_THIRD;
        //self.contentNameL.adjustsFontSizeToFitWidth = YES;
        [superView addSubview:self.contentNameL];
        self.contentNameL.numberOfLines = 2;
        self.contentNameL.textAlignment = NSTextAlignmentLeft;
        [self.contentNameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(wself.imageV.mas_left);
            make.right.equalTo(wself.imageV.mas_right);
            make.top.equalTo(wself.imageV.mas_bottom).offset(7*kDeviceRate);
            make.bottom.equalTo(superView.mas_bottom).offset(-10*kDeviceRate);
        }];
    
        
        self.deleteImg = [UIImageView new];
        [self.deleteImg setImage:[UIImage imageNamed:@"sl_user_common_del_checkbox_normal"]];
        [self.imageV addSubview:self.deleteImg];
        [self.deleteImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(30*kDeviceRate);
            make.top.mas_equalTo(wself.imageV.mas_top);//.offset(5*kRateSize);
            make.right.mas_equalTo(wself.imageV.mas_right);//.offset(-27*kRateSize);
        }];
        
        
        //整个cell的点击高亮图
        self.highLigthBtn = [[VODDeleteButton alloc] init];
        [self.imageV addSubview:_highLigthBtn];
        UIImage *image = [UIImage imageNamed:@"bg_common_vod_pressed"];
        [_highLigthBtn setBackgroundImage:image forState:UIControlStateHighlighted];
        [_highLigthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(wself.imageV);
        }];

    }
    return self;
}

@end
