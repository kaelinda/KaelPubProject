//
//  DemandViewcontrol.m
//  HIGHTONG
//
//  Created by Kael on 15/7/27.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DemandViewController.h"
#import "DemandVideoPlayerVC.h"
#import "UIButton_Block.h"
#import "HTRequest.h"
#import "headScroll.h"
#import "Masonry.h"
#import "AD_Scroll.h"
#import "ProgramListCell.h"
//#import "DemandVideoPlayerVC.h"

#import "DemandSecondViewcontrol.h"

#import "SearchViewController.h"

#import "AD_View.h"

#import "MBProgressHUD.h"
#import "DemandVideoPlayerVC.h"

static NSInteger DIANBO_count_jihao = 0 ;
@interface DemandViewController()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,MBProgressHUDDelegate>
{
    UIView *_HeadView;
    AD_Scroll *_ad;
    
    
    
    
    AD_View *_AD;
    headScroll *_scroll;
    //定义关于防止重复加载的成员变量
    BOOL _tableViewCanScroll;//tableview可不可以滚动
    NSInteger  _reqCount;//请求的个数 综艺  6：
    BOOL _reqFinish;//请求完成
    NSDate *_requsetDate;//请求的时间
    BOOL  _first;
    BOOL  _first1;
    NSString *_operationcode;//运营组 码
    
    UILabel *_seachTextView;//搜索热词
    
    MBProgressHUD *_hub;//加载等待按钮
    
    NSInteger selectIndex;//当前选择的第几个策略，院线还是少儿乐园
    
    
    NSArray *HBarray ;//海报数组
    
    BOOL FinishRequest;//请求完成了
    
    NSDate *_requestDate;//加载时间

    
}
@property (nonatomic,strong) UIButton *goBtn;

@property (nonatomic,strong) UITableView *tableView_Base; //基础的大tableView

@property (nonatomic,strong) NSMutableArray *dataArray; //点播tableView的数据源
@end


@implementation DemandViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.tableView_Base) {
        NSLog(@"x:%f  Y:%f W:%f H:%f",self.tableView_Base.origin.x,self.tableView_Base.origin.y,self.tableView_Base.size.width,self.tableView_Base.size.height);
        NSLog(@"%@",self.tableView_Base);
        NSLog(@"%@",self.tableView_Base.tableHeaderView);
    }
    
    
    NSLog(@"重新加载页面");
    if (_first1) {
        _first1 = NO;
        return;
    }
//zzsc
//    [self reloadData];
    NSDate *date = [NSDate date];
    if ([self RequstEnabledWithFirstTime:_requestDate andSecondData:date]) {
        [self reRequestHomeData];
    }
    
    [MobCountTool pushInWithPageView:@"DemandPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"DemandPage"];
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    
    
}
-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setNaviBarTitle:@"点播"];
    _first = YES;
    _first1 = YES;
    
    FinishRequest = YES;
    selectIndex =  0 ;
    //请求时间
    _requestDate = [NSDate date];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reRequestHomeData) name:@"loginSuccess" object:nil];
    
    _requsetDate = [NSDate dateWithTimeIntervalSince1970:2];
    
    NSLog(@"重新加载页面");
    [self reRequestHomeData];
    
    _requsetDate = [[NSDate alloc]initWithTimeInterval:-11 sinceDate:[NSDate date]];
    NSLog(@"%@ %@",[NSDate date],_requsetDate);
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setNaviBarTitle:@"Demand"];
    [self setNaviBarLeftBtn:nil];
    
    //    _goBtn = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"go" target:self action:@selector(gogogo)];
    //    [self setNaviBarRRightBtn:_goBtn];
    
    [self setSeachBar];
    
    
    
    //adjust the UI for iOS 7
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
        NSLog(@"dfafasfsfadsfsfsf");
    }
#endif
    
    self.dataArray = [NSMutableArray array];
    [self setUpBaseView];
    
    _reqFinish = YES;
    
    _hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hub.delegate = self;
    [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
    HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
    [requst VODMenuConfigListWithTimeout:10];
    
}
- (void)showLoading
{
    _hub.hidden = NO;
}
- (void)hideLoading
{
    _hub.hidden = YES;
}



#pragma mark - 设置tableview初始化
- (void)setUpBaseView
{
    
    UIView *view  = [[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:view];
    {//titles放到导航下边
        _scroll = [[headScroll alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, 32*kRateSize)];
        
        _scroll.contentSize = CGSizeMake(kDeviceWidth, KDeviceHeight);
        
        [self.view addSubview:_scroll];
        
        _scroll.showsHorizontalScrollIndicator = NO;
        [_scroll updataTitle];
        
    }
    
    
    self.tableView_Base = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+32*kRateSize, self.view.frame.size.width, (self.view.frame.size.height-110-32*kRateSize)) style:UITableViewStylePlain];
    
    
    
    
    
    
    self.view.backgroundColor = App_background_color;
    //    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+40, self.view.frame.size.width, (self.view.frame.size.height-110))];
    //    image.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    //    [self.view addSubview:image];
    
    self.tableView_Base.delegate = self;
    self.tableView_Base.dataSource = self;
    //    self.tableView_Base.estimatedRowHeight = UITableViewAutomaticDimension;**kankan
    [self.view addSubview:self.tableView_Base];
    [self.tableView_Base setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    //设置tableView的头视图
    {
        
        _HeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (32+181-32)*kRateSize)];
        
        //添加故广告页面，上广告选中就滚到指定位置
        
        //        _ad = [[AD_Scroll alloc]init];
        _AD = [[AD_View alloc]init];
        
        [_HeadView addSubview:_AD];
        [_AD mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_HeadView.mas_top);
            make.left.equalTo(_HeadView.mas_left);
            make.right.equalTo(_HeadView.mas_right);
            make.bottom.equalTo(_HeadView.mas_bottom);
        }];
        __weak typeof(self) WS = self;
        _AD.AD_BLOCK = ^(UIButton_Block*btn)
        {
            NSLog(@"要播放的ID是: %@",btn.serviceIDName);
            NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
            
            if ([btn.serviceIDName isEqualToString:@"NULL"]) {
                [AlertLabel AlertInfoWithText:@"暂无节目信息" andWith:WS.view withLocationTag:10011];
            }else
            {
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
            demandVC.contentID = btn.serviceIDName;
            demandVC.contentTitleName = btn.eventName;
            
            
            [WS.navigationController pushViewController:demandVC animated:YES];
                
            }
        };

        __weak headScroll *weakscroll = _scroll;
        
        __weak NSString *weakoperationcode = _operationcode;
        
        __weak NSArray *weakHBarray = HBarray;
        
        __block BOOL weakreqFinish = _reqFinish;
        
        __weak MBProgressHUD *weakhub = _hub;
        
        [_scroll setBlock:^(NSInteger index,UIButton_Block*btn)
         {
             NSLog(@"第几个%ld",index);
             selectIndex = index;

             [WS.dataArray removeAllObjects];
             [WS.tableView_Base reloadData];
             

             
             NSLog(@"选择了 %@",weakscroll.titledataArray[index]);
             NSDictionary *dic = weakscroll.titledataArray[index];
             NSString *opid = [dic objectForKey:@"operationCode"];
             _operationcode = opid;
             
             [WS haibaoWithOperationcode:weakoperationcode andConfigLis:weakHBarray];
             
             if (weakreqFinish) {
                 
                 [weakhub show:YES];
                 weakhub.hidden = NO;
                 [NSTimer scheduledTimerWithTimeInterval:4 target:WS selector:@selector(hide) userInfo:nil repeats:NO];
                 
                 //             点播分类运营组
                 HTRequest *request = [[HTRequest alloc]initWithDelegate:WS];
                 [request VODOperationCategoryListWithOperationCode:opid andTimeout:10];
                 
             }
             
         }];
        
        self.tableView_Base.tableHeaderView = _HeadView;
        self.tableView_Base.tableFooterView = [UIView new];
    }
    
}
- (void)hide
{
    _hub.hidden = YES;
}

- (void)setSeachBar
{
    
    
    
    //设置搜索textField
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = size.height/2;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(40 ,2,size.width - 43*kRateSize/kRateSize , 26)];
    
    _seachTextView.text = @"";
//        _seachTextView.backgroundColor = [UIColor redColor];
    _seachTextView.font = [UIFont systemFontOfSize:16];
    _seachTextView.textColor = UIColorFromRGB(0xaaaaaa);
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake((_seachTextView.frame.origin.x-30)*kRateSize,( _seachTextView.frame.origin.y+1)*kRateSize, 24, 24)];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
        
        SearchViewController *search = [[SearchViewController alloc]init];

//        search.searchText = [array lastObject];;
        [self.navigationController pushViewController:search animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 27, kDeviceWidth-30*2*kRateSize, 28)];
}

#pragma mark -TableView  Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.dataArray.count) {
        self.tableView_Base.backgroundColor = [UIColor whiteColor];
    }else
    {
        self.tableView_Base.backgroundColor = [UIColor clearColor];
    }
    NSLog(@"一共几个分组%ld ",self.dataArray.count);
    return self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dic = [self.dataArray objectAtIndex:section];
    //    return [[dic objectForKey:@"Array"] count];
    NSInteger numOfRow = [self numberOfCellRowsWithArray: [dic objectForKey:@"Array"] ];
    
    if (numOfRow>0) {
        if (numOfRow == 1) {
            id string = [[dic objectForKey:@"Array"] firstObject];;
            if ([string isKindOfClass:[NSString class]]) {
                if ([string isEqualToString:@"waitingwaiting"]) {
                    numOfRow = 0;
                }
            }
        }
        return numOfRow;
    }else{
        
        return 0;
    }
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *Progranidentifier = @"Progranidentifier";
    ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
    if (!cell) {
        cell = [[ProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
    }
    if (self.dataArray.count == 0) {
        return cell;
    }
    NSDictionary *dic = [self.dataArray objectAtIndex:indexPath.section];
    NSArray * array = [dic objectForKey:@"Array"];
    NSString * title = [dic objectForKey:@"title"];
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    cell.dateArray.array = [self arrayFOR_ALLArray:array withBeginnumb:indexPath.row];
    cell.isMovie = [self isMovieOR:title];
    [cell update];
    
    cell.block = ^(UIButton_Block*btn)
    {
        NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
        
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = btn.serviceIDName;
        demandVC.contentTitleName = btn.eventName;
        
        
        [self.navigationController pushViewController:demandVC animated:YES];
        
        
        
    };
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170*kRateSize;
}


//每个分组的头标题
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), 40*kRateSize)];
    UIImageView *background = [UIImageView new];
    [headerView addSubview:background];
    [background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_top).offset(5*kRateSize);
        make.leading.equalTo(headerView.mas_leading);
        make.right.equalTo(headerView.mas_right).offset(0*kRateSize);
        make.bottom.equalTo(headerView.mas_bottom).offset(0*kRateSize);
    }];
    background.image = [UIImage imageNamed:@"bg_home_section_header@2x"];
    
    //左边线段
    UIImageView *Leftline = [[UIImageView alloc]initWithFrame:CGRectMake(5*kRateSize, 11*kRateSize, 3*kRateSize, 18*kRateSize)];
        Leftline.image = [UIImage imageNamed:@"ic_vertical_yellow_line"];
//    Leftline.backgroundColor = [UIColor grayColor];
    Leftline.layer.cornerRadius = 2.5;
    Leftline.clipsToBounds =YES;
    [headerView addSubview:Leftline];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13*kRateSize, 5*kRateSize, 100*kRateSize, 30*kRateSize)];
    if (self.dataArray.count) {
        NSString *str =[(NSDictionary*) self.dataArray[section] objectForKey:@"title"];
        label.text = str;
    }
    label.textColor = [UIColor blackColor];
    //    label.backgroundColor = [UIColor whiteColor];
    
    
    NSInteger numOfRow=0;
    {//计算出section下有几个数据
        
    NSDictionary *dic = [self.dataArray objectAtIndex:section];
    //    return [[dic objectForKey:@"Array"] count];
     numOfRow = [self numberOfCellRowsWithArray: [dic objectForKey:@"Array"] ];
    
    if (numOfRow>0) {
        if (numOfRow == 1) {
            id string = [[dic objectForKey:@"Array"] firstObject];;
            if ([string isKindOfClass:[NSString class]]) {
                if ([string isEqualToString:@"waitingwaiting"]) {
                    numOfRow = 0;
                }
            }
        }
        
    }
        
    }
    NSDictionary *dic = [self.dataArray objectAtIndex:section];
    NSArray *array = [dic objectForKey:@"Array"];
    if(array.count >= 6)
    {
    UIButton_Block *btn = [UIButton_Block new];
    btn.frame =   CGRectMake(FRAME_W(self.view) - (60-7)*kRateSize, 5*kRateSize, 60*kRateSize, 30*kRateSize);
    btn.titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
    //    btn.backgroundColor =[UIColor grayColor];
    btn.serviceIDName = label.text;
    [btn setClick:^(UIButton_Block*btn,NSString* name){
        if (self.dataArray.count) {
            
            
            NSDictionary *dic = (NSDictionary*)self.dataArray[section];
            NSString *title = [dic objectForKey:@"title"];
            NSString *categoryID =[dic objectForKey:@"categoryID"];
            NSLog(@"%@ 频道:%@",[btn currentTitle],title);
            NSLog(@"%@",self.dataArray[section]);
            
            
            DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
            demandSecond.isMovie = [self isMovieOR:btn.serviceIDName];
            demandSecond.categoryID = categoryID;
            demandSecond.operationCode = _operationcode;
            demandSecond.name = title;
            [self.navigationController pushViewController:demandSecond animated:YES];
            
        }
    }];
    
    if (numOfRow == 0) {
        btn.hidden = YES;
    }else
    {
        btn.hidden = NO;
    }
    
    UIImageView *Rightline = [[UIImageView alloc]initWithFrame:CGRectMake(12*kRateSize, 8*kRateSize, 1*kRateSize, 14*kRateSize)];
    Rightline.backgroundColor = App_selected_color;
    Rightline.layer.cornerRadius = 2.5;
    Rightline.clipsToBounds =YES;
    [btn addSubview:Rightline];
    
    [headerView addSubview:btn];
    
    }
    [headerView addSubview:label];
    
    return headerView;
    
    
}
//设置分组header的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
 
        if (self.dataArray.count>section) {
            NSDictionary *dic = [self.dataArray objectAtIndex:section];
            NSArray *array = [dic objectForKey:@"Array"];
            if ((array.count == 1) && [array[0] isKindOfClass:[NSString class]] &&[array[0] isEqualToString:@"waitingwaiting"]) {
                return 0;
            }
            return 35*kRateSize;
        }
    return 35*kRateSize;
}



-(void)gogogo{
    NSLog(@"你想在这里干什么？");
    
    
    
}



#pragma mark - HT请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    //    NSLog(@"dianbo返回数据: %ld result:%@  type:%@",status,result,type );
    switch (status) {
        case 0://请求成功
        {
            
            
            
            if ([type isEqualToString:@"topSearchWords"]) {
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                
                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时搜索热词记录");
                    _seachTextView.text = @"";
                    
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        NSLog(@"名字是：： %@",name);
                        if (kDeviceWidth == 320) {
                            
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                            
                        }else if (kDeviceWidth == 375)
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        else
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        
                        
                        
                    }
                }
                
            }

            
            
            //点播运营组
            if ([type isEqualToString:VOD_OPERATION_LIST]) {
                
                
                
                NSLog(@"%@",result);
                NSArray *list = [result objectForKey:@"operationList"];
                if ([list respondsToSelector:@selector(count)]) {
                    
                    _scroll.titledataArray.array = list ;
                    NSLog(@"1反会了这么么多数据%ld",(unsigned long)_scroll.titledataArray.count);
                    if (selectIndex < list.count) {
                        _scroll.MubiaoNum= selectIndex;
                    }else
                    {
                        _scroll.MubiaoNum = 0;
                    }
                    
                    [_scroll updataTitle];
                    
                    if (_scroll.titledataArray.count > selectIndex) {
                        NSDictionary *dic = _scroll.titledataArray[selectIndex];
                        NSString *title = [dic objectForKey:@"operationName"];
//                        [_AD updateDIC:[self adImageAndTitle:title]];
                        NSLog(@"biaoti %@",title);
                    }else
                    {
                        NSDictionary *dic = _scroll.titledataArray[0];
                        NSString *title = [dic objectForKey:@"operationName"];
//                        [_AD updateDIC:[self adImageAndTitle:title]];
                        NSLog(@"22222biaoti %@",title);
                        
                    }
                    
                    
                    if (list.count > selectIndex) {
                        NSDictionary *requstDic = list[selectIndex];
                        NSString *opid = [requstDic objectForKey:@"operationCode"];
                        _operationcode = opid;
                        //             点播分类运营组
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        [request VODOperationCategoryListWithOperationCode:opid andTimeout:10];
                        
                    }else
                    {
                        NSDictionary *requstDic = list[0];
                        NSString *opid = [requstDic objectForKey:@"operationCode"];
                        
                        _operationcode = opid;
                        //             点播分类运营组
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        [request VODOperationCategoryListWithOperationCode:opid andTimeout:10];
                    }
                    
                     [self haibaoWithOperationcode:_operationcode andConfigLis:HBarray];
                    
                    
                    if (list.count) {
                        _tableViewCanScroll = YES;//可以滚动的
                        [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                    }else
                    {
                        _tableViewCanScroll = NO;//不可以滚动的
                        [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                    }
                     [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                }
            }
            
            
            //获取海报的专题列表
            if ([type isEqualToString:VOD_TOPIC_LIST]) {
                NSLog(@"%@",result);
                NSArray *list = [result objectForKey:@"topicList"];
                if ([list respondsToSelector:@selector(count)]) {
                    NSLog(@"反会了这么么多数据%ld",(unsigned long)list.count);
                   
                    //                    _ad.adArray   =  list;
                    //                    _ad.selectedIndex = 1;
                    
                    
                }
            }
            
            
            //点播运营组分类
            if ([type isEqualToString:VOD_OPER_CATEGORY_LIST]) {
                NSLog(@"分类是 %@",result);
                
                self.dataArray = nil;
                self.dataArray =[NSMutableArray array];
                
                
                NSDictionary *dic = result;
                NSArray *list = [dic objectForKey:@"categoryList"];
                NSInteger indexNum = 0;
                for (NSDictionary *zidian in list) {
                    NSString *title =[zidian objectForKey:@"name"];
                    NSString *categoryID =[zidian objectForKey:@"categoryID"];
                    NSMutableDictionary *titleDIC = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSArray arrayWithObject:@"waitingwaiting"],@"Array",title,@"title",categoryID,@"categoryID",nil];
                    [self.dataArray addObject:titleDIC];
                    
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    [request VODProgramListWithOperationCode:_operationcode andRootCategpryID:categoryID andStartSeq:1 andCount:6 andTimeout:10 andIndex:indexNum];
                    
                    indexNum++;
                    NSLog(@"请求的次数%ld",(long)indexNum);
                    
                }
                _hub.hidden = YES;
                //******1.设置要请求 的次数
                _reqCount = indexNum;
                NSLog(@"请求==的次数%ld",(long)indexNum);
                if (_reqCount==0) {
                    _reqFinish = YES;
                    
                    _tableViewCanScroll = NO;//不可以滚动的
                    [self.tableView_Base setContentOffset:CGPointMake(0, 0)];
                    [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                }
                NSLog(@"个数： %ld",(unsigned long)self.dataArray.count);
                [self.tableView_Base reloadData];
                
            }
            
            //分类下的数据电视电影点播分类运营组  节目列表
            if ([type isEqualToString:VOD_PROGRAM_LIST]) {
                DIANBO_count_jihao++;
                NSLog(@"点播分类运营组节目: %ld result:%@  type:%@",(long)status,result,type );
                NSInteger count  = [[result objectForKey:@"count"] integerValue];;
                _reqCount--;
                if (_reqCount==0) {
                    FinishRequest = YES;
                }
                if (count)
                {
                    _tableViewCanScroll = YES;//可以滚动的
                    
                    NSDictionary *dic = (NSDictionary*)result;
                    NSInteger index = [[dic objectForKey:@"index"]integerValue];
                    
                    if (index < self.dataArray.count) {
                        
                        NSMutableDictionary *zidian = (NSMutableDictionary*)[self.dataArray objectAtIndex:index];
                        NSLog(@"这个是%ld类型的L : %ld %@",count,(long)index,[zidian objectForKey:@"title"]);
                        if (count) {
                            NSArray *array = [dic objectForKey:@"vodList"];
                            if (array) {
                                [zidian setObject:array forKey:@"Array"];
                            }
                            for (NSDictionary *list in array) {
                                NSLog(@"-------|| %ld %@列*表 \n%@ \n%@ \n%@\n",(long)index,[zidian objectForKey:@"title"],[list objectForKey:@"seq"],[list objectForKey:@"name"],[list objectForKey:@"lastEpisode"]);
                            }
                            
                        }
                        [self.tableView_Base reloadData];
                        if (DIANBO_count_jihao == 1) {
                            _hub.hidden = YES;
                        }
                        NSLog(@"请求的次数是 ： %ld",_reqCount);
                        if (DIANBO_count_jihao == _reqCount) {
                            _reqFinish = YES;
                            NSLog(@"请求完成了啊啊啊啊啊啊啊啊 啊啊");
                            
                            _hub.hidden = YES;
                            
                            if (self.dataArray.count) {
                                _tableViewCanScroll = YES;//可以滚动的
                                [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                            }else
                            {
                                _tableViewCanScroll = NO;//不可以滚动的
                                [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                            }
                            
                        }
                    }
                }
                
                
                
//                [self setBaseTableView:self.tableView_Base ScrollEnabled:_tableViewCanScroll];
                
            }
            
            
            //顶部的搜索文字
            if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
                NSLog(@"热门记录: %ld result:%@  type:%@",status,result,type );
                
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                
                if ([count integerValue] == -1 ) {
                    NSLog(@"暂时热门记录");
                    HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
                    [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
                }else if ([count integerValue] == 1 )
                {
                    NSArray *array = [dic objectForKey:@"keyList"];
                    if (array.count) {
                        NSDictionary *record = array[0];
                        NSString *name = [record objectForKey:@"name"];
                        if (kDeviceWidth == 320) {
                            
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                            
                        }else if (kDeviceWidth == 375)
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                        else
                        {
                            _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                        }
                    }
                    
                }
                
            }
            
            if ([type isEqualToString:VOD_MenuConfigList]) {
                NSLog(@"%@",result);
                NSLog(@"%@",_operationcode);
                
                HBarray = [result objectForKey:@"configList"];
                if (_operationcode) {
                    [self haibaoWithOperationcode:_operationcode andConfigLis:HBarray];
                }
               
                
                
            }
            
        }
        
            break;
            
        default:
            break;
    }
    
    
    
    
}

- (void)haibaoWithOperationcode:(NSString*)op andConfigLis:(NSArray*)array
{
    for (NSDictionary *dic in array) {
        NSString *code = [dic objectForKey:@"code"];
        if ([code isEqualToString:op]) {
            NSString *imageLink = [dic objectForKey:@"imageLink"];
            NSString *name = [dic objectForKey:@"name"];
            NSString *contentID = [dic objectForKey:@"contentID"];
            NSString *programName = [dic objectForKey:@"programName"];
            
            if (name.length&&programName.length&&contentID.length&&imageLink.length) {
                NSDictionary *diction = [NSDictionary dictionaryWithObjectsAndKeys:imageLink,@"Image",name,@"topicName",programName,@"programName",contentID ,@"contentID",nil];
                
                [_AD updateDIC:diction];
                return;
            }
           
        }else
        {
            NSDictionary *diction = [NSDictionary dictionaryWithObjectsAndKeys:@"NULL",@"Image",@"NULL",@"topicName",@"NULL",@"programName",@"NULL" ,@"contentID",nil];
            
            [_AD updateDIC:diction];
        }
    }
    
    
}

#pragma  mark - 刷新界面，请求
- (void)reRequestHomeData
{
    //距离上次请求已经超过规定时间了可以刷新
    NSLog(@"要刷新首页了");
    NSInteger freshtime;
    if (!_first) {
        freshtime = 10;
        
    }else
    {
        freshtime = 90;
        _first = NO;
    }
    
    if([self RequstEnabledWithFirstTime:_requsetDate andSecondData:[NSDate date] andTime:freshtime])
    {
        //请求时间
//        _requestDate = [NSDate date];
        _requestDate = [NSDate date];//开始第一次大请求
        FinishRequest = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            FinishRequest = YES;
        });
        NSLog(@"重新加载页面111");;
        
        //        if (_reqFinish) {
        
        NSLog(@"完了再请吧");
        //            self.dataArray = [NSMutableArray array];
        //1请求header的title们，
        HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
        //*****1请求运营组列表
        [requst VODOperationListWithTimeout:10];
        
        //*****2请求搜索热门字第一天
        //            [requst VODTopicListWithTimeout:10];
        
       
        if(IS_SUPPORT_VOD)
        {
            //*****3请求搜索热门字第一天
            [requst VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
        }else{
            [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
        }
        
        //        }
        
    }
}





#pragma  mark -header跟随tabelView动
//header跟随tabelView动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView_Base)
    {
        //YOUR_HEIGHT 为最高的那个headerView的高度
        CGFloat sectionHeaderHeight = 40;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (NSDictionary*)adImageAndTitle:(NSString*)name
{
    
    NSDictionary *dic4k专区 = @{@"name":@"4k专区",@"Image":[UIImage imageNamed:@"dianbo1"],@"topicName":@"宇宙时空之旅3",@"contentID":@"12bd25ebe36c4ab3939867477"};
    NSDictionary *dic精彩点 = @{@"name":@"精彩点",@"Image":@"http://116.199.7.7//CMS/20150828/2015082818315254995_450_645.jpg?id=1484&crc=4149446701",@"topicName":@"何以笙箫默",@"contentID":@"3145726665674faf8844ca03d"};
    NSDictionary *dic少儿乐园 =@{@"name":@"少儿乐园",@"Image":@"http://116.199.7.7//CMS/20150825/2015082520572347745_450_645.jpg?id=1232&crc=1845707656",@"topicName":@"哆啦A梦：伴我同行",@"contentID":@"b0351b5af9b04a46a6f12924f"};
    NSDictionary *dic院线大片 =@{@"name":@"院线大片",@"Image":@"http://116.199.7.7//CMS/20150909/2015090911481733087_450_645.jpg?id=1516&crc=1443289639",@"topicName":@"银河系漫游指南",@"contentID":@"ba28911ae23643d7ad965f119"};
    NSDictionary *dic潮汕专区 =@{@"name":@"潮汕专区",@"Image":@"http://116.199.7.7//CMS/20150825/2015082521491526084_450_645.jpg?id=1366&crc=2056684896",@"topicName":@"天南地北潮汕人",@"contentID":@"412312c295164af69c5cbc2db55a02d7"};
    
    NSArray *array = @[dic4k专区,dic精彩点,dic少儿乐园,dic院线大片,dic潮汕专区];

    NSDictionary *Anserdic = [NSDictionary dictionary];
    for (NSDictionary *dic in array) {
        if ([[dic objectForKey:@"name"] isEqualToString:name]) {
            Anserdic = dic;
            break;
        }
    }
    return Anserdic;
}

#pragma mark - 工具类
- (BOOL)isMovieOR:(NSString*)string
{
    if ([@"电影" rangeOfString:string].length) {
        return YES;
    }
    return NO;
}

- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    if ([date1 timeIntervalSinceDate:date] >= 10) {
        NSLog(@"超时了啊");
        return YES;
    }
    return NO;
}

//可以放在工具类中，开一个时间是不是超时了
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1 andTime:(NSInteger) time
{
    
    return FinishRequest;
    
    
//    if ([date1 timeIntervalSinceDate:date] >= time) {
//        NSLog(@"超时了啊");
//        return YES;
//    }
//    return NO;
}

//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        return numb % 3 == 0?  numb/3 : numb/3+1;
    }
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    if (array.count > index*3) {
        [anser addObject:array[index*3]];
    }
    if (array.count > index*3+1) {
        [anser addObject:array[index*3+1]];
    }
    if (array.count > index*3+2) {
        [anser addObject:array[index*3+2]];
    }
    
    return anser;
}
//设置tableView可不可以滚动
- (void)setBaseTableView:(UITableView*) tableView ScrollEnabled:(BOOL)enable
{
    tableView.scrollEnabled = enable;
    tableView.hidden = !enable;
}
#pragma mark -
- (void)dealloc
{
    NSLog(@"估计不会出现释放吧");
    _HeadView = nil;
    _ad = nil;
    _scroll = nil;
}






@end







//        headScroll *scroll = [headScroll new];
//        [_HeadView addSubview:scroll];
//        [scroll mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(_HeadView.mas_top);
//            make.left.equalTo(_HeadView.mas_left);
//            make.right.equalTo(_HeadView.mas_right);
//            make.height.equalTo(@(32*kRateSize ));
//        }];
//        self.automaticallyAdjustsScrollViewInsets  = NO;

//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            ad.selectedIndex = scroll.dataArray.count;
//        });

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return [[self tableView:tableView cellForRowAtIndexPath:indexPath] frame].size.height;
//}
