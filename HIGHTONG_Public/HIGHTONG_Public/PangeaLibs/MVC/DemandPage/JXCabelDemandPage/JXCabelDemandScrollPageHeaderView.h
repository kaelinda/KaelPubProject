//
//  JXCabelDemandScrollPageHeaderView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface JXCabelDemandScrollPageHeaderView : UIView<SDCycleScrollViewDelegate>
/**
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有类型
 */
@property (nonatomic,strong) NSMutableArray *cycleTypes;

/**
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有参数（包括条转链接，id等）
 */
@property (nonatomic,strong) NSMutableArray *cycleParames;
/**
 *  @author Kael
 *
 *  @brief 轮播图数组 这里面包含轮播图所需要的所有数据
 */
@property (nonatomic,strong) NSMutableArray *cycleImagesData;

/**
 *  @author Kael
 *
 *  @brief 轮播图标题数组
 */
@property (nonatomic,strong) NSMutableArray *cycleTitles;

/**
 *  @author Kael
 *
 *  @brief 轮播图 本地图片数组
 */
@property (nonatomic,strong) NSMutableArray *cycleImages;

/**
 *  @author Kael
 *
 *  @brief 轮播图网络图片数组
 */
@property (nonatomic,strong) NSMutableArray *cycleLinks;

/**
 *  @author Kael
 *
 *  @brief 整个header的数据
 */
@property (nonatomic,strong) NSMutableDictionary *headerResult;

/**
 *  @author Kael
 *
 *  @brief 轮播图视图
 */
@property (nonatomic,strong) SDCycleScrollView *cycleScrollView;



@property (nonatomic,strong) void (^ headerViewReactChangedBlock)(CGRect frame);

/**
 *  @author Kael
 *
 *  @brief 选中轮播图中的某一个按钮
 */
@property (nonatomic,copy) void (^ didSelectedImagesBclok)(NSString *channelID,NSString *title);


-(void)reloadHeaderResult:(NSDictionary *)headerResult;

@end
