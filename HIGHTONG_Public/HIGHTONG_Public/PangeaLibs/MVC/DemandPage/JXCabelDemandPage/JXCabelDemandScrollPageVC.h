//
//  JXCabelDemandScrollPageVC.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HT_FatherViewController.h"
#import "JXCabelDemandScrollPageHeaderView.h"
#import "YJProgramListCell.h"
//#import "ProgramListCell.h"
#import "JXCabelProgramCell.h"
#import "UIButton_Block.h"
#import "DemandSecondViewcontrol.h"
#import "HTEmptyView.h"
@interface JXCabelDemandScrollPageVC : HT_FatherViewController<UITableViewDelegate,UITableViewDataSource>
/**
 *  @author Kael
 *
 *  @brief 是否需要导航条
 */
@property (nonatomic,assign) BOOL isNeedNavBar;


@property (nonatomic,strong) JXCabelDemandScrollPageHeaderView *headerView;


@property (nonatomic,strong)NSMutableArray *demandDisaplayArray;

@property (nonatomic,strong)NSMutableArray *demandListArray;

@property (nonatomic,strong)UITableView *disPlayTableView;

@property (nonatomic,strong)NSString *disPlayDemandType;

@property (nonatomic,strong)NSString *disPlayDemandCode;

@property (nonatomic,strong)NSString *disPlayDemandName;

@property (nonatomic,strong)NSDictionary *disPlayHeaderDic;

/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;


-(void)disPlayDemandData:(NSMutableArray *)disPlayData;

//-(void)disPlayRequestData:(NSMutableArray *)menuArr andType:(NSString *)type;


-(void)disPlayRequestCode:(NSString *)code andType:(NSString *)type andHeaderDic:(NSDictionary*)headerDic;

-(void)disPlayRequestCode:(NSString *)code andType:(NSString *)type andHeaderArray:(NSMutableArray*)headerArr andSeq:(NSString *)seqFlay;
@end
