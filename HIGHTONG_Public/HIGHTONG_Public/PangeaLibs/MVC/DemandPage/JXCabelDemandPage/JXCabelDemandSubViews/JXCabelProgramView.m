//
//  JXCabelProgramView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "JXCabelProgramView.h"
#import "UIImageView+AFNetworking.h"
@interface JXCabelProgramView ()
{
    
    UIButton_Block * _btn;
    UIImageView *BGview;
    UIImageView * Badge ;
    
}

@end
@implementation JXCabelProgramView
- (instancetype)init
{
    if (self = [super init]) {
        //        self.backgroundColor = [UIColor lightGrayColor];
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    UIView *superView = self;
    
    self.photo = [UIImageView new];
    [self.photo setBackgroundColor:[UIColor clearColor]];
    [superView addSubview:self.photo];
    
    
    
    self.subTitle = [UILabel new];
        [self.subTitle setBackgroundColor:[UIColor clearColor]];
    [superView addSubview:self.subTitle];
    
    
    self.timeTitle = [UILabel new];
        [self.timeTitle setBackgroundColor:[UIColor clearColor]];
    [superView addSubview:self.timeTitle];
    
    [self.photo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom).offset((-23)*kDeviceRate);
    }];
    
    //    self.photo.backgroundColor = [UIColor redColor];
    //    self.photo.contentMode = UIViewContentModeScaleAspectFit;
    self.photo.image =[UIImage imageNamed:@"poster"] ;
    
    self.photo.backgroundColor = UIColorFromRGB(0xe3e2e3);
    //江西版本不需要圆角
    self.photo.layer.cornerRadius = 5;
    self.photo.layer.masksToBounds = YES;
    
    Badge = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_common_special_net"]];
    //    Badge.backgroundColor = [UIColor redColor];
    [self.photo addSubview:Badge];
    [Badge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.photo.mas_top);
        make.right.equalTo(self.photo.mas_right);
        make.width.equalTo(@(42*kDeviceRate));
        make.height.equalTo(@(42*kDeviceRate));
        
    }];
    
    BGview = [UIImageView new];
    [self.photo addSubview:BGview];
    [BGview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.photo.mas_left);
        make.right.equalTo(self.photo.mas_right);
        make.bottom.equalTo(self.photo.mas_bottom);
        make.height.equalTo(@(15*kDeviceRate));
    }];
    BGview.image = [UIImage imageNamed:@"bg_home_poster_content"];
    BGview.layer.cornerRadius = 5;
    BGview.layer.masksToBounds = YES;
    self.title = [UILabel new];
    self.title.textColor = [UIColor whiteColor];
    self.title.backgroundColor = [UIColor clearColor];
    [BGview addSubview:self.title];
    self.title.text = @"当前没有数据";
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(BGview);
    }];
    
    
    
    
    
    [self.subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(self.photo.mas_bottom).offset((3+2)*kDeviceRate);
        //        make.bottom.equalTo(superView.mas_bottom).offset(-18*kRateSize);
        make.bottom.equalTo(superView.mas_bottom).offset(-10*kDeviceRate);
    }];
    self.subTitle.text = @"当前没有数据";
    //    self.subTitle.backgroundColor = [UIColor redColor];
    
    //    [self.timeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.equalTo(superView.mas_left);
    //        make.right.equalTo(superView.mas_right);
    //        make.top.equalTo(self.subTitle.mas_bottom);
    //        make.bottom.equalTo(superView.mas_bottom).offset(0);
    //    }];
    //            self.timeTitle.backgroundColor = [UIColor yellowColor];
    self.timeTitle.text = @"当前没有数据";
    
    
    
    UIFont *font  = [UIFont systemFontOfSize:10*kRateSize];
    self.title.font = HT_FONT_FIFTH; //font;
    font =  [UIFont systemFontOfSize:15*kRateSize];
    self.subTitle.font =  [UIFont systemFontOfSize:11*kDeviceRate];//  font;
    font =  [UIFont systemFontOfSize:11*kRateSize];
    self.timeTitle.font = font;
    self.subTitle.textColor = UIColorFromRGB(0x242424);
    
    _btn  = [UIButton_Block new];
    [self addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.photo);
    }];
    
    WS(wself);
    _btn.serviceIDName = @"ID";
    _btn.eventName = self.title.text;
    [_btn setImage:[UIImage imageNamed:@"bg_common_vod_pressed"] forState:UIControlStateHighlighted];
    _btn.Click = ^(UIButton_Block*btn,NSString * name)
    {
        if (wself.block!=nil) {
            NSLog(@"cell知道点击了那个view");
            wself.block(btn);
        }
    };
    
    
    //    self.backgroundColor = [UIColor orangeColor];
}
- (void)updata
{
    WS(wSelf);
    
    
    //    contentID = 8e0e314d1cb24bd9b60b74e1a;
    //    imageLink = "http://192.168.3.23//CMS/20150814/2015081410400391451_450_645.jpg?id=652&crc=1484819196";
    //    name = "\U7834\U574f\U8005";
    //    seq = 3;
    NSDictionary *dic = self.dic;
    if (dic) {
        self.hidden = NO;
        //再赋值之前要判断   电影，  电视剧  综艺   或者其他， 以显示不同的效果
        if ([dic objectForKey:@"amount"] &&  [dic objectForKey:@"lastEpisode"]) {//电视剧应为有总集数

            NSString *lastEpisodeTitle = [dic objectForKey:@"lastEpisodeTitle"];
            //            if ([lastEpisode integerValue] ==  [amout integerValue]) {
            //                self.title.text = [NSString stringWithFormat:@"%@集全",amout];
            //            }else
            //            {
            //                self.title.text = [NSString stringWithFormat:@"更新至%@集",lastEpisode];
            //            }
            if (lastEpisodeTitle.length) {
                self.title.text = [NSString stringWithFormat:@"%@",lastEpisodeTitle];
            }
            
        }else
        {
            self.title.text = @"";
        }
        
        
        if (self.isMovie) {
            self.title.hidden = YES;
            BGview.hidden = YES;
        }else
        {
            self.title.hidden = NO;
            BGview.hidden = NO;
        }
        
        if([[dic objectForKey:@"flagPlayOuter"] isEqualToString:@"1"])
        {
            Badge.hidden = NO;
        }else{
            Badge.hidden = YES;
        }
        //        else if ([dic objectForKey:@"playTime"])
        //        {
        //            NSString *time = [dic objectForKey:@"playTime"];
        //            self.title.text = [NSString stringWithFormat:@"更新至%@",time];
        //        }
        //        else
        //        {
        //
        //        }
        if ([dic objectForKey:@"name"]) {
            self.subTitle.text = [dic objectForKey:@"name"];
        }
        
        if ([dic objectForKey:@"imageLink"]) {
            
            [self tranAnimationWith:self.photo andImageLink:[dic objectForKey:@"imageLink"]];
            //             [self.photo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"imageLink"])] placeholderImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];//公版改版
        }
        
        if ([dic objectForKey:@"contentID"]) {
            _btn.serviceIDName = [dic objectForKey:@"contentID"];
        }
        _btn.eventName = self.subTitle.text;
        
        if ([dic objectForKey:@"type"]) {
            _btn.polyMedicType = [dic objectForKey:@"type"];
        }
        _btn.Click = ^(UIButton_Block*btn,NSString * name)
        {
            if (wSelf.block) {
                NSLog(@"cell知道点击了那个view");
                wSelf.block(btn);
            }
        };
        
    }else
    {
        self.hidden = YES;
        self.photo.image =[UIImage imageNamed:@"poster"] ;
        self.title.text = @"";
        self.subTitle.text = @"";
        self.timeTitle.text = @"";
    }
    
}




-(void)tranAnimationWith:(UIImageView *)imageView andImageLink:(NSString *)imageLink{
    
    CATransition *animation = [CATransition animation];
    //动画时间
    animation.duration = 0.3f;
    //display mode, slow at beginning and end
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //在动画执行完时是否被移除
    animation.removedOnCompletion = YES;
    //过渡效果
    animation.type = kCATransitionFade;
    //    animation.type = @"rippleEffect";
    
    //过渡方向
    animation.subtype = kCATransitionFromTop;
    //    //暂时不知,感觉与Progress一起用的,如果不加,Progress好像没有效果
    //    animation.fillMode = kCAFillModeForwards;
    //    //动画停止(在整体动画的百分比).
    //    animation.endProgress = 1;
    [imageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)]  placeholderImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [imageView.layer addAnimation:animation forKey:nil];
    }];
    
    
    
    
}


@end
