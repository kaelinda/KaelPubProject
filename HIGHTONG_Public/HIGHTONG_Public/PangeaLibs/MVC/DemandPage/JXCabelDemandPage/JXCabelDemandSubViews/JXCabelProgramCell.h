//
//  JXCabelProgramCell.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCabelProgramView.h"
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@class JXCabelProgramCell;
typedef void (^ProgramCellBlock) (UIButton_Block*);

@interface JXCabelProgramCell : UITableViewCell
@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,copy)ProgramCellBlock block;

@property (nonatomic,strong)NSMutableArray *dateArray; //存放字典数组

@property (nonatomic,strong)JXCabelProgramView *leftView;//左边

@property (nonatomic,strong)JXCabelProgramView *middleView;//中间

@property (nonatomic,strong)JXCabelProgramView *rightView;//右边

//@property (assign,nonatomic)CGFloat interval;//间隙默认

- (void)update;//跟新数据
@end
