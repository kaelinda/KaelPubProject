//
//  JXCabelDemandMenuVC.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "JXCabelDemandMenuVC.h"
#import "HTRequest.h"
#import "VODPageRequestManger.h"
#import "JXCabelDemandScrollPageVC.h"
#import "SearchViewController.h"
#import "UIButton_Block.h"
#import "CustomSearchBarView.h"


#define JXCabelMenuSelectColor   UIColorFromRGB(0xc32011)
#define JXCabelMenuColor         UIColorFromRGB(0x010101)
#define JXCabelMenuFont          [UIFont systemFontOfSize:14*kDeviceRate]
#define JXCabelWhiteColor        [UIColor whiteColor]
@interface JXCabelDemandMenuVC ()<VTMagicViewDataSource,VTMagicViewDelegate,VODPageRequestDelegate,HTRequestDelegate>
{
    UIImageView *Guide ;//引导图    
}

@property (strong,nonatomic)NSMutableArray *menuPageArray;
@property (strong,nonatomic)NSMutableArray *menuNameList;
@property (strong,nonatomic)NSMutableArray *vodDisplayArray;
@property (strong,nonatomic)CustomSearchBarView *searchBarView;

@end

@implementation JXCabelDemandMenuVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view bringSubviewToFront:Guide];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request VODTopSearchKeyWithKey:nil andCount:1 andTimeout:10];
    
    self.view.backgroundColor = JXCabelWhiteColor;
    
    self.magicView.navigationColor = JXCabelWhiteColor;
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDefault;
    self.magicView.navigationInset = UIEdgeInsetsMake(0, 15*kDeviceRate, 0, 25*kDeviceRate);
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 10.0;
    self.magicView.itemScale = 1.1;
    self.magicView.itemSpacing = 25*kDeviceRate;
    _menuNameList = [NSMutableArray array];
    
    VODPageRequestManger *vodMenu = [[VODPageRequestManger alloc]initWithDelegate:self];
    [vodMenu reloadVODMenuData];
    [self showCustomeHUD];
    [self setSeachBar];
    
    //点播页的引导
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODGUIDE"];
    if (string.length == 0 && IS_EPG_CONFIGURED) {
        Guide = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
        UIImage *image = [UIImage imageNamed:@"bg_first_geture"];
        
        Guide.image =  image;
        Guide.userInteractionEnabled = YES;
        
        UIImageView *guideTap = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+4*kRateSize, kDeviceWidth, 110*kRateSize)];
        UIImage *imageTap = [UIImage imageNamed:@"ic_first_tap_gesture"];
        
        guideTap.image = imageTap;
        
        [Guide addSubview:guideTap];
        
        UIButton_Block *block = [[UIButton_Block alloc]initWithFrame:Guide.frame];
        [Guide addSubview:block];
        block.Click = ^(UIButton_Block *btn,NSString *name){
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VODGUIDE"];
            [Guide removeFromSuperview];
        };
        
        [self.tabBarController.view  addSubview:Guide];
    }
}

//设置搜索栏
- (void)setSeachBar
{
    _searchBarView = [[CustomSearchBarView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth-90, 28)];
    _searchBarView.center = CGPointMake(self.m_viewNaviBar.centerX, self.m_viewNaviBar.centerY + 15);
    [self.m_viewNaviBar addSubview:_searchBarView];

    WS(wself);
    _searchBarView.searchBlock = ^()
    {
        SearchViewController *search = [[SearchViewController alloc]init];
        [wself.navigationController pushViewController:search animated:YES];
    };
        
    if (!HomeNewDisplayTypeOn) {
        [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 27, kDeviceWidth-30*2*kRateSize, 28)];
    }else{
        [self setNaviBarSearchViewFrame:CGRectMake(40*kRateSize, 27, kDeviceWidth-30*2*kRateSize-10*kRateSize, 28)];
    }
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if (status==0) {
        
        if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
            NSLog(@"热门记录: %ld result:%@  type:%@",status,result,type );
        
            NSDictionary *dic = result;
            NSString *count = [dic objectForKey:@"count"];
        
            if ([count integerValue] == -1){
                NSLog(@"暂时搜索热词记录");
                HTRequest *requst = [[HTRequest alloc]initWithDelegate:self];
                [requst TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
                
            }else if ([count integerValue] == 1){
                
                NSArray *array = [dic objectForKey:@"keyList"];
                if (array.count) {
                    NSDictionary *record = array[0];
                    NSString *name = [record objectForKey:@"name"];
                    
                    _searchBarView.placeholderLab.text = [NSString stringWithFormat:@"%@",name];
                }
            }
        }
        if ([type isEqualToString:@"topSearchWords"]) {
            NSDictionary *dic = result;
            NSString *count = [dic objectForKey:@"count"];
            
            if ([count integerValue] == -1 ) {
                NSLog(@"暂时搜索热词记录");
                
            }else if ([count integerValue] == 1 ){
                NSArray *array = [dic objectForKey:@"keyList"];
                if (array.count) {
                    NSDictionary *record = array[0];
                    NSString *name = [record objectForKey:@"name"];
                    NSLog(@"名字是：： %@",name);
                    
                    _searchBarView.placeholderLab.text = [NSString stringWithFormat:@"%@",name];

                }
            }
        }
    }
}


-(void)VODMenuDataGetSuccessWith:(NSMutableArray *)result
{
    _menuPageArray = [NSMutableArray array];
    _menuPageArray = result;
    
    if (_menuPageArray.count>0) {
        [self hiddenCustomHUD];
        for (int i = 0; i<_menuPageArray.count; i++) {
            
            [_menuNameList addObject:[[_menuPageArray objectAtIndex:i] objectForKey:@"name"]];
            
        }
        [self.magicView reloadData];
        NSLog(@"最终获取到的Menu数据是%@",_menuNameList);
    }else
    {
        [AlertLabel AlertInfoWithText:@"暂无数据，请重试" andWith:self.view withLocationTag:2];
    }
    
}




#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    if (_menuNameList.count>0) {
        
        return _menuNameList;
        
    }
    return 0;
}


- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:JXCabelMenuColor forState:UIControlStateNormal];
        [menuItem setTitleColor:JXCabelMenuSelectColor forState:UIControlStateSelected];
        menuItem.titleLabel.font = JXCabelMenuFont;
    }
    return menuItem;
}


- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    static NSString *gridIdd = @"grid.Identifier";
    JXCabelDemandScrollPageVC *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
    if (!viewController) {
        
        viewController = [[JXCabelDemandScrollPageVC alloc]init];
        
        if (_menuPageArray.count>0) {
            
            for (NSDictionary *dic in _menuPageArray) {
                
                if ([[dic objectForKey:@"seq"] isEqualToString:[NSString stringWithFormat:@"%ld",pageIndex+1]]) {
                    
                    viewController.disPlayDemandCode = [dic objectForKey:@"code"];
                    viewController.disPlayDemandType = [dic objectForKey:@"type"];
                    viewController.disPlayDemandName = [dic objectForKey:@"name"];
                    [viewController setDisPlayHeaderDic:dic];
                    
                    viewController.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,[dic objectForKey:@"code"]];
                    viewController.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,[dic objectForKey:@"name"]];
                    viewController.EN = [NSString stringWithFormat:@"%@",self.EN];
                    
                }
            }
            
        }
        
    }
    
    viewController.isNeedNavBar = YES;
    
    
    NSLog(@"滑动的PageIndex%ld",pageIndex);
    
    return viewController;
    
}


#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(JXCabelDemandScrollPageVC *)viewController atPage:(NSUInteger)pageIndex
{
    if (_menuPageArray.count>0) {
        
        for (NSDictionary *dic in _menuPageArray) {
            
            if ([[dic objectForKey:@"seq"] isEqualToString:[NSString stringWithFormat:@"%ld",pageIndex+1]]) {
                [viewController setDisPlayHeaderDic:dic];
                [viewController setDisPlayDemandName:[dic objectForKey:@"name"]];
                [viewController disPlayRequestCode:[dic objectForKey:@"code"] andType:[dic objectForKey:@"type"] andHeaderDic:dic];
                
                viewController.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,[dic objectForKey:@"code"]];
                viewController.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,[dic objectForKey:@"name"]];
                viewController.EN = [NSString stringWithFormat:@"%@",self.EN];
            }
        }
        
        
        NSLog(@"已经显示的这个index%ld",pageIndex);
        
    }
    
}




-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
