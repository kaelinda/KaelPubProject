//
//  JXCabelDemandScrollPageHeaderView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "JXCabelDemandScrollPageHeaderView.h"
#define kCycleScrollViewHeight (173*kDeviceRate)
@interface JXCabelDemandScrollPageHeaderView ()
{
    CGFloat kWidth;
    CGFloat kHeight;
}
@end
@implementation JXCabelDemandScrollPageHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        //初始化数据
        [self initData];
        //添加子视图
        [self initSubViews];
        
    }
    
    return self;
}

-(void)initData
{
    kWidth = self.frame.size.width;
    kHeight = self.frame.size.height;
    
    _cycleTypes = [[NSMutableArray alloc]init];
    _cycleParames= [[NSMutableArray alloc]init];
    _cycleImages = [[NSMutableArray alloc]init];
    _cycleLinks= [[NSMutableArray alloc]init];
    _cycleTitles = [[NSMutableArray alloc]init];
}


-(void)initSubViews{
    
    //初始化轮播图
    [self initCycleScrollView];
    //初始化选择器界面
    //    [self initFilterView];
    
}


-(void)initCycleScrollView
{
    WS(wself);
    CGRect cycleRect = CGRectMake(0, 0, kDeviceWidth, kCycleScrollViewHeight);
    
    
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:cycleRect delegate:self placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
    _cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    _cycleScrollView.titlesGroup = _cycleTitles;
    _cycleScrollView.pageDotImage = [UIImage imageNamed:@"ic_home_poster_mark_def"];
    _cycleScrollView.currentPageDotImage = [UIImage imageNamed:@"ic_home_poster_mark_sel"]; // 自定义分页控件小圆标颜色
    _cycleScrollView.autoScrollTimeInterval = PAGESCROLLTIME;
    [self addSubview:_cycleScrollView];
    
    
    [_cycleScrollView setBackgroundColor:App_white_color];
 
    __block NSMutableArray *parames = _cycleParames;
    __block NSMutableArray *titles = _cycleTitles;
    //    __block NSMutableArray *ids ;
    _cycleScrollView.clickItemOperationBlock = ^(NSInteger index) {
        NSLog(@">>>>>  %ld", (long)index);
        //        NSString *eventProgram = [imagesData objectAtIndex:index];
        //        if (isEmptyStringOrNilOrNull(eventProgram)) {
        //            eventProgram = @"";
        //        }
        if (wself.didSelectedImagesBclok) {
            wself.didSelectedImagesBclok([parames objectAtIndex:index],[titles objectAtIndex:index]);
        }
    };
    
    _cycleScrollView.itemDidScrollOperationBlock = ^(NSInteger index){
        NSLog(@">>>>>  %ld", (long)index);
        //        NSString *eventProgram = [imagesData objectAtIndex:index];
        //        if (isEmptyStringOrNilOrNull(eventProgram)) {
        //            eventProgram = @"";
        //        }
        if (wself.didSelectedImagesBclok) {
            wself.didSelectedImagesBclok([parames objectAtIndex:index],[titles objectAtIndex:index]);
        }
    };
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _cycleScrollView.imageURLStringsGroup = _cycleLinks;
    });
    
    
    
}



-(void)reloadHeaderResult:(NSDictionary *)headerResult
{
    [self reloadCycleImage:headerResult];
}


-(void)reloadCycleImage:(NSDictionary *)dicImage
{
    
    [_cycleLinks removeAllObjects];
    [_cycleTitles removeAllObjects];
    [_cycleParames removeAllObjects];
    //    _cycleLinks = [NSMutableArray array];
    //    _cycleParames = [NSMutableArray array];
    //    _cycleTitles = [NSMutableArray array];
    NSString * imageLink = [dicImage objectForKey:@"imageLink"];
    NSString * titleLink = [dicImage objectForKey:@"programName"];
    NSString * contentIDLink = [dicImage objectForKey:@"contentID"];
    
    [_cycleLinks addObject:NSStringIMGFormat(imageLink)];
    [_cycleTitles addObject:titleLink];
    [_cycleParames addObject:contentIDLink];
    
    
    [_cycleScrollView setTitlesGroup:_cycleTitles];
    [_cycleScrollView setImageURLStringsGroup:_cycleLinks];
    
    
}

@end
