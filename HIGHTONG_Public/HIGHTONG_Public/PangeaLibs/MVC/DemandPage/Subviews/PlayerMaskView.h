//
//  PlayerMaskView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/12/1.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
#import "LoadingView.h"
#import "PolyMedicineLoadingView.h"
typedef void(^MaskViewReturn)();
typedef void(^RetryActionBlock)();

@interface PlayerMaskView : UIView
@property (nonatomic,strong) UIView *blackView;
@property (nonatomic,strong) UIImageView *backImageView;//背景图
@property (nonatomic,strong) UILabel *pointLbael;//提示label
@property (nonatomic,strong) LoadingView *loadingView;//加载转动视图
//@property (nonatomic,strong) PolyMedicineLoadingView *loadingView;
@property (nonatomic,strong) UIButton_Block *actionBtn;//行为按钮
@property (nonatomic,strong) UIButton_Block *playBtn;//播放按钮
@property (nonatomic,strong) UIButton_Block *retryBtn;//重试按钮
@property (nonatomic,strong) UIButton_Block *installBtn;//报装按钮
@property (nonatomic,strong) UILabel *progressLabel;//加载进度label
@property (nonatomic,assign) PlayerMaskViewType programType;//类型 0 暂停按钮  1电影订购 2电视剧剧集订购  3确认是否登录 4   5----->拥有黑色遮罩的视频loading框   6.接入广电网络才可看--cap 7.报装的按钮


//江西的Label和按钮
@property (nonatomic,strong)UILabel *jxPointLeftLabel;
@property (nonatomic,strong)UILabel *jxPointRightLabel;
@property (nonatomic,strong)UIButton_Block *jxLoginBtn;
@property (nonatomic,strong)UIImageView *jxPointIV;
@property (nonatomic,assign)JXPlayerMaskViewType jxProgramType;
@property (nonatomic,strong)UIButton_Block *jxBindBtn;
@property (nonatomic,strong)UIImageView *jxBackImageView;



@property (nonatomic,copy) MaskViewReturn maskViewReturnBlock;//遮罩层上的按钮的响应事件 行为按钮和播放按钮 二选一
@property (nonatomic,copy) RetryActionBlock retryAction;//重试按钮的响应事件


-(instancetype)initWithAppType:(APPMaskType)type;

-(void)clearProgressWithStatus:(BOOL)isStart;

@end
