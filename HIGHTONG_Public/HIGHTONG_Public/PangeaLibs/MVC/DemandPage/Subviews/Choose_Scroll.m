//
//  Choose_Scroll.m
//  HIGHTONG
//
//  Created by testteam on 15/8/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "Choose_Scroll.h"

#import "UIButton_Block.h"
#define  jianxi 4

@interface Choose_Scroll ()
{
    UIButton_Block *selectedBtn;//被选择的按钮蓝色
}
@end


@implementation Choose_Scroll
- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}
- (instancetype)initWithArray:(NSArray*)array
{
    if ( self = [super init]) {
        
        
        
    }
    return self;
}
- (void)upDataViewWithArray:(NSDictionary*)diction
{
    NSArray *typelist = [diction objectForKey:@"typeList"];
    NSMutableArray *array = [NSMutableArray arrayWithArray:typelist];
    
    NSString *categoryID = [diction objectForKey:@"categoryID"];
    NSString *ID = [diction objectForKey:@"id"];
    
    if (typelist.count) {
        //如果有数category数组才插入一个总的
        
        //插入类型总名称  热门上架除外
        if (ID && ![ID isEqualToString:@"0"]) {
            NSDictionary *firstDIC = [NSDictionary dictionaryWithObjectsAndKeys:categoryID,@"categoryID",ID,@"id",@"全部",@"name", nil];
            [array insertObject:firstDIC atIndex:0];
        }
        
    }
    
    
    
    [self removeAllSubviews];
    UIView *contentview = [UIView new];
    [self addSubview:contentview];
    [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
        make.height.equalTo(self);
    }];
    
    NSInteger index = 0;

    UIButton_Block *last;
    for (NSDictionary *dic in array) {
        
        self.dataArray =array;
        
        NSString *title = [dic objectForKey:@"name"];
        index++;
        NSLog(@"%@",title);
        
        UIButton_Block *btn = [UIButton_Block new];
        [contentview addSubview:btn];
        CGSize size = [self MYString:title StringSizeOfFont:[UIFont systemFontOfSize:13*kRateSize]];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (last) {
                make.left.equalTo(last.mas_right).offset(jianxi*kRateSize);
            }else
            {
                make.left.equalTo(self).offset(jianxi*kRateSize);
            }
            make.top.equalTo(self);
            make.width.equalTo(@(size.width));
            make.bottom.equalTo(self);
        }];
        if (index == 1) {
            selectedBtn.selected = NO;
            selectedBtn = btn;
            selectedBtn.selected = YES;
        }
        last = btn;
        
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
        //        [btn setBackgroundImage:[UIImage imageNamed:@"Unchoose"] forState:UIControlStateNormal];
        btn.eventName = [NSString stringWithFormat:@"%ld",(long)index-1];
        btn.Click = ^(UIButton_Block*btn,NSString *name)
        {
            selectedBtn.selected = NO;
            selectedBtn = btn;
            selectedBtn.selected = YES;
            NSInteger index = [btn.eventName integerValue];
            NSLog(@"位置是: %@",btn.eventName);
            
            if (index<self.dataArray.count) {
                NSDictionary *dic = [self.dataArray objectAtIndex:index];
                NSMutableDictionary *result = [[NSMutableDictionary alloc]initWithDictionary:dic];
                [result setObject:ID forKey:@"index"];
                NSLog(@"%@ %@ %@",[dic objectForKey:@"id"],[dic objectForKey:@"categoryID"],[dic objectForKey:@"name"]);
                if (_Scroll_Block) {
                    _Scroll_Block(result,btn.eventName);
                }else
                {
                    NSLog(@"chooseView 还没有准备接收 字典");
                }
            }
            
        };
        
        //        [btn setBackgroundImage:[UIImage imageNamed:@"choose"] forState:UIControlStateSelected];
        //        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //***分割线
        {
            //        if (index != array.count) {
            //            UIView * lineview1 = [UIView new];
            //            [contentview addSubview:lineview1];
            //            lineview1.layer.cornerRadius = 2;
            //            [lineview1 mas_makeConstraints:^(MASConstraintMaker *make) {
            //                make.left.equalTo(btn.mas_right).offset(0.5*kRateSize);
            //                //                make.right.equalTo(btn.mas_right).offset(4*kRateSize);
            //                make.width.equalTo(@(3*kRateSize));
            //                make.top.equalTo(contentview.mas_top).offset(3.5*kRateSize);
            //                make.bottom.equalTo(contentview.mas_bottom).offset(-3*kRateSize);
            //            }];
            //            lineview1.backgroundColor =[UIColor lightGrayColor];
            //        }
        }
        //****
        
        
    }
    
    
    
    
    [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(last.mas_right);
    }];
    
    
    
    
    //
    
    
    
    
    
    
    
    //        btn.Click= ^(UIButton_Block*btn,NSString *name)
    //        {
    //            _index = [btn.eventName integerValue];
    //            NSLog(@"%ld",index);
    //
    //            lineView.backgroundColor = [UIColor blueColor];
    //
    //            if (_block) {
    //                NSLog(@"scrollView实现了block");
    //                _block(_index-1);
    //            }else
    //            {
    //                NSLog(@"scrollView没有实现block");
    //                _block(_index-1);
    //            }
    //
    //
    //            UIView *view = [btn viewWithTag:989898];
    //            view.hidden = NO;
    //            selected.hidden = YES;
    //            selected = view;
    //        };
    
    
    
    
    
    
    
}
- (CGSize)MYString:(NSString*) string StringSizeOfFont:(UIFont*)font
{
    CGSize size;
    if (isAfterIOS6) {
        size =  [string sizeWithFont:font forWidth:99999 lineBreakMode:NSLineBreakByCharWrapping];
        
        size.width += 30*kRateSize;
    }else
    {
        NSDictionary *attrs = @{NSFontAttributeName : font};
        size = [string boundingRectWithSize:CGSizeMake(30, 9999999) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    }
    
    return size;
}
@end
