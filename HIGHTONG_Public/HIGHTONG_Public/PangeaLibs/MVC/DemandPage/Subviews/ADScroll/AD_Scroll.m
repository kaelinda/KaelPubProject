

//
//  AD_Scroll.m
//  HIGHTONG
//
//  Created by testteam on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "AD_Scroll.h"
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"


@interface AD_Scroll ()<UIScrollViewDelegate>
{
    UIImageView *_BGview;
}



@end

@implementation AD_Scroll

//重写了get set 方法后
@synthesize selectedIndex = _selectedIndex;
- (id)init {
    self = [super init];
    if (!self) return nil;
    
    UIScrollView *scrollView = UIScrollView.new;
    self.scrollView = scrollView;
    self.scrollView.delegate = self;
    self.adArray =[NSMutableArray array];
    scrollView.backgroundColor = [UIColor grayColor];
    [self addSubview:scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    //    [self generateContent];
    
    return self;
}
- (void)setAdArray:(NSMutableArray *)adArray
{
    if (_adArray != adArray) {
        _adArray = nil;
        _adArray = adArray;
    }
    [self setup];
}


- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    NSLog(@"%f %f",self.scrollView.contentOffset.x,self.scrollView.contentOffset.y);
    //    [self.scrollView setContentOffset:CGPointMake(500, 100) animated:YES];
    
    if (selectedIndex <= self.adArray.count ) {
        NSDictionary *dic = self.adArray[selectedIndex-1];
        NSInteger num = 10000 + selectedIndex;
        
        UIImageView *view = (UIImageView *)[self.scrollView viewWithTag:num];
        if ([[dic objectForKey:@"bgImage"] length]) {
            [view sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"bgImage"])]];
        }else
        {
            UIImage *image = [dic objectForKey:@"Image"];
            view.image = image;
        }
        
        [self.scrollView scrollRectToVisible:view.frame animated:NO];
    }
    if (self.adArray.count >= selectedIndex) {
        NSDictionary *dic = self.adArray[selectedIndex-1];
        if ([dic objectForKey:@"topicName"]) {
            self.titleLabel.text = [dic objectForKey:@"topicName"];
            
        }
        
    }
    
    
    
}
- (NSInteger)selectedIndex
{
    
    return _selectedIndex;
}
- (void)setup
{
    
    
    
    if (self.adArray.count) {
        
        [self.scrollView removeAllSubviews];
        
        UIView *contentView = [UIView new];
        
        [self.scrollView addSubview:contentView]; //****1  scrollview
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.scrollView);
            make.height.equalTo(self.scrollView);//****2
        }];
        
        //        UIView *
        
        
        UIView *lastView;//****3
        NSInteger i = 0;
        
        for (NSDictionary *dic in self.adArray) {
            UIImageView * view = [UIImageView new];
            [contentView addSubview:view];
            i++;
            view.tag = i+10000;
            view.backgroundColor = [self randomColor];
            NSLog(@"次数");
            //            lastView ?  NSLog(@"次数1111"): NSLog(@"次数2222");
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                //****4
                make.left.equalTo(lastView ? lastView.mas_right:contentView.mas_left);
                make.top.equalTo(contentView.mas_top);
                make.width.equalTo(self.mas_width);
                make.height.equalTo(contentView.mas_height);
            }];
            
            lastView = view;
            if ([[dic objectForKey:@"bgImage"] length]) {
                [view sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([dic objectForKey:@"bgImage"])]];
            }else
            {
                UIImage *image = [dic objectForKey:@"Image"];
                view.image = image;
            }

        }
        
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(lastView.mas_right);//****5
        }];
        
        //        lastView.backgroundColor = [UIColor redColor];
        self.scrollView.pagingEnabled = YES;
        //        NSLog(@"%@ %@",lastView,lastView.backgroundColor);
        
    }
    
    
//            [UIImage imageNamed:pos]
        [_BGview removeFromSuperview];
        self.titleLabel = nil;
        //广告栏标题背景
        _BGview = [UIImageView new];
        [self addSubview:_BGview];
        _BGview.backgroundColor = [UIColor clearColor];
        _BGview.image = [UIImage imageNamed:@"poster_script_bgIV.png"];
        [_BGview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.equalTo(@(30*kRateSize));
        }];
        //    view.backgroundColor = [UIColor grayColor];
    
        _BGview.alpha = 0.7 ;
        //广告栏标题
        self.titleLabel = [UILabel new];
        [_BGview addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_BGview);
        }];
        if (self.adArray.count) {

            //        self.titleLabel.text = [dic objectForKey:@"topicName"];;
            //        self.titleLabel.text = @"topicName";
        }
        
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
    
}

- (void)selectIndex:(NSInteger)index
{
    if (index < self.adArray.count) {
        
    }
}
- (void)updata
{
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@" 高度： %f %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    
    NSInteger count = (int)(scrollView.contentOffset.x / kDeviceWidth);
    NSLog(@"\\\\\%ld",count);
    if (count == 0) {
        
    }
    NSDictionary *dic = self.adArray[count];
    if ([dic objectForKey:@"topicName"]) {
        self.titleLabel.text = [dic objectForKey:@"topicName"];
        
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
//    NSLog(@" 高1度： %f %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
}







- (void)generateContent {
    UIView* contentView = UIView.new;
    [self.scrollView addSubview:contentView];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.height.equalTo(self.scrollView);
    }];
    
    UIView *lastView;
    CGFloat height = 25;
    
    for (int i = 0; i < 10; i++) {
        UIView *view = UIView.new;
        view.backgroundColor = [self randomColor];
        [contentView addSubview:view];
        
        
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastView ? lastView.mas_right : @0);
            make.top.equalTo(@0);
            make.width.equalTo(@100);
            make.height.equalTo(@(height));
        }];
        
        height += 25;
        lastView = view;
    }
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastView.mas_right);
    }];
}
- (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

@end
