//
//  AD_View.h
//  HIGHTONG
//
//  Created by testteam on 15/8/27.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
typedef void (^AD_View_block) (UIButton_Block*);
@interface AD_View : UIImageView

@property(nonatomic,strong)AD_View_block AD_BLOCK;

- (void)updateDIC:(NSDictionary*)dic;

@end
