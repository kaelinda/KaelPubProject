//
//  AD_View.m
//  HIGHTONG
//
//  Created by testteam on 15/8/27.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "AD_View.h"
#import "Masonry.h"
#import "UIButton_Block.h"
@interface AD_View ()
{
    UILabel *label;
    UIButton_Block *_click;
}
@end

@implementation AD_View
- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
        self.userInteractionEnabled = YES;
    }
    return self;
}
- (void)setup
{
    label = [UILabel new];

    label.alpha = 0.8;
    UIImageView *BG = [UIImageView new];
    BG.image = [UIImage imageNamed:@"bg_home_poster_content"];
    [self addSubview:BG];
    [BG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(25*kRateSize));
    }];
    label.textColor = [UIColor whiteColor];
    [BG addSubview:label];
    label.backgroundColor = [UIColor clearColor];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(BG);
    }];
    _click = [UIButton_Block new];
    [self addSubview:_click];
    [_click mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
}
- (void)updateDIC:(NSDictionary*)dic
{
    WS(wself);
    NSString *name = [dic objectForKey:@"programName"];
    NSString *image = [dic objectForKey:@"Image"];
    self.backgroundColor = [UIColor clearColor];
    label.text = name;
//    self.image = image;
    [self sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(image)] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
 
    if ([name isEqualToString:@"NULL"]&&[image isEqual:@"NULL"]) {
        [self setImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        label.text = @"暂无节目信息";
    }
    _click.serviceIDName = [dic objectForKey:@"contentID"];
    _click.Click = ^(UIButton_Block*btn,NSString*name)
    {
        if (wself.AD_BLOCK) {
            NSLog(@"ADVIEW 实现block");
            btn.serviceIDName = [dic objectForKey:@"contentID"];
            btn.eventName = [dic objectForKey:@"name"];
            wself.AD_BLOCK(btn);
        }else
        {
            NSLog(@"ADVIEW 没有实现block");
        }
        
    };
}
@end
