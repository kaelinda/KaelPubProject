//
//  AD_Scroll.h
//  HIGHTONG
//
//  Created by testteam on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AD_Scroll : UIView

@property (nonatomic,strong)NSMutableArray *adArray; //广告的图片url，title 等数据

@property (nonatomic,assign) NSInteger selectedIndex;//当前选的是第几个

@property (nonatomic,strong)UIScrollView *scrollView;

@property (nonatomic,strong)UILabel *titleLabel;//ad的标题

- (void)updata;


//- (void)selectIndex:(NSInteger)index;

@end
