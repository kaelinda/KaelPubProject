//
//  ChooseView.h
//  HIGHTONG
//
//  Created by testteam on 15/8/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ChooseView_TO_Controller) (NSDictionary*categoryDiction,NSString *indexOfRows);
@interface ChooseView : UIView

@property (nonatomic,strong)NSMutableArray *Allarray;///总的数据数组

@property (nonatomic,copy)ChooseView_TO_Controller Choose_Block;///block 传递到View上,view传递到controlller上

- (void)upDataViewWithArray:(NSDictionary*)diction;

@end
