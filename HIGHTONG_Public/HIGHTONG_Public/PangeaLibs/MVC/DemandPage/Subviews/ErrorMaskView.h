//
//  ErrorMaskView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/1/27.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButton_Block.h"

typedef void(^errorRetryAction)();

@interface ErrorMaskView : UIView

@property (nonatomic,strong) UIImageView *backImageView;//背景图

@property (nonatomic,strong) UILabel *pointLbael;//提示label

@property (nonatomic,strong) UIButton_Block *actionBtn;//行为按钮

@property (nonatomic,copy) errorRetryAction retryAction;//

@end
