//
//  ADLiveVodProgramView.h
//  HIGHTONG_Public
//
//  Created by testteam on 16/2/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
typedef NS_ENUM(NSInteger, ADPLAYER_MENUTYPE){
    ADPLAYER_MENULive = 0,
    ADPLAYER_MENUPause,
    ADPLAYER_MENUVOD,
};
typedef void (^ADLVPViewBlock) (NSDictionary *diction);

@interface ADLiveVodProgramView : UIImageView

@property(nonatomic,assign)NSInteger High;//中间广告图的高度  某人为100*100
@property(nonatomic,assign)NSInteger Width;//中间广告图的宽度

@property(nonatomic,assign)NSInteger HighClose;//中间广告图关闭按钮的高度  某人为10*10
@property(nonatomic,assign)NSInteger WidthClose;//中间广告图关闭按钮的宽度
@property (nonatomic,assign)ADPLAYER_MENUTYPE adplayerMenuType;

@property(nonatomic,copy)ADLVPViewBlock ADClickBlock;//广告图点击响应


//不需要外部初始化，但可以外部重新设置他们的自动布局
@property(nonatomic,strong)UIImageView *centerADViewImage;//中间广告图
@property(nonatomic,strong)UIButton_Block *centerADView;//中间广告图
@property(nonatomic,strong)UIButton_Block *centerADViewClose;//中间广告图关闭按钮

//设置广告的 图片和跳转信息
- (void)setUpWithDiction:(NSDictionary *)diction;
- (void)setUpWithDiction:(NSDictionary *)diction WithIsHidden:(BOOL)hidden;
- (void)UpDateViewHW:(ADPLAYER_MENUTYPE)MenueType;
@end
