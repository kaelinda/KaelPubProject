//
//  Choose_Scroll.h
//  HIGHTONG
//
//  Created by testteam on 15/8/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^Scroll_TO_View_With_Diction)(NSDictionary*categoryDiction,NSString*indexOfRow);
@interface Choose_Scroll : UIScrollView
@property (nonatomic,strong)NSMutableArray *dataArray;///总的数据数组

@property (nonatomic,copy)Scroll_TO_View_With_Diction Scroll_Block;///block 传递到View上

- (void)upDataViewWithArray:(NSDictionary*)diction;
@end
