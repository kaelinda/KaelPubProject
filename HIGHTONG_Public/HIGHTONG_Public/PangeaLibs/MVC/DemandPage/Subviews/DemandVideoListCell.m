//
//  DemandVideoListCell.m
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DemandVideoListCell.h"

@implementation DemandVideoListCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        WS(wsel);
        
        
        
        
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        _videoTitleLabel = [[UILabel alloc]init];
        _videoTitleLabel.text = @"15:51";
        _videoTitleLabel.textColor = [UIColor whiteColor];
        _videoTitleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
        [_videoTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:_videoTitleLabel];
        
        
        
        _seperateLine = [[UIImageView alloc]init];
        [_seperateLine setImage:[UIImage imageNamed:@"ic_player_full_menuitem_seperate_line.png"]];
        _seperateLine.hidden = YES;
        
        
        [self.contentView addSubview:_seperateLine];
        
        
        [_videoTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(22*kRateSize);
            make.top.mas_equalTo(wsel.contentView.mas_top).offset(8*kRateSize);
            make.width.mas_equalTo(125*kRateSize);
            make.height.mas_equalTo(20*kRateSize);
            
        }];
        
        
        [_seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(wsel.contentView.mas_bottom);
            make.left.mas_equalTo(wsel.contentView.mas_left).offset(0);
            make.width.mas_equalTo(139*kRateSize);
            make.height.mas_equalTo(1*kRateSize);
            
            
        }];

        
        
        
        
    
        
    }
    
    
    
    
    
    
    
    
    
    return self;
    
}
@end
