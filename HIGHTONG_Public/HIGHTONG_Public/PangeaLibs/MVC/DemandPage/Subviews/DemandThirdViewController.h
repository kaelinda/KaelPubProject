//
//  DemandThirdViewController.h
//  HIGHTONG_Public
//
//  Created by testteam on 16/3/11.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"

@interface DemandThirdViewController : HT_FatherViewController
@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,assign)BOOL hidenChoose;//隐藏筛选按钮

@property (nonatomic,assign)BOOL isTopic;//是主题节目列表
@property (nonatomic,copy)NSString *TopicCode;

@property (nonatomic,copy)NSString *operationCode;
@property (nonatomic,copy)NSString *categoryID;
@property (nonatomic,copy)NSString *name;
@end
