//
//  ADLiveVodProgramView.m
//  HIGHTONG_Public
//
//  Created by testteam on 16/2/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ADLiveVodProgramView.h"

@implementation ADLiveVodProgramView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
    if (self = [super init]) {
        [self setUpView];
    }
    return self;
}


- (void)UpDateViewHW:(ADPLAYER_MENUTYPE)MenueType
{
    switch (MenueType) {
        case ADPLAYER_MENULive:
        {
//            _adplayerMenuType = ADPLAYER_MENULive;
            [self.centerADViewImage mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.mas_equalTo(285*kRateSize);
                make.height.mas_equalTo(205*kRateSize);
                
            }];
        }
            break;
        case ADPLAYER_MENUPause:
        {
//            _adplayerMenuType = ADPLAYER_MENUPause;
            [self.centerADViewImage mas_updateConstraints:^(MASConstraintMaker *make) {
               
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.mas_equalTo(230*kRateSize);
                make.height.mas_equalTo(175*kRateSize);
                
            }];
        }
            break;
        case ADPLAYER_MENUVOD:
        {
//            _adplayerMenuType = ADPLAYER_MENUVOD;
            [self.centerADViewImage mas_updateConstraints:^(MASConstraintMaker *make) {
               
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.mas_equalTo(285*kRateSize);
                make.height.mas_equalTo(205*kRateSize);
                
            }];
        }
            break;
            
        default:
            break;
    }
}

- (void)setUpView{
    self.userInteractionEnabled = YES;
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.centerADViewImage = [[UIImageView alloc]init];
    self.centerADViewImage.backgroundColor = [UIColor whiteColor];
//        self.centerADViewImage.backgroundColor = [UIColor whiteColor];
    self.centerADViewImage.layer.cornerRadius = 8;
    self.centerADViewImage.layer.masksToBounds = YES;
    [self addSubview:self.centerADViewImage];
    [self.centerADViewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.top.mas_equalTo(_ADViewBase.mas_top).offset(60*kRateSize);
        //        make.left.mas_equalTo(_ADViewBase.mas_left).offset(60*kRateSize);
        //        make.right.mas_equalTo(_ADViewBase.mas_right).offset(-60*kRateSize);
        //        make.bottom.mas_equalTo(_ADViewBase.mas_bottom).offset(-60*kRateSize);
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(100*kRateSize);
        make.height.mas_equalTo(100*kRateSize);
        
    }];
    
    self.centerADView = [[UIButton_Block alloc]init];
    [self.centerADViewImage addSubview:self.centerADView];
    [self.centerADView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.centerADViewImage);
        
    }];
    self.centerADViewImage.userInteractionEnabled = YES;

    
    
     WS(wakeSelf);
    
    
    
    self.centerADViewClose = [[UIButton_Block alloc]init];
    [self.centerADViewImage addSubview:self.centerADViewClose];
    [self.centerADViewClose mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.centerADViewImage.mas_top).offset(0*kRateSize);
        make.right.mas_equalTo(self.centerADViewImage.mas_right).offset(-0*kRateSize);
        make.width.mas_equalTo(45*kRateSize);
        make.height.mas_equalTo(30*kRateSize);
        
    }];
    self.centerADView.userInteractionEnabled = YES;
    
    
    
    
    self.centerADViewClose.backgroundColor = [UIColor clearColor];
    [self.centerADViewClose setBackgroundImage:[UIImage imageNamed:@"btn_ad_player_close"] forState:UIControlStateNormal];
   
    
    self.centerADViewClose.Click = ^(UIButton_Block *btn,NSString *str){
        wakeSelf.hidden  = YES;
        _adplayerMenuType = -1;
    };
    
    
    
}
- (void)setUpWithDiction:(NSDictionary *)diction WithIsHidden:(BOOL)hidden
{
    NSLog(@"广告孤傲%@",diction);
    WS(wakeSelf);
    
    
    NSString *cout = [diction objectForKey:@"count"];
    NSArray *adList = [diction objectForKey:@"adList"];
    NSString *url;
    if ([cout integerValue] > 0) {
        NSDictionary *AD = [adList objectAtIndex:0];
        NSDictionary *pictureRes = [AD objectForKey:@"pictureRes"];
        NSString *picLink = [pictureRes objectForKey:@"picLink"];
        url = [pictureRes objectForKey:@"urlLink"];
        [self.centerADViewImage sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(picLink)] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (hidden) {
                if (error) {
                    self.hidden = YES;
                }else{
                    self.hidden = NO;
                    
                }
            }
           
        }];
        
        
        
        
        
    }else
    {
        self.hidden = YES;
    }
    
    //设置好了，展示字典后点击回调
    self.centerADView.Click = ^(UIButton_Block *btn,NSString *str){
        if (wakeSelf.ADClickBlock) {
            wakeSelf.ADClickBlock([NSDictionary dictionaryWithObjectsAndKeys:url,@"url", nil]);
        }else{
            NSLog(@"点击广告没响应");
        }
    };

}
- (void)setUpWithDiction:(NSDictionary *)diction
{
    [self setUpWithDiction:diction WithIsHidden:YES];
    
}
@end
