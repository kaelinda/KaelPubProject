//
//  headScroll.h
//  HIGHTONG
//
//  Created by testteam on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
@interface headScroll : UIScrollView



typedef void (^HEADBLOCK)(NSInteger index,UIButton_Block*);
@property (nonatomic,copy)NSString *KEY;
@property (nonatomic,strong)NSMutableArray *titledataArray;

@property (nonatomic,assign)NSInteger selectedNum;//当前选的是哪一个
@property (nonatomic,assign)BOOL isSelectedNum;//当前选的是哪一个
@property (nonatomic,assign)BOOL dianjizhuangtai;//当前选的是哪一个

@property (nonatomic,strong)NSMutableDictionary *SelectDiction;//删选的条件

@property (nonatomic,assign)NSInteger MubiaoNum;//当前选的是哪一个
- (void)setSelectedNum:(NSInteger)selectedNum withAnimate:(BOOL)bo;

/**
 *  要实现的block：void (^HEADBLOCK)(NSInteger index,UIButton_Block*);
 */
@property (nonatomic,copy)HEADBLOCK block;

- (void)updataTitle;

/**
 *  为筛选页面添加的接口
 *
 *  @param dic 筛选的  条件字典
 */
- (void)updateTitleWithDiction:(NSDictionary*)dic;

@end
