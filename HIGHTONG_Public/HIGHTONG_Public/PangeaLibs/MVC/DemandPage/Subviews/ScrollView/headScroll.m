

//
//  headScroll.m
//  HIGHTONG
//
//  Created by testteam on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "headScroll.h"
#import "UIButton_Block.h"
#import "Masonry.h"
#import <UIKit/UIKit.h>
#define TITLEKEY @"operationName"
#define  jianxi 4
@interface headScroll ()
{
    CGFloat X;
    NSInteger _index;//当前第几页
    UIView *   lineView;//下划线
    
    UIView *selected;
    UIButton_Block *selectedBTN;
}
@end

@implementation headScroll
- (instancetype)init
{
    if (self = [super init]) {
        self.titledataArray = [NSMutableArray array];
        _selectedNum = 0;
        self.backgroundColor = HT_COLOR_FONT_INVERSE;
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titledataArray = [NSMutableArray array];
        _selectedNum = 0;
        self.backgroundColor = HT_COLOR_FONT_INVERSE;
    }
    return self;
}
//- (void)
- (void)updataTitle1
{
    if (self.titledataArray.count) {
        NSInteger index = 0;
        NSInteger count = self.titledataArray.count;
        for (NSString *title in self.titledataArray) {
            if (title.length == 4) {
                index++;
                NSLog(@"+++++ %f",X);
                UIButton_Block *label = [[UIButton_Block alloc]initWithFrame:CGRectMake(X, 0, 76*kRateSize, self.size.height)];
                X = label.origin.x + label.size.width + jianxi;
                //                label.backgroundColor = [UIColor blackColor];
                if (index != count && count  >= 0 ) {
                    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(X-jianxi, 7*kRateSize, 2*kRateSize, label.size.height-14*kRateSize)];
                    view.backgroundColor = [UIColor whiteColor];
                    [self addSubview:view];
                }
                if (isAfterIOS7) {
                    label.titleLabel.textAlignment = NSTextAlignmentCenter;
                }else
                {
                    label.titleLabel.textAlignment = NSTextAlignmentCenter;
                }
                [label setTitle:title forState:UIControlStateNormal];
                label.eventName = [NSString stringWithFormat:@"%ld",(long)index];
                label.Click= ^(UIButton_Block*btn,NSString *name)
                {
                    _index = [btn.eventName integerValue];
                    if (_block) {
                        NSLog(@"scrollView实现了block");
                        _block(_index-1,btn);
                    }else
                    {
                        NSLog(@"scrollView没有实现block");
                    }
                };
                [self addSubview:label];
            }else
            {
                index++;
                NSLog(@"++++| %f",X);
                UIButton_Block *label = [[UIButton_Block alloc]initWithFrame:CGRectMake(X, 0, 63*kRateSize, self.size.height)];
                X = label.origin.x + label.size.width + jianxi;
                //                label.backgroundColor = [UIColor blackColor];
                if (index != count && count  >= 0 ) {
                    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(X-jianxi, 7*kRateSize, 2*kRateSize, label.size.height-14*kRateSize)];
                    view.backgroundColor = [UIColor whiteColor];
                    [self addSubview:view];
                }
                if (isAfterIOS7) {
                    label.titleLabel.textAlignment = NSTextAlignmentCenter;
                }else
                {
                    label.titleLabel.textAlignment = NSTextAlignmentCenter;
                }
                [label setTitle:title forState:UIControlStateNormal];
                label.eventName = [NSString stringWithFormat:@"%ld",(long)index];
                label.Click= ^(UIButton_Block*btn,NSString *name)
                {
                    _index = [btn.eventName integerValue];
                    if (_block) {
                        NSLog(@"scrollView实现了block");
                        _block(_index-1,btn);
                    }else
                    {
                        NSLog(@"scrollView没有实现block");
                    }
                };
                [self addSubview:label];
            }
        }
        self.contentSize = CGSizeMake(X, self.size.height);
    }
}
- (void)setSelectedNum:(NSInteger)selectedNum withAnimate:(BOOL)bo
{
    if(selectedNum <= self.titledataArray.count)
    {
        UIView *viewbtn = [self viewWithTag:selectedNum +8080];
        UIView *view = [viewbtn viewWithTag:989898];
         [self scrollRectToVisible:viewbtn.frame animated:NO];
        selected.hidden = YES;
        view.hidden = NO;
        selected = view;
        
        
        UIButton_Block *btn = (UIButton_Block *)viewbtn;
        if (btn != selectedBTN) {
            btn.selected = YES;
            selectedBTN.selected = NO;
            selectedBTN = btn;
        }

    }
}
- (void)setSelectedNum:(NSInteger)selectedNum
{
    if(selectedNum <= self.titledataArray.count)
    {
        UIView *viewbtn = [self viewWithTag:selectedNum +8080];
        [self scrollRectToVisible:viewbtn.frame animated:NO];
        UIView *view = [viewbtn viewWithTag:989898];
        
        if (self.isSelectedNum) {
            if (selectedNum == self.MubiaoNum) {
                self.isSelectedNum = NO;
                return;
            }
        }else
        {
            if (selectedNum != self.MubiaoNum && self.dianjizhuangtai) {
                return;
            }else
            {
                self.dianjizhuangtai = NO;
            }
        }
//        if (!self.isSelectedNum) {
            selected.hidden = YES;
            view.hidden = NO;
            selected = view;
        
        
        UIButton_Block *btn = (UIButton_Block *)viewbtn;
        if (btn != selectedBTN) {
            btn.selected = YES;
            selectedBTN.selected = NO;
            selectedBTN = btn;
        }
        
//        }
        
        _selectedNum = selectedNum;
        
    }
}

- (void)updataTitle
{
    self.showsVerticalScrollIndicator = NO;
    if (self.titledataArray.count) {
        
        [self removeAllSubviews];
        UIView *contentview = [UIView new];
        contentview.backgroundColor = HT_COLOR_FONT_INVERSE;
        [self addSubview:contentview];
        [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
            make.height.equalTo(self);
        }];
        
        NSInteger index = 0;
        NSInteger count = self.titledataArray.count;
        UIButton_Block *last;
        for (NSDictionary *dic in self.titledataArray) {
            NSString *title ;
            if (self.KEY.length) {
                title = [dic objectForKey:self.KEY];
            }else
            {
                title = [dic objectForKey:TITLEKEY];
            }
           
            index++;
            NSLog(@"%@",title);
            
            UIButton_Block *btn = [UIButton_Block new];
            btn.tag = index + 8080;
            [contentview addSubview:btn];
            btn.titleLabel.font = [UIFont systemFontOfSize:15*kRateSize];
            [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            CGSize size = [self MYString:title StringSizeOfFont:[UIFont systemFontOfSize:13*kRateSize]];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                if (last) {
                    make.left.equalTo(last.mas_right).offset(jianxi*kRateSize);
                }else
                {
                    make.left.equalTo(self).offset(jianxi*kRateSize);
                }
                make.top.equalTo(self);
                make.width.equalTo(@(size.width));
                make.bottom.equalTo(self);
            }];
            
            UIImageView *lineH = [UIImageView new];
            lineH.tag = 989898;
            lineH.image = [UIImage imageNamed:@"ic_search_hon_line"];
            [btn addSubview:lineH];
            [lineH mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(btn.mas_left).offset(4*kRateSize);;
                make.right.equalTo(btn.mas_right).offset(-4*kRateSize);;
                make.height.equalTo(@(3/2+0.5));
                make.bottom.equalTo(btn.mas_bottom).offset(-2);
            }];
            
            lineH.hidden = YES;
            if (_MubiaoNum < count && index == _MubiaoNum+1) {
                lineH.hidden = NO;
                selected = lineH;
                if (btn != selectedBTN) {
                    btn.selected = YES;
                    selectedBTN = btn;
                }
                
            }
     
            [btn setTitle:title forState:UIControlStateNormal];
            [btn setTitle:title forState:UIControlStateSelected];
            btn.serviceIDName = title;
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
            //            btn.backgroundColor = [UIColor redColor];
            last = btn;
            
            //***分割线
            if (index != self.titledataArray.count) {
                UIView * lineview1 = [UIView new];
                [contentview addSubview:lineview1];
                lineview1.layer.cornerRadius = 2;
                [lineview1 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(btn.mas_right).offset(0.5*kRateSize);
                    //                make.right.equalTo(btn.mas_right).offset(4*kRateSize);
                    make.width.equalTo(@((3/2+0.5)*kRateSize));
                    make.top.equalTo(contentview.mas_top).offset(7*kRateSize);
                    make.bottom.equalTo(contentview.mas_bottom).offset(-6.5*kRateSize);
                }];
//                lineview1.backgroundColor =[UIColor lightGrayColor];
            }
            //****
            
            if (isAfterIOS7) {
                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            }else
            {
//                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            }
            [btn setTitle:title forState:UIControlStateNormal];
            [btn setTitle:title forState:UIControlStateSelected];
            btn.eventName = [NSString stringWithFormat:@"%ld",(long)index];
            btn.Click= ^(UIButton_Block*btn,NSString *name)
            {

                _index = [btn.eventName integerValue];
                NSLog(@"%ld",(long)index);
                
                lineView.backgroundColor = App_selected_color;
                
                if (_block) {
                    NSLog(@"scrollView实现了block");
                    btn.Diction = self.titledataArray[_index-1];
                    _block(_index-1,btn);
                }else
                {
                    NSLog(@"scrollView没有实现block");
                   
                }
                
                
                UIView *view = [btn viewWithTag:989898];
                if (view.hidden) {
                    view.hidden = NO;
                    selected.hidden = YES;
                    selected = view;
                    
                }
                
                if (btn != selectedBTN) {
                    btn.selected =  YES;
                    selectedBTN.selected = NO;
                    selectedBTN = btn;
                }
               
                
                
                
            };
            
            
            
        }
        
        NSLog(@"2反会了这么么多数据%ld",(long)index);
        [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(last.mas_right);
        }];
        
        
        //        self.contentSize = CGSizeMake(X, self.size.height);
        
    }else
    {
        [self removeAllSubviews];
        UILabel *text = [UILabel new];
        [self addSubview:text];
        text.text = @"暂无数据";
        [text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.width.equalTo(self.mas_width);
            make.top.equalTo(self.mas_top);
            make.height.equalTo(self.mas_height);
        }];
        if (!isAfterIOS6) {
            text.textAlignment = NSTextAlignmentCenter;
        }else
        {
            text.textAlignment = NSTextAlignmentCenter;
        }

    }
}

- (void)updateTitleWithDiction:(NSDictionary*)dic
{
    NSMutableArray *SelectArray = [NSMutableArray array];
    NSString *Key1 = [dic objectForKey:@"0"];
    NSString *Key2 = [dic objectForKey:@"1"];
    NSString *Key3 = [dic objectForKey:@"2"];
    NSString *Key4 = [dic objectForKey:@"3"];
    if (Key1.length) {
        [SelectArray addObject:Key1];
    }if (Key2.length) {
        [SelectArray addObject:Key2];
    }
    if (Key3.length) {
        [SelectArray addObject:Key3];
    }
    if (Key4.length) {
        [SelectArray addObject:Key4];
    }
    
    self.titledataArray.array = SelectArray;
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = NO;
    if (self.titledataArray.count) {
        
        [self removeAllSubviews];
        UIView *contentview = [UIView new];
        contentview.backgroundColor = UIColorFromRGB(0xffffff);
        [self addSubview:contentview];
        [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
            make.height.equalTo(self);
        }];
        
        NSInteger index = 0;
        NSInteger count = self.titledataArray.count;
        UIButton_Block *last;
        for (NSString *title in self.titledataArray) {
    
            index++;
            NSLog(@"%@",title);
            
            UIButton_Block *btn = [UIButton_Block new];
            btn.tag = index + 8080;
            [contentview addSubview:btn];
            btn.titleLabel.font = [UIFont systemFontOfSize:15*kRateSize];
            [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
            CGSize size = [self MYString:title StringSizeOfFont:[UIFont systemFontOfSize:13*kRateSize]];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                if (last) {
                    make.left.equalTo(last.mas_right).offset(jianxi*kRateSize);
                }else
                {
                    make.left.equalTo(self).offset(jianxi*kRateSize);
                }
                make.top.equalTo(self);
                make.width.equalTo(@(size.width));
                make.bottom.equalTo(self);
            }];
            
//            UIImageView *lineH = [UIImageView new];
//            lineH.tag = 989898;
//            lineH.image = [UIImage imageNamed:@"ic_search_hon_line"];
//            [btn addSubview:lineH];
//            [lineH mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(btn.mas_left).offset(4*kRateSize);;
//                make.right.equalTo(btn.mas_right).offset(-4*kRateSize);;
//                make.height.equalTo(@(3/2+0.5));
//                make.bottom.equalTo(btn.mas_bottom).offset(-2);
//            }];
            
//            lineH.hidden = YES;
            if (_MubiaoNum < count && index == _MubiaoNum+1) {
//                lineH.hidden = NO;
//                selected = lineH;
                if (btn != selectedBTN) {
                    btn.selected = YES;
                    selectedBTN = btn;
                }
                
            }
            
            [btn setTitle:title forState:UIControlStateNormal];
            [btn setTitle:title forState:UIControlStateSelected];
            btn.serviceIDName = title;
            [btn setTitleColor:App_selected_color  forState:UIControlStateNormal];
            [btn setTitleColor:App_selected_color forState:UIControlStateSelected];
            //            btn.backgroundColor = [UIColor redColor];
            last = btn;
            
            //***分割线
            if (index != self.titledataArray.count) {
                UIView * lineview1 = [UIView new];
                [contentview addSubview:lineview1];
                lineview1.layer.cornerRadius = 2;
                [lineview1 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(btn.mas_right).offset(0.5*kRateSize);
                    //                make.right.equalTo(btn.mas_right).offset(4*kRateSize);
                    make.width.equalTo(@((3+0.5+0.5)*kRateSize));
                    make.height.equalTo(@((3+0.5+0.5)*kRateSize));
                    make.centerY.equalTo(btn.mas_centerY);
//                    make.top.equalTo(contentview.mas_top).offset(7*kRateSize);
//                    make.bottom.equalTo(contentview.mas_bottom).offset(-6.5*kRateSize);
                }];
                lineview1.backgroundColor =App_selected_color;
            }
            //****
            
            if (isAfterIOS7) {
                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            }else
            {
                //                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            }
            [btn setTitle:title forState:UIControlStateNormal];
            [btn setTitle:title forState:UIControlStateSelected];
            btn.eventName = [NSString stringWithFormat:@"%ld",(long)index];
            btn.Click= ^(UIButton_Block*btn,NSString *name)
            {
                
                _index = [btn.eventName integerValue];
                NSLog(@"%ld",(long)index);
                
                lineView.backgroundColor = App_selected_color;
                
                if (_block) {
                    NSLog(@"scrollView实现了block");
                    btn.Diction = self.titledataArray[_index-1];
                    _block(_index-1,btn);
                }else
                {
                    NSLog(@"scrollView没有实现block");
                    
                }
                
                
//                UIView *view = [btn viewWithTag:989898];
//                if (view.hidden) {
//                    view.hidden = NO;
//                    selected.hidden = YES;
//                    selected = view;
//                    
//                }
//                
//                if (btn != selectedBTN) {
//                    btn.selected =  YES;
//                    selectedBTN.selected = NO;
//                    selectedBTN = btn;
//                }
                
              
                
            };
            
            
            
        }
        
        NSLog(@"2反会了这么么多数据%ld",(long)index);
        [contentview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(last.mas_right);
        }];
        
        
        //        self.contentSize = CGSizeMake(X, self.size.height);
        
    }else
    {
        [self removeAllSubviews];
        UILabel *text = [UILabel new];
        [self addSubview:text];
        text.text = @"暂无数据";
        [text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.width.equalTo(self.mas_width);
            make.top.equalTo(self.mas_top);
            make.height.equalTo(self.mas_height);
        }];
        if (!isAfterIOS6) {
            text.textAlignment = NSTextAlignmentCenter;
        }else
        {
            text.textAlignment = NSTextAlignmentCenter;
        }
        
    }

}

- (CGSize)MYString:(NSString*) string StringSizeOfFont:(UIFont*)font
{
    CGSize size;
    if (isAfterIOS6) {
        size =  [string sizeWithFont:font forWidth:99999 lineBreakMode:NSLineBreakByCharWrapping];
        size.width += 30*kRateSize;
    }else
    {
        NSDictionary *attrs = @{NSFontAttributeName : font};
        size = [string boundingRectWithSize:CGSizeMake(30, 9999999) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    }
    
    return size;
}

@end
