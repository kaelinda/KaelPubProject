//
//  DetailIntrolView.m
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DetailIntrolView.h"

@implementation DetailIntrolView


-(instancetype)init{

    self = [super init];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setupSubViews];
    }

    return self;
    
}


-(void)setupSubViews{

    _tranLineImage = [[UIImageView alloc] init];
    [_tranLineImage setBackgroundColor:UIColorFromRGB(0xaaaaaa)];
    _tranLineImage.alpha = 0.5;
    [self addSubview:_tranLineImage];
    
    WS(wself);
    [_tranLineImage mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(7*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-7*kRateSize);
        make.height.mas_equalTo(1);
        
    }];
    
    
    //-----------------
    
    _mainActorLabel = [[UILabel alloc] init];
    [_mainActorLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:15.0f]];
    [_mainActorLabel setTextAlignment:NSTextAlignmentLeft];
    [_mainActorLabel setBackgroundColor:[UIColor clearColor]];

    [_mainActorLabel setText:@"主演： "];
    [self addSubview:_mainActorLabel];
    
    [_mainActorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(5*kRateSize);
        make.left.mas_equalTo(6*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-6*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
        
    }];
    
    //--------------------
    
    _directorLabel = [[UILabel alloc] init];
    [_directorLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:15.0f]];
    [_directorLabel setTextAlignment:NSTextAlignmentLeft];
    [_directorLabel setText:@"导演： "];
    [_directorLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_directorLabel];
    
    [_directorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself).offset(30*kRateSize);
        make.left.mas_equalTo(wself).offset(6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
    _yearLabel = [[UILabel alloc] init];
    [_yearLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:15.0f]];
    [_yearLabel setTextAlignment:NSTextAlignmentLeft];
    [_yearLabel setText:@"年份：2015"];
    [_yearLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_yearLabel];
    
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(30*kRateSize);
        make.left.mas_equalTo(6*kRateSize+kDeviceWidth/2);
        make.width.mas_equalTo(kDeviceWidth/2-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
    _typeLabel = [[UILabel alloc] init];
    [_typeLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:15.0f]];
    [_typeLabel setTextAlignment:NSTextAlignmentLeft];
    [_typeLabel setText:@"类型：  "];
    [_typeLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_typeLabel];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(55*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-6*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
    _areaLabel = [[UILabel alloc] init];
    [_areaLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:15.0f]];
    [_areaLabel setTextAlignment:NSTextAlignmentLeft];
    [_areaLabel setText:@"地区："];
    [_areaLabel setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_areaLabel];
    [_areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(55*kRateSize);
        make.left.mas_equalTo(6*kRateSize+kDeviceWidth/2);
        make.width.mas_equalTo(kDeviceWidth/2-6*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
//-----------------------
    _introLabel = [[UILabel alloc] init];
    [_introLabel setNumberOfLines:30];
    _introLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_introLabel setBackgroundColor:[UIColor clearColor]];
    [_introLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [_introLabel setText:@"简介：***********[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];[_introLabel setTextColor:UIColorFromRGB(0x999999)];***********"];
    [self addSubview:_introLabel];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];
//    _introLabel.preferredMaxLayoutWidth = kDeviceWidth-12*kRateSize;
    
    [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.equalTo(wself).offset(109*kRateSize);
        make.left.equalTo(wself).offset(6*kRateSize);
        
    }];
    
//----------
    
    _praiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_praiseBtn addTarget:self action:@selector(praiseBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_normal"] forState:UIControlStateNormal];
    [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_pressed"] forState:UIControlStateHighlighted];
    [self addSubview:_praiseBtn];
//    [_praiseBtn setBackgroundColor:App_selected_color];
    
    [_praiseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_typeLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(wself.mas_left).offset(87*kRateSize);
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        
    }];
    
    _praiseNumLabel = [[UILabel alloc] init];
    [_praiseNumLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_praiseNumLabel setText:@"(0)"];
    [_praiseNumLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_praiseNumLabel];
    
    [_praiseNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(_praiseBtn.mas_centerY);
        make.left.mas_equalTo(_praiseBtn.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24*kRateSize));
        
    }];
    
    
//-------
    _stampBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_stampBtn addTarget:self action:@selector(stampBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_normal"] forState:UIControlStateNormal];
    [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_pressed"] forState:UIControlStateHighlighted];
//    [_stampBtn setBackgroundColor:App_selected_color];
    [self addSubview:_stampBtn];
    
    [_stampBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_typeLabel.mas_bottom).offset(0);
        make.right.mas_equalTo(wself.mas_right).offset(-95*kRateSize);
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        
    }];
    
    _stampNumLabel = [[UILabel alloc] init];
    [_stampNumLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_stampNumLabel setText:@"(0)"];
    [_stampNumLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_stampNumLabel];
    
    [_stampNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(_stampBtn.mas_centerY);
        make.left.mas_equalTo(_stampBtn.mas_right).offset(3*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24));
        
    }];
    
    _pushAndPopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_pushAndPopBtn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
    [_pushAndPopBtn addTarget:self action:@selector(pushAndPopBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [_pushAndPopBtn setBackgroundColor:App_selected_color];
    [self addSubview:_pushAndPopBtn];
    
    [_pushAndPopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_introLabel.mas_bottom).offset(3*kRateSize);
        make.centerX.mas_equalTo(wself);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 27*kRateSize));
        
    }];

//-------------------
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(_pushAndPopBtn.mas_bottom).offset(15);
        
    }];
    

    //-------------初始化introlview 默认收起
    
    [self pushAndPopBtnAction:_pushAndPopBtn];
 
}





-(void)loadIntolWith:(NSString *)introl andMainActors:(NSString *)actors andDirector:(NSString *)director andYears:(NSString *)years andType:(NSString *)type andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum{

    if (introl.length>0) {
        
        CGFloat rowSpace = 6;
        if (kDeviceWidth == 320) {
            rowSpace = 3;
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:introl];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:rowSpace];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [introl length])];
        _introLabel.attributedText = attributedString;
        [_introLabel sizeToFit];
        
    }
    
    if (actors.length>0) {
        [_mainActorLabel setText:[NSString stringWithFormat:@"主演：%@",actors]];
    }
    if (director.length>0) {
        [_directorLabel setText:[NSString stringWithFormat:@"导演：%@",director]];

    }
    if (years.length>0) {
        [_yearLabel setText:[NSString stringWithFormat:@"年份：%@",years]];

    }
    if (type.length>0) {
        [_typeLabel setText:[NSString stringWithFormat:@"类型：%@",type]];

    }
    if (area.length>0) {
        [_areaLabel setText:[NSString stringWithFormat:@"地区：%@",area]];
    }
    if (praiseNum.length>0) {
        [_praiseNumLabel setText:[NSString stringWithFormat:@"(%@)",praiseNum]];
    }
    if (stampNum.length>0) {
        [_stampNumLabel setText:[NSString stringWithFormat:@"(%@)",stampNum]];

    }

}


-(void)praiseBtnAction:(UIButton *)btn{
    [KaelTool scaleBigAnimation:btn];

    NSLog(@"赞一个");
    if ([self.delegate respondsToSelector:@selector(praiseBtnSelected:)]) {
        [self.delegate praiseBtnSelected:btn];
    }
}
-(void)stampBtnAction:(UIButton *)btn{
    [KaelTool scaleBigAnimation:btn];

    NSLog(@"踩一个");
    if ([self.delegate respondsToSelector:@selector(stampBtnSelected:)]) {
        [self.delegate stampBtnSelected:btn];
    }
}

-(void)praiseSuccecc:(BOOL)isSuccecc{
    if (isSuccecc) {
        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_pressed"] forState:UIControlStateNormal];

    }else
    {
        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_normal"] forState:UIControlStateNormal];
    }

}

-(void)stampSuccecc:(BOOL)isSuccecc{
    if (isSuccecc) {
        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_pressed"] forState:UIControlStateNormal];
    }else
    {
         [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_normal"] forState:UIControlStateNormal];
    }

}

-(void)pushAndPopBtnAction:(UIButton *)btn{

    NSLog(@"收起还是展开啊？？");
    WS(wself);
    
    [_introLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(wself).offset(119*kRateSize);
        make.left.equalTo(wself).offset(6*kRateSize);

        if (_introLabel.hidden) {

//            if (isAfterIOS7) {
            make.width.mas_equalTo(kDeviceWidth-12*kRateSize);
//            }else{
//                
//            }
            //            make.right.mas_equalTo(wself.mas_right).offset(-6*kRateSize);

        }else{

            make.height.mas_equalTo(@0);


        }
        
    }];
    
    if (_introLabel.hidden) {
    
        [_pushAndPopBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_introLabel.mas_bottom).offset(1*kRateSize);
            make.centerX.mas_equalTo(wself);
            make.size.mas_equalTo(CGSizeMake(40*kRateSize, 27*kRateSize));
            
        }];
    }else{
        [_pushAndPopBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_introLabel.mas_bottom).offset(-16*kRateSize);
            make.centerX.mas_equalTo(wself);
            make.size.mas_equalTo(CGSizeMake(40*kRateSize, 27*kRateSize));
            
        }];
    
    }
    
 //--------------
    
    if (_introLabel.hidden) {
        [btn setImage:[UIImage imageNamed:@"ic_vod_filter_fold"] forState:UIControlStateNormal];
        
    }else{
        [btn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
        
    }
    
//-----------
    _introLabel.hidden = !_introLabel.hidden;
  
    if ([self.delegate respondsToSelector:@selector(pushAndPopBtnSelected:)]) {
        [self.delegate pushAndPopBtnSelected:btn];
    }
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
