//
//  EpisodeButton.m
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EpisodeButton.h"
#import "ZZSBadgeBtn.h"

#define ios7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)

@interface EpisodeButton(){
    
    NSString *_EBadgeValue;


}

// 数字提醒
@property (nonatomic, weak) ZZSBadgeBtn *badgeButton;
@property (nonatomic,strong) UILabel *badgeLabel;
@property (nonatomic,strong) UIImageView *badgeImage;//徽章视图
@property (nonatomic,strong) UIImageView *backImageView;

@end


@implementation EpisodeButton

-(instancetype)init{
    self = [super init];
    if (self) {
        //这里可以自定义一些试图的
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11*kRateSize];
        UIColor *titleColor = (ios7 ? [UIColor blackColor]:[UIColor whiteColor]);
        [self setTitleColor:titleColor forState:UIControlStateNormal];
        [self setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        
        // 提醒数字按钮
        
        [self addBadgeView];
        
    }
    return self;
}


-(void)addBadgeView{
    WS(wself);

//    _backImageView = [[UIImageView alloc] init];
//    [_backImageView setBackgroundColor:CLEARCOLOR];
//    [self addSubview:_backImageView];
//    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(wself);
//    }];
    
    _badgeImage = [[UIImageView alloc] init];
    _badgeImage.image = [UIImage imageNamed:@"icon_fufei"];

    [self addSubview:_badgeImage];
    
    CGSize badgeSize = CGSizeMake(14, 14);
    
    [_badgeImage mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(1);
        make.right.mas_equalTo(wself.mas_right).offset(-1);
        make.size.mas_equalTo(badgeSize);
        
    }];
    
    
    

}
-(void)setEBadgeValue:(NSString *)EBadgeValue{
    if (EBadgeValue.length>0) {
        _badgeLabel.hidden = NO;
        [_badgeLabel setText:EBadgeValue];
        
    }else{
    
        _badgeLabel.hidden = YES;
    }
    

}
-(void)hiddenBadgeView:(BOOL)isHidden{

    _badgeImage.hidden = isHidden;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
