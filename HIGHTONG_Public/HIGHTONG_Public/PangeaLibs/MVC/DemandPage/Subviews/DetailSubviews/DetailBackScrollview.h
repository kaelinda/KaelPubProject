//
//  DetailBackScrollview.h
//  HIGHTONG
//
//  Created by Kael on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DetailHeaderView.h"
#import "DetailIntrolView.h"
#import "VarietyDetailIntrolView.h"
#import "EpisodeSelectedView.h"
#import "DetailRecommendView.h"

#import "VarietySelecetedView.h"
#import "NewIntrolView.h"


@class DetailBackScrollview;
@protocol DetailScrollviewDelegate <NSObject>

-(void)headerViewFavorateBtnSeleced:(UIButton *)btn;

-(void)introlViewPraiseBtnSelected:(UIButton *)btn;

-(void)introlViewStampBtnSelected:(UIButton *)btn;

-(void)varityViewPraiseBtnSelected:(UIButton *)btn;

-(void)varityViewStampBtnSelected:(UIButton *)btn;

-(void)episodeViewEBtnSelectedAtIndex:(NSInteger)index;
-(void)varityViewSetselectedAtIndex:(NSInteger)index;

-(void)VTabSelectedIndex:(NSInteger)index;

-(void)recommandViewItemsSelectedAtIndex:(NSInteger)index;

@end


@interface DetailBackScrollview : UIScrollView<DetailHeaderViewDelegate,DetailIntrolViewDelegate,DetailScrollviewDelegate,VarietyDetailIntrolViewDelegate,EpisodeViewDelegate,VSelectedViewDelegate,DetailRecommandViewDelegate>


{

}

#pragma mark - property
@property (nonatomic,strong) UILabel *BufferLabel;
@property (nonatomic,strong) UIImageView *bufferBackImageView;
@property (nonatomic,strong) UIView *bufferView;

//  下面是各种代理

@property(nonatomic,assign)id<DetailScrollviewDelegate> delegate;


@property (nonatomic,strong)NewIntrolView *KNewIntrolView;

/**
 *  是否是综艺节目
 */
@property (nonatomic,assign) BOOL isVarity;
/**
 *  类型   0：电影  1：电视剧  2：综艺
 */
@property (nonatomic,assign) NSInteger programType;


/**
 *  scrollview的视图容器
 */
@property (nonatomic,strong) UIView *scrollContentView;

/**
 *  这是相对固定的 详情的头
 */
@property (nonatomic,strong) DetailHeaderView *headerView;

/**
 *  这个是 电影和电视剧的详情介绍
 */
@property (nonatomic,strong) DetailIntrolView *introlView;

/**
 *  这是综艺节目的详情视图
 */
@property (nonatomic,strong) VarietyDetailIntrolView *varityIntrolView;

/**
 *  这是电视剧的剧集选择界面
 */
@property (nonatomic,strong) EpisodeSelectedView *episodeView;

/**
 *  综艺剧集
 */
@property (nonatomic,strong) VarietySelecetedView *variteySelecView;

/**
 *  这是相关推荐视图
 */
@property (nonatomic,strong) DetailRecommendView *recommandView;



#pragma mark ------->> function
/**
 *  设置当前节目的类型 1、电影 2、电视剧 3、综艺
 *
 *  @param programType 节目类型 1、电影 2、电视剧 3、综艺
 */
-(void)setProgramTypeWith:(NSInteger)programType;


/**
 *  一次性加载完毕
 *
 *  @param imageUrl 图片链接
 *  @param name     节目名称
 *  @param playNum  播放量
 */
-(void)headerViewLoadeEventImage:(NSString *)imageUrl andName:(NSString *)name andPlayNum:(NSString *)playNum;

/**
 *  加载节目图片
 *
 *  @param imageUrl 图片地址
 */
-(void)headerViewLoadEventImageWith:(NSString *)imageUrl;

/**
 *  加载节目名称
 *
 *  @param name 节目名
 */
-(void)headerViewLoadEventNameWith:(NSString *)name;

/**
 *  加载播放量
 *
 *  @param playNum 播放量
 */
-(void)headerViewLoadePlayNumLabelTextWith:(NSString *)playNum;


/**
 *  收藏成功
 */
-(void)headerViewFavorSucceed;

/**
 *  取消收藏成功
 */
-(void)headerViewCancellledFavor;




-(void)KNewIntrolViewLoadingIntrolInfoWithName:(NSString *)name andActor:(NSString *)actor andDirector:(NSString *)director andType:(NSString *)type andYears:(NSString *)years andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum andIntrol:(NSString *)introl;


/**
 *  介绍模块界面
 *
 *  @param introl    简介
 *  @param actors    主演
 *  @param director  导演
 *  @param years     年份
 *  @param type      类型
 *  @param praiseNum 点赞量
 *  @param stampNum  点踩量
 */
-(void)introlViewLoadeIntrolWith:(NSString *)introl andMainActors:(NSString *)actors andDirector:(NSString *)director andYears:(NSString *)years andArea:(NSString *)area andType:(NSString *)type andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum;

/**
 *  综艺剧集介绍模块
 *
 *  @param varityArr 综艺子集
 *  @param allNum    总集数
 *  @param updateNum 已更新剧集数
 */
-(void)reloadVarityDateWith:(NSArray *)varityArr andAllEpisodNum:(NSInteger)allNum andUpdateNum:(NSString *)updateNum;



/**
 *  点赞吐槽
 *
 *  @return 返回值
 */
//点赞或者吐槽 是否成功
-(void)praiseSuccecc:(BOOL)isSuccecc;

-(void)stampSuccecc:(BOOL)isSuccecc;


/**
 *  综艺介绍模块
 *
 *  @param introl    简介
 *  @param year      年份
 *  @param type      类型
 *  @param area      区域
 *  @param mostNew   最新
 *  @param praiseNum 点赞量
 *  @param stampNum   点踩量
 */
-(void)varityIntrolViewLoadeIntrolWith:(NSString *)introl andYear:(NSString *)year andType:(NSString *)type andArea:(NSString *)area andMostNew:(NSString *)mostNew andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum;


/**
 *  设置需要隐藏的徽章的位置 index
 *
 *  @param index 位置
 */
-(void)episodeViewHiddenBadgeAtIndex:(NSInteger)index;

/**
 *  设置选中按钮的位置
 *
 *  @param index 选中按钮的位置
 */
-(void)episodeViewSetSelectedBtnAtIndex:(NSInteger)index;
/**
 *  剧集选择模块
 *
 *  @param allNum    总共剧集数
 *  @param updateNum 已更新剧集数
 */
-(void)episodeViewLoadeAllEpisodNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum;
-(void)episodeViewLoadeAllEpisodeNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum andEpisodeArr:(NSArray *)episodeArr;

/**
 *  加载剧集数
 *
 *  @param episodNum 剧集数
 */
-(void)episodeViewLoadeBtnWithEpisodeNum:(NSInteger)episodNum;

/**
 *  设置综艺视图 的选中剧集数 (代码强制视图聚焦在某一位置)
 *
 *  @param index 剧集位置
 */
-(void)varityViewSetselectedAtIndex:(NSInteger)index;

/**
 *  推荐模块加载
 *
 *  @param recommandItems 推荐元素（数组）
 */
-(void)recommandViewLoadeRecommandItems:(NSArray *)recommandItems;



@end
