//
//  DetailIntrolView.h
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//这是 电视剧电影相同 只有综艺不同的 那一块

#import <UIKit/UIKit.h>


@class DetailIntrolView;

@protocol DetailIntrolViewDelegate <NSObject>

-(void)praiseBtnSelected:(UIButton *)btn;//赞一个
-(void)stampBtnSelected:(UIButton *)btn;//踩一个
-(void)pushAndPopBtnSelected:(UIButton *)btn;//

@end


@interface DetailIntrolView : UIView


@property (nonatomic,assign) id<DetailIntrolViewDelegate>delegate;


@property (nonatomic,assign) NSInteger eventType;//节目类型 1、电影 2、电视剧 3、综艺   这个用来判断视图类型

@property (nonatomic,strong) UIImageView *tranLineImage;//顶部横线
@property (nonatomic,strong) UILabel *introLabel;//剧情介绍

@property (nonatomic,strong) UILabel *mainActorLabel;//主演
@property (nonatomic,strong) UILabel *directorLabel;//导演
@property (nonatomic,strong) UILabel *yearLabel;//年份
@property (nonatomic,strong) UILabel *typeLabel;//类型
@property (nonatomic,strong) UILabel *areaLabel;//地区

@property (nonatomic,strong) UIButton *praiseBtn;//赞
@property (nonatomic,strong) UIButton *stampBtn;//踩

@property (nonatomic,strong) UILabel *praiseNumLabel;//点赞量
@property (nonatomic,strong) UILabel *stampNumLabel;//踩量


@property (nonatomic,strong) UIButton *pushAndPopBtn;//拉伸 回收 按钮






-(void)loadIntolWith:(NSString *)introl andMainActors:(NSString *)actors andDirector:(NSString *)director andYears:(NSString *)years andType:(NSString *)type andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum;


-(void)loadAreaWith:(NSString *)area;
-(void)loadMostNewWith:(NSString *)mostNew;



-(void)praiseSuccecc:(BOOL)isSuccecc;

-(void)stampSuccecc:(BOOL)isSuccecc;





@end
