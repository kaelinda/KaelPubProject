//
//  VarietyDetailIntrolView.h
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//这是那个 特殊的综艺分类 的部分

#import <UIKit/UIKit.h>

@class VarietyDetailIntrolView;

@protocol VarietyDetailIntrolViewDelegate <NSObject>

-(void)varityPraiseBtnSelected:(UIButton *)btn;//综艺部分的 点赞按钮

-(void)varityStampBtnSelected:(UIButton *)btn;//综艺部分的  踩  按钮

-(void)varityPushAndPopBtnSelected:(UIButton *)btn;// 综艺部分的 拉伸按钮

@end

@interface VarietyDetailIntrolView : UIView

@property (nonatomic,assign) id<VarietyDetailIntrolViewDelegate>delegate;
@property (nonatomic,assign) NSInteger eventType;//节目类型 1、电影 2、电视剧 3、综艺   这个用来判断视图类型

@property (nonatomic,strong) UIImageView *tranLineImage;//顶部横线
@property (nonatomic,strong) UILabel *introLabel;//剧情介绍

@property (nonatomic,strong) UILabel *yearLabel;//年份
@property (nonatomic,strong) UILabel *typeLabel;//类型
@property (nonatomic,strong) UILabel *areaLabel;//地区
@property (nonatomic,strong) UILabel *mostNewLabel;//最新

@property (nonatomic,strong) UIButton *praiseBtn;//赞
@property (nonatomic,strong) UIButton *stampBtn;//踩

@property (nonatomic,strong) UILabel *praiseNumLabel;//点赞量
@property (nonatomic,strong) UILabel *stampNumLabel;//踩量


@property (nonatomic,strong) UIButton *VpushAndPopBtn;//拉伸 回收 按钮



-(void)loadIntrolWith:(NSString *)introl andYear:(NSString *)year andType:(NSString *)type andArea:(NSString *)area andMostNew:(NSString *)mostNew andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stamNum;

-(void)VpraiseSuccecc:(BOOL)isSuccecc;

-(void)VstampSuccecc:(BOOL)isSuccecc;


@end
