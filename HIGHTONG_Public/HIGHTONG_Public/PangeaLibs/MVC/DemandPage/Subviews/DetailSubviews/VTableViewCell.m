//
//  VTableViewCell.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VTableViewCell.h"

@implementation VTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setCustomView];
  
    }

    return self;
}

- (void)awakeFromNib {
    // Initialization code

}


-(void)setCustomView{
    WS(wself);
    _backImage = [[UIImageView alloc] init];
    [_backImage setBackgroundColor:[UIColor orangeColor]];
    _backImage.layer.cornerRadius = 3;
    _backImage.layer.borderWidth = 1.0;
    _backImage.layer.borderColor = [[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] CGColor];
    [self.contentView addSubview:_backImage];
    
    [_backImage mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(2*kRateSize);
        make.left.mas_equalTo(10*kRateSize);
        make.height.mas_equalTo(32*kRateSize);
        make.width.mas_equalTo(kDeviceWidth-20*kRateSize);
        
    }];

    _VTitleLabel = [[UILabel alloc] init];
    [_VTitleLabel setBackgroundColor:[UIColor clearColor]];
    _VTitleLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:13.0f];
    [self.contentView  addSubview:_VTitleLabel];
    
    [_VTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_backImage.mas_top);
        make.left.mas_equalTo(_backImage.mas_left).offset(3*kRateSize);
        make.height.mas_equalTo(_backImage.mas_height);
        make.width.mas_equalTo(_backImage.mas_width);
        
    }];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
