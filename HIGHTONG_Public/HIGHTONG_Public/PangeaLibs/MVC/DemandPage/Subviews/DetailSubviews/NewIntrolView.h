//
//  NewIntrolView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
#import "DemandModel.h"
#import "DetailPropertyView.h"

typedef void(^BtnReturnBlock)(UIButton_Block *btn);

@interface NewIntrolView : UIView


@property (nonatomic,copy)BtnReturnBlock praiseBlock;
@property (nonatomic,copy)BtnReturnBlock stampBlock;


/**
 *  节目名
 */
@property (nonatomic,strong) UILabel *programNameLabel;
/**
 *  主演演员
 */
@property (nonatomic,strong) UILabel *actorLabel;
/**
 *  导演
 */
@property (nonatomic,strong) UILabel *directorLabel;
/**
 *  节目类型
 */
@property (nonatomic,strong) UILabel *programTypeLabel;
/**
 *  节目年份
 */
@property (nonatomic,strong) UILabel *yearsLabel;
/**
 *  区域
 */
@property (nonatomic,strong) UILabel *areaLabel;
/**
 *  点赞按钮
 */
@property (nonatomic,strong) UIButton_Block *praiseBtn;
/**
 *  点踩按钮
 */
@property (nonatomic,strong) UIButton_Block *stampBtn;
/**
 *  点赞量
 */
@property (nonatomic,strong) UILabel *praiseNumLabel;
/**
 *  点踩量
 */
@property (nonatomic,strong) UILabel *stampNumLabel;
/**
 *  介绍区域 伸缩按钮
 */
@property (nonatomic,strong) UIButton_Block *pushBtn;
/**
 *  节目介绍
 */
@property (nonatomic,strong) UILabel *introLabel;
/**
 *  属性视图
 */
@property (nonatomic,strong) DetailPropertyView *propertyView;



/**
 *  点播介绍视图 填充数据
 *
 *  @param name      节目名称
 *  @param actor     演员
 *  @param director  导演
 *  @param type      类型
 *  @param years     年份
 *  @param area      地区
 *  @param praiseNum 点赞量
 *  @param stampNum  点踩量
 *  @param introl    简介
 */

-(void)setIntrolInfoWithName:(NSString *)name andActor:(NSString *)actor andDirector:(NSString *)director andType:(NSString *)type andYears:(NSString *)years andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum andIntrol:(NSString *)introl;

-(void)setIntroInfoWithModel:(DemandModel *)demandModel;

-(void)setPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum;

-(void)resetPraiseNum:(NSString *)praiseNum;

-(void)resetStampNum:(NSString *)stampNum;

/**
 *  重置点赞按钮图片
 *
 *  @param isSucceed 是否点赞成功
 */
-(void)praiseSucceed:(BOOL)isSucceed;

/**
 *  重置点踩按钮图片
 *
 *  @param isSucceed 是否点踩成功
 */
-(void)stampSucceed:(BOOL)isSucceed;


@end
