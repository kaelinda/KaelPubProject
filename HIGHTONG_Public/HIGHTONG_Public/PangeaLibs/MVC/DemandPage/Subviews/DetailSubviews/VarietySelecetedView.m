//
//  VarietySelecetedView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VarietySelecetedView.h"
#import "VTableViewCell.h"
@implementation VarietySelecetedView


-(instancetype)init{
    self = [super init];
    if (self) {
        [self setBackgroundColor:UIColorFromRGB(0xffffff)];
        cellHeigh = 35;

        _episodeNum = 30;
        _selectedIndex = 0;
        [self setupSubView];
        
//        _VArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12"];

    }

    return self;
}

-(void)setupSubView{
    WS(wself);
    
//    _lightGrayView = [[UIView alloc] init];
//    [_lightGrayView setBackgroundColor:App_background_color];
//    [self addSubview:_lightGrayView];
//    
//    [_lightGrayView mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.mas_equalTo(wself.mas_top);
//        make.left.mas_equalTo(wself.mas_left);
//        make.height.mas_equalTo(27*kRateSize);
//        make.width.mas_equalTo(kDeviceWidth);
//    }];

//    UIView *BLineview = [[UIView alloc] init];
//    [BLineview setBackgroundColor:App_background_color];
//    [self addSubview:BLineview];
//    
//    [BLineview mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.mas_equalTo(wself.mas_top).offset(-2*kRateSize);
//        make.left.mas_equalTo(wself.mas_left);
//        make.width.mas_equalTo(wself.mas_width);
//        make.height.mas_equalTo(4*kRateSize);
//        
//    }];
    _episodeTitleLabel = [[UILabel alloc] init];
    [_episodeTitleLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
    [_episodeTitleLabel setTintColor:[UIColor colorWithRed:0.31 green:0.31 blue:0.31 alpha:1]];
    
    [_episodeTitleLabel setTextAlignment:NSTextAlignmentLeft];
    [_episodeTitleLabel setText:@"剧 集 "];
    [_episodeTitleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_episodeTitleLabel];
    [_episodeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(8*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-10*kRateSize);
        make.height.mas_equalTo(27*kRateSize);
        
    }];
    
    //-----------------
    
    _episodeNumLabel = [[UILabel alloc] init];
    [_episodeNumLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
    [_episodeNumLabel setTextAlignment:NSTextAlignmentRight];
    [_episodeNumLabel setBackgroundColor:[UIColor clearColor]];
    [_episodeNumLabel setTintColor:UIColorFromRGB(0x666666)];
    
    [self addSubview:_episodeNumLabel];
    
    [_episodeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(8*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-15*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2);
        
        
    }];
    //----------
//    _lineImageView = [[UIImageView alloc] init];
//    [_lineImageView setBackgroundColor:UIColorFromRGB(0xaaaaaa)];
//    _lineImageView.alpha = 0.7;
//    [wself addSubview:_lineImageView];
//    
//    [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.mas_equalTo(wself.mas_top).offset(25*kRateSize+29*kRateSize);
//        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
//        make.right.mas_equalTo(wself.mas_right).offset(-10*kRateSize);
//        make.height.mas_equalTo(0.5);
//        
//    }];
    
    _numScrollView = [[UIScrollView alloc] init];
    _numScrollView.showsVerticalScrollIndicator = NO;
    _numScrollView.showsHorizontalScrollIndicator = NO;
    _numScrollView.bounces = NO;
    [_numScrollView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_numScrollView];
    
    [_numScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(30*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(2*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-10*kRateSize);
        
    }];
    
//    [self fillScrollView];
    
    _VTableView = [[UITableView alloc] init];
    [_VTableView setBackgroundColor:[UIColor clearColor]];
    _VTableView.delegate = self;
    _VTableView.dataSource = self;
    _VTableView.contentOffset = CGPointMake(0, 0);
    _VTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_VTableView];
    
    [_VTableView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(55*kRateSize);
        make.left.mas_equalTo(_numScrollView.mas_left);
        make.height.mas_equalTo(0*kRateSize);
        make.width.mas_equalTo(kDeviceWidth);
    }];
    
    //----------
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_VTableView.mas_bottom).offset(10*kRateSize);
    }];
    

}
-(void)fillScrollView{
    
    [_numScrollView removeAllSubviews];
    _numScrolContentView = [[UIView alloc] init];
    [_numScrolContentView setBackgroundColor:[UIColor clearColor]];
    [_numScrollView addSubview:_numScrolContentView];
    
    [_numScrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_numScrollView);
        make.height.mas_equalTo(_numScrollView);
        
    }];
    
    NSInteger btnNum = 0;
//    _episodeNum = 0;
    if (_episodeNum>0) {
        
        btnNum = _episodeNum/5+1;
        if (_episodeNum%5==0) {
            btnNum = _episodeNum/5;
        }
    }
    
    //--------------
    UIButton *lastBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    for (int i=0; i<btnNum; i++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        if ((i+1)*5 >= _episodeNum) {
            if (_VArray.count>0) {
                NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[[_VArray objectAtIndex:i*5] objectForKey:@"seq"],[[_VArray objectAtIndex:_episodeNum-1] objectForKey:@"seq"]];
                [btn setTitle:titleStr forState:UIControlStateNormal];

            }else{
                [btn setTitle:[NSString stringWithFormat:@"%.2d-%.2ld",i*5+1,(long)_episodeNum] forState:UIControlStateNormal];
            }
            
        }else{
            if (_VArray.count>0) {
                NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[[_VArray objectAtIndex:i*5] objectForKey:@"seq"],[[_VArray objectAtIndex:(i+1)*5-1] objectForKey:@"seq"]];
                [btn setTitle:titleStr forState:UIControlStateNormal];
                
            }else{
                [btn setTitle:[NSString stringWithFormat:@"%.2d-%.2d",i*5+1,(i+1)*5] forState:UIControlStateNormal];
            }
        }
        btn.tag = i;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btn addTarget:self action:@selector(changeBtnNum:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:[UIColor clearColor]];
        [_numScrolContentView addSubview:btn];
        
        //------------添加限制
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_numScrolContentView.mas_top).offset(1*kRateSize);
            make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
            make.left.mas_equalTo(61*kRateSize*i);
            
        }];
        
        lastBtn = btn;
        
    }
    
    //---------------
    
    [_numScrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastBtn.mas_right);
        
    }];
    
    
}
-(void)changeBtnNum:(UIButton *)btn{

    NSLog(@"tag 值是 %ld的按钮被选中了     ----  %@",(long)btn.tag,btn.titleLabel.text);
    //----范围选择的  按钮颜色
    NSArray *subAOEBtnArr = [_numScrolContentView subviews];
    for (int i=0; i<[subAOEBtnArr count]; i++) {
        UIButton *Itmebtn = (UIButton *)[subAOEBtnArr objectAtIndex:i];
        if (Itmebtn.tag==btn.tag) {
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
        }else{
            [Itmebtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
    }
    
    NSIndexPath *VIndexpath=  [NSIndexPath indexPathForRow:btn.tag*5 inSection:0];
    if ([_VArray count]==0) {
        return;
    }
    if ((btn.tag*5)>[_VArray count]) {
        return;
    }
    [_VTableView scrollToRowAtIndexPath:VIndexpath
                            atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}
-(void)reloadVarityDateWith:(NSArray *)varityArr andAllEpisodNum:(NSInteger)allNum andUpdateNum:(NSString *)updateNum{
    

    
    if (updateNum.length==0 || [updateNum isEqualToString:@"(null)"]) {
        updateNum = @"";
    }
    [_episodeNumLabel setText:[NSString stringWithFormat:@"%@",updateNum]];
    if (_VArray) {
        _VArray = nil;
    }
    _VArray = [[NSMutableArray alloc] initWithArray:varityArr];
    
    _episodeNum = varityArr.count;
    [self fillScrollView];
    [_VTableView reloadData];

    NSInteger vount = (_episodeNum>=5) ? 5:_episodeNum;
    
//    vount = _episodeNum%6;
    [_VTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        
//        make.height.mas_equalTo((180-vount*cellHeigh)*kRateSize);
        make.height.mas_equalTo(vount*cellHeigh*kRateSize);

    }];
    
    
    NSLog(@"Kael 综艺 %@ ",_VArray);


    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_VTableView.mas_bottom).offset(5*kRateSize);
    }];


}

-(void)setSelectedIndex:(NSInteger)selectedIndex{
    _selectedIndex = (selectedIndex-1)>=0?(selectedIndex-1):0;
    //----范围选择的  按钮颜色
    NSArray *subAOEBtnArr = [_numScrolContentView subviews];
    for (int i=0; i<[subAOEBtnArr count]; i++) {
        UIButton *Itmebtn = (UIButton *)[subAOEBtnArr objectAtIndex:i];
        if (Itmebtn.tag==(selectedIndex/6)) {
            [Itmebtn setTitleColor:App_selected_color forState:UIControlStateNormal];
        }else{
            [Itmebtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
    }

    [_VTableView reloadData];

}


#pragma mark - delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [_VArray count];
}

-(VTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    static NSString *CellIdentifier = @"Cell";
    VTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[VTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if (indexPath.row == _selectedIndex) {
        
        [cell.backImage setBackgroundColor:[UIColor colorWithRed:0.95 green:0.98 blue:1 alpha:1]];
        cell.VTitleLabel.textColor = App_selected_color;
        cell.backImage.layer.borderColor = [[UIColor colorWithRed:0.62 green:0.86 blue:0.98 alpha:1] CGColor];
    }else{
        [cell.backImage setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
        cell.VTitleLabel.textColor = [UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1];
        cell.backImage.layer.borderColor = [[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] CGColor];

    }
    
    
    NSDictionary *itemDic = [_VArray objectAtIndex:indexPath.row];
    NSString *playTime =[itemDic objectForKey:@"playTime"];
    NSString *seq = [itemDic objectForKey:@"seq"];
    NSString *intro = [itemDic objectForKey:@"intro"];
    NSString *name = [itemDic objectForKey:@"subName"];
    if (playTime.length==0) {
        playTime = @"";
    }else{
    
        playTime = [[playTime componentsSeparatedByString:@" "] firstObject];
    }
    if (seq.length == 0) {
        seq = @"";
    }
    if (intro.length == 0) {
        intro = @"";
    }
    if (name.length == 0) {
        name = @"";
    }
    
//    NSString *VTitle = [NSString stringWithFormat:@"%@ 第%@期 %@",playTime,seq,name];
    NSString *VTitle = [NSString stringWithFormat:@"%@",name];
    [cell.VTitleLabel setTextAlignment:NSTextAlignmentLeft];
//    cell.VTitleLabel.text = @"值是 0的按钮被选中了值是 0的按钮被选中了";
    cell.VTitleLabel.text =VTitle;

    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return cellHeigh*kRateSize;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectedIndex = indexPath.row;
    [self setSelectedIndex:indexPath.row+1];
//    [_VTableView reloadData];
    
    if ([self.delegate respondsToSelector:@selector(VTableViewSelectedIndex:)]) {
        [self.delegate VTableViewSelectedIndex:_selectedIndex+1];
    }

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
