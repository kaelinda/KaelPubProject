//
//  DetailTopBarView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void(^ReturnBlock)();
typedef void(^FavReturnBlock)(BOOL);


@interface DetailTopBarView : UIView


//@property (nonatomic,copy) ReturnBlock shareBclok;
//@property (nonatomic,copy) ReturnBlock favBlock;

/**
 *  播放量
 */
@property (nonatomic,strong) UILabel *playNumLabel;
/**
 *  分享按钮
 */
@property (nonatomic,strong) UIButton_Block *shareBtn;
/**
 *  收藏按钮
 */
@property (nonatomic,strong) UIButton_Block *favBtn;



/**
 *  初始化视图
 *
 *  @param shareBlock 分享按钮的响应事件
 *  @param favBlock   收藏按钮的响应事件
 *
 *  @return 视图对象
 */
-(instancetype)initTopBarWithShareBlock:(ReturnBlock)shareBlock andWithFavBlock:(FavReturnBlock)favBlock;
/**
 *  重置播放量
 *
 *  @param playNum 播放量
 */
-(void)resetPlayNumWith:(NSString *)playNum;
/**
 *  重置收藏按钮视图
 *
 *  @param isFavorated 是否已经添加收藏
 */
-(void)resetFavorate:(BOOL)isFavorated;



@end
