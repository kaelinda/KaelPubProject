//
//  DetailPropertyView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/15.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DetailPropertyView.h"
@implementation DetailPropertyView
-(instancetype)init{
    self = [super init];
    if (self) {
        WS(wself);
        _hiddenProperty = YES;
        _modelArr = [[NSMutableArray alloc] init];
        _sortArr = [NSArray arrayWithObjects:@"actor",@"",@"",@"",@"",@"",@"",@"",@"", nil];
        
        _introlLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x999999) andFont:[UIFont systemFontOfSize:11*kRateSize]];
        [_introlLabel setNumberOfLines:0];
        _introlLabel.tag = 99;
        _introlLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self addSubview:_introlLabel];
        
        [_introlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(wself.mas_top);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            
        }];
        
    }
    return self;
}


-(void)reloadProperTypeViewWithModel:(DemandModel *)demandMoel{
    
    [_modelArr removeAllObjects];
    _modelArr =[self getModelArrWithModel:demandMoel];
    
    NSArray *subViews = [self subviews];
    for (int i=0; i<subViews.count; i++) {
        UILabel *label = (UILabel *)[subViews objectAtIndex:i];
        if (label.tag != 99) {
            
            [label removeFromSuperview];
        }else{
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@""];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:3];//调整行间距
            //            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_demandModel.intro length])];
            _introlLabel.attributedText = attributedString;
            [_introlLabel sizeToFit];
            
        }
    }
    WS(wself);
    UILabel *lastLabel = [[UILabel alloc] init];//最后一个label
    
    //************** 属性 **************
    
    
    for (int i=0; i<_modelArr.count; i++) {
        
        UILabel *propertyLabel = [KaelTool setlabelPropertyWithBackgroundColor:App_white_color andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:11*kRateSize]];
        propertyLabel.tag = i;
        propertyLabel.attributedText = [_modelArr objectAtIndex:i];
        [self addSubview:propertyLabel];
        
        if (i==0) {
            propertyLabel.tag = 98;
            _actorLabel = propertyLabel;
            [propertyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(wself.mas_top);
                make.left.mas_equalTo(wself.mas_left);
                make.right.mas_equalTo(wself.mas_right);
                make.height.mas_equalTo(15*kRateSize);
            }];
        }else{
            propertyLabel.hidden = _hiddenProperty;
            NSInteger count = (i-1)/2 + 1;
            CGFloat leftPosition = (i%2==0)?((wself.width-10*kRateSize)*3/5):0;
            CGFloat LWidth = (wself.width-10*kRateSize)/2;
            
            [propertyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(wself.mas_top).offset(count*(15+5)*kRateSize);
                make.left.mas_equalTo(wself.mas_left).offset(leftPosition);
                make.width.mas_equalTo(LWidth);
                make.height.mas_equalTo(15*kRateSize);
            }];
        }
        if (i==(_modelArr.count-1)) {
            lastLabel = propertyLabel;
            _lastLabel = propertyLabel;
        }
        
    }
    
    //*************** 简介：label  *************
    //    [self addSubview:_introlLabel];
    //
    //    [_introlLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(wself.mas_top);
    //        make.left.mas_equalTo(wself.mas_left);
    //        make.right.mas_equalTo(wself.mas_right);
    //
    //    }];
    CGFloat rowSpace = 8;
    if (kDeviceWidth == 320) {
        rowSpace = 5;
    }
    
    NSString *introl = @"";
    if (_demandModel.intro.length>0) {
        introl = _demandModel.intro;
    }
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:introl];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:rowSpace];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_demandModel.intro length])];
    _introlLabel.attributedText = attributedString;
    [_introlLabel sizeToFit];
    
    _introlLabel.backgroundColor = App_white_color;
    if (_hiddenProperty) {
        _introlLabel.hidden = _hiddenProperty;
        [_introlLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(wself.mas_top).offset(15*kRateSize);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            make.height.mas_equalTo(0);
            
            
        }];
    }else{
        [_introlLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(lastLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            
            
        }];
    }
    
    
    //    if (_hiddenProperty) {
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_introlLabel.mas_bottom).offset(5*kRateSize);
    }];
    //    }else{
    //        [self mas_updateConstraints:^(MASConstraintMaker *make) {
    //            make.bottom.mas_equalTo(wself.mas_top).offset(15*kRateSize);
    //        }];
    //    }
    
    
}
-(NSMutableArray *)getModelArrWithModel:(DemandModel *)model{
    
    _demandModel = model;
    NSMutableArray *mModelArr = [NSMutableArray array];
    
    if (NotEmptyStringAndNilAndNull(model.Actor)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"主演：%@",model.Actor]]];
    }
    if (NotEmptyStringAndNilAndNull(model.Director)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"导演：%@",model.Director]]];
        
    }
    if (NotEmptyStringAndNilAndNull(model.releaseYear)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"年份：%@",model.releaseYear]]];
        
    }
    if (NotEmptyStringAndNilAndNull(model.category)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"类型：%@",model.category]]];
        
    }
    
    if (NotEmptyStringAndNilAndNull(model.country)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"地区：%@",model.country]]];
        
    }
    if (NotEmptyStringAndNilAndNull(model.language)) {
        [mModelArr addObject:[self getDiffirentColorstringWithString:[NSString stringWithFormat:@"语言：%@",model.language]]];
        
    }
    //    [mModelArr addObject:model.intro];
    
    return mModelArr;
}


-(NSMutableAttributedString *)getDiffirentColorstringWithString:(NSString *)str{
    NSMutableAttributedString *Kstr = [[NSMutableAttributedString alloc] initWithString:str];
    [Kstr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x333333) range:NSMakeRange(0,2)];
    return Kstr;
}

-(void)setPropertyHideen:(BOOL)hidden{
    WS(wself);
    _hiddenProperty = hidden;
    _introlLabel.hidden = _hiddenProperty;
    
    UILabel *lastLabel = [[UILabel alloc] init];
    NSArray *subViews = [self subviews];
    
    for ( int i=0; i<=_modelArr.count; i++) {
        UILabel *label = (UILabel *)[subViews objectAtIndex:i];
        
        if (label.tag == (_modelArr.count-1)) {
            lastLabel = label;
        }
        
        if (label.tag!=98) {
            label.hidden = hidden;
        }
        
    }
    
    
    
    
    if (hidden) {
        [_introlLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(wself.mas_top).offset(15*kRateSize);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            make.height.mas_equalTo(0);
            
            
        }];
        
        //        [self mas_updateConstraints:^(MASConstraintMaker *make) {
        //            make.bottom.mas_equalTo(wself.mas_top).offset(15*kRateSize);
        //        }];
        
    }else{
        [_introlLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_lastLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            
        }];
        
        //        [self mas_updateConstraints:^(MASConstraintMaker *make) {
        ////            make.bottom.mas_equalTo(_lastLabel.mas_bottom).offset(5*kRateSize);
        //            make.bottom.mas_equalTo(_introlLabel.mas_bottom).offset(5*kRateSize);
        ////            make.bottom.mas_equalTo(wself.mas_top).offset(150*kRateSize);
        ////            make.height.mas_equalTo(215);
        //
        //
        //        }];
        
    }
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
