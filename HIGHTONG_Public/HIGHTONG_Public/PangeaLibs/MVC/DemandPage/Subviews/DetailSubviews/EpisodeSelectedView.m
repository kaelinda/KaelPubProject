//
//  EpisodeSelectedView.m
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "EpisodeSelectedView.h"
#import "EpisodeButton.h"
@implementation EpisodeSelectedView



-(instancetype)init{
    self = [super init];
    if (self) {
        [self setBackgroundColor:UIColorFromRGB(0xffffff)];

        [self setupSubViews];
        _freeItemsArr = [[NSMutableArray alloc] init];
        _truthEpisodeArr = [[NSMutableArray alloc] init];
        _episodeNum = 131;
    }

    return self;
}

-(void)setupSubViews{

    WS(wself);

    
    _episodeTitleLabel = [[UILabel alloc] init];
    [_episodeTitleLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
//    [_episodeTitleLabel setTintColor:[UIColor colorWithRed:0.31 green:0.31 blue:0.31 alpha:1]];

    [_episodeTitleLabel setTextAlignment:NSTextAlignmentLeft];
    [_episodeTitleLabel setText:@"剧 集 "];
    [_episodeTitleLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_episodeTitleLabel];
    [_episodeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(8*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-10*kRateSize);
        make.height.mas_equalTo(27*kRateSize);
        
    }];
    
//-----------------
    
    _episodeNumLabel = [[UILabel alloc] init];
    [_episodeNumLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
    [_episodeNumLabel setTextAlignment:NSTextAlignmentRight];
    [_episodeNumLabel setText:@"已更新至*集/全*集"];
    [_episodeNumLabel setBackgroundColor:[UIColor clearColor]];
    [_episodeNumLabel setTintColor:UIColorFromRGB(0x666666)];

    [self addSubview:_episodeNumLabel];
    
    [_episodeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(8*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-15);
        make.width.mas_equalTo(kDeviceWidth/2);
        
        
    }];
//----------
    _lineImageView = [[UIImageView alloc] init];
    [_lineImageView setBackgroundColor:UIColorFromRGB(0xaaaaaa)];
    _lineImageView.alpha = 0.7;
    _lightGrayView.hidden = YES;
    [wself addSubview:_lineImageView];
    
    [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(25*kRateSize+29*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-10*kRateSize);
        make.height.mas_equalTo(0);
        
    }];
    
//---------
    _numScrollView = [[UIScrollView alloc] init];
    _numScrollView.showsVerticalScrollIndicator = NO;
    _numScrollView.showsHorizontalScrollIndicator = NO;
    _numScrollView.bounces = NO;
    [_numScrollView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_numScrollView];
    
    [_numScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(30*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(2*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-10*kRateSize);

    }];
    
    [self fillScrollView];
    
    
    //-----------添加选集按钮  因为这里固定的  最多是12个 所以  在初始化的时候就给添加上了
    
    CGSize EBtnSize = CGSizeMake(44*kRateSize, 30*kRateSize);
    CGFloat EBtn_widthSpace = 7*kRateSize;
    CGFloat EBtn_highSpace = 6*kRateSize;
    
    
    EpisodeButton *lastEBtn = [[EpisodeButton alloc] init];
    
    for (int i=0; i<3; i++) {
        
        for (int j=0; j<6; j++) {
            
            NSInteger ETag = j+i*6+1;
            EpisodeButton *EBtn = [[EpisodeButton alloc] init];
            [EBtn setEBadgeValue:@"付费"];
            [EBtn setTag:ETag];
            [self setEpisodeBtnWithEBtn:EBtn andSelected:NO];
            [EBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [EBtn setTitle:[NSString stringWithFormat:@"%02ld",(long)ETag] forState:UIControlStateNormal];
            [EBtn addTarget:self action:@selector(eBtnSelected:) forControlEvents:UIControlEventTouchUpInside];

            [EBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];

            [self addSubview:EBtn];
            
            [EBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.top.mas_equalTo(wself.mas_top).offset((EBtnSize.height+EBtn_highSpace)*i+60*kRateSize);
                make.left.mas_equalTo(wself.mas_left).offset(j*(EBtnSize.width+EBtn_widthSpace)+10*kRateSize);
                make.size.mas_equalTo(EBtnSize);
                
            }];
            if (i==1||j==5) {
                lastEBtn = EBtn;
            }
        
            
        }
   
    }
    

//    [self mas_makeConstraints:^(MASConstraintMaker *make) {
//        
////        make.bottom.mas_equalTo(_numScrollView.mas_bottom).offset(5);
//        make.bottom.mas_equalTo(lastEBtn.mas_bottom).offset(10);
//        
//    }];
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(_lineImageView.mas_bottom).offset((65+2*20)*kRateSize);
        
    }];
    
}

#pragma mark - 剧集按钮 点击响应
-(void)eBtnSelected:(EpisodeButton *)EBtn{

    
    
    NSLog(@"EBtn clicked ! ~%@(这里表示 选中了某一剧集 将要请求 并播放)",EBtn.titleLabel.text);
    _selectedIndex = [EBtn.titleLabel.text integerValue];
    for (int i=0; i<_truthEpisodeArr.count; i++) {
        NSString *subName = [[_truthEpisodeArr objectAtIndex:i] objectForKey:@"subName"];
        if ([EBtn.titleLabel.text isEqualToString:subName]) {
            _selectedIndex = (NSInteger)(i+1);
        }
        
//        if ([EBtn.titleLabel.text integerValue] == [[[_truthEpisodeArr objectAtIndex:i] objectForKey:@"subName"] integerValue]) {
//            _selectedIndex = (NSInteger)(i+1);
//        }
    }
    [self changeSelectedBtn:EBtn];

    if ([self.delegate respondsToSelector:@selector(EpisodeViewBtnSelectedAtIndex:)]) {
        [self.delegate EpisodeViewBtnSelectedAtIndex:_selectedIndex];
    }
    
    
}

#pragma mark - 免费的隐藏徽章
-(void)hiddenBadgeAtIndex:(NSInteger)index{

    [_freeItemsArr addObject:[NSString stringWithFormat:@"%02ld",(long)index]];
    NSArray *subViewArray = [NSArray array];
    subViewArray = [self subviews];

    NSDictionary *episodeItem = [NSDictionary dictionaryWithDictionary:[_truthEpisodeArr objectAtIndex:(index-1)]];
    NSString *freeTitle = [episodeItem objectForKey:@"subName"];
    
    for (int i=0; i<subViewArray.count; i++) {
        
        if ([[subViewArray objectAtIndex:i] isKindOfClass:[EpisodeButton class]]) {
            
            EpisodeButton *btn;
            btn = [subViewArray objectAtIndex:i];

//            NSLog(@"K按钮 ：%@-----%@",btn.titleLabel.text,freeTitle);
            if ([btn.titleLabel.text isEqualToString:freeTitle]) {
                    
                [btn hiddenBadgeView:YES];
            }else{
                //                [btn hiddenBadgeView:NO];
            }

        }
        
    }

}

#pragma mark - 设置默认选中某一个按钮
-(void)setSelectedBtnAtIndex:(NSInteger)index{

    
    _selectedIndex = index;
    NSInteger TIndex = index%12;
    index = TIndex;
    NSArray *subViewArray = [NSArray array];
    subViewArray = [self subviews];
    
    NSString *selectedStr = @"";
    if (_selectedIndex<=_truthEpisodeArr.count &&_truthEpisodeArr.count>0) {
        selectedStr= [[_truthEpisodeArr objectAtIndex:(_selectedIndex-1)] objectForKey:@"subName"];
        
    }
    
    for (int i=0; i<subViewArray.count; i++) {
        
        if ([[subViewArray objectAtIndex:i] isKindOfClass:[EpisodeButton class]]) {
            
            EpisodeButton *btn = [[EpisodeButton alloc] init];
            btn = [subViewArray objectAtIndex:i];
            
            if ([btn.titleLabel.text isEqualToString:selectedStr]) {
                
                [self setEpisodeBtnWithEBtn:btn andSelected:YES];
            }else{
        
                [self setEpisodeBtnWithEBtn:btn andSelected:NO];

            }
            
           
        }
        
    }
    


}

#pragma mark - 剧集按钮初始化 先初始化 范围按钮 在初始化剧集按钮
-(void)loadBtnWithEpisodNum:(NSInteger)episodNum{

    [_numScrolContentView removeAllSubviews];
    
    NSInteger btnNum = 0;
    _episodeNum = episodNum;
    if (episodNum>0) {
        if (episodNum%18>0) {
            btnNum = episodNum/18+1;

        }else{
            btnNum = episodNum/18;
        }
        
    }
    //--------------滑动视图上的 按钮（剧集范围）
    UIButton *lastBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    for (int i=0; i<btnNum; i++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        if (((i+1)*18) >= (int)episodNum) {
            if (_truthEpisodeArr.count>0 && _truthEpisodeArr.count>(i*18)) {
                NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:i*18] objectForKey:@"seq"]],[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:(episodNum-1)] objectForKey:@"seq"]]];
                [btn setTitle:titleStr forState:UIControlStateNormal];
   
            }else{
                [btn setTitle:[NSString stringWithFormat:@"%02d-%02ld",i*18+1,(long)episodNum] forState:UIControlStateNormal];
            }
            
        }else{
            if (_truthEpisodeArr.count>0) {
                NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:i*18] objectForKey:@"seq"]],[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:(i+1)*18-1] objectForKey:@"seq"]]];
                [btn setTitle:titleStr forState:UIControlStateNormal];
            }else{
                [btn setTitle:[NSString stringWithFormat:@"%02d-%02d",i*18+1,(i+1)*18] forState:UIControlStateNormal];
            }


        }
        btn.tag = i;
        [btn addTarget:self action:@selector(changeBtnNum:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:[UIColor clearColor]];
        [_numScrolContentView addSubview:btn];
        
        //------------添加限制
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_numScrolContentView.mas_top).offset(1*kRateSize);
            make.size.mas_equalTo(CGSizeMake(50*kRateSize, 20*kRateSize));
            make.left.mas_equalTo(51*kRateSize*i);
            
        }];
        
        lastBtn = btn;
        
        //---------默认选中第一部分 这个很重要！！！
        if (btn.tag==0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                //这里是1.0f后 你要做的事情
                [self changeBtnNum:btn];
            });
        }
        
        
    }
    
    //---------------
    [_numScrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastBtn.mas_right);
        
    }];
//----------------- 选择某一剧集的按钮
    
    NSArray *subViewArray = [NSArray array];
    subViewArray = [self subviews];
    
    EpisodeButton *lastEBtn = [[EpisodeButton alloc] init];
    
    for (int i=0; i<subViewArray.count; i++) {
        
        if ([[subViewArray objectAtIndex:i] isKindOfClass:[EpisodeButton class]]) {
            
            EpisodeButton *btn = [[EpisodeButton alloc] init];
            btn = [subViewArray objectAtIndex:i];
            if ((btn.tag-1)<_truthEpisodeArr.count) {
                NSString *titleNum = [[_truthEpisodeArr objectAtIndex:btn.tag-1] objectForKey:@"subName"];
                btn.titleLabel.text = titleNum;
                [btn setTitle:titleNum forState:UIControlStateNormal];
//                btn.titleLabel.text = [NSString stringWithFormat:@"%02ld",[titleNum integerValue]];

//                if ([btn.titleLabel.text integerValue]>episodNum) {
//                    btn.hidden = YES;
//                }else{
                    btn.hidden = NO;
//                }
            }else{
                btn.hidden = YES;
            }
        
           
            
            if ([btn.titleLabel.text integerValue]==_selectedIndex ) {
                
//                [btn setBackgroundColor:App_selected_color];
                [self setEpisodeBtnWithEBtn:btn andSelected:YES];

                
            }else{
                
//                [btn setBackgroundColor:[UIColor whiteColor]];
                [self setEpisodeBtnWithEBtn:btn andSelected:NO];

            
            }
            if (episodNum == [btn.titleLabel.text integerValue] ) {
                lastEBtn = btn;
//                [self mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.bottom.mas_equalTo(lastEBtn.mas_bottom).offset(5);
//                    
//                }];
                
            }
            
            //适配按钮个数  导致的剧集选择 下部空白
            
            NSInteger BtnHigh = 2;
            if (_episodeNum > 12) {
                BtnHigh = 2;
            }else if (_episodeNum > 6){
                BtnHigh = 1;
            }else{
                BtnHigh = 0;
            }
            
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.mas_equalTo(_lineImageView.mas_bottom).offset(55*kRateSize+(BtnHigh*25+6*(BtnHigh-1))*kRateSize);
                
            }];
            
            //---改变是否付费的按钮显示 的方法
            for (int i=0; i<_freeItemsArr.count; i++) {
                if ([btn.titleLabel.text integerValue]==[[_freeItemsArr objectAtIndex:i] integerValue]) {
                    [btn hiddenBadgeView:YES];
                    
                }else{
                    [btn hiddenBadgeView:NO];
                }
                
            }
            
        }
        
    }
    
//-----------默认选中第一个按钮
   
    
}

#pragma mark - 加载视图
-(void)loadAllEpisodNumWithAllNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum{
    if (updateNum.length==0 || [updateNum isEqualToString:@"(null)"] || [updateNum isEqualToString:@"null"]) {
        updateNum = @"";
    }
    if (allNum.length==0 || [allNum isEqualToString:@"(null)"] || [allNum isEqualToString:@"null"]) {
        allNum = @"";
    }
    [_episodeNumLabel setText:[NSString stringWithFormat:@"%@/全%@集",updateNum,allNum]];
    
    [self loadBtnWithEpisodNum:_truthEpisodeArr.count];
}
#pragma mark - 重新加载视图（新版API）
-(void)reloadAllEpisodeNumWithAllNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum andEpisodeArr:(NSArray *)episodeArr{
    _truthEpisodeArr =  [[NSMutableArray alloc] initWithArray:episodeArr];
    [self reloadAllEpisodeNumWithAllNum:allNum andUpdateNum:updateNum andEpisodeArr:episodeArr];
    
}

#pragma mark - 填充聚集范围视图
-(void)fillScrollView{
    
    _numScrolContentView = [[UIView alloc] init];
    [_numScrolContentView setBackgroundColor:[UIColor clearColor]];
    [_numScrollView addSubview:_numScrolContentView];
    
    [_numScrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_numScrollView);
        make.height.mas_equalTo(_numScrollView);
        
    }];
    
    NSInteger btnNum = 0;
    _episodeNum = 18;
    if (_episodeNum>0) {
        if (_episodeNum%18>0) {
            btnNum = _episodeNum/18+1;
            
        }else{
            btnNum = _episodeNum/18;
        }
    }

    //--------------
        UIButton *lastBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
        for (int i=0; i<btnNum; i++) {
    
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            if ((i+1)*18 >= _episodeNum) {
                if (_truthEpisodeArr.count>0) {
                    NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:i*18] objectForKey:@"seq"]],[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:(_episodeNum-1)] objectForKey:@"seq"]]];
                    [btn setTitle:titleStr forState:UIControlStateNormal];
                }else{
                    [btn setTitle:[NSString stringWithFormat:@"%02d-%02ld",i*18+1,(long)_episodeNum] forState:UIControlStateNormal];
                }

    
            }else{
                if (_truthEpisodeArr.count>0) {
                    NSString *titleStr = [NSString stringWithFormat:@"%@-%@",[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:i*18] objectForKey:@"seq"]],[self getAreaNumBtnStandardStringWithString:[[_truthEpisodeArr objectAtIndex:((i+1)*18-1)] objectForKey:@"seq"]]];
                    [btn setTitle:titleStr forState:UIControlStateNormal];
                }else{
                    [btn setTitle:[NSString stringWithFormat:@"%02d-%02d",i*18+1,(i+1)*18] forState:UIControlStateNormal];
                }
            }
            btn.tag = i;
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn addTarget:self action:@selector(changeBtnNum:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundColor:[UIColor clearColor]];
            [_numScrolContentView addSubview:btn];
    
            //------------添加限制
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
    
                make.top.mas_equalTo(_numScrolContentView.mas_top).offset(1*kRateSize);
                make.size.mas_equalTo(CGSizeMake(50*kRateSize, 20*kRateSize));
                make.left.mas_equalTo(51*kRateSize*i);
    
            }];
    
            lastBtn = btn;
    
        }
    
  
    
    //---------------
   
    [_numScrolContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastBtn.mas_right);
        
    }];


}

#pragma mark - 更改选中按钮
-(void)changeSelectedBtn:(EpisodeButton *)EBtn{
    
    NSArray *subViewArray = [NSArray array];
    subViewArray = [self subviews];
    
    for (int i=0; i<subViewArray.count; i++) {
        
        if ([[subViewArray objectAtIndex:i] isKindOfClass:[EpisodeButton class]]) {
            
            EpisodeButton *btn = [[EpisodeButton alloc] init];
            btn = [subViewArray objectAtIndex:i];
            
            if ([btn.titleLabel.text isEqualToString:EBtn.titleLabel.text]) {
                [self setEpisodeBtnWithEBtn:btn andSelected:YES];
            }else{
                
                [self setEpisodeBtnWithEBtn:btn andSelected:NO];
            }

        }
        
    }
    

}

#pragma mark - 剧集按钮响应事件
-(void)changeBtnNum:(UIButton *)btn{
    
    
    NSInteger index = btn.tag;
    NSLog(@"tag 值是 %ld的按钮被选中了     ----  %@",(long)btn.tag,btn.titleLabel.text);
    NSArray *subViewArray = [NSArray array];
    subViewArray = [self subviews];
    
    NSInteger BtnHigh = 2;
    if (_episodeNum-(btn.tag)*18 > 12) {
        BtnHigh = 2;
    }else if (_episodeNum-(btn.tag)*18 > 6){
        BtnHigh = 1;
    }else{
        BtnHigh = 0;
    }
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(_lineImageView.mas_bottom).offset(55*kRateSize+(BtnHigh*25+6*(BtnHigh-1))*kRateSize);

    }];

    NSString *selectedStr = @"";
    if (_selectedIndex<=_truthEpisodeArr.count &&_truthEpisodeArr.count>0) {
        selectedStr= [[_truthEpisodeArr objectAtIndex:(_selectedIndex-1)] objectForKey:@"subName"];

    }

    for (int i=0; i<subViewArray.count; i++) {
        
        if ([[subViewArray objectAtIndex:i] isKindOfClass:[EpisodeButton class]]) {
            
            EpisodeButton *Ebtn = [[EpisodeButton alloc] init];
            Ebtn = [subViewArray objectAtIndex:i];
            NSInteger titleNum = (18*index+Ebtn.tag);
            if (_truthEpisodeArr.count>=titleNum) {
             NSString *titleNumStr = [[_truthEpisodeArr objectAtIndex:titleNum-1] objectForKey:@"subName"];
                [Ebtn setTitle:titleNumStr forState:UIControlStateNormal];
//                [Ebtn setTitle:[NSString stringWithFormat:@"%02ld",(long)titleNum] forState:UIControlStateNormal];
//                if ([Ebtn.titleLabel.text integerValue]>_episodeNum) {
//                    Ebtn.hidden = YES;
//                }else{
                    Ebtn.hidden = NO;
//                }

            }else{
                Ebtn.hidden = YES;
            }
            

            
            if ([Ebtn.titleLabel.text integerValue] ==_episodeNum ) {
//                [self mas_makeConstraints:^(MASConstraintMaker *make) {
//                    
//                    make.bottom.mas_equalTo(Ebtn.mas_bottom).offset(5);
//                    
//                }];
            }
            
            if ([Ebtn.titleLabel.text isEqualToString:selectedStr]) {
//                [Ebtn setBackgroundColor:App_selected_color];
//                [Ebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self setEpisodeBtnWithEBtn:Ebtn andSelected:YES];


            }else{
//                [Ebtn setBackgroundColor:[UIColor whiteColor]];
//                [Ebtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [self setEpisodeBtnWithEBtn:Ebtn andSelected:NO];

            }
            
            for (int i=0; i<_freeItemsArr.count; i++) {
                if ([Ebtn.titleLabel.text integerValue]==[[_freeItemsArr objectAtIndex:i] integerValue]) {
                    [Ebtn hiddenBadgeView:YES];
                    
                }else{
//                    [Ebtn hiddenBadgeView:NO];
                }
                
            }

 
        }
        
    }
    
    
    //----范围选择的  按钮颜色
    NSArray *subAOEBtnArr = [_numScrolContentView subviews];
    for (int i=0; i<[subAOEBtnArr count]; i++) {
        UIButton *Itmebtn = (UIButton *)[subAOEBtnArr objectAtIndex:i];
        if (Itmebtn.tag==btn.tag) {
             [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
        }else{
            [Itmebtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        }
        
    }
    
    
}

#pragma mark - 剧集按钮状态
- (void)setEpisodeBtnWithEBtn:(EpisodeButton *)Ebtn andSelected:(BOOL)isSelected{
    if (isSelected) {
        [Ebtn setBackgroundImage:[UIImage imageNamed:@"sl_vod_details_episode_pressed"] forState:UIControlStateNormal];
        [Ebtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    }else{
        [Ebtn setBackgroundImage:[UIImage imageNamed:@"sl_vod_details_episode_normal"] forState:UIControlStateNormal];
        [Ebtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
        }
}


-(NSString *)getAreaNumBtnStandardStringWithString:(NSString *)str{
    if ([str integerValue]>=0 || [str integerValue]<=9) {
        NSString *string = [NSString stringWithFormat:@"%02ld",(long)[str integerValue]];
        return string;
    }
    return str;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
