//
//  DetailHeaderView.h
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//这是所有类型 都固定的那一块

#import <UIKit/UIKit.h>

@class DetailHeaderView;
@protocol DetailHeaderViewDelegate <NSObject>

-(void)detailHeaderViewFavorBtnSelected:(UIButton *)btn;

@end



@interface DetailHeaderView : UIView

@property (nonatomic,assign) id<DetailHeaderViewDelegate> delegate;


@property (nonatomic,strong) UIImageView *eventImageView;//节目海报
@property (nonatomic,strong) UILabel *eventNameLabel;//节目名称
@property (nonatomic,strong) UIImageView *playNumImage;//播放量图片
@property (nonatomic,strong) UILabel *playNumLabel;//播放量label
@property (nonatomic,strong) UILabel *favorLabel;//收藏显示label
@property (nonatomic,strong) UIButton *favorBtn;//收藏按钮


/**
 *  一次性加载完毕
 *
 *  @param imageUrl 图片链接
 *  @param name     节目名称
 *  @param playNum  播放量
 */
-(void)loadEventImage:(NSString *)imageUrl andName:(NSString *)name andPlayNum:(NSString *)playNum;


/**
 *  加载节目图片
 *
 *  @param imageUrl 图片地址
 */
-(void)loadEventImageWith:(NSString *)imageUrl;

/**
 *  加载节目名称
 *
 *  @param name 节目名
 */
-(void)loadEventNameWith:(NSString *)name;

/**
 *  加载播放量
 *
 *  @param playNum 播放量
 */
-(void)loadPlayNumLabelTextWith:(NSString *)playNum;


/**
 *  收藏成功
 */
-(void)favorSucceed;

/**
 *  取消收藏成功
 */
-(void)cancellledFavor;



@end
