//
//  DemandModel.h
//  HIGHTONG
//
//  Created by HTYunjiang on 13-12-16.
//  Copyright (c) 2013年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DemandModel : NSObject
@property (nonatomic,copy)   NSString            *TRACKID_VOD;//轨道的trackID
@property (nonatomic,copy)   NSString            *TRACKNAME_VOD;//轨道的trackName
@property (nonatomic,copy)   NSString            *EN_VOD;//
@property (nonatomic,strong) NSString            *vodFenleiType;//采集的VOD分类类型
@property (nonatomic,strong) NSString            *vodWatchDuration;//采集的VOD的观看时长
@property (nonatomic,strong) NSString            *vodWatchTime;//采集的VOD的观看时刻
@property (copy,nonatomic  ) NSString            *url;
@property (assign,nonatomic) NSInteger           categoryID;
@property (assign,nonatomic) NSInteger           indexID;//序号
@property (copy ,nonatomic ) NSString            *NAME;//名称,最外层的Name名字
@property (copy,nonatomic  ) NSString            *CONTENTID;//名称，最外层的ContentID
@property (copy ,nonatomic ) NSString            *name;//名称,当前播放的电视剧名字
@property (assign,nonatomic) NSInteger           playCount;//播放次数
@property (assign,nonatomic) NSInteger praiseCount         ;//点赞次数
@property (assign,nonatomic) NSInteger degradeCount        ;//吐槽次数
@property (copy,nonatomic  ) NSMutableDictionary *resultDictionary;
@property (copy,nonatomic) NSString               *vodBID;//总集ID，单集类型节目传@“”
@property (copy,nonatomic) NSString               *VodBName;//总集的Name，单集类型节目传@""

@property (copy,nonatomic  ) NSString       *category;//剧集类型
@property (copy,nonatomic  ) NSString       *rootCategory;//剧集类型
@property (copy ,nonatomic ) NSString       *contentID;//视频编号
@property (copy, nonatomic ) NSString       *favorContenID;//收藏ContentID，分集不支持收藏
@property (copy ,nonatomic ) NSString       *imageLink;//海报链接
@property (copy,nonatomic  ) NSString       *multipleType;//0或者9  单集或多集
@property (copy , nonatomic) NSString       *country;//国家
@property (copy ,nonatomic ) NSString       *Actor;//演员
@property (copy ,nonatomic ) NSString       *Director;//海报链接
@property (assign,nonatomic) NSInteger      Amount;//总集数  //电视剧
@property (assign,nonatomic) NSInteger      lastEpisode;//更新到的集数
@property (copy,nonatomic  ) NSString       * lastPlayEpisode;//更新到的集数
@property (copy,nonatomic)   NSString       * lastPlayEpisodeTitle;
@property (copy,nonatomic)   NSString       *STYLE;
@property (copy,nonatomic)   NSString       *definination;
@property (copy,nonatomic  ) NSMutableArray *adList;

@property (copy ,nonatomic)NSMutableArray *seriesList;//➢	seriesList = 剧集列表，单集视频可不写
@property (assign ,nonatomic)NSInteger seriesListSeq; //➢	seriesList.seq = 剧集号（Integer）
@property (copy ,nonatomic)NSString *seriesListContentID; //➢	seriesList.contentID = 剧集视频编号（String，不可为空）
@property (copy ,nonatomic)NSString *seriesListIntro; //➢	seriesList.intro = 分集剧情介绍（String，不可为空）
@property (copy ,nonatomic)NSString *seriesListName; //➢	seriesList.name = 剧集视频名称（String，可为空）
@property (assign ,nonatomic)BOOL seriesListIsOwn; //➢	seriesList.isOwn = 剧集是否拥有播放权限(boolean，不可为空)
@property (copy ,nonatomic)NSString *seriesListDeadline; //➢	seriesList.deadline  = 截至时间（String 格式：yyyy-MM-dd HH:mm）seriesList.deadline主要针对单条购买，其他情况可以为空。
@property (assign ,nonatomic)NSInteger seriesListBreakPoint; //➢	seriesList.breakPoint = 播放时长(Integer，单位秒)
@property (assign ,nonatomic)NSInteger seriesListDuration; //➢	seriesList.duration = 影片时长(Integer，单位分钟)
@property (assign ,nonatomic)NSInteger seriesListPlayCount; //➢	seriesList.playCount = 播放次数 (Integer,单位次)
@property (assign ,nonatomic)NSInteger seriesPraiseCount; //➢	seriesList.praiseCount = 点赞次数 (Integer,单位次)
@property (assign ,nonatomic)NSInteger seriesDegradeCount; //➢	seriesList.degradeCount = 吐槽次数 (Integer,单位次)


@property (copy ,nonatomic)NSString *licensingEndTime;//版权结束时间
@property (copy ,nonatomic)NSString *orderEndTime ;//订购结束时间
@property (copy ,nonatomic)NSString *intro ;//➢	intro = 介绍（String）
//@property (copy ,nonatomic)NSString *orderEndTime ;//订购结束时间
//@property (copy ,nonatomic)NSString *orderEndTime ;//订购结束时间
//@property (copy ,nonatomic)NSString *orderEndTime ;//订购结束时间

@property (assign ,nonatomic) NSInteger duration;//视频长度
@property (assign ,nonatomic) NSInteger breakPoint;


@property (assign,nonatomic) NSInteger ret;
@property (copy,nonatomic  ) NSString  *releaseYear;//上映年份
@property (copy,nonatomic  ) NSString  *language;//语言

@property (assign,nonatomic) BOOL     isOwn;//是否拥有播放权限
@property (strong,nonatomic) NSString *deadline;//视频截止日期



@property (nonatomic, copy) NSString *contentPullID;

@property (nonatomic, copy) NSString *fullstartTimeP;
@property (nonatomic, copy) NSString *endTimeP;

@property (nonatomic, copy) NSString *liveTitleP;






@end
