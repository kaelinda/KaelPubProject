//
//  VarietyDetailIntrolView.m
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "VarietyDetailIntrolView.h"

@implementation VarietyDetailIntrolView

-(instancetype)init{
    
    self = [super init];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];

        [self setupSubViews];
    }
    
    return self;
    
}

-(void)setupSubViews{
    //顶部灰线
    _tranLineImage = [[UIImageView alloc] init];
    [_tranLineImage setBackgroundColor:[UIColor lightGrayColor]];
    
    [self addSubview:_tranLineImage];
    
    WS(wself);
    [_tranLineImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(7*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-7*kRateSize);
        make.height.mas_equalTo(1);
        
    }];
//---------------------
    
    _mostNewLabel = [[UILabel alloc] init];
    [_mostNewLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_mostNewLabel setTextAlignment:NSTextAlignmentLeft];
    [_mostNewLabel setText:@"最新:"];
    [_mostNewLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_mostNewLabel];
    
    [_mostNewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(5*kRateSize);
        make.left.mas_equalTo(6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
        
    }];
    
    //--------------------
    
    _yearLabel = [[UILabel alloc] init];
    [_yearLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_yearLabel setTextAlignment:NSTextAlignmentLeft];
    [_yearLabel setBackgroundColor:[UIColor clearColor]];

    [_yearLabel setText:@"年份:"];
    [self addSubview:_yearLabel];
    
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(5*kRateSize);
        make.left.mas_equalTo(kDeviceWidth/2+6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
    
    //--------------
    
    _typeLabel = [[UILabel alloc] init];
    [_typeLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_typeLabel setTextAlignment:NSTextAlignmentLeft];
    [_typeLabel setText:@"类型:"];
    [_typeLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_typeLabel];
    
    [_typeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(30*kRateSize);
        make.left.mas_equalTo(6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
    }];
    
//-----------------------

    _areaLabel = [[UILabel alloc] init];
    [_areaLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:13.0f]];
    [_areaLabel setTextAlignment:NSTextAlignmentLeft];
    [_areaLabel setText:@"地区:"];
    [_areaLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_areaLabel];
    
    [_areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(30*kRateSize);
        make.left.mas_equalTo(6*kRateSize+kDeviceWidth/2);
        make.width.mas_equalTo(kDeviceWidth/2-12*kRateSize);
        make.height.mas_equalTo(25*kRateSize);

        
    }];
    //-------------------------
    
    _introLabel = [[UILabel alloc] init];
    [_introLabel setNumberOfLines:30];
    _introLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_introLabel setBackgroundColor:[UIColor clearColor]];
    [_introLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:14.0f]];
    [_introLabel setText:@"简介： Bacon ipsum dolor sit amet spare ribs fatback kielbasa salami, tri-tip jowl pastrami flank short loin rump sirloin. Tenderloin frankfurter chicken biltong rump chuck filet mignon pork t-bone flank ham hock.Bacon ipsum dolor sit amet spare ribs fatback kielbasa salami, tri-tip jowl pastrami flank short loin rump sirloin. Tenderloin frankfurter chicken biltong rump chuck filet mignon pork t-bone flank ham hock.end"];
    [self addSubview:_introLabel];

    
    NSString *str = @"简介： Bacon ipsum dolor sit amet spare ribs fatback kielbasa salami, tri-tip jowl pastrami flank short loin rump sirloin. Tenderloin frankfurter chicken biltong rump chuck filet mignon pork t-bone flank ham hock.Bacon ipsum dolor sit amet spare ribs fatback kielbasa salami, tri-tip jowl pastrami flank short loin rump sirloin. Tenderloin frankfurter chicken biltong rump chuck filet mignon pork t-bone flank ham hock.end";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    _introLabel.attributedText = attributedString;
    [_introLabel sizeToFit];
    _introLabel.preferredMaxLayoutWidth = kDeviceWidth-12*kRateSize;
    
    [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(wself).offset(109*kRateSize);
        make.left.mas_equalTo(6*kRateSize);
        
    }];
//------------------------
    
    _praiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_praiseBtn addTarget:self action:@selector(praiseBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_normal"] forState:UIControlStateNormal];

    [self addSubview:_praiseBtn];
    
    [_praiseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.top.mas_equalTo(55*kRateSize);
//        make.left.mas_equalTo(87*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(24*kRateSize, 24*kRateSize));
        make.top.mas_equalTo(_typeLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(wself.mas_left).offset(87*kRateSize);
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        
    }];
    
    _praiseNumLabel = [[UILabel alloc] init];
    [_praiseNumLabel setFont:[UIFont systemFontOfSize:13.0f]];
    [_praiseNumLabel setText:@"(0)"];
    [_praiseNumLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_praiseNumLabel];
    
    [_praiseNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.centerY.mas_equalTo(_praiseBtn.mas_centerY);
//        make.left.mas_equalTo(_praiseBtn.mas_right).offset(0);
//        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24*kRateSize));
        make.centerY.mas_equalTo(_praiseBtn.mas_centerY);
        make.left.mas_equalTo(_praiseBtn.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24*kRateSize));
        
    }];
    
    
    //-------
    _stampBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_stampBtn addTarget:self action:@selector(stampBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_normal"] forState:UIControlStateNormal];
    [self addSubview:_stampBtn];
    
    [_stampBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.top.mas_equalTo(55*kRateSize);
//        make.right.mas_equalTo(-87*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(24*kRateSize, 24*kRateSize));
        make.top.mas_equalTo(_typeLabel.mas_bottom).offset(0);
        make.right.mas_equalTo(wself.mas_right).offset(-95*kRateSize);
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        
    }];
    
    _stampNumLabel = [[UILabel alloc] init];
    [_stampNumLabel setFont:[UIFont systemFontOfSize:13.0f]];
    [_stampNumLabel setText:@"(0)"];
    [_stampNumLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_stampNumLabel];
    
    [_stampNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.centerY.mas_equalTo(_stampBtn.mas_centerY);
//        make.left.mas_equalTo(_stampBtn.mas_right);
//        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24));
        make.centerY.mas_equalTo(_stampBtn.mas_centerY);
        make.left.mas_equalTo(_stampBtn.mas_right).offset(3*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 24));
    }];
    //-------------------------


    //----------------
    _VpushAndPopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_VpushAndPopBtn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
    [_VpushAndPopBtn addTarget:self action:@selector(pushAndPopBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_VpushAndPopBtn];
//    [_VpushAndPopBtn setBackgroundColor:App_selected_color];
    [_VpushAndPopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_introLabel.mas_bottom);
        make.left.mas_equalTo(wself.mas_left).offset(kDeviceWidth/2-20*kRateSize);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 27*kRateSize));
        
    }];

    //----------------
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(_VpushAndPopBtn.mas_top).offset(40*kRateSize);
        
    }];
    
    //---------introlView默认收起
    [self pushAndPopBtnAction:_VpushAndPopBtn];

    
}


-(void)praiseBtnAction:(UIButton *)btn{
    [KaelTool scaleBigAnimation:btn];

    NSLog(@"赞一个");
    if ([self.delegate respondsToSelector:@selector(varityPraiseBtnSelected:)]) {
        [self.delegate varityPraiseBtnSelected:btn];
    }
}
-(void)stampBtnAction:(UIButton *)btn{
    [KaelTool scaleBigAnimation:btn];

    NSLog(@"踩一个");
    if ([self.delegate respondsToSelector:@selector(varityStampBtnSelected:)]) {
        [self.delegate varityStampBtnSelected:btn];
    }
}

-(void)VpraiseSuccecc:(BOOL)isSuccecc{
    if (isSuccecc) {
        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_pressed"] forState:UIControlStateNormal];
        
    }
    
}

-(void)VstampSuccecc:(BOOL)isSuccecc{
    if (isSuccecc) {
        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_pressed"] forState:UIControlStateNormal];
    }
    
}



-(void)loadIntrolWith:(NSString *)introl andYear:(NSString *)year andType:(NSString *)type andArea:(NSString *)area andMostNew:(NSString *)mostNew andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stamNum{
    if (introl.length>0) {
        
        CGFloat rowSpace = 6;
        if (kDeviceWidth == 320) {
            rowSpace = 3;
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:introl];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:rowSpace];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [introl length])];
        _introLabel.attributedText = attributedString;
        [_introLabel sizeToFit];
        
    }
    
    if (area.length>0) {
        [_areaLabel setText:[NSString stringWithFormat:@"地区：%@",area]];
    }
    if (type.length>0) {
        [_typeLabel setText:[NSString stringWithFormat:@"类型：%@",type]];
        
    }
    if (year.length>0) {
        [_yearLabel setText:[NSString stringWithFormat:@"年份：%@",year]];
        
    }
    if (mostNew.length>0) {
        [_mostNewLabel setText:[NSString stringWithFormat:@"最新：%@",mostNew]];
        
    }
    if (praiseNum.length>0) {
        [_praiseNumLabel setText:[NSString stringWithFormat:@"(%@)",praiseNum]];
    }
    if (stamNum.length>0) {
        [_stampNumLabel setText:[NSString stringWithFormat:@"(%@)",stamNum]];
        
    }


}
-(void)pushAndPopBtnAction:(UIButton *)btn{

    NSLog(@"收起还是展开啊？？");
    WS(wself);
    [_introLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(wself).offset(89*kRateSize);
        make.left.mas_equalTo(6*kRateSize);
        make.width.mas_equalTo(kDeviceWidth-12*kRateSize);

        if (_introLabel.hidden) {

//            if (isAfterIOS7) {
//
//            }else{
//                
//            }


        }else{

            make.height.mas_equalTo(@0);
        }
        
    }];
    //--------------
    
    if (_introLabel.hidden) {
        
        [_VpushAndPopBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_introLabel.mas_bottom).offset(1*kRateSize);
            make.left.mas_equalTo(kDeviceWidth/2-20*kRateSize);
            make.width.mas_equalTo(40*kRateSize);
            make.height.mas_equalTo(27*kRateSize);
        }];
    }else{
        [_VpushAndPopBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_introLabel.mas_top).offset(1*kRateSize);
            make.left.mas_equalTo(kDeviceWidth/2-20*kRateSize);
            make.width.mas_equalTo(40*kRateSize);
            make.height.mas_equalTo(27*kRateSize);
            
        }];
        
    }
    

    
    
//-------------------
    if (_introLabel.hidden) {
        [btn setImage:[UIImage imageNamed:@"ic_vod_filter_fold"] forState:UIControlStateNormal];
        
    }else{
        [btn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
        
    }
    
 //--------------------
    _introLabel.hidden = !_introLabel.hidden;

    if ([self.delegate respondsToSelector:@selector(varityPushAndPopBtnSelected:)]) {
        [self.delegate varityPushAndPopBtnSelected:btn];
    }


}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
