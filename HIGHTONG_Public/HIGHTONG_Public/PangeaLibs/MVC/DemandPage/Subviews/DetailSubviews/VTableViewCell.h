//
//  VTableViewCell.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *backImage;
@property (nonatomic,strong) UILabel *VTitleLabel;


@end
