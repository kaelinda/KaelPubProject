//
//  EpisodeSelectedView.h
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//这个是 剧集选择的 模块

#import <UIKit/UIKit.h>

@class EpisodeSelectedView;
@protocol EpisodeViewDelegate <NSObject>

-(void)EpisodeViewBtnSelectedAtIndex:(NSInteger)index;

@end

@interface EpisodeSelectedView : UIView

@property (nonatomic,assign) id<EpisodeViewDelegate> delegate;

@property (nonatomic,assign) NSInteger episodeNum;

@property (nonatomic,strong) UIView *lightGrayView;
@property (nonatomic,strong) UIView *whiteBackView;
@property (nonatomic,strong) UILabel *episodeTitleLabel;//“剧集” 这俩字的 标题
@property (nonatomic,strong) UILabel *episodeNumLabel;//剧集数  比如  共多少集 更新了多少集
@property (nonatomic,strong) UIImageView *lineImageView;//横线

@property (nonatomic,strong) UIScrollView *numScrollView;//剧集分段按钮
@property (nonatomic,strong) UIView *numScrolContentView;

@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,strong) NSMutableArray *freeItemsArr;
@property (nonatomic,strong) NSMutableArray *truthEpisodeArr;


/**
 *  这只需要隐藏徽章的 位置
 *
 *  @param index <#index description#>
 */
-(void)hiddenBadgeAtIndex:(NSInteger)index;
-(void)setSelectedBtnAtIndex:(NSInteger)index;


/**
 *  加载按钮个数 剧集数（滑动视图）
 *
 *  @param episodNum 剧集数
 */
-(void)loadBtnWithEpisodNum:(NSInteger)episodNum;

/**
 *  加载总剧集数 和 已更新剧集数
 *
 *  @param allNum    总剧集数
 *  @param updateNum 已更新剧集数
 */
-(void)loadAllEpisodNumWithAllNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum;//推荐使用这个

-(void)reloadAllEpisodeNumWithAllNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum andEpisodeArr:(NSArray *)episodeArr;



@end
