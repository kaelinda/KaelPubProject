//
//  DetailTopBarView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DetailTopBarView.h"

@implementation DetailTopBarView




-(instancetype)initTopBarWithShareBlock:(ReturnBlock)shareBlock andWithFavBlock:(FavReturnBlock)favBlock{
    self = [super init];
    if (self) {
        
        WS(wself);
        [self setBackgroundColor:[UIColor colorWithRed:0.21 green:0.21 blue:0.21 alpha:1]];
        //************ 播放量 ***********
        _playNumLabel = [KaelTool setlabelPropertyWithBackgroundColor:[UIColor colorWithRed:0.21 green:0.21 blue:0.21 alpha:1] andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0xffffff) andFont:[UIFont systemFontOfSize:13*kRateSize]];
        
        [self addSubview:_playNumLabel];
        
        [_playNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
            make.height.mas_equalTo(30*kRateSize);
            make.width.mas_equalTo(100*kRateSize);
            make.centerY.mas_equalTo(wself.mas_centerY);
            
        }];
        
        _favBtn = [[UIButton_Block alloc] init];
        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateHighlighted];
        
        __weak UIButton_Block*WFavBtn  = _favBtn;
        _favBtn.Click = ^(UIButton_Block *btn,NSString *name){
            if (WFavBtn.selected) {
                favBlock(NO);
//                [wself resetFavorate:NO];

            }else{
                favBlock(YES);
//                [wself resetFavorate:YES];

            }
//            WFavBtn.selected = !WFavBtn.selected;
        };
        [self addSubview:_favBtn];
        
        [_favBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(46*kRateSize, 46*kRateSize));
            make.centerY.mas_equalTo(wself.centerY);
            make.right.mas_equalTo(wself.mas_right).offset(7);
        }];
        
        _shareBtn = [[UIButton_Block alloc] init];
        [_shareBtn setImage:[UIImage imageNamed:@"btn_detail_share_selected"] forState:UIControlStateNormal];
        [_shareBtn setImage:[UIImage imageNamed:@"btn_detail_share"] forState:UIControlStateHighlighted];
        
        _shareBtn.Click = ^(UIButton_Block *btn,NSString *name){
            shareBlock();
        };
        [self addSubview:_shareBtn];
        
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(46*kRateSize, 46*kRateSize));
            make.centerY.mas_equalTo(wself.centerY);
            make.right.mas_equalTo(_favBtn.mas_left);
        }];
        
        
        
        
        
        
    }
    return self;
}


-(void)resetFavorate:(BOOL)isFavorated{
    if (isFavorated) {
        _favBtn.selected = YES;

        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
//        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateHighlighted];

    }else{
        _favBtn.selected = NO;

        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
//        [_favBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateHighlighted];
    }
   
}
-(void)resetPlayNumWith:(NSString *)playNum{
    if ([playNum integerValue]>10000) {
        playNum = [NSString stringWithFormat:@"%.1f万",[playNum floatValue]/10000.0];
    }
    
    _playNumLabel.text = [NSString stringWithFormat:@"播放：%@次",playNum];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
