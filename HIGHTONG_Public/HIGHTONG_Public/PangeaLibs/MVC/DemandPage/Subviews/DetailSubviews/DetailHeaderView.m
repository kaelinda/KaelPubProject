//
//  DetailHeaderView.m
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DetailHeaderView.h"
#import "UIImageView+WebCache.h"
@implementation DetailHeaderView


-(instancetype)init{
    self = [super init];
    
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self initSubViews];
    }
    return self;

}

-(void)initSubViews{

    //-------eventImageview
    _eventImageView = [[UIImageView alloc] init];
    _eventImageView.image = [UIImage imageNamed:@"bg_msgbox"];
//    [_eventImageView setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:_eventImageView];
    WS(wself);
    [_eventImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.mas_top).offset((6+3)*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(6*kRateSize);
//        make.size.width.mas_equalTo(50*kRateSize);
//        make.size.height.mas_equalTo(56*kRateSize);
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 60*kRateSize));
        
        
    }];
 
    //----------_eventNameLabel
    _eventNameLabel = [[UILabel alloc] init];
    [_eventNameLabel setBackgroundColor:[UIColor clearColor]];
    _eventNameLabel.text = @"麻衣神相(尸鬼宗)";
    [_eventNameLabel setTextAlignment:NSTextAlignmentLeft];//文字居左显示
    _eventNameLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    _eventNameLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:15.0f];

    [self addSubview:_eventNameLabel];
    
    [_eventNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset((9+3)*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(64*kRateSize);
        make.size.mas_equalTo(CGSizeMake(150*kRateSize, 20*kRateSize));
    }];
    
    //-----------_playNumImage
    _playNumImage = [[UIImageView alloc] init];
//    _playNumImage.contentMode = UIViewContentModeScaleAspectFit;
    [_playNumImage setBackgroundColor:[UIColor clearColor]];
    [_playNumImage setImage:[UIImage imageNamed:@"ic_vod_player_playnum"]];
    [self addSubview:_playNumImage];
    
    [_playNumImage mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(_eventImageView.mas_right).offset((8+2)*kRateSize);
        make.top.mas_equalTo(_eventNameLabel.mas_bottom).offset(8*kRateSize);
        make.size.mas_equalTo(CGSizeMake(20*kRateSize, 20*kRateSize));
        
    }];
    
    
    //
    _playNumLabel = [[UILabel alloc] init];
    _playNumLabel.text = @"100";
    [_playNumLabel setBackgroundColor:[UIColor clearColor]];
    _playNumLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    _playNumLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:15.0f];

    [self addSubview:_playNumLabel];
    
    [_playNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_eventNameLabel.mas_bottom).offset((4+3)*kRateSize);
        make.left.mas_equalTo(_playNumImage.mas_right).offset(9*kRateSize);
        make.size.mas_equalTo(CGSizeMake(100*kRateSize, 20*kRateSize));
        
        
        
    }];
    
    _favorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_favorBtn setBackgroundColor:[UIColor clearColor]];
    [_favorBtn setImage:[UIImage imageNamed:@"star_22"] forState:UIControlStateNormal];
    [_favorBtn addTarget:self action:@selector(favorAction:)  forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_favorBtn];
    
    [_favorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(11*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-52*kRateSize);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize, 30*kRateSize));

        
    }];
    

    
    _favorLabel = [[UILabel alloc] init];
    [_favorLabel setText:@"收藏"];
    [_favorLabel setBackgroundColor:[UIColor clearColor]];
    [_favorLabel setTextAlignment:NSTextAlignmentCenter];
//    [_favorLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [_favorLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:14.0f]];
    
    [self addSubview:_favorLabel];
    
    [_favorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_favorBtn.mas_bottom).offset(2);
        make.centerX.mas_equalTo(_favorBtn.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
        
    }];
    
    
    

    
    
    
}



-(void)favorAction:(UIButton *)btn{
    [KaelTool scaleBigAnimation:btn];
    NSLog(@"哎呀  竟然收藏我了！抓紧的 代理传值给主界面~");
    if ([self.delegate respondsToSelector:@selector(detailHeaderViewFavorBtnSelected:)]) {
        [self.delegate detailHeaderViewFavorBtnSelected:btn];
    }


}

-(void)loadEventImage:(NSString *)imageUrl andName:(NSString *)name andPlayNum:(NSString *)playNum{

    if (imageUrl.length>0) {
        [_eventImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageUrl)] placeholderImage:nil];

    }
    if (name.length>0) {
        [_eventNameLabel setText:name];

    }
    if (playNum.length>0) {
        [_playNumLabel setText:[NSString stringWithFormat:@"%@",playNum]];
        if ([playNum integerValue]==0) {
            [_playNumLabel setText:[NSString stringWithFormat:@"%@",playNum]];
        }
    }
    if(playNum.length>5){
    
        [_playNumLabel setText:[NSString stringWithFormat:@"%ld万",[playNum integerValue]/10000]];

    }


}

-(void)loadEventNameWith:(NSString *)name{

    [_eventNameLabel setText:name];

}
-(void)loadEventImageWith:(NSString *)imageUrl{

    [_eventImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageUrl)] placeholderImage:nil];

}

-(void)loadPlayNumLabelTextWith:(NSString *)playNum{

    [_playNumLabel setText:[NSString stringWithFormat:@"%@",playNum]];

}

-(void)favorSucceed{

    NSLog(@"收藏成功~");
    [_favorBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
    [_favorLabel setText:@"已收藏"];

}


-(void)cancellledFavor{

    NSLog(@"取消收藏！");
    [_favorBtn setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
    [_favorLabel setText:@"收藏"];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
