//
//  DetailRecommendView.h
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailRecommandViewDelegate <NSObject>

-(void)recommandItemsSelectedAtIndex:(NSInteger)index;

@end



@interface DetailRecommendView : UIView

@property (nonatomic,assign) id<DetailRecommandViewDelegate> delegate;


@property (nonatomic,strong) UILabel *recommandTitleLabel;//“相关推荐” 这四个字的 标题
@property (nonatomic,strong) UIImageView *HLineImageView;//竖线
@property (nonatomic,strong) UILabel *recommandNumLabel;//剧集数  比如  共多部
@property (nonatomic,strong) UIImageView *VLineImageView;//横线
@property (nonatomic,strong) UIScrollView *recommandItemsScrollView;//推荐元素显示图 滑动视图




-(void)loadRecommandNum:(NSString *)recommandNum;

/**
 *  加载推荐元素
 *
 *  @param recommandItems 推荐元素（这是一个数组）
 */
-(void)loadRecommandItems:(NSArray *)recommandItems;





@end
