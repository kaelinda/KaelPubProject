//
//  VarietySelecetedView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol VSelectedViewDelegate <NSObject>

-(void)VTableViewSelectedIndex:(NSInteger)index;

@end


@interface VarietySelecetedView : UIView<UITableViewDelegate,UITableViewDataSource>
{

    CGFloat cellHeigh;

}

@property (nonatomic,assign)id<VSelectedViewDelegate>delegate;

@property (nonatomic,strong) UIView *lightGrayView;

@property (nonatomic,strong) UILabel *episodeTitleLabel;//“剧集” 这俩字的 标题
@property (nonatomic,strong) UILabel *episodeNumLabel;//剧集数  比如  共多少集 更新了多少集
@property (nonatomic,strong) UIImageView *lineImageView;//横线
@property (nonatomic,strong) UITableView *VTableView;//剧集选择view
@property (nonatomic,strong) UIScrollView *numScrollView;//剧集分段按钮
@property (nonatomic,strong) UIView *numScrolContentView;

@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,assign) NSInteger episodeNum;

@property (nonatomic,strong)NSMutableArray *VArray;//tableview的数据源


-(void)reloadVarityDateWith:(NSArray *)varityArr andAllEpisodNum:(NSInteger)allNum andUpdateNum:(NSString *)updateNum;

@end
