//
//  DetailRecommendView.m
//  HIGHTONG
//
//  Created by Kael on 15/8/9.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DetailRecommendView.h"
#import "ProgramView.h"
@implementation DetailRecommendView

-(instancetype)init{

    self = [super init];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];

        [self setupSubviews];
        
    }
    
    return self;
}

-(void)setupSubviews{
    WS(wself);

    
    _VLineImageView = [[UIImageView alloc] init];
    [_VLineImageView setBackgroundColor:[UIColor orangeColor]];
    _VLineImageView.hidden = YES;
    [self addSubview:_VLineImageView];
    
    [_VLineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(5);
        make.left.mas_equalTo(wself.mas_left).offset(6);
        make.size.mas_equalTo(CGSizeMake(0, 17*kRateSize));
        
        
    }];
    
    
    _recommandTitleLabel = [[UILabel alloc] init];
    [_recommandTitleLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];

    [_recommandTitleLabel setTextAlignment:NSTextAlignmentLeft];
    [_recommandTitleLabel setBackgroundColor:[UIColor clearColor]];

    [_recommandTitleLabel setText:@"相关推荐"];
    [self addSubview:_recommandTitleLabel];
    [_recommandTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top);
        make.left.mas_equalTo(wself.mas_left).offset(8*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2-10*kRateSize);
        make.height.mas_equalTo(27*kRateSize);
        
    }];
 //-----------------
    
    _recommandNumLabel = [[UILabel alloc] init];
    [_recommandNumLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
    [_recommandNumLabel setTextAlignment:NSTextAlignmentRight];
    [_recommandNumLabel setText:@"共*部"];
    [_recommandNumLabel setBackgroundColor:[UIColor clearColor]];

    [self addSubview:_recommandNumLabel];
    
    [_recommandNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(8*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-15*kRateSize);
        make.width.mas_equalTo(kDeviceWidth/2);
        
        
    }];
//----------------

    //竖线就先算了吧
    _HLineImageView = [[UIImageView alloc] init];
    [_HLineImageView setBackgroundColor:UIColorFromRGB(0xaaaaaa)];
    _HLineImageView.alpha = 0.5;
    _HLineImageView.hidden = YES;
    [wself addSubview:_HLineImageView];
    
    [_HLineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(25*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-5*kRateSize);
        make.bottom.mas_equalTo(wself.mas_top).offset(25*kRateSize);
        
    }];
    
//------------

//初始化的时候不加载 items
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.bottom.mas_equalTo(wself.mas_top).offset(35*kRateSize);
        
    }];
    
    
    [self loadRecommandItems:nil];


}



-(void)loadRecommandNum:(NSString *)recommandNum{

    [_recommandNumLabel setText:[NSString stringWithFormat:@"共%@部",recommandNum]];


}


-(void)loadRecommandItems:(NSArray *)recommandItems{
    
    NSInteger recommandNum = [recommandItems count];
    [_recommandNumLabel setText:[NSString stringWithFormat:@"共%lu部",(long)recommandNum]];

    
    if ([recommandItems count]<=0) {
        return;
    }
    
//-------加载之前要把之前的 btn remove 掉
    NSArray *subViews = [self subviews];
    
    for (int i=0; i<subViews.count; i++) {
        
        if ([[subViews objectAtIndex:i] isKindOfClass:[ProgramView class]]) {
            ProgramView *btn = [[ProgramView alloc] init];
            btn = (ProgramView *)[subViews objectAtIndex:i];
            [btn removeFromSuperview];
        }
        
        
    }
//-------end 加载之前要把之前的 btn remove 掉
    
//----------------重新创建btn-----------------
    
    if (recommandItems.count<=0) {
        return;
    }else{

        NSInteger itmesNum =  recommandItems.count;
        CGSize itemsSzie = CGSizeMake(((kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight)-(6+8)*kRateSize)/3, 170*kRateSize);
    
        NSInteger row = (itmesNum-1)/3;
        ProgramView *lastItemsBtn = [[ProgramView alloc] init];

        for (int i=0; i<=row; i++) {
            
            for (int j=0; j <= 2; j++) {
                
                NSDictionary *infoDic;
                if (recommandItems.count>i*3+j) {
                   infoDic  = [[NSDictionary alloc] initWithDictionary:[recommandItems objectAtIndex:i*3+j]];
  
                }
                
                ProgramView *itemsBtn = [[ProgramView alloc]init];
                [itemsBtn setBackgroundColor:[UIColor whiteColor]];
//                [itemsBtn.timeTitle setBackgroundColor:[UIColor clearColor]];

                itemsBtn.tag = i*3+j+1;

                WS(wself);
                __weak  ProgramView *WitemsBtn = itemsBtn;
                WitemsBtn.tag = itemsBtn.tag;
                
                itemsBtn.block = ^(UIButton_Block* btn)
                {
                    //点击的响应事件
                    NSLog(@"已经点击了 第%ld个 btn",(long)WitemsBtn.tag);
                    if ([wself.delegate respondsToSelector:@selector(recommandItemsSelectedAtIndex:)]) {
                        [wself.delegate recommandItemsSelectedAtIndex:WitemsBtn.tag];
                    }
                    
                };
                
//-----------给元素赋值
                NSString *name = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"name"]];
                NSString *subTitle = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"subTitle"]];
                NSString *imageLink = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"imageLink"]];
                NSString *lastEpisode = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"lastEpisode"]];
//                NSString *name = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"name"]];
//                NSString *name = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"name"]];

                [itemsBtn.subTitle setBackgroundColor:[UIColor clearColor]];
                [itemsBtn.title setBackgroundColor:[UIColor clearColor]];
                [itemsBtn.timeTitle setBackgroundColor:[UIColor clearColor]];
                
                itemsBtn.dic = [NSDictionary dictionaryWithDictionary:infoDic];
                [itemsBtn updata];
                if (name.length>0&&(![name isEqualToString:@"(null)"])) {
                    [itemsBtn.subTitle setText:name];
 
                }else{
                [itemsBtn.subTitle setText:@""];
                }
                if (subTitle.length>0&&(![subTitle isEqualToString:@"(null)"])) {
                    [itemsBtn.timeTitle setText:subTitle];
 
                }else{
                    [itemsBtn.timeTitle setText:@""];

                }
                if (imageLink.length>0) {
                    
                    [itemsBtn.photo sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)] placeholderImage:[UIImage imageNamed:@"bg_vod_gridview_item_onloading"]];
  
                }
                if (lastEpisode.length>0&&(![lastEpisode isEqualToString:@"(null)"])) {
                    [itemsBtn.title setText:[NSString stringWithFormat:@"已更新至%@集",lastEpisode]];
  
                }else{
                    [itemsBtn.title setText:[NSString stringWithFormat:@""]];
                    [itemsBtn.title setHidden:YES];

                }
//-----------给元素赋值 end
                
                [self addSubview:itemsBtn];
                
                [itemsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                   
                    make.top.mas_equalTo(wself.mas_top).offset(30*kRateSize+i*(itemsSzie.height+5*kRateSize));
                    make.left.mas_equalTo(wself.mas_left).offset(3*kRateSize*(j+1)+(itemsSzie.width)*j);
                    make.size.mas_equalTo(itemsSzie);
                    
                }];
                
                
                if (itemsBtn.tag == itmesNum) {
                    lastItemsBtn = itemsBtn;
                    
                    NSLog(@"lastItemsBtn  %ld",(long)lastItemsBtn.tag);
                    break;
                 }
            
            }
            
        }
        
        
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
           
            make.bottom.mas_equalTo(lastItemsBtn.mas_bottom).offset(0*kRateSize);
            
        }];
        
        
    }
    
  
}


-(void)itemsSelectedWithIndex:(NSInteger)index{

    NSLog(@"选中 第%ld个推荐节目了~~抓紧传值给主界面吧~",(long)index);
    
    if ([self.delegate respondsToSelector:@selector(recommandItemsSelectedAtIndex:)]) {
        
        [self.delegate recommandItemsSelectedAtIndex:index];
    }
    
    
    
    

}






















/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
