//
//  EpisodeButton.h
//  HIGHTONG
//
//  Created by Kael on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//这个是 剧集选择的 按钮~

#import <UIKit/UIKit.h>

@class EpisodeButton;
@protocol EpisodeBtnDelegate <NSObject>

-(void)episodeBtnDelegate:(EpisodeButton *)EBtn;

@end



@interface EpisodeButton : UIButton

@property (nonatomic, copy) NSString *badgeValue;
@property (nonatomic,assign)id<EpisodeBtnDelegate> delegate;

/**
 *  设置徽章（最多俩字）
 *
 *  @param EBadgeValue 徽章内容
 */
-(void)setEBadgeValue:(NSString *)EBadgeValue;

/**
 *  隐藏徽章
 *
 *  @param isHidden 是否隐藏徽章
 */
-(void)hiddenBadgeView:(BOOL)isHidden;

@end
