//
//  DetailPropertyView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/15.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemandModel.h"
#import "HYBJSONModel.h"
@interface DetailPropertyView : UIView
{
    
    BOOL _hiddenProperty;

}

@property (nonatomic,strong) NSArray *sortArr;
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,strong) UILabel *introlLabel;
@property (nonatomic,strong) DemandModel *demandModel;
@property (nonatomic,strong) UILabel *lastLabel;
@property (nonatomic,strong) UILabel *actorLabel;


-(void)setPropertyHideen:(BOOL)hidden;


-(void)reloadProperTypeViewWithModel:(DemandModel *)demandMoel;

@end
