//
//  NewIntrolView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/9.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "NewIntrolView.h"

@implementation NewIntrolView




-(instancetype)init{

    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

-(void)initSubViews{
    WS(wself);
    //************ 节目名称 *************
    _programNameLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x333333) andFont:[UIFont systemFontOfSize:15*kRateSize]];

    [self addSubview:_programNameLabel];
    [_programNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself).offset(5*kRateSize);
        make.top.mas_equalTo(wself).offset(10*kRateSize);
        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 20*kRateSize));
    }];
    
    //*********** 主演演员 *************
    _actorLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:11*kRateSize]];
    [self addSubview:_actorLabel];
    [_actorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5*kRateSize);
        make.top.mas_equalTo(_programNameLabel.mas_bottom).offset(5*kRateSize);
        make.right.mas_equalTo(wself.right).offset(-5*kRateSize);
        make.height.mas_equalTo(15*kRateSize);
    }];
    
    //************ 导演 ***************
    _directorLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_directorLabel];
    [_directorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_actorLabel.mas_bottom).offset(5*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
        make.width.mas_equalTo((kDeviceWidth-10*kRateSize)/2);
        make.height.mas_equalTo(15*kRateSize);
    }];
    
    //************ 类型 ***************
    _programTypeLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_programTypeLabel];
    [_programTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_directorLabel.mas_bottom).offset(5*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);
        make.width.mas_equalTo((kDeviceWidth-10*kRateSize)/2);
        make.height.mas_equalTo(15*kRateSize);
    }];
    
    //************ 年份 ***************
    _yearsLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_yearsLabel];
    [_yearsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.mas_right).offset(5*kRateSize);
        make.width.mas_equalTo((kDeviceWidth-10*kRateSize)*2/5);
        make.height.mas_equalTo(15*kRateSize);
        make.centerY.mas_equalTo(wself.directorLabel.mas_centerY);
    }];
    
    //************ 地区 ****************
    _areaLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x666666) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_areaLabel];
    [_areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.mas_right).offset(5*kRateSize);
        make.width.mas_equalTo((kDeviceWidth-10*kRateSize)*2/5);
        make.height.mas_equalTo(15*kRateSize);
        make.centerY.mas_equalTo(wself.programTypeLabel.mas_centerY);
    }];
    
    
    //*********** 详情介绍 **********
    _introLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x999999) andFont:[UIFont systemFontOfSize:14]];
    [_introLabel setNumberOfLines:30];
    _introLabel.hidden = YES;
    _introLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:_introLabel];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];
    
    [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.equalTo(wself.programTypeLabel.mas_bottom).offset(5*kRateSize);
        make.left.equalTo(wself).offset(5*kRateSize);
        make.right.mas_equalTo(wself).offset(-5*kRateSize);
        make.height.mas_equalTo(0);
    }];
    
    //*********** 属性视图 **********
    _propertyView = [[DetailPropertyView alloc] init];
    [_propertyView setBackgroundColor:App_white_color];
    [self addSubview:_propertyView];
    
    [_propertyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_programNameLabel.mas_bottom).offset(5*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-5*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(5*kRateSize);

    }];
    
    
    //*********** 点赞按钮 **********
    _praiseBtn = [[UIButton_Block alloc] init];
    [self praiseSucceed:NO];
    [self addSubview:_praiseBtn];
    
    [_praiseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        make.left.mas_equalTo(wself.mas_left).offset(85*kRateSize);
        make.top.mas_equalTo(wself.propertyView.mas_bottom).offset(-5);
    }];
    _praiseBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.praiseBlock) {
            wself.praiseBlock(btn);
        }
    };
    //*********** 点赞量 **********
    _praiseNumLabel = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x999999) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_praiseNumLabel];
    [_praiseNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 32*kRateSize));
        make.left.mas_equalTo(wself.praiseBtn.mas_right);
        make.centerY.mas_equalTo(wself.praiseBtn);
    }];
    //*********** 点踩按钮 **********
    _stampBtn = [[UIButton_Block alloc] init];
    [self stampSucceed:NO];
    [self addSubview:_stampBtn];
    
    [_stampBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32*kRateSize, 32*kRateSize));
        make.right.mas_equalTo(wself.mas_right).offset(-115*kRateSize);
        make.top.mas_equalTo(wself.praiseBtn.mas_top);
    }];
    _stampBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.stampBlock) {
            wself.stampBlock(btn);
        }
    };
    //*********** 点踩量 **********
    _stampNumLabel  = [KaelTool setlabelPropertyWithBackgroundColor:CLEARCOLOR andNSTextAlignment:NSTextAlignmentLeft andTextColor:UIColorFromRGB(0x999999) andFont:[UIFont systemFontOfSize:13]];
    [self addSubview:_stampNumLabel];
    [_stampNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 32*kRateSize));
        make.left.mas_equalTo(wself.stampBtn.mas_right);
        make.centerY.mas_equalTo(wself.stampBtn);
    }];

    //*********** 伸展按钮 **********
    _pushBtn = [[UIButton_Block alloc] init];
    [_pushBtn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
    [self addSubview:_pushBtn];
    
    [_pushBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.mas_top);
        make.right.mas_equalTo(wself.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(35*kRateSize, 30*kRateSize));
    }];
    
    _pushBtn.Click = ^(UIButton_Block *btn,NSString *str){
        if (btn.selected) {
            btn.selected = NO;
            [wself.pushBtn setImage:[UIImage imageNamed:@"ic_vod_filter_expand"] forState:UIControlStateNormal];
            [wself pushBtnSelectedWithisPush:NO];
        }else{
            btn.selected = YES;
            [wself.pushBtn setImage:[UIImage imageNamed:@"ic_vod_filter_fold"] forState:UIControlStateNormal];
            [wself pushBtnSelectedWithisPush:YES];
        }
    };
    
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.praiseBtn.mas_bottom).offset(10);
        
    }];
    _actorLabel.hidden = YES;
    _directorLabel.hidden = YES;
    _yearsLabel.hidden = YES;
    _areaLabel.hidden = YES;
    _programTypeLabel.hidden = YES;
}


-(void)pushBtnSelectedWithisPush:(BOOL)isPush{
    WS(wself);
    if (_programNameLabel.text.length==0) {
        return;
    }
    _actorLabel.hidden = YES;
    _directorLabel.hidden = YES;
    _yearsLabel.hidden = YES;
    _areaLabel.hidden = YES;
    _programTypeLabel.hidden = YES;
    
    if (_introLabel.hidden) {
            
        [_introLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wself.programTypeLabel.mas_bottom).offset(5*kRateSize);
            make.left.equalTo(wself).offset(5*kRateSize);
            make.right.mas_equalTo(wself).offset(-5*kRateSize);
            make.height.mas_equalTo(0);

        }];
        [_propertyView setPropertyHideen:NO];
        
        
    }else{
        
        [_introLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(wself.programTypeLabel.mas_bottom).offset(5*kRateSize);
            make.left.equalTo(wself).offset(5*kRateSize);
            make.right.mas_equalTo(wself).offset(-5*kRateSize);
            make.height.mas_equalTo(0);
        }];
        [_propertyView setPropertyHideen:YES];

    }
    _introLabel.hidden = !_introLabel.hidden;

}


-(void)setIntrolInfoWithName:(NSString *)name andActor:(NSString *)actor andDirector:(NSString *)director andType:(NSString *)type andYears:(NSString *)years andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum andIntrol:(NSString *)introl{
    return;
    if (isEmptyStringOrNilOrNull(name)) {
        name = @"";
    }
    if (isEmptyStringOrNilOrNull(actor)) {
        actor = @"";
    }
    if (isEmptyStringOrNilOrNull(director)) {
        director = @"";
    }
    if (isEmptyStringOrNilOrNull(type)) {
        type = @"";
    }
    if (isEmptyStringOrNilOrNull(years)) {
        years = @"";
    }
    if (isEmptyStringOrNilOrNull(area)) {
        area = @"";
    }
    
    _programNameLabel.text = [NSString stringWithFormat:@"%@",name];
    _actorLabel.attributedText = [self getDiffirentColorstringWithString:[NSString stringWithFormat:@"演员：%@",actor]];
    _directorLabel.attributedText = [self getDiffirentColorstringWithString:[NSString stringWithFormat:@"导演：%@",director]];
    _programTypeLabel.attributedText = [self getDiffirentColorstringWithString:[NSString stringWithFormat:@"类型：%@",type]];
    _yearsLabel.attributedText = [self getDiffirentColorstringWithString:[NSString stringWithFormat:@"年份：%@",years]];
    _areaLabel.attributedText = [self getDiffirentColorstringWithString:[NSString stringWithFormat:@"地区：%@",area]];


    if (NotEmptyStringAndNilAndNull(introl)) {
        
        CGFloat rowSpace = 6;
        if (kDeviceWidth == 320) {
            rowSpace = 3;
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:introl];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:rowSpace];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [introl length])];
        _introLabel.attributedText = attributedString;
        [_introLabel sizeToFit];
        
    }
    if (praiseNum.length>0) {
        _praiseNumLabel.text = [NSString stringWithFormat:@"(%@)",praiseNum];
    }else{
        _praiseNumLabel.text = @"(0)";
    }
    if (stampNum.length>0) {
        _stampNumLabel.text = [NSString stringWithFormat:@"(%@)",stampNum];
    }else{
        _stampNumLabel.text = @"(0)";
    }

}

-(void)setIntroInfoWithModel:(DemandModel *)demandModel{
    _programNameLabel.text = [NSString stringWithFormat:@"%@",demandModel.NAME];
    [self setPraiseNum:[NSString stringWithFormat:@"%ld",demandModel.praiseCount] andStampNum:[NSString stringWithFormat:@"%ld",demandModel.degradeCount]];
    [_propertyView reloadProperTypeViewWithModel:demandModel];
}

-(NSMutableAttributedString *)getDiffirentColorstringWithString:(NSString *)str{
    NSMutableAttributedString *Kstr = [[NSMutableAttributedString alloc] initWithString:str];
    [Kstr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x333333) range:NSMakeRange(0,2)];
    return Kstr;
}
-(void)setPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum{
    if (praiseNum.length>0) {
        _praiseNumLabel.text = [NSString stringWithFormat:@"(%@)",praiseNum];
    }else{
        _praiseNumLabel.text = @"(0)";
    }
    if (stampNum.length>0) {
        _stampNumLabel.text = [NSString stringWithFormat:@"(%@)",stampNum];
    }else{
        _stampNumLabel.text = @"(0)";
    }

}

-(void)resetPraiseNum:(NSString *)praiseNum{
    if (praiseNum.length>0) {
        _praiseNumLabel.text = [NSString stringWithFormat:@"(%@)",praiseNum];
    }else{
        _praiseNumLabel.text = @"(0)";
    }

}

-(void)resetStampNum:(NSString *)stampNum{
    if (stampNum.length>0) {
        _stampNumLabel.text = [NSString stringWithFormat:@"(%@)",stampNum];
    }else{
        _stampNumLabel.text = @"(0)";
    }
}

-(void)praiseSucceed:(BOOL)isSucceed{
    if (isSucceed) {
        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_pressed"] forState:UIControlStateNormal];
//        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_normal"] forState:UIControlStateHighlighted];

    }else{
        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_normal"] forState:UIControlStateNormal];
//        [_praiseBtn setImage:[UIImage imageNamed:@"btn_vod_praise_pressed"] forState:UIControlStateHighlighted];

    }
   
}

-(void)stampSucceed:(BOOL)isSucceed{
    if (isSucceed) {
        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_pressed"] forState:UIControlStateNormal];
//        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_normal"] forState:UIControlStateHighlighted];

    }else
    {
        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_normal"] forState:UIControlStateNormal];
//        [_stampBtn setImage:[UIImage imageNamed:@"btn_vod_shits_pressed"] forState:UIControlStateHighlighted];

    }

}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
