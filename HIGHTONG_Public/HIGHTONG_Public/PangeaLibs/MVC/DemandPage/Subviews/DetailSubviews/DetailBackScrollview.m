//
//  DetailBackScrollview.m
//  HIGHTONG
//
//  Created by Kael on 15/8/12.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DetailBackScrollview.h"
#import "VarietySelecetedView.h"
#import "MyGesture.h"

@implementation DetailBackScrollview


-(instancetype)init{
    self = [super init];
    
    if (self) {

       
        
        [self setupSubviews];
    }

    return self;
}

-(void)setupSubviews{
    _scrollContentView = UIView.new;
    [_scrollContentView setBackgroundColor:App_background_color];
    _scrollContentView.userInteractionEnabled = YES;
    [self addSubview:_scrollContentView];
    
    WS(wself);
    [_scrollContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
        make.width.equalTo(wself);
        
    }];
    
    _BufferLabel = [[UILabel alloc] init];
    _BufferLabel.backgroundColor = CLEARCOLOR;
    _BufferLabel.textAlignment = NSTextAlignmentCenter;
    _BufferLabel.text = @"未获取到内容，请点击页面重试";
    _BufferLabel.userInteractionEnabled = YES;
    _BufferLabel.hidden = YES;
    [self addSubview:_BufferLabel];
    MyGesture *refreshTap = [[MyGesture alloc] initWithTarget:self action:@selector(detailRefresh)];
    refreshTap.numberOfTapsRequired = 1;
    [_BufferLabel addGestureRecognizer:refreshTap];
    
    
    [_BufferLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.top.mas_equalTo(wself.mas_top).offset(90*kRateSize);
        make.size.mas_equalTo(CGSizeMake(kDeviceWidth-20*kRateSize, 100*kRateSize));
        
    }];
//------------------------------------
    
    //-----------------------------------
    _headerView = [[DetailHeaderView alloc] init];
    [_headerView setBackgroundColor:[UIColor whiteColor]];
    _headerView.userInteractionEnabled = YES;
    _headerView.delegate = self;
    [_scrollContentView addSubview:_headerView];
    
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_scrollContentView.mas_top).offset(35*kRateSize);
        make.left.mas_equalTo(_scrollContentView.mas_left);
        make.width.mas_equalTo(_scrollContentView.mas_width);
        make.height.mas_equalTo(0*kRateSize);
        
    }];
    
    //-----------------------------------
    _KNewIntrolView = [[NewIntrolView alloc] init];
    [_KNewIntrolView setBackgroundColor:UIColorFromRGB(0xffffff)];
//    [_KNewIntrolView setBackgroundColor:App_selected_color];

    [_scrollContentView addSubview:_KNewIntrolView];
    [_KNewIntrolView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.scrollContentView.mas_top).offset(35*kRateSize);
        make.left.mas_equalTo(wself.scrollContentView.mas_left);
        make.width.mas_equalTo(wself.scrollContentView.mas_width);
        
    }];
//    [_KNewIntrolView setPraiseNum:@"3" andStampNum:@"55"];
    _KNewIntrolView.praiseBlock = ^(UIButton_Block *btn){
        [wself praiseBtnSelected:btn];
    };
    _KNewIntrolView.stampBlock = ^(UIButton_Block *btn){
        [wself stampBtnSelected:btn];
    };
    
    //-----------------------------------
    
    _episodeView = [[EpisodeSelectedView alloc] init];
    [_episodeView setBackgroundColor:UIColorFromRGB(0xffffff)];
//    [_episodeView setBackgroundColor:[UIColor greenColor]];
    [_scrollContentView addSubview:_episodeView];
    _episodeView.delegate = self;
//    [_episodeView loadBtnWithEpisodNum:26];
    [_episodeView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(1*kRateSize);
        make.left.mas_equalTo(_scrollContentView.mas_left);
        make.width.mas_equalTo(_scrollContentView.mas_width);
        
    }];
    
    
    
    _variteySelecView  = [[VarietySelecetedView alloc] init];
    [_variteySelecView setBackgroundColor:UIColorFromRGB(0xffffff)];
//    [_variteySelecView setBackgroundColor:[UIColor yellowColor]];
    _variteySelecView.delegate = self;
    [_scrollContentView addSubview:_variteySelecView];
    
    [_variteySelecView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(14*kRateSize);
        make.left.mas_equalTo(_scrollContentView.mas_left);
        make.width.mas_equalTo(_scrollContentView.mas_width);
//        make.height.mas_equalTo(200*kRateSize);
        
    }];
    
    //--------------------
    _recommandView = [[DetailRecommendView alloc] init];
    [_recommandView setBackgroundColor:UIColorFromRGB(0xffffff)];
//    [_recommandView setBackgroundColor:[UIColor redColor]];
    [_scrollContentView addSubview:_recommandView];
    _recommandView.delegate = self;
    [_recommandView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_episodeView.mas_bottom).offset(4*kRateSize);
        make.left.mas_equalTo(_scrollContentView.mas_left);
        make.right.mas_equalTo(_scrollContentView.mas_right);
    }];
    
    
    //-------------------
    
    
    [_scrollContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_recommandView.mas_bottom);
    }];

    _headerView.hidden = YES;
    _varityIntrolView.hidden = YES;
    _introlView.hidden = YES;
    _episodeView.hidden = YES;
    _variteySelecView.hidden = YES;
    _recommandView.hidden = YES;
    
    
    
    
    
    
    
}


-(void)setIsVarity:(BOOL)isVarity{
    _isVarity = isVarity;
    
    if (isVarity) {

        [_episodeView setHidden:YES];
        [_variteySelecView setHidden:NO];
        
        
    }else{

        [_episodeView setHidden:NO];
        [_variteySelecView setHidden:YES];
    
    
    }
    


}

-(void)setProgramTypeWith:(NSInteger)programType{

    _programType = programType;
    [_BufferLabel setHidden:YES];

    switch (programType) {
        case 1:
        {
            
            [_episodeView setHidden:YES];
            [_variteySelecView setHidden:YES];
            
            [_episodeView mas_remakeConstraints:^(MASConstraintMaker *make) {
               
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.width.mas_equalTo(_scrollContentView.mas_width);
                make.bottom.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                
            }];
            [_variteySelecView mas_remakeConstraints:^(MASConstraintMaker *make) {
               
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.right.mas_equalTo(_scrollContentView.mas_right);
                make.bottom.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                
            }];
            
            
            break;
        }
        case 2:
        {
            
            [_episodeView setHidden:NO];
            [_variteySelecView setHidden:YES];
            
            [_episodeView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.width.mas_equalTo(_scrollContentView.mas_width);
//                make.height.mas_equalTo(155*kRateSize);
                
            }];
            [_variteySelecView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.right.mas_equalTo(_scrollContentView.mas_right);
                make.bottom.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                
            }];
            [_recommandView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.top.mas_equalTo(_episodeView.mas_bottom).offset(10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.right.mas_equalTo(_scrollContentView.mas_right);
//                make.height.mas_equalTo(900);

            }];
            break;
        }
        case 3:
        {
            [_episodeView setHidden:YES];
            [_variteySelecView setHidden:NO];
            
            [_episodeView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(-10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.width.mas_equalTo(_scrollContentView.mas_width);
            }];
 
            [_variteySelecView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_KNewIntrolView.mas_bottom).offset(10*kRateSize);
                make.left.mas_equalTo(_scrollContentView.mas_left);
                make.width.mas_equalTo(_scrollContentView.mas_width);
//                make.height.mas_equalTo(200*kRateSize);
                
            }];
            
            [_recommandView mas_updateConstraints:^(MASConstraintMaker *make) {
               
                make.top.mas_equalTo(_variteySelecView.mas_bottom).offset(10*kRateSize);
                make.left.mas_equalTo(_scrollContentView);
                make.right.mas_equalTo(_scrollContentView);
//                make.height.mas_equalTo(500);
            }];
            
            break;
        }
        default:
            break;
    }
    
   
//    if (programType == 3) {
//        [_scrollContentView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(_recommandView.mas_bottom).offset(230*kRateSize);
//        }];
//    }else{
//        [_scrollContentView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(_recommandView.mas_bottom).offset(250*kRateSize);
//        }];
//    }
//    _recommandView.hidden = YES;
    if (programType == 3) {
        [_scrollContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_recommandView.mas_bottom).offset(230*kRateSize);
        }];
    }else{
        [_scrollContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_recommandView.mas_bottom).offset(250*kRateSize);
        }];
    }

}



-(void)headerViewLoadeEventImage:(NSString *)imageUrl andName:(NSString *)name andPlayNum:(NSString *)playNum{
    [_BufferLabel setHidden:YES];
    _headerView.hidden = NO;
    [_headerView loadEventImage:imageUrl andName:name andPlayNum:playNum];

}

-(void)headerViewCancellledFavor{
    [_headerView cancellledFavor];
    
}
-(void)headerViewFavorSucceed{
    [_headerView favorSucceed];
    
}

//-----------------
-(void)KNewIntrolViewLoadingIntrolInfoWithName:(NSString *)name andActor:(NSString *)actor andDirector:(NSString *)director andType:(NSString *)type andYears:(NSString *)years andArea:(NSString *)area andPraiseNum:(NSString *)praiseNum andStampNum:(NSString *)stampNum andIntrol:(NSString *)introl{
    
    [_KNewIntrolView setIntrolInfoWithName:name andActor:actor andDirector:director andType:type andYears:years andArea:area andPraiseNum:praiseNum andStampNum:stampNum andIntrol:introl];

}

-(void)introlViewLoadeIntrolWith:(NSString *)introl andMainActors:(NSString *)actors andDirector:(NSString *)director andYears:(NSString *)years andArea:(NSString *)area andType:(NSString *)type andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum{
    [_BufferLabel setHidden:YES];
    _varityIntrolView.hidden = YES;
    _introlView.hidden = NO;

    //    [_introlView loadIntolWith:introl andMainActors:actors andDirector:director andYears:years andType:type andArea:area andPraiseNum:praiseNum andStamNum:stampNum];
    [_KNewIntrolView setIntrolInfoWithName:@"诛仙（萧鼎）" andActor:actors andDirector:director andType:type andYears:years andArea:area andPraiseNum:praiseNum andStampNum:stampNum andIntrol:introl];

    
}

-(void)varityIntrolViewLoadeIntrolWith:(NSString *)introl andYear:(NSString *)year andType:(NSString *)type andArea:(NSString *)area andMostNew:(NSString *)mostNew andPraiseNum:(NSString *)praiseNum andStamNum:(NSString *)stampNum{
    [_BufferLabel setHidden:YES];
    _introlView.hidden = YES;
    _varityIntrolView.hidden = NO;

    [_KNewIntrolView setIntrolInfoWithName:@"诛仙（萧鼎）" andActor:@"哈哈哈" andDirector:@"啦啦啦" andType:type andYears:year andArea:area andPraiseNum:praiseNum andStampNum:stampNum andIntrol:introl];

//    [_varityIntrolView loadIntrolWith:introl andYear:year andType:type andArea:area andMostNew:mostNew andPraiseNum:praiseNum andStamNum:stampNum];
    
}
-(void)reloadVarityDateWith:(NSArray *)varityArr andAllEpisodNum:(NSInteger)allNum andUpdateNum:(NSString *)updateNum{

    _episodeView.hidden = YES;
    _variteySelecView.hidden = NO;
    [self.variteySelecView reloadVarityDateWith:varityArr andAllEpisodNum:allNum andUpdateNum:updateNum];
    
}

//-----------
-(void)episodeViewLoadeBtnWithEpisodeNum:(NSInteger)episodNum{

    NSLog(@"推荐使用    -(void)episodeViewLoadeAllEpisodNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum ");
    return;
    _episodeView.hidden = NO;
    [_episodeView loadBtnWithEpisodNum:episodNum];

}

-(void)episodeViewHiddenBadgeAtIndex:(NSInteger)index{
    [_episodeView hiddenBadgeAtIndex:index];

}
-(void)episodeViewSetSelectedBtnAtIndex:(NSInteger)index{
    [_episodeView setSelectedBtnAtIndex:index];


}

-(void)episodeViewLoadeAllEpisodNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum{
    [_BufferLabel setHidden:YES];

    _episodeView.hidden = NO;

    //    [_episodeView loadAllEpisodNum:allNum andUpdateNum:updateNum];
    [_episodeView loadAllEpisodNumWithAllNum:allNum andUpdateNum:updateNum];

}
-(void)episodeViewLoadeAllEpisodeNum:(NSString *)allNum andUpdateNum:(NSString *)updateNum andEpisodeArr:(NSArray *)episodeArr{
    [_BufferLabel setHidden:YES];
    
    _episodeView.hidden = NO;
    [_episodeView reloadAllEpisodeNumWithAllNum:allNum andUpdateNum:updateNum andEpisodeArr:episodeArr];
    //    [_episodeView loadAllEpisodNum:allNum andUpdateNum:updateNum];
//    [_episodeView loadAllEpisodNumWithAllNum:allNum andUpdateNum:updateNum];

}

-(void)varityViewSetselectedAtIndex:(NSInteger)index{
    [_variteySelecView setSelectedIndex:index];
}

//---------
-(void)recommandViewLoadeRecommandItems:(NSArray *)recommandItems{
    [_BufferLabel setHidden:YES];

    _recommandView.hidden = NO;
    
    CGSize itemsSzie = CGSizeMake(((kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight)-(6+8)*kRateSize)/3, 170*kRateSize);
    
    CGFloat heigh;
    NSInteger count;
    count = (recommandItems.count%3 == 0) ? (recommandItems.count/3) : (recommandItems.count/3 + 1);
    heigh = 25*kRateSize + (itemsSzie.height +5*kRateSize) *count;
    
    CGFloat top = 20;
    if (_programType == 1) {
        top = 20;
    }
    if ((_programType == 2) || (_programType == 3)) {
        top = 10;
    }
    [_recommandView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_episodeView.mas_bottom).offset(top*kRateSize);
        make.left.mas_equalTo(_scrollContentView.mas_left);
        make.right.mas_equalTo(_scrollContentView.mas_right);
        make.height.mas_equalTo(heigh);
        
    }];
//    //**********  donghua ******************
//    // tell constraints they need updating
//    [self setNeedsUpdateConstraints];
//    
//    // update constraints now so we can animate the change
//    [self updateConstraintsIfNeeded];
//    
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        [self layoutIfNeeded];
//        
//    } completion:^(BOOL finished) {
//        
//        
//    }];
    
    [_recommandView loadRecommandItems:recommandItems];

}
//点赞或者吐槽 是否成功
-(void)praiseSuccecc:(BOOL)isSuccecc{
    
    [_KNewIntrolView praiseSucceed:isSuccecc];
}

-(void)stampSuccecc:(BOOL)isSuccecc{
    [_KNewIntrolView stampSucceed:isSuccecc];


}



#pragma mark - delegate 

-(void)detailRefresh{
    NSLog(@"刷新详情界面~");

}
-(void)detailHeaderViewFavorBtnSelected:(UIButton *)btn{
   
    if ([self.delegate respondsToSelector:@selector(headerViewFavorateBtnSeleced:)]) {
        [self.delegate headerViewFavorateBtnSeleced:btn];
    }
    
    
}

-(void)praiseBtnSelected:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(introlViewPraiseBtnSelected:)]) {
        [self.delegate introlViewPraiseBtnSelected:btn];
    }
   
    
}

-(void)stampBtnSelected:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(introlViewStampBtnSelected:)]) {
        [self.delegate introlViewStampBtnSelected:btn];
    }

}

-(void)varityPraiseBtnSelected:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(varityViewPraiseBtnSelected:)]) {
//        [self.delegate varityViewStampBtnSelected:btn];
        //没有修改抓狂
        [self.delegate varityViewPraiseBtnSelected:btn];

    }
    

}
-(void)varityStampBtnSelected:(UIButton *)btn{

    if ([self.delegate respondsToSelector:@selector(varityViewStampBtnSelected:)]) {
        [self.delegate varityViewStampBtnSelected:btn];
    }

    
}



-(void)EpisodeViewBtnSelectedAtIndex:(NSInteger)index{
    if ([self.delegate respondsToSelector:@selector(episodeViewEBtnSelectedAtIndex:)]) {
        [self.delegate episodeViewEBtnSelectedAtIndex:index];
    }

}

-(void)VTableViewSelectedIndex:(NSInteger)index{
    if ([self.delegate respondsToSelector:@selector(VTabSelectedIndex:)]) {
        [self.delegate VTabSelectedIndex:index];
    }

}


-(void)recommandItemsSelectedAtIndex:(NSInteger)index{
    
    if ([self.delegate respondsToSelector:@selector(recommandViewItemsSelectedAtIndex:)]) {
        [self.delegate recommandViewItemsSelectedAtIndex:index];
    }
    
    
}

-(BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view{

    if ([view class] == [_variteySelecView.VTableView class] ) {
        return YES;
    }else{
    
        return [super touchesShouldBegin:touches withEvent:event inContentView:view];
    }
    
    return YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
 
    // Drawing code
}
*/

@end
