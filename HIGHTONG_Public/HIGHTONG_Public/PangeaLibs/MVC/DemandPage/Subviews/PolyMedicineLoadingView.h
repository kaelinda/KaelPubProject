//
//  PolyMedicineLoadingView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface PolyMedicineLoadingView : UIImageView
@property (nonatomic,strong)UIImageView *loadingBV;
@property (nonatomic,strong)UILabel *loadingLabel;
@property (nonatomic,strong)UIActivityIndicatorView *loadingAV;
@end
