//
//  DemandSecondViewcontrol.m
//  HIGHTONG
//
//  Created by testteam on 15/8/20.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DemandSecondViewcontrol.h"
#import "ProgramListCell.h"
#import "DemandVideoPlayerVC.h"
#import "HTRequest.h"
#import "DemandThirdViewController.h"
//#import "DemandChooseViewcontrol.h"

#import "ChooseView.h"

#import "HotSearchTableViewCell.h"
#import "MJRefresh.h"
@interface DemandSecondViewcontrol ()<UITableViewDataSource,UITableViewDelegate,HTRequestDelegate>
{
//    UIButton *_FENLEIBTN;
    
    //分类搜素按钮请求字典
    NSMutableDictionary *_requstDiction;
    
    
    
    BOOL _isNOresult;//搜不到东西
    
    UIView *hasBeenRequst;//已经请求过筛选类型了
    NSDate *requstDate;//请求分类筛选的时间
    
    NSMutableDictionary *_categoryStringDIC;//选择的category字典
    
    NSInteger _realCountOfResault;//实际请求的个数
    NSInteger _allCountOfResault;//实际请求的个数
    
    HTRequest *request;//
    
    BOOL StateFENLEI;//筛选状态
    BOOL Statefresh;//筛选状态
}
@property (nonatomic,strong)UIButton *FENLEIBTN;
@property (nonatomic,strong)UITableView *tableView_base;
@property (nonatomic,strong)NSMutableArray *dataArray;

@end
#define requestCount 18
@implementation DemandSecondViewcontrol

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    if (self.hidenChoose) {
        
    }else
    {
        _FENLEIBTN = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"ic_vod_filter_normal" imgHighlight:@"ic_vod_filter_pressed" target:self action:@selector(FENLEI:)];
//        _FENLEIBTN = [HT_NavigationgBarView createNormalNaviBarBtnByTitle:@"筛选" target:self action:@selector(FENLEI:)];
        [self setNaviBarRightBtn:_FENLEIBTN];
    }
    
    UIButton *Back = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_common_title_back_normal" target:self action:@selector(Back)];
    [self setNaviBarLeftBtn:Back];
    
//    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
//    title.backgroundColor = [UIColor clearColor];
//    title.text = self.name;
//    [title setTextColor:[UIColor whiteColor]];
//    [self setNaviBarSearchView:title];
//    [self setNaviLeftTitle:self.name];
//    [self setNaviBarTitle:self.name];
    
    [self setNaviLeftTitle:self.name];
    
    
    if ([APP_ID isEqualToString:JXCABLE_SERIAL_NUMBER]) {
        
        [self setNaviLeftTitleColor:UIColorFromRGB(0x4e4d4d)];

    }

    
    [MobCountTool pushInWithPageView:@"DemandSecondPage"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobCountTool popOutWithPageView:@"DemandSecondPage"];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    _realCountOfResault = 1;
    if ( isAfterIOS7 )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
#endif
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    _categoryStringDIC = [NSMutableDictionary dictionary];
    
    //******请求的地点搜索分类
    _requstDiction =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"1",@"orderType",@"",@"operationCode",@"",@"rootCategoryID",@"",@"categoryID",nil];
    
    
    NSLog(@"%@ %@",self.name,self.categoryID);
    self.dataArray = [NSMutableArray array];
    [self setupBase];
}
- (void)setupBase
{
    self.tableView_base = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, KDeviceHeight - 64)];
    self.tableView_base.delegate  = self;
    self.tableView_base.dataSource = self;
    [self.tableView_base setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView_base.scrollsToTop = YES;
    [self.view addSubview:self.tableView_base];
    self.tableView_base.backgroundColor = App_background_color;
    
    
    
    //*******1,请求category下的数据默认  20个
    request = [[HTRequest alloc]initWithDelegate:self];
    
    
//    if (self.isTopic) {
//        [request VODTopicProgramListWithTopicCode:self.TopicCode andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
//    }else
//    {
//         [request VODProgramListWithOperationCode:_operationCode andRootCategpryID:self.categoryID andStartSeq:1 andCount:requestCount andTimeout:10];
//    }
    
    if ([_disPlayDemandType isEqualToString:@"3"]) {
        [request VODTopicProgramListWithTopicCode:_operationCode andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
    }else if ([_disPlayDemandType isEqualToString:@"1"])
    {
        [request VODProgramListWithOperationCode:_operationCode andRootCategpryID:self.categoryID andStartSeq:1 andCount:requestCount andTimeout:10];
    }

    
   
    [self  showLoading];
    
}
#pragma  mark- ht请求回调
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    switch (status) {
        case 0:
        {
            
            if ([type isEqualToString:VOD_PROGRAM_LIST]) {
                NSLog(@"dianbo返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDIC = result;
                NSString *count = [resDIC objectForKey:@"count"];
                NSInteger allcout = [count integerValue];
                NSArray *vodList = [resDIC objectForKey:@"vodList"];
                if ([vodList count]) {
                    
//                    NSLog(@"数据源：%@",vodList);
                    if (self.dataArray.count && Statefresh) {
                        [self.dataArray addObjectsFromArray:vodList];
                    }else
                    {
                        self.dataArray.array = vodList;
                    }
                    Statefresh = NO;
                    
                    [self.tableView_base reloadData];
                     _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    if (([vodList count] < allcout) && ([vodList count] >= requestCount) ) {
                        
                        if (!self.tableView_base.mj_footer) {
                            self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(MyloadMoreData)];
                        }
                    
                        [self.tableView_base.mj_footer endRefreshing];
                    }else
                    {
                        
                        [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                        _realCountOfResault += [vodList count];
                        _allCountOfResault = allcout;
                        //self.tableView_base.footer = nil
                    }
                    
                   
                    
                    [self hideLoading];
                }else
                {
                    
                    [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                     //self.tableView_base.footer = nil
                }
            }
            
            
            
            //一个分类下的所有电影电视
            if ([type isEqualToString:VOD_CATEGORY_LIST]) {
                
                NSLog(@"分类筛选返回数据: %ld result:%@  type:%@",status,result,type );
                
                NSLog(@"运营组:%@ rootID:%@ ",self.operationCode,self.categoryID);
                //****点播页是有operationCode的，但首页是没有的
                if (self.operationCode) {
                    [_requstDiction setObject:self.operationCode forKey:@"operationCode"];
                }
                if (self.categoryID) {
                    [_requstDiction setObject:self.categoryID forKey:@"rootCategoryID"];
                }
                
                NSDictionary *dic = result;
                NSString *count = [dic objectForKey:@"count"];
                if ([count integerValue]) {
                    NSArray *array = [dic objectForKey:@"typeList"];
                    
//                    NSMutableArray *categoryIDsArray = [NSMutableArray array];
                    
                    for (NSInteger  i =0 ;i<array.count; i++) {
                        [_categoryStringDIC setObject:@"" forKey:[NSNumber numberWithInteger:i]];
                    }
                    
                    
                    {
//                    NSInteger i = 0;
//                    for (NSDictionary *subdic in array) {
//                        i++;
//                        NSArray *subArray = [subdic objectForKey:@"typeList"];
//                        NSString *categoryID = [subdic objectForKey:@"categoryID"];
//                        [categoryIDsArray addObject:categoryID];//把区域年份类型的categoryID加入到数组中
//                        
//                        NSLog(@"%@\n\n",[subdic objectForKey:@"name"]);
//                        
//                    }
                    
//                    NSString *categoryREAL = [categoryIDsArray componentsJoinedByString:@","];
//                    NSLog(@"拼接的category是%@",categoryREAL);
//                    [_requstDiction setObject:categoryREAL forKey:@"categoryID"];
//                    默认是不传categoryID
                    }
                }
                
                //点击了筛选按钮
                UIView *HeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 130*kRateSize)];
                HeadView.backgroundColor = [UIColor lightGrayColor];
                self.tableView_base.tableHeaderView = HeadView;
                //****112121优化
                requstDate = [NSDate date];
                hasBeenRequst = HeadView;
                
                ChooseView *choose = [ChooseView new];
                [HeadView addSubview:choose];
                [choose mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(HeadView);
                }];
                choose.Choose_Block = ^(NSDictionary*dic,NSString *indexOfRows)
                {
                    NSLog(@"带有三个选择框的%@ 某个scrollView 选择了\nid： %@ \ncategoryud： %@ \nname：%@",[dic objectForKey:@"index"],[dic objectForKey:@"id"],[dic objectForKey:@"categoryID"],[dic objectForKey:@"name"]);
                    
                    NSString *index = [dic objectForKey:@"index"];
                    NSString *categoryID = [dic objectForKey:@"categoryID"];
                    NSInteger count = [index integerValue];
                    if (count) {
                        
//                       NSString *cate = [self chooseWithNEW_CategoryString:categoryID andCategoryROW_INDEX:count andCategory_Record_DICTION:_categoryStringDIC];
                        NSString *cate = [self chooseWithNEW_CategoryString:categoryID andCategoryROW_INDEX:count andCategory_Record_DICTION:_categoryStringDIC andINDEX_IN_CurrentRow:indexOfRows];
                        [_requstDiction setObject:cate forKey:@"categoryID"];
                        NSLog(@"区域年份的搜索字典 %@",_requstDiction);
                        
                        
                            _realCountOfResault = 1;
                        //*****点击了按钮******新的搜索出现了调用搜索刷新数据
                        [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];
                    }else
                    {
                        [_requstDiction setObject:[dic objectForKey:@"id"] forKey:@"orderType"];
                        
                            _realCountOfResault = 1;
                        NSLog(@"热门，上架 的搜索字典 %@",_requstDiction);
                        //*******点击了  最热门，新上架****新的搜索出现了调用搜索刷新数据
                        [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];

                    }
                    
                    
                    
                    
                    {
//                    NSString *categoryID_string = [_requstDiction objectForKey:@"categoryID"];
                    
                    
                    
//                    if (categoryID_string.length) {
//                        
//                        NSArray *array = [categoryID_string componentsSeparatedByString:@","];
//                        NSMutableArray *mutArray = [NSMutableArray arrayWithArray:array];
//                        NSInteger index = [[dic objectForKey:@"index"] integerValue];
//                        
//                        if (index >= 1) {
//                            NSLog(@"新的ID: %ld",index);
//                            NSString *index_String = [mutArray objectAtIndex:index-1];
//                            NSString *NEW_categoryID = [dic objectForKey:@"categoryID"];
//                            [mutArray replaceObjectAtIndex:(index-1) withObject:NEW_categoryID];
//                            
//                            NSLog(@"新的ID: %@",NEW_categoryID);
//                            NSLog(@"旧的ID: %@",index_String);
//                            NSLog(@"分类iDL : %@",mutArray);
//                            NEW_categoryID = [mutArray componentsJoinedByString:@","];
//                            [_requstDiction setObject:NEW_categoryID forKey:@"categoryID"];
//                            
//                            NSLog(@"区域年份的搜索字典 %@",_requstDiction);
//                            //*****点击了按钮******新的搜索出现了调用搜索刷新数据
//                            [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];
//                            
//                        }else
//                        {
//                            [_requstDiction setObject:[dic objectForKey:@"id"] forKey:@"orderType"];
//                            NSLog(@"热门，上架 的搜索字典 %@",_requstDiction);
//                            //*******点击了  最热门，新上架****新的搜索出现了调用搜索刷新数据
//                            [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];
//                            
//                        }
//                    }
                    }
                    
                };
                [choose upDataViewWithArray:dic];
                choose.backgroundColor = [UIColor whiteColor];
                _realCountOfResault = 1;
                NSLog(@"默认的搜索字典 %@",_requstDiction);
                //******第一次刷新数4个列表是的默认请求字典*****新的搜索出现了调用搜索刷新数据
                [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];
                [self hideLoading];
            }
            
            
            //*******点击按钮搜索了
            if ([type isEqualToString:VOD_SEARCH]) {
//                NSLog(@"默认搜索返回数据: %ld result:%@  type:%@",status,result,type );
                NSDictionary *resDIC = result;
                NSString *count = [resDIC objectForKey:@"count"];
                NSInteger allcout = [count integerValue];
                NSArray *vodList = [resDIC objectForKey:@"vodList"];
                if ([vodList count]) {
                    
//                    NSLog(@"数据源：%@",vodList);
                    if (self.dataArray.count &&Statefresh) {
                        [self.dataArray addObjectsFromArray:vodList];
                    }else
                    {
                        self.dataArray.array = vodList;
                    }
                    Statefresh = NO;
                    _isNOresult = NO;
                    [self.tableView_base reloadData];
                    
                    
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    if (([vodList count] < allcout) && ([vodList count] >= requestCount) ) {
                        if (!self.tableView_base.mj_footer) {
                            self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(MyloadMoreData)];
                        }
                        
                         [self.tableView_base.mj_footer endRefreshing];
                    }else
                    {
                        
                        [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                        _realCountOfResault += [vodList count];
                        _allCountOfResault = allcout;
                        //self.tableView_base.footer = nil
                    }
                    
                   
                }else{
                    _isNOresult = YES;
                    [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                    _realCountOfResault = 1;
                    _allCountOfResault = 0;
                    [self.dataArray removeAllObjects];
                    [self.tableView_base reloadData];
                }
                
                if (self.dataArray.count){
//                    if ([vodList count] == 0) {
//                        [self.tableView_base.footer endRefreshing];
//                        [self.tableView_base.footer noticeNoMoreData];
//
//                    }
                }else
                {
                   
                    NSString *toastStr ;
                    if (kDeviceWidth == 320) {
                        toastStr = @"暂无搜索结果，换个筛选类型试试";
                    }else if (kDeviceWidth == 375)
                    {
                        toastStr = @"暂无搜索结果，换个筛选类型试试";
                    }
                    
                    NSDictionary *dicNULL = [NSDictionary dictionaryWithObjectsAndKeys:toastStr,@"name",@"0000",@"seq", nil];
                    

                    [self.dataArray removeAllObjects];
                    [self.dataArray addObject:dicNULL];

                    

               
                    [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
//                    [self.tableView_base.footer endRefreshing];
                    _isNOresult = YES;
                    _realCountOfResault = 1;
                    _allCountOfResault = 0;
                    //self.tableView_base.footer = nil
                    [self.tableView_base reloadData];
                }
                
                [self hideLoading];
            }
            
            
            //推荐页面的
            if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
                NSLog(@"推荐广告: %ld result:%@  type:%@",status,result,type );
                NSArray *vodList = [result objectForKey:@"vodList"];
                NSString *count = [result objectForKey:@"count"];
                NSInteger allcout = [count integerValue];
                if ([[result objectForKey:@"count"] integerValue] >= 1) {
                    
                    
                    if (self.dataArray.count &&Statefresh) {
                        [self.dataArray addObjectsFromArray:vodList];
                    }else
                    {
                        self.dataArray.array = vodList;
                    }
                    Statefresh = NO;
                    
                        [self.tableView_base reloadData];
                    
                    
                    _realCountOfResault += [vodList count];
                    _allCountOfResault = allcout;
                    if (([vodList count] < allcout) && ([vodList count] >= requestCount) ) {
                        
                        if (!self.tableView_base.mj_footer) {
                            self.tableView_base.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(MyloadMoreData)];
                        }
                        
                        [self.tableView_base.mj_footer endRefreshing];
                    }else
                    {
                        
                        [self.tableView_base.mj_footer endRefreshingWithNoMoreData];
                        _realCountOfResault += [vodList count];
                        _allCountOfResault = allcout;
                        //self.tableView_base.footer = nil
                    }
                    

                    
                }else{
                    [AlertLabel AlertInfoWithText:@"暂无专题节目" andWith:self.view withLocationTag:1];

                }
               
                
                [self hideLoading];
                
            }
        }
            break;
            
        default:
        {
            if ([type isEqualToString:VOD_PROGRAM_LIST] || [type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
                [self.tableView_base.mj_footer endRefreshing];
            }
        }
            break;
    }
    
    
}
#pragma mark - mj刷新//这个是运营组的刷新方法
- (void)MyloadMoreData
{
    
    NSLog(@"%ld",_realCountOfResault);
    
    NSLog(@"下拉刷新了");
    
    if (StateFENLEI) {
        //*******点击了  最热门，新上架****新的搜索出现了调用搜索刷新数据
        [self SearchWithLeixingQuyuNianfenDiction:_requstDiction];
    }else
    {
//        if (self.isTopic) {
//            [request VODTopicProgramListWithTopicCode:self.TopicCode andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];
//        }else
//        {
//            [request VODProgramListWithOperationCode:_operationCode andRootCategpryID:self.categoryID andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10];
//        }
        
        if ([_disPlayDemandType isEqualToString:@"3"]) {
        [request VODTopicProgramListWithTopicCode:_operationCode andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10 andIndex:21];

        }else if ([_disPlayDemandType isEqualToString:@"1"])
        {
      [request VODProgramListWithOperationCode:_operationCode andRootCategpryID:self.categoryID andStartSeq:_realCountOfResault andCount:requestCount andTimeout:10];
        }

        
    }
    
    Statefresh =  YES;
    
}

-(void)setDisPlayDemandType:(NSString *)disPlayDemandType
{
    _disPlayDemandType = disPlayDemandType;
    
    if ([_disPlayDemandType isEqualToString:@"3"]) {

        [self setHidenChoose:YES];
    }else if ([_disPlayDemandType isEqualToString:@"1"])
    {
        [self setHidenChoose:NO];
    }
    
    
}
#pragma mark - 点击了按钮，按照 年代、区域、类型  最热门 、 新上架 排序进行搜索
- (void)SearchWithLeixingQuyuNianfenDiction:(NSDictionary*)dic
{
    
    NSLog(@"整整的请求字典%@",dic);
    request = [[HTRequest alloc]initWithDelegate:self];

    NSString *type = [dic objectForKey:@"orderType"];
    if (type.length==0) {
        type =@"1";
    }
    [request VODSearchWithOperationCode:[dic objectForKey:@"operationCode"] andRootCategoryID:[dic objectForKey:@"rootCategoryID"] andCategoryID:[dic objectForKey:@"categoryID"] andTopicCode:nil andKey:nil andStarSeq:_realCountOfResault andCount:requestCount andOrderType:[type integerValue] andTimeout:10];
}


#pragma mark - tabelView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isNOresult == NO) {
        return [self numberOfCellRowsWithArray:self.dataArray];;
    }else
    {
        return self.dataArray.count;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isNOresult == NO) {
        return 172*kRateSize;;
    }else
    {
        return (KDeviceHeight- 170-130)*kRateSize;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isNOresult == NO) {
        static NSString *Progranidentifier = @"Progranidentifier";
        ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
        if (!cell) {
            cell = [[ProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
        }
        NSArray * array = self.dataArray;
        
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.dateArray.array = [self arrayFOR_ALLArray:array withBeginnumb:indexPath.row];
        cell.isMovie = self.isMovie;
        [cell update];
        cell.block = ^(UIButton_Block*btn)
        {
            NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
            
            DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
            demandVC.TRACKID = self.TRACKID;
            demandVC.TRACKNAME = self.TRACKNAME;
            demandVC.EN = self.EN;
            demandVC.contentID = btn.serviceIDName;
            demandVC.contentTitleName = btn.eventName;
            [self.navigationController pushViewController:demandVC animated:YES];
        };
        return cell;
    }else
    {
        static NSString *Hotidentifier = @"HotIdentifier";
        HotSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Hotidentifier];
        if (!cell) {
            cell = [[HotSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Hotidentifier];
        }
        [cell.btn setImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
        cell.btn.layer.cornerRadius = 0;
        cell.label.font = [UIFont systemFontOfSize:15*kRateSize];
        
//        [cell.btn setImage:[UIImage imageNamed:@"icon_search_01"] forState:UIControlStateNormal];
        cell.numLable.text = @"";
//        cell.backgroundColor = [UIColor grayColor];
        cell.label.text = @"暂无搜索结果，换个词试试吧！";//[valueDIC objectForKey:@"name"];
        cell.label.textAlignment = NSTextAlignmentCenter;
        [cell.label mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(cell.contentView.mas_top).offset(15);
            make.centerY.equalTo(cell.contentView.mas_centerY);
            make.left.equalTo(cell.contentView.mas_left).offset(0*kRateSize);
            make.right.equalTo(cell.contentView.mas_right);
            make.height.equalTo(@(40*kRateSize));
        }];
//        cell.title = @"此关键字没有对应的结果";
         cell.textLabel.textAlignment = NSTextAlignmentCenter;
//        cell.textLabel.text = @"此关键字没有对应的结果";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.deleteBtn.hidden = YES;
        return cell;
    }
    
    
}


#pragma  mark - navigation上的按钮
- (void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"返回点播首页");
}

- (void)FENLEI:(UIButton*)btn
{
    
    
    DemandThirdViewController *demandSecond = [[DemandThirdViewController alloc]init];
    demandSecond.isMovie = self.isMovie;
    demandSecond.categoryID = self.categoryID;
    demandSecond.operationCode = self.operationCode;
    demandSecond.name = self.name;
    [self.navigationController pushViewController:demandSecond animated:YES];
    return;
    
    
    _realCountOfResault = 1;
    _allCountOfResault = 1;
    
    [self.dataArray removeAllObjects];
    StateFENLEI = YES;
    
    if ([btn.currentTitle isEqualToString:@"取消"]) {
//        [btn setTitle:@"筛选" forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"ic_vod_filter_normal"]forState:UIControlStateNormal];
        self.tableView_base.tableHeaderView = nil;
    }else
    {
        
        [btn setTitle:@"取消" forState:UIControlStateNormal];
        NSLog(@"分类搜索");
        if (hasBeenRequst && ![self RequstEnabledWithFirstTime:requstDate andSecondData:[NSDate date] andTime:20]) {
            self.tableView_base.tableHeaderView = hasBeenRequst;
            
            
        }else
        {
            //请求分类信息
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request VODCategoryListWithParentID:self.categoryID andTimeout:10];
            
            [self.tableView_base setContentOffset:CGPointMake(0, 0)];
        }
        
        [self showLoading];
    }
    {
//    btn.selected = !btn.selected;
//    if (btn.selected == NO) {
//        self.tableView_base.tableHeaderView = nil;
//    }else
//    {
//        NSLog(@"分类搜索");
//        if (hasBeenRequst && ![self RequstEnabledWithFirstTime:requstDate andSecondData:[NSDate date] andTime:20]) {
//            self.tableView_base.tableHeaderView = hasBeenRequst;
//        }else
//        {
//            //请求分类信息
//            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//            [request VODCategoryListWithParentID:self.categoryID andTimeout:10];
//            
//            [self.tableView_base setContentOffset:CGPointMake(0, 0)];
//        }
//    }
    }
    
}
#pragma mark - 工具

- (void)showLoading
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hideLoading) userInfo:nil repeats:NO];
}
- (void)hideLoading
{
    [MBProgressHUD hideAllHUDsForView:self.view  animated:YES];
}
#pragma mark - 工具类
- (NSString*)chooseWithNEW_CategoryString:(NSString*)string andCategoryROW_INDEX:(NSInteger)index andCategory_Record_DICTION:(NSMutableDictionary*)diction
{
    index -= 1;
    if (string.length) {
        [diction setObject:string forKey:[NSNumber numberWithInteger:index]];
    }

    NSMutableArray *anser = [NSMutableArray array];
    NSArray *array = [diction allKeys];
    for (id obj  in array) {
        id value = [diction objectForKey:obj];
        if ([value length] && [value isKindOfClass:[NSString class]]) {
            [anser addObject:value];
        }
    }
    
    NSString *requestStringCategory = [anser componentsJoinedByString:@","];
    NSLog(@"请求的cate\n 是%@\n%@\n",requestStringCategory,_requstDiction);
    return requestStringCategory;
}

- (NSString*)chooseWithNEW_CategoryString:(NSString*)string andCategoryROW_INDEX:(NSInteger)index andCategory_Record_DICTION:(NSMutableDictionary*)diction andINDEX_IN_CurrentRow:(NSString*)indexInRow
{
    index -= 1;
    if (string.length) {
        if ([indexInRow integerValue] != 0) {//如果传入的时某一行的第0个元素就是全部按钮，就不要修改categoryDiction 了
            [diction setObject:string forKey:[NSNumber numberWithInteger:index]];
        }else
        {
            [diction setObject:@"" forKey:[NSNumber numberWithInteger:index]];
        }
    }
    
    NSMutableArray *anser = [NSMutableArray array];
    NSArray *array = [diction allKeys];
    for (id obj  in array) {
        id value = [diction objectForKey:obj];
        if ([value length] && [value isKindOfClass:[NSString class]]) {
            [anser addObject:value];
        }
    }
    
    NSString *requestStringCategory = [anser componentsJoinedByString:@","];
    NSLog(@"请求的cate\n 是%@\n%@\n",requestStringCategory,_requstDiction);
    return requestStringCategory;
}




//可以放在工具类中，开一个时间是不是超时了
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1 andTime:(NSInteger) time
{
    if ([date1 timeIntervalSinceDate:date] >= time) {
        NSLog(@"超时了啊");
        return YES;
    }
    return NO;
}
//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        return numb % 3 == 0?  numb/3 : numb/3+1;
    }
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    if (array.count > index*3) {
        [anser addObject:array[index*3]];
    }
    if (array.count > index*3+1) {
        [anser addObject:array[index*3+1]];
    }
    if (array.count > index*3+2) {
        [anser addObject:array[index*3+2]];
    }
    
    return anser;
}


@end










///**
// *  gab
// */
//
////                            for (NSDictionary *diccccc in subArray) {
////                                NSLog(@"%@",[diccccc objectForKey:@"id"]);
////                                NSLog(@"%@",[diccccc objectForKey:@"name"]);
////                            }
//{
//
//    //                    if (i == 1) {
//    //                        Choose_Scroll *scroll = [Choose_Scroll new];
//    //                        [self.view addSubview:scroll];
//    //                        [scroll mas_makeConstraints:^(MASConstraintMaker *make) {
//    //                            make.top.equalTo(self.view.mas_top).offset(90);
//    //                            make.height.equalTo(@(50));
//    //                            make.left.equalTo(self.view.mas_left);
//    //                            make.right.equalTo(self.view.mas_right).offset(90);
//    //                        }];
//    //
//    //                        scroll.backgroundColor = [UIColor lightGrayColor];
//    //                        [scroll upDataViewWithArray:subdic];
//    //                    }
//}
