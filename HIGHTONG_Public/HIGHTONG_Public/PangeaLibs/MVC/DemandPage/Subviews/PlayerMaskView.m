//
//  PlayerMaskView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/12/1.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "PlayerMaskView.h"

@interface PlayerMaskView(){
    
    NSTimer *_resetProgressTimer;
    
}
@end


@implementation PlayerMaskView

-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }

    return self;
}
-(instancetype)initWithAppType:(APPMaskType)type{
    self = [super init];
    if (self) {
        switch (type) {
            case JKTV:
            {
//                [self setupSubViews];

            }
                break;
            case JXCABLE_ASTV:
            {
                [self setupJXSubViews];
            }
                break;
                
            default:
                break;
        }
    }
    
    return self;
}



-(void)setupJXSubViews
{
    WS(wself);
    self.backgroundColor = [UIColor clearColor];
    
    _backImageView = [[UIImageView alloc] init];
    _backImageView.image = [UIImage imageNamed:@""];
    _backImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:_backImageView];
    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
    }];
    
    _blackView = [[UIView alloc] init];
    [_blackView setBackgroundColor:[UIColor blackColor]];
    _blackView.alpha = 0.6;
    [self addSubview:_blackView];
    [_blackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
    }];
    
    
    
    _jxBackImageView = [[UIImageView alloc]init];
    _jxBackImageView.userInteractionEnabled = YES;
    [self addSubview:_jxBackImageView];
    
    
    [_jxBackImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kDeviceWidth, 17*kDeviceRate));
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY).offset(30*kDeviceRate);
    }];
    
    
    
    
    _jxPointIV = [[UIImageView alloc]init];
    [_jxPointIV setBackgroundColor:[UIColor clearColor]];
    [_jxPointIV setImage:[UIImage imageNamed:@"jx_image_tear"]];
    [wself addSubview:_jxPointIV];
    
    [_jxPointIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(33*kDeviceRate, 33*kDeviceRate));
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY);
    }];
    
    
    
    
    _jxPointLeftLabel = [[UILabel alloc]init];
    [_jxPointLeftLabel setBackgroundColor:[UIColor clearColor]];
    _jxPointLeftLabel.textColor = [UIColor whiteColor];
    _jxPointLeftLabel.textAlignment = NSTextAlignmentRight;
    _jxPointLeftLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
    [_jxPointLeftLabel setText:@"主人，您需要进行"];
    [_jxBackImageView addSubview:_jxPointLeftLabel];

    [_jxPointLeftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(150*kDeviceRate, 17*kDeviceRate));
//        make.centerX.mas_equalTo(wself.mas_centerX).offset(-60*kDeviceRate);
//        make.centerY.mas_equalTo(wself.mas_centerY).offset(30*kDeviceRate);
        make.left.mas_equalTo(_jxBackImageView.mas_left).offset(10*kDeviceRate);
        make.top.mas_equalTo(_jxBackImageView.mas_top);
    }];
    
    
    _jxPointRightLabel = [[UILabel alloc]init];
    [_jxPointRightLabel setBackgroundColor:[UIColor clearColor]];
    _jxPointRightLabel.textColor = [UIColor whiteColor];
    _jxPointRightLabel.textAlignment = NSTextAlignmentLeft;
    _jxPointRightLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];

    [_jxPointRightLabel setText:@"才能观看本节目"];
    [_jxBackImageView addSubview:_jxPointRightLabel];
    
    [_jxPointRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(150*kDeviceRate, 17*kDeviceRate));
//        make.centerX.mas_equalTo(wself.mas_centerX).offset(60*kDeviceRate);
//        make.centerY.mas_equalTo(wself.mas_centerY).offset(30*kDeviceRate);
        make.right.mas_equalTo(_jxBackImageView.mas_right).offset(-10*kDeviceRate);
        make.top.mas_equalTo(_jxBackImageView.mas_top);
    }];

    
    
    _jxLoginBtn = [[UIButton_Block alloc]init];
    [_jxLoginBtn setBackgroundColor:[UIColor clearColor]];
    [_jxLoginBtn setImage:[UIImage imageNamed:@"jx_btn_login"] forState:UIControlStateNormal];
    [wself addSubview:_jxLoginBtn];
    [_jxLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(54.5*kDeviceRate, 17*kDeviceRate));
//        make.centerX.mas_equalTo(wself.mas_centerX);
//        make.centerY.mas_equalTo(wself.mas_centerY).offset(30*kDeviceRate);
        make.left.mas_equalTo(_jxPointLeftLabel.mas_right);
        make.right.mas_equalTo(_jxPointRightLabel.mas_left);
        make.top.mas_equalTo(_jxBackImageView.mas_top);
    }];
    _jxLoginBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.maskViewReturnBlock) {
            NSLog(@"点击了江西登录按钮~~");
            wself.maskViewReturnBlock();
            
        }else
        {
            NSLog(@"没有实现江西登录Block");
        }
        
    };
    
    _jxBindBtn = [[UIButton_Block alloc]init];
    [_jxBindBtn setBackgroundColor:[UIColor clearColor]];
    [_jxBindBtn setImage:[UIImage imageNamed:@"jx_btn_bind"] forState:UIControlStateNormal];
    [_jxBackImageView addSubview:_jxBindBtn];
    [_jxBindBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(54.5*kDeviceRate, 17*kDeviceRate));
        //        make.centerX.mas_equalTo(wself.mas_centerX);
//        make.centerY.mas_equalTo(wself.mas_centerY).offset(30*kDeviceRate);
        make.left.mas_equalTo(_jxPointLeftLabel.mas_right);
        make.right.mas_equalTo(_jxPointRightLabel.mas_left);
        make.top.mas_equalTo(_jxBackImageView.mas_top);
    }];

    _jxBindBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.maskViewReturnBlock) {
            NSLog(@"点击了绑定按钮~~");
            wself.maskViewReturnBlock();
            
        }else
        {
            NSLog(@"没有实现绑定Block");
        }
        
    };
    
}

-(void)setJxProgramType:(JXPlayerMaskViewType )jxProgramType
{
    switch (jxProgramType) {
        case JXLoginActionType:
            
        {
            
            _jxBindBtn.hidden = YES;
            _jxLoginBtn.hidden = NO;
            
        }
            break;
        case JXBindingActionType:
            
        {
            _jxBindBtn.hidden = NO;
            _jxLoginBtn.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}




-(void)setupSubViews{

    WS(wself);
    self.backgroundColor = [UIColor clearColor];

    _backImageView = [[UIImageView alloc] init];
    _backImageView.image = [UIImage imageNamed:@""];
    _backImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:_backImageView];
    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
    }];
    
    _blackView = [[UIView alloc] init];
    [_blackView setBackgroundColor:[UIColor blackColor]];
    _blackView.alpha = 0.6;
    [self addSubview:_blackView];
    [_blackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
    }];
    
    
    _pointLbael = [[UILabel alloc] init];
    _pointLbael.backgroundColor = CLEARCOLOR;
    _pointLbael.text = @"本影片为订购影片!";
    _pointLbael.font = [UIFont systemFontOfSize:17.0f];
    _pointLbael.textColor = [UIColor whiteColor];
    _pointLbael.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_pointLbael];
    
    [_pointLbael mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(230, 30*kRateSize));
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY).offset(-15*kRateSize);
    }];
    
    
    _actionBtn = [[UIButton_Block alloc] init];
    _actionBtn.backgroundColor = CLEARCOLOR;
    [_actionBtn setTitle:@"订购" forState:UIControlStateNormal];
    [_actionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [_actionBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    _actionBtn.layer.borderWidth = 1.0f;
    _actionBtn.layer.borderColor = [App_selected_color CGColor];
    _actionBtn.layer.cornerRadius = 3.0f;
    _actionBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.maskViewReturnBlock) {
            NSLog(@"点击了订购按钮~~");
            wself.maskViewReturnBlock();
            
        }else
        {
            NSLog(@"没有实现订购Block");
        }
        
    };
    [self addSubview:_actionBtn];
    
    [_actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 33));
        make.top.mas_equalTo(_pointLbael.mas_bottom).offset(12*kRateSize);
        make.centerX.mas_equalTo(wself.mas_centerX);
        
    }];
    
    _playBtn = [[UIButton_Block alloc] init];
    [_playBtn setImage:[UIImage imageNamed:@"btn_player_pause_normal"] forState:UIControlStateNormal];
    _playBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.maskViewReturnBlock) {
            wself.maskViewReturnBlock();
        }else{
        
        }
        
    };
    [self addSubview:_playBtn];
    
    [_playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.center.mas_equalTo(wself);
    }];
    
    _retryBtn = [[UIButton_Block alloc] init];
    [_retryBtn setImage:[UIImage imageNamed:@"btn_player_reload"] forState:UIControlStateNormal];
    _retryBtn.hidden = YES;
    _retryBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.retryAction) {
            wself.retryAction();
        }else{
        
        }
        
    };
    [self addSubview:_retryBtn];
    
    [_retryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(92, 25));
        make.centerY.mas_equalTo(_pointLbael.mas_centerY).offset(35);
        make.centerX.mas_equalTo(_pointLbael.mas_centerX);
    }];
    
    //报装
    _installBtn = [[UIButton_Block alloc]init];
    [self addSubview:_installBtn];
    _installBtn.backgroundColor = CLEARCOLOR;
    [_installBtn setTitle:@"我要报装" forState:UIControlStateNormal];
    [_installBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [_installBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    _installBtn.layer.borderWidth = 1.0f;
    _installBtn.layer.borderColor = [App_selected_color CGColor];
    _installBtn.layer.cornerRadius = 3.0f;
     _installBtn.titleLabel.font = App_font(11);
    [_installBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 31*kRateSize));
        make.top.mas_equalTo(_pointLbael.mas_bottom).offset(12*kRateSize);
        make.centerX.mas_equalTo(wself.mas_centerX);

    }];
    
    
    
    
    _installBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.maskViewReturnBlock) {
            NSLog(@"点击了报装按钮~~");
            wself.maskViewReturnBlock();
                        
        }else
        {
            NSLog(@"没有实现订购Block");
        }
        
    };


    
    
}

-(void)setProgramType:(NSInteger)programType{

    WS(wself);
    switch (programType) {
        case PlayBtnType:
        {//这是只有暂停按钮的视图
            _pointLbael.hidden = YES;
            _actionBtn.hidden = YES;
            _playBtn.hidden = NO;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            
            break;
        }
        case MovieOrderType:
        {//电影
            _retryBtn.hidden = YES;
            _pointLbael.hidden = NO;
            _actionBtn.hidden = NO;
            _playBtn.hidden = YES;
            _pointLbael.text = @"本影片为订购影片!";
            _installBtn.hidden = YES;
//            [_pointLbael mas_remakeConstraints:^(MASConstraintMaker *make) {
//                
//                make.size.mas_equalTo(CGSizeMake(200, 30*kRateSize));
//                make.centerX.mas_equalTo(wself.mas_centerX);
//                make.centerY.mas_equalTo(wself.mas_centerY).offset(-15*kRateSize);
//            }];
//            [_actionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//                
//                make.size.mas_equalTo(CGSizeMake(60, 33));
//                make.top.mas_equalTo(_pointLbael.mas_bottom).offset(12*kRateSize);
//                make.centerX.mas_equalTo(wself.centerX);
//                
//            }];
            
            break;
        }
        case TelePlayOrderType:
        {//电视剧
            _pointLbael.hidden = NO;
            _actionBtn.hidden = NO;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"本剧集为订购剧集!";
//            [_pointLbael mas_remakeConstraints:^(MASConstraintMaker *make) {
//                
//                make.size.mas_equalTo(CGSizeMake(200, 30*kRateSize));
//                make.centerX.mas_equalTo(wself.mas_centerX);
//                make.centerY.mas_equalTo(wself.mas_centerY).offset(-15*kRateSize);
//            }];
//            [_actionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//                
//                make.size.mas_equalTo(CGSizeMake(60, 33));
//                make.top.mas_equalTo(_pointLbael.mas_bottom).offset(12*kRateSize);
//                make.centerX.mas_equalTo(wself.centerX);
//                
//            }];
            break;
        }
        case LoginActionType:
        {//登录
            _pointLbael.hidden = NO;
            _actionBtn.hidden = NO;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"后才可观看";
            [_actionBtn setTitle:@"登录" forState:UIControlStateNormal];

            [_pointLbael mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(CGSizeMake(230, 30*kRateSize));
                make.centerX.mas_equalTo(wself.mas_centerX).offset(20);
                make.centerY.mas_equalTo(wself.mas_centerY).offset(0);
            }];
            [_actionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(CGSizeMake(60, 33));
                make.centerY.mas_equalTo(_pointLbael.mas_centerY);
                make.centerX.mas_equalTo(wself.centerX).offset(-55);
                
            }];
            
            break;
        }
        case DemandLoginType:
        {//
            _pointLbael.hidden = NO;
            _actionBtn.hidden = NO;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"后才可观看";

            break;
        }
        case InstallType:
        {
            _pointLbael.hidden = NO;
            _installBtn.hidden = NO;
            _actionBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _pointLbael.text = @"连接广电宽带网络 观看精彩视频";
            _pointLbael.font = App_font(12);
//            _pointLbael.textColor = UIColorFromRGB(0x000000);
            break;
        }
            
            
        case ZhuanWangViewType:
        {//
            _pointLbael.hidden = NO;
            _actionBtn.hidden = NO;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"尊敬的用户\n您当前连接的网络暂未开通此服务";
            [_pointLbael setNumberOfLines:30];
            [_pointLbael setTextColor:UIColorFromRGB(0xffffff)];
            _pointLbael.textAlignment = NSTextAlignmentCenter;
            _pointLbael.lineBreakMode = NSLineBreakByWordWrapping;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_pointLbael.text];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:8];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_pointLbael.text length])];
            _pointLbael.attributedText = attributedString;
            [_pointLbael sizeToFit];
            _pointLbael.preferredMaxLayoutWidth = 280;
            [_pointLbael setBackgroundColor:[UIColor clearColor]];
            [_pointLbael mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(wself.mas_centerY).offset(-20*kRateSize);
                make.centerX.mas_equalTo(wself.mas_centerX);
//                make.top.equalTo(wself).offset(109*kRateSize);
//                make.left.mas_equalTo(6*kRateSize);
//                make.height.mas_equalTo(60);
            }];
            _actionBtn.showsTouchWhenHighlighted = YES;
            [_actionBtn setTitle:@"查看更多免费WiFi场所" forState:UIControlStateNormal];
            [_actionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_actionBtn setBackgroundColor:App_selected_color];
//            [_actionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//            [_actionBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
//            _actionBtn.layer.borderWidth = 1.0f;
//            _actionBtn.layer.borderColor = [App_selected_color CGColor];
            [_actionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(CGSizeMake(200*kRateSize, 33*kRateSize));
                make.top.mas_equalTo(_pointLbael.mas_bottom).offset(20*kRateSize);;
                make.centerX.mas_equalTo(wself.centerX).offset(0*kRateSize);
            }];
            
            
            
            
//            _pointLbael.hidden = NO;
//            _actionBtn.hidden = YES;
//            _playBtn.hidden = YES;
//            _retryBtn.hidden = YES;
//            
//            _pointLbael.text = @"接入广电网络才可观看";
//            [_pointLbael mas_updateConstraints:^(MASConstraintMaker *make) {
//                
//                make.centerY.mas_equalTo(wself.mas_centerY);
//            }];

            
            
            
            
            break;
        }
        case LoadingViewType:
        {//
//            _loadingView  = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
//            _loadingView = [[LoadingView alloc]init];
//            [self addSubview:_loadingView];
////            _loadingView = [[PolyMedicineLoadingView alloc]init];
////            [self addSubview:_loadingView];
//            CGSize loadingSize = CGSizeMake(200*kRateSize, 60*kRateSize);
            _loadingView  = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
            [self addSubview:_loadingView];
            CGSize loadingSize = CGSizeMake(65, 65);
            [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(loadingSize);
                make.centerX.mas_equalTo(wself.centerX);
                make.centerY.mas_equalTo(wself.centerY);
//                make.left.mas_equalTo(wself).offset(200*kRateSize);
//                make.top.mas_equalTo(wself).offset(110*kRateSize);
                
            }];
            
            _progressLabel = [[UILabel alloc] init];
            [_progressLabel setBackgroundColor:CLEARCOLOR];
            [_progressLabel setTextColor:[UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1]];
            [_progressLabel setFont:[UIFont systemFontOfSize:13.0f]];
            [_progressLabel setText:@"10%"];
            [_progressLabel setHidden:YES];
            [_progressLabel setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:_progressLabel];
            
            [_progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(40, 40));
                make.centerX.mas_equalTo(_loadingView.mas_centerX);
                make.centerY.mas_equalTo(_loadingView.mas_centerY);
            }];
            
            if (_resetProgressTimer) {
                [_resetProgressTimer invalidate];
                _resetProgressTimer = nil;
            }
//            _resetProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(resetProgressWith:) userInfo:nil repeats:YES];
            
            _pointLbael.hidden = YES;
            _actionBtn.hidden = YES;
            _playBtn.hidden = YES;
            _retryBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"登录后才可观看 马上去";
            _loadingView.hidden = NO;
            _backImageView.image = [UIImage imageNamed:@"bg_player_share_masking"];
            [_blackView setBackgroundColor:[UIColor clearColor]];

//            _backImageView.alpha = 0.6;

            break;
        }
        case NetBreakedType:{
            _retryBtn.hidden = NO;
            _pointLbael.hidden = NO;
            _actionBtn.hidden = YES;
            _playBtn.hidden = YES;
            _installBtn.hidden = YES;
            _pointLbael.text = @"网络已断开，请检查网络设置";
            [_pointLbael mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(wself.mas_centerY).offset(-20*kRateSize);
            }];

            break;
        }
            
            
        default:
            break;
    }

}

-(void)resetProgressWith:(CGFloat)progressValue{
    
    
    if (_progressLabel) {
        
        NSMutableString *progressString = [NSMutableString stringWithFormat:@"%@",_progressLabel.text];
        if (progressString.length>=2) {

            if (![progressString rangeOfString:@"%"].location == NSNotFound) {
                [progressString deleteCharactersInRange:NSMakeRange([progressString rangeOfString:@"%"].location, 1)];
            }
            
            if ([progressString integerValue]>94) {
                [_resetProgressTimer invalidate];
                _resetProgressTimer = nil;
                return;
            }
        }
        
        progressValue = [progressString integerValue] + arc4random()%5;
        _progressLabel.text = [NSString stringWithFormat:@"%d%%",(int)progressValue];
        NSLog(@"缓冲进度 -- %@",_progressLabel.text);
        
    }else{
    
    }
}

-(void)clearProgressWithStatus:(BOOL)isStart{
    NSString *value;
   
    if (isStart) {
        
        value = [NSString stringWithFormat:@"%d%%",10+arc4random()%4];
        _progressLabel.text = value;
        NSLog(@"随机初始值 %@",value);

        if (_resetProgressTimer) {
            [_resetProgressTimer invalidate];
            _resetProgressTimer = nil;
        }
//        _resetProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(resetProgressWith:) userInfo:nil repeats:YES];

    }else{
        value = [NSString  stringWithFormat:@"%d%%",94+arc4random()%5];
        _progressLabel.text = value;
        NSLog(@"随机最终值 %@",value);

    }
}

-(void)setHidden:(BOOL)hidden{
    
    
    
    if ((_programType == LoadingViewType) && (hidden==NO) ) {
        [self clearProgressWithStatus:YES];
        NSLog(@"显示的时候随机初始值 %@",_progressLabel.text);

    }else if ((_programType == LoadingViewType)&&(hidden==YES)){
        [self clearProgressWithStatus:NO];
        NSLog(@"结束的时候随机结束值 %@",_progressLabel.text);

    }
    
    if (_loadingView) {
        if (_loadingView.hidden==hidden) {
            if (hidden) {
                NSLog(@"loading视图隐藏重复了~");
            }else{
                NSLog(@"loading视图显示重复了~");
            }
            _loadingView.hidden = hidden;

            return;
        }else{
            _loadingView.hidden = hidden;
            
        }
    }else{
        NSLog(@"还没创建loading视图呢~");
    }
    
    [super setHidden:hidden];
  
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
