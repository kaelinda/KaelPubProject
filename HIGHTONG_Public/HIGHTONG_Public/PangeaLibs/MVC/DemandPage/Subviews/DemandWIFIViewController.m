//
//  DemandWIFIViewController.m
//  HIGHTONG_Public
//
//  Created by 赖利波 on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DemandWIFIViewController.h"
#import "AlertLabel.h"
@interface DemandWIFIViewController ()
@property (nonatomic,strong)UIButton *backBtn;

@end

@implementation DemandWIFIViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:NO];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:NO];
    if (_isVertical == YES) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }else{
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }
 
}
//-(void)setPageName:(NSString *)pageName{
//    _pageName = [pageName copy];
//    for (UIView *labelView in [self.view subviews]) {
//        if (labelView.tag == 99) {
//
//            UILabel *nameLabel = (UILabel *)labelView;
//            nameLabel.text = pageName;
//        
//        }
//    }
//
//}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self hideNaviBar:YES];
    WS(wself);
    float widht;
    float hight;
    
    if (_isVertical == YES)
    {
        widht = kDeviceWidth;
        hight = KDeviceHeight;
    }else{
        widht = KDeviceHeight;
        hight = kDeviceWidth;
    }
    [self.view setBackgroundColor:App_selected_color];
    UILabel *titleLabelWIFI = [[UILabel alloc]init];
    [titleLabelWIFI setText:@"更多免费WIFI场所"];
    if (_pageName) {
        [titleLabelWIFI setText:_pageName];

    }
    [titleLabelWIFI setTextAlignment:NSTextAlignmentCenter];
    [titleLabelWIFI setBackgroundColor:[UIColor clearColor]];
    [titleLabelWIFI setTextColor:UIColorFromRGB(0xffffff)];
    [titleLabelWIFI setTag:99];
    [self.view addSubview:titleLabelWIFI];
    [titleLabelWIFI mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(25);
        //        make.left.mas_equalTo((widht-200)/2);
        make.size.mas_equalTo(CGSizeMake(200, 30));
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    _backBtn = [[UIButton alloc]init];
    [_backBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(returnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backBtn];
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5*kRateSize);
        make.top.mas_equalTo(15*kRateSize);
        make.size.mas_equalTo(CGSizeMake(54, 44));
    }];
    
    //    _backBtn = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"btn_common_title_back_normal" imgHighlight:@"btn_back_click" target:self action:@selector(returnClick:)];
    ////    UILabel *titleLabel = [[UILabel alloc]init];
    ////    [HT_Naviga]
    //    UIView *viewd = [[UIView alloc]init];
    //    [viewd setBackgroundColor:[UIColor redColor]];
    //    [self setNaviBarLeftBtn:_backBtn];
    ////    [self setNaviBarSearchView:viewd];
    //    [self setNaviBarRightBtn:nil];
    //    [self setNaviBarTitle:@"更多免费WIFI场所"];
    
    _detailADWebView  = [[UIWebView alloc]init];
    WS(wss);
    [_detailADWebView setUserInteractionEnabled:YES];//是否支持交互
    _detailADWebView.delegate=self;
    _detailADWebView.backgroundColor= App_background_color;
    [_detailADWebView setOpaque:NO];//opaque是不透明的意思
    [_detailADWebView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:_detailADWebView];
    [_detailADWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top).offset(64);//Kael_change
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.bottom.equalTo(wss.view.mas_bottom);
    }];
    
    
    
    //    _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    //显示的文字
    //    _HUD.labelText = @"加载中...";
    //    //是否有庶罩
    //    _HUD.dimBackground = NO;
    //    [_HUD show:YES];
    //加载网页的方式
    //1.创建并加载远程网页
    if (!isEmptyStringOrNilOrNull(_detailADURL)) {
        //        NSURL *url = [NSURL URLWithString:_detailADURL];
        
        [_detailADWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_detailADURL]]];
        [self showCustomeHUD];
        
    }else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //这里是1.0f后 你要做的事情
            [AlertLabel AlertInfoWithText:@"加载失败" andWith:self.view withLocationTag:0];
            
        });
        
    }
    
    
    //    UIButton *returnBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 25, 30, 30)];
    //    [returnBtn setImage:[UIImage imageNamed:@"btn_ad_black_back"] forState:UIControlStateNormal];
    //    [returnBtn addTarget:self action:@selector(returnClick:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:returnBtn];
    
    
    
    


}
-(void)returnClick:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self hiddenCustomHUD];
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

-(BOOL)shouldAutorotate
{
    //    if (_isVertical == YES) {
    return NO;
    //    }
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

@end
