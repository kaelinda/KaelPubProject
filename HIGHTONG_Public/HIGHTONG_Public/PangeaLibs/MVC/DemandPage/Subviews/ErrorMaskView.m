//
//  ErrorMaskView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/1/27.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ErrorMaskView.h"

@implementation ErrorMaskView


-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }
    
    return self;

}


-(void)setupSubViews{
    WS(wself);
    self.backgroundColor = [UIColor lightGrayColor];
    
    //背景图
    _backImageView = [[UIImageView alloc] init];
//    _backImageView.image = [UIImage imageNamed:@"blackMask"];
    _backImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:_backImageView];
    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(wself);
    }];
    
    
    
    
    //提示语
    _pointLbael = [[UILabel alloc] init];
    _pointLbael.backgroundColor = CLEARCOLOR;
    _pointLbael.text = @"加载失败";
    _pointLbael.font = [UIFont systemFontOfSize:15.0f];
    _pointLbael.textColor = [UIColor whiteColor];
    _pointLbael.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_pointLbael];
    
    [_pointLbael mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(250, 30*kRateSize));
        make.centerY.mas_equalTo(wself.mas_centerY).offset(-10);
        make.centerX.mas_equalTo(wself.mas_centerX);
    }];

    //重试按钮
    _actionBtn = [[UIButton_Block alloc] init];
    [_actionBtn setImage:[UIImage imageNamed:@"btn_player_reload"] forState:UIControlStateNormal];
    _actionBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.retryAction) {
            NSLog(@"点击了重试按钮~~");
            wself.retryAction();
            
        }else
        {
            NSLog(@"没有实现重试Block");
        }
        
    };
    [self addSubview:_actionBtn];
    
    [_actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(92, 25));
        make.centerX.mas_equalTo(wself.mas_centerX);
//        make.top.mas_equalTo(_pointLbael.bottom).offset(10);
        make.centerY.mas_equalTo(wself.mas_centerY).offset(20);
        
    }];

    
    
}










/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
