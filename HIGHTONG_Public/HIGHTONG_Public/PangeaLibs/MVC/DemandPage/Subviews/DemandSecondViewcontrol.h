//
//  DemandSecondViewcontrol.h
//  HIGHTONG
//
//  Created by testteam on 15/8/20.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
//#import "HT_FatherBarViewcontroller.h"
@interface DemandSecondViewcontrol : HT_FatherViewController

@property (nonatomic,assign)BOOL isMovie;//是电影类型

@property (nonatomic,assign)BOOL hidenChoose;//隐藏筛选按钮

@property (nonatomic,assign)BOOL isTopic;//是主题节目列表
@property (nonatomic,copy)NSString *TopicCode;
@property (nonatomic,copy)NSString *disPlayDemandType;
@property (nonatomic,copy)NSString *operationCode;
@property (nonatomic,copy)NSString *categoryID;
@property (nonatomic,copy)NSString *name;
/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;

@end
