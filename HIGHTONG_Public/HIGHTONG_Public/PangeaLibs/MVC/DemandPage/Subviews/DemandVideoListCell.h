//
//  DemandVideoListCell.h
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemandVideoListCell : UITableViewCell


@property(nonatomic,strong)UILabel *videoTitleLabel;
@property (strong,nonatomic) UIImageView *seperateLine;



@end
