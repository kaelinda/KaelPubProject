//
//  DemandADDetailViewController.m
//  HIGHTONG_Public
//
//  Created by 赖利波 on 16/2/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "DemandADDetailViewController.h"
@implementation DemandADDetailViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_isVertical == YES) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }else{
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self.view setBackgroundColor:App_selected_color];
    _detailADWebView  = [[UIWebView alloc]init];
    WS(wss);
    [_detailADWebView setUserInteractionEnabled:YES];//是否支持交互
    _detailADWebView.delegate=self;
    [_detailADWebView setOpaque:NO];//opaque是不透明的意思
    [_detailADWebView setScalesPageToFit:YES];//自动缩放以适应屏幕
    [self.view addSubview:_detailADWebView];
    [_detailADWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wss.view.mas_top).offset(20);//Kael_change
        make.width.equalTo(wss.view.mas_width);
        make.centerX.equalTo(wss.view.mas_centerX);
        make.bottom.equalTo(wss.view.mas_bottom);
    }];
    //加载网页的方式
    //1.创建并加载远程网页
    NSURL *url = [NSURL URLWithString:_detailADURL];
    [_detailADWebView loadRequest:[NSURLRequest requestWithURL:url]];
    
    _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //显示的文字
//    _HUD.labelText = @"加载中...";
    
    
    FLAnimatedImageView *animationView = [[FLAnimatedImageView alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gif_loading_pageCenter" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:gifData];
    animationView.animatedImage = image;
    animationView.frame = CGRectMake(0, 0, 100, 100);
    _HUD.customView = animationView;
    _HUD.mode = MBProgressHUDModeCustomView;
    _HUD.animationType = MBProgressHUDAnimationFade;
    
//    _HUD.delegate = self;
    _HUD.color = [UIColor clearColor];//这儿表示无背景
    
    //是否有庶罩
//    _HUD.dimBackground = NO;
    [_HUD show:YES];
    
    UIButton *returnBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 25, 30, 30)];
    [returnBtn setImage:[UIImage imageNamed:@"btn_ad_black_back"] forState:UIControlStateNormal];
    [returnBtn addTarget:self action:@selector(returnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:returnBtn];
    
}


-(void)returnClick:(UIButton*)sender
{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



-(void)webViewDidFinishLoad:(UIWebView *)webView{
    if (_HUD) {
        [_HUD hide:YES];
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(BOOL)shouldAutorotate
{
    //    if (_isVertical == YES) {
    return NO;
    //    }
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

@end
