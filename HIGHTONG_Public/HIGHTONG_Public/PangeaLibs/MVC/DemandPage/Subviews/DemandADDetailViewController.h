//
//  DemandADDetailViewController.h
//  HIGHTONG_Public
//
//  Created by 赖利波 on 16/2/23.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <UIKit/UIKit.h>
@interface DemandADDetailViewController : UIViewController<UIWebViewDelegate>
{
    MBProgressHUD *_HUD;
    
}
@property (nonatomic,strong)UIWebView *detailADWebView;
@property (nonatomic,strong)NSString *detailADURL;
@property (nonatomic,strong)UIView *opaqueView;
@property (nonatomic,assign)BOOL isVertical;
@property (nonatomic,assign)BOOL isVerticalDerection;

@property (nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;

@end
