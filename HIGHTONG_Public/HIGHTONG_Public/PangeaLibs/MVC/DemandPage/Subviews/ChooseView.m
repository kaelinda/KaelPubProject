//
//  ChooseView.m
//  HIGHTONG
//
//  Created by testteam on 15/8/21.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ChooseView.h"
#import "Choose_Scroll.h"
#define H_jiange 40
#define HangJianGe 5
#define HangHigh 26
@implementation ChooseView

- (instancetype)init
{
    if (self = [super init]) {
        self.Allarray = [NSMutableArray array];
        
    }
    return self;
}
- (instancetype)initWithArray:(NSArray*)array
{
    if ( self = [super init]) {
        
        self.Allarray = [NSMutableArray arrayWithArray:array];
        
        
    }
    return self;
}


- (void)upDataViewWithArray:(NSDictionary*)diction;
{
    
    NSString *count = [diction objectForKey:@"count"];
    NSString *parentID = [diction objectForKey:@"parentID"];
    NSArray *typeList = [diction objectForKey:@"typeList"];
    
    Choose_Scroll *BIGLast ;
    //最热门、新上架
    NSDictionary *HOT = @{@"id":@"0",@"categoryID":@"RE",@"name":@"REMEN",@"typeList":@[
                                  @{@"categoryID":@"RE",@"name":@"新上架",@"id":@"1"},
                                  @{@"categoryID":@"ZUI",@"name":@"最热门",@"id":@"2"}
                                  ]};
    [self removeAllSubviews];
    UIView *contentView = [UIView new];
    [self addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
        make.width.equalTo(self);
    }];
    
    Choose_Scroll *FIRST = [Choose_Scroll new];
    [contentView addSubview:FIRST];
    [FIRST mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_top).offset(HangJianGe*kRateSize);
        make.height.equalTo(@(HangHigh*kRateSize));
        make.left.equalTo(contentView.mas_left);
        make.right.equalTo(contentView.mas_right);
    }];
    [FIRST upDataViewWithArray:HOT];
    BIGLast = FIRST;
    FIRST.Scroll_Block= ^(NSDictionary*dic,NSString *indexOfRow)
    {
//        NSLog(@"上架某%d个scrollView 选择了 %@ %@ %@",index-1,[dic objectForKey:@"id"],[dic objectForKey:@"categoryID"],[dic objectForKey:@"name"]);
        if (_Choose_Block) {
            _Choose_Block(dic,indexOfRow);
        }else
        {
            NSLog(@"controller还没有准备好接收字典");
        }
    };
    
    //下边的三个类型年份区域
    
    
    
    if (typeList.count) {
        NSInteger index = 0;
        Choose_Scroll *last;
        for (NSDictionary *diction in typeList) {
            
            Choose_Scroll *scroll = [Choose_Scroll new];
            [self addSubview:scroll];
            [scroll mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(last?(last.mas_bottom):contentView.mas_top).offset(last ? (HangJianGe*kRateSize ): (2*HangJianGe+HangHigh)*kRateSize);
                make.height.equalTo(@(HangHigh*kRateSize));
                make.left.equalTo(contentView.mas_left);
                make.right.equalTo(contentView.mas_right);
            }];
            [scroll upDataViewWithArray:diction];
            scroll.showsHorizontalScrollIndicator = NO;
            scroll.showsVerticalScrollIndicator = NO;
            last = scroll;
            index++;
            scroll.Scroll_Block= ^(NSDictionary*dic,NSString*indexOfRow)
            {
                //                 NSLog(@"某%d个scrollView 选择了 %@ %@ %@",index-1,[dic objectForKey:@"id"],[dic objectForKey:@"categoryID"],[dic objectForKey:@"name"]);
                if (_Choose_Block) {
                    _Choose_Block(dic,indexOfRow);
                }else
                {
                    NSLog(@"controller还没有准备好接收字典");
                }
            };
            BIGLast = scroll;
        }
        NSLog(@"筛选列表有数据 数量%@ parentID%@ ",count,parentID);
    }else
    {
        //没有要填充的btn行
        NSLog(@"筛选列表没有数据");
    }
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(BIGLast.mas_bottom);
    }];
    
}


@end
