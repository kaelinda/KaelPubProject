//
//  TVADMaskView.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/2/19.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"
#import "MyGesture.h"
#import "UIImageView+AFNetworking.h"

typedef void (^TVADMaskViewRoatReturn)(BOOL isFullWindown);
typedef void (^TVADDetailReturn) (NSString *url);
typedef void (^TVADMskVolumReturn) (BOOL isSilence);
typedef void (^TVADMaskViewBackReturn)(BOOL isBack);
typedef void (^TVADMaskViewEndReturn)(BOOL isEnd);


@interface TVADMaskView : UIView

/**
 *  数据部分
 */
@property (nonatomic,copy  ) NSString       *ADDetailLink;//广告详情 连接
@property (nonatomic,copy  ) NSString       *ADImageLink;//广告展示图片连接
@property (nonatomic,strong) NSMutableArray *ADDataArray;//广告图片 数组
 @property (nonatomic,strong) NSTimer *CountTimer;

/**
 *  控件部分
 */
@property (nonatomic,strong) UIScrollView   *ADImagesView;//广告滑动视图
@property (nonatomic,strong) UIView         *ADScrollFillView;//填充视图
@property (nonatomic,strong) UIImageView    *backImageView;//背景视图
@property (nonatomic,strong) UIButton_Block *backBtn;//返回按钮
@property (nonatomic,strong) UIButton_Block *volumeBtn;//静音按钮
@property (nonatomic,strong) UIButton_Block *rotateBtn;//旋转屏幕按钮
@property (nonatomic,strong) UILabel        *timeLabel;//视频倒计时
@property (nonatomic,strong) UILabel        *realizeDetailLabel;//了解详情


/**
 *  回调部分
 */
@property (nonatomic,copy) TVADMskVolumReturn volumeBlock;//静音响应事件
@property (nonatomic,copy) TVADMaskViewRoatReturn rotateBlock;//旋屏响应事件
@property (nonatomic,copy) TVADDetailReturn   ADDetailBlock;//跳转广告响应事件
@property (nonatomic,copy) TVADMaskViewBackReturn backBlcok;//返回按钮
@property (nonatomic,copy) TVADMaskViewEndReturn ADEndBlock;//广告结束

/**
 *  广告遮罩类型
 */
@property (nonatomic,assign) TVADMaskViewType maskViewType;//广告遮罩视图类型
@property (nonatomic,assign) BOOL isCanContinu;//广告倒计时是否递减的开关


-(void)resetvolumBtn:(BOOL)isSilence;
/**
 *  重置广告数据
 *
 *  @param ADArray 广告数组
 */
-(void)resetImageADViewWithData:(NSArray *)ADArray;

-(void)resetVideoADViewWithData:(NSArray *)ADArray;







/**
 *  重新设置广告视图数据
 *
 *  @param ADTime       广告时间
 *  @param ADDetailLink 广告详情连接
 *  @param ADImageLink  广告图片连接
 */
-(void)reSetImageADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink andADImsgeLink:(NSString *)ADImageLink;


/**
 *  广告是图片数组时 重置广告
 *
 *  @param ADTime       广告持续时间
 *  @param ADDetailLink 广告详情连接
 *  @param ADArray      广告数组
 */
-(void)reSetImageADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink andADImsgeArray:(NSArray *)ADArray;

/**
 *  重新设置广告视频视图数据
 *
 *  @param ADTime       广告持续时间
 *  @param ADDetailLink 广告详情连接
 */

-(void)reSetVideoADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink;


@end
