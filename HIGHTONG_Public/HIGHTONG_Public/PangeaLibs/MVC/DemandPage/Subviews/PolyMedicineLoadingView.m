//
//  PolyMedicineLoadingView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/5/18.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "PolyMedicineLoadingView.h"

@implementation PolyMedicineLoadingView

-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
        
    }
    
    
    return self;
}
-(instancetype)initWithImage:(UIImage *)image{
    self = [super initWithImage:image];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

-(void)setupSubviews{
    
    
    //    // 对Y轴进行旋转（指定Z轴的话，就和UIView的动画一样绕中心旋转）
    //    _animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //
    //    // 设定动画选项
    //    _animation.duration = 2; // 持续时间
    //    _animation.repeatCount = CGFLOAT_MAX; // 重复次数
    //    // 设定旋转角度
    //    _animation.fromValue = [NSNumber numberWithFloat:0.0]; // 起始角度
    //    _animation.toValue = [NSNumber numberWithFloat: 2*M_PI]; // 终止角度
    //
    //    // 添加动画
    //    [self.layer addAnimation:_animation forKey:@"rotate-layer"];
    
    
    WS(wself);
    _loadingBV = [[UIImageView alloc]init];
    [_loadingBV setImage:[UIImage imageNamed:@"ic_player_loading_upicon"]];
    [wself addSubview:_loadingBV];
    _loadingAV = [[UIActivityIndicatorView alloc]init];
    _loadingAV.color = App_selected_color;;
    //     [_loadingAV setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];//设置进度轮显示类型
    [wself addSubview:_loadingAV];
    
    _loadingLabel = [[UILabel alloc]init];
    [_loadingLabel setBackgroundColor:[UIColor clearColor]];
    [_loadingLabel setFont:[UIFont systemFontOfSize:13*kRateSize]];
    [_loadingLabel setTextColor:App_selected_color];
    _loadingLabel.text = @"加 载 中";
    [wself addSubview:_loadingLabel];
    
    
    
    [_loadingBV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself);
        make.top.mas_equalTo(wself);
        make.size.mas_equalTo(CGSizeMake(150*kRateSize, 45*kRateSize));
        
    }];
    
    
    [_loadingAV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_loadingBV.mas_bottom).offset(0*kRateSize);
        make.left.mas_equalTo(_loadingBV.mas_left).offset(30*kRateSize);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize, 30*kRateSize));
        
    }];
    
    [_loadingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_loadingAV.mas_right).offset(5*kRateSize);
        make.top.mas_equalTo(_loadingBV.mas_bottom).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(150*kRateSize, 30*kRateSize));
        
    }];
    
    
    
    
}

-(void)setHidden:(BOOL)hidden{
    [super setHidden:hidden];
    if (hidden) {
        [_loadingAV stopAnimating];
        //        [self.layer removeAnimationForKey:@"rotate-layer"];
    }else
    {
        [_loadingAV startAnimating];
        //        [self.layer addAnimation:_animation forKey:@"rotate-layer"];
    }
}


@end
