//
//  DemandWIFIViewController.h
//  HIGHTONG_Public
//
//  Created by 赖利波 on 16/4/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HT_FatherViewController.h"
#import <UIKit/UIKit.h>
@interface DemandWIFIViewController : HT_FatherViewController<UIWebViewDelegate>
{
    MBProgressHUD *_HUD;
    
}
@property (nonatomic,strong)UIWebView *detailADWebView;
@property (nonatomic,copy)NSString *detailADURL;
@property (nonatomic,strong)UIView *opaqueView;
@property (nonatomic,assign)BOOL isVertical;
@property (nonatomic,assign)BOOL isVerticalDerection;
/**
 *  界面名称
 */
@property (nonatomic,copy) NSString *pageName;


@property (nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;
@end
