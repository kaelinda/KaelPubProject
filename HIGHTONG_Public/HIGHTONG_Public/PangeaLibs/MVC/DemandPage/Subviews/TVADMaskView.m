//
//  TVADMaskView.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/2/19.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "TVADMaskView.h"

@implementation TVADMaskView
{
//    NSTimer *_CountTimer;
    NSInteger _timeCount;
    UIView *ADContentView;
    NSInteger _selectedIndex;
    MyGesture *_imagedetailTap;

}

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];//视图背景色透明
        [self setupSubViews];
        _isCanContinu = YES;
    }
    return self;
}
- (void)setupSubViews{
    WS(wself);
    //背景视图************************************
    _backImageView = [[UIImageView alloc] init];
    _backImageView.image = [UIImage imageNamed:@""];//健康电视要求
    _backImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:_backImageView];
    
    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself);
    }];
    
/*
 *弃用该控件
 _ADImagesView = [[UIScrollView alloc] init];
 _ADImagesView.pagingEnabled = YES;
 _ADImagesView.scrollEnabled = NO;
 [_ADImagesView setBackgroundColor:CLEARCOLOR];
 [self addSubview:_ADImagesView];
 
 [_ADImagesView mas_makeConstraints:^(MASConstraintMaker *make) {
 
 make.edges.mas_equalTo(wself);
 
 }];
 */
    //************ 背景黑条 *******
    UIImageView *blackImage = [[UIImageView alloc] init];
    [blackImage setBackgroundColor:CLEARCOLOR];
    [blackImage setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
    [self addSubview:blackImage];
    
    [blackImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(60*kRateSize);
        make.bottom.mas_equalTo(wself.mas_bottom);
        make.left.mas_equalTo(wself.mas_left);
        make.right.mas_equalTo(wself.mas_right);
    }];
    //静音按钮***********************************
    _volumeBtn = [[UIButton_Block alloc] init];
    [_volumeBtn setImage:[UIImage imageNamed:@"btn_player_ad_volum"] forState:UIControlStateNormal];
    _volumeBtn.Click = ^(UIButton_Block *btn,NSString *str){
    
        if (!wself.volumeBtn.selected) {
            [wself.volumeBtn setImage:[UIImage imageNamed:@"btn_player_ad_silence"] forState:UIControlStateNormal];
            if (wself.volumeBlock) {
//                NSLog(@"点击了静音按钮~~");
                wself.volumeBlock(YES);
            }else{
                NSLog(@"没有实现静音Block");
            }

        }else{
            [wself.volumeBtn setImage:[UIImage imageNamed:@"btn_player_ad_volum"] forState:UIControlStateNormal];
            if (wself.volumeBlock) {
//                NSLog(@"点击了还原音量按钮~~");
                wself.volumeBlock(NO);
            }else{
                NSLog(@"没有实现静音Block");
            }

        }
        wself.volumeBtn.selected = !wself.volumeBtn.selected;
        
    };
    [self addSubview:_volumeBtn];
    
    [_volumeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.size.mas_equalTo(CGSizeMake(45*kRateSize, 30*kRateSize));
        make.left.mas_equalTo(wself.mas_left);
        make.bottom.mas_equalTo(wself.mas_bottom);
        
    }];
    
    //旋转按钮*********************************
    _rotateBtn = [[UIButton_Block alloc] init];
    [_rotateBtn setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal"] forState:UIControlStateNormal];
    _rotateBtn.Click = ^(UIButton_Block *btn,NSString *str){
    
        if (wself.rotateBlock) {
//            NSLog(@"点击了旋转屏按钮");
            wself.rotateBlock(YES);
        }else{
    
            NSLog(@"没有实现旋转屏幕Block");
        
        }
        
    };
    [self addSubview:_rotateBtn];
    
    [_rotateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(43*kRateSize, 30*kRateSize));
        make.right.mas_equalTo(wself.mas_right);
        make.bottom.mas_equalTo(wself.mas_bottom);
        
    }];
    
    //视频倒计时*************************
    _timeLabel = [[UILabel alloc] init];
    [_timeLabel setTextColor:UIColorFromRGB(0xffffff)];
    [_timeLabel setFont:[UIFont systemFontOfSize:11*kRateSize]];
    [_timeLabel setTextAlignment:NSTextAlignmentCenter];
//    [_timeLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"广告倒计时背景"]]];
    [_timeLabel setBackgroundColor:[UIColor blackColor]];
    [wself addSubview:_timeLabel];
    [_timeLabel setText:@"广告倒计时: "];
    CGSize timaLabelSize = CGSizeMake(100*kRateSize, 22*kRateSize);
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.size.mas_equalTo(timaLabelSize);
        make.top.mas_equalTo(wself.mas_top).offset(20*kRateSize);
        make.right.mas_equalTo(wself.mas_right).offset(-8*kRateSize);
        
    }];
    
    //了解详情label*******************************
    _realizeDetailLabel = [[UILabel alloc] init];
    _realizeDetailLabel.text = @"了解详情 >";
    [_realizeDetailLabel setTextAlignment:NSTextAlignmentRight];
    [_realizeDetailLabel setFont:[UIFont systemFontOfSize:15*kRateSize]];
    [_realizeDetailLabel setTextColor:App_blue_color];
    _realizeDetailLabel.userInteractionEnabled = YES;
    [wself addSubview:_realizeDetailLabel];
    //******************************
    _realizeDetailLabel.hidden = YES;//这个完全可以用Btn来实现的所以在这里先隐藏掉吧
    //******************************
    CGSize detailLabelSize = CGSizeMake(88*kRateSize, 30*kRateSize);
    [_realizeDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(detailLabelSize);
        make.right.mas_equalTo(_rotateBtn.mas_left);
        make.bottom.mas_equalTo(wself.mas_bottom);
        
    }];
    
    UITapGestureRecognizer *detailTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoDetail)];
    detailTap.numberOfTapsRequired = 1;
    [_realizeDetailLabel addGestureRecognizer:detailTap];
    
    //********************************************************
    UIButton_Block *detailBtn = [[UIButton_Block alloc] init];
    [detailBtn setImage:[UIImage imageNamed:@"bg_player_ad_relaise_detail"] forState:UIControlStateNormal];
    detailBtn.Click = ^(UIButton_Block *btn,NSString *str){
        if (wself.ADDetailBlock) {
            wself.ADDetailBlock(_ADDetailLink);
        }else{
            NSLog(@"没实现 进入详情 Block");
        }
    };
    [self addSubview:detailBtn];
    
    
    [detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(detailLabelSize);
        make.right.mas_equalTo(_rotateBtn.mas_left);
        make.bottom.mas_equalTo(wself.mas_bottom);
        
    }];
    
    //返回按钮****************************
    _backBtn = [[UIButton_Block alloc] init];
    [_backBtn setImage:[UIImage imageNamed:@"btn_ad_player_back"] forState:UIControlStateNormal];
    _backBtn.Click = ^(UIButton_Block *btn,NSString *str){
        
        if (wself.backBlcok) {
//            NSLog(@"实现了 广告视图内的返回按钮响应事件");
            wself.backBlcok(YES);
//            NSLog(@"返回按钮的值%ld",_backBtn.tag);
//            if (_backBtn.tag ==0) {
//                
//                if (_CountTimer) {
//                    [_CountTimer invalidate];//先让定时器失效
//                    _CountTimer = nil;//再置空定时器
//                }
//            }
            
            
           
        }else{
        NSLog(@"未实现 返回 Block");
        }
        
    };
    [self addSubview:_backBtn];
    
   
    
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.top.mas_equalTo(wself.mas_top).offset(20*kRateSize);
        make.left.mas_equalTo(wself.mas_left).offset(10*kRateSize);
        
    }];
    
    
    //跳转手势
    _backImageView.userInteractionEnabled = YES;
    _imagedetailTap = [[MyGesture alloc] initWithTarget:self action:@selector(gotoDetail)];
    detailTap.numberOfTapsRequired = 1;
    [_backImageView addGestureRecognizer:_imagedetailTap];
    
    
    UITapGestureRecognizer *viewdetailTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoDetail)];
    detailTap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:viewdetailTap];

    
    
}

/**
 *  设置视图类型
 *
 *  @param maskViewType 试图类型
 */
-(void)setMaskViewType:(TVADMaskViewType)maskViewType{
    _maskViewType = maskViewType;
    switch (maskViewType) {
        case ImageAD://图片广告
        {
            _backImageView.hidden = NO;
            _timeLabel.hidden = NO;
            _volumeBtn.hidden = YES;
            
            break;
        }
        case VideoAD://视频广告
        {
//            _backImageView.image = [UIImage imageNamed:@""];//背景图滞空
            _backImageView.hidden = YES;
            _timeLabel.hidden = NO;
            _volumeBtn.hidden = NO;
            
            break;
        }
        case OtherAD://其他类型 （待开发）
        {
            
            break;
        }

        default:
            break;
    }

}


-(void)resetImageADViewWithData:(NSArray *)ADArray{

    [self setMaskViewType:ImageAD];

    if (ADArray.count<=0) {
        return;
    }
    if (ADArray.count>0) {
        
        NSString *picLink = [[[ADArray objectAtIndex:0] objectForKey:@"pictureRes"] objectForKey:@"picLink"];
        
        if (picLink.length>0) {//图片类型广告
            //清理数组
            [_ADDataArray removeAllObjects];
            //更换数据
            _ADDataArray = [NSMutableArray arrayWithArray:ADArray];
            //先把第一张图片附上 更换图片 是几秒钟之后的事儿
            [_backImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(picLink)] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    self.hidden = YES;
                }else
                {
                    self.hidden = NO;
                }
            }];
            
            
            //计算广告总时长 duration
            _timeCount = 0;
            
            //两个广告的时候跳转一次 三个广告跳转两次 所以应该是ADArray.count-1   最后一张图片，等倒计时为0的时候消失掉就ok了
            for (int i=0; i<ADArray.count-1; i++) {
                NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[ADArray objectAtIndex:i]];
                NSString *duration = [[dic objectForKey:@"pictureRes"] objectForKey:@"duration"];
                _timeCount = [duration integerValue]+_timeCount;
                //        NSLog(@"定时器创建%d-----%ld",i,_timeCount);
                [NSTimer scheduledTimerWithTimeInterval:_timeCount target:self selector:@selector(changeImage:) userInfo:[NSNumber numberWithInt:i] repeats:NO];
            }
            _timeCount = _timeCount + [[[[ADArray lastObject] objectForKey:@"pictureRes"] objectForKey:@"duration"] integerValue];
            
        }
       
        }
    
    
    //倒计时行为
    if (_CountTimer) {
        [_CountTimer invalidate];//先让定时器失效
        _CountTimer = nil;//再置空定时器
    }
    if (_CountTimer==nil) {
      _CountTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(resetTimeLable) userInfo:nil repeats:YES];
    }
    
    _timeLabel.text = [NSString stringWithFormat:@"广告倒计时: %ld秒",(long)_timeCount];
    _timeCount--;
}


//-(void)setIsCanContinu:(BOOL)isCanContinu
//{
//    
//    
//    if (_CountTimer) {
//        [_CountTimer invalidate];//先让定时器失效
//        _CountTimer = nil;//再置空定时器
//    }
//    
//    _CountTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(resetTimeLable) userInfo:nil repeats:YES];
//    
//    _timeLabel.text = [NSString stringWithFormat:@"广告倒计时: %ld秒",(long)_timeCount];
//    
//    
//}


-(void)reSetVideoADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink
{
    //视频类型广告
        //清理数组
    _timeCount = 0;
    _isCanContinu = YES;
    [self setMaskViewType:VideoAD];
    if (_CountTimer) {
        [_CountTimer invalidate];//先让定时器失效
        _CountTimer = nil;//再置空定时器
    }
    _timeCount = ADTime;

    self.timeLabel.text = @"";
    _CountTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(resetTimeLable) userInfo:nil repeats:YES];
    
    _timeLabel.text = [NSString stringWithFormat:@"广告倒计时: %ld秒",(long)_timeCount];

}





-(void)gotoDetail{
    if (self.ADDetailBlock) {
//        NSLog(@"点击了 了解详情 >");
        self.ADDetailBlock(_ADDetailLink);
    }else{
        NSLog(@"还未实现 了解详情 > Block");
        
    }
    
}

/**
 *  重置图片
 *
 *  @param timer 定时器对象
 */
-(void)changeImage:(NSTimer *)timer{
    
    if (!_isCanContinu) {
        return;
    }else{
//        NSLog(@"我要更换图片了~");
    }

    NSNumber *num = [timer userInfo];
    NSInteger imageIndex = [num integerValue];
    NSDictionary *dic = [_ADDataArray objectAtIndex:imageIndex];

    _ADDetailLink = [[dic objectForKey:@"pictureRes"] objectForKey:@"urlLink"];
    [_backImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat([[dic objectForKey:@"pictureRes"] objectForKey:@"picLink"])] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];

}

-(void)resetTimeLable{
    if (!_isCanContinu) {
        
        NSLog(@"不继续了");
        
        return;
    }
    NSString *timeText = [NSString stringWithFormat:@"广告倒计时: %ld秒",(long)_timeCount];
    _timeLabel.text = timeText;
    NSLog(@"timeText广告倒计时 : %@",timeText);
    
    if (_timeCount<=0) {
        [_CountTimer invalidate];
        _CountTimer = nil;
        
        __weak TVADMaskViewEndReturn endBlock = _ADEndBlock;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //这里是1.0f后 你要做的事情
            self.hidden = YES;
            if (_ADEndBlock) {
//                NSLog(@"广告结束了");
                _isCanContinu = NO;
                endBlock(YES);
                if (_CountTimer) {
                    [_CountTimer invalidate];//先让定时器失效
                    _CountTimer = nil;//再置空定时器
                }
                
            }else{
                NSLog(@"广告Block未实现");
            }
            
        });
        return;
    }
    
    _timeCount--;

}

-(void)resetvolumBtn:(BOOL)isSilence{
    if (isSilence) {
        [_volumeBtn setImage:[UIImage imageNamed:@"btn_player_ad_silence"] forState:UIControlStateNormal];
        self.volumeBtn.selected = YES;
        
    }else{
        [_volumeBtn setImage:[UIImage imageNamed:@"btn_player_ad_volum"] forState:UIControlStateNormal];
        self.volumeBtn.selected = NO;
    }

}

#pragma mark- **************** 弃用 *********************
-(void)reSetADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink andADImsgeLink:(NSString *)ADImageLink{
    
    if (_CountTimer) {
        [_CountTimer invalidate];//先让定时器失效
        _CountTimer = nil;//再置空定时器
    }
    _timeCount = ADTime;
    _CountTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(resetTimeLable) userInfo:nil repeats:YES];
    
    _timeLabel.text = [NSString stringWithFormat:@"广告倒计时: %ld秒",(long)_timeCount];
    _timeCount--;
    
    _ADDetailLink = ADDetailLink;//广告详情链接
    
    _ADImageLink = ADImageLink;//广告图片链接
    [_backImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(_ADDetailLink)] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
    
    
    
    
}


/**
 *  重置广告数组
 *
 *  @param ADTime       广告持续时间
 *  @param ADDetailLink 详情连接
 *  @param ADArray      广告数组
 */
-(void)reSetADTime:(NSInteger)ADTime andADDetailLink:(NSString *)ADDetailLink andADImsgeArray:(NSArray *)ADArray{
   // [self resetADViewWithData:ADArray];
}
-(void)gotoWebView:(MyGesture *)tap{
    
    if (_ADDetailBlock) {
        _ADDetailBlock(tap.urlLink);
    }
    
}
//************************************************************
/**
 *  暂时弃用 滑动视图的策略
 *
 *  @param ADListArr 广告数组
 */
-(void)fillADScrllImagesViewWithArray:(NSMutableArray *)ADListArr{
    WS(wself);
    //修改下数据源
    _ADDataArray  = [NSMutableArray arrayWithArray:ADListArr];
    //移除子视图
    [_ADImagesView removeAllSubviews];
    //创建 滑动视图的 承载式图 （masonry的特殊要求）
    if (ADContentView == nil) {
        ADContentView = [[UIView alloc] init];
        [ADContentView setBackgroundColor:CLEARCOLOR];
        [_ADImagesView addSubview:ADContentView];
    }
    
    [ADContentView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.ADImagesView);
        make.height.mas_equalTo(wself.ADImagesView.mas_height);//高度固定
    }];
    //添加新的子视图之前 移除之前的视图
    [ADContentView removeAllSubviews];
    UIImageView *lastImage = [[UIImageView alloc] init];
    for (int i=0; i<_ADDataArray.count; i++) {
        
        UIImageView *ADImageView = [[UIImageView alloc] init];
        NSString *imagelink = [[[_ADDataArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"picLink"];
        [ADContentView addSubview: ADImageView];
        
        [ADImageView sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imagelink)] placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
        
        [ADImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.size.mas_equalTo(_ADImagesView);
            make.top.mas_equalTo(_ADImagesView);
            if (i==0) {
                make.left.mas_equalTo(_ADImagesView);
            }else{
                make.left.mas_equalTo(lastImage.mas_right);
            }
        }];
        if (i>0) {
            lastImage = ADImageView;
        }
        //添加手势 直接将url传出去
        MyGesture *gotoADDetailTap = [[MyGesture alloc] initWithTarget:self action:@selector(gotoWebView:)];
        gotoADDetailTap.urlLink = [[[_ADDataArray objectAtIndex:i] objectForKey:@"pictureRes"] objectForKey:@"urlLink"];
        [ADImageView addGestureRecognizer:gotoADDetailTap];
        
    }
    //约束 ADcontentView
    [ADContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(lastImage.mas_right);
    }];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
