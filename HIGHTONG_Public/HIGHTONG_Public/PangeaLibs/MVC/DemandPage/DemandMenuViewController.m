//
//  DemandMenuViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/9/14.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "DemandMenuViewController.h"
#import "HTRequest.h"
#import "VODPageRequestManger.h"
#import "DemandScrollPageViewController.h"
#import "SearchViewController.h"
#import "NewSearchViewController.h"
@interface DemandMenuViewController ()<VTMagicViewDataSource,VTMagicViewDelegate,VODPageRequestDelegate,HTRequestDelegate>
{
    UILabel *_seachTextView;//搜索热词
    UIImageView *Guide ;//引导图
    NSString *defaultSearchStr;

}
@property (strong,nonatomic)NSMutableArray *menuPageArray;
@property (strong,nonatomic)NSMutableArray *menuNameList;
@property (strong,nonatomic)NSMutableArray *vodDisplayArray;
@end



@implementation DemandMenuViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view bringSubviewToFront:Guide];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.view.backgroundColor = HT_COLOR_BACKGROUND_APP;
    
    self.magicView.navigationColor = HT_COLOR_BACKGROUND_APP;
    self.magicView.sliderColor = App_selected_color;
    self.magicView.layoutStyle = VTLayoutStyleDefault;
    self.magicView.againstStatusBar = YES;
    self.magicView.navigationHeight = 40*kDeviceRate;
    self.magicView.sliderExtension = 10.0;
    self.magicView.itemSpacing = 3*kDeviceRate;
    self.magicView.itemScale = 1.1;
    self.magicView.itemSpacing = 25*kDeviceRate;
    _menuNameList = [NSMutableArray array];
    defaultSearchStr = [[NSString alloc]init];
    
    VODPageRequestManger *vodMenu = [[VODPageRequestManger alloc]initWithDelegate:self];
    [vodMenu reloadVODMenuData];
    [self showCustomeHUD];
    [self setSeachBar];
    [self requestVodRankByWeek];

    //点播页的引导
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODGUIDE"];
    if (string.length == 0 && IS_EPG_CONFIGURED) {
        Guide = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
        UIImage *image = [UIImage imageNamed:@"bg_first_geture"];
        
        Guide.image =  image;
        Guide.userInteractionEnabled = YES;
        
        UIImageView *guideTap = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+4*kRateSize+kTopSlimSafeSpace, kDeviceWidth, 110*kRateSize)];
        UIImage *imageTap = [UIImage imageNamed:@"ic_first_tap_gesture"];
        
        guideTap.image = imageTap;
        
        [Guide addSubview:guideTap];
        
        UIButton_Block *block = [[UIButton_Block alloc]initWithFrame:Guide.frame];
        [Guide addSubview:block];
        block.Click = ^(UIButton_Block *btn,NSString *name){
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VODGUIDE"];
            [Guide removeFromSuperview];
        };
        
//        if (HomeNewDisplayTypeOn) {
        
            [self.view  addSubview:Guide];
//        }
//        else
//        {
//            [self.tabBarController.view  addSubview:Guide];
//        }
//        
        
    }

    
}

- (void)requestVodRankByWeek
{
    
    HTRequest *_request  =[[HTRequest alloc]initWithDelegate:self];
    
    [_request PubCommonVodRankByWeekWithWeekly:@""];//请求热搜词
    
}



//设置搜索栏
- (void)setSeachBar
{
    
    
    
    //设置搜索textField
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 5*kDeviceRate;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(32*kDeviceRate,6*kDeviceRate,260*kDeviceRate, 17*kDeviceRate)];
    
    _seachTextView.text = @"请输入您想要查找的关键字";
    _seachTextView.font = HT_FONT_THIRD ;
    _seachTextView.textColor = UIColorFromRGB(0x999999);
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]init];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    [searchview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(6*kDeviceRate);
        make.top.equalTo(view.mas_top).offset(6*kDeviceRate);
        make.bottom.equalTo(view.mas_bottom).offset(-6*kDeviceRate);
        make.width.equalTo(@(17*kDeviceRate));
    }];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
        NewSearchViewController *search = [[NewSearchViewController alloc]init];
        //        search.searchText = [array lastObject];;
        [self.navigationController pushViewController:search animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    
    
    UIButton *Back = [[UIButton alloc]initWithFrame:CGRectMake(2, 18+kTopSlimSafeSpace, 44, 44)];
    [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
    [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateHighlighted];
    Back.showsTouchWhenHighlighted = YES;
    [Back addTarget:self action:@selector(Back) forControlEvents:UIControlEventTouchUpInside];
    
//    [self setNaviBarLeftBtn:Back];
    [self.m_viewNaviBar addSubview:Back];
    
    
    
    if (!HomeNewDisplayTypeOn) {
        [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 25+kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    }else{
        [self setNaviBarSearchViewFrame:CGRectMake(40*kRateSize, 25+kTopSlimSafeSpace, 295*kDeviceRate, 29*kDeviceRate)];
    }
    
}




-(void)VODMenuDataGetSuccessWith:(NSMutableArray *)result
{
    _menuPageArray = [NSMutableArray array];
    _menuPageArray = result;
    
    if (_menuPageArray.count>0) {
        [self hiddenCustomHUD];
        for (int i = 0; i<_menuPageArray.count; i++) {
            
            [_menuNameList addObject:[[_menuPageArray objectAtIndex:i] objectForKey:@"name"]];
            
        }
        [self.magicView reloadData];
        NSLog(@"最终获取到的Menu数据是%@",_menuNameList);
    }else
    {
        [AlertLabel AlertInfoWithText:@"暂无数据，请重试" andWith:self.view withLocationTag:2];
    }
    
}




#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    if (_menuNameList.count>0) {
        
        return _menuNameList;

    }
    return 0;
}


- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:HT_COLOR_FONT_FIRST forState:UIControlStateNormal];
        [menuItem setTitleColor:App_selected_color forState:UIControlStateSelected];
        //        menuItem.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17*kRateSize];
        menuItem.titleLabel.font = HT_FONT_SECOND;   //App_font(12*kDeviceRate);
    }
    return menuItem;
}


- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    static NSString *gridIdd = @"grid.Identifier";
    DemandScrollPageViewController *viewController = [magicView dequeueReusablePageWithIdentifier:gridIdd];
    if (!viewController) {
        
        viewController = [[DemandScrollPageViewController alloc]init];
        
        if (_menuPageArray.count>0) {
            
            for (NSDictionary *dic in _menuPageArray) {
                
                if ([[dic objectForKey:@"seq"] isEqualToString:[NSString stringWithFormat:@"%ld",pageIndex+1]]) {
                    
                    viewController.disPlayDemandCode = [dic objectForKey:@"code"];
                    viewController.disPlayDemandType = [dic objectForKey:@"type"];
                    viewController.disPlayDemandName = [dic objectForKey:@"name"];
                    [viewController setDisPlayHeaderDic:dic];
                    
                    viewController.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,[dic objectForKey:@"code"]];
                    viewController.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,[dic objectForKey:@"name"]];
                    viewController.EN = [NSString stringWithFormat:@"%@",self.EN];
                    
                }
            }
  
        }
        
    }
    
    viewController.isNeedNavBar = YES;
   

    NSLog(@"滑动的PageIndex%ld",pageIndex);
    
    return viewController;
    
}


#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppeare:(DemandScrollPageViewController *)viewController atPage:(NSUInteger)pageIndex
{
    if (_menuPageArray.count>0) {
        
        for (NSDictionary *dic in _menuPageArray) {
            
            if ([[dic objectForKey:@"seq"] isEqualToString:[NSString stringWithFormat:@"%ld",pageIndex+1]]) {
                [self showCustomeHUD];
                [viewController setDisPlayHeaderDic:dic];
                [viewController setDisPlayDemandName:[dic objectForKey:@"name"]];
                [viewController disPlayRequestCode:[dic objectForKey:@"code"] andType:[dic objectForKey:@"type"] andHeaderDic:dic];
                
                viewController.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,[dic objectForKey:@"code"]];
                viewController.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,[dic objectForKey:@"name"]];
                viewController.EN = [NSString stringWithFormat:@"%@",self.EN];
            }
        }
        
        
        NSLog(@"已经显示的这个index%ld",pageIndex);
   
    }
    
}

#pragma mark - 代理方法
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    switch (status) {
        case 0:
            
            //得到热搜词
            if ([type isEqualToString:PUB_COMMON_VODRANKBYWEEK]) {
                NSLog(@"PUB_COMMON_VODRANKBYWEEK----%@",result);
                
                NSDictionary *resultDIC = result;
                NSArray *List = [resultDIC objectForKey:@"dataList"];
                NSInteger numofList = [List count];
                
                
                if(numofList)
                {
                    
                    NSArray *useArr = [self dateSequenceWithsortArr:List andKeyName:@"nums"];
                    
                    defaultSearchStr = [[useArr objectAtIndex:0]objectForKey:@"groupName"];
                    
                    if (defaultSearchStr.length) {
                        _seachTextView.text = defaultSearchStr;
                    }
                }
                
            }

            
            break;
            
        default:

            
            break;
    }

    
}

/**
 *  排列数据按照日期先后进行排序
 *
 *  @param arr 原始数组
 *
 *  @return 排序后数组
 */
- (NSArray *)dateSequenceWithsortArr:(NSArray *)arr andKeyName:(NSString *)keyName
{
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:keyName ascending:YES];
    NSArray *sortArr = [NSArray arrayWithObjects:desc, nil];
    NSArray *sortedArr = [arr sortedArrayUsingDescriptors:sortArr];
    return sortedArr;
}




-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
