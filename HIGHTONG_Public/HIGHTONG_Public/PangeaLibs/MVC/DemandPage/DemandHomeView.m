//
//  DemandHomeView.m
//  HIGHTONG
//
//  Created by testteam on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DemandHomeView.h"
#import "Tool.h"
#import "MJRefresh.h"
#define numToString(a) [NSString stringWithFormat:@"index%ld",a]
#import "UIImageView+AFNetworking.h"
#define TableViewTag   1000000
#define TableArrayTag  100000

@interface DemandHomeView()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSInteger togalIndex;//目标页数
    
    NSInteger _index;//当前页数
}
@property(nonatomic,strong)UIScrollView *BaseScroll;
@property(nonatomic,strong)UIView *contentView;



@end
@implementation DemandHomeView


- (id)initWith_Array:(NSArray*)array
{
    if(self = [super init])
    {
        self.DataSource = [NSMutableArray arrayWithArray:array];
//        [self setupTableView];
        self.ALLCategoryDiction = [NSMutableDictionary dictionary];
        for(NSInteger i = 0 ; i < self.DataSource.count;i++)
        {
            NSMutableArray *array = [NSMutableArray array];
            NSString *key = numToString(TableArrayTag + i);
            [self.ALLCategoryDiction setObject:array forKey:key];
        }
    }
    return self;
}
- (void)loadIndex:(NSInteger)index
{
    if ([self.ALLCategoryDiction objectForKey:numToString(TableArrayTag + index)]) {
        UITableView *tableView = [self.contentView viewWithTag:index + TableViewTag];
        _index = index;
        [tableView reloadData];
    }
}
- (UITableView*)TableViewIndex:(NSInteger)index
{
    if (index < self.DataSource.count) {
        UITableView *tableView = [self.contentView viewWithTag:index + TableViewTag];
        return tableView;
    }
    return nil;
}
- (void)setupView
{
    //1
    [self removeAllSubviews];
    self.BaseScroll = [UIScrollView new];
    [self addSubview:self.BaseScroll];
    [self.BaseScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    if(self.bouderal)
    {
        self.BaseScroll.bounces = NO;
    }
    self.BaseScroll.delegate = self;
    self.BaseScroll.directionalLockEnabled = YES;
    //2
    self.contentView = [UIView new];
    [self.BaseScroll addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        //3
        make.edges.equalTo(self.BaseScroll);
        make.height.equalTo(self.BaseScroll.mas_height);
    }];
    
    
    //3.5
    UIView *lastView;
    
    //4
    for(NSInteger i = 0 ; i < self.DataSource.count;i++)
    {
        UITableView *view = [UITableView new];
        view.backgroundColor = App_background_color;
        view.separatorStyle = UITableViewCellSeparatorStyleNone;
//        view.separatorColor = App_background_color;
        view.delegate = self.delegate;
        view.dataSource = self.delegate;
//        view.tableFooterView = [UIView new];
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastView?lastView.mas_right:self.contentView.mas_left);
            make.top.equalTo(self.contentView.mas_top);
            make.width.equalTo(self.mas_width);
//            make.bottom.equalTo(self.contentView.mas_bottom).offset((-44-self.addHigh)*kRateSize);
            make.bottom.equalTo(self.contentView.mas_bottom);
        }];
        
        view.backgroundColor = App_background_color;
        view.tag = i + TableViewTag;
        
        if (self.HaveHeader) {
            
            NSDictionary *dic = [self.DataSource objectAtIndex:i];
            NSString * imageLink = [dic objectForKey:@"imageLink"];
            NSString * contentID = [dic objectForKey:@"contentID"];
            NSString * programName = [dic objectForKey:@"programName"];
            if (contentID.length) {
                UIImageView * ADVIew =  [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 160*kRateSize)];
                UIButton_Block *ActionBtn = [[UIButton_Block alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, 160*kRateSize)];
                [ADVIew sd_setImageWithURL:[NSURL URLWithString:NSStringIMGFormat(imageLink)]placeholderImage:[UIImage imageNamed:@"bg_home_poster_onloading"]];
                UIImageView *BGview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 130*kRateSize, kDeviceWidth, 30*kRateSize)];
                BGview.image = [UIImage imageNamed:@"bg_home_poster_content"];
                [ADVIew addSubview:BGview];
                UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(5, 2, kDeviceWidth, 30*kRateSize)];
                title.text = programName;
                title.textColor = [UIColor whiteColor];
                [BGview addSubview:title];
                [ADVIew addSubview:ActionBtn];
                ActionBtn.serviceIDName = contentID;
                ADVIew.userInteractionEnabled = YES;
                ActionBtn.Diction = dic;
                ActionBtn.Click = ^(UIButton_Block *btn,NSString *name){
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"ADCLICKVODHOME" object:btn.Diction];
                };
                
                view.tableHeaderView = ADVIew;
            }
            
        }
        
        
        
  
        
        
        
//        view.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            NSLog(@"添加了下拉刷新的位置是%d",view.tag);
//            
//            [self.delegate didRefreshDateWithTag:view.tag];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [view.header endRefreshing];
//            });
//        }];
        if (!self.notHaveMJ) {
            {
                // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的loadNewData方法）
                MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
                // 设置普通状态的动画图片
                //            [header setImages:idleImages forState:MJRefreshStateIdle];
                // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
                NSMutableArray *array = [NSMutableArray array];
                //            [array addObject:[UIImage imageNamed:@"Myloading"]];
                for (NSInteger i = 1; i<25; i++) {
                    NSString *name = [NSString stringWithFormat:@"Myloading%ld",i];
                    [array addObject:[UIImage imageNamed:name]];
                }
                //            [array addObject:[UIImage imageNamed:@"arrow"]];
                [header setImages:array forState:MJRefreshStatePulling];
                // 设置正在刷新状态的动画图片
                //            [header setImages:refreshingImages forState:MJRefreshStateRefreshing];
                // 设置header
                [header setTitle:@"正在刷新,请稍后..." forState:MJRefreshStateRefreshing];
                
                header.backgroundColor = [UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1];
                
                header.lastUpdatedTimeLabel.hidden = YES;
                view.mj_header = header;
            }
        }
       
        
        //5
        lastView = view;
    }
    
    //6
    if(lastView)
    {
        self.BaseScroll.pagingEnabled = YES;
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(lastView.mas_right);
        }];
    }
    
    
    
    
}
- (void)loadNewData
{

    [self.delegate didRefreshDateWithTag:_index];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UITableView *view = [self.contentView viewWithTag:_index+TableViewTag];
        [view.mj_header endRefreshing];
    });

}
//12 切页数  Fram 1 to ...
- (void)ChangePage:(NSInteger)page
{
    togalIndex = page;
    NSLog(@"%ld",page);
    if(page <= self.DataSource.count)
    {
        UITableView *tableView = [self.contentView viewWithTag:page + TableViewTag - 1];
        [self.BaseScroll scrollRectToVisible:tableView.frame animated:NO];
    }
}

//#pragma mark - 代理方法
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    NSArray *array = [self.ALLCategoryDiction objectForKey:numToString(TableArrayTag + _index)];
//
//    return array.count;
//}
//- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *identifier = @"identifier";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (!cell) {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    cell.textLabel.text = numToString(indexPath.row);
//    return cell;
//}






#pragma  mark - 滚动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float page = (scrollView.contentOffset.x + self.size.width/2)/ self.size.width;
     NSLog(@"第%f %lf页",scrollView.contentOffset.x,page);
    
        page = (int)page+1;
        if([self.delegate respondsToSelector:@selector(didChangePageWithPage:)]){
            
            if(self.pinbiScroll)
            {
                if([Tool togal:page WidthIndex:togalIndex] && page ==(togalIndex))
                {
                    [self.delegate didChangePageWithPage:page];
                   
                    
                    self.pinbiScroll = NO;
                }
            }else
            {
                [self.delegate didChangePageWithPage:page];
               
            }
            
    
        }

}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float page = (scrollView.contentOffset.x + self.size.width/2)/ self.size.width;
    page = (int)page+1;
    if ([self.delegate respondsToSelector:@selector(didChangePageWithPages:)]) {
        [self.delegate didChangePageWithPages:page];
    }

}

- (void)hideLoading
{
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
}
- (void)showLoading
{
    [MBProgressHUD showHUDAddedTo:self animated:YES];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
