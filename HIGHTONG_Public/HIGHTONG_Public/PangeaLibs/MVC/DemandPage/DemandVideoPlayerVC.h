//
//  FourthViewController.h
//  Hightong
//
//  Created by 赖利波 on 15/7/19.
//  Copyright (c) 2015年 lailibo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HT_FatherViewController.h"
#import "RemoteControlView.h"
#import "DemandVideoListCell.h"
#import "DetailBackScrollview.h"
#import "HTRequest.h"
#import "DemandModel.h"
//#import "UMSocial.h"
#import <UMSocialCore/UMSocialCore.h>
#import "AsyncUdpSocket.h"
#import "BONavigaitonView.h"
#import "ShareView.h"
#import  "MBProgressHUD.h"
#import "SAIInformationManager.h"
#import "PlayerMaskView.h"
#import "LoginViewController.h"
#import "DataPlist.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ErrorMaskView.h"
#import "TVADMaskView.h"
#import "DemandADDetailViewController.h"
#import "ADLiveVodProgramView.h"
#import "DetailTopBarView.h"
#import "ShareViewVertical.h"
#import "PolyMedicineLoadingView.h"
//static NSInteger  _adVideoHiddenSecend;
#import "BlackInstallView.h"
@interface DemandVideoPlayerVC : HT_FatherViewController<RemoteControlViewDelegate,UITableViewDataSource,UITableViewDelegate,HTRequestDelegate,AsyncUdpSocketDelegate,DetailScrollviewDelegate,MBProgressHUDDelegate>
{
//    HTRequest *request;
    BOOL _isEPGPlayerEnter;
    AsyncUdpSocket *_sendSocket;//发送广播的 套接字
    AsyncUdpSocket *_receiveSocket;//接收广播的 套接字 （因为发送和接收的 端口不同 所以 要用不同的套接字）
    CGFloat offset_x;
    CGFloat offset_y;
    BOOL _isForwardBackground;
    BOOL OnMove;

    ErrorMaskView *_errorMaskView;

}
@property (nonatomic,strong)BlackInstallView *blackInstallView;
@property (nonatomic,strong)PolyMedicineLoadingView *polyLoadingView;
@property (nonatomic,strong)TVADMaskView *adPlayerView;
@property (nonatomic,assign)TVADMaskViewType ADPlayType;
@property (nonatomic,assign)BOOL isHaveAD;
@property (assign, atomic) NSInteger  adImageHiddenSecend;
@property (nonatomic,strong)NSString *adImageURL;
@property (nonatomic,strong)NSString *adVideoURL;
@property (nonatomic,strong)NSString *adImageDetailURL;
@property (nonatomic,strong)NSString *adVideoDetailURL;
@property (nonatomic,assign)BOOL isADDetailEnter;
@property (nonatomic,assign)BOOL  ISADDETAIL;
@property (nonatomic,strong)ADLiveVodProgramView *ADViewBase;//增加广告的呼出按钮
@property (nonatomic,strong)ADLiveVodProgramView *ADViewPauseBase;//增加广告的暂停按钮
@property (assign, nonatomic) NSInteger  adVideoHiddenSecend;
@property (nonatomic,strong) DetailTopBarView *moviePlayerBottomView;



@property (nonatomic,assign)BOOL isFirstVideo;
@property (nonatomic,assign)BOOL isVODEX;
@property (nonatomic,assign)BOOL isEnterClick;
@property (nonatomic,assign)BOOL isEneterLoginVC;

@property (nonatomic,strong)UIImageView *screenLockedIV;//屏幕枷锁view
@property (nonatomic,assign)CurrentPLAYType currentPlayType;
@property (nonatomic,assign)VODPLAYType vodPlayType;
@property (nonatomic,assign)PraiseDegrade praiseDegradeType;

@property(nonatomic,assign)BOOL isVerticalDirection;
@property (strong, nonatomic) DemandModel *demandModel;
@property (nonatomic,strong) NSString *episodeContentID;
@property (nonatomic,strong) NSString *episodeName;
@property (nonatomic,strong) NSString *episodeSeq;
@property (nonatomic,strong) NSString *breakPointRecord;
@property (nonatomic,strong)NSString *contentID;
@property (nonatomic,strong)NSString *contentTitleName;
@property (nonatomic,strong)UIView *backgroundView;
@property (nonatomic,strong)UIImageView *topBarIV;
@property (nonatomic,strong)UIImageView *bottomBarIV;
@property (nonatomic,strong)UIButton*returnBtn;
@property (nonatomic,strong)UISlider*videoProgressV;
@property (nonatomic,strong)UIProgressView*volumeProgressV;
@property (nonatomic,strong)UIImageView*returnBtnIV;
@property (nonatomic,strong)UIButton*returnMainBtn;
@property (nonatomic,strong)UIView *orangeView;
@property (strong, nonatomic) MPMoviePlayerController * moviePlayer;
@property (strong,nonatomic)MPMusicPlayerController * musicPlayerController;
@property (nonatomic,assign)float isSlineceValue;
@property (nonatomic,assign)BOOL  isVolumeSlinece;

@property (strong,nonatomic) UIView *contentView;
@property (strong,nonatomic) UIView *tapView;
@property (strong, nonatomic) NSTimer * autoHiddenTimer;
@property (assign, atomic) int  autoHiddenSecend;
@property (nonatomic,strong)UIProgressView *brightnessProgress;
@property (nonatomic,assign)GestureType gestureType;
@property (nonatomic,assign)DLNAPLAYType dlnaplayType;
@property (assign ,nonatomic) NSInteger playIndex;//记录当前播放的是哪个视频。
@property (nonatomic,assign)int JumMoveStart;
@property (nonatomic,assign)CGPoint originalLocation;
@property(nonatomic,strong)UIPanGestureRecognizer   * panGesture;//增加手势
@property(nonatomic,assign)PanDirection               panDirection; // 定义一个实例变量，保存枚举值

@property (nonatomic,assign)CGFloat systemBrightness;

@property (nonatomic,strong)UIImageView *brightnessView;
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UIView *bottomView;
@property (strong,nonatomic) UIImageView *IPBackIV;//ip背景图
@property (strong,nonatomic) UITableView *IPTableView;
@property (strong,nonatomic) UIImageView *IPSeperateLineView;
@property (strong,nonatomic) UIButton *IPButton;
@property (strong,nonatomic) NSMutableArray *IPArray;
@property (nonatomic,strong) UIButton *playPauseBigBtn;//大屏幕上面暂停中间大播放按钮。

@property (nonatomic,strong)UIImageView *progressTimeView;
@property (nonatomic,strong)UIImageView *progressDirectionIV;
@property (nonatomic,strong)UILabel *progressTimeLable_top;
@property (nonatomic,strong)UILabel *progressTimeLable_bottom;
@property (nonatomic,assign)CGFloat ProgressBeginToMove;

@property (retain,nonatomic)  NSTimer *myNewTimer;//新的url更新进度定时器
@property (strong,nonatomic) UITableView *videoListTV;
@property (strong,nonatomic) NSMutableArray *videoListArr;//存放电视剧的相关推荐的数组
@property (strong,nonatomic) NSMutableArray *asscciationArr;//存放相关推荐的数组
@property (strong,nonatomic) UIImageView *playBackgroundIV;
@property (nonatomic,strong) UIButton *playBackgroundIVBtn;//小屏幕上面初始化下的中间大播放按钮。
@property (nonatomic, copy) NSString *currentURL;//极为重要的属性，传个大屏播放器用
@property (nonatomic, strong)UILabel *serviceTitleLabel;
@property (strong,nonatomic)  NSTimer *getPlayBackRecordListRepitTimer;//回看时重置断点时间

@property (nonatomic,strong) UILabel *BufferLabel;
@property (nonatomic,strong) UIImageView *bufferBackImageView;
@property (nonatomic,strong) UIView *bufferView;
@property (nonatomic,assign)NSInteger JumMoveStartDemand;
@property (nonatomic,strong)ShareViewVertical *shareViewVertical;
/**
 *  采集VOD的一些信息属性
 */

@property (retain,nonatomic)  NSString      * watchDuration;//采集 观看时长
@property (retain,nonatomic)  NSDate        * nowDate;//采集  获取服务器现在的时间
@property (retain,nonatomic)  NSString      *nowDateString;//采集  获取时间的字符串


//是否是爱上TV的逻辑，比如登录和绑定

@property (nonatomic,assign) APPMaskType *appMaskType;
@property (nonatomic,assign) BOOL  isEnterBind;


/*
 界面信息采集所需的一些界面属性
 */
@property (nonatomic,strong) NSString      *TRACKID;
@property (nonatomic,strong) NSString      *TRACKNAME;
@property (nonatomic,strong) NSString      *EN;

-(void)setSmallScreenPlayer;;
-(void)setBigScreenPlayer;


@end
