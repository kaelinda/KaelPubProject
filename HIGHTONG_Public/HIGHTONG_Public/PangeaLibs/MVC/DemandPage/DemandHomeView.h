//
//  DemandHomeView.h
//  HIGHTONG
//
//  Created by testteam on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Masonry.h"
//#import "NewDemandViewcontrolViewController.h"

@protocol DemandHomeViewDelegate

- (void)didChangePageWithPage:(NSInteger)page;//已经改变了页码数，方便外边的vc知道当前这个ScrollView滑动到什么地方了
- (void)didChangePageWithPages:(NSInteger)page;//通过改变页面，方便外边vc知道当前这个
- (void)didRefreshDateWithTag:(NSInteger)tag;//已经下拉刷新了

@end

@interface DemandHomeView : UIView


//@property(nonatomic,assign)NewDemandViewcontrolViewController <DemandHomeViewDelegate>*delegate;//实现tableview的代理

@property(nonatomic,assign)id <DemandHomeViewDelegate,UITableViewDataSource,UITableViewDelegate>delegate;//实现tableview的代理

@property(nonatomic,strong)NSMutableArray *DataSource;//tableView展示的数据分组有几个

@property(nonatomic,assign)BOOL pinbiScroll;//

@property (nonatomic,assign)BOOL HaveHeader;//有无头视图

@property (nonatomic,assign)NSInteger addHigh;//加高

@property (nonatomic,assign)BOOL bouderal;//弹性

@property(nonatomic,strong)NSMutableDictionary *ALLCategoryDiction;//存放tableview数据源的数组不确定数量

@property (nonatomic,assign)BOOL notHaveMJ;//弹性

- (UITableView*)TableViewIndex:(NSInteger)index;

//加载刷新数第几个的数据
- (void)loadIndex:(NSInteger)index;
/**
 *  切换页数
 *
 *  @param page 页码  Fram 1 to ...
 */
- (void)ChangePage:(NSInteger)page;

- (instancetype)initWith_Array:(NSArray*)array;



/**
 *  已经设置好数组数据源，开始创建视图吧
 */
- (void)setupView;
/**
 *  显示加载中ing
 */
- (void)showLoading;
/**
 *  隐藏加载中end
 */
- (void)hideLoading;

@end
