
//
//  FourthViewController.m
//  Hightong
//
//  Created by 赖利波 on 15/7/19.
//  Copyright (c) 2015年 lailibo. All rights reserved.
//

#import "DemandVideoPlayerVC.h"
#import "MyGesture.h"
#import "FavoriteAlertView.h"
#import  "GetBaseInfo.h"
#import "LinkButton.h"
#import "LinkDeviceTableViewCell.h"
#import  "AlertLabel.h"
#import "CustomAlertView.h"
#import "UserCollectCenter.h"
#import "UserPlayRecorderCenter.h"
#import "MBProgressHUD.h"
#import "GetBaseInfo.h"
#import "ShareView.h"
#import "EPGDateSelectedView.h"
#import "EPGListTableViewCell.h"
#import "GETBaseInfo.h"
#import "LoadingView.h"
#import "DateTools.h"
#import "SystermTimeControl.h"
#import "EpgSmallVideoPlayerVC.h"
#import "OrderView.h"
#import "DemandWIFIViewController.h"
#import "NewLoginViewController.h"
#import "NetWorkNotification_shared.h"
#import "HTTimerManager.h"
#import "UserLoginViewController.h"
#import "PlayerErrorManager.h"
#import "LocalCollectAndPlayRecorder.h"
#import "UserBindingVC.h"
#import "HTVolumeUtil.h"
#import "PlayerHeader.h"
static NSString * const kMASCellReuseIdentifier = @"kMASCellReuseIdentifier";
static NSString * const kMNSCellReuseIdentifier = @"kMASCellReuseIdentifier";
static RemoteControlView *remote;;
#define ISIntall (IS_PUBLIC_ENV && [APP_ID isEqualToString:@"562172033"])

@interface DemandVideoPlayerVC ()<HTRequestDelegate,UIAlertViewDelegate,CustomAlertViewDelegate,MBProgressHUDDelegate,shareViewDelegate,HTTimerManagerDelegate>
{
    BOOL _isCollectionInfo;
    UIImageView *_gestureGuidView;  //zzsc
    NSString *_shareURL;
    
    UITableView *_EPGListTableView;
    BONavigaitonView  *boNavi;
    MBProgressHUD *_HUD;
    UITapGestureRecognizer * tapGestuerImfo;
    UIView *downView;
    DetailBackScrollview *_DBackScrollView;//详情视图
    CGFloat statusHeight;
    NSString *_message;
    BOOL OnClick;
    BOOL _isPrepareToPlay;
    BOOL _isClickedClear;           // 是否点击过清晰度的按钮
    UIImageView *_selectClearIV;    // 选择清晰度界面
    NSMutableArray *clearTitles;
    BOOL _isMovieCategory;
    CGFloat ZTTimeInterval;
    
    UILabel *_currentTimeL;         // 当前时间
    UILabel *_slantlineL;
    
    UILabel *_totalTimeL;           // 总时间
    
    //小屏的暂停按钮和全屏播放按钮
    UIButton *smallbtn_stop;
    UIButton *fullWindowbutton;

    
    NSInteger JumpStartTime;
    BOOL _isScreenLocked;//判断屏幕是否枷锁的标记。
    
    //Bar上的小按钮
    UIButton *_favourButton;
    UIButton *_favourLabel;
    
    
    UIImageView *_candyTV;
    
    UIButton *_TVButton;
    UIButton *_MTButton;
    UIButton *_RemoteButton;
    
    BOOL _isPlayEnd;
    
    BOOL _isPraiseAction;
    
    //favourButton
    
    NSInteger _videoPlayPosition;   // 记录当前播放的视频的位置
    
    //    UIButton *superClearButton;
    //    UIButton *highClearButton;
    //    UIButton *originalClearButton;
    //    UIButton *smoothClearButton;
    //    /判断是否高清类型类型
    
    NSInteger PlayTVType;
    
    //当前播放的URL
    NSURL *currentPlayUrl;
    
    
    //互动按钮
    UIButton * _interactButton;
    
    UIImageView *_interactView;
    UIImageView *_moreBackView;
    //三个小按钮
    
    UIView *remoteControlBackV;
    UIView *pushBackV;
    UIView *pullBackV;
    
    UIImageView *remoteLeftV;
    UIImageView *pushLeftV;
    UIImageView *pullLeftV;
    
    UILabel *remoteLabel;
    UILabel *pushLabel;
    UILabel *pullLabel;
    
    
    
    UIImageView *remoteDownLine;
    UIImageView *pushDownLine;
    UIImageView *pullDownLine;
    
    NSDate *timeAgoDate;
    
    
    BOOL isremote;
    
    BOOL _isTapPlayBtn;//必须点击了，才能选装才能播放
    
    NSInteger _channelSelectIndex;
    
    
    
    //分享按钮
    
    UIButton *_shareButton;
    UIButton *_shareRightLabel;
    
    //剧集
    
    UIButton *_videoListButton;
    UIImageView *_videoListView;
    
    //时间按钮
    
    UIButton *_timeButton;
    
    
    //更多按钮
    
    UIButton *_moreButton;
    
    //上一曲
    
    UIButton *_forwardButton;
    
    //暂停播放按钮
    
    
    UIButton *_playButton;
    
    //锁屏按钮
    UIButton *lockedBtn;
    
    
    
    
    
    //下一曲
    
    UIButton *_backwardButton;
    
    UIImageView *_voiceView;
    
    
    //清晰度
    
    UIButton *_clearButton;         // 清晰度按钮
    NSString *_allCodeRateLink;//记录多有码率的所有链接  每次鉴权时候清空
    
    
    BOOL _isPaused;                 // 播放按钮状态, 假正在播放, 真暂停状态
    
    UIView *footerView;
    
    
    
    UIView *waitAndLoadLabel2;
    //    UIActivityIndicatorView *ac;
    LoadingView *ac;
    UILabel *loading;
    
    
    NSString *netWork;
    BOOL _isPlayLastVideo;           //是否播放的是第一个视频
    BOOL _isPlayFirstVideo;         // 是否播放的是最后一个视频
    
    
    
    
    
    UIView *_noDataViewDemand;
    UILabel *_noDataLabelDemand;
    UILabel *_noEPGListViewLabelDemand;
    
    
    
    //    UIView *_orderView;
    UILabel *_orderLabel;
    UILabel *_orderButton;
    
    
//    OrderView *_orderView;
    
    
    UILabel *_orderDeadlineLabel;
    
    PlayerMaskView *_playerLoginView;
    
    PlayerMaskView *_orderView;
    
    PlayerMaskView *_loadingMaskView;
    
    PlayerMaskView *_ZhuanWangMaskView;    // cap  添加
    PlayerMaskView *_installMaskView;
    PlayerMaskView *_jxPlayerLoginView;
    PlayerMaskView *_jxPlayerBindView;

    NSInteger recordTime;//新增全局断点的变量
    
    
    BOOL isJX_ASTV;
    
    

}
@property (nonatomic,strong)ShareView *shareView;
@property (assign, nonatomic) BOOL isPlay;
@property (nonatomic,strong) HTTimerManager *timeManager;
@property (nonatomic,strong) PlayerErrorManager *errorManager;//采集错误信息管理类
@property (copy,nonatomic) LocalVideoModel *localVideoModel;
@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;

@end

@implementation DemandVideoPlayerVC



- (id)init
{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(volumeChangedNotification:)
                                                     name:@"AVSystemController_SystemVolumeDidChangeNotification"
                                                   object:nil];
  

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                     name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadStateDidChange:)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:_moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackStateDidChange:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:_moviePlayer];
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocationOrder:) name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PausePlayer) name:@"MSG_PAUSEPLAYER" object:nil];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_PlayPlayer) name:@"MSG_PLAYPLAYER" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveMsg_StopPlayer) name:@"MSG_STOPPLAYER" object:nil];
        
        if (_timeManager) {
            _timeManager = nil;
        }
        _timeManager = [[HTTimerManager alloc]init];
        _timeManager.delegate = self;
        
        _watchDuration = nil;
        
        
        if (_errorManager) {
            _errorManager = nil;
        }
        
        _errorManager = [[PlayerErrorManager alloc]init];
        
        
        if (_localVideoModel) {
            _localVideoModel = nil;
        }
        
        _localVideoModel = [[LocalVideoModel alloc]init];
        
        
        if ([APP_ID isEqualToString:JXCABLE_SERIAL_NUMBER]) {
            
            isJX_ASTV = YES;
        }else
        {
            isJX_ASTV = NO;
        }
        
        
    }
    return self;
}

#pragma mark----------播放暂停
//播放暂停
-(void)receiveMsg_PausePlayer
{
    [self actionPauseForVideo];
}

#pragma mark----------恢复播放

-(void)receiveMsg_PlayPlayer
{
    [self actionPlayForVideo];
}


-(void)receiveMsg_StopPlayer
{
    
//    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];

    
    if (USER_ACTION_COLLECTION == YES)
    {
        
        if (_errorManager.isAVLoading == YES) {
            
            _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
            [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_demandModel.NAME andOPSCode:PlayerLoadingExitError andOPSST:[SystermTimeControl getNowServiceTimeString]];
        }
        
        
        [_errorManager resetPlayerStatusInfo];
        
        
        //播放记录
        _localVideoModel.lvNAME = _demandModel.NAME;
        _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
        _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
        _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
        _localVideoModel.lvImageLink = _demandModel.imageLink;
        _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
        
        //                [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
        [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
        [_localVideoModel resetLocalVideoModel];
        
        if (_isCollectionInfo) {
            _demandModel.contentID = _demandModel.contentID;
            _demandModel.vodFenleiType = [self collectVodInformationType];
            _demandModel.name = _demandModel.name;
            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
            _demandModel.vodBID = _demandModel.vodBID;
            _demandModel.VodBName = _demandModel.VodBName;
            _demandModel.category = _demandModel.category;
            _demandModel.rootCategory = _demandModel.rootCategory;
            _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
            _demandModel.EN_VOD = _demandModel.EN_VOD;
            
            if ([_watchDuration intValue] >= 5) {
                
                [SAIInformationManager VodInformation:_demandModel];
            }
            
            
            [_timeManager reset];
        }
        
    }
    [self removeMovieControllerNotification];
    
}




- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    [MobCountTool popOutWithPageView:@"DemandVideoPlayerPage"];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
    }
    

    //        [_moviePlayer stop];
    //        _moviePlayer = nil;
}

-(void)setExclusiveTouchForButtons:(UIView *)myView
{
    for (UIView * v in [myView subviews]) {
        if([v isKindOfClass:[UIButton class]])
            [((UIButton *)v) setExclusiveTouch:YES];
        else if ([v isKindOfClass:[UIView class]]){
            [self setExclusiveTouchForButtons:v];
        }
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isEnterClick = YES;
    [self setExclusiveTouchForButtons:self.view];

    
    if (_isEneterLoginVC == YES) {
        if (IsLogin) {
            _playerLoginView.hidden = YES;
            _jxPlayerLoginView.hidden = YES;
            _isEPGPlayerEnter = NO;
            NSDictionary *vodDIC = [[NSDictionary alloc]init];
            
            vodDIC =  [DataPlist ReadToFileDic:VODINFOPLIST];
            if (vodDIC!=nil) {
                self.demandModel.contentID = [vodDIC objectForKey:@"contentID"];
                self.demandModel.NAME = [vodDIC objectForKey:@"demandName"];
                HTRequest *requestVOD = [[HTRequest alloc]initWithDelegate:self];
//                [requestVOD VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                if (ISIntall) {
                    
                    _installMaskView.hidden = NO;
                }else
                {
                    [requestVOD VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                }
                
                
            }
        }else
        {
            return;
        }
 
    }
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    if ([APP_ID isEqualToString:JXCABLE_SERIAL_NUMBER]) {
        
        isJX_ASTV = YES;
    }else
    {
        isJX_ASTV = NO;
    }

    [self hiddenPanlwithBool:NO];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    if (_isVerticalDirection == YES) {
        
        [self setSmallScreenPlayer];
    }
    
    if (_isADDetailEnter == YES) {
        _isADDetailEnter = NO;
        _adPlayerView.isCanContinu = YES;
//        _isHaveAD = NO;
        _returnMainBtn.hidden = YES;
        _topBarIV.hidden = YES;
        _bottomBarIV.hidden = YES;
        _returnBtnIV.hidden = YES;
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        [_moviePlayer play];
        
        
//            if (_isVerticalDirection == YES) {
//        
//                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
//            }else
//            {
//                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
//            }
        
        
    }
    _systemBrightness = [UIScreen mainScreen].brightness;
    [self hideNaviBar:YES];
    if (IsLogin) {
        _orderView.hidden = YES;
        _playerLoginView.hidden = YES;
        _jxPlayerLoginView.hidden = YES;
        _isEPGPlayerEnter = NO;
        

    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CAN_SHOW_TOAST" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];

    
        [self afterRefreshBindAuthority];

    [MobCountTool pushInWithPageView:@"DemandVideoPlayerPage"];
    
    
    
    [AVAudioSession sharedInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:nil];

    
    
    
}

//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(afterRefreshBindAuthority:) name:@"SaveTokenSuccessed" object:nil];
/**
 这个是绑定成功后，出现界面重新刷新界面
 */
-(void)afterRefreshBindAuthority
{
    
        if (_isEnterBind == YES) {
            _isEnterBind = NO;
            
            if (IsBinding) {
                
                 [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(afterRefreshBind:) name:@"SaveTokenSuccessed" object:nil];
            }
            
        }
    
}


-(void)afterRefreshBind:(NSNotification*)noti
{
    if ([[NSString stringWithFormat:@"%@",noti.object] length]>0) {
        
        NSDictionary *vodDIC = [[NSDictionary alloc]init];
        vodDIC =  [DataPlist ReadToFileDic:VODINFOPLIST];
        
        self.demandModel.contentID = [vodDIC objectForKey:@"contentID"];
        
        
        
    
        //                [requestVOD VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
        if (ISIntall) {
            
            _installMaskView.hidden = NO;
        }else
        {
            //                [requestVOD VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:KTimeout];
            
            
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request VODDetailWithContentID:self.demandModel.contentID andTimeout:kTIME_TEN];
            
            
        }

    }
}


-(void)loginAfterEnterVideo
{
    if (IS_PERMISSION_GROUP) {
        
    if (_isEPGPlayerEnter == YES) {
        
        if (IsLogin) {
            _playerLoginView.hidden = YES;
            _jxPlayerLoginView.hidden = YES;
            _isEPGPlayerEnter = NO;
            
            [self loginAfterRefreshFavor];

            
            NSDictionary *vodDIC = [[NSDictionary alloc]init];
            vodDIC =  [DataPlist ReadToFileDic:VODINFOPLIST];
            
            self.demandModel.contentID = [vodDIC objectForKey:@"contentID"];
            
            
            
            HTRequest *requestVOD = [[HTRequest alloc]initWithDelegate:self];
            //                [requestVOD VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
                [requestVOD VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:KTimeout];
            }
            
            

            
        }else
        {
            return;
        }
        
        
        
    }else
    {
        return;
    }
        
    }

}



- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            NSLog(@"Headphone/Line plugged in");
            
            [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            _isPlay = YES;//改变暂停标记
            _playPauseBigBtn.hidden = YES;
            [_moviePlayer play];//暂停视频
            
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            
            self.isPlay = NO;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            _playPauseBigBtn.hidden = NO;
            [_moviePlayer pause];
            [self.myNewTimer invalidate];
            
            
            NSLog(@"Headphone/Line was pulled. Stopping player....");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }
}




-(void)logo{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)handleGesture:(UIGestureRecognizer*)tapsender
{
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
    _demandModel = [[DemandModel alloc] init];
    _videoListArr = [[NSMutableArray alloc]init];
    _asscciationArr = [[NSMutableArray alloc]init];
    _demandModel.resultDictionary = [[NSMutableDictionary alloc]init];
    //zzsc 取消侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    _isFirstVideo = YES;
    
    //    self.demandModel = [[DemandModel alloc] init];
    
    netWork =[NSString stringWithFormat:@"%@",[GETBaseInfo getNetworkType]];
    //    netWork = @"3G";
    
    _isVerticalDirection = YES;
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"PlayTVType"]) {
        PlayTVType = 4;
    }else
    {
        PlayTVType = [[NSUserDefaults standardUserDefaults] integerForKey:@"PlayTVType"];
    }
    
    
    //接收拉的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playPullTV:) name:@"RECEVEPULLTVINFO" object:nil];//接收到拉平消息
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveBoxIPNoti:) name:@"RECEIVE_BOXIP" object:nil];//接收到 盒子IP 消息  更新列表
    [self StartNewTimer];
    
    
    
    
    UIButton *logo = [UIButton buttonWithType:UIButtonTypeCustom];
    
    logo = [HT_NavigationgBarView createImgNaviBarBtnByImgNormal:@"abtn_back" imgHighlight:@"btn_common_title_back_normal"  target:self action:@selector(logo)];
    [self setNaviBarLeftBtn:logo];
    
    NSLog(@"hehhehe");
    
    if (isAfterIOS7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    }
    self.view.backgroundColor = App_background_color;
    //***************************************//
    
    //*******************记得返回界面的时候恢复亮度[UIScreen mainScreen].brightness = _systemBrightness;
    //    *********************//
    
    
    if (!IsIOS6) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    if (!IsIOS6) {
        
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }else{
        
        [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationUnknown;
        
    }
    
    
    statusHeight = IsIOS6?0:20;
    
    
    //初始化
    clearTitles = [[NSMutableArray alloc]init];
    
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request VODDetailWithContentID:self.contentID andTimeout:10];
    
    [request VODAssccationListWithContentID:self.contentID andCount:12 andTimeout:kTIME_TEN];
    
    
    
    //背景底图
    
    //    [self setNaviBarTitle:[NSString stringWithFormat:@"%@",self.demandModel.name]];
    
    
    
    //******************初始化背景图**********************************
    
    
    _backgroundView = [[UIView alloc]init];
    [_backgroundView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:_backgroundView];
    
    
    WS(wself);
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        //        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.left.mas_equalTo(wself.view.mas_left);
        make.height.mas_equalTo(194*kRateSize);
        make.width.mas_equalTo(wself.view.mas_width);
        
    }];
    
    //******************end**********************************
    
    
    
    //******初始化播放器界面和用于放播放器界面的ContentView************************
    
    
    //小播放器图界面
//    boNavi = [[BONavigaitonView alloc]init];
//    
//    [self.backgroundView addSubview:boNavi];
//    [boNavi mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top);
//        make.left.mas_equalTo(self.view.mas_left);
//        make.width.mas_equalTo(self.view.mas_width);
//        make.height.mas_equalTo(50*kRateSize);
//    }];
    
    
    
    _shareView = [[ShareView alloc]init];
    _shareView.delegate = self;
    _shareView.hidden = YES;
    MyGesture *gestureRemote = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gestureRemote.numberOfTapsRequired = 1;
    gestureRemote.tag = SHAREVIEWTAG;
    [_shareView addGestureRecognizer:gestureRemote];
    //    [_backgroundView addSubview:_shareView];
    
    
    
    if (_moviePlayer != nil) {
        //        [player release];
        _moviePlayer = nil; //i prefer to do both, just to be sure.
    }
    
    _moviePlayer = [[MPMoviePlayerController alloc] init];
    //    if (_moviePlayer) {
    
    _moviePlayer.allowsAirPlay = YES;
    //    [_backgroundView addSubview:_moviePlayer.view];
    _moviePlayer.view.tag = 328;
    _moviePlayer.view.userInteractionEnabled = YES;
    [_moviePlayer setControlStyle:MPMovieControlStyleNone];
    [_moviePlayer setScalingMode:MPMovieScalingModeFill];
    _moviePlayer.backgroundView.backgroundColor = [UIColor blackColor];
    //    }
    
    
    
    //锁屏
    
    _screenLockedIV = [[UIImageView alloc]init];
    _screenLockedIV.hidden = YES;
    _screenLockedIV.userInteractionEnabled = YES;
    //    _screenLockedIV.image = [UIImage imageNamed:@"smallSC_locked_bg"];
    [_selectClearIV setBackgroundColor:[UIColor clearColor]];
    lockedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [lockedBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_unlock"] forState:UIControlStateNormal];
    
    [lockedBtn addTarget:self action:@selector(changeTheLockedState:) forControlEvents:UIControlEventTouchUpInside];
    

    
    _contentView = [[UIView alloc]init];
    _contentView.userInteractionEnabled = YES;
    [_contentView setBackgroundColor:[UIColor clearColor]];
    
    
    
    [_contentView     addGestureRecognizer:self.panGesture];

  //_adPlayerView 开始
    _adPlayerView = [[TVADMaskView alloc]init];
    _adPlayerView.hidden = YES;
    [_adPlayerView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_adPlayerView"];

    //Block
    _adPlayerView.backBtn.tag = 0;
    _adPlayerView.backBlcok = ^(BOOL isBack){
        
        if (_adPlayerView.backBtn.tag == 0) {
            [wself removeMovieControllerNotification];
            
//            [self hideNaviBar:NO];

            [wself.navigationController popViewControllerAnimated:YES];
        }else
        {
//            [self hiddenPanlwithBool:YES];
//            [self hideNaviBar:NO];
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        }
        
        
        
    };
    
    _adPlayerView.ADDetailBlock = ^(NSString *str){
        NSLog(@"进入详情Block");
        
        
        [wself hideNaviBar:YES];
        DemandADDetailViewController *adDetail = [[DemandADDetailViewController alloc]init];
        if (_adPlayerView.maskViewType == ImageAD) {
//            adDetail.detailADURL = [[[self.demandModel.adList objectAtIndex:0] valueForKey:@"pictureRes"] objectForKey:@"videoLink"];;
            adDetail.detailADURL = [[[wself.demandModel.adList objectAtIndex:0] valueForKey:@"pictureRes"] objectForKey:@"urlLink"];;
        }else if (_adPlayerView.maskViewType == VideoAD)
        {
            adDetail.detailADURL = wself.adVideoDetailURL;
        }
        
        if (adDetail.detailADURL.length>0) {
                _isADDetailEnter = YES;
            if (_isADDetailEnter == YES) {
                wself.adPlayerView.isCanContinu = NO;
                [wself.moviePlayer pause];
                
                _isPlay = NO;
                
            }
            adDetail.isVertical = wself.isVerticalDirection;
            [wself presentViewController:adDetail animated:YES completion:^{

            }];
            
            
        }else
        {
            return ;
        }
        
        
       
        
       
    };
    
        
    _adPlayerView.volumeBlock = ^(BOOL  isSilence){
        if (isSilence == NO) {
            _isVolumeSlinece = NO;

            [HTVolumeUtil shareInstance].volumeValue = wself.isSlineceValue;
        }else
        {
            wself.isSlineceValue = [HTVolumeUtil shareInstance].volumeValue;
            
            wself.isVolumeSlinece = YES;

            NSLog(@"没有静音了啊");
            [HTVolumeUtil shareInstance].volumeValue =  wself.volumeProgressV.progress;
            
        }
        
    };
    
    
    _adPlayerView.rotateBtn.tag = 0;
    _adPlayerView.rotateBlock = ^(BOOL isFullWindown){
        if (_adPlayerView.rotateBtn.tag == 0) {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                //这里是1.0f后 你要做的事情
                [AlertLabel AlertInfoWithText:@"已切换至全屏模式" andWith:wself.contentView withLocationTag:1];
                
            });
            
            
        }else if (_adPlayerView.rotateBtn.tag == 1)
        {
            return ;
//            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                
//                //这里是1.0f后 你要做的事情
//                [AlertLabel AlertInfoWithText:@"已切换至小屏模式" andWith:wself.contentView withLocationTag:1];
//                
//            });
        }
    };
    _adPlayerView.ADEndBlock = ^(BOOL isEnd){
        _isHaveAD = NO;
        

//        _adPlayerView.backImageView.hidden = YES;
        [wself clearMoviePlayerStop];
//            _returnMainBtn.alpha = 1;
                                if (_isHaveAD == YES) {
                                    wself.returnMainBtn.alpha = 0;
                                    wself.returnBtnIV.hidden = YES;
        
                                }else{
                                    wself.returnMainBtn.alpha = 1;
                                    wself.returnBtnIV.hidden = NO;
                                }

        [wself allcodeRateRequest:[NSURL URLWithString:wself.currentURL]];
//        if (_isVerticalDirection == NO) {
//            [wself setBigScreenPlayer];
//            [wself showGestureGuidView];
//        }else
//        {
//            [wself setSmallScreenPlayer];
//        }
       
        NSLog(@"广告的时间结束");
        
        
        if (_adPlayerView.maskViewType == VideoAD) {
            if (_isVolumeSlinece == YES) {
                [HTVolumeUtil shareInstance].volumeValue = wself.isSlineceValue;
                
            }else
            {
                return ;
            }
            
        }
        
        
        
    };
    

    
    _ADViewBase = [[ADLiveVodProgramView alloc]init]; // cap
    _ADViewBase.hidden = YES;
    [_ADViewBase UpDateViewHW:ADPLAYER_MENUVOD];
    _ADViewBase.alpha = 1;
    
    
    
    _ADViewPauseBase = [[ADLiveVodProgramView alloc]init]; // cap
    _ADViewPauseBase.hidden = YES;
    [_ADViewPauseBase UpDateViewHW:ADPLAYER_MENUPause];
    [_ADViewPauseBase setBackgroundColor:[UIColor clearColor]];
//    _ADViewPauseBase.alpha = 1;
//    [_contentView addSubview:_ADViewBase];
//    [_contentView addSubview:_ADViewPauseBase];
    
    //******************end**********************************
    
    
    //******************初始化上下bar**********************************
    
    
    _bottomBarIV = [[UIImageView alloc] init];
    
    
    _topBarIV = [[UIImageView alloc] init];
    
    [_topBarIV setImage:[UIImage imageNamed:@"bg_player_playbill_listview_epg"]];
    [_bottomBarIV setImage:[UIImage imageNamed:@"bg_player_controller_bottom"]];
    
    _topBarIV.userInteractionEnabled = YES;
    _bottomBarIV.userInteractionEnabled = YES;
    
    //******************end**********************************
    
    _orderDeadlineLabel = [[UILabel alloc]init];
    [_orderDeadlineLabel setBackgroundColor:[UIColor clearColor]];
    _orderDeadlineLabel.text = @"订购剩余12时30分";
    _orderDeadlineLabel.hidden = YES;
    _orderDeadlineLabel.font = [UIFont systemFontOfSize:18];
    _orderDeadlineLabel.textColor = UIColorFromRGB(0xffffff);
    
    _serviceTitleLabel = [[UILabel alloc]init];
    [_serviceTitleLabel setBackgroundColor:[UIColor clearColor]];
    _serviceTitleLabel.font = [UIFont systemFontOfSize:18];
    _serviceTitleLabel.textColor = UIColorFromRGB(0xffffff);
    
    if (!_contentTitleName || [_contentTitleName isEqualToString:@""]) {
        
        _contentTitleName = @"";
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",_contentTitleName];
        
    }
    else
    {
        //        [self.view reloadInputViews];
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",_contentTitleName];
        
    }
    
    
    //开始时刻的大播放按钮和背景View
    
    _playBackgroundIV = [[UIImageView alloc]init];
    _playBackgroundIV.image = [UIImage imageNamed:@""];
    _playBackgroundIV.userInteractionEnabled = YES;
    _playBackgroundIVBtn  = [[UIButton alloc]init];
    _playBackgroundIVBtn.showsTouchWhenHighlighted = YES;
    //    _playBackgroundIVBtn.frame = CGRectMake(0, 0, 57, 57);
    //    _playBackgroundIVBtn.center = _smallContentView.center;
    [_playBackgroundIVBtn setBackgroundColor:[UIColor blackColor]];
    //    _isTapPlayBtn = NO;
    
    //暂时这么做，还不知道会不会出问题
    
    _playBackgroundIV.hidden = YES;
    _isTapPlayBtn = YES;
    
    [_playBackgroundIVBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_pause_normal"] forState:UIControlStateNormal];
    [_playBackgroundIVBtn addTarget:self action:@selector(TapSmallScreenbtn_playNoButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    _playPauseBigBtn  = [[UIButton alloc]init];
    [_playPauseBigBtn setBackgroundColor:[UIColor clearColor]];
    _playPauseBigBtn.showsTouchWhenHighlighted = YES;
    [_playPauseBigBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_pause_normal"] forState:UIControlStateNormal];
    _playPauseBigBtn.hidden = YES;
    [_playPauseBigBtn addTarget:self action:@selector(playPauseBigBtnPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    _IPArray = [[NSMutableArray alloc]init];//初始化数据源
    
    _IPBackIV = [[UIImageView alloc]init];
    [_IPBackIV setBackgroundColor:[UIColor blackColor]];
    _IPBackIV.alpha = 0.6;
    
    _IPBackIV.userInteractionEnabled = YES;
    
    
    
    
    
    _IPTableView = [[UITableView alloc]init];
    _IPTableView.delegate = self;
    _IPTableView.dataSource = self;
    //        [_IPTableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_player_interaction_item"]]];
    [_IPTableView setBackgroundColor:[UIColor clearColor]];
    _IPTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _IPTableView.alpha = 0.5;
    _IPTableView.hidden = YES;
    
    
    
    _IPSeperateLineView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_player_full_menuitem_seperate_line"]];
    
    _IPButton = [[UIButton alloc]init];
    [_IPButton setTitle:@"收起" forState:UIControlStateNormal];
    [_IPButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [_IPButton addTarget:self action:@selector(ipButtonCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _RemoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _RemoteButton.hidden = YES;
    _RemoteButton.enabled = NO;
    _RemoteButton.tag = REMOTECONTROL;
    [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_normal.png"] forState:UIControlStateNormal];
     [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_pressed"] forState:UIControlStateHighlighted];
    
    [_RemoteButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    _TVButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _TVButton.hidden = YES;
    _TVButton.tag = TVTAG;
    [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_normal.png"] forState:UIControlStateNormal];
     [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_pressed"] forState:UIControlStateHighlighted];
    [_TVButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _MTButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _MTButton.hidden = YES;
    _MTButton.tag = MTTAG;
    [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_normal.png"] forState:UIControlStateNormal];
    [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_pressed"] forState:UIControlStateHighlighted];
    [_MTButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //******************初始化进度条**********************************
    
    
    _videoProgressV = [[UISlider alloc] init];
    
    [_videoProgressV setThumbImage:[UIImage imageNamed:@"ic_player_seekbar_progress_cursor.png"] forState:UIControlStateNormal];
    _videoProgressV.userInteractionEnabled = YES;
    [_videoProgressV setMaximumTrackImage:[UIImage imageNamed:@"bg_player_progress_normal"] forState:UIControlStateNormal];
    [_videoProgressV setMinimumTrackImage:[UIImage imageNamed:@"bg_player_progress_pressed"] forState:UIControlStateNormal];
    [_videoProgressV setBackgroundColor:[UIColor clearColor]];
    
    
    [_videoProgressV setMaximumValue:1000.00];
    [_videoProgressV setMinimumValue:0.00];
//    _videoProgressV.continuous = NO;
    [_bottomBarIV addSubview:_videoProgressV];
    
    
    
    [_videoProgressV addTarget:self action:@selector(ProgressValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_videoProgressV addTarget:self action:@selector(ProgressValueConfirm:) forControlEvents:UIControlEventTouchUpInside];
    [_videoProgressV addTarget:self action:@selector(ProgressValueConfirm:) forControlEvents:UIControlEventTouchUpOutside];
    
    
    
    
    
    //******************end**********************************
    
    
    
    
    
    //******************初始化tapView**********************************
    
    _tapView = [[UIView alloc] init];
    [_tapView setBackgroundColor:[UIColor clearColor]];
    
    
    UITapGestureRecognizer * tapGestuer = [[UITapGestureRecognizer alloc] init];
    [tapGestuer addTarget:self action:@selector(singleTapAction:)];
    [_tapView addGestureRecognizer:tapGestuer];
    
    //******************初始化tapView**********************************
    
    
    //******************初始化全屏按钮和上方左侧小按钮**********************************
    
    //全屏播放按钮
    fullWindowbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal.png"] forState:UIControlStateNormal];
    [fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_pressed"] forState:UIControlStateHighlighted];
    fullWindowbutton.backgroundColor = [UIColor clearColor];
    fullWindowbutton.tag = 0;
    [fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBarIV addSubview:fullWindowbutton];
    
    
    _returnBtnIV = [[UIImageView alloc]init];
    _returnBtnIV.userInteractionEnabled = YES;
  _returnBtnIV.image = ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER])?[UIImage imageNamed:@"btn_player_common_returnMain_back_normal"]:[UIImage imageNamed:@"btn_common_title_back_normal"];
    
    _returnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_returnBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
//    [_returnBtn setImage:[UIImage imageNamed:@"btn_back_click"] forState:UIControlStateHighlighted];
    [_returnBtn setBackgroundColor:[UIColor clearColor]];
    _returnBtn.showsTouchWhenHighlighted = YES;
    //******************end**********************************
    
    
    _returnMainBtn= [UIButton buttonWithType:UIButtonTypeCustom];
//    [_returnMainBtn setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
//    [_returnMainBtn setImage:[UIImage imageNamed:@"btn_back_click"] forState:UIControlStateHighlighted];
    [_returnMainBtn setBackgroundColor:[UIColor clearColor]];
    _returnMainBtn.showsTouchWhenHighlighted = YES;
    
    
    
    
    
    //***************初始化快进快退View，亮度等View**********************************
    
    
    _brightnessView = [[UIImageView alloc]init];
    _brightnessView.image = [UIImage imageNamed:@"bg_player_brightness.png"];
    
    _brightnessProgress = [[UIProgressView alloc]init];
    _brightnessProgress.trackImage = [UIImage imageNamed:@"video_num_bg.png"];
    _brightnessProgress.progressImage = [UIImage imageNamed:@"video_num_front.png"];
    _brightnessProgress.progress = [UIScreen mainScreen].brightness;
    _brightnessView.hidden = YES;
    
    
    _volumeProgressV = [[UIProgressView alloc]init];
    _volumeProgressV.hidden = YES;
    
    
    _progressTimeView = [[UIImageView alloc]init];
    _progressTimeView.hidden = YES;
    [_progressTimeView setImage:[UIImage imageNamed:@"bg_player_full_fast_action"]];
//    _progressTimeView.alpha = 0.5;
    
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    
    
    
    
    
    _progressDirectionIV = [[UIImageView alloc]init];
    _progressDirectionIV.hidden = NO;
    _progressTimeLable_top = [[UILabel alloc]init];
    _progressTimeLable_top.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_top.textColor = App_selected_color;
    _progressTimeLable_top.backgroundColor = [UIColor clearColor];
    _progressTimeLable_top.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_top.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_top.shadowOffset = CGSizeMake(1.0, 1.0);
    
    _progressTimeLable_bottom = [[UILabel alloc]init];
    _progressTimeLable_bottom.textAlignment = NSTextAlignmentCenter;
    _progressTimeLable_bottom.textColor = UIColorFromRGB(0xffffff);
    _progressTimeLable_bottom.backgroundColor = [UIColor clearColor];
    _progressTimeLable_bottom.font = [UIFont systemFontOfSize:13*kRateSize];
    _progressTimeLable_bottom.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _progressTimeLable_bottom.hidden = NO;
    _progressTimeLable_bottom.shadowOffset = CGSizeMake(1.0, 1.0);
    
    
    
    
    
    
    //******************end**********************************
    
    
    
    
    //******************初始化开始Label结束Label**********************************
    
    _currentTimeL = [[UILabel alloc]init];
    _currentTimeL.backgroundColor = [UIColor clearColor];
    _currentTimeL.text = @"00:00:00";
    _currentTimeL.textColor = COLOR_FROM_RGB(0xeeeeee);
    _currentTimeL.font = [UIFont systemFontOfSize:11*kRateSize];
    //_currentTimeL.textAlignment = NSTextAlignmentRight;
    _currentTimeL.textAlignment = NSTextAlignmentRight;
    //_currentTimeL.text = @"00:01:23";
    
    
//    _slantlineL = [[UILabel alloc]init];
//    _slantlineL.backgroundColor = [UIColor clearColor];
//    _slantlineL.font = [UIFont systemFontOfSize:10];
//    _slantlineL.textColor = COLOR_FROM_RGB(0xffffff);;
//    _slantlineL.text = @"/";
//    
    
    
    _totalTimeL = [[UILabel alloc]init];
    _totalTimeL.backgroundColor = [UIColor clearColor];
    _totalTimeL.text = @"00:00:00";
    _totalTimeL.font = [UIFont systemFontOfSize:11*kRateSize];
    
    _totalTimeL.textColor = COLOR_FROM_RGB(0xeeeeee);
    //_totalTimeL.textAlignment = NSTextAlignmentLeft;
    _totalTimeL.textAlignment = NSTextAlignmentLeft;
    
    
    
    
    
//    _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    //    _moreButton.backgroundColor = [UIColor clearColor];
//    _moreButton.tag = MORE;
//    _moreButton.showsTouchWhenHighlighted = YES;
//    [_moreButton setImage:[UIImage imageNamed:@"btn_more"] forState:UIControlStateNormal];
//    [_moreButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    _moreBackView = [[UIImageView alloc]init];
    _moreBackView.hidden = YES;
    _moreBackView.userInteractionEnabled = YES;
    [_moreBackView setImage:[UIImage imageNamed:@"btn_player_more"]];
    
    
    
    
    
    
    //******************初始化开始Label结束Label*****end*****************************
    
    //收藏按钮
    _favourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _favourButton.tag = FAVOUR;
    [self favorSucceed:NO];
    //    [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
//    [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
    //    [_favourButton setBackgroundImage:[UIImage imageNamed:@"btn_player_collect_normal.png"] forState:UIControlStateNormal];
    //    [_favourButton setBackgroundColor:[UIColor redColor]];
    
    [_favourButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _favourLabel = [[UIButton alloc]init];
    //    _favourLabel.text = @"添加";
    [_favourLabel setTitle:@"收藏" forState:UIControlStateNormal];
    //    _favourLabel.textColor = [UIColor whiteColor];
    _favourLabel.hidden = YES;
    _favourLabel.tag = FAVOUR;
    [_favourLabel setBackgroundColor:[UIColor clearColor]];
    [_favourLabel addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    if (self.demandModel.contentID.length == 0) {
//        self.demandModel.contentID = _contentID;
//    }
    
//    if (self.demandModel.favorContenID.length == 0) {
//        self.demandModel.favorContenID = _contentID;
//    }
    
    
    
    
    
    
    //****************初始化互动按钮及初始化下方的View**********************************
    
    //互动按钮
    
    _interactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _interactButton.tag = INTERACT;
    _interactButton.backgroundColor = [UIColor clearColor];
//    _interactButton.titleLabel.font = [UIFont systemFontOfSize:18];
    _interactButton.showsTouchWhenHighlighted = YES;
    [_interactButton setImage:[UIImage imageNamed:@"btn_player_interaction_normal"] forState:UIControlStateNormal];
    [_interactButton setImage:[UIImage imageNamed:@"btn_player_interaction_pressed"] forState:UIControlStateHighlighted];
//    [_interactButton setTitle:@"互动" forState:UIControlStateNormal];
    [_interactButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    _interactView = [[UIImageView alloc]init];
    //        [_interactView setBackgroundColor:[UIColor clearColor]];
    _interactView.hidden = YES;
    _interactView.userInteractionEnabled = YES;
    [_interactView setImage:[UIImage imageNamed:@"bg_player_interaction_item"]];
    
    
    
    
    
    
    
    //三个小背景
    remoteControlBackV = [[UIView alloc]init];
    [remoteControlBackV setBackgroundColor:[UIColor clearColor]];
    remoteControlBackV.userInteractionEnabled = YES;
    
    pushBackV = [[UIView alloc]init];
    [pushBackV setBackgroundColor:[UIColor clearColor]];
    pushBackV.userInteractionEnabled = YES;
    pullBackV = [[UIView alloc]init];
    [pullBackV setBackgroundColor:[UIColor clearColor]];
    pushBackV.userInteractionEnabled = YES;
    
    
    //三个小背景加按钮
    
    
    
    
    remoteLeftV  = [[UIImageView alloc]init];
    [remoteLeftV setImage:[UIImage imageNamed:@"remoteSmall"]];
    remoteLeftV.userInteractionEnabled = YES;
    
    pushLeftV  = [[UIImageView alloc]init];
    [pushLeftV setImage:[UIImage imageNamed:@"btn_player_full_interact_push"]];
    pullLeftV.userInteractionEnabled = YES;
    
    
    pullLeftV  = [[UIImageView alloc]init];
    [pullLeftV setImage:[UIImage imageNamed:@"btn_player_full_interact_pull"]];
    pullLeftV.userInteractionEnabled = YES;
    
    
    
    remoteLabel = [[UILabel alloc]init];
    remoteLabel.userInteractionEnabled = YES;
    [remoteLabel setBackgroundColor:[UIColor clearColor]];
    [remoteLabel setText:@"智能遥控器"];
    [remoteLabel setTextColor:UIColorFromRGB(0xffffff)];
    [remoteLabel setFont:[UIFont systemFontOfSize:15]];
    remoteLabel.textAlignment = NSTextAlignmentLeft;
    
    pushLabel = [[UILabel alloc]init];
    pushLabel.userInteractionEnabled = YES;
    [pushLabel setBackgroundColor:[UIColor clearColor]];
    [pushLabel setText:@"推屏"];
    [pushLabel setTextColor:UIColorFromRGB(0xffffff)];
    [pushLabel setFont:[UIFont systemFontOfSize:15]];
    
    pullLabel.textAlignment = NSTextAlignmentLeft;
    
    pullLabel = [[UILabel alloc]init];
    [pullLabel setBackgroundColor:[UIColor clearColor]];
    pullLabel.userInteractionEnabled = YES;
    [pullLabel setText:@"拉屏"];
    [pullLabel setFont:[UIFont systemFontOfSize:15]];
    pullLabel.textAlignment = NSTextAlignmentLeft;
    [pullLabel setTextColor:UIColorFromRGB(0xffffff)];
    
    
    
    remoteDownLine = [[UIImageView alloc]init];
    [remoteDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    pushDownLine = [[UIImageView alloc]init];
    [pushDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    pullDownLine = [[UIImageView alloc]init];
    [pullDownLine setImage:[UIImage imageNamed:@"ic_player_interact_down_line"]];
    
    
    
    
    
    
    
    
    
    
    
    
    //***********初始化互动按钮及初始化下方的View*********end*************************
    
    
    
    //****************初始化分享按钮及初始化下方的View**********************************
    
    
    //分享按钮
    
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.tag= SHARE;
    //    [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
    _shareButton.hidden = YES;
    //    _shareButton.backgroundColor = [UIColor clearColor];
     if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession] && ![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ])
     {
         [_shareButton setImage:[UIImage imageNamed:@"btn_more_share_not"] forState:UIControlStateNormal];
         _shareButton.userInteractionEnabled = NO;
     }else
     {
    [_shareButton setImage:[UIImage imageNamed:@"btn_player_share_normal"] forState:UIControlStateNormal];
    [_shareButton setImage:[UIImage imageNamed:@"btn_player_share_pressed"] forState:UIControlStateHighlighted];
     }
    [_shareButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    _shareRightLabel = [[UIButton alloc]init];
//    //    _shareRightLabel.text = @"分享";
//    [_shareRightLabel setTitle:@"分享" forState:UIControlStateNormal];
//    _shareRightLabel.hidden = YES;
//    _shareRightLabel.tag = SHARE;
//    //    _shareRightLabel.textColor = [UIColor whiteColor];
//    [_shareRightLabel setBackgroundColor:[UIColor clearColor]];
//    [_shareRightLabel addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //*************初始化分享按钮及初始化下方的View*******end***************************
    
    
    
    //****************初始化剧集按钮及初始化下方的View**********************************
    
    
    //剧集
    
    _videoListButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _videoListButton.tag = VIDEO_LIST;
    //    [_videoListButton setTitle:@"选集" forState:UIControlStateNormal];
    _videoListButton.titleLabel.font = [UIFont systemFontOfSize:18];
    _videoListButton.showsTouchWhenHighlighted = YES;
    
    
    _videoListButton.backgroundColor = [UIColor clearColor];
    [_videoListButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //****************初始化剧集按钮及初始化下方的View*******end***********************
    
    
    
    
    //****************初始化时间按钮**********************************
    
    
//    _timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _timeButton.backgroundColor = [UIColor clearColor];
//    [_timeButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
//    
//    
//    
//    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timeSystemStr) userInfo:nil repeats:YES];
    
    //    timeLabel.text = currentDateStr;
    
    
    
    //****************初始化时间按钮********end**************************
    
    
    
    
    
    
    
    
    //初始化遥控器********************
    //     CGRect frame = self.view.bounds;
    //    remote = [[RemoteControlView alloc] initWithFrame:frame];
    
    
    //    remote = [[RemoteControlView alloc]init];
    //    remote.backgroundColor = [UIColor cyanColor];
    //    remote.delagete = self;
    //    remote.hidden = YES;
    //    isremote = NO;
    //    [self.view addSubview:remote];
    
    
    //****************初始化遥控器按钮********end**************************
    
    
    
    
    //******************初始化上下播放按钮**********************************
    
    
    //上一曲
    
    //    UIButton *_forwardButton;
    
//    _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _forwardButton.tag = FORWARD;
//    [_forwardButton setImage:[UIImage imageNamed:@"btn_last.png"] forState:UIControlStateNormal];
//    [_forwardButton setImage:[UIImage imageNamed:@"btn_last_click"] forState:UIControlStateHighlighted];
//    //    [_forwardButton setTitle:@"上一曲" forState:UIControlStateNormal];
//    _forwardButton.backgroundColor = [UIColor clearColor];
//    [_forwardButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
//    _videoPlayPosition= 0;
//    _forwardButton.enabled = NO;
    
    
    //暂停播放按钮
    
    
    //    UIButton *_playButton;
    
    _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _playButton.tag = PLAY;
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    //    [_playButton setTitle:@"播放" forState:UIControlStateNormal];
    _playButton.backgroundColor = [UIColor clearColor];
    [_playButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //下一曲
    
    //    UIButton *_backwardButton;
    
    _backwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _backwardButton.tag = BACKWARD;
    [_backwardButton setImage:[UIImage imageNamed:@"btn_next.png"] forState:UIControlStateNormal];
    [_backwardButton setImage:[UIImage imageNamed:@"btn_player_movenext_pressed"] forState:UIControlStateHighlighted];
    //    [_backwardButton setTitle:@"下一曲" forState:UIControlStateNormal];
    _backwardButton.backgroundColor = [UIColor clearColor];
    [_backwardButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //******************初始化上下播放按钮end**********************************
    
    
    
    //******************清晰度按钮**********************************
    
    //清晰度
    
    //    UIButton *_clearButton;
    
    _clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _clearButton.tag= CLEAR;
    //    [_clearButton setImage:[UIImage imageNamed:@"cleartitle"] forState:UIControlStateNormal];
    _clearButton.titleLabel.font = [UIFont systemFontOfSize:11*kRateSize];
    
    _clearButton.titleLabel.textColor = UIColorFromRGB(0xeeeeee);
    
    
    
    
    
    //    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SUPER_CLEAR) {
    //        [_clearButton setTitle:@"超清" forState:UIControlStateNormal];
    //    }
    //    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == HIGH_DEFINE) {
    //        [_clearButton setTitle:@"高清" forState:UIControlStateNormal];
    //    }
    //    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == ORIGINAL_PAINT) {
    //        [_clearButton setTitle:@"标清" forState:UIControlStateNormal];
    //    }
    //    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SMOOTH) {
    //        [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
    //    }
    //    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == 0) {
    //        [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
    //    }
    _clearButton.showsTouchWhenHighlighted = YES;
    
    _clearButton.titleLabel.textColor = UIColorFromRGB(0x999999);
    _clearButton.backgroundColor = [UIColor clearColor];
    _clearButton.hidden = YES;
    [_clearButton addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //******************清晰度按钮end**********************************
    
    
    //清晰度选择界面
    
    _selectClearIV = [[UIImageView alloc]init];
    
    [_selectClearIV setImage:[UIImage imageNamed:@"bg_coderate_btn"]];
    _selectClearIV.alpha = 0.8;
    _selectClearIV.userInteractionEnabled = YES;
    
    _selectClearIV.hidden = YES;
    
    //    [_contentView addSubview:_selectClearIV];
    
    
    
    //    superClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [_selectClearIV addSubview:superClearButton];
    //
    //    highClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_selectClearIV addSubview:highClearButton];
    //
    //
    //    originalClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [_selectClearIV addSubview:originalClearButton];
    //
    //    smoothClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //
    //    [_selectClearIV addSubview:smoothClearButton];
    //
    //
    
    //******************声音按钮**********************************
    
    //隐藏掉
    //    _voiceView  = [[UIImageView alloc]init];
    //
    //    [_voiceView setImage:[UIImage imageNamed:@"voiceView.png"]];
    //    [_bottomBarIV addSubview:_voiceView];
    
    //******************声音按钮******end****************************
    
    
    
    
    _videoListView = [[UIImageView alloc]init];
    [_videoListView setBackgroundColor:[UIColor clearColor]];
    _videoListView.userInteractionEnabled = YES;
    _videoListView.hidden = YES;
    //注册监听button的enabled状态
    _videoListTV = [[UITableView alloc]init];
    _videoListTV.delegate = self;
    _videoListTV.dataSource = self;
    [_videoListTV setBackgroundColor:[UIColor clearColor]];
    
//    _videoListTV.alpha = 0.8;
//    
//    [_videoListTV setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];
    
    [_videoListTV setBackgroundView:[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_fullscreen_menu"]]];

    _videoListTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_videoListTV registerClass:[DemandVideoListCell class] forCellReuseIdentifier:kMASCellReuseIdentifier];
    
    
    
    
    
    [self hideNaviBar:YES];
    [_backgroundView addSubview:_moviePlayer.view];
    [_moviePlayer.view addSubview:_contentView];
    
    [_interactView addSubview:remoteControlBackV];
    [_interactView addSubview:pullBackV];
    [_interactView addSubview:pushBackV];
    [remoteControlBackV addSubview:remoteLeftV];
    
    [pushBackV addSubview:pushLeftV];
    [pullBackV addSubview:pullLeftV];
    
    
    [remoteControlBackV addSubview:remoteLabel];
    [remoteControlBackV addSubview:remoteDownLine];
    [pushBackV addSubview:pushLabel];
    [pushBackV addSubview:pushDownLine];
    [pullBackV addSubview:pullLabel];
    [pullBackV addSubview:pullDownLine];
    [_moviePlayer.view addSubview:_RemoteButton];
    [_moviePlayer.view addSubview:_TVButton];
    [_moviePlayer.view addSubview:_MTButton];
    [_contentView addSubview:_tapView];
    [_contentView addSubview:_playPauseBigBtn];
    [_tapView addSubview:_ADViewBase];
    [_contentView addSubview:_ADViewPauseBase];
    [_contentView addSubview:_adPlayerView];

    [_moviePlayer.view addSubview:_screenLockedIV];
    [_screenLockedIV addSubview:lockedBtn];
    [_contentView addSubview:_topBarIV];
    [_topBarIV addSubview:_serviceTitleLabel];
    [_topBarIV addSubview:_orderDeadlineLabel];
    [_contentView addSubview:_bottomBarIV];
    [_contentView addSubview:_returnBtnIV];
    [_contentView addSubview:_returnBtn];
    [_contentView addSubview:_returnMainBtn];
    [_contentView addSubview:_videoListView];
    [_videoListView addSubview:_videoListTV];
    [_contentView addSubview:_interactView];
    [_contentView addSubview:_moreBackView];
    [_contentView addSubview:_IPBackIV];
    [_IPBackIV addSubview:_IPTableView];
    [_IPBackIV addSubview:_IPSeperateLineView];
    [_IPBackIV addSubview:_IPButton];
    [_tapView addSubview:_playBackgroundIV];
//    [_playBackgroundIV addSubview:_shareRightLabel];

    [_playBackgroundIV addSubview:_playBackgroundIVBtn];
    
    //    [_contentView addSubview:_returnMainBtnIV];
    [_contentView addSubview:_selectClearIV];
    
    
    [_contentView addSubview:_brightnessView];
    [_brightnessView addSubview:_brightnessProgress];
    [_contentView addSubview:_volumeProgressV];
    [_contentView addSubview:_progressTimeView];
    [_progressTimeView addSubview:_progressDirectionIV];
    [_progressTimeView addSubview:_progressTimeLable_bottom];
    [_progressTimeView addSubview:_progressTimeLable_top];
    //上下button
    
    [_topBarIV addSubview:_interactButton];
    
    
    [_topBarIV addSubview:_videoListButton];
//    [_topBarIV addSubview:_timeButton];
//    [_topBarIV addSubview:_moreButton];
    [_topBarIV addSubview:_shareButton];
//    [_moreBackView addSubview:_shareRightLabel];
    [_topBarIV addSubview:_favourButton];
    [_moreBackView addSubview:_favourLabel];
    
//    [_bottomBarIV addSubview:_forwardButton];
    
    [_bottomBarIV addSubview:_playButton];
    
    
    [_bottomBarIV addSubview:_clearButton];
    
    [_bottomBarIV addSubview:_backwardButton];
    
    //********************************************************
    
    [_bottomBarIV addSubview:_currentTimeL];
//    [_bottomBarIV addSubview:_slantlineL];
    [_bottomBarIV addSubview:_totalTimeL];
    
    

    
    
    _orderView = [[PlayerMaskView alloc] init];
    _orderView.backgroundColor = [UIColor clearColor];
    _orderView.programType = MovieOrderType;
    _orderView.hidden = YES;
    [_orderView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_orderView"];
    _orderView.maskViewReturnBlock = ^(){
       
        if (_isEnterClick == YES) {
            
            _isEnterClick = NO;
        
        if (IsLogin) {
            HTRequest *request = [[HTRequest alloc]initWithDelegate:wself];

            [request VODInquiryWithContentID:wself.demandModel.contentID andTimeout:kTIME_TEN];
            
        }else
        {
            if (isAuthCodeLogin) {
                _isPaused = YES;
                [wself actionPauseForVideo];
                [wself changeLoginVerticalDirection];
                
                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                [liveDic setObject:wself.demandModel.contentID forKey:@"contentID"];
                [liveDic setObject:wself.demandModel.NAME forKey:@"demandName"];
                
                [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];                UserLoginViewController *user = [[UserLoginViewController alloc] init];
                user.loginType = kVideoLogin;
                [wself.view addSubview:user.view];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                user.backBlock = ^()
                {
                    [[UIApplication sharedApplication]setStatusBarHidden:NO];
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    _isPaused = NO;
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                    [wself actionPlayForVideo];
                };
                
                user.tokenBlock = ^(NSString *token)
                {
                    [[UIApplication sharedApplication]setStatusBarHidden:NO];
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    _isPaused = NO;

                    [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                    _isEPGPlayerEnter = YES;
                    [wself loginAfterEnterVideo];
                    [wself actionPlayForVideo];
                };
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                return;
            }else
            {
                [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.view withLocationTag:1];
                
                
            }

        }
        
        }
        
        NSLog(@"这里写上 订购询价的方法");
        
    };
    [_tapView addSubview:_orderView];
    
    
    
    
    
    
    //存放视图的contentView尺寸****************
    
    
    
    [_moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(wself.backgroundView);
        make.top.mas_equalTo(wself.backgroundView.mas_top);
        make.left.mas_equalTo(wself.backgroundView.mas_left);
        make.height.mas_equalTo(wself.backgroundView.mas_height);
        make.width.mas_equalTo(wself.backgroundView.mas_width);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerX.equalTo(wself.moviePlayer.view);
        //        make.centerY.mas_equalTo(wself.moviePlayer.view);
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.width.mas_equalTo(wself.moviePlayer.view.mas_width);
        
    }];
    
    [_returnMainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(wself.contentView.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(50*kRateSize);
    }];

    
    _adPlayerView.userInteractionEnabled = YES;
    [_adPlayerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(_contentView);
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    
    //大按钮
    
    [_playBackgroundIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wself.tapView.mas_centerX);
        make.centerY.mas_equalTo(wself.tapView.mas_centerY);
        make.height.mas_equalTo(wself.tapView.mas_height);
        make.width.mas_equalTo(wself.tapView.mas_width);
        
    }];
    
    
    
    [_playBackgroundIVBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_playBackgroundIV.mas_centerX);
        make.centerY.mas_equalTo(_playBackgroundIV.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
        
    }];
    
    
    
    
    
    
    
    //tap视图的尺寸****************
    
    [_tapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
        
    }];
    
    [_orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];
    
    if ([self.view viewWithTag:8080] == nil) {
        
        _noDataViewDemand = [[UIView alloc]init];
        _noDataViewDemand.tag = 8080;
        [_noDataViewDemand setBackgroundColor:[UIColor clearColor]];
        
        [_contentView addSubview:_noDataViewDemand];
        
        _noDataLabelDemand = [[UILabel alloc]init];
        [_noDataLabelDemand setBackgroundColor:[UIColor clearColor]];
        _noDataLabelDemand.font = [UIFont systemFontOfSize:13];
        [_noDataLabelDemand setBackgroundColor:[UIColor clearColor]];
        [_noDataLabelDemand setTextColor:UIColorFromRGB(0x999999)];
        _noDataLabelDemand.text = @"暂无相关数据";
        _noDataLabelDemand.center = CGPointMake(80, 0);
        [_noDataViewDemand addSubview:_noDataLabelDemand];
        
        
        
    }
    
    
    [_noDataViewDemand mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(wself.contentView);
        //        make.size.mas_equalTo(CGSizeMake(212*kRateSize, 40*kRateSize));
        //        make.left.mas_equalTo(_channelListView.mas_height/2);
        make.right.mas_equalTo(wself.contentView.mas_right);
        make.width.mas_equalTo(212*kRateSize);
        make.height.mas_equalTo(40*kRateSize);
        
    }];
    
    
    
    
    [_noDataLabelDemand mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_noDataViewDemand.mas_left).offset(40);
        make.top.mas_equalTo(_noDataViewDemand.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    
    [[self.view viewWithTag:8080]setHidden:YES];
    
    
    
    //top视图尺寸****************
    
    _topBarIV.hidden = NO;
    _IPTableView.hidden = YES;
    
    
    
    
    
    
    _TVButton.hidden = YES;
    
    //_MTButton
    _MTButton.hidden = YES;
    
    _RemoteButton.hidden = YES;
    
    
    
    //时间按钮视图尺寸****************
    
//    _timeButton.hidden = YES;
//    _moreButton.hidden = YES;
    
    _interactButton.hidden = YES;
    
    
    
    _videoListButton.hidden = YES;
    
    
    _shareButton.hidden = YES;
    
//    _forwardButton.hidden = YES;
    
    
    
    
    [_topBarIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(0);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
        
        
    }];
    
//    _returnBtn.tag = RETURNBIG;
//    [_returnBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _returnMainBtn.tag = RETURNSMALL;
    [_returnMainBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_returnBtnIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.top.mas_equalTo(self.topBarIV.mas_top).offset(5*kRateSize);
//        make.left.mas_equalTo(self.topBarIV.mas_left).offset(-5*kRateSize);
//        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(44*kRateSize);
        
        make.top.mas_equalTo(self.topBarIV.mas_top).offset(20*kDeviceRate);
        make.left.mas_equalTo(self.topBarIV.mas_left).offset(12*kDeviceRate);
        make.width.mas_equalTo(28*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
        
    }];

    
    
//    [_returnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.top.mas_equalTo(self.topBarIV.mas_top).offset(0);
////        make.left.mas_equalTo(self.topBarIV.mas_left).offset(0);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
//        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(55*kRateSize);
//    }];
    
    
    
    [_serviceTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(35*kRateSize);
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(14*kRateSize);
//        make.width.mas_equalTo(120*kRateSize);
//        make.height.mas_equalTo(25*kRateSize);
        
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(40*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kDeviceRate);
        make.width.mas_equalTo(120*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);

        
    }];
    
    
    
    [_orderDeadlineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.topBarIV.mas_right);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(5*kRateSize);
        make.width.mas_equalTo(140*kRateSize);
        make.height.mas_equalTo(25*kRateSize);
        
        
    }];
    
    
    
    
    
    
    
    
    //底部Bar视图尺寸****************
    
    [_bottomBarIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.contentView.mas_bottom);
        make.centerX.mas_equalTo(wself.contentView);
        make.height.mas_equalTo(60*kRateSize);
        make.width.mas_equalTo(wself.contentView);
    }];
    
    
    //播放按钮视图尺寸****************
    
    [_playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-5*kRateSize);
        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(0);
//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.width.mas_equalTo(35*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
    }];
    
//    _forwardButton.hidden = YES;
    _backwardButton.hidden = YES;
    
    
    
    [_playPauseBigBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];
    
    
    
    //    _slantlineL.hidden = YES;
    
    
    
    //进度条视图尺寸****************
    
    //*********************************
    
    
    
    
    
    
    //全屏按钮视图尺寸****************
    
    [fullWindowbutton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(0*kRateSize);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-4*kRateSize);
        make.width.mas_equalTo(35*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
        
    }];
    
    //开始时间Label视图尺寸****************
    
    
    [_currentTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-11*kRateSize);
        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(25*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
    }];
    
    
    
//    [_slantlineL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(10*kRateSize
//                             ).offset(10*kRateSize);
////        make.right.mas_equalTo(_totalTimeL.mas_left).offset(-1.5*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(3*kRateSize, 20*kRateSize));
//        make.left.mas_equalTo(_currentTimeL.mas_right).offset(-4*kRateSize);
//        
//
//    }];
    
    
    
    //总时间Label视图尺寸****************
    
    [_totalTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerY.mas_equalTo(wself.bottomBarIV).offset(12);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-11*kRateSize);
//        make.right.mas_equalTo(fullWindowbutton.mas_left).offset(0*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-20*kRateSize);
    }];
    
    
    
    [_videoProgressV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-18*kRateSize);
        make.left.mas_equalTo(_currentTimeL.mas_right).offset(10*kRateSize);
        make.right.mas_equalTo(_totalTimeL.mas_left).offset(-10*kRateSize);//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.height.mas_equalTo(6*kRateSize);
        //        make.width.mas_equalTo(169*kRateSize);
    }];

    
    
    
    _clearButton.hidden = YES;
    _selectClearIV.hidden = YES;
    
    _returnBtnIV.hidden = NO;
    _returnBtn.hidden = YES;
    _serviceTitleLabel.hidden = NO;
    
    
    
    
    
    
    //亮度视图尺寸****************
    
    [_brightnessView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessProgress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
    
    
    
    
    
    
    [_backgroundView addSubview:_shareView];
    
    //******************************************************************************************************************//
    
    
    
   
    
    
    
    _polyLoadingView = [[PolyMedicineLoadingView alloc]init];
    if (ISIntall) {
        
        _polyLoadingView.hidden = YES;
    }else
    {
    _polyLoadingView.hidden = NO;
    }
    [_tapView addSubview:_polyLoadingView];
    [_polyLoadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 60*kRateSize));
        make.centerX.mas_equalTo(wself.tapView.centerX).offset(30*kRateSize);
        make.centerY.mas_equalTo(wself.tapView.centerY);
        
    }];
    
    
    if ([self.view viewWithTag:4040]== nil) {
        
    
    _loadingMaskView = [[PlayerMaskView alloc] init];
    _loadingMaskView.backgroundColor = [UIColor clearColor];
    _loadingMaskView.programType = LoadingViewType;
    _loadingMaskView.hidden = YES;
    _loadingMaskView.tag = 4040;
    _loadingMaskView.maskViewReturnBlock = ^(){
        
        
    };
    [_tapView addSubview:_loadingMaskView];
    
    }
    [_loadingMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];

    
    
    
    
    
    
    
    
    //PlayMaskView
    
    
    
 
    
    //JXPlayerMaskView
    
    
    if ([APP_ID isEqualToString:JXCABLE_SERIAL_NUMBER]) {
        
        _jxPlayerLoginView = [[PlayerMaskView alloc]initWithAppType:JXCABLE_ASTV];
        _jxPlayerLoginView.backgroundColor = [UIColor clearColor];
        [_jxPlayerLoginView setJxProgramType:JXLoginActionType];
        _jxPlayerLoginView.hidden = YES;
        [_tapView addSubview:_jxPlayerLoginView];
        [_jxPlayerLoginView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"jxplayerLoginView"];
        [_jxPlayerLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(_tapView);
        }];
        
        _jxPlayerLoginView.maskViewReturnBlock = ^()
        {
            NSLog(@"我已经实现了江西的登录按钮的点击事件");
            
            if (_isEnterClick == YES) {
                
                
                _isEnterClick = NO;
                
                if (_isVerticalDirection == NO) {
                    
                    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                    
                }
                
                
                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                [liveDic setObject:wself.demandModel.contentID forKey:@"contentID"];
                [liveDic setObject:wself.demandModel.NAME forKey:@"demandName"];
                
                [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                
                
                
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [wself actionPauseForVideo];
                    [wself changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:wself.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:wself.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    _isEnterClick = YES;
                    [wself.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [wself actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [wself loginAfterEnterVideo];
                        [wself actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                    
                    
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.view withLocationTag:1];
                }
                
                
                
            }
            
            
        };
        
        
        _jxPlayerBindView = [[PlayerMaskView alloc]initWithAppType:JXCABLE_ASTV];
        _jxPlayerBindView.backgroundColor = [UIColor redColor];
        [_jxPlayerBindView setJxProgramType:JXBindingActionType];
        _jxPlayerBindView.hidden = YES;
        [_tapView addSubview:_jxPlayerBindView];
        [_jxPlayerBindView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(_tapView);
        }];
        
        _jxPlayerBindView.maskViewReturnBlock = ^()
        {
            
            if (_isVerticalDirection == NO) {
                
                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                
            }
            
            _isEnterBind = YES;
            UserBindingVC *bindVC = [[UserBindingVC alloc]init];
            bindVC.isFroming = YES;
            [wself.navigationController pushViewController:bindVC animated:YES];
        };

        
        
        
        
        
    }
    else
    {
        _playerLoginView = [[PlayerMaskView alloc] init];
        _playerLoginView.backgroundColor = [UIColor clearColor];
        _playerLoginView.programType = LoginActionType;
        _playerLoginView.hidden = YES;
        _jxPlayerLoginView.hidden = YES;
        [_playerLoginView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"playerLoginView"];
        _playerLoginView.maskViewReturnBlock = ^(){
            
            if (_isEnterClick == YES) {
                
                
                _isEnterClick = NO;
                
                if (_isVerticalDirection == NO) {
                    
                    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
                    
                }
                
                
                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                [liveDic setObject:wself.demandModel.contentID forKey:@"contentID"];
                [liveDic setObject:wself.demandModel.NAME forKey:@"demandName"];
                
                [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                
                
                
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [wself actionPauseForVideo];
                    [wself changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:wself.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:wself.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    _isEnterClick = YES;
                    [wself.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [wself actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:wself.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [wself loginAfterEnterVideo];
                        [wself actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                    
                    
                    
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:wself.view withLocationTag:1];
                }
                
                
                
            }
            
        };
        [_tapView addSubview:_playerLoginView];
        
        [_playerLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.mas_equalTo(_tapView);
            
        }];
        
    }

    
    
    
    
    
    
    
    //cap  添加   专网下才可以观看
    _ZhuanWangMaskView = [[PlayerMaskView alloc] init];
    _ZhuanWangMaskView.backgroundColor = [UIColor clearColor];
    _ZhuanWangMaskView.programType = ZhuanWangViewType;
    _ZhuanWangMaskView.hidden = YES;
    [_ZhuanWangMaskView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"_ZhuanWangMaskView"];
    
    [_tapView addSubview:_ZhuanWangMaskView];
    
    [_ZhuanWangMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];
    
    _ZhuanWangMaskView.maskViewReturnBlock = ^(){
        [wself hideNaviBar:YES];
        
        
//        if ([Home_more_Wifi_Link length]>0) {
            DemandWIFIViewController *adDetail = [[DemandWIFIViewController alloc]init];
            adDetail.isVertical = wself.isVerticalDirection;
            adDetail.detailADURL = [NSString stringWithFormat:@"%@",Home_more_Wifi_Link];
            adDetail.isVertical = wself.isVerticalDirection;
            [wself presentViewController:adDetail animated:NO completion:nil];
//        }else
//        {
//            return ;
//        }
        
        
        
        
    };
    
    
    
    
    
    
    
    
    //小屏播放器界面的详情界面
    downView = [[UIView alloc]init];
    [downView setBackgroundColor:App_background_color];
    
    [self.view addSubview:downView];
    
    
    
    
    [downView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.mas_equalTo(wself.backgroundView.mas_bottom);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        
        make.width.mas_equalTo(wself.view.mas_width);
//        make.height.mas_equalTo(wself.view.mas_height).multipliedBy(0.97);
                make.bottom.equalTo(wself.view.mas_bottom).offset(-kTopSafeSpace);
        
    }];
    
    
    
    _DBackScrollView = [[DetailBackScrollview alloc] init];
    _DBackScrollView.userInteractionEnabled = YES;
    _DBackScrollView.hidden = NO;
    _DBackScrollView.bounces = YES;
    _DBackScrollView.delegate = self;
    [downView addSubview:_DBackScrollView];
    [_DBackScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(downView.mas_top);
        make.left.mas_equalTo(downView.mas_left);
        make.right.mas_equalTo(downView.mas_right);
        make.bottom.mas_equalTo(downView.mas_bottom);
        
    }];
    
    
    
    //    EPGDateSelectedView *dateView = [[EPGDateSelectedView alloc] init];
    //    [dateView setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
    //    [downView addSubview:dateView];
    //
    //    [dateView mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(downView.mas_top);
    //        make.left.mas_equalTo(downView.mas_left);
    //        make.right.mas_equalTo(downView.mas_right);
    ////        make.bottom.mas_equalTo(downView.mas_bottom);
    //        make.height.mas_equalTo(32*kRateSize);
    //
    //
    //    }];
    //
    //    _EPGListTableView = [[UITableView alloc] init];
    //    [_EPGListTableView setBackgroundColor:[UIColor clearColor]];
    //    _EPGListTableView.delegate = self;
    //    _EPGListTableView.dataSource = self;
    //    _EPGListTableView.contentOffset = CGPointMake(0, 0);
    //    _EPGListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    [downView addSubview:_EPGListTableView];
    //
    //    [_EPGListTableView mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(dateView.mas_bottom).offset(1);
    //        make.left.mas_equalTo(downView.mas_left);
    ////        make.height.mas_equalTo(150*kRateSize);
    //        make.width.mas_equalTo(kDeviceWidth);
    //        make.bottom.mas_equalTo(wself.view.mas_bottom);
    //
    //    }];
    
    
    
    //只显示文字
    
    //
    //    _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    _HUD.delegate = self;
    //
    //    _HUD.mode = MBProgressHUDModeAnnularDeterminate;
    //    _HUD.delegate = self;
    //    _HUD.labelText = @"Loading";
    _BufferLabel = [[UILabel alloc] init];
    _BufferLabel.backgroundColor = CLEARCOLOR;
    _BufferLabel.textAlignment = NSTextAlignmentCenter;
    _BufferLabel.text = @"未获取到内容，请点击页面重试";
    _BufferLabel.userInteractionEnabled = YES;
    _BufferLabel.hidden = YES;
    [downView addSubview:_BufferLabel];
//***************  加载数据失败 **********
    _bufferView = [[UIView alloc] init];
    [_bufferView setBackgroundColor:App_background_color];
    _bufferView.hidden = YES;
    [downView addSubview:_bufferView];
    
    [_bufferView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(downView.mas_top).offset(35*kRateSize);
        make.left.mas_equalTo(downView);
        make.right.mas_equalTo(downView);
        make.bottom.mas_equalTo(downView);
    }];
    
    _bufferBackImageView = [[UIImageView alloc] init];
    _bufferBackImageView.image = [UIImage imageNamed:@"bg_home_poster_onloading"];
    [_bufferView addSubview:_bufferBackImageView];
    
    [_bufferBackImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(320*kRateSize, 160*kRateSize));
        make.centerX.mas_equalTo(_bufferView);
        make.top.mas_equalTo(_bufferView.mas_top).offset(80*kRateSize);
    }];
    
    MyGesture *refreshTap = [[MyGesture alloc] initWithTarget:self action:@selector(detailRefresh)];
    refreshTap.numberOfTapsRequired = 1;
    [_bufferView addGestureRecognizer:refreshTap];
    [_BufferLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(downView.centerX);
        make.top.mas_equalTo(downView.mas_top).offset(90*kRateSize);
        make.size.mas_equalTo(CGSizeMake(kDeviceWidth-20*kRateSize, 100*kRateSize));
        
    }];
    
    __block NSInteger weakPlayTVType = PlayTVType;
    
    _errorMaskView = [[ErrorMaskView alloc] init];
    _errorMaskView.retryAction = ^(){
        [wself playBackChannelVideoModoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",wself.currentURL]]  andPlayTVType:weakPlayTVType];
        NSLog(@"ok ~");
    };
    
    _errorMaskView.hidden = YES;
    [_tapView addSubview:_errorMaskView];
    
    [_errorMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.tapView);
    }];
    
    
    _shareViewVertical = [[ShareViewVertical alloc]init];
    _shareViewVertical.hidden = YES;
    [_shareViewVertical setInstalledQQ:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]];
    [_shareViewVertical setInstalledWX:[[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]];
    
    [wself.view addSubview:_shareViewVertical];
    
    [_shareViewVertical mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.view);
    }];

    
    
    
    _shareViewVertical.returnBlock = ^(NSInteger tag){
        NSString *tempLink;
        
        if (!isEmptyStringOrNilOrNull([KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:CurrentPlayVod])) {
            tempLink = [KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:CurrentPlayVod];

        }else{
            tempLink = @"";
        }
        
        NSString* thumbURL = NSStringIMGFormat(wself.demandModel.imageLink);
        
        NSString *contentString = [NSString stringWithFormat:@"我正在看%@，太精彩了！安装#%@#手机客户端，电视点播免费看啦！快快安装一起看吧。",wself.demandModel.name,kAPP_NAME];
        switch (tag) {
            case 0:
            {
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:Wechat currentController:wself];
                wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        [wself hideNaviBar:YES];
                        [wself actionPlayForVideo];
                        
                    }
                };
                
            }
                break;
            case 1:
            {
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:WechatZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess) {
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        [wself hideNaviBar:YES];
                        [wself actionPlayForVideo];
                        
                    }
                };
                
            }
                break;
            case 2:
            {
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:QQ currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess) {
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        [wself hideNaviBar:YES];
                        [wself actionPlayForVideo];
                        
                    }
                };
                
            }
                break;
            case 3:
            {
                [wself.shareViewVertical UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:QQZone currentController:wself];
                wself.shareViewVertical.shareSuccessBlock = ^(NSInteger isSuccess) {
                    if (isSuccess == 1) {
                        [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                        NSLog(@"分享成功！");
                        wself.shareViewVertical.hidden = YES;
                        [wself hideNaviBar:YES];
                        [wself actionPlayForVideo];
                        
                    }
                };
                
            }
                break;
                
            default:
                break;
        }
        
    };
    
    _moviePlayerBottomView = [[DetailTopBarView alloc] initTopBarWithShareBlock:^{
        NSLog(@"我要分享了");

        _shareViewVertical.hidden = NO;
        
        
        
    } andWithFavBlock:^(BOOL isFavor){
        if (isFavor) {
            NSLog(@"我要收藏了");
//            [_moviePlayerBottomView resetFavorate:YES];
            if (IsLogin) {
                
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                
                [request ADDVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;

                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }

            }
            

            
        }else{
            NSLog(@"我要取消收藏了");
//            [_moviePlayerBottomView resetFavorate:NO];
            if (IsLogin) {
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                
                [request DELVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;

                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;

                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }

            }

            
        }
    }];

    
    if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession] && ![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]) {
        [_moviePlayerBottomView.shareBtn setImage:[UIImage imageNamed:@"btn_more_share_not"] forState:UIControlStateNormal];
        _moviePlayerBottomView.shareBtn.userInteractionEnabled = NO;
    }
    //    [topBar resetFavorate:YES];
    [downView addSubview:_moviePlayerBottomView];
    
    [_moviePlayerBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(downView);
        make.top.mas_equalTo(downView);
        make.right.mas_equalTo(downView);
        make.height.mas_equalTo(35*kRateSize);
    }];

    
    _installMaskView = [[PlayerMaskView alloc]init];
    _installMaskView.programType = InstallType;
    _installMaskView.hidden = YES;
    [_installMaskView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"installMaskView"];
    
    _blackInstallView = [[BlackInstallView alloc]init];
    _blackInstallView.userInteractionEnabled = YES;
    [_blackInstallView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    //    _blackInstallView.alpha = 0.8;
    [_blackInstallView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"blackInstallView"];
    _blackInstallView.hidden = YES;
    [self.view  addSubview:_blackInstallView];
    [_blackInstallView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.view.mas_left);
        make.top.mas_equalTo(wself.view.mas_top);
        make.bottom.mas_equalTo(wself.view.mas_bottom);
        make.right.mas_equalTo(wself.view.mas_right);
    }];
    
    
    _blackInstallView.cancelButtonClick = ^(BOOL isSelect)
    {
        wself.blackInstallView.hidden = YES;
        
    };
    
    _blackInstallView.confirmButtonClick = ^(NSString *nameStr,NSString *addressStr,NSString *teleStr,NSString *remarkStr,BOOL isSelect)
    {
        HTRequest *htrequest = [[HTRequest alloc]initWithDelegate:wself];
        [htrequest JXBaseBusinessInstallInfoWithName:nameStr withTel:teleStr withAddress:addressStr withDescription:remarkStr];
        
    };
    
    
    
    _installMaskView.maskViewReturnBlock = ^(){
        wself.blackInstallView.hidden = NO;
        
        
        
    };
    [_tapView addSubview:_installMaskView];
    
    [_installMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(_tapView);
        
    }];

    
    
}

//添加监听观察者
/**
 *  监听按钮状态改变的方法
 *
 *  @param keyPath 按钮改变的属性
 *  @param object  按钮
 *  @param change  改变后的数据
 *  @param context 注册监听时context传递过来的值
 */
//-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    UIImageView *videoView = (UIImageView *)object;
//    if (_videoListView == videoView && [@"hidden" isEqualToString:keyPath]) {
//        NSLog(@"self.videolistView的hidden属性改变了%@",[change objectForKey:@"new"]);
////        if ([[change objectForKey:@"new"] integerValue] == 0) {
////            
////            _ADViewBase.hidden = NO;
////        }
//        _ADViewBase.hidden = _videoListView.hidden;
//    }
//    
//    
//    
//    
//}



-(void)loginAfterRefreshFavor
{
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_DEMAND_LIST object:nil];
    
//    [[UserCollectCenter defaultCenter] refreshDemandListWith:^(BOOL isSuccess, NSArray *resultList) {
//        if (isSuccess) {
//            if (IsLogin) {
//                BOOL isFavored = [[UserCollectCenter defaultCenter]checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID];
//                
//                if (isFavored) {
//                    
//                    //            [self favorSucceed:YES];
//                    [_moviePlayerBottomView resetFavorate:YES];
//                    [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
//                    
//                }else
//                {
//                    
//                    //            [self favorSucceed:NO];
//                    [_moviePlayerBottomView resetFavorate:NO];
//                    [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
//                    
//                }
//                
//                
//            }else
//            {
//                
//                
//                //        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:_contentView withLocationTag:2];
//                
//            }
//            
//        }
//    }];
    
    //ww...
    [_localListCenter refreshVODCollectListWithUserType:IsLogin andBlock:^(BOOL isExist) {
        if (isExist) {
            BOOL isFavored = [_localListCenter checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID andUserType:IsLogin];
            if (isFavored) {
                //            [self favorSucceed:YES];
                [_moviePlayerBottomView resetFavorate:YES];
                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
            }else{
                //            [self favorSucceed:NO];
                [_moviePlayerBottomView resetFavorate:NO];
                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
            }
        }
    }];
   
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    
    UIView *listVIew = (UIView *)object;
    if (_adPlayerView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_adPlayerView.hidden == YES) {
            
            _adPlayerView.isCanContinu = NO;
            _returnMainBtn.alpha = 1;
//            _topBarIV.hidden = NO;

        }else
        {
            _adPlayerView.isCanContinu = YES;
            _returnMainBtn.alpha = 0;
//            _topBarIV.hidden = YES;

        }
    }
    
    
    if (_playerLoginView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_playerLoginView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;
            _bottomBarIV.hidden = NO;

        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _installMaskView.hidden = YES;
        }
    }
    
    
    if (_jxPlayerLoginView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_jxPlayerLoginView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;
            _bottomBarIV.hidden = NO;
            
        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _installMaskView.hidden = YES;
        }
    }

    
    
    if (_ZhuanWangMaskView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_ZhuanWangMaskView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;

            _bottomBarIV.hidden = NO;

        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _installMaskView.hidden = YES;
        }
    }
    
    if (_orderView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        
        if (_orderView.hidden == YES) {
            
            [_contentView addGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = YES;
            _bottomBarIV.hidden = NO;

        }else
        {
            [_contentView removeGestureRecognizer:_panGesture];
            _videoProgressV.userInteractionEnabled = NO;
            _videoProgressV.value = 0;
            _currentTimeL.text = @"00:00:00";
            _totalTimeL.text = @"00:00:00";
            _polyLoadingView.hidden = YES;
            _installMaskView.hidden = YES;
        }
    }

    if (_installMaskView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        if (_installMaskView.hidden == NO) {
            _playerLoginView.hidden = YES;
            _jxPlayerLoginView.hidden = YES;
            _bottomBarIV.hidden = YES;
            _errorMaskView.hidden = YES;
            _polyLoadingView.hidden = YES;
            _loadingMaskView.hidden = YES;
            _isEPGPlayerEnter = NO;
        }
    }

    if (_blackInstallView == listVIew && [@"hidden" isEqualToString:keyPath]) {
        if (_blackInstallView.hidden == NO) {
            
            _isScreenLocked = YES;
            
        }else
        {
            _isScreenLocked = NO;
            
        }
    }

    
//    UIImageView *listVIEW = (UIImageView *)object;
//
//    if (_topBarIV == listVIEW && [@"hidden" isEqualToString:keyPath]) {
//        
//        if (_topBarIV.hidden == YES) {
//            
//            _screenLockedIV.hidden = YES;
//            _screenLockedIV.alpha = 0;
//            
//        }else{
//            _screenLockedIV.hidden = NO;
//            _screenLockedIV.alpha = 1;
//            
//            
//        }
//    }

    
    
    
    
}



- (void)volumeChangedNotification:(NSNotification *)notification
{
    
    float volume =
    [[[notification userInfo]
      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
     floatValue];
    
    if (volume == 0) {
        [_adPlayerView resetvolumBtn:YES];
    }else{
        [_adPlayerView resetvolumBtn:NO];
    }
    
    
    
    
    
    //    DDLogVerbose(@"current volume = %f", volume);
}

-(void)detailRefresh{
    NSLog(@"刷新页面");
    
}



#pragma mark - 创建 拖动手势
- (UIPanGestureRecognizer*)panGesture
{
    if (!_panGesture) {
        _panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panAction:)];
        
    }
    return _panGesture;
    
}



#pragma mark - pan水平移动的方法
-(void)panAction:(UIPanGestureRecognizer*)pan
{
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:_contentView];
    CGPoint translatePoint = [pan locationInView:_contentView];
    // 判断是垂直移动还是水平移动
    
    
    CGRect frame = [UIScreen mainScreen].bounds;
    
    //判断屏幕得
    
    if([[[UIDevice currentDevice] valueForKey:@"orientation"] integerValue] == 1 ){
        
        frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width);
        
    }else{
        
        if (IsIOS8) {
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width);
        }else
        {
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
        }
        
    }
    
    
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{ // 开始移动
            //            NSLog(@"x:%f  y:%f",veloctyPoint.x, veloctyPoint.y);
            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            
            
            if (_isVerticalDirection == NO && _isHaveAD == NO) {
                
                
                if (x > y) { // 水平移动
                    _panDirection = PanDirectionHorizontalMoved;
                    // 取消隐藏
                    _progressTimeView.hidden = NO;
                    
                    
                    //                self.backView.alpha = 1;
                    
                    _ProgressBeginToMove = _videoProgressV.value;
                    
                    _JumMoveStartDemand = [self secondsFrom:_currentTimeL.text];
                    
                    // 给sumTime初值
                    //                _sumTime = CMTimeGetSeconds(_moviePlayer.currentTime);
                }
                else if (x < y){ // 垂直移动
                    
                    
                    if ((translatePoint.x > frame.size.height*0.5)){
                        
                        _panDirection = PanDirectionVerticalVolumeMoved;
                        // 显示音量控件
                        //                self.backView.alpha = 1;
                        // 开始滑动的时候，状态改为正在控制音量
                        //                    _isVolume = YES;
                        
                    }else if ((translatePoint.x < frame.size.height*0.5)){
                        _brightnessView.hidden = NO;
                        
                        _panDirection = PanDirectionVerticalBrighteMoved;
                    }
                    
                    
                    
                }
            }else
            {
                return;
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{ // 正在移动
            if (_isVerticalDirection == NO && _isHaveAD == NO) {
                
                switch (_panDirection) {
                    case PanDirectionHorizontalMoved:{
                        JumpStartTime = 0;
                        OnMove = YES;
                        [self StartNewTimer];
                        [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
                        break;
                    }
                    case PanDirectionVerticalVolumeMoved:{
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                    case PanDirectionVerticalBrighteMoved:{
                        _brightnessView.hidden = NO;
                        [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值
                        break;
                    }
                        
                    default:
                        break;
                }
            }else
            {
                return;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:{ // 移动停止
            // 移动结束也需要判断垂直或者平移
            // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
            switch (_panDirection) {
                case PanDirectionHorizontalMoved:{
                    _progressTimeView.hidden = YES;
                    
                    
                    if (_isVerticalDirection == NO && _isHaveAD == NO)
                        
                    {
                        
                        OnMove = NO;

                        [self updateProfressTimeLable];
                        
                        _ProgressBeginToMove = 0;
                        
                        }
                    else
                    {
                        return;
                    }
                    
                    
                    
                    break;
                }
                case PanDirectionVerticalVolumeMoved:{
                    
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                case PanDirectionVerticalBrighteMoved:{
                    _brightnessView.hidden = YES;
                    // 且，把状态改为不再控制音量
                    //                    _isVolume = NO;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark - pan垂直移动的方法
- (void)verticalMoved:(CGFloat)value
{
    
    NSInteger index = (NSInteger)value;
    
    switch (_panDirection) {
        case PanDirectionVerticalVolumeMoved:
            
        {
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素声音减一格
                    
                    [self volumeAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素声音增加一格
                        [self volumeAdd:+0.05];
                }
            }
            
            
        }
            break;
        case PanDirectionVerticalBrighteMoved:
        {
            
            if(index>0)
            {
                if(index%5==0)
                {//每10个像素亮度减一格
                    [self brightnessAdd:-0.05];
                }
            }
            else
            {
                if(index%5==0)
                {//每10个像素亮度增加一格
                    
                    [self brightnessAdd:0.05];
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
}

#pragma mark - pan水平移动的方法
- (void)horizontalMoved:(CGFloat)value
{
    _progressTimeView.hidden = NO;
    // 每次滑动需要叠加时间
    _ProgressBeginToMove += value / 150;
    
    NSLog(@"水平移动的方法的值%lf",value);
    
    if (value >0) {
        
        [_progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_forward"]];
    }else
    {
        [_progressDirectionIV setImage:[UIImage imageNamed:@"ic_player_full_fast_backward"]];
    }


    if (_ProgressBeginToMove > _moviePlayer.duration) {
        _videoProgressV.value = _videoProgressV.value + 200;
    }else if (_ProgressBeginToMove < 0){
        _ProgressBeginToMove = 0;
    }
    _videoProgressV.value = _ProgressBeginToMove;

    NSInteger mmmInterger = _moviePlayer.duration * (_videoProgressV.value/1000);
    [_moviePlayer setCurrentPlaybackTime:mmmInterger];
    
    NSInteger Jumphour = (mmmInterger/3600);
    NSInteger JumpMinute = (mmmInterger - (Jumphour*3600)) / 60 ;
    NSInteger JumpSecond = (mmmInterger -  Jumphour*3600 - JumpMinute*60);
    NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
    [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
    
    
    
    [_progressTimeLable_top setText:[NSString stringWithFormat:@"%@",TimeString]];
    
    
    
    NSInteger nInterger = _moviePlayer.duration;
    NSInteger NJumphour = (nInterger/3600);
    NSInteger NJumpMinute = (nInterger - (NJumphour*3600)) / 60 ;
    NSInteger NJumpSecond = (nInterger -  NJumphour*3600 - NJumpMinute*60);
    NSString *NTimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",NJumphour,NJumpMinute,NJumpSecond];
    
    [_progressTimeLable_bottom setText:[NSString stringWithFormat:@"/%@",NTimeString]];
    
    
    
    
}

- (void)updateProfressTimeLable{
    
    JumpStartTime = _moviePlayer.duration * (_videoProgressV.value/1000);
    
    NSInteger Jumphour = (JumpStartTime/3600);
    NSInteger JumpMinute = (JumpStartTime - (Jumphour*3600)) / 60 ;
    NSInteger JumpSecond = (JumpStartTime -  Jumphour*3600 - JumpMinute*60);
    NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
    [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
    
    if (IsLogin) {
        [self recordPlaybackTime];
    }else{
        //        [self recordLocalPlaybackTime];
    }
    
    [_moviePlayer setCurrentPlaybackTime:JumpStartTime];

    if (_moviePlayer.duration == JumpStartTime) {
        // 用户拉到最后
        [self ChangeCurTimeForNewPort];
        return;
    }
    
  
    
}


-(void)timeSystemStr
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"HH:mm"];
    
    NSString *timestamp = [formatter stringFromDate:[NSDate date]];
    
    //    ti.text = timestamp;
    [_timeButton setTitle:[NSString stringWithFormat:@"%@",timestamp] forState:UIControlStateNormal];
}




#pragma tap点击事件


-(void)TapSmallScreenbtn_playNoButton
{

    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request VODDetailWithContentID:self.demandModel.contentID andTimeout:kTIME_TEN];
    
    
}

-(void)TapSmallScreenbtn_play:(UIButton*)sender
{
    
    
    [_moviePlayer stop];
    
    BOOL isOwn = self.demandModel.isOwn;
    if ([self.demandModel.seriesList count]>0) {
        
        NSString *ownStr = [[self.demandModel.seriesList objectAtIndex:0] objectForKey:@"isOwn"];
        if ([ownStr isEqualToString:@"true"]) {
            isOwn = YES;
        }else{
            isOwn = NO;
        }
        
    }
    
    if (!isOwn) {
        //        CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"该节目为付费节目，是否继续播放？" andButtonNum:2 withDelegate:self];
        //        cusAlert.tag = 77;
        //        [cusAlert show];
        
    }
    
    
    _isTapPlayBtn = YES;
    _playBackgroundIV.hidden = YES;
    
    
    //    [_moviePlayer play];
    //    [self StartNewTimer];
    _isTapPlayBtn = YES;
    _bottomBarIV.userInteractionEnabled = YES;
    
    if (_isMovieCategory == NO) {
        
        //        NSString *contentID = self.contentID;
        if ([self.demandModel.seriesList count]>0) {
            
            if (!self.episodeSeq) {
                self.contentID = [[self.demandModel.seriesList objectAtIndex:0] objectForKey:@"contentID"];
                
                
                self.demandModel.name = [[self.demandModel.seriesList objectAtIndex:0] objectForKey:@"name"];
                
                [_DBackScrollView episodeViewSetSelectedBtnAtIndex:1];
                _channelSelectIndex = 0;
                
            }else
            {
                self.contentID = [[self.demandModel.seriesList objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"contentID"];
                if (!self.episodeName) {
                    if (_contentTitleName) {
                        _serviceTitleLabel.text = _contentTitleName;
                    }else
                    {
                    _serviceTitleLabel.text = @"";
                    }
                }
                _serviceTitleLabel.text = self.episodeName;
                
                [_DBackScrollView episodeViewSetSelectedBtnAtIndex:[self.episodeSeq integerValue]];
                _channelSelectIndex = [self.episodeSeq integerValue]-1;
                
                
            }
            
            
            
        }
        
        
    }else
    {
        _channelSelectIndex = -1;
    }

    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

//    [request VODPlayWithContentID:self.contentID andWithMovie_type:movieType andTimeout:kTIME_TEN];
    if (ISIntall) {
        
        _installMaskView.hidden = NO;
    }else
    {
    [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETPLAYRECORDERSUCCESS" object:nil];
    
    
    
    
}


#pragma mark - 多码率相关
//多码率
-(NSMutableString *)deleteString:(NSString *)DStr fromString:(NSMutableString *)FStr{
    NSMutableString *mFstr = [[NSMutableString alloc] initWithFormat:@"%@",FStr];
    NSInteger location = [FStr rangeOfString:DStr].location;
    if ([FStr rangeOfString:DStr].location == NSNotFound) {
        return FStr;
    }else{
        [mFstr deleteCharactersInRange:NSMakeRange(location, 2)];
        [self deleteString:DStr fromString:mFstr];
    }
    
    return FStr;
    
}
//<<<<<<< .mine
//=======
#pragma mark - 多码率相关
-(NSMutableArray *)getCodeRateCategoryWithLivType:(NSInteger)liveType{
    
    NSMutableArray *mCodeArr = [[NSMutableArray alloc] init];
    
    NSString *superRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:1 andLiveType:0];
    NSString *highRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:2 andLiveType:0];
    NSString *biaoRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:3 andLiveType:0];
    NSString *liuRateURL = [self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:4 andLiveType:0];
    
    if (superRateURL.length>0) {
        //        [mCodeDic setObject:@"超清" forKey:@"superRateURL"];
        NSMutableDictionary *superdic = [[NSMutableDictionary alloc]init];
        [superdic setValue:@"超清" forKey:@"type"];
        [superdic setValue:[NSString stringWithFormat:@"%d",SUPER_CLEAR] forKey:@"tag"];
        [mCodeArr addObject:superdic];
    }
    
    if (highRateURL.length>0) {
        NSMutableDictionary *highdic = [[NSMutableDictionary alloc]init];
        [highdic setValue:@"高清" forKey:@"type"];
        [highdic setValue:[NSString stringWithFormat:@"%d",HIGH_DEFINE] forKey:@"tag"];
        [mCodeArr addObject:highdic];
    }
    
    
    if (biaoRateURL.length>0) {
        NSMutableDictionary *origindic = [[NSMutableDictionary alloc]init];
        [origindic setValue:@"标清" forKey:@"type"];
        [origindic setValue:[NSString stringWithFormat:@"%d",ORIGINAL_PAINT] forKey:@"tag"];
        [mCodeArr addObject:origindic];
        
    }
    
    
    if (liuRateURL.length>0) {
        //        [mCodeDic setObject:@"流畅" forKey:@"liuRateURL"];
        NSMutableDictionary *smoothdic = [[NSMutableDictionary alloc]init];
        [smoothdic setValue:@"流畅" forKey:@"type"];
        [smoothdic setValue:[NSString stringWithFormat:@"%d",SMOOTH] forKey:@"tag"];
        [mCodeArr addObject:smoothdic];
        
    }
    
    
    return mCodeArr;
}
////多码率
//-(NSMutableString *)deleteString:(NSString *)DStr fromString:(NSMutableString *)FStr{
//    NSMutableString *mFstr = [[NSMutableString alloc] initWithFormat:@"%@",FStr];
//    NSInteger location = [FStr rangeOfString:DStr].location;
//    if ([FStr rangeOfString:DStr].location == NSNotFound) {
//        return FStr;
//    }else{
//        [mFstr deleteCharactersInRange:NSMakeRange(location, 2)];
//        [self deleteString:DStr fromString:mFstr];
//    }
//
//    return FStr;
//
//}
//根据 直播方式LiveType 和 清晰度TVCodeType
-(NSString *)getHIGHDefintionUrlStringWithM3U8String:(NSString *)m3u8 andTVCodeType:(NSInteger)TVCodeType andLiveType:(NSInteger)LiveType{
    
    //-----------type    1：超清  2：高清  3：标清  4：流畅
    if (m3u8.length<20) {
        //如果M3U8字符本来就是错误的那么就不用解析了 直接返回空播放链接
        return @"";
    }
    NSMutableString *m3u8Str = [[NSMutableString alloc] initWithString:m3u8];
    [m3u8Str deleteCharactersInRange:NSMakeRange(0, 7)];
    NSArray *m3u8Arr = [m3u8Str componentsSeparatedByString:@"?"];
    NSMutableArray *IPArr = [[NSMutableArray alloc] init];
    NSMutableArray *bandWidthArr = [[NSMutableArray alloc] init];
    
    //------分割 拼接 播放链接数组 和 带宽（码率）数组
    for (int i=0; i<m3u8Arr.count; i++) {
        NSString *str = [m3u8Arr objectAtIndex:i];
        NSString *IPStr = [[str componentsSeparatedByString:@"http"] lastObject];
        NSString *bandStr = [[[[str componentsSeparatedByString:@"http"] objectAtIndex:0] componentsSeparatedByString:@"BANDWIDTH="] lastObject];
        if ([bandStr rangeOfString:@","].location!=NSNotFound) {
            //带宽（码率）字符串后面还有可能 拼接上其他的字符 是以","字符分割的
            bandStr = [[bandStr componentsSeparatedByString:@","] firstObject];
        }
        [IPArr addObject:[NSString stringWithFormat:@"http%@",IPStr]];
        [bandWidthArr addObject:bandStr];
    }
    [bandWidthArr removeLastObject];//过滤掉非数值的 假的 带宽元素
    
    NSArray *truthArr = [m3u8Str componentsSeparatedByString:@"#"];
    NSMutableArray *mutTruthArr = [[NSMutableArray alloc] initWithArray:truthArr];
    //zzsc
    for (int i=0; i<mutTruthArr.count; i++) {
        NSString *link = [mutTruthArr objectAtIndex:i];
        if (link.length<22) {
            [mutTruthArr removeObjectAtIndex:i];
        }
    }
    
    NSMutableArray *longIPArr = [[NSMutableArray alloc] init];//这个数组用来接收 拼接上 http 字符的的真正的全连接
    for (int i=0; i<mutTruthArr.count; i++) {
        
        NSArray *itemArr = [[mutTruthArr objectAtIndex:i] componentsSeparatedByString:@"http"];
        [longIPArr addObject:[NSString stringWithFormat:@"http%@",[itemArr lastObject]]];
        
    }
    //------码率 、连接 分割结束
    
    if (longIPArr.count>0) {
        
        NSString *firstLink = [longIPArr objectAtIndex:0];
        if ([firstLink hasPrefix:@"httpEXT"]) {
            [longIPArr removeObjectAtIndex:0];//就点播的需要加 因为点播的比直播多了一行
            
        }
        NSString *lastLink = [longIPArr lastObject];
        if ([lastLink hasPrefix:@"httpEXT"]) {
            [longIPArr removeLastObject];
        }
        
    }else{
        return @"";
    }
    //-----------数组排序
    if (longIPArr.count==bandWidthArr.count) {
        //只有IP数组和带宽数组长度一致才允许进入这里
        
        for (int i=0; i<bandWidthArr.count; i++) {
            
            for (int j=1; j<bandWidthArr.count; j++) {
                //码率（带宽）按照从高到底排列
                if ([bandWidthArr objectAtIndex:i]<[bandWidthArr objectAtIndex:j]) {
                    [bandWidthArr exchangeObjectAtIndex:i withObjectAtIndex:j];
                    [longIPArr exchangeObjectAtIndex:i withObjectAtIndex:j];
                }
                
            }
            
        }
        
        
    }else{
        
        longIPArr = (NSMutableArray *)[[longIPArr reverseObjectEnumerator] allObjects];
    }
    //--------数组排序结束
    
    if (TVCodeType<0 || TVCodeType>4) {
        
        return [longIPArr lastObject];//默认 播放流畅的
    }
    
    //zzs码率策略修改   下面的是
    if (longIPArr.count>(4-TVCodeType)) {
        if (TVCodeType<=0) {
            return [longIPArr firstObject];
        }
        return [longIPArr objectAtIndex:(4-TVCodeType)];
        
    }else{
        return @"";
        
    }
    //zzs码率策略修改  上面的是
//    switch (TVCodeType) {
//        case 1:{
//            for (int i=0; i<longIPArr.count; i++) {
//                
//                if (bandWidthArr.count<=i) {
//                    return @"";
//                }
//                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==1) {
//                    
//                    return [longIPArr objectAtIndex:i];
//                }
//            }
//            
//            break;
//        }
//        case 2:{
//            for (int i=0; i<longIPArr.count; i++) {
//                if (bandWidthArr.count<=i) {
//                    return @"";
//                }
//                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==2) {
//                    
//                    
//                    return [longIPArr objectAtIndex:i];
//                }
//            }
//        }
//        case 3:{
//            for (int i=0; i<longIPArr.count; i++) {
//                if (bandWidthArr.count<=i) {
//                    return @"";
//                }
//                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==3) {
//                    
//                    return [longIPArr objectAtIndex:i];
//                }
//            }
//        }
//        case 4:{
//            for (int i=0; i<longIPArr.count; i++) {
//                if (bandWidthArr.count<=i) {
//                    return @"";
//                }
//                if ([self getTheTypeWithTVRate:[[bandWidthArr objectAtIndex:i] floatValue] andTVType:LiveType]==4) {
//                    
//                    return [longIPArr objectAtIndex:i];
//                }
//            }
//        }
//    }
//    
//    return @"";
}


//--------------根据码率  区分高清还是标清
-(NSInteger)getTheTypeWithTVRate:(CGFloat)rate andTVType:(NSInteger)type{
    
    if (type==0) {
        if (rate< 819200) {// 0.5M 524288 0.8M  819200
            return 4;//流畅
        }
        if (rate>524288&&rate<1048576) {
            return 3;//标清
        }
        if (rate>1048576&&rate<2097152) {
            return 2;//高清
        }
        if (rate>2097152&&rate<4194304) {
            return 1;//超清
        }
        
    }else{
        
        if (rate<1048576) {
            return 4;//流畅
        }
        if (rate>1048576&&rate<2097152) {
            return 3;//标清
        }
        if (rate>2097152&&rate<4194304) {
            return 2;//高清
        }
        if (rate>4194304&&rate<8388608) {
            return 1;//超清
        }
        
    }
    return 0;//返回类型 3：超清 2：高清 1：标清 0：流畅
    //   type:  0-直播 1-回看
    /*
     0.5M - 524288
     1M - 1024     1048576
     2M - 2048     2097152
     4M - 4096     4194304
     8M - 8192     8388608
     -------直播---------
     超清：2M--4M
     高清：1M--2M
     标清：0.5M--1M
     流畅：<0.5M
     --------回看---------
     超清：4M--8M
     高清：2M--4M
     标清：1M--2M
     流畅：<1M
     */
}




- (void)playBackChannelVideoModoWithURL:(NSURL *)url andPlayTVType:(NSInteger)PlayTVType
{
    _errorMaskView.hidden = YES;
    [self clearMoviePlayerStop];
    
    if (_isHaveAD == YES) {
        _topBarIV.hidden = YES;
        _returnMainBtn.alpha = 0;
        _bottomBarIV.hidden = YES;
        _returnBtnIV.hidden = YES;

    }else
    {
        _topBarIV.hidden = NO;
        _returnMainBtn.alpha = 1;
        _bottomBarIV.hidden = NO;
        _returnBtnIV.hidden = NO;

        
    }
    //    if ([[url description] length] == 0) {
    //        [self displayError:nil];
    //        return;
    //    }
    //
    //----改变UI
    //JumpStartTime = 0;
    //    _videoProgressV.value = 0;
    
    
    [_currentTimeL setText:@"00:00:00"];
    [_totalTimeL setText:@"00:00:00"];
    
    //    _isPaused = NO;
    
    
    
    
    //    NSString *breakpoint =   [[UserPlayRecorderCenter defaultCenter]checkTheVODListWithContentID:_demandModel.contentID];
    
    
    
    //    if (!JumpStartTime) {
    //        JumpStartTime = 0;
    //    }
    
    //    _isPrepareToPlay = YES;
    
    
    //    JumpStartTime = [breakpoint integerValue];
    //    _videoProgressV.value = JumpStartTime;//修改 进度条的 值
    
    /*
     这里应该添加新的多码率的相关代码
     */
    //    [_moviePlayer setContentURL:url];
    //    self.isPlay = YES;
    //    [_moviePlayer play];
    //    [self StartNewTimer];
    
    //<<<<<< .mine
    //=======
    
    //---下面是多码率代码
    //_allCodeRateLink 是包含所有么率的那个全部的链接 每次鉴权成功的时候清空
   
        if ([netWork isEqualToString:@"WIFI"]) {
            
            [self allcodeRateRequest:url];
            
        }else if ([netWork isEqualToString:@"无网络"])
        {
            return;
        }else if([netWork isEqualToString:@"2G"]||[netWork isEqualToString:@"3G"]||[netWork isEqualToString:@"4G"])
        {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 2222;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
                
                
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
                
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
                    
                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
                    cusAlert.tag = 333333;
                    cusAlert.URLStr = url;
                    [cusAlert show];
                    
                    
                    
                }else
                {
                    [self allcodeRateRequest:url];
                }
                
           
                
            }
          
        }
    
    //
//    [[NetWorkNotification_shared shardNetworkNotification] getNetWorkingTypeAndNameWith:^(NSInteger status, NSString *netname) {
//        if ([netname isEqualToString:@"当前无可用网络"]) {
//            return ;
//        }else if([netname isEqualToString:@"您正在使用移动网络"]){
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"0"]) {
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 2222;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//            }
//            
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] isEqualToString:@"1"]) {
//                
//                //            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
//                
//                
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstAlert"] == YES) {
//                    
//                    CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"您正在使用移动网络！\n继续观看将产生流量，是否继续" andButtonNum:2 withDelegate:self];
//                    cusAlert.tag = 333333;
//                    cusAlert.URLStr = url;
//                    [cusAlert show];
//                    
//                    
//                    
//                }else
//                {
//                    [self allcodeRateRequest:url];
//                }
//                
//                
//                
//                
//                
//            }
//            
//            
//            
//            
//
//            
//        }else{
//            [self allcodeRateRequest:url];
//            
//        }
//        
//    }];

    
    
    
    
    
    
    
    
    
    
    
    
}




-(void)actionPlayBackRecorderListRepitTime
{
    if (self.getPlayBackRecordListRepitTimer) {
        [self.getPlayBackRecordListRepitTimer invalidate];
        self.getPlayBackRecordListRepitTimer = nil;
    }
    self.getPlayBackRecordListRepitTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(recordPlaybackTime) userInfo:nil repeats:YES];
}



-(void)allcodeRateRequest:(NSURL *)url
{

    [_errorManager resetPlayerStatusInfo];
    
    _adPlayerView.timeLabel.text = @"";
    if (_isHaveAD == NO) {
    
    
    NSString *playLink = [url absoluteString];
    NSString *formLink = [[playLink componentsSeparatedByString:@"?"] firstObject];
    NSString *type;
    if ([playLink rangeOfString:@"?"].location == NSNotFound) {
        type= [[playLink componentsSeparatedByString:@"."] lastObject];
    }else{
        type= [[formLink componentsSeparatedByString:@"."] lastObject];
    }
    if (![type isEqualToString:@"m3u8"]) {
        [clearTitles removeAllObjects];
        [self allCodeClearButtonTitle];

        [self clearMoviePlayerStop];
        
        
        [_moviePlayer setContentURL:url];
        
//        _moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
        [_moviePlayer prepareToPlay];
        [_moviePlayer play];

        _isPrepareToPlay = YES;
        self.isPlay = YES;
        [self actionPlayBackRecorderListRepitTime];
        return;
        
    }
    if (_allCodeRateLink.length>0) {
       

        NSMutableString *URLStr;
        //
        if (_allCodeRateLink.length>15) {
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:0]];
            [self allCodeClearButtonTitle];

            URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:_allCodeRateLink andTVCodeType:PlayTVType andLiveType:0]];
            if (URLStr.length<10 ) {
                if (clearTitles.count>0) {
                    [clearTitles removeAllObjects];
                }
                [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self.view withLocationTag:1];
                return;

            }else{
                if (clearTitles.count>0) {
                    [clearTitles removeAllObjects];
                }
                clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:0]];
                [self allCodeClearButtonTitle];

                [URLStr deleteCharactersInRange:NSMakeRange(URLStr.length-3, 2)];
                
                _shareURL =URLStr;
                
                self.demandModel.url = URLStr;
                
                [_moviePlayer setContentURL:[self getCanPlayURLWithString:URLStr]];
                // [_moviePlayer setContentURL:[NSURL URLWithString:URLStr ]];
                
                [_moviePlayer play];
                _isPrepareToPlay = YES;
                
                self.isPlay = YES;
                [self actionPlayBackRecorderListRepitTime];

                return;
            }
        }else{
            //可能不是完整的视频连接
            [clearTitles removeAllObjects];
            [self allCodeClearButtonTitle];

            [AlertLabel AlertInfoWithText:@"该视频暂无法播放" andWith:self.view withLocationTag:1];
            return ;
        }
    }else{
        NSString *mUrl = [NSString stringWithFormat:@"%@",url];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:mUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)  {
             NSString *text = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"获取到的多码率连接为：%@BOBO原URL---%@",text,url);
            _allCodeRateLink = text;
            if (clearTitles.count>0) {
                [clearTitles removeAllObjects];
            }
            clearTitles =[NSMutableArray arrayWithArray:[self getCodeRateCategoryWithLivType:0]];
            [self allCodeClearButtonTitle];
            
            NSMutableString *URLStr;
            if (text.length>15) {
                if (PlayTVType == 5) {
                    //----自动 默认选择
                    URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:text andTVCodeType:4 andLiveType:0]];
                    
                }else{
                    
                    URLStr = [NSMutableString stringWithFormat:@"%@",[self getHIGHDefintionUrlStringWithM3U8String:text andTVCodeType:PlayTVType andLiveType:0]];
                    
                }
                
            }else{
                
                [AlertLabel AlertInfoWithText:@"该码率暂无法播放,请切换可用码率" andWith:self.view withLocationTag:1];
                return ;
            }
            
            //                    URLStr = [[URLStr componentsSeparatedByString:@"?"] objectAtIndex:0];
            if (URLStr.length<22 ) {
                [AlertLabel AlertInfoWithText:@"该码率暂无法播放,请切换可用码率" andWith:self.view withLocationTag:1];
                
            }else{
                URLStr = [self deleteString:@" " fromString:URLStr];
                
                //         [URLStr deleteCharactersInRange:NSMakeRange(URLStr.length-2, 2)];
                
                NSLog(@"当前码率 ： %@",URLStr);
                
                self.demandModel.url = URLStr;
                
                
                [_moviePlayer setContentURL:[self getCanPlayURLWithString:URLStr]];
                
                [_moviePlayer prepareToPlay];
                [_moviePlayer play];
                _isPrepareToPlay = YES;
                self.isPlay = YES;
                [self actionPlayBackRecorderListRepitTime];

                
            }
            
            //            _moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
            [self allCodeClearButtonTitle];
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"发生错误！%@",error);
            [AlertLabel AlertInfoWithText:@"多码率信息错误" andWith:_contentView withLocationTag:1];
            [clearTitles removeAllObjects];
            [self allCodeClearButtonTitle];
            
            [_moviePlayer setContentURL:url];
            [_moviePlayer prepareToPlay];
            [_moviePlayer play];
            _isPrepareToPlay = YES;
            self.isPlay = YES;
            [self actionPlayBackRecorderListRepitTime];

        }];
        // ***********************************************
        
    }
        
        if (IsLogin) {
                    NSInteger break_hours = (JumpStartTime/3600);
        NSInteger break_minutes = (JumpStartTime - (break_hours*3600)) / 60 ;
        NSInteger break_seconds =(JumpStartTime -  break_hours*3600 - break_minutes*60);
        
        
        if (JumpStartTime>0) {
            [AlertLabel AlertInfoWithText:[NSString stringWithFormat:@"您上次观看至%02ld:%02ld:%02ld,即将为您续播",(long)break_hours,(long)break_minutes,(long)break_seconds] andWith:_contentView withLocationTag:2];;
        }
    }
//    [self StartNewTimer];
        
    }
    else
    {
        _adPlayerView.isCanContinu = YES;
        if (_adPlayerView.maskViewType == ImageAD) {
  
            [_adPlayerView resetImageADViewWithData:self.demandModel.adList];
        }else if (_adPlayerView.maskViewType == VideoAD)
        {
//            _adPlayerView.backImageView.image = nil;
            [_moviePlayer setContentURL:[NSURL URLWithString:_adVideoURL]];
            [_moviePlayer prepareToPlay];
            [_moviePlayer play];
            _adPlayerView.timeLabel.text = @"";
            _isPrepareToPlay = YES;
            self.isPlay = YES;
            
            
           

        }
        
        
        
         
        
    }
    
    [[self.view viewWithTag:4040]setHidden:YES];
    
    _errorManager.startErrorDate = [SystermTimeControl getNowServiceTimeDate];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(moviePlayBackDidFinish:)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:_moviePlayer];
}


-(NSURL *)getCanPlayURLWithString:(NSString *)urlStr{
    
    NSMutableString *mCanplay = [NSMutableString stringWithFormat:@"%@",urlStr];
    [mCanplay deleteCharactersInRange:NSMakeRange(mCanplay.length-2, 2)];
    
    NSURL *canPlayURL = [NSURL URLWithString:mCanplay];
    return canPlayURL;
}

-(void)StartNewTimer
{
    if (self.myNewTimer)
    {
        [self.myNewTimer invalidate];
    }
    self.myNewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ChangeCurTimeForNewPort) userInfo:nil repeats:YES];
}

-(void)ChangeCurTimeForNewPort
{
//    NSLog(@"HHHHHHH走了没有");
    //    if (_currentPlayType == CurrentPlayTimeMove) {
    //        return;
    //    }
    
    if (_isHaveAD == NO) {
        
   
    
    NSInteger movieDuration =  (NSInteger)_moviePlayer.duration;
    NSInteger currentPlaybackTime;

    if (movieDuration>0) {
        
        if (_moviePlayer.currentPlaybackTime <=0) {
            currentPlaybackTime = 0;
        }else
        {
            
            
            currentPlaybackTime = _moviePlayer.currentPlaybackTime;
        }
        if (currentPlaybackTime>= movieDuration) {
            currentPlaybackTime = movieDuration;
        }
    
    
        NSInteger total_hours = (movieDuration/3600);
        NSInteger total_minutes = (movieDuration - (total_hours*3600)) / 60 ;
        NSInteger total_seconds =(movieDuration -  total_hours*3600 - total_minutes*60);
        
        [_totalTimeL setText:[NSString stringWithFormat:@"%02ld:%02ld:%02ld",total_hours,total_minutes,total_seconds]];
        
        NSInteger currentPlayback_hours = (currentPlaybackTime / 3600);
        NSInteger currentPlayback_minutes = (currentPlaybackTime - (currentPlayback_hours*3600)) / 60 ;
        
        NSInteger currentPlayback_seconds =(currentPlaybackTime - currentPlayback_hours*3600 - currentPlayback_minutes*60);
        
        
        
        
        if ( currentPlaybackTime ==  movieDuration-10) {
            
            if (self.demandModel.seriesList.count<=0) {
                [AlertLabel AlertInfoWithText:@"即将播放下一节目" andWith:_contentView withLocationTag:2];
                
            }else
            {
                [AlertLabel AlertInfoWithText:@"即将播放下一集" andWith:_contentView withLocationTag:2];
                
            }
            
        }
        
        
//        NSLog(@"KKKKK%ld----%ld",movieDuration,currentPlaybackTime);
        if (movieDuration <= (currentPlaybackTime+1) || _isPlayEnd == YES)
            
        {
            _isPlayEnd = NO;
            
            _currentTimeL.text = [NSString stringWithFormat:@"%@", _totalTimeL.text];
//            [self.myNewTimer invalidate];
            if (IsLogin) {
                [self recordPlaybackTime];
            }else{
                //                [self recordLocalPlaybackTime];
            }
            
            [self playTheNextVideo];
            
            
        }
        
        
          if (!OnMove) {
       
        [_currentTimeL setText:[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currentPlayback_hours,currentPlayback_minutes,currentPlayback_seconds]];
        float progressValue = 1000 * currentPlaybackTime / movieDuration;
        [_videoProgressV setValue:progressValue animated:NO];
          }
        
    }
 }
}


-(void)playTheForwardVideo
{
    //    [_moviePlayer stop];
    [self clearMoviePlayerStop];
    switch (_vodPlayType) {
        case VODPlayMovie:
        {
            _videoPlayPosition -=1;
 
        }
            break;
        case VodPlayTVSerial:
        {

            _isPlayLastVideo = NO;

            if (_isPlayFirstVideo == YES) {
               
//                [_forwardButton setEnabled:YES];
            }else
            {
                
                if (_videoListArr.count>0) {
                    _videoPlayPosition -=1;
                    NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                    _channelSelectIndex = _videoPlayPosition;
                    
                    _demandModel.contentID = [video objectForKey:@"contentID"];
                    if (![video objectForKey:@"name"]) {
                        if (_contentTitleName) {
                            _serviceTitleLabel.text = _contentTitleName;
                        }else
                        {
                            _serviceTitleLabel.text = @"";
                        }                    }
                    if (_videoPlayPosition <= 0) {
                        
//                        [_forwardButton setEnabled:NO];
                        _isPlayFirstVideo = YES;
                        [AlertLabel AlertInfoWithText:@"已到节目首集" andWith:_contentView withLocationTag:2];
                    }
                    _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[video objectForKey:@"name"]];
                    [_DBackScrollView episodeViewSetSelectedBtnAtIndex:_videoPlayPosition+1];

                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    
//                    [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    
                    if (ISIntall) {
                        
                        _installMaskView.hidden = NO;
                    }else
                    {
                    [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    }
                }
                
            }
            
        }
            break;
        case VOdPlayVariety:
        {
            _isPlayLastVideo = NO;

            if (_isPlayFirstVideo == YES) {
                
//                _forwardButton.enabled = NO;
            }else
            {
                
            if (_videoListArr.count>0) {
                
                _videoPlayPosition -=1;
                NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                _channelSelectIndex = _videoPlayPosition;
                _demandModel.contentID = [video objectForKey:@"contentID"];
                if (![video objectForKey:@"subName"]) {
                    _serviceTitleLabel.text = @"";
                }
               
                _serviceTitleLabel.text = [video objectForKey:@"subName"];
                [_DBackScrollView varityViewSetselectedAtIndex:_videoPlayPosition+1];
                if (_videoPlayPosition <= 0) {
                    
//                    [_forwardButton setEnabled:NO];
                    _isPlayFirstVideo = YES;
                    [AlertLabel AlertInfoWithText:@"已到节目首集" andWith:_contentView withLocationTag:2];
                }
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                
//                [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                if (ISIntall) {
                    
                    _installMaskView.hidden = NO;
                }else
                {
                [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                }

            }
            
            

        }
            break;
    }
        default:
            break;
    }
    
    
    
    
    
}





-(void)playTheNextVideo
{
//    [[self.view viewWithTag:4040] setHidden:NO];
    _polyLoadingView.hidden = NO;
    [self clearMoviePlayerStop];
    
    
    //播放记录
    _localVideoModel.lvNAME = _demandModel.NAME;
    _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
    _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
    _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
    _localVideoModel.lvImageLink = _demandModel.imageLink;
    _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
    
//    [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    
    [_localVideoModel resetLocalVideoModel];
    
    
//    _forwardButton.enabled = YES;
                    if (USER_ACTION_COLLECTION == YES) {
    
    
//                        _watchDuration = [self timeLengthForCurrent];
    
//                        if ([_watchDuration intValue] >=5) {
//    
//    //                        [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//                            [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//                        }
    
                        if (_isCollectionInfo) {
                            _demandModel.contentID = _demandModel.contentID;
                            _demandModel.vodFenleiType = [self collectVodInformationType];
                            _demandModel.name = _demandModel.name;
                            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
                            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
                            _demandModel.vodBID = _demandModel.vodBID;
                            _demandModel.VodBName = _demandModel.VodBName;
                            _demandModel.category = _demandModel.category;
                            _demandModel.rootCategory = _demandModel.rootCategory;
                            _demandModel.TRACKID_VOD = _demandModel.TRACKNAME_VOD;
                            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
                            _demandModel.EN_VOD = _demandModel.EN_VOD;

                            
                        if ([_watchDuration intValue] >= 5) {
    
                            [SAIInformationManager VodInformation:_demandModel];
                        }
    
                        [_timeManager reset];
                        }
    
                    }
    
    
    
    switch (_vodPlayType) {
        case VOdPlayVariety:
        {
            if (_videoListArr.count>0) {
                
                _backwardButton.enabled = YES;
                
                _isPlayFirstVideo = NO;
                
                if (_isPlayLastVideo == YES) {
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//                    _forwardButton.enabled = NO;
                    _demandModel.TRACKID_VOD = [[_asscciationArr objectAtIndex:0] objectForKey:@"contentID"];
                    _demandModel.TRACKNAME_VOD = [[_asscciationArr objectAtIndex:0] objectForKey:@"name"];
                    _demandModel.EN_VOD = @"9";
                    [request VODDetailWithContentID:[[_asscciationArr objectAtIndex:0] objectForKey:@"contentID"] andTimeout:kTIME_TEN
                     ];
                    _isPlayLastVideo = NO;
                }else
                {
                    if (_videoPlayPosition < _videoListArr.count-1) {
                        _videoPlayPosition +=1;
                        
                        _channelSelectIndex = _videoPlayPosition;
                        
                        NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                        JumpStartTime = [[video objectForKey:@"breakPoint"] integerValue];
                        _serviceTitleLabel.text = [video objectForKey:@"subName"];
                        [_DBackScrollView varityViewSetselectedAtIndex:_videoPlayPosition+1];
                        
                        if (![video objectForKey:@"subName"]) {
                            _serviceTitleLabel.text = @"";
                        }
                        
                        
                        _demandModel.contentID = [video objectForKey:@"contentID"];
                        
//                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//                        
//                        [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                        _isPlayLastVideo = NO;
                        
                    }
                    
                    
                    if ( _videoPlayPosition == _videoListArr.count-1) {
                        _videoPlayPosition = _videoListArr.count-1;
                        _channelSelectIndex = _videoPlayPosition;
                        
                        NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                        _demandModel.contentID = [video objectForKey:@"contentID"];
                        JumpStartTime = [[video objectForKey:@"breakPoint"] integerValue];

                        _isPlayLastVideo = YES;
                        //                    _backwardButton.enabled = YES;
                        [AlertLabel AlertInfoWithText:@"已到最后集数" andWith:_contentView withLocationTag:2];
                        
                    }
                    
                    
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    
//                    [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    
                    _demandModel.TRACKID_VOD = @"";
                    _demandModel.TRACKNAME_VOD = @"";
                    _demandModel.EN_VOD = @"9";
                    
                    if (ISIntall) {
                        
                        _installMaskView.hidden = NO;
                    }else
                    {
                    [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    }
                    
                }
                
                
                
                
            }
  
        }
            break;
        case VodPlayTVSerial:
        {
            if (_videoListArr.count>0) {
                
                
                _isPlayFirstVideo = NO;
                _backwardButton.enabled = YES;
                
                if (_isPlayLastVideo == YES) {
//                    _forwardButton.enabled = NO;
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                    
                    [request VODDetailWithContentID:[[_asscciationArr objectAtIndex:0] objectForKey:@"contentID"] andTimeout:kTIME_TEN
                     ];
                    _isPlayLastVideo = NO;
                }else
                {
                    if (_videoPlayPosition < _videoListArr.count-1) {
                        _videoPlayPosition +=1;
                        
                        _channelSelectIndex = _videoPlayPosition;
                        
                        NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                        
                        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[video objectForKey:@"name"]];
                        JumpStartTime = [[video objectForKey:@"breakPoint"] integerValue];

                        [_DBackScrollView episodeViewSetSelectedBtnAtIndex:_videoPlayPosition+1];
                        
                        
                        if (![video objectForKey:@"name"]) {
                            _serviceTitleLabel.text = @"";
                        }
                        
                        
                        _demandModel.contentID = [video objectForKey:@"contentID"];
                        
//                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//                        [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                        
                        _isPlayLastVideo = NO;
                        
                        
                    }
                    
                    if ( _videoPlayPosition == _videoListArr.count-1) {
                        _videoPlayPosition = _videoListArr.count-1;
                        NSDictionary *video = [_videoListArr objectAtIndex:_videoPlayPosition];
                        JumpStartTime = [[video objectForKey:@"breakPoint"] integerValue];

                        _demandModel.contentID = [video objectForKey:@"contentID"];
                        
                        _isPlayLastVideo = YES;
                        [AlertLabel AlertInfoWithText:@"已到最后集数" andWith:_contentView withLocationTag:2];
                        
                    }
                    
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//                    [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    
                    _demandModel.TRACKID_VOD = @"_";
                    _demandModel.TRACKNAME_VOD = @"选集";
                    _demandModel.EN_VOD = @"9";
                    
                    
                    if (ISIntall) {
                        
                        _installMaskView.hidden = NO;
                    }else
                    {
                    [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                    }
                    
                    
                    
                }
                
                
                
                
            }

        }
            break;
        case VODPlayMovie:
        {
            if (_videoListArr.count>0) {
                
                NSDictionary *video = [[NSDictionary alloc]initWithDictionary:[_videoListArr objectAtIndex:0]];
                _demandModel.contentID = [video objectForKey:@"contentID"];
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                
                [request VODDetailWithContentID:_demandModel.contentID andTimeout:10];
                [request VODAssccationListWithContentID:_demandModel.contentID andCount:12 andTimeout:kTIME_TEN];
                
                
                _demandModel.TRACKID_VOD = @"";
                _demandModel.TRACKNAME_VOD = @"";
                _demandModel.EN_VOD = @"9";

                //            _isFirstVideo = YES;
                
//                [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                if (ISIntall) {
                    
                    _installMaskView.hidden = NO;
                }else
                {
                [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                }
                _backwardButton.enabled = YES;
                _isPlayLastVideo = NO;
                
            }
 
        }
            break;
        default:
            break;
    }
    
    
    
    
    
    
}




#pragma mark Error Reporting

-(void)displayError:(NSError *)theError
{
    if (theError)
    {
        if (_contentView.hidden == YES) {
            
            [AlertLabel AlertInfoWithText:@"该视频暂时无法播放" andWith:_contentView withLocationTag:2];
            
        }else{
            [AlertLabel AlertInfoWithText:@"该视频暂时无法播放" andWith:_contentView withLocationTag:2];
        }
        
//        [_forwardButton setImage:[UIImage imageNamed:@"btn_lastI_none"] forState:UIControlStateNormal];
//        [_forwardButton setEnabled:NO];
        [_backwardButton setImage:[UIImage imageNamed:@"btn_player_movenext_normal"] forState:UIControlStateNormal];
        [_backwardButton setEnabled:NO];
        
        [self performSelector:@selector(returnBackBtnAction:) withObject:nil afterDelay:2];
    }
}




#pragma mark --
#pragma mark Video Progress

-(void)ProgressValueChanged:(UISlider*)sender
{
    
    JumpStartTime = 0;
    [self.myNewTimer invalidate];
    OnMove = YES;
    
    _videoProgressV.value = sender.value;
    //    OnClick = YES;
    NSInteger JumpTime = _moviePlayer.duration * ([_videoProgressV value]/1000);
    
    NSInteger Jumphour = (JumpTime/3600);
    NSInteger JumpMinute = (JumpTime - (Jumphour*3600)) / 60 ;
    NSInteger JumpSecond = (JumpTime -  Jumphour*3600 - JumpMinute *60);
    
    
    //    NSInteger total_hours = (JumpTime/3600);
    //    NSInteger total_minutes = (JumpTime - (total_hours*3600)) / 60 ;
    //    NSInteger total_seconds =(JumpTime -  total_hours*3600)%60;
    
    
    
    
    NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
    [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
    self.autoHiddenSecend = 5;
    
}






-(void)TouchEndConfirm
{
    [[self.view viewWithTag:4040] setHidden:YES];
    
    //    [self.myNewTimer invalidate];
    
    NSLog(@"zozozozoozozu");
    
    JumpStartTime = _moviePlayer.duration * ([_videoProgressV value]/1000);
    [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
    
    NSInteger Jumphour = (JumpStartTime/3600);
    NSInteger JumpMinute = (JumpStartTime - (Jumphour*3600)) / 60 ;
    NSInteger JumpSecond = (JumpStartTime -  Jumphour*3600 - JumpMinute*60);
    
    
    
    NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
    [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
    
    
    
    if (IsLogin) {
        [self recordPlaybackTime];
    }else{
        //        [self recordLocalPlaybackTime];
    }
    
    
    
    
    if (_moviePlayer.duration == JumpStartTime) {
        // 用户拉到最后
        [AlertLabel AlertInfoWithText:@"节目已播放完毕,加载播放下一节目" andWith:_contentView withLocationTag:2];
        [self ChangeCurTimeForNewPort];
        return;
    }
    
    
    
    
}


-(void)ProgressValueConfirm:(UISlider*)slider
{
    [[self.view viewWithTag:4040] setHidden:YES];
    
    if (_moviePlayer.duration <= 0) {
        
        _videoProgressV.value = 0;
        
    }else
    {
        //    _videoProgressV.value = slider.value;
    OnMove = NO;
        JumpStartTime = _moviePlayer.duration * ([_videoProgressV value]/1000);
        
        NSInteger Jumphour = (JumpStartTime/3600);
        NSInteger JumpMinute = (JumpStartTime - (Jumphour*3600)) / 60 ;
        NSInteger JumpSecond = (JumpStartTime -  Jumphour*3600 - JumpMinute*60);
        
        NSString *TimeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",Jumphour,JumpMinute,JumpSecond];
        [_currentTimeL setText:[NSString stringWithFormat:@"%@", TimeString]];
        
        if (IsLogin) {
            [self recordPlaybackTime];
        }else{
            //        [self recordLocalPlaybackTime];
        }
        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
        if (((NSInteger)_moviePlayer.duration) <= (JumpStartTime+1)) {
            // 用户拉到最后
            [AlertLabel AlertInfoWithText:@"节目播放完毕.加载下一节目" andWith:_contentView withLocationTag:2];
            
            
            [self ChangeCurTimeForNewPort];
            //        [self playTheNextVideo];
            return;
        }
        
        self.autoHiddenSecend = 5;
    }
    
    

    
    
}




//声音增加
- (void)volumeAdd:(CGFloat)step{
    [HTVolumeUtil shareInstance].volumeValue += step;;
}
//亮度增加
- (void)brightnessAdd:(CGFloat)step{
    [UIScreen mainScreen].brightness += step;
    _brightnessProgress.progress = [UIScreen mainScreen].brightness;
}





#pragma mark 记录断点

-(NSInteger)recordPlaybackTime
{
    
    //    NSInteger currentPlaybackTime;
    //
    //    NSInteger movieDuration =  _moviePlayer.duration;
    //    if (movieDuration) {
    //
    //        if (_moviePlayer.currentPlaybackTime <0) {
    //            currentPlaybackTime = 0;
    //        }else
    //        {
    //
    //
    //            currentPlaybackTime = _moviePlayer.currentPlaybackTime;
    //
    //            if (currentPlaybackTime<=movieDuration-10) {
    //                currentPlaybackTime =0;
    //            }
    //
    //
    //
    //        }
    //        if (currentPlaybackTime>= movieDuration) {
    //            currentPlaybackTime = movieDuration;
    //        }
    //    }
    
    if (_isHaveAD == NO) {
        
    
    recordTime = (NSInteger)(_moviePlayer.currentPlaybackTime);
    
    if (recordTime>=0) {
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request VODStopWithContentID:_demandModel.contentID andBreakPoint:recordTime andTimeout:kTIME_TEN];
    }
    
//    JumpStartTime = recordTime;
   
    
    return recordTime;
    }else
    {
        return 0;
    }
}









-(NSInteger)demandSecondsFrom:(NSString*)timeLabelText
{
    NSInteger currentTimeSeconds;
    NSArray *timeARR;
    
    if (timeLabelText.length >0) {
        timeARR = [timeLabelText  componentsSeparatedByString:@":"];
        
        if (timeARR.count>0) {
            currentTimeSeconds = [[timeARR objectAtIndex:0] integerValue] * 3600+[[timeARR objectAtIndex:1] integerValue] * 60 + [[timeARR objectAtIndex:2] integerValue];
        }
    }
    
    return currentTimeSeconds;
    
}








#pragma mark --
#pragma mark  自动旋转
- (BOOL)shouldAutorotate {
    
    
    BOOL canRotation = [[NSUserDefaults standardUserDefaults]boolForKey:@"CAN_ROTATION"];
    
    
    //    return _isTapbtn_play&&!_isScreenLocked&&canRotation&&YES;
    
    
    
    
    
    return  (_isTapPlayBtn&&!_isScreenLocked&&canRotation)?YES:NO;
    
    
    //
    //    if (_isScreenLocked == YES) {
    //        return NO;
    //    }
    //
    //    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"CAN_ROTATION"] == NO) {
    //
    //        return NO;
    //    }
    //    return  _isTapPlayBtn?YES:NO;
}
- (NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskAllButUpsideDown;
    
}




//zzsc
-(void)showGestureGuidView{
    _gestureGuidView.hidden = NO;
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstPlayback"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstPlayback"];
        NSLog(@"第一次启动");
        _gestureGuidView = [[UIImageView alloc] init];
        _gestureGuidView.image = [UIImage imageNamed:@"guide_player_other_guesture"];
        _gestureGuidView.hidden = NO;
        _gestureGuidView.userInteractionEnabled = YES;
        _gestureGuidView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_gestureGuidView];
        
        MyGesture *tap = [[MyGesture alloc] initWithTarget:self action:@selector(hiddenGestureGuid)];
        tap.numberOfTapsRequired = 1;
        [_gestureGuidView addGestureRecognizer:tap];
        
        WS(wself);
        [_gestureGuidView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(wself.view);
        }];
        
    }else{
        NSLog(@"不是第一次启动");
        
        
        
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
-(void)hiddenGestureGuid{
    _gestureGuidView.hidden = YES;
    [_gestureGuidView removeFromSuperview];
    
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    
    // 如果当前处于横屏显示状态
    if(UIInterfaceOrientationIsLandscape(interfaceOrientation))
    {
        //        if (isAfterIOS7) {
        //            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        //
        //        }
        _isVerticalDirection = NO;
        NSLog(@"++++____%ld", [[[UIDevice currentDevice] valueForKey:@"orientation"] integerValue]);
        if (_isHaveAD == NO) {
            
            [self showGestureGuidView];
        }
        [self hideNaviBar:YES];
        _isClickedClear = NO;
        [self setBigScreenPlayer];
        [self hiddenPanlwithBool:YES];
        _shareViewVertical.hidden = YES;
//        [[UIApplication sharedApplication]setStatusBarHidden:YES];
        
        
    }else
    {
       
        _isVerticalDirection= YES;
        [self hiddenGestureGuid];
        [self setSmallScreenPlayer];
        [self hideNaviBar:NO];
        
        //        if (isAfterIOS7) {
        //            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        //
        //        }
//        if (!IsIOS6) {
//            
//            [[UIApplication sharedApplication]setStatusBarHidden:NO];
//        }else{
//            
//            [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationUnknown;
//            
//        }
        
    }
    
}

#pragma mark --
#pragma mark  setSmallScreenPlayer
-(void)setSmallScreenPlayer
{
    [[self.view viewWithTag:8080]setHidden:YES];
    _adPlayerView.backBtn.tag = 0;
    _ADViewPauseBase.hidden = YES;
    _ADViewPauseBase.adplayerMenuType = -1;
    _ADViewBase.hidden = YES;
    
    if (_isHaveAD == YES) {
        _returnMainBtn.alpha = 0;
        _adPlayerView.rotateBtn.hidden = NO;
        [_topBarIV setHidden:YES];
        [_bottomBarIV setHidden:YES];
        _returnBtnIV.hidden = YES;
        
    }else{
        _returnMainBtn.alpha = 1;
        _adPlayerView.rotateBtn.hidden = YES;
        [_topBarIV setHidden:NO];
        [_bottomBarIV setHidden:NO];
        _returnBtnIV.hidden = NO;
    }
    [_adPlayerView.rotateBtn setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal"] forState:UIControlStateNormal];
    _adPlayerView.rotateBtn.tag = 0;
    _clearButton.hidden = YES;
    //遥控器视图进入小屏要进行隐藏****************
    _screenLockedIV.hidden = YES;
    _screenLockedIV.alpha = 0;
    _shareView.hidden = YES;
//    _topBarIV.alpha = 1;
    remote.hidden = YES;
//    _timeButton.hidden = YES;
    _shareButton.hidden = YES;
//    _shareRightLabel.hidden = YES;
    _favourButton.hidden = YES;
    _favourLabel.hidden = YES;
    _interactButton.hidden = YES;
//    _moreButton.hidden = YES;
    
    _selectClearIV.hidden = YES;
//    _returnBtnIV.hidden = NO;
    _returnBtn.hidden = YES;
    _returnMainBtn.hidden = NO;
    _returnMainBtn.tag = RETURNSMALL;
    [_returnMainBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _serviceTitleLabel.hidden = NO;
//    _forwardButton.hidden = YES;
    _backwardButton.hidden = YES;
    _TVButton.hidden = YES;
    _MTButton.hidden = YES;
    _RemoteButton.hidden = YES;
//    _topBarIV.hidden = NO;
    _videoListButton.hidden = YES;
    
//    _slantlineL.hidden = NO;
//    _IPTableView.hidden = YES;
    _IPBackIV.hidden = YES;
    _videoListView.hidden = YES;
    _interactView.hidden = YES;
    _moreBackView.hidden = YES;
    
    
    
    
    //旋转小屏视图全屏按钮的图片赋值****************
    fullWindowbutton.hidden = NO;
    [fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_normal.png"] forState:UIControlStateNormal];
    [fullWindowbutton setImage:[UIImage imageNamed:@"btn_player_switchover_max_pressed"] forState:UIControlStateHighlighted];
    fullWindowbutton.backgroundColor = [UIColor clearColor];
    fullWindowbutton.tag = 0;
    [fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    //背景底图
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    WS(wself);
    
    //背景视图尺寸****************

    [_backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(0*kRateSize);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        make.height.mas_equalTo(194*kRateSize);
        make.width.mas_equalTo(wself.view.mas_width);
    }];
    
    
    //播放器界面图视图尺寸****************
    
    
    [_moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.backgroundView.mas_top);
        make.left.mas_equalTo(wself.backgroundView.mas_left);
        make.height.mas_equalTo(wself.backgroundView.mas_height);
        make.width.mas_equalTo(wself.backgroundView.mas_width);
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));

    }];
    
    
    //内容图，放topBar bottomBar视图尺寸****************
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wself.moviePlayer.view.mas_top);
        make.left.equalTo(wself.moviePlayer.view.mas_left);
        make.height.equalTo(wself.moviePlayer.view.mas_height);
        make.width.equalTo(wself.moviePlayer.view.mas_width);
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //tapview界面图视图尺寸****************
    
    [_tapView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
        
    }];
    
    
    
    
    
    
    //旋转小屏界面topBar视图尺寸(隐藏)****************
    
    [_topBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(0);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize);
        make.width.mas_equalTo(wself.contentView);
        
        
        
    }];
    
    //小屏幕隐藏的控件
    
    
    //    [_returnMainBtnIV mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(wself.contentView.mas_top).offset(0);
    //        make.left.mas_equalTo(wself.contentView.mas_left).offset(0);
    //        make.width.mas_equalTo(45*kRateSize);
    //        make.height.mas_equalTo(34*kRateSize);
    //
    //    }];
    
    
   
    
    //旋转小屏界面时间按钮、分享按钮、节目单按钮视图尺寸(隐藏)****************
    
    //    [_timeButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.right.mas_equalTo(self.topBarIV.mas_right).offset(0);
    //        make.top.mas_equalTo(self.topBarIV.mas_top);
    //        make.size.mas_equalTo(CGSizeMake(0, 0));
    //
    //    }];
    
    
    
    
    //    [_videoListButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.right.mas_equalTo(_timeButton.mas_left);
    //        make.top.mas_equalTo(self.topBarIV.mas_top);
    //        make.size.mas_equalTo(CGSizeMake(0, 0));
    //
    //    }];
    //    _timeButton.hidden = YES;
    
    
    
    
    //    [_shareButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.right.mas_equalTo(_videoListButton.mas_left).offset(0);
    //        make.top.mas_equalTo(self.topBarIV.mas_top);
    //        make.size.mas_equalTo(CGSizeMake(0, 0));
    //
    //    }];
    //
    
    
    
    
    
    //
    //    [_favourButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.right.mas_equalTo(_interactButton.mas_left).offset(-10);
    //        make.top.mas_equalTo(self.topBarIV.mas_top);
    //        make.size.mas_equalTo(CGSizeMake(0*kRateSize
    //, 0*kRateSize
    //));
    //
    //    }];
    
    
    
    
    
    //    [_interactButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.right.mas_equalTo(_shareButton.mas_left).offset(0);
    //        make.top.mas_equalTo(self.topBarIV.mas_top);
    //        make.size.mas_equalTo(CGSizeMake(0, 0));
    //
    //    }];
    
    
    
    
    
    
    
    //***********************end***************
    
    
    
    //旋转小屏界面底部Bottom视图尺寸****************
    
    
    [_bottomBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.contentView);
        make.height.mas_equalTo(60*kRateSize);
        make.width.mas_equalTo(wself.contentView);
    }];
    
    
    
    //********************************end************
    
    
    
    
    //旋转小屏界面播放按钮视图尺寸(隐藏)****************
    
    [_playButton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-3*kRateSize);
//        make.centerY.mas_equalTo(wself.bottomBarIV.centerY);
        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(0);
        make.width.mas_equalTo(35*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
    }];
    
    [_playPauseBigBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];
    
    
    //旋转小屏界面上一曲按钮视图尺寸(隐藏)****************
    
    //    [_forwardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.centerY.mas_equalTo(self.bottomBarIV.mas_centerY);
    //        make.right.mas_equalTo(0);
    //        make.width.mas_equalTo(0);
    //        make.height.mas_equalTo(0);
    //
    //    }];
    //旋转小屏界面下一曲按钮视图尺寸(隐藏)****************
    
    
    
    
    //    [_backwardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.centerY.mas_equalTo(self.bottomBarIV.mas_centerY);
    //        make.right.mas_equalTo(0);
    //        make.width.mas_equalTo(0);
    //        make.height.mas_equalTo(0);
    //
    //    }];
    
    
    //旋转小屏界面底部bottom左侧暂停播放视图尺寸(不隐藏)****************
    [smallbtn_stop mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.bottomBarIV.mas_top).offset(0);
        make.left.mas_equalTo(wself.bottomBarIV).offset(0);
        make.width.mas_equalTo(20*kRateSize);
        make.height.mas_equalTo(28*kRateSize);
        
    }];
    
    //旋转小屏界面全屏按钮视图尺寸****************
    
    [fullWindowbutton mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(0);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-4*kRateSize);

//        make.top.mas_equalTo(wself.bottomBarIV).offset(5);
        make.width.mas_equalTo(35*kRateSize);
        make.height.mas_equalTo(35*kRateSize);
        
    }];
    
    
    //旋转小屏界面清晰度按钮视图尺寸(隐藏)****************
    
    //    [_clearButton mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(w_videoProgressV.mas_bottom);
    //        make.right.mas_equalTo(fullWindowbutton).offset(-30);
    //        make.width.mas_equalTo(0);
    //        make.height.mas_equalTo(0);
    //
    //    }];
    
    
    
    
    
    //    [_selectClearIV mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.bottom.mas_equalTo(_clearButton.mas_top);
    //        make.width.mas_equalTo(0);
    //        make.height.mas_equalTo(10);
    //        make.right.mas_equalTo(fullWindowbutton).offset(-30);
    //
    //
    //    }];
    
    
    
    
    
    
    //旋转小屏界面返回按钮视图尺寸(隐藏)****************
    
    [_returnBtnIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(5*kRateSize);
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(-5*kRateSize);
//        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(44*kRateSize);
        make.top.mas_equalTo(self.topBarIV.mas_top).offset(20*kDeviceRate);
        make.left.mas_equalTo(self.topBarIV.mas_left).offset(12*kDeviceRate);
        make.width.mas_equalTo(28*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
    }];

    
//    [_returnBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(-10);
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(0);
////        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
//        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(55*kRateSize);
//    }];
    //**************end**************
    
    [_serviceTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(35*kRateSize);
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(14*kRateSize);
//        make.width.mas_equalTo(120*kRateSize);
//        make.height.mas_equalTo(25*kRateSize);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(40*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kDeviceRate);
        make.width.mas_equalTo(120*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        
    }];
    
    [_returnMainBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top).offset(0*kRateSize);
        make.left.mas_equalTo(wself.contentView.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(50*kRateSize);
    }];
    
    //旋转小屏界面亮度视图尺寸****************
    
    [_brightnessView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize, 125*kRateSize));
        
        
    }];
    
    [_brightnessProgress mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize, 10*kRateSize));
    }];
    
    
    
    
    //    [_progressTimeView mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.center.mas_equalTo(wself.contentView);
    //        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 60*kRateSize));
    //
    //    }];
    //
    
    
    
    
    
    
    
    
    
    
    
    [_currentTimeL mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-11*kRateSize);
        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(25*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    
//    [_slantlineL mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(10*kRateSize
//                             ).offset(10*kRateSize);
//        make.right.mas_equalTo(_totalTimeL.mas_left).offset(-1.5*kRateSize);
//        make.size.mas_equalTo(CGSizeMake(6*kRateSize, 20*kRateSize));
//    }];
    
    
    
    //总时间Label视图尺寸****************
    
    [_totalTimeL mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.centerY.mas_equalTo(wself.bottomBarIV).offset(12);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-11*kRateSize);
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-25*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
    }];
    
    
    
    
    
    
    
    
    
    //旋转小屏界面进度条视图尺寸****************
    
    [_videoProgressV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-18*kRateSize);
        make.left.mas_equalTo(_currentTimeL.mas_right).offset(10*kRateSize);
        make.right.mas_equalTo(_totalTimeL.mas_left).offset(-10*kRateSize);//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.height.mas_equalTo(6*kRateSize);
//        make.height.mas_equalTo(6*kRateSize);
//        make.width.mas_equalTo(169*kRateSize);
    }];
    //旋转小屏界面声音按钮视图尺寸****************
    
    //    [_voiceView mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
    //        make.size.mas_equalTo(CGSizeMake(0, 0));
    //    }];
    
    
    
    //小屏详情界面
    
    [downView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.backgroundView.mas_bottom);
        //        make.bottom.mas_equalTo(wself.view.mas_bottom);
        make.centerX.mas_equalTo(wself.view.mas_centerX);
        //        make.left.mas_equalTo(wself.view).offset(10);
        //        make.right.mas_equalTo(wself.view).offset(-10);
        //        make.bottom.mas_equalTo(@400);
        //        make.top.mas_equalTo(wself.backgroundView.mas_bottom);
        //        make.centerY.mas_equalTo(wself.backgroundView.mas_bottom);
        make.width.mas_equalTo(wself.view.mas_width);
//        make.height.mas_equalTo(wself.view.mas_height).multipliedBy(0.97);
                make.bottom.equalTo(wself.view.mas_bottom).offset(-kTopSafeSpace);
        
        
        
    }];
    
    downView.hidden = NO;
}


#pragma mark --
#pragma mark  setBigScreenPlayer


//多码率方法

-(void)allCodeClearButtonTitle
{
    if (clearTitles.count >0) {
        
        _clearButton.hidden = _isVerticalDirection?YES:NO;
        
        
        for (int i = 0; i<clearTitles.count; i++) {
            
            if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"]== [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] integerValue]) {
                
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SUPER_CLEAR) {
                    PlayTVType = 1;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == HIGH_DEFINE) {
                    PlayTVType = 2;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == ORIGINAL_PAINT) {
                    PlayTVType = 3;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"] == SMOOTH) {
                    PlayTVType = 4;
                    [_clearButton setTitle:[[clearTitles objectAtIndex:i] valueForKey:@"type"] forState:UIControlStateNormal];
                }
                
                [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                break;
                
            }
            else
            {
                
                if (i==clearTitles.count-1) {
                    [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];
                    [[NSUserDefaults standardUserDefaults] setInteger:SMOOTH forKey:@"BtnTag"];
                    PlayTVType = 4;
                    [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
                
                continue;
            }
        }
    }else
    {
        _clearButton.hidden = YES;
    }
}




-(void)setBigScreenPlayer
{
    //旋转大屏界面宽高尺寸以及等比例缩放尺寸****************
//    boNavi.hidden = YES;
    _shareView.hidden = YES;
    _adPlayerView.backBtn.tag = 1;
//    _screenLockedIV.hidden = NO;
//    _screenLockedIV.alpha = 1;
    
    //判定是否是遥控
    if (isremote== YES) {
        remote.hidden = YES;
    }else{
        remote.hidden = YES;
    }
    
    if (_isHaveAD == NO) {
        if (_ADViewPauseBase.adplayerMenuType == ADPLAYER_MENUPause || _ADViewBase.adplayerMenuType == ADPLAYER_MENUVOD) {
            [_topBarIV setHidden:YES];
            [_bottomBarIV setHidden:YES];
            _screenLockedIV.hidden = YES;
            _screenLockedIV.alpha = 0;
        }else
        {
            [_topBarIV setHidden:NO];
            [_bottomBarIV setHidden:NO];
            _screenLockedIV.hidden = NO;
            _screenLockedIV.alpha = 1;
        }
        _returnBtnIV.hidden = NO;

//        _adPlayerView.rotateBtn.hidden = YES;
//        [_topBarIV setHidden:NO];
//        [_bottomBarIV setHidden:NO];
    }else
    {
        [_topBarIV setHidden:YES];
        [_bottomBarIV setHidden:YES];
        _screenLockedIV.hidden = YES;
        _screenLockedIV.alpha = 0;
        _adPlayerView.rotateBtn.hidden = NO;
        _returnBtnIV.hidden = YES;

        
    }
    [_adPlayerView.rotateBtn setImage:nil forState:UIControlStateNormal];
    //        [_adPlayerView.rotateBtn setImage:[UIImage imageNamed:@"toSmallScreen_click"] forState:UIControlStateHighlighted];
    
    
    _adPlayerView.rotateBtn.tag = 1;

//    _timeButton.hidden = NO;
//    _moreButton.hidden = NO;
    _shareButton.hidden = NO;
//    _shareRightLabel.hidden = NO;
    _favourButton.hidden = NO;
    _favourLabel.hidden = NO;
    //    _interactButton.hidden = NO;
    _interactButton.hidden = IS_SUPPORT_MULTI_SCREEN_INERACT?NO:YES;
    
    _videoListButton.hidden = NO;
//    _slantlineL.hidden = NO;
    _videoListView.hidden = YES;
    
    //    _clearButton.hidden = NO;
    _selectClearIV.hidden = YES;
//    _returnBtnIV.hidden = NO;
    _returnBtn.hidden = NO;
    _returnMainBtn.hidden = YES;
    _returnBtn.tag = RETURNBIG;
    [_returnBtn addTarget:self action:@selector(returnBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _serviceTitleLabel.hidden = NO;
//    _forwardButton.hidden = NO;
    _backwardButton.hidden = NO;
    //    _TVButton.hidden = NO;
    //    _MTButton.hidden = NO;
//    _topBarIV.hidden = NO;
    
    _IPTableView.hidden = YES;
    //    _TVButton.hidden = YES;
    //    _MTButton.hidden = YES;
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    float widht;
    float hight;
    
    if (isAfterIOS8)
    {
        widht = KDeviceHeight;
        hight = kDeviceWidth;
    }else    {
        widht = kDeviceWidth;
        hight = KDeviceHeight;
    }
    
    
    
    WS(wself);
    
    
    
    //旋转大屏界面背景底图视图尺寸******************
    
    [_backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //xiaoxiugai
        make.top.mas_equalTo(wself.view.mas_top).offset(0);
        make.left.mas_equalTo(wself.view.mas_left);
        make.width.mas_equalTo(widht);
        make.height.mas_equalTo(hight);
        make.bottom.mas_equalTo(wself.view.mas_bottom);
    }];
    
    [downView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.backgroundView.mas_bottom);
        make.left.mas_equalTo(wself.view.mas_left);
        make.width.mas_equalTo(wself.view.mas_width);
        make.bottom.equalTo(wself.view.mas_bottom);
    }];
    
    
    
    [_shareView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //xiaoxiugai
        make.top.mas_equalTo(_backgroundView.mas_top).offset(isAfterIOS6A?0: 0);
        make.left.mas_equalTo(_backgroundView.mas_left);
        make.width.mas_equalTo(_backgroundView);
        make.height.mas_equalTo(_backgroundView);
        make.bottom.mas_equalTo(_backgroundView.mas_bottom);
        
        
    }];
    //旋转大屏界面播放器视图尺寸******************
    //播放器界面图
    
    [_moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself.backgroundView);
    }];
    
    
    
    
    [_screenLockedIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.centerY.mas_equalTo(wself.moviePlayer.view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30*kRateSize
                                         , 30*kRateSize
                                         ));
    }];
    
    [lockedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(7.5*kRateSize
                             );
        make.left.mas_equalTo(7.5*kRateSize
                              );
        make.size.mas_equalTo(CGSizeMake(20*kRateSize
                                         , 20*kRateSize
                                         ));
    }];
    
    
    
    
    //旋转大屏界面内容图，放topBar bottomBar视图尺寸******************
    //内容图，放topBar bottomBar
    
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        make.width.mas_equalTo(wself.moviePlayer.view.mas_width);
        //Kael
        //        _contentView.backgroundColor = [UIColor cyanColor];
        //        _contentView.alpha = 0.2;
        
    }];
    
    
    
    
    //cap
    [_ADViewBase mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV.mas_top);
        make.right.mas_equalTo(_videoListView.mas_left).offset(-10*kRateSize);
        //        make.size.mas_equalTo(CGSizeMake(120.5*kRateSize, 81*kRateSize));
        //        make.width.mas_equalTo((kDeviceWidth-264)*kRateSize);
        make.left.mas_equalTo(wself.tapView.mas_left).offset(10*kRateSize);
        //        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
        //         make.height.mas_equalTo(100);
        make.bottom.mas_equalTo(wself.tapView.mas_bottom);
        
    }];
    
    
    
    //cap
    [_ADViewPauseBase mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(wself.contentView.mas_top);
//        make.right.mas_equalTo(_videoListView.mas_left);
//        //        make.size.mas_equalTo(CGSizeMake(120.5*kRateSize, 81*kRateSize));
//        //        make.width.mas_equalTo((kDeviceWidth-264)*kRateSize);
//        make.left.mas_equalTo(wself.contentView.mas_left);
//        //        make.height.mas_equalTo(wself.moviePlayer.view.mas_height);
//        //         make.height.mas_equalTo(100);
//        make.bottom.mas_equalTo(wself.contentView.mas_bottom);
        make.edges.mas_equalTo(wself.contentView);
    }];

    
    
    //旋转大屏界面tapView视图尺寸******************
    
    [_tapView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.contentView.mas_top);
        make.left.mas_equalTo(wself.contentView.mas_left);
        make.height.mas_equalTo(wself.contentView.mas_height);
        make.width.mas_equalTo(wself.contentView.mas_width);
        
    }];
    
    //旋转大屏界面topBar视图尺寸******************
    
    [_topBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(-15*kRateSize);
        make.left.mas_equalTo(wself.moviePlayer.view.mas_left);
        make.height.mas_equalTo(80*kRateSize
                                );
        make.width.mas_equalTo(wself.contentView);
        
        
        
    }];
    //旋转大屏界面时间按钮视图尺寸******************
    
    
    [_shareButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(wself.topBarIV.mas_right).offset(0*kRateSize
                                                               );
//        make.centerY.mas_equalTo(wself.topBarIV);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize
        //);
        make.size.mas_equalTo(CGSizeMake(42*kRateSize
                                         , 45*kRateSize
                                         ));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);
 
    }];
    
    [_favourButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_shareButton.mas_left).offset(0*kRateSize
                                                               );
//        make.centerY.mas_equalTo(wself.topBarIV);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize
        //);
        make.size.mas_equalTo(CGSizeMake(42*kRateSize
                                         , 45*kRateSize
                                         ));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];

    
    
    //旋转大屏界面节目单按钮视图尺寸******************
    
    [_videoListButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_favourButton.mas_left);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize
        //);
//        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize
                                         , 45*kRateSize
                                         ));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(32*kDeviceRate);

    }];
    
    
    //旋转大屏界面分享按钮视图尺寸******************
    
    
    
    
    //收藏按钮********************
    
//    [_favourButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_moreBackView.mas_left).offset(0);
//        make.top.mas_equalTo(_moreBackView.mas_top).offset(0*kRateSize
//                                                           );
//        make.size.mas_equalTo(CGSizeMake(32*kRateSize
//                                         , 35*kRateSize
//                                         ));
//        
//    }];
//    
    
    
//    [_favourLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_favourButton.mas_right).offset(0);
//        make.top.mas_equalTo(_moreBackView.mas_top).offset(0*kRateSize
//                                                           );
//        //        make.size.mas_equalTo(CGSizeMake(42*kRateSize
//        //, 35*kRateSize
//        //));
//        make.height.mas_equalTo(35*kRateSize
//                                );
//        make.right.mas_equalTo(_moreBackView.mas_right);
//        
//    }];
    
    
    
//    [_shareButton mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_moreBackView.mas_left).offset(0);
//        make.top.mas_equalTo(5).offset(0*kRateSize
//                                                              );
//        make.size.mas_equalTo(CGSizeMake(32*kRateSize
//                                         , 35*kRateSize
//                                         ));
//        
//    }];
    
    
//    [_shareRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(_shareButton.mas_right).offset(0);
//        make.top.mas_equalTo(_favourLabel.mas_bottom).offset(0*kRateSize
//                                                             );
//        //        make.size.mas_equalTo(CGSizeMake(42*kRateSize
//        //, 35*kRateSize
//        //));
//        make.height.mas_equalTo(35*kRateSize
//                                );
//        make.right.mas_equalTo(_moreBackView.mas_right);
//        
//    }];
    
    
    //end**************
    
    
    //旋转大屏界面互动按钮视图尺寸******************
    
    [_interactButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_videoListButton.mas_left).offset(0);
        make.centerY.mas_equalTo(wself.topBarIV.mas_centerY);
        //        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(0*kRateSize
        //);
        make.size.mas_equalTo(CGSizeMake(62*kRateSize
                                         , 45*kRateSize
                                         ));
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(25*kDeviceRate);

    }];
    
    
    
    
    
    [_IPBackIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 165*kRateSize));
    }];
    
    
    
    
    [_IPTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.mas_equalTo(_IPBackIV.mas_left);
        make.right.mas_equalTo(_IPBackIV.mas_right);
        make.top.mas_equalTo(_IPBackIV.mas_top);
        make.height.mas_equalTo(135*kRateSize);
        
        
        
    }];
    
    
    [_IPSeperateLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_IPTableView.mas_bottom);
        make.left.mas_equalTo(_IPTableView.mas_left);
        make.width.mas_equalTo(200*kRateSize);
        make.height.mas_equalTo(1*kRateSize);
    }];
    
    [_IPButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_IPSeperateLineView.mas_bottom);
        make.centerX.mas_equalTo(_IPBackIV.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 30*kRateSize));
    }];
    
    
    [_videoListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(10*kRateSize);
        make.width.mas_equalTo(139*kRateSize
                               );
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right);
        
    }];
    
    
    
    //节目单的数据列表
    
    [_videoListTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_videoListView.mas_top);
        make.width.mas_equalTo(139*kRateSize
                               );
        make.bottom.mas_equalTo(_videoListView.mas_bottom);
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right);
    }];
    
    
    
    
    
    //    [remote mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.width.mas_equalTo(wself.view.mas_width);
    //        make.height.mas_equalTo(wself.view.mas_height);
    //        make.top.mas_equalTo(wself.view.mas_top);
    //        make.right.mas_equalTo(wself.view.mas_right);
    //    }];
    
    
    [_RemoteButton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        
        make.bottom.mas_equalTo(_TVButton.mas_top).offset(0);
        
        make.size.mas_equalTo(CGSizeMake(50*kRateSize
                                         , 50*kRateSize
                                         ));
        
    }];
    
    
    
    
    //TVButton
    [_TVButton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        //        make.top.mas_equalTo(wself.moviePlayer.view.mas_top).offset(60);
        make.size.mas_equalTo(CGSizeMake(50*kRateSize, 50*kRateSize));
        make.centerY.mas_equalTo(wself.moviePlayer.view.mas_centerY).offset(-20*kRateSize);
        
        
        
    }];
    
    //_MTButton
    
    [_MTButton mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.right.mas_equalTo(wself.contentView.right).offset(0);
        //        make.top.mas_equalTo(_TVButton.mas_bottom).offset(0);
        //
        //        make.size.mas_equalTo(CGSizeMake(50*kRateSize
        //, 50*kRateSize
        //));
        
        make.right.mas_equalTo(wself.moviePlayer.view.mas_right).offset(0);
        
        make.top.mas_equalTo(_TVButton.mas_bottom).offset(0);
        
        make.size.mas_equalTo(CGSizeMake(50*kRateSize
                                         , 50*kRateSize
                                         ));
        
        
        
    }];
    
    
    
    
    
    
    
    [_interactView mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.top.mas_equalTo(wself.topBarIV.mas_bottom).offset(10*kRateSize
        //);
        //        make.left.mas_equalTo(_interactButton.mas_left).offset(30*kRateSize
        //);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(140*kRateSize
                                         , 120*kRateSize
                                         ));
        
        
    }];
    
    
    
    
    [_moreBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.topBarIV.mas_bottom).offset(0*kRateSize
                                                               );
        make.right.mas_equalTo(wself.topBarIV.mas_right).offset(0*kRateSize
                                                                );
        make.size.mas_equalTo(CGSizeMake(100*kRateSize
                                         , 70*kRateSize
                                         ));
        
        
    }];
    
    
    
    [remoteControlBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_interactView.mas_top);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gestureRemote = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gestureRemote.numberOfTapsRequired = 1;
    gestureRemote.tag = REMOTECONTROL;
    [remoteControlBackV addGestureRecognizer:gestureRemote];
    
    
    
    
    [remoteLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remoteControlBackV.mas_top).offset(0);
        make.left.mas_equalTo(remoteControlBackV.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize
                                         , 40*kRateSize
                                         ));
        
    }];
    
    
    [remoteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(remoteLeftV.mas_right).offset(0);
        make.top.mas_equalTo(remoteControlBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize
                               );
        make.height.mas_equalTo(remoteControlBackV.mas_height);
        
    }];
    
    [remoteDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(remoteControlBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize
                               );
        make.height.mas_equalTo(1*kRateSize
                                );
        make.bottom.mas_equalTo(remoteControlBackV.mas_bottom);
        
    }];
    
    
    
    
    [pushBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(remoteControlBackV.mas_bottom).offset(0);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gesturePush = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gesturePush.numberOfTapsRequired = 1;
    gesturePush.tag = PUSH;
    gesturePush.cancelsTouchesInView = YES;
    [pushBackV addGestureRecognizer:gesturePush];
    
    
    
    
    
    [pushLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pushBackV.mas_top).offset(0);
        make.left.mas_equalTo(pushBackV.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize
                                         , 40*kRateSize
                                         ));
        
    }];
    
    [pushLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pushLeftV.mas_right).offset(0);
        make.top.mas_equalTo(pushBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize
                               );
        make.height.mas_equalTo(pushBackV.mas_height);
        
    }];
    
    [pushDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pushBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize
                               );
        make.height.mas_equalTo(1*kRateSize
                                );
        make.bottom.mas_equalTo(pushBackV.mas_bottom);
        
    }];
    
    
    
    [pullBackV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(pushBackV.mas_bottom).offset(0);
        make.left.mas_equalTo(_interactView.mas_left);
        make.width.mas_equalTo(_interactView.mas_width);
        make.height.mas_equalTo(_interactView.mas_height).dividedBy(3);
        
        
    }];
    
    
    MyGesture *gesturePull = [[MyGesture alloc] initWithTarget:self action:@selector(goToGesture:)];
    gesturePull.numberOfTapsRequired = 1;
    gesturePull.tag = PULL;
    [pullBackV addGestureRecognizer:gesturePull];
    
    [pullLeftV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pullBackV.mas_top).offset(0);
        make.left.mas_equalTo(pullBackV.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(40*kRateSize
                                         , 40*kRateSize
                                         ));
        
    }];
    
    [pullLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pullLeftV.mas_right).offset(0);
        make.top.mas_equalTo(pullBackV.mas_top);
        make.width.mas_equalTo(100*kRateSize
                               );
        make.height.mas_equalTo(pullBackV.mas_height);
        
    }];
    [pullDownLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pullBackV.mas_left).offset(0);
        //        make.top.mas_equalTo(remoteControlBackV.mas_top);
        //        make.center.mas_equalTo(remoteControlBackV.center);
        make.width.mas_equalTo(120*kRateSize
                               );
        make.height.mas_equalTo(1*kRateSize
                                );
        make.bottom.mas_equalTo(pullBackV.mas_bottom);
        
    }];
    
    
    
    
    
    
    
    //旋转大屏界面底部BottomBar视图尺寸******************
    
    [_bottomBarIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //isBeforeIOS6 ? -(30): statusHeight+kyyy
        
        make.bottom.mas_equalTo(wself.contentView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.height.mas_equalTo(60*kRateSize
                                );
        make.width.mas_equalTo(wself.contentView.mas_width);
    }];
    
    //旋转大屏界面进度条视图尺寸******************
    
    [_videoProgressV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-22*kRateSize);
//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.left.mas_equalTo(_currentTimeL.mas_right).offset(5*kRateSize);
        make.right.mas_equalTo(_totalTimeL.mas_left).offset(-5*kRateSize);//       //        make.centerX.mas_equalTo(wself.bottomBarIV).offset(-10);
        make.height.mas_equalTo(6*kRateSize
                                );
////        make.width.mas_equalTo(isAfterIOS6A?350*kRateSize:438*kRateSize
//                               );
        //        isAfterIOS6A?350*kRateSize:438*kRateSize
        //        make.width.mas_equalTo(@375);
    }];
    
    
    
    
    //旋转大屏界面开始时间Label视图尺寸******************
    
    _currentTimeL.textAlignment = NSTextAlignmentRight;
    
    [_currentTimeL mas_updateConstraints:^(MASConstraintMaker *make) {
        
        //        make.centerY.mas_equalTo(wself.bottomBarIV).offset(-15);
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);
        //        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(100*kRateSize
        //                                                                     );
        make.left.mas_equalTo(wself.bottomBarIV.mas_left).offset(75*kRateSize
                                                                 );
        
        make.size.mas_equalTo(CGSizeMake(60*kRateSize
                                         , 20*kRateSize
                                         ));
        
    }];
    
    //旋转大屏界面结束时间Label视图尺寸******************
    
    
    
    //    _slantlineL.text = @"/";
    //    [_slantlineL mas_remakeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(10*kRateSize
    //                             ).offset(0);
    //        make.left.mas_equalTo(_currentTimeL.mas_right).offset(1*kRateSize
    //                                                              );
    //        make.size.mas_equalTo(CGSizeMake(6*kRateSize
    //                                         , 20*kRateSize
    //                                         ));
    //    }];
    
    
    
    
    
    [_totalTimeL mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);
        //make.top.mas_equalTo(wself.bottomBarIV.mas_top).offset(10);
        //        make.right.mas_equalTo(fullWindowbutton.mas_left);
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-50*kRateSize);
        
        make.size.mas_equalTo(CGSizeMake(60*kRateSize
                                         , 20*kRateSize
                                         ));
        //        make.width.mas_equalTo(50);
        //        make.height.mas_equalTo(10);
    }];
    
    
    
    //旋转大屏界面底部bottom左侧小暂停按钮视图尺寸（隐藏）******************
    
    //    [smallbtn_stop mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.top.mas_equalTo(wself.bottomBarIV).offset(10);
    //        make.left.mas_equalTo(wself.bottomBarIV).offset(15);
    //        make.width.mas_equalTo(0);
    //        make.height.mas_equalTo(0);
    //
    //    }];
    
    //旋转大屏界面上一曲按钮视图尺寸******************
    
//    
//    [_forwardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        
//        
//        make.left.mas_equalTo(wself.bottomBarIV).offset(18*kRateSize
//                                                        );
//        make.top.mas_equalTo(_videoProgressV.mas_bottom).offset(5*kRateSize
//                                                                     );
//        make.size.mas_equalTo(CGSizeMake(39*kRateSize
//                                         , 39*kRateSize
//                                         ));
//        
//    }];
    //
    
    //旋转大屏界面播放暂停按钮视图尺寸******************
    
    
    [_playButton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(_bottomBarIV.mas_left
                              );
        make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-5*kRateSize
                                                                     );
        make.width.mas_equalTo(39*kRateSize
                               );
        make.height.mas_equalTo(39*kRateSize
                                );
        
    }];
    //旋转大屏界面下一曲按钮视图尺寸******************
    
    
    
    [_playPauseBigBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.height.mas_equalTo(50*kRateSize);
        make.width.mas_equalTo(50*kRateSize);
        
    }];
    
    
    [_backwardButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-5*kRateSize
                                                                     );
        make.left.mas_equalTo(_playButton.mas_right);
        make.width.mas_equalTo(39*kRateSize
                               );
        make.height.mas_equalTo(39*kRateSize
                                );
        
    }];
    
    //旋转大屏界面全屏按钮视图尺寸******************
    fullWindowbutton.hidden = YES;
    [fullWindowbutton setImage:[UIImage imageNamed:@"toSmallScreen.png"] forState:UIControlStateNormal];
    [fullWindowbutton setImage:[UIImage imageNamed:@"toSmallScreen_click"] forState:UIControlStateHighlighted];
    
    [fullWindowbutton mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(0*kRateSize
                                                                   );
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom);
//        //        make.centerY.mas_equalTo(wself.bottomBarIV);
//        make.top.mas_equalTo(wself.bottomBarIV.mas_top).offset(25*kRateSize
//                                                               );
        make.width.mas_equalTo(22.5*kRateSize
                               );
        make.height.mas_equalTo(23.5*kRateSize
                                );
        
    }];
    fullWindowbutton.tag = 1;
    //全屏按钮的点击时间**********
    [fullWindowbutton addTarget:self action:@selector(jumpToFullWindowToPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //清晰度按钮****************
    
    [_clearButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-15*kRateSize);//zzsc
       make.right.mas_equalTo(wself.bottomBarIV.right).offset(-15*kRateSize);//zzsc
        make.width.mas_equalTo(50*kRateSize);
        make.height.mas_equalTo(20*kRateSize);
        
    }];
    
    
    
    if (clearTitles.count >0) {
        _clearButton.hidden = NO;
    }else
    {
        _clearButton.hidden = YES;
    }
    
    [self allCodeClearButtonTitle];
    
    
    
    
    
    
    [_selectClearIV mas_updateConstraints:^(MASConstraintMaker *make) {
       make.bottom.mas_equalTo(wself.bottomBarIV.mas_bottom).offset(-40*kRateSize);
        make.width.mas_equalTo(50*kRateSize
                               );
        make.height.mas_equalTo(clearTitles.count*30*kRateSize
                                );
        make.right.mas_equalTo(wself.bottomBarIV.mas_right).offset(-15*kRateSize
                                                        );
    }];
    
    
    
    
    
    //旋转大屏界面左上返回按钮视图尺寸******************
    
    //左上返回按钮
    
    
    [_returnBtnIV mas_updateConstraints:^(MASConstraintMaker *make) {
        
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kRateSize);
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(-5*kRateSize);
//        make.width.mas_equalTo(55*kRateSize);
//        make.height.mas_equalTo(44*kRateSize);
        
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(42*kDeviceRate);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(12*kDeviceRate);
        make.width.mas_equalTo(28*kDeviceRate);
        make.height.mas_equalTo(28*kDeviceRate);
        
    }];
    
    
    [_returnBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        //        make.centerY.mas_equalTo(wself.topBarIV);
        
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(20*kRateSize);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(-5*kRateSize);
        make.width.mas_equalTo(55*kRateSize);
        make.height.mas_equalTo(44*kRateSize);
    }];

    
    
    
    
    [_serviceTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
//        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(50*kRateSize);
//        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(29*kRateSize);        make.width.mas_equalTo(220*kRateSize);
//        make.height.mas_equalTo(25*kRateSize);
        make.left.mas_equalTo(wself.topBarIV.mas_left).offset(50*kDeviceRate);
        make.top.mas_equalTo(wself.topBarIV.mas_top).offset(42*kDeviceRate);        make.width.mas_equalTo(220*kDeviceRate);
        make.height.mas_equalTo(25*kDeviceRate);
        
    }];
    
//    [_serviceTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.mas_equalTo(wself.returnBtn.mas_right).offset(-0*kRateSize);
//        make.top.mas_equalTo(wself.topBarIV).offset(30);
////        make.centerY.mas_equalTo(self.topBarIV.mas_centerY);
//        make.width.mas_equalTo(120*kRateSize);
//        make.height.mas_equalTo(25*kRateSize);
//        
//    }];
    
    
    
    
    //旋转大屏界面亮度按钮视图尺寸******************
    
    [_brightnessView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.contentView);
        make.size.mas_equalTo(CGSizeMake(125*kRateSize
                                         , 125*kRateSize
                                         ));
        
        
    }];
    
    [_brightnessProgress mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.brightnessView.mas_bottom);
        make.centerX.mas_equalTo(wself.brightnessView);
        make.size.mas_equalTo(CGSizeMake(80*kRateSize
                                         , 10*kRateSize
                                         ));
    }];
    
    //    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.center.mas_equalTo(wself.contentView);
    //        make.size.mas_equalTo(CGSizeMake(200*kRateSize
    //, 60*kRateSize
    //));
    //
    //    }];
    
    

    
    
    //旋转大屏界面声音按钮视图尺寸******************
    
    //    [_voiceView mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.centerY.mas_equalTo(wself.bottomBarIV.mas_centerY);
    //        make.centerX.mas_equalTo(wself.bottomBarIV.mas_centerX);
    //        make.size.mas_equalTo(CGSizeMake(20*kRateSize
    //, 20*kRateSize
    //));
    //    }];
    
    
    [_progressTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(wself.contentView.mas_centerX);
        make.centerY.mas_equalTo(wself.contentView.mas_centerY);
        make.height.mas_equalTo(70*kRateSize);
        make.width.mas_equalTo(130*kRateSize);
        //        make.center.mas_equalTo(wself.contentView);
        //        make.size.mas_equalTo(CGSizeMake(200*kRateSize, 60*kRateSize));
        //        make.bottom.mas_equalTo(wself.videoProgressV.mas_top).offset(-20*kRateSize);
        //        make.size.mas_equalTo(CGSizeMake(130*kRateSize, 70*kRateSize));
        ////        make.left.mas_equalTo(wself.contentView.mas_left);
        //        make.centerX.mas_equalTo(wself.contentView.centerX);
        //        make.centerY.mas_equalTo(wself.contentView.centerY);
        
    }];
    
    
    
    [_progressDirectionIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_progressTimeView.mas_centerY).offset(-15*kRateSize);
        make.centerX.mas_equalTo(_progressTimeView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(30, 23*kRateSize));
    }];
    
    
    
    
    [_progressTimeLable_top mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.left.mas_equalTo(wself.progressTimeView.mas_left).offset(5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(60*kRateSize, 20*kRateSize));
        
    }];
    
    
    [_progressTimeLable_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(wself.progressTimeView.mas_bottom).offset(-13*kRateSize);
        make.right.mas_equalTo(wself.progressTimeView.mas_right).offset(-5*kRateSize);
        make.size.mas_equalTo(CGSizeMake(70*kRateSize, 20*kRateSize));
        
    }];
    

    
    
    
    
    
    
    
    
}


-(void)clearCodeRateAction:(UIButton*)sender
{
    [[self.view viewWithTag:4040] setHidden:NO];
    [self hiddenPanlwithBool:YES];

    if ((NSInteger)_moviePlayer.currentPlaybackTime >0) {
        JumpStartTime = (NSInteger)(_moviePlayer.currentPlaybackTime);
    }

    
    switch (sender.tag) {
        case SUPER_CLEAR:
        {
            PlayTVType = 1;
            
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:SUPER_CLEAR forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_clearButton setTitle:@"超清" forState:UIControlStateNormal];
            [self clearMoviePlayerStop];
            
            [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:1];
            
            
        }
            break;
        case HIGH_DEFINE:
        {
            PlayTVType = 2;
            
            
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:HIGH_DEFINE forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"高清" forState:UIControlStateNormal];
            [self clearMoviePlayerStop];
            [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:2];
        }
            break;
        case ORIGINAL_PAINT:
        {
            PlayTVType = 3;
            
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:ORIGINAL_PAINT forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"标清" forState:UIControlStateNormal];
            [self clearMoviePlayerStop];
            sender.titleLabel.textColor = [UIColor redColor];
            [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:3];
            
            
        }
            break;
        case SMOOTH:
        {
            PlayTVType = 4;
            
            
            [[NSUserDefaults standardUserDefaults]setInteger:PlayTVType forKey:@"PlayTVType"];
            [[NSUserDefaults standardUserDefaults]setInteger:SMOOTH forKey:@"BtnTag"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_clearButton setTitle:@"流畅" forState:UIControlStateNormal];

            [self clearMoviePlayerStop];
            [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:4];
        }
            break;
            
            
        default:
            break;
    }
//    _topBarIV.hidden = YES;
//    _bottomBarIV.hidden = YES;
    
        [self hiddenPanlwithBool:YES];
}






/**
 *  TableViewDelegate
 *
 *  @param
 */

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _videoListTV) {
        return _videoListArr.count;
    }else if (tableView == _EPGListTableView){
        
        return 30;
    }
    else
    {
        return _IPArray.count;
        
        //        }
        
    }
    
    
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _EPGListTableView) {
        return 45*kRateSize;
    }else if (tableView == _videoListTV) {
        return 37*kRateSize
        ;
    }else
    {
        return 30*kRateSize
        ;
    }
    
    
    
    
}



//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//
//
//    if (tableView == _IPTableView) {
//
//        if (section == 0)
//
//        {
//
//
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
//            [button setTitle:@"连接设备列表" forState:UIControlStateNormal];
//
//            [button setTitleColor:[UIColor grayColor]forState:UIControlStateNormal];
//
//            return button;
//
//
//
//        }
//
//
//    }
//
//
//    return nil;
//
//
//}
//

//
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//
//{
//
//    if (tableView == _IPTableView)
//
//    {
//
//
//
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [button setBackgroundColor:[UIColor blueColor]];
//        [button setTitle:@"取消" forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//
//        return button;
//
//    }
//
//
//
//
//    return nil;
//
//}
//
//-(void)cancelButtonClicked
//{
//    _IPTableView.hidden = YES;
//}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self reStartHiddenTimer];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _videoListTV) {
        
        DemandVideoListCell *cell=[tableView dequeueReusableCellWithIdentifier:kMASCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[DemandVideoListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMASCellReuseIdentifier];
            
        }
        if (indexPath.row == _channelSelectIndex) {
            cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_player_focus_select_cell"]];
            cell.videoTitleLabel.textColor = App_selected_color;
        }else
        {
            cell.backgroundView = [[UIImageView alloc]initWithImage:nil];
            cell.videoTitleLabel.textColor = UIColorFromRGB(0x999999);
        }
        
        cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        if (_videoListArr.count>0) {
            NSDictionary *videoDic = [[NSDictionary alloc]initWithDictionary:[_videoListArr objectAtIndex:indexPath.row]];
            
            
            
            if (_vodPlayType == VODPlayMovie ) {
                cell.videoTitleLabel.text = [videoDic objectForKey:@"name"];
                
            }
            
            
            if (_vodPlayType == VOdPlayVariety) {
                cell.videoTitleLabel.text = [videoDic objectForKey:@"subName"];
                
            }
            
            if (_vodPlayType == VodPlayTVSerial) {
                
                
                NSString *seqM = [videoDic objectForKey:@"seq"];
                
                if ([seqM intValue]<=10) {
                    seqM = [NSString stringWithFormat:@"%02ld",[seqM integerValue]];
                }
                cell.videoTitleLabel.text = [NSString stringWithFormat:@"第%@集",seqM];
            }
            
 
        }
        
        
        
        
        
        
        
        
        return cell;
        
        
    }else
    {
        LinkDeviceTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:kMNSCellReuseIdentifier];
        
        if (cell == nil) {
            cell = [[LinkDeviceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMNSCellReuseIdentifier];
            
        }
        
        cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.IPLabel.text = [NSString stringWithFormat:@"%@",[_IPArray objectAtIndex:indexPath.row]];
        cell.linkBtn.linkStr = [NSString stringWithFormat:@"%@",[_IPArray objectAtIndex:indexPath.row]];
        cell.linkBtn.tag = indexPath.row;
        
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length>2) {
//#warning 没有连接，所以不显示

//            _MTButton.hidden = NO;
//            _TVButton.hidden = NO;
//            _RemoteButton.hidden = NO;
        }
        if ([cell.IPLabel.text isEqualToString:remberIP] ) {
            cell.linkBtn.selected = YES;
            cell.IPLabel.textColor = App_selected_color;
            [cell.linkBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
            
        }else{
            cell.linkBtn.selected = NO;
            cell.IPLabel.textColor = App_background_color;

            [cell.linkBtn setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_connect@2x"] forState:UIControlStateNormal];
            
        }
        
        
        [cell.linkBtn addTarget:self action:@selector(linkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        return cell;
    }
    
    
    
    
    
}


-(void)linkBtnClick:(LinkButton*)sender
{
    
    [self reStartHiddenTimer];
    [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
    if (sender.selected) {
        
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"pointToPointIP"];
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_connect@2x"] forState:UIControlStateNormal];
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length==0) {
            _MTButton.hidden = YES;
            _TVButton.hidden = YES;
            _RemoteButton.hidden = YES;
            [_MTButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_normal"] forState:UIControlStateNormal];
            [_TVButton setImage:[UIImage imageNamed:@"btn_player_push_screen_normal"] forState:UIControlStateNormal];
            [_RemoteButton setImage:[UIImage imageNamed:@"btn_player_remote_normal"] forState:UIControlStateNormal];
            
        }
        sender.selected = NO;
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"islink"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        //这个地方 可能会发通知 让手机连接
    }else
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:sender.linkStr forKey:@"pointToPointIP"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"islink"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_player_interaction_disconnect@2x"] forState:UIControlStateNormal];
        sender.selected = YES;
        NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (remberIP.length>2) {
            _MTButton.hidden = NO;
            _TVButton.hidden = NO;
            _RemoteButton.hidden = NO;
        }
        
        
    }
    
    
    [_IPTableView reloadData];
    
    
}







- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"点击了%ld",(long)indexPath.row);
    
    
    
    [self clearMoviePlayerStop];
    
    
    
    if (USER_ACTION_COLLECTION == YES) {
        //        _watchDuration = [self timeLengthForCurrent];
        //
        //        if ([self.watchDuration intValue] >=5) {
        //
        ////            [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:self.nowDateString andVodDuration:self.watchDuration];
        //            [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
        //        }
        if (_isCollectionInfo) {
            _demandModel.contentID = _demandModel.contentID;
            _demandModel.vodFenleiType = [self collectVodInformationType];
            _demandModel.name = _demandModel.name;
            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
            _demandModel.vodBID = _demandModel.vodBID;
            _demandModel.VodBName = _demandModel.VodBName;
            _demandModel.category = _demandModel.category;
            _demandModel.rootCategory = _demandModel.rootCategory;
            _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
            _demandModel.EN_VOD = _demandModel.EN_VOD;
            
            if ([_watchDuration intValue] >= 5) {
                
                [SAIInformationManager VodInformation:_demandModel];
            }
            [_timeManager reset];
        }
    }

    //本地播放记录
    _localVideoModel.lvNAME = _demandModel.NAME;
    _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
    _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
    _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
    _localVideoModel.lvImageLink = _demandModel.imageLink;
    _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
    
//    [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localVideoModel resetLocalVideoModel];
    
    
    
    
    
    
    
    if (tableView == _videoListTV) {
        
        NSDictionary *videoDic = [[NSDictionary alloc]init];
        videoDic =  [_videoListArr objectAtIndex:indexPath.row];
        _videoPlayPosition = indexPath.row;
        if (_videoPlayPosition == 0) {
//            _forwardButton.enabled = NO;
        }else
        {
//            _forwardButton.enabled = YES;
        }
        
        if (_vodPlayType != VODPlayMovie) {
            
            _videoListArr  = _demandModel.seriesList;
            
            if (![[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"name"]) {
                _serviceTitleLabel.text = @"";
            }

            if (_vodPlayType == VodPlayTVSerial) {
                

                self.demandModel.name = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"name"]];
                
                [_DBackScrollView episodeViewSetSelectedBtnAtIndex:_videoPlayPosition+1];

                
                
                
            }
            
            if (_vodPlayType == VOdPlayVariety) {
                
                self.demandModel.name = [[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"subName"];
                [_DBackScrollView varityViewSetselectedAtIndex:_videoPlayPosition+1];
            }
            
            
            if (self.demandModel.name.length<=0) {
                _serviceTitleLabel.text = @"";
            }else
            {
            _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",self.demandModel.name];
            
            }
            _demandModel.contentID = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"contentID"]];
            if (IS_PERMISSION_GROUP) {
                NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];

                [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            }
            _channelSelectIndex = _videoPlayPosition;
            
            _demandModel.TRACKID_VOD = @"_";
            _demandModel.TRACKNAME_VOD = @"选集";
            _demandModel.EN_VOD = @"9";

            NSString *ownStr = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"isOwn"]];;
            
            if ([ownStr isEqualToString:@"true"]) {
                self.demandModel.isOwn = YES;

                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

//                [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                if (ISIntall) {
                    
                    _installMaskView.hidden = NO;
                }else
                {
                [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                }
            }else
            {
                self.demandModel.isOwn = NO;
                
                [self clearMoviePlayerStop];

                if (IsLogin) {
                    _orderView.hidden = NO;
                }else
                {
                    _playerLoginView.hidden = NO;
                    _jxPlayerLoginView.hidden = NO;
                    _isEPGPlayerEnter = YES;
                    _jxPlayerLoginView.hidden = NO;
                }
                
                
              
                
            }
            
            
            
        }
        
        else
        {
            _demandModel.contentID = [videoDic objectForKey:@"contentID"];
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            _demandModel.TRACKID_VOD = _demandModel.contentID;
            _demandModel.TRACKNAME_VOD = _demandModel.name;
            _demandModel.EN_VOD = @"2";
            [request VODDetailWithContentID:_demandModel.contentID andTimeout:10];
            [request VODAssccationListWithContentID:_demandModel.contentID andCount:12 andTimeout:kTIME_TEN];
//            [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:@"1" andTimeout:kTIME_TEN];
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
            [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
            }
            
           
            
        }
        
        
        
        if (IsLogin) {
            [self recordPlaybackTime];
        }else{
            //                    [self recordLocalPlaybackTime];
        }
        
       
        [_videoListTV reloadData];
        
        
        
    }else
    {
        
        NSLog(@"点击的IP");
    }
    
    
    _videoListTV.hidden = YES;
    
}



/**
 *  点击的mygesture事件
 *
 *  @param
 */

- (void)goToGesture:(MyGesture *)gesture
{

    switch (gesture.tag) {
        case REMOTECONTROL:
        {
            NSLog(@"remoteControl");
            
//            for (int i = 0; i<3; i++) {
//                
//                [_IPArray addObject:[NSString stringWithFormat:@"192.168.1.%d",arc4random()%123]];
//            }
            _interactView.hidden = !_interactView.hidden;
            [self reStartHiddenTimer];
            
            //
            [_IPTableView reloadData];
            
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
            
            if (pointToPointIP.length <7) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [self showSearchDeviceLoadingView];
                
                break;
            }else{
                
                [self reloadIPTableView];
                
                break;
            }
            
            isremote = YES;
            
            
            
        }
            break;
        case PUSH:
        {
            NSLog(@"push");
            _interactView.hidden = !_interactView.hidden;
            
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length <7 ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                
                //                _IPTableView.hidden = NO;
                
                break;
            }else{
                [AlertLabel AlertInfoWithText:@"推屏啦~" andWith:self.view withLocationTag:0];
                
                NSInteger point = [self recordPlaybackTime];
                NSMutableDictionary *paramDic ;
                
                NSString *contentID;
                NSString *name;
                NSDictionary *resultDic;
                
                if ([_demandModel.contentPullID isEqualToString:@""]) {
                    
                    contentID = self.demandModel.contentID;
                    if ([self.demandModel.seriesList count]>0) {
                        contentID = [[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"contentID"];
                    }
                    
                    name = self.demandModel.name;
                    
                    
                    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)point],@"seekPos",name,@"name", nil];
                    resultDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"DLNAPLAY_VOD",@"@command",paramDic,@"param", nil];
                    
                    
                }else
                {
                    contentID = self.demandModel.contentPullID;
                    
                    
                    name = [NSString stringWithFormat:@"%@",_serviceTitleLabel.text];
                    
                    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)point],@"seekPos",name,@"name", nil];
                    resultDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"DLNAPLAY_VOD",@"@command",paramDic,@"param", nil];
                }
                
                
                
                
                if (self.demandModel.fullstartTimeP.length >0||self.demandModel.endTimeP.length >0) {
                    
                    paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",_serviceTitleLabel.text],@"channelName",self.demandModel.fullstartTimeP,@"startDate",self.demandModel.endTimeP,@"endDate",point,@"seekPos", nil];
                    
                    resultDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_EVENT",@"@command",paramDic,@"param", nil];
                    
                }
                
                if (self.demandModel.liveTitleP.length >0) {
                    
                    paramDic = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",self.demandModel.liveTitleP] forKey:@"channelName"];
                    
                    resultDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_LIVE",@"@command",paramDic,@"param", nil];
                    
                }
                
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:resultDic];
                
            }
            
            
            
            
            
        }
            break;
        case PULL:
        {
            NSLog(@"pull");
            _interactView.hidden = !_interactView.hidden;
            
            NSDictionary *daramDic;
            daramDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_REQUEST",@"@command",nil];
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length <7 ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                //                _IPTableView.hidden = NO;
                
                break;
            }else{
                [AlertLabel AlertInfoWithText:@"拉屏啦~" andWith:self.view withLocationTag:0];
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:daramDic];
                
            }
            
            
            
        }
            break;
            
        case SHAREVIEWTAG:
        {
            _shareView.hidden = YES;
            
        }
            break;
            
            
            
        default:
            break;
    }
}



-(void)remoteControlViewSelectedWith:(NSString *)selectedBtn
{
    if ([selectedBtn isEqualToString:@"hiden"]) {
        
        remote.hidden = YES;
        isremote = NO;
        
    }
    
    
    
}




/**
 *  点击的tap事件
 *
 *  @param
 */

- (void)singleTapAction:(UITapGestureRecognizer *)sender
{
    
    if (![_topBarIV isHidden]) {
        [self hiddenPanlwithBool:YES];
        
    } else {
        [self hiddenPanlwithBool:NO];
    }
    
   

    
}



-(void)reStartHiddenTimer
{
    self.autoHiddenSecend = 5;
    if (self.autoHiddenTimer) {
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    }
    self.autoHiddenTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(autoHiddenPanel:) userInfo:nil repeats:YES];
}




- (void) hiddenPanlwithBool:(BOOL) bol
{
    [[self.view viewWithTag:8080]setHidden:YES];

    if (bol) {
        [UIView animateWithDuration:0.1 animations:^{
            if (_isHaveAD == YES) {
                return ;
            }else
            {
            [_topBarIV setHidden:YES];
            [_bottomBarIV setHidden:YES];
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _moreBackView.hidden = YES;
            _bottomBarIV.userInteractionEnabled = YES;
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _MTButton.alpha = 0;
            _TVButton.alpha = 0;
            _RemoteButton.alpha = 0;
            _ADViewBase.hidden = YES;
            //            //zzs
            //            _BigView.hidden = YES;
            //            _backHistoryView.hidden = YES;
            //            _bottomBarIV.userInteractionEnabled = YES;
            //
            if (!_isScreenLocked) {
                _screenLockedIV.hidden = YES;
                _screenLockedIV.alpha = 0;
            }
            //
            //            [self.smallBottomBarIV setHidden:YES];
            //            [_leftIV setHidden:YES];
            //            [_videoNameL setHidden:YES];
            //            //403 用
            //
//                        if (_contentView.hidden != YES) {
                
                
            
            if (_isVerticalDirection == YES) {
                
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
                _returnBtnIV.hidden = NO;
                _returnMainBtn.hidden = NO;

            }else
            {
                            [[UIApplication sharedApplication] setStatusBarHidden:YES];
                _returnBtnIV.hidden = YES;
                _returnBtn.hidden = NO;
            }
//                        }
            
            //            _selectVolumeIV.hidden = YES;
            //            _isShowSelectVolumeIV = NO;
            [_selectClearIV setHidden:YES];
            //            [_videoListTV setHidden:YES];
            //            [_selectShareIV setHidden:YES];
            _isClickedClear = YES;
            if (clearTitles.count >0) {
                _clearButton.hidden = YES;
            }else
            {
                _clearButton.hidden = YES;
            }
                
                
                //            _isClickedVideoList = NO;
                }
        }];
        [self.autoHiddenTimer invalidate];
        self.autoHiddenTimer = nil;
    } else {
        
        if (_isHaveAD == YES) {
            
            return;
        }else
        {
        [_topBarIV setHidden:NO];
            
//            if (_ZhuanWangMaskView.hidden == NO || _playerLoginView.hidden == NO ||_orderView.hidden == NO || _jxPlayerLoginView.hidden == NO) {
//                
//                _playButton.enabled = NO;
//            }else{
//                _playButton.enabled = YES;
//            }
            
           
            if (ISIntall) {
                
                if (_installMaskView.hidden == NO) {
                    [_bottomView setHidden:YES];
                }else
                {
                    [_bottomBarIV setHidden:NO];
                    
                }
  
            }else
            {
                [_bottomBarIV setHidden:NO];
            }
            
            
        _serviceTitleLabel.hidden = NO;
        _isClickedClear = NO;
        
        
        [self allCodeClearButtonTitle];
            if (_isVerticalDirection == YES) {
                _screenLockedIV.hidden = YES;
                _screenLockedIV.alpha = 0;
                _returnBtnIV.hidden = NO;
                _returnMainBtn.hidden = NO;
            }else
            {
            if (!_isScreenLocked) {
                _screenLockedIV.hidden = NO;
                _screenLockedIV.alpha = 1;
                _returnBtnIV.hidden = NO;
                _returnBtn.hidden = NO;

            }
            }
            
         
            
            
            
            
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        _interactView.hidden = YES;
        _videoListView.hidden = YES;
//        _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

        _moreBackView.hidden = YES;
        _MTButton.alpha = 1;
        _TVButton.alpha = 1;
        _RemoteButton.alpha =1;
//        _screenLockedIV.hidden = NO;
//        _screenLockedIV.alpha = 1;
            _ADViewBase.hidden = YES;
                [_selectClearIV setHidden:YES];
        //        [_videoListTV setHidden:YES];
        [self reStartHiddenTimer];
            
            
          
        }
    }
    
   }
#pragma mark --
#pragma mark 自动隐藏

- (void) autoHiddenPanel:(NSTimer *)sender
{
    [self.view setBackgroundColor:App_background_color];
    if (self.autoHiddenSecend == 0) {
        [self hiddenPanlwithBool:YES];
    } else {
        self.autoHiddenSecend --;
    }
}


#pragma mark --
#pragma mark fullWindowbutton
-(void)jumpToFullWindowToPlay:(UIButton*)btn
{
    if (btn.tag == 0) {
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [AlertLabel AlertInfoWithText:@"已切换至全屏模式" andWith:_contentView withLocationTag:1];
            
        });
        
    }else
    {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [AlertLabel AlertInfoWithText:@"已切换至小屏模式" andWith:_contentView withLocationTag:1];
            
        });
        
    }
    
    
    
    
    
}

-(void)returnBackBtnAction:(UIButton*)sender
{
    
    switch (sender.tag) {
        case RETURNBIG:
            
        {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
            [self setSmallScreenPlayer];
        }
            break;
        case RETURNSMALL:
        {
            
            if (USER_ACTION_COLLECTION == YES)
            {
//                _watchDuration = [self timeLengthForCurrent];
//                
//                if ([self.watchDuration intValue] >=5) {
//                    
////                    [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:self.nowDateString andVodDuration:self.watchDuration];
//                    [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//                    
//                }
                
                
                
                if (_errorManager.isAVLoading == YES) {
                    
                    _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
                    [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_demandModel.NAME andOPSCode:PlayerLoadingExitError andOPSST:[SystermTimeControl getNowServiceTimeString]];
                }
                
                
                [_errorManager resetPlayerStatusInfo];
                
                
                //播放记录
                _localVideoModel.lvNAME = _demandModel.NAME;
                _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
                _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
                _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
                _localVideoModel.lvImageLink = _demandModel.imageLink;
                _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
                
//                [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
                [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
                [_localVideoModel resetLocalVideoModel];
                
                if (_isCollectionInfo) {
                    _demandModel.contentID = _demandModel.contentID;
                    _demandModel.vodFenleiType = [self collectVodInformationType];
                    _demandModel.name = _demandModel.name;
                    _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
                    _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]]; // _watchDuration;
                    _demandModel.vodBID = _demandModel.vodBID;
                    _demandModel.VodBName = _demandModel.VodBName;
                    _demandModel.category = _demandModel.category;
                    _demandModel.rootCategory = _demandModel.rootCategory;
                    _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
                    _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
                    _demandModel.EN_VOD = _demandModel.EN_VOD;

                    if ([_watchDuration intValue] >= 5) {
                        
                        [SAIInformationManager VodInformation:_demandModel];
                    }
                   
                    
                    [_timeManager reset];
                }
                
            }
            [self removeMovieControllerNotification];
            
           
            [self.navigationController popViewControllerAnimated:YES];
            
        }
            break;
        default:
            break;
    }
    
    
    
}



-(void)removeMovieControllerNotification
{
    if (IsLogin) {
        [self recordPlaybackTime];
    }else{
        //                [self recordLocalPlaybackTime];
    }
    if (self.getPlayBackRecordListRepitTimer) {
        [self.getPlayBackRecordListRepitTimer invalidate];
        _getPlayBackRecordListRepitTimer = nil;
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CAN_HIDDEN_TOAST" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RECEVEPULLTVINFO" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LOCALORDERNOTI_TO_LIVE" object:nil];
    //  //这里应该把socket取消掉
    //    [_sendSocket close];
    //    [_receiveSocket close];
    [_adPlayerView.CountTimer invalidate];
    _adPlayerView.CountTimer = nil;
    [_moviePlayer stop];
    [_moviePlayer setContentURL:nil];
    [self.myNewTimer invalidate];
    _moviePlayer = nil;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"REFRESHRECORDERTABLECELL" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REFRESHMINEVCTABLECELL" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETPLAYRECORDERSUCCESS" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveTokenSuccessed" object:nil];
    
    [_adPlayerView removeObserver:self forKeyPath:@"hidden"];
    
    [_orderView removeObserver:self forKeyPath:@"hidden"];
    
    [_jxPlayerLoginView removeObserver:self forKeyPath:@"hidden"];
    
    [_playerLoginView removeObserver:self forKeyPath:@"hidden"];
    
    [_ZhuanWangMaskView removeObserver:self forKeyPath:@"hidden"];
    
    [_installMaskView removeObserver:self forKeyPath:@"hidden"];
    
    [_blackInstallView removeObserver:self forKeyPath:@"hidden"];
    

}



#pragma  mark - 采集信息

//采集  观看时长,每回调一次方法，就可计算出时长
-(NSString *)timeLengthForCurrent
{
    
    // 获取时长的方法
    
    NSString *BOBOString;
    
    NSString *nowString = [NSString stringWithFormat:@"%.0f",[[SystermTimeControl GetSystemTimeWith] timeIntervalSinceDate:self.nowDate]];
    if (ZTTimeInterval>0) {
        
        BOBOString = [NSString stringWithFormat:@"%.0d",[nowString intValue] - [[NSString stringWithFormat:@"%.0f",ZTTimeInterval] intValue]];
    }else
    {
        BOBOString = [NSString stringWithFormat:@"%.0d",[nowString intValue]];
    }
    
    self.nowDate = [SystermTimeControl GetSystemTimeWith];
    return BOBOString;
    
}

//over






#pragma mark ---
#pragma mark 按钮的点击事件


- (void)changeTheLockedState:(UIButton *)btn
{//点击屏幕上的小锁按钮
    _isScreenLocked = !_isScreenLocked;
    if (_isScreenLocked) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"screenLocked"];
        
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_lock"] forState:UIControlStateNormal];
        _contentView.userInteractionEnabled = NO;
        _MTButton.alpha =0;
        _TVButton.alpha = 0;
        _RemoteButton.alpha = 0;
        _selectClearIV.hidden = YES;
        _interactView.hidden = YES;
        _moreBackView.hidden = YES;
        _videoListView.hidden = YES;
        _returnBtnIV.hidden = YES;
        
        //        if (_contentView.hidden != YES) {
        //            [[UIApplication sharedApplication] setStatusBarHidden:YES];
        //        }
        
        [_topBarIV setHidden:YES];
        [_bottomBarIV setHidden:YES];
        //        _selectShareIV.hidden = YES;
        [AlertLabel AlertInfoWithText:@"关闭屏幕旋转" andWith:_contentView withLocationTag:1];
        
    }else{
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"screenLocked"];
        
        _contentView.userInteractionEnabled = YES;
        //        self.smallContentView.userInteractionEnabled = YES;
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_player_controller_unlock"] forState:UIControlStateNormal];
        [AlertLabel AlertInfoWithText:@"开启屏幕旋转" andWith:_contentView withLocationTag:1];
        
    }
    
}



-(void)playPauseBigBtnPlay:(UIButton*)sender
{
    _playPauseBigBtn.hidden = YES;
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    //----上面是改变按钮图标
    _isPaused = NO;//更换暂停标记
    _playPauseBigBtn.hidden = YES;
    [_moviePlayer play];
    
    
    
    
}


NSDate *startDate;

NSDate *endDate;
DTTimePeriod *firstPeriod;
-(void)buttonClickAction:(UIButton*)aButton
{
    NSLog(@"点击事件");
    
    switch (aButton.tag) {
        case PLAY:
        {
            _isClickedClear = NO;
            
            
            if (_isPaused) {
                //----改变按钮图标
                [aButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
                //----上面是改变按钮图标
                _isPaused = NO;//更换暂停标记
                _playPauseBigBtn.hidden = YES;
                [_moviePlayer play];
                
            }else
            {
                
                if (self.moviePlayer.duration<=0) {
                    return;
                }else{
                    [aButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                    _isPaused = YES;//改变暂停标记
                    [_moviePlayer pause];//暂停视频
                    _playPauseBigBtn.hidden = NO;
                }
                
                
                if (IsLogin) {
                    [self recordPlaybackTime];
                }
                
                [self hiddenPanlwithBool:YES];
                if (SUPPORT_AD == YES) {
                    
                
                HTRequest *requester = [[HTRequest alloc]initWithDelegate:self];
                [requester ADInfoWithADCode:PANGEA_PHONE_VOD_PAUSE andType:AD_VODTYPE andAssetID:self.demandModel.contentID andAssetName:self.demandModel.NAME andGroup:AD_GroupID andTimeout:KTimeout andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {
                    
                    if (status == 0) {

                        if (_isVerticalDirection == YES) {
                            _ADViewPauseBase.hidden = YES;
                        }else
                        {
                            _ADViewPauseBase.hidden = NO;
                            _ADViewPauseBase.adplayerMenuType = ADPLAYER_MENUPause;
                        }
                        [_ADViewPauseBase setUpWithDiction:result WithIsHidden:NO];

                        
                    }else
                    {
                        _ADViewPauseBase.hidden = YES;
                    }
                    
                }];

                }
                
                
            }
            
        }
            break;
        case FORWARD:
        {
            _isClickedClear = NO;
            
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _moreBackView.hidden = YES;
            if (_isMovieCategory == YES) {
//                _forwardButton.enabled = NO;
            }else
            {
//                _forwardButton.enabled =  YES;
            }
            
            if (_isPlayFirstVideo == NO) {
                [self playTheForwardVideo];
                
            }else
            {
                return;
            }
            
        }
            break;
            
        case BACKWARD:
        {
            _isClickedClear = NO;
            if (_isMovieCategory == YES) {
                _backwardButton.enabled = YES;
                
            }else
            {
                _backwardButton.enabled = YES;
            }
            
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _moreBackView.hidden = YES;
            
            [self playTheNextVideo];
            
            
        }
            break;
            
        case INTERACT:
        {
            //            if (_IPTableView.hidden == NO) {
            //
            //                _interactView.hidden = YES;
            //            }else
            //            {
            [self hiddenPanlwithBool:YES];
            NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            if (remberIP.length<=0) {
                _interactView.hidden=!_interactView.hidden;
            }else
            {
                BOOL isExist = NO;//判断新获取的IP是否已经存在
                for (NSString *ip in _IPArray) {
                    if ([ip isEqualToString:remberIP]) {
                        isExist = YES;
                    }
                }
                
                if (!isExist) {
                    [_IPArray addObject:remberIP];
                    
//                    if (_IPArray.count <=0) {
//                        
//                        //            [AlertLabel AlertInfoWithText:@"未搜索到盒子,稍后请重试" andWith:_contentView withLocationTag:2];
//                        
//                    }
//                    
//                    
//                    //        _IPTableView.hidden = NO;
//                    _IPBackIV.hidden = NO;
                    
                }

                _IPBackIV.hidden = !_IPBackIV.hidden;
                [self reloadIPTableView];
            }
            //            }
            
            _isClickedClear = NO;
            _videoListView.hidden = YES;
//            _IPTableView.hidden = YES;
//            _IPBackIV.hidden = YES;

            _selectClearIV.hidden = YES;
            _moreBackView.hidden = YES;
            
            
            
            
        }
            break;
        case SHARE:
        {
            
            [self reStartHiddenTimer];
            [self hiddenPanlwithBool:YES];
            
            WS(wself);
            NSString *url = [KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:CurrentPlayVod];

            if (url.length <=0) {
                
                [AlertLabel AlertInfoWithText:@"服务器正在打盹，请稍后再试" andWith:self.view withLocationTag:2];
                return;
            }
            
            _shareView.hidden = !_shareView.hidden;

            _shareView.installedWX = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession];
            _shareView.installedQQ = [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ];
            _shareView.returnBlock = ^(NSInteger tag) {
                NSString *tempLink;
                if (!isEmptyStringOrNilOrNull([KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:CurrentPlayVod])) {
                    tempLink = [KaelTool getChannelId:@"" andChannelTitle:@"" withPlayType:CurrentPlayVod];
                }else{
                    tempLink = @"";
                }
                
                NSString* thumbURL = NSStringIMGFormat(wself.demandModel.imageLink);
                
                NSString *contentString = [NSString stringWithFormat:@"我正在看%@，太精彩了！安装#%@#手机客户端，电视点播免费看啦！快快安装一起看吧。",wself.demandModel.name,kAPP_NAME];
                switch (tag) {
                    case 0:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechat currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                wself.shareViewVertical.hidden = YES;
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };

                    }
                        break;
                    case 1:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsWechatZone currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                wself.shareViewVertical.hidden = YES;
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];
                                
                            }
                        };

                    }
                        break;
                    case 2:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQ currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                wself.shareViewVertical.hidden = YES;
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };

                    }
                        break;
                    case 3:
                    {
                        [wself.shareView UMSocialShareWithAppLinkUrl:tempLink shareContent:contentString shareImage:thumbURL shareType:LsQQZone currentController:wself];
                        wself.shareView.shareSuccessBlock = ^(NSInteger isSuccess) {
                            if (isSuccess == 1) {
                                [AlertLabel AlertInfoWithText:@"分享成功~" andWith:wself.view withLocationTag:0];
                                NSLog(@"分享成功！");
                                wself.shareViewVertical.hidden = YES;
                                [wself hideNaviBar:YES];
                                [wself actionPlayForVideo];

                            }
                        };

                    }
                        break;
                        
                    default:
                        break;
                }
   
                
            };
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _selectClearIV.hidden = YES;
            //            [self shareAction];
            
        }
            break;
            
        case VIDEO_LIST:
        {
            
            [self reStartHiddenTimer];
            [[UIApplication sharedApplication]setStatusBarHidden:YES];

//            _ADViewBase.hidden = NO;
            _videoListView.hidden=!_videoListView.hidden;
            _isClickedClear = NO;
            _screenLockedIV.hidden = YES;
            //            NSString *remberIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            //            if (remberIP.length>2) {
            //
            if (_videoListView.hidden == NO) {
                
                
                _MTButton.alpha = 0;
                _TVButton.alpha = 0;
                _RemoteButton.alpha = 0;
            }else
            {
                _MTButton.alpha = 1;
                _TVButton.alpha = 1;
                _RemoteButton.alpha = 1;
            }
            
            
            
            if (_videoListArr.count>0) {
                [[self.view viewWithTag:8080]setHidden:YES];
                
            }else
            {
                [[self.view viewWithTag:8080]setHidden:_isVerticalDirection?YES:NO];
                [[self.view viewWithTag:8080]setHidden:NO];
                
                
            }
            
            
            
            
            _interactView.hidden = YES;
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _selectClearIV.hidden = YES;
            _videoListTV.hidden = NO;
            _moreBackView.hidden = YES;
            _topBarIV.hidden = YES;
            _bottomBarIV.hidden = YES;
            
            if (_vodPlayType !=VODPlayMovie) {
                if (_demandModel.seriesList>0) {
                    _videoListArr = _demandModel.seriesList;

                    [[self.view viewWithTag:8080]setHidden:YES];

                }
                
            }else
            {
                if (_asscciationArr.count>0) {
                    _videoListArr = _asscciationArr;
                    [[self.view viewWithTag:8080]setHidden:YES];


                }
                

            }
            
            [_videoListTV reloadData];

            if (SUPPORT_AD == YES) {
                
            
            HTRequest *requester = [[HTRequest alloc]initWithDelegate:self];
            [requester ADInfoWithADCode:PANGEA_PHONE_VOD_PLAYER_MENU andType:AD_VODTYPE andAssetID:self.demandModel.contentID andAssetName:self.demandModel.NAME andGroup:AD_GroupID andTimeout:KTimeout andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {

                if (status == 0) {
                    
                    _ADViewBase.hidden = NO;
//                    - (void)setUpWithDiction:(NSDictionary *)diction;
                    _ADViewBase.adplayerMenuType = ADPLAYER_MENUVOD;
                    [_ADViewBase setUpWithDiction:result];
                }else
                {
                    _ADViewBase.hidden = YES;
                }
            
            }];
            
        }
            
        }
            break;
        case FAVOUR:
            
        {
            //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
            [_moviePlayerBottomView resetFavorate:NO];
            
            //            aButton.selected = !aButton.selected;
            if (aButton.selected) {
                
                
                
                
                if (IsLogin) {
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

                    [request DELVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
                }else
                {
                    if (isAuthCodeLogin) {
                        _isPaused = YES;

                        [self actionPauseForVideo];
                        [self changeLoginVerticalDirection];
                        NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                        [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                        [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                        
                        [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                        UserLoginViewController *user = [[UserLoginViewController alloc] init];
                        user.loginType = kVideoLogin;
                        [self.view addSubview:user.view];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                        user.backBlock = ^()
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self actionPlayForVideo];
                        };
                        
                        user.tokenBlock = ^(NSString *token)
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;

                            [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            _isEPGPlayerEnter = YES;
                            [self loginAfterEnterVideo];
                            [self actionPlayForVideo];
                        };
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        return;
                    }else
                    {
                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        
                        
                    }

                }
                
                
                
                
                
                
            }else
            {
                //已收藏
                
                
                
                //                [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
                //
                //                aButton.selected = YES;
                //
                if (IsLogin) {
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

                    [request ADDVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
                }else
                {
                    if (isAuthCodeLogin) {
                        _isPaused = YES;
                        [self actionPauseForVideo];
                        [self changeLoginVerticalDirection];
                        NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                        [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                        [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                        
                        [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                        UserLoginViewController *user = [[UserLoginViewController alloc] init];
                        user.loginType = kVideoLogin;
                        [self.view addSubview:user.view];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                        user.backBlock = ^()
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            [self actionPlayForVideo];
                        };
                        
                        user.tokenBlock = ^(NSString *token)
                        {
                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                            _isPaused = NO;
                            [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                            _isEPGPlayerEnter = YES;
                            [self loginAfterEnterVideo];
                            [self actionPlayForVideo];
                        };
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        return;
                    }else
                    {
                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                        
                        
                    }

                }
                
                
                
                
                
                
                
                //                }else
                //                {
                //                    [LocalCollectAndPlayRecorder addLiveFavoriteWithDic:_localFavourListArr andUserType:NO];
                //                }
                //
                
                
                
                
            }
            
            
            
            
        }
            
            break;
            
        case CLEAR:
        {
            [self reStartHiddenTimer];
            
            _interactView.hidden = YES;
            _videoListView.hidden = YES;
            _moreBackView.hidden = YES;
            
            if (_isClickedClear == NO) {
                _selectClearIV.hidden = NO;
                _isClickedClear = YES;
            }else{
                _selectClearIV.hidden = YES;
                
                _isClickedClear = NO;
                
            }
            
            clearTitles = [self getCodeRateCategoryWithLivType:1];
            if (clearTitles.count>0) {
                
                
                
                [_selectClearIV mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(_bottomBarIV.mas_bottom).offset(-40*kRateSize);
                    make.width.mas_equalTo(50*kRateSize);
                    make.height.mas_equalTo(clearTitles.count*30*kRateSize);
                    make.right.mas_equalTo(_bottomBarIV.mas_right).offset(-15*kRateSize
                                                                               );
                }];
                
                
                [_selectClearIV removeAllSubviews];
                for (int i = 0; i<clearTitles.count; i++) {
                    
                    
                    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    [playButton setBackgroundColor:[UIColor clearColor]];
                    [playButton setTitleColor:UIColorFromRGB(0xeeeeee) forState:UIControlStateNormal];
                     playButton.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
                    playButton.tag = [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] intValue];
                    playButton.showsTouchWhenHighlighted = YES;
                    [playButton setTitle:[NSString stringWithFormat:@"%@",[[clearTitles objectAtIndex:i] valueForKey:@"type"]] forState:UIControlStateNormal];
                    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"BtnTag"]== [[[clearTitles objectAtIndex:i] valueForKey:@"tag"] integerValue]) {
                        [playButton setTitleColor:App_white_color forState:UIControlStateNormal];
                        playButton.titleLabel.font = [UIFont boldSystemFontOfSize:11*kRateSize];
                        [playButton setBackgroundImage:[UIImage imageNamed:@"bg_coderate_btn_click"] forState:UIControlStateNormal];
                       
                    }

                    [playButton addTarget:self action:@selector(clearCodeRateAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [_selectClearIV addSubview:playButton];
                    
                    [playButton mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_selectClearIV.mas_top).offset(30*(kRateSize*i));
                        //            make.left.mas_equalTo(@0);
                        make.left.mas_equalTo(_selectClearIV.mas_left);
                        make.right.mas_equalTo(_selectClearIV.mas_right);
                        make.height.mas_equalTo(30*kRateSize);

                        
                    }];
                    
                    
                    
                }
                
                
            }else
            {
                [AlertLabel AlertSSIDInfoWithText:@"暂无多码率" andWith:self.view withLocationTag:2];
                return;
            }
            
            
            
            
            
            
            
        }
            
            break;
            
        case MTTAG:
        {
            
            //            [aButton setImage:[UIImage imageNamed:@"btn_player_pull_screen_pressed"] forState:UIControlStateNormal];
            _isClickedClear = NO;
            
            NSDictionary *daramDic;
            daramDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_REQUEST",@"@command",nil];
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length == 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                _IPTableView.hidden = NO;
                _IPBackIV.hidden = NO;

                break;
            }else{
                [AlertLabel AlertInfoWithText:@"拉屏啦~" andWith:self.view withLocationTag:0];
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:daramDic];
                
            }
            
            
            
            
        }
            break;
            
        case TVTAG:
        {
            _isClickedClear = NO;
            
            //            [aButton setImage:[UIImage imageNamed:@"btn_player_push_screen_pressed"] forState:UIControlStateNormal];
            //
            
            
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            
            if (pointToPointIP.length == 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
                [self showSearchDeviceLoadingView];
                
                _IPTableView.hidden = NO;
                _IPBackIV.hidden = NO;

                break;
            }else{
                [AlertLabel AlertInfoWithText:@"推屏啦~" andWith:self.view withLocationTag:0];
                
                NSInteger point = [self recordPlaybackTime];
                NSMutableDictionary *paramDic ;
                
                
                NSString *contentID;
                NSString *name;
                NSDictionary *resultDic;
                
                if ([_demandModel.contentPullID isEqualToString:@""]) {
                    
                    contentID = self.demandModel.contentID;
                    if ([self.demandModel.seriesList count]>0) {
                        contentID = [[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"contentID"];
                    }
                    
                    name = self.demandModel.name;
                    
                    
                    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)point],@"seekPos",name,@"name", nil];
                    resultDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"DLNAPLAY_VOD",@"@command",paramDic,@"param", nil];
                    
                    
                }else
                {
                    contentID = self.demandModel.contentPullID;
                    
                    
                    name = [NSString stringWithFormat:@"%@",_serviceTitleLabel.text];
                    
                    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",@"1",@"movie_type",[NSString stringWithFormat:@"%ld",(long)point],@"seekPos",name,@"name", nil];
                    resultDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"DLNAPLAY_VOD",@"@command",paramDic,@"param", nil];
                }
                
                
                
                
                if (self.demandModel.fullstartTimeP.length >0||self.demandModel.endTimeP.length >0) {
                    
                    paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",_serviceTitleLabel.text],@"channelName",self.demandModel.fullstartTimeP,@"startDate",self.demandModel.endTimeP,@"endDate",point,@"seekPos", nil];
                    
                    resultDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_EVENT",@"@command",paramDic,@"param", nil];
                    
                }
                
                if (self.demandModel.liveTitleP.length >0) {
                    
                    paramDic = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",self.demandModel.liveTitleP] forKey:@"channelName"];
                    
                    resultDic=[NSDictionary dictionaryWithObjectsAndKeys:@"DLNAPLAY_LIVE",@"@command",paramDic,@"param", nil];
                    
                }
                
                
                
                
                
                
                
                
                
                //                contentID = self.demandModel.contentID;
                //                if ([self.demandModel.seriesList count]>0) {
                //                    contentID = [[self.demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"contentID"];
                //                }
                //
                //                name = self.demandModel.name;
                
                
                
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDINFOTOBOX" object:self userInfo:resultDic];
                
                
            }
            
            
            
            
        }
            
            break;
            
            
            
        case MORE:
        {
            [self reStartHiddenTimer];
            _isClickedClear = NO;
            
            _moreBackView.hidden = !_moreBackView.hidden;
            _videoListView.hidden = YES;
//            _IPTableView.hidden = YES;
            _IPBackIV.hidden = YES;

            _selectClearIV.hidden = YES;
            
            
            
            
            
            
        }
            
            break;
            
            
        case REMOTECONTROL:
        {
            NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
            
            if (pointToPointIP.length <7) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SENDLOCALLIP" object:nil];//发送本地IP
//                _IPTableView.hidden = YES;
                _IPBackIV.hidden = YES;

                
                [self showSearchDeviceLoadingView];
                
                break;
            }else{
                
                [self reloadIPTableView];
                break;
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
    
    
}






-(void)buttonClicked
{
    _bottomBarIV.hidden = NO;
}

#pragma mark - alertView delegate

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    _isEnterClick =YES;
    switch (alertView.tag) {
        case 77:
        {//这个是 是否选择付费
            
            
            if (buttonIndex==0) {
                //确定付费
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

                [request VODPayPlayWithContentID:self.demandModel.contentID andWithLockPWD:@"" andTimeout:kTIME_TEN];
                
                
            }else{
                //确定不付费
                
                
                
                
                return;
                //                _videoPlayPosition//这是选中的剧集在数组中的位置
                //付费
                //                [request VODPayPlayWithContentID:@"" andWithLockPWD:@"" andTimeout:10];
                
            }
            
            
            
        }
            break;
        case 2222:
        {//这个是 是否选择付费
            
            if (buttonIndex==0) {
                //取消，不付费
                [self allcodeRateRequest:alertView.URLStr];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
                
            }else{
                NSLog(@"点击的是取消");
                //                [_moviePlayer stop];
                [self clearMoviePlayerStop];
                _isTapPlayBtn = NO;
                _playBackgroundIV.hidden = NO;
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
                return;
            }
            
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
            
            break;
        case 333333:
        {//这个是 是否选择付费
            
            if (buttonIndex==0) {
                //取消，不付费
                [self allcodeRateRequest:alertView.URLStr];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
            }else{
                NSLog(@"点击的是取消");
                
                //                [_moviePlayer stop];
                [self clearMoviePlayerStop];
                _isTapPlayBtn = NO;
                _playBackgroundIV.hidden = NO;
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstAlert"];
                return;
            }
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            
        }
            
            break;
        case 1008:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            
        default:
            break;
    }
    
}


#pragma mark - detailBackScrollview delegate

-(void)headerViewFavorateBtnSeleced:(UIButton *)btn{
    //收藏按钮
    
    
    if (IsLogin) {
        if (_favourButton.selected == NO || _favourLabel.selected == NO) {
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

            [request ADDVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
            //        [self favorSucceed:YES];
            //        _favourButton.selected = YES;
        }else{
            //        [self favorSucceed:NO];
            //        _favourButton.selected = NO;
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

            [request DELVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
            
        }
        
    }else
    {
        if (isAuthCodeLogin) {
            _isPaused = YES;
            [self actionPauseForVideo];
            [self changeLoginVerticalDirection];
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
            
            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kVideoLogin;
            [self.view addSubview:user.view];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
            user.backBlock = ^()
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                [self actionPlayForVideo];
            };
            
            user.tokenBlock = ^(NSString *token)
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                _isEPGPlayerEnter = YES;
                [self loginAfterEnterVideo];
                [self actionPlayForVideo];
            };
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }else
        {
            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
            
            
        }

    }
    
    
    
    
    
}


-(void)introlViewPraiseBtnSelected:(UIButton *)btn{
    //点赞
    if (IsLogin) {
        
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        [request VODPraiseDegradeWithContentID:_demandModel.contentID andType:1 andTimeout:kTIME_TEN];
        
        
        _isPraiseAction = YES;
    }else
    {
        if (isAuthCodeLogin) {
            _isPaused = YES;
            [self actionPauseForVideo];
            [self changeLoginVerticalDirection];
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
            
            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kVideoLogin;
            [self.view addSubview:user.view];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
            user.backBlock = ^()
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                [self actionPlayForVideo];
            };
            
            user.tokenBlock = ^(NSString *token)
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                _isEPGPlayerEnter = YES;
                [self loginAfterEnterVideo];
                [self actionPlayForVideo];
            };
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }else
        {
            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
            
            
        }

    }
    
    
   
    
}
-(void)introlViewStampBtnSelected:(UIButton *)btn{
    //踩一下
    if (IsLogin) {
        _isPraiseAction = NO;
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request VODPraiseDegradeWithContentID:_demandModel.contentID andType:2 andTimeout:kTIME_TEN];
    }else
    {
        if (isAuthCodeLogin) {
            _isPaused = YES;
            [self actionPauseForVideo];
            [self changeLoginVerticalDirection];
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
            
            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kVideoLogin;
            [self.view addSubview:user.view];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
            user.backBlock = ^()
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                [self actionPlayForVideo];
            };
            
            user.tokenBlock = ^(NSString *token)
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                _isEPGPlayerEnter = YES;
                [self loginAfterEnterVideo];
                [self actionPlayForVideo];
            };
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }else
        {
            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
            
            
        }

    }
    
    
}



-(void)varityViewPraiseBtnSelected:(UIButton *)btn{
    //综艺 点赞
    
    if (IsLogin) {
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request VODPraiseDegradeWithContentID:_demandModel.contentID andType:1 andTimeout:kTIME_TEN];
        
        
        _isPraiseAction = YES;
    }else
    {
        if (isAuthCodeLogin) {
            _isPaused = YES;
            [self actionPauseForVideo];
            [self changeLoginVerticalDirection];
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
            
            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kVideoLogin;
            [self.view addSubview:user.view];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
            user.backBlock = ^()
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                [self actionPlayForVideo];
            };
            
            user.tokenBlock = ^(NSString *token)
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                _isEPGPlayerEnter = YES;
                [self loginAfterEnterVideo];
                [self actionPlayForVideo];
            };
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }else
        {
            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
            
            
        }

    }
    
   
    
}
-(void)varityViewStampBtnSelected:(UIButton *)btn{
    
    
    //综艺 踩一下
    
    if (IsLogin) {
        
        _isPraiseAction = NO;
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request VODPraiseDegradeWithContentID:_demandModel.contentID andType:2 andTimeout:kTIME_TEN];
  
    }else
    {
        if (isAuthCodeLogin) {
            _isPaused = YES;
            [self actionPauseForVideo];
            [self changeLoginVerticalDirection];
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
            
            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
            UserLoginViewController *user = [[UserLoginViewController alloc] init];
            user.loginType = kVideoLogin;
            [self.view addSubview:user.view];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
            user.backBlock = ^()
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                [self actionPlayForVideo];
            };
            
            user.tokenBlock = ^(NSString *token)
            {
                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                _isPaused = NO;
                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                _isEPGPlayerEnter = YES;
                [self loginAfterEnterVideo];
                [self actionPlayForVideo];
            };
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return;
        }else
        {
            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
            
            
        }

    }
    
    
}

-(void)VTabSelectedIndex:(NSInteger)index
{
    _polyLoadingView.hidden = NO;

    //剧集按钮
    firstPeriod = nil;
    startDate = [SystermTimeControl GetSystemTimeWith];
    ZTTimeInterval=0;
    //    [_DBackScrollView episodeViewEBtnSelectedAtIndex:index];
    //这里的剧集是从1 开始算得
    NSLog(@"EBtn selected at %ld",index);
    //    [_moviePlayer stop];
    //    [_moviePlayer setContentURL:nil];
    
    [self clearMoviePlayerStop];
    
    
    if (USER_ACTION_COLLECTION == YES) {
//        _watchDuration = [self timeLengthForCurrent];
//        
//        
//        if ([_watchDuration intValue] >=5) {
//            
////            [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//            [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//            
//        }
        
        if (_isCollectionInfo) {
            _demandModel.contentID = _demandModel.contentID;
            _demandModel.vodFenleiType = [self collectVodInformationType];
            _demandModel.name = _demandModel.name;
            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
            _demandModel.vodBID = _demandModel.vodBID;
            _demandModel.VodBName = _demandModel.VodBName;
            _demandModel.category = _demandModel.category;
            _demandModel.rootCategory = _demandModel.rootCategory;
            _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
            _demandModel.EN_VOD = _demandModel.EN_VOD;

        if ([_watchDuration intValue] >= 5) {
            
            [SAIInformationManager VodInformation:_demandModel];
        }

        [_timeManager reset];
        }
        //end
        
    }
    
    //播放记录
    _localVideoModel.lvNAME = _demandModel.NAME;
    _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
    _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
    _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
    _localVideoModel.lvImageLink = _demandModel.imageLink;
    _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
    
//    [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localVideoModel resetLocalVideoModel];
    
    if (self.demandModel.seriesList.count>0) {
        if (self.demandModel.seriesList.count == index) {
            _isPlayLastVideo = YES;
        }
        
        self.demandModel.name = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"subName"]];
        
        
        if (![[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"subName"]) {
            _serviceTitleLabel.text = @"";
        }
        
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",self.demandModel.name];
        _demandModel.contentID = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"contentID"]];
        JumpStartTime = [[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"breakPoint"]integerValue];
        
        if (IS_PERMISSION_GROUP) {
            
            
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];

            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
        }
//        _videoPlayPosition = [[NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"seq"]] integerValue]-1;
        //有的综艺确实没有seq
        _videoPlayPosition = index-1;
        
        if (_videoPlayPosition == 0) {
//            _forwardButton.enabled = NO;
        }else
        {
//            _forwardButton.enabled = YES;
        }
        
        _channelSelectIndex = _videoPlayPosition;
        
        _demandModel.TRACKID_VOD = @"_";
        _demandModel.TRACKNAME_VOD = @"选集";
        _demandModel.EN_VOD = @"9";
        
        
        
        NSString *ownStr = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"isOwn"]];;
        
        if ([ownStr isEqualToString:@"true"]) {
            self.demandModel.isOwn = YES;

            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

//            [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:movieType andTimeout:10];
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
            [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
            }
        }else
        {
            self.demandModel.isOwn = NO;
            [self clearMoviePlayerStop];
            
        }
        
        
        
        
        
      
        
    }

}

-(void)episodeViewEBtnSelectedAtIndex:(NSInteger)index{
    //剧集按钮
    _polyLoadingView.hidden = NO;

    firstPeriod = nil;
    startDate = [SystermTimeControl GetSystemTimeWith];
    ZTTimeInterval=0;
    //    [_DBackScrollView episodeViewEBtnSelectedAtIndex:index];
    //这里的剧集是从1 开始算得
    NSLog(@"EBtn selected at %ld",index);
    //    [_moviePlayer stop];
    //    [_moviePlayer setContentURL:nil];
    
    [self clearMoviePlayerStop];
    
    
    if (USER_ACTION_COLLECTION == YES) {
//        _watchDuration = [self timeLengthForCurrent];
//        
//        
//        if ([_watchDuration intValue] >=5) {
//            
////            [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//            
//            [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//            
//        }
        if (_isCollectionInfo) {
            _demandModel.contentID = _demandModel.contentID;
            _demandModel.vodFenleiType = [self collectVodInformationType];
            _demandModel.name = _demandModel.name;
            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
            _demandModel.vodBID = _demandModel.vodBID;
            _demandModel.VodBName = _demandModel.VodBName;
            _demandModel.category = _demandModel.category;
            _demandModel.rootCategory = _demandModel.rootCategory;
            _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
            _demandModel.EN_VOD = _demandModel.EN_VOD;

            
        if ([_watchDuration intValue] >= 5) {
            
            [SAIInformationManager VodInformation:_demandModel];
        }
        [_timeManager reset];
        }
        //end
        
    }
    
    //播放记录
    _localVideoModel.lvNAME = _demandModel.NAME;
    _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
    _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
    _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
    _localVideoModel.lvImageLink = _demandModel.imageLink;
    _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
    
//    [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localVideoModel resetLocalVideoModel];
    
    if (self.demandModel.seriesList.count>0) {
        
        if (self.demandModel.seriesList.count == index) {
            _isPlayLastVideo = YES;
        }
//        NSString *seqM = [[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"seq"];
//        
//
//        if ([seqM intValue]<=10) {
//            seqM = [NSString stringWithFormat:@"%02ld",[seqM integerValue]];
//        }
//        
//        NSString *seqAmount = [NSString stringWithFormat:@"%ld",self.demandModel.Amount];
//        
//        if ([seqAmount intValue]<=10) {
//            seqAmount = [NSString stringWithFormat:@"%02ld",[seqAmount integerValue]];
//        }

        self.demandModel.name = [[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"name"];
//        self.demandModel.name = [NSString stringWithFormat:@"%@(%@/%@)",self.demandModel.NAME,seqM,seqAmount];
        
        if (![[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"name"]) {
            _serviceTitleLabel.text = @"";
        }
        if (self.demandModel.name.length<=0) {
            _serviceTitleLabel.text = @"";
        }else
        {
        _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",self.demandModel.name];
        }
        _demandModel.contentID = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"contentID"]];
        JumpStartTime = [[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"breakPoint"]integerValue];
        
        if (IS_PERMISSION_GROUP) {
            
            
            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];

            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
        }
        
        _videoPlayPosition = index-1;

//        _videoPlayPosition = [[NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"seq"]] integerValue]-1;
        
        
        if (_videoPlayPosition == 0) {
//            _forwardButton.enabled = NO;
        }else
        {
//            _forwardButton.enabled = YES;
        }
        
        _channelSelectIndex = _videoPlayPosition;
        
        _demandModel.TRACKID_VOD = @"_";
        _demandModel.TRACKNAME_VOD = @"选集";
        _demandModel.EN_VOD = @"9";
        
        
        NSString *ownStr = [NSString stringWithFormat:@"%@",[[self.demandModel.seriesList objectAtIndex:index-1] objectForKey:@"isOwn"]];;
        
        if ([ownStr isEqualToString:@"true"]) {
            self.demandModel.isOwn = YES;

            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

//            [request VODPlayWithContentID:_demandModel.contentID andWithMovie_type:movieType andTimeout:10];
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
            [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
            }
        }else
        {
            self.demandModel.isOwn = NO;
            [self clearMoviePlayerStop];
            
        }
        
        
        
        
        
        
        
        
        
        
    }
    
}
-(void)recommandViewItemsSelectedAtIndex:(NSInteger)index{
    //推荐剧集 选中元素
    //这里从1 开始计数
    NSLog(@"推荐元素 selected at %ld",index);
    _polyLoadingView.hidden = NO;
    firstPeriod = nil;
    startDate = [SystermTimeControl GetSystemTimeWith];
    ZTTimeInterval=0;
    _playPauseBigBtn.hidden = YES;
    _isPaused = NO;
    //    [_moviePlayer stop];
    //    [_moviePlayer setContentURL:nil];
    //    if (self.myNewTimer)
    //    {
    //        [self.myNewTimer invalidate];
    //    }
    [self clearMoviePlayerStop];
    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    
    
    if (USER_ACTION_COLLECTION == YES) {
//        _watchDuration = [self timeLengthForCurrent];
//        
//        if ([self.watchDuration intValue] >=5) {
//            
////            [SAIInformationManager VODinformationForVodFenlei:@"" andVodName:_demandModel.name andVodTime:self.nowDateString andVodDuration:self.watchDuration];
//            [SAIInformationManager VODinformationForVodContentID:self.demandModel.contentID andVodFenlei:[self collectVodInformationType] andVodName:_demandModel.name andVodTime:_nowDateString andVodDuration:_watchDuration];
//        }
        if (_isCollectionInfo) {
            _demandModel.contentID = _demandModel.contentID;
            _demandModel.vodFenleiType = [self collectVodInformationType];
            _demandModel.name = _demandModel.name;
            _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
            _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
            _demandModel.vodBID = _demandModel.vodBID;
            _demandModel.VodBName = _demandModel.VodBName;
            _demandModel.category = _demandModel.category;
            _demandModel.rootCategory = _demandModel.rootCategory;
            _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
            _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
            _demandModel.EN_VOD = @"2";
            
        if ([_watchDuration intValue] >= 5) {
            
            [SAIInformationManager VodInformation:_demandModel];
        }
//            _demandModel.TRACKID_VOD = nil;
//            _demandModel.TRACKNAME_VOD = nil;
//            _demandModel.EN_VOD = nil;
        [_timeManager reset];
        }
    }
    
    
    
    //播放记录
    _localVideoModel.lvNAME = _demandModel.NAME;
    _localVideoModel.lvCONTENTID = _demandModel.CONTENTID;
    _localVideoModel.lvBreakPoint = [NSString stringWithFormat:@"%ld",recordTime];
    _localVideoModel.lvLastPlayEpisode = _demandModel.lastPlayEpisode;
    _localVideoModel.lvImageLink = _demandModel.imageLink;
    _localVideoModel.lvLastEpisodeTitle = _demandModel.lastPlayEpisodeTitle;
    
//    [LocalCollectAndPlayRecorder addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localListCenter addVODPlayRecordWithModel:_localVideoModel andUserType:IsLogin];
    [_localVideoModel resetLocalVideoModel];
    
    
    
    
//    if (_vodPlayType !=VODPlayMovie) {
//        NSDictionary *videoDic = [[NSDictionary alloc]init];
//        videoDic =  [self.demandModel.seriesList objectAtIndex:index-1];
//        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//        
//        [request VODDetailWithContentID:[videoDic objectForKey:@"contentID"] andTimeout:10];
//        [request VODAssccationListWithContentID:[videoDic objectForKey:@"contentID"] andCount:12 andTimeout:kTIME_TEN];
//        _isFirstVideo = YES;
//    }
    
    NSDictionary *videoDic = [[NSDictionary alloc]init];
    if (_asscciationArr.count>0) {
        videoDic =  [_asscciationArr objectAtIndex:index-1];
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        _demandModel.TRACKID_VOD = [videoDic objectForKey:@"contentID"];
        _demandModel.TRACKNAME_VOD = [videoDic objectForKey:@"name"];
        _demandModel.EN_VOD = @"2";
        [request VODDetailWithContentID:[videoDic objectForKey:@"contentID"] andTimeout:10];
        [request VODAssccationListWithContentID:[videoDic objectForKey:@"contentID"] andCount:12 andTimeout:kTIME_TEN];
        _isFirstVideo = YES;
        _channelSelectIndex = 0;
        _videoPlayPosition = 0;

    }
    
    
    
}




-(NSString *)resetOrderMoneyString:(NSString*)MoneyString
{
    NSString *anotherString;
    NSString *moneyStr =[NSString stringWithFormat:@"￥%@/月",MoneyString];
    NSInteger len = [moneyStr length];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:moneyStr];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, len-1)];
    
    anotherString=[str string];
    
    return anotherString;
}


#pragma mark - 信息采集类型
-(NSString *)collectVodInformationType
{
    NSString *vodPlayTypeStr;
    if (_vodPlayType == VODPlayMovie) {
        
        vodPlayTypeStr = @"电影";
    }
    if (_vodPlayType == VodPlayTVSerial) {
        
        vodPlayTypeStr = @"电视剧";
    }
    if (_vodPlayType == VOdPlayVariety) {
        
        vodPlayTypeStr = @"综艺";
    }
    
    return vodPlayTypeStr;
}




#pragma mark - request delegate


-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    [_HUD hide:YES afterDelay:0.2];
    NSLog(@"status : %ld result : %@ type : %@",(long)status,result,type);
    
    if ([type isEqualToString:JXTV_BUSINESS_INSTALLINFO]) {
        NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        _blackInstallView.hidden = YES;
        if (ret == 0) {
            
            ZSToast(@"报装成功啦 小晴会尽快与您联系^_^")
            
        }else
        {
            ZSToast(@"提交信息失败啦 再来一次吧 加油^_^");
        }
        
    }
    


    if ([type isEqualToString:@"programDetail"]) {
        _isHaveAD = NO;
        _adPlayerView.timeLabel.text = @"";
        //先隐藏广告，在VODPlay回调中确定有广告了，再不隐藏
        _adPlayerView.hidden = YES;
//        _adPlayerView.isCanContinu = NO;
        _returnMainBtn.alpha = 1;
        
        //********* 详情加载
        if ([[result objectForKey:@"ret"] integerValue]!=0 || status !=0 || [result count]<=2) {
            if (_contentTitleName.length>0) {
                _serviceTitleLabel.text = _contentTitleName;
            }else{
                _serviceTitleLabel.text = @"";
            }
//            _BufferLabel.hidden = NO;
            _bufferView.hidden = NO;
            if (ISIntall) {
                _installMaskView.hidden = NO;
            }
        }else{
            _bufferView.hidden = YES;
        }
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {
                NSString *vodTrackID = @"";
                NSString *vodTrackName = @"";
                NSString *vodEN = @"";
                
                vodTrackID =   isEmptyStringOrNilOrNull(self.demandModel.TRACKID_VOD)?self.TRACKID:self.demandModel.TRACKID_VOD;
                vodTrackName = isEmptyStringOrNilOrNull(self.demandModel.TRACKNAME_VOD)?self.TRACKNAME:self.demandModel.TRACKNAME_VOD;
                vodEN = isEmptyStringOrNilOrNull(self.demandModel.EN_VOD)?self.EN:@"9";
                
                
                
                if (self.demandModel) {
                    self.demandModel = nil;
                    self.demandModel = [[DemandModel alloc] init];
                }
                self.demandModel.resultDictionary = (NSMutableDictionary *)result;
                self.demandModel.TRACKID_VOD = vodTrackID;
                self.demandModel.TRACKNAME_VOD = vodTrackName;
                self.demandModel.EN_VOD = vodEN;
                NSLog(@"==看看==%@+%@",self.demandModel.TRACKID_VOD,self.demandModel.TRACKNAME_VOD);

                if ([[result objectForKey:@"deadline"] length]>0) {
                    self.demandModel.deadline = [result objectForKey:@"deadline"];
                }
                
                if ([[result objectForKey:@"actor"] length]>0) {
                    
                    self.demandModel.Actor = [result objectForKey:@"actor"];
                }
                
                if ([result objectForKey:@"playCount"]>0) {
                    
                    self.demandModel.playCount = [[result objectForKey:@"playCount"] integerValue];
                }
                
                [_moviePlayerBottomView resetPlayNumWith:[NSString stringWithFormat:@"%ld",self.demandModel.playCount]];
                
                if ([result objectForKey:@"praiseCount"]>0) {
                    self.demandModel.praiseCount = [[result objectForKey:@"praiseCount"] integerValue];
                }
                
                if ([result objectForKey:@"degradeCount"]>0) {
                    self.demandModel.degradeCount = [[result objectForKey:@"degradeCount"] integerValue];
                }
                
                if ([[result objectForKey:@"director"] length]>0) {
                    self.demandModel.Director = [result objectForKey:@"director"];
                    
                }
                
                if ([[result objectForKey:@"country"] length]>0) {
                    self.demandModel.country = [result objectForKey:@"country"];
                }
                
                if ([[result objectForKey:@"imageLink"] length]>0) {
                    self.demandModel.imageLink = [result objectForKey:@"imageLink"];
                }
                
                if ([[result objectForKey:@"intro"] length]>0) {
                    self.demandModel.intro = [result objectForKey:@"intro"];
                }
                
                if ([[result objectForKey:@"language"] length]>0) {
                    self.demandModel.language = [result objectForKey:@"language"];
                }
                
                if ([[result objectForKey:@"releaseYear"] length]>0) {
                    self.demandModel.releaseYear = [result objectForKey:@"releaseYear"];
                    
                }
                
                if ([[result objectForKey:@"licensingEndTime"] length]>0) {
                    self.demandModel.licensingEndTime = [result objectForKey:@"licensingEndTime"];
                }
                
                if ([[result objectForKey:@"orderEndTime"] length]>0) {
                    self.demandModel.orderEndTime = [result objectForKey:@"orderEndTime"];
                }
                
                if ([result objectForKey:@"type"]) {
                    self.demandModel.multipleType = [result objectForKey:@"type"];
                    
                }
                
                if ([result objectForKey:@"ret"]>0) {
                    self.demandModel.ret = [[result objectForKey:@"ret"] integerValue];
                    
                }
                
                if ([result objectForKey:@"duration"]>0) {
                    self.demandModel.duration = [[result objectForKey:@"duration"] integerValue];
                    
                }
                
                
                if ([result objectForKey:@"amount"]>0) {
                    
                    self.demandModel.Amount = [[result objectForKey:@"amount"] integerValue];
                    
                }
                
                if ([result objectForKey:@"name"]>0) {
                    
                    self.demandModel.NAME = [result objectForKey:@"name"];
                    
                }
                
                
                if ([result objectForKey:@"category"]>0) {
                    self.demandModel.category = [result objectForKey:@"category"];
                }
                
                
                
                if ([result objectForKey:@"rootCategory"]>0) {
                    
                    self.demandModel.rootCategory = [result objectForKey:@"rootCategory"];
                }
                
                
                self.demandModel.lastPlayEpisode = [result objectForKey:@"lastPlayEpisode"];
                self.demandModel.lastPlayEpisodeTitle = [result objectForKey:@"lastEpisodeTitle"];
                self.demandModel.STYLE = [result objectForKey:@"style"];
                self.demandModel.definination = [result objectForKey:@"definition"];
                    
                    if ( self.demandModel.lastPlayEpisode == nil || self.demandModel.lastPlayEpisode.length<=0) {
                        self.demandModel.lastPlayEpisode = @"1";
                    }
                    
                
                
                _DBackScrollView.hidden = NO;
                NSLog(@"VOD_detail:%@",_demandModel.Actor);
                [_DBackScrollView.KNewIntrolView setIntroInfoWithModel:self.demandModel];
                
                self.demandModel.CONTENTID = [result objectForKey:@"contentID"];
                
                
                [self loginAfterRefreshFavor];

                
                
//                self.demandModel.TRACKID_VOD = isEmptyStringOrNilOrNull(self.TRACKID)?[result objectForKey:@"contentID"] :self.TRACKID ;
//                self.demandModel.TRACKNAME_VOD = self.TRACKNAME;
//                self.demandModel.EN_VOD = self.EN;
                
                
                
//                if (IsLogin) {
//                    BOOL isFavored = [[UserCollectCenter defaultCenter]checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID];
//                    
//                    if (isFavored) {
//                        
//                        //            [self favorSucceed:YES];
//                        [_moviePlayerBottomView resetFavorate:YES];
//                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
//                        
//                    }else
//                    {
//                        
//                        //            [self favorSucceed:NO];
//                        [_moviePlayerBottomView resetFavorate:NO];
//                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
//                        
//                    }
//                }
                
                //ww...
                if (IsLogin) {
                    BOOL isFavored = [_localListCenter checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID andUserType:YES];
                    if (isFavored) {
                        [_moviePlayerBottomView resetFavorate:YES];
                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
                    }else{
                        
                        [_moviePlayerBottomView resetFavorate:NO];
                        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
                    }
                }
                
                
//                self.demandModel.seriesList = [result objectForKey:@"seriesList"];

                if ([self.demandModel.multipleType isEqualToString:@"0"]) {
                    
                    //是单集的标识
                    
                    _vodPlayType = VODPlayMovie;
                    //是电影
                    [_DBackScrollView setProgramTypeWith:1];//设置剧集类型
                    
                    [_DBackScrollView KNewIntrolViewLoadingIntrolInfoWithName:_demandModel.NAME andActor:_demandModel.Actor andDirector:_demandModel.Director andType:_demandModel.category andYears:_demandModel.releaseYear andArea:_demandModel.country  andPraiseNum:[NSString stringWithFormat:@"%ld",_demandModel.praiseCount] andStampNum:[NSString stringWithFormat:@"%ld",_demandModel.degradeCount] andIntrol:_demandModel.intro];
                    
//                    [_videoListButton setTitle:@"推荐" forState:UIControlStateNormal];
                    [_videoListButton setImage:[UIImage imageNamed:@"ic_player_recommend_normal"] forState:UIControlStateNormal];
                    [_videoListButton setImage:[UIImage imageNamed:@"ic_player_recommend_pressed"] forState:UIControlStateHighlighted];
                    self.demandModel.name = [result objectForKey:@"name"];
                    _backwardButton.enabled = YES;
//                    _forwardButton.enabled = NO;
                    self.demandModel.contentID = [result valueForKey:@"contentID"];
                    JumpStartTime = [[result valueForKey:@"breakPoint"] integerValue];
//                    self.demandModel.favorContenID = self.demandModel.contentID;
                    
                    self.demandModel.vodBID = @"";
                    self.demandModel.VodBName = @"";
                    if ([[result objectForKey:@"isOwn"] length]>0) {
                        NSString *M_isOwn = [result objectForKey:@"isOwn"];
                        
                        if ([M_isOwn isEqualToString:@"true"]) {
                            self.demandModel.isOwn = YES;
                        }else{
                            self.demandModel.isOwn = NO;
                        }

                        
                        if (self.demandModel.isOwn == NO) {
                            [self clearMoviePlayerStop];
                            
                            
                            if (IsLogin) {
                                
                                if (IsBinding) {
                                    
                                    _jxPlayerBindView.hidden = YES;
                                }else
                                {
                                    
                                    _jxPlayerBindView.hidden = NO;
                                }
                                [[self.view viewWithTag:4040] setHidden:YES];
                                
                            }else
                            {
                                
                                if (IS_PERMISSION_GROUP) {
                                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                                    
                                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                                    
                                    _playerLoginView.hidden = NO;
                                    _jxPlayerLoginView.hidden = NO;
                                    _isEPGPlayerEnter = YES;
                                    _jxPlayerLoginView.hidden = NO;
                                    
                                }else
                                {
                                    //                                _playBackgroundIV.hidden = NO;
                                    if (isAuthCodeLogin) {
                                        _isPaused = YES;
                                        [self actionPauseForVideo];
                                        [self changeLoginVerticalDirection];
                                        NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                                        [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                                        [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                                        
                                        [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                                        UserLoginViewController *user = [[UserLoginViewController alloc] init];
                                        user.loginType = kVideoLogin;
                                        [self.view addSubview:user.view];
                                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                                        user.backBlock = ^()
                                        {
                                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                            _isPaused = NO;
                                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                            [self actionPlayForVideo];
                                        };
                                        
                                        user.tokenBlock = ^(NSString *token)
                                        {
                                            [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                            _isPaused = NO;
                                            [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                            _isEPGPlayerEnter = YES;
                                            [self loginAfterEnterVideo];
                                            [self actionPlayForVideo];
                                        };
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        
                                        return;
                                    }else
                                    {
                                        [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                                        
                                        
                                    }
                                }
                            }
                            
                            
                            
                        }
                        
                        else
                        {
                            
                            _channelSelectIndex = -1;

                            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                            
                            //                        [request VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:movieType andTimeout:kTIME_TEN];
                            if (ISIntall) {
                                
                                _installMaskView.hidden = NO;
                            }else
                            {
                                [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                            }
                            
                            
                        }


                        
                    }
                    
                    
                   
    
                    
                }
                
                
                if ([self.demandModel.multipleType isEqualToString:@"9"]) {
                    //是多集的标识
//                    [_videoListButton setTitle:@"选集" forState:UIControlStateNormal];
                    [_videoListButton setImage:[UIImage imageNamed:@"btn_player_recommend_normal"] forState:UIControlStateNormal];
                    [_videoListButton setImage:[UIImage imageNamed:@"btn_player_recommend_pressed"] forState:UIControlStateHighlighted];
                    //多集的数组
                    self.demandModel.seriesList = [result objectForKey:@"seriesList"];
//                    self.demandModel.seriesList = [KaelTool increaseArrayWithArray:self.demandModel.seriesList andKey:@"seq"];
                    
                    
                    _videoListArr =   self.demandModel.seriesList;
                    
            
                    if ([self.demandModel.rootCategory isEqualToString:@"综艺"]||[self.demandModel.rootCategory isEqualToString:@"纪实"]) {
                        _vodPlayType = VOdPlayVariety;
                       //综艺类
                        [_DBackScrollView setProgramTypeWith:3];//设置剧集类型 这里应该是3 暂时传2
                        
                        [_DBackScrollView KNewIntrolViewLoadingIntrolInfoWithName:_demandModel.NAME andActor:_demandModel.Actor andDirector:_demandModel.Director andType:_demandModel.category andYears:_demandModel.releaseYear andArea:_demandModel.country  andPraiseNum:[NSString stringWithFormat:@"%ld",(long)_demandModel.praiseCount] andStampNum:[NSString stringWithFormat:@"%ld",_demandModel.degradeCount] andIntrol:_demandModel.intro];
                        
                        [_DBackScrollView reloadVarityDateWith:self.demandModel.seriesList andAllEpisodNum:self.demandModel.Amount andUpdateNum:[result objectForKey:@"lastEpisodeTitle"]];
                   
//                        
//                        if (!self.episodeSeq) {
//                            
//                            self.episodeContentID = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"contentID"];
//                            JumpStartTime = [[[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"breakPoint"] integerValue];
//                            self.episodeName = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"name"];
//                            NSString *ownStr = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"isOwn"];
//                            
//                            if ([ownStr isEqualToString:@"true"]) {
//                                self.demandModel.isOwn = YES;
//                            }else
//                            {
//                                self.demandModel.isOwn = NO;
//                            }
//                            
//                            
//                            self.demandModel.name = [NSString stringWithFormat:@"%@",self.episodeName];
//                            
//                      
//                            [_DBackScrollView varityViewSetselectedAtIndex:1];
//                            
//                            _channelSelectIndex = 0;
//                        }else
//                        {
                            //最后的剧集数按0后台返回的，改一下
                        _episodeSeq = self.demandModel.lastPlayEpisode;
                        
                        for ( int i=0; i<self.demandModel.seriesList.count; i++) {
                            NSString *tmp = [[self.demandModel.seriesList objectAtIndex:i] objectForKey:@"seq"];
                            if ([_episodeSeq integerValue] == [tmp integerValue]) {
                                
                                _episodeSeq = [NSString stringWithFormat:@"%d",i+1];
                                break;
                            }else{
                                _episodeSeq = @"1";
                            }
                            
                        }

                            
                            self.episodeContentID = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"contentID"];
                            JumpStartTime = [[[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"breakPoint"] integerValue];
                            self.episodeName = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"subName"];
                            NSString *ownStr = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"isOwn"];
      
                            self.demandModel.name = [NSString stringWithFormat:@"%@",self.episodeName];
                            
                            
                            if ([ownStr isEqualToString:@"true"]) {
                                self.demandModel.isOwn = YES;
                            }else
                            {
                                self.demandModel.isOwn = NO;
                            }

                            
                            [_DBackScrollView varityViewSetselectedAtIndex:[self.episodeSeq integerValue]];
                            
                            _videoPlayPosition = [self.episodeSeq integerValue]-1;
                            _channelSelectIndex = _videoPlayPosition;
                        
                        self.demandModel.contentID = self.episodeContentID;
                        
                        

//                        }

                                              //----判断 付费否 的方法
//                        NSArray *arr = self.demandModel.seriesList;
//                        for (int i = 1; i<=[self.demandModel.seriesList count]; i++) {
//                            
//                            NSDictionary *episodeDic = [[NSDictionary alloc] initWithDictionary:[self.demandModel.seriesList objectAtIndex:i-1]];
//                            NSString *isTrue = [episodeDic objectForKey:@"isOwn"];
//                            
//                            if ([isTrue isEqualToString:@"true"]) {
//                                NSLog(@"第 %d 集免费",i);
//                                _orangeView.hidden = YES;
//                                [_DBackScrollView episodeViewHiddenBadgeAtIndex:i];
//                                
//                            }else
//                            {
//                                _orangeView.hidden = NO;
//                                
//                                [self clearMoviePlayerStop];
//                            }
//                            
//                            
//                        }
                        
                        
                    }else
                    {
                        
                        _vodPlayType = VodPlayTVSerial;
                        
                        _orderView.hidden = YES;
                        //电视剧类
                        [_videoListButton setImage:[UIImage imageNamed:@"btn_player_recommend_normal"] forState:UIControlStateNormal];
                        [_videoListButton setImage:[UIImage imageNamed:@"btn_player_recommend_pressed"] forState:UIControlStateHighlighted];
                        
                            [_DBackScrollView setProgramTypeWith:2];//设置剧集类型
                        self.demandModel.vodBID = self.demandModel.CONTENTID;
                        self.demandModel.VodBName = self.demandModel.NAME;
                        NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:self.demandModel.seriesList];
//                        if (marr.count>=4) {
//                            [marr removeObjectAtIndex:1];
//                        }
                            _DBackScrollView.episodeView.truthEpisodeArr = marr;

                        [_DBackScrollView KNewIntrolViewLoadingIntrolInfoWithName:_demandModel.NAME andActor:_demandModel.Actor andDirector:_demandModel.Director andType:_demandModel.category andYears:_demandModel.releaseYear andArea:_demandModel.country  andPraiseNum:[NSString stringWithFormat:@"%ld",(long)_demandModel.praiseCount] andStampNum:[NSString stringWithFormat:@"%ld",_demandModel.degradeCount] andIntrol:_demandModel.intro];

                        
                            [_DBackScrollView episodeViewLoadeAllEpisodNum:[NSString stringWithFormat:@"%@",[result objectForKey:@"amount"]] andUpdateNum:[NSString stringWithFormat:@"%@",[result objectForKey:@"lastEpisodeTitle"]]];
                        
                        
//                        if (!self.episodeSeq) {
//                            self.episodeContentID = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"contentID"];
//                            JumpStartTime = [[[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"breakPoint"] integerValue];
//                            self.episodeName = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"name"];
//                            NSString *ownStr = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"isOwn"];
//                            
//                            if ([ownStr isEqualToString:@"true"]) {
//                                self.demandModel.isOwn = YES;
//                            }else
//                            {
//                                self.demandModel.isOwn = NO;
//                            }
//                            
//                            NSString *seqM = [[[result objectForKey:@"seriesList"] objectAtIndex:0] objectForKey:@"seq"];
//                            
//                            if ([seqM intValue]<=10) {
//                                seqM = [NSString stringWithFormat:@"0%@",seqM];
//                            }
//                            
//                            NSString *seqAmount = [NSString stringWithFormat:@"%ld",self.demandModel.Amount];
//                            
//                            if ([seqAmount intValue]<=10) {
//                                seqAmount = [NSString stringWithFormat:@"0%@",seqAmount];
//                            }
//                            
//                            
//                            
//                            self.demandModel.name = [NSString stringWithFormat:@"%@(%@/%@)",self.demandModel.NAME,seqM,seqAmount];
//
//                            
//                            
//                            [_DBackScrollView episodeViewSetSelectedBtnAtIndex:1];//设置默认选中按钮数
//                            
//                            _channelSelectIndex = 0;
//                        }else
//                        {
                        
                        _episodeSeq = self.demandModel.lastPlayEpisode;

                        for ( int i=0; i<self.demandModel.seriesList.count; i++) {
                            NSString *tmp = [[self.demandModel.seriesList objectAtIndex:i] objectForKey:@"seq"];
                            if ([_episodeSeq integerValue] == [tmp integerValue]) {
                                
                                _episodeSeq = [NSString stringWithFormat:@"%d",i+1];
                                break;
                            }else{
                                _episodeSeq = @"1";
                            }
                            
                        }
                        
                        
                            self.demandModel.contentID = [[self.demandModel.seriesList objectAtIndex:[_episodeSeq integerValue]-1] objectForKey:@"contentID"];
                            JumpStartTime = [[[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"breakPoint"] integerValue];
                            self.episodeName = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"name"];
                            NSString *ownStr = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"isOwn"];
//                            NSString *seqM = [[[result objectForKey:@"seriesList"] objectAtIndex:[_episodeSeq intValue]-1] objectForKey:@"seq"];
//                            
//                            if ([seqM intValue]<=10) {
//                                seqM = [NSString stringWithFormat:@"%02ld",[seqM integerValue]];
//                            }
//                            
//                            NSString *seqAmount = [NSString stringWithFormat:@"%ld",self.demandModel.Amount];
//                            
//                            if ([seqAmount intValue]<=10) {
//                                seqAmount = [NSString stringWithFormat:@"%02ld",[seqAmount integerValue]];
//                            }
//                            
//                            self.demandModel.name = [NSString stringWithFormat:@"%@(%@/%@)",self.demandModel.NAME,seqM,seqAmount];
                        self.demandModel.name = [NSString stringWithFormat:@"%@",self.episodeName];
                            
                            if ([ownStr isEqualToString:@"true"]) {
                                self.demandModel.isOwn = YES;
                            }else
                            {
                                self.demandModel.isOwn = NO;
                            }

                            [_DBackScrollView episodeViewSetSelectedBtnAtIndex:[self.episodeSeq integerValue]];//设置默认选中按钮数
                            
                            _videoPlayPosition = [self.episodeSeq integerValue]-1;
                            _channelSelectIndex = _videoPlayPosition;
//                        }

                        
//                        self.demandModel.contentID = self.episodeContentID;
                        
                            //----判断 付费否 的方法
                            for (int i = 1; i<=[self.demandModel.seriesList count]; i++) {
                                
                                NSDictionary *episodeDic = [[NSDictionary alloc] initWithDictionary:[self.demandModel.seriesList objectAtIndex:i-1]];
                                NSString *isTrue = [episodeDic objectForKey:@"isOwn"];
                                
                                if ([isTrue isEqualToString:@"true"]) {
                                    NSLog(@"第 %d 集免费",i);
                                    _orangeView.hidden = YES;
                                    [_DBackScrollView episodeViewHiddenBadgeAtIndex:i];
                                    
                                }else
                                {
                                    _orangeView.hidden = NO;
                                    [self clearMoviePlayerStop];
                                }
                                
                                
                            }
                            
                        }
                    
                    
                    if (self.demandModel.isOwn == YES) {
                        
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

                        NSLog(@"HHLLLLLLL%@",self.demandModel.contentID);

//                        [request VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:movieType andTimeout:kTIME_TEN];
                        if (ISIntall) {
                            
                            _installMaskView.hidden = NO;
                        }else
                        {
                        [request VODPlayWithContentID:self.demandModel.contentID andName:self.demandModel.name andWithMovie_type:@"1" andTimeout:kTIME_TEN];
                        }
//                        if (IsLogin) {
//                            NSString *movieType = @"1";
//                            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
//                            
//                            NSLog(@"HHLLLLLLL%@",self.demandModel.contentID);
//                            
//                            [request VODPlayWithContentID:self.demandModel.contentID andWithMovie_type:movieType andTimeout:kTIME_TEN];
//                           
//                            
//                        }else
//                        {
//                            if (IS_PERMISSION_GROUP) {
//                                _playerLoginView.hidden = NO;
//  
//                            }else
//                            {
//                                [AlertLabel AlertInfoWithText:@"请先登录!" andWith:_contentView withLocationTag:2];
//                            }
//                        }
                        
                        
                        
                        
                    }else{
                        [self clearMoviePlayerStop];
                        
                        if (IsLogin) {
                            _orderView.hidden = YES;
                            
                        }else
                        {
                            [self clearMoviePlayerStop];

                            if (IS_PERMISSION_GROUP) {
                                
                                _playerLoginView.hidden = NO;
                                _isEPGPlayerEnter = YES;
                                _jxPlayerLoginView.hidden = NO;
                            }else
                            {
                                if (isAuthCodeLogin) {
                                    _isPaused = YES;
                                    [self actionPauseForVideo];
                                    [self changeLoginVerticalDirection];
                                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                                    
                                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                                    user.loginType = kVideoLogin;
                                    [self.view addSubview:user.view];
                                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                                    user.backBlock = ^()
                                    {
                                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                        _isPaused = NO;
                                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                        [self actionPlayForVideo];
                                    };
                                    
                                    user.tokenBlock = ^(NSString *token)
                                    {
                                        _isPaused = NO;
                                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                        _isEPGPlayerEnter = YES;
                                        [self loginAfterEnterVideo];
                                        [self actionPlayForVideo];
                                    };
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    return;
                                }else
                                {
                                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                                    
                                    
                                }

                            }
                            
                            
                            
                        }
                        
                    }

    
                        
                }
                
//
                //复制电视剧的剧集数
                
             
                if (!self.demandModel.name) {
                    if (_contentTitleName) {
                        _serviceTitleLabel.text = _contentTitleName;
                    }else
                    {
                        _serviceTitleLabel.text = @"";
                    }
                }else
                {
                _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",self.demandModel.name];
                }
                if (!self.demandModel.name) {
                    
                    [self setNaviBarTitle:@""];
                }else
                {
                    [self setNaviBarTitle:[NSString stringWithFormat:@"%@",self.demandModel.name]];
                }
                
                    
//                if (IsLogin) {
//                    BOOL isFavored = [[UserCollectCenter defaultCenter]checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID];
//                    
//                    if (isFavored) {
//                        
////                        [self favorSucceed:YES];
//                        [_moviePlayerBottomView resetFavorate:YES];
//                        
//                    }else
//                    {
//                        
////                        [self favorSucceed:NO];
//                        
//                        [_moviePlayerBottomView resetFavorate:NO];
//
//                    }
//                    
//                    
//                }else
//                {
//                }
                
                //ww...
                if (IsLogin) {
                    
                    BOOL isFavored = [_localListCenter checkTheFavoriteDemandListWithContentID:self.demandModel.CONTENTID andUserType:YES];
                    if (isFavored) {
                        [_moviePlayerBottomView resetFavorate:YES];
                    }else{
                        [_moviePlayerBottomView resetFavorate:NO];
                    }
                }
                
                
//                _isTapPlayBtn = YES;
//                if (_isFirstVideo == YES) {
//                    [self TapSmallScreenbtn_playNoButton];
//
//                    _isFirstVideo = NO;
//                }
                
                //点赞吐槽查询
                if (IS_CHECTOUT_PRAISEDEGRADE) {
                    HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self] ;
                    [reqest VODPraiseDegradeDetailWithContentID:_demandModel.contentID andTimeout:10];
                    
                }
                
            }
                break;
            case -1:
            {
                CustomAlertView *customView = [[CustomAlertView alloc]initWithTitle:@"温馨提示" andMessage:@"抱歉，该视频不存在或已下架" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
                
                
                customView.tag = 1008;
                [customView show];

                return;
                
                
            }
                break;
                
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:0];
                return;
            }
                
                break;
                
            case -3:
            {
                [AlertLabel AlertInfoWithText:@"获取数据失败" andWith:self.view withLocationTag:0];
                
            }
                break;
                
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:self.view withLocationTag:0];

                return;
            }
                break;
                
            default:
                break;
        }
        
    }
    
    //点赞吐槽查询接口
    if ([type isEqualToString:VOD_PraiseDegradeDetail]) {
        NSDictionary *returnDiction = result;
        NSLog(@"点赞查询结果：%@",result);
        NSString *ret = [returnDiction objectForKey:@"ret"];
        NSString *type = [returnDiction objectForKey:@"type"];
        if ([ret isEqualToString:@"1"]) {
            if ([type isEqualToString:@"1"]) {
                _praiseDegradeType = PassedPraise;
                [_DBackScrollView.KNewIntrolView praiseSucceed:YES];
                [_DBackScrollView.KNewIntrolView stampSucceed:NO];
                
            }
            
            if ([type isEqualToString:@"2"]) {
                _praiseDegradeType = PassedDegrade;
                [_DBackScrollView.KNewIntrolView praiseSucceed:NO];
                [_DBackScrollView.KNewIntrolView stampSucceed:YES];

            }
        }
        if ([ret isEqualToString:@"0"] || [ret isEqualToString:@"2"] || [ret isEqualToString:@"-9"]) {
            _praiseDegradeType = NotPraiseDegrade;
            [_DBackScrollView.KNewIntrolView praiseSucceed:NO];
            [_DBackScrollView.KNewIntrolView stampSucceed:NO];

        }
        
    }
    
    //询价接口
    if ([type isEqualToString:VOD_INQUIRY]) {
        
        NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        switch (ret) {
                
            case 0:
            {
                NSString *moneyString;
                if ([[result objectForKey:@"amount"] length]<=0 || [[result objectForKey:@"amount"] isEqualToString:@"null"]) {
                    
                    moneyString = [self resetOrderMoneyString:@""];
                }else
                {
                    moneyString = [self resetOrderMoneyString:[result objectForKey:@"amount"]];
                }
                
                CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMoreMessage:[NSString stringWithFormat:@"本影片为付费影片!\n%@,您确定要付费吗？",moneyString] andButtonNum:2 withDelegate:self];
                cusAlert.tag = 77;
                [cusAlert show];
                
                
                
                
            }
                break;
            case -1:
            {
                [AlertLabel AlertInfoWithText:@"询价失败" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"询价失败" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                
            }
                break;
                
                
            default:
                break;
        }
        
        
        
    }
    
    
    
    
    //付费点播接口
    if ([type isEqualToString:VOD_PAY_PLAY]) {
        
        
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {
                [AlertLabel AlertInfoWithText:@"成功" andWith:_contentView withLocationTag:2];
                _playerLoginView.hidden = YES;
                _jxPlayerLoginView.hidden = YES;
                _orderView.hidden = YES;

                _isTapPlayBtn = YES;
                _playBackgroundIV.hidden = YES;
                _allCodeRateLink = @"";
                NSString *playlink = [result objectForKey:@"playLink"];
                self.currentURL = playlink;
                self.demandModel.contentPullID = @"";
                _isEPGPlayerEnter = NO;
                
                
                //                NSDictionary *dic = [[NSDictionary alloc]init];
                //                dic = [[UserPlayRecorderCenter defaultCenter] getVODListItemWithContentID:self.demandModel.contentID];
                //
                //                if (![dic valueForKey:@"breakPoint"]|| JumpStartTime ) {
                //                    JumpStartTime = 0;
                //
                //                }else
                //                {
                //
                //                    JumpStartTime = [[dic valueForKey:@"breakPoint"] intValue];
                //
                //                }
                
                
                
                
                //做一个判断，如果该断点就等于最后的时间，那么就默认为0，总不能一上来就让跳下个节目
                
                
                self.demandModel.fullstartTimeP = @"";
                self.demandModel.endTimeP = @"";
                self.demandModel.liveTitleP = @"";
                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:self.currentURL] andPlayTVType:PlayTVType];
                
                
                
            }
                break;
                
            case 1:
            {
                
                _jxPlayerBindView .hidden = NO;
                
                return;
//                [AlertLabel AlertInfoWithText:@"已付费成功" andWith:_contentView withLocationTag:2];
//                
//                _isTapPlayBtn = YES;
//                //                _playBackgroundIV.hidden = YES;
//                _orderView.hidden = YES;
//                _allCodeRateLink = @"";
//                NSString *playlink = [result objectForKey:@"playLink"];
//                self.currentURL = playlink;
//                self.demandModel.contentPullID = @"";
//                
//                
//                //                NSDictionary *dic = [[NSDictionary alloc]init];
//                //                dic = [[UserPlayRecorderCenter defaultCenter] getVODListItemWithContentID:self.demandModel.contentID];
//                //
//                //                if (![dic valueForKey:@"breakPoint"]) {
//                //                    JumpStartTime = 0;
//                //
//                //                }else
//                //                {
//                //
//                //                    JumpStartTime = [[dic valueForKey:@"breakPoint"] intValue];
//                //
//                //                }
//                
//                self.demandModel.fullstartTimeP = @"";
//                self.demandModel.endTimeP = @"";
//                self.demandModel.liveTitleP = @"";
//                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:self.currentURL] andPlayTVType:PlayTVType];
                
            }
                
                break;
                
                
            case -1:
            {
                [AlertLabel AlertInfoWithText:@"订购失败，请重试" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"订购失败，请重试" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -3:
            {
                [AlertLabel AlertInfoWithText:@"订购失败，请重试" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -5:
            {
                [AlertLabel AlertInfoWithText:@"订购失败，请重试" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -8:
            {
                [AlertLabel AlertInfoWithText:@"订购失败，请重试" andWith:_contentView withLocationTag:2];
                
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                
            }
                break;
                
                
            default:
                break;
        }
        
        
        
        
    }
    
    
    
    if ([type isEqualToString:VOD_STOP]) {
        int ret = [[result objectForKey:@"ret"] intValue];
        
        switch (ret) {
            case 0:
            {
                //                [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
                
            }
                break;
            case -1:
            {
                //                [AlertLabel AlertInfoWithText:@"当前节目不存在" andWith:self.view withLocationTag:1];
                
            }
                break;
            case -2:
            {
                //                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
                
            }
                break;
            case -9:
            {

                
            }
                break;
                
            default:
                break;
        }
        
        
        
    }
    
    if ([type isEqualToString:VOD_Praise_Degrade]) {
        
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {
                
                if (_isPraiseAction == YES) {
                    [AlertLabel AlertInfoWithText:@"点赞成功" andWith:self.view withLocationTag:1];
                    _praiseDegradeType = PassedPraise;
                    
                    NSInteger praiseNum = [[self handleStringWithString:_DBackScrollView.KNewIntrolView.praiseNumLabel.text] integerValue];
                    [_DBackScrollView.KNewIntrolView praiseSucceed:YES];
                    
                    [_DBackScrollView.KNewIntrolView.praiseNumLabel setText:[NSString stringWithFormat:@"(%ld)",(long)praiseNum+1]];
                    
                    if (_vodPlayType == VOdPlayVariety) {
                        //修改数据源的点赞吐槽量
                        NSMutableDictionary *itemDic = [NSMutableDictionary dictionaryWithDictionary:[_demandModel.seriesList objectAtIndex:_videoPlayPosition]];
                        [itemDic setObject:[NSString stringWithFormat:@"%ld",praiseNum+1] forKey:@"praiseCount"];
                        [_demandModel.seriesList replaceObjectAtIndex:_videoPlayPosition withObject:itemDic];

//                        [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] setObject:[NSString stringWithFormat:@"%ld",praiseNum+1] forKey:@"praiseCount"];
                    }else if(_vodPlayType == VodPlayTVSerial){
                        //修改数据源的点赞吐槽量
                        NSMutableDictionary *itemDic = [NSMutableDictionary dictionaryWithDictionary:[_demandModel.seriesList objectAtIndex:_videoPlayPosition]];
                        [itemDic setObject:[NSString stringWithFormat:@"%ld",praiseNum+1] forKey:@"praiseCount"];
                        [_demandModel.seriesList replaceObjectAtIndex:_videoPlayPosition withObject:itemDic];
//                        [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] setObject:[NSString stringWithFormat:@"%ld",praiseNum+1] forKey:@"praiseCount"];
                    }
                    
                }else
                {
                    
                    [AlertLabel AlertInfoWithText:@"吐槽成功" andWith:self.view withLocationTag:1];
                    _praiseDegradeType = PassedDegrade;
                    
                    NSInteger stampNum = [ [self handleStringWithString:_DBackScrollView.KNewIntrolView.stampNumLabel.text]  integerValue];
                    

                    [_DBackScrollView.KNewIntrolView stampSucceed:YES];
                    [_DBackScrollView.KNewIntrolView.stampNumLabel setText:[NSString stringWithFormat:@"(%ld)",(long)stampNum+1]];//点踩成功
                    
                    
                    if (_vodPlayType == VOdPlayVariety) {
                        //修改数据源的点赞吐槽量
                        NSMutableDictionary *itemDic = [NSMutableDictionary dictionaryWithDictionary:[_demandModel.seriesList objectAtIndex:_videoPlayPosition]];
                        [itemDic setObject:[NSString stringWithFormat:@"%ld",stampNum+1] forKey:@"degradeCount"];
                        [_demandModel.seriesList replaceObjectAtIndex:_videoPlayPosition withObject:itemDic];
                        
//                        [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] setObject:[NSString stringWithFormat:@"%ld",stampNum+1] forKey:@"degradeCount"];
                    }else if(_vodPlayType == VodPlayTVSerial){
                        //修改数据源的点赞吐槽量
                        NSMutableDictionary *itemDic = [NSMutableDictionary dictionaryWithDictionary:[_demandModel.seriesList objectAtIndex:_videoPlayPosition]];
                        [itemDic setObject:[NSString stringWithFormat:@"%ld",stampNum+1] forKey:@"degradeCount"];
                        [_demandModel.seriesList replaceObjectAtIndex:_videoPlayPosition withObject:itemDic];
//                        [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] setObject:[NSString stringWithFormat:@"%ld",stampNum+1] forKey:@"degradeCount"];
                    }
                    
                }
                
                
            }
                break;
            case 1:
            {
                //                if (_isPraiseAction == YES) {
                //
                //                    [AlertLabel AlertInfoWithText:@"已" andWith:self.view withLocationTag:1];
                //                }else
                //                {
                //                    [AlertLabel AlertInfoWithText:@"已吐槽" andWith:self.view withLocationTag:1];
                //
                //                }
                
                if (_praiseDegradeType == PassedPraise) {
                    [AlertLabel AlertInfoWithText:@"已点赞" andWith:self.view withLocationTag:1];
  
                }
                else if (_praiseDegradeType == PassedDegrade)
                {
                    [AlertLabel AlertInfoWithText:@"已吐槽" andWith:self.view withLocationTag:1];

                }
//                    else
//                {
//                    [AlertLabel AlertInfoWithText:@"为点赞吐槽" andWith:self.view withLocationTag:1];
//
//                }
//                
//                
//                [AlertLabel AlertInfoWithText:@"已点赞吐槽" andWith:self.view withLocationTag:1];
                
                
            }
                break;
            case -1:
            {
                //                if (_isPraiseAction == YES) {
                //
                //                    [AlertLabel AlertInfoWithText:@"点赞失败" andWith:self.view withLocationTag:1];
                //                }else
                //                {
                //                    [AlertLabel AlertInfoWithText:@"吐槽失败" andWith:self.view withLocationTag:1];
                //
                //                }
                
                [AlertLabel AlertInfoWithText:@"点赞吐槽失败" andWith:self.view withLocationTag:1];
                
                
            }
                break;
            case -2:
            {
                //                if (_isPraiseAction == YES) {
                //
                //                    [AlertLabel AlertInfoWithText:@"点赞超时" andWith:self.view withLocationTag:1];
                //                }else
                //                {
                //                    [AlertLabel AlertInfoWithText:@"吐槽超时" andWith:self.view withLocationTag:1];
                //
                //                }
                
                [AlertLabel AlertInfoWithText:@"登录超时" andWith:self.view withLocationTag:1];
                
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"有异常" andWith:self.view withLocationTag:1];
            }
                break;
                
                
            default:
                break;
        }
        
        
    }
    
    
    if ([type isEqualToString:VOD_ASSCCIATION]) {
        
        if ([result objectForKey:@"count"]>0) {
            
            NSMutableArray *asscciationArray = [[NSMutableArray alloc]init];
            asscciationArray = [result objectForKey:@"vodList"];
            if (asscciationArray.count>0) {
                
                _DBackScrollView.recommandView.hidden = NO;
                _asscciationArr = asscciationArray;
                if (_vodPlayType == VODPlayMovie) {
                     _videoListArr =[NSMutableArray arrayWithArray:_asscciationArr];
                }
                
//                if ([self.demandModel.seriesList count] <=0) {
//                    
//                    _videoListArr =[NSMutableArray arrayWithArray:_asscciationArr];
//                }
                
                [_DBackScrollView recommandViewLoadeRecommandItems:asscciationArray];
                
//                [_videoListTV reloadData];
//                return;
            }else{
                _DBackScrollView.recommandView.hidden = YES;
                
                [AlertLabel AlertInfoWithText:@"暂无推荐节目" andWith:_contentView withLocationTag:2];
                return;
            }
            
            
            
        }
        
        
    }
    
    
    
    if ([type isEqualToString:VOD_PLAY]) {
       

        int ret = [[[result objectForKey:@"playDic"] objectForKey:@"ret"] intValue];
        [clearTitles removeAllObjects];
        [self allCodeClearButtonTitle];
        /*
         1、做一个点赞查询
         2、赋值  if 电视剧 把分集点赞吐槽量赋值   if电影 点赞吐槽量直接赋值
         （3、点播详情介绍赋值）
         */
        
//-------------赋值
        if (_vodPlayType == VOdPlayVariety) {
        //点赞查询下  然后 点赞量赋值
            
            HTRequest *praiseRequest = [[HTRequest alloc] initWithDelegate:self];
            [praiseRequest VODPraiseDegradeDetailWithContentID:_demandModel.contentID andTimeout:5];
            
            NSString *praiseNum = [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"praiseCount"];
            NSString *stampNum = [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"degradeCount"];
            _DBackScrollView.KNewIntrolView.praiseNumLabel.text = [NSString stringWithFormat:@"(%@)",praiseNum];
            _DBackScrollView.KNewIntrolView.stampNumLabel.text = [NSString stringWithFormat:@"(%@)",stampNum];
        }else if(_vodPlayType == VodPlayTVSerial){
            HTRequest *praiseRequest = [[HTRequest alloc] initWithDelegate:self];
            [praiseRequest VODPraiseDegradeDetailWithContentID:_demandModel.contentID andTimeout:5];
            
            NSString *praiseNum = [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"praiseCount"];
            NSString *stampNum = [[_demandModel.seriesList objectAtIndex:_videoPlayPosition] objectForKey:@"degradeCount"];
            _DBackScrollView.KNewIntrolView.praiseNumLabel.text = [NSString stringWithFormat:@"(%@)",praiseNum];
            _DBackScrollView.KNewIntrolView.stampNumLabel.text = [NSString stringWithFormat:@"(%@)",stampNum];
        
        }
        else{
        
        
        }
        
        if (ret!=1) {
            
            _orderView.hidden = YES;
            
        }
        
        if (ret !=0) {
            if (USER_ACTION_COLLECTION == YES) {
                
                _isCollectionInfo = NO;

            }
        }
        
        switch (ret) {
            case 0:
            {
                
                
                NSLog(@"******加广告的鉴权%@",result);
                
                if ([[[result objectForKey:@"adDic"] objectForKey:@"count"] integerValue]>0) {
                    _adPlayerView.timeLabel.text = @"";

                        if ([[[result objectForKey:@"adDic"] objectForKey:@"adList"] count]>0) {
                        
                        self.demandModel.adList = [[result objectForKey:@"adDic"] objectForKey:@"adList"];
                            
                            NSLog(@"hhhhh%@",self.demandModel.adList);
                        NSDictionary *ADdic = [NSDictionary dictionaryWithDictionary:[self.demandModel.adList objectAtIndex:0]] ;
                        if ([[ADdic objectForKey:@"pictureRes"] allValues]>0) {
                            _adPlayerView.timeLabel.text = @"";

                            [_adPlayerView setMaskViewType:ImageAD];;
                            _isHaveAD = YES;
                            _adPlayerView.hidden = NO;
                            _adPlayerView.rotateBtn.hidden = NO;

                        }
                       else if ([[ADdic objectForKey:@"videoRes"] allValues]>0)
                        {
                            _adPlayerView.timeLabel.text = @"";
                            [_adPlayerView setMaskViewType:VideoAD];
                            self.adVideoDetailURL = [[[self.demandModel.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"urlLink"];
                            
                           
                           self.adVideoURL = [[[self.demandModel.adList objectAtIndex:0] valueForKey:@"videoRes"] objectForKey:@"videoLink"];
                            if (self.adVideoURL.length>0) {
                                NSLog(@"是有广告视频链接的");
                                _isHaveAD = YES;
                                _adPlayerView.hidden = NO;
                                _adPlayerView.timeLabel.text = @"";
                                _adPlayerView.rotateBtn.hidden = NO;

                            }else
                            {
                                _isHaveAD = NO;
                                _adPlayerView.hidden = YES;
                                _adPlayerView.rotateBtn.hidden = YES;

                            NSLog(@"是没有广告视频链接的");
                            }
                        }else
                        {
                            _isHaveAD = NO;
                            _adPlayerView.hidden = YES;
                            _adPlayerView.rotateBtn.hidden = YES;

                        }
                        
                    }
                    
//                    _isHaveAD = YES;
//                    _adPlayerView.hidden = NO;
                    
                }else
                {
                    _isHaveAD = NO;
                    
                    _adPlayerView.hidden = YES;
                    _adPlayerView.rotateBtn.hidden = YES;
                }

                
                if (_isHaveAD == YES) {
                    _returnMainBtn.alpha = 0;
                   
                }else{
                    _returnMainBtn.alpha = 1;
                }
                
//                if (_isVerticalDirection == NO) {
//                    [self setBigScreenPlayer];
//                }else
//                {
//                    [self setSmallScreenPlayer];
//                }
                
                _playerLoginView.hidden = YES;
                _jxPlayerLoginView.hidden = YES;
                _jxPlayerBindView.hidden = YES;
                _installMaskView.hidden = YES;
                _orderView.hidden = YES;
                _ZhuanWangMaskView.hidden = YES;
                _isTapPlayBtn = YES;
                _playBackgroundIV.hidden = YES;
                _allCodeRateLink = @"";
                NSString *playlink = [[result objectForKey:@"playDic"] objectForKey:@"playLink"];
                _isEPGPlayerEnter = NO;
                self.currentURL = playlink;
              
                
                
                //由于拉屏和点播全都走这个地，所以拉过来的点播的断点暂时用负值加以区分
                
                if (_isVODEX == YES) {
                    
                    _isVODEX = NO;
                }else
                {
                    self.demandModel.contentPullID = @"";
                    
                }
                
                self.demandModel.fullstartTimeP = @"";
                self.demandModel.endTimeP = @"";
                self.demandModel.liveTitleP = @"";
                
                
                
                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:self.currentURL] andPlayTVType:PlayTVType];
                
                
            }
                break;
            case 1:
            {   _ZhuanWangMaskView.hidden = YES;
                _adPlayerView.hidden = YES;
                _orderView.hidden =NO;
                [[self.view viewWithTag:4040] setHidden:YES];
                
                [self clearMoviePlayerStop];

                if (IsLogin) {
                    
                    [AlertLabel AlertInfoWithText:@"该视频暂不能播放" andWith:_contentView withLocationTag:2];
                    
                }else
                {
                    if (IS_PERMISSION_GROUP) {
                        
                        _playerLoginView.hidden = NO;
                        _jxPlayerLoginView.hidden = NO;
                        _isEPGPlayerEnter = YES;
                        
                    }else
                    {
                        if (isAuthCodeLogin) {
                            _isPaused = YES;
                            [self actionPauseForVideo];
                            [self changeLoginVerticalDirection];
                            NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                            [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                            [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                            
                            [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                            UserLoginViewController *user = [[UserLoginViewController alloc] init];
                            user.loginType = kVideoLogin;
                            [self.view addSubview:user.view];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                            user.backBlock = ^()
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                [self actionPlayForVideo];
                            };
                            
                            user.tokenBlock = ^(NSString *token)
                            {
                                [[UIApplication sharedApplication]setStatusBarHidden:NO];
                                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                                _isPaused = NO;
                                [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                                _isEPGPlayerEnter = YES;
                                [self loginAfterEnterVideo];
                                [self actionPlayForVideo];
                            };
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            return;
                        }else
                        {
                            [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                            
                            
                        }
                    }
                }
                
             
               
            }
                break;
            case -1:
            {    _ZhuanWangMaskView.hidden = YES;
                _adPlayerView.hidden = YES;
                _polyLoadingView.hidden = YES;
                
                CustomAlertView *customView = [[CustomAlertView alloc]initWithTitle:@"温馨提示" andMessage:@"抱歉，该视频不存在或已下架" andButtonNum:1 andCancelButtonMessage:@"确定" andOtherButtonMessage:nil withDelegate:self];
                
                
                customView.tag = 1008;
                [customView show];
//                [AlertLabel AlertInfoWithText:@"视频不存在或者视频已下架" andWith:_contentView withLocationTag:2];
                [self clearMoviePlayerStop];
                [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                [[self.view viewWithTag:4040] setHidden:YES];

//                _isTapPlayBtn = YES;
//                self.isPlay = NO;
//                _playBackgroundIV.hidden = NO;
//                _videoProgressV.value = 0;
                
                return;
            }
                break;
            case -2:
            {    _ZhuanWangMaskView.hidden = YES;
                _adPlayerView.hidden = YES;
                _polyLoadingView.hidden = YES;
                
                
                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
                [self clearMoviePlayerStop];
                [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                [[self.view viewWithTag:4040] setHidden:YES];

                
                return;
                
            }
                break;
            case -4:   //  cap   添加专网的提示问题
            {
//                [AlertLabel AlertInfoWithText:@"接入广电网络才可观看" andWith:_contentView withLocationTag:2];
                [self clearMoviePlayerStop];
//                [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                [[self.view viewWithTag:4040] setHidden:YES];
                _ZhuanWangMaskView.hidden = NO;
                _adPlayerView.hidden = YES;
                _polyLoadingView.hidden = YES;

                //                _isTapPlayBtn = YES;
                //                self.isPlay = NO;
                //                _playBackgroundIV.hidden = NO;
                //                _videoProgressV.value = 0;
                
                return;
                
            }
                break;
            case -5:
            {
                _ZhuanWangMaskView.hidden = YES;
                _adPlayerView.hidden = YES;
                [AlertLabel AlertInfoWithText:@"视频对应此终端无播放链接" andWith:_contentView withLocationTag:2];
                [self clearMoviePlayerStop];
                _polyLoadingView.hidden = YES;
                [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                [[self.view viewWithTag:4040] setHidden:YES];

//                _isTapPlayBtn = YES;
//                self.isPlay = NO;
//                _playBackgroundIV.hidden = NO;
//                _videoProgressV.value = 0;
                
                return;
                
            }
                break;
            case -9:
            {
                _ZhuanWangMaskView.hidden = YES;
                _adPlayerView.hidden = YES;
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
                _polyLoadingView.hidden = YES;
                [self clearMoviePlayerStop];
                [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
                [[self.view viewWithTag:4040] setHidden:YES];

//                _isTapPlayBtn = YES;
//                self.isPlay = NO;
//                _videoProgressV.value = 0;
//                _playBackgroundIV.hidden = NO;
                return;
            }
                break;
                
            default:
                break;
        }
        
        
        
    }
    
    
    if ([type isEqualToString:ADD_VOD_FAVORITE]) {
        
        int ret = [[result objectForKey:@"ret"] intValue];
        
        if (ret == 0) {
            [AlertLabel AlertInfoWithText:@"收藏成功" andWith:self.view withLocationTag:1];
//            [self favorSucceed:YES];
            [_moviePlayerBottomView resetFavorate:YES];
            _favourButton.selected = YES;
            _favourLabel.selected = YES;
            [_favourLabel setTitle:@"已收藏" forState:UIControlStateNormal];
            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
            //            _favourButton.enabled = YES;
//            [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_DEMAND_LIST object:nil];
            //                    [[NSNotification def]postNotificationName:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];

            
//            [LocalCollectAndPlayRecorder addVODFavoriteWithSeq:@"" andContentID:_demandModel.CONTENTID andName:_demandModel.NAME andSubTitle:_demandModel.name andFlagPlayOuter:@"" andDefinition:@"" andType:_demandModel.multipleType andIsOffShelves:@"" andLastEpisode:_demandModel.lastPlayEpisode andLastEpisodeTitle:_demandModel.name andAmount:[NSString stringWithFormat:@"%ld",(long)_demandModel.Amount] andImageLink:_demandModel.imageLink andStyle:@"" andUserType:IsLogin andKey:@"contentID"];
            [_localListCenter addVODFavoriteWithSeq:@"" andContentID:_demandModel.CONTENTID andName:_demandModel.NAME andSubTitle:_demandModel.name andFlagPlayOuter:@"" andDefinition:@"" andType:_demandModel.multipleType andIsOffShelves:@"" andLastEpisode:_demandModel.lastPlayEpisode andLastEpisodeTitle:_demandModel.name andAmount:[NSString stringWithFormat:@"%ld",(long)_demandModel.Amount] andImageLink:_demandModel.imageLink andStyle:@"" andUserType:IsLogin andKey:@"contentID"];
        }
        
        else if (ret == 1){
            if (IsLogin) {
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                
                [request DELVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
                
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }
                
            }
            
            
        }else
        {
            
            if (IsLogin) {
                [AlertLabel AlertInfoWithText:@"添加收藏失败 请重试" andWith:self.view withLocationTag:1];
                
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }
                
            }
        }

    
        
    }
    
    if ([type isEqualToString:DEL_VOD_FAVORITE]) {
        int ret = [[result objectForKey:@"ret"] intValue];
        
        if (ret == 0) {
            //                    [FavoriteAlertView showOtherAlertViewWithText:@"取消收藏节目成功" inView:_contentView andWidth:KDeviceHeight];
            [AlertLabel AlertInfoWithText:@"取消收藏成功" andWith:self.view withLocationTag:1];
            [_favourLabel setTitle:@"收藏" forState:UIControlStateNormal];

            //            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
            //            _favourButton.enabled = YES;
//            [self favorSucceed:NO];
            _favourButton.selected = NO;
            _favourLabel.selected = NO;
            [_moviePlayerBottomView resetFavorate:NO];
            [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
           
            

//            [LocalCollectAndPlayRecorder singleDeleteVODFavoriteWithDeleteID:self.demandModel.CONTENTID andUserType:IsLogin andKey:@"contentID"];
            [_localListCenter singleDeleteVODFavoriteWithDeleteID:self.demandModel.CONTENTID andUserType:IsLogin andKey:@"contentID"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_FAVORITE_DEMAND_LIST object:nil];
        }
        else if (ret == 1)
        {
            if (IsLogin) {
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                [request ADDVODFavoryteWithContentID:self.demandModel.CONTENTID andTimeout:kTIME_TEN];
  
            }else{
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }

            }
            
        }else
        {
            if (IsLogin) {
                [AlertLabel AlertInfoWithText:@"取消收藏失败 请重试" andWith:self.view withLocationTag:1];
                
            }else
            {
                if (isAuthCodeLogin) {
                    _isPaused = YES;
                    [self actionPauseForVideo];
                    [self changeLoginVerticalDirection];
                    NSMutableDictionary *liveDic = [[NSMutableDictionary alloc] init];
                    [liveDic setObject:self.demandModel.contentID forKey:@"contentID"];
                    [liveDic setObject:self.demandModel.NAME forKey:@"demandName"];
                    
                    [DataPlist WriteToPlistDictionary:liveDic ToFile:VODINFOPLIST];
                    UserLoginViewController *user = [[UserLoginViewController alloc] init];
                    user.loginType = kVideoLogin;
                    [self.view addSubview:user.view];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
                    user.backBlock = ^()
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        [self actionPlayForVideo];
                    };
                    
                    user.tokenBlock = ^(NSString *token)
                    {
                        [[UIApplication sharedApplication]setStatusBarHidden:NO];
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                        _isPaused = NO;
                        [AlertLabel AlertInfoWithText:@"您已登录成功!" andWith:self.view withLocationTag:1];                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
                        _isEPGPlayerEnter = YES;
                        [self loginAfterEnterVideo];
                        [self actionPlayForVideo];
                    };
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    return;
                }else
                {
                    [AlertLabel AlertInfoWithText:@"请先登录!" andWith:self.view withLocationTag:1];
                    
                    
                }

                
            }

        }
        
    }
    
    if ([type isEqualToString:EPG_TVPLAYEX]) {
        int ret = [[result objectForKey:@"ret"] intValue];
    
        switch (ret) {
            case 0:
            {
                _allCodeRateLink = @"";
                
                //                _isNowPlayLiveTV = YES;
                //                _currentURL = [[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"];
                //                [self playVideoModeWithURL:[NSURL URLWithString:[[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                //                self.serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[[result valueForKey:@"serviceinfo"] valueForKey:@"tvName"]];
                
                _currentURL =  [[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"];
                
                
                if (![[result valueForKey:@"serviceinfo"] valueForKey:@"tvName"]) {
                    _serviceTitleLabel.text = @"";
                }
                _serviceTitleLabel.text = [NSString stringWithFormat:@"%@",[[result valueForKey:@"serviceinfo"] valueForKey:@"tvName"]];
                self.demandModel.fullstartTimeP = @"";
                self.demandModel.endTimeP = @"";
                self.demandModel.contentPullID = @"";
                
                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:[[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                
                
                
            }
                break;
            case -1:
            {
                [AlertLabel AlertInfoWithText:@"当前频道不存在" andWith:self.view withLocationTag:1];
            }
                break;
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
            }
                break;
            case -3:
            {
                [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:self.view withLocationTag:1];
            }
                break;
            case -5:
            {
                [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:1];
            }
                break;
            case -8:
            {
                [AlertLabel AlertInfoWithText:@"鉴权成功待付费" andWith:self.view withLocationTag:1];
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:self.view withLocationTag:1];
            }
                break;
                
                
            default:
                break;
        }
        
        
        
    }else if ([type isEqualToString:EPG_EVENTPLAYEX])
    {
        int ret = [[result objectForKey:@"ret"] intValue];
        
        switch (ret) {
            case 0:
            {
                _allCodeRateLink = @"";
                self.demandModel.liveTitleP = @"";
                //BOBO清理记录直播和时移的布尔值
                //                _isNowPlayLiveTV = NO;
                //                        _isTimeMove = NO;
                
                //end
                self.demandModel.contentPullID = @"";
                self.demandModel.liveTitleP = @"";
                _currentURL =  [[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"];
                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:[[result valueForKey:@"serviceinfo"] valueForKey:@"playLink"]] andPlayTVType:PlayTVType];
                
            }
                break;
                
            case -1:
            {
                [AlertLabel AlertInfoWithText:@"当前频道不存在" andWith:self.view withLocationTag:1];
            }
                break;
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:self.view withLocationTag:1];
            }
                break;
            case -3:
            {
                [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:self.view withLocationTag:1];
            }
                break;
            case -5:
            {
                [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:self.view withLocationTag:1];
            }
                break;
            case -8:
            {
                [AlertLabel AlertInfoWithText:@"鉴权成功待付费" andWith:self.view withLocationTag:1];
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:self.view withLocationTag:1];
            }
                break;
                
                
            default:
                break;
        }
        
        
        
    }
    else if ([type isEqualToString:EPG_PAUSETVPLAYEX])
    {
        NSInteger ret = [[result objectForKey:@"ret"] integerValue];
        
        if (ret !=0) {
            {
                [[self.view viewWithTag:4040] setHidden:YES];
                
            }
        }
        switch (ret) {
            case 0:
            {
                _allCodeRateLink = @"";
                
                //                        [AlertLabel AlertInfoWithText:@"即将切换进入时移状态" andWith:_contentView withLocationTag:2];
                _videoProgressV.value = 1000;
                _playButton.hidden = NO;
                //                _currentPlayType = CurrentPlayTimeMove;
                //                [self showGestureGuidView];
                
                NSLog(@"HHJH***********%@",result);
                NSString *channelURLStr = [NSString stringWithFormat:@"%@",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                
                NSLog(@"timemove str = %@",channelURLStr);
                _demandModel.contentPullID = [[result objectForKey:@"serviceinfo"] objectForKey:@"id"];
                _demandModel.name = [[result objectForKey:@"serviceinfo"]objectForKey:@"name"];
                _currentPlayType = CurrentPlayTimeMove;
                //                _theVideoInfo.fullStartTimeP = @"";
                //                _theVideoInfo.endTimeP = @"";
                //                _theVideoInfo.contentIDP = @"";
                //                //                    _isNowPlayLiveTV = YES;
                _currentPlayType = CurrentPlayTimeMove;
                //                _videoProgressV.value = 1000;
                
                _currentURL = [NSString stringWithFormat:@"%@",[[result objectForKey:@"serviceinfo"] objectForKey:@"playLink"]];
                //                _currentTimeL.text = liveStartTime;
                //                _totalTimeL.text = liveEndTime;
                [self playBackChannelVideoModoWithURL:[NSURL URLWithString:_currentURL] andPlayTVType:PlayTVType];
                [self StartNewTimer];
                
                
            }
                break;
            case -1:
            {
                [AlertLabel AlertInfoWithText:@"当前频道不存在" andWith:_contentView withLocationTag:2];
            }
                break;
            case -2:
            {
                [AlertLabel AlertInfoWithText:@"用户未登录或者登录超时" andWith:_contentView withLocationTag:2];
            }
                break;
            case -3:
            {
                [AlertLabel AlertInfoWithText:@"鉴权失败" andWith:_contentView withLocationTag:2];
            }
                break;
                
            case -5:
            {
                [AlertLabel AlertInfoWithText:@"频道对应此终端无播放链接" andWith:_contentView withLocationTag:2];
            }
                break;
            case -7:
            {
                [AlertLabel AlertInfoWithText:@"该频道不提供时移功能" andWith:_contentView withLocationTag:2];
            }
                break;
            case -9:
            {
                [AlertLabel AlertInfoWithText:@"连接失败 请换一个节目试试" andWith:_contentView withLocationTag:2];
            }
                break;
                
            default:
                break;
        }
        
    }
    
    
    
    
    
}




-(NSString *)handleStringWithString:(NSString *)str{
    
    NSString *mstr;
    NSMutableString *mKStr = [NSMutableString stringWithString:str];
    
    
    if ([str rangeOfString:@"("].location !=NSNotFound ) {
        if ([[mKStr componentsSeparatedByString:@"("] count]>0) {
            
            NSMutableString *nKstr = [NSMutableString stringWithString:[[mKStr componentsSeparatedByString:@"("] objectAtIndex:1]];
            
            
            if ([[nKstr componentsSeparatedByString:@")"] count]>0) {
                mstr = [[nKstr componentsSeparatedByString:@")"] objectAtIndex:0];
            }
            
        }
    }
    
    if (mstr.length==0) {
        mstr = @"0";
    }
    return mstr;
}



#pragma 停掉播放器，清空播放器

-(void)clearMoviePlayerStop
{
    _playPauseBigBtn.hidden = YES;
    _ZhuanWangMaskView.hidden = YES;
    [_adPlayerView.CountTimer invalidate];
    _adPlayerView.CountTimer = nil;
    [_moviePlayer stop];
    [_moviePlayer setContentURL:nil];
    [self.myNewTimer invalidate];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//                                                  object:nil];
    
}


#pragma mark -播放器的特殊情况问题

// 从后台进入前台后，恢复视频的状态
- (void)replayTheVideo
{
    if (_isPaused) {
        [_moviePlayer pause];
        //        _playBackgroundIV.hidden = NO;
        
    }else{
        [_moviePlayer play];
        _playBackgroundIV.hidden = YES;
        
        
    }
}


-(void)WillResignActive:(NSNotification*)notification
{
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        _adPlayerView.isCanContinu = NO;

        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        _playPauseBigBtn.hidden = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
//        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        _adPlayerView.isCanContinu = NO;

        [_moviePlayer pause];//暂停视频

        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
//        self.isPlay = NO;//改变暂停标记

//        _playPauseBigBtn.hidden = NO;
    }
 
    
//    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
//    _isPaused = YES;//改变暂停标记
//    [_moviePlayer pause];//暂停视频
//    _playPauseBigBtn.hidden = NO;
    
    
    
}

-(void)DidBecomeActive:(NSNotification*)notification
{
//    self.isPlay = YES;
////    [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
//    _playPauseBigBtn.hidden = YES;
//    [_moviePlayer play];
//    [se]
//    [self.myNewTimer invalidate];
    
//    if (_isPaused) {
//        //----改变按钮图标
//        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
//        //----上面是改变按钮图标
//        _isPaused = NO;//更换暂停标记
//        _playPauseBigBtn.hidden = YES;
//        [_moviePlayer play];
//        
//    }
    
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        _adPlayerView.isCanContinu = NO;

        _playPauseBigBtn.hidden = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
        //        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
        
        
        if (_isADDetailEnter == NO) {
            _adPlayerView.isCanContinu = YES;
            
            //        _isPaused = YES;//改变暂停标记
            [_moviePlayer play];
        }
       //暂停视频
        //        _playPauseBigBtn.hidden = NO;
    }

    
    
    
}



-(void)actionPauseForVideo
{
    if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
        _adPlayerView.isCanContinu = NO;
        
        self.isPlay = NO;
        [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        _playPauseBigBtn.hidden = NO;
        [_moviePlayer pause];
        [self.myNewTimer invalidate];
        //        324 俩个的播放按钮都要变化 smallBottomBarIV
        
    }else
    {
        _adPlayerView.isCanContinu = NO;
        
        [_moviePlayer pause];//暂停视频
        
        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
        //        self.isPlay = NO;//改变暂停标记
        
        //        _playPauseBigBtn.hidden = NO;
    }
    
    
    //    [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
    //    _isPaused = YES;//改变暂停标记
    //    [_moviePlayer pause];//暂停视频
    //    _playPauseBigBtn.hidden = NO;
    

}



-(void)actionPlayForVideo
{
    //    self.isPlay = YES;
    ////    [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
    //    _playPauseBigBtn.hidden = YES;
    //    [_moviePlayer play];
    //    [se]
    //    [self.myNewTimer invalidate];
    
    //    if (_isPaused) {
    //        //----改变按钮图标
    //        [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
    //        //----上面是改变按钮图标
    //        _isPaused = NO;//更换暂停标记
    //        _playPauseBigBtn.hidden = YES;
    //        [_moviePlayer play];
    //
    //    }
    
    
        
        if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
            self.isPlay = NO;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            _adPlayerView.isCanContinu = NO;
            
            _playPauseBigBtn.hidden = NO;
            [_moviePlayer pause];
            [self.myNewTimer invalidate];
            //        324 俩个的播放按钮都要变化 smallBottomBarIV
            
        }else
        {
            [_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            
            
            if (_isADDetailEnter == NO) {
                _adPlayerView.isCanContinu = YES;
                
                //        _isPaused = YES;//改变暂停标记
                [_moviePlayer play];
            }
            //暂停视频
                    _playPauseBigBtn.hidden = YES;
        }
   
    
   
    
    
 
}






#pragma mark -

#pragma mark ======   MPMoviePlayerPlaybackStateDidChangeNotification ====
- (void) moviePlayBackStateDidChange:(NSNotification*)notification
{
    
    MPMoviePlayerController *player = notification.object;
    
    /* Playback is currently stopped. */
    if (player.playbackState == MPMoviePlaybackStateStopped)
    {
//        [[self.view viewWithTag:4040] setHidden:NO];
        
        NSLog(@"stopped");
        [self clearMoviePlayerStop];
       
    }
    /*  Playback is currently under way. */
    else if (player.playbackState == MPMoviePlaybackStatePlaying)
    {
        //        if (self.myNewTimer) {
        //            [self.myNewTimer invalidate];
        //        }
        if (_isHaveAD == YES) {

            return;
        }else
        {
            
            
            [[self.view viewWithTag:4040] setHidden:YES];
            [self StartNewTimer];
            //        [_moviePlayer prepareToPlay];
            NSLog(@"playing");
            self.isPlay = YES;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            //324 俩个的播放按钮都要变化 smallBottomBarIV
            [(UIButton *)smallbtn_stop setImage:[UIImage imageNamed:@"btn_player_controller_pause_normal.png"] forState:UIControlStateNormal];
            _isPaused = NO;
            //
//            endDate = [SystermTimeControl GetSystemTimeWith];
//            
//            firstPeriod = [DTTimePeriod timePeriodWithStartDate:startDate endDate:endDate];
//            
//            ZTTimeInterval +=firstPeriod.durationInSeconds;
            [_timeManager start];
            
            _isCollectionInfo = YES;
            
            [self hiddenPanlwithBool:NO];
        }

        
        
    }
    /* Playback is currently paused. */
    else if (player.playbackState == MPMoviePlaybackStatePaused)
    {
        
        
        
        
        // 创建时间段
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat: @"YYYY-MM-dd HH:mm:ss"];
        
        NSLog(@"paused");
        //513  改完还没有试过，流有问题
        
        if (_isHaveAD == YES) {
            return;
        }else
        {
        
        
        if (_isPaused) { //如果是手点播放暂停的话再换图，否则就是系统自动暂停，是卡的，那么直接给播放状态就好了
            self.isPlay = NO;
            [(UIButton *)_playButton setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            //324 俩个的播放按钮都要变化 smallBottomBarIV
            [(UIButton *)smallbtn_stop setImage:[UIImage imageNamed:@"btn_player_controller_play_normal.png"] forState:UIControlStateNormal];
            _playPauseBigBtn.hidden = NO;
//            startDate = [SystermTimeControl GetSystemTimeWith];
            if (_timeManager.counting) {
                [_timeManager pause];
            }
            
        }
        else{
            
            if (kcurrentTimeDemand != ktotalTimeDemand) {
                if (_moviePlayer.loadState == 0) {
                    [_moviePlayer play];
                    
                }else if (_moviePlayer.loadState == 3)
                {
                    [_moviePlayer pause];
                    _isPlayEnd = NO;
                    
                }else{
                    
                        _isPlayEnd = NO;
                    [_moviePlayer setCurrentPlaybackTime:ktotalTimeDemand];
                }
            }else
            {
//                [_moviePlayer prepareToPlay];
//                [_moviePlayer play];
                
//                if (_moviePlayer.loadState == 3) {
//                    
//                    _isPlayEnd = NO;
//                    _isPaused = NO;
//                    _playPauseBigBtn.hidden = YES;
//                    [_moviePlayer pause];
//
//                }else
//                {
////                    if (ktotalTimeDemand>0) {
////                        _isPlayEnd = YES;
////                        _playPauseBigBtn.hidden = YES;
//                        [_moviePlayer play];
//                    [self StartNewTimer];
////                    }
//                    
//
//                }
               
                
//                [[self.view viewWithTag:4040] setHidden:NO];
               
            }
            //                    [_moviePlayer prepareToPlay];
            //        #warning 这里要检查下，那中自动暂停发生时候，播放器到底是不是暂停的，如果是那么play，如果不是那么不需要play
            //                    [_moviePlayer play];
            [self StartNewTimer];
        }
        }
        
        
    }
    /* Playback is temporarily interrupted, perhaps because the buffer
     ran out of content. */
    else if (player.playbackState == MPMoviePlaybackStateInterrupted)
    {
        NSLog(@"interrupted");
        // 当网络状态不好，请求不到视频切片时
        [[self.view viewWithTag:4040] setHidden:NO];
        [_moviePlayer prepareToPlay];
        if (USER_ACTION_COLLECTION == YES) {
            
            [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_demandModel.NAME andOPSCode:PlayerAVError andOPSST:[SystermTimeControl getNowServiceTimeString]];
           
        }
        
    }
}

#pragma mark ======   MPMediaPlaybackIsPreparedToPlayDidChangeNotification ====
- (void) mediaIsPreparedToPlayDidChange:(NSNotification*)notification
{
    NSLog(@"Now Begin to play!");
    
    [[self.view viewWithTag:4040] setHidden:YES];
    _errorManager.isAVLoading = NO;
    //广告继续播放
    //8 这里应该加入断点播放啊亲

    if (_isHaveAD == YES && _adPlayerView.maskViewType == VideoAD) {
        
        
        if ([[[_adPlayerView.timeLabel.text componentsSeparatedByString:@" "] lastObject] integerValue]<=0) {
            //            if (self.adImageHiddenSecend) {
            //                self.adImageHiddenSecend = 0;
            //                _adPlayerView.timeLabel.text = @"";
            //            }
            _adPlayerView.timeLabel.text = @"";
            _adVideoHiddenSecend = (NSInteger)_moviePlayer.duration;
            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
        }else
        {
            
            return;
        }
        
        
        
        
    }else
    {
    self.isPlay = YES;
    
    if (_isPrepareToPlay) {
        
        if (((NSInteger)_moviePlayer.duration - JumpStartTime)<10) {
            JumpStartTime = 0;
        }
        [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
        _isPrepareToPlay = NO;
        
//        [self ChangeCurTimeForNewPort];
        
    }else{
        
        _isPrepareToPlay = YES;
        //         [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
        
    }
    
    [self StartNewTimer];

    }
}

#pragma mark ======   MPMoviePlayerLoadStateDidChangeNotification ====
- (void) loadStateDidChange:(NSNotification*)notification
{
    MPMoviePlayerController *player = notification.object;
    MPMovieLoadState loadState = player.loadState;
    
    /* The load state is not known at this time. */
    if (loadState & MPMovieLoadStateUnknown)
    {
        NSLog(@"Movie is unknow");
        [[self.view viewWithTag:4040] setHidden:NO];
        _polyLoadingView.hidden = YES;
        _errorManager.isAVLoading = YES;

    }
    
    /* The buffer has enough data that playback can begin, but it
     may run out of data before playback finishes. */
    if (loadState & MPMovieLoadStatePlayable)
    {
        //        [_moviePlayer pause];
        //        _isPlay = NO;
        NSLog(@"Movie is playable");
        
        _errorManager.endErrorDate = [SystermTimeControl getNowServiceTimeDate];
        
        [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_demandModel.NAME andOPSCode:PlayerOutTimeError andOPSST:[SystermTimeControl getNowServiceTimeString]];
        
        _errorManager.isAVLoading = YES;
        _polyLoadingView.hidden = YES;
        [[self.view viewWithTag:4040] setHidden:NO];
        if (_isHaveAD == YES && _adPlayerView.maskViewType == VideoAD) {
            
            
            //            if (self.adImageHiddenSecend) {
            //                self.adImageHiddenSecend = 0;
            //                _adPlayerView.timeLabel.text = @"";
            //            }
            _adPlayerView.timeLabel.text = @"";
            _adVideoHiddenSecend = (NSInteger)_moviePlayer.duration;
            [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
            [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];
            
            
            
            
            
        }
//        _adPlayerView.isCanContinu = NO;
//        [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
//        [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];

        
    }
    
    /* Enough data has been buffered for playback to continue uninterrupted. */
    if (loadState & MPMovieLoadStatePlaythroughOK)
    {
        //        _isPlay = NO;
        //        [_moviePlayer pause];
        _errorManager.isAVLoading = NO;
        NSLog(@"Movie is start!/resumed!");
        [[self.view viewWithTag:4040] setHidden:YES];
        _polyLoadingView.hidden = YES;

//        _adPlayerView.isCanContinu = YES;
//        [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
//        [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];

        _errorMaskView.hidden = YES;
        
    }
    
    /* The buffering of data has stalled. */
    if (loadState & MPMovieLoadStateStalled)
    {
        //        _isPlay = YES;
        //        [_moviePlayer play];
        _errorManager.isAVLoading = YES;
        
        
               

        NSLog(@"Movie is Stalled!");
        if (self.myNewTimer) {
            [self.myNewTimer invalidate];
        }
        [[self.view viewWithTag:4040] setHidden:NO];
        
//        _adPlayerView.isCanContinu = NO;
//        [_adPlayerView.timeLabel setText:[NSString stringWithFormat:@"%ld",_adVideoHiddenSecend]];
//        [_adPlayerView reSetVideoADTime:_adVideoHiddenSecend andADDetailLink:self.adVideoDetailURL];

        
    }
    
}




-(NSInteger)secondsFrom:(NSString*)timeLabelText
{
    NSInteger currentTimeSeconds;
    NSArray *timeARR;
    
    if (timeLabelText.length >0) {
        timeARR = [timeLabelText  componentsSeparatedByString:@":"];
        
        if (timeARR.count>0) {
            currentTimeSeconds = [[timeARR objectAtIndex:0] integerValue] * 3600+[[timeARR objectAtIndex:1] integerValue] * 60 + [[timeARR objectAtIndex:2] integerValue];
        }
    }
    
    return currentTimeSeconds;
    
}





#pragma mark ======   MPMoviePlayerNowPlayingMovieDidChangeNotification ====
- (void) MPMoviePlayerPlayingNotification:(NSObject *)object
{
    NSLog(@"MovieNotificationCenter === %@",@"hello Movie");
}


NSMutableArray *currentDemand;
NSMutableArray *totalDemand ;
NSInteger kcurrentTimeDemand;
NSInteger ktotalTimeDemand;
/*  Notification called when the movie finished playing. */
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    
//    return;
    
    NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    
    [_HUD show:NO];
    
    if (_currentTimeL.text.length <=0) {
        _currentTimeL.text = @"00:00:00";
    }
    if (_totalTimeL.text.length <=0) {
        _totalTimeL.text = @"00:00:00";
    }
    currentDemand = [NSMutableArray arrayWithArray:[_currentTimeL.text  componentsSeparatedByString:@":"]];
    if (currentDemand.count>0) {
        
        if ([[currentDemand objectAtIndex:0] length]>0 && [[currentDemand objectAtIndex:1] length]>0 && [[currentDemand objectAtIndex:2] length]>0) {
            kcurrentTimeDemand = [[currentDemand objectAtIndex:0] integerValue] * 3600+[[currentDemand objectAtIndex:1] integerValue] * 60 + [[currentDemand objectAtIndex:2] integerValue];
        }
        
        
    }
    totalDemand =[NSMutableArray arrayWithArray:[_totalTimeL.text componentsSeparatedByString:@":"]];
    
    if (totalDemand.count>0) {
        
        if ([[totalDemand objectAtIndex:0] length]>0 && [[totalDemand objectAtIndex:1] length]>0 && [[totalDemand objectAtIndex:2] length]>0)
        {
            ktotalTimeDemand = [[totalDemand objectAtIndex:0] integerValue] * 3600+[[totalDemand objectAtIndex:1] integerValue] * 60 + [[totalDemand objectAtIndex:2] integerValue];
        }
        
        
    }
    
    switch ([reason integerValue])
    {
            /* The end of the movie was reached. */
        case MPMovieFinishReasonPlaybackEnded:
            
        {
            
            if (_isHaveAD == YES) {
                return;
            }else
            {
            
            
            if (kcurrentTimeDemand != ktotalTimeDemand) {
                if (_moviePlayer.loadState == 0) {
                    [_moviePlayer prepareToPlay];
                    [_moviePlayer play];
                    
                }else{
                    
                    _isPlayEnd = YES;
                    [_moviePlayer setCurrentPlaybackTime:ktotalTimeDemand];
                }
            }else
            {
                if (kcurrentTimeDemand == 0&& ktotalTimeDemand == 0) {
                    _isPlayEnd = NO;
                }
            }
        }
            
//            if (kcurrentTimeDemand != ktotalTimeDemand) {
//                
//                
//                switch (_moviePlayer.loadState) {
//                    case MPMovieLoadStateUnknown:
//                    {
//                        NSLog(@"MPMovieLoadStateUnknown")
//                        return;
//                    }
//                        break;
//                    case MPMovieLoadStatePlayable:
//                    {
//                        _isPlayEnd = YES;
//                        [_moviePlayer setCurrentPlaybackTime:ktotalTimeDemand];
//                        
//                        NSLog(@"MPMovieLoadStatePlayable")
//                        
//                    }
//                        break;
//                    case MPMovieLoadStatePlaythroughOK:
//                    {
//                        NSLog(@"MPMovieLoadStatePlaythroughOK")
//                        
//                    }
//                        break;
//                    case MPMovieLoadStateStalled:
//                    {
//                        NSLog(@"MPMovieLoadStateStalled")
//                        
//                    }
//                        break;
//                        
//                    default:
//                        break;
//                }
//                
//                
//                
//                //                if (_moviePlayer.loadState == 0) {
//                //                    [_moviePlayer play];
//                //
//                //                }else if (_moviePlayer.loadState == 3)
//                //                {
//                //                    [_moviePlayer pause];
//                //                    _isPlayEnd = NO;
//                //
//                //                }else{
//                //
//                //                        _isPlayEnd = NO;
//                //                    [_moviePlayer setCurrentPlaybackTime:ktotalTimeDemand];
//                //                }
//            }

            
            
            
        }
            
            break;
            /* An error was encountered during playback. */
        case MPMovieFinishReasonPlaybackError:
            NSLog(@"An error was encountered during playback");
            _isPlayEnd = NO;
            _errorMaskView.hidden = NO;
            if (_currentPlayType == CurrentPlayEvent) {
                
                if (IsLogin) {
                    [self recordPlaybackTime];
                }
            }
            
            [_errorManager OPSForPlayerErrorWithSourceType:VideoSource_CableNet andAVName:_demandModel.NAME andOPSCode:PlayerAVError andOPSST:[SystermTimeControl getNowServiceTimeString]];

            
            if (kcurrentTimeDemand != ktotalTimeDemand) {
                
                
                //                [_moviePlayer prepareToPlay];
                //                [_moviePlayer setCurrentPlaybackTime:JumpStartTime];
                //                [self performSelectorOnMainThread:@selector(displayError:) withObject:[[notification userInfo] objectForKey:@"error"] waitUntilDone:NO];
            }
            
            break;
            
            /* The user stopped playback. */
        case MPMovieFinishReasonUserExited:{
            
        }
            break;
            
        default:
            break;
    }
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    if ([self isViewLoaded] && self.view.window == nil) {
        self.view = nil;
    }
    
//    _videoListArr = nil;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)receiveBoxIPNoti:(NSNotification *)noti{
    //    NSDictionary *IPDic = [[NSDictionary alloc] initWithDictionary:[noti object]];
    
    
    NSDictionary *IPDic = [[NSDictionary alloc] initWithDictionary:[noti object]];
    
    NSLog(@" 接收到盒子IP信息 %@",IPDic);
    NSString *IPStr = [[IPDic objectForKey:@"param"] objectForKey:@"IPAddress"];
    if (IPStr.length==0) {
        IPStr = @"";
        
        
        
    }
    
    BOOL isExist = NO;//判断新获取的IP是否已经存在
    for (NSString *ip in _IPArray) {
        if ([ip isEqualToString:IPStr]) {
            isExist = YES;
        }
    }
    
    if (!isExist) {
        [_IPArray addObject:IPStr];
        
        
        if (_IPArray.count <=0) {
            
            //            [AlertLabel AlertInfoWithText:@"未搜索到盒子,稍后请重试" andWith:_contentView withLocationTag:2];
            
        }
        
        
//        _IPTableView.hidden = NO;
//        _IPBackIV.hidden = NO;

    }
    
    
//    [_IPTableView reloadData];
    
    
    
    
    
    NSLog(@" 接收到盒子IP信息 %@",IPDic);//接收到IP 之后 我应该怎么做？？？？ 在这之后 写上UI的显示效果 然后点击UI 记住IP
    
}


#pragma mark - IP取消按钮

-(void)ipButtonCancelClick:(UIButton*)sender
{
    _IPBackIV.hidden = YES;
}



-(void)playPullTV:(NSNotification *)not{
    
    NSDictionary *pullDic = [[NSDictionary alloc] initWithDictionary:[not userInfo]];
    
    NSLog(@"接收到拉屏信息-----%@",pullDic);
    NSString *type = [pullDic objectForKey:@"@command"];
    NSDictionary *param = [[NSDictionary alloc] initWithDictionary:[pullDic objectForKey:@"param"]];
    
    NSLog(@"param*******%@",param);
    //这里写 点播甩屏的接口
    //    [AlertLabel AlertInfoWithText:@"接收到甩屏信息" andWith:self.view withLocationTag:0];
    //    DataInterface *data = [[DataInterface alloc] initWithDelegate:self];
    
    
    
    
    
    if ([type isEqualToString:@"DLNAPLAY_VOD"]) {
        //由于拉屏和点播全都走这个地，所以拉过来的点播的断点暂时用负值加以区分
        
        JumpStartTime = -[[param objectForKey:@"seekPos"] integerValue];
        //        [AlertLabel AlertSSIDInfoWithText:[NSString stringWithFormat:@"断点%ld处续播",JumpStartTime] andWith:self.view withLocationTag:0];
        //        [request VODPlayWithContentID:[param objectForKey:@"contentID"] andWithMovie_type:[param objectForKey:@"movie_type"]  andTimeout:10];
        
        NSString *MType =[param objectForKey:@"movie_type"];
        if ([MType integerValue]==0) {
            MType = @"1";
        }
        
        NSString *McontenID = [param objectForKey:@"contentID"];
        if (McontenID.length >0) {
            
            self.demandModel.contentPullID = McontenID;
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            if (ISIntall) {
                
                _installMaskView.hidden = NO;
            }else
            {
            [request VODPlayWithContentID:[param objectForKey:@"contentID"] andWithMovie_type:MType andTimeout:kTIME_TEN];
            }
        }else
        {
            [AlertLabel AlertInfoWithText:@"拉屏操作暂无播放节目" andWith:_contentView withLocationTag:2];
            
            
            return;
        }
        
        if ([[param objectForKey:@"name"] length]>0) {
            [_serviceTitleLabel setText:[param objectForKey:@"name"]];
        }else
        {
            [_serviceTitleLabel setText:@"暂无"];
        }
        
        
        //        [self.serviceTitleLabel setText:[param objectForKey:@"name"]];
        if ([self.demandModel.multipleType isEqualToString:@"0"]) {
            //为了点播电影的二次甩屏做处理
            self.demandModel.name = [param objectForKey:@"name"];
            self.contentID = [param objectForKey:@"contentID"];
            
        }else if ([self.demandModel.multipleType isEqualToString:@"9"]){
            //电视剧 二次甩屏 未作处理
            
        }
        
        
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_LIVE"]){
        //        [data exchangeScreenTVPlayWithTVName:[param objectForKey:@"channelName"] andTimeout:10];
        
        
        //        [request TVPlayEXWithTVName:[param objectForKey:@"channelName"] andTimeout:kTIME_TEN];
        //
        //        [self.serviceTitleLabel setText:[param objectForKey:@"channelName"]];
        
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            [_serviceTitleLabel setText:[param objectForKey:@"channelName"]];
        }else
        {
            [_serviceTitleLabel setText:@"暂无"];
        }
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            self.demandModel.liveTitleP = [NSString stringWithFormat:@"%@",[param objectForKey:@"channelName"]];

            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

            [request TVPlayEXWithTVName:[param objectForKey:@"channelName"] andTimeout:kTIME_TEN];
            
        }else
        {
            [AlertLabel AlertInfoWithText:@"拉屏操作无频道名称" andWith:_contentView withLocationTag:2];
            
            return;
        }
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_EVENT"]){
        JumpStartTime = [[param objectForKey:@"seekPos"] integerValue];
        
        //        [data exchangeScreenEventPlayWithTVName:[param objectForKey:@"channelName"] andStartTime:@"startDate" andEndTime:@"endDate" andTimeout:10];
        
        //        [request EventPlayEXWithTVName:[param objectForKey:@"channelName"]  andStarTime:@"startDate" andEndTime:@"endDate" andTimeout:kTIME_TEN];
        //
        //
        //        [self.serviceTitleLabel setText:[param objectForKey:@"channelName"]];
        if ([[param objectForKey:@"channelName"] length]>0) {
            [_serviceTitleLabel setText:[param objectForKey:@"channelName"]];
            
        }else
        {
            [_serviceTitleLabel setText:@"暂无"];
        }
        
        if ([[param objectForKey:@"seekPos"] integerValue]>0) {
            JumpStartTime = [[param objectForKey:@"seekPos"] integerValue];
        }else
        {
            JumpStartTime = 0;
        }
        
        if ([[param objectForKey:@"channelName"] length]>0) {
            
            
            self.demandModel.fullstartTimeP = [NSString stringWithFormat:@"%@",[param objectForKey:@"startDate"]];
            self.demandModel.endTimeP = [NSString stringWithFormat:@"%@",[param objectForKey:@"endDate"]];
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

            [request EventPlayEXWithTVName:[param objectForKey:@"channelName"] andStarTime:[param objectForKey:@"startDate"] andEndTime:[param objectForKey:@"endDate"] andTimeout:kTIME_TEN];
        }else
        {
            
            [AlertLabel AlertInfoWithText:@"拉屏获取频道名称为空" andWith:_contentView withLocationTag:2];
            
            
            return;
        }
        
        
        
    }else if ([type isEqualToString:@"DLNAPLAY_PAUSE"]){
        //时移节目 增强版时移鉴权
        [self.serviceTitleLabel setText:[param objectForKey:@"channelName"]];
        //        [data exchangeScreenTimeMovePlayWithTVName:[param objectForKey:@"channelName"] andTimeout:10];
        JumpStartTime = [[param objectForKey:@"seekPos"] integerValue]+10;
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];

        [request PauseTVPlayEXWithTVName:[param objectForKey:@"channelName"] andTimeout:kTIME_TEN];
        return;
        
    }
    
    else{
        
        [AlertLabel AlertInfoWithText:@"非点播节目，暂无法播放" andWith:self.view withLocationTag:0];
        
        
    }
    
    
}






-(void)changeLoginVerticalDirection
{
    if (_isVerticalDirection == NO) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
    }
}

#pragma mark - 本地预约通知

-(void)receiveLocationOrder:(NSNotification*)notifi
{
    //    NSMutableDictionary *notiItem = [[NSMutableDictionary alloc] initWithDictionary:notifi.object];
//    NSLog(@"11111 :%@ 22222:%@",notifi.userInfo,notifi.object);
    
    
    
    if (_isVerticalDirection == NO) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
    }
    
    
    
    if (_isCollectionInfo) {
        _demandModel.contentID = _demandModel.contentID;
        _demandModel.vodFenleiType = [self collectVodInformationType];
        _demandModel.name = _demandModel.name;
        _demandModel.vodWatchTime = [SystermTimeControl getNowServiceTimeString];
        _demandModel.vodWatchDuration = [self changeWathchTimeDurationForWatchDuration:_watchDuration andEventDuration:[NSString stringWithFormat:@"%ld",_demandModel.duration]];;
        _demandModel.vodBID = _demandModel.vodBID;
        _demandModel.VodBName = _demandModel.VodBName;
        _demandModel.category = _demandModel.category;
        _demandModel.rootCategory = _demandModel.rootCategory;
        _demandModel.TRACKID_VOD = _demandModel.TRACKID_VOD;
        _demandModel.TRACKNAME_VOD = _demandModel.TRACKNAME_VOD;
        _demandModel.EN_VOD = _demandModel.EN_VOD;
        
        if ([_watchDuration intValue] >= 5) {
            
            [SAIInformationManager VodInformation:_demandModel];
        }
        
        
        [_timeManager reset];
    }

    
    
    EpgSmallVideoPlayerVC *live = [[EpgSmallVideoPlayerVC alloc] init];
    live.serviceID = [notifi.userInfo objectForKey:@"serviceID"];
    live.serviceName = [notifi.userInfo objectForKey:@"serviceName"];
    live.eventURLID = [notifi.userInfo valueForKey:@"eventID"];
    NSString *MtimeDateString = [NSString stringWithFormat:@"%@",[notifi.userInfo valueForKey:@"startTime"]];
    
    if ([MtimeDateString componentsSeparatedByString:@" "].count>0) {
        
        live.currentDateStr = [[MtimeDateString componentsSeparatedByString:@" "] objectAtIndex:0];
    }
    
    live.isLocationOrder = YES;
    live.isLocationOrderVOD = YES;
    
    
    
    [self removeMovieControllerNotification];

    
    [self.navigationController pushViewController:live animated:YES];
    
}








#pragma mark ------- 旋转方向


-(void)orientationChanged:(NSNotification *)notification
{
    NSInteger orientation = [[UIDevice currentDevice] orientation];
    
    NSString *str;
    switch (orientation) {
            
        case 1://home bottom
        {
            
            _isVerticalDirection = YES;
            
        }
            
            break;
        case 3://home right
        {
            str=@"right";
            _isVerticalDirection = NO;
            
        }
            
            break;
        case 4://home left
        {
            str=@"left";
            _isVerticalDirection = NO;
            
            // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
            
            //[self makeRotationWithAngel:0];
            
        }
            
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"orientChange" object:str];
    
}





//弹出列表方法presentSnsIconSheetView需要设置delegate为self
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}




-(void)favorSucceed:(BOOL)Succeed{
    if (Succeed) {
        [_DBackScrollView headerViewFavorSucceed];
        _favourButton.selected = YES;
        _favourLabel.selected = YES;
        //        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collect_normal"] forState:UIControlStateNormal];
        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_selected"] forState:UIControlStateNormal];
        [_favourLabel setTitle:@"已收藏" forState:UIControlStateNormal];

        //        [AlertLabel AlertInfoWithText:@"收藏节目成功" andWith:self.view withLocationTag:1];
        
        
    }else{
        [_DBackScrollView headerViewCancellledFavor];
        _favourButton.selected = NO;
        _favourLabel.selected = NO;
        //        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_live"] forState:UIControlStateNormal];
        [_favourButton setImage:[UIImage imageNamed:@"btn_player_collection_vod_normal"] forState:UIControlStateNormal];
        
        //        [AlertLabel AlertInfoWithText:@"取消收藏节目成功" andWith:self.view withLocationTag:1];
        
        [_favourLabel setTitle:@"收藏" forState:UIControlStateNormal];

        
    }
    
}

-(void)showSearchDeviceLoadingView{
    
    MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:_contentView animated:YES];
    HUD.customView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"ic_player_loading"]];
    
    HUD.mode = MBProgressHUDModeCustomView;
    
    HUD.delegate = self;
    
    HUD.labelText = @"正在搜索设备...";
    
    [HUD show:YES];
    
    [HUD hide:YES afterDelay:3];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //这里是1.0f后 你要做的事情
        [self reloadIPTableView];
    });
    
    
}

-(void)reloadIPTableView
{
    //    [_IPArray addObject:@"192.1.1"];
    if (_IPArray.count<=0) {
        
        [AlertLabel AlertInfoWithText:@"暂无可连接设备！" andWith:_contentView withLocationTag:2];
//        _IPTableView.hidden = YES;
        _IPBackIV.hidden = YES;

        _MTButton.hidden = YES;
        _TVButton.hidden = YES;
        _RemoteButton.hidden = YES;
    }else{
        _IPTableView.hidden = NO;
        _IPBackIV.hidden = NO;

        [_IPTableView reloadData];
//#warning 没有连接，所以不显示
NSString *pointToPointIP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
        if (pointToPointIP.length>0) {
            _MTButton.hidden = NO;
            _TVButton.hidden = NO;
            _RemoteButton.hidden = NO;
        }
        
        
    }
    
    
}



#pragma mark 采集时间的时长


-(NSString *)changeWathchTimeDurationForWatchDuration:(NSString*)watchDuration andEventDuration:(NSString*)eventDuration
{
    
    if (watchDuration == nil) {
        
        watchDuration = @"";
    }
    
    if (eventDuration == nil) {
        
        eventDuration = @"";
    }
    
    if ([watchDuration integerValue]>[eventDuration integerValue]) {
        
        watchDuration = eventDuration;
    }
    
    
    return watchDuration;
}

-(void)countingTo:(NSTimeInterval)time timeToShowString:(NSString*)timeToShowString
{
    //    label.text =[NSString stringWithFormat:@"%.3f/%@",time,timeToShowString];
    _watchDuration = [NSString stringWithFormat:@"%.0f",time];
}


@end




