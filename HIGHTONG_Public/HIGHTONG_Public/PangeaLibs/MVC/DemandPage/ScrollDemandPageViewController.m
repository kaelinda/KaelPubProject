//
//  ScrollDemandPageViewController.m
//  HIGHTONG_Public
//
//  Created by testteam on 15/12/15.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "ScrollDemandPageViewController.h"
#import "UIButton_Block.h"
#import "SearchViewController.h"
#import "HTRequest.h"
#import "headScroll.h"
#import "DemandHomeView.h"
#import "MJRefresh.h"
#import "MBProgressHUD.h"
#import "DemandSecondViewcontrol.h"
#import "ProgramListCell.h"
#import "DemandVideoPlayerVC.h"
#import "GeneralPromptView.h"
#import "ExtranetPromptView.h"
#import "KnowledgeBaseViewController.h"
#import "UrlPageJumpManeger.h"

typedef NS_ENUM(NSInteger,DemandConfigType) {
    DemandConfigTypeOperation = 0,
    DemandConfigTypeTopic,
    DemandConfigTypeCategory,
    DemandConfigTypeProgram
};


@interface ScrollDemandPageViewController ()<HTRequestDelegate,DemandHomeViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    //*****视图部分
    UILabel *_seachTextView;//搜索热词
    headScroll *_headTitleView;//头部的标题视图，运营组或者是专题等
    DemandHomeView *_scrollTablePage;//可以滚动的tableview的视图
    
    UIView *_VerticalLine;//头部试图下的分割线
    
    ExtranetPromptView *_exPromptView;//了解更多view
    
    NSMutableArray * _typeArray;//类型数组，每一个分页到底是运营组还是专题
    UIImageView *Guide ;//引导图
    
    GeneralPromptView *_promptView;//如果不支持点播
    //分页dataSource
    
    
    //*****状态记忆的字段
    NSDate *_firstReqestDate;//第一次请求数据的时间
    NSInteger _currentHeadTitleSeq;//当前的选中的哪一个头部的title
    NSString *_currentCode;//当前选择的code id
    NSString *_currentType;//当前选择的type
    NSInteger _currentPageReqestNumber;//当前页请求分类次数
    
    BOOL _IS_SUPPORT_VOD;//是否支持点播
    
}
@property (nonatomic,strong)NSMutableArray *headTitleArray;//头部标题数组
@property (nonatomic,strong)NSMutableDictionary *configPageAllDiction;//运营组或分类数据字典
@end

@implementation ScrollDemandPageViewController

- (void)viewWillAppear:(BOOL)animated{
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"pagechange" object:nil];
    
    [super viewWillAppear:animated];
    
    if (!IS_EPG_CONFIGURED) {
        
        _headTitleView.hidden = YES;
        
        _VerticalLine.hidden = YES;
        
        _scrollTablePage.hidden = YES;
        
        _exPromptView.hidden = NO;
        
        _promptView.hidden = YES;
        
        return;
        
    }
    else
    {
        _VerticalLine.hidden = NO;
        
        _headTitleView.hidden = NO;
        
        _scrollTablePage.hidden = NO;
        
        _exPromptView.hidden = YES;
        
        [self setSeachBar];
    }
    
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    //请求点播首页数据，需要刷新的数据
    [self reqestDemandHomedata];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _IS_SUPPORT_VOD = [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_VOD"];
    
    if (_IS_SUPPORT_VOD == NO) {
        self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
        
//        [self setNaviBarTitle:@"点播"];
        
        _promptView = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 64+3*kRateSize, kDeviceWidth, 88*kRateSize) andImgName:@"" andTitle:@"点播功能即将上线，敬请期待！"];
        [self.view addSubview:_promptView];
        //1*****设置搜索
        [self setSeachBar];
        if (HomeNewDisplayTypeOn) {
            UIButton *Back = [[UIButton alloc]initWithFrame:CGRectMake(2, 18, 44, 44)];
            [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
            [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateHighlighted];
            Back.showsTouchWhenHighlighted = YES;
            [Back addTarget:self action:@selector(goBackPage) forControlEvents:UIControlEventTouchUpInside];
            [self.m_viewNaviBar addSubview:Back];
        }
        
        
        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
        
        [request TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
        
    }else{
        
        
        if (HomeNewDisplayTypeOn) {
            
            UIButton *Back = [[UIButton alloc]initWithFrame:CGRectMake(2, 18, 44, 44)];
            [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateNormal];
            [Back setImage:[UIImage imageNamed:@"btn_common_title_back_normal"] forState:UIControlStateHighlighted];
            Back.showsTouchWhenHighlighted = YES;
            [Back addTarget:self action:@selector(goBackPage) forControlEvents:UIControlEventTouchUpInside];
            [self.m_viewNaviBar addSubview:Back];
        }
        _typeArray = [[NSMutableArray alloc]init];
        
        //2.2头部的视图数组初始化
        self.headTitleArray = [[NSMutableArray alloc]init];
        //2.3当前选中了第0 个title
        _currentHeadTitleSeq = 0;
        
        //5.所有分页的保存字典
        self.configPageAllDiction = [[NSMutableDictionary alloc]init];
        
        //解决scroll的问题
        [self.view addSubview:[UIView new]];
        
        //4.创建可以滚动分页视图
        NSInteger HIGH = 0;
        if (HomeNewDisplayTypeOn) {
            HIGH = 0;
        }else{
            HIGH = 44;
        }
        _scrollTablePage = [[DemandHomeView alloc]initWithFrame:CGRectMake(0, 64+40*kRateSize, kDeviceWidth, KDeviceHeight-64-40*kRateSize-HIGH)];
        _scrollTablePage.delegate = self;
        _scrollTablePage.HaveHeader = YES;
        _scrollTablePage.notHaveMJ = YES;
        _scrollTablePage.addHigh = -4;
        [self.view addSubview:_scrollTablePage];
        
        //******3，头部
        _headTitleView = [[headScroll alloc]initWithFrame:CGRectMake(0, 64, kDeviceWidth, 40*kRateSize)];
        __weak typeof(_scrollTablePage) WeakscrollTablePage = _scrollTablePage;
        __weak typeof(self) WeakSelf = self;
        
        __block NSInteger weakcurrentHeadTitleSeq = _currentHeadTitleSeq;
        __weak NSString *weakcurrentCode = _currentCode;
    
        _headTitleView.block = ^(NSInteger index,UIButton_Block*btn){
            NSLog(@"当前点击的时第%ld个",index+1);//为了和下面的分页视图保持当前页的顺序一致，从1开始计数
            _currentHeadTitleSeq = index;
            [WeakscrollTablePage ChangePage:_currentHeadTitleSeq + 1];
            //刷新当前页 数据
            NSDictionary *selectHeadTitleDiction = [WeakSelf.headTitleArray objectAtIndex:weakcurrentHeadTitleSeq];
            [WeakSelf show];
            NSString *type = [selectHeadTitleDiction objectForKey:@"type"];
            if ([type isEqualToString:@"1"]) {//判断当前的点播页的 类型type
                if (index < WeakSelf.headTitleArray.count) {
                    NSDictionary *configDiction = [WeakSelf.headTitleArray objectAtIndex:index];
                    NSString *code = [configDiction objectForKey:@"code"];
                    _currentCode = code;
                    NSString *type = [configDiction objectForKey:@"type"];
                    _currentType = type;
                    //             点播分类运营组
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:WeakSelf];
                    [request VODOperationCategoryListWithOperationCode:weakcurrentCode andTimeout:10];
                }else{
                    NSLog(@"点击的位置超出配置范围");
                }
            }
            
            if ([type isEqualToString:@"2"]) {//判断当前的点播页的 类型type
                if (index < WeakSelf.headTitleArray.count) {
                    NSDictionary *configDiction = [WeakSelf.headTitleArray objectAtIndex:index];
                    NSString *code = [configDiction objectForKey:@"code"];
                    _currentCode = code;
                    NSString *type = [configDiction objectForKey:@"type"];
                    _currentType = type;
                    //             点播分类运营组
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:WeakSelf];
                    [request VODTopicProgramListWithTopicCode:weakcurrentCode andStartSeq:0 andCount:18 andTimeout:10];
                }else{
                    NSLog(@"点击的位置超出配置范围");
                }
                
                if ([type isEqualToString:@"3"]) {
                    if (index < WeakSelf.headTitleArray.count) {
                        NSDictionary *configDiction = [WeakSelf.headTitleArray objectAtIndex:index];
                        NSString *code = [configDiction objectForKey:@"code"];
                        _currentCode = code;
                        NSString *type = [configDiction objectForKey:@"type"];
                        _currentType = type;
                            //             点播分类运营组
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:WeakSelf];
                        [request VODProgramListWithOperationCode:@"" andRootCategpryID:weakcurrentCode andStartSeq:0 andCount:99 andTimeout:5 andIndex:0];
                    }else{
                        NSLog(@"点击的位置超出配置范围");
                    }
                }
                
                
            }
            
            if ([type isEqualToString:@"3"]) {
                if (index < WeakSelf.headTitleArray.count) {
                    NSDictionary *configDiction = [WeakSelf.headTitleArray objectAtIndex:index];
                    NSString *code = [configDiction objectForKey:@"code"];
                    _currentCode = code;
                    NSString *type = [configDiction objectForKey:@"type"];
                    _currentType = type;
                        //             点播分类运营组
                    HTRequest *request = [[HTRequest alloc]initWithDelegate:WeakSelf];
                    [request VODProgramListWithOperationCode:@"" andRootCategpryID:weakcurrentCode andStartSeq:0 andCount:99 andTimeout:5 andIndex:0];
                }else{
                    NSLog(@"点击的位置超出配置范围");
                }
            }
            
            UITableView *table =  [WeakSelf findCurrentTableView];
            [table setContentOffset:CGPointMake(0, 0)];
            
        };
        [self.view addSubview:_headTitleView];
        _VerticalLine = [[UIView alloc]init];
        [self.view addSubview:_VerticalLine];
        _VerticalLine.backgroundColor = UIColorFromRGB(0xeeeeee);
        [_VerticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_headTitleView.mas_bottom).offset(-2);
            make.left.equalTo(_headTitleView.mas_left);
            make.right.equalTo(_headTitleView.mas_right);
            make.height.equalTo(@(2));
        }];
        
        [self show];
        //******页面请求
        [self reqestDemandHomedata_REAL];
        //*****
        
        
        //6.2订阅首页广告页的通知，点击了会发通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ADClick:) name:@"ADCLICKVODHOME" object:nil];
        
        [self setupExtranetPrompt];
        
    }
    
}

- (void)setupExtranetPrompt
{
    
    _exPromptView = [[ExtranetPromptView alloc]init];
    
    [self.view addSubview:_exPromptView];
    
    _exPromptView.hidden = YES;
    
    WS(wself);
    
    [_exPromptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wself.view.mas_top).offset(162*kRateSize);
        make.right.mas_equalTo(wself.view.mas_right);
        make.left.mas_equalTo(wself.view.mas_left);
        make.height.mas_equalTo(@(400*kRateSize));
    }];
    
    _exPromptView.moreInformationActionBlock = ^(){
        NSLog(@"点我  你想干什么  你倒是说话啊~");
        KnowledgeBaseViewController *knowledgeVC = [[KnowledgeBaseViewController alloc]init];
        
        knowledgeVC.urlStr = [UrlPageJumpManeger extranetPromptPageJump];
        
        knowledgeVC.existNaviBar = YES;
        
        [wself.navigationController pushViewController:knowledgeVC animated:YES];
    };
    
}


//返回按钮操作
- (void)goBackPage
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 接收到通知的操作
- (void)ADClick:(NSNotification*)notif{
    NSDictionary *dic = notif.object;
    NSLog(@"点击了广告%@",dic);
    NSString *contentID = [dic objectForKey:@"contentID"];
    NSString *programName = [dic objectForKey:@"programName"];
    DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
    demandVC.contentID = contentID;
    demandVC.contentTitleName = programName;
    
    
    [self.navigationController pushViewController:demandVC animated:YES];
}


#pragma mark - 请求数据统一
- (void)reqestDemandHomedata{
    BOOL canReqestData = NO;
    canReqestData = [self RequstEnabledWithFirstTime:_firstReqestDate andSecondData:[NSDate date]];
    if (canReqestData) {//可以请求数据
        
        [self reqestDemandHomedata_REAL];
        
    }else{
        NSLog(@"点播首页暂时无法去请求数据，请求过于频繁了");
    }
}

- (void)reqestDemandHomedata_REAL{
    
    _firstReqestDate = [NSDate date];
    //1.1****请求热搜词 分为 有无点播功能
    HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self];
    if (_IS_SUPPORT_VOD) {
        [reqest VODTopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }else
    {
        [reqest TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
    }
    
    //*****2，请求点播首页的 配置文件 menuConfig
    [reqest VODMenuConfigListWithTimeout:10];
    
}




#pragma mark- DemandHome的代理方法，当前滚动到第几页,并有tableview的代理方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    if (_typeArray.count) {
    //        NSString *type = [_typeArray objectAtIndex:_currentHeadTitleSeq];
    
    if ([_currentType integerValue] == 1) {
        NSArray *souceArray = [self currentDataSource];
        return souceArray.count;
    }else if ([_currentType integerValue] == 2 || [_currentType integerValue] == 3) {
        return 1;
    }else{
        return 0;
    }
    
    //    }else{
    //        return 0;
    //    }
    //
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([_currentType integerValue] == 1) {
        NSArray *souceArray = [self currentDataSource];
        NSDictionary * categoryMutableReqestDiction = [souceArray objectAtIndex:section];
        NSArray *programList = [categoryMutableReqestDiction objectForKey:@"Array"];
        //一行三个
        NSInteger number = [self numberOfCellRowsWithArray:programList];
        if ((programList.count == 1)) {
            id objc = [programList objectAtIndex:0];
            if ([objc isKindOfClass:[NSString class]]) {
                return 0;
            }
        }
        return number;
    }else if ([_currentType integerValue] == 2 ) {
        NSArray *souceArray = [self currentDataSource];
        NSArray *programList = souceArray;
        //一行三个
        NSInteger number = [self numberOfCellRowsWithArray:programList];
        if ((programList.count == 1)) {
            id objc = [programList objectAtIndex:0];
            if ([objc isKindOfClass:[NSString class]]) {
                return 0;
            }
        }
        return number;
    }else if([_currentType integerValue] == 3){
        NSArray *souceArray = [self currentDataSource];
        NSArray *programList = souceArray;
        //一行三个
        NSInteger number = [self numberOfCellRowsWithArray:programList];
        if ((programList.count == 1)) {
            id objc = [programList objectAtIndex:0];
            if ([objc isKindOfClass:[NSString class]]) {
                return 0;
            }
        }
        return number;
    }else{
        return 0;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    if ([_currentType integerValue] == 1) {
        NSArray *souceArray = [self currentDataSource];
        NSDictionary * categoryMutableReqestDiction = [souceArray objectAtIndex:section];
        NSArray *programList = [categoryMutableReqestDiction objectForKey:@"Array"];
        if ((programList.count == 1)) {
            id objc = [programList objectAtIndex:0];
            if ([objc isKindOfClass:[NSString class]]) {
                return 0;
            }
        }
        if (section == 0) {
            return 30;
        }
        return 40;
        //            return (35+5);
    }else if ([_currentType integerValue] == 2) {
        return 0;
    }else{
        return 0;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170*kRateSize;
}
//每个分段的头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([_currentType integerValue] == 1) {
        
        CGFloat sectionHeigh = 40;
        if (section == 0) {
            sectionHeigh = 30;
        }
        
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAME_W(self.view), sectionHeigh)];
        headerView.userInteractionEnabled = YES;
        //            headerView.backgroundColor = [UIColor redColor];
        UIImageView *background = [UIImageView new];
        background.userInteractionEnabled = YES;
        [headerView addSubview:background];
        
        CGFloat HighOfSectionBlow = 8;
        CGFloat space = 5;
        
        UIView *BlackLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (5+HighOfSectionBlow)*kRateSize)];
        BlackLine.backgroundColor =App_background_color;
        if (section == 0) {
            BlackLine.backgroundColor =[UIColor whiteColor];
            BlackLine.frame = CGRectMake(0, 0, kDeviceWidth, 0);
        }
        [headerView addSubview:BlackLine];
        
        
        if (section == 0) {
            space = 0;
            HighOfSectionBlow = 2;
            
        }else{
            
            
        }
        
        [background mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headerView.mas_top).offset((HighOfSectionBlow)*kRateSize);
            make.leading.equalTo(headerView.mas_leading);
            make.right.equalTo(headerView.mas_right);
            make.bottom.equalTo(headerView.mas_bottom);
        }];
        
        background.image = [UIImage imageNamed:@"bg_home_section_header@2x"];
        [background setBackgroundColor:[UIColor greenColor]];
        
        //****************下面的视图都是background 的子视图
        //左边线段
        UIImageView *Leftline = [[UIImageView alloc]initWithFrame:CGRectMake(5*kRateSize, (5+HighOfSectionBlow), 3, 18)];
        Leftline.image = [UIImage imageNamed:@"ic_vertical_yellow_line"];
        //    Leftline.backgroundColor = [UIColor grayColor];
        Leftline.layer.cornerRadius = 2.5;
        Leftline.clipsToBounds =YES;
        [background addSubview:Leftline];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13*kRateSize, (HighOfSectionBlow-1), 100*kRateSize, 30)];
        //********数据中的头标题的名称
        NSArray *categoryArray = [self currentDataSource];
        NSDictionary *sectionDiction = [categoryArray objectAtIndex:section];
        NSString *str =[sectionDiction objectForKey:@"name"];
        label.text = str;
        
        label.textColor = [UIColor blackColor];
        
        
        NSArray *array = [sectionDiction objectForKey:@"Array"];

        if(array.count >= 6)
        {
            UIButton_Block *btn = [UIButton_Block new];
            
            btn.frame =   CGRectMake(FRAME_W(self.view) - (60-7)*kRateSize, (0+HighOfSectionBlow)*kRateSize, 60*kRateSize, 30);
            
            btn.titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
            [btn setTitle:@"更多" forState:UIControlStateNormal];
            [btn setTitleColor:App_selected_color forState:UIControlStateNormal];
            //                    btn.backgroundColor =[UIColor grayColor];
            btn.serviceIDName = label.text;
            [btn setClick:^(UIButton_Block*btn,NSString* name){
                
                NSString *title = [sectionDiction objectForKey:@"name"];
                NSString *categoryID =[sectionDiction objectForKey:@"categoryID"];
                NSLog(@"%@ 频道:%@",categoryID,title);
                
                DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
                //                demandSecond.isMovie = [self isMovieOR:btn.serviceIDName];
                demandSecond.categoryID = categoryID;
                demandSecond.operationCode = _currentCode;
                demandSecond.name = title;
                [self.navigationController pushViewController:demandSecond animated:YES];
                
                
            }];
            
            UIImageView *Rightline = [[UIImageView alloc]initWithFrame:CGRectMake(12*kRateSize, 8, 1*kRateSize, 14)];
            Rightline.backgroundColor = App_selected_color;
            Rightline.layer.cornerRadius = 2.5;
            Rightline.clipsToBounds =YES;
            [btn addSubview:Rightline];
            
            [background addSubview:btn];
            
        }
        [background addSubview:label];
        
        
        //            UIView *BlackLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, (5+HighOfSectionBlow)*kRateSize)];
        //            BlackLine.backgroundColor =App_background_color;
        //            if (section == 0) {
        //                BlackLine.backgroundColor =[UIColor whiteColor];
        //                BlackLine.frame = CGRectMake(0, 0, kDeviceWidth, 0);
        //            }
        //            [headerView addSubview:BlackLine];
        
        return headerView;
    }else{
        return nil;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Progranidentifier = @"Progranidentifier";
    ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:Progranidentifier];
    if (!cell) {
        cell = [[ProgramListCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Progranidentifier];
    }
    
    //取数据
    NSArray *souceArray = [self currentDataSource];
    NSString * title;
    NSArray *programList ;
    
    if ([_currentType integerValue] == 1) {
        
        NSDictionary * categoryMutableReqestDiction = [souceArray objectAtIndex:indexPath.section];
        programList = [categoryMutableReqestDiction objectForKey:@"Array"];
        title = [categoryMutableReqestDiction objectForKey:@"name"];
    }else if ([_currentType integerValue] == 2 || [_currentType integerValue] == 3) {
        programList = souceArray;
    }
    
    if (programList.count == 0) {
        return cell;
    }
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    cell.dateArray.array = [self arrayFOR_ALLArray:programList withBeginnumb:indexPath.row];
    cell.isMovie = [self isMovieOR:title];
    [cell update];
    
    cell.block = ^(UIButton_Block*btn)
    {
        NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
        
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = btn.serviceIDName;
        demandVC.contentTitleName = btn.eventName;
        
        [self.navigationController pushViewController:demandVC animated:YES];
        
    };
    
    return cell;
    
}



#pragma mark-
//已经改变了页码数，方便外边的vc知道当前这个ScrollView滑动到什么地方了
- (void)didChangePageWithPages:(NSInteger)page{//这个页数是从1 开始计数的
    NSLog(@"当前pageView已经滑动到了第%ld页",(long)page);
    BOOL canReqest = NO;
    
    if (_currentHeadTitleSeq != (page - 1)) {
        canReqest = YES;
    }else{
        canReqest = YES;
    }
    _currentHeadTitleSeq = page -1;
    
    
    [_headTitleView setSelectedNum:_currentHeadTitleSeq + 1];
    
    if (canReqest) {//可以请求数据
        NSLog(@"可以请求数据的");
        //        刷新当前页 数据
        NSDictionary *selectHeadTitleDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
        //        [self show];
        NSString *type = [selectHeadTitleDiction objectForKey:@"type"];
        if ([type isEqualToString:@"1"]) {//判断当前的点播页的 类型type
            if (_currentHeadTitleSeq < self.headTitleArray.count) {
                NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                NSString *code = [configDiction objectForKey:@"code"];
                _currentCode = code;
                NSString *type = [configDiction objectForKey:@"type"];
                _currentType = type;
                //             点播分类运营组
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                [request VODOperationCategoryListWithOperationCode:_currentCode andTimeout:10];
            }else{
                NSLog(@"滚动的位置超出配置范围");
            }
            
            
        }
        
        if ([type isEqualToString:@"2"] ) {//判断当前的点播页的 类型type
            if (_currentHeadTitleSeq < self.headTitleArray.count) {
                NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                NSString *code = [configDiction objectForKey:@"code"];
                _currentCode = code;
                NSString *type = [configDiction objectForKey:@"type"];
                _currentType = type;
                //             点播分类运营组
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                [request VODTopicProgramListWithTopicCode:_currentCode andStartSeq:0 andCount:18 andTimeout:10];
            }else{
                NSLog(@"滚动的的位置超出配置范围");
            }
            
        }
        
        if ([type isEqualToString:@"3"]) {
            
            if (_currentHeadTitleSeq < self.headTitleArray.count) {
                NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                NSString *code = [configDiction objectForKey:@"code"];
                _currentCode = code;
                NSString *type = [configDiction objectForKey:@"type"];
                _currentType = type;
                
                HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                [request VODProgramListWithOperationCode:@"" andRootCategpryID:_currentCode andStartSeq:0 andCount:99 andTimeout:5 andIndex:0];
            }else{
                NSLog(@"滚动的的位置超出配置范围");
            }
        }
        
        UITableView *table =  [self findCurrentTableView];
        [table setContentOffset:CGPointMake(0, 0)];
        
    }
}


//已经下拉刷新了
- (void)didRefreshDateWithTag:(NSInteger)tag{
    NSLog(@"pageView第%ld页,向下拉动了",(long)tag);
    UITableView *Tab  =  [_scrollTablePage TableViewIndex:_currentHeadTitleSeq];
    if (Tab) {
        //        [Tab reloadData];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Tab.mj_header endRefreshing];
    });
}


#pragma mark- 请求回调部分
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    //1.1点播搜索热词
    if ([type isEqualToString:VOD_TOP_SEARCH_KEY]) {
        if (status == 0 ){
            NSDictionary *dic = result;
            NSString *count = [dic objectForKey:@"count"];
            
            if ([count integerValue] == -1 ) {
                NSLog(@"暂时热门记录");
            }else if ([count integerValue] >= 1 )
            {
                NSArray *array = [dic objectForKey:@"keyList"];
                if (array.count) {
                    NSDictionary *record = array[0];
                    NSString *name = [record objectForKey:@"name"];
                    
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                    
                }
                
            }else{//如果请求的点播搜索热词要请求直播热词
                HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self];
                [reqest TopSearchKeyWithKey:@"" andCount:1 andTimeout:10];
            }
            
        }else{
            NSLog(@"请求点播搜索热词失败");
        }
    }
    
    
    //1.2直播搜索热词
    if ([type isEqualToString:EPG_TOPSEARCHWORDS]) {
        if (status == 0) {
            
            NSDictionary *dic = result;
            NSString *count = [dic objectForKey:@"count"];
            
            if ([count integerValue] == -1 ) {
                NSLog(@"暂时热门记录");
            }else if ([count integerValue] >= 1 )
            {
                NSArray *array = [dic objectForKey:@"keyList"];
                if (array.count) {
                    NSDictionary *record = array[0];
                    NSString *name = [record objectForKey:@"name"];
                    
                    _seachTextView.text = [NSString stringWithFormat:@"%@",name];
                    
                }
                
            }else{
                NSLog(@"请求直播热词个数为空");
            }
            
            
        }else{
            NSLog(@"请求直播热词失败");
        }
        
        
        
        
        
        
    }
    
    
    //2.3请求了点播首页的配置 信息，区分了不同的战士下过，可以左右滑动，并且tableview的一些样式是根据这个配置信息来确定的
    if([type isEqualToString:VOD_MenuConfigList]){
        NSLog(@"%@",result);
        if (status ==0 ) {
            NSArray *array  = [result objectForKey:@"configList"];
            //更新title视图
            self.headTitleArray = [NSMutableArray arrayWithArray:array];
            _headTitleView.titledataArray = [NSMutableArray arrayWithArray:self.headTitleArray];
            _headTitleView.KEY = @"name";
            _headTitleView.MubiaoNum = _currentHeadTitleSeq;
            [_headTitleView updataTitle];//更新头部的标题
            if (![_scrollTablePage.DataSource isEqual:self.headTitleArray]) {
                [_scrollTablePage.DataSource removeAllObjects];
                _scrollTablePage.DataSource = self.headTitleArray;
                [_scrollTablePage setupView];
                
            }else{
                [_scrollTablePage.DataSource removeAllObjects];
                _scrollTablePage.DataSource = self.headTitleArray;
            }
            
            //            if (self.configPageAllDiction.allKeys.count == 0) {
            //5.1刷新一个所有分页的共同字典
            for (NSDictionary *configDic  in self.headTitleArray) {
                NSString *code = [configDic objectForKey:@"code"];
                [self.configPageAllDiction setObject:[NSMutableArray array] forKey:code];
                NSDictionary *selectHeadTitleDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                //        [self show];
                NSString *type = [selectHeadTitleDiction objectForKey:@"type"];
                if ([type isEqualToString:@"1"]) {//判断当前的点播页的 类型type
                    if (_currentHeadTitleSeq < self.headTitleArray.count) {
                        NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                        NSString *code = [configDiction objectForKey:@"code"];
                        _currentCode = code;
                        NSString *type = [configDiction objectForKey:@"type"];
                        _currentType = type;
                        //             点播分类运营组
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        [request VODOperationCategoryListWithOperationCode:_currentCode andTimeout:10];
                    }else{
                        NSLog(@"滚动的位置超出配置范围");
                    }
                    
                    
                }
                
                if ([type isEqualToString:@"2"] ) {//判断当前的点播页的 类型type
                    if (_currentHeadTitleSeq < self.headTitleArray.count) {
                        NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                        NSString *code = [configDiction objectForKey:@"code"];
                        _currentCode = code;
                        NSString *type = [configDiction objectForKey:@"type"];
                        _currentType = type;
                        //             点播分类运营组
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        [request VODTopicProgramListWithTopicCode:_currentCode andStartSeq:0 andCount:18 andTimeout:10];
                    }else{
                        NSLog(@"滚动的的位置超出配置范围");
                    }
                    
                }
                
                if ([type isEqualToString:@"3"]) {
                    
                    if (_currentHeadTitleSeq < self.headTitleArray.count) {
                        NSDictionary *configDiction = [self.headTitleArray objectAtIndex:_currentHeadTitleSeq];
                        NSString *code = [configDiction objectForKey:@"code"];
                        _currentCode = code;
                        NSString *type = [configDiction objectForKey:@"type"];
                        _currentType = type;
                        
                        HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
                        [request VODProgramListWithOperationCode:@"" andRootCategpryID:_currentCode andStartSeq:0 andCount:99 andTimeout:5 andIndex:0];
                    }else{
                        NSLog(@"滚动的的位置超出配置范围");
                    }
                }

                //运营组,专题等类型的标记数组
                [_typeArray addObject:type];
                
                
            }
            //            }else{
            //                NSLog(@"已经请求过配置文件的分类，总字典中有每个code");
            //            }
            
            //点播页的引导
            NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODGUIDE"];
            if (string.length == 0 && IS_EPG_CONFIGURED) {
                Guide = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight)];
                UIImage *image = [UIImage imageNamed:@"bg_first_geture"];
                
                Guide.image =  image;
                Guide.userInteractionEnabled = YES;
                
                UIImageView *guideTap = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64+4*kRateSize, kDeviceWidth, 110*kRateSize)];
                UIImage *imageTap = [UIImage imageNamed:@"ic_first_tap_gesture"];
                
                guideTap.image = imageTap;
                
                [Guide addSubview:guideTap];
                
                UIButton_Block *block = [[UIButton_Block alloc]initWithFrame:Guide.frame];
                [Guide addSubview:block];
                block.Click = ^(UIButton_Block *btn,NSString *name){
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VODGUIDE"];
                    [Guide removeFromSuperview];
                };
                
                if (HomeNewDisplayTypeOn) {
                    
                    [self.view  addSubview:Guide];
                    [self.view bringSubviewToFront:Guide];
                }
                else
                {
                    [self.tabBarController.view  addSubview:Guide];
                }
                
                
            }
            
            
            //更新下面的分页视图
            _scrollTablePage.delegate = self;
            [_scrollTablePage ChangePage:_currentHeadTitleSeq + 1];
            [self findCurrentAndReloadTableView];
        }else{
            NSLog(@"请求运营组失败");
        }
    }
    
    //6.0 如果这个分页下的是专题
    if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
        NSLog(@"VOD_TOPIC_PROGRAM_LIST-----%@",result);
        NSDictionary *resaultDiction = result;
        NSMutableArray *topicList = [resaultDiction objectForKey:@"vodList"];
        if (topicList.count) {
            //找到改专题下的存放节目的数组
            NSMutableArray *TopicArray = [NSMutableArray arrayWithArray:[self.configPageAllDiction objectForKey:_currentCode]] ;
            [TopicArray removeAllObjects];
            TopicArray = topicList;
            //该专题已经请求完毕，开始刷新节目
            [self findCurrentAndReloadTableView];
        }else{
            NSLog(@"%@专题下的节目暂时为空",_currentCode);
        }
        
    }
    
    
    //5.3点播运营组分类
    if ([type isEqualToString:VOD_OPER_CATEGORY_LIST]) {
        NSLog(@"分类是 %@",result);
        if (status == 0) {
            NSArray *categoryListArray = [result objectForKey:@"categoryList"];
            NSString *count = [result objectForKey:@"count"];
            _currentPageReqestNumber = [count integerValue] ;
            //这个数组要根据id  顺序去排序
            NSMutableArray *categoryArray = [self.configPageAllDiction objectForKey:_currentCode];
            [categoryArray removeAllObjects];
            for (NSDictionary* categoryDic in categoryListArray) {
                NSString * ID= [categoryDic objectForKey:@"id"];
                NSLog(@"%@",[ID class]);
                NSString * categoryID= [categoryDic objectForKey:@"categoryID"];
                //..1为接受数据做准备
                NSMutableDictionary *categoryMutableReqestDiction = [[NSMutableDictionary alloc]initWithDictionary:categoryDic];
                [categoryMutableReqestDiction setObject:[NSArray arrayWithObject:@""] forKey:@"Array"];
                [categoryArray addObject:categoryMutableReqestDiction];
                //..2去请求数据
                HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self];
                [reqest VODProgramListWithOperationCode:_currentCode andRootCategpryID:categoryID andStartSeq:1 andCount:6 andTimeout:10 andIndex:[ID integerValue]-1];//这个地方的排序是从1开始，跟数组差一个
                
            }
            
            NSLog(@"gege %@-",categoryArray);
        }else{
            NSLog(@"请求运营组下分类失败");
        }
    }
    
    
    
    //5.4分类下的数据电视电影点播分类运营组  节目列表
    if ([type isEqualToString:VOD_PROGRAM_LIST]) {
        
        
        NSLog(@"点播分类运营组节目: %ld result:%@  type:%@",(long)status,result,type );
        if (status == 0) {
            NSDictionary *categoryProgramDiction = result;
            NSString *index = [categoryProgramDiction objectForKey:@"index"];
            NSString *count = [categoryProgramDiction objectForKey:@"count"];
            
            if ([_currentType integerValue] == 3) {
                
                NSDictionary *resaultDiction = result;
                NSArray *topicList = [resaultDiction objectForKey:@"vodList"];
                if (topicList.count) {
                    //找到改专题下的存放节目的数组
                    NSMutableArray *TopicArray = [self.configPageAllDiction objectForKey:_currentCode];
                    [TopicArray removeAllObjects];
                    TopicArray.array = topicList;
                    //该专题已经请求完毕，开始刷新节目
                    [self findCurrentAndReloadTableView];
                }else{
                    NSLog(@"%@专题下的节目暂时为空",_currentCode);
                }
                
                return;
            }
            
            _currentPageReqestNumber-- ;
            if ([count integerValue]>0) {
                //取到当前页数组
                NSMutableArray *categoryArray = [self.configPageAllDiction objectForKey:_currentCode];
                //取到当前顺序运营组分类
                NSMutableDictionary *categoryMutableReqestDiction;
                if (categoryArray.count>[index integerValue]) {
                    categoryMutableReqestDiction = [categoryArray objectAtIndex:[index integerValue]];
                }else{
                    categoryMutableReqestDiction = [[NSMutableDictionary alloc] init];
                }
                //                NSMutableDictionary *categoryMutableReqestDiction;// = [NSMutableDictionary dictionary];
                //                if (categoryArray.count>[index integerValue]) {
                //                    categoryMutableReqestDiction= [[NSMutableDictionary alloc] initWithDictionary:[categoryArray objectAtIndex:[index integerValue]]];
                //                }else{
                //                    categoryMutableReqestDiction = [[NSMutableDictionary alloc] init];
                //                }
                
                NSArray *vodList = [categoryProgramDiction objectForKey:@"vodList"];
                [categoryMutableReqestDiction setObject:vodList forKey:@"Array"];
                
            }else{
                NSLog(@"当前运营组分类下没有任何节目，不展示该分类");
            }
            //*********至关重要的刷新界面，数据获取完成时刷新***********
            if (_currentPageReqestNumber == 0) {
                [self hide];
                
            }
            [self findCurrentAndReloadTableView];
        }else{
            NSLog(@"请求运营组下分类下节目失败");
        }
        
        
    }
    
    
}





#pragma mark - 设置视图部分
//设置搜索栏
- (void)setSeachBar
{
    
    
    
    //设置搜索textField
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = size.height/2;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(40 ,2,size.width - 43*kRateSize/kRateSize , 26)];
    
    _seachTextView.text = @"";
    //        _seachTextView.backgroundColor = [UIColor redColor];
    _seachTextView.font = [UIFont systemFontOfSize:16];
    _seachTextView.textColor = UIColorFromRGB(0xaaaaaa);
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake((_seachTextView.frame.origin.x-32)*kRateSize,( _seachTextView.frame.origin.y)*kRateSize, 24, 24)];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
        
        SearchViewController *search = [[SearchViewController alloc]init];

        //        search.searchText = [array lastObject];;
        [self.navigationController pushViewController:search animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    if (!HomeNewDisplayTypeOn) {
        [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 27, kDeviceWidth-30*2*kRateSize, 28)];
    }else{
        [self setNaviBarSearchViewFrame:CGRectMake(40*kRateSize, 27, kDeviceWidth-30*2*kRateSize-10*kRateSize, 28)];
    }
    
}

#pragma mark - 工具方法
/**
 *  找到当前页并刷新
 */
- (void)findCurrentAndReloadTableView{
    UITableView *Tab  =  [_scrollTablePage TableViewIndex:_currentHeadTitleSeq];
    Tab.tableFooterView = [UIView new];
    if (Tab) {
        [Tab reloadData];
    }
}
- (UITableView *)findCurrentTableView{
    UITableView *Tab  =  [_scrollTablePage TableViewIndex:_currentHeadTitleSeq];
    return Tab;
}
/**
 *  请求超时判定
 *
 *  @param date  第一次请求时间
 *  @param date1 第二次请求时间
 *
 *  @return 是否超时  YES 可以再次请求  NO不能再次请求
 */
- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    if ([date1 timeIntervalSinceDate:date] >= 3) {
        NSLog(@"超时了啊");
        return YES;
    }
    return NO;
}
- (NSArray*)currentDataSource{
    NSMutableArray *categoryArray = [self.configPageAllDiction objectForKey:_currentCode];
    return categoryArray;
}
/**
 *  显示loading框
 */
- (void)show{
    [self showCustomeHUDAndHiddenAfterDelay:3];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}
/**
 *  隐藏loading框
 */
- (void)hide{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (BOOL)isMovieOR:(NSString*)string
{
    if (string.length) {
        if ([@"电影" rangeOfString:string].length) {
            return YES;
        }
        return NO;
    }
    return NO;
}
//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        return numb % 3 == 0?  numb/3 : numb/3+1;
    }
}
//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    if (array.count > index*3) {
        [anser addObject:array[index*3]];
    }
    if (array.count > index*3+1) {
        [anser addObject:array[index*3+1]];
    }
    if (array.count > index*3+2) {
        [anser addObject:array[index*3+2]];
    }
    
    return anser;
}
//section‘headView follow scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:[self findCurrentTableView]]) {
        float MaxHighOfSectionHeadView = (35+5)*kRateSize;;
        if (scrollView.contentOffset.y < MaxHighOfSectionHeadView && scrollView.contentOffset.y > 0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }else if (scrollView.contentOffset.y >= MaxHighOfSectionHeadView){
            scrollView.contentInset = UIEdgeInsetsMake(-MaxHighOfSectionHeadView, 0, 0, 0);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
