//
//  DemandScrollPageViewController.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/9/14.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "DemandScrollPageViewController.h"
#import "VODPageRequestManger.h"
#import "DemandVideoPlayerVC.h"
#import "HTEmptyView.h"
#define kSection_Header_Height 35*kDeviceRate //(40*kRateSize)
#define kSection_Footer_Height (32*kRateSize)
#define kSection_GrayLine_Height (20*kRateSize)
#define kSection_Footer_MaxNum (4)
@interface DemandScrollPageViewController ()<VODPageRequestDelegate>
@property (strong,nonatomic)HTEmptyView *emptyLoadingView;
@property (strong,nonatomic)HTEmptyView *emptyView;
@end


@implementation DemandScrollPageViewController

-(instancetype)init{
    self = [super init];
    if (self) {
    }
    return self;
}




-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    _emptyLoadingView.hidden = NO;

//    [self showCustomeHUD];
}





-(void)setDisPlayHeaderDic:(NSDictionary *)disPlayHeaderDic
{
    if (_disPlayHeaderDic) {
        _disPlayHeaderDic = nil;
    }
    _disPlayHeaderDic = disPlayHeaderDic;
    [_headerView reloadHeaderResult:_disPlayHeaderDic];

}


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self hideNaviBar:_isNeedNavBar];
    _demandDisaplayArray = [NSMutableArray array];
    _demandListArray = [NSMutableArray array];

    [self.view setBackgroundColor:App_background_color];
    
    
    _emptyLoadingView = [[HTEmptyView alloc]initWithEmptyFatherView:self.view andEmptyImageString:@"bg_vod_gridview_item_onloading" andEmptyLabelText:nil emptyViewClick:nil];

    [self showCustomeHUD];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        VODPageRequestManger *vodPageManger = [[VODPageRequestManger alloc]initWithDelegate:self];
        [vodPageManger reloadDataForMenuDataCode:_disPlayDemandCode withMenuType:_disPlayDemandType];
    });

    
}




-(void)initTableView
{

    UIView *nilView = [UIView new];
    nilView.frame = CGRectZero;
    [self.view addSubview:nilView];
    
    CGRect tableViewReact;
    tableViewReact = CGRectMake(0, 0, kDeviceWidth, KDeviceHeight-64-HT_HIGHT_SECONDNAVIBAR-kTopSlimSafeSpace-kBottomSafeSpace);
    _disPlayTableView = [[UITableView alloc] initWithFrame:tableViewReact style:UITableViewStyleGrouped];
    _disPlayTableView.delegate = self;
    _disPlayTableView.dataSource = self;
    _disPlayTableView.showsVerticalScrollIndicator = NO;
    _disPlayTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _disPlayTableView.backgroundColor = HT_COLOR_BACKGROUND_APP;
    _disPlayTableView.tableHeaderView = [self setupHeaderView];
    [_headerView reloadHeaderResult:_disPlayHeaderDic];
    _disPlayTableView.delaysContentTouches = NO;
    [self.view addSubview:_disPlayTableView];
    
    _emptyView = [[HTEmptyView alloc]initWithEmptyFatherView:self.disPlayTableView andEmptyImageString:@"bg_retry_gesture" andEmptyLabelText:nil emptyViewClick:^{
        VODPageRequestManger *vodPageManger = [[VODPageRequestManger alloc]initWithDelegate:self];
        [vodPageManger reloadDataForMenuDataCode:_disPlayDemandCode withMenuType:_disPlayDemandType];
    }];

}


-(UIView*)setupHeaderView
{
    
    WS(wself);

    CGRect headerReact = CGRectMake(0, 64+kTopSlimSafeSpace, kDeviceWidth, HT_HIGHT_POSTERVIEW);
    
    if (_headerView == nil) {
        _headerView = [[DemandScrollPageHeaderView alloc] initWithFrame:headerReact];
        [_headerView setBackgroundColor:[UIColor blueColor]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            _headerView.didSelectedImagesBclok = ^(NSString *channelID,NSString *title){
                DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
                demandVC.contentID = channelID;
                demandVC.contentTitleName = title;
                
                [wself.navigationController pushViewController:demandVC animated:YES];
            };
  
        });
        
        
       
    }
    
    __block UIView *header = _headerView;
    __block UITableView *tableView = _disPlayTableView;
    _headerView.headerViewReactChangedBlock = ^(CGRect frame){
        
        header.frame = frame;
        tableView.tableHeaderView = header;
        
    };

    
        return _headerView;
}


#pragma mark - tableView 代理方法



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSInteger sectionCount;
    
    if ([_disPlayDemandType isEqualToString:@"3"])
    {
        
        sectionCount = _demandDisaplayArray.count;
        
        return sectionCount;
        
    }else if ([_disPlayDemandType isEqualToString:@"2"])
    {
        sectionCount = 1;
        
        return sectionCount;
    }else if ([_disPlayDemandType isEqualToString:@"1"])
    {
        sectionCount = _demandDisaplayArray.count;
        return sectionCount;
    }
    
    
    return 0;
 
    
}


//这个是计算有多少个cell的
- (NSInteger)numberOfCellRowsWithArray:(NSArray*)array
{
    NSInteger numb = array.count;
    if (numb == 0) {
        return 0;
    }else
    {
        if (array.count>=6) {
            numb = 6;
        }else
        {
            numb = numb;
        }
        
        return numb % 3 == 0?  numb/3 : numb/3+1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSMutableArray *array = [NSMutableArray array];
    
    NSLog(@"=====8888888====%ld",[[[_demandDisaplayArray objectAtIndex:section] objectForKey:@"vodList"] count]);
    if (_demandDisaplayArray.count>0) {
        
       return   [self numberOfCellRowsWithArray:[[_demandDisaplayArray objectAtIndex:section] objectForKey:@"vodList"]];
    }
    
   
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    if ([_disPlayDemandType isEqualToString:@"3"]|| [_disPlayDemandType isEqualToString:@"1"]) {
        
//        return section == 0 ? kSection_Header_Height : kSection_Header_Height -2* kSection_GrayLine_Height;
        return kSection_Header_Height;
    }else if ([_disPlayDemandType isEqualToString:@"2"])
    {
        return  0;//section == 0 ? kSection_Header_Height - kSection_GrayLine_Height -20 : kSection_Header_Height -2* kSection_GrayLine_Height;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{

    return 5*kDeviceRate;
 
}




-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    if ([_disPlayDemandType isEqualToString:@"3"] || [_disPlayDemandType isEqualToString:@"1"]) {
        //整个header
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, kDeviceWidth, kSection_Header_Height);
        [sectionHeaderView setBackgroundColor:[UIColor redColor]];
        
        
        //白色部分的 header
        UIView *headerContentView = [UIView new];
        headerContentView.frame = CGRectMake(0, 0, kDeviceWidth,kSection_Header_Height);
        headerContentView.backgroundColor = [UIColor whiteColor];
        [sectionHeaderView addSubview:headerContentView];
        
        
        //段头标题视图
        UILabel *sectionTitleLabel = [[UILabel alloc] init];
        sectionTitleLabel.frame = CGRectMake(12*kDeviceRate, 10*kDeviceRate, kDeviceWidth/2, kSection_Header_Height/2);
        sectionTitleLabel.text = [[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicName"];
        sectionTitleLabel.font = HT_FONT_SECOND;
        [sectionTitleLabel setTextColor:HT_COLOR_FONT_FIRST];
        [headerContentView addSubview:sectionTitleLabel];
        
        
        //橙色竖线
        CGSize lineSize = CGSizeMake(1*kDeviceRate, 16*kDeviceRate);
        UIView *orangeLine = [UIView new];
        orangeLine.frame = CGRectMake( 6*kDeviceRate,(headerContentView.frame.size.height-lineSize.height)/2, lineSize.width, lineSize.height);
        orangeLine.backgroundColor = App_orange_color;
        orangeLine.layer.cornerRadius = 2;
        [headerContentView addSubview:orangeLine];
        orangeLine.center = CGPointMake(sectionTitleLabel.frame.origin.x-6, sectionTitleLabel.center.y+1);

        if (_demandDisaplayArray.count>0) {
            
            NSMutableArray *array = [NSMutableArray arrayWithArray: [[_demandDisaplayArray objectAtIndex:section] objectForKey:@"vodList"]];
            if(array.count >= 6)
            {
                UIButton_Block *btn = [UIButton_Block new];
                [headerContentView addSubview:btn];

               btn.frame =   CGRectMake(FRAME_W(self.view) - (65)*kDeviceRate, 5*kDeviceRate, 60*kDeviceRate, 30);
//                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.right.mas_equalTo(rightLabel.mas_left).offset(5*kDeviceRate);
//                    make.top.mas_equalTo(5*kDeviceRate);
//                    make.size.mas_equalTo(CGSizeMake(40*kDeviceRate, 30*kDeviceRate));
//                }];
                btn.titleLabel.font = [UIFont systemFontOfSize:13*kRateSize];
                [btn setTitle:@"更多 >" forState:UIControlStateNormal];
                [btn setBackgroundColor:[UIColor clearColor]];
                btn.titleLabel.font = HT_FONT_THIRD;
                [btn setTitleColor:HT_COLOR_FONT_ASSIST forState:UIControlStateNormal];
                //                    btn.backgroundColor =[UIColor grayColor];
                //            btn.serviceIDName = label.text;
                [btn setClick:^(UIButton_Block*btn,NSString* name){
                    
                    NSString *title = [[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicName"];
                    NSString *categoryID =[[_demandDisaplayArray objectAtIndex:section] objectForKey:@"categoryID"];
                    NSLog(@"%@ 频道:%@",categoryID,title);
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        
                        NSString *codeString = @"";
                        NSString *codeName = @"";
                        if ([_disPlayDemandType isEqualToString:@"1"]) {
                            
                            codeString = _disPlayDemandCode;
                        }else if ([_disPlayDemandType isEqualToString:@"3"])
                        {
                            codeString = [[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicCode"];
                        }
                        
                        
//                        if ([_disPlayDemandType isEqualToString:@"3"]) {
//                            
//                            codeString = [[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicCode"];
//                        }
                        
                        codeName =  isEmptyStringOrNilOrNull([[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicName"])?@"_":[[_demandDisaplayArray objectAtIndex:section] objectForKey:@"topicName"];

                        
                        DemandSecondViewcontrol *demandSecond = [[DemandSecondViewcontrol alloc]init];
                        //                demandSecond.isMovie = [self isMovieOR:btn.serviceIDName];
                        demandSecond.categoryID = categoryID;
                        demandSecond.operationCode = codeString;
                        [demandSecond setDisPlayDemandType:_disPlayDemandType];
                        demandSecond.name = title;
                        demandSecond.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,codeString];
                        demandSecond.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,codeName];
                        demandSecond.EN = [NSString stringWithFormat:@"%@",self.EN];
                        
                        [self.navigationController pushViewController:demandSecond animated:YES];
 
                    });
                    
                    
                }];                
                
                
            }
  
        }
        
      
        return sectionHeaderView;
    }
    
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 192*kDeviceRate;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProgramListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[ProgramListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    
    }
    
    NSString * title;
    
    if (_demandDisaplayArray.count>0) {
        
        
        if ([_disPlayDemandType isEqualToString:@"1"]||[_disPlayDemandType isEqualToString:@"3"]) {
            
            NSArray *dataArr = [NSArray array];
            
            dataArr = [self arrayFOR_ALLArray:[[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"vodList"] withBeginnumb:indexPath.row];
            cell.dateArray = [NSMutableArray arrayWithArray:dataArr];
            NSDictionary * categoryMutableReqestDiction = [[[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"vodList"] objectAtIndex:indexPath.row];
            title = [categoryMutableReqestDiction objectForKey:@"name"];
            
        }else if ([_disPlayDemandType isEqualToString:@"2"])
        {
             cell.dateArray.array = [self arrayFOR_ALLArray:[[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"vodList"] withBeginnumb:indexPath.row];
        }
        
        
      
    }
    cell.isMovie = [self isMovieOR:title];

    [cell update];
    
    cell.block = ^(UIButton_Block *btn)
    {
        NSLog(@"%@ %@",btn.serviceIDName,btn.eventName);
        
        NSString *codeString = @"";
        NSString *codeName = @"";
        if ([_disPlayDemandType isEqualToString:@"1"]) {
            
            codeString = [[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"categoryID"];
        }
        
        if ([_disPlayDemandType isEqualToString:@"2"]) {
            
            codeString = @"_";
        }
        
        if ([_disPlayDemandType isEqualToString:@"3"]) {
            
            codeString = [[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"topicCode"];
        }
        
        codeName =  isEmptyStringOrNilOrNull([[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"topicName"])?@"_":[[_demandDisaplayArray objectAtIndex:indexPath.section] objectForKey:@"topicName"];
        
        
        NSLog(@"点播一级界面上边的Name%@++ID:%@++下边的ID%@++下边的Name%@",self.disPlayDemandName,self.disPlayDemandCode,codeString,codeName);
        DemandVideoPlayerVC *demandVC = [[DemandVideoPlayerVC alloc]init];
        demandVC.contentID = btn.serviceIDName;
        demandVC.contentTitleName = btn.eventName;
        demandVC.TRACKID = [NSString stringWithFormat:@"%@,%@",self.TRACKID,codeString];
        demandVC.TRACKNAME = [NSString stringWithFormat:@"%@,%@",self.TRACKNAME,codeName];
        demandVC.EN = [NSString stringWithFormat:@"%@",self.EN];
        [self.navigationController pushViewController:demandVC animated:YES];
    };

    
    

    return cell;
}


- (BOOL)isMovieOR:(NSString*)string
{
    if (string.length) {
        if ([@"电影" rangeOfString:string].length) {
            return YES;
        }
        return NO;
    }
    return NO;
}


//这个是取出相应行cell 中的那三个数据放到  programlist的array中
- (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index
{
    NSMutableArray *anser = [NSMutableArray array];
    if (array.count > index*3) {
        [anser addObject:array[index*3]];
    }
    if (array.count > index*3+1) {
        [anser addObject:array[index*3+1]];
    }
    if (array.count > index*3+2) {
        [anser addObject:array[index*3+2]];
    }
    
    return anser;
}



-(void)disPlayRequestCode:(NSString *)code andType:(NSString *)type andHeaderDic:(NSDictionary*)headerDic;
{
    
//    _disPlayHeaderDic = headerDic;
//    [self setDisPlayHeaderDic:headerDic];
//    [_headerView reloadHeaderResult:_disPlayHeaderDic];
    
    _disPlayDemandCode = code;
    _disPlayDemandType = type;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        VODPageRequestManger *vodPageManger = [[VODPageRequestManger alloc]initWithDelegate:self];
        [vodPageManger reloadDataForMenuDataCode:_disPlayDemandCode withMenuType:_disPlayDemandType];
    });
    
    
}



-(void)VODDisplayDataGetSuccessWith:(NSMutableArray *)result
{

    _emptyLoadingView.hidden = YES;
    if ([_disPlayDemandType isEqualToString:@"3"]) {
        
        if (_demandDisaplayArray) {
            [_demandDisaplayArray removeAllObjects];
            _demandDisaplayArray = nil;
        }
        
        [self initTableView];
        [self setDemandDisaplayArray:[NSMutableArray arrayWithArray:result]];


    }else if ([_disPlayDemandType isEqualToString:@"2"]){
        
        if (_demandDisaplayArray) {
            [_demandDisaplayArray removeAllObjects];
            _demandDisaplayArray = nil;
        }
        
        
        [self initTableView];
        
        [self setDemandDisaplayArray:[NSMutableArray arrayWithArray:result]];
        
    }else if ([_disPlayDemandType isEqualToString:@"1"]){
        if (_demandDisaplayArray) {
            [_demandDisaplayArray removeAllObjects];
            _demandDisaplayArray = nil;
        }
        
        [self initTableView];
        [self setDemandDisaplayArray:[NSMutableArray arrayWithArray:[KaelTool increaseArrayWithArray:result andKey:@"index"]]];
        

    }
        
    
}

-(void)setDemandDisaplayArray:(NSMutableArray *)demandDisaplayArray
{
    _demandDisaplayArray = demandDisaplayArray;
    [self hiddenCustomHUD];
    if (_demandDisaplayArray.count<=0) {
        _emptyView.hidden = NO;
    }else
    {
        _emptyView.hidden = YES;
    }
    
    [_disPlayTableView reloadData];

}


@end
