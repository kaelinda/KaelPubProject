//
//  BufferDemandeViewController.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/11/19.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "BufferDemandeViewController.h"
#import "GeneralPromptView.h"
#import "UIButton_Block.h"
@interface BufferDemandeViewController ()

@property (nonatomic, strong)GeneralPromptView *promptView;

@end

@implementation BufferDemandeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xe6e6e6);
    
    [self setNaviBarTitle:@"点播"];
    
    _promptView = [[GeneralPromptView alloc] initWithFrame:CGRectMake(0, 64+3*kRateSize, kDeviceWidth, 88*kRateSize) andImgName:@"" andTitle:@"点播功能即将上线，敬请期待！"];
    [self.view addSubview:_promptView];
    [self setSeachBar];
}


- (void)setSeachBar
{
    
    
    
    //设置搜索textField
    CGRect HTframe = [HT_NavigationgBarView titleViewFrame];
    CGSize size = HTframe.size;
    NSLog(@"%f %f ",size.width,size.height);
    UIView *view =  [[UIView alloc ]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    UIColor *viewBgC = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"bg_search_input"]];
    view.backgroundColor = viewBgC;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = size.height/2;
    view.clipsToBounds = YES;
    
    _seachTextView = [[UILabel alloc]initWithFrame:CGRectMake(40 ,2,size.width - 43*kRateSize/kRateSize , 26)];
    
    _seachTextView.text = @"";
    //        _seachTextView.backgroundColor = [UIColor redColor];
    _seachTextView.font = [UIFont systemFontOfSize:16];
    _seachTextView.textColor = UIColorFromRGB(0xaaaaaa);
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_seachTextView.bounds byRoundingCorners:UIRectCornerTopRight |UIRectCornerBottomRight cornerRadii:CGSizeMake(_seachTextView.size.width, _seachTextView.size.height)];
    CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];
    masklayer.path = maskPath.CGPath;
    _seachTextView.layer.mask = masklayer;
    
    
    [view addSubview:_seachTextView];
    
    UIImageView *searchview =[[UIImageView alloc]initWithFrame:CGRectMake((_seachTextView.frame.origin.x-30)*kRateSize,( _seachTextView.frame.origin.y+1)*kRateSize, 24, 24)];
    searchview.image = [UIImage imageNamed:@"ic_common_title_search"];
    searchview.tag = 996633;
    [view addSubview:searchview];
    
    UIButton_Block *btn = [UIButton_Block new];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    btn.Click = ^(UIButton_Block *btn,NSString *name)
    {
        //搜索按钮被点击
        NSLog(@"搜索按钮被点击了： %@",_seachTextView.text);
        
        
//        SearchViewController *search = [[SearchViewController alloc]init];
//        NSArray *array  = [_seachTextView.text componentsSeparatedByString:@"\t"];
//        //        search.searchText = [array lastObject];;
//        [self.navigationController pushViewController:search animated:YES];
        
    };
    
    view.backgroundColor = [UIColor whiteColor];
    [self setNaviBarSearchView:view];
    [self setNaviBarSearchViewFrame:CGRectMake(30*kRateSize, 27, kDeviceWidth-30*2*kRateSize, 28)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
