//
//  AppSwitch.h
//  HIGHTONG_Public
//
//  Created by Kael on 2016/12/2.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#ifndef AppSwitch_h
#define AppSwitch_h

/**
 这是一个项目中的功能模块开关的控制类   UI相关
 */

#pragma mark - ---------------------

typedef NS_ENUM(NSUInteger, HomePageType) {
    kHomePageType_JXCable_v1_0 = 0,//江西有线版
    kHomePageType_JKTV_v1_1 = 1,//健康电视V1.1.0版 七个大板块
    kHomePageType_JKTV_V1_2 = 2,//健康电视V1.2.0版 首页糅合多个功能入口
    kHomePageType_JKTV_V1_3 = 3,//健康电视V1.3.0版 新添加轻松一刻模块的首页
    kHomePageType_JKTV_V1_4 = 4,//新增预约挂号功能
    kHomePageType_JKTV_V1_5 = 5,//健康地那是v1.5.0版本 新增加UGC直播功能
};



#pragma mark - 健康电视

#ifdef DISTRICT_JKTV
#define KUNITE_PORTAL                   YES//统一PORTAL开关
#define IS_SUPPORT_VOD                  YES//是否支持点播功能的开关
#define IS_SUPPORT_MULTI_SCREEN_INERACT NO//支持多屏互动
#define MOB_COUNT                       YES//是否采用友盟统计开关
#define USER_ACTION_COLLECTION          YES//用户行为信息采集
#define IS_SUPPORT_PAUSEPLAY            YES//用户时移功能的开关
#define IS_PERMISSION_GROUP             YES//权限分组的开关
#define IS_CHECTOUT_PRAISEDEGRADE       YES//点赞吐槽的开关
#define IS_SUPPORT_TV_AD                YES//视频广告开关
#define IS_SUPPORT_ADList               YES//是否使用appbase的ADList接口  这个是控制广告页的广告
#define SUPPORT_AD              [[NSUserDefaults standardUserDefaults] boolForKey:@"SUPPORT_AD"]
#define HomeNewDisplayTypeOn            YES//首页用新的展示方式
#define YJVideoNewDisplayTypeOn         YES//医聚视频新展示方式开关
#define isAuthCodeLogin                 YES//是否是动态口令登录(YES：动态登录；NO：账号密码)
#define isKnowledgeBase                 YES//是否有知识库
#define is_Separate_API                 YES//是否分离首页数据 决定了首页轮播图组成！！！！！！！
#define isUsePolymedicine_2_0           [[NSUserDefaults standardUserDefaults] boolForKey:@"HEALTHVOD"] //是否是使用 医聚 2.0 的接口
#define IS_EPG_CONFIGURED           [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_EPG_CONFIGURED"] //是否获取到epg和vod的url 跟内外网区分 页面跳转有关
#define IS_VODHOMEPage_NewVersion       YES//是否是新版本的点播首页：赖利波
#define IS_RECOMMENDARRAY               YES//是否是新的推荐列表展示 和 播放逻辑 （医视频界面）
#define IS_MOBILEPUSH_NATIVE_HUMOUR     YES//是否是进入native界面，如果NO就是网页 (推送幽默视频之后页面跳转逻辑开关)
#define IS_MERGEPORJECTLIST             YES//是否合并内外网projectList(内外网切换)
#define IS_SHAREEX                      YES//是否是新分享EX

#define INTELLIGENT_TERMINAL_TYPE       @"2"  //1是机顶盒  2是智能终端

/**
 *  首页类型控制
 *
 * kHomePageType_JXCable_v1_0 = 0,江西有线版 （使用中）
 * kHomePageType_JKTV_v1_1 = 1,健康电视V1.1.0版 七个大板块 (已废弃)
 * kHomePageType_JKTV_V1_2 = 2,健康电视V1.2.0版 首页糅合多个功能入口 （已废弃）
 * kHomePageType_JKTV_V1_3 = 3,健康电视V1.3.0版 新添加轻松一刻模块的首页
 * kHomePageType_JKTV_V1_4 = 4,新增预约挂号功能 ------》目前版本 （爱上TV 增加了预约挂号功能，但是健康电视没有这个功能的入口，依旧使用的是H5;在这个版本，爱上TV 和 健康电视 分成了两个项目）
 * kHomePageType_JKTV_V1_5 = 5,健康地那是v1.5.0版本 新增加UGC直播功能（已废弃）
 */
#define kApp_HomePageType 4


#endif




#endif /* AppSwitch_h */
