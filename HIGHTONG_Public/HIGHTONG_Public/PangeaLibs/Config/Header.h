//
//  Header.h
//  HIGHTONG
//
//  Created by Kael on 15/7/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#ifndef HIGHTONG_Header_h
#define HIGHTONG_Header_h
#import "UIView+Toast.h"

typedef NS_ENUM(NSInteger, BindingType){
    DigitBinding = 0,
    InteractBinding,
//    FiberBinding,
};

typedef NS_ENUM(NSInteger, GestureType){
    GestureTypeOfNone = 0,
    GestureTypeOfVolume,
    GestureTypeOfBrightness,
    GestureTypeOfProgress,
};



typedef NS_ENUM(NSInteger, DLNAPLAYType){
    DLNAPLAYTypeOfLive = 0,
    DLNAPLAYTypeOfEvent,
    DLNAPLAYTypeOfVod,
};



//日期选择是一周还是10天
typedef NS_ENUM(NSUInteger, EPG_DateType) {
    EPG_Date_week,
    EPG_Date_TenDays,
};



//EPGScrollView视图类型
typedef NS_ENUM(NSUInteger, EPGScrollViewType) {
    EPGScrollViewChannel,
    EPGScrollViewVideoList,
};


typedef NS_ENUM(NSInteger, CurrentPLAYType){
    CurrentPlayLive = 0,//当前播放的视频节目是直播节目
    CurrentPlayEvent,//当前播放的视频是回看节目
    CurrentPlayTimeMove,//当前播放的视频是时移节目
    CurrentPlayVod,//当前播放的视频是点播节目
    CurrentPlayPolyMedic,//当前播放的视频是医视频节目
    CurrentPlayRelax,//当前播放的视频是轻松一刻也就是幽默视频
};

typedef NS_ENUM(NSInteger, PlayerMaskViewType){
    PlayBtnType = 0,
    MovieOrderType,
    TelePlayOrderType,
    LoginActionType,
    DemandLoginType,
    LoadingViewType,
    ZhuanWangViewType, //专网   cap  添加
    NetBreakedType,//网络 已断开
    InstallType,
};

typedef NS_ENUM(NSInteger, JXPlayerMaskViewType){
    JXLoginActionType = 0,
    JXBindingActionType,
   };


//广告视图类型
typedef NS_ENUM(NSUInteger, TVADMaskViewType) {
    ImageAD,
    VideoAD,
    OtherAD,
};

typedef NS_ENUM(NSInteger, VODPLAYType){
    VODPlayMovie = 0,
    VodPlayTVSerial,
    VOdPlayVariety,
    VodPlayGame,
};

// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger,PanDirection){
    PanDirectionHorizontalMoved,
    PanDirectionVerticalVolumeMoved,
    PanDirectionVerticalBrighteMoved,
    
};
//点赞吐槽的tag值
typedef NS_ENUM(NSInteger,PraiseDegrade){
    NotPraiseDegrade,
    PassedPraise,
    PassedDegrade,
    
};

//是不是爱上TV值
typedef NS_ENUM(NSInteger,APPMaskType){
    JXCABLE_ASTV = 0,
    JKTV,

};


//Plist的宏定义
#define EPGINFOPLIST @"EpgInfo.plist"
#define VODINFOPLIST @"EventInfo.plist"
#define PLAYRECORDERPLIST @"PlayRecorderPlist.plist" //登录用户本地播放记录
#define LIVEFAVORITEPLIST @"LiveFavoritePlist.plist" //登录用户直播收藏
#define VODFAVORITEPLIST @"VODFavoritePlist.plist" //登录用户点播收藏
//#define SEARCHRECORDERPLIST @"SearchRecorderPlist.plist" //登录用户本地搜索记录
#define GUESTPLAYRECORDERPLIST @"GuestPlayRecorderPlist.plist" //游客本地播放记录
#define GUESTLIVEFAVORITEPLIST @"GuestLiveFavoritePlist.plist" //游客直播收藏
#define GUESTVODFAVORITEPLIST @"GuestVODFavoritePlist.plist" //游客点播收藏
//#define GUESTSEARCHRECORDERPLIST @"GuestSearchRecorderPlist.plist" //游客本地搜索记录
#define REWATCHPLAYRECORDERPLIST @"ReWatchPlayRecorderPlist.plist" //登录用户本地回看播放记录
#define GUESTREWATCHPLAYRECORDERPLIST @"GuestReWatchPlayRecorderPlist.plist" //游客本地回看播放记录
#define USER_HEALTHY_COLLECTION_PLIST @"UserHealthyCollectionPlist.plist"//用户健康视频收藏记录
#define GUEST_HEALTHY_COLLECTION_PLIST @"GuestHealthyCollectionPlist.plist"//游客健康视频收藏记录
#define USER_SCHEDULE_LIST_PLIST @"UserScheduleListPlist.plist"//用户预约列表
#define UGC_UPLOADING_LIST_PLIST @"UGCUpLoadingListPlist.plist"//UGC上传任务列表
#define UGC_CATEGORY_LIST_PLIST @"UGCCommonGetCategoryList.plist"//视频编辑分类列表
#define UGC_TAG_LIST_PLIST @"UGCCommonGetTagList.plist"//视频编辑标签列表
#define UGC_ACADEMIC_TITLE_LIST_PLIST @"UGCCommonGetAcademicTitle.plist"//获取医生职称接口
#define MOBILESEARCHRECORDERPLIST @"MobileSearchRecorderPlist.plist" //手机设备本地搜索记录

//通知name的宏定义
#define REFRESH_THE_FAVORITE_CHANNEL_LIST @"REFRESH_THE_FAVORITE_CHANNEL_LIST" //频道收藏
#define REFRESH_THE_FAVORITE_DEMAND_LIST @"REFRESH_THE_FAVORITE_DEMAND_LIST" //点播收藏
#define REFRESH_THE_PLAY_RECORDER_LIST @"REFRESH_THE_PLAY_RECORDER_LIST" //播放记录
#define REFRESH_THE_RE_WATCH_LIST @"REFRESH_THE_RE_WATCH_LIST"//回看播放记录列表
#define REFRESH_THE_SCHEDUEL_CHANNEL_LIST @"refreshScheduelChannelList"//预约列表
#define NOTI_UPDATE_FAVORATE_CHANELL_LIST @"NOTI_UPDATE_FAVORATE_CHANELL_LIST"




//BOBO所有
//是否为空或是[NSNull null]
#define NotNilAndNull(_ref)  (((_ref) != nil) && (![(_ref) isEqual:[NSNull null]]))
#define IsNilOrNull(_ref)   (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]))

#define NotEmptyStringAndNilAndNull(_ref)       (((_ref) != nil) && !([(_ref) isEqual:[NSNull null]]) && !([(_ref)isEqualToString:@""]) && !([(_ref)isEqualToString:@"(null)"]) )

#define isEmptyStringOrNilOrNull(_ref)          (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) || ([(_ref)isEqualToString:@""]) || ([(_ref)isEqualToString:@"(null)"]) ||([(_ref)isEqualToString:@"null"]))
//字符串是否为空
#define IsStrEmpty(_ref)    (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) ||([(_ref)isEqualToString:@""]))
//数组是否为空
#define IsArrEmpty(_ref)    (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) ||([(_ref) count] == 0))




#define IsIOS6 ([[UIDevice currentDevice].systemVersion floatValue]<7.0)

#define App_background_color UIColorFromRGB(0xf7f7f7)//app视图默认背景颜色 (灰白)
#define App_white_color  UIColorFromRGB(0xffffff)//app白色背景
#define App_line_color  UIColorFromRGB(0xeeeeee)//app分割线颜色
#define App_orange_color UIColorFromRGB(0xff9c00)//app橘色
#define App_blue_color UIColorFromRGB(0x3ab7f5)//app蓝色
#define App_unselected0range_color UIColorFromRGB(0x999999)//app未选中时的 非橘色

#define APP_TirdBlack_color UIColorFromRGB(0x333333)//app黑色
#define APP_Six_color UIColorFromRGB(0x666666)//app黑色
#define APP_Nine_color UIColorFromRGB(0x999999)//app黑色




#define IsIOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >=8.0 ? YES : NO)
#define kyyy  (IsIOS6?20:0)
// 将16进制的值转换为对应的颜色
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define COLOR_FROM_RGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]




#define MSG_HEALTHPLAYERTAG   631
#define MSG_LOGINTAG          632
#define MSG_EPGDEMANDTAG      633
#define MSG_HEALTHH5TAG       634
#define MSG_LOGINH5TAG        635
#define MSG_EPGDEMANDH5TAG    636
#define MSG_COMMONPLAYERTAG   637
#define MSG_COMMONH5TAG       638
#define MSG_UGCLIVEPLAYER_PROGRAMH5TAG 639
#define MSG_UGCLIVEPLAYER_PROGRAMPLAYERTAG 640
#define MSG_UGCALVIEWCONTROLLERH5TAG        641
#define MSG_UGCALVIEWCONTROLLERPLAYERTAG        642
#define MSG_PUBHUMOURTAG        643
#define MSG_PUBHUMOURH5TAG      644
//BOBO end





//...ww begin
#define IsLogin [[NSUserDefaults standardUserDefaults] boolForKey:@"IsLogin"]//用户登录状态
#define AD_GroupID [[NSUserDefaults standardUserDefaults] objectForKey:@"groupID"]//广告用户分组
#define KMobileNum [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"]//手机号

#define IsBinding [[NSUserDefaults standardUserDefaults] boolForKey:@"IsBinding"]//用户是否绑定

#define buff_instansCode  ([NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"]])
#define KInstanceCode (NotEmptyStringAndNilAndNull(buff_instansCode) ? buff_instansCode:@"")

#define topHigh (64 + 10*([UIScreen mainScreen].bounds.size.width)/320)
#define fiveGap (5*kRateSize)
#define generalWidth (280*kRateSize)//我的部分所有横分割线的长
#define generalBtnWidth (305*kRateSize)//登录、退出、提交等按钮的宽度



#define App_font(fontValue) [UIFont systemFontOfSize:((CGFloat)(fontValue*kRateSize))]
#define HEITI_Light_FONT @"STHeitiJ-Light"


#define IsSignIn [[NSUserDefaults standardUserDefaults] boolForKey:@"IsFirstSignIn"]//判断账号是不是第一次签到用的
#define Sign [[NSUserDefaults standardUserDefaults] boolForKey:@"sign"]//判断账号是不是第一次签到用的

//...ww-----------------------------------------------


//************************************
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define APPDELEGATE [(AppDelegate*)[UIApplication sharedApplication]  delegate]
//----------------------系统设备相关----------------------------

#define kViewOriginY (isAfterIOS7?64:44)//有nav的情况下，view的起点坐标
//
#define kWindow [UIApplication sharedApplication].keyWindow
//
//添加到window上的吐司提示
#define ZSToast(str)              CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle]; \
[kWindow  makeToast:str duration:2 position:CSToastPositionCenter style:style];\
kWindow.userInteractionEnabled = NO; \
dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{\
kWindow.userInteractionEnabled = YES;\
});\


//获取设备屏幕尺寸
#define kDeviceWidth  [UIScreen mainScreen].bounds.size.width
#define KDeviceHeight [UIScreen mainScreen].bounds.size.height
#define kRateSize ((KDeviceHeight>kDeviceWidth)?kDeviceWidth:KDeviceHeight)/320
#define kDeviceRate ((KDeviceHeight>kDeviceWidth)?kDeviceWidth:KDeviceHeight)/375//以iphone6为标准尺寸

//数字键盘高度
#define kNumKeyboardHeight (kDeviceWidth > 375 ? 226 : 216)
//默认键盘高度
#define kDefaultKeyboardHeight (kDeviceWidth > 375 ? 271 : ((kDeviceWidth > 320) ? 258 : 253))
//适配iPhone X的安全空间 safe space
#define kBottomSafeSpace (KDeviceHeight==812 ? 34 : 0)
#define kTopSafeSpace (KDeviceHeight==812 ? 44 : 20)
#define kTopSlimSafeSpace (KDeviceHeight==812 ? 24 : 0)

//获取系统版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion [[UIDevice currentDevice] systemVersion]
#define isIOS4 ([[[UIDevice currentDevice] systemVersion] intValue]==4)
#define isIOS5 ([[[UIDevice currentDevice] systemVersion] intValue]==5)
#define isIOS6 ([[[UIDevice currentDevice] systemVersion] intValue]==6)
#define isIOS7 ([[[UIDevice currentDevice] systemVersion] intValue]==7)
#define isIOS8 ([[[UIDevice currentDevice] systemVersion] intValue]==8)
#define isIOS9 ([[[UIDevice currentDevice] systemVersion] intValue]==9)

#define isAfterIOS4 ([[[UIDevice currentDevice] systemVersion] intValue]>=4)
#define isAfterIOS5 ([[[UIDevice currentDevice] systemVersion] intValue]>=5)
#define isAfterIOS6 ([[[UIDevice currentDevice] systemVersion] intValue]>=6)
#define isAfterIOS6A (([[[UIDevice currentDevice] systemVersion] intValue]>=6)?((IS_4INCH)?YES:NO):NO)
#define isAfterIOS7 ([[[UIDevice currentDevice] systemVersion] intValue]>=7)
#define isAfterIOS8 ([[[UIDevice currentDevice] systemVersion] intValue]>=8)
#define isAfterIOS9 ([[[UIDevice currentDevice] systemVersion] intValue]>=9)
#define isAfterIOS10 ([[[UIDevice currentDevice] systemVersion] intValue]>=10)


#define isBeforeIOS4 ([[[UIDevice currentDevice] systemVersion] intValue]<=4)
#define isBeforeIOS5 ([[[UIDevice currentDevice] systemVersion] intValue]<=5)
#define isBeforeIOS6 (([[[UIDevice currentDevice] systemVersion] intValue]<=6)?(IS_4INCH?YES:NO):NO)
#define isBeforeIOS7 ([[[UIDevice currentDevice] systemVersion] intValue]<=7)
#define isBeforeIOS8 ([[[UIDevice currentDevice] systemVersion] intValue]<=8)
#define isBeforeIOS9 ([[[UIDevice currentDevice] systemVersion] intValue]<=9)

//判断ios7
#define IsIOS7Plus ([[UIDevice currentDevice].systemVersion floatValue]>=7.0?YES:NO)
#define IsIOS8Plus ([[UIDevice currentDevice].systemVersion floatValue]>=8.0?YES:NO)



//获取当前语言
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

//判断是否 Retina屏、设备是否%fhone 5、是否是iPad
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define IS_4INCH ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


//判断是真机还是模拟器
#if TARGET_OS_IPHONE
//iPhone Device
#endif
#if TARGET_IPHONE_SIMULATOR
//iPhone Simulator
#endif
//----------------------系统设备相关----------------------------

//----------------------内存相关----------------------------
//使用ARC和不使用ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif
//释放一个对象
#define SAFE_DELETE(P) if(P) { [P release], P = nil; }
#define SAFE_RELEASE(x) [x release];x=nil
//----------------------内存相关----------------------------



//----------------------图片相关----------------------------
//读取本地图片
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]
//定义UIImage对象
#define IMAGE(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]
//定义UIImage对象
#define ImageNamed(_pointer) [UIImage imageNamed:_pointer]
//可拉伸的图片
#define ResizableImage(name,top,left,bottom,right) [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake(top,left,bottom,right)]
#define ResizableImageWithMode(name,top,left,bottom,right,mode) [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake(top,left,bottom,right) resizingMode:mode]
//建议使用前两种宏定义,性能高于后者
//----------------------图片相关----------------------------

//----------------------颜色相关---------------------------
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)

//清除背景色
#define CLEARCOLOR [UIColor clearColor]
//----------------------颜色相关--------------------------

//----------------------其他----------------------------
//方正黑体简体字体定义
#define FONT(F) [UIFont fontWithName:@"FZHTJW--GB1-0" size:F]
//file
//读取文件的文本内容,默认编码为UTF-8
#define FileString(name,ext)            [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)] encoding:NSUTF8StringEncoding error:nil]
#define FileDictionary(name,ext)        [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)]]
#define FileArray(name,ext)             [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)]]

//G－C－D
#define GCD_BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define GCD_MAIN(block) dispatch_async(dispatch_get_main_queue(),block)

//Alert
#define ALERT(msg) [[[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil] show]

//由角度获取弧度 有弧度获取角度
#define degreesToRadian(x) (M_PI * (x) / 180.0)
#define radianToDegrees(radian) (radian*180.0)/(M_PI)

//轮播图间隔时间
#define PAGESCROLLTIME 6.0
//----------------------其他-------------------------------

//----------------------视图相关----------------------------
//设置需要粘贴的文字或图片
#define PasteString(string)   [[UIPasteboard generalPasteboard] setString:string];
#define PasteImage(image)     [[UIPasteboard generalPasteboard] setImage:image];

//得到视图的left top的X,Y坐标点
#define VIEW_TX(view) (view.frame.origin.x)
#define VIEW_TY(view) (view.frame.origin.y)

//得到视图的right bottom的X,Y坐标点
#define VIEW_BX(view) (view.frame.origin.x + view.frame.size.width)
#define VIEW_BY(view) (view.frame.origin.y + view.frame.size.height )

//得到视图的尺寸:宽度、高度
#define VIEW_W(view)  (view.frame.size.width)
#define VIEW_H(view)  (view.frame.size.height)
//得到frame的X,Y坐标点
#define FRAME_TX(frame)  (frame.origin.x)
#define FRAME_TY(frame)  (frame.origin.y)
//得到frame的宽度、高度
#define FRAME_W(frame)  (frame.size.width)
#define FRAME_H(frame)  (frame.size.height)
//----------------------视图相关----------------------------

//---------------------打印日志--------------------------
//Debug模式下打印日志,当前行,函数名
#if DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

//Debug模式下打印日志,当前行,函数名 并弹出一个警告
#ifdef DEBUG
#   define  WDLog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define NSLog(...)
#endif
//打印Frame
#define LogFrame(frame) NSLog(@"frame[X=%.1f,Y=%.1f,W=%.1f,H=%.1f",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height)
//打印Point
#define LogPoint(point) NSLog(@"Point[X=%.1f,Y=%.1f]",point.x,point.y)
//--







#endif
