//
//  ParamHeader.h
//  HIGHTONG
//
//  Created by Kael on 15/7/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#ifndef HIGHTONG_ParamHeader_h
#define HIGHTONG_ParamHeader_h
/*
 这个类主要是跟数据包 相关的宏定义
 */

//定义返回请求数据的block类型
typedef void (^ReturnValueBlock) (id returnValue);
typedef void (^ErrorCodeBlock) (id errorCode);
typedef void (^FailureBlock)();
typedef void (^NetWorkBlock)(BOOL netConnetState);

//网络请求 超时时间
#define kTIME_TEN 10
//视频播放超时时间 单位：s(秒)
#define kVIDEO_OUTTIME 10

//静态网页
#define web_About_Us        [NSString stringWithFormat:@"%@/shphone/pages/aboutus/index.html",YJ_Shphone]
#define web_User_Delegate   [NSString stringWithFormat:@"%@/shphone/pages/aboutus/index.html",YJ_Shphone]


//----------- 首页相关 -------------

//专家出诊
#define Home_ZHUANJIACHUZHEN       [[NSUserDefaults standardUserDefaults] objectForKey:@"chuzhenStr"]
//就诊助手
#define Home_JIUZHENZHUSHOU       [[NSUserDefaults standardUserDefaults] objectForKey:@"jiuzhenStr"]
//更多WiFi
#define Home_more_Wifi_Link        [[NSUserDefaults standardUserDefaults] objectForKey:@"moreWifiLink"]
//-----------------------------------
//医聚 shphone 的存储 前缀
#define YJ_Shphone  [[NSUserDefaults standardUserDefaults] objectForKey:@"shphone"]
//医聚的 互联网服务器的Portal
#define YJ_Shportal   [[NSUserDefaults standardUserDefaults] objectForKey:@"shportal"]
//appbase 基础PORTAL的
#define YJ_Appbase  [[NSUserDefaults standardUserDefaults] objectForKey:@"appbase"]
//机顶盒使用的 shtv
#define YJ_shtv  [[NSUserDefaults standardUserDefaults] objectForKey:@"shtv"]
//是否是公网
#define IS_PUBLIC_ENV  [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_PUBLIC_ENV"]
//#define IS_PUBLIC_ENV  YES

//屏聚系统的 图片Portal
#define IMGPORTAL   [[NSUserDefaults standardUserDefaults] objectForKey:@"IMGPORTAL"]
//医聚的 互联网服务器的Portal
#define PM_IMGPORTAL   [[NSUserDefaults standardUserDefaults] objectForKey:@"IMG_PUBLIC"]




//U值
#define U_KEY [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]

//健康视频的分享
#define HEALTHY_SHARE_LINK  [[NSUserDefaults standardUserDefaults] objectForKey:@"healthshareurl"] 

//*************************** 内外网相关的存储
//微医院的连接
#define kMicroHospital [[NSUserDefaults standardUserDefaults] objectForKey:@"microHospital"]
//############## 下列是web网页 但是需要传参 regionCode  和 instanceCode
//医院资讯
#define Home_Hospital_News     [NSString stringWithFormat:@"%@/shphone/pages/hosp_info/index.html",YJ_Shphone]
//医生排班
#define Home_Doctors_Order     [NSString stringWithFormat:@"%@/shphone/pages/doctor/index.html",YJ_Shphone]
//就诊导航
#define Home_Doctors_Guide     [NSString stringWithFormat:@"%@/shphone/pages/medical/index.html",YJ_Shphone]
//内外网切换 了解更多(友谊的小船说翻就翻) 网页
#define web_OutNet_More_Detail [NSString stringWithFormat:@"%@/shphone/pages/outnet/moredetail.html",YJ_Shphone];

//*******************

//API 版本 如果是0信息采集不加密 如果是1 信息采集需要加密
#define APIVERSION @"1"//IMG_PUBLIC
#define Encrypt_version @"00"//sign值的版本号

#define NSStringIMGFormat(URL)  [NSString stringWithFormat:@"%@%@",([URL rangeOfString:@"htt"].location != NSNotFound)?@"":IMGPORTAL,URL]
#define NSStringPM_IMGFormat(URL)  [NSString stringWithFormat:@"%@%@",([URL rangeOfString:@"htt"].location != NSNotFound)?@"":PM_IMGPORTAL,URL]



#pragma mark -------->> PORTAL类型配置

#define DEFAULT_WORK_PATH        @"/EPGPORTAL/"//正常EPG
#define DEFAULT_USER_PATH        @"/UPORTAL/"  //用户
#define DEFAULT_DEMAND_WORK_PATH @"/VODPORTAL/"//点播
#define DEFAULT_AD_WORK_PATH     @"/ADPORTAL"//广告系统
#define DEFAULT_YJ_WORK_PATH     @"/polymedicine/"//医院
#define DEFAULT_JXTV_BASE_WORK_PATH    @"/tvbiz/"//江西有线基础

#pragma mark -------->> 接口中常用参数

#define kDev_ID  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"dev_ID"]]
#define dev_ID (kDev_ID.length > 0 ? kDev_ID : @"12345678")

#define ICS_deviceID [[NSUserDefaults standardUserDefaults] objectForKey:@"ICS_deviceID"]








//--------EPG
#pragma mark -------->> EPGPORTAL  数据接口类型

#define EPG_CHANNELALLLIST    @"channelAllList"//全部节目列表
#define EPG_EVENTLIST         @"eventList"
#define EPG_CHANNELLIST       @"channelList"
#define EPG_CHANNELQUERY      @"channelQuery"
#define EPG_CURRENTEVENTLIST  @"currentEventList"//当前后继节目列表
#define EPG_TVPLAY            @"tvPlay"
#define EPG_TVPLAYEX          @"tvPlayEX"
#define EPG_PAUSETVPLAY       @"pauseTvPlay"
#define EPG_PAUSETVPLAYEX     @"pauseTvPlayEX"
#define EPG_EVENTPLAY         @"eventPlay"
#define EPG_EVENTPLAYEX       @"eventPlayEX"
#define EPG_TYPELIST          @"typeList"
#define EPG_CONFIG            @"config"
#define EPG_SOFIWAREDOWN      @"softwareDown"
#define EPG_SYSTEMTIME        @"systemTime"
#define EPG_EVENTSEARCH       @"eventSearch"
#define EPG_TOPSEARCHWORDS    @"topSearchWords"
#define EPG_ADDTVFAVORATE     @"addTvFavorite"
#define EPG_DELTVFAVORATE     @"deleteTvFavorite"
#define EPG_TVFAVORATELIST    @"tvFavoriteList"
#define EPG_TVPLAYLIST        @"tvPlayList"
#define EPG_DELTVPLAY         @"deleteTvPlay"
#define EPG_EVENTSTOP         @"eventStop"
#define EPG_EVENTPLAYLIST     @"eventPlayList"
#define EPG_DELEVENTPLAY      @"deleteEventPlay"
#define EPG_ADDEVENTFAVORATE  @"addEventFavorite"
#define EPG_DELEVENTFAVORATE  @"deleteEventFavorite"
#define EPG_EVENTFAVORATELIST @"eventFavoriteList"
#define EPG_HOTCHANNELLIST    @"hotChannelList"
#define EPG_ADDEVENTRESERVE   @"addEventReserve"
#define EPG_DELEVENTRESERVE   @"deleteEventReserve"
#define EPG_EVENTRESERVELIST  @"eventReserveList"
#define EPG_ADLIST            @"adList"
#define EPG_UPDATE_TVFAVORATE @"updateTvFavorite"

#define EPG_REGION_LIST       @"regionList"
#define EPG_FIND_REGION       @"findRegion"
#define EPG_REGION_INFO       @"regionInfo"
//---------------EPG END

//--------PUB
#pragma mark -------->> PUB  数据接口类型

#define PUB_COMMON_NAVIGATIONCHANNELLIST    @"common/navigationChannelList" //2.1 导航频道列表
#define PUB_COMMON_HOMEPAGECHANNELLIST      @"common/homePageChannelList"   //2.2 首页推荐频道列表
#define PUB_COMMON_PROGRAMINFOLIST          @"common/programInfoList"       //2.3 节目列表
#define PUB_COMMON_PROGRAMINFOPLAYURL       @"common/programInfoPlayUrl"    //2.4 节目播放链接
#define PUB_COMMON_INSERTFAVORITES          @"common/insertFavorites"       //2.5 节目收藏
#define PUB_COMMON_INSERTPRAISE             @"common/insertPraise"          //2.6 节目点赞
#define PUB_COMMON_ISORNOTFAVORITES         @"common/isOrNotFavorites"      //2.7 查询是否收藏
#define PUB_COMMON_ISORNOTPRAISE            @"common/isOrNotPraise"         //2.8 查询是否点赞
#define PUB_COMMON_FAVORITESINFOLIST        @"common/favoritesInfoList"     //2.9 收藏列表
#define PUB_COMMON_PRAISEINFOLIST           @"common/praiseInfoList"        //2.10 点赞列表
#define PUB_COMMON_CANCELPRAISE             @"common/cancelPraise"          //2.11 取消节目点赞
#define PUB_COMMON_CANCELFAVORITES          @"common/cancelFavorites"       //2.12 取消节目收藏
#define PUB_COMMON_VODRANKBYWEEK            @"common/vodRankByWeek"         //2.14 获取点播节目周排行榜
#define PUB_COMMON_GETRESULTBYKEYWORD       @"common/getResultByKeyword"    //2.15 获取关键词搜索结果


//---------------PUB END



#pragma mark -------->> VODPORTAL  数据接口类型

#define VOD_LIST              @"VOD_LIST"
#define CATEGORY_LIST         @"CATEGORY_LIST"        //点播分类
#define VOD_OPER_PROGRAM_LIST @"VOD_OPER_PROGRAM_LIST"//点播运营组节目列表

/*---------珠江数码--------*/  //cap change 3.7接口
#define VOD_OPERATION_LIST         @"operationList"        //点播运营组2.1
#define VOD_TOPIC_LIST             @"topicList"            //点播专题2.2
#define VOD_TOPIC_DETAIL           @"topicDetail"          //点播专题详情 2.3
#define VOD_CATEGORY_LIST          @"categoryList"          //点播分类2.4
#define VOD_OPER_CATEGORY_LIST     @"operationCategoryList" //点播运营组分类 2.5
#define VOD_CATEGORY_OPER_LIST     @"categoryOperationList" //点播分类运营组...2.6
#define VOD_CATEGORY_TOPICLIST     @"categoryTopicList"     //1.1	点播分类专题列表
#define VOD_OPER_MENU              @"operationMenu"        //点播运营组菜单（暂不实现）...2.7
#define VOD_CATEGORY_MENU          @"categoryMenu"          //点播分类菜单（暂不实现）...2.8
#define VOD_PROGRAM_LIST           @"programList"          //点播节目列表...2.9
#define VOD_TOPIC_PROGRAM_LIST     @"topicProgramList"      //点播专题节目列表...2.10

#define VOD_SEARCH                 @"vodSearch"            //点播搜索 2.11
#define VOD_DETAIL                 @"programDetail"        //点播视频详细信息  2.12
#define VOD_PLAY                   @"vodPlay"              //点播播放2.13
#define VOD_INQUIRY                @"inquiry"              //点播询价2.14
#define VOD_PAY_PLAY               @"payPlay"              //付费点播2.15
#define VOD_STOP                   @"interruptPlay"        //点播停止2.16
#define VOD_PLAY_LIST              @"playRecordList"        //点播播放记录列表2.17
#define DEL_VOD_PLAY               @"deletePlayRecord"      //删除点播播放记录2.18
#define ADD_VOD_FAVORITE           @"addFavorite"          //增加点播收藏2.19
#define DEL_VOD_FAVORITE           @"deleteFavorite"        //删除点播收藏2.20
#define VOD_FAVORITE_LIST          @"favoriteList"          //点播收藏列表2.21
#define VOD_ASSCCIATION            @"associationProgramList"//点播关联影片列表2.22

#define VOD_TOP_SEARCH_KEY         @"topSearchWrods"        //点播热门搜索关键词...  2.23
#define VOD_Order_Product          @"orderProduct"          //点播销售品订购... 2.24
#define VOD_Praise_Degrade         @"praiseDegrade"        // 点赞吐槽影片...2.25
#define VOD_Operation_Product_List @"operationProductList"  //运营组销售品列表...2.26

#define VOD_OperationDetail        @"operationDetail"
#define VOD_NewPlayRecordList      @"newPlayRecordList"
#define VOD_NewFavorateList        @"newFavoriteList"
#define VOD_HomeConfigList         @"homeConfigList"
#define VOD_MenuConfigList         @"menuConfigList"
#define VOD_PraiseDegradeDetail    @"praiseDegradeDetail"
#define VOD_UpLoadOrder            @"uploadOrder"
#define VOD_HospitalInfo           @"hospitalInfo"
#define VOD_GetHospital            @"getHospital"
#define VOD_HospitalList           @"hospitalList"

//-------------------------------UPORTAL---------------------------
#pragma mark -------->> UPORTAL  数据接口类型

#define getChallenge             @"getChallenge"            //获取登录挑战字
#define userLogin                @"userLogin"               //用户登陆
#define mobileLogin              @"mobileLogin"             //手机号登陆
#define guestLogin               @"guestLogin"              //游客登陆
#define signIn                   @"signIn"                  //用户签到
#define UPlogout                 @"logout"                  //用户注销
#define deviceActive             @"deviceActive"            //设备激活
//#define setLockNumber            @"setLockNumber"           //设置付费密码锁
//#define resetLockNumber          @"resetLockNumber"         //重置付费密码锁
//#define isSetLockNumber          @"isSetLockNumber"         //查询付费密码锁状态
#define queryUserInfo            @"queryUserInfo"           //查询用户信息
#define changeUserPwd            @"changeUserPwd"           //修改用户密码
#define verityCode               @"authCode"                //手机验证码
#define mobileRegister           @"mobileRegister"          //手机号注册
#define resetUserPwd             @"resetUserPwd"            //用户密码重置
#define alterMobilePhone         @"alterMobilePhone"        //变更手机号

#define USER_LOGIN_EX3           @"USER_LOGIN_EX3"          //增强版 用户登录

#define uploadHeadPhoto          @"uploadHeadPhoto"         //上传头像
#define updateNickName           @"updateNickName"          //修改昵称
#define helpBack                 @"helpBack"                //帮助反馈
#define updateUserInfo           @"updateUserInfo"          //修改用户信息
#define User_Check_Nickname      @"checkNickName"           //2.22	校验昵称
#define User_Check_Authcode      @"checkAuthCode"           //2.23	验证手机验证码
#define UGetToken                @"getToken"                //根据identityToken获取鉴权token接口
#define User_Is_SignIn           @"isSignIn"                //2.25	查询用户今日是否已经签到
#define checkPwd                 @"checkPwd"                //查询用户密码
#define Wifi_Auth                @"wifiAuth"                //2.27	无线认证（验证+注册）
#define authCodeLogin            @"authCodeLogin"           //动态口令登陆
#define authCodeAlterMobilePhone @"authCodeAlterMobilePhone"//动态口令变更手机号
#define getTemporalKey           @"getTemporalKey"          //获取临时密钥
#define temporalKeyLogin         @"temporalKeyLogin"        //临时密钥登录（验证+注册+登录）

#define bindJxTV                 @"bindJxTV"                //江西TV用户绑定
#define unBindJxTV               @"unBindJxTV"              //江西TV用户解除绑定
#define userBindInfoJxTV         @"userBindInfoJxTV"        //获取江西TV用户绑定信息
#define userBindStatusJxTV       @"userBindStatusJxTV"      //查询江西TV用户是否绑定

//广告系统
#pragma mark -------->> ADPORTAL 数据接口类型

#define AD_USER_GROUP @"user_group"
#define AD_INFO       @"ad_info"
#define AD_CONFIG     @"config"
#define AD_VODTYPE    @"2"
#define AD_LIVETYPE   @"1"


#pragma mark - 广告区分标识

#define PANGEA_PHONE_BOOT_IMAGE              @"PANGEA_PHONE_BOOT_IMAGE"//屏聚手机端开机广告
#define PANGEA_PHONE_HOMEPAGE_CAROUSEL_IMAGE @"PANGEA_PHONE_HOMEPAGE_CAROUSEL_IMAGE"//屏聚手机端首页轮播图广告
#define PANGEA_PHONE_LIVE_PLAYER_MENU        @"PANGEA_PHONE_LIVE_PLAYER_MENU"//屏聚手机端直播播放器菜单广告
#define PANGEA_PHONE_SOTV_PAUSE              @"PANGEA_PHONE_SOTV_PAUSE"//屏聚手机端回看暂停广告
#define PANGEA_PHONE_SOTV_HEAD_PATCH         @"PANGEA_PHONE_SOTV_HEAD_PATCH"//屏聚手机端回看片前贴片广告
#define PANGEA_PHONE_SOTV_TAIL_PATCH         @"PANGEA_PHONE_SOTV_TAIL_PATCH"//屏聚手机端回看片尾贴片广告
#define PANGEA_PHONE_VOD_PAUSE               @"PANGEA_PHONE_VOD_PAUSE"//屏聚手机端点播暂停广告
#define PANGEA_PHONE_VOD_HEAD_PATCH          @"PANGEA_PHONE_VOD_HEAD_PATCH"//屏聚手机端点播片前贴片广告
#define PANGEA_PHONE_VOD_TAIL_PATCH          @"PANGEA_PHONE_VOD_TAIL_PATCH"//屏聚手机端点播片尾贴片广告
#define PANGEA_PHONE_EXIT_APP                @"PANGEA_PHONE_EXIT_APP"//屏聚手机端退出应用广告
#define PANGEA_PHONE_VOD_PLAYER_MENU         @"PANGEA_PHONE_VOD_PLAYER_MENU"//点播菜单广告位编码
#define PANGEA_PHONE_LIVE_HEAD_PATCH         @"PANGEA_PHONE_LIVE_HEAD_PATCH"//屏聚网站(手机端)直播片前贴片广告


#define HomeADSLiderPage @"HomeADSLiderPage"


//-----------------医聚-------------------------------//
#pragma mark -------->> 医聚 PORTAL  数据接口类型

#define YJ_INTRODUCE_DETAIL                       @"introduce/detail"                      //2.1	首页医院logo和名称
#define YJ_INTRODUCE_LIST    @"introduce/list" //2.22	医院列表

#define YJ_MOBILELOGIN                            @"mobileLogin"                           //2.2	手机登录
#define YJ_DEPARTMENT_ALLLIST                     @"department/allList"                    //2.3	科室列表
//#define YJ_DEPARTMENT_DETAIL                      @"department/detail"                     //2.4	科室详情
#define YJ_BUILDING_SEARCH                        @"building/search"                       //2.5	根据楼栋名称查询科室
//#define YJ_DEPARTMENT_LIST                        @"department/list"                       //2.6	科室搜索
#define YJ_EXPERT_ALLLIST                         @"expert/allList"                        //2.7	专家列表
#define YJ_EXPERT_DETAIL                          @"expert/detail"                         //2.8	专家详情
#define YJ_EXPERT_BYNAMELIST                      @"expert/byNameList"                     //2.9	专家搜索
#define YJ_EXPERT_BYDEPTIDLIST                    @"expert/byDeptIdList"                   //2.10	根据科室查询医生
//#define YJ_HOTWORDS_LIST                          @"hotwords/list"                         //2.11	热词列表
#define YJ_NEWS_LIST                              @"news/list"                             //2.12	医讯列表

#define YJ_VISIT_MENU  @"visit/menu"//新增————2.10	就诊须知【菜单】

#define YJ_EXPERT_NOMINATE  @"expert/nominate"  //新增————2.12	名医列表

#define YJ_BUILDING_MENU  @"building/menu"//新增————2.13	导诊图-大楼列表

#define YJ_BUILDING_FLOORS      @"building/floors"  //新增————2.14	导诊图-楼层列表

//#define //2.13	医讯详情
#define YJ_PATIENT_LIST                           @"patient/list"                          //2.14	就诊卡列表
#define YJ_PATIENT_DETAIL                         @"patient/detail"                        //2.15	就诊卡详情
#define YJ_PATIENT_SAVE                           @"patient/save"                          //2.16	就诊卡绑定
#define YJ_PATIENT_EDIT                           @"patient/edit"                          //2.17	就诊卡解除绑定
//#define YJ_COLLECTION_SAVE                        @"collection/save"                       //2.18	关注医生
#define YJ_COLLECTION_LIST                        @"collection/list"                       //2.19	我的关注医生列表
#define YJ_COLLECTION_SAVE @"collection/save"//2.20	关注/取消关注医生

//#define YJ_COLLECTION_ISEXIST                     @"collection/isExist"                    //2.20	查询该医生是否被关注
//#define YJ_COLLECTION_EDIT                        @"collection/edit"                       //2.21	取消医生关注
//#define YJ_ABOUT_DETAIL                           @"about/detail"                          //2.22	关于我们
#define YJ_SUGGESTION_SAVE                        @"suggestion/save"                       //2.23	意见反馈
#define YJ_REGION_POPULARIZEIMAGE     @"region/popularizeimage"  //2.23	推广图片

#define YJ_NEWS_SLIDE   @"news/slide"   //2.24	资讯轮播图片

#define YJ_MENU_LIST                              @"menu/list"                             //2.24	版本菜单配置列表
//#define YJ_SMS_SEND                               @"sms/send"                              //2.25	发送短信
#define YJ_INTRODUCE_INFO                         @"introduce/info"                        //2.28	就诊导航界面医院电话与交通信息
#define YJ_REPORT_SMS_SEND                        @"report/sms/send"                       //2.29	报告单查看验证码短信
#define YJ_REPORT_SMS_VALID                       @"report/sms/valid"                      //2.30	报告单查看验证码校验

#define YJ_HEALTHCARE_CATEGORY                    @"healthCare/category"      //2.26(V1.3)  养生保健分类
#define YJ_HEALTHCARE_LIST                        @"healthCare/list"          //2.27(V1.3)  养生保健列表
#define YJ_HEALTHCARE_SLIDE                       @"healthCare/slide"         //2.28(V1.3)  养生保健轮播列表
#define YJ_HEALTHCARE_NEWSLIST                    @"healthCare/newsList"      //2.29(V1.3)  养生保健资讯列表
#define YJ_INTRODUCE_AREALIST                     @"introduce/areaList"       //2.30(V1.3)  医院关联区域列表
#define YJ_HEALTHCARE_PULL                        @"healthCare/pull"          //2.33(V1.5)  养生保健拉取推荐（单内容推荐）





#define YJ_INTRODUCE_NEARBY @"introduce/nearby" //新增 3.1	附近医院

#define YJ_CDN_VIDEO_URL                          @"cdn/video/url"                         //3.1	获取视频播放地址
#define YJ_HEALTHY_TYPELIST                       @"healthy/typelist"                      //3.2	获取视频分类列表
#define YJ_HEALTHY_HEALTHYVIDEOLIST               @"healthy/healthyVideolist"              //3.3	获取视频分页列表
#define YJ_CDN_VIDEO_RECOMMEND                    @"cdn/video/recommend"                   //3.4	视频推荐
#define YJ_CDN_VIDEO_ADDFAVORITE                  @"cdn/video/addfavorite"                 //3.5	加入收藏夹
#define YJ_CDN_VIDEO_LISTFAVORITE                 @"cdn/video/listfavorite"                //3.6	获取收藏夹列表
#define YJ_CDN_VIDEO_DELFAVORITE                  @"cdn/video/delfavorite"                 //3.7	删除收藏夹中的记录
#define YJ_CDN_VIDEO_CLEANFAVORITE                @"cdn/video/cleanfavorite"               //3.8	清空收藏夹
#define YJ_HEALTHY_TYPELISTVIDEO                  @"healthy/typelistVideo"                 //3.9	获取视频分类与视频
#define YJ_REGISTRATIONCOST_LIST                  @"registrationcost/list"                 //4.1	我的费用[挂号费]
#define YJ_INSPECTCOST_LIST                       @"inspectcost/list"                      //4.2	我的费用[检查费]
#define YJ_HOSPITALRECORD_LIST                    @"hospitalrecord/list"                   //5.1	就诊记录
#define YJ_TESTBILL_LIST                          @"testbill/list"                         //6.1	检验单
#define YJ_TESTBILL_DELETE                        @"testbill/delete"                       //6.2	检验单删除
#define YJ_CHECKBILL_LIST                         @"checkbill/list"                        //6.3	检查单
#define YJ_CHECKBILL_DELETE                       @"checkbill/delete"                      //6.4	检查单删除
//#define ---------------------------- 预约挂号接口 ----------------------------
#define YJ_APPOINTMENT_TIMELINE_TIMELINEDATE      @"appointment/timeline/timelinedate"     //7.1	获取医生排班日期列表
#define YJ_APPOINTMENT_TIMELINE_EXPERTSUMMARY     @"appointment/timeline/expertsummary"    //7.2	获取医生排班汇总信息
#define YJ_APPOINTMENT_TIMELINE_EXPERTDAILY       @"appointment/timeline/expertdaily"      //7.3	获取医生排班日信息列表
#define YJ_APPOINTMENT_TIMELINE_DEPERTMENTSUMMARY @"appointment/timeline/depertmentsummary"//7.4	获取科室医生排班汇总信息列表
#define YJ_APPOINTMENT_ORDER_CREATE               @"appointment/order/create"              //7.5	预约挂号
#define YJ_APPOINTMENT_ORDER_LIST                 @"appointment/order/list"                //7.6	获取挂号订单列表
#define YJ_APPOINTMENT_ORDER_CANCEL               @"appointment/order/cancel"              //7.7	预约挂号取消
#define YJ_APPOINTMENT_ORDER_REMOVE               @"appointment/order/remove"              //7.8	删除预约挂号订单
#define YJ_APPOINTMENT_CHECKORDER_LIST            @"appointment/checkorder/list"           //7.9	获取检查订单列表
#define YJ_APPOINTMENT_CHECKORDER_REMOVE          @"appointment/checkorder/remove"         //7.10	删除检查订单
#define YJ_APPOINTMENT_ORDER_DETAIL               @"appointment/order/detail"              //7.11	获取预约挂号与检查订单明细

#define YJ_PUSHTAG_BIND                           @"pushtag/bind"                          //8.2	消息推送绑定
#define YJ_PUSHMSG_DETAIL                @"pushmsg/detail"               //8.3	消息详情

#define YJ_TREATMENT_FIND                @"treatment/find"               //9.1	排队叫号查询
#define YJ_TREATMENT_CALLED              @"treatment/called"             //9.2	排队叫号通知完成

#define YJ_HEALTHY_TOP_LIST              @"healthy/toplist"              //4.10	获取轮播节目列表(新增接口)

#define YJ_HEALTHY_YEAR_LIST             @"healthy/yearlist"             //4.11	获取视频发布年份列表(新增接口)

#define YJ_HEALTHY_TAG_LIST              @"healthy/taglist"              //4.12	获取视频疾病标签列表(新增接口)


#define YJ_HEALTHYVOD_GETCHILDCHANNEL         @"healthyVod/getChildChannel"         //2.1  获取子频道列表

#define YJ_HEALTHYVOD_CHANNELINFO             @"healthyVod/channelInfo"             //2.2  获取频道信息

#define YJ_HEALTHYVOD_CHANNELFILTERS          @"healthyVod/channelFilters"          //2.3  获取频道过滤器列表

#define YJ_HEALTHYVOD_CHANNELPROGRAMLIST      @"healthyVod/channelProgramList"      //2.4  获取子频道节目列表

#define YJ_HEALTHYVOD_PROGRAMLIST             @"healthyVod/programList"             //2.5  获取节目列表

#define YJ_HEALTHYVOD_MEDIAPLAYLIST           @"healthyVod/mediaPlayList"           //2.6  获取节目视频播放地址

#define YJ_HEALTHYVOD_RECOMMENDPROGRAM        @"healthyVod/recommendProgram"        //2.7  推荐节目

#define YJ_HEALTHYVOD_LISTFAVORITE            @"healthyVod/listfavorite"            //2.8  获取收藏夹中节目

#define YJ_HEALTHYVOD_ADDFAVORITE             @"healthyVod/addfavorite"             //2.9  节目加入收藏夹

#define YJ_HEALTHYVOD_DELFAVORITE             @"healthyVod/delfavorite"             //2.10 删除收藏夹中的节目

#define YJ_HEALTHYVOD_CLEANFAVORITE           @"healthyVod/cleanfavorite"           //2.11 清空收藏夹

#define YJ_HEALTHYVOD_GETTOPCHANNEL           @"healthyVod/getTopChannel"           //2.14 获取一级频道


#define YJ_HEALTHYVOD_GETSELECTEDCHANNEL      @"healthyVod/getSelectedChannel"      //2.15 获取精选频道列表

#define YJ_HEALTHYVOD_GETHOTCHANNEL           @"healthyVod/getHotChannel"           //2.16 获取热门频道和节目列表

//-----------------APP基础接口-------------------------------//
#pragma mark -------->> APP基础接口 PORTAL  数据接口类型

#define CN_COMMON_SYSTEMTIME                  @"common/systemTime"                  //2.1	 系统时间
#define CN_COMMON_SOFTWAREDOWN                @"common/softwareDown"                //2.2	 终端软件下载
#define CN_COMMON_SLIDESHOWLIST               @"common/slideshowList"               //2.3	 轮播图列表
#define CN_COMMON_SUGGEST                     @"common/suggest"                     //2.4	 帮助反馈
#define CN_COMMON_ADLIST                      @"common/adList"                      //2.5    广告列表
#define CN_COMMON_APPNAME                     @"common/appName"                     //2.6    产品名称
#define CN_COMMON_APPINFO                     @"common/appInfo"                     //2.7    产品信息
#define CN_COMMON_APPCONFIG                   @"common/appConfig"                   //2.8    产品配置
#define CN_COMMON_MODULECONFIG                @"common/moduleConfig"                //2.9    首页模块列表
#define CN_INSTANCE_INSTANCELIST              @"instance/instanceList"              //3.1    实体列表
#define CN_INSTANCE_INSTANCEINFO              @"instance/instanceInfo"              //3.2    实体信息
#define CN_INSTANCE_INSTANCELOCATION          @"instance/instanceLocation"          //3.3    实体定位
#define CN_OPERATOR_OPERATORINFO              @"operator/operatorInfo"              //4.1    运营商信息
#define CN_INNER_SMSTEMPLETE                  @"inner/smsTemplete"                  //5.1    短信模版
#define CN_INNER_APPLIST                      @"inner/appList"                      //5.2    产品列表
#define CN_INNER_MESSAGEINFO                  @"inner/messageInfo"                  //5.3    message信息

#define CN_COMMON_BOTTOMIMAGE                 @"common/bottomImage"                 //5.5    首页底部图

//-----------------UGC基础接口-------------------------------//
#pragma mark -------->> UGC基础接口 PORTAL  数据接口类型

#define UGC_COMMON_ISVALIDUSER                @"common/isValidUser"                 //2.1    判断是否合法主播
#define UGC_COMMON_IDENTIFICATION             @"common/identification"              //2.2    身份认证
#define UGC_COMMON_STARTLIVE                  @"common/startLive"                   //2.3    开始播放
#define UGC_COMMON_STOPLIVE                   @"common/stopLive"                    //2.4    停止播放
#define UGC_COMMON_INSERTPROGRAM              @"common/insertProgram"               //2.5    点播节目上传
#define UGC_COMMON_UPDATEPROGRAM              @"common/updateProgram"               //2.6    点播节目修改
#define UGC_COMMON_GETPROGRAMLISTBYUSERINFO   @"common/getProgramListByUserInfo"    //2.7    用户获取自己的点播列表
#define UGC_COMMON_GETCHANNELLIST             @"common/getChannelList"              //2.8    获取直播列表
#define UGC_COMMON_GETPROGRAMLIST             @"common/getProgramList"              //2.9    获取点播列表
#define UGC_COMMON_GETTAGLIST                 @"common/getTagList"                  //2.10   获取标签列表
#define UGC_COMMON_DELETEPROGRAM              @"common/deleteProgram"               //2.11   删除视频
#define UGC_COMMON_GETCATEGORYLIST            @"common/getCategoryList"             //2.12   获取分类列表
#define UGC_COMMON_GETLIVEURL                 @"common/getLiveUrl"                  //2.13   直播播放
#define UGC_COMMON_GETPROGRAMURL              @"common/getProgramUrl"               //2.14   点播播放
#define UGC_COMMON_UGCCALLBACK                @"common/ugcCallBack"                 //2.15   当虹云回调接口
#define UGC_COMMON_GETTOKEN                   @"common/getToken"                    //2.16   获取token接口
#define UGC_COMMON_GETACADEMICTITLE           @"common/getAcademicTitle"            //2.17   获取医生职称接口
#define UGC_COMMON_GETPROGRAMPLAYCOUNT        @"common/getProgramPlayCount"         //2.18   获取点播播放次数接口
#define UGC_COMMON_GETCHANNELCOUNT            @"common/getChannelCount"             //2.19   获取直播在线人数接口
#define UGC_COMMON_GETCOLUMNINFO              @"common/getColumnInfo"               //2.20   获取栏目信息接口
#define UGC_COMMON_GETCOLUMNINFOCONTENT       @"common/getColumnInfoContent"        //2.21   获取栏目内容信息接口




//-----------------江西有线基础接口----------------------------------//
#pragma mark--------->>xtvbase

#define JXTV_PROBLEM_PROBLEMLIST       @"problem/problemList"      //1.1	问题列表接口

#define JXTV_PROBLEM_PROBLEMBYID       @"problem/problemById"      //1.1	根据ID获取问题解答

#define JXTV_BUSINESS_CITYLIST         @"business/cityList"        //1.1	查询江西省下的所有市区

#define JXTV_BUSINESS_BUSINESSBYCITYID @"business/businessByCityId"//1.1	查询市区所有的营业厅

#define JXTV_BUSINESS_INSTALLINFO      @"business/installInfo"     //1.1	报装接口



//-----------------COMMONPORTAL统一门户接口-------------------------------//
#pragma mark -------->> COMMONPORTAL  数据接口类型

#define  UN_PROJECTVERSION                 @"projectVersion"
#define  UN_PROJECTLIST                    @"projectList"
#define  UN_REGIONVERSION                  @"regionVersion"
#define  UN_REGIONLIST                     @"regionList"
#define  UN_RESETCONFIG                    @"resetConfig"
#endif


//----------------------------------手机推送通知接口---------------------//


#pragma mark -------------手机推送通知接口

//关于相关推送的一些define值
#define MSG_CHANNELID                          [[NSUserDefaults standardUserDefaults] objectForKey:@"channelId"]
#define MSG_DEVICETOKEN                        [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]               
#define MSG_USERMOBILE                         [[NSUserDefaults standardUserDefaults] objectForKey:@"msgMobile"]
#define MSG_SUCCESSPORTAL                      [[NSUserDefaults standardUserDefaults] boolForKey:@"isSuccessPortal"]
#define MSG_USERINFO                           [[NSUserDefaults standardUserDefaults] objectForKey:@"msgUserInfo"]
#define MSG_ISCLOUDNOTI                        [[NSUserDefaults standardUserDefaults] boolForKey:@"isCloudNoti"]
#define MSG_ISBACKGROUND                        @"1"
#define MSG_ISFOREGROUND                        @"0"



#define MSG_GETGROUPS                       @"/msg/getGroups"
#define MSG_BINDGOUP                        @"/msg/bindGoup"
#define MSG_TOGROUPS                        @"/msg/toGroups"
#define MSG_UNBINDGOUP                      @"/msg/unBindGoup"
#define MSG_TOSINGLEDEVICE                  @"/msg/toSingleDevice"
#define MSG_BINDINGUPDATE                   @"/msg/bindingUpdate"


