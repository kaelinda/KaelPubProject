//
//  OfficialDefine.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/10/13.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#ifndef OfficialDefine_h
#define OfficialDefine_h
//各位看官如有问题咨询，请浏览officialDefineDoc文档的解释


//----------------------颜色相关---------------------------
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
//颜色
//主题色
//特别提醒的颜色
#define HT_COLOR_REMIND          UIColorFromRGB(0xff9c00)
//文字
//用于主要文字信息和标题文字颜色
#define HT_COLOR_FONT_FIRST      UIColorFromRGB(0x333333)
//用于次要文字信息颜色
#define HT_COLOR_FONT_SECOND     UIColorFromRGB(0x666666)
//用于辅助性文字信息颜色
#define HT_COLOR_FONT_ASSIST     UIColorFromRGB(0x999999)
//用于表单提示文字信息和禁用颜色
#define HT_COLOR_FONT_WARNING    UIColorFromRGB(0xcccccc)
//用于反白文字颜色
#define HT_COLOR_FONT_INVERSE    UIColorFromRGB(0xffffff)
//用于弹出框弹出时的背景颜色和吐司的颜色
#define HT_COLOR_ALERTCUSTOM     UIColorFromRGB(0x000000)
//用于APP的背景色
#define HT_COLOR_BACKGROUND_APP  UIColorFromRGB(0xf7f7f7)
//用于分割线的颜色
#define HT_COLOR_SPLITLINE       UIColorFromRGB(0xe8e8e8)
//02 控件规范

#define kSearch_bg_color   [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.00]

#pragma mark - 尺寸适配 自行缩放（kDeviceRate）
#pragma mark -02-1-1顶部导航
//状态条的高度
#define HT_HIGHT_STATUSBAR       20
//naviBar的高度
#define HT_HIGHT_NAVIBAR   44
//naviBar上的按钮的宽度
#define HT_WIGHT_BUTTON_NAVIBAR 28
//naviBar上的按钮的高度
#define HT_HIGHT_BUTTON_NAVIBAR  28
//naviBar上的logo的宽度
#define HT_WIGHT_LOGO_NAVIBAR   80
//naviBar上的logo的高度
#define HT_HIGHT_LOGO_NAVIBAR   20
//naviBar上的距离window的间隙
#define HT_SPACE_TOWINDOW_NAVIBAR  12 *kDeviceRate
//naviBar相隔间隙
#define HT_SPACE_INTERVAL_NAVIBAR   15 *kDeviceRate

#define HT_HIGHT_POSTERVIEW       188

#pragma mark -02-4-二级导航
//二级导航栏距离左边距的间隙
#define HT_SPACE_TOWINDOW_SECONDNAVIBAR    18
//二级导航栏间隔间隙
#define HT_SPACE_FONTINTERVAL_SECONDNAVIBAR 22
//二级导航栏的宽度
#define HT_WIGHT_SECONDNAVIBAR        375
//二级导航栏的高度
#define HT_HIGHT_SECONDNAVIBAR           40
//二级导航栏右侧按钮的宽度
#define HT_WIGHT_RIGHTBUTTON_SECONDNAVIBAR  46
//二级导航栏右侧按钮的高度
#define HT_HIGHT_RIGHTBUTTON_SECONDNAVIBAR  40
//二级导航栏分割线颜色
#define HT_COLOR_SPLITLINE_SECONDNAVIBAR    UIColorFromRGB(0xe8e8e8)
#pragma mark -02-6-搜索框（1）
//搜索框距离左侧间隙
#define HT_SPACE_LEFT_SEARCHBAR     12
//搜索框距离右侧间隙
#define HT_SPACE_RIGHT_SEARCHBAR    35
//搜索框高度
#define HT_HIGHT_SEARCHBAR          29
#pragma mark -02-6-搜索框（2）
//搜索二级框距离边缘线间隙
#define HT_SPACE_TOWINDOW_SEARCHVIEW   12
//搜索二级框宽度
#define HT_WIGHT_SEARCHVIEW          375
//搜索二级框高度
#define HT_HIGHT_SEARCHVIEW          29
//搜索二级框上的搜索按钮宽度
#define HT_WIGHT_SEARCHBUTTON_SEARCHVIEW   55
//搜索二级框上的搜索按钮高度
#define HT_HIGHT_SEARCHBUTTON_SEARCHVIEW   29
//搜索二级框上的放大镜按钮宽度
#define HT_WIGHT_SEARCHICON_SEARCHVIEW    16
//搜索二级框上的放大镜按钮高度
#define HT_HIGHT_SEARCHICON_SEARCHVIEW    16
//适用于就诊导航、出诊专家的相关搜索



#pragma mark -02-7-按钮
//登录按钮的宽高
#define HT_WIGHT_LOGINBTN      351
#define HT_HIGHT_LOGINBTN      40
//注册按钮的宽高
#define HT_WIGHT_REGISTERBTN   351
#define HT_HIGHT_REGISTERBTN   40
//喜爱频道删除的宽高
#define HT_WIGHT_FAVORDELEBTN  351
#define HT_HIGHT_FAVORDELEBTN  40
//播放记录删除按钮的宽高
#define HT_WIGHT_RECORDDELEBTN  170
#define HT_HIGHT_RECORDDELEBTN   40


#pragma mark -二级导航展开标签
//二级导航展开标签的宽度高度
#define HT_WIGHT_TAB_SECONDNAVIBAR 76
#define HT_HIGHT_TAB_SECONDNAVIBAR 31
//02-7-弹出框、吐司
//弹出框的宽高
#define HT_WIGHT_ALERT  295
#define HT_HIGHT_ALERT  135//(这个是大约值，高度是撑起的)
//距离弹出框上方间隙
#define HT_SPACE_TOP_TOALERT  20
//距离弹出框边缘的间隙
#define HT_SPACE_EDGE_TOALERT  30
//距离弹出框底部的间隙
#define HT_SPACE_BOTTOM_TOALERT 20
//距离底部弹出框的按钮的高度
#define HT_HIGHT_BOTTOMBTN_TOALERT 40
//距离吐司的高度
#define HT_HIGHT_CUSTOM   30
//距离吐司的边缘间隙
#define HT_SPACE_EDGE_TOCUSTOM  15



#pragma mark -03-文字及排版规范
//特殊字号
#define HT_FONT_SPECIAL             [UIFont systemFontOfSize:20*kDeviceRate]
//一级字号
#define HT_FONT_FIRST               [UIFont systemFontOfSize:18*kDeviceRate]
//二级字号
#define HT_FONT_SECOND              [UIFont systemFontOfSize:16*kDeviceRate]
//三级字号
#define HT_FONT_THIRD               [UIFont systemFontOfSize:14*kDeviceRate]
//四级字号
#define HT_FONT_FOURTH              [UIFont systemFontOfSize:12*kDeviceRate]
//五级字号
#define HT_FONT_FIFTH               [UIFont systemFontOfSize:11*kDeviceRate]



#pragma mark -03-图标规范
//主要功能入口图标
#define HT_WIGHT_FOUNCTION_ENTER   43
#define HT_HIGHT_FOUNCTION_ENTER   43
//健康资讯图标
#define HT_WIGHT_MEDICINENES      75
#define HT_HIGHT_MEDICINENEWS       52.5
//模块小标题竖线
#define HT_WIGHT_TITLELINE      1
#define HT_HIGHT_TITLELINE      16

//标签导航icon
#define HT_WIGHT_TABBTN_TABBAR    20
#define HT_HIGHT_TABBTN_TABBAR    20
//医学知识
#define HT_WIGHT_MEDICINEKNOWLEDGE   28
#define HT_HIGHT_MEDICINEKNOWLEDGE   28
//就诊导航
#define HT_WIGHT_MEDICINENAVIGATION   35
#define HT_HIGHT_MEDICINENAVIGATION   35
//我的导航
#define HT_WIGHT_MAIN                16
#define HT_HIGHT_MAIN                16












#endif /* OfficialDefine_h */
