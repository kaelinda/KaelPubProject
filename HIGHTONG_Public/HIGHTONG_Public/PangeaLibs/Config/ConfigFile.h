//
//  ConfigFile.h
//  HIGHTONG
//
//  Created by Kael on 15/7/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#ifndef HIGHTONG_ConfigFile_h
#define HIGHTONG_ConfigFile_h




/*
 1 生成其他地区版本时只需要将下面替换区中该地区对应的宏去掉注释 并同时注释掉其他地区的宏定义
 (*保证有且只有一个被编译的宏定义)
 2 这只是控制不同项目固定参数的编译。UI参数的变动，功能开关的变动在 APPSwitch.h内
 */

#pragma mark
#pragma mark ======== 地区相关宏定义 =========


/******************替换区*********************/
//#define DISTRICT_QHD      1
//#define DISTRICT_SY       2
//#define DISTRICT_CQ       3//重庆有线
//#define DISTRICT_NMG      4
//#define DISTRICT_AS       5
//#define DISTRICT_JX       6//睛彩江西   爱上TV  就说low不low👎！！
//#define DISTRICT_HA       7
//#define DISTRICT_HF       8
//#define DIfSTRICT_ZJ       9//甜果生活
//#define DISTRICT_SX_0     10//山西健康电视
//#define DISTRICT_JX_0     11//江西健康电视
//#define DISTRICT_CD       12//常德一医
//#define DISTRICT_HN       13//湖南健康电视
//#define DISTRICT_HB       14//河北健康电视
//#define DISTRICT_GZH      15//广州健康电视
//#define DISTRICT_HN       16//湖南健康电视

//#define DISTRICT_JX       6//睛彩江西   爱上TV  就说low不low👎！！
#define DISTRICT_JKTV     99//健康电视 全国统一版 九九归一 妹的 终于不再是分散的项目了



//#define Test_PORTAL                @"http://192.168.3.27:8080/CommonPORTAL"
//#define Test_PORTAL                @"http://192.168.3.171:8080/CommonPORTAL"
//#define Test_PORTAL                @"https://www.pangeachina.com/CommonPORTAL"






#define JKTV_SERIAL_NUMBER                   @"1099034502"//健康电视APP_ID
#define SX_SERIAL_NUMBER                     @"1071241443"//山西健康电视APP_ID
#define JX_SERIAL_NUMBER                     @"1077216776"//江西健康电视APP_ID
#define HB_SERIAL_NUMBER                     @"1077219148"//河北健康电视APP_ID
#define GZH_SERIAL_NUMBER                    @"1106728727"//广州健康电视APP_ID
#define HN_SERIAL_NUMBER                     @"1107480482"//湖南健康电视APP_ID
#define JXCABLE_SERIAL_NUMBER                @"562172033" //江西有线APP_ID


/******************替换区*********************/

//*******全国统一版本 健康电视
#ifdef DISTRICT_JKTV
#define REGION_ID                  @"JKTV"
#define HTML_ID                    @"/jktv.html"
#define APP_ID                     JKTV_SERIAL_NUMBER //@"1099034502"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.6.4"
#define kAPP_NAME                  @"健康电视"//app名称GZZYY
#define KREGION_CODE               @"jktv-0-0"//区域标识
//#define KREGION_CODE               @"jktv-0-0"//区域标识
#define GROUPID                    @""//如果是测试的话就testgroup 否则就是空串
#define KSCHEMEID                  @"jktv"
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识

//URL scheme  wxf33e2321b3935472    QQ41de3cf6
#define UM_SHARE_APP_KEY           @"57636fdbe0f55adc6a000d2f"

#define QQ_SHARE_APP_ID            @"1105228389"//41E07265
#define QQ_SHARE_APP_KEY           @"zBuyYoWnd1oAuBo5"

#define WECHAT_SHARE_APP_ID        @"wx28811e5c17a35887"
#define WECHAT_SHARE_APP_SECRET    @"997b6954db306d4f40bf215af4185d84"


#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]//分享连接
#define BMK_LOCATION_KEY           @"vMgoSk5UUWD87KH3dIbfFZT3"

#define DEFAULT_SERVER_PATH        @"http://www.pangeachina.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.pangeachina.com:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL
#define PROJECT_LANPORTAL          Test_PORTAL


#else
#define PROJECT_PORTAL             @"http://www.pangeachina.com:8080/CommonPORTAL"

#define PROJECT_LANPORTAL          @"http://www.pangeachina.tv.com:8080/CommonPORTAL"

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色：蓝色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif



//重庆
#ifdef DISTRICT_CQ

#define REGION_ID                  @"CQ"
#define KSCHEMEID                  @"cqyx"
#define HTML_ID                    @"/cq.html"

#define APP_ID                     @"647027352"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"2.0.2"
#define kAPP_NAME                  @"重庆有线"

#define KREGION_CODE                  @"cq-0-0"//区域标识cq-0-0
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识 山西中西医结合医院

//URL scheme  wx9dfbe1801e0f792f    QQ06052564
//重庆有线分享
#define UM_SHARE_APP_KEY           @"564bee3767e58e3511000c4c"

#define QQ_SHARE_APP_ID            @"101000548"
#define QQ_SHARE_APP_KEY           @"9af88e1de2229398ec8ba32d1813cbf1"

#define WECHAT_SHARE_APP_ID        @"wx9dfbe1801e0f792f"
#define WECHAT_SHARE_APP_SECRET    @"e1efde8b74d5ab8ad263ab65075b9bfb"

#define SHARE_LINK                 @"http://123.57.37.43/pangea_share/pages/s.html"
#define BMK_LOCATION_KEY           @"vrwyloQleoWFMfE62h5weEMa"

#define DEFAULT_SERVER_PATH        @"http://www.panyuncq.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.panyuncq.com:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.panyuncq.com:8080/CommonPORTAL"
#endif

#define App_selected_color UIColorFromRGB(0xff9c00)//app选中色 (主题色)
#define App_Theme_Color_num        @"ff9c00"//主题色的值，webview中传给头端用得

#endif


//珠江数码
#ifdef DISTRICT_ZJ

#define REGION_ID                  @"ZJ"
#define HTML_ID                    @"/zj.html"
#define APP_ID                     @""
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.0.0"
#define kAPP_NAME                  @"甜果电视"
#define KSCHEMEID                  @"zjsm"

#define KREGION_CODE                 @"zj-0-0"//区域标识cq-0-0
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识 山西中西医结合医院

//URL scheme  wxe5ffb8b99a8a55d2    QQ41D8EFD7
//甜果生活分享
#define UM_SHARE_APP_KEY           @"564b006f67e58e390200531e"

#define QQ_SHARE_APP_ID            @"1104736215"
#define QQ_SHARE_APP_KEY           @"9D66m9gbSsMU6F1E"

#define WECHAT_SHARE_APP_ID        @"wxe5ffb8b99a8a55d2"
#define WECHAT_SHARE_APP_SECRET    @"521bc2303cb13e07c7e9cc7660ebd601"

#define SHARE_LINK                 @"http://123.57.37.43/zj_share/pages/s.html"
#define BMK_LOCATION_KEY           @"6ZsgAt5643lZWsrU3EmKIPR8"

#define DEFAULT_SERVER_PATH        @"http://portal.candytime.com.cn:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://portal.candytime.com.cn:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://portal.candytime.com.cn:8080/CommonPORTAL"

#endif

#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif



//山西健康电视
#ifdef DISTRICT_SX_0

#define REGION_ID                  @"SX"
#define HTML_ID                    @"/sx.html"
#define APP_ID                     SX_SERIAL_NUMBER //@"1071241443"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.1.2"
#define kAPP_NAME                  @"山西健康电视"//app名称GZZYY
#define KREGION_CODE               @"sx-0-0"//区域标识
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @"03510001"//医院标识 山西中西医结合医院
#define KSCHEMEID                  @"sxjktv"

//URL scheme  wxb54fe578234a1e4d    QQ41dbf70f
//山西健康电视分享
#define UM_SHARE_APP_KEY           @"56667b8967e58ee7890019a5"

#define QQ_SHARE_APP_ID            @"1104934671"//41dbf70f
#define QQ_SHARE_APP_KEY           @"yuQRPtmKKx6rJ3Fy"

#define WECHAT_SHARE_APP_ID        @"wxb54fe578234a1e4d"
#define WECHAT_SHARE_APP_SECRET    @"494ec41e993e23d2e6edaa13e52d66e8"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]
#define BMK_LOCATION_KEY           @"o9klE5P8Cq0d31Q1fhCZwW9b"

#define DEFAULT_SERVER_PATH        @"http://www.pangeasx.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.pangeasx.com:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeasx.com:8080/CommonPORTAL"

#endif

#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif



//江西健康电视
#ifdef DISTRICT_JX_0
#define REGION_ID                  @"JX"
#define HTML_ID                    @"/jx.html"
#define APP_ID                     JX_SERIAL_NUMBER //@"1077216776"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.3.0"
#define kAPP_NAME                  @"江西健康电视"//app名称GZZYY
#define KREGION_CODE                  @"jx-0-0"//区域标识
#define KSCHEMEID                  @"jxjktv"

#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识

//URL scheme  wxf33e2321b3935472    QQ41de3cf6
//江西健康电视分享
#define UM_SHARE_APP_KEY           @"568b328767e58eab7600148a"

#define QQ_SHARE_APP_ID            @"1105009391"//41dd1aef
#define QQ_SHARE_APP_KEY           @"mNjMWYBRcLnmGlVZ"

#define WECHAT_SHARE_APP_ID        @"wxf33e2321b3935472"
#define WECHAT_SHARE_APP_SECRET    @"0747647f0b8be4e6ec29f49789afe3ce"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]
#define BMK_LOCATION_KEY           @"vMgoSk5UUWD87KH3dIbfFZT3"

#define DEFAULT_SERVER_PATH        @"http://www.pangeajx.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.pangeajx.com:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeajx.com:8080/CommonPORTAL"

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif


//常德一医
#ifdef DISTRICT_CD
#define REGION_ID                  @"CD"
#define HTML_ID                    @"/cd.html"
#define APP_ID                     @"1071509560"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.0.1"
#define kAPP_NAME                  @"常德一医"//app名称GZZYY
#define KREGION_CODE                 @"hn-cd-07310001"//区域标识cq-0-0
//#define KREGION_CODE       @"GZZYY"//区域标识cq-0-0
#define KSCHEMEID                  @"cdsyy"

#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @"07310001"//医院标识
//URL scheme  wxb54fe578234a1e4d    QQ41dbf70f
//常德一医分享
#define UM_SHARE_APP_KEY           @"5694a30567e58e71f0001342"

#define QQ_SHARE_APP_ID            @"1105034786"
#define QQ_SHARE_APP_KEY           @"9VsdUABonOPwW5k1"

#define WECHAT_SHARE_APP_ID        @"wx94808b4409ad5e69"
#define WECHAT_SHARE_APP_SECRET    @"d71b28454ad2ab351efab47ae155f748"

#define SHARE_LINK                 @"http://123.57.37.43/share_cdsyy/pages/s.html"
#define BMK_LOCATION_KEY           @"QC8moIL0gmd5jzxBv1RxcAhT"

#define DEFAULT_SERVER_PATH        @"http://www.pangeahunan.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.pangeahunan.com:8080"//点播


#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeahunan.com:8080/CommonPORTAL"

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif

//河北健康电视
#ifdef DISTRICT_HB
#define REGION_ID                  @"HB"
#define HTML_ID                    @"/hb.html"
#define APP_ID                     HB_SERIAL_NUMBER //@"1077219148"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.1.2"
#define kAPP_NAME                  @"河北健康电视"//app名称GZZYY
#define KREGION_CODE                 @"hb-0-0"//区域标识
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @"03110001"//医院标识
#define KSCHEMEID                  @"hbjktv"

//河北健康电视的分享
#define UM_SHARE_APP_KEY           @"5694b83f67e58e0c08000067"
#define QQ_SHARE_APP_ID            @"1105234810"//41e08b7a
#define QQ_SHARE_APP_KEY           @"mskxZIdDYAYd6oWa"

#define WECHAT_SHARE_APP_ID        @"wxbc69f3244ef0a624"  
#define WECHAT_SHARE_APP_SECRET    @"f7c56f89edc44d1c61a6dc77952e4215"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]
#define BMK_LOCATION_KEY           @"BkyZBWu7Ztmw4xGxw81PVyfV"

#define DEFAULT_SERVER_PATH        @"http://www.pangeahebei.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.pangeahebei.com:8080"//点播


#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeahebei.com:8080/CommonPORTAL"

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif

//广州健康电视
#ifdef DISTRICT_GZH
#define REGION_ID                  @"GZH"
#define HTML_ID                    @"/gzh.html"
#define APP_ID                     GZH_SERIAL_NUMBER //@"1106728727"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.0.0"
#define kAPP_NAME                  @"广州健康电视"//app名称GZZYY
#define KREGION_CODE                  @"gz-0-0"//区域标识cq-0-0
//#define KREGION_CODE        @"GZZYY"//区域标识cq-0-0
#define KSCHEMEID                  @"gzjktv"

#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识
//URL scheme  wxb54fe578234a1e4d    QQ41dbf70f
//广州健康电视分享
#define UM_SHARE_APP_KEY           @"5715eb5ee0f55adda4000e47"

#define QQ_SHARE_APP_ID            @"1105348448"
#define QQ_SHARE_APP_KEY           @"0agmHQFR7WbtdsuB"

#define WECHAT_SHARE_APP_ID        @"wxad566a0c3616921f"
#define WECHAT_SHARE_APP_SECRET    @"d191c9cdf5878d1cd4ff693e8d764db0"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]
#define BMK_LOCATION_KEY           @"A4I8plw6zYFt1U7sclWuibbWFYN5l7yV"

#define DEFAULT_SERVER_PATH        @""
#define DEFAULT_DEMAND_SERVER_PATH @""//点播


#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeaguangdong.com:8080/CommonPORTAL"

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif


//湖南健康电视
#ifdef DISTRICT_HN
#define REGION_ID                  @"HN"
#define HTML_ID                    @"/hn.html"
#define APP_ID                     HN_SERIAL_NUMBER //@"1107480482"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"1.0.0"
#define kAPP_NAME                  @"湖南健康电视"//app名称GZZYY
#define KREGION_CODE                  @"hn-0-0"//区域标识
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识
#define KSCHEMEID                  @"hnjktv"

//湖南健康电视分享
#define UM_SHARE_APP_KEY           @"573ade9be0f55a80fb001949"

#define QQ_SHARE_APP_ID            @"1105289229"//41e1600d
#define QQ_SHARE_APP_KEY           @"3k8HDZXGZ9XHWITL"

#define WECHAT_SHARE_APP_ID        @"wx3bfe75c4469a9671"
#define WECHAT_SHARE_APP_SECRET    @"bc774ce70fab398997695d7741629086"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]
#define BMK_LOCATION_KEY           @"YRfGwpdPlGe5L277javIY8kFNMFrhZ0n"

#define DEFAULT_SERVER_PATH        @""
#define DEFAULT_DEMAND_SERVER_PATH @""//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @""

#endif
#define App_selected_color UIColorFromRGB(0x3ab7f5)//app选中色 (主题色)
#define App_Theme_Color_num        @"3ab7f5"//主题色的值，webview中传给头端用得

#endif



//江西有线  睛彩江西
#ifdef DISTRICT_JX
#define REGION_ID                  @"JX"
#define HTML_ID                    @"/jx.html"
#define APP_ID                     JXCABLE_SERIAL_NUMBER //@"562172033"
#define TERMINAL_TYPE              @"0" //iphone=0 ipad =2
#define APP_VERSION                @"2.2.0"
#define kAPP_NAME                  @"爱上TV"//app名称
#define KREGION_CODE                  @"jxtv-0-0"//区域标识
#define REGION_TYPE                @"1"//1：医院;2：社区;3：学校 默认为1
#define REGION_CODE                @""//医院标识
#define KSCHEMEID                  @"jcjx"

//***************分享账号
#define UM_SHARE_APP_KEY           @"57611b5867e58e4aa500036c"

#define QQ_SHARE_APP_ID            @"100589710"
#define QQ_SHARE_APP_KEY           @"306ac7a2382e907cfb6b55066786f9e7"

#define WECHAT_SHARE_APP_ID        @"wxeb2bd4c7cda51c85"
#define WECHAT_SHARE_APP_SECRET    @"6a4938154dfe45994dca8ed6019022d3"

#define SHARE_LINK                 [[NSUserDefaults standardUserDefaults] objectForKey:@"shareurl"]//分享连接//@"http://123.57.37.43/jx_cable/pages/s.html"
#define BMK_LOCATION_KEY           @"vMgoSk5UUWD87KH3dIbfFZT3"
//***************分享账号

//***************各默认PORTAL
#define DEFAULT_SERVER_PATH        @"http://www.jxtv.com:8080"
#define DEFAULT_DEMAND_SERVER_PATH @"http://www.jxtv.com:8080"//点播

#ifdef Test_PORTAL
#define PROJECT_PORTAL             Test_PORTAL

#else
#define PROJECT_PORTAL             @"http://www.pangeajxdtv.com:8080/CommonPORTAL"

#endif

#define App_selected_color UIColorFromRGB(0xe70405)//app选中色 (红色主题色)
#define App_Theme_Color_num        @"e70405"//主题色的值，webview中传给头端用得

#endif








#endif
