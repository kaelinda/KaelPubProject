//
//  HTEPGPortalInterface.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTEPGPortalInterface.h"
@interface HTEPGPortalInterface ()
{
    NSString *_token;
    NSString *_EPGPORTAL;
    NSString *_VODPORTAL;
    NSString *_UPORTAL;
    NSString *_ADPORTAL;
    NSString *_PMPORTAL;
}

@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong) NSMutableArray *hotChannelArr;
@property (nonatomic,strong) NSMutableArray *allChannelArr;
@end

@implementation HTEPGPortalInterface
- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }

        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
            _EPGPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
            _VODPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODPORTAL"];
            _UPORTAL   = [[NSUserDefaults standardUserDefaults] objectForKey:@"UPORTAL"];
            
            //            _UPORTAL  = @"http://101.200.150.40:9090/UPORTAL";
            
            NSLog(@"_EPGPORTAL-------------%@",_EPGPORTAL);
            NSLog(@"_VODPORTAL-------------%@",_VODPORTAL);
            NSLog(@"_UPORTAL-------------%@",_UPORTAL);
            
            //            _ADPORTAL  = @"http://192.168.3.60:8080/ADPORTAL";
            _ADPORTAL  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ADPORTAL"];
            if (_ADPORTAL.length==0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
            }
            
            _PMPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"polymedicine"];
            
           
        

    }
    return self;
}

//2.1 EPG浏览 EPG_EVENTLIST全部频道列表
-(void)EPGChannelAllListWithLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock{
    NSMutableDictionary*paramDic = [NSMutableDictionary dictionary];
    
    [paramDic setObject:[NSNumber numberWithInteger:lookbackFlag] forKey:@"lookbackFlag"];
    [paramDic setObject:TERMINAL_TYPE forKey:@"terminal_type"];
    if (_token.length>0) {
        [paramDic setObject:_token forKey:@"token"];
    }
    
    [self postEPGPortalRequestWith:paramDic andType:EPG_CHANNELALLLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type); 
    }];
}


//2.1EPG浏览
-(void)EPGSHOWWithServiceID:(NSString *)serviceID andDate:( NSString *)date andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary*paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:serviceID,@"serviceID",date,@"date",startDate,@"startDate",endDate,@"endDate", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//2.2 频道列表

-(void)EPGChannelWithType:(NSString *)type andLookbackFlag:(NSString *)lookbackFlag andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary*paramDic ;
    
    if ([lookbackFlag isEqualToString:@"1"]) {
        paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",lookbackFlag,@"lookbackFlag", nil];
    }else{
        paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
    }
    
//    if (type>=0) {
//        [paramDic setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
//    }
    if (isEmptyStringOrNilOrNull(type)) {
        [paramDic setObject:@"" forKey:@"type"];
    }else
    {
       [paramDic setObject:type forKey:@"type"];
    }
//    if (_token.length>0) {
//        [paramDic setObject:_token forKey:@"token"];
//    }
    if (isEmptyStringOrNilOrNull(_token)) {
        [paramDic setObject:@"" forKey:@"token"];
    }else
    {
        [paramDic setObject:_token forKey:@"token"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    
    [self postEPGPortalRequestWith:paramDic andType:EPG_CHANNELLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}


//2.3频道查询 （不需要拼接、判断参数）

- (void)TVChangeWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(_token)) {
        [paramDic setObject:@"" forKey:@"token"];
    }else
    {
        [paramDic setObject:_token forKey:@"token"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CHANNELQUERY];
    [self postEPGPortalRequestWith:paramDic andType:EPG_CHANNELQUERY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
    

}

//2.4 当前后继节目列表

-(void)EPGPFWithServiceID:(NSString *)serviceID  andType:(NSString*)type andCount:(NSString *)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    if (isEmptyStringOrNilOrNull(serviceID)) {
        [paramDic setObject:@"" forKey:@"id"];
    }else
    {
        [paramDic setObject:serviceID forKey:@"id"];
    }
    
    if (isEmptyStringOrNilOrNull(type)) {
        [paramDic setObject:@"" forKey:@"type"];
    }else
    {
        [paramDic setObject:type forKey:@"type"];
    }
    
    
    if (isEmptyStringOrNilOrNull(count)) {
        [paramDic setObject:@"" forKey:@"count"];
    }else
    {
        [paramDic setObject:@"2" forKey:@"count"];

    }

    _manager.requestSerializer.timeoutInterval = timeout;
    
    [self postEPGPortalRequestWith:paramDic andType:EPG_CURRENTEVENTLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.5 鉴权直播  TV_PLAY
-(void)TVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_TVPLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
 
}
//2.6 增强版鉴权直播

-(void)TVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:tvName,@"tvName",TERMINAL_TYPE,@"terminal_type", nil];
    [paramDic setObject:_token forKey:@"token"];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_TVPLAYEX andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}

//2.7 鉴权时移

-(void)PauseTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_PAUSETVPLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.8 增强版鉴权时移
-(void)PauseTVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:tvName,@"tvName",TERMINAL_TYPE,@"terminal_type", nil];
    [paramDic setObject:_token forKey:@"token"];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_PAUSETVPLAYEX andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.9 鉴权回看
-(void)EventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:eventID,@"id",TERMINAL_TYPE,@"terminal_type", nil];
    [paramDic setObject:_token forKey:@"token"];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTPLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}


-(void)EventPlayEXWithTVName:(NSString *)tvName andStarTime:(NSString *)startTime andEndTime:(NSString *)endTime andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [paramDic setObject:tvName forKey:@"tvName"];
    [paramDic setObject:startTime forKey:@"startTime"];
    [paramDic setObject:TERMINAL_TYPE forKey:@"terminal_type"];
    [paramDic setObject:_token forKey:@"token"];
    
    if (isEmptyStringOrNilOrNull(endTime)) {
        
    }else
    {
        [paramDic setObject:endTime forKey:@"endTime"];
    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTPLAYEX andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.11 分类列表

-(void)TypeListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:REGION_ID,@"regionID",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_TYPELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.15 EPG 搜索2

-(void)EPGSearchEXWithKey:(NSString *)key andType:(NSString*)type andServiceID:(NSString *)serviceID andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    if (isEmptyStringOrNilOrNull(type)) {
       [paramDic setObject:@"" forKey:@"type"];
    }else
    {
        [paramDic setObject:type forKey:@"type"];
    }
    
    if (key) {
        [paramDic setObject:key forKey:@"key"];
    }
    if (isEmptyStringOrNilOrNull(startDate)) {
        
        [paramDic setObject:@"" forKey:@"startDate"];
    }else
    {
        [paramDic setObject:startDate forKey:@"startDate"];
    }

    if (isEmptyStringOrNilOrNull(endDate)) {
        [paramDic setObject:@"" forKey:@"endDate"];

    }else
    {
        [paramDic setObject:endDate forKey:@"endDate"];
    }

    if (isEmptyStringOrNilOrNull(serviceID)) {
        [paramDic setObject:@"" forKey:@"serviceID"];
    }else
    {
        [paramDic setObject:serviceID forKey:@"serviceID"];
    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTSEARCH andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.16 热门搜索关键词

-(void)TopSearchKeyWithKey:(NSString *)key andCount:(NSString *)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
    
    if (isEmptyStringOrNilOrNull(key)) {
        [paramDic setObject:@"" forKey:@"key"];
    }else
    {
        [paramDic setObject:key forKey:@"key"];

    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        [paramDic setObject:@"" forKey:@"count"];

    }else{
        [paramDic setObject:count forKey:@"count"];

    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_TOPSEARCHWORDS andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//2.17 增加频道收藏

-(void)AddTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_ADDTVFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}

//2.18 删除频道收藏

-(void)DelTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_DELTVFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}

//2.19 频道收藏列表

-(void)TVFavorateListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{

    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_TVFAVORATELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    

}

//2.20 播放记录列表
//********************************可以不用这个接口啦*************************
-(void)TVPlayListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_TVPLAYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.20 3。5版本的接口

-(void)TVPlayListWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTImeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
    paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    if (isEmptyStringOrNilOrNull(startDate)) {
        [paramDic setObject:@"" forKeyedSubscript:@"startDate"];
    }else
    {
        [paramDic setObject:startDate forKeyedSubscript:@"startDate"];
    }
    if (isEmptyStringOrNilOrNull(endDate)) {
        [paramDic setObject:@"" forKeyedSubscript:@"endDate"];
    }else
    {
        [paramDic setObject:endDate forKeyedSubscript:@"endDate"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_TVPLAYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}


//2.21 删除播放记录

-(void)DelTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    if (isEmptyStringOrNilOrNull(serviceID)) {
        
        [paramDic setObject:@"" forKey:@"serviceID"];
    }else{
         [paramDic setObject:serviceID forKey:@"serviceID"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_DELTVPLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}

//2.22 回看停止

-(void)EventStopWithEventID:(NSString *)eventID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:eventID,@"eventID",TERMINAL_TYPE,@"terminal_type",_token,@"token",[numberFormatter stringFromNumber: [NSNumber numberWithInteger: breakPoint]],@"breakPoint",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTSTOP andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];


}
//2.23 查询回看播放记录


-(void)EventPlayListWithEventID:(NSString *)eventID andCount:(NSString *)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",_token,@"token",nil];
    if (isEmptyStringOrNilOrNull(eventID)) {
        
        [paramDic setObject:@"" forKey:@"eventID"];
        if (isEmptyStringOrNilOrNull(count)) {
            [paramDic setObject:@"" forKey:@"count"];
        }else
        {
        [paramDic setObject:count forKey:@"count"];
        }
    }else{
        [paramDic setObject:eventID forKey:@"eventID"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTPLAYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}
//2.24 删除回看播放记录   DEL_EVENT_PLAY

-(void)DelEventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",nil];
    if (isEmptyStringOrNilOrNull(eventID)) {
        [paramDic setObject:@"" forKey:@"eventID"];
    }else
    {
        [paramDic setObject:eventID forKey:@"eventID"];

    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_DELEVENTPLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}

//2.25 增加回看收藏  ADD_EVENT_FAVORITE


-(void)AddEventFavorateWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_ADDEVENTFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}


//2.26 删除回看收藏  DEL_EVENT_FAVORITE

-(void)DelEventFavorateWithEventID:(NSString *)eventID anTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",nil];
    if (isEmptyStringOrNilOrNull(eventID)) {
        
        [paramDic setObject:@"" forKey:@"eventID"];
    }else
    {
        [paramDic setObject:eventID forKey:@"eventID"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_DELEVENTFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

    
}

//  2.27 回看收藏列表  EVENT_FAVORITE_LIST

-(void)EventFavorateListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTFAVORATELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.28 热门频道列表  HOT_CHANNEL_LIST

-(void)HotChannelListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    
    if (_EPGPORTAL.length == 0) {
        
        returnBlock(-1,nil,EPG_HOTCHANNELLIST);
        
    }
    [self postEPGPortalRequestWith:paramDic andType:EPG_HOTCHANNELLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.29 预约节目   ADD_EVENT_RESERVE

-(void)AddEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_ADDEVENTRESERVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.30 取消预约节目 DEL_EVENT_RESERVE

-(void)DelEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",nil];
    if (isEmptyStringOrNilOrNull(eventID)) {
        [paramDic setObject:@"" forKey:@"eventID"];
    }else
    {
        [paramDic setObject:eventID forKey:@"eventID"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_DELEVENTRESERVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)ADListWith:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    if (regionCode.length==0) {
        regionCode = REGION_CODE;
        if (![KREGION_CODE isEqualToString:@"jx-0-0"]) {
            //非江西版本 上传写死的regionCode
            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
        }else{
            //江西版本 自动获取 若 regionCode 为空，暂不需要传入该参数
        }
        
    }else{
        [paramDic setObject:regionCode forKey:@"regionCode"];
    }
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_ADLIST];
    
    [self postEPGPortalRequestWith:paramDic andType:EPG_ADLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
 
}

//2.30 取消预约节目 DEL_EVENT_RESERVE

-(void)EPGUpdateTvFavoriteWithSeq:(NSString*)seq andServiceID:(NSString *)serviceID andTVName:(NSString *)name andTimeou:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:seq forKey:@"seq"];
    [dic setValue:serviceID forKey:@"id"];
    [dic setValue:name forKey:@"name"];
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",@[dic],@"servicelist",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_UPDATE_TVFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}



- (void)EPGUpdateTvFavoriteWithNewSortArr:(NSArray *)sortArr andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",sortArr,@"servicelist",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_UPDATE_TVFAVORATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}



-(void)epgSearchOrderResultWith:(NSString *)key andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:key,@"key",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(startDate)) {
        [paramDic setObject:@"" forKey:@"startDate"];
    }else
    {
        [paramDic setObject:startDate forKey:@"startDate"];

    }
    if (isEmptyStringOrNilOrNull(endDate)) {
        [paramDic setObject:@"" forKey:@"endDate"];
    }else
    {
        [paramDic setObject:endDate forKey:@"endDate"];

    }
    if (isEmptyStringOrNilOrNull(count)) {
        [paramDic setObject:@"" forKey:@"count"];

    }else
    {
        [paramDic setObject:count forKey:@"count"];

    }
    
    [self postEPGPortalRequestWith:paramDic andType:@"epgSearch" andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];


}
//2.31 预约节目列表  EVENT_RESERVE_LIST

-(void)EventRevserveWithTimeou:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:EPG_EVENTRESERVELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}





#pragma --mark POST请求方式



-(void)getHomePageSlideshowListAndReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    if (regionCode.length==0) {
        if (![KREGION_CODE isEqualToString:@"jx-0-0"]) {
            //非江西版本 需要传入写死的 regionCode
            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
        }else{
            //江西版本 regionCode参数 取不到就 暂不上传了吧
        }
    }else{
        [paramDic setObject:regionCode forKey:@"regionCode"];
    }
    _manager.requestSerializer.timeoutInterval = 10;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPortalRequestWith:paramDic andType:@"slideshowList" andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}

//首页轮播图广告


-(void)EPGGetRegionListWithTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",REGION_TYPE,@"regionType",nil];
    
    _manager.requestSerializer.timeoutInterval = 10;
    [self postEPGPortalRequestWith:paramDic andType:EPG_REGION_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}

-(void)EPGFindRegionWithTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
    NSString *longitude = @"114.989792";//0.000000
    NSString *latitude  = @"27.116745";//0.000000
    if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
        longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
        latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
    }
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",REGION_TYPE,@"regionType",nil];
    _manager.requestSerializer.timeoutInterval = 10;
    [self postEPGPortalRequestWith:paramDic andType:EPG_FIND_REGION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}
-(void)EPGFindRegionWithLongitude:(NSString *)longitude andLatitude:(NSString *)latitude andTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",REGION_TYPE,@"regionType",nil];
    _manager.requestSerializer.timeoutInterval = 10;
    [self postEPGPortalRequestWith:paramDic andType:EPG_FIND_REGION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)EPGGetRegionInfoWithRegionCode:(NSString *)regionCode andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:regionCode,@"regionCode",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPortalRequestWith:paramDic andType:EPG_REGION_INFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)postEPGPortalRequestWith:(NSMutableDictionary *)paramDic andType:(NSString *)type andReturn:(HTEPGPortalRequest_Return)returnBlock
{
    
    if (_EPGPORTAL.length == 0) {
        returnBlock(-1,nil,type);
        return;
    }
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,type];
    NSLog(@"上传的数据 : %@ URL : %@",paramDic,URLString);
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *channelListArr = [NSMutableArray  arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"channelList"]];
        
        if (channelListArr.count<=0 && ![type isEqualToString:EPG_CHANNELALLLIST]) {
            
            [self EPGChannelAllListWithLookbackFlag:0 andTimeout:5 andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
                NSMutableArray *m_channelListArr = [result objectForKey:@"serviceList"];
                
                [[NSUserDefaults standardUserDefaults] setObject:m_channelListArr forKey:@"channelList"];
            }];
        }
        
        NSMutableDictionary *CHANNELListDic = [NSMutableDictionary dictionary];
        for (NSDictionary* dic in channelListArr) {
            NSArray * serviceinfo = [dic objectForKey:@"serviceinfo"];
            
            if (serviceinfo.count == 0) {
                NSString *ID = [dic objectForKey:@"id"];
                if (ID.length) {
                    [CHANNELListDic setObject:dic forKey:ID];
                }
            }else{
                for (NSDictionary *channelInfo in serviceinfo) {
                    NSString *ID = [channelInfo objectForKey:@"id"];
                    if (ID.length) {
                        [CHANNELListDic setObject:channelInfo forKey:ID];
                    }
                }
            }
            
           
        }
        
        if (![type isEqualToString:EPG_EVENTSEARCH]) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
        }
        
        
        if ([type isEqualToString:EPG_CHANNELLIST]) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
            NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
            
            _allChannelArr = [[NSMutableArray alloc] initWithArray:mSevicelistArr];
            
            if (_allChannelArr.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:_allChannelArr forKey:@"servicelist"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        }
        
        
        
        if ([type isEqualToString:EPG_EVENTSEARCH]) {
            
            
            
            
            NSMutableArray *resultArr = [[NSMutableArray alloc] init];
            
            if (responseObject != nil) {//返回结果是有搜索结果的
                if ([CHANNELListDic count]) { //如果有频道具体信息
                    for (NSDictionary *searchResault in responseObject) {
                        NSDictionary *epginfo = [searchResault objectForKey:@"epginfo"];
                        NSString *serviceID  =  [searchResault objectForKey:@"serviceID"];
                        
                        NSDictionary *ChannelInfo = [CHANNELListDic objectForKey:serviceID];
                        
                        if ([ChannelInfo objectForKey:@"imageLink"]) {
                            //                             NSMutableDictionary *AN_epginfo = [[NSMutableDictionary alloc]initWithDictionary:epginfo];
                            
                            
                            NSString * channelID = [ChannelInfo objectForKey:@"id"];
                            NSString * channelName = [ChannelInfo objectForKey:@"name"];
                            NSString * imageLink = [ChannelInfo objectForKey:@"imageLink"];
                            
                            NSMutableArray *AN_epginfo =  [NSMutableArray array];
                            for (NSDictionary *diccccc in epginfo) {
                                NSMutableDictionary *AN_diccccc = [[NSMutableDictionary alloc]initWithDictionary:diccccc];
                                //添加一些需要的信息进入目标字典中
                                if (channelID.length) {
                                    [AN_diccccc setObject:channelID forKey:@"channelID"];
                                }
                                if (channelName.length) {
                                    [AN_diccccc setObject:channelName forKey:@"channelName"];
                                }
                                if (imageLink.length) {
                                    [AN_diccccc setObject:imageLink forKey:@"imageLink"];
                                }
                                
                                [AN_epginfo addObject:AN_diccccc];
                            }
                            
                            //拼接好的节目详细信息拼进目标中
                            NSMutableDictionary *AN_searchResault = [[NSMutableDictionary alloc]initWithDictionary:searchResault];
                            
                            if ([AN_epginfo count]) {
                                [AN_searchResault setObject:AN_epginfo forKey:@"epginfo"];
                            }
                            if (imageLink.length) {
                                [AN_searchResault setObject:imageLink forKey:@"imageLink"];
                            }
                            if (channelID.length) {
                                [AN_searchResault setObject:channelID forKey:@"serviceID"];
                            }
                            //添加上这个数据
                            [resultArr addObject:AN_searchResault];
                        }
                        
                        
                        
                    }
                }
            }
            
            
            
            if (responseObject!=nil) {
                
            }
            
            if ([type isEqual:EPG_EVENTSEARCH]) {
                
                returnBlock(0,resultArr,type);
                return ;
            }
            
            returnBlock(0,responseObject,EPG_CHANNELLIST);
            
            
        }
        
        
        if ([type isEqualToString:@"epgSearch"]) {
            NSString *count = [responseObject objectForKey:@"count"];
            if ([count integerValue] > 0) {
                
                NSMutableArray *datelistArray = [NSMutableArray array];
                
                datelistArray.array = [[responseObject objectForKey:@"dateList"] mutableCopy];
                
                NSMutableDictionary *ALL = [NSMutableDictionary dictionary];
                NSInteger count = 0;
                NSMutableArray *JIEGUOdatelistArray = [NSMutableArray array];
                
                for (NSDictionary *dateEPG in datelistArray) {
                    
                    
                    NSMutableArray *jieguoserviceList= [NSMutableArray array];
                    
                    
                    NSMutableArray *serviceListArray = [dateEPG objectForKey:@"serviceList"];
                    NSMutableArray *date = [dateEPG objectForKey:@"date"];
                    
                    //判断是否有此频道  如果有的话，那么就加入到  节目单中
                    for (NSDictionary *EPGinfo in serviceListArray) {
                        NSString * serviceId = [EPGinfo objectForKey:@"serviceId"];
                        
                        //如果存在这个频道的话
                        NSDictionary *ChannelInfo = [CHANNELListDic objectForKey:serviceId];
                        
                        if ([ChannelInfo count]) {
                            NSMutableDictionary *EPGJIEGUO = [NSMutableDictionary dictionary];
                            
                            NSString * channelID = [ChannelInfo objectForKey:@"id"];
                            NSString * channelName = [ChannelInfo objectForKey:@"name"];
                            NSString * imageLink = [ChannelInfo objectForKey:@"imageLink"];
                            
                            NSMutableDictionary *jieguoEPGinfo = [[ NSMutableDictionary alloc]initWithDictionary:EPGinfo];
                            
                            
                            if (channelName.length) {
                                [EPGJIEGUO setObject:channelName forKey:@"channelName"];
                            }
                            if (imageLink.length) {
                                [EPGJIEGUO setObject:imageLink forKey:@"imageLink"];
                            }
                            if (channelID.length) {
                                [EPGJIEGUO setObject:channelID forKey:@"serviceID"];
                            }
                            
                            
                            [EPGJIEGUO setObject:jieguoEPGinfo forKey:@"epgList"];
                            
                            count++;
                            [jieguoserviceList addObject:EPGJIEGUO];
                        }
                        
                        
                    }
                    //遍历了好多节目了
                    if([jieguoserviceList count])
                    {
                        NSMutableDictionary *dataServiceListDic = [NSMutableDictionary dictionary];
                        
                        [dataServiceListDic setObject:date forKey:@"date"];
                        [dataServiceListDic setObject:jieguoserviceList forKey:@"serviceList"];
                        
                        [JIEGUOdatelistArray addObject:dataServiceListDic];
                    }
                    
                    
                }
                
                [ALL setObject:JIEGUOdatelistArray forKey:@"dateList"];
                [ALL setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
                returnBlock(0,ALL,type);
                return;
            }else
            {

                returnBlock (0,responseObject,type);
            }
            
            
        }
        
        if ([type isEqualToString:EPG_SYSTEMTIME]) {
            
            NSString *sysTime=[responseObject objectForKey:@"sysTime"];
            if (!sysTime) {
                return ;
            }
            
            //获取服务器时间
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *SystemDateTime = [formatter dateFromString:sysTime];
            int TimeDiffence = [SystemDateTime timeIntervalSinceNow];
            NSLog(@"我的差值时间差值%d",TimeDiffence);
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",TimeDiffence] forKey:@"SystemTimeDiffence"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            //end
        }
        
        returnBlock(0,responseObject,type);
        
        return;
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        returnBlock([error code],nil,type);
        
    }];

 
}






@end
