//
//  VODPageRequestManger.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/9/14.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "VODPageRequestManger.h"
#import "HTRequest.h"
#import "HTVODPortalInterface.h"
@interface VODPageRequestManger ()<HTRequestDelegate>
@property (nonatomic,strong)NSMutableArray *menuArray;

@property (nonatomic,strong)NSMutableArray *menuOperationArray;
@property (nonatomic,strong)NSString *operationCode;
@property (nonatomic,strong)NSMutableArray *topicProgramListArray;
@property (nonatomic,strong)NSMutableArray *categoryTopicArray;
@property (nonatomic,strong)NSString *categoryName;
@property (nonatomic,strong)NSMutableArray *menuDemandArray;
@property (nonatomic,assign)NSInteger seqCount;
@end


@implementation VODPageRequestManger
-(instancetype)init{
    NSAssert(NO, @"请用正确的代理方式额");
    return self;
}

-(id)initWithDelegate:(id<VODPageRequestDelegate>)delegate{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        
        _menuOperationArray = [NSMutableArray array];
        _menuDemandArray = [NSMutableArray array];

    }
    return self;
}


-(void)reloadVODMenuData
{
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    
    [request VODMenuConfigListWithTimeout:KTimeout];
}


//运营组列表
-(void)reloadOperationCategoryListWithOperationCode:(NSString *)operationCode
{
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request VODOperationCategoryListWithOperationCode:operationCode andTimeout:KTimeout];
}


//专题列表

-(void)reloadDataTopicProgramListWithTopicCode:(NSString*)topicCode andStartSeq:(NSString*)StartSeq andCount:(NSString *)count
{
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request VODTopicProgramListWithTopicCode:topicCode andStartSeq:1 andCount:[count intValue] andTimeout:KTimeout];
    
}


//分类专题列表
-(void)reloadCategoryTopicListWithCategoryID:(NSString *)categoryID
{
    HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
    [request VODCategoryTopicListWithCategoryID:categoryID andTimeout:KTimeout];
}



-(void)reloadDataForMenuDataCode:(NSString *)code withMenuType:(NSString *)menuType
{
    _operationCode = code;
    if ([menuType isEqualToString:@"1"])
    {
        //运营组列表
        [self reloadOperationCategoryListWithOperationCode:code];
        
    }else if ([menuType isEqualToString:@"2"])
    {
        //专题列表
        
        [self reloadDataTopicProgramListWithTopicCode:code andStartSeq:@"1" andCount:@"100"];
        
    }else if ([menuType isEqualToString:@"3"])
    {
        //分类列表
        [self reloadCategoryTopicListWithCategoryID:code];
    }

}











- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    if ([type isEqualToString:VOD_MenuConfigList]) {
       
            _menuArray = [NSMutableArray array];
             
             _menuArray = [result objectForKey:@"configList"];
             
             if (_delegate && [_delegate respondsToSelector:@selector(VODMenuDataGetSuccessWith:)]) {
                 [_delegate VODMenuDataGetSuccessWith:[_menuArray mutableCopy]];
             }

        
        
        }
    
    
    //运营组
    if ([type isEqualToString:VOD_OPER_CATEGORY_LIST]) {
        
        NSString *ID;
        NSString *categoryID;
        NSString *categoryName;
        _menuOperationArray = [[NSMutableArray alloc] init];
        
        _seqCount = [[result objectForKey:@"count"] integerValue];
        _menuDemandArray = [result objectForKey:@"categoryList"];
        for (NSDictionary *dic in [result objectForKey:@"categoryList"]) {
            
            ID = [dic objectForKey:@"id"];
            categoryID= [dic objectForKey:@"categoryID"];
            categoryName = [dic objectForKey:@"name"];
            //HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self];
           // [reqest VODProgramListWithOperationCode:_operationCode andRootCategpryID:[dic objectForKey:@"categoryID"] andStartSeq:1 andCount:100 andTimeout:KTimeout];
            _categoryName = [dic objectForKey:@"name"];
            HTRequest *reqest = [[HTRequest alloc]initWithDelegate:self];
//            [reqest VODProgramListWithOperationCode:_operationCode andRootCategpryID:categoryID andStartSeq:1 andCount:6 andTimeout:10 andIndex:[ID integerValue]-1];//这个地方的排序是从1开始，跟数组差一个
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
              [reqest VODProgramListWithOperationCode:_operationCode andRootCategpryID:categoryID andStartSeq:1 andCount:100 andTimeout:KTimeout andIndex:[ID integerValue]-1 andProgramListIndexName:categoryName];
//            });
            
//            _seqCount--;

           
        }
        
    }
    
    
    
    
    
    if ([type isEqualToString:VOD_PROGRAM_LIST]) {
        
        
        NSLog(@"点播分类运营组节目: %ld result:%@  type:%@",(long)status,result,type );
        _seqCount --;

        
        if (status == 0) {
            
                NSMutableDictionary *dicResult = [NSMutableDictionary dictionary];
        
                    if (![result objectForKey:@"vodList"]) {
                        
                    }else
                    {
                        //原来的代码
//                        if ([[result objectForKey:@"vodList"] count]>=6) {
//                        
//                            [dicResult setObject:[[result objectForKey:@"vodList"] subarrayWithRange:NSMakeRange(0, 6)] forKey:@"vodList"];
//                        }
                        
                      //我暂时先改个代码
                        if ([[result objectForKey:@"vodList"] count]>0) {
                            
                            [dicResult setObject:[result objectForKey:@"vodList"] forKey:@"vodList"];
                            if (![result objectForKey:@"topicName"]) {
                                
                            }else
                            {
                                [dicResult setObject:[result objectForKey:@"topicName"] forKey:@"topicName"];
                            }
                        }
                        
                        
                        if (![result objectForKey:@"categoryID"]) {
                            
                        }else
                        {
                            [dicResult setObject:[result objectForKey:@"categoryID"] forKey:@"categoryID"];
                        }
                        [dicResult setObject:[result objectForKey:@"count"] forKey:@"count"];
                        [dicResult setObject:[result objectForKey:@"index"] forKey:@"index"];
                        [_menuOperationArray addObject:dicResult];

                    }
            
//            _seqCount --;

            
            if (_seqCount == 0) {
                if (_delegate && [_delegate respondsToSelector:@selector(VODDisplayDataGetSuccessWith:)]) {
                    
                    [_delegate VODDisplayDataGetSuccessWith:[_menuOperationArray mutableCopy]];

                }
                NSLog(@"终于获取到了menuArr:%@",_menuOperationArray);
   
            }
        }
       
        
    }
    
    //专题
    if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
        
        _topicProgramListArray = [NSMutableArray array];
        
        [_topicProgramListArray addObject:result]; ;
        
        if (_delegate && [_delegate respondsToSelector:@selector(VODDisplayDataGetSuccessWith:)]) {
            [_delegate VODDisplayDataGetSuccessWith: [_topicProgramListArray mutableCopy]];
        }
    }
    
    //分类
    if ([type isEqualToString:VOD_CATEGORY_TOPICLIST]) {
        
        _categoryTopicArray = [NSMutableArray array];
        _categoryTopicArray = [result objectForKey:@"topicList"];
        if (_delegate && [_delegate respondsToSelector:@selector(VODDisplayDataGetSuccessWith:)]) {
            [_delegate VODDisplayDataGetSuccessWith:[_categoryTopicArray mutableCopy]];
        }
    }
    
 
}





@end
