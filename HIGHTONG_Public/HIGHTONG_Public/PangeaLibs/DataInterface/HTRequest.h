//
//  HTRequest.h
//  DataInterface
//
//  Created by Kael on 15/7/7.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define kAD_GROUPID  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"groupID"] ]
#define kGroupID     (kAD_GROUPID.length > 0 ? kAD_GROUPID : @"1")

typedef NS_ENUM(NSUInteger, HTCertificateType) {
    Cer_pangeachina,
    Cer_jktv,
};

typedef  void (^HTRequest_AD_Return) (NSInteger status,NSDictionary *result,NSString *type);

@protocol HTRequestDelegate <NSObject>

/**
 * @brief 网络请求回调协议
 * @param ret
 *      返回成功：ret = 0；
 *      超时：ret = 100（借用HTTP状态码表示）；
 *      其它ret值参照portal文档中的描述
 * @param result   返回数据
 * @param type     请求类型
 **/

- (void) HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type;


@end


@interface HTRequest : NSObject <HTRequestDelegate>
{

    id<HTRequestDelegate> _delegate;

}

/**
 HTTPS 证书验证

 @return 验证对象
 */
+ (AFSecurityPolicy*)customSecurityPolicyWithCertificateType:(HTCertificateType)type;

- (id)initWithDelegate:(id<HTRequestDelegate>)delegate;

- (NSString *)encryptingPasswordWithEncryToken:(NSString *)encry;

#pragma mark ---- projectVersion、projectList
//PORTAL版本控制

/**
 *  门户系统版本
 *
 *  @param timeout 超时
 */
-(void)getHightongPORTALVersionWithTimeout:(NSInteger)timeout;

/**
 *  门户系统列表
 *
 *  @param timeout 超时时间  
 */
-(void)getHightongPORTALProjectListWithTimeout:(NSInteger)timeout;
//-(void)getHightongPORTALProjectListWithTimeout:(NSInteger)timeout andResultBlcok:(void (^)(NSInteger status, NSMutableDictionary *result, NSString *type))resultBlock;
/**
 * 区域配置版本
 * @param timeout timeout 超时时间
 */
-(void)getHightongRegionVersionWithTimeout:(NSInteger)timeout;

/**
 * 区域配置列表
 * @param timeout timeout 超时时间
 */
-(void)getHightongRegionListWithTimeout:(NSInteger)timeout;

/**
 * 重新加载配置
 * @param timeout 超时时间
 */
-(void)resetHightongConfigWithTimeout:(NSInteger)timeout;

#pragma mark - ------UGC PORTAL
/**
 *  2.1	判断是否合法主播
 *
 */
- (void)UGCCommonIsValidUser;


/**
 *  2.2 身份认证
 *  @param idNumber   身份证号（String 必传）
 *  @param name   名字（String 必传）
 *  @param mobile   电话号码编码（String 必传）
 *  @param hospital   医院（String 必传）
 *  @param department    科室（String 必传）
 *  @param academicTitle   职称（String 必传）
 *  @param physicianType   医生认证类型（String 必传）
 *  @param physicianCode   执业编码（String 非必传）
 *  @param officialCard    军官证图片（file 非必传）
 *  @param chestCard   胸牌图片（file 非必传）
 *
 */
- (void)UGCCommonIdentificationWithIdNumber:(NSString *)idNumber andName:(NSString *)name andMobile:(NSString *)mobile andHospital:(NSString *)hospital andDepartment:(NSString *)department andAcademicTitle:(NSString *)academicTitle andPhysicianType:(NSString *)physicianType andPhysicianCode:(NSString *)physicianCode andOfficialCard:(UIImage *)officialCard andChestCard:(UIImage *)chestCard;


/**
 *  2.3 开始播放
 *  @param name   会话名称（String 必传）
 *  @param screenFlg   横屏/竖屏；0：横屏，1：竖屏（String 必传）
 *  @param channelId   频道ID（String 必传）
 *
 */
- (void)UGCCommonStartLiveWithName:(NSString *)name andScreenFlg:(NSString *)screenFlg andChannelId:(NSString *)channelId;


/**
 *  2.4 停止播放
 *  @param sessionId   会话ID（String 必传）
 *
 */
- (void)UGCCommonStopLiveWithSessionId:(NSString *)sessionId;


/**
 *  2.5 点播节目上传
 *  @param videoId   视频ID（String 必传）
 *  @param name   点播节目名称（String 必传）
 *  @param thumbnaill   缩略图（String 非必传）
 *  @param cover   封面图片（当虹云返回）（String 非必传）
 *  @param pictureFile   封面图片(用户上传)（File 非必传）
 *  @param size   视频大小（String 非必传）
 *  @param isEncrypt   视频加密状态（String 非必传）
 *  @param categoryId   视频类别ID（String 非必传）
 *  @param category   视频类别（String 非必传）
 *  @param status   视频转码状态（String 非必传）
 *  @param remarks   节目描述（String 非必传）
 *  @param url   播放地址（String 非必传）
 *  @param tags   标签（String 非必传）
 *  @param duration   播放时长（String 非必传）
 *
 */
- (void)UGCCommonInsertProgramWithVideoId:(NSString *)videoId andName:(NSString *)name andThumbnaill:(NSString *)thumbnaill andCover:(NSString *)cover andPictureFile:(UIImage *)pictureFile andSize:(NSString *)size andIsEncrypt:(NSString *)isEncrypt andCategoryId:(NSString *)categoryId andCategory:(NSString *)category andStatus:(NSString *)status andRemarks:(NSString *)remarks andUrl:(NSString *)url andTags:(NSString *)tags andDuration:(NSString *)duration;

/**
 *  2.6 点播节目修改
 *  @param programId   点播节目ID（String 必传）
 *  @param name   点播节目名称（String 非必传）
 *  @param cover   封面图片（File 非必传）
 *  @param categoryId   分类ID（String 非必传）
 *  @param tags   标签（String 非必传）
 *  @param remarks   描述信息（String 非必传）
 *
 */
- (void)UGCCommonUpdateProgramWithProgramId:(NSString *)programId andName:(NSString *)name andCover:(UIImage *)cover andCategoryId:(NSString *)categoryId andTags:(NSString *)tags andRemarks:(NSString *)remarks;

/**
 *  2.7	用户获取自己的点播列表
 *
 */
- (void)UGCCommonGetProgramListByUserInfo;

/**
 *  2.8	获取直播列表
 *
 */
- (void)UGCCommonGetChannelList;

/**
 *  2.9	获取点播列表
 *
 */
- (void)UGCCommonGetProgramList;

/**
 *  2.10	获取标签列表
 *
 */
- (void)UGCCommonGetTagList;

/**
 *  2.11 删除视频
 *  @param programId   视频ID（String 必传）
 *  @param isCleanedOut   是否清空所有节目（String 非必传）
 *
 */
- (void)UGCCommonDeleteProgramWithProgramId:(NSString *)programId andIsCleanedOut:(NSString *)isCleanedOut;

/**
 *  2.12 获取分类列表
 *  @param userId   用户ID（String 必传）
 *
 */
- (void)UGCCommonGetCategoryListWithUserId:(NSString *)userId;

/**
 *  2.13 直播播放
 *  @param channelId   频道ID（String 必传）
 *
 */
- (void)UGCCommonGetLiveUrlWithChannelId:(NSString *)channelId;

/**
 *  2.14 点播播放
 *  @param programId   频道ID（String 必传）
 *
 */
- (void)UGCCommonGetProgramUrlWithProgramId:(NSString *)programId;


/**
 *  2.15 当虹云回调接口
 *  @param videoId   当虹云点播ID（String 必传）
 *  @param status   转码状态（String 必传）
 *  @param playCode   播放码（String 必传）
 *
 */
- (void)UGCCommonUgcCallBackWithVideoId:(NSString *)videoId andStatus:(NSString *)status andPlayCode:(NSString *)playCode;

/**
 *  2.16	获取token接口
 *
 */
- (void)UGCCommonGetToken;

/**
 *  2.17	获取医生职称接口
 *
 */
- (void)UGCCommonGetAcademicTitleWithReturn;


/**
 *  2.18	获取点播播放次数接口
 *  @param programId   视频ID（String 必传）
 *
 */
- (void)UGCCommonGetProgramPlayCountWithProgramId:(NSString *)programId;


/**
 *  2.19	获取直播在线人数接口
 *  @param channelId   视频ID（String 必传）
 *
 */
- (void)UGCCommonGetChannelCountWithChannelId:(NSString *)channelId;


/**
 *  2.20	获取栏目信息接口
 *
 */
- (void)UGCCommonGetColumnInfoWithReturn;


/**
 *  2.21	获取栏目内容信息接口
 *  @param channelId   视频ID（String 必传）
 *
 */
- (void)UGCCommonGetColumnInfoContentWithColumnInId:(NSString *)columnInId;


#pragma mark - ------EPG PORTAL

/**
 *  2.1 EPG浏览 EPG_EVENTLIST         @"eventList"
 *
 *	serviceID = 频道编号（Integer 不可为空）
 @param date = 日期（String 格式：yyyy-MM-dd，如：2011-06-16，空串则默认当天）date参数不传递时根据startDate和endDate参数确定查询范围
 *	@param startDate = 查询开始日期（String 格式：yyyy-MM-dd）startDate不为空时，endDate为必传项
 *	@param endDate = 查询截止日期（String 格式：yyyy-MM-dd）endDate不为空时，startDate为必传项
 *	@param lastUpdateTime = 上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 *
 *
 *  @param timeout 超时时间
 */


/**
 *  @author Kael
 *
 *  @brief 所有频道的列表   (新增)
 *  @param lookbackFlag 回看标识（Integer）1：查询支持回看的频道 其它情况返回全部频道
 *  @param timeout      超时时间
 */
-(void)EPGChannelAllListWithLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout;



-(void)EPGSHOWWithServiceID:(NSString *)serviceID andDate:( NSString *)date andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout;

/**
 *  2.2 频道列表 EPG_CHANNELLIST       @"channelList"
 *
 *  token = token串（可为空,为空时不鉴权）
 terminal_type = 终端类型(Integer 不可为空 0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC )
 lastUpdateTime = 上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 *  @param type         频道类型（Integer 可为空，为空时返回全部频道）取值分类列表type字段
 *  @param lookbackFlag 回看标识（Integer）1：查询支持回看的频道 其它情况返回全部频道
 *  @param timeout      上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 */
-(void)EPGChannelWithType:(NSInteger)type andLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout;

/**
 * @brief 2.3 频道信息查询   EPG_CHANNELQUERY      @"channelQuery"
 *
 * @type TV_CHANGE 接口类型
 *	token = token串（String可为空，为空时不鉴权）
 offset = 偏移（Integer）（预留字段）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD  5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 
 * @param serviceID 频道编号
 * @param timeout 设定请求超时时间
 **/
- (void)TVChangeWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;

/**
 *  2.4 当前后继节目列表  EPG_CURRENTEVENTLIST  @"currentEventList"
 *
 *  @param id  = 频道编号（Integer 可为空）
 *  @param type = 频道类型（Integer为空时返回全部频道）取值分类列表type参数
 *  @param count     条数（Integer 可为空，为空时默认为3）
 *  @param timeout   设定请求超时时间
 */
-(void)EPGPFWithServiceID:(NSString *)serviceID  andType:(NSInteger)type andCount:(NSInteger)count andTimeout:(NSInteger)timeout;

/**
 *  2.5 鉴权直播  EPG_TVPLAY            @"tvPlay"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param serviceID id = 频道编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)TVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;

/**
 *  2.6 增强版鉴权直播  EPG_TVPLAYEX          @"tvPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)TVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout;

/**
 *  2.7 鉴权时移 EPG_PAUSETVPLAY       @"pauseTvPlay"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param serviceID id = 频道编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)PauseTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;

/**
 *  2.8 增强版鉴权时移  EPG_PAUSETVPLAYEX     @"pauseTvPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)PauseTVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout;


/**
 *  2.9 鉴权回看 EPG_EVENTPLAY         @"eventPlay"
 *
 *	token = token串（String不可为空）
 *  terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param eventID id = 节目编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)EventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout;

/**
 *  2.10 增强版鉴权回看  EPG_EVENTPLAYEX       @"eventPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param startTime   播出时间（String 必填）格式为yyyy-MM-dd HH:mm
 *  @param endTime     结束时间（String 可不填）格式为yyyy-MM-dd HH:mm 不填的时候就只匹配播出时间，如果填写了就要匹配结束时间，结束时间填错也不会返回播放链接;第三方的EPG(与本地不匹配)播出时间和结束时间必须成对出现。
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)EventPlayEXWithTVName:(NSString *)tvName andStarTime:(NSString *)startTime andEndTime:(NSString *)endTime andTimeout:(NSInteger)timeout;

/**
 *  2.11 分类列表  EPG_TYPELIST          @"typeLis"
 *
 *	regionID = 区域标识（预留字段）
 terminal_type = 终端类型（0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0）
 *  @param timeout 设定超时时间
 */
-(void)TypeListWithTimeout:(NSInteger)timeout;

/**
 *  2.12 系统配置  EPG_CONFIG            @"config"
 *
 *	regionID = 区域标识（预留字段）
 terminal_type = 终端类型（0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0）
 *  @param timeout 设定超时时间
 */
-(void)ConfigWithTimeout:(NSInteger)timeout;

/**
 *  2.13 终端软件下载   EPG_SOFIWAREDOWN      @"softwareDown"
 *
 *	regionID = 区域标识（预留字段）
 terminal_type = 终端类型（0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0）
 *  @param timeout 设定超时时间
 */
-(void)SoftwareWithTImeout:(NSInteger)timeout;


/**
 *  2.14 系统时间   EPG_SYSTEMTIME        @"systemTime"
 *
 *  @param timeout 设定超时时间
 */
-(void)SystemTimeWithTimeout:(NSInteger)timeout;

/**
 *  2.15 EPG 搜索2  EPG_EVENTSEARCH       @"eventSearch"
 *
 *	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param key       关键字（String）
 *  @param type      频道类型（Integer 可为空）不传或者传递空串时不对频道类型限制，传入错误的频道类型返回空集
 *  @param serviceID 频道编号（String 可为空）不传或者传递空串时不对频道类型限制，允许传入多个频道编号，中间以半角逗号分隔，传入错误的频道编号返回空集
 *  @param startDate 查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param endDate   查询截止日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对截止日期做限制
 *  @param timeout   设定超时时间
 */
-(void)EPGSearchEXWithKey:(NSString *)key andType:(NSInteger)type andServiceID:(NSString *)serviceID andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout;


/**
 *  2.16 热门搜索关键词  EPG_TOPSEARCHWORDS    @"topSearchWords"
 *
 *	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param key     关键字（String 可为空）
 *  @param count   条数（Integer 可为空 默认为10）
 *  @param timeout 设定超时时间
 */
-(void)TopSearchKeyWithKey:(NSString *)key andCount:(NSInteger)count andTimeout:(NSInteger)timeout;

/**
 *  2.17 增加频道收藏  EPG_ADDTVFAVORATE     @"addTvFavorite"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)AddTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;
/**
 *  2.18 删除频道收藏  EPG_DELTVFAVORATE     @"deleteTvFavorite"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)DelTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;

/**
 *  2.19 频道收藏列表 EPG_TVFAVORATELIST    @"tvFavoriteList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout   设定超时时间
 */
-(void)TVFavorateListWithTimeout:(NSInteger)timeout;

/**
 *  2.20 播放记录列表  EPG_TVPLAYLIST        @"tvPlayList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *	@param startDate = 开始日期（String 格式：yyyy-MM-dd）可为空
 * 	@param endDate = 截止日期（String 格式：yyyy-MM-dd）可为空
 *  @param timeout   设定超时时间
 */
-(void)TVPlayListWithTimeout:(NSInteger)timeout;
-(void)TVPlayListWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTImeout:(NSInteger)timeout;


/**
 *  2.21 删除播放记录 EPG_DELTVPLAY         @"deleteTvPlay"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)DelTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout;

/**
 *  2.22 回看停止  EPG_EVENTSTOP         @"eventStop"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param eventID    eventID =节目编号（String 不可为空）
 *  @param breakPoint 断点位置（Integer）记录观看到多长时间的位置，单位：秒
 *  @param timeout    设定超时时间
 */
-(void)EventStopWithEventID:(NSString *)eventID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout;

/**
 *  2.23 查询回看播放记录  EPG_EVENTPLAYLIST     @"eventPlayList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param eventID 节目编号（String）可为空，为空时全部回看记录
 *  @param count   条数（Integer）默认为10，eventID传值时此参数无效
 *  @param timeout 设定超时时间
 */
-(void)EventPlayListWithEventID:(NSString *)eventID andCount:(NSInteger)count andTimeout:(NSInteger)timeout;


/**
 *  2.24 删除回看播放记录   EPG_DELEVENTPLAY      @"deleteEventPlay"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout;

/**
 *  2.25 增加回看收藏  EPG_ADDEVENTFAVORATE  @"addEventFavorite"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *	@param startDate = 开始日期（String 格式：yyyy-MM-dd）可为空
 * 	@param endDate = 截止日期（String 格式：yyyy-MM-dd）可为空

 *  @param timeout 设定超时时间
 */
-(void)AddEventFavorateWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout;
/**
 *  2.26 删除回看收藏  EPG_DELEVENTFAVORATE  @"deleteEventFavorite"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventFavorateWithEventID:(NSString *)eventID anTimeout:(NSInteger)timeout;
/**
 *  2.27 回看收藏列表  EPG_EVENTFAVORATELIST @"eventFavoriteList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */
-(void)EventFavorateListWithTimeout:(NSInteger)timeout;
/**
 *  2.28 热门频道列表  EPG_HOTCHANNELLIST    @"hotChannelList"
 *
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */
-(void)HotChannelListWithTimeout:(NSInteger)timeout;

/**
 *  2.29 预约节目   EPG_ADDEVENTRESERVE   @"addEventReserve"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)AddEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout;
/**
 *  2.30 取消预约节目 EPG_DELEVENTRESERVE   @"deleteEventReserve"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout;

/**
 *  2.31 预约节目列表  EPG_EVENTRESERVELIST  @"eventReserveList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */

/**
 *  2.34 广告列表 eventReserveList
 *
 *  @param regionCode = 区域编码（String）不为空时返回对应区域的广告以及未标识区域的广告；为空时只返回未标识区域的广告
 *  @param timeout 请求超时时间
 */
-(void)ADListWith:(NSInteger)timeout;


/**
 *  更新频道收藏（3.5版本新加的接口）
 *
 *	token = token串（String不可为空）
 *  @param seq          seq= 排序号（Integer 不可为空）
 *  @param serviceID 	id = 频道编号（Integer 不可为空）
 *  @param name      	name = 频道名称（string 不可为空）
 *  @param timeout   请求超时时间
 */
-(void)EPGUpdateTvFavoriteWithSeq:(NSString*)seq andServiceID:(NSString *)serviceID andTVName:(NSString *)name andTimeou:(NSInteger)timeout;



- (void)EPGUpdateTvFavoriteWithNewSortArr:(NSArray *)sortArr andTimeout:(NSInteger)timeout;


/**
 *  序列化的 EPG搜索  epgSearch   3.7.1
 *
 *	token = token串（String不可为空）
 *  @param key          关键字（String不可为空）
 *  @param startDate 	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param endDate      	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param terminal_type      	终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout   请求超时时间
 */


//- (void)EPGUpdateTvFavoriteWithNewSortArr:(NSArray *)sortArr andTimeout:(NSInteger)timeout;


/**
 *  序列化的 EPG搜索  epgSearch  3.7.4
 *
 *	token = token串（String不可为空）
 *  @param key          关键字（String不可为空）
 *  @param startDate 	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param endDate      	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param terminal_type      	终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout   请求超时时间
 */
-(void)epgSearchOrderResultWith:(NSString *)key andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andCount:(NSString*)count andTimeout:(NSInteger)timeout;

-(void)EventRevserveWithTimeou:(NSInteger)timeout;

//获取首页 热门频道  EPG_HOTCHANNELLIST
-(void)EPGGetHomepageHotChannel;

//获取直播界面的频道以及当前后继节目   channelList  返回类型
-(void)EPGGetLivePageChannelInfo;
-(void)EPGGetLivePageChannelInfoAndisType:(NSInteger)type;
//-(void)EPGGetLivePageChannelInfoWithType:(NSInteger)type;
/**
 *  获取首页轮播图  type  slideshowList
 *  @param regionCode = 区域编码（String）不为空时返回对应区域的广告以及未标识区域的广告；为空时只返回未标识区域的广告
 */
-(void)getHomePageSlideshowList;

- (void)HomeADSLiderPagess;//首页轮播图广告

-(void)commAPI_HomeSliderpages;

/**
 *  区域列表   regionType
 *
 *  @param regionType 区域类型(Integer)  1：医院;2：社区;3：学校 默认为1
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param outTime 超时时间
 */
-(void)EPGGetRegionListWithTimeout:(NSInteger)outTime;
/**
 *  查找区域
 *
 *  @param regionType 区域类型(Integer)  1：医院;2：社区;3：学校 默认为1
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param longitude  当前位置的经度（Double,不可为空）
 *  @param latitude   当前位置的纬度 （Double,不可为空）
 *  @param outTime 超时时间
 */
-(void)EPGFindRegionWithTimeout:(NSInteger)outTime;
-(void)EPGFindRegionWithLongitude:(NSString *)longitude andLatitude:(NSString *)latitude andTimeout:(NSInteger)outTime;
/**
 *  区域信息 regionInfo
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param regionCode 区域编码
 *  @param timeout    超时时间
 */
-(void)EPGGetRegionInfoWithRegionCode:(NSString *)regionCode andTimeout:(NSInteger)timeout;


#pragma mark -
#pragma mark ------- VOD PORTAL
#pragma mark - 珠江数码

/*----------------珠江数码点播---------------*/
/**
 * @brief 2.1 点播运营组
 *
 * @type  VOD_OPERATION_LIST 接口类型
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationListWithTimeout:(NSInteger)timeout;

/**
 * @brief 2.2 点播专题
 *
 * @type  VOD_TOPIC_LIST 接口类型
 
 * @param type =类型（Interger，为空默认返回文字专题列表）；1：文字专题；2：海报推荐专题
 * @param scope  筛选范围 （Interger，为空默认返回指定type的全部专题列表）0：全部专题列表；1：运营组专题列表；2：分类专题列表；3：全局专题列表
 * @param operationCode  运营组编码（String）当scope=1时必填
 * @param categoryID  分类编号（Integer）当scope=2时必填
 * @param timeout  设定请求超时时间
 **/
//Old
-(void)VODTopicListWithTimeout:(NSInteger)timeout;
//New
-(void)VODTopicListWithType:(NSInteger)type andScope:(NSString *)scope andOperationCode:(NSString *)operationCode amdCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout;


/**  cap change
 *  2.3点播专题详情
 *  @type    VOD_TOPIC_DETAIL 接口类型
 *  @param topicCode 专题编码（String 不可为空）
 *  @param timeout   设定请求超时时间
 */
- (void)VODTopicDetailWithTopicCode:(NSString*)topicCode andTimeout:(NSInteger)timeout;

/**
 * @brief 2.4 点播分类
 *
 * @type  CATEGORY_LIST 接口类型
 * @param parentID  父分类编号（String）为空默认无父节点
 * @param timeout  设定请求超时时间
 **/
-(void)VODCategoryListWithParentID:(NSString *)parentID andTimeout:(NSInteger)timeout;

/**
 * @brief 2.5 点播运营组分类
 *
 * @type  VOD_OPER_CATEGORY_LIST 接口类型
 * @param operationCode  运营组编码（String 不可为空）
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationCategoryListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout;

//New
/**
 *  2.6 点播分类运营组   VOD_CATEGORY_OPER_LIST
 *
 *  @type VOD_CATEGORY_OPER_LIST 接口类型
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param timeout    设定请求超时时间
 */
-(void)VODCategoryOperationListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout;
//New

/**
 *  1.1	点播分类专题列表
 *  @param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param timeout    设定请求超时时间
 */
-(void)VODCategoryTopicListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout;


/**
 *  2.9 点播节目列表
 *
 *  @type VOD_PROGRAM_LIST 接口类型
 *	@param token = token串（String不可为空）
 @param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param operationCode  运营组编码 （String 可为空）
 *  @param rootCategoryID 根分类编码 （String 可为空）
 *  @param startSeq       起始序号  （Integer）默认为1
 *  @param count          条数    （Integer）默认为10
 *  @param timeout        设定请求超时时间
 */
-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;
//真正仅供首页使用 814点播节目列表
-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index andProgramListIndexName:(NSString *)programName;



//供点播一级界面使用

-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index;


/**
 * @brief 点播运营组节目列表
 *
 * @type  VOD_OPER_PROGRAM_LIST 接口类型
 * @param operationCode  运营组编码（String 不可为空）
 * @param rootCategoryID  根分类编号（String可为空）
 * @param startSeq   起始序号（Integer）默认为1
 * @param count   条数（Integer）默认为10
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;
//******首页的推荐专题获取，需要发送两次数据，标记第几次
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index;

/**
 * @brief 2.10 点播专题节目列表
 *
 * @type  VOD_TOPIC_PROGRAM_LIST 接口类型
 * @param token  token串（String不可为空）
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 * @param topicCode  专题编码（String 不可为空）
 * @param startSeq   起始序号（Integer）默认为1
 * @param count   条数（Integer）默认为10
 * @param timeout  设定请求超时时间
 **/
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;


/**
 * @brief 2.11 点播搜索
 *
 * @type  VOD_SEARCH 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0) ）
 * @param operationCode    运营组编码（String 可为空）
 * @param rootCategoryID   根分类编号（String可为空,但categoryID 非空时为必传项）
 * @param categoryID       分类编号（String可为空）多个分类时以逗号分隔，查询时要求同时满足几个分类条件
 * @param topicCode        专题编码（String 可为空）
 * @param key              关键字（String 不可为空）
 * @param startSeq         起始序号（Integer）默认为1
 * @param count            条数（Integer）默认为10
 * @param timeout          设定请求超时时间
 **/
//old 3.4
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;
//* @param orderType = 排序类型（Integer 可为空）1：新上架 2：最热门
//NEW  3.7 orderType
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andOrderType:(NSInteger)orderType andTimeout:(NSInteger)timeout;

/**
 * @brief 2.12 点播视频详细信息
 *
 * @type  VOD_DETAIL 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;


/**
 * @brief 2.13 点播播放
 *
 * @type  VOD_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param movie_type 媒体类型 (1:正片 2:预览片 默认为1)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODPlayWithContentID:(NSString *)contentID andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout;


/**
 * @brief 2.14 点播询价
 *
 * @type  VOD_INQUIRY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODInquiryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;


/**
 * @brief 2.15 付费点播
 *
 * @type  VOD_PAY_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param lockPWD 解锁密码(String 可以为空)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间...
 **/
-(void)VODPayPlayWithContentID:(NSString *)contentID andWithLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout;


/**
 * @brief 2.16 点播停止
 *
 * @type  VOD_STOP 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param breakPoint  断点位置（Integer）记录观看到多长时间的位置，单位：秒
 * @param timeout  设定请求超时时间
 **/
-(void)VODStopWithContentID:(NSString *)contentID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout;


/**
 * @brief 2.17 点播播放记录列表
 *
 * @type  VOD_PLAY_LIST 接口类型
 * @param token      token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 可为空。不为空(电影contentID或电视剧子集contentID)时查询指定编号的视频播放记录；为空时查询全部视频播放记录（按照startSeq和count截断）
 * @param startSeq  起始序号（Integer）默认为1，contentID不为空时这个字段无效
 * @param count     条数（Integer）默认为10，contentID不为空时这个字段无效
 * @param timeout   设定请求超时时间
 **/
-(void)VODPlayListWithContentID:(NSString *)contentID andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;


/**
 * @brief 2.18 删除点播播放记录
 *
 * @type  DEL_VOD_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String可为空，为空时清空全部点播播放列表）可以删除电视剧单集播放记录，也可以输入电视剧总的contentID删除该剧全部播放记录
 * @param timeout  设定请求超时时间
 **/
-(void)DELVODPlayHistoryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;


/**
 * @brief 2.19 增加点播收藏
 *
 * @type  ADD_VOD_FAVORITE 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String不可为空）不支持传入分集contentID
 * @param timeout  设定请求超时时间
 **/
-(void)ADDVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;


/**
 * @brief 2.20 删除点播收藏
 *
 * @type  DEL_VOD_FAVORITE 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String可为空，为空时清空全部点播收藏列表）不支持传入分集contentID
 * @param timeout  设定请求超时时间
 **/
-(void)DELVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;


/**
 * @brief 2.21 点播收藏列表
 *
 * @type  VOD_FAVORITE_LIST 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param startSeq  起始序号（Integer）默认为1
 * @param count  条数（Integer）默认为10，如果startSeq和count都不传返回全部数据
 * @param timeout  设定请求超时时间
 **/
-(void)VODFavoryteListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;

/**
 * @brief 2.21 点播关联影片列表（推荐节目相关接口）
 *
 * @type VOD_ASSCCIATION 接口类型
 * @param token token串（string 不可为空）
 * @param terminal_type 终端类型
 * @param contentID 视频编号(string)不可为空
 * @param count 条数（integer）默认为10
 **/
-(void)VODAssccationListWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout;
//New
-(void)VODAssccationListWithContentID:(NSString *)contentID andCount:(NSInteger)count  andTimeout:(NSInteger)timeout;

//New
/**
 *  2.23 点播热门搜索关键词
 *
 *  @type  VOD_TOP_SEARCH_KEY 接口类型
 *	@param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param key     关键字（String 可为空）终端类型为5、6时不支持汉字，其他终端可以支持汉字
 *  @param count   条数（Integer 可为空 默认为10）
 *  @param timeout 请求超时时间
 */
-(void)VODTopSearchKeyWithKey:(NSString *)key andCount:(NSInteger)count andTimeout:(NSInteger)timeout;
//New
/**
 *  2.24 点播销售品订购
 *
 *  @type orderProduct 接口类型
 *  @param token token串（String不可为空）
 *  @param productId 销售品编号(String 不可为空)
 *  @param lockPWD   解锁密码(String 可以为空)
 *  @param timeout   青泥鳅超时时间
 */
-(void)VODOrderProductWithProductID:(NSString *)productId andLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout;
/**
 *  2.25 点赞吐槽影片
 *  @type praiseDegrade 接口类型
 *  @param token token串（String不可为空）
 *  @param contentID 视频编号（String 不可为空）电视剧需传剧集视频编号
 *  @param type      类型 (Integer ) 1：点赞 2：吐槽 默认为1
 *  @param timeout   请求超时时间
 */
- (void)VODPraiseDegradeWithContentID:(NSString*)contentID andType:(NSInteger)type andTimeout:(NSInteger)timeout;

/**
 *  2.26 运营组销售品列表
 *  @type operationProductList 接口类型
 *	@param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param token token串（String不可为空）
 *  @param operationCode 运营组编码（String 不可为空）
 *  @param timeout       请求超时时间
 */
- (void)VODOperationProductListWithOperationCode:(NSString*)operationCode andTimeout:(NSInteger)timeout;


////NEW
//- (void)VODSeatchKeyListWithTimeout:(NSInteger)timeout;

/**
 *  2.7 点播运营组菜单
 *
 *  @type VOD_OPER_MENU 接口类型
 *  @param 	operationCode = 运营组编码（String 不可为空）
 *  @param 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0) 预留字段
 *  @param timeout 请求超时时间
 */
- (void)VODOPerMenuQWithOperationCode:(NSString*)operationCode andTimeout:(NSInteger)timeout;

/**
 *  2.8 点播分类菜单
 *
 *  @type VOD_CATEGORY_MENU 接口类型
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒9：PC 默认为0) 预留字段
 *  @param timeout   请求超时时间
 */
- (void)VODCategoryMenuWithCategoryID:(NSInteger)categoryID andTimeout:(NSInteger)timeout;

/**
 *  点播运营组详情
 *
 *  @param operationCode 运营组编码
 *  @param timeout       超时时间
 */
-(void)VODOperationDetailWithOperationCode:(NSString *)operationCode andTimeOut:(NSInteger)timeout;



//点播播放记录列表（新版）
/**
 *  点播播放记录列表 （新版）
 *
 *  @param satrtSeq  起始序号（Integer）默认为1
 *  @param count     条数（Integer）默认为10
 *  @param startDate 开始日期（String 格式：yyyy-MM-dd）可为空
 *  @param endDate   截止日期（String 格式：yyyy-MM-dd）可为空
 *  @param timeout   超时时间
 */
-(void)VODNewPlayRecordListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andSatrtDte:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout;

//点播收藏记录列表（新版）
/**
 *  点播收藏列表（新版）
 *
 *  token  = token串（String不可为空）
 *  @param startSeq 起始序号（Integer）默认为1
 *  @param count    条数（Integer）默认为10，如果startSeq和count都不传返回全部数据
 *  @param timeout  超时时间
 */
-(void)VODNewFavorateListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout;

//点播 首页配置信息列表
/**
 *  首页配置信息列表
 *
 *  @param timeout 超时时间
 */
-(void)VODHomeConfigListWithTimeOut:(NSInteger)timeout;

//点播首页 配置信息列表
/**
 *  点播页菜单配置信息列表
 *
 *  @param timeout 超时时间
 */
-(void)VODMenuConfigListWithTimeout:(NSInteger)timeout;



//获取首页轮播图   POSTIMAGE
-(void)VODGetHomepageADImage;

/**
 *  2.32	上传订单
 *
 *  	token = token串（String不可为空）
 * @param 	orderId = 订单编号（String 不可为空）
 * @param 	price = 影片价格（float不可为空）保留两位小数（单位：元）
 * @param 	contentId = 视频编号（String 不可为空）电视剧需传子集视频编号
 * @param  vodId = 总集视频编号（String 可为空）电视剧需传总集视频编号
 */
- (void)VODUpLoadOrderWithOrderId:(NSString*)orderId andPrice:(float)price andContentId:(NSString*)contentId andVodId:(NSString*)vodId andTimeout:(NSInteger)outTime;
/**
 *  2.33	点赞吐槽影片详情
 *
 *  token  = token串（String不可为空）
 *  @param 	contentID = 视频编号（String 不可为空）
 *
 *
 */
- (void)VODPraiseDegradeDetailWithContentID:(NSString*)contentID andTimeout:(NSInteger)outTime;
/**
 *  2.34 医院信息 
 *
 *  terminal_type  = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  code  = 医院编码（String,不可为空）
 *  @param outTime 超时时间
 */
- (void)VODHospitalInfoWithTimeout:(NSInteger)outTime;//使用默认的医院code
- (void)VODHospitalInfoWithCode:(NSString *)code andTimeout:(NSInteger)outTime;//接口传入code

/**
 *  2.35 查找医院  getHospital
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param longitude  当前位置的经度（Double,不可为空）
 *  @param latitude   当前位置的纬度 （Double,不可为空）
 *  @param outTime 超时时间
 */
-(void)VODFindHospitalWithTimeout:(NSInteger)outTime;
/**
 *  2.36 医院列表 hospitalList
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param outTime 超时时间
 */
-(void)VODHospitalListWithTimeout:(NSInteger)outTime;

#pragma mark - 
#pragma mark -
#pragma mark - ------ UPORTAL (New)

/**
 *  用户登陆 userLogin
 *
 *  @param userName 用户名（String 不可为空）
 *  @param passWord 用户密码（String 不可为空）要求终端加密后传入
 *  @param timeout  设定请求超时时间
 */
-(void)userLoginWithUserName:(NSString *)userName andPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout;

/**
 *  手机号登陆 mobileLogin
 *
 *  @param mobilePhone 手机号（String 不可为空）
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param timeout     设定请求超时时间
 */
-(void)mobileLoginWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout;

/**
 *  游客登录 guestLogin
 * 	deviceID= 设备唯一码（String 不可为空）
 *  @param timeout 设定超时时间
 */
-(void)guestLoginWithTimeout:(NSInteger)timeout;

/**
 *  用户签到 signIn
 * 	token = token串（String 不可为空）
 *  @param timeout 设定网络超时时间
 */
-(void)signInWithTimeout:(NSInteger)timeout;

/**
 *  查询用户今日是否已经签到
 *
 *  @param timeout 设定网络超时时间
 */
- (void)isSignInWithTimeout:(NSInteger)timeout;

/**
 *  用户注销 logout
 * 	token = token串（String 不可为空）
 *  deviceID= 唯一标识（String 不可为空）
 *  @param timeout 设定网络超时时间
 */
-(void)logoutWithTimeout:(NSInteger)timeout;

/**
 *  设备激活  deviceActive
 *	deviceID= 唯一标识（String 不可为空）
 *  @param userName 用户名
 *  @param passWord 密码
 *  @param timeout  设定网络超时时间
 */
-(void)deviceActiveWithUserName:(NSString *)userName andPassword:(NSString *)passWord andTimeout:(NSInteger)timeout;

/**
 *  设置付费码锁  setLockNumber
 *	token = token串（String 不可为空）
 *  @param oldNumber 旧密码锁（String）设置密码时填空串
 *  @param newNumber 新密码锁（String）取消密码锁时填空串
 *  @param timeout   设定网络超时时间
 */
-(void)setLockNumberWithOldNumber:(NSString *)oldNumber andNewnumber:(NSString *)newNumber andTimeout:(NSInteger)timeout;
/**
 *  重置付费码锁  resetLockNumber
 *	token = token串（String 不可为空）
 *  @param userName 用户名（String 不可为空）
 *  @param passWord 用户密码（String 不可为空）要求终端加密后传入
 *  @param timeout  设定网络超时时间
 */
-(void)resetLockNumberWithUserName:(NSString *)userName andPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout;
/**
 *  查询付费码锁状态  isSetLockNumber
 *	token = token串（String 不可为空）
 *  @param timeout 设定网络超时时间
 */
-(void)isSetLockNumberWithTimeout:(NSInteger)timeout;
/**
 *  查询用户信息  queryUserInfo
 *	token = token串（String 不可为空）
 *  @param timeout 设定网络超时时间
 */
-(void)NewqueryUserInfoWithTimeout:(NSInteger)timeout;

/**
 *  修改用户密码  changeUserPwd
 *	token = token串（String 不可为空）
 *  @param oldPassWord 旧密码（String 不可为空）要求终端加密后传入
 *  @param newPassWord 新密码（String 不可为空）要求终端加密后传入
 *  @param timeout     设定网络超时时间
 */
-(void)NewchangeUserPwdWithOldPassWord:(NSString *)oldPassWord andNewPassWord:(NSString *)newPassWord andTimeout:(NSInteger)timeout;
/**
 *  获取手机验证码  authCode
 *
 *  @param type        验证码类型(0：注册 2：重置密码)
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param timeout     设定网络超时时间
 */
-(void)NewauthCodeOfPhoneWithType:(NSString *)type andMobilePhone:(NSString *)mobilePhone andTimeout:(NSInteger)timeout;
/**
 *  手机注册  mobileRegister
 *
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空）
 *  @param timeout     设定网络超时时间
 */
-(void)NewmobileRegisterWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;
/**
 *  用户密码重置  resetUserPwd
 *
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空）
 *  @param timeout     设定网络超时时间
 */
-(void)NewresetUserPwdWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;
/**
 *  变更手机号  alterMobilePhone
 *
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空）
 *  @param timeout     设定网络超时时间
 */
-(void)NewalterMobilePhoneWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;

/**
 *  上传头像  uploadHeadPhoto
 *
 *  @param imageFile  图片流
 *  @param width     图片宽度
 *  @param height    推按高度
 *  @param timeout   网络超时
 */
-(void)NewUploadHeadPhotoWithImageData:(UIImage *)imageFile andWidth:(NSInteger)width andHeight:(NSInteger)height andTimeout:(NSInteger)timeout;
/**
 *  修改昵称  updateNickName
 *
 *  @param nickName 昵称
 *  @param timeout  超时
 */
-(void)NewUpdateNickNameWithNickName:(NSString *)nickName andTimeout:(NSInteger)timeout;
/**
 *  帮助反馈 helpBack
 *
 *  @param suggestionType 建议类型
 *  @param suggestion     建议内容
 *  @param contact        联系方式
 *  @param timeout        超时
 */
-(void)NewHelpBackWithSuggestionType:(NSString *)suggestionType andSuggestion:(NSString *)suggestion andContact:(NSString *)contact andTimemout:(NSInteger)timeout;


/**
 *  校验昵称 checkNickName
 *
 *  @param nickName 用户昵称
 *  @param timeout  超时
 */
-(void)checkNickNameWithNickName:(NSString *)nickName andTimeout:(NSInteger)timeout;


/**
 *  验证手机验证码 checkAuthCode
 *
 *  @param mobilePhone 手机号
 *  @param authCode    验证码
 *  @param timeout     超时
 */
-(void)checkAuthCodeWithMobilePhone:(NSString *)mobilePhone andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;


/**
 *  修改用户信息 updateUserInfo
 *
 *  @param nickName   昵称名称（String）不传代表不修改该项数据
 *  @param birthday   出生日期（String格式：yyyy-MM-dd）不传代表不修改该项数据
 *  @param sex        性别（Integer） 数据含义参考附录3.1 不传代表不修改该项数据
 *  @param profession 职业（Integer） 数据含义参考附录3.2 不传代表不修改该项数据
 *  @param education  文化程度（Integer） 数据含义参考附录3.3 不传代表不修改该项数据
 *  @param userName   用户姓名 （String）不传代表不修改该项数据
 *  @param timeout    超时
 */
-(void)updateUserInfoWithNickName:(NSString *)nickName andBirthday:(NSString *)birthday andSex:(NSString *)sex andProfession:(NSString *)profession andEducation:(NSString *)education andUserName:(NSString *)userName andTimeout:(NSInteger)timeout;

/**
 *  身份token 换取 鉴权token
 *
 *  @param dentityToken= 身份令牌（String 不可为空）
 *  @param outTime 超时时间
 */
-(void)UPORTALGetTokenWithTimeout:(NSInteger)outTime;


/**
 *  查询用户密码是否正确
 *
 *  @param passWord 密码
 *  @param timeout  请求超时时间
 */
-(void)checkPassWordWithPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout;

/**
 *  2.28	动态口令登录（验证+注册+登录）
 *
 *  @param mobilePhone 手机号 (String不可为空)
 *  @param authCode    验证码 (String不可为空)
 *  @param code        区域编码（String）
 *  @param deviceID    唯一标识（String 可为空）
 *  @param timeout     请求超时时间
 */
-(void)authCodeLoginWithMobilePhone:(NSString *)mobilePhone andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;

/**
 *  2.29	动态口令变更手机号
 *
 *在用户登录的情况下，输入原手机和新手机的验证码变更登录手机号
  动态口令变更手机号有两种方案，终端可根据情况选择使用
  方案一：直接使用本接口，任何以下参数不能为空
  方案二：首先调用2.23（验证手机验证码）接口，校验过原手机验证码，再调用该接口（此时用户密码可以不传，要求两个接口调用间隔不超过5分钟，否则报-6错误）进行手机号变更
 *  @param  token             串（String 不可为空）
 *  @param mobilePhone        手机号(String 不可为空)
 *  @param oldMobileAauthCode 验证码（String 方案二可不传） 需要给原手机发送注册验证码
 *  @param authCode           验证码（String 不可为空） 需要给新手机发送注册验证码
 *  @param timeout            请求超时时间
 */

-(void)authCodeAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone andOldMobileAauthCode:(NSString *)oldMobileAuthCode andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout;


/**
 *  江西TV用户绑定
 *
 *  @param idCard
 *  @param identityNum
 *  @param phoneNo
 *  @param realName
 *  @param type
 */
- (void)UPORTALBindJxTVWithIdCard:(NSString *)idCard withIdentityNum:(NSString *)identityNum withPhoneNo:(NSString *)phoneNo withRealName:(NSString *)realName withType:(NSInteger)type;


/**
 *  江西TV用户解除绑定
 *
 */
- (void)UPORTALUNBindJxTV;


/**
 *  获取江西TV用户绑定信息
 *
 */
- (void)UPORTALUserBindInfoJxTV;


/**
 *  查询江西TV用户是否绑定
 *
 */
- (void)UPORTALUserBindStatusJxTV;

#pragma mark - ADPORTAL
//点播播放 + 广告
-(void)VODPlayWithContentID:(NSString *)contentID andName:(NSString *)name andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout;

-(void)TVPlayWithServiceID:(NSString*)serviceID andServiceName:(NSString *)serviceName andTimeout:(NSInteger)timeout;


-(void)EPGPlayWithServiceID:(NSString *)serviceID andServiceName:(NSString *)serviceName andEventID:(NSString *)eventID  andTimeout:(NSInteger)timeout;



/**
 *  广告 用户分组
 *
 *  @param token  token串（String可为空）token串为空时代表是游客身份
 *  @param devID  设备唯一编号（String 不可为空）卡号、序列号或者MAC地址，终端设备根据实际情况填写
 *  @param devType 设备类型（String 不可为空）：APHONE（Android手机）、APAD（AndroidPad）、ASTB（Android机顶盒）、ATV（Android一体机）、IPHONE（苹果手机）、IPAD（苹果PAD）
 *  @param screenWidth 屏幕宽（Integer可为空）
 *  @param screenHeight 请求超时
 *  @param outTime 请求超时
 */

-(void)ADUserGroupWithTimeout:(NSInteger)outTime;


/**
 *  广告信息
 *  ----->>actionInfo:
 *  @param ADCode    广告位编码（String 不可为空）广告系统提供编码值
 *  @param type      资产类型（Integer） 0：无 1：直播频道 2：点播节目
 *  @param assetID   资产编号(String 可为空) 直播和回看时传频道ID，点播时传ContentID
 *  @param assetName 资产名称(String 可为空)直播和回看时传频道名称，点播时传视频名称
 *  ----->>userInfo:
 *  @param groupID   用户分组编号(Integer 不可为空)
 *  @param token     token串（String可为空）token串为空时代表是游客身份
 *  @param outTime   请求 超时时间
 */
-(void)ADInfoWithADCode:(NSString *)ADCode andType:(NSString *)type andAssetID:(NSString *)assetID andAssetName:(NSString *)assetName andGroup:(NSString *)groupID andTimeout:(NSInteger)outTime;

-(void)ADInfoWithADCode:(NSString *)ADCode andType:(NSString *)type andAssetID:(NSString *)assetID andAssetName:(NSString *)assetName andGroup:(NSString *)groupID andTimeout:(NSInteger)outTime andReturnBlcok:(HTRequest_AD_Return)returnBlock;

/**
 *  广告 配置文件
 *
 *  @param outTime 超时时间
 */
-(void)ADConfigWithTimout:(NSInteger)outTime;


#pragma mark   医院部分 ------------- polymedicine---------------------------医院部分-----------------------


/**
 *  2.1	首页医院logo和名称
 *
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
//- (void)YJIntroduceDetailWithInstanceCode:(NSString *)instanceCode;
- (void)YJIntroduceDetail;

/**
 *  2.22	医院列表
 *
 *  @param areaId   区域编号(非必填)
 *  @param pageNo   页码
 *  @param pageSize 每页大小
 */
- (void)YJIntroduceListWithAreaId:(NSString *)areaId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;

/**
 *  2.3	科室列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param level        层级 (非必填,为空时返回所有)
 *  @param returnBlock  返回数据
 */
- (void)YJDepartmentAllListWithInstanceCode:(NSString *)instanceCode withLevel:(NSString *)level;

/**
 *  2.5	根据楼栋名称查询科室 新版————2.15	根据楼栋名称查询科室
 *
 *  @param pageNo       页码
 *  @param pageSize     每页大小
 *  @param keyWord      搜索关键字
 *  @param buildId      建筑物ID(就诊导航全局搜索时可不传该参数,医院的建筑物ID)
 *  @param returnBlock  返回数据
 */
- (void)YJBuildingSearchWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyWord:(NSString *)keyWord withBuildId:(NSString *)buildId withInstanceCode:(NSString *)instanceCode;


/**
 *  2.6	科室搜索
 *
 *  @param name 搜索内容（String不可为空）
 *  @param returnBlock 返回数据
 */
//- (void)YJDepartmentListWithName:(NSString *)name;


/**
 *  2.7	专家列表   新版的2.4
 *
 *  @param instanceCode 应用编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param depId        科室Id(服务端需要对下一级或更多级进行处理)
 *  @param returnBlock  返回数据
 */
- (void)YJExpertAllListWithInstanceCode:(NSString *)instanceCode WithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withDepId:(NSString *)depId withMobile:(NSString *)mobile;


/**
 *  2.8	专家详情   新版2.5专家明细
 *
 *  @param expertID    专家编号
 *  @param returnBlock 返回数据
 */
- (void)YJExpertDetailWithID:(NSString *)expertID withMobile:(NSString *)mobile;


/**
 *  2.9	专家搜索   2.6
 *
 *  @param instanceCode 医院编号
 *  @param name        搜索内容（String不可为空）
 *  @param pageNo      当前页码
 *  @param pageSize    每页显示条数
 *  @param source      搜索来源(此字段TV终端可忽略,手机终端必传【全部专家搜索:ALL,名医榜内搜索:SUPER,科室内搜索:科室ID】)
 *  @param returnBlock 返回数据
 */
- (void)YJExpertByNameListWithInstanceCode:(NSString *)instanceCode withName:(NSString *)name withPageNo:(NSString *)pageNo andPageSize:(NSString *)pageSize withSource:(NSString *)source;


/**
 *  2.10	根据科室查询医生  2.7
 *
 *  @param depId       科室编号（String不可为空）
 *  @param pageNo      当前页码
 *  @param pageSize    每页显示条数
 *  @param returnBlock 返回数据
 */
- (void)YJExpertByDeptidListWithDepID:(NSString *)depId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;


/**
 *  2.11	热词列表
 *
 *  @param businessType 热词类型（Integer不可为空；1.科室热词  2.医生热词）
 *  @param returnBlock  返回数据
 */
//- (void)YJHotwordsListWithBusinessType:(NSInteger)businessType;

/**
 *  新2.8	资讯列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param returnBlock  返回数据
 */
- (void)YJNewsListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType;

/**
 *  新版增加————2.10	就诊须知【菜单】
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJVisitMenuWithInstanceCode:(NSString *)instanceCode;


/**
 *  新版增加————2.12	名医列表
 *
 *  @param instanceCode 应用编码(非必填，带入时获取特定医院的名医)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param returnBlock  返回数据
 */
- (void)YJExpertNominateWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;

/**
 *  新版增加————2.13	导诊图-大楼列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJBuildingMenuWithInstanceCode:(NSString *)instanceCode;


/**
 *  新版增加————2.14	导诊图-楼层列表
 *
 *  @param buildId     医院大楼ID(必填)
 *  @param pageNo      当前页码
 *  @param pageSize    每页大小
 *  @param returnBlock 返回数据
 */
- (void)YJBuildingFloorsWithBuildId:(NSString *)buildId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;




/**
 *  2.14	就诊卡列表   10.1.1	就诊卡列表
 *
 *  @param mobile      手机号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;

/**
 *  2.15	就诊卡详情  10.1.2	就诊卡详情
 *
 *  @param clinicNo    门诊卡号
 *  @param returnBlock 返回数据
 */
- (void)YJPatientDetailWithClinicNo:(NSString *)clinicNo;

/**
 *  2.16	就诊卡绑定  10.1.3	就诊卡绑定
 *
 *  @param name        病人姓名（String不可为空）
 *  @param clinicNo    住院号（String不可为空）
 *  @param identify    身份证号（String不可为空）
 *  @param mobile      手机号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientSaveWithName:(NSString *)name withClinicNo:(NSString *)clinicNo withIdentify:(NSString *)identify withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;


/**
 *  2.17	就诊卡解除绑定
 *
 *  @param cardID      编号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientEditWithID:(NSString *)cardID withInstanceCode:(NSString *)instanceCode;


/**
 *  2.19	我的关注医生列表  新2.19
 *
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
- (void)YJCollectionListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;


/**
 *  新接口 2.20	关注/取消关注医生
 *
 *  @param expertId     医生编号（String不可为空）
 *  @param focusFlag    关注标识 0：取消关注  1：关注
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
- (void)YJCollectionSaveWithExpertId:(NSString *)expertId withFocusFlag:(NSString *)focusFlag withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;


/**
 *  2.22	关于我们
 *
 *  @param returnBlock 返回数据
 */
//- (void)YJAboutDetail;


/**
 *  2.23	意见反馈  2.21
 *
 *  @param optionContent 意见选择内容（String）
 *  @param content       意见输入内容（String）
 *  @param contact       联系方式（String）
 *  @param returnBlock   返回数据
 */
- (void)YJSuggestionSaveWithOptionContent:(NSString *)optionContent withContent:(NSString *)content withContact:(NSString *)contact withMobile:(NSString *)mobile;


/**
 *  2.23	推广图片
 *
 *  @param imageType   图片类型 0:二维码 1:wife推广大图
 *  @param returnBlock 返回数据
 */
- (void)YJRegionPopularizeimageWithImageType:(NSString *)imageType;


/**
 *  2.24	资讯轮播图片
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJNewsSlideWithInstanceCode:(NSString *)instanceCode withPlatType:(NSString *)platType;


/**
 *  2.24	版本菜单配置列表  2.18	版本菜单配置列表
 *
 *  @param position    菜单位置（1.首页 2.我的医疗 3.我的费用 ......）
 *  @param returnBlock 返回数据
 */
- (void)YJMenuListWithInstanceCode:(NSString *)instanceCode withPosition:(NSString *)position;


/**
 *  2.28	就诊导航界面医院电话与交通信息  新版2.17	就诊导航界面医院电话与交通信息(乘车指南)
 *
 *  @param returnBlock 返回数据
 */
- (void)YJIntroduceInfoWithInstanceCode:(NSString *)instanceCode;


/**
 *  2.25	发送短信   6.1	验证码短信
 
 *
 *  @param instanceCode 运用编码（必填）
 *  @param megCategory  消息分类【见消息类型说明】（必填）
 *  @param returnBlock  返回数据
 */
- (void)YJSmsSendWithInstanceCode:(NSString *)instanceCode withMsgCategory:(NSString *)megCategory withMobile:(NSString *)mobile
;



/**
 *  2.30	报告单查看验证码校验   6.2	验证码校验
 *
 *  @param randomNum   验证码（必填）
 *  @param mobile      手机号码（必填）
 *  @param returnBlock 返回数据
 */
- (void)YJReportSmsValidWithRandomNum:(NSString *)randomNum withMobile:(NSString *)mobile withMsgCategory:(NSString *)megCategory;


/**
 *  2.26(V1.3)  养生保健分类
 *
 *  @param instanceCode 应用编码(必填)
 */
- (void)YJHealthCareCategoryWithInstanceCode:(NSString *)instanceCode;


/**
 *  2.27(V1.3)  养生保健列表
 *
 *  @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *  @param categoryId   分类编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 */
- (void)YJHealthCareListWithCategoryId:(NSString *)categoryId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType;



/**
 *  2.28(V1.3)  养生保健轮播图片
 *
 *  @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *  @param categoryId   分类编码(必填)
 */
- (void)YJHealthCareSlideWithCategoryId:(NSString *)categoryId withPlatType:(NSString *)platType;


/**
 *  2.29(V1.3)  养生保健咨询列表
 * @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *
 */
- (void)YJHealthCareNewsListWithPlatType:(NSString *)platType;


/**
 *  2.30(V1.3)  医院关联区域列表
 *
 */
- (void)YJIntroduceAreaList;


/**
 *  2.33(V1.5)  养生保健拉取推荐（单内容推荐）
 *
 */
- (void)YJHealthCarePull;



/**
 *  3.1	附近医院
 *
 *  @param longitude   经度
 *  @param latitude    纬度
 *  @param returnBlock 返回数据
 */
- (void)YJIntroduceNearbyWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude;


/**
 *  4.1	获取视频节目播放地址(原接口3.1)
 *
 *  @param instanceCode 医院实例（String 可为空）
 *  @param videoId      视频ID（String 不可为空）
 *  @param returnBlock
 */
- (void)YJCdnVideoUrlWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withVideoId:(NSString *)videoId;



/**
 *  4.2	获取视频节目分类信息(原接口3.2)
 *
 *  @param instanceCode 医院实例编码（可为空）
 *  @param global      是否包括系统公共的分类(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param homeFlag    首页显示标记(可为空,传空默认值为0)
 *  @param pageNo      当前页码(可为空，传空不分页)
 *  @param pageSize    分页大小(可为空，传空不分页)
 *  @param returnBlock 请求返回数据
 */
- (void)YJHealthyTypeListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;


/**
 *  4.3	获取视频节目信息(原接口3.3)
 *
 *  @param global      是否包括系统该分类下全国性的视频节目(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param pageNo      页码（String 不可为空）
 *  @param pageSize    分页大小（String 不可为空）
 *  @param typeId      分类(String 不可为空)
 *  @param order       年份排序顺序(可为空，传空默认值为1)
 *  @param year        年份(可为空)
 *  @param tagId       标签ID(可为空)
 *  @param returnBlock 请求返回数据
 */
- (void)YJHealthyHealthyVideoListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withTypeId:(NSString *)typeId withOrder:(NSString *)order withYear:(NSString *)year withTagId:(NSString *)tagId;



/**
 *  4.4	视频节目推荐（原接口3.4）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoId      视频节目ID（String 不可为空）
 *  @param type         视频分类（String 不可为空）
 *  @param maxCount     最多返回多少条推荐视频
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoRecommendWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withType:(NSString *)type withMaxCount:(NSString *)maxCount;



/**
 *  4.5	加入收藏夹（原接口3.5）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoId      视频节目ID（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoAddfavoriteWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withMobile:(NSString *)mobile;



/**
 *  4.6	获取收藏夹列表（原接口3.6）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param pageNo       页码（String 不可为空）
 *  @param pageSize     分页大小（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoListFavoriteWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withMobile:(NSString *)mobile;



/**
 *  4.7	删除收藏夹中的记录(原接口3.7)
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoIds     视频ID列表（String 不可为空，多个id之间用”,”分割）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoDelFavoriteWithInstanceCode:(NSString *)instanceCode WithVideoIds:(NSString *)videoIds withMobile:(NSString *)mobile;



/**
 *  4.8	清空收藏夹(原接口3.8)
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;


/**
 *  4.9	获取视频分类与视频(原接口3.9)
 *
 *  @param global       是否包括系统公共的分类与视频(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoCount   每个分类最多返回视频信息的记录数
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTypeListVideoWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode WithVideoCount:(NSString *)videoCount;

//--------------------------------------------

/**
 *  4.10	获取轮播节目列表(新增接口)
 *
 *  @param global       是否包括系统公共的分类(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param instanceCode 医院编号(可为空)
 *  @param order        排序顺序(可为空,默认值为1)
 *  @param maxCount     返回视频节目最大记录数
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTopListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withOrder:(NSString *)order withMaxCount:(NSString *)maxCount;



/**
 *  4.11	获取视频发布年份列表(新增接口)
 *
 *  @param instanceCode 医院编号(可为空)
 *  @param typeId       视频分类ID(不可为空)
 *  @param order        年份排序顺序(可为空，默认值为1)
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyYearListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withOrder:(NSString *)order;

/**
 *  4.12	获取视频疾病标签列表(新增接口)
 *
 *  @param instanceCode 医院编号(可为空)
 *  @param typeId       视频分类ID(可为空)
 *  @param year         年份(可为空)
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTagListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withYear:(NSString *)year;



//---------------------------------------------------------------
/**
 *  4.1	我的费用[挂号费] 9.1.1	我的费用
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJRegistrationCostListWithInHospitalNo:(NSString *)inHospitalNo;


/**
 *  4.2	我的费用[检查费]  9.1.2	我的费用[检查费]
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJInspectCostListWithInHospitalNo:(NSString *)inHospitalNo;


/**
 *  5.1	就诊记录  8.1.1	就诊记录
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJHospitalRecordListWithInHospitalNo:(NSString *)inHospitalNo;


/**
 *  6.1	检验单  7.1.1	检验单
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJTestBillListWithInHospitalNo:(NSString *)inHospitalNo;


/**
 *  6.2	检验单删除  7.1.2	检验单删除
 *
 *  @param billID      编号字符串（多选唯一编号 用逗号拼接成字符串）
 *  @param returnBlock 返回数据
 */
- (void)YJTestBillDeleteWithID:(NSString *)billID;


/**
 *  6.3	检查单  7.1.3	检查单
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJCheckBillListWithInHospitalNo:(NSString *)inHospitalNo;


/**
 *  6.4	检查单删除  7.1.4	检查单删除
 *
 *  @param checkBillID 编号字符串（多选唯一编号 用逗号拼接成字符串）
 *  @param returnBlock 返回数据
 */
- (void)YJCheckBillDeleteWithId:(NSString *)checkBillID;


/**
 *  7.1	获取医生排班日期列表  5.1	获取医生排班日期列表.
 *
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineTimeLineDateWithInstanceCode:(NSString *)instanceCode;


/**
 *  7.2	获取医生排班汇总信息 5.2	获取医生排班汇总信息
 *
 *  @param expertId    医生id编码（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineExpertsSummaryWithExpertID:(NSString *)expertId withInstanceCode:(NSString *)instanceCode;


/**
 *  7.3	获取医生排班日信息列表      5.3
 *
 *  @param expertId    医生id编码（String 不可为空）
 *  @param date        日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param middy       上午与下午标识(上午：1， 下午：2)
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineExpertDailyWithInstanceCode:(NSString *)instanceCode withExpertid:(NSString *)expertId withDate:(NSString *)date withMiddy:(NSString *)middy;


/**
 *  7.4	获取科室医生排班汇总信息列表   5.4	获取科室医生排班汇总信息列表
 *
 *  @param departmentId 科室id编码（String 不可为空）
 *  @param date         日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param returnBlock  返回数据
 */
- (void)YJAppointmentTimeLineDepartmentSummaryWithDepartmentId:(NSString *)departmentId withDate:(NSString*)date withInstanceCode:(NSString *)instanceCode;


/**
 *  7.5	预约挂号   5.5	预约挂号
 *
 *  @param identify        身份证号码（String 不可为空）
 *  @param expertId        医生Id编码（String 不可为空）
 *  @param timeLineId      预约排班id编码（String 不可为空）
 *  @param timeLineDate    排班日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param patientName     患者名称（String 不可为空）
 *  @param patientClinicNo 诊疗卡号码（String 可为空）
 *  @param returnBlock     返回数据
 */
- (void)YJAppointmentOrderCreateWithIdentify:(NSString *)identify withExpertId:(NSString *)expertId withTimeLineId:(NSString *)timeLineId withTimeLineDate:(NSString *)timeLineDate withPatientName:(NSString *)patientName withPatientClinicNo:(NSString *)patientClinicNo withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;

/**
 *  7.6	获取挂号订单列表   5.6	获取挂号订单列表
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param pageNo      页码(int 不可为空)
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withMobile:(NSString *)mobile;


/**
 *  7.7	预约挂号取消   5.7
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param orderId     订单Id（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderCancelWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMobile:(NSString *)mobile;


/**
 *  7.8	删除预约挂号订单  5.8
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param orderIds    订单Id列表（String 不可为空，多个ID采用逗号进行分割）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile;


/**
 *  7.9	获取检查订单列表  5.9	获取检查订单列表
 *
 *  @param pageNo      页码
 *  @param pageSize    分页大小
 *  @param mobile      手机号码
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentCheckOrderListWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;

/**
 *  7.10	删除检查订单  5.10	删除检查订单
 *
 *  @param mobile      手机号码
 *  @param orderIds    订单Id列表（String 不可为空，多个ID采用逗号进行分割）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentCheckOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile;


/**
 *  7.11	获取预约挂号与检查订单明细  5.11	获取预约挂号与检查订单明细
 *
 *  @param mobile      手机号码
 *  @param orderId     订单Id
 *  @param msgBsy      订单类型，回传推送消息中的msgBsy字段
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderDetailWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMsgBsy:(NSString *)msgBsy withMobile:(NSString *)mobile;


/**
 *  8.2	消息推送绑定
 *
 *  @param channelId   百度云推送分派设备唯一标识
 *  @param deviceType  设备系统类型 3： android  4: IOS
 *  @param bindBsy     绑定业务 0: 全局业务推送
 *  @param returnBlock 返回数据
 */
- (void)YJPushTagBindWithChannelId:(NSString *)channelId withDeviceType:(NSString *)deviceType withBindBsy:(NSString *)bindBsy;


/**
 *  8.3	消息详情
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJPushMsgDetailWithInhospitalNo:(NSString *)inHospitalNo;


/**
 *  9.1	排队叫号查询
 *
 *  @param mobile      手机号码（必填）
 *  @param name        姓名（可选）
 *  @param returnBlock 返回数据
 */
- (void)YJTreatmentFindWithMobile:(NSString *)mobile withName:(NSString *)name;


/**
 *  9.2	排队叫号通知完成
 *
 *  @param queueId     队列ID(必填)
 *  @param returnBlock 返回数据
 */
- (void)YJTreatmentCalledWithQueueId:(NSString *)queueId;



/**
 *  2.1	获取子频道列表
 *
 *  @param instanceCode 医院编号，（String 可为空）
 *  @param parentId     父级频道ID，（String 可为空）
 *  @param homeFlag     首页显示标记，（String 不可为空）
 *  @param maxCount     最大记录数，（String 不可为空）
 */
- (void)YJHealthyVodGetChildChannelWithInstanceCode:(NSString *)instanceCode withParentId:(NSString *)parentId withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount;


/**
 *  2.2	获取频道信息
 *
 *  @param channelId    频道ID，（String 不可为空）
 *  @param instanceCode 医院编号，（String 可为空）
 *  @param homeFlag     首页显示标记，（String 不可为空）
 *  @param maxCount     子频道最大记录数，（String 不可为空）
 */
- (void)YJHealthyVodChannelInfoWithChannelId:(NSString *)channelId withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount;


/**
 *  2.3	获取频道过滤器列表
 *
 *  @param channelId   频道ID，（String 不可为空）
 *  @param includeYear 是否包括年份过滤器，（String 不可为空）
 */
- (void)YJHealthyVodChannelFiltersWithChannelId:(NSString *)channelId withIncludeYear:(NSString *)includeYear;


/**
 *  2.4	获取子频道节目列表
 *
 *  @param channelId   频道ID，（JSON数组 不可为空）
 *  @param maxCount    节目最大记录数，（String 不可为空）
 */
- (void)YJHealthyVodChannelProgramListWithChannelId:(NSMutableArray *)channelIdArr withMaxCount:(NSString *)maxCount;


/**
 *  2.5	获取节目列表
 *
 *  @param channelId   频道ID，（String 不可为空）
 *  @param pageNo      页码，（String 不可为空）
 *  @param pageSize    分页大小，（String 不可为空）
 *  @param keyWord     查询关键字，（String 可为空，该字段为保留字段，搜索功能暂不实现）
 *  @param filter      过滤器列表，（JSON对象数组， 可为空）
 */
- (void)YJHealthyVodProgramListWithChannelId:(NSString *)channelId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyword:(NSString *)keyWord withFilter:(NSMutableArray *)filterArr;

/**
 *  2.6	获取节目视频播放地址
 *
 *  @param programId    节目ID，（String 不可为空）
 *  @param mobile       手机号
 *  @param instanceCode  医院实例编码
 */
- (void)YJHealthyVodMediaPlayListWithProgramId:(NSString *)programId withMobile:(NSString *)mobile withInstanceCode:(NSString *)instanceCode;

/**
 *  2.7	推荐节目
 *
 *  @param programId   节目ID，（String 不可为空）
 */
- (void)YJHealthyVodRecommendProgramWithProgramId:(NSString *)programId withMaxCount:(NSString *)maxCount;

/**
 *  2.8	获取收藏夹中节目
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param pageNo       页码（String不可为空）
 *  @param pageSize     分页大小（String不可为空）
 */
- (void)YJHealthyVodListFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize;


/**
 *  2.9	节目加入收藏夹
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param programId    节目ID（String不可为空）
 */
- (void)YJHealthyVodAddFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramId:(NSString *)programId;


/**
 *  2.10	删除收藏夹中的节目
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param programIds   节目ID列表（String不可为空，多个ID采用逗号分割）
 */
- (void)YJHealthyVodDelFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramIds:(NSString *)programIds;


/**
 *  2.11	清空收藏夹
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 */
- (void)YJHealthyVodCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile;


/**
 *  2.14    获取一级频道
 *
 *  @param instanceCode 医院实例编码（String 可为空）
 */
- (void)YJHealthyVodGetTopChannelWithInstanceCode:(NSString *)instanceCode;


/**
 *  2.15    获取精选频道列表
 *
 */
- (void)YJHealthyGetSelectedChannel;


/**
 *  2.16    获取热门频道与节目列表
 *
 */
- (void)YJHealthyVodGetHotChannel;

#pragma mark------------jxtvBase
/**
 *  1.1	问题列表接口
 */
- (void)JXBaseProblemProblemList;


/**
 *  1.1	根据ID获取问题解答
 */
- (void)JXBaseProblemProblemById;


/**
 *  1.1	查询江西省下的所有市区
 */
- (void)JXBaseBusinessCityList;


/**
 *  1.1	查询市区所有的营业厅
 */
- (void)JXBaseBusinessBusinessByCityId;


/**
 *  1.1	报装接口
 *
 *  @param name        姓名 必填
 *  @param tel         电话 必填
 *  @param address     地址 必填
 *  @param description 描述 非必填
 */
- (void)JXBaseBusinessInstallInfoWithName:(NSString *)name withTel:(NSString *)tel withAddress:(NSString *)address withDescription:(NSString *)description;

#pragma mark - ------PUB PORTAL pub基础接口
/**
 *  2.1	导航频道列表
 *
 *  @param category   大分类（String 必传）
 *
 */
- (void)PubCommonNavigationChannelListWithCategory:(NSString *)category;

/**
 *  2.2	首页推荐列表频道
 *
 *  @param category   大分类（String 必传）
 *
 */
- (void)PubCommonHomePageChannelListWithCategory:(NSString *)category;

/**
 *  2.3	节目列表
 *
 *  @param channelId     频道Id（String 必传）
 *  @param currentPage   当前页码（从1开始）（Integer 必传）
 *  @param pageSize      每页显示条数（Integer 必传）
 *
 */
- (void)PubCommonProgramInfoListWithChannelId:(NSString *)channelId withCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize;

/**
 *  2.4	节目播放链接
 *
 *  @param programId   节目Id（String 必传）
 *
 */
- (void)PubCommonProgramInfoPlayUrltWithProgramId:(NSString *)programId;

/**
 *  2.5	节目收藏
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 */
- (void)PubCommonInsertFavoritesListWithCategory:(NSString *)category withProgramId:(NSString *)programId;

/**
 *  2.6	节目点赞
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 */
- (void)PubCommonInsertPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId;

/**
 *  2.7	查询是否收藏
 *
 *  @param programId   节目Id（String 必传）
 *
 */
- (void)PubCommonIsOrNotFavoritesWithProgramId:(NSString *)programId;

/**
 *  2.8	查询是否点赞
 *
 *  @param programId   节目Id（String 必传）
 *
 */
- (void)PubCommonIsOrNotPraiseWithProgramId:(NSString *)programId;

/**
 *  2.9	收藏列表
 *
 *  @param category   大分类（String 必传）
 *
 */
- (void)PubCommonFavoritesInfoListWithCategory:(NSString *)category;

/**
 *  2.10 点赞列表
 *
 *  @param category   大分类（String 必传）
 *
 */
- (void)PubCommonPraiseInfoListWithCategory:(NSString *)category;

/**
 *  2.11 取消节目点赞
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 */
- (void)PubCommonCancelPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId;

/**
 *  2.12 取消节目收藏
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 */
- (void)PubCommonCancelFavoritesWithCategory:(NSString *)category withProgramId:(NSString *)programId;

/**
 *  2.14 获取点播节目周排行榜
 *
 *  @param weekly    一年中的第几周（String 必传）
 *
 */
- (void)PubCommonVodRankByWeekWithWeekly:(NSString *)weekly;

/**
 *  2.15 获取关键词搜索结果
 *
 *  @param pageNo   第几页（String 必传）
 *  @param pageSize    每页条数（String 必传）
 *  @param key   搜索关键字（String 必传）
 *
 */
- (void)PubCommonGetResultByKeywordWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKey:(NSString *)key;


#pragma mark - ------COMMON PORTAL app基础接口
/**
 *  2.1	系统时间
 *
 */
- (void)CNCommonSystemTime;

/**
 *  2.2	终端软件下载
 *	@param packageName   包名（String 可为空）
 *	@param versionCode   版本编码(Integer可为空)
 *	@param versionName   版本信息(String 可为空)
 *
 */
- (void)CNCommonSoftwareDownWithPackageName:(NSString *)packageName withVersionCode:(NSInteger)versionCode withVersionName:(NSString *)versionName;

/**
 *  2.3	轮播图列表
 *
 */
- (void)CNCommonSlideshowList;

/**
 *  2.4	帮助反馈
 *
 *  @param suggestionType   意见类型（String 不可为空）
 *  @param suggestion       意见内容 (String 可为空)
 *  @param contact          联系方式（String 可为空）
 */
- (void)CNCommonSuggestWithSuggestionType:(NSString *)suggestionType withSuggestion:(NSString *)suggestion withContact:(NSString *)contact;

/**
 *  2.5	广告页列表
 *
 */
- (void)CNCommonAdList;

/**
 *  2.6	产品名称
 *
 */
- (void)CNCommonAppName;

/**
 *  2.7	产品信息
 *
 */
- (void)CNCommonAppInfo;

/**
 *  2.8	产品配置
 *
 */
- (void)CNCommonAppConfig;

/**
 *  2.9	首页模块列表
 *
 */
- (void)CNCommonModuleConfig;

/**
 *  3.1	实体列表
 *	@param currentPage   当前页码(从1开始)
 *	@param pageSize      每页显示条数
 *
 */
- (void)CNInstanceInstanceListWithCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize;

/**
 *  3.2	实体信息
 *
 */
- (void)CNInstanceInstanceInfo;

/**
 *  3.3	实体定位
 *
 *  @param longitude   经度
 *  @param latitude    纬度
 *
 */
- (void)CNInstanceInstanceLocationWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude;

/**
 *  4.1	运营商信息
 *	@param operatorCode  运营商编码
 *
 */
- (void)CNOperatorOperatorInfo;

/**
 *  5.1	短信模版
 *	@param type          短信类型
 *
 */
- (void)CNInnerSmsTempleteWithType:(NSUInteger)type;

/**
 *  5.2	产品列表
 *
 */
- (void)CNInnerAppList;

/**
 *  5.3 message信息
 *
 *  @param projectCode        项目编码
 *  @param errorCode          错误码
 *
 */
- (void)CNInnerMessageInfoWithProjectCode:(NSString *)projectCode withErrorCode:(NSString *)errorCode;

/**
 *  5.5 首页底部图
 *
 */
- (void)CNCommonBottomImage;

/**
 获取分组列表
 
 @param busSystem 业务系统编号
 */
-(void)getMobileMessageGetGroupsWithBusSystem:(NSString*)busSystem;

/**
 1.1	用户绑定分组
 
 @param userID 用户编号(必填)
 @param mobile 手机号码(必填)
 @param channelID 设备标识号(必填)
 @param groupID 分组编号(必填)
 @param deviceType 设备类型(必填) 0：iphone 1：android
 @param busSystem 业务系统编号(必填)  (医聚：medicine)
 */
-(void)getMobileMessageBindGoupWithChannelID:(NSString*)channelID withGroupID:(NSString*)groupID withDeviceType:(NSString*)deviceType withBusSystem:(NSString*)busSystem;


/**
 推送分组消息
 
 @param msgTitle 消息标题(必填)
 @param msgBody 消息内容 (必填)
 @param groupIds 分组编号(必填) 多组时用逗号分隔
 @param pushTime 推送时间(非必填),空值时默认为及时推送，填入日期格式：yyyy-MM-dd HH:mm:ss
 @param msgType 通知类型 0：透传  1：通知(默认)
 @param extra 其他业务参数
 */
-(void)getMobileMessageToGroupsWithMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withGroupIds:(NSString*)groupIds withPushTime:(NSString*)pushTime withMsgType:(NSString*)msgType andExtra:(NSString*)extra;

/**
 用户解除绑定
 @param channelId channelId = 设备标识号(必填)
 @param groupId groupId =分组编号(必填)
 */
-(void)getMobileMessageUNBindGoupWithChannelId:(NSString*)channelId withGroupId:(NSString*)groupId;

/**
 推送单台设备
 
 @param userId 用户编号(必填)
 @param mobile 手机号码(必填)
 @param msgTitle 消息标题(必填)
 @param msgBody 消息内容 (必填)
 @param deviceType 设备类型(必填) 0：iphone 1：android
 @param channelId 设备标识号(必填)
 @param msgType 通知类型 0：透传  1：通知(默认)
 @param busSystem 业务系统编号    医聚：medicine
 @param extra 其他业务参数
 */
-(void)getMobileMessageToSingleDeviceWithUserId:(NSString*)userId withMobile:(NSString*)mobile withMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withDeviceType:(NSString*)deviceType withChannelId:(NSString*)channelId withMsgType:(NSString*)msgType withBusSystem:(NSString*)busSystem withExtra:(NSString*)extra;


/**
 1.1	绑定信息更新
 
 @param channelId 设备标识号(必填)
 @param mobile 手机号码(必填)
 */
-(void)getMobileMessageBindingUpdateWithChannelId:(NSString*)channelId withMobile:(NSString*)mobile;


#pragma mark - Tool Function

+(NSString *)getFormOrAfterDateWithDays:(NSInteger )days;
#pragma 监测网络的可链接性
+ (BOOL) netWorkReachabilityWithURLString:(NSString *) strUrl;

#pragma POST请求
+ (void) NetRequestPOSTWithRequestURL: (NSString *) requestURLString
                        WithParameter: (NSDictionary *) parameter
                 WithReturnValeuBlock: (ReturnValueBlock) block
                   WithErrorCodeBlock: (ErrorCodeBlock) errorBlock
                     WithFailureBlock: (FailureBlock) failureBlock;

#pragma GET请求
+ (void) NetRequestGETWithRequestURL: (NSString *) requestURLString
                       WithParameter: (NSDictionary *) parameter
                WithReturnValeuBlock: (ReturnValueBlock) block
                  WithErrorCodeBlock: (ErrorCodeBlock) errorBlock
                    WithFailureBlock: (FailureBlock) failureBlock;

@end
