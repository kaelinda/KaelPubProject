//
//  HTUNPortalInterface.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


typedef void(^HTUNPortalRequest_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTUNPortalInterface : NSObject

@property (nonatomic, strong) NSString *requestType;


/**
 * 2.1 门户系统版本
 * @param returnBlock 返回的数据
 */
- (void)UNPortalProjectVersion:(HTUNPortalRequest_Return)returnBlock;
/**
 * 2.2 门户系统列表
 * @param regionKey   regionKey (string  必传)关键信息
 * @param returnBlock returnBlock 返回数据
 */
- (void)UNPortalProjectListWithRegionKey:(NSString*)regionKey andReturn:(HTUNPortalRequest_Return)returnBlock;
/**
 * 2.3 区域配置版本
 * @param returnBlock 返回数据
 */
- (void)UNPortalRegionVersion:(HTUNPortalRequest_Return)returnBlock;
/**
 * 2.4 区域配置列表
 * @param returnBlock 返回数据
 */
- (void)UNPortalRegionList:(HTUNPortalRequest_Return)returnBlock;
/**
 * 2.5 区域配置列表
 * @param type        type (integer  必传后修改为string类型)重新加载的配置标识 1:机顶盒配置 2：智能终端配置
 * @param returnBlock returnBlock 返回数据
 */
- (void)UNPortalResetConfigWithType:(NSString *)type  andReturn:(HTUNPortalRequest_Return)returnBlock;

@end
