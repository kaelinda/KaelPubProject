//
//  HTUGCInterface.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/16.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTUGCInterface.h"

@interface HTUGCInterface ()
{
    NSString *_UGCURL;
    NSString *_regionCode;//区域编号
    NSString *_appCode;//医院编号
    NSString *_token;
}

@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation HTUGCInterface

- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
//        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
//        
//        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        
        [_manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        //        [_manager.requestSerializer setValue:@"jx-0-0" forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        
        //--------------------
        
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
//         [_manager.requestSerializer setValue:_token forHTTPHeaderField:@"test"];
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        _UGCURL = [userDefaults objectForKey:@"ugc"];
        
//        _UGCURL = @"http://192.168.3.77:8080/ugc/";
        
    }
    
    return self;
}


/**
 *  2.1	判断是否合法主播
 *
 */
- (void)UGCCommonIsValidUserWithReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:KMobileNum,@"phone", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_ISVALIDUSER andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}

/**
 *  2.2 身份认证
 *  @param idNumber   身份证号（String 必传）
 *  @param name   医生姓名（String 必传）
 *  @param mobile   电话号码编码（String 必传）
 *  @param hospital   医院（String 必传）
 *  @param department    科室（String 必传）
 *  @param academicTitle   职称（String 必传）
 *  @param physicianType   医生认证类型（String 必传）
 *  @param physicianCode   执业编码（String 非必传）
 *  @param officialCard    军官证图片（file 非必传）
 *  @param chestCard   胸牌图片（file 非必传）
 *
 */
- (void)UGCCommonIdentificationWithIdNumber:(NSString *)idNumber andName:(NSString *)name andMobile:(NSString *)mobile andHospital:(NSString *)hospital andDepartment:(NSString *)department andAcademicTitle:(NSString *)academicTitle andPhysicianType:(NSString *)physicianType andPhysicianCode:(NSString *)physicianCode andOfficialCard:(UIImage *)officialCard andChestCard:(UIImage *)chestCard andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:idNumber,@"idNumber",name,@"name",mobile,@"mobile",hospital,@"hospital",department,@"department",academicTitle,@"academicTitle",physicianType,@"physicianType", nil];

    

    if ([physicianType isEqualToString:@"0"]) {
        if (NotEmptyStringAndNilAndNull(physicianCode)) {
            [paramDic setObject:physicianCode forKey:@"physicianCode"];
        }
    }
    
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UGCURL,UGC_COMMON_IDENTIFICATION];
    
    NSLog(@"医生认证上传参数------%@",paramDic);
    
    [_manager POST:URLString parameters:paramDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if ([physicianType isEqualToString:@"1"]) {
            
            if (chestCard) {
                
                NSData *chestCardData = UIImageJPEGRepresentation(chestCard, 0.1f);
                
                if (chestCardData) {
                    
                    NSLog(@"我上传了chestCard照片-----%lu",(unsigned long)chestCardData.length);
                    
                    [formData appendPartWithFileData:chestCardData name:@"chestCard" fileName:@"file.png" mimeType:@"image/png"];
                }
            }
            
            if (officialCard) {
                NSData *officialCardData = UIImageJPEGRepresentation(officialCard, 0.1f);
                
                if (officialCardData) {
                    
                    NSLog(@"我上传了officialCard照片-----%lu",(unsigned long)officialCardData.length);
                    
                    [formData appendPartWithFileData:officialCardData name:@"officialCard" fileName:@"file.png" mimeType:@"image/png"];
                }
            }
            
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
        NSLog(@"上传图片进度--------%@",uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        returnBlock(0,responseObject,UGC_COMMON_IDENTIFICATION);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,UGC_COMMON_IDENTIFICATION);
    }];

    
}

/**
 *  2.3 开始播放
 *
 */
- (void)UGCCommonStartLiveWithName:(NSString *)name andScreenFlg:(NSString *)screenFlg andChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:name,@"name",screenFlg,@"screenFlg",channelId,@"channelId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_STARTLIVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];

}

/**
 *  2.4 停止播放
 *
 */
- (void)UGCCommonStopLiveWithSessionId:(NSString *)sessionId andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:sessionId,@"sessionId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_STOPLIVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}


/**
 *  2.5 点播节目上传
 *
 */
- (void)UGCCommonInsertProgramWithVideoId:(NSString *)videoId andName:(NSString *)name andThumbnaill:(NSString *)thumbnaill andCover:(NSString *)cover andPictureFile:(UIImage *)pictureFile andSize:(NSString *)size andIsEncrypt:(NSString *)isEncrypt andCategoryId:(NSString *)categoryId andCategory:(NSString *)category andStatus:(NSString *)status andRemarks:(NSString *)remarks andUrl:(NSString *)url andTags:(NSString *)tags andDuration:(NSString *)duration andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:KMobileNum,@"phone",videoId,@"videoId",name,@"name", nil];
    
    size = [NSString stringWithFormat:@"%@",size];
    
    categoryId = [NSString stringWithFormat:@"%@",categoryId];
    
    status = [NSString stringWithFormat:@"%@",status];

    if (NotEmptyStringAndNilAndNull(thumbnaill)) {
        [paramDic setObject:thumbnaill forKey:@"thumbnaill"];
    }
    
    if (NotEmptyStringAndNilAndNull(size)) {
        [paramDic setObject:size forKey:@"size"];
    }
    
    if (NotEmptyStringAndNilAndNull(isEncrypt)) {
        [paramDic setObject:isEncrypt forKey:@"isEncrypt"];
    }
    
    if (NotEmptyStringAndNilAndNull(categoryId)) {
        [paramDic setObject:categoryId forKey:@"categoryId"];
    }
    
    if (NotEmptyStringAndNilAndNull(category)) {
        [paramDic setObject:category forKey:@"category"];
    }
    
    if (NotEmptyStringAndNilAndNull(url)) {
        [paramDic setObject:url forKey:@"url"];
    }
    
    if (NotEmptyStringAndNilAndNull(status)) {
        [paramDic setObject:status forKey:@"status"];
    }
    
    if (NotEmptyStringAndNilAndNull(remarks)) {
        [paramDic setObject:remarks forKey:@"remarks"];
    }
    
    if (NotEmptyStringAndNilAndNull(tags)) {
        [paramDic setObject:tags forKey:@"tags"];
    }
    
    if (NotEmptyStringAndNilAndNull(duration)) {
        [paramDic setObject:duration forKey:@"duration"];
    }
    
    if (NotEmptyStringAndNilAndNull(cover)) {
        [paramDic setObject:cover forKey:@"cover"];
    }


    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UGCURL,UGC_COMMON_INSERTPROGRAM];
    
    NSLog(@"点播节目上传------%@",paramDic);
    
    [_manager POST:URLString parameters:paramDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (pictureFile) {
            NSData *imgData = UIImageJPEGRepresentation(pictureFile, 0.01f);
            
            if (imgData) {
                
                [formData appendPartWithFileData:imgData name:@"pictureFile" fileName:@"file.png" mimeType:@"image/png"];
            }
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
        NSLog(@"上传图片进度--------%@",uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        returnBlock(0,responseObject,UGC_COMMON_INSERTPROGRAM);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@",error);
        
        returnBlock([error code],nil,UGC_COMMON_INSERTPROGRAM);
    }];
    
}


/**
 *  2.6 点播节目修改
 *
 */
- (void)UGCCommonUpdateProgramWithProgramId:(NSString *)programId andName:(NSString *)name andCover:(UIImage *)cover andCategoryId:(NSString *)categoryId andTags:(NSString *)tags andRemarks:(NSString *)remarks andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId", nil];
    
    if (NotEmptyStringAndNilAndNull(KMobileNum)) {
        [paramDic setObject:KMobileNum forKey:@"phone"];
    }
    
    if (NotEmptyStringAndNilAndNull(name)) {
        [paramDic setObject:name forKey:@"name"];
    }
        
    if (NotEmptyStringAndNilAndNull(categoryId)) {
        [paramDic setObject:categoryId forKey:@"categoryId"];
    }
    
//    if (NotEmptyStringAndNilAndNull(tags)) {
    [paramDic setObject:tags forKey:@"tags"];
//    }
    
//    if (NotEmptyStringAndNilAndNull(remarks)) {
    [paramDic setObject:remarks forKey:@"remarks"];
//    }
    
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UGCURL,UGC_COMMON_UPDATEPROGRAM];
    
    NSLog(@"检测参数%@",paramDic);
    
    [_manager POST:URLString parameters:paramDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (cover) {
            NSData *imgData = UIImageJPEGRepresentation(cover, 0.5);
            
            if (imgData) {
                
                [formData appendPartWithFileData:imgData name:@"cover" fileName:@"file.png" mimeType:@"image/png"];
            }
        } 

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
        NSLog(@"上传图片进度--------%@",uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        returnBlock(0,responseObject,UGC_COMMON_UPDATEPROGRAM);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,UGC_COMMON_UPDATEPROGRAM);
    }];

}


/**
 *  2.7	用户获取自己的点播列表
 *
 */
- (void)UGCCommonGetProgramListByUserInfoWithReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:KMobileNum,@"phone", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETPROGRAMLISTBYUSERINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}


/**
 *  2.8	获取直播列表
 *
 */
- (void)UGCCommonGetChannelListByUserInfoWithReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETCHANNELLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}


/**
 *  2.9	获取点播列表
 *
 */
- (void)UGCCommonGetProgramListWithReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETPROGRAMLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.10	获取标签列表
 *
 */
- (void)UGCCommonGetTagListWithReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETTAGLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.11 删除视频
 *
 */
- (void)UGCCommonDeleteProgramWithProgramId:(NSString *)programId andIsCleanedOut:(NSString *)isCleanedOut andReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId", nil];
    
    if (NotEmptyStringAndNilAndNull(isCleanedOut)) {
        [paramDic setObject:isCleanedOut forKey:@"isCleanedOut"];
    }
    
    [self postRequestWith:paramDic andType:UGC_COMMON_DELETEPROGRAM andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.12 获取分类列表
 *
 */
- (void)UGCCommonGetCategoryListWithUserId:(NSString *)userId andReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETCATEGORYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.13 直播播放
 *
 */
- (void)UGCCommonGetLiveUrlWithChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:channelId,@"channelId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETLIVEURL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.14 点播播放
 *
 */
- (void)UGCCommonGetProgramUrlWithProgramId:(NSString *)programId andReturn:(HTUGCInterface_Block)returnBlock{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETPROGRAMURL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.15 当虹云回调接口
 *
 */
- (void)UGCCommonUgcCallBackWithVideoId:(NSString *)videoId andStatus:(NSString *)status andPlayCode:(NSString *)playCode andReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:videoId,@"videoId",status,@"status",playCode,@"playCode", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_UGCCALLBACK andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.16	获取token接口
 *
 */
- (void)UGCCommonGetTokenWithReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETTOKEN andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.17	获取医生职称接口
 *
 */
- (void)UGCCommonGetAcademicTitleWithReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETACADEMICTITLE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.18	获取点播播放次数接口
 *
 */
- (void)UGCCommonGetProgramPlayCountWithProgramId:(NSString *)programId andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programlId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETPROGRAMPLAYCOUNT andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.19	获取直播在线人数接口
 *
 */
- (void)UGCCommonGetChannelCountWithChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:channelId,@"channelId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETCHANNELCOUNT andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
}


/**
 *  2.20	获取栏目信息接口
 *
 */
- (void)UGCCommonGetColumnInfoWithReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETCOLUMNINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}


/**
 *  2.21	获取栏目内容信息接口
 *
 */
- (void)UGCCommonGetColumnInfoContentWithColumnInId:(NSString *)columnInId andReturn:(HTUGCInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:columnInId,@"columnInId", nil];
    
    [self postRequestWith:paramDic andType:UGC_COMMON_GETCOLUMNINFOCONTENT andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
        
    }];
    
}





-(void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andReturn:(HTUGCInterface_Block)returnBlock{
    
    NSLog(@"UGC_request header :%@ \n UGC_contentType:%@",_manager.requestSerializer.HTTPRequestHeaders,_manager.responseSerializer.acceptableContentTypes);
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UGCURL,type];
    
    NSLog(@"ugc基础接口:%@  \n上传数据==%@ \nType = %@",URLString,paramDic,type);
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress){
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        NSLog(@"pub_result responseObject:%@ \n type:%@",responseObject,type);
        
        if ([type isEqualToString:UGC_COMMON_GETTOKEN]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"accessToken"] forKey:@"UGCAccessToken"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        
        }
        
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
        NSLog(@"%@",error);
        
    }];
    
}

@end
