
//  HTUGCUpLoadInterface.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/21.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTUGCUpLoadInterface.h"

static HTUGCUpLoadInterface *shared = nil;

@interface HTUGCUpLoadInterface ()
{
    NSString *_ugcUpLoadUrl;
    
    NSString *_token;
    
    BOOL first;

}

@property (nonatomic,strong)AFHTTPSessionManager *manager;
    
@property (nonatomic,assign)int64_t bytesSent;
    
@property (nonatomic,strong)NSDate *taskData;

@end


@implementation HTUGCUpLoadInterface

+ (id)shareInstance
{
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        shared = [[[self class] alloc] init];
        
    });
    
    return shared;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _manager = [AFHTTPSessionManager manager];

        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"ication/json",@"text/html", nil];
        
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
      
        
        first = YES;
    
    }
    return self;
}

/**
 *  上传接口
 *
 *
 */
- (void)UGCUpLoadVideoWithVideoUrl:(NSURL *)videoUrl andVideoDic:(NSMutableDictionary *)videoDic andReturn:(HTUGCUpLoadInterface_Block)returnBlock andSpeedReturn:(HTUGCUpLoadInterfaceSpeed_Block)returnSpeedBlock
{
    
    WS(wself);
    
    NSMutableString *addstring;
        
    addstring=[NSMutableString stringWithFormat:@"http://upload.hongshiyun.net/multipart/upload"];
    
    NSString *videoName = [NSString stringWithFormat:@"%@.mp4",[videoDic objectForKey:@"name"]];
    
    NSString *UGCAccessToken =  [[NSUserDefaults standardUserDefaults] objectForKey:@"UGCAccessToken"];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:UGCAccessToken,@"token",@"0",@"isEncrypt",@"2",@"categoryId", nil];

    NSLog(@"视频上传 header：%@ \n参数：%@",_manager.requestSerializer.HTTPRequestHeaders,dic);


    _task = [_manager POST:addstring parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        [formData appendPartWithFileURL:videoUrl
                                   name:@"file"
                               fileName:videoName
                               mimeType:@"video/mp4" error:nil];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {

                
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功------%@",responseObject);
        
        _task = nil;
        
        first = YES;
        
        returnBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"上传失败------%@",error);
        
        _task = nil;
        
        first = YES;
        
        returnBlock([error code],nil);
        
    }];
    
    __block BOOL weakfirst = first;
    
    [_manager setTaskDidSendBodyDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionTask * _Nonnull task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        if (weakfirst) {
            
            wself.taskData = [NSDate date];
            
            first = NO;
        }
        
        //获取当前时间
        NSDate *currentDate = [NSDate date];
        
        //计算一秒中的速度
        wself.bytesSent += bytesSent;
        
//        NSLog(@"task %@ 我幸福要 is %lld 当前时间：%@；上次时间：%@ ", task, bytesSent,currentDate,self.taskData);
        
        //当前时间和上一秒时间做对比，大于等于一秒就去计算
        if ([currentDate timeIntervalSinceDate:self.taskData] >= 1) {
            //时间差
            double time = [currentDate timeIntervalSinceDate:wself.taskData];
            
            //计算速度
            long long speed = self.bytesSent/time;
            
            NSString *upLoadSpeed = [[NSString alloc]init];
            
            //把速度转成KB或M
            upLoadSpeed = [self formatByteCount:speed];
            
            //维护变量，将计算过的清零
            wself.bytesSent = 0.0;
            
            //维护变量，记录这次计算的时间
            
            wself.taskData = currentDate;
            
            CGFloat upLoadingProgress = (CGFloat)totalBytesSent/totalBytesExpectedToSend;
            
            NSLog(@"我幸福要的速度 is %@ ", upLoadSpeed);
            
            returnSpeedBlock(upLoadSpeed,upLoadingProgress);
            
        }
        
    }];
    
}
    
- (NSString*)formatByteCount:(long long)size
{
    
    return [NSByteCountFormatter stringFromByteCount:size countStyle:NSByteCountFormatterCountStyleFile];
}
    
/**
 *  取消上传
 *
 *
 */
- (void)cancelOperations
{
    
    [_task cancel];
    
    _task = nil;
    
    NSLog(@"上传取消");
    
}
@end
