//
//  HTUGCInterface.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/16.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTUGCInterface_Block)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTUGCInterface : NSObject

/**
 *  2.1	判断是否合法主播
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonIsValidUserWithReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.2 身份认证
 *  @param idNumber   身份证号（String 必传）
 *  @param name   医生姓名（String 必传）
 *  @param mobile   电话号码编码（String 必传）
 *  @param hospital   医院（String 必传）
 *  @param department    科室（String 必传）
 *  @param academicTitle   职称（String 必传）
 *  @param physicianType   医生认证类型（String 必传）
 *  @param physicianCode   执业编码（String 非必传）
 *  @param officialCard    军官证图片（file 非必传）
 *  @param chestCard   胸牌图片（file 非必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonIdentificationWithIdNumber:(NSString *)idNumber andName:(NSString *)name andMobile:(NSString *)mobile andHospital:(NSString *)hospital andDepartment:(NSString *)department andAcademicTitle:(NSString *)academicTitle andPhysicianType:(NSString *)physicianType andPhysicianCode:(NSString *)physicianCode andOfficialCard:(UIImage *)officialCard andChestCard:(UIImage *)chestCard andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.3 开始播放
 *  @param name   会话名称（String 必传）
 *  @param screenFlg   横屏／竖屏（String 必传）
 *  @param channelId   频道ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonStartLiveWithName:(NSString *)name andScreenFlg:(NSString *)screenFlg andChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.4 停止播放
 *  @param sessionId   会话ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonStopLiveWithSessionId:(NSString *)sessionId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.5 点播节目上传
 *  @param videoId   视频ID（String 必传）
 *  @param name   点播节目名称（String 必传）
 *  @param thumbnaill   缩略图（String 非必传）
 *  @param cover   封面图片（当虹云返回）（String 非必传）
 *  @param pictureFile   封面图片(用户上传)（File 非必传）
 *  @param size   视频大小（String 非必传）
 *  @param isEncrypt   视频加密状态（String 非必传）
 *  @param categoryId   视频类别ID（String 非必传）
 *  @param category   视频类别（String 非必传）
 *  @param status   视频转码状态（String 非必传）
 *  @param remarks   节目描述（String 非必传）
 *  @param url   播放地址（String 非必传）
 *  @param tags   标签（String 非必传）
 *  @param duration   播放时长（String 非必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonInsertProgramWithVideoId:(NSString *)videoId andName:(NSString *)name andThumbnaill:(NSString *)thumbnaill andCover:(NSString *)cover andPictureFile:(UIImage *)pictureFile andSize:(NSString *)size andIsEncrypt:(NSString *)isEncrypt andCategoryId:(NSString *)categoryId andCategory:(NSString *)category andStatus:(NSString *)status andRemarks:(NSString *)remarks andUrl:(NSString *)url andTags:(NSString *)tags andDuration:(NSString *)duration andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.6 点播节目修改
 *  @param programId   点播节目ID（String 必传）
 *  @param name   点播节目名称（String 非必传）
 *  @param cover   封面图片（File 非必传）
 *  @param categoryId   分类ID（String 非必传）
 *  @param tags   标签（String 非必传）
 *  @param remarks   描述信息（String 非必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonUpdateProgramWithProgramId:(NSString *)programId andName:(NSString *)name andCover:(UIImage *)cover andCategoryId:(NSString *)categoryId andTags:(NSString *)tags andRemarks:(NSString *)remarks andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.7	用户获取自己的点播列表
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetProgramListByUserInfoWithReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.8	获取直播列表
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetChannelListWithReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.9	获取点播列表
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetProgramListWithReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.10	获取标签列表
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetTagListWithReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.11 删除视频
 *  @param programId   视频ID（String 必传）
 *  @param isCleanedOut   是否清空所有节目（String 非必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonDeleteProgramWithProgramId:(NSString *)programId andIsCleanedOut:(NSString *)isCleanedOut andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.12 获取分类列表
 *  @param userId   用户ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetCategoryListWithUserId:(NSString *)userId andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.13 获取直播播放地址
 *  @param channelId   频道ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetLiveUrlWithChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.14 获取点播播放地址
 *  @param programId   频道ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetProgramUrlWithProgramId:(NSString *)programId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.15 当虹云回调接口
 *  @param videoId   当虹云点播ID（String 必传）
 *  @param status   转码状态（String 必传）
 *  @param playCode   播放码（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonUgcCallBackWithVideoId:(NSString *)videoId andStatus:(NSString *)status andPlayCode:(NSString *)playCode andReturn:(HTUGCInterface_Block)returnBlock;

/**
 *  2.16	获取token接口
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetTokenWithReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.17	获取医生职称接口
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetAcademicTitleWithReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.18	获取点播播放次数接口
 *  @param programId   视频ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetProgramPlayCountWithProgramId:(NSString *)programId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.19	获取直播在线人数接口
 *  @param channelId   视频ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetChannelCountWithChannelId:(NSString *)channelId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.20	获取栏目信息接口
 *
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetColumnInfoWithReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  2.21	获取栏目内容信息接口
 *  @param columnInId   视频ID（String 必传）
 *
 *  @param returnBlock
 */
- (void)UGCCommonGetColumnInfoContentWithColumnInId:(NSString *)columnInId andReturn:(HTUGCInterface_Block)returnBlock;


/**
 *  上传接口
 *
 *
 *  @param returnBlock
 */
+ (void)UGCUpLoadVideoWithVideoUrl:(NSString *)videoUrl andVideoName:(NSString *)videoName;


@end
