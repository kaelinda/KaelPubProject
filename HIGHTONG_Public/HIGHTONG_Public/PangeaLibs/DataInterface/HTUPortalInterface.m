//
//  HTUPortalInterface.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/16.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTUPortalInterface.h"
#import <CommonCrypto/CommonDigest.h>


#define isUserToken     YES


@interface HTUPortalInterface ()
{
    NSString *_UPORTAL;
    NSString *_token;
    NSString *_passWord;
    NSString *_mobilePhone;
    NSString *_authCode;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation HTUPortalInterface

/**
 *  code ----> instanceCode  手机验证码变更了参数名;动态口令登录去掉了这个参数
 *  是否传token加开关
 *  手机号登陆接口如何处理  盐值
 *  ret=-2的处理
 *
 */

- (id)init
{
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"ication/json", nil];
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        //--------------------
        
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的秘钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        
        _UPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"UPORTAL"];
    }
    
    return self;
}


/**
 *  2.2	注册用户登录
 */
- (void)HTMobileLoginWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    _passWord = passWord;
    _mobilePhone = mobilePhone;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [self postRequestWith:paramDic andType:mobileLogin andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

/**
 *  2.3	游客登录
 */
- (void)HTGuestLoginWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:dev_ID,@"deviceID", nil];
    [self postRequestWith:paramDic andType:guestLogin andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

/**
 *  2.4	用户注销
 */
- (void)HTLogoutWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSString *kDeviceID = dev_ID;
    if (kDeviceID.length) {
        [paramDic setValue:kDeviceID forKey:@"deviceID"];
    }
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:UPlogout andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.5	修改用户密码
 */
- (void)HTChangeUserPwdWithOldPassWord:(NSString *)oldPassWord withNewPassWord:(NSString *)newPassWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    oldPassWord = [self md5:oldPassWord];
    newPassWord = [self md5:newPassWord];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:oldPassWord,@"oldPassWord",newPassWord,@"newPassWord", nil];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:changeUserPwd andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.6	手机验证码
 */
- (void)HTAuthCodeWithType:(NSString *)type withInstanceCode:(NSString *)instanceCode withMobilePhone:(NSString *)mobilePhone withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:type,@"type",mobilePhone,@"mobilePhone", nil];

    if (instanceCode.length) {
        [paramDic setValue:instanceCode forKey:@"instanceCode"];
    }
    [self postRequestWith:paramDic andType:verityCode andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.7	手机号注册
 */
- (void)HTMobileRegisterWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    _passWord = [self md5:passWord];
    _mobilePhone = mobilePhone;
    _authCode = authCode;
    
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [self postRequestWith:paramDic andType:mobileRegister andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.8	用户密码重置
 */
- (void)HTResetUserPwdWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    passWord = [self md5:passWord];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    [self postRequestWith:paramDic andType:resetUserPwd andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.9	变更手机号
 */
- (void)HTAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    passWord = [self md5:passWord];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    [self postRequestWith:paramDic andType:alterMobilePhone andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.10 验证手机验证码
 */
- (void)HTCheckAuthCodeWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    [self postRequestWith:paramDic andType:User_Check_Authcode andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.11	获取业务鉴权token
 */
- (void)HTGetTokenWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSString *IDToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"idToken"];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:IDToken,@"identityToken", nil];
    NSString *kDeviceID = dev_ID;
    if (kDeviceID.length) {
        [paramDic setValue:kDeviceID forKey:@"deviceID"];
    }
    [self postRequestWith:paramDic andType:UGetToken andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.12	查询密码是否正确接口
 */
- (void)HTCheckPwdWithPassWord:(NSString *)passWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    passWord = [self md5:passWord];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord", nil];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:checkPwd andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.13	无线认证（验证+注册）
 */
- (void)HTWifiAuthWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withInstanceCode:(NSString *)instanceCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    
    if (instanceCode.length) {
        [paramDic setValue:instanceCode forKey:@"instanceCode"];
    }
    
    [self postRequestWith:paramDic andType:Wifi_Auth andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.14	动态口令登录（验证+注册+登录）
 */
- (void)HTAuthCodeLoginWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    NSString *kDeviceID = dev_ID;
    if (kDeviceID.length) {
        [paramDic setValue:kDeviceID forKey:@"deviceID"];
    }else{
//        [paramDic setValue:@"123456" forKey:@"deviceID"];
    }
    [self postRequestWith:paramDic andType:authCodeLogin andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type){
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.15	动态口令变更手机号
 */
- (void)HTAuthCodeAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone withOldMobileAuthCode:(NSString *)oldMobileAuthCode withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
    if (oldMobileAuthCode.length) {
        [paramDic setValue:oldMobileAuthCode forKey:@"oldMobileAuthCode"];
    }
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:authCodeAlterMobilePhone andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.16	获取临时密钥
 */
- (void)HTGetTemporalKeyWithMobilePhone:(NSString *)mobilePhone withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobilePhone,@"mobilePhone", nil];
    [self postRequestWith:paramDic andType:getTemporalKey andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  2.17	临时密钥登录（验证+注册+登录）
 */
- (void)HTTemporalKeyLoginWithMobilePhone:(NSString *)mobilePhone withKey:(NSString *)key withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobilePhone,@"mobilePhone",key,@"key", nil];
    NSString *kDeviceID = dev_ID;
    if (kDeviceID.length) {
        [paramDic setValue:kDeviceID forKey:@"deviceID"];
    }
    [self postRequestWith:paramDic andType:temporalKeyLogin andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

/**
 *  3.1	用户签到
 */
- (void)HTSignInWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:signIn andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  3.2	查询用户今日是否已经签到
 */
- (void)HTIsSignInWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:User_Is_SignIn andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  3.3	修改昵称
 */
- (void)HTUpdateNickNameWithNickName:(NSString *)nickName withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:nickName,@"nickName", nil];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:updateNickName andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  3.4	头像上传
 */
- (void)HTUploadHeadPhotoWithImgFile:(UIImage *)imgFile withWidth:(NSInteger)width withHeight:(NSInteger)height withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@(width),@"width",@(height),@"height", nil];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
    
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,uploadHeadPhoto];
    
    [_manager POST:URLString parameters:paramDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *imgData = UIImagePNGRepresentation(imgFile);
        
        if (imgData) {
            
            [formData appendPartWithFileData:imgData name:@"imgFile" fileName:@"file.png" mimeType:@"image/png"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        returnBlock(0,responseObject,uploadHeadPhoto);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,uploadHeadPhoto);
    }];
}


/**
 *  3.5	查询用户信息
 */
- (void)HTQueryUserInfoWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    [self postRequestWith:paramDic andType:queryUserInfo andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  3.6	修改用户信息
 */
- (void)HTUpdateUserInfoWithNickName:(NSString *)nickName withBirthday:(NSString *)birthday withSex:(NSString *)sex withProfession:(NSString *)profession withEducation:(NSString *)education withUserName:(NSString *)userName withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    if (isUserToken) {
        [paramDic setValue:_token forKey:@"token"];
    }
    
    if (nickName.length) {
        [paramDic setValue:nickName forKey:@"nickName"];
    }
    if (birthday.length) {
        [paramDic setValue:birthday forKey:@"birthday"];
    }
    if (sex.length) {
        [paramDic setValue:sex forKey:@"sex"];
    }
    if (profession.length) {
        [paramDic setValue:profession forKey:@"profession"];
    }
    if (education.length) {
        [paramDic setValue:education forKey:@"education"];
    }
    if (userName.length) {
        [paramDic setValue:userName forKey:@"userName"];
    }

    [self postRequestWith:paramDic andType:updateUserInfo andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


/**
 *  江西TV用户绑定
 *
 */
- (void)HTBindJxTVWithIdCard:(NSString *)idCard withIdentityNum:(NSString *)identityNum withPhoneNo:(NSString *)phoneNo withRealName:(NSString *)realName withType:(NSInteger)type andReturn:(HTUPortalInterface_Block)returnBlock
{
//    _UPORTAL = @"http://192.168.3.13:9090/UPORTAL";
//    [_manager.requestSerializer setValue:@""  forHTTPHeaderField:@"sign"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:idCard,@"idcard",identityNum,@"identitynum",phoneNo.length>0?phoneNo:@"",@"phoneno",realName,@"realname",[NSNumber numberWithInteger:type],@"type", nil];
    
    [self postRequestWith:paramDic andType:bindJxTV andTimeOut:10 andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];

}


/**
 *  江西TV用户解除绑定
 *
 */
- (void)HTUNBindJxTVAndReturn:(HTUPortalInterface_Block)returnBlock
{
//    _UPORTAL = @"http://192.168.3.13:9090/UPORTAL";
//    [_manager.requestSerializer setValue:@""  forHTTPHeaderField:@"sign"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [self postRequestWith:paramDic andType:unBindJxTV andTimeOut:10 andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];

}


/**
 *  获取江西TV用户绑定信息
 *
 */
- (void)HTUserBindInfoJxTVAndReturn:(HTUPortalInterface_Block)returnBlock
{
//    _UPORTAL = @"http://192.168.3.13:9090/UPORTAL";
//    [_manager.requestSerializer setValue:@""  forHTTPHeaderField:@"sign"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [self postRequestWith:paramDic andType:userBindInfoJxTV andTimeOut:10 andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}


/**
 *  查询江西TV用户是否绑定
 *
 */
- (void)HTUserBindStatusJxTVAndReturn:(HTUPortalInterface_Block)returnBlock
{
//    _UPORTAL = @"http://192.168.3.13:9090/UPORTAL";
//    [_manager.requestSerializer setValue:@""  forHTTPHeaderField:@"sign"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [self postRequestWith:paramDic andType:userBindStatusJxTV andTimeOut:10 andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}


-(void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andTimeOut:(NSInteger)timeOut andReturn:(HTUPortalInterface_Block)returnBlock{
    
    if (isEmptyStringOrNilOrNull(_UPORTAL)) {
        
        returnBlock(-1,nil,type);
        return;
    }
    
    _manager.requestSerializer.timeoutInterval = timeOut;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,type];

    NSLog(@"UPortal header%@上传数据%@ ==%@ Type = %@",_manager.requestSerializer.HTTPRequestHeaders,URLString,paramDic,type);
    
    if ([type isEqualToString:mobileLogin]||[type isEqualToString:mobileRegister]||[type isEqualToString:alterMobilePhone]||[type isEqualToString:resetUserPwd]||[type isEqualToString:changeUserPwd]) {
        
        URLString  = [NSString stringWithFormat:@"%@/%@",_UPORTAL,getChallenge];
        
    }

    NSLog(@"type----%@;_manager.requestSerializer.HTTPRequestHeaders-----%@;_manager.responseSerializer.acceptableContentTypes------------%@",type,_manager.requestSerializer.HTTPRequestHeaders,_manager.responseSerializer.acceptableContentTypes);
    
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (([[responseObject objectForKey:@"ret"] integerValue] == -2) && ([type isEqualToString:signIn]||[type isEqualToString:queryUserInfo]||[type isEqualToString:changeUserPwd]||[type isEqualToString:uploadHeadPhoto]||[type isEqualToString:updateNickName]||[type isEqualToString:updateUserInfo]||[type isEqualToString:alterMobilePhone]||[type isEqualToString:checkPwd]||[type isEqualToString:authCodeAlterMobilePhone]||[type isEqualToString:User_Is_SignIn])) {//用户未登录或者登录超时
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];//获取鉴权token
        }
        
        if ([type isEqualToString:UGetToken]) {
            
            if ([[responseObject objectForKey:@"ret"] integerValue] == 0) {
                
                NSString *token = [responseObject objectForKey:@"token"];
                
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];//存储鉴权token
                
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{//获取身份token
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_ID_TOKEN" object:nil];
            }
        }
        
        if ([type isEqualToString:mobileLogin]){//...ww加手机登录接口
            
            NSString *encryToken = [responseObject objectForKey:@"encryToken"];
            NSLog(@"手机获取挑战字 ： %@",encryToken);
            
            NSString *newPassword = [self encryptingPasswordWithEncryToken:encryToken];
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:dev_ID,@"deviceID",_mobilePhone,@"mobilePhone",newPassword,@"passWord", nil];
            
//            _manager.requestSerializer.timeoutInterval = 10;
            
            NSLog(@"手机号登录 ：%@\n%@",[NSString stringWithFormat:@"%@/%@",_UPORTAL,@"mobileLogin"],paramDic);
            
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,mobileLogin];

            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@" 手机号登陆 %@",responseObject);
                
                returnBlock(0,responseObject,type);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
                returnBlock([error code],nil,type);
            }];
            return;
        }
        
        if ([type isEqualToString:mobileRegister]) {
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_passWord,@"passWord",_mobilePhone,@"mobilePhone",_authCode,@"authCode", nil];
            
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,mobileRegister];
            
            NSLog(@"用户手机注册 ：%@",paramDic);
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"手机号注册 %@",responseObject);
                
                returnBlock(0,responseObject,type);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                returnBlock([error code],nil,type);
            }];
            
            return;
        }
        
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
    }];
}


- (NSString *)encryptingPasswordWithEncryToken:(NSString *)encry{
    
    return [self md5:[NSString stringWithFormat:@"%@%@", [self md5:_passWord], encry]];
}

- (NSString *)md5:(NSString *)str{
    if (str.length<=0) {
        return @"";
    }
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result);  // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
    
}

@end
