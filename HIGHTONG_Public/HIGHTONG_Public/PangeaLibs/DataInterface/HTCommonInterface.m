//
//  HTCommonInterface.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTCommonInterface.h"

@interface HTCommonInterface ()
{
    NSString *_appBaseURL;
    NSString *_regionCode;//区域编号
    NSString *_appCode;//医院编号
    NSString *_token;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation HTCommonInterface

- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
//        [_manager.requestSerializer setValue:@"jx-0-0" forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        
        //--------------------
        
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        _regionCode = KREGION_CODE;
//        _regionCode = @"jx-0-0";

        _appCode = [userDefaults objectForKey:@"InstanceCode"];
        
        _appBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"appbase"];
//        _appBaseURL = [NSString stringWithFormat:@"%@/common",_appBaseURL];
        

    }
    
    return self;
}

//2.1系统时间
- (void)CNCommonSystemTimeWithReturn:(HTCommonRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_COMMON_SYSTEMTIME andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.2终端软件下载
- (void)CNCommonSoftwareDownWithPackageName:(NSString *)packageName withVersionCode:(NSInteger)versionCode withVersionName:(NSString *)versionName andReturn:(HTCommonRequest_Return)returnBlock
{
    
    if (versionName.length == 0) {
        
        versionName = @"";
    }

    [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"terminal_type"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",versionName,@"versionName",packageName,@"packageName",dev_ID,@"deviceID",[NSNumber numberWithInteger:versionCode],@"versionCode", nil];
    
 
    [self postRequestWith:paramDic andType:CN_COMMON_SOFTWAREDOWN andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.3轮播图列表
- (void)CNCommonSlideshowListWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
    
//    if (NotEmptyStringAndNilAndNull(_appCode)) {
//        [paramDic setObject:_appCode forKey:@"instanceCode"];
//    }
    NSLog(@"轮播图上传数据------>>:%@",paramDic);

    [self postRequestWith:paramDic andType:CN_COMMON_SLIDESHOWLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        NSLog(@"轮播图返回数据------>>:%@",result);
        returnBlock(status, result, type);
    }];
    
}

//2.4帮助反馈
- (void)CNCommonSuggestWithSuggestionType:(NSString *)suggestionType withSuggestion:(NSString *)suggestion withContact:(NSString *)contact andReturn:(HTCommonRequest_Return)returnBlock
{
    
    if (suggestion.length == 0) {
        suggestion = @"";
    }
    
    if (contact.length == 0) {
        contact = @"";
    }
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:suggestionType,@"suggestionType",suggestion,@"suggestion",contact,@"contact",_token,@"token", nil];
    
    [self postRequestWith:paramDic andType:CN_COMMON_SUGGEST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.5广告页列表
- (void)CNCommonAdListWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
    
    if (NotEmptyStringAndNilAndNull(_appCode)) {
        [paramDic setObject:_appCode forKey:@"instanceCode"];
    }
    
    [self postRequestWith:paramDic andType:CN_COMMON_ADLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.6产品名称
- (void)CNCommonAppNameWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_COMMON_APPNAME andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.7产品信息
- (void)CNCommonAppInfoWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_COMMON_APPINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//2.8产品配置
- (void)CNCommonAppConfigWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_COMMON_APPCONFIG andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
                
        returnBlock(status, result, type);
    }];
    
}

//2.9首页模块信息
- (void)CNCommonModuleConfigWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:APP_VERSION,@"versionCode", nil];
    
    [self postRequestWith:paramDic andType:CN_COMMON_MODULECONFIG andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//3.1实体列表
- (void)CNInstanceInstanceListWithCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize andReturn:(HTCommonRequest_Return)returnBlock
{
    
    if (currentPage < 1) {
        currentPage = 1;
    }
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:currentPage],@"currentPage",[NSNumber numberWithInteger:pageSize],@"pageSize", nil];
    
    [self postRequestWith:paramDic andType:CN_INSTANCE_INSTANCELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//3.2实体信息
- (void)CNInstanceInstanceInfoWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_appCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:CN_INSTANCE_INSTANCEINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//3.3实体定位
- (void)CNInstanceInstanceLocationWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude andReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:longitude,@"longitude",latitude,@"latitude", nil];
    
    [self postRequestWith:paramDic andType:CN_INSTANCE_INSTANCELOCATION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//4.1运营商信息
- (void)CNOperatorOperatorInfoWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *operatorCode = [userDefaults objectForKey:@"operatorCode"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:operatorCode,@"operatorCode", nil];
    
    [self postRequestWith:paramDic andType:CN_OPERATOR_OPERATORINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//5.1短信模版
- (void)CNInnerSmsTempleteWithType:(NSUInteger)type andReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:KREGION_CODE,@"regionCode",[NSNumber numberWithInteger:type],@"type", nil ];
    
    [self postRequestWith:paramDic andType:CN_INNER_SMSTEMPLETE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//5.2产品列表
- (void)CNInnerAppListWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_INNER_APPLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

//5.3message信息
- (void)CNInnerMessageInfoWithProjectCode:(NSString *)projectCode withErrorCode:(NSString *)errorCode andReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSURL *apiUrl = [[NSUserDefaults standardUserDefaults] objectForKey:projectCode];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:apiUrl,@"apiUrl",projectCode,@"projectCode",errorCode,@"errorCOde", nil];
    
    [self postRequestWith:paramDic andType:CN_INNER_MESSAGEINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}

// 5.5 首页底部图
- (void)CNCommonBottomImageWithReturn:(HTCommonRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    
    [self postRequestWith:paramDic andType:CN_COMMON_BOTTOMIMAGE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status, result, type);
    }];
    
}


-(void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andReturn:(HTCommonRequest_Return)returnBlock{
    
    NSLog(@"request header :%@",_manager.requestSerializer.HTTPRequestHeaders);
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    if (isEmptyStringOrNilOrNull(_appBaseURL)) {
        
        returnBlock(-1,nil,type);
        return;
    }
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_appBaseURL,type];
    
    NSLog(@"app基础接口%@  上传数据==%@ \nType = %@",URLString,paramDic,type);

    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"appbase_result responseObject:%@ \n type:%@",responseObject,type);
        
        if ([type isEqualToString:CN_COMMON_SYSTEMTIME]) {
            
            NSString *sysTime=[responseObject objectForKey:@"sysTime"];
            if (!sysTime) {
                return ;
            }
            
            //获取服务器时间
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *SystemDateTime = [formatter dateFromString:sysTime];
            int TimeDiffence = [SystemDateTime timeIntervalSinceNow];
            NSLog(@"我的差值时间差值%d",TimeDiffence);
            //服务器时间2016-06-13 17:31:30*****2016-06-13 09:31:30 +0000
            NSLog(@"服务器时间%@*****%@",sysTime,[NSDate date]);
            
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",TimeDiffence] forKey:@"SystemTimeDiffence"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            //end

        }
     
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
        
    }];
    
}
@end
