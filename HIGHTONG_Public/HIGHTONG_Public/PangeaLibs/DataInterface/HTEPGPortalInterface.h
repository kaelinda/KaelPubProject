//
//  HTEPGPortalInterface.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/24.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTEPGPortalRequest_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTEPGPortalInterface : NSObject

/**
 *  2.1 EPG浏览 EPG_EVENTLIST         @"eventList"
 *
 *	serviceID = 频道编号（Integer 不可为空）
 @param date = 日期（String 格式：yyyy-MM-dd，如：2011-06-16，空串则默认当天）date参数不传递时根据startDate和endDate参数确定查询范围
 *	@param startDate = 查询开始日期（String 格式：yyyy-MM-dd）startDate不为空时，endDate为必传项
 *	@param endDate = 查询截止日期（String 格式：yyyy-MM-dd）endDate不为空时，startDate为必传项
 *	@param lastUpdateTime = 上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 *
 *
 *  @param timeout 超时时间
 */


/**
 *  @author Kael
 *
 *  @brief 所有频道的列表   (新增)
 *  @param lookbackFlag 回看标识（Integer）1：查询支持回看的频道 其它情况返回全部频道
 *  @param timeout      超时时间
 */
-(void)EPGChannelAllListWithLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;



-(void)EPGSHOWWithServiceID:(NSString *)serviceID andDate:( NSString *)date andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.2 频道列表 EPG_CHANNELLIST       @"channelList"
 *
 *  token = token串（可为空,为空时不鉴权）
 terminal_type = 终端类型(Integer 不可为空 0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC )
 lastUpdateTime = 上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 *  @param type         频道类型（Integer 可为空，为空时返回全部频道）取值分类列表type字段
 *  @param lookbackFlag 回看标识（Integer）1：查询支持回看的频道 其它情况返回全部频道
 *  @param timeout      上次更新时间（String 格式：yyyy-MM-dd HH:mm，如：2012-09-24 17:09，空则代表第一次取数据）
 */
-(void)EPGChannelWithType:(NSString *)type andLookbackFlag:(NSString *)lookbackFlag andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 * @brief 2.3 频道信息查询   EPG_CHANNELQUERY      @"channelQuery"
 *
 * @type TV_CHANGE 接口类型
 *	token = token串（String可为空，为空时不鉴权）
 offset = 偏移（Integer）（预留字段）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD  5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 
 * @param serviceID 频道编号
 * @param timeout 设定请求超时时间
 **/
- (void)TVChangeWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.4 当前后继节目列表  EPG_CURRENTEVENTLIST  @"currentEventList"
 *
 *  @param id  = 频道编号（Integer 可为空）
 *  @param type = 频道类型（Integer为空时返回全部频道）取值分类列表type参数
 *  @param count     条数（Integer 可为空，为空时默认为3）
 *  @param timeout   设定请求超时时间
 */
-(void)EPGPFWithServiceID:(NSString *)serviceID  andType:(NSString*)type andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.5 鉴权直播  EPG_TVPLAY            @"tvPlay"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param serviceID id = 频道编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)TVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.6 增强版鉴权直播  EPG_TVPLAYEX          @"tvPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)TVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.7 鉴权时移 EPG_PAUSETVPLAY       @"pauseTvPlay"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param serviceID id = 频道编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)PauseTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.8 增强版鉴权时移  EPG_PAUSETVPLAYEX     @"pauseTvPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)PauseTVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.9 鉴权回看 EPG_EVENTPLAY         @"eventPlay"
 *
 *	token = token串（String不可为空）
 *  terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param eventID id = 节目编号（Integer 不可为空）
 *  @param timeout   设定请求超时时间
 */
-(void)EventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.10 增强版鉴权回看  EPG_EVENTPLAYEX       @"eventPlayEX"
 *
 *	token = token串（String不可为空）
 terminal_type = 终端类型 (0：iphone 1：android 2：IPAD 3：android PAD 6：OTT机顶盒9：PC 默认为0)
 *  @param startTime   播出时间（String 必填）格式为yyyy-MM-dd HH:mm
 *  @param endTime     结束时间（String 可不填）格式为yyyy-MM-dd HH:mm 不填的时候就只匹配播出时间，如果填写了就要匹配结束时间，结束时间填错也不会返回播放链接;第三方的EPG(与本地不匹配)播出时间和结束时间必须成对出现。
 *  @param tvName   频道名称（String 必填）
 *  @param timeout  设定请求超时时间
 */
-(void)EventPlayEXWithTVName:(NSString *)tvName andStarTime:(NSString *)startTime andEndTime:(NSString *)endTime andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.11 分类列表  EPG_TYPELIST          @"typeLis"
 *
 *	regionID = 区域标识（预留字段）
 terminal_type = 终端类型（0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0）
 *  @param timeout 设定超时时间
 */
-(void)TypeListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.15 EPG 搜索2  EPG_EVENTSEARCH       @"eventSearch"
 *
 *	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param key       关键字（String）
 *  @param type      频道类型（Integer 可为空）不传或者传递空串时不对频道类型限制，传入错误的频道类型返回空集
 *  @param serviceID 频道编号（String 可为空）不传或者传递空串时不对频道类型限制，允许传入多个频道编号，中间以半角逗号分隔，传入错误的频道编号返回空集
 *  @param startDate 查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param endDate   查询截止日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对截止日期做限制
 *  @param timeout   设定超时时间
 */
-(void)EPGSearchEXWithKey:(NSString *)key andType:(NSString *)type andServiceID:(NSString *)serviceID andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.16 热门搜索关键词  EPG_TOPSEARCHWORDS    @"topSearchWords"
 *
 *	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param key     关键字（String 可为空）
 *  @param count   条数（Integer 可为空 默认为10）
 *  @param timeout 设定超时时间
 */
-(void)TopSearchKeyWithKey:(NSString *)key andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.17 增加频道收藏  EPG_ADDTVFAVORATE     @"addTvFavorite"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)AddTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.18 删除频道收藏  EPG_DELTVFAVORATE     @"deleteTvFavorite"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)DelTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.19 频道收藏列表 EPG_TVFAVORATELIST    @"tvFavoriteList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout   设定超时时间
 */
-(void)TVFavorateListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.20 播放记录列表  EPG_TVPLAYLIST        @"tvPlayList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *	@param startDate = 开始日期（String 格式：yyyy-MM-dd）可为空
 * 	@param endDate = 截止日期（String 格式：yyyy-MM-dd）可为空
 *  @param timeout   设定超时时间
 */
-(void)TVPlayListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
-(void)TVPlayListWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTImeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.21 删除播放记录 EPG_DELTVPLAY         @"deleteTvPlay"
 *
 *	token = token串（String不可为空）
 *  @param serviceID =频道编号（Interger不可为空）
 *  @param timeout   设定超时时间
 */
-(void)DelTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.22 回看停止  EPG_EVENTSTOP         @"eventStop"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param eventID    eventID =节目编号（String 不可为空）
 *  @param breakPoint 断点位置（Integer）记录观看到多长时间的位置，单位：秒
 *  @param timeout    设定超时时间
 */
-(void)EventStopWithEventID:(NSString *)eventID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.23 查询回看播放记录  EPG_EVENTPLAYLIST     @"eventPlayList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param eventID 节目编号（String）可为空，为空时全部回看记录
 *  @param count   条数（Integer）默认为10，eventID传值时此参数无效
 *  @param timeout 设定超时时间
 */
-(void)EventPlayListWithEventID:(NSString *)eventID andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  2.24 删除回看播放记录   EPG_DELEVENTPLAY      @"deleteEventPlay"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.25 增加回看收藏  EPG_ADDEVENTFAVORATE  @"addEventFavorite"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *	@param startDate = 开始日期（String 格式：yyyy-MM-dd）可为空
 * 	@param endDate = 截止日期（String 格式：yyyy-MM-dd）可为空
 
 *  @param timeout 设定超时时间
 */
-(void)AddEventFavorateWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.26 删除回看收藏  EPG_DELEVENTFAVORATE  @"deleteEventFavorite"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventFavorateWithEventID:(NSString *)eventID anTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.27 回看收藏列表  EPG_EVENTFAVORATELIST @"eventFavoriteList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */
-(void)EventFavorateListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.28 热门频道列表  EPG_HOTCHANNELLIST    @"hotChannelList"
 *
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */
-(void)HotChannelListWithTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.29 预约节目   EPG_ADDEVENTRESERVE   @"addEventReserve"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)AddEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  2.30 取消预约节目 EPG_DELEVENTRESERVE   @"deleteEventReserve"
 *
 *	token = token串（String不可为空）
 *  @param eventID 节目编号（String 可为空，为空时清空全部回看播放列表）
 *  @param timeout 设定超时时间
 */
-(void)DelEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

/**
 *  2.31 预约节目列表  EPG_EVENTRESERVELIST  @"eventReserveList"
 *
 *	token = token串（String不可为空）
 * 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout 设定超时时间
 */

/**
 *  2.34 广告列表 eventReserveList
 *
 *  @param regionCode = 区域编码（String）不为空时返回对应区域的广告以及未标识区域的广告；为空时只返回未标识区域的广告
 *  @param timeout 请求超时时间
 */
-(void)ADListWith:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;


/**
 *  更新频道收藏（3.5版本新加的接口）
 *
 *	token = token串（String不可为空）
 *  @param seq          seq= 排序号（Integer 不可为空）
 *  @param serviceID 	id = 频道编号（Integer 不可为空）
 *  @param name      	name = 频道名称（string 不可为空）
 *  @param timeout   请求超时时间
 */
-(void)EPGUpdateTvFavoriteWithSeq:(NSString*)seq andServiceID:(NSString *)serviceID andTVName:(NSString *)name andTimeou:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;



- (void)EPGUpdateTvFavoriteWithNewSortArr:(NSArray *)sortArr andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;




/**
 *  序列化的 EPG搜索  epgSearch  3.7.4
 *
 *	token = token串（String不可为空）
 *  @param key          关键字（String不可为空）
 *  @param startDate 	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param endDate      	查询开始日期（String 格式：yyyy-MM-dd）不传或者传递空串时不对开始日期做限制
 *  @param terminal_type      	终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param timeout   请求超时时间
 */
-(void)epgSearchOrderResultWith:(NSString *)key andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

-(void)EventRevserveWithTimeou:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;

//获取首页 热门频道  EPG_HOTCHANNELLIST
//-(void)EPGGetHomepageHotChannelAndReturn:(HTEPGPortalRequest_Return)returnBlock;

//获取直播界面的频道以及当前后继节目   channelList  返回类型
//-(void)EPGGetLivePageChannelInfoAndReturn:(HTEPGPortalRequest_Return)returnBlock;
//-(void)EPGGetLivePageChannelInfoWithType:(NSInteger)type;
/**
 *  获取首页轮播图  type  slideshowList
 *  @param regionCode = 区域编码（String）不为空时返回对应区域的广告以及未标识区域的广告；为空时只返回未标识区域的广告
 */
-(void)getHomePageSlideshowListAndReturn:(HTEPGPortalRequest_Return)returnBlock;

//- (void)HomeADSLiderPagessAndReturn:(HTEPGPortalRequest_Return)returnBlock;//首页轮播图广告


/**
 *  区域列表   regionType
 *
 *  @param regionType 区域类型(Integer)  1：医院;2：社区;3：学校 默认为1
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param outTime 超时时间
 */
-(void)EPGGetRegionListWithTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  查找区域
 *
 *  @param regionType 区域类型(Integer)  1：医院;2：社区;3：学校 默认为1
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param longitude  当前位置的经度（Double,不可为空）
 *  @param latitude   当前位置的纬度 （Double,不可为空）
 *  @param outTime 超时时间
 */
-(void)EPGFindRegionWithTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock;
-(void)EPGFindRegionWithLongitude:(NSString *)longitude andLatitude:(NSString *)latitude andTimeout:(NSInteger)outTime andReturn:(HTEPGPortalRequest_Return)returnBlock;
/**
 *  区域信息 regionInfo
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param regionCode 区域编码
 *  @param timeout    超时时间
 */
-(void)EPGGetRegionInfoWithRegionCode:(NSString *)regionCode andTimeout:(NSInteger)timeout andReturn:(HTEPGPortalRequest_Return)returnBlock;
@end
