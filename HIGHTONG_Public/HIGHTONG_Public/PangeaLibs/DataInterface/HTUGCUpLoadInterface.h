//
//  HTUGCUpLoadInterface.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/21.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTUGCUpLoadInterface_Block)(NSInteger status, NSMutableDictionary *result);

typedef void(^HTUGCUpLoadInterfaceSpeed_Block)(NSString *speed,CGFloat uploadProgress);

@interface HTUGCUpLoadInterface : NSObject

@property (nonatomic,strong)NSURLSessionDataTask *task;

+ (id)shareInstance;

/**
 *  上传接口
 *
 *
 */
- (void)UGCUpLoadVideoWithVideoUrl:(NSURL *)videoUrl andVideoDic:(NSMutableDictionary *)videoDic andReturn:(HTUGCUpLoadInterface_Block)returnBlock andSpeedReturn:(HTUGCUpLoadInterfaceSpeed_Block)returnSpeedBlock;


/**
 *  取消上传
 *
 *
 */
- (void)cancelOperations;

@end
