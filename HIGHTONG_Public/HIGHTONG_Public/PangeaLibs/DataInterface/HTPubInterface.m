//
//  HTPubInterface.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/15.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTPubInterface.h"

@interface HTPubInterface ()
{
    NSString *_pubURL;
    NSString *_regionCode;//区域编号
    NSString *_appCode;//医院编号
    NSString *_token;
}

@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation HTPubInterface

- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        //        [_manager.requestSerializer setValue:@"jx-0-0" forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        
        //--------------------
        
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        _pubURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"pubapi"];
        
//        _pubURL = @"http://192.168.3.62:8080/pub";

    }
    
    return self;
}

//2.1	导航频道列表
- (void)PubCommonNavigationChannelListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_NAVIGATIONCHANNELLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.2	首页推荐列表频道
- (void)PubCommonHomePageChannelListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_HOMEPAGECHANNELLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.3	节目列表
- (void)PubCommonProgramInfoListWithChannelId:(NSString *)channelId withCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize andReturn:(HTPubInterface_Block)returnBlock
{
    
    if (currentPage <= 0) {
        
        currentPage = 1;
        
    }
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:channelId,@"channelId",[NSNumber numberWithInteger:currentPage],@"currentPage",[NSNumber numberWithInteger:pageSize],@"pageSize", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_PROGRAMINFOLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.4	节目播放链接
- (void)PubCommonProgramInfoPlayUrltWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"id", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_PROGRAMINFOPLAYURL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.5	节目收藏
- (void)PubCommonInsertFavoritesListWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_INSERTFAVORITES andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.6	节目点赞
- (void)PubCommonInsertPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_INSERTPRAISE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.7	查询是否收藏
- (void)PubCommonIsOrNotFavoritesWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_ISORNOTFAVORITES andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.8	查询是否点赞
- (void)PubCommonIsOrNotPraiseWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_ISORNOTPRAISE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.9	收藏列表
- (void)PubCommonFavoritesInfoListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:category,@"category",nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_FAVORITESINFOLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.10 点赞列表
- (void)PubCommonPraiseInfoListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:category,@"category",nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_PRAISEINFOLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.11 取消节目点赞
- (void)PubCommonCancelPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_CANCELPRAISE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.12 取消节目收藏
- (void)PubCommonCancelFavoritesWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:programId,@"programId",category,@"category", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_CANCELFAVORITES andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.14 获取点播节目周胖行榜
- (void)PubCommonVodRankByWeekWithWeekly:(NSString *)weekly andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:weekly,@"weekly", nil];
    
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    
    [paramDic setObject:operatorCode forKey:@"operatorCode"];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_VODRANKBYWEEK andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.15 获取关键词搜索结果
- (void)PubCommonGetResultByKeywordWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKey:(NSString *)key andReturn:(HTPubInterface_Block)returnBlock
{
    
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:pageNo,@"pageNo",pageSize,@"pageSize",key,@"key",operatorCode,@"operatorCode", nil];
    
    [self postRequestWith:paramDic andType:PUB_COMMON_GETRESULTBYKEYWORD andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

-(void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andReturn:(HTPubInterface_Block)returnBlock{
    
    NSLog(@"request header :%@",_manager.requestSerializer.HTTPRequestHeaders);
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_pubURL,type];
    
    NSLog(@"pub基础接口%@  上传数据==%@ \nType = %@",URLString,paramDic,type);
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"pub_result responseObject:%@ \n type:%@",responseObject,type);
        
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
        
    }];
    
}

@end
