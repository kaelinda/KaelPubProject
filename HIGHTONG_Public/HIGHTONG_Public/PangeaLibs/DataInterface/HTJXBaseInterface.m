//
//  HTJXBaseInterface.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/8.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTJXBaseInterface.h"

@interface HTJXBaseInterface ()
{
    NSString *_jxtvbase;
}
@property (nonatomic, strong)AFHTTPSessionManager *manager;

@end

@implementation HTJXBaseInterface

- (id)init
{
    self = [super init];
    if (self) {
        
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _jxtvbase = [userDefaults objectForKey:@"tvbiz"];

    }
    return self;
}

//1.1	问题列表接口
- (void)JXBaseProblemProblemListWithReturn:(HTJXBaseInterface_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    [self postRequestWith:paramDic andType:JXTV_PROBLEM_PROBLEMLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//查询江西省下的所有市区
- (void)JXBaseBusinessCityListWithReturn:(HTJXBaseInterface_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [self postRequestWith:paramDic andType:JXTV_BUSINESS_CITYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//根据ID获取问题解答
- (void)JXBaseProblemProblemByIdWithReturn:(HTJXBaseInterface_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [self postRequestWith:paramDic andType:JXTV_PROBLEM_PROBLEMBYID andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//查询市区所有的营业厅
- (void)JXBaseBusinessBusinessByCityIdWithReturn:(HTJXBaseInterface_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [self postRequestWith:paramDic andType:JXTV_BUSINESS_BUSINESSBYCITYID andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//报装接口
- (void)JXBaseBusinessInstallInfoWithName:(NSString *)name withTel:(NSString *)tel withAddress:(NSString *)address withDescription:(NSString *)description withReturn:(HTJXBaseInterface_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:name,@"name",tel,@"tel",address,@"address", nil];
    if (!isEmptyStringOrNilOrNull(description)) {
        [paramDic setObject:description forKey:@"description"];
    }
    
    NSLog(@"报装的参数一%@",paramDic);
    [self postRequestWith:paramDic andType:JXTV_BUSINESS_INSTALLINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



- (void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andReturn:(HTJXBaseInterface_Return)returnBlock
{
    if (isEmptyStringOrNilOrNull(_jxtvbase)) {
        returnBlock(-1,nil,type);
        return;
    }
    
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_jxtvbase,type];
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        returnBlock(0, responseObject, type);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code], nil, type);
    }];
}

@end
