//
//  HTPubInterface.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2016/11/15.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTPubInterface_Block)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTPubInterface : NSObject

/**
 *  2.1	导航频道列表
 *
 *  @param category   大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonNavigationChannelListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.2	首页推荐列表频道
 *
 *  @param category   大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonHomePageChannelListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.3	节目列表
 *
 *  @param channelId     频道Id（String 必传）
 *  @param currentPage   当前页码（从1开始）（Integer 必传）
 *  @param pageSize      每页显示条数（Integer 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonProgramInfoListWithChannelId:(NSString *)channelId withCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.4	节目播放链接
 *
 *  @param programId   节目Id（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonProgramInfoPlayUrltWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.5	节目收藏
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonInsertFavoritesListWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.6	节目点赞
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonInsertPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.7	查询是否收藏
 *
 *  @param programId   节目Id（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonIsOrNotFavoritesWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.8	查询是否点赞
 *
 *  @param programId   节目Id（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonIsOrNotPraiseWithProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.9	收藏列表
 *
 *  @param category   大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonFavoritesInfoListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.10 点赞列表
 *
 *  @param category   大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonPraiseInfoListWithCategory:(NSString *)category andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.11 取消节目点赞
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonCancelPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.12 取消节目收藏
 *
 *  @param programId   节目Id（String 必传）
 *  @param category    大分类（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonCancelFavoritesWithCategory:(NSString *)category withProgramId:(NSString *)programId andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.14 获取点播节目周排行榜
 *
 *  @param weekly    一年中的第几周（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonVodRankByWeekWithWeekly:(NSString *)weekly andReturn:(HTPubInterface_Block)returnBlock;

/**
 *  2.15 获取关键词搜索结果
 *
 *  @param pageNo   第几页（String 必传）
 *  @param pageSize    每页条数（String 必传）
 *  @param key   搜索关键字（String 必传）
 *
 *  @param returnBlock
 */
- (void)PubCommonGetResultByKeywordWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKey:(NSString *)key andReturn:(HTPubInterface_Block)returnBlock;

@end
