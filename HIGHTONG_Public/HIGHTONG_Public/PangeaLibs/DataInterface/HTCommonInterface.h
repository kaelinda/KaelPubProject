//
//  HTCommonInterface.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/5/31.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTCommonRequest_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTCommonInterface : NSObject

/**
 *  2.1	系统时间
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonSystemTimeWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.2	终端软件下载
 *
 *	@param packageName   包名（String 可为空）
 *	@param versionCode   版本编码(Integer可为空)
 *	@param versionName   版本信息(String 可为空)
 *
 *  @param returnBlock   返回数据
 */
- (void)CNCommonSoftwareDownWithPackageName:(NSString *)packageName withVersionCode:(NSInteger)versionCode withVersionName:(NSString *)versionName andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.3	轮播图列表
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonSlideshowListWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.4	帮助反馈
 *
 *  @param suggestionType   意见类型（String 不可为空）
 *  @param suggestion       意见内容 (String 可为空)
 *  @param contact          联系方式（String 可为空）
 *
 *  @param returnBlock      返回数据
 */
- (void)CNCommonSuggestWithSuggestionType:(NSString *)suggestionType withSuggestion:(NSString *)suggestion withContact:(NSString *)contact andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.5	广告页列表
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonAdListWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.6	产品名称
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonAppNameWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.7	产品信息
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonAppInfoWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.8	产品配置
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonAppConfigWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  2.9	首页模块列表
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonModuleConfigWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  3.1	实体列表
 *	@param currentPage   当前页码(从1开始)
 *	@param pageSize      每页显示条数
 *
 *  @param returnBlock   返回数据
 */
- (void)CNInstanceInstanceListWithCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  3.2	实体信息
 *
 *  @param returnBlock 返回数据
 */
- (void)CNInstanceInstanceInfoWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  3.3	实体定位
 *
 *  @param longitude   经度
 *  @param latitude    纬度
 *
 *  @param returnBlock 返回数据
 */
- (void)CNInstanceInstanceLocationWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  4.1	运营商信息
 *	@param operatorCode  运营商编码
 *
 *  @param returnBlock   返回数据
 */
- (void)CNOperatorOperatorInfoWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  5.1	短信模版
 *	@param type          短信类型
 *
 *  @param returnBlock   返回数据
 */
- (void)CNInnerSmsTempleteWithType:(NSUInteger)type andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  5.2	产品列表
 *
 *  @param returnBlock 返回数据
 */
- (void)CNInnerAppListWithReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  5.3 message信息
 *
 *  @param projectCode        项目编码
 *  @param errorCode          错误码
 *
 *  @param returnBlock        返回数据
 */
- (void)CNInnerMessageInfoWithProjectCode:(NSString *)projectCode withErrorCode:(NSString *)errorCode andReturn:(HTCommonRequest_Return)returnBlock;

/**
 *  5.5 首页底部图
 *
 *  @param returnBlock 返回数据
 */
- (void)CNCommonBottomImageWithReturn:(HTCommonRequest_Return)returnBlock;

@end
