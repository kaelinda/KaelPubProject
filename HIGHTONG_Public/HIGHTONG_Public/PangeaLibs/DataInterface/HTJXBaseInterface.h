//
//  HTJXBaseInterface.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/8.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTJXBaseInterface_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTJXBaseInterface : NSObject

/**
 *  1.1	问题列表接口
 *
 *  @param returnBlock
 */
- (void)JXBaseProblemProblemListWithReturn:(HTJXBaseInterface_Return)returnBlock;

/**
 *  1.1	根据ID获取问题解答
 *
 *  @param returnBlock
 */
- (void)JXBaseProblemProblemByIdWithReturn:(HTJXBaseInterface_Return)returnBlock;


/**
 *  1.1	查询江西省下的所有市区
 *
 *  @param returnBlock
 */
- (void)JXBaseBusinessCityListWithReturn:(HTJXBaseInterface_Return)returnBlock;

/**
 *  1.1	查询市区所有的营业厅
 *
 *  @param returnBlock
 */
- (void)JXBaseBusinessBusinessByCityIdWithReturn:(HTJXBaseInterface_Return)returnBlock;

/**
 *  1.1	报装接口
 *
 *  @param name        姓名 必填
 *  @param tel         电话 必填
 *  @param address     地址 必填
 *  @param description 描述 非必填
 *  @param returnBlock 
 */
- (void)JXBaseBusinessInstallInfoWithName:(NSString *)name withTel:(NSString *)tel withAddress:(NSString *)address withDescription:(NSString *)description withReturn:(HTJXBaseInterface_Return)returnBlock;

@end
