//
//  HTVODPortalInterface.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTVODPortalInterface.h"
@interface HTVODPortalInterface ()
{
    NSString *_token;
    NSString *_EPGPORTAL;
    NSString *_VODPORTAL;
    NSString *_UPORTAL;
    NSString *_ADPORTAL;
    NSString *_PMPORTAL;
}

@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong) NSMutableArray *hotChannelArr;
@property (nonatomic,strong) NSMutableArray *allChannelArr;
@end

@implementation HTVODPortalInterface

- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"ication/json", nil];
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
//        _token = @"19beefd610da5bb0c85ead906415fda2";
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }

            _EPGPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
            _VODPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODPORTAL"];
            _UPORTAL   = [[NSUserDefaults standardUserDefaults] objectForKey:@"UPORTAL"];
            
            //            _UPORTAL  = @"http://101.200.150.40:9090/UPORTAL";
            
            NSLog(@"_EPGPORTAL-------------%@",_EPGPORTAL);
            NSLog(@"_VODPORTAL-------------%@",_VODPORTAL);
            NSLog(@"_UPORTAL-------------%@",_UPORTAL);
            
            //            _ADPORTAL  = @"http://192.168.3.60:8080/ADPORTAL";
            _ADPORTAL  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ADPORTAL"];
            if (_ADPORTAL.length==0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
            }
            
            _PMPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"polymedicine"];
            
            
        
        
    }
    return self;
}



#pragma mark ------- VODPORTAL (珠江数码)
/*------------珠江数码相关点播API-------------*/

//------------点播运营组
-(void)VODOperationListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSDictionary *paramDic = [[NSDictionary alloc] init];

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPERATION_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
    
}
//-----------点播专题
-(void)VODTopicListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    
    NSDictionary *paramDic = [[NSDictionary alloc] init];
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
 
    
}

//New...
-(void)VODTopicListWithType:(NSString*)type andScope:(NSString *)scope andOperationCode:(NSString *)operationCode amdCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:type,@"type",scope,@"scope",operationCode,@"operationCode",categoryID,@"categoryID", nil];
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//---点播专题详情
- (void)VODTopicDetailWithTopicCode:(NSString *)topicCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:topicCode,@"topicCode", nil];
    
       [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//-----------点播分类
-(void)VODCategoryListWithParentID:(NSString *)parentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSDictionary *paramDic = [[NSDictionary alloc] init];
    
    if (NotEmptyStringAndNilAndNull(parentID)) {
       paramDic = [NSDictionary dictionaryWithObjectsAndKeys:parentID,@"parentID", nil];
    }
    
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_CATEGORY_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//-----------点播运营组分类
-(void)VODOperationCategoryListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]init];
    if (NotEmptyStringAndNilAndNull(operationCode)) {
        [paramDic setObject:operationCode forKey:@"operationCode"];
    }
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPER_CATEGORY_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//--------点播分类运营组...
- (void)VODCategoryOperationListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]init];
    if (NotEmptyStringAndNilAndNull(categoryID)) {
        [paramDic setObject:categoryID forKey:@"categoryID"];
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_CATEGORY_OPER_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}

//---------1.1	点播分类专题列表
- (void)VODCategoryTopicListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]init];
    if (NotEmptyStringAndNilAndNull(categoryID)) {
        [paramDic setObject:categoryID forKey:@"categoryID"];
        [paramDic setObject:TERMINAL_TYPE forKey:@"terminal_type"];
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_CATEGORY_TOPICLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//-------------点播节目列表
- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
     paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];

    if (isEmptyStringOrNilOrNull(operationCode)) {
        
    [paramDic setObject:@"" forKey:@"operationCode"];
        
     }else
     {
         
    [paramDic setObject:operationCode forKey:@"operationCode"];
  
     }
    
    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];

    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PROGRAM_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//------供首页用的点播分类下的节目使用仅供 真正
- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
    
     paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(operationCode)) {
        
        [paramDic setObject:@"" forKey:@"operationCode"];
        
    }else
    {
        
        [paramDic setObject:operationCode forKey:@"operationCode"];
        
    }
    
    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
        
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }

    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_PROGRAM_LIST];

    NSLog(@"首页特供节目列表接口 %@",paramDic);
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        
        [mDic setObject:index forKey:@"index"];
        
        returnBlock(0,mDic,VOD_PROGRAM_LIST);
 
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],[NSMutableDictionary dictionary],VOD_PROGRAM_LIST);
 
        
    }];
    
    
}



- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andProgramName:(NSString*)ProgramName andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
    
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(operationCode)) {
        
        [paramDic setObject:@"" forKey:@"operationCode"];
        
    }else
    {
        
        [paramDic setObject:operationCode forKey:@"operationCode"];
        
    }
    
    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
        
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_PROGRAM_LIST];

    NSLog(@"首页特供节目列表接口 %@",paramDic);
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        
        [mDic setObject:index forKey:@"index"];
        [mDic setObject:ProgramName forKey:@"topicName"];
        [mDic setObject:rootCategoryID forKey:@"categoryID"];
        returnBlock(0,mDic,VOD_PROGRAM_LIST);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],[NSMutableDictionary dictionary],VOD_PROGRAM_LIST);
        
        
    }];

}



//--------点播运营组节目列表
-(void)VODOperationProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(operationCode)) {
        
        [paramDic setObject:@"" forKey:@"operationCode"];
    }else
    {
        [paramDic setObject:operationCode forKey:@"operationCode"];
    }

    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }

    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
        
    }

    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPER_PROGRAM_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//******首页的推荐专题获取，需要发送两次数据，标记第几次
- (void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    
    if (NotEmptyStringAndNilAndNull(topicCode)) {
        [paramDic setObject:topicCode forKey:@"topicCode"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
        
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
    
    _manager.requestSerializer.timeoutInterval = timeout;

    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_TOPIC_PROGRAM_LIST];

    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    NSLog(@"\n请求推荐专题的发送数据   ：%@   \nURL ：%@\n",paramDic,URLString);
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        NSMutableDictionary * returnDIC = [NSMutableDictionary dictionaryWithDictionary:responseObject];
        [returnDIC setObject:index forKey:@"index"];
        
        returnBlock(0,returnDIC,VOD_TOPIC_PROGRAM_LIST);

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时

        returnBlock([error code],nil,VOD_TOPIC_PROGRAM_LIST);

    }];
    
    
}
//-----点播专题节目列表
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    
    if (NotEmptyStringAndNilAndNull(topicCode)) {
        [paramDic setObject:topicCode forKey:@"topicCode"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
        
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_PROGRAM_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//------------点播搜索
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    
    if (isEmptyStringOrNilOrNull(operationCode)) {
        [paramDic setObject:@"" forKey:@"operationCode"];
    }else
    {
        [paramDic setObject:operationCode forKey:@"operationCode"];
    }
    
    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(categoryID)) {
        
        [paramDic setObject:@"" forKey:@"categoryID"];
    }else
    {
        [paramDic setObject:categoryID forKey:@"categoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
    
    if (isEmptyStringOrNilOrNull(key)) {
        
        [paramDic setObject:@"" forKey:@"key"];
    }else
    {
        [paramDic setObject:key forKey:@"key"];
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_SEARCH andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//NEW  ------------点播搜索
- (void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSString*)startSeq andCount:(NSString*)count andOrderType:(NSString*)orderType andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
     paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    if (isEmptyStringOrNilOrNull(operationCode)) {
        [paramDic setObject:@"" forKey:@"operationCode"];
    }else
    {
        [paramDic setObject:operationCode forKey:@"operationCode"];
    }
    
    if (isEmptyStringOrNilOrNull(rootCategoryID)) {
        [paramDic setObject:@"" forKey:@"rootCategoryID"];
    }else{
        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(categoryID)) {
        
        [paramDic setObject:@"" forKey:@"categoryID"];
    }else
    {
        [paramDic setObject:categoryID forKey:@"categoryID"];
    }
    
    if (isEmptyStringOrNilOrNull(topicCode)) {
        
        [paramDic setObject:@"" forKey:@"topicCode"];
    }else
    {
        [paramDic setObject:topicCode forKey:@"topicCode"];
    }
    if (isEmptyStringOrNilOrNull(orderType)) {
        
        [paramDic setObject:@"" forKey:@"orderType"];
    }else
    {
        [paramDic setObject:orderType forKey:@"orderType"];
    }
    
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
    
    if (isEmptyStringOrNilOrNull(key)) {
        
        [paramDic setObject:@"" forKey:@"key"];
    }else
    {
        [paramDic setObject:key forKey:@"key"];
    }

  
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_SEARCH];
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_SEARCH andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//-----------点播视频详情信息
-(void)VODDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type", nil];
    if (NotEmptyStringAndNilAndNull(contentID)) {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//-------点播播放
-(void)VODPlayWithContentID:(NSString *)contentID andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",movie_type,@"movie_type", nil];
    
    if (NotEmptyStringAndNilAndNull(contentID)) {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//----------点播询价
-(void)VODInquiryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID", nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_INQUIRY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//---------付费点播
-(void)VODPayPlayWithContentID:(NSString *)contentID andWithLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID", nil];
    if (lockPWD.length>0) {
        [paramDic setObject:lockPWD forKey:@"lockPWD"];
    }
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PAY_PLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//--------点播停止
-(void)VODStopWithContentID:(NSString *)contentID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",[NSNumber numberWithInteger:breakPoint],@"breakPoint", nil];
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_STOP andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//-------点播播放记录列表
-(void)VODPlayListWithContentID:(NSString *)contentID andStarSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic;
     paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
    if (contentID.length>0) {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
    if (isEmptyStringOrNilOrNull(startSeq)) {
        
        [paramDic setObject:@"1" forKey:@"startSeq"];
    }else
    {
        [paramDic setObject:startSeq forKey:@"startSeq"];
    }
    
    if (isEmptyStringOrNilOrNull(count)) {
        
        [paramDic setObject:@"10" forKey:@"count"];
    }else
    {
        [paramDic setObject:count forKey:@"count"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PLAY_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//---------删除点播播放记录
-(void)DELVODPlayHistoryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
    if (contentID.length>0) {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:DEL_VOD_PLAY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//--------增加点播收藏
-(void)ADDVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic;
  paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
    if (NotEmptyStringAndNilAndNull(contentID)) {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
    [self postDemandRequestWithDictionary:paramDic  andType:ADD_VOD_FAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//--------删除点播收藏
-(void)DELVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
    if (isEmptyStringOrNilOrNull(contentID)) {
        [paramDic setObject:@"" forKey:@"contentID"];
    }else
    {
        [paramDic setObject:contentID forKey:@"contentID"];
    }
   
    [self postDemandRequestWithDictionary:paramDic  andType:DEL_VOD_FAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//------点播收藏列表
-(void)VODFavoryteListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    if (_token.length==0||!_token) {
        return;
    }
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",startSeq,@"startSeq",count,@"count", nil];
        [self postDemandRequestWithDictionary:paramDic  andType:VOD_FAVORITE_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}





//--------------点播 推荐 影片关联列表
-(void)VODAssccationListWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSDictionary *paramDic = [[NSDictionary alloc] init];
    if (_token.length>0) {
        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:10],@"count", nil];
    }else{
        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:10],@"count", nil];
    }
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_ASSCCIATION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//New...
- (void)VODAssccationListWithContentID:(NSString *)contentID andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSDictionary *paramDic = [[NSDictionary alloc] init];
    if (!count) {
        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",@"10",@"count", nil];
    }else{
        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",count,@"count", nil];
    }
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_ASSCCIATION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//----------------点播热门搜索关键词
- (void)VODTopSearchKeyWithKey:(NSString *)key andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSMutableDictionary *paramDic;
    if (count !=0) {
        paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:count,@"count",TERMINAL_TYPE,@"terminal_type", nil];
    }else
    {
        paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"10",@"count",TERMINAL_TYPE,@"terminal_type", nil];
    }
    if (key.length>0) {
        [paramDic setObject:key forKey:@"key"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOP_SEARCH_KEY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//调拨销售品订购
- (void)VODOrderProductWithProductID:(NSString *)productId andLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSDictionary *paramDic;
    if (lockPWD) {
        paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",lockPWD,@"lockPWD", nil];
    }else
    {
        //如果空
        paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token", nil];
    }
   
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_Order_Product andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
//点播吐槽影片
- (void)VODPraiseDegradeWithContentID:(NSString *)contentID andType:(NSString*)type andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_token, @"token",contentID,@"contentID",type,@"type", nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_Praise_Degrade andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
//2.25 运营组销售品列表
- (void)VODOperationProductListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
     NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_token, @"token",TERMINAL_TYPE,@"terminal_type", nil];
    
    if (NotEmptyStringAndNilAndNull(operationCode)) {
        [paramDic setObject:operationCode forKey:@"operationCode"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_Operation_Product_List andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//--------------点播运营组菜单
- (void)VODOPerMenuQWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"TERMINAL_TYPE",operationCode,@"operationCode", nil];
       [self postDemandRequestWithDictionary:paramDic  andType:@"VOD_OPER_MENU" andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

//------------点播分类菜单
- (void)VODCategoryMenuWithCategoryID:(NSInteger)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"TERMINAL_TYPE",categoryID,@"categoryID" ,nil];
    
    [self postDemandRequestWithDictionary:paramDic  andType:@"VOD_CATEGORY_MENU" andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)VODOperationDetailWithOperationCode:(NSString *)operationCode andTimeOut:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:operationCode,@"operationCode",nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OperationDetail andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
-(void)VODNewPlayRecordListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andSatrtDte:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",startSeq,@"startSeq",count,@"count",nil];
    
    if (startDate.length>0) {
        [paramDic setObject:startDate forKey:@"startDate"];
    }
    if (endDate.length>0) {
        [paramDic setObject:endDate forKey:@"endDate"];
    }
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_NewPlayRecordList andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)VODNewFavorateListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",startSeq,@"startSeq",count,@"count",nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_NewFavorateList andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
}
-(void)VODHomeConfigListWithTimeOut:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
    
    if (_VODPORTAL.length == 0) {
        
        returnBlock(-1,nil,VOD_HomeConfigList);
        
    }

    [self postDemandRequestWithDictionary:paramDic  andType:VOD_HomeConfigList andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
-(void)VODMenuConfigListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_MenuConfigList andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
- (void)VODUpLoadOrderWithOrderId:(NSString *)orderId andPrice:(NSString*)price andContentId:(NSString *)contentId andVodId:(NSString *)vodId andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock

{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",contentId,@"contentID",orderId,@"orderId",price,@"price",nil];
    if (vodId.length) {
        [paramDic setObject:vodId forKey:@"vodId"];
    }
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_UpLoadOrder andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
- (void)VODPraiseDegradeDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock

{
    if (_token.length==0||!_token) {
        return;
    }
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",contentID,@"contentID",nil];
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PraiseDegradeDetail andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
-(void)VODHospitalInfoWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock
{
    
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    if (regionCode.length==0) {
        regionCode = REGION_CODE;
    }
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:regionCode,@"code",TERMINAL_TYPE,@"terminal_type",nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_HospitalInfo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
-(void)VODHospitalInfoWithCode:(NSString *)code andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:code,@"code",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = outTime;
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_HospitalInfo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}
-(void)VODFindHospitalWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
    NSString *longitude = @"114.989792";
    NSString *latitude  = @"27.116745";
    if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
        longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
        latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
    }
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",nil];
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_GetHospital andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)VODHospitalListWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    [self postDemandRequestWithDictionary:paramDic  andType:VOD_HospitalList andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}

-(void)VODGetHomepageADImageAndReturn:(HTVODPortalRequest_Return)returnBlock{
    
    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"2",@"type", nil];
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_TOPIC_LIST];
    
    _manager.requestSerializer.timeoutInterval = 10;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        NSDictionary *dictionrespon = responseObject;
        NSArray *array = [dictionrespon objectForKey:@"topicList"];
        if (array.count) {
            dictionrespon = array[0];
        }
        NSString *topiccode = [dictionrespon objectForKey:@"topicCode"];
        
        if (topiccode) {
                        
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_TOPIC_PROGRAM_LIST];

            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                }

                returnBlock(0,responseObject,@"ADVOD_TOPIC_PROGRAM_LIST");

                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                returnBlock([error code],nil,@"ADVOD_TOPIC_PROGRAM_LIST");

            }];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        returnBlock([error code],nil,VOD_TOPIC_LIST);

        
    }];
    
    
}





#pragma mark -----------
-(void)postDemandRequestWithDictionary:(NSDictionary *)requestDic andType:(NSString *)type andReturn:(HTVODPortalRequest_Return)returnBlock{
    
    if (isEmptyStringOrNilOrNull(_VODPORTAL)) {
        
        returnBlock(-1,nil,type);
        return;
    }
    
    _manager.requestSerializer.timeoutInterval = KTimeout;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,type];
    
    NSLog(@"\n请求点播的发送数据   ：%@   \nURL ：%@\n",requestDic,URLString);
    [_manager POST:URLString parameters:requestDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        if ([type isEqualToString:VOD_TOPIC_PROGRAM_LIST]) {
            
            NSLog(@"这是首页轮播图");
            
            //请求成功时回调方法
            returnBlock(0,responseObject,VOD_TOPIC_LIST);
            
        }
        //请求成功时回调方法
        returnBlock(0,responseObject,type);

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
      returnBlock([error code],nil,type);
        
    }];
    
    
}

@end
