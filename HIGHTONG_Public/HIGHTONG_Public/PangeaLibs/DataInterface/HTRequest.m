//
//  HTRequest.m
//  DataInterface
//
//  Created by Kael on 15/7/7.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "HTRequest.h"
#import "DateTools.h"
#import <CommonCrypto/CommonDigest.h>
#import "HTPolymedicineInterface.h"
#import "HTCommonInterface.h"
#import "HTUNPortalInterface.h"
#import "HTEPGPortalInterface.h"
#import "HTJXBaseInterface.h"
#import "HTVODPortalInterface.h"
#import "HTUPortalInterface.h"
#import "HTPubInterface.h"
#import "HTNotificationInterface.h"
#import "HTUGCInterface.h"
@interface HTRequest ()
{
    NSString *_token;
    NSString *_UserName;
    NSString *_passWord;
    NSString *_resetPassword;
    
    NSString *_fogetPWDMobilePhone;
    NSString *_authCode;
    NSString *_mobilePhone;
    NSString *_newMobilePhone;
    NSString *_EPGPORTAL;
    NSString *_VODPORTAL;
    NSString *_UPORTAL;
    NSString *_ADPORTAL;
    NSString *_PMPORTAL;
    NSString *_JXTVPORTAL;
    
}
@property (nonatomic,strong) AFHTTPSessionManager *manager;
@property (nonatomic,strong) NSMutableArray *hotChannelArr;
@property (nonatomic,strong) NSMutableArray *allChannelArr;


@end



@implementation HTRequest

+(AFSecurityPolicy*)customSecurityPolicyWithCertificateType:(HTCertificateType)type{
    // 先导入证书
    NSString *cerPath;
    switch (type) {
        case Cer_pangeachina:
        {
            cerPath= [[NSBundle mainBundle] pathForResource:@"pangeachina" ofType:@"cer"];//证书的路径
        }
            break;
        case Cer_jktv:
        {
            cerPath= [[NSBundle mainBundle] pathForResource:@"jktv" ofType:@"cer"];//证书的路径
        }
            break;
        default:
            break;
    }
    
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    NSSet *cerSet = [NSSet setWithObjects:certData, nil];
    // AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    // allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    // 如果是 需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = NO;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    //如置为NO，建议自己添加对应域名的校验逻辑。
    securityPolicy.validatesDomainName = YES;
    
    securityPolicy.pinnedCertificates = cerSet;
    
    return securityPolicy;
}

-(id)initWithDelegate:(id<HTRequestDelegate>)delegate{

    self = [super init];
    if (self) {
        _delegate = delegate;
        self.manager = [AFHTTPSessionManager manager];
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;//允许菊花旋转

        _manager.responseSerializer = [AFJSONResponseSerializer serializer];//申明返回的结果是json类型
//        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",nil];//如果报接受类型不一致请替换一致text/html或别的

        _manager.requestSerializer = [AFJSONRequestSerializer serializer];//申明请求的数据是json类型
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
//        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
//--------------------

        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        
        NSLog(@"_token-------%@",_token);
        if (_token.length == 0) {

            _token = @"";
            
        }
        
//         48位的token 需要截取前32位之后才能使用 后16位是AES加密的秘钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];

            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
//            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        

        //________________________________________________________________

        
//        NSLog(@"aaaaaaa_manager.requestSerializer.HTTPRequestHeaders-----%@",_manager.requestSerializer.HTTPRequestHeaders);
//        NSLog(@"_manager.responseSerializer.acceptableContentTypes-----%@",_manager.responseSerializer.acceptableContentTypes);
            _EPGPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"EPGPORTAL"];
            _VODPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"VODPORTAL"];
            _UPORTAL   = [[NSUserDefaults standardUserDefaults] objectForKey:@"UPORTAL"];
            
//            _UPORTAL  = @"http://101.200.150.40:9090/UPORTAL";
            
            NSLog(@"_EPGPORTAL-------------%@",_EPGPORTAL);
            NSLog(@"_VODPORTAL-------------%@",_VODPORTAL);
            NSLog(@"_UPORTAL-------------%@",_UPORTAL);
            
//            _ADPORTAL  = @"http://60.205.14.72:9090/ADPORTAL";
            _ADPORTAL  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ADPORTAL"];
            if (_ADPORTAL.length==0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
            }
            
            _PMPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"polymedicine"];

            
            _JXTVPORTAL = [[NSUserDefaults standardUserDefaults] objectForKey:@"tvbiz"];
            
        
        
    }
    
    return self;
}

- (instancetype)init
{
    NSAssert(NO, @"请使用 initWithDelegate 方法进行初始化");//断言
    
    return nil;
}

#pragma mark -----
-(void)getHightongPORTALVersionWithTimeout:(NSInteger)timeout{

    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postCommandPrtal_RequestWithParam:paramDic andType:@"projectVersion"];
    
    
    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
    [unportal UNPortalProjectVersion:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
    
    
}
-(void)getHightongPORTALProjectListWithTimeout:(NSInteger)timeout{
    _manager.requestSerializer.timeoutInterval = timeout;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];//默认 不支持广告

//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:KREGION_CODE,@"regionKey",nil];
//    
//    [self postCommandPrtal_RequestWithParam:paramDic andType:@"projectList"];
    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
    [unportal UNPortalProjectListWithRegionKey:KREGION_CODE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
//-(void)getHightongPORTALProjectListWithTimeout:(NSInteger)timeout andResultBlcok:(void (^)(NSInteger status, NSMutableDictionary *result, NSString *type))resultBlock{
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];//默认 不支持广告
//    
//    //    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:KREGION_CODE,@"regionKey",nil];
//    //
//    //    [self postCommandPrtal_RequestWithParam:paramDic andType:@"projectList"];
//    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
//    [unportal UNPortalProjectListWithRegionKey:KREGION_CODE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        if (resultBlock) {
//            resultBlock(status,result,type);
//        }
//    }];
//
//
//}



-(void)getHightongRegionVersionWithTimeout:(NSInteger)timeout
{
    _manager.requestSerializer.timeoutInterval = timeout;
    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
    [unportal UNPortalRegionVersion:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


-(void)getHightongRegionListWithTimeout:(NSInteger)timeout
{
    _manager.requestSerializer.timeoutInterval = timeout;
    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
    [unportal UNPortalRegionList:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        
    }];
}



-(void)resetHightongConfigWithTimeout:(NSInteger)timeout
{
    _manager.requestSerializer.timeoutInterval = timeout;
    HTUNPortalInterface *unportal = [[HTUNPortalInterface alloc]init];
    [unportal UNPortalResetConfigWithType:INTELLIGENT_TERMINAL_TYPE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];

    }];
}



//暂时不用该方法了
-(void)postCommandPrtal_RequestWithParam:(NSMutableDictionary *)paramDic andType:(NSString *)type{
    return;
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",PROJECT_PORTAL,type];
    
    NSLog(@"\n请求版本的发送数据   ：%@   \nURL ：%@\n",paramDic,URLString);
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([type isEqualToString:@"projectVersion"]) {
            
            
            
            
        }
        if ([type isEqualToString:@"projectList"]) {
            [self loadingPRTALDataWith:responseObject];
            
        }
        
        
        
        //请求成功时回调方法
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
        }
        
    }  failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:type];
        }

    }];

    
}
//暂时不用该方法了
-(void)loadingPRTALDataWith:(NSDictionary *)result{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:result];
    
    NSArray *portalList = [mDic objectForKey:@"projectList"];
    
    for (int i=0; i<portalList.count; i++) {
        
        NSDictionary *itemDic = [portalList objectAtIndex:i];
        NSString *projectCode = [NSString stringWithFormat:@"%@",[itemDic objectForKey:@"projectCode"]];

        NSMutableString *projectUrl = [NSMutableString stringWithFormat:@"%@",[itemDic objectForKey:@"projectUrl"]];
       
        projectUrl = projectUrl.length>0 ? projectUrl :[NSMutableString stringWithFormat:@""];
        
        if ([projectUrl hasSuffix:@"/"]) {
            [projectUrl deleteCharactersInRange:NSMakeRange(projectUrl.length-1, 1)];
        }
        
        if ([projectCode isEqualToString:@"EPGPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"EPGPORTAL"];
 

            
        }
        if ([projectCode isEqualToString:@"VODPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"VODPORTAL"];


            
        }
        if ([projectCode isEqualToString:@"UPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"UPORTAL"];

        }
        if ([projectCode isEqualToString:@"ICS"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ICS"];

        }
        if ([projectCode isEqualToString:@"VODSWITCH"]) {
            if (![projectUrl isEqualToString:@"close"]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_VOD"];
            }

        }
        if ([projectCode isEqualToString:@"ADPORTAL"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ADPORTAL"];
            if (projectUrl.length==0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
            }
        }
        if ([projectCode isEqualToString:@"shphone"]) {
            //shphone
            //兼容 后缀 无论有没有 都删除一下 再拼接上
            if ([projectUrl hasSuffix:@"/shphone"]) {
                [KaelTool deleteString:@"/shphone" fromString:projectUrl];
                
            }
            if ([projectUrl hasSuffix:@"/shphone/"]) {
                [KaelTool deleteString:@"/shphone/" fromString:projectUrl];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone"];
//            [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
            [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/shphone/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
           
        }
        
        
        if ([projectCode isEqualToString:@"polymedicine"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"polymedicine"];
        }
        
        
        if ([projectCode isEqualToString:@"IMG"]) {
            
            if (APIVERSION.length>0) {
               [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMGPORTAL"];
            }else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMGPORTAL"];
            }
        }
        if ([projectCode isEqualToString:@"IMG_PUBLIC"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMG_PUBLIC"];
        }
        
        if ([projectCode isEqualToString:@"shphone_public"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone_public"];
            
        }
        if ([projectCode isEqualToString:@"appbase"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"appbase"];
            
        }
        if ([projectCode isEqualToString:@"IS_PUBLIC_ENV"]) {
            
            BOOL kIS_PUBLIC_ENV = [projectUrl integerValue]==1 ? YES : NO;
            [[NSUserDefaults standardUserDefaults] setBool:kIS_PUBLIC_ENV forKey:@"IS_PUBLIC_ENV"];
            
        }
        if ([projectCode isEqualToString:@"shtv"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shtv"];

        }
        
        if ([projectCode isEqualToString:@"operatorCode"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"operatorCode"];

        }
        
        
        if ([projectCode isEqualToString:@"mobilepush"]) {
            
            [[NSUserDefaults standardUserDefaults]setObject:projectCode forKey:@"mobilepush"];
        }
        
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    

}


#pragma mark ------ EPG_PORTAL

//全部频道列表
-(void)EPGChannelAllListWithLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout{
//    NSMutableDictionary*paramDic = [NSMutableDictionary dictionary];
//    
//    [paramDic setObject:[NSNumber numberWithInteger:lookbackFlag] forKey:@"lookbackFlag"];
//    [paramDic setObject:TERMINAL_TYPE forKey:@"terminal_type"];
//    if (_token.length>0) {
//        [paramDic setObject:_token forKey:@"token"];
//    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CHANNELALLLIST];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGChannelAllListWithLookbackFlag:lookbackFlag andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                        [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
            }
        
    }];
    
    
    
}


//2.1EPG浏览
-(void)EPGSHOWWithServiceID:(NSString *)serviceID andDate:( NSString *)date andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout
{
//    NSDictionary*paramDic = [NSDictionary dictionaryWithObjectsAndKeys:serviceID,@"serviceID",date,@"date",startDate,@"startDate",endDate,@"endDate", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGSHOWWithServiceID:serviceID andDate:date andStartDate:startDate andEndDate:endDate andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}
//2.2 频道列表
-(void)EPGChannelWithType:(NSInteger)type andLookbackFlag:(NSInteger)lookbackFlag andTimeout:(NSInteger)timeout
{
    
//    NSMutableDictionary*paramDic ;
//    
//    if (lookbackFlag == 1) {
//        paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:lookbackFlag],@"lookbackFlag", nil];
//    }else{
//        paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
//    }
//
//    if (type>=0) {
//        [paramDic setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
//    }
//    
//    if (_token.length>0) {
//        [paramDic setObject:_token forKey:@"token"];
//    }
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CHANNELLIST];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGChannelWithType:[NSString stringWithFormat:@"%ld",type] andLookbackFlag:[NSString stringWithFormat:@"%ld",lookbackFlag] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


//2.3频道查询 （不需要拼接、判断参数）
-(void)TVChangeWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout{
    
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CHANNELQUERY];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal TVChangeWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
   
}

//2.4 当前后继节目列表
-(void)EPGPFWithServiceID:(NSString *)serviceID  andType:(NSInteger)type andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
    
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    NSDictionary*paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type", nil];
    
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
//    if (serviceID.length>0) {
//        [paramDic setObject:serviceID forKey:@"id"];
//    }
//    if (type>=0) {
//        [paramDic setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
//    }
////    if (count>3) {
//        [paramDic setObject:[NSNumber numberWithInteger:2] forKey:@"count"];
////    }
    
    
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CURRENTEVENTLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGPFWithServiceID:serviceID andType:[NSString stringWithFormat:@"%ld",type] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}

//2.5 鉴权直播  TV_PLAY
-(void)TVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TVPLAY];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    
    [epgPortal TVPlayWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

    
}

//2.6 增强版鉴权直播
-(void)TVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:tvName,@"tvName",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TVPLAYEX];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal TVPlayEXWithTVName:tvName andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.7 鉴权时移
-(void)PauseTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_PAUSETVPLAY];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal PauseTVPlayWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.8 增强版鉴权时移[AlertLabel AlertInfoWithText:@"已到最后集数" andWith:_contentView withLocationTag:2];
-(void)PauseTVPlayEXWithTVName:(NSString *)tvName andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:tvName,@"tvName",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_PAUSETVPLAYEX];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal PauseTVPlayEXWithTVName:tvName andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.9 鉴权回看
-(void)EventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:eventID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTPLAY];
    
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal EventPlayWithEventID:eventID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.10 增强版鉴权回看  EVENT_PLAY_EX
-(void)EventPlayEXWithTVName:(NSString *)tvName andStarTime:(NSString *)startTime andEndTime:(NSString *)endTime andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:tvName,@"tvName",TERMINAL_TYPE,@"terminal_type",_token,@"token",startTime,@"startTime",endTime,@"endTime", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTPLAYEX];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EventPlayEXWithTVName:tvName andStarTime:startTime andEndTime:endTime andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.11 分类列表


-(void)TypeListWithTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:REGION_ID,@"regionID",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TYPELIST];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal TypeListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.12 系统配置
-(void)ConfigWithTimeout:(NSInteger)timeout
{
    
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:REGION_ID,@"regionID",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_CONFIG];
}

//2.13 终端软件下载

-(void)SoftwareWithTImeout:(NSInteger)timeout
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:REGION_ID,@"regionID",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_SOFIWAREDOWN];
}

//2.14 系统时间

-(void)SystemTimeWithTimeout:(NSInteger)timeout
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_SYSTEMTIME];
    
}

//2.15 EPG 搜索2

-(void)EPGSearchEXWithKey:(NSString *)key andType:(NSInteger)type andServiceID:(NSString *)serviceID andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout
{
    
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",_token,@"token",[NSNumber numberWithInteger:type],@"type", nil];
//    
//    if (key) {
//        [paramDic setObject:key forKey:@"key"];
//    }
//    if (startDate) {
//        [paramDic setObject:startDate forKey:@"startDate"];
//    }
//    if (endDate) {
//        [paramDic setObject:endDate forKey:@"endDate"];
//    }
//    if (serviceID) {
//        [paramDic setObject:serviceID forKey:@"serviceID"];
//    }
//    
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTSEARCH];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGSearchEXWithKey:key andType:[NSString stringWithFormat:@"%ld",type] andServiceID:serviceID andStartDate:startDate andEndDate:endDate andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}

//2.16 热门搜索关键词


-(void)TopSearchKeyWithKey:(NSString *)key andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:key,@"key",[numberFormatter stringFromNumber: [NSNumber numberWithInteger: count]],@"count",TERMINAL_TYPE,@"terminal_type", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TOPSEARCHWORDS];
    
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal TopSearchKeyWithKey:key andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}

//2.17 增加频道收藏

-(void)AddTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout
{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_ADDTVFAVORATE];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal AddTVFavorateWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.18 删除频道收藏

-(void)DelTVFavorateWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_DELTVFAVORATE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal DelTVFavorateWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}

//2.19 频道收藏列表
-(void)TVFavorateListWithTimeout:(NSInteger)timeout
{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TVFAVORATELIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal TVFavorateListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.20 播放记录列表
-(void)TVPlayListWithTimeout:(NSInteger)timeout
{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TVPLAYLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal TVPlayListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
 
    }];
    
}
//2.20 3。5版本的接口
-(void)TVPlayListWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andTImeout:(NSInteger)timeout{

//    paramDic= [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
//    
//    if (startDate.length!=0) {
//        [paramDic setObject:startDate forKeyedSubscript:@"startDate"];
//        
//    }
//    if (endDate!=0) {
//        [paramDic setObject:endDate forKeyedSubscript:@"endDate"];
//        
//    }
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_TVPLAYLIST];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal TVPlayListWithStartDate:startDate andEndDate:endDate andTImeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];

}

//2.21 删除播放记录
-(void)DelTVPlayWithServiceID:(NSString *)serviceID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",serviceID,@"serviceID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_DELTVPLAY];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal DelTVPlayWithServiceID:serviceID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
    
}

//2.22 回看停止

-(void)EventStopWithEventID:(NSString *)eventID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout
{
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:eventID,@"eventID",TERMINAL_TYPE,@"terminal_type",_token,@"token",[numberFormatter stringFromNumber: [NSNumber numberWithInteger: breakPoint]],@"breakPoint",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTSTOP];
    
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal EventStopWithEventID:eventID andBreakPoint:breakPoint andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//2.23 查询回看播放记录

-(void)EventPlayListWithEventID:(NSString *)eventID andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:eventID,@"eventID",TERMINAL_TYPE,@"terminal_type",_token,@"token",[numberFormatter stringFromNumber: [NSNumber numberWithInteger: count]],@"count",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTPLAYLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal EventPlayListWithEventID:eventID andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
    
}

//2.24 删除回看播放记录   DEL_EVENT_PLAY

-(void)DelEventPlayWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_DELEVENTPLAY];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal DelEventPlayWithEventID:eventID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.25 增加回看收藏  ADD_EVENT_FAVORITE

-(void)AddEventFavorateWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout
{
    
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
//    
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_ADDEVENTFAVORATE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal AddEventFavorateWithEventID:eventID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.26 删除回看收藏  DEL_EVENT_FAVORITE

-(void)DelEventFavorateWithEventID:(NSString *)eventID anTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_DELEVENTFAVORATE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal DelEventFavorateWithEventID:eventID anTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



//  2.27 回看收藏列表  EVENT_FAVORITE_LIST

-(void)EventFavorateListWithTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTFAVORATELIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EventFavorateListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}



//2.28 热门频道列表  HOT_CHANNEL_LIST

-(void)HotChannelListWithTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_HOTCHANNELLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];

    [epgPortal HotChannelListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.29 预约节目   ADD_EVENT_RESERVE

-(void)AddEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_ADDEVENTRESERVE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal AddEventReserveWithEventID:eventID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.30 取消预约节目 DEL_EVENT_RESERVE
-(void)DelEventReserveWithEventID:(NSString *)eventID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",eventID,@"eventID",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_DELEVENTRESERVE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal DelEventReserveWithEventID:eventID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



- (void)epgSearchOrderResultWith:(NSString *)key andStartDate:(NSString *)startDate andEndDate:(NSString *)endDate andCount:(NSString *)count andTimeout:(NSInteger)timeout
{
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:@"epgSearch"];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal epgSearchOrderResultWith:key andStartDate:startDate andEndDate:endDate andCount:count andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



-(void)ADListWith:(NSInteger)timeout{
  
//    if (regionCode.length==0) {
//        regionCode = REGION_CODE;
//        if (![KREGION_CODE isEqualToString:@"jx-0-0"]) {
//            //非江西版本 上传写死的regionCode
//            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
//        }else{
//           //江西版本 自动获取 若 regionCode 为空，暂不需要传入该参数
//        }
//        
//    }else{
//        [paramDic setObject:regionCode forKey:@"regionCode"];
//    }
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_ADLIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal ADListWith:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


-(void)EPGUpdateTvFavoriteWithSeq:(NSString*)seq andServiceID:(NSString *)serviceID andTVName:(NSString *)name andTimeou:(NSInteger)timeout{
    
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setValue:seq forKey:@"seq"];
//    [dic setValue:serviceID forKey:@"id"];
//    [dic setValue:name forKey:@"name"];
//    //    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",seq,@"seq",name,@"name",serviceID,@"id",nil];
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",@[dic],@"servicelist",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_UPDATE_TVFAVORATE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGUpdateTvFavoriteWithSeq:seq andServiceID:serviceID andTVName:name andTimeou:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

- (void)EPGUpdateTvFavoriteWithNewSortArr:(NSArray *)sortArr andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",sortArr,@"servicelist",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_UPDATE_TVFAVORATE];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGUpdateTvFavoriteWithNewSortArr:sortArr andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}





//2.31 预约节目列表  EVENT_RESERVE_LIST

-(void)EventRevserveWithTimeou:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_EVENTRESERVELIST];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EventRevserveWithTimeou:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


-(void)EPGGetHomepageHotChannel{

    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = 10;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_HOTCHANNELLIST];

    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        _hotChannelArr = [[NSMutableArray alloc] init];
        _hotChannelArr = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"list"]];

        _manager.requestSerializer.timeoutInterval = 10;
        NSString *URLStr = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_CURRENTEVENTLIST];


        [_manager POST:URLStr parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
            //这里应该对 responseObject  遍历 去除所需数据  然后添加到 _hotChannelArr  中
            
            if ([[responseObject objectForKey:@"ret"] integerValue] == 0) {
                
                NSDictionary *serviceList = [[NSDictionary alloc] initWithDictionary:[[responseObject objectForKey:@"servicelist"] firstObject] ];

                NSArray *serviceInfo = [serviceList objectForKey:@"serviceinfo"];
                
                for (int i = 0; i < serviceInfo.count; i++) {
                    
                    for (int j = 0; j < _hotChannelArr.count; j++) {
                        
                        NSString *hotChannelID = [[_hotChannelArr objectAtIndex:j] objectForKey:@"serviceID"];
                        NSString *serviceID = [[serviceInfo objectAtIndex:i] objectForKey:@"serviceID"];
                        NSArray *EPGInfoArr = [[serviceInfo objectAtIndex:i] objectForKey:@"epginfo"];
                        
                        if ([hotChannelID isEqualToString:serviceID]) {
                            NSMutableDictionary *hotDic = [NSMutableDictionary dictionaryWithDictionary:[_hotChannelArr objectAtIndex:j]];
                            [hotDic setObject:EPGInfoArr forKey:@"epginfo"];
                            [_hotChannelArr replaceObjectAtIndex:j withObject:hotDic];
//                            [[_hotChannelArr objectAtIndex:j] setObject:EPGInfoArr forKey:@"epginfo"];
                            break;
                        }
                        
                        
                    }
                    
                }

                
            }
            
            
            
            
            NSMutableDictionary *mResultDic = [[NSMutableDictionary alloc] init];
            if ([_hotChannelArr count]>0) {
                [mResultDic setObject:_hotChannelArr forKey:@"list"];

            }

            [mResultDic setObject:@"0" forKey:@"ret"];
            
            
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:mResultDic andType:EPG_HOTCHANNELLIST];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"fail = %@",error);
            //请求失败时候，回调方法
            // -1001 请求超时
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:EPG_HOTCHANNELLIST];
            }
            
            
        }];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:EPG_HOTCHANNELLIST];
        }
        
        
    }];
    

}

-(void)EPGGetLivePageChannelInfo{

    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = 10;
        NSString *URLStr = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_CHANNELLIST];
        //    NSArray *ACArr = [[NSUserDefaults standardUserDefaults] objectForKey:@"servicelist"];

        [_manager POST:URLStr parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
            
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
            NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
            
            _allChannelArr = [[NSMutableArray alloc] initWithArray:mSevicelistArr];
            
            if (_allChannelArr.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:_allChannelArr forKey:@"servicelist"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            //下面是请求当前后继节目列表了
            NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",@"2",@"count",nil];
            _manager.requestSerializer.timeoutInterval = 10;
            
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_CURRENTEVENTLIST];

            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {                if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                }
                NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
                
                NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
                
                NSMutableDictionary *MSDic = [[NSMutableDictionary alloc] init];
                if ([mSevicelistArr count]>=1) {
                    MSDic= [[NSMutableDictionary alloc] initWithDictionary:[mSevicelistArr objectAtIndex:0]];
                    
                }
                else{
                    MSDic = [[NSMutableDictionary alloc] init];
                }
                NSMutableArray *KserviceinfoArr = [[NSMutableArray alloc] initWithArray:[MSDic objectForKey:@"serviceinfo"]];

                _allChannelArr = [NSMutableArray arrayWithArray:[self getLivePageArrayWithChannelList:_allChannelArr andEPGList:KserviceinfoArr]];
                NSMutableDictionary *resultListDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"ret"],@"ret",_allChannelArr,@"channelList", nil];
                
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultListDic andType:@"allCategoryChannelList"];
                }
                
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                NSLog(@"fail = %@",error);
                //请求失败时候，回调方法
                NSMutableDictionary *resultListDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"ret"],@"ret",_allChannelArr,@"channelList", nil];
                // -1001 请求超时
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:resultListDic andType:EPG_CURRENTEVENTLIST];
                }
                
            }];
            
            
            
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:EPG_CHANNELLIST];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"fail = %@",error);
            //请求失败时候，回调方法
            // -1001 请求超时
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:EPG_CHANNELLIST];
            }
           
        }];

}

-(void)EPGGetLivePageChannelInfoAndisType:(NSInteger )type{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    if (type>=0) {
        [paramDic setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
    }
    _manager.requestSerializer.timeoutInterval = 10;
    
    NSString *URLStr = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_CHANNELLIST];
    
    //这个很重要！
    //这个很重要！
    //这个很重要！ EPG_CHANNELLIST这个是 请求的所有分类下的所有频道  typeCategoryChannelList 是某个分类下的 所有频道
    NSString *request_Type = type<=0 ? EPG_CHANNELLIST : @"typeCategoryChannelList";
    
    [_manager POST:URLStr parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
        
        _allChannelArr = [NSMutableArray arrayWithArray:mSevicelistArr];//包含type的频道列表
        if ((type <= 0) && (_allChannelArr.count>0)) {
            [[NSUserDefaults standardUserDefaults] setObject:_allChannelArr forKey:@"channelList"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if (_allChannelArr.count<=0) {
            
            NSMutableDictionary *typeDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)type],@"type", nil];
            
            _allChannelArr = [NSMutableArray arrayWithObject:typeDic];
            
            NSMutableDictionary *resultListDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"ret"],@"ret",_allChannelArr,@"channelList", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultListDic andType:@"typeCategoryChannelList"];
            }
            return ;
        }

        //下面是请求当前后继节目列表了
        NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",@"2",@"count",nil];
        _manager.requestSerializer.timeoutInterval = 10;
        
        NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_CURRENTEVENTLIST];
        [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
            
            NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
            
            NSMutableDictionary *MSDic = [[NSMutableDictionary alloc] init];
            if ([mSevicelistArr count]>=1) {
                MSDic= [[NSMutableDictionary alloc] initWithDictionary:[mSevicelistArr objectAtIndex:0]];
                
            }
            else{
                MSDic = [[NSMutableDictionary alloc] init];
            }
            NSMutableArray *KserviceinfoArr = [[NSMutableArray alloc] initWithArray:[MSDic objectForKey:@"serviceinfo"]];
            
            _allChannelArr = [NSMutableArray arrayWithArray:[self getLivePageArrayWithChannelList:_allChannelArr andEPGList:KserviceinfoArr]];
            NSMutableDictionary *resultListDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"ret"],@"ret",_allChannelArr,@"channelList", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultListDic andType:request_Type];
                
            }
            
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"fail = %@",error);
            //请求失败时候，回调方法
            // -1001 请求超时
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                NSMutableDictionary *resultListDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"ret"],@"ret",_allChannelArr,@"channelList", nil];
                
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultListDic andType:request_Type];
            }
            
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:request_Type];
        }
        
    }];
    
}



//首页轮播图
-(void)getHomePageSlideshowList{
//    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
//    if (regionCode.length==0) {
//        if (![KREGION_CODE isEqualToString:@"jx-0-0"]) {
//            //非江西版本 需要传入写死的 regionCode
//            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
//        }else{
//             //江西版本 regionCode参数 取不到就 暂不上传了吧
//        }
//    }else{
//        [paramDic setObject:regionCode forKey:@"regionCode"];
//    }
    _manager.requestSerializer.timeoutInterval = 10;
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal getHomePageSlideshowListAndReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:@"slideshowList"];
}

- (void)HomeADSLiderPagess{
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    if (regionCode.length==0) {
        if (![KREGION_CODE isEqualToString:@"jx-0-0"] && ![KREGION_CODE isEqualToString:@"hb-0-0"]) {
            //非江西版本 需要传入写死的 regionCode
            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
        }else{
            //江西版本 regionCode参数 取不到就 暂不上传了吧
        }
    }else{
        [paramDic setObject:regionCode forKey:@"regionCode"];
    }
    _manager.requestSerializer.timeoutInterval = 5;
    NSString *type = @"slideshowList";
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,type];
    NSLog(@"上传的数据 : %@ URL : %@",paramDic,URLString);

    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];

    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
        if([type isEqualToString:@"slideshowList"]){
            
            NSDictionary *dictionSlide = responseObject;//epg广告
            NSMutableArray *SlideGuangGaoarray = [NSMutableArray array];
            NSArray *AAA =[responseObject objectForKey:@"slideshowList"];
            SlideGuangGaoarray.array = AAA;
            
            
            
            NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"PANGEA_PHONE_HOMEPAGE_CAROUSEL_IMAGE",@"code",@"0",@"type",@"",@"assetID",@"",@"assetName", nil];
            
            NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:actionInfo forKey:@"actionInfo"];
            [paramDic setObject:userInfo forKey:@"userInfo"];
            
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
            
            NSLog(@" 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,type);
            if (!SUPPORT_AD) {
               //这里是可以直接返回paramDic的
            }
            

            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                }
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"adList"]];
                for (NSDictionary *Dic in array1) {
                    NSDictionary *pictureRes = [Dic objectForKey:@"pictureRes"];
                    NSString *imageLink = [pictureRes objectForKey:@"picLink"];
                    NSString *urlLink = [pictureRes objectForKey:@"urlLink"];
                    //                    NSString *ID = [Dic objectForKey:@"id"];
                    //                    NSString *duration = [pictureRes objectForKey:@"duration"];
                    //                    NSString *style = [pictureRes objectForKey:@"0"];
                    //                    NSString *type = [pictureRes objectForKey:@"6"];
                    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
                    if (urlLink.length > 0) {
                        [diction setObject:urlLink forKey:@"urlLink"];
                    }
                    if ((imageLink.length > 0)) {
                        [diction setObject:imageLink forKey:@"imageLink"];
                        
                        [diction setObject:@"0" forKey:@"seq"];
                        [diction setObject:@"7" forKey:@"type"];
                        [SlideGuangGaoarray addObject:diction];
                    }
                    
                }
                NSDictionary *resp = dictionSlide;
                NSMutableDictionary *deepDiction = [resp mutableCopy];
                NSMutableArray *guanggaoSlide = [deepDiction objectForKey:@"slideshowList"];
                guanggaoSlide = [NSMutableArray arrayWithArray:SlideGuangGaoarray];
        
                [deepDiction setObject:SlideGuangGaoarray forKey:@"slideshowList"];
                
                if (_delegate &&[_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:deepDiction andType:@"HomeADSLiderPage"];
                    return ;
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                //请求失败时候，回调方法
                // -1001 请求超时
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:dictionSlide andType:@"HomeADSLiderPage"];
                    return ;
                }
                
            }];
        
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:@"HomeADSLiderPage"];
            return;
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:@"HomeADSLiderPage"];
        }
        
    }];
    
   
}//首页轮播图广告

-(void)commAPI_HomeSliderpages{
    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    if (regionCode.length==0) {
        if ( ![KREGION_CODE isEqualToString:@"hb-0-0"]) {
            //非江西版本 需要传入写死的 regionCode
            [paramDic setObject:REGION_CODE forKey:@"regionCode"];
        }else{
            //江西版本 regionCode参数 取不到就 暂不上传了吧
        }
    }else{
        [paramDic setObject:regionCode forKey:@"regionCode"];
    }
    _manager.requestSerializer.timeoutInterval = 5;
    NSString *type = @"slideshowList";
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,type];
    if (_EPGPORTAL.length == 0) {
        return;
    }
    NSLog(@"上传的数据 : %@ URL : %@",paramDic,URLString);

    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    
    [common CNCommonSlideshowListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (status == 0) {
            NSDictionary *dictionSlide = result;//epg广告
            NSMutableArray *SlideGuangGaoarray = [NSMutableArray array];
            NSArray *AAA =[result objectForKey:@"slideshowList"];
            SlideGuangGaoarray.array = AAA;
            
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:status andResult:dictionSlide andType:@"HomeADSLiderPage"];
            }
            return ;
            //接口如果需要拼接 那就要拼一下
            [common CNCommonAdListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
                
                if (status == 0) {
                    if ([[result objectForKey:@"ret"] integerValue] == -2) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                    }
                    
                    NSMutableArray *array1 = [NSMutableArray arrayWithArray:[result objectForKey:@"adList"]];
                    for (NSDictionary *Dic in array1) {

                        NSString *imageLink = [Dic objectForKey:@"picLink"];
                        NSString *urlLink = [Dic objectForKey:@"urlLink"];
                        //                    NSString *ID = [Dic objectForKey:@"id"];
                        //                    NSString *duration = [pictureRes objectForKey:@"duration"];
                        //                    NSString *style = [pictureRes objectForKey:@"0"];
                        //                    NSString *type = [pictureRes objectForKey:@"6"];
                        NSMutableDictionary *diction = [NSMutableDictionary dictionary];
                        if (urlLink.length > 0) {
                            [diction setObject:urlLink forKey:@"urlLink"];
                        }
                        if ((imageLink.length > 0)) {
                            [diction setObject:imageLink forKey:@"imageLink"];
                            
                            [diction setObject:@"0" forKey:@"seq"];
                            [diction setObject:@"7" forKey:@"type"];
                            [SlideGuangGaoarray addObject:diction];
                        }
                        
                    }
                    NSDictionary *resp = dictionSlide;
                    NSMutableDictionary *deepDiction = [resp mutableCopy];
                    NSMutableArray *guanggaoSlide = [deepDiction objectForKey:@"slideshowList"];
                    guanggaoSlide = [NSMutableArray arrayWithArray:SlideGuangGaoarray];
                    
                    [deepDiction setObject:SlideGuangGaoarray forKey:@"slideshowList"];
                    
                    if (_delegate &&[_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                        [_delegate HTRequestFinishedWithReturnStatus:0 andResult:deepDiction andType:@"HomeADSLiderPage"];
                        return ;
                    }
                    
                }else{
                    // -1001 请求超时
                    if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                        [_delegate HTRequestFinishedWithReturnStatus:status andResult:dictionSlide andType:@"HomeADSLiderPage"];
                    }
                
                }
            }];
            
            
        }else{
        
            // -1001 请求超时
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:status andResult:nil andType:@"HomeADSLiderPage"];
            }
        }
        
    }];
    
    return;
    //这是老接口这么做的
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if([type isEqualToString:@"slideshowList"]){
            
            NSDictionary *dictionSlide = responseObject;//epg广告
            NSMutableArray *SlideGuangGaoarray = [NSMutableArray array];
            NSArray *AAA =[responseObject objectForKey:@"slideshowList"];
            SlideGuangGaoarray.array = AAA;
            
            NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"PANGEA_PHONE_HOMEPAGE_CAROUSEL_IMAGE",@"code",@"0",@"type",@"",@"assetID",@"",@"assetName", nil];
            
            NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:actionInfo forKey:@"actionInfo"];
            [paramDic setObject:userInfo forKey:@"userInfo"];
            
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
            
            NSLog(@" 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,type);
            if (!SUPPORT_AD) {
                //这里是可以直接返回paramDic的
            }
            
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
                }
                
                NSMutableArray *array1 = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"adList"]];
                for (NSDictionary *Dic in array1) {
                    NSDictionary *pictureRes = [Dic objectForKey:@"pictureRes"];
                    NSString *imageLink = [pictureRes objectForKey:@"picLink"];
                    NSString *urlLink = [pictureRes objectForKey:@"urlLink"];
                    //                    NSString *ID = [Dic objectForKey:@"id"];
                    //                    NSString *duration = [pictureRes objectForKey:@"duration"];
                    //                    NSString *style = [pictureRes objectForKey:@"0"];
                    //                    NSString *type = [pictureRes objectForKey:@"6"];
                    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
                    if (urlLink.length > 0) {
                        [diction setObject:urlLink forKey:@"urlLink"];
                    }
                    if ((imageLink.length > 0)) {
                        [diction setObject:imageLink forKey:@"imageLink"];
                        
                        [diction setObject:@"0" forKey:@"seq"];
                        [diction setObject:@"7" forKey:@"type"];
                        [SlideGuangGaoarray addObject:diction];
                    }
                    
                }
                NSDictionary *resp = dictionSlide;
                NSMutableDictionary *deepDiction = [resp mutableCopy];
                NSMutableArray *guanggaoSlide = [deepDiction objectForKey:@"slideshowList"];
                guanggaoSlide = [NSMutableArray arrayWithArray:SlideGuangGaoarray];
                
                [deepDiction setObject:SlideGuangGaoarray forKey:@"slideshowList"];
                
                if (_delegate &&[_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:deepDiction andType:@"HomeADSLiderPage"];
                    return ;
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                //请求失败时候，回调方法
                // -1001 请求超时
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:dictionSlide andType:@"HomeADSLiderPage"];
                    return ;
                }
                
            }];
            
            
            
            
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:@"HomeADSLiderPage"];
            return;
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:@"HomeADSLiderPage"];
        }
        
    }];

}


-(void)EPGGetRegionListWithTimeout:(NSInteger)outTime{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",REGION_TYPE,@"regionType",nil];
    
    _manager.requestSerializer.timeoutInterval = 10;

//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_REGION_LIST];

    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGGetRegionListWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)EPGFindRegionWithTimeout:(NSInteger)outTime{
//    NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
//    NSString *longitude = @"114.989792";//0.000000
//    NSString *latitude  = @"27.116745";//0.000000
//    if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
//        longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
//        latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
//    }
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",REGION_TYPE,@"regionType",nil];
    _manager.requestSerializer.timeoutInterval = 10;

//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_FIND_REGION];
    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGFindRegionWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
-(void)EPGFindRegionWithLongitude:(NSString *)longitude andLatitude:(NSString *)latitude andTimeout:(NSInteger)outTime{

//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",REGION_TYPE,@"regionType",nil];
    _manager.requestSerializer.timeoutInterval = 10;
    
//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_FIND_REGION];

    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGFindRegionWithLongitude:longitude andLatitude:latitude andTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}

-(void)EPGGetRegionInfoWithRegionCode:(NSString *)regionCode andTimeout:(NSInteger)timeout{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:regionCode,@"regionCode",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = timeout;

//    [self postEPGPORTAL_RequestWithParam:paramDic andType:EPG_REGION_INFO];

    HTEPGPortalInterface *epgPortal = [[HTEPGPortalInterface alloc]init];
    [epgPortal EPGGetRegionInfoWithRegionCode:regionCode andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



#pragma mark -----------
-(void)postEPGPORTAL_RequestWithParam:(NSDictionary *)param andType:(NSString *)type{
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,type];
    NSLog(@"上传的数据 : %@ URL : %@",param,URLString);
    
    [_manager POST:URLString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *channelListArr = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelList"];
        
        NSMutableDictionary *CHANNELListDic = [NSMutableDictionary dictionary];
        for (NSDictionary* dic in channelListArr) {
            NSArray * serviceinfo = [dic objectForKey:@"serviceinfo"];
            for (NSDictionary *channelInfo in serviceinfo) {
                NSString *ID = [channelInfo objectForKey:@"id"];
                if (ID.length) {
                    [CHANNELListDic setObject:channelInfo forKey:ID];
                }
            }
        }
        
        if (![type isEqualToString:EPG_EVENTSEARCH]) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
        }
        
        
        if ([type isEqualToString:EPG_CHANNELLIST]) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
            NSMutableArray *mSevicelistArr = [[NSMutableArray alloc] initWithArray:[resultDic objectForKey:@"servicelist"]];
            
            _allChannelArr = [[NSMutableArray alloc] initWithArray:mSevicelistArr];
            
            if (_allChannelArr.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:_allChannelArr forKey:@"servicelist"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }

        }
        
       
        
        if ([type isEqualToString:EPG_EVENTSEARCH]) {
            
            
          
            
            NSMutableArray *resultArr = [[NSMutableArray alloc] init];

            if (responseObject != nil) {//返回结果是有搜索结果的
                if ([CHANNELListDic count]) { //如果有频道具体信息
                    for (NSDictionary *searchResault in responseObject) {
                        NSDictionary *epginfo = [searchResault objectForKey:@"epginfo"];
                        NSString *serviceID  =  [searchResault objectForKey:@"serviceID"];
                        
                        NSDictionary *ChannelInfo = [CHANNELListDic objectForKey:serviceID];
                       
                        if ([ChannelInfo objectForKey:@"imageLink"]) {
//                             NSMutableDictionary *AN_epginfo = [[NSMutableDictionary alloc]initWithDictionary:epginfo];
                            
                            
                            NSString * channelID = [ChannelInfo objectForKey:@"id"];
                            NSString * channelName = [ChannelInfo objectForKey:@"name"];
                            NSString * imageLink = [ChannelInfo objectForKey:@"imageLink"];
                            
                            NSMutableArray *AN_epginfo =  [NSMutableArray array];
                            for (NSDictionary *diccccc in epginfo) {
                                NSMutableDictionary *AN_diccccc = [[NSMutableDictionary alloc]initWithDictionary:diccccc];
                                //添加一些需要的信息进入目标字典中
                                if (channelID.length) {
                                    [AN_diccccc setObject:channelID forKey:@"channelID"];
                                }
                                if (channelName.length) {
                                    [AN_diccccc setObject:channelName forKey:@"channelName"];
                                }
                                if (imageLink.length) {
                                    [AN_diccccc setObject:imageLink forKey:@"imageLink"];
                                }
                                
                                [AN_epginfo addObject:AN_diccccc];
                            }
                            
                            //拼接好的节目详细信息拼进目标中
                            NSMutableDictionary *AN_searchResault = [[NSMutableDictionary alloc]initWithDictionary:searchResault];
                            
                            if ([AN_epginfo count]) {
                                [AN_searchResault setObject:AN_epginfo forKey:@"epginfo"];
                            }
                            if (imageLink.length) {
                                [AN_searchResault setObject:imageLink forKey:@"imageLink"];
                            }
                            if (channelID.length) {
                                [AN_searchResault setObject:channelID forKey:@"serviceID"];
                            }
                            //添加上这个数据
                            [resultArr addObject:AN_searchResault];
                        }
                       
                      
                        
                    }
                }
            }
            
            
            
            if (responseObject!=nil) {

            }
            
            
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                if ([type isEqual:EPG_EVENTSEARCH]) {
                     [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultArr andType:type];
                    return ;
                }
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:EPG_CHANNELLIST];
               
            }

            
        }
        
        
        if ([type isEqualToString:@"epgSearch"]) {
            NSString *count = [responseObject objectForKey:@"count"];
            if ([count integerValue] > 0) {
                
                NSMutableArray *datelistArray = [NSMutableArray array];
                
                datelistArray.array = [[responseObject objectForKey:@"dateList"] mutableCopy];
                
                NSMutableDictionary *ALL = [NSMutableDictionary dictionary];
                NSInteger count = 0;
                NSMutableArray *JIEGUOdatelistArray = [NSMutableArray array];
                
                for (NSDictionary *dateEPG in datelistArray) {
                    
                    
                    NSMutableArray *jieguoserviceList= [NSMutableArray array];
                    
                    
                    NSMutableArray *serviceListArray = [dateEPG objectForKey:@"serviceList"];
                    NSMutableArray *date = [dateEPG objectForKey:@"date"];
                    
                    //判断是否有此频道  如果有的话，那么就加入到  节目单中
                    for (NSDictionary *EPGinfo in serviceListArray) {
                        NSString * serviceId = [EPGinfo objectForKey:@"serviceId"];
                        
                        //如果存在这个频道的话
                        NSDictionary *ChannelInfo = [CHANNELListDic objectForKey:serviceId];
                        
                        if ([ChannelInfo count]) {
                            NSMutableDictionary *EPGJIEGUO = [NSMutableDictionary dictionary];
                            
                            NSString * channelID = [ChannelInfo objectForKey:@"id"];
                            NSString * channelName = [ChannelInfo objectForKey:@"name"];
                            NSString * imageLink = [ChannelInfo objectForKey:@"imageLink"];
                            
                            NSMutableDictionary *jieguoEPGinfo = [[ NSMutableDictionary alloc]initWithDictionary:EPGinfo];
                
                        
                            if (channelName.length) {
                                [EPGJIEGUO setObject:channelName forKey:@"channelName"];
                            }
                            if (imageLink.length) {
                                [EPGJIEGUO setObject:imageLink forKey:@"imageLink"];
                            }
                            if (channelID.length) {
                                [EPGJIEGUO setObject:channelID forKey:@"serviceID"];
                            }

                            
                            [EPGJIEGUO setObject:jieguoEPGinfo forKey:@"epgList"];
                            
                            count++;
                            [jieguoserviceList addObject:EPGJIEGUO];
                        }
                        
                       
                    }
                    //遍历了好多节目了
                    if([jieguoserviceList count])
                    {
                        NSMutableDictionary *dataServiceListDic = [NSMutableDictionary dictionary];
                        
                        [dataServiceListDic setObject:date forKey:@"date"];
                        [dataServiceListDic setObject:jieguoserviceList forKey:@"serviceList"];
                        
                        [JIEGUOdatelistArray addObject:dataServiceListDic];
                    }

                    
                }
                
                [ALL setObject:JIEGUOdatelistArray forKey:@"dateList"];
                [ALL setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:ALL andType:type];
                    return;
                }
                
            }else
            {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            }
            
            
        }
        
        if ([type isEqualToString:EPG_SYSTEMTIME]) {
            
            NSString *sysTime=[responseObject objectForKey:@"sysTime"];
            if (!sysTime) {
                return ;
            }
            
            //获取服务器时间
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *SystemDateTime = [formatter dateFromString:sysTime];
            int TimeDiffence = [SystemDateTime timeIntervalSinceNow];
            NSLog(@"我的差值时间差值%d",TimeDiffence);
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",TimeDiffence] forKey:@"SystemTimeDiffence"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            //end
        }
        
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
            return;
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:type];
        }
        
    }];
    

    
}

#pragma mark ------- VODPORTAL (珠江数码)
/*------------珠江数码相关点播API-------------*/

//------------点播运营组
-(void)VODOperationListWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] init];
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPERATION_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOperationListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//-----------点播专题
-(void)VODTopicListWithTimeout:(NSInteger)timeout{
    
//    NSDictionary *paramDic = [[NSDictionary alloc] init];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_TOPIC_LIST,@"@command",paramDic,@"param", nil];
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopicListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//New...
-(void)VODTopicListWithType:(NSInteger)type andScope:(NSString *)scope andOperationCode:(NSString *)operationCode amdCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:type],@"type",scope,@"scope",operationCode,@"operationCode",categoryID,@"categoryID", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_TOPIC_LIST,@"command",paramDic,@"param" ,nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_TOPIC_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopicListWithType:[NSString stringWithFormat:@"%ld",type] andScope:scope andOperationCode:operationCode amdCategoryID:categoryID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//---点播专题详情
- (void)VODTopicDetailWithTopicCode:(NSString *)topicCode andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:topicCode,@"topicCode", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_TOPIC_DETAIL,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_DETAIL];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopicDetailWithTopicCode:topicCode andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//-----------点播分类
-(void)VODCategoryListWithParentID:(NSString *)parentID andTimeout:(NSInteger)timeout{
    
//    if (parentID.length>0) {
//        paramDic = [NSDictionary dictionaryWithObjectsAndKeys:parentID,@"parentID", nil];
//    }
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:CATEGORY_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_CATEGORY_LIST];
    
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODCategoryListWithParentID:parentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}
//-----------点播运营组分类
-(void)VODOperationCategoryListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic= [NSDictionary dictionaryWithObjectsAndKeys:operationCode,@"operationCode", nil];
//    
//    
//    //    paramDic = [NSDictionary dictionaryWithObjectsAndKeys:operationCode,@"operationCode", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_OPER_CATEGORY_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPER_CATEGORY_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOperationCategoryListWithOperationCode:operationCode andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//--------点播分类运营组...
- (void)VODCategoryOperationListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [[NSDictionary alloc]initWithObjectsAndKeys:categoryID,@"categoryID", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_CATEGORY_OPER_LIST,@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_CATEGORY_OPER_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODCategoryOperationListWithCategoryID:categoryID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//---------1.1	点播分类专题列表
- (void)VODCategoryTopicListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:categoryID,@"categoryID",TERMINAL_TYPE,@"terminal_type", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_CATEGORY_TOPICLIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODCategoryTopicListWithCategoryID:categoryID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
}

//-------------点播节目列表
- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic;
//
//    //3.4中接口  可以为空
//    //    operationCode,@"operationCode",rootCategoryID,@"rootCategoryID",
//    
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
//    //3.7中接口
//    if (operationCode) {
//        [paramDic setObject:operationCode forKey:@"operationCode"];
//    }
//    if (rootCategoryID) {
//        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_PROGRAM_LIST,@"@command",paramDic,@"param", nil];
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PROGRAM_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODProgramListWithOperationCode:operationCode andRootCategpryID:rootCategoryID andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//------供首页用的点播分类下的节目使用仅供 真正
- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index
{
    
    _manager.requestSerializer.timeoutInterval = timeout;
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODProgramListWithOperationCode:operationCode andRootCategpryID:rootCategoryID andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andIndex:[NSString stringWithFormat:@"%ld",index] andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index andProgramListIndexName:(NSString *)programName
{
    
    _manager.requestSerializer.timeoutInterval = timeout;
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODProgramListWithOperationCode:operationCode andRootCategpryID:rootCategoryID andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andIndex:[NSString stringWithFormat:@"%ld",index] andProgramName:programName andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}






//--------点播运营组节目列表
-(void)VODOperationProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",operationCode,@"operationCode",rootCategoryID,@"rootCategoryID",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_OPER_PROGRAM_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_OPER_PROGRAM_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOperationProgramListWithOperationCode:operationCode andRootCategpryID:rootCategoryID andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//******首页的推荐专题获取，需要发送两次数据，标记第几次
- (void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout andIndex:(NSInteger)index
{
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",topicCode,@"topicCode",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
   
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopicProgramListWithTopicCode:topicCode andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andIndex:[NSString stringWithFormat:@"%ld",index] andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
//-----点播专题节目列表
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",topicCode,@"topicCode",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_TOPIC_PROGRAM_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_TOPIC_PROGRAM_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopicProgramListWithTopicCode:topicCode andStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//------------点播搜索
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",key,@"key",[NSNumber numberWithInteger:count],@"count", nil];
//    
//    if (operationCode.length>0) {
//        [paramDic setObject:operationCode forKey:@"operationCode"];
//    }
//    if (rootCategoryID.length>0) {
//        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
//    }
//    if (categoryID.length>0) {
//        [paramDic setObject:categoryID forKey:@"categoryID"];
//    }
//    if (topicCode.length>0) {
//        [paramDic setObject:topicCode forKey:@"topicCode"];
//    }
//    if (startSeq) {
//        [paramDic setObject:[NSNumber numberWithInteger:startSeq] forKey:@"startSeq"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_SEARCH,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_SEARCH];
    
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODSearchWithOperationCode:operationCode andRootCategoryID:rootCategoryID andCategoryID:categoryID andTopicCode:topicCode andKey:key andStarSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}
//NEW  ------------点播搜索
- (void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andOrderType:(NSInteger)orderType andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:count],@"count",[NSNumber numberWithInteger:orderType],@"orderType", nil];
//    if (key.length>0) {
//        [paramDic setObject:key forKey:@"key"];
//    }
//    if (operationCode.length>0) {
//        [paramDic setObject:operationCode forKey:@"operationCode"];
//    }
//    if (rootCategoryID.length>0) {
//        [paramDic setObject:rootCategoryID forKey:@"rootCategoryID"];
//    }
//    if (categoryID.length>0) {
//        [paramDic setObject:categoryID forKey:@"categoryID"];
//    }
//    if (topicCode.length>0) {
//        [paramDic setObject:topicCode forKey:@"topicCode"];
//    }
//    if (startSeq) {
//        [paramDic setObject:[NSNumber numberWithInteger:startSeq] forKey:@"startSeq"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_SEARCH,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_SEARCH];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    
    [vodPortal VODSearchWithOperationCode:operationCode andRootCategoryID:rootCategoryID andCategoryID:categoryID andTopicCode:topicCode andKey:key andStarSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andOrderType:[NSString stringWithFormat:@"%ld",orderType] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
}
//-----------点播视频详情信息
-(void)VODDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_DETAIL,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_DETAIL];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODDetailWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
}

//-------点播播放
-(void)VODPlayWithContentID:(NSString *)contentID andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID",[NSNumber numberWithInteger:[movie_type integerValue]],@"movie_type", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_PLAY,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PLAY];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODPlayWithContentID:contentID andWithMovie_type:movie_type andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//----------点播询价
-(void)VODInquiryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_INQUIRY,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_INQUIRY];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODInquiryWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//---------付费点播
-(void)VODPayPlayWithContentID:(NSString *)contentID andWithLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout{
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID", nil];
//    if (lockPWD.length>0) {
//        [paramDic setObject:lockPWD forKey:@"lockPWD"];
//    }
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_PAY_PLAY,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PAY_PLAY];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODPayPlayWithContentID:contentID andWithLockPWD:lockPWD andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//--------点播停止
-(void)VODStopWithContentID:(NSString *)contentID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",[NSNumber numberWithInteger:breakPoint],@"breakPoint", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_STOP,@"@command", paramDic,@"param",nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_STOP];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODStopWithContentID:contentID andBreakPoint:breakPoint andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//-------点播播放记录列表
-(void)VODPlayListWithContentID:(NSString *)contentID andStarSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
//    if (contentID.length>0) {
//        [paramDic setObject:contentID forKey:@"contentID"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_PLAY_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PLAY_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODPlayListWithContentID:contentID andStarSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//---------删除点播播放记录
-(void)DELVODPlayHistoryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
//    if (contentID.length>0) {
//        [paramDic setObject:contentID forKey:@"contentID"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:DEL_VOD_PLAY,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:DEL_VOD_PLAY];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal DELVODPlayHistoryWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//--------增加点播收藏
-(void)ADDVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:ADD_VOD_FAVORITE,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:ADD_VOD_FAVORITE];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal ADDVODFavoryteWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//--------删除点播收藏
-(void)DELVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
//    if (contentID.length>0) {
//        [paramDic setObject:contentID forKey:@"contentID"];
//    }
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:DEL_VOD_FAVORITE,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:DEL_VOD_FAVORITE];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal DELVODFavoryteWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//------点播收藏列表
-(void)VODFavoryteListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSMutableDictionary *paramDic;
//    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_FAVORITE_LIST,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_FAVORITE_LIST];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODFavoryteListWithStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}





//--------------点播 推荐 影片关联列表
-(void)VODAssccationListWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] init];
//    if (_token.length>0) {
//        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:10],@"count", nil];
//    }else{
//        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:10],@"count", nil];
//    }
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_ASSCCIATION,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_ASSCCIATION];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODAssccationListWithContentID:contentID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}
//New...
- (void)VODAssccationListWithContentID:(NSString *)contentID andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [[NSDictionary alloc] init];
//    if (!count) {
//        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:10],@"count", nil];
//    }else{
//        paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",contentID,@"contentID",TERMINAL_TYPE,@"terminal_type",[NSNumber numberWithInteger:count],@"count", nil];
//    }
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_ASSCCIATION,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_ASSCCIATION];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODAssccationListWithContentID:contentID andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//----------------点播热门搜索关键词
- (void)VODTopSearchKeyWithKey:(NSString *)key andCount:(NSInteger)count andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic;
//    if (count !=0) {
//        paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:count],@"count",TERMINAL_TYPE,@"terminal_type", nil];
//    }else
//    {
//        paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:10],@"count",TERMINAL_TYPE,@"terminal_type", nil];
//    }
//    if (key.length>0) {
//        [paramDic setObject:key forKey:@"key"];
//    }
//    
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_TOP_SEARCH_KEY,@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_TOP_SEARCH_KEY];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODTopSearchKeyWithKey:key andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//调拨销售品订购
- (void)VODOrderProductWithProductID:(NSString *)productId andLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic;
//    if (lockPWD) {
//        paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token",lockPWD,@"lockPWD", nil];
//    }else
//    {
//        //如果空
//        paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_token,@"token", nil];
//    }
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_Order_Product,@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval  = timeout;
    
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_Order_Product];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOrderProductWithProductID:productId andLockPWD:lockPWD andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//点播吐槽影片
- (void)VODPraiseDegradeWithContentID:(NSString *)contentID andType:(NSInteger)type andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_token, @"token",contentID,@"contentID",@(type),@"type", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_Praise_Degrade,@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_Praise_Degrade];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODPraiseDegradeWithContentID:contentID andType:[NSString stringWithFormat:@"%ld",type] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
//2.25 运营组销售品列表
- (void)VODOperationProductListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_token, @"token",TERMINAL_TYPE,@"terminal_type",operationCode,@"operationCode", nil];
//    
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_Operation_Product_List,@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
    
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_Operation_Product_List];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOperationProductListWithOperationCode:operationCode andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//--------------点播运营组菜单
- (void)VODOPerMenuQWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"TERMINAL_TYPE",operationCode,@"operationCode", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"VOD_OPER_MENU",@"command",paramDic,@"param" ,nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic andType:@"VOD_OPER_MENU"];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOPerMenuQWithOperationCode:operationCode andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//------------点播分类菜单
- (void)VODCategoryMenuWithCategoryID:(NSInteger)categoryID andTimeout:(NSInteger)timeout
{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"TERMINAL_TYPE",categoryID,@"categoryID" ,nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"VOD_CATEGORY_MENU",@"command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic andType:@"VOD_CATEGORY_MENU"];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODCategoryMenuWithCategoryID:categoryID andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)VODOperationDetailWithOperationCode:(NSString *)operationCode andTimeOut:(NSInteger)timeout{

//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:operationCode,@"operationCode",nil];
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_OperationDetail];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODOperationDetailWithOperationCode:operationCode andTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
-(void)VODNewPlayRecordListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andSatrtDte:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout{

//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count",nil];
//    
//    if (startDate.length>0) {
//        [paramDic setValue:startDate forKey:@"startDate"];
//    }
//    if (endDate.length>0) {
//        [paramDic setValue:endDate forKey:@"endDate"];
//    }
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_NewPlayRecordList];
    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODNewPlayRecordListWithStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andSatrtDte:startDate andEndDate:endDate andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)VODNewFavorateListWithStartSeq:(NSInteger)startSeq andCount:(NSInteger)count andTimeout:(NSInteger)timeout{

//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",[NSNumber numberWithInteger:startSeq],@"startSeq",[NSNumber numberWithInteger:count],@"count",nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_NewFavorateList];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODNewFavorateListWithStartSeq:[NSString stringWithFormat:@"%ld",startSeq] andCount:[NSString stringWithFormat:@"%ld",count] andTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
-(void)VODHomeConfigListWithTimeOut:(NSInteger)timeout{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_HomeConfigList];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODHomeConfigListWithTimeOut:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
-(void)VODMenuConfigListWithTimeout:(NSInteger)timeout{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
//    
    _manager.requestSerializer.timeoutInterval = timeout;
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_MenuConfigList];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODMenuConfigListWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
- (void)VODUpLoadOrderWithOrderId:(NSString *)orderId andPrice:(float)price andContentId:(NSString *)contentId andVodId:(NSString *)vodId andTimeout:(NSInteger)outTime
{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",contentId,@"contentID",orderId,@"orderId",[NSNumber numberWithFloat:price],@"price",nil];
//    if (vodId.length) {
//        [paramDic setObject:vodId forKey:@"vodId"];
//    }
    _manager.requestSerializer.timeoutInterval = outTime;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_UpLoadOrder];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODUpLoadOrderWithOrderId:orderId andPrice:[NSString stringWithFormat:@"%.2f",price] andContentId:contentId andVodId:vodId andTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
}
- (void)VODPraiseDegradeDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)outTime
{
//    if (_token.length==0||!_token) {
//        return;
//    }
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",contentID,@"contentID",nil];
    
    _manager.requestSerializer.timeoutInterval = outTime;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_PraiseDegradeDetail];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODPraiseDegradeDetailWithContentID:contentID andTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
-(void)VODHospitalInfoWithTimeout:(NSInteger)outTime{
    
//    NSString *regionCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"InstanceCode"];
//    if (regionCode.length==0) {
//        regionCode = REGION_CODE;
//    }
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:regionCode,@"code",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = outTime;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_HospitalInfo];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODHospitalInfoWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}
-(void)VODHospitalInfoWithCode:(NSString *)code andTimeout:(NSInteger)outTime{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:code,@"code",TERMINAL_TYPE,@"terminal_type",nil];
    _manager.requestSerializer.timeoutInterval = outTime;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_HospitalInfo];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODHospitalInfoWithCode:code andTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}
-(void)VODFindHospitalWithTimeout:(NSInteger)outTime{
//    NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
//    NSString *longitude = @"114.989792";
//    NSString *latitude  = @"27.116745";
//    if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
//        longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
//        latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
//    }
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",longitude,@"longitude",latitude,@"latitude",nil];
    _manager.requestSerializer.timeoutInterval = 10;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_GetHospital];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODFindHospitalWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}

-(void)VODHospitalListWithTimeout:(NSInteger)outTime{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    _manager.requestSerializer.timeoutInterval = 10;
//    [self postDemandRequestWithDictionary:paramDic andType:VOD_HospitalList];
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODHospitalListWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)VODGetHomepageADImage{

    
    HTVODPortalInterface *vodPortal = [[HTVODPortalInterface alloc]init];
    [vodPortal VODGetHomepageADImageAndReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];

}





#pragma mark ------- UPORTAL (New)

//手机号登陆
-(void)mobileLoginWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout{
    
    _UserName = mobilePhone;
    _passWord = passWord;
    
    _manager.requestSerializer.timeoutInterval = timeout;
    
    NSDictionary *requestDic = [[NSDictionary alloc] init];

    [self postUPotalWithDictionary:requestDic andType:@"mobileLogin"];

}


//游客登录
-(void)guestLoginWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:dev_ID,@"deviceID", nil];
//    
//    NSLog(@"shebeiweiyima----%@",dev_ID);
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"guestLogin",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"guestLogin"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTGuestLoginWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//签到
-(void)signInWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"signIn",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"signIn"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTSignInWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//查询用户今日是否已经签到
- (void)isSignInWithTimeout:(NSInteger)timeout{
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTIsSignInWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//注销
-(void)logoutWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:dev_ID,@"deviceID",_token,@"token", nil];
//    
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"logout",@"@command",paramDic,@"param", nil];
//    
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"logout"];
    
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTLogoutWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

////设备激活
//-(void)deviceActiveWithUserName:(NSString *)userName andPassword:(NSString *)passWord andTimeout:(NSInteger)timeout{
//    
//    _UserName = userName;
//    _passWord = passWord;
//    
//    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:userName,@"userName",passWord,@"passWord",dev_ID,@"deviceID", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"deviceActive",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"deviceActive"];
//    
//    
//}
////设置付费码锁
//-(void)setLockNumberWithOldNumber:(NSString *)oldNumber andNewnumber:(NSString *)newNumber andTimeout:(NSInteger)timeout{
//    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:oldNumber,@"oldNumber",newNumber,@"newNumber",_token,@"token", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"setLockNumber",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"setLockNumber"];
//    
//    
//}
//
////重置付费码锁
//-(void)resetLockNumberWithUserName:(NSString *)userName andPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout{
//    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:userName,@"userName",passWord,@"passWord",_token,@"token", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"resetLockNumber",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"resetLockNumber"];
//    
//    
//}
////查询付费码锁状态
//-(void)isSetLockNumberWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
//    
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"isSetLockNumber",@"@command",paramDic,@"param", nil];
//    
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"isSetLockNumber"];
//    
//}

//查询用户信息
-(void)NewqueryUserInfoWithTimeout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token", nil];
//    
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"queryUserInfo",@"@command",paramDic,@"param", nil];
//    
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"queryUserInfo"];
    
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTQueryUserInfoWithTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//修改用户密码
-(void)NewchangeUserPwdWithOldPassWord:(NSString *)oldPassWord andNewPassWord:(NSString *)newPassWord andTimeout:(NSInteger)timeout{
    
//    _passWord = oldPassWord;
//    
//    _resetPassword = newPassWord;
//    
//
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:_token,@"token",oldPassWord,@"oldPassWord",newPassWord,@"newPassWord", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"changeUserPwd",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    
//    [self postUPotalWithDictionary:paramDic andType:@"changeUserPwd"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTChangeUserPwdWithOldPassWord:oldPassWord withNewPassWord:newPassWord withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//手机验证码
//#warning 区域编码（String）为空默认为“屏聚XX”的公共APP
-(void)NewauthCodeOfPhoneWithType:(NSString *)type andMobilePhone:(NSString *)mobilePhone andTimeout:(NSInteger)timeout{
    
//    NSString *code = KREGION_CODE;
//    
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:code,@"code",type,@"type",mobilePhone,@"mobilePhone", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"authCode"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTAuthCodeWithType:type withInstanceCode:[KInstanceCode length]?KInstanceCode:@"" withMobilePhone:mobilePhone withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//手机号注册
-(void)NewmobileRegisterWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout{
    
    _authCode = authCode;
    _UserName = mobilePhone;
    _passWord = passWord;
    
    
    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"mobileRegister",@"@command",paramDic,@"param", nil];

    _manager.requestSerializer.timeoutInterval = timeout;
    [self postUPotalWithDictionary:paramDic andType:@"mobileRegister"];
  
//    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
//    [uPortal HTMobileRegisterWithMobilePhone:mobilePhone withPassWord:passWord withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}

//忘记密码
-(void)NewresetUserPwdWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout{
    
//    _fogetPWDMobilePhone = mobilePhone;
//    _authCode = authCode;
//    _passWord = passWord;
//    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"resetUserPwd",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"resetUserPwd"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTResetUserPwdWithMobilePhone:mobilePhone withPassWord:passWord withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//更改手机号
-(void)NewalterMobilePhoneWithMobilePhone:(NSString *)mobilePhone andPassWord:(NSString *)passWord andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout{
    
//    _authCode = authCode;
//    _newMobilePhone = mobilePhone;
//    _passWord = passWord;
//    
//    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:passWord,@"passWord",mobilePhone,@"mobilePhone",authCode,@"authCode",_token,@"token", nil];
////    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:@"alterMobilePhone",@"@command",paramDic,@"param", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postUPotalWithDictionary:paramDic andType:@"alterMobilePhone"];
    
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTAlterMobilePhoneWithMobilePhone:mobilePhone withPassWord:passWord withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//头像上传
-(void)NewUploadHeadPhotoWithImageData:(UIImage *)imageFile andWidth:(NSInteger)width andHeight:(NSInteger)height andTimeout:(NSInteger)timeout{
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUploadHeadPhotoWithImgFile:imageFile withWidth:width withHeight:height withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



//修改昵称
-(void)NewUpdateNickNameWithNickName:(NSString *)nickName andTimeout:(NSInteger)timeout{
    
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:nickName,@"nickName",_token,@"token", nil];
//    _manager.requestSerializer.timeoutInterval = timeout;
//    
//    [self postUPotalWithDictionary:paramDic andType:updateNickName];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUpdateNickNameWithNickName:nickName withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

////帮助反馈
//-(void)NewHelpBackWithSuggestionType:(NSString *)suggestionType andSuggestion:(NSString *)suggestion andContact:(NSString *)contact andTimemout:(NSInteger)timeout{
//    NSDictionary *paramDic = [[NSDictionary alloc] initWithObjectsAndKeys:suggestionType,@"suggestionType",suggestion,@"suggestion",contact,@"contact",_token,@"token", nil];
//    
//    _manager.requestSerializer.timeoutInterval = timeout;
//    
//    [self postUPotalWithDictionary:paramDic andType:@"helpBack"];
//    
//}
//
//
////校验昵称
//-(void)checkNickNameWithNickName:(NSString *)nickName andTimeout:(NSInteger)timeout
//{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:nickName,@"nickName", nil];
//    _manager.requestSerializer.timeoutInterval = timeout;
//    
//    [self postUPotalWithDictionary:paramDic andType:@"checkNickName"];
//}


//验证手机验证码
-(void)checkAuthCodeWithMobilePhone:(NSString *)mobilePhone andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout
{
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
//    _manager.requestSerializer.timeoutInterval = timeout;
//    
//    [self postUPotalWithDictionary:paramDic andType:@"checkAuthCode"];
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTCheckAuthCodeWithMobilePhone:mobilePhone withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//修改用户信息
-(void)updateUserInfoWithNickName:(NSString *)nickName andBirthday:(NSString *)birthday andSex:(NSString *)sex andProfession:(NSString *)profession andEducation:(NSString *)education andUserName:(NSString *)userName andTimeout:(NSInteger)timeout
{
    
    //    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token", nil];
    //
    //    if (nickName.length>0) {
    //        [paramDic setValue:nickName forKey:@"nickName"];
    //    }
    //    if (birthday.length>0) {
    //        [paramDic setValue:birthday forKey:@"birthday"];
    //    }
    //    if (sex>0) {
    //
    //        [paramDic setValue:[NSNumber numberWithInteger:sex] forKey:@"sex"];
    //    }
    //    if (profession>0) {
    //        [paramDic setValue:[NSNumber numberWithInteger:profession] forKey:@"profession"];
    //    }
    //    if (education>0) {
    //        [paramDic setValue:[NSNumber numberWithInteger:education] forKey:@"education"];
    //    }
    //    if (userName.length>0) {
    //        [paramDic setValue:userName forKey:@"userName"];
    //    }
    //
    //    _manager.requestSerializer.timeoutInterval = timeout;
    //
    //    [self postUPotalWithDictionary:paramDic andType:@"updateUserInfo"];
    
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUpdateUserInfoWithNickName:nickName withBirthday:birthday withSex:sex withProfession:profession withEducation:education withUserName:userName withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.24	根据identityToken获取鉴权token接口
-(void)UPORTALGetTokenWithTimeout:(NSInteger)outTime{
//    NSString *IDToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"idToken"];
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:IDToken,@"identityToken", nil];
//    
//    [self postUPotalWithDictionary:paramDic andType:UGetToken];
    
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTGetTokenWithTimeout:outTime andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)checkPassWordWithPassWord:(NSString *)passWord andTimeout:(NSInteger)timeout{

//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",passWord,@"passWord", nil];
//    [self postUPotalWithDictionary:paramDic andType:checkPwd];

    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTCheckPwdWithPassWord:passWord withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

-(void)authCodeLoginWithMobilePhone:(NSString *)mobilePhone andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout{
//    NSString *kDeviceID = dev_ID;
//    if (kDeviceID.length == 0) {
//        kDeviceID = @"";
//    }
//    
//    NSString *code = KREGION_CODE;
//    
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:kDeviceID,@"deviceID",mobilePhone,@"mobilePhone",authCode,@"authCode",code,@"code", nil];
//    
//    [self postUPotalWithDictionary:paramDic andType:authCodeLogin];

    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTAuthCodeLoginWithMobilePhone:mobilePhone withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


-(void)authCodeAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone andOldMobileAauthCode:(NSString *)oldMobileAuthCode andAuthCode:(NSString *)authCode andTimeout:(NSInteger)timeout{
    
//    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",mobilePhone,@"mobilePhone",authCode,@"authCode", nil];
//    if (oldMobileAuthCode.length > 0) {
//        [paramDic setObject:oldMobileAuthCode forKey:@"oldMobileAuthCode"];
//    }
//    
//    [self postUPotalWithDictionary:paramDic andType:authCodeAlterMobilePhone];

    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTAuthCodeAlterMobilePhoneWithMobilePhone:mobilePhone withOldMobileAuthCode:oldMobileAuthCode withAuthCode:authCode withTimeout:timeout andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  江西TV用户绑定
 *
 */
- (void)UPORTALBindJxTVWithIdCard:(NSString *)idCard withIdentityNum:(NSString *)identityNum withPhoneNo:(NSString *)phoneNo withRealName:(NSString *)realName withType:(NSInteger)type
{
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTBindJxTVWithIdCard:idCard withIdentityNum:identityNum withPhoneNo:phoneNo withRealName:realName withType:type andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
    
}


/**
 *  江西TV用户解除绑定
 *
 */
- (void)UPORTALUNBindJxTV
{
 
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUNBindJxTVAndReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
    
}


/**
 *  获取江西TV用户绑定信息
 *
 */
- (void)UPORTALUserBindInfoJxTV
{
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUserBindInfoJxTVAndReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
    
}


/**
 *  查询江西TV用户是否绑定
 *
 */
- (void)UPORTALUserBindStatusJxTV
{
    
    HTUPortalInterface *uPortal = [[HTUPortalInterface alloc] init];
    [uPortal HTUserBindStatusJxTVAndReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
    
}


#pragma mark ---------------
-(void)postUPotalWithDictionary:(NSDictionary *)requestDic andType:(NSString *)type {
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,type];
    NSLog(@" 用户信息上传数据%@ ==%@ Type = %@",URLString,requestDic,type);

    if ([type isEqualToString:USER_LOGIN_EX3] || [type isEqualToString:mobileLogin]||[type isEqualToString:mobileRegister]||[type isEqualToString:alterMobilePhone]||[type isEqualToString:resetUserPwd]||[type isEqualToString:changeUserPwd]) {
       
//        URLString  = [NSString stringWithFormat:@"%@%@%@",DEFAULT_SERVER_PATH,DEFAULT_USER_PATH,@"getChallenge"];
        URLString  = [NSString stringWithFormat:@"%@/%@",_UPORTAL,getChallenge];

    }
    
//    [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
    
    NSLog(@"type----%@;_manager.requestSerializer.HTTPRequestHeaders-----%@;_manager.responseSerializer.acceptableContentTypes------------%@",type,_manager.requestSerializer.HTTPRequestHeaders,_manager.responseSerializer.acceptableContentTypes);

    //New API
    [_manager POST:URLString parameters:requestDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (([[responseObject objectForKey:@"ret"] integerValue] == -2) && ([type isEqualToString:signIn]||[type isEqualToString:queryUserInfo]||[type isEqualToString:changeUserPwd]||[type isEqualToString:uploadHeadPhoto]||[type isEqualToString:updateNickName]||[type isEqualToString:helpBack]||[type isEqualToString:updateUserInfo])) {//用户未登录或者登录超时
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];//获取鉴权token
        }
        if ([type isEqualToString:UGetToken]) {
            
            if ([[responseObject objectForKey:@"ret"] integerValue] == 0) {
                
                NSString *token = [responseObject objectForKey:@"token"];
                
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];//存储鉴权token
                
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{//获取身份token
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_ID_TOKEN" object:nil];
            }
        }
        
        if ([type isEqualToString:USER_LOGIN_EX3])
        {
            
            NSString *encryToken = [responseObject objectForKey:@"encryToken"];
            NSLog(@"挑战子 ： %@",encryToken);
            
            NSString *newPassword = [self encryptingPasswordWithEncryToken:encryToken];
            NSString *devID = dev_ID;
            
            NSDictionary *paramDic = @{@"userName":_UserName,@"passWord":newPassword,@"deviceID":devID};
           
            NSLog(@"用户登陆 : %@",paramDic);
            _manager.requestSerializer.timeoutInterval = 10;
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,userLogin];
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:USER_LOGIN_EX3];
                }
            }];
            
            return;
        }
        else if ([type isEqualToString:mobileLogin]){//...ww加手机登录接口
            
            NSString *encryToken = [responseObject objectForKey:@"encryToken"];
            NSLog(@"手机获取挑战字 ： %@",encryToken);
            
            NSString *newPassword = [self encryptingPasswordWithEncryToken:encryToken];

            NSString *devID = dev_ID;
            NSDictionary *paramDic = @{@"mobilePhone":_UserName,@"passWord":newPassword,@"deviceID":devID};
            
            _manager.requestSerializer.timeoutInterval = 10;
            
            NSLog(@"手机号登录 ：%@\n%@",[NSString stringWithFormat:@"%@/%@",_UPORTAL,@"mobileLogin"],paramDic);
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,mobileLogin];

            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@" 手机号登陆 %@",responseObject);
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:mobileLogin];
                }
                
            }];
            //...ww OVER
            return;
        }
        else if ([type isEqualToString:mobileRegister]){//手机号注册
            
            NSLog(@"加密钱的密码：：    %@",_passWord);
            NSString *newPassword = [self md5:_passWord];
            NSLog(@"此时的加密密码是：： %@",newPassword);

            NSDictionary *paramDic = @{@"mobilePhone":_UserName,@"passWord":newPassword,@"authCode":_authCode};
            

            _manager.requestSerializer.timeoutInterval = 10;
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,mobileRegister];

            NSLog(@"用户手机注册 ：%@",paramDic);
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"手机号注册 %@",responseObject);
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:mobileRegister];
                }
                
            }];
            //...ww OVER
            return;

        }else if ([type isEqualToString:alterMobilePhone]){//更改手机号
            
            NSString *newPassword = [self md5:_passWord];
            
            NSDictionary *paramDic = @{@"mobilePhone":_newMobilePhone,@"passWord":newPassword,@"authCode":_authCode,@"token":_token};
        
            _manager.requestSerializer.timeoutInterval = 10;
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,alterMobilePhone];

            NSLog(@"变更手机号 ：%@",paramDic);
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"变更手机号 %@",responseObject);
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:alterMobilePhone];
                }
                
            }];

            return;
            
        }else if ([type isEqualToString:resetUserPwd]){//忘记密码
            
            NSString *newPassword = [self md5:_passWord];

            NSDictionary *paramDic = @{@"mobilePhone":_fogetPWDMobilePhone,@"passWord":newPassword,@"authCode":_authCode};
            
            _manager.requestSerializer.timeoutInterval = 10;
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,resetUserPwd];

            NSLog(@"忘记密码 ：%@",paramDic);
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"忘记密码 %@",responseObject);
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:resetUserPwd];
                }
                
            }];
            
            return;
            
        }else if ([type isEqualToString:changeUserPwd]){//修改密码
            
            NSString *oldPassword = [self md5:_passWord];
            
            NSString *newPassword = [self md5:_resetPassword];
            

            NSDictionary *paramDic = @{@"token":_token,@"oldPassWord":oldPassword,@"newPassWord":newPassword};
            
        
            _manager.requestSerializer.timeoutInterval = 10;
            NSString *URLString = [NSString stringWithFormat:@"%@/%@",_UPORTAL,changeUserPwd];

            NSLog(@"忘记密码 ：%@",paramDic);
            [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"忘记密码 %@",responseObject);
                //请求成功时回调方法
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                    [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:changeUserPwd];
                }
                
            }];
            //...ww OVER
            return;
        }
        
        //请求成功时回调方法
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:type];
        }
    }];
}


#pragma mark - --->ADPORTAL

//-------点播播放+广告
-(void)VODPlayWithContentID:(NSString *)contentID andName:(NSString *)name andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout{
    
    NSMutableDictionary *paramDic;
    paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",TERMINAL_TYPE,@"terminal_type",contentID,@"contentID",[NSNumber numberWithInteger:[movie_type integerValue]],@"movie_type", nil];
//    NSDictionary *requestDic = [NSDictionary dictionaryWithObjectsAndKeys:VOD_PLAY,@"@command",paramDic,@"param", nil];
    _manager.requestSerializer.timeoutInterval = timeout;
//    [self postDemandRequestWithDictionary:paramDic  andType:VOD_PLAY];
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_VODPORTAL,VOD_PLAY];
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *playDic = [NSDictionary dictionaryWithDictionary:responseObject];
        //-------------->>请求广告
        NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"PANGEA_PHONE_VOD_HEAD_PATCH",@"code",@"2",@"type",contentID,@"assetID",name,@"assetName", nil];
        
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:actionInfo forKey:@"actionInfo"];
        [paramDic setObject:userInfo forKey:@"userInfo"];
        
        
        _manager.requestSerializer.timeoutInterval = 10;
        
        NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
        NSLog(@"点播片前 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,AD_INFO);

        [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *adDic = [NSDictionary dictionaryWithDictionary:responseObject];
            
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:VOD_PLAY];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSDictionary *adDic = [NSDictionary dictionaryWithObjectsAndKeys:@"-99",@"ret",error,@"error", nil];
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:VOD_PLAY];
            }

            
        }];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //这里如果写了回调的话 会导致 一个接口两次回调
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:VOD_PLAY];
//        }

    }];

    
}


//直播+广告
-(void)TVPlayWithServiceID:(NSString*)serviceID andServiceName:(NSString *)serviceName andTimeout:(NSInteger)timeout
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:serviceID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    
    _manager.requestSerializer.timeoutInterval = timeout;
   
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_TVPLAY];
    NSLog(@"上传的数据 : %@ URL : %@",paramDic,URLString);
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *playDic = [NSDictionary dictionaryWithDictionary:responseObject];
        //-------------->>请求广告
        NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:PANGEA_PHONE_LIVE_HEAD_PATCH,@"code",AD_LIVETYPE,@"type",serviceID,@"assetID",serviceName,@"assetName", nil];
        
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:actionInfo forKey:@"actionInfo"];
        [paramDic setObject:userInfo forKey:@"userInfo"];
        
        _manager.requestSerializer.timeoutInterval = 10;
        
        NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
        NSLog(@"EPG 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,AD_INFO);
        [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *adDic = [NSDictionary dictionaryWithDictionary:responseObject];
            
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:EPG_TVPLAY];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //请求失败时候，回调方法
            // -1001 请求超时
            NSDictionary *adDic = [NSDictionary dictionaryWithObjectsAndKeys:@"-99",@"ret",error,@"error", nil];
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:EPG_TVPLAY];
            }
        }];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:EPG_TVPLAY];
        }
        
        
    }];

    

    
}






-(void)EPGPlayWithServiceID:(NSString *)serviceID andServiceName:(NSString *)serviceName andEventID:(NSString *)eventID andTimeout:(NSInteger)timeout{

    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:eventID,@"id",TERMINAL_TYPE,@"terminal_type",_token,@"token", nil];
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_EPGPORTAL,EPG_EVENTPLAY];
    NSLog(@"上传的数据 : %@ URL : %@",paramDic,URLString);


    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *playDic = [NSDictionary dictionaryWithDictionary:responseObject];
        //-------------->>请求广告
        NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:PANGEA_PHONE_SOTV_HEAD_PATCH,@"code",AD_LIVETYPE,@"type",serviceID,@"assetID",serviceName,@"assetName", nil];
        
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:actionInfo forKey:@"actionInfo"];
        [paramDic setObject:userInfo forKey:@"userInfo"];
        
        _manager.requestSerializer.timeoutInterval = 10;
        
        NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
        NSLog(@"EPG 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,AD_INFO);

        [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *adDic = [NSDictionary dictionaryWithDictionary:responseObject];
            
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:EPG_EVENTPLAY];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //请求失败时候，回调方法
            // -1001 请求超时
            NSDictionary *adDic = [NSDictionary dictionaryWithObjectsAndKeys:@"-99",@"ret",error,@"error", nil];
            NSDictionary *resultDic = [NSDictionary dictionaryWithObjectsAndKeys:adDic,@"adDic",playDic,@"playDic", nil];
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:resultDic andType:EPG_EVENTPLAY];
            }
        }];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"fail = %@",error);
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:EPG_EVENTPLAY];
        }

        
    }];
    
    

}

-(void)ADUserGroupWithTimeout:(NSInteger)outTime{
    
    if (!IsLogin) {
        _token = @"";
    }
    NSMutableDictionary *paramDic =[NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",nil];
    NSDictionary *devInfo =  [NSDictionary dictionaryWithObjectsAndKeys:dev_ID,@"devID",@"IPHONE",@"devType",[NSString stringWithFormat:@"%d",(int)kDeviceWidth],@"screenWidth",[NSString stringWithFormat:@"%d",(int)KDeviceHeight],@"screenHeight", nil];
    
    [paramDic setObject:devInfo forKey:@"devInfo"];
        
    _manager.requestSerializer.timeoutInterval = KTimeout;
    
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];//申明请求的数据是json类型
    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
    [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
    [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];

    [self ADPortalPostRequestWithDictionary:paramDic andType:AD_USER_GROUP];

}

-(void)ADInfoWithADCode:(NSString *)ADCode andType:(NSString *)type andAssetID:(NSString *)assetID andAssetName:(NSString *)assetName andGroup:(NSString *)groupID andTimeout:(NSInteger)outTime{


    NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:ADCode,@"code",type,@"type",assetID,@"assetID",assetName,@"assetName", nil];
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:actionInfo forKey:@"actionInfo"];
    [paramDic setObject:userInfo forKey:@"userInfo"];
    
    
    _manager.requestSerializer.timeoutInterval = 10;
    
    [self ADPortalPostRequestWithDictionary:paramDic andType:AD_INFO];

}

-(void)ADInfoWithADCode:(NSString *)ADCode andType:(NSString *)type andAssetID:(NSString *)assetID andAssetName:(NSString *)assetName andGroup:(NSString *)groupID andTimeout:(NSInteger)outTime andReturnBlcok:(HTRequest_AD_Return)returnBlock{
    NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:ADCode,@"code",type,@"type",assetID,@"assetID",assetName,@"assetName", nil];
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:actionInfo forKey:@"actionInfo"];
    [paramDic setObject:userInfo forKey:@"userInfo"];
    
    
    _manager.requestSerializer.timeoutInterval = outTime;
    
//    NSString *type = AD_INFO;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
    NSLog(@"暂停 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,AD_INFO);

    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //请求失败时候，回调方法
        // -1001 请求超时
        returnBlock([error code],nil,type);

    }];
    

}

-(void)ADConfigWithTimout:(NSInteger)outTime{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:TERMINAL_TYPE,@"terminal_type",nil];
    
    _manager.requestSerializer.timeoutInterval = 10;
    
    [self ADPortalPostRequestWithDictionary:paramDic andType:AD_CONFIG];

}

#pragma mark ---------------
-(void)ADPortalPostRequestWithDictionary:(NSDictionary *)requestDic andType:(NSString *)type{
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,type];
    NSLog(@"GROUP 广告系统 上传数据%@ ==%@ Type = %@",URLString,requestDic,type);
    
    NSLog(@"KcontentType:%@",_manager.responseSerializer.acceptableContentTypes);
    NSLog(@"request header :%@",_manager.requestSerializer.HTTPRequestHeaders);
    
    [_manager POST:URLString parameters:requestDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        if (_delegate &&[_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:0 andResult:responseObject andType:type];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //请求失败时候，回调方法
        // -1001 请求超时
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:[error code] andResult:nil andType:type];
        }

    }];

    
}


#pragma mark  ---------------------------医院部分----------------------------------

/**
 *  2.1	首页医院logo和名称
 */
//- (void)YJIntroduceDetailWithInstanceCode:(NSString *)instanceCode
//{
//    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
//    [polymedicine YJIntroduceDetailWithInstanceCode:instanceCode withReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
//}
- (void)YJIntroduceDetail
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJIntroduceDetailWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.22	医院列表
 */
- (void)YJIntroduceListWithAreaId:(NSString *)areaId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJIntroduceListWithAreaId:(NSString *)areaId withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.3	科室列表
 */
- (void)YJDepartmentAllListWithInstanceCode:(NSString *)instanceCode withLevel:(NSString *)level
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJDepartmentAllListWithInstanceCode:instanceCode withLevel:level withReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.5	根据楼栋名称查询科室 新版————2.15	根据楼栋名称查询科室
 *
 */
- (void)YJBuildingSearchWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyWord:(NSString *)keyWord withBuildId:(NSString *)buildId withInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJBuildingSearchWithPageNo:pageNo withPageSize:pageSize withKeyWord:keyWord withBuildId:buildId withInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.6	科室搜索
 *
 */
//- (void)YJDepartmentListWithName:(NSString *)name
//{
//    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
//    [polymedicine YJDepartmentListWithName:name andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
//}


/**
 *  2.7	专家列表   新版的2.4
 *
 */
- (void)YJExpertAllListWithInstanceCode:(NSString *)instanceCode WithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withDepId:(NSString *)depId withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJExpertAllListWithInstanceCode:instanceCode WithPageNo:pageNo withPageSize:pageSize withDepId:depId withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.8	专家详情   新版2.5专家明细
 *
 */
- (void)YJExpertDetailWithID:(NSString *)expertID withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJExpertDetailWithID:expertID withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
}


/**
 *  2.9	专家搜索   2.6
 *
 */
- (void)YJExpertByNameListWithInstanceCode:(NSString *)instanceCode withName:(NSString *)name withPageNo:(NSString *)pageNo andPageSize:(NSString *)pageSize withSource:(NSString *)source
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJExpertByNameListWithInstanceCode:instanceCode withName:name withPageNo:pageNo andPageSize:pageSize withSource:source andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.10	根据科室查询医生  2.7
 *
 */
- (void)YJExpertByDeptidListWithDepID:(NSString *)depId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJExpertByDeptidListWithDepID:depId withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.11	热词列表
 *
 */
//- (void)YJHotwordsListWithBusinessType:(NSInteger)businessType
//{
//    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
//    [polymedicine YJHotwordsListWithBusinessType:businessType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
//}


/**
 *  2.12	医讯列表  新2.8	资讯列表
 *
 */
- (void)YJNewsListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    
    [polymedicine YJNewsListWithInstanceCode:instanceCode withPageNo:pageNo withPageSize:pageSize withPlatType:platType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
//    [polymedicine YJNewsListWithInstanceCode:instanceCode withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}

/**
 *  新版增加————2.10	就诊须知【菜单】
 *
 */
- (void)YJVisitMenuWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJVisitMenuWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  新版增加————2.12	名医列表
 *
 */
- (void)YJExpertNominateWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJExpertNominateWithInstanceCode:instanceCode withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  新版增加————2.13	导诊图-大楼列表
 *
 */
- (void)YJBuildingMenuWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJBuildingMenuWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  新版增加————2.14	导诊图-楼层列表
 *
 */
- (void)YJBuildingFloorsWithBuildId:(NSString *)buildId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJBuildingFloorsWithBuildId:buildId withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.14	就诊卡列表   10.1.1	就诊卡列表
 *
 */
- (void)YJPatientListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPatientListWithInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        
    }];
}


/**
 *  2.15	就诊卡详情  10.1.2	就诊卡详情
 *
 */
- (void)YJPatientDetailWithClinicNo:(NSString *)clinicNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPatientDetailWithClinicNo:clinicNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.16	就诊卡绑定  10.1.3	就诊卡绑定
 */
- (void)YJPatientSaveWithName:(NSString *)name withClinicNo:(NSString *)clinicNo withIdentify:(NSString *)identify withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPatientSaveWithName:name withClinicNo:clinicNo withIdentify:identify withInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.17	就诊卡解除绑定
 *
 */
- (void)YJPatientEditWithID:(NSString *)cardID withInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPatientEditWithID:cardID withInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.19	我的关注医生列表  新2.19
 *
 */
- (void)YJCollectionListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCollectionListWithInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  新接口 2.20	关注/取消关注医生
 *
 */
- (void)YJCollectionSaveWithExpertId:(NSString *)expertId withFocusFlag:(NSString *)focusFlag withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCollectionSaveWithExpertId:expertId withFocusFlag:focusFlag withInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.22	关于我们
 *
 */
//- (void)YJAboutDetail
//{
//    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
//    [polymedicine YJAboutDetailWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
//}


/**
 *  2.23	意见反馈  2.21
 *
 */
- (void)YJSuggestionSaveWithOptionContent:(NSString *)optionContent withContent:(NSString *)content withContact:(NSString *)contact withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJSuggestionSaveWithOptionContent:optionContent withContent:content withContact:contact withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.23	推广图片
 *
 */
- (void)YJRegionPopularizeimageWithImageType:(NSString *)imageType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJRegionPopularizeimageWithImageType:imageType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.24	资讯轮播图片
 *
 */
- (void)YJNewsSlideWithInstanceCode:(NSString *)instanceCode withPlatType:(NSString *)platType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJNewsSlideWithInstanceCode:instanceCode withPlatType:platType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
//    [polymedicine YJNewsSlideWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}


/**
 *  2.24	版本菜单配置列表  2.18	版本菜单配置列表
 */
- (void)YJMenuListWithInstanceCode:(NSString *)instanceCode withPosition:(NSString *)position
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJMenuListWithInstanceCode:instanceCode withPosition:position andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.28	就诊导航界面医院电话与交通信息  新版2.17	就诊导航界面医院电话与交通信息(乘车指南)
 *
 */
- (void)YJIntroduceInfoWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJIntroduceInfoWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.25	发送短信   6.1	验证码短信
 */
- (void)YJSmsSendWithInstanceCode:(NSString *)instanceCode withMsgCategory:(NSString *)megCategory withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJSmsSendWithInstanceCode:instanceCode withMsgCategory:megCategory withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.30	报告单查看验证码校验   6.2	验证码校验
 *
 */
- (void)YJReportSmsValidWithRandomNum:(NSString *)randomNum withMobile:(NSString *)mobile withMsgCategory:(NSString *)megCategory
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJReportSmsValidWithRandomNum:randomNum withMobile:mobile withMsgCategory:megCategory andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.26(V1.3)  养生保健分类
- (void)YJHealthCareCategoryWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthCareCategoryWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.27(V1.3)  养生保健列表
- (void)YJHealthCareListWithCategoryId:(NSString *)categoryId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthCareListWithCategoryId:categoryId withPageNo:pageNo withPageSize:pageSize withPlatType:platType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
//    [polymedicine YJHealthCareListWithInstanceCode:instanceCode withCategoryId:categoryId withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}



//2.28(V1.3)  养生保健轮播图片
- (void)YJHealthCareSlideWithCategoryId:(NSString *)categoryId withPlatType:(NSString *)platType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthCareSlideWithCategoryId:categoryId withPlatType:platType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
    
//    [polymedicine YJHealthCareSlideWithInstanceCode:instanceCode withCategoryId:categoryId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}


//2.29(V1.3)  养生保健咨询列表
- (void)YJHealthCareNewsListWithPlatType:(NSString *)platType
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthCareNewsListWithPlatType:platType andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
//    [polymedicine YJHealthCareNewsListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
//        }
//    }];
}


//2.30(V1.3)  医院关联区域列表
- (void)YJIntroduceAreaList
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJIntroduceAreaListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.33(V1.5)  养生保健拉取推荐（单内容推荐）
 *
 */
- (void)YJHealthCarePull
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthCarePullWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  3.1	附近医院
 *
 */
- (void)YJIntroduceNearbyWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJIntroduceNearbyWithLongitude:longitude withLatitude:latitude andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.1	获取视频节目播放地址(原接口3.1)
 *
 */

- (void)YJCdnVideoUrlWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withVideoId:(NSString *)videoId
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoUrlWithInstanceCode:instanceCode withMobile:mobile withVideoId:videoId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.2	获取视频节目分类信息(原接口3.2)
 */
- (void)YJHealthyTypeListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyTypeListWithGlobal:global withInstanceCode:instanceCode withHomeFlag:homeFlag withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//4.3	获取视频节目信息(原接口3.3)
- (void)YJHealthyHealthyVideoListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withTypeId:(NSString *)typeId withOrder:(NSString *)order withYear:(NSString *)year withTagId:(NSString *)tagId
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyHealthyVideoListWithGlobal:global withInstanceCode:instanceCode withPageNo:pageNo withPageSize:pageSize withTypeId:typeId withOrder:order withYear:year withTagId:tagId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.4	视频节目推荐（原接口3.4）
 *
 */
- (void)YJCdnVideoRecommendWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withType:(NSString *)type withMaxCount:(NSString *)maxCount
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoRecommendWithInstanceCode:instanceCode WithVideoId:videoId withType:type withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.5	加入收藏夹（原接口3.5）
 *
 */
- (void)YJCdnVideoAddfavoriteWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoAddfavoriteWithInstanceCode:instanceCode WithVideoId:videoId withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.6	获取收藏夹列表（原接口3.6）
 *
 */
- (void)YJCdnVideoListFavoriteWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoListFavoriteWithInstanceCode:instanceCode withPageNo:pageNo withPageSize:pageSize withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



/**
 *  4.7	删除收藏夹中的记录(原接口3.7)
 *
 */
- (void)YJCdnVideoDelFavoriteWithInstanceCode:(NSString *)instanceCode WithVideoIds:(NSString *)videoIds withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoDelFavoriteWithInstanceCode:instanceCode WithVideoIds:videoIds withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}



/**
 *  4.8	清空收藏夹(原接口3.8)
 *
 */
- (void)YJCdnVideoCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCdnVideoCleanFavoriteWithInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.9	获取视频分类与视频(原接口3.9)
 *
 */
- (void)YJHealthyTypeListVideoWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode WithVideoCount:(NSString *)videoCount
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyTypeListVideoWithGlobal:global withInstanceCode:instanceCode WithVideoCount:videoCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.10	获取轮播节目列表(新增接口)
 */
- (void)YJHealthyTopListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withOrder:(NSString *)order withMaxCount:(NSString *)maxCount
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyTopListWithGlobal:global withInstanceCode:instanceCode withOrder:order withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.11	获取视频发布年份列表(新增接口)
 *
 */
- (void)YJHealthyYearListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withOrder:(NSString *)order
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyYearListWithInstanceCode:instanceCode withTypeId:typeId withOrder:order andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  4.12	获取视频疾病标签列表(新增接口)
 *
 */
- (void)YJHealthyTagListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withYear:(NSString *)year
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyTagListWithInstanceCode:instanceCode withTypeId:typeId withYear:year andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.1	我的费用[挂号费] 9.1.1	我的费用
 *
 */
- (void)YJRegistrationCostListWithInHospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJRegistrationCostListWithInHospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  4.2	我的费用[检查费]  9.1.2	我的费用[检查费]
 *
 */
- (void)YJInspectCostListWithInHospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJInspectCostListWithInHospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  5.1	就诊记录  8.1.1	就诊记录
 *
 */
- (void)YJHospitalRecordListWithInHospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHospitalRecordListWithInHospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  6.1	检验单  7.1.1	检验单
 *
 */
- (void)YJTestBillListWithInHospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJTestBillListWithInHospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  6.2	检验单删除  7.1.2	检验单删除
 *
 */
- (void)YJTestBillDeleteWithID:(NSString *)billID
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJTestBillDeleteWithID:billID andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  6.3	检查单  7.1.3	检查单
 *
 */
- (void)YJCheckBillListWithInHospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCheckBillListWithInHospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  6.4	检查单删除  7.1.4	检查单删除
 *
 */
- (void)YJCheckBillDeleteWithId:(NSString *)checkBillID
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJCheckBillDeleteWithId:checkBillID andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.1	获取医生排班日期列表  5.1	获取医生排班日期列表.
 *
 */
- (void)YJAppointmentTimeLineTimeLineDateWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentTimeLineTimeLineDateWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.2	获取医生排班汇总信息 5.2	获取医生排班汇总信息
 *
 */
- (void)YJAppointmentTimeLineExpertsSummaryWithExpertID:(NSString *)expertId withInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentTimeLineExpertsSummaryWithExpertID:expertId withInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.3	获取医生排班日信息列表      5.3
 *
 */
- (void)YJAppointmentTimeLineExpertDailyWithInstanceCode:(NSString *)instanceCode withExpertid:(NSString *)expertId withDate:(NSString *)date withMiddy:(NSString *)middy
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentTimeLineExpertDailyWithInstanceCode:instanceCode withExpertid:expertId withDate:date withMiddy:middy andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.4	获取科室医生排班汇总信息列表   5.4	获取科室医生排班汇总信息列表
 *
 */
- (void)YJAppointmentTimeLineDepartmentSummaryWithDepartmentId:(NSString *)departmentId withDate:(NSString*)date withInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentTimeLineDepartmentSummaryWithDepartmentId:departmentId withDate:date withInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.5	预约挂号   5.5	预约挂号
 *
 */
- (void)YJAppointmentOrderCreateWithIdentify:(NSString *)identify withExpertId:(NSString *)expertId withTimeLineId:(NSString *)timeLineId withTimeLineDate:(NSString *)timeLineDate withPatientName:(NSString *)patientName withPatientClinicNo:(NSString *)patientClinicNo withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentOrderCreateWithIdentify:identify withExpertId:expertId withTimeLineId:timeLineId withTimeLineDate:timeLineDate withPatientName:patientName withPatientClinicNo:patientClinicNo withInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.6	获取挂号订单列表   5.6	获取挂号订单列表
 *
 */
- (void)YJAppointmentOrderListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    
    [polymedicine YJAppointmentOrderListWithInstanceCode:instanceCode withPageNo:pageNo withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.7	预约挂号取消   5.7
 *
 */
- (void)YJAppointmentOrderCancelWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentOrderCancelWithInstanceCode:instanceCode withOrderId:orderId withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.8	删除预约挂号订单  5.8
 *
 */
- (void)YJAppointmentOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentOrderRemoveWithInstanceCode:instanceCode withOrderIds:orderIds withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.9	获取检查订单列表  5.9	获取检查订单列表
 *
 */
- (void)YJAppointmentCheckOrderListWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentCheckOrderListWithPageNo:pageNo withPageSize:pageSize withInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.10	删除检查订单  5.10	删除检查订单
 *
 */
- (void)YJAppointmentCheckOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentCheckOrderRemoveWithInstanceCode:instanceCode withOrderIds:orderIds withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  7.11	获取预约挂号与检查订单明细  5.11	获取预约挂号与检查订单明细
 *
 */
- (void)YJAppointmentOrderDetailWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMsgBsy:(NSString *)msgBsy withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJAppointmentOrderDetailWithInstanceCode:instanceCode withOrderId:orderId withMsgBsy:msgBsy withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  8.2	消息推送绑定
 *
 */
- (void)YJPushTagBindWithChannelId:(NSString *)channelId withDeviceType:(NSString *)deviceType withBindBsy:(NSString *)bindBsy
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPushTagBindWithChannelId:channelId withDeviceType:deviceType withBindBsy:bindBsy andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  8.3	消息详情
 *
 */
- (void)YJPushMsgDetailWithInhospitalNo:(NSString *)inHospitalNo
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJPushMsgDetailWithInhospitalNo:inHospitalNo andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  9.1	排队叫号查询
 *
 */
- (void)YJTreatmentFindWithMobile:(NSString *)mobile withName:(NSString *)name
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJTreatmentFindWithMobile:mobile withName:name andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  9.2	排队叫号通知完成
 *
 */
- (void)YJTreatmentCalledWithQueueId:(NSString *)queueId
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJTreatmentCalledWithQueueId:queueId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.1	获取子频道列表
- (void)YJHealthyVodGetChildChannelWithInstanceCode:(NSString *)instanceCode withParentId:(NSString *)parentId withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount {
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodGetChildChannelWithInstanceCode:instanceCode withParentId:parentId withHomeFlag:homeFlag withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.2	获取频道信息
- (void)YJHealthyVodChannelInfoWithChannelId:(NSString *)channelId withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount {
    
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodChannelInfoWithChannelId:channelId withInstanceCode:instanceCode withHomeFlag:homeFlag withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.3	获取频道过滤器列表
- (void)YJHealthyVodChannelFiltersWithChannelId:(NSString *)channelId withIncludeYear:(NSString *)includeYear {
    
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodChannelFiltersWithChannelId:channelId withIncludeYear:includeYear andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.4	获取子频道节目列表
- (void)YJHealthyVodChannelProgramListWithChannelId:(NSMutableArray *)channelIdArr withMaxCount:(NSString *)maxCount {
    
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodChannelProgramListWithChannelId:channelIdArr withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.5	获取节目列表
- (void)YJHealthyVodProgramListWithChannelId:(NSString *)channelId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyword:(NSString *)keyWord withFilter:(NSMutableArray *)filterArr {
    
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodProgramListWithChannelId:channelId withPageNo:pageNo withPageSize:pageSize withKeyword:keyWord withFilter:filterArr andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.6	获取节目视频播放地址
- (void)YJHealthyVodMediaPlayListWithProgramId:(NSString *)programId withMobile:(NSString *)mobile withInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodMediaPlayListWithProgramId:programId withMobile:mobile withInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.7	推荐节目
- (void)YJHealthyVodRecommendProgramWithProgramId:(NSString *)programId withMaxCount:(NSString *)maxCount
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodRecommendProgramWithProgramId:programId withMaxCount:maxCount andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.8	获取收藏夹中节目
- (void)YJHealthyVodListFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodListFavoriteWithInstanceCode:instanceCode withMobile:mobile withPageNo:pageNo withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.9	节目加入收藏夹
- (void)YJHealthyVodAddFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramId:(NSString *)programId
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodAddFavoriteWithInstanceCode:instanceCode withMobile:mobile withProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.10	删除收藏夹中的节目
- (void)YJHealthyVodDelFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramIds:(NSString *)programIds
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodDelFavoriteWithInstanceCode:instanceCode withMobile:mobile withProgramIds:programIds andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.11	清空收藏夹
- (void)YJHealthyVodCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodCleanFavoriteWithInstanceCode:instanceCode withMobile:mobile andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.14    获取一级频道
- (void)YJHealthyVodGetTopChannelWithInstanceCode:(NSString *)instanceCode
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodGetTopChannelWithInstanceCode:instanceCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.15    获取精选频道列表
- (void)YJHealthyGetSelectedChannel
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyGetSelectedChannelWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


//2.16    获取热门频道与节目列表
- (void)YJHealthyVodGetHotChannel
{
    HTPolymedicineInterface *polymedicine = [[HTPolymedicineInterface alloc] init];
    [polymedicine YJHealthyVodGetHotChannelWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


#pragma mark------------jxtvBase
/**
 *  1.1	问题列表接口
 */
- (void)JXBaseProblemProblemList
{
    HTJXBaseInterface *jxtvBase = [[HTJXBaseInterface alloc] init];
    [jxtvBase JXBaseProblemProblemListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  1.1	根据ID获取问题解答
 */
- (void)JXBaseProblemProblemById
{
    HTJXBaseInterface *jxtvBase = [[HTJXBaseInterface alloc] init];
    [jxtvBase JXBaseProblemProblemByIdWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  1.1	查询江西省下的所有市区
 */
- (void)JXBaseBusinessCityList
{
    HTJXBaseInterface *jxtvBase = [[HTJXBaseInterface alloc] init];
    [jxtvBase JXBaseBusinessCityListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  1.1	查询市区所有的营业厅
 */
- (void)JXBaseBusinessBusinessByCityId
{
    HTJXBaseInterface *jxtvBase = [[HTJXBaseInterface alloc] init];
    [jxtvBase JXBaseBusinessBusinessByCityIdWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  1.1	报装接口
 */
- (void)JXBaseBusinessInstallInfoWithName:(NSString *)name withTel:(NSString *)tel withAddress:(NSString *)address withDescription:(NSString *)description
{
    HTJXBaseInterface *jxtvBase = [[HTJXBaseInterface alloc] init];
    [jxtvBase JXBaseBusinessInstallInfoWithName:name withTel:tel withAddress:address withDescription:description withReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


#pragma mark - ------COMMON PORTAL app基础接口
//2.1	系统时间
- (void)CNCommonSystemTime
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonSystemTimeWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.2	终端软件下载
- (void)CNCommonSoftwareDownWithPackageName:(NSString *)packageName withVersionCode:(NSInteger)versionCode withVersionName:(NSString *)versionName
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonSoftwareDownWithPackageName:packageName withVersionCode:versionCode withVersionName:versionName andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

// 2.3	轮播图列表
- (void)CNCommonSlideshowList
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonSlideshowListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        NSLog(@"1号分支------%@",result);

        NSDictionary *dictionSlide = result;//epg广告
        NSMutableArray *SlideGuangGaoarray = [NSMutableArray array];
        NSArray *AAA =[result objectForKey:@"slideshowList"];
        SlideGuangGaoarray.array = AAA;
  
        NSMutableDictionary *actionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"PANGEA_PHONE_HOMEPAGE_CAROUSEL_IMAGE",@"code",@"0",@"type",@"",@"assetID",@"",@"assetName", nil];
        
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:kGroupID,@"groupID",_token,@"token", nil];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:actionInfo forKey:@"actionInfo"];
        [paramDic setObject:userInfo forKey:@"userInfo"];
        
        NSString *URLString = [NSString stringWithFormat:@"%@/%@",_ADPORTAL,AD_INFO];
        
        if (_ADPORTAL.length == 0 || !SUPPORT_AD) {
            
            NSLog(@"3号分支");
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:dictionSlide andType:@"HomeADSLiderPage"];
                return ;
            }
            
        }
        
        NSLog(@" 广告系统 上传数据%@ ==%@ Type = %@",URLString,paramDic,type);
        if (!SUPPORT_AD) {
            //这里是可以直接返回paramDic的
        }
        _manager.requestSerializer.timeoutInterval = kTIME_TEN;
        [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([[responseObject objectForKey:@"ret"] integerValue] == -2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
            }
            
            NSLog(@"2号分支");
            NSMutableArray *array1 = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"adList"]];
            for (NSDictionary *Dic in array1) {
                NSDictionary *pictureRes = [Dic objectForKey:@"pictureRes"];
                NSString *imageLink = [pictureRes objectForKey:@"picLink"];
                NSString *urlLink = [pictureRes objectForKey:@"urlLink"];
                //                    NSString *ID = [Dic objectForKey:@"id"];
                //                    NSString *duration = [pictureRes objectForKey:@"duration"];
                //                    NSString *style = [pictureRes objectForKey:@"0"];
                //                    NSString *type = [pictureRes objectForKey:@"6"];
                NSMutableDictionary *diction = [NSMutableDictionary dictionary];
                if (urlLink.length > 0) {
                    [diction setObject:urlLink forKey:@"urlLink"];
                }
                if ((imageLink.length > 0)) {
                    [diction setObject:imageLink forKey:@"imageLink"];
                    
                    [diction setObject:@"0" forKey:@"seq"];
                    [diction setObject:@"7" forKey:@"type"];
                    [SlideGuangGaoarray addObject:diction];
                }
                
            }
            NSDictionary *resp = dictionSlide;
            NSMutableDictionary *deepDiction = [resp mutableCopy];
            NSMutableArray *guanggaoSlide = [deepDiction objectForKey:@"slideshowList"];
            guanggaoSlide = [NSMutableArray arrayWithArray:SlideGuangGaoarray];
            
            [deepDiction setObject:SlideGuangGaoarray forKey:@"slideshowList"];
            
            if (_delegate &&[_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:deepDiction andType:@"HomeADSLiderPage"];
                return ;
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //请求失败时候，回调方法
            // -1001 请求超时
            
            NSLog(@"3号分支");
            if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
                [_delegate HTRequestFinishedWithReturnStatus:0 andResult:dictionSlide andType:@"HomeADSLiderPage"];
                return ;
            }
            
        }];

//        
//        
//        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:RequestType];
//        }
    }];
    
}

// 2.4	帮助反馈
- (void)CNCommonSuggestWithSuggestionType:(NSString *)suggestionType withSuggestion:(NSString *)suggestion withContact:(NSString *)contact
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonSuggestWithSuggestionType:suggestionType withSuggestion:suggestion withContact:contact andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

// 2.5	广告页列表
- (void)CNCommonAdList
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonAdListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.6  产品名称
- (void)CNCommonAppName
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonAppNameWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.7  产品信息
- (void)CNCommonAppInfo
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonAppInfoWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

// 2.8	产品配置
- (void)CNCommonAppConfig
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonAppConfigWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        NSLog(@"CN_COMMON_APPCONFIG:%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue] == 0) {
            NSDictionary *dic = [result objectForKey:@"appConfig"];
            
            NSString *aboutusurl = [dic objectForKey:@"aboutusurl"];
            NSString *agreementurl = [dic objectForKey:@"agreementurl"];
            NSString *shareurl = [dic objectForKey:@"shareurl"];
            NSString *healthshareurl = [dic objectForKey:@"healthshareurl"];
            
            if (aboutusurl.length == 0) {
                aboutusurl = @"";
            }
            
            if (agreementurl.length == 0) {
                agreementurl = @"";
            }
            
            if (shareurl.length == 0) {
                shareurl = @"";
            }
            
            if (healthshareurl.length == 0) {
                healthshareurl = @"";
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:aboutusurl forKey:@"aboutusurl"];
            [[NSUserDefaults standardUserDefaults] setObject:agreementurl forKey:@"agreementurl"];
            [[NSUserDefaults standardUserDefaults] setObject:shareurl forKey:@"shareurl"];
            [[NSUserDefaults standardUserDefaults] setObject:healthshareurl forKey:@"healthshareurl"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.9	首页模块列表
- (void)CNCommonModuleConfig
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNCommonModuleConfigWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//3.1	实体列表
- (void)CNInstanceInstanceListWithCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNInstanceInstanceListWithCurrentPage:currentPage withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//3.2	实体信息
- (void)CNInstanceInstanceInfo
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc] init];
    [common CNInstanceInstanceInfoWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//3.3	实体定位
- (void)CNInstanceInstanceLocationWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNInstanceInstanceLocationWithLongitude:longitude withLatitude:latitude andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//4.1	运营商信息

- (void)CNOperatorOperatorInfo
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNOperatorOperatorInfoWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
     }];
    
}

//5.1	短信模版
- (void)CNInnerSmsTempleteWithType:(NSUInteger)type
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNInnerSmsTempleteWithType:type andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//5.2	产品列表
- (void)CNInnerAppList
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNInnerAppListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//5.3 message信息
- (void)CNInnerMessageInfoWithProjectCode:(NSString *)projectCode withErrorCode:(NSString *)errorCode
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNInnerMessageInfoWithProjectCode:projectCode withErrorCode:errorCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//5.5 首页底部图
- (void)CNCommonBottomImage
{
    
    HTCommonInterface *common = [[HTCommonInterface alloc]init];
    [common CNCommonBottomImageWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


#pragma mark - ------PUB PORTAL pub基础接口
//2.1	导航频道列表
- (void)PubCommonNavigationChannelListWithCategory:(NSString *)category
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonNavigationChannelListWithCategory:category andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.2	首页推荐列表频道
- (void)PubCommonHomePageChannelListWithCategory:(NSString *)category
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonHomePageChannelListWithCategory:category andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.3	节目列表
- (void)PubCommonProgramInfoListWithChannelId:(NSString *)channelId withCurrentPage:(NSInteger)currentPage withPageSize:(NSInteger)pageSize
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonProgramInfoListWithChannelId:channelId withCurrentPage:currentPage withPageSize:pageSize andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.4	节目播放链接
- (void)PubCommonProgramInfoPlayUrltWithProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonProgramInfoPlayUrltWithProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.5	节目收藏
- (void)PubCommonInsertFavoritesListWithCategory:(NSString *)category withProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonInsertFavoritesListWithCategory:category withProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.6	节目点赞
- (void)PubCommonInsertPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonInsertPraiseWithCategory:category withProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.7	查询是否收藏
- (void)PubCommonIsOrNotFavoritesWithProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonIsOrNotFavoritesWithProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.8	查询是否点赞
- (void)PubCommonIsOrNotPraiseWithProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonIsOrNotPraiseWithProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.9	收藏列表
- (void)PubCommonFavoritesInfoListWithCategory:(NSString *)category
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonFavoritesInfoListWithCategory:category andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.10 点赞列表
- (void)PubCommonPraiseInfoListWithCategory:(NSString *)category
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonPraiseInfoListWithCategory:category andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.11 取消节目点赞
- (void)PubCommonCancelPraiseWithCategory:(NSString *)category withProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonCancelPraiseWithCategory:category withProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.12 取消节目收藏
- (void)PubCommonCancelFavoritesWithCategory:(NSString *)category withProgramId:(NSString *)programId
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonCancelFavoritesWithCategory:category withProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

//2.14 获取点播节目周排行榜
- (void)PubCommonVodRankByWeekWithWeekly:(NSString *)weekly
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonVodRankByWeekWithWeekly:weekly andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

//2.15 获取关键词搜索结果
- (void)PubCommonGetResultByKeywordWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKey:(NSString *)key
{
    
    HTPubInterface *pub = [[HTPubInterface alloc]init];
    [pub PubCommonGetResultByKeywordWithPageNo:pageNo withPageSize:pageSize withKey:key andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

#pragma mark -----------UGC_PORTAL
/**
 *  2.1	判断是否合法主播
 *
 */
- (void)UGCCommonIsValidUser
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonIsValidUserWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


/**
 *  2.2 身份认证
 *
 */
- (void)UGCCommonIdentificationWithIdNumber:(NSString *)idNumber andName:(NSString *)name andMobile:(NSString *)mobile andHospital:(NSString *)hospital andDepartment:(NSString *)department andAcademicTitle:(NSString *)academicTitle andPhysicianType:(NSString *)physicianType andPhysicianCode:(NSString *)physicianCode andOfficialCard:(UIImage *)officialCard andChestCard:(UIImage *)chestCard
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonIdentificationWithIdNumber:idNumber andName:name andMobile:mobile andHospital:hospital andDepartment:department andAcademicTitle:academicTitle andPhysicianType:physicianType andPhysicianCode:physicianCode andOfficialCard:officialCard andChestCard:chestCard andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.3 开始播放
 *
 */
- (void)UGCCommonStartLiveWithName:(NSString *)name andScreenFlg:(NSString *)screenFlg andChannelId:(NSString *)channelId
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonStartLiveWithName:name andScreenFlg:screenFlg andChannelId:channelId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.4 停止播放
 *
 */
- (void)UGCCommonStopLiveWithSessionId:(NSString *)sessionId
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonStopLiveWithSessionId:sessionId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
//            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.5 点播节目上传
 *
 */
- (void)UGCCommonInsertProgramWithVideoId:(NSString *)videoId andName:(NSString *)name andThumbnaill:(NSString *)thumbnaill andCover:(NSString *)cover andPictureFile:(UIImage *)pictureFile andSize:(NSString *)size andIsEncrypt:(NSString *)isEncrypt andCategoryId:(NSString *)categoryId andCategory:(NSString *)category andStatus:(NSString *)status andRemarks:(NSString *)remarks andUrl:(NSString *)url andTags:(NSString *)tags andDuration:(NSString *)duration
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonInsertProgramWithVideoId:videoId andName:name andThumbnaill:thumbnaill andCover:cover andPictureFile:pictureFile andSize:size andIsEncrypt:isEncrypt andCategoryId:categoryId andCategory:category andStatus:status andRemarks:remarks andUrl:url andTags:tags andDuration:duration andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.6 点播节目修改
 *
 */
- (void)UGCCommonUpdateProgramWithProgramId:(NSString *)programId andName:(NSString *)name andCover:(UIImage *)cover andCategoryId:(NSString *)categoryId andTags:(NSString *)tags andRemarks:(NSString *)remarks
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonUpdateProgramWithProgramId:programId andName:name andCover:cover andCategoryId:categoryId andTags:tags andRemarks:remarks andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.7	用户获取自己的点播列表
 *
 */
- (void)UGCCommonGetProgramListByUserInfo
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetProgramListByUserInfoWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.8	获取直播列表
 *
 */
- (void)UGCCommonGetChannelList
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetChannelListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.9	获取点播列表
 *
 */
- (void)UGCCommonGetProgramList
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetProgramListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.10	获取标签列表
 *
 */
- (void)UGCCommonGetTagList
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetTagListWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.11 删除视频
 *
 */
- (void)UGCCommonDeleteProgramWithProgramId:(NSString *)programId andIsCleanedOut:(NSString *)isCleanedOut
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonDeleteProgramWithProgramId:programId andIsCleanedOut:isCleanedOut andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.12 获取分类列表
 *
 */
- (void)UGCCommonGetCategoryListWithUserId:(NSString *)userId
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetCategoryListWithUserId:userId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.13 直播播放
 *
 */
- (void)UGCCommonGetLiveUrlWithChannelId:(NSString *)channelId
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetLiveUrlWithChannelId:channelId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.14 点播播放
 *
 */
- (void)UGCCommonGetProgramUrlWithProgramId:(NSString *)programId
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetProgramUrlWithProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


/**
 *  2.15 当虹云回调接口
 *
 */
- (void)UGCCommonUgcCallBackWithVideoId:(NSString *)videoId andStatus:(NSString *)status andPlayCode:(NSString *)playCode
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonUgcCallBackWithVideoId:videoId andStatus:status andPlayCode:playCode andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.16	获取token接口
 *
 */
- (void)UGCCommonGetToken
{
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetTokenWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}

/**
 *  2.17	获取医生职称接口
 *
 */
- (void)UGCCommonGetAcademicTitleWithReturn
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetAcademicTitleWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


/**
 *  2.18	获取点播播放次数接口
 *
 */
- (void)UGCCommonGetProgramPlayCountWithProgramId:(NSString *)programId
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetProgramPlayCountWithProgramId:programId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


/**
 *  2.19	获取直播在线人数接口
 *
 */
- (void)UGCCommonGetChannelCountWithChannelId:(NSString *)channelId
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetChannelCountWithChannelId:channelId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


/**
 *  2.20	获取栏目信息接口
 *
 */
- (void)UGCCommonGetColumnInfoWithReturn
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetColumnInfoWithReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


/**
 *  2.21	获取栏目内容信息接口
 *
 */
- (void)UGCCommonGetColumnInfoContentWithColumnInId:(NSString *)columnInId
{
    
    HTUGCInterface *ugcPortal = [[HTUGCInterface alloc]init];
    [ugcPortal UGCCommonGetColumnInfoContentWithColumnInId:columnInId andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}


#pragma mark -----------Msg_NotificatonInterface

-(void)getMobileMessageGetGroupsWithBusSystem:(NSString*)busSystem
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
    [msgInter getMobileMessageGetGroupsWithBusSystem:busSystem andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
}



-(void)getMobileMessageBindGoupWithChannelID:(NSString*)channelID withGroupID:(NSString*)groupID withDeviceType:(NSString*)deviceType withBusSystem:(NSString*)busSystem
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
        
    [msgInter getMobileMessageBindGoupWithChannelID:channelID withGroupID:groupID withDeviceType:deviceType withBusSystem:busSystem andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }

    }];
    
    
}


-(void)getMobileMessageToGroupsWithMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withGroupIds:(NSString*)groupIds withPushTime:(NSString*)pushTime withMsgType:(NSString*)msgType andExtra:(NSString*)extra
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
    [msgInter getMobileMessageToGroupsWithMsgTitle:msgTitle withMsgBody:msgBody withGroupIds:groupIds withPushTime:pushTime withMsgType:msgType andExtra:extra andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
}


-(void)getMobileMessageUNBindGoupWithChannelId:(NSString*)channelId withGroupId:(NSString*)groupId
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
      [msgInter getMobileMessageUNBindGoupWithChannelId:channelId withGroupId:groupId andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
    
}

-(void)getMobileMessageToSingleDeviceWithUserId:(NSString*)userId withMobile:(NSString*)mobile withMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withDeviceType:(NSString*)deviceType withChannelId:(NSString*)channelId withMsgType:(NSString*)msgType withBusSystem:(NSString*)busSystem withExtra:(NSString*)extra
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
    [msgInter getMobileMessageToSingleDeviceWithUserId:userId withMobile:mobile withMsgTitle:msgTitle withMsgBody:msgBody withDeviceType:deviceType withChannelId:channelId withMsgType:msgType withBusSystem:busSystem withExtra:extra andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
    }];
}


-(void)getMobileMessageBindingUpdateWithChannelId:(NSString*)channelId withMobile:(NSString*)mobile
{
    HTNotificationInterface *msgInter = [[HTNotificationInterface alloc]init];
    [msgInter getMobileMessageBindingUpdateWithChannelId:channelId withMobile:mobile andReturnBlock:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        if (_delegate && [_delegate respondsToSelector:@selector(HTRequestFinishedWithReturnStatus:andResult:andType:)]) {
            
            [_delegate HTRequestFinishedWithReturnStatus:status andResult:result andType:type];
        }
        
    }];
}




#pragma mark ------- 工具方法
+(NSString *)getFormOrAfterDateWithDays:(NSInteger )days{
    
    NSDateFormatter *mydateformat = [[NSDateFormatter alloc] init];
    [mydateformat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *mydate = [NSDate date];
    
    if (days>=0) {
        mydate = [mydate dateByAddingDays:days];
        
    }else{
        
        mydate = [mydate dateBySubtractingDays:-days];
    }
    
    NSString *mydateStr= [mydateformat stringFromDate:mydate];
    return mydateStr;
}
- (NSString *)encryptingPasswordWithEncryToken:(NSString *)encry{
    
        return [self md5:[NSString stringWithFormat:@"%@%@", [self md5:_passWord], encry]];
}

- (NSString *)md5:(NSString *)str{
    if (str.length<=0) {
        return @"";
    }
        const char *cStr = [str UTF8String];
        unsigned char result[16];
        CC_MD5(cStr, strlen(cStr), result);  // This is the md5 call
        return [NSString stringWithFormat:
                @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                result[0], result[1], result[2], result[3],
                result[4], result[5], result[6], result[7],
                result[8], result[9], result[10], result[11],
                result[12], result[13], result[14], result[15]
                ];
    
}
    // crc32加密算法，返回加密的编码
- (NSString *)crc32WithString:(NSString *)string
    {
        unsigned int i;
        unsigned int crc_table[256] = {0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9,
            0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005, 0x2608edb8,
            0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61, 0x350c9b64, 0x31cd86d3,
            0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7, 0x4593e01e,
            0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
            0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014,
            0x7ddc5da3, 0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57,
            0x91a18d8e, 0x95609039, 0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52,
            0x8664e6e5, 0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81,
            0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d, 0xd4326d90,
            0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
            0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46,
            0xff79a6f1, 0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d,
            0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae, 0x278206ab,
            0x23431b1c, 0x2e003dc5, 0x2ac12072, 0x128e9dcf, 0x164f8078,
            0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4, 0x0808d07d,
            0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
            0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf,
            0x5a5e5b08, 0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4,
            0x44190b0d, 0x40d816ba, 0xaca5c697, 0xa864db20, 0xa527fdf9,
            0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692,
            0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6, 0x99a95df3,
            0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
            0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055,
            0xfef34de2, 0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686,
            0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a, 0x690ce0ee,
            0x6dcdfd59, 0x608edb80, 0x644fc637, 0x7a089632, 0x7ec98b85,
            0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1, 0x46863638,
            0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
            0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42,
            0x32d850f5, 0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91,
            0x0a97ed48, 0x0e56f0ff, 0x1011a0fa, 0x14d0bd4d, 0x19939b94,
            0x1d528623, 0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7,
            0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b, 0xd727bbb6,
            0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
            0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610,
            0xb07daba7, 0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b,
            0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f, 0x8832161a,
            0x8cf30bad, 0x81b02d74, 0x857130c3, 0x5d8a9099, 0x594b8d2e,
            0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b,
            0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
            0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9,
            0x155a565e, 0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082,
            0x0b1d065b, 0x0fdc1bec, 0x3793a651, 0x3352bbe6, 0x3e119d3f,
            0x3ad08088, 0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654,
            0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5,
            0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
            0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673,
            0xfde69bc4, 0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0,
            0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c, 0xafb010b1,
            0xab710d06, 0xa6322bdf, 0xa2f33668, 0xbcb4666d, 0xb8757bda,
            0xb5365d03, 0xb1f740b4};
        
        unsigned int crc = 0xffffffff;
        for (i = 0; i < string.length; i++) {
            crc = crc_table[((crc >> 24) ^ [string characterAtIndex:i]) & 0xff] ^ (crc << 8);
        }
        
        return [NSString stringWithFormat:@"%x", crc];
    }





#pragma 监测网络的可链接性

#pragma --mark GET请求方式

#pragma --mark POST请求方式
/**
 *  @author Kael
 *
 *  @brief 获取混合后的数组
 *  @param channelList  所有频道 包含分类
 *  @param epgListArray 当前后继节目列表
 *  @return 混合数组
 */
-(NSArray *)getLivePageArrayWithChannelList:(NSArray *)channelList andEPGList:(NSArray *)epgListArray{
    NSMutableArray *KserviceinfoArr = [NSMutableArray arrayWithArray:epgListArray];
    NSMutableArray *resultArr = [NSMutableArray array];
    for (int i = 0; i<[channelList count]; i++) {
        //获取分类下的频道列表
        NSMutableArray *typeArr = [[NSMutableArray alloc] initWithArray:[[channelList objectAtIndex:i] objectForKey:@"serviceinfo"]];
        NSMutableArray *KChannelInfo = [NSMutableArray array];
        
        NSMutableDictionary *channelTypedic = [NSMutableDictionary dictionary];
        
        for (int j = 0; j<[typeArr count]; j++) {
            //得到具体的某个频道
            NSDictionary *typeItemsDic = [typeArr objectAtIndex:j];
            NSMutableDictionary *KEPGListDic = [NSMutableDictionary dictionaryWithDictionary:typeItemsDic];
            //            NSMutableDictionary *KEPGListDic = [NSMutableDictionary dictionary];
            
            for (int k = 0; k<[KserviceinfoArr count]; k++) {
                
                //得到某个频道的EPG列表
                NSDictionary *KEPGDic = [KserviceinfoArr objectAtIndex:k];
                
                if ([[KEPGDic objectForKey:@"serviceID"]isEqualToString:[typeItemsDic objectForKey:@"id"]]) {
                    
                    [KEPGListDic setValue:KEPGDic forKey:@"epglist"];
                    
                    //KEPGListDic 这是调整好的那个频道信息
                    //                    [KChannelInfo addObject:KEPGListDic];//这是拼接好的 频道信息数组 在上层字典中对应的Key是serviceinfo
                    
                    break;
                    
                }
                
                //-----------------
            }
            
            [KChannelInfo addObject:KEPGListDic];//这是拼接好的 频道信息数组 在上层字典中对应的Key是serviceinfo
            
            
            if (KChannelInfo) {
                [channelTypedic setObject:KChannelInfo forKey:@"serviceinfo"];
                
            }else{
                
            }
            [channelTypedic setObject:[[channelList objectAtIndex:i] objectForKey:@"type"] forKey:@"type"];
            
            //-------------------------
        }
        
        [resultArr addObject:channelTypedic];
        //----------------------------
    }
    
    return resultArr;
}





@end
