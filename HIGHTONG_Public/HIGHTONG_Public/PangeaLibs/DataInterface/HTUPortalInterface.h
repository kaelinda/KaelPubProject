//
//  HTUPortalInterface.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 16/8/16.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTUPortalInterface_Block)(NSInteger status, NSMutableDictionary *result, NSString *type);

@interface HTUPortalInterface : NSObject


/**
 *  2.2	注册用户登录
 *
 *  @param mobilePhone 手机号（String 不可为空）
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param deviceID    唯一标识（String 不可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTMobileLoginWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;

/**
 *  2.3	游客登录
 *
 *  @param deviceID    设备唯一码（String 不可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTGuestLoginWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;

/**
 *  2.4	用户注销
 *
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTLogoutWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.5	修改用户密码
 *
 *  @param oldPassWord 旧密码（String 不可为空）要求终端加密后传入
 *  @param newPassWord 新密码（String 不可为空）要求终端加密后传入
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTChangeUserPwdWithOldPassWord:(NSString *)oldPassWord withNewPassWord:(NSString *)newPassWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.6	手机验证码
 *
 *  @param type         验证码类型（Integer）0：注册 1：变更（新手机） 2：重置密码 3：无线认证 4：动态口令登录 5：变更（原手机）
 *  @param instanceCode 实体编码（String 可为空）无线认证时用户在短信内容里拼接医院名称，其它类型可不传此参数。
 *  @param mobilePhone  手机号（String 不可为空）
 *  @param timeout      设定超时时间
 *  @param returnBlock  回调block
 */
- (void)HTAuthCodeWithType:(NSString *)type withInstanceCode:(NSString *)instanceCode withMobilePhone:(NSString *)mobilePhone withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.7	手机号注册
 *
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTMobileRegisterWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.8	用户密码重置
 *
 *  @param mobilePhone 手机号(String 不可为空)
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTResetUserPwdWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.9	变更手机号
 *
 *  @param mobilePhone 手机号（String 不可为空）
 *  @param passWord    用户密码（String 不可为空）要求终端加密后传入
 *  @param authCode    验证码（String 不可为空） 需要给新手机发送注册验证码
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone withPassWord:(NSString *)passWord withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.10 验证手机验证码
 *
 *  @param mobilePhone 手机号 (String不可为空)
 *  @param authCode    验证码 (String不可为空)
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTCheckAuthCodeWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.11	获取业务鉴权token
 *
 *  @param identityToken 身份令牌（String 不可为空）
 *  @param deviceID      唯一标识（String 可为空）
 *  @param timeout       设定超时时间
 *  @param returnBlock   回调block
 */
- (void)HTGetTokenWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.12	查询密码是否正确接口
 *
 *  @param passWord    密码（String 不可为空）要求终端加密后传入
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTCheckPwdWithPassWord:(NSString *)passWord withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.13	无线认证（验证+注册）
 *
 *  @param mobilePhone  手机号 （String不可为空）
 *  @param authCode     验证码 （String不可为空）
 *  @param instanceCode 实体编码（String）预留字段，程序暂不处理
 *  @param timeout      设定超时时间
 *  @param returnBlock  回调block
 */
- (void)HTWifiAuthWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withInstanceCode:(NSString *)instanceCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.14	动态口令登录（验证+注册+登录）
 *
 *  @param mobilePhone 手机号（String不可为空）
 *  @param authCode    验证码（String不可为空）
 *  @param deviceID    唯一标识（String 可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTAuthCodeLoginWithMobilePhone:(NSString *)mobilePhone withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.15	动态口令变更手机号
 *
 *  @param mobilePhone       手机号(String 不可为空)
 *  @param oldMobileAuthCode 验证码（String 不可为空） 需要给原手机发送注册验证码
 *  @param authCode          验证码（String 不可为空） 需要给新手机发送注册验证码
 *  @param timeout           设定超时时间
 *  @param returnBlock       回调block
 */
- (void)HTAuthCodeAlterMobilePhoneWithMobilePhone:(NSString *)mobilePhone withOldMobileAuthCode:(NSString *)oldMobileAuthCode withAuthCode:(NSString *)authCode withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.16	获取临时密钥
 *
 *  @param mobilePhone 手机号（String 不可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTGetTemporalKeyWithMobilePhone:(NSString *)mobilePhone withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  2.17	临时密钥登录（验证+注册+登录）
 *
 *  @param mobilePhone 手机号 （String不可为空）
 *  @param key         临时密钥 （String不可为空）调用获取临时密钥接口后需要解密再去掉前面的手机号再传入，使用前需要比对手机号。
 *  @param deviceID    唯一标识（String 可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTTemporalKeyLoginWithMobilePhone:(NSString *)mobilePhone withKey:(NSString *)key withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;

/**
 *  3.1	用户签到
 *
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTSignInWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  3.2	查询用户今日是否已经签到
 *
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTIsSignInWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  3.3	修改昵称
 *
 *  @param nickName    昵称名称 (String不可为空)
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTUpdateNickNameWithNickName:(NSString *)nickName withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  3.4	头像上传
 *
 *  @param imgFile     图片流 (File 不可为空)
 *  @param width       图片宽度（Integer可为空）
 *  @param height      图片高度（Integer可为空）
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTUploadHeadPhotoWithImgFile:(UIImage *)imgFile withWidth:(NSInteger)width withHeight:(NSInteger)height withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  3.5	查询用户信息
 *
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTQueryUserInfoWithTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  3.6	修改用户信息
 *
 *  @param nickName    昵称名称（String）不传代表不修改该项数据
 *  @param birthday    出生日期（String格式：yyyy-MM-dd）不传代表不修改该项数据
 *  @param sex         性别（Integer） 数据含义参考附录3.1 不传代表不修改该项数据
 *  @param profession  职业（Integer） 数据含义参考附录3.2 不传代表不修改该项数据
 *  @param education   文化程度（Integer） 数据含义参考附录3.3 不传代表不修改该项数据
 *  @param userName    用户姓名 （String）不传代表不修改该项数据
 *  @param timeout     设定超时时间
 *  @param returnBlock 回调block
 */
- (void)HTUpdateUserInfoWithNickName:(NSString *)nickName withBirthday:(NSString *)birthday withSex:(NSString *)sex withProfession:(NSString *)profession withEducation:(NSString *)education withUserName:(NSString *)userName withTimeout:(NSInteger)timeout andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  江西TV用户绑定
 *
 *  @param idCard
 *  @param identityNum
 *  @param phoneNo
 *  @param realName
 *  @param type
 *  @param returnBlock 回调block
 */
- (void)HTBindJxTVWithIdCard:(NSString *)idCard withIdentityNum:(NSString *)identityNum withPhoneNo:(NSString *)phoneNo withRealName:(NSString *)realName withType:(NSInteger)type andReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  江西TV用户解除绑定
 *
 *  @param returnBlock 回调block
 */
- (void)HTUNBindJxTVAndReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  获取江西TV用户绑定信息
 *
 *  @param returnBlock 回调block
 */
- (void)HTUserBindInfoJxTVAndReturn:(HTUPortalInterface_Block)returnBlock;


/**
 *  查询江西TV用户是否绑定
 *
 *  @param returnBlock 回调block
 */
- (void)HTUserBindStatusJxTVAndReturn:(HTUPortalInterface_Block)returnBlock;


@end
