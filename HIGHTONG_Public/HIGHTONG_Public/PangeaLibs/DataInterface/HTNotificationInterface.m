//
//  HTNotificationInterface.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/1/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "HTNotificationInterface.h"
@interface HTNotificationInterface ()
{
    NSString *_token;

    NSString *_mobilepush;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation HTNotificationInterface
- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"text/plain",@"ication/json", nil];
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的密钥
        if (_token.length == 48) {
            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            [_manager.requestSerializer setValue:sign  forHTTPHeaderField:@"sign"];
            
            //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
            
        }
        
        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _mobilepush = [userDefaults objectForKey:@"mobilepush"];
    
//        _polymedicine = @"http://192.168.3.13:9005/mobilepush";
    }
    return self;
}


-(void)getMobileMessageGetGroupsWithBusSystem:(NSString*)busSystem andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:busSystem,@"busSystem", nil];
    
    
    [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_GETGROUPS andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status,result,type);
    }];
    
    
    
}


-(void)getMobileMessageBindGoupWithChannelID:(NSString*)channelID withGroupID:(NSString*)groupID withDeviceType:(NSString*)deviceType withBusSystem:(NSString*)busSystem andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];

    if (NotEmptyStringAndNilAndNull(channelID)) {
        
        [paramDic setObject:channelID forKey:@"channelId"];
    }
    
    if (NotEmptyStringAndNilAndNull(groupID)) {
        [paramDic setObject:groupID forKey:@"groupId"];
    }
    
    if (NotEmptyStringAndNilAndNull(deviceType)) {
        [paramDic setObject:deviceType forKey:@"deviceType"];
    }
    
    if (NotEmptyStringAndNilAndNull(busSystem)) {
        [paramDic setObject:busSystem forKey:@"busSystem"];
    }
    
    
   [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_BINDGOUP andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
       returnBlock(status,result,type);
   }];

}


-(void)getMobileMessageToGroupsWithMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withGroupIds:(NSString*)groupIds withPushTime:(NSString*)pushTime withMsgType:(NSString*)msgType andExtra:(NSString*)extra andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    
    if (NotEmptyStringAndNilAndNull(msgTitle)) {
        
        [paramDic setObject:msgTitle forKey:@"msgTitle"];
    }
    
    if (NotEmptyStringAndNilAndNull(msgBody)) {
        [paramDic setObject:msgBody forKey:@"msgBody"];
    }
    
    if (NotEmptyStringAndNilAndNull(pushTime)) {
        
        [paramDic setObject:pushTime forKey:@"pushTime"];
    }
    
    if (NotEmptyStringAndNilAndNull(msgType)) {
        [paramDic setObject:msgType forKey:@"msgType"];
    }
    
    if (NotEmptyStringAndNilAndNull(extra)) {
        [paramDic setObject:extra forKey:@"extra"];
    }
    
    
    
    [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_TOGROUPS andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}


-(void)getMobileMessageUNBindGoupWithChannelId:(NSString*)channelId withGroupId:(NSString*)groupId andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    

    if (NotEmptyStringAndNilAndNull(channelId)) {
        
        [paramDic setObject:channelId forKey:@"channelId"];
    }
    
    if (NotEmptyStringAndNilAndNull(groupId)) {
        [paramDic setObject:groupId forKey:@"groupId"];
    }
    
    
    [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_UNBINDGOUP andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}


-(void)getMobileMessageToSingleDeviceWithUserId:(NSString*)userId withMobile:(NSString*)mobile withMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withDeviceType:(NSString*)deviceType withChannelId:(NSString*)channelId withMsgType:(NSString*)msgType withBusSystem:(NSString*)busSystem withExtra:(NSString*)extra andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    
    if (NotEmptyStringAndNilAndNull(userId)) {
        
        [paramDic setObject:userId forKey:@"userId"];
    }
    
    if (NotEmptyStringAndNilAndNull(mobile)) {
        [paramDic setObject:mobile forKey:@"mobile"];
    }
    
    if (NotEmptyStringAndNilAndNull(msgTitle)) {
        
        [paramDic setObject:msgTitle forKey:@"msgTitle"];
    }
    
    if (NotEmptyStringAndNilAndNull(msgBody)) {
        [paramDic setObject:msgBody forKey:@"msgBody"];
    }
    if (NotEmptyStringAndNilAndNull(deviceType)) {
        [paramDic setObject:deviceType forKey:@"deviceType"];
    }
    if (NotEmptyStringAndNilAndNull(channelId)) {
        [paramDic setObject:channelId forKey:@"channelId"];
    }
    if (NotEmptyStringAndNilAndNull(msgTitle)) {
        [paramDic setObject:msgType forKey:@"msgType"];
    }
    if (NotEmptyStringAndNilAndNull(busSystem)) {
        [paramDic setObject:busSystem forKey:@"busSystem"];
    }
    if (NotEmptyStringAndNilAndNull(extra)) {
        [paramDic setObject:extra forKey:@"extra"];
    }
    
    
    
    [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_TOSINGLEDEVICE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
}


-(void)getMobileMessageBindingUpdateWithChannelId:(NSString*)channelId withMobile:(NSString*)mobile andReturnBlock:(HTNotificationInterface_Block)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    
    if (NotEmptyStringAndNilAndNull(channelId)) {
        
        [paramDic setObject:channelId forKey:@"channelId"];
    }
    
    if (NotEmptyStringAndNilAndNull(mobile)) {
        [paramDic setObject:mobile forKey:@"mobile"];
    }
    
    [self postMsgNotificationPortalRequestWith:paramDic andType:MSG_BINDINGUPDATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];

}


-(void)postMsgNotificationPortalRequestWith:(NSMutableDictionary *)paramDic andType:(NSString *)type andReturn:(HTNotificationInterface_Block)returnBlock
{
    NSLog(@"request header :%@",_manager.requestSerializer.HTTPRequestHeaders);
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;

    NSString *URLString = [NSString stringWithFormat:@"%@%@",_mobilepush,type];

    NSLog(@"推送路径=====%@\n类型====%@\n参数===%@",URLString,type,paramDic);
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"请求成功");
        returnBlock(0,responseObject,type);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"请求失败");
        returnBlock([error code],nil,type);

    }];
    
}



@end
