//
//  HTPolymedicineInterface.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/6.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTPolymedicineInterface.h"

@interface HTPolymedicineInterface ()
{
    NSString *_polymedicine;
    NSString *_token;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end


@implementation HTPolymedicineInterface

- (id)init
{
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        
        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"ication/json",@"text/plain",@"text/html", nil];
        
        _manager.requestSerializer.timeoutInterval = KTimeout;
        
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];//申明请求的数据是json类型

        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
        if ([U_KEY length]) {
            [_manager.requestSerializer setValue:U_KEY forHTTPHeaderField:@"u"];
        }
        //--------------------
        
        
        
        
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        
        NSLog(@"32412341243124----------%@",_token);
        
        if (_token.length == 0) {
            
            _token = @"";
            
        }
        
        // 48位的token 需要截取前32位之后才能使用 后16位是AES加密的秘钥
        if (_token.length == 48) {

            NSString *sign = [KaelTool getRquestSignValueWith:_token andVersionNum:Encrypt_version];
            
            [_manager.requestSerializer setValue:sign forHTTPHeaderField:@"sign"];

                //如果token48位 那就截取一下 如果32位 那就直接使用。
            _token = [KaelTool getTruthTokenWith:_token];
        }
    
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _polymedicine = [userDefaults objectForKey:@"polymedicine"];
//            _polymedicine = @"http://60.205.3.11:8081/polymedicine";
    }
    
    return self;
}


//2.1	首页医院logo和名称
//- (void)YJIntroduceDetailWithInstanceCode:(NSString *)instanceCode withReturn:(HTPolymedicineRequest_Return)returnBlock
//{
//    //    instanceCode = [instanceCode length] ? instanceCode : @"";
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
//    [self postRequestWith:paramDic andType:YJ_INTRODUCE_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        returnBlock(status, result, type);
//    }];
//}
- (void)YJIntroduceDetailWithReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:KInstanceCode,@"instanceCode", nil];
    [self postRequestWith:paramDic andType:YJ_INTRODUCE_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.22	医院列表
- (void)YJIntroduceListWithAreaId:(NSString *)areaId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pageNo,@"pageNo",pageSize,@"pageSize", nil];
    
    if (NotEmptyStringAndNilAndNull(areaId)) {
        [paramDic setObject:areaId forKey:@"areaId"];
    }
    
    [self postRequestWith:paramDic andType:YJ_INTRODUCE_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.3	科室列表
- (void)YJDepartmentAllListWithInstanceCode:(NSString *)instanceCode withLevel:(NSString *)level withReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSString *levelStr = [level length] ? level : @"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",levelStr,@"level ", nil];
    
    [self postRequestWith:paramDic andType:YJ_DEPARTMENT_ALLLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

////2.4	科室详情
//- (void)YJDepartmentDetailWithID:(NSString *)departmentID andReturn:(HTPolymedicineRequest_Return)returnBlock
//{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:departmentID,@"id", nil];
//    
//    [self postRequestWith:paramDic andType:YJ_DEPARTMENT_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        returnBlock(status, result, type);
//    }];
//}

//2.15	根据楼栋查询科室
- (void)YJBuildingSearchWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyWord:(NSString *)keyWord withBuildId:(NSString *)buildId withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    buildId = [buildId length] ? buildId : @"";
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:buildId,@"buildId",pageNo,@"pageNo",pageSize,@"pageSize",keyWord,@"keyWord",instanceCode,@"instanceCode", nil];
    
    
    [self postRequestWith:paramDic andType:YJ_BUILDING_SEARCH andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

////2.6	科室搜索
//- (void)YJDepartmentListWithName:(NSString *)name andReturn:(HTPolymedicineRequest_Return)returnBlock
//{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:name,@"name",KInstanceCode,@"instanceCode", nil];
//    
//    [self postRequestWith:paramDic andType:YJ_DEPARTMENT_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        returnBlock(status, result, type);
//    }];
//}


//2.4	专家列表
- (void)YJExpertAllListWithInstanceCode:(NSString *)instanceCode WithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withDepId:(NSString *)depId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    depId = [depId length] ? depId: @"";
    mobile = [NSString stringWithFormat:@"%@",[mobile length] ? mobile: @""];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",pageNo,@"pageNo",pageSize,@"pageSize",depId,@"deptId",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_EXPERT_ALLLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.5	专家明细
- (void)YJExpertDetailWithID:(NSString *)expertID withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    mobile = [NSString stringWithFormat:@"%@",[mobile length] ? mobile : @""];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:expertID,@"id",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_EXPERT_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.6	专家搜索
- (void)YJExpertByNameListWithInstanceCode:(NSString *)instanceCode withName:(NSString *)name withPageNo:(NSString *)pageNo andPageSize:(NSString *)pageSize withSource:(NSString *)source andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pageNo,@"pageNo",pageSize,@"pageSize",name,@"name",instanceCode,@"instanceCode",source,@"source", nil];
    
    [self postRequestWith:paramDic andType:YJ_EXPERT_BYNAMELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.10	根据科室查询医生  2.7	根据科室查询专家
- (void)YJExpertByDeptidListWithDepID:(NSString *)depId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pageNo,@"pageNo",pageSize,@"pageSize",depId,@"deptId", nil];
    
    [self postRequestWith:paramDic andType:YJ_EXPERT_BYDEPTIDLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

////2.11	热词列表
//- (void)YJHotwordsListWithBusinessType:(NSString *)businessType andReturn:(HTPolymedicineRequest_Return)returnBlock
//{
//    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:businessType,@"businessType", nil];
//    
//    [self postRequestWith:paramDic andType:YJ_HOTWORDS_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        returnBlock(status, result, type);
//    }];
//}

//2.8	资讯列表
- (void)YJNewsListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    platType = [platType length] ? platType : @"1";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",pageNo,@"pageNo",pageSize,@"pageSize",platType,@"platType", nil];
    
    [self postRequestWith:paramDic andType:YJ_NEWS_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//新增 2.10	就诊须知【菜单】
- (void)YJVisitMenuWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_VISIT_MENU andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//新版增加————2.12	名医列表
- (void)YJExpertNominateWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSString *instanceStr = [instanceCode length] ? instanceCode : @"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceStr,@"instanceCode",pageNo,@"pageNo",pageSize,@"pageSize", nil];
    [self postRequestWith:paramDic andType:YJ_EXPERT_NOMINATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//新版增加————2.13	导诊图-大楼列表
- (void)YJBuildingMenuWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_BUILDING_MENU andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//新版增加————2.14	导诊图-楼层列表
- (void)YJBuildingFloorsWithBuildId:(NSString *)buildId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:buildId,@"buildId",pageNo,@"pageNo",pageSize,@"pageSize", nil];
    [self postRequestWith:paramDic andType:YJ_BUILDING_FLOORS andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.26(V1.3)  养生保健分类

- (void)YJHealthCareCategoryWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length]?instanceCode:@"";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHCARE_CATEGORY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.27(V1.3)  养生保健列表
- (void)YJHealthCareListWithCategoryId:(NSString *)categoryId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
//    categoryId = [categoryId length] ? categoryId : @"";
    platType = [platType length] ? platType : @"1";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:categoryId,@"categoryId",pageNo,@"pageNo",pageSize,@"pageSize",platType,@"platType", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHCARE_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.28(V1.3)  养生保健轮播图片
- (void)YJHealthCareSlideWithCategoryId:(NSString *)categoryId withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
//    categoryId = [categoryId length]?categoryId:@"";
    
    platType = [platType length] ? platType: @"1";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:platType,@"platType",categoryId,@"categoryId", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHCARE_SLIDE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.29(V1.3)  养生保健咨询列表

- (void)YJHealthCareNewsListWithPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    platType = [platType length] ? platType: @"1";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:platType,@"platType", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHCARE_NEWSLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.30(V1.3)  医院关联区域列表

- (void)YJIntroduceAreaListWithReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [_manager.requestSerializer setValue:@""  forHTTPHeaderField:@"sign"];

    [self postRequestWith:paramDic andType:YJ_INTRODUCE_AREALIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 2.33(V1.5)  养生保健拉取推荐（单内容推荐）
- (void)YJHealthCarePullWithReturn:(HTPolymedicineRequest_Return)returnBlock
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"1",@"platType",nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHCARE_PULL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}


//10.1.1	就诊卡列表
- (void)YJPatientListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_PATIENT_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//10.1.2	就诊卡详情
- (void)YJPatientDetailWithClinicNo:(NSString *)clinicNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:clinicNo,@"clinicNo",nil];
    
    [self postRequestWith:paramDic andType:YJ_PATIENT_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//10.1.3	就诊卡绑定
- (void)YJPatientSaveWithName:(NSString *)name withClinicNo:(NSString *)clinicNo withIdentify:(NSString *)identify withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:clinicNo,@"clinicNo",name,@"name",identify,@"identify",mobile,@"mobile",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:parameterDic andType:YJ_PATIENT_SAVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//10.1.4	就诊卡解除绑定
- (void)YJPatientEditWithID:(NSString *)cardID withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:cardID,@"id",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_PATIENT_EDIT andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.19	我的关注医生列表
- (void)YJCollectionListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode", nil];
    [self postRequestWith:parameterDic andType:YJ_COLLECTION_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.20	关注/取消关注医生
- (void)YJCollectionSaveWithExpertId:(NSString *)expertId withFocusFlag:(NSString *)focusFlag withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",expertId,@"expertId",instanceCode,@"instanceCode",focusFlag,@"focusFlag", nil];
    [self postRequestWith:paramDic andType:YJ_COLLECTION_SAVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

////2.22	关于我们
//- (void)YJAboutDetailWithReturn:(HTPolymedicineRequest_Return)returnBlock
//{
//    NSDictionary *paramDic = [NSDictionary dictionary];
//    
//    [self postRequestWith:paramDic andType:YJ_ABOUT_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
//        
//        returnBlock(status, result, type);
//    }];
//}

//2.21	意见反馈
- (void)YJSuggestionSaveWithOptionContent:(NSString *)optionContent withContent:(NSString *)content withContact:(NSString *)contact withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[mobile length]?mobile:@"",@"mobile", nil];
    
    if (optionContent.length) {
        [paramDic setObject:optionContent forKey:@"optionContent"];
    }
    if (contact.length) {
        [paramDic setObject:contact forKey:@"contact"];
    }
    if (content.length) {
        [paramDic setObject:content forKey:@"content"];
    }
    
    [self postRequestWith:paramDic andType:YJ_SUGGESTION_SAVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
    
}

//2.23	推广图片
- (void)YJRegionPopularizeimageWithImageType:(NSString *)imageType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:imageType,@"imageType", nil];
    
    [self postRequestWith:paramDic andType:YJ_REGION_POPULARIZEIMAGE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.24	资讯轮播图片
- (void)YJNewsSlideWithInstanceCode:(NSString *)instanceCode withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    platType = [platType length] ? platType : @"1";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",platType,@"platType", nil];
    
    [self postRequestWith:paramDic andType:YJ_NEWS_SLIDE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.18	版本菜单配置列表
- (void)YJMenuListWithInstanceCode:(NSString *)instanceCode withPosition:(NSString *)position andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",position,@"position",APP_VERSION,@"version", nil];
    
    [self postRequestWith:paramDic andType:YJ_MENU_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.17	就诊导航界面医院电话与交通信息(乘车指南)
- (void)YJIntroduceInfoWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_INTRODUCE_INFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//6.1	验证码短信
- (void)YJSmsSendWithInstanceCode:(NSString *)instanceCode withMsgCategory:(NSString *)megCategory withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",mobile,@"mobile",megCategory,@"msgCategory", nil];
    [self postRequestWith:paramDic andType:YJ_REPORT_SMS_SEND andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//6.2	验证码校验
- (void)YJReportSmsValidWithRandomNum:(NSString *)randomNum withMobile:(NSString *)mobile withMsgCategory:(NSString *)megCategory andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",randomNum,@"randomNum",megCategory,@"msgCategory", nil];
    [self postRequestWith:paramDic andType:YJ_REPORT_SMS_VALID andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//3.1	附近医院
- (void)YJIntroduceNearbyWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:longitude,@"longitude",latitude,@"latitude", nil];
    [self postRequestWith:paramDic andType:YJ_INTRODUCE_NEARBY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//4.1	获取视频节目播放地址(原接口3.1)
- (void)YJCdnVideoUrlWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withVideoId:(NSString *)videoId andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    mobile = [mobile length] ? mobile : @"";
    NSString *instanceStr = [instanceCode length] ? instanceCode : @"";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",videoId,@"videoId",instanceStr,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_URL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.2	获取视频节目分类信息(原接口3.2)
- (void)YJHealthyTypeListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode : @"";
    homeFlag = [homeFlag length] ? homeFlag : @"";
    pageNo = [pageNo length] ? pageNo : @"";
    pageSize = (pageSize > 0) ? pageSize : @"";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:global,@"global",instanceCode,@"instanceCode",homeFlag,@"homeFlag",pageNo,@"pageNo",pageSize,@"pageSize",nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHY_TYPELIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//4.3	获取视频节目信息(原接口3.3)
- (void)YJHealthyHealthyVideoListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withTypeId:(NSString *)typeId withOrder:(NSString *)order withYear:(NSString *)year withTagId:(NSString *)tagId andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode : @"";
    order = [order length] ? order: @"";
    year = [year length] ? year : @"";
    tagId = [tagId length] ? tagId: @"";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:global,@"global",instanceCode,@"instanceCode",typeId,@"typeId",pageNo,@"pageNo",pageSize,@"pageSize",order,@"order",year,@"year",tagId,@"tagId", nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHY_HEALTHYVIDEOLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.4	视频节目推荐（原接口3.4）
- (void)YJCdnVideoRecommendWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withType:(NSString *)type withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",videoId,@"videoId",type,@"type",maxCount,@"maxCount", nil];
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_RECOMMEND andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//4.5	加入收藏夹（原接口3.5）
- (void)YJCdnVideoAddfavoriteWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode",videoId,@"videoId", nil];
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_ADDFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.6	获取收藏夹列表（原接口3.6）
- (void)YJCdnVideoListFavoriteWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode",pageNo,@"pageNo",pageSize,@"pageSize", nil];
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_LISTFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.7	删除收藏夹中的记录(原接口3.7)
- (void)YJCdnVideoDelFavoriteWithInstanceCode:(NSString *)instanceCode WithVideoIds:(NSString *)videoIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",videoIds,@"videoIds",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_DELFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.8	清空收藏夹(原接口3.8)
- (void)YJCdnVideoCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_CDN_VIDEO_CLEANFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



//4.9	获取视频分类与视频(原接口3.9)
- (void)YJHealthyTypeListVideoWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode WithVideoCount:(NSString *)videoCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:global,@"global",instanceCode,@"instanceCode",videoCount,@"videoCount", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHY_TYPELISTVIDEO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//4.10	获取轮播节目列表(新增接口)
- (void)YJHealthyTopListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withOrder:(NSString *)order withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode: @"";
    order = [order length] ? order : @"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:global,@"global",instanceCode,@"instanceCode",order,@"order",maxCount,@"maxCount", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHY_TOP_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//4.11	获取视频发布年份列表(新增接口)
- (void)YJHealthyYearListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withOrder:(NSString *)order andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode: @"";
    order = [order length] ? order : @"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",order,@"order",typeId,@"typeId", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHY_YEAR_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//4.12	获取视频疾病标签列表(新增接口)
- (void)YJHealthyTagListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withYear:(NSString *)year andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode: @"";
    typeId = [typeId length] ? typeId : @"";
    year = [year length] ? year : @"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",typeId,@"typeId",year,@"year", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHY_TAG_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


// 9.1.1	我的费用
- (void)YJRegistrationCostListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    [self postRequestWith:paramDic andType:YJ_REGISTRATIONCOST_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//9.1.2	我的费用[检查费]
- (void)YJInspectCostListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    [self postRequestWith:paramDic andType:YJ_INSPECTCOST_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//8.1.1	就诊记录
- (void)YJHospitalRecordListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    [self postRequestWith:paramDic andType:YJ_HOSPITALRECORD_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//7.1.1	检验单
- (void)YJTestBillListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    [self postRequestWith:paramDic andType:YJ_TESTBILL_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//7.1.2	检验单删除
- (void)YJTestBillDeleteWithID:(NSString *)billID andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:billID,@"id", nil];
    [self postRequestWith:paramDic andType:YJ_TESTBILL_DELETE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//7.1.3	检查单
- (void)YJCheckBillListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    [self postRequestWith:paramDic andType:YJ_CHECKBILL_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//7.1.4	检查单删除
- (void)YJCheckBillDeleteWithId:(NSString *)checkBillID andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:checkBillID,@"id", nil];
    [self postRequestWith:paramDic andType:YJ_CHECKBILL_DELETE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.1	获取医生排班日期列表
- (void)YJAppointmentTimeLineTimeLineDateWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_TIMELINE_TIMELINEDATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.2	获取医生排班汇总信息
- (void)YJAppointmentTimeLineExpertsSummaryWithExpertID:(NSString *)expertId withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:expertId,@"expertId",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_TIMELINE_EXPERTSUMMARY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.3	获取医生排班日信息列表
- (void)YJAppointmentTimeLineExpertDailyWithInstanceCode:(NSString *)instanceCode withExpertid:(NSString *)expertId withDate:(NSString *)date withMiddy:(NSString *)middy andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:instanceCode,@"instanceCode",expertId,@"expertId",date,@"date",middy,@"midday",nil];
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_TIMELINE_EXPERTDAILY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.4	获取科室医生排班汇总信息列表
- (void)YJAppointmentTimeLineDepartmentSummaryWithDepartmentId:(NSString *)departmentId withDate:(NSString *)date withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:instanceCode,@"instanceCode",departmentId,@"departmentId",date,@"date",nil];
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_TIMELINE_DEPERTMENTSUMMARY andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.5	预约挂号
- (void)YJAppointmentOrderCreateWithIdentify:(NSString *)identify withExpertId:(NSString *)expertId withTimeLineId:(NSString *)timeLineId withTimeLineDate:(NSString *)timeLineDate withPatientName:(NSString *)patientName withPatientClinicNo:(NSString *)patientClinicNo withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    patientClinicNo = [patientClinicNo length] ? patientClinicNo : @"";
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:patientClinicNo,@"patientClinicNo",_token,@"token",instanceCode,@"instanceCode",mobile,@"mobile",identify,@"identify",expertId,@"expertId",timeLineId,@"timelineId",timeLineDate,@"timelineDate",patientName,@"patientName",nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_ORDER_CREATE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.6	获取挂号订单列表
- (void)YJAppointmentOrderListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:mobile,@"mobile",pageNo,@"pageNo",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_ORDER_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.7	预约挂号取消
- (void)YJAppointmentOrderCancelWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:mobile,@"mobile",orderId,@"orderId",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_ORDER_CANCEL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.8	删除预约挂号订单
- (void)YJAppointmentOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:mobile,@"mobile",orderIds,@"orderIds",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_ORDER_REMOVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.9	获取检查订单列表
- (void)YJAppointmentCheckOrderListWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_token,@"token",mobile,@"mobile",pageNo,@"pageNo",pageSize,@"pageSize",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_CHECKORDER_LIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.10	删除检查订单
- (void)YJAppointmentCheckOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",orderIds,@"orderIds",mobile,@"mobile",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_CHECKORDER_REMOVE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//5.11	获取预约挂号与检查订单明细  
- (void)YJAppointmentOrderDetailWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMsgBsy:(NSString *)msgBsy withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_token,@"token",mobile,@"mobile",instanceCode,@"instanceCode",msgBsy,@"msgBsy",orderId,@"orderId", nil];
    
    [self postRequestWith:paramDic andType:YJ_APPOINTMENT_ORDER_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//8.2	消息推送绑定
- (void)YJPushTagBindWithChannelId:(NSString *)channelId withDeviceType:(NSString *)deviceType withBindBsy:(NSString *)bindBsy andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:channelId,@"channelId",deviceType,@"deviceType",bindBsy,@"bindBsy",KInstanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_PUSHTAG_BIND andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//8.3	消息详情
- (void)YJPushMsgDetailWithInhospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:inHospitalNo,@"inHospitalNo", nil];
    
    [self postRequestWith:paramDic andType:YJ_PUSHMSG_DETAIL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//9.1	排队叫号查询
- (void)YJTreatmentFindWithMobile:(NSString *)mobile withName:(NSString *)name andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    if (name.length == 0) {
        name = @"";
        //        [paramDic setObject:name forKey:@"name"];
    }
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:name,@"name",KInstanceCode,@"instanceCode",mobile,@"mobile", nil];
    
    [self postRequestWith:paramDic andType:YJ_TREATMENT_FIND andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//9.2	排队叫号通知完成
- (void)YJTreatmentCalledWithQueueId:(NSString *)queueId andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:KInstanceCode,@"instanceCode",queueId,@"queueId", nil];
    
    [self postRequestWith:paramDic andType:YJ_TREATMENT_CALLED andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



#pragma mark--------------parting line--------------
//2.1	获取子频道列表
- (void)YJHealthyVodGetChildChannelWithInstanceCode:(NSString *)instanceCode withParentId:(NSString *)parentId withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length]?instanceCode:@"";
    parentId = [parentId length]?parentId:@"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",parentId,@"parentId",homeFlag,@"homeFlag",maxCount,@"maxCount", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_GETCHILDCHANNEL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.2	获取频道信息
- (void)YJHealthyVodChannelInfoWithChannelId:(NSString *)channelId withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length]?instanceCode:@"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:channelId,@"channelId",instanceCode,@"instanceCode",homeFlag,@"homeFlag",maxCount,@"maxCount", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_CHANNELINFO andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.3	获取频道过滤器列表
- (void)YJHealthyVodChannelFiltersWithChannelId:(NSString *)channelId withIncludeYear:(NSString *)includeYear
                                      andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:channelId,@"channelId",includeYear,@"includeYear", nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_CHANNELFILTERS andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.4	获取子频道节目列表
- (void)YJHealthyVodChannelProgramListWithChannelId:(NSMutableArray *)channelIdArr withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:channelIdArr,@"channelId",maxCount,@"maxCount", nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_CHANNELPROGRAMLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.5	获取节目列表
- (void)YJHealthyVodProgramListWithChannelId:(NSString *)channelId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyword:(NSString *)keyWord withFilter:(NSMutableArray *)filterArr andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    keyWord = [keyWord length]?keyWord:@"";
    filterArr = [filterArr count]?filterArr:[NSMutableArray array];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:channelId,@"channelId",pageNo,@"pageNo",pageSize,@"pageSize",keyWord,@"keyword",filterArr,@"filter", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_PROGRAMLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.6	获取节目视频播放地址
- (void)YJHealthyVodMediaPlayListWithProgramId:(NSString *)programId withMobile:(NSString *)mobile withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    mobile = [mobile length] ? mobile : @"";
    instanceCode = [instanceCode length] ? instanceCode :@"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",programId,@"programId",instanceCode,@"instanceCode", nil];
    
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_MEDIAPLAYLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.7	推荐节目
- (void)YJHealthyVodRecommendProgramWithProgramId:(NSString *)programId withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:programId,@"programId",maxCount,@"maxCount", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_RECOMMENDPROGRAM andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.8	获取收藏夹中节目
- (void)YJHealthyVodListFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode",pageNo,@"pageNo",pageSize,@"pageSize", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_LISTFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.9	节目加入收藏夹
- (void)YJHealthyVodAddFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramId:(NSString *)programId andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mobile,@"mobile",instanceCode,@"instanceCode",programId,@"programId", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_ADDFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.10	删除收藏夹中的节目
- (void)YJHealthyVodDelFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramIds:(NSString *)programIds andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",programIds,@"programIds",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_DELFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}


//2.11	清空收藏夹
- (void)YJHealthyVodCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode",mobile,@"mobile", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_CLEANFAVORITE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.14  获取一级频道
- (void)YJHealthyVodGetTopChannelWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock
{
    instanceCode = [instanceCode length] ? instanceCode :@"";
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:instanceCode,@"instanceCode", nil];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_GETTOPCHANNEL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.15  获取精选频道列表
- (void)YJHealthyGetSelectedChannelWithReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_GETSELECTEDCHANNEL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}

//2.16  获取热门频道与节目列表
- (void)YJHealthyVodGetHotChannelWithReturn:(HTPolymedicineRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [self postRequestWith:paramDic andType:YJ_HEALTHYVOD_GETHOTCHANNEL andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        
        returnBlock(status, result, type);
    }];
}



-(void)postRequestWith:(NSDictionary *)paramDic andType:(NSString *)type andReturn:(HTPolymedicineRequest_Return)returnBlock{
    
    if (isEmptyStringOrNilOrNull(_polymedicine)) {
        returnBlock(-1,nil,type);
        return;
    }
    
//    _manager.requestSerializer.timeoutInterval = KTimeout;
//    
//    _manager.requestSerializer = [AFJSONRequestSerializer serializer];//申明请求的数据是json类型
//    [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//    [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
//    [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
//    
////    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
//    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/plain",@"text/html", nil];
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",_polymedicine,type];

    NSLog(@"医院哈哈医院 header%@上传数据%@ ==%@ Type = %@",_manager.requestSerializer.HTTPRequestHeaders,URLString,paramDic,type);

    NSLog(@"KcontentType:%@",_manager.responseSerializer.acceptableContentTypes);
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        returnBlock(0,responseObject,type);

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
        NSLog(@"%@",error);

    }];

}







@end
