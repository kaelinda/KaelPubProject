//
//  HTNotificationInterface.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/1/9.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTNotificationInterface_Block)(NSInteger status, NSMutableDictionary *result, NSString *type);


@interface HTNotificationInterface : NSObject


/**
 获取分组列表

 @param busSystem 业务系统编号
 */
-(void)getMobileMessageGetGroupsWithBusSystem:(NSString*)busSystem andReturnBlock:(HTNotificationInterface_Block)returnBlock;


/**
 1.1	用户绑定分组
 @param channelID 设备标识号(必填)
 @param groupID 分组编号(必填)
 @param deviceType 设备类型(必填) 0：iphone 1：android
 @param busSystem 业务系统编号(必填)  (医聚：medicine)
 */
-(void)getMobileMessageBindGoupWithChannelID:(NSString*)channelID withGroupID:(NSString*)groupID withDeviceType:(NSString*)deviceType withBusSystem:(NSString*)busSystem andReturnBlock:(HTNotificationInterface_Block)returnBlock;


/**
 推送分组消息

 @param msgTitle 消息标题(必填)
 @param msgBody 消息内容 (必填)
 @param groupIds 分组编号(必填) 多组时用逗号分隔
 @param pushTime 推送时间(非必填),空值时默认为及时推送，填入日期格式：yyyy-MM-dd HH:mm:ss
 @param msgType 通知类型 0：透传  1：通知(默认)
 @param extra 其他业务参数
 */
-(void)getMobileMessageToGroupsWithMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withGroupIds:(NSString*)groupIds withPushTime:(NSString*)pushTime withMsgType:(NSString*)msgType andExtra:(NSString*)extra andReturnBlock:(HTNotificationInterface_Block)returnBlock;


/**
 用户解除绑定

 @param userId userId = 用户编号(必填)
 @param mobile mobile = 手机号码(必填)
 @param channelId channelId = 设备标识号(必填)
 @param groupId groupId =分组编号(必填)
 */
-(void)getMobileMessageUNBindGoupWithChannelId:(NSString*)channelId withGroupId:(NSString*)groupId andReturnBlock:(HTNotificationInterface_Block)returnBlock;


/**
 推送单台设备

 @param userId 用户编号(必填)
 @param mobile 手机号码(必填)
 @param msgTitle 消息标题(必填)
 @param msgBody 消息内容 (必填)
 @param deviceType 设备类型(必填) 0：iphone 1：android
 @param channelId 设备标识号(必填)
 @param msgType 通知类型 0：透传  1：通知(默认)
 @param busSystem 业务系统编号    医聚：medicine
 @param extra 其他业务参数
 */
-(void)getMobileMessageToSingleDeviceWithUserId:(NSString*)userId withMobile:(NSString*)mobile withMsgTitle:(NSString*)msgTitle withMsgBody:(NSString*)msgBody withDeviceType:(NSString*)deviceType withChannelId:(NSString*)channelId withMsgType:(NSString*)msgType withBusSystem:(NSString*)busSystem withExtra:(NSString*)extra andReturnBlock:(HTNotificationInterface_Block)returnBlock;


/**
 1.1	绑定信息更新

 @param channelId 设备标识号(必填)
 @param mobile 手机号码(必填)
 */
-(void)getMobileMessageBindingUpdateWithChannelId:(NSString*)channelId withMobile:(NSString*)mobile andReturnBlock:(HTNotificationInterface_Block)returnBlock;



@end
