//
//  HTVODPortalInterface.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/16.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^HTVODPortalRequest_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);
@interface HTVODPortalInterface : NSObject
#pragma mark -
#pragma mark ------- VOD PORTAL
#pragma mark - 珠江数码

/*----------------珠江数码点播---------------*/
/**
 * @brief 2.1 点播运营组
 *
 * @type  VOD_OPERATION_LIST 接口类型
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.2 点播专题
 *
 * @type  VOD_TOPIC_LIST 接口类型
 
 * @param type =类型（Interger，为空默认返回文字专题列表）；1：文字专题；2：海报推荐专题
 * @param scope  筛选范围 （Interger，为空默认返回指定type的全部专题列表）0：全部专题列表；1：运营组专题列表；2：分类专题列表；3：全局专题列表
 * @param operationCode  运营组编码（String）当scope=1时必填
 * @param categoryID  分类编号（Integer）当scope=2时必填
 * @param timeout  设定请求超时时间
 **/
//Old
-(void)VODTopicListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//New
-(void)VODTopicListWithType:(NSString*)type andScope:(NSString *)scope andOperationCode:(NSString *)operationCode amdCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**  cap change
 *  2.3点播专题详情
 *  @type    VOD_TOPIC_DETAIL 接口类型
 *  @param topicCode 专题编码（String 不可为空）
 *  @param timeout   设定请求超时时间
 */
- (void)VODTopicDetailWithTopicCode:(NSString*)topicCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.4 点播分类
 *
 * @type  CATEGORY_LIST 接口类型
 * @param parentID  父分类编号（String）为空默认无父节点
 * @param timeout  设定请求超时时间
 **/
-(void)VODCategoryListWithParentID:(NSString *)parentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.5 点播运营组分类
 *
 * @type  VOD_OPER_CATEGORY_LIST 接口类型
 * @param operationCode  运营组编码（String 不可为空）
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationCategoryListWithOperationCode:(NSString *)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

//New
/**
 *  2.6 点播分类运营组   VOD_CATEGORY_OPER_LIST
 *
 *  @type VOD_CATEGORY_OPER_LIST 接口类型
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param timeout    设定请求超时时间
 */
-(void)VODCategoryOperationListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//New

/**
 *  1.1	点播分类专题列表
 *  @param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param timeout    设定请求超时时间
 */
-(void)VODCategoryTopicListWithCategoryID:(NSString *)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 *  2.9 点播节目列表
 *
 *  @type VOD_PROGRAM_LIST 接口类型
 *	@param token = token串（String不可为空）
 @param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param operationCode  运营组编码 （String 可为空）
 *  @param rootCategoryID 根分类编码 （String 可为空）
 *  @param startSeq       起始序号  （Integer）默认为1
 *  @param count          条数    （Integer）默认为10
 *  @param timeout        设定请求超时时间
 */
-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//真正仅供首页使用 814点播节目列表
-(void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andReturn:(HTVODPortalRequest_Return)returnBlock;


//供点播一级界面使用

- (void)VODProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andProgramName:(NSString*)ProgramName andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 点播运营组节目列表
 *
 * @type  VOD_OPER_PROGRAM_LIST 接口类型
 * @param operationCode  运营组编码（String 不可为空）
 * @param rootCategoryID  根分类编号（String可为空）
 * @param startSeq   起始序号（Integer）默认为1
 * @param count   条数（Integer）默认为10
 * @param timeout  设定请求超时时间
 **/
-(void)VODOperationProgramListWithOperationCode:(NSString *)operationCode andRootCategpryID:(NSString *)rootCategoryID andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//******首页的推荐专题获取，需要发送两次数据，标记第几次
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andIndex:(NSString*)index andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.10 点播专题节目列表
 *
 * @type  VOD_TOPIC_PROGRAM_LIST 接口类型
 * @param token  token串（String不可为空）
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 * @param topicCode  专题编码（String 不可为空）
 * @param startSeq   起始序号（Integer）默认为1
 * @param count   条数（Integer）默认为10
 * @param timeout  设定请求超时时间
 **/
-(void)VODTopicProgramListWithTopicCode:(NSString *)topicCode andStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.11 点播搜索
 *
 * @type  VOD_SEARCH 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0) ）
 * @param operationCode    运营组编码（String 可为空）
 * @param rootCategoryID   根分类编号（String可为空,但categoryID 非空时为必传项）
 * @param categoryID       分类编号（String可为空）多个分类时以逗号分隔，查询时要求同时满足几个分类条件
 * @param topicCode        专题编码（String 可为空）
 * @param key              关键字（String 不可为空）
 * @param startSeq         起始序号（Integer）默认为1
 * @param count            条数（Integer）默认为10
 * @param timeout          设定请求超时时间
 **/
//old 3.4
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//* @param orderType = 排序类型（Integer 可为空）1：新上架 2：最热门
//NEW  3.7 orderType
-(void)VODSearchWithOperationCode:(NSString *)operationCode andRootCategoryID:(NSString *)rootCategoryID andCategoryID:(NSString *)categoryID andTopicCode:(NSString *)topicCode andKey:(NSString *)key andStarSeq:(NSString*)startSeq andCount:(NSString*)count andOrderType:(NSString*)orderType andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.12 点播视频详细信息
 *
 * @type  VOD_DETAIL 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODDetailWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.13 点播播放
 *
 * @type  VOD_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param movie_type 媒体类型 (1:正片 2:预览片 默认为1)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODPlayWithContentID:(NSString *)contentID andWithMovie_type:(NSString *)movie_type andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.14 点播询价
 *
 * @type  VOD_INQUIRY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间
 **/
-(void)VODInquiryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.15 付费点播
 *
 * @type  VOD_PAY_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param lockPWD 解锁密码(String 可以为空)
 * @param terminal_type  终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：OTT机顶盒9：PC 默认为0)
 * @param timeout  设定请求超时时间...
 **/
-(void)VODPayPlayWithContentID:(NSString *)contentID andWithLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.16 点播停止
 *
 * @type  VOD_STOP 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 不可为空
 * @param breakPoint  断点位置（Integer）记录观看到多长时间的位置，单位：秒
 * @param timeout  设定请求超时时间
 **/
-(void)VODStopWithContentID:(NSString *)contentID andBreakPoint:(NSInteger)breakPoint andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.17 点播播放记录列表
 *
 * @type  VOD_PLAY_LIST 接口类型
 * @param token      token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String） 可为空。不为空(电影contentID或电视剧子集contentID)时查询指定编号的视频播放记录；为空时查询全部视频播放记录（按照startSeq和count截断）
 * @param startSeq  起始序号（Integer）默认为1，contentID不为空时这个字段无效
 * @param count     条数（Integer）默认为10，contentID不为空时这个字段无效
 * @param timeout   设定请求超时时间
 **/
-(void)VODPlayListWithContentID:(NSString *)contentID andStarSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.18 删除点播播放记录
 *
 * @type  DEL_VOD_PLAY 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String可为空，为空时清空全部点播播放列表）可以删除电视剧单集播放记录，也可以输入电视剧总的contentID删除该剧全部播放记录
 * @param timeout  设定请求超时时间
 **/
-(void)DELVODPlayHistoryWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.19 增加点播收藏
 *
 * @type  ADD_VOD_FAVORITE 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String不可为空）不支持传入分集contentID
 * @param timeout  设定请求超时时间
 **/
-(void)ADDVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.20 删除点播收藏
 *
 * @type  DEL_VOD_FAVORITE 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param contentID  视频编号（String可为空，为空时清空全部点播收藏列表）不支持传入分集contentID
 * @param timeout  设定请求超时时间
 **/
-(void)DELVODFavoryteWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


/**
 * @brief 2.21 点播收藏列表
 *
 * @type  VOD_FAVORITE_LIST 接口类型
 * @param token   token串（String）不可为空  (接口内部传)
 * @param startSeq  起始序号（Integer）默认为1
 * @param count  条数（Integer）默认为10，如果startSeq和count都不传返回全部数据
 * @param timeout  设定请求超时时间
 **/
-(void)VODFavoryteListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 * @brief 2.21 点播关联影片列表（推荐节目相关接口）
 *
 * @type VOD_ASSCCIATION 接口类型
 * @param token token串（string 不可为空）
 * @param terminal_type 终端类型
 * @param contentID 视频编号(string)不可为空
 * @param count 条数（integer）默认为10
 **/
-(void)VODAssccationListWithContentID:(NSString *)contentID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//New
-(void)VODAssccationListWithContentID:(NSString *)contentID andCount:(NSString*)count  andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

//New
/**
 *  2.23 点播热门搜索关键词
 *
 *  @type  VOD_TOP_SEARCH_KEY 接口类型
 *	@param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param key     关键字（String 可为空）终端类型为5、6时不支持汉字，其他终端可以支持汉字
 *  @param count   条数（Integer 可为空 默认为10）
 *  @param timeout 请求超时时间
 */
-(void)VODTopSearchKeyWithKey:(NSString *)key andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
//New
/**
 *  2.24 点播销售品订购
 *
 *  @type orderProduct 接口类型
 *  @param token token串（String不可为空）
 *  @param productId 销售品编号(String 不可为空)
 *  @param lockPWD   解锁密码(String 可以为空)
 *  @param timeout   青泥鳅超时时间
 */
-(void)VODOrderProductWithProductID:(NSString *)productId andLockPWD:(NSString *)lockPWD andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;
/**
 *  2.25 点赞吐槽影片
 *  @type praiseDegrade 接口类型
 *  @param token token串（String不可为空）
 *  @param contentID 视频编号（String 不可为空）电视剧需传剧集视频编号
 *  @param type      类型 (Integer ) 1：点赞 2：吐槽 默认为1
 *  @param timeout   请求超时时间
 */
- (void)VODPraiseDegradeWithContentID:(NSString*)contentID andType:(NSString*)type andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 *  2.26 运营组销售品列表
 *  @type operationProductList 接口类型
 *	@param terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0)
 *  @param token token串（String不可为空）
 *  @param operationCode 运营组编码（String 不可为空）
 *  @param timeout       请求超时时间
 */
- (void)VODOperationProductListWithOperationCode:(NSString*)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;


////NEW
//- (void)VODSeatchKeyListWithTimeout:(NSInteger)timeout;

/**
 *  2.7 点播运营组菜单
 *
 *  @type VOD_OPER_MENU 接口类型
 *  @param 	operationCode = 运营组编码（String 不可为空）
 *  @param 	terminal_type = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒 7：组播OTT机顶盒9：PC 默认为0) 预留字段
 *  @param timeout 请求超时时间
 */
- (void)VODOPerMenuQWithOperationCode:(NSString*)operationCode andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 *  2.8 点播分类菜单
 *
 *  @type VOD_CATEGORY_MENU 接口类型
 *  @param categoryID 分类编号（Integer 不可为空）
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒9：PC 默认为0) 预留字段
 *  @param timeout   请求超时时间
 */
- (void)VODCategoryMenuWithCategoryID:(NSInteger)categoryID andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 *  点播运营组详情
 *
 *  @param operationCode 运营组编码
 *  @param timeout       超时时间
 */
-(void)VODOperationDetailWithOperationCode:(NSString *)operationCode andTimeOut:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;



//点播播放记录列表（新版）
/**
 *  点播播放记录列表 （新版）
 *
 *  @param satrtSeq  起始序号（Integer）默认为1
 *  @param count     条数（Integer）默认为10
 *  @param startDate 开始日期（String 格式：yyyy-MM-dd）可为空
 *  @param endDate   截止日期（String 格式：yyyy-MM-dd）可为空
 *  @param timeout   超时时间
 */
-(void)VODNewPlayRecordListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andSatrtDte:(NSString *)startDate andEndDate:(NSString *)endDate andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

//点播收藏记录列表（新版）
/**
 *  点播收藏列表（新版）
 *
 *  token  = token串（String不可为空）
 *  @param startSeq 起始序号（Integer）默认为1
 *  @param count    条数（Integer）默认为10，如果startSeq和count都不传返回全部数据
 *  @param timeout  超时时间
 */
-(void)VODNewFavorateListWithStartSeq:(NSString*)startSeq andCount:(NSString*)count andTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

//点播 首页配置信息列表
/**
 *  首页配置信息列表
 *
 *  @param timeout 超时时间
 */
-(void)VODHomeConfigListWithTimeOut:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;

//点播首页 配置信息列表
/**
 *  点播页菜单配置信息列表
 *
 *  @param timeout 超时时间
 */
-(void)VODMenuConfigListWithTimeout:(NSInteger)timeout andReturn:(HTVODPortalRequest_Return)returnBlock;



//获取首页轮播图   POSTIMAGE
-(void)VODGetHomepageADImageAndReturn:(HTVODPortalRequest_Return)returnBlock;

/**
 *  2.32	上传订单
 *
 *  	token = token串（String不可为空）
 * @param 	orderId = 订单编号（String 不可为空）
 * @param 	price = 影片价格（float不可为空）保留两位小数（单位：元）
 * @param 	contentId = 视频编号（String 不可为空）电视剧需传子集视频编号
 * @param  vodId = 总集视频编号（String 可为空）电视剧需传总集视频编号
 */
- (void)VODUpLoadOrderWithOrderId:(NSString*)orderId andPrice:(NSString*)price andContentId:(NSString*)contentId andVodId:(NSString*)vodId andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;
/**
 *  2.33	点赞吐槽影片详情
 *
 *  token  = token串（String不可为空）
 *  @param 	contentID = 视频编号（String 不可为空）
 *
 *
 */
- (void)VODPraiseDegradeDetailWithContentID:(NSString*)contentID andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;
/**
 *  2.34 医院信息
 *
 *  terminal_type  = 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  code  = 医院编码（String,不可为空）
 *  @param outTime 超时时间
 */
- (void)VODHospitalInfoWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;//使用默认的医院code
- (void)VODHospitalInfoWithCode:(NSString *)code andTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;//接口传入code

/**
 *  2.35 查找医院  getHospital
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param longitude  当前位置的经度（Double,不可为空）
 *  @param latitude   当前位置的纬度 （Double,不可为空）
 *  @param outTime 超时时间
 */
-(void)VODFindHospitalWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;
/**
 *  2.36 医院列表 hospitalList
 *
 *  @param terminal_type 终端类型(0：iphone 1：android 2：IPAD 3：android PAD 5：机顶盒6：单播OTT机顶盒7：组播OTT机顶盒9：PC 默认为0)
 *  @param outTime 超时时间
 */
-(void)VODHospitalListWithTimeout:(NSInteger)outTime andReturn:(HTVODPortalRequest_Return)returnBlock;

@end
