//
//  VODPageRequestManger.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/9/14.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol VODPageRequestDelegate <NSObject>

-(void)VODMenuDataGetSuccessWith:(NSMutableArray *)result;

-(void)VODDisplayDataGetSuccessWith:(NSMutableArray *)result;

@end
@interface VODPageRequestManger : NSObject
@property (nonatomic,assign) id<VODPageRequestDelegate> delegate;
-(id)initWithDelegate:(id <VODPageRequestDelegate>)delegate;

-(void)reloadVODMenuData;


-(void)reloadDataForMenuDataArray:(NSMutableArray*)menuDataArray withMenuType:(NSString *)menuType;

-(void)reloadDataForMenuDataCode:(NSString *)code withMenuType:(NSString *)menuType;

//运营组分类列表
-(void)reloadOperationCategoryListWithOperationCode:(NSString *)operationCode;

//专题列表

-(void)reloadDataTopicProgramListWithTopicCode:(NSString*)topicCode andStartSeq:(NSString*)StartSeq andCount:(NSString *)count;

//分类专题列表
-(void)reloadCategoryTopicListWithCategoryID:(NSString *)categoryID;



@end
