//
//  HTUNPortalInterface.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTUNPortalInterface.h"
#import "BPushSingleTon.h"
@interface HTUNPortalInterface ()


@property (nonatomic,strong)AFHTTPSessionManager *manager;
@end
@implementation HTUNPortalInterface
- (id)init
{
    
    self = [super init];
    
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
//        [self.manager setSecurityPolicy:[HTRequest customSecurityPolicyWithCertificateType:Cer_pangeachina]];
//        //        _manager.securityPolicy.allowInvalidCertificates = NO;;
        _manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/json",@"text/javascript",@"ication/json", nil];
        
//        [_manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
//        [_manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
//        [_manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
        
    }
    return self;
}

- (void)UNPortalProjectVersion:(HTUNPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"",@"",nil];
 
    [self postUNPortalRequestWith:paramDic andType:UN_PROJECTVERSION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
    }];
    
    
}



- (void)UNPortalProjectListWithRegionKey:(NSString*)regionKey andReturn:(HTUNPortalRequest_Return)returnBlock
{


    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:regionKey,@"regionKey", nil];
    
    
    if (IS_MERGEPORJECTLIST) {
        
        [self postUNPortalMergeRequestWith:paramDic andType:UN_PROJECTLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
            returnBlock(status,result,type);
            
        }];
    }else{
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];//默认是支持VOD
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];//默认 不支持广告
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EPGPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"VODPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"UPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ICS"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shphone"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"moreWifiLink"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"polymedicine"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMGPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMG_PUBLIC"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shphone_public"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"appbase"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_PUBLIC_ENV"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shtv"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorCode"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pubapi"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pubportal"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"mobilepush"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"shportal"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ugc"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self postUNPortalRequestWith:paramDic andType:UN_PROJECTLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
            returnBlock(status,result,type);
            
        }];
    }

    

    

}


- (void)UNPortalRegionVersion:(HTUNPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"",@"",nil];
    [self postUNPortalRequestWith:paramDic andType:UN_REGIONVERSION andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);

    }];
    
}


- (void)UNPortalRegionList:(HTUNPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"",@"",nil];
    [self postUNPortalRequestWith:paramDic andType:UN_REGIONLIST andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
        
    }];
}



- (void)UNPortalResetConfigWithType:(NSString *)type  andReturn:(HTUNPortalRequest_Return)returnBlock
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:type,@"type",nil];
    [self postUNPortalRequestWith:paramDic andType:UN_RESETCONFIG andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
        returnBlock(status,result,type);
        }];
}

//内外网合并专属
-(void)postUNPortalMergeRequestWith:(NSMutableDictionary *)paramDic andType:(NSString *)type andReturn:(HTUNPortalRequest_Return)returnBlock{
    
    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [[NSString alloc]init];
    
    if ([_requestType isEqualToString:@"PUB"]) {
        
        URLString = [NSString stringWithFormat:@"%@/%@",PROJECT_PORTAL,type];
        
    }else if([_requestType isEqualToString:@"LAN"]){
        
        URLString = [NSString stringWithFormat:@"%@/%@",PROJECT_LANPORTAL,type];
        
    }
    
    
    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
     
        
        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
        
    }];
    
}







-(void)postUNPortalRequestWith:(NSMutableDictionary *)paramDic andType:(NSString *)type andReturn:(HTUNPortalRequest_Return)returnBlock{

    _manager.requestSerializer.timeoutInterval = kTIME_TEN;
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@",PROJECT_PORTAL,type];

    [_manager POST:URLString parameters:paramDic progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([type isEqualToString:@"projectVersion"]) {
            
            
            
            
        }
        if ([type isEqualToString:@"projectList"]) {
            [self loadingPRTALDataWith:responseObject];
            
        }
        [[BPushSingleTon shareInstance]bindChannel];

        returnBlock(0,responseObject,type);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        returnBlock([error code],nil,type);
  
    }];

}



-(void)loadingPRTALDataWith:(NSDictionary *)result{
    
    NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:result];
    
    NSArray *portalList = [mDic objectForKey:@"projectList"];
    
    for (int i=0; i<portalList.count; i++) {
        
        NSDictionary *itemDic = [portalList objectAtIndex:i];
        NSString *projectCode = [NSString stringWithFormat:@"%@",[itemDic objectForKey:@"projectCode"]];

        NSMutableString *projectUrl = [NSMutableString stringWithFormat:@"%@",[itemDic objectForKey:@"projectUrl"]];
        
        projectUrl = projectUrl.length>0 ? projectUrl :[NSMutableString stringWithFormat:@""];
        
        if ([projectUrl hasSuffix:@"/"]) {
            [projectUrl deleteCharactersInRange:NSMakeRange(projectUrl.length-1, 1)];
        }
        
        if ([projectCode isEqualToString:@"EPGPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"EPGPORTAL"];
            
            if (projectUrl.length == 0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_EPG_CONFIGURED"];
            }
            
        }
        if ([projectCode isEqualToString:@"VODPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"VODPORTAL"];
            
            if (projectUrl.length == 0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_EPG_CONFIGURED"];
            }
            
        }
        if ([projectCode isEqualToString:@"UPORTAL"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"UPORTAL"];
            
        }
        if ([projectCode isEqualToString:@"ICS"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ICS"];
            
        }
        if ([projectCode isEqualToString:@"VODSWITCH"]) {
            if (![projectUrl isEqualToString:@"close"]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_VOD"];
            }
            
        }
        if ([projectCode isEqualToString:@"ADPORTAL"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ADPORTAL"];
            if (projectUrl.length==0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
            }else{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
            }
        }
        if ([projectCode isEqualToString:@"shphone"]) {
            //shphone
            //兼容 后缀 无论有没有 都删除一下 再拼接上
            if ([projectUrl hasSuffix:@"/shphone"]) {
                [KaelTool deleteString:@"/shphone" fromString:projectUrl];
                
            }
            if ([projectUrl hasSuffix:@"/shphone/"]) {
                [KaelTool deleteString:@"/shphone/" fromString:projectUrl];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone"];
            //            [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
            [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/shphone/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
            
        }
        
        
        if ([projectCode isEqualToString:@"polymedicine"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"polymedicine"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSuccessPortal"];
        }
        
        
        if ([projectCode isEqualToString:@"IMG"]) {
            
            if (APIVERSION.length>0) {
                [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMGPORTAL"];
            }else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMGPORTAL"];
            }
        }
        if ([projectCode isEqualToString:@"IMG_PUBLIC"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMG_PUBLIC"];
        }
        
        if ([projectCode isEqualToString:@"shphone_public"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone_public"];
            
        }
        if ([projectCode isEqualToString:@"appbase"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"appbase"];
            
        }
        if ([projectCode isEqualToString:@"IS_PUBLIC_ENV"]) {
            
            BOOL kIS_PUBLIC_ENV = [projectUrl integerValue]==1 ? YES : NO;
            [[NSUserDefaults standardUserDefaults] setBool:kIS_PUBLIC_ENV forKey:@"IS_PUBLIC_ENV"];
            
        }
        if ([projectCode isEqualToString:@"shtv"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shtv"];
            
        }
        
        
        if ([projectCode isEqualToString:@"operatorCode"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"operatorCode"];
            
        }
        if ([projectCode isEqualToString:@"tvbiz"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"tvbiz"];
            
        }
        
        if ([projectCode isEqualToString:@"pubapi"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"pubapi"];
            
        }

        if ([projectCode isEqualToString:@"mobilepush"]) {
            [[NSUserDefaults standardUserDefaults]setObject:projectUrl forKey:@"mobilepush"];
        }
        
        if ([projectCode isEqualToString:@"ugc"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ugc"];
            
            
        }
        
        if ([projectCode isEqualToString:@"shportal"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shportal"];
            
            
        }else
        {
            //轻松一刻 网页版本
            if ([projectCode isEqualToString:@"pubportal"]) {
                [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"pubportal"];
                
            }
        }

        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    
    
}





@end
