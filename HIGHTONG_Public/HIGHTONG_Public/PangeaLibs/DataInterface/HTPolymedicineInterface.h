//
//  HTPolymedicineInterface.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/5/6.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


typedef void(^HTPolymedicineRequest_Return)(NSInteger status, NSMutableDictionary *result, NSString *type);


@interface HTPolymedicineInterface : NSObject

/**
 *  2.1	首页医院logo和名称
 *
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
//- (void)YJIntroduceDetailWithInstanceCode:(NSString *)instanceCode withReturn:(HTPolymedicineRequest_Return)returnBlock;
- (void)YJIntroduceDetailWithReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  2.22	医院列表
 *
 *  @param areaId      区域编号(非必填)
 *  @param pageNo      页码
 *  @param pageSize    每页大小
 *  @param returnBlock 返回数据
 */
- (void)YJIntroduceListWithAreaId:(NSString *)areaId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  2.3	科室列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param level        层级 (非必填,为空时返回所有)
 *  @param returnBlock  返回数据
 */
- (void)YJDepartmentAllListWithInstanceCode:(NSString *)instanceCode withLevel:(NSString *)level withReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.15	根据楼栋查询科室
 *
 *  @param pageNo       页码
 *  @param pageSize     每页大小
 *  @param keyWord      搜索关键字
 *  @param buildId      建筑物ID(就诊导航全局搜索时可不传该参数,医院的建筑物ID)
 *  @param instanceCode 应用代码
 *  @param returnBlock  返回数据
 */
- (void)YJBuildingSearchWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyWord:(NSString *)keyWord withBuildId:(NSString *)buildId withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


///**
// *  2.6	科室搜索
// *
// *  @param name 搜索内容（String不可为空）
// *  @param returnBlock 返回数据
// */
//- (void)YJDepartmentListWithName:(NSString *)name andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  新版的2.4
 *
 *  @param instanceCode 应用编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param depId        科室Id(服务端需要对下一级或更多级进行处理)
 *  @param returnBlock  返回数据
 */
- (void)YJExpertAllListWithInstanceCode:(NSString *)instanceCode WithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withDepId:(NSString *)depId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  新版2.5专家明细
 *
 *  @param expertID    专家编号
 *  @param returnBlock 返回数据
 */
- (void)YJExpertDetailWithID:(NSString *)expertID withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.6	专家搜索
 *
 *  @param instanceCode 医院编号
 *  @param name         搜索内容（String不可为空
 *  @param pageNo       当前页码
 *  @param pageSize     每页显示条数
 *  @param source       搜索来源(此字段TV终端可忽略,手机终端必传【全部专家搜索:ALL,名医榜内搜索:SUPER,科室内搜索:科室ID】)
 *  @param returnBlock  返回数据
 */
- (void)YJExpertByNameListWithInstanceCode:(NSString *)instanceCode withName:(NSString *)name withPageNo:(NSString *)pageNo andPageSize:(NSString *)pageSize withSource:(NSString *)source andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.7	根据科室查询医生 
 *
 *  @param depId       科室编号（String不可为空）
 *  @param pageNo      当前页码
 *  @param pageSize    每页显示条数
 *  @param returnBlock 返回数据
 */
- (void)YJExpertByDeptidListWithDepID:(NSString *)depId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;


///**
// *  2.11	热词列表
// *
// *  @param businessType 热词类型（Integer不可为空；1.科室热词  2.医生热词）
// *  @param returnBlock  返回数据
// */
//- (void)YJHotwordsListWithBusinessType:(NSString *)businessType andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  2.8	资讯列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param returnBlock  返回数据
 */
- (void)YJNewsListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  新版增加————2.10	就诊须知【菜单】
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJVisitMenuWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  新版增加————2.12	名医列表
 *
 *  @param instanceCode 应用编码(非必填，带入时获取特定医院的名医)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param returnBlock  返回数据
 */
- (void)YJExpertNominateWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  新版增加————2.13	导诊图-大楼列表
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJBuildingMenuWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  新版增加————2.14	导诊图-楼层列表
 *
 *  @param buildId     医院大楼ID(必填)
 *  @param pageNo      当前页码
 *  @param pageSize    每页大小
 *  @param returnBlock 返回数据
 */
- (void)YJBuildingFloorsWithBuildId:(NSString *)buildId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;




/**
 *  10.1.1	就诊卡列表
 *
 *  @param mobile      手机号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  10.1.2	就诊卡详情
 *
 *  @param clinicNo    门诊卡号
 *  @param returnBlock 返回数据
 */
- (void)YJPatientDetailWithClinicNo:(NSString *)clinicNo andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  10.1.3	就诊卡绑定
 *
 *  @param name        病人姓名（String不可为空）
 *  @param clinicNo    住院号（String不可为空）
 *  @param identify    身份证号（String不可为空）
 *  @param mobile      手机号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientSaveWithName:(NSString *)name withClinicNo:(NSString *)clinicNo withIdentify:(NSString *)identify withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.17	就诊卡解除绑定
 *
 *  @param cardID      编号（String不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJPatientEditWithID:(NSString *)cardID withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.19	我的关注医生列表
 *
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
- (void)YJCollectionListWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  新接口 2.20	关注/取消关注医生
 *
 *  @param expertId     医生编号（String不可为空）
 *  @param focusFlag    关注标识 0：取消关注  1：关注
 *  @param instanceCode 医院编号
 *  @param returnBlock  返回数据
 */
- (void)YJCollectionSaveWithExpertId:(NSString *)expertId withFocusFlag:(NSString *)focusFlag withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


///**
// *  2.22	关于我们
// *
// *  @param returnBlock 返回数据
// */
//- (void)YJAboutDetailWithReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.23	意见反馈  2.21
 *
 *  @param optionContent 意见选择内容（String）
 *  @param content       意见输入内容（String）
 *  @param contact       联系方式（String）
 *  @param returnBlock   返回数据
 */
- (void)YJSuggestionSaveWithOptionContent:(NSString *)optionContent withContent:(NSString *)content withContact:(NSString *)contact withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.23	推广图片
 *
 *  @param imageType   图片类型 0:二维码 1:wife推广大图
 *  @param returnBlock 返回数据
 */
- (void)YJRegionPopularizeimageWithImageType:(NSString *)imageType andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.24	资讯轮播图片
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJNewsSlideWithInstanceCode:(NSString *)instanceCode withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.24	版本菜单配置列表  2.18	版本菜单配置列表
 *
 *  @param position    菜单位置（1.首页 2.我的医疗 3.我的费用 ......）
 *  @param returnBlock 返回数据
 */
- (void)YJMenuListWithInstanceCode:(NSString *)instanceCode withPosition:(NSString *)position andReturn:(HTPolymedicineRequest_Return)returnBlock;

///**
// *  2.25	发送短信
//
// *
// *  @param instanceCode 运用编码（必填）
// *  @param megCategory  消息分类【见消息类型说明】（必填）
// *  @param returnBlock  返回数据
// */
//- (void)YJSmsSendWithMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.28	就诊导航界面医院电话与交通信息  新版2.17	就诊导航界面医院电话与交通信息(乘车指南)
 *
 *  @param returnBlock 返回数据
 */
- (void)YJIntroduceInfoWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.26(V1.3)  养生保健分类
 *
 *  @param instanceCode 应用编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJHealthCareCategoryWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.27(V1.3)  养生保健列表
 *
 *  @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *  @param categoryId   分类编码(必填)
 *  @param pageNo       当前页码
 *  @param pageSize     每页大小
 *  @param returnBlock  返回数据
 */
- (void)YJHealthCareListWithCategoryId:(NSString *)categoryId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  2.28(V1.3)  养生保健轮播图片
 *
 *  @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *  @param categoryId   分类编码(必填)
 *  @param returnBlock  返回数据
 */
- (void)YJHealthCareSlideWithCategoryId:(NSString *)categoryId withPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.29(V1.3)  养生保健咨询列表
 *  @param platType 平台类型(1=APP，2=微信，默认返回APP格式，微信端必填)
 *  @param returnBlock  返回数据
 */
- (void)YJHealthCareNewsListWithPlatType:(NSString *)platType andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.30(V1.3)  医院关联区域列表
 *
 *  @param returnBlock  返回数据
 */
- (void)YJIntroduceAreaListWithReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.33(V1.5)  养生保健拉取推荐（单内容推荐）
 *
 *  @param returnBlock  返回数据
 */
- (void)YJHealthCarePullWithReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  6.1	验证码短信
 
 *
 *  @param instanceCode 运用编码（必填）
 *  @param megCategory  消息分类【见消息类型说明】（必填）
 *  @param returnBlock  返回数据
 */
- (void)YJSmsSendWithInstanceCode:(NSString *)instanceCode withMsgCategory:(NSString *)megCategory withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  2.30	报告单查看验证码校验   6.2	验证码校验
 *
 *  @param randomNum   验证码（必填）
 *  @param mobile      手机号码（必填）
 *  @param returnBlock 返回数据
 */
- (void)YJReportSmsValidWithRandomNum:(NSString *)randomNum withMobile:(NSString *)mobile withMsgCategory:(NSString *)megCategory andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  3.1	附近医院
 *
 *  @param longitude   经度
 *  @param latitude    纬度
 *  @param returnBlock 返回数据
 */
- (void)YJIntroduceNearbyWithLongitude:(NSString *)longitude withLatitude:(NSString *)latitude andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.1	获取视频节目播放地址(原接口3.1)
 *
 *  @param instanceCode 医院实例（String 可为空）
 *  @param videoId      视频ID（String 不可为空）
 *  @param returnBlock
 */
- (void)YJCdnVideoUrlWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withVideoId:(NSString *)videoId andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.2	获取视频节目分类信息(原接口3.2)
 *
 *  @param instanceCode 医院实例编码（可为空）
 *  @param global      是否包括系统公共的分类(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param homeFlag    首页显示标记(可为空,传空默认值为0)
 *  @param pageNo      当前页码(可为空，传空不分页)
 *  @param pageSize    分页大小(可为空，传空不分页)
 *  @param returnBlock 请求返回数据
 */
- (void)YJHealthyTypeListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  4.3	获取视频节目信息(原接口3.3)
 *
 *  @param global      是否包括系统该分类下全国性的视频节目(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param pageNo      页码（String 不可为空）
 *  @param pageSize    分页大小（String 不可为空）
 *  @param typeId      分类(String 不可为空)
 *  @param order       年份排序顺序(可为空，传空默认值为1)
 *  @param year        年份(可为空)
 *  @param tagId       标签ID(可为空)
 *  @param returnBlock 请求返回数据
 */
- (void)YJHealthyHealthyVideoListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withTypeId:(NSString *)typeId withOrder:(NSString *)order withYear:(NSString *)year withTagId:(NSString *)tagId andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.4	视频节目推荐（原接口3.4）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoId      视频节目ID（String 不可为空）
 *  @param type         视频分类（String 不可为空）
 *  @param maxCount     最多返回多少条推荐视频
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoRecommendWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withType:(NSString *)type withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.5	加入收藏夹（原接口3.5）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoId      视频节目ID（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoAddfavoriteWithInstanceCode:(NSString *)instanceCode WithVideoId:(NSString *)videoId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.6	获取收藏夹列表（原接口3.6）
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param pageNo       页码（String 不可为空）
 *  @param pageSize     分页大小（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoListFavoriteWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.7	删除收藏夹中的记录(原接口3.7)
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoIds     视频ID列表（String 不可为空，多个id之间用”,”分割）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoDelFavoriteWithInstanceCode:(NSString *)instanceCode WithVideoIds:(NSString *)videoIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.8	清空收藏夹(原接口3.8)
 *
 *  @param instanceCode AppID（String 不可为空）
 *  @param returnBlock  请求返回数据
 */
- (void)YJCdnVideoCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  4.9	获取视频分类与视频(原接口3.9)
 *
 *  @param global       是否包括系统公共的分类与视频(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param instanceCode AppID（String 不可为空）
 *  @param videoCount   每个分类最多返回视频信息的记录数
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTypeListVideoWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode WithVideoCount:(NSString *)videoCount andReturn:(HTPolymedicineRequest_Return)returnBlock;

//--------------------------------------------

/**
 *  4.10	获取轮播节目列表(新增接口)
 *
 *  @param global       是否包括系统公共的分类(String 可为空,默认值为0, 1:包括, 0:不包括)
 *  @param instanceCode 医院编号(可为空)
 *  @param order        排序顺序(可为空,默认值为1)
 *  @param maxCount     返回视频节目最大记录数
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTopListWithGlobal:(NSString *)global withInstanceCode:(NSString *)instanceCode withOrder:(NSString *)order withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;



/**
 *  4.11	获取视频发布年份列表(新增接口)
 *
 *  @param instanceCode 医院编号(可为空)
 *  @param typeId       视频分类ID(不可为空)
 *  @param order        年份排序顺序(可为空，默认值为1)
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyYearListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withOrder:(NSString *)order andReturn:(HTPolymedicineRequest_Return)returnBlock;

/**
 *  4.12	获取视频疾病标签列表(新增接口)
 *
 *  @param instanceCode 医院编号(可为空)
 *  @param typeId       视频分类ID(可为空)
 *  @param year         年份(可为空)
 *  @param returnBlock  请求返回数据
 */
- (void)YJHealthyTagListWithInstanceCode:(NSString *)instanceCode withTypeId:(NSString *)typeId withYear:(NSString *)year andReturn:(HTPolymedicineRequest_Return)returnBlock;



//---------------------------------------------------------------
/**
 *  9.1.1	我的费用
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJRegistrationCostListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  9.1.2	我的费用[检查费]
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJInspectCostListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  8.1.1	就诊记录
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJHospitalRecordListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  7.1.1	检验单
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJTestBillListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  7.1.2	检验单删除
 *
 *  @param billID      编号字符串（多选唯一编号 用逗号拼接成字符串）
 *  @param returnBlock 返回数据
 */
- (void)YJTestBillDeleteWithID:(NSString *)billID andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  7.1.3	检查单
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJCheckBillListWithInHospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  7.1.4	检查单删除
 *
 *  @param checkBillID 编号字符串（多选唯一编号 用逗号拼接成字符串）
 *  @param returnBlock 返回数据
 */
- (void)YJCheckBillDeleteWithId:(NSString *)checkBillID andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.1	获取医生排班日期列表.
 *
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineTimeLineDateWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.2	获取医生排班汇总信息
 *
 *  @param expertId    医生id编码（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineExpertsSummaryWithExpertID:(NSString *)expertId withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.3	获取医生排班日信息列表
 *
 *  @param expertId    医生id编码（String 不可为空）
 *  @param date        日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param middy       上午与下午标识(上午：1， 下午：2)
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentTimeLineExpertDailyWithInstanceCode:(NSString *)instanceCode withExpertid:(NSString *)expertId withDate:(NSString *)date withMiddy:(NSString *)middy andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.4	获取科室医生排班汇总信息列表
 *
 *  @param departmentId 科室id编码（String 不可为空）
 *  @param date         日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param returnBlock  返回数据
 */
- (void)YJAppointmentTimeLineDepartmentSummaryWithDepartmentId:(NSString *)departmentId withDate:(NSString*)date withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.5	预约挂号
 *
 *  @param identify        身份证号码（String 不可为空）
 *  @param expertId        医生Id编码（String 不可为空）
 *  @param timeLineId      预约排班id编码（String 不可为空）
 *  @param timeLineDate    排班日期（String 不可为空，格式:yyyy-mm-dd）
 *  @param patientName     患者名称（String 不可为空）
 *  @param patientClinicNo 诊疗卡号码（String 可为空）
 *  @param returnBlock     返回数据
 */
- (void)YJAppointmentOrderCreateWithIdentify:(NSString *)identify withExpertId:(NSString *)expertId withTimeLineId:(NSString *)timeLineId withTimeLineDate:(NSString *)timeLineDate withPatientName:(NSString *)patientName withPatientClinicNo:(NSString *)patientClinicNo withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.6	获取挂号订单列表
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param pageNo      页码(int 不可为空)
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderListWithInstanceCode:(NSString *)instanceCode withPageNo:(NSString *)pageNo withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.7	预约挂号取消
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param orderId     订单Id（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderCancelWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.8	删除预约挂号订单  
 *
 *  @param mobile      手机号码（String 不可为空）
 *  @param orderIds    订单Id列表（String 不可为空，多个ID采用逗号进行分割）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.9	获取检查订单列表
 *
 *  @param pageNo      页码
 *  @param pageSize    分页大小
 *  @param mobile      手机号码
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentCheckOrderListWithPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.10	删除检查订单
 *
 *  @param mobile      手机号码
 *  @param orderIds    订单Id列表（String 不可为空，多个ID采用逗号进行分割）
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentCheckOrderRemoveWithInstanceCode:(NSString *)instanceCode withOrderIds:(NSString *)orderIds withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  5.11	获取预约挂号与检查订单明细
 *
 *  @param mobile      手机号码
 *  @param orderId     订单Id
 *  @param msgBsy      订单类型，回传推送消息中的msgBsy字段
 *  @param returnBlock 返回数据
 */
- (void)YJAppointmentOrderDetailWithInstanceCode:(NSString *)instanceCode withOrderId:(NSString *)orderId withMsgBsy:(NSString *)msgBsy withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  8.2	消息推送绑定
 *
 *  @param channelId   百度云推送分派设备唯一标识
 *  @param deviceType  设备系统类型 3： android  4: IOS
 *  @param bindBsy     绑定业务 0: 全局业务推送
 *  @param returnBlock 返回数据
 */
- (void)YJPushTagBindWithChannelId:(NSString *)channelId withDeviceType:(NSString *)deviceType withBindBsy:(NSString *)bindBsy andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  8.3	消息详情
 *
 *  @param inHospitalNo 住院号
 *  @param returnBlock  返回数据
 */
- (void)YJPushMsgDetailWithInhospitalNo:(NSString *)inHospitalNo andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  9.1	排队叫号查询
 *
 *  @param mobile      手机号码（必填）
 *  @param name        姓名（可选）
 *  @param returnBlock 返回数据
 */
- (void)YJTreatmentFindWithMobile:(NSString *)mobile withName:(NSString *)name andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  9.2	排队叫号通知完成
 *
 *  @param queueId     队列ID(必填)
 *  @param returnBlock 返回数据
 */
- (void)YJTreatmentCalledWithQueueId:(NSString *)queueId andReturn:(HTPolymedicineRequest_Return)returnBlock;



#pragma mark--------------parting line--------------
/**
 *  2.1	获取子频道列表
 *
 *  @param instanceCode 医院编号，（String 可为空）
 *  @param parentId     父级频道ID，（String 可为空）
 *  @param homeFlag     首页显示标记，（String 不可为空）
 *  @param maxCount     最大记录数，（String 不可为空）
 *  @param returnBlock  返回数据
 */
- (void)YJHealthyVodGetChildChannelWithInstanceCode:(NSString *)instanceCode withParentId:(NSString *)parentId withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.2	获取频道信息
 *
 *  @param channelId    频道ID，（String 不可为空）
 *  @param instanceCode 医院编号，（String 可为空）
 *  @param homeFlag     首页显示标记，（String 不可为空）
 *  @param maxCount     子频道最大记录数，（String 不可为空）
 *  @param returnBlock  返回数据
 */
- (void)YJHealthyVodChannelInfoWithChannelId:(NSString *)channelId withInstanceCode:(NSString *)instanceCode withHomeFlag:(NSString *)homeFlag withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.3	获取频道过滤器列表
 *
 *  @param channelId   频道ID，（String 不可为空）
 *  @param includeYear 是否包括年份过滤器，（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJHealthyVodChannelFiltersWithChannelId:(NSString *)channelId withIncludeYear:(NSString *)includeYear
 andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.4	获取子频道节目列表
 *
 *  @param channelId   频道ID，（JSON数组 不可为空）
 *  @param maxCount    节目最大记录数，（String 不可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJHealthyVodChannelProgramListWithChannelId:(NSMutableArray *)channelIdArr withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.5	获取节目列表
 *
 *  @param channelId   频道ID，（String 不可为空）
 *  @param pageNo      页码，（String 不可为空）
 *  @param pageSize    分页大小，（String 不可为空）
 *  @param keyWord     查询关键字，（String 可为空，该字段为保留字段，搜索功能暂不实现）
 *  @param filter      过滤器列表，（JSON对象数组， 可为空）
 *  @param returnBlock 返回数据
 */
- (void)YJHealthyVodProgramListWithChannelId:(NSString *)channelId withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize withKeyword:(NSString *)keyWord withFilter:(NSMutableArray *)filterArr andReturn:(HTPolymedicineRequest_Return)returnBlock;


//-----------------健康电视2.0------

/**
 *  2.6	获取节目视频播放地址
 *
 *  @param programId    节目ID，（String 不可为空）
 *  @param mobile       手机号
 *  @param instanceCode  医院实例编码
 *  @param returnBlock
 */
- (void)YJHealthyVodMediaPlayListWithProgramId:(NSString *)programId withMobile:(NSString *)mobile withInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.7	推荐节目
 *
 *  @param programId   节目ID，（String 不可为空）
 *  @param maxCount    最多返回多少条推荐视频，（String 不可为空）
 *  @param returnBlock
 */
- (void)YJHealthyVodRecommendProgramWithProgramId:(NSString *)programId withMaxCount:(NSString *)maxCount andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.8	获取收藏夹中节目
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param pageNo       页码（String不可为空）
 *  @param pageSize     分页大小（String不可为空）
 *  @param returnBlock
 */
- (void)YJHealthyVodListFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withPageNo:(NSString *)pageNo withPageSize:(NSString *)pageSize andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.9	节目加入收藏夹
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param programId    节目ID（String不可为空）
 *  @param returnBlock
 */
- (void)YJHealthyVodAddFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramId:(NSString *)programId andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.10	删除收藏夹中的节目
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param programIds   节目ID列表（String不可为空，多个ID采用逗号分割）
 *  @param returnBlock
 */
- (void)YJHealthyVodDelFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile withProgramIds:(NSString *)programIds andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.11	清空收藏夹
 *
 *  @param instanceCode 实例编码（String不可为空）
 *  @param mobile       手机号码（String不可为空）
 *  @param returnBlock
 */
- (void)YJHealthyVodCleanFavoriteWithInstanceCode:(NSString *)instanceCode withMobile:(NSString *)mobile andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.14    获取一级频道
 *
 *  @param instanceCode 医院实例编码（String 可为空）
 *  @param returnBlock
 */
- (void)YJHealthyVodGetTopChannelWithInstanceCode:(NSString *)instanceCode andReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.15    获取精选频道列表
 *
 *  @param returnBlock
 */
- (void)YJHealthyGetSelectedChannelWithReturn:(HTPolymedicineRequest_Return)returnBlock;


/**
 *  2.16    获取热门频道与节目列表
 *
 *  @param returnBlock
 */
- (void)YJHealthyVodGetHotChannelWithReturn:(HTPolymedicineRequest_Return)returnBlock;

@end
