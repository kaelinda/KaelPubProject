//
//  AppDelegate.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AsyncSocket.h"
#import "AsyncUdpSocket.h"
//#import "UMSocial.h"
#import <UMSocialCore/UMSocialCore.h>
#import "UMSocialQQHandler.h"
#import "UMSocialWechatHandler.h"
#import "BPush.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
//#import "UGCLiveManager.h"
#import <notify.h>

#define NotificationLock CFSTR("com.apple.springboard.lockcomplete")

#define NotificationChange CFSTR("com.apple.springboard.lockstate")

#define NotificationPwdUI CFSTR("com.apple.springboard.hasBlankedScreen")

@interface AppDelegate : UIResponder <UIApplicationDelegate,AsyncUdpSocketDelegate,UIScrollViewDelegate,UIViewControllerTransitioningDelegate>

{
    
    AsyncUdpSocket *_sendSocket;//发送广播的 套接字
    AsyncUdpSocket *_receiveSocket;//接收广播的 套接字 （因为发送和接收的 端口不同 所以 要用不同的套接字）
    
    AsyncUdpSocket *_TVSocket;//服务器负责接收因为单播中端口都是同一个所以只需要一个socket就行了
    
    NSString *_message;
    BOOL _CanDisplay;//本地通知重复提示的开关
    BOOL _isLaunch;//是否走了didFinisnLaunch

    
    NSString *_pointToPointIP;
    UIImageView *_imagev;
    
}

@property (nonatomic,strong) NSMutableArray *localNotiArr;//本地通知的数组
@property (nonatomic,strong) NSMutableDictionary *localNotiDic;//本地通知的字典 携带节目信息

@property (strong,nonatomic) NSMutableArray *IPArray;
//socket & UDP 传输信息
@property (strong,nonatomic) AsyncUdpSocket *TVSocket;
@property (strong,nonatomic) NSString *pointToPointIP;
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,strong) UIScrollView *ADScrollView;//广告图
@property (nonatomic,strong) UIScrollView *guideScrollView;//引导图
@property (nonatomic,assign) BOOL isLocked;


@end

