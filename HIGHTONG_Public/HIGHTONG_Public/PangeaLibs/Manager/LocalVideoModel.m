//
//  LocalVideoModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "LocalVideoModel.h"

@implementation LocalVideoModel
- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)liveCollectionWithDict:(NSDictionary *)dict;
{
    return  [[self alloc]initWithDict:dict];
}
+ (instancetype)eventCollectionWithDict:(NSDictionary *)dict
{
    return  [[self alloc]initWithDict:dict];
}
+ (instancetype)timeMoveCollectionWithDict:(NSDictionary *)dict
{
    return  [[self alloc]initWithDict:dict];
}



-(void)resetLocalVideoModel
{
    _lvID = nil;
    _lvEventID = nil;
    _lvEventName = nil;
    _lvServiceName = nil;
    _lvBreakPoint = nil;
    _lvChannelImage = nil;
    _lvStartTime = nil;
    _lvEndTime = nil;
    _lvCONTENTID = nil;
    _lvNAME = nil;
    _lvMultipleType = nil;
    _lvname = nil;
    _lvDefinition = nil;
    _lvLastPlayEpisode = nil;
    _lvLastEpisodeTitle = nil;
    _lvAmount = nil;
    _lvImageLink = nil;
    _lvStyle = nil;
    _lvFullStartTime = nil;
}


@end
