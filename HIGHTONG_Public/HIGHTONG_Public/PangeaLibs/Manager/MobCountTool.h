//
//  MobCountTool.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/18.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobCountTool : NSObject

+ (void)pushInWithPageView:(NSString *)pageView;

+ (void)popOutWithPageView:(NSString *)pageView;


@end
