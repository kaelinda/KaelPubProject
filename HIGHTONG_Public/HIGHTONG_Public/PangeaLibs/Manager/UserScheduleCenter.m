//
//  UserScheduleCenter.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/28.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "UserScheduleCenter.h"

@implementation UserScheduleCenter


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _didScheduledArr = [[NSMutableArray alloc] init];

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshScheduelChannelList) name:REFRESH_THE_SCHEDUEL_CHANNEL_LIST object:nil];
    }
    return self;
}

+ (id)defaultCenter
{
    static UserScheduleCenter * center;
    
    if (!center) {
        center = [[UserScheduleCenter alloc]init];
    }
    
    return center;
}
- (void)refreshScheduelChannelList
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request EventRevserveWithTimeou:10];
}

- (BOOL)checkTheScheduledListwithID:(NSString *)eventID
{
    for (int i = 0; i<_didScheduledArr.count; i++) {
        
//        NSDictionary *video = [[NSDictionary alloc] init];
//        
//        video = [_didScheduledArr objectAtIndex:i];
        
        NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_didScheduledArr objectAtIndex:i]];
        
        NSLog(@"video*****%@",[video objectForKey:@"eventID"]);
        
        if ([[video objectForKey:@"eventID"] isEqualToString:[NSString stringWithFormat:@"%@",eventID]]) {
            return YES;
        }

    }
    
    return NO;
}
//- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:EPG_EVENTRESERVELIST]) {
        //预约节目列表
        if ([[result objectForKey:@"ret"] integerValue] == 0 ||[[result objectForKey:@"ret"] integerValue] == -1) {
            
//            _didScheduledArr = [result objectForKey:@"eventlist"];
            _didScheduledArr = [NSMutableArray arrayWithArray:[result objectForKey:@"eventlist"]];
            if (_didScheduledArr>0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:_didScheduledArr userInfo:nil];

            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil userInfo:nil];

            }
            
        //------kael
            if (_finishedBlcok) {
                _finishedBlcok([_didScheduledArr copy]);
            }
        }
    }
}

- (NSArray *)getScheduleArr{

    return _didScheduledArr;
}


//- (NSMutableArray *)didScheduledArr
//{
//    if (!_didScheduledArr) {
//        _didScheduledArr = [NSMutableArray array];
//    }
//    
//    return _didScheduledArr;
//}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Kael新添加方法
-(void)refreshUserSchedule:(FinishedRefeshBlock)didFinishedBlock{
    _finishedBlcok = didFinishedBlock;
    [self refreshScheduelChannelList];
}

-(void)addEventWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink{
   
    NSMutableDictionary *scheduleDic = [NSMutableDictionary new];
    [scheduleDic setObject:serviceID.length>0?serviceID:@"" forKey:@"serviceID"];
    [scheduleDic setObject:serviceName.length>0?serviceName:@"" forKey:@"serviceName"];
    [scheduleDic setObject:eventID.length>0?eventID:@"" forKey:@"eventID"];
    [scheduleDic setObject:eventName.length>0?eventName:@"" forKey:@"eventName"];
    [scheduleDic setObject:startTime.length>0?startTime:@"" forKey:@"startTime"];
    [scheduleDic setObject:endTime.length>0?endTime:@"" forKey:@"endTime"];
    [scheduleDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [scheduleDic setObject:levelImageLink.length>0?levelImageLink:@"" forKey:@"levelImageLink"];
    
    [_didScheduledArr addObject:scheduleDic];
}

-(void)deletEventWith:(NSString *)eventID{

    for (int i = 0; i<_didScheduledArr.count; i++) {
        
        NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_didScheduledArr objectAtIndex:i]];
        
        if ([[video objectForKey:@"eventID"] isEqualToString:[NSString stringWithFormat:@"%@",eventID]]) {
            [_didScheduledArr removeObjectAtIndex:i];
            NSLog(@"成功删除预约节目");
            return;
        }
        
    }

    NSLog(@"删除预约节目失败~");
}


//存在着取消预约和预约节目请求失败的情况，头端未上传或者未删除，但本地有----所以本地添加和删除的方法放在请求的回调中处理，此处不需再处理
-(void)cleanScheduleList
{
    [_didScheduledArr removeAllObjects];
}


@end
