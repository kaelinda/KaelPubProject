//
//  DataPlist.h
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPlist : NSObject
//   *******   读取  本地  plist  文件  ********
+(NSMutableArray *)ReadToFileArr:(NSString *)Plist;

//   *******   写入  本地  plist  文件  ********
+(void)WriteToPlistArray:(NSMutableArray *)array ToFile:(NSString *)Plist;


+(NSMutableDictionary *)ReadToFileDic:(NSString *)Plist;

+(void)WriteToPlistDictionary:(NSMutableDictionary *)dic ToFile:(NSString *)Plist;



@end
