//
//  HTYJUserCenter.m
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/4/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "HTYJUserCenter.h"
#import "UserScheduleCenter.h"
#import "LocalCollectAndPlayRecorder.h"
#import "UserToolClass.h"
#import "UGCLocalListCenter.h"
#import "KLocalNotificationManager.h"

static HTYJUserCenter *shared = nil;

@interface HTYJUserCenter ()<HTRequestDelegate>
{
    NSTimer *_autoLoginTimer;//自动登录
    
    NSDate *_requestDate;//加载时间
    
    BOOL requestYES;
}
@property (nonatomic, strong)LocalCollectAndPlayRecorder *localListCenter;
@end

@implementation HTYJUserCenter


+ (instancetype)userCenter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc] init];
    });
    
    return shared;
}


- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
//        _requestDate = [NSDate date];
        
        requestYES = YES;
        
        HTRequest *requst  = [[HTRequest alloc]initWithDelegate:self];
        [requst CNCommonAppConfig];//请求产品配置
        
        _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
        [self operationInAppdelegate];
    }
    
    return self;
}

- (BOOL)RequstEnabledWithFirstTime:(NSDate*)date andSecondData:(NSDate*)date1
{
    if ([date1 timeIntervalSinceDate:date] >= 5) {
        NSLog(@"两次请求时间差值够了，请求去吧");
        return YES;
    }
    return NO;
}

- (void)saveAuthCodeLoginDataWithDic:(NSDictionary *)requestDic andPhoneNum:(NSString *)phone
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:YES forKey:@"isGetUserInfo"];
    
    [userDefaults setBool:YES forKey:@"isRequestUserList"];
   
    [userDefaults setObject:phone forKey:@"msgMobile"];
    
    [userDefaults setObject:phone forKey:@"mindUserName"];//账号——手机号
    
    [userDefaults setObject:[requestDic objectForKey:@"identityToken"] forKey:@"idToken"];
    
    NSString *uStr = [requestDic objectForKey:@"u"];
    
    [userDefaults setObject:uStr.length ? uStr:@"" forKey:@"UserID"];
    
    [userDefaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AUTOLOGIN" object:nil];//自动登录的通知
}


//获取用户的所有数据列表
- (void)getUserList
{
    [_localListCenter cleanUserAllList];
    
    if (IS_EPG_CONFIGURED) {//内网
        
        HTRequest *VODRequest = [[HTRequest alloc] initWithDelegate:self];
        [VODRequest VODNewFavorateListWithStartSeq:1 andCount:100 andTimeout:10];//点播收藏
        
        HTRequest *epgRequest = [[HTRequest alloc] initWithDelegate:self];
        [epgRequest EventPlayListWithEventID:@"" andCount:100 andTimeout:10];//直播播放记录
        
        
        HTRequest *demandRequest = [[HTRequest alloc] initWithDelegate:self];
        [demandRequest VODNewPlayRecordListWithStartSeq:1 andCount:100 andSatrtDte:@"" andEndDate:@"" andTimeout:10];//点播播放记录
        
        
        HTRequest *TVFavRequest = [[HTRequest alloc] initWithDelegate:self];
        [TVFavRequest TVFavorateListWithTimeout:10];//直播收藏
        
        
        HTRequest *eventRequest = [[HTRequest alloc] initWithDelegate:self];
        [eventRequest EventRevserveWithTimeou:10];//预约节目列表

    }
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    if (isUsePolymedicine_2_0) {//健康视频收藏
        
        [request YJHealthyVodListFavoriteWithInstanceCode:KInstanceCode withMobile:KMobileNum withPageNo:@"1" withPageSize:@"100"];
//        return;
    }else{
        
        [request YJCdnVideoListFavoriteWithInstanceCode:KInstanceCode withPageNo:@"1" withPageSize:@"100" withMobile:KMobileNum];
    }

    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRequestUserList"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)operationInAppdelegate
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGetIDToken) name:@"GET_ID_TOKEN" object:nil];//获取身份token通知
    
    /**
     下面是获取鉴权token通知
     用途一：各接口报-2时调用
     用途二：与web交互时JS调用refreshToken这个方法时也调用
     用途三：每次运行应用时，为确保token的有效性都会调用
     **/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(repeatGetAuthenticToken) name:@"GET_AUTHENTIC_TOKEN" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoGetAuthenticToken) name:@"AUTOLOGIN" object:nil];//接收 自动登录的通知
    if (IsLogin) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isGetUserInfo"];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isGetUserInfo"];
    }
   
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *pwdStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"];
    if (!isAuthCodeLogin && IsLogin) {//针对原账号密码登录，多人用同一账号，一人修改了密码，他人再次启用APP时应重新登录，已判断出密码的改变
        //!isAuthCodeLogin && [pwdStr length]  、、!isAuthCodeLogin && IsLogin
        
        NSLog(@"%lu",(unsigned long)pwdStr.length);
        NSLog(@"哈哈哈，不该来的来了");
        HTRequest *userRequest = [[HTRequest alloc] initWithDelegate:self];
        [userRequest mobileLoginWithMobilePhone:[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"] andPassWord:[[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"] andTimeout:10]; 
    }else{//游客和动态口令登录
        
        //获取鉴权token
        [self repeatGetAuthenticToken];
    }
    
    if (IsLogin) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRequestUserList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRequestUserList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    HTRequest *academicTitleRequest = [[HTRequest alloc] initWithDelegate:self];
    [academicTitleRequest UGCCommonGetAcademicTitleWithReturn];//获取医生职称接口
    
    [UGCLocalListCenter defaultCenter];
}


//每十分钟获取一次鉴权token
- (void)autoGetAuthenticToken
{
    if (_autoLoginTimer == nil) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(100.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            _autoLoginTimer = [NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(repeatGetAuthenticToken) userInfo:nil repeats:YES];
        });
    }
}

//根据identityToken获取鉴权token
- (void)repeatGetAuthenticToken
{
    
    if ([self RequstEnabledWithFirstTime:_requestDate andSecondData:[NSDate date]] || requestYES) {
        
//        [self refreshHomeInViewWillAppear];
        
        
        
        
        //网络环境变化的时候需要清理本地通知----预约列表刷新完毕之后会自动重新注册本地通知
        [[KLocalNotificationManager shareLocalNotificationManager] removeAllLocalNotification];
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        NSString *IDToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"idToken"];
        
        if (IDToken.length) {
            
            [request UPORTALGetTokenWithTimeout:10];
            
            return;
        }else{
            
            [self userGetIDToken];
        }
        
        
    }
    
    

}

//无法获取鉴权token后，调用此方法获取identityToken
- (void)userGetIDToken
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    if (IsLogin) {//用户
        
        if (isAuthCodeLogin) {//动态口令登录
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IsLogin" ];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [request guestLoginWithTimeout:10];
            
            return;
        }else{//原账号密码登录用户
            
            [request mobileLoginWithMobilePhone:[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"] andPassWord:[[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"] andTimeout:10];
            
            return;
        }
    }else{//游客
        
        [request guestLoginWithTimeout:10];
    }
}

/**
 *  原手机号、密码登录成功后——保存用户登录信息   手机号、密码、登录状态等等
 */
+ (void)saveUserLoginProfileWithDic:(NSDictionary *)resultDic andUserAccount:(NSString *)account andUserPassword:(NSString *)password
{
    //记住密码
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account forKey:@"msgMobile"];
    [userDefaults setObject:account forKey:@"mindUserName"];//账号——手机号
    [userDefaults setObject:[password length]?password:@"" forKey:@"mindPWD"];//密码
    
    //登录状态
    [userDefaults setBool:YES forKey:@"IsLogin"];
    NSLog(@"++++++++++++++++++%d",[[NSUserDefaults standardUserDefaults] boolForKey:@"IsLogin"]);
    [userDefaults synchronize];
    
    //存token
    [self saveTokenWithDic:resultDic];
}

//获取鉴权token成功后的相关操作
- (void)relativeOperationAfterGetAuthenticTokenSuccessWithResultDic:(NSDictionary *)resultDic
{
    NSString *tokenStr = [NSString stringWithFormat:@"%@",[resultDic objectForKey:@"token"]];
    
    if (tokenStr.length) {
        
        [[NSUserDefaults standardUserDefaults] setObject:tokenStr forKey:@"token"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
//    NSLog(@"IsLogin____%hhd,isAuthCodeLogin____%d",IsLogin,isAuthCodeLogin);
    if ((IsLogin && isAuthCodeLogin) || (IsLogin && !isAuthCodeLogin)) {//动态口令用户登录 或者 账号密码用户登录
        
        //IsLogin状态下，每次发起获取鉴权token的请求操作成功后，重新获取一遍用户数据列表
        [self getUserList];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isRequestUserList"]) {
            
            //[self getUserList];//用户登陆状态启动APP时要获取一次列表数据；
            
            //开机如果是登录状态就做一次“判断是否合法主播”的请求
            HTRequest *legalRequest = [[HTRequest alloc] initWithDelegate:self];
            [legalRequest UGCCommonIsValidUser];
        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isGetUserInfo"]) {
            
            //登陆成功，请求用户信息
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            [request NewqueryUserInfoWithTimeout:KTimeout];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil];
        
    }else{//游客

        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeAllLocalNotification" object:nil];//清空本地通知
    }
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshHome" object:nil];
    
    //请求广告——用户分组
    HTRequest *requestUserG = [[HTRequest alloc] initWithDelegate:self];
    [requestUserG ADUserGroupWithTimeout:KTimeout];
}


/**
 *  进应用就该有的token
 *
 *  @param resultDic 传入
 */
+ (void)saveTokenWithDic:(NSDictionary *)resultDic
{
    //存储登陆之后的数据
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:[resultDic objectForKey:@"identityToken"] forKey:@"idToken"];
    
    NSString *uStr = [resultDic objectForKey:@"u"];
    
    [userDefaults setObject:uStr.length ? uStr:@"" forKey:@"UserID"];
    
    NSLog(@"呦西呦西%@",[userDefaults objectForKey:@"UserID"]);
    
    if ([[resultDic objectForKey:@"token"] length]>0) {
        
        [userDefaults setObject:[resultDic objectForKey:@"token"] forKey:@"token"];
        
    }
        
    [userDefaults synchronize];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:shared];
    [request UPORTALGetTokenWithTimeout:10];
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
    
    if ([type isEqualToString:@"mobileLogin"]) {
        
        NSLog(@"%ld",(long)[[result objectForKey:@"ret"] integerValue]);
        if (result.count>0) {
            
            if ([[result objectForKey:@"ret"] integerValue] == 0 && (status == 0)) {//手机号登录成功
                
                NSLog(@"手机-------------------登陆成功");
        
                //存储用户登录后信息
                NSString *mobileStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"];
                NSString *pwdStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"mindPWD"];
                
                [HTYJUserCenter saveUserLoginProfileWithDic:result andUserAccount:mobileStr andUserPassword:pwdStr];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isGetUserInfo"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRequestUserList"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{//不成功===游客登录
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IsLogin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                [request guestLoginWithTimeout:10];
                
            }
            

        }
    }
    else if ([type isEqualToString:guestLogin]){//游客登录
        if ([[result objectForKey:@"ret"] integerValue]==0 && (status == 0) && [result objectForKey:@"ret"]) {
            NSLog(@"+++++++++++++++游客上线啦=====%@", result);
            
            //存储信息
            [HTYJUserCenter saveTokenWithDic:result];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AuthenticSuccessed"];//置返认证状态
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isGetUserInfo"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRequestUserList"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }else{
            NSLog(@"youkedenglushibai");
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GuestLoginFailed" object:nil];
        }
    }
    else if ([type isEqualToString:UGetToken]){//根据identityToken获取鉴权token接口
        
        if (result.count >0) {
            if ([[result objectForKey:@"ret"] integerValue]==0) {//获取成功
                
                //原用户、动态口令、游客三种状态的相关操作
                [self relativeOperationAfterGetAuthenticTokenSuccessWithResultDic:result];
                [[HT_UsercollectionService shareUserCollectionservice] changeUser:YES];//获取上传信息采集的配置文件

                _requestDate = [NSDate date];
                
                requestYES = NO;
            }
//            else{//获取失败
//                
//                [self userGetIDToken];// 此处不再做else的操作，init中注册的调取这个方法的通知，在数据包中获取鉴权token失败返回-2的时候会发送消息调取这个方法
//            }
        }
    }
    else if ([type isEqualToString:AD_USER_GROUP]){
        
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{//success!!
                    NSLog(@"请求GROUPID成功");
                    
                    NSString *groupStr = [NSString stringWithFormat:@"%@",[result objectForKey:@"groupID"]];
                    NSLog(@"用户分组ID：%@",groupStr);
                    
                    [[NSUserDefaults standardUserDefaults] setObject:groupStr forKey:@"groupID"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSLog(@"转存的分组ID：%@",AD_GroupID);
                    
                    break;
                }
                case -1:{//无用户分组
                    
                    break;
                }
                case -9:{//失败（连接数据库失败或者参数错误）
                    //request again
                    HTRequest *requestUserG = [[HTRequest alloc] initWithDelegate:self];
                    [requestUserG ADUserGroupWithTimeout:10];
                    
                    break;
                }
            }
        }
    }
    else if ([type isEqualToString:queryUserInfo]) {//登陆成功后请求用户信息
        
        NSLog(@"status = %ld  result = %@  Type = %@",(long)status,result,type);
        
        if (result.count > 0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"请求用户信息成功");
                    
                    [HTYJUserCenter saveUserDetailProfileWithDic:result];
                    
                    break;
                }
                case -2:{
                    
                    break;
                }
                case -9:{
                    break;
                }
            }
        }
    }
    else if ([type isEqualToString:VOD_NewFavorateList]){//点播收藏列表
        
        NSLog(@"点播收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"count"] integerValue] > 0) {//++++成功
                
                NSArray *arr = [NSArray arrayWithArray:[result objectForKey:@"vodList"]];
                
                if (arr.count) {
                    
                    [_localListCenter writeListArrInPlistWithPlistName:VODFAVORITEPLIST andListArr:arr];
                }
            }else if ([[result objectForKey:@"count"] integerValue]==0){
                
                NSLog(@"查询结果为空");
                
                return;
                
            }else if ([[result objectForKey:@"count"] integerValue]==-2){
                
                NSLog(@"用户未登录或者登录超时");
                
                return;
                
            }else if ([[result objectForKey:@"count"] integerValue]==-9){
                
                NSLog(@"失败（连接个人中心失败或者参数错误）");
                
                return;
                
            }else{
                
                return;
            }
        }
    }
    else if ([type isEqualToString:YJ_CDN_VIDEO_LISTFAVORITE]){//我的医视频
        
        NSLog(@"我的医视频请求成功结果:%ld result:%@  type:%@",(long)status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {//++++成功
                
                [_localListCenter writeListArrInPlistWithPlistName:USER_HEALTHY_COLLECTION_PLIST andListArr:[NSArray arrayWithArray:[result objectForKey:@"list"]]];
            }else{
                
                return;
            }
        }
    }
    else if ([type isEqualToString:YJ_HEALTHYVOD_LISTFAVORITE]){//我的医视频
        
        NSLog(@"我的医视频请求成功结果:%ld result:%@  type:%@",(long)status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {//++++成功
                
                NSArray *arr = [NSArray arrayWithArray:[result objectForKey:@"list"]];
                if (arr.count) {
                    
                    [_localListCenter writeListArrInPlistWithPlistName:USER_HEALTHY_COLLECTION_PLIST andListArr:arr];
                }
                
            }else{
                
                return;
            }
        }
    }
    else if ([type isEqualToString:EPG_TVFAVORATELIST]) {//直播收藏列表
        
        NSLog(@"直播收藏列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"++++成功");
                    
                    NSArray *tempArr = [NSArray arrayWithArray:[result objectForKey:@"servicelist"]];
                    
                    if (tempArr.count > 0) {
                        
                        [_localListCenter writeListArrInPlistWithPlistName:LIVEFAVORITEPLIST andListArr:tempArr];
                    }

                    break;
                }
                case -1:{
                    NSLog(@"收藏列表为空");
                    
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    break;
                }
                    
                default:
                    
                    break;
            }
        }
    }
    else if ([type isEqualToString:EPG_EVENTPLAYLIST]) {//回看播放记录列表
        
        NSLog(@"回看播放记录列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    NSLog(@"回看记录列表请求成功");
                    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[result objectForKey:@"eventlist"]];
                    
                    if (tempArr.count>0) {
                        
                        NSMutableArray *reMakeArr = [UserToolClass dependIfTheSameWeekCompareWithTodayDateGroupWithArray:tempArr];
                        
                        [_localListCenter writeListArrInPlistWithPlistName:REWATCHPLAYRECORDERPLIST andListArr:[NSArray arrayWithArray:reMakeArr]];
                    }
                    
                    break;
                }
                case -1:{
                    NSLog(@"没有符合条件的回看播放记录");
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    break;
                }
                    
                default:
                break;
            }
        }
    }
    else if ([type isEqualToString:VOD_NewPlayRecordList]){//点播播放记录列表
        
        NSLog(@"点播播放记录列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        
        if (result.count>0) {
            
            if ([[result objectForKey:@"count"] integerValue] > 0) {
                
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[result objectForKey:@"vodList"]];
                
                if (tempArr.count>0) {
                    
                    NSMutableArray *reMakeArr = [UserToolClass dependIfTheSameWeekCompareWithTodayDateGroupWithArray:tempArr];
                    
                    [_localListCenter writeListArrInPlistWithPlistName:PLAYRECORDERPLIST andListArr:[NSArray arrayWithArray:reMakeArr]];
                }
                
            }else if ([[result objectForKey:@"count"] integerValue]==0){
                
                return;
                
            }else if ([[result objectForKey:@"count"] integerValue]==-2){
                
                return;
                
            }else if ([[result objectForKey:@"count"] integerValue]==-9){
                
                return;
                
            }else{
                
                return;
            }
        }
    }
    else if ([type isEqualToString:EPG_EVENTRESERVELIST]) {//预约节目列表
        
        NSLog(@"预约节目列表请求成功结果: %ld result:%@  type:%@",status,result,type );
        if (result.count>0) {
            switch ([[result objectForKey:@"ret"] integerValue]) {
                case 0:{
                    
                    NSMutableArray *oriArr = [NSMutableArray arrayWithArray:[result objectForKey:@"eventlist"]];
                    
                    if (oriArr>0) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:oriArr userInfo:nil];
                        
                        [_localListCenter writeListArrInPlistWithPlistName:USER_SCHEDULE_LIST_PLIST andListArr:[NSArray arrayWithArray:oriArr]];
                    }else{
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil userInfo:nil];
                    }
                    
                    break;
                }
                case -1:{
                    NSLog(@"预约列表为空");
                    
                    break;
                }
                case -2:{
                    NSLog(@"用户未登录或者登录超时");
                    
                    break;
                }
                case -9:{
                    NSLog(@"失败（连接个人中心失败或者参数错误）");
                    
                    break;
                }
                    
                default:
                    
                    break;
            }
        }
    }
    else if ([type isEqualToString:UGC_COMMON_ISVALIDUSER]) {////2.1    判断是否合法主播
        NSLog(@"认证状态界面返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,status,type);
    
        switch ([[result objectForKey:@"ret"] integerValue]) {
            case 0:
            {//合法主播
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                //1、存认证成功的标记
                [userDefaults setBool:YES forKey:@"AuthenticSuccessed"];
                
                //防止用户进入app自动登录后获取网络误差
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UGC_ISVALIDUSER_SUCCESS" object:nil];
                    
                });
                
                //2、下面的key统一存储认证状态
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                
                //3、存相应数据
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[result objectForKey:@"channelId"] length]>0?[result objectForKey:@"channelId"]:@"",@"channelId",[[result objectForKey:@"name"] length]>0?[result objectForKey:@"name"]:@"",@"name",[[result objectForKey:@"hospital"] length]>0?[result objectForKey:@"hospital"]:@"",@"hospital",[[result objectForKey:@"department"] length]>0?[result objectForKey:@"department"]:@"",@"department",[[result objectForKey:@"academicTitle"] length]>0?[result objectForKey:@"academicTitle"]:@"",@"academicTitle", nil];
                
                [userDefaults setObject:dic forKey:@"AuthenticSuccessedData"];
                
                [userDefaults synchronize];
                
                if (_authCallBack) {
                    _authCallBack(KAuthenticed,dic);
                }
                
                _authCallBack = nil;
                
                break;
            }
            case 1:
            {//未认证
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                if (_authCallBack) {
                    _authCallBack(KunAuthentic,[NSDictionary dictionary]);
                }
                
                _authCallBack = nil;
                
                break;
            }
            case 2:
            {//认证中
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];

                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                if (_authCallBack) {
                    _authCallBack(KAuthenticing,[NSDictionary dictionary]);
                }
                
                _authCallBack = nil;
                
                break;
            }
            case 3:
            {//审核不通过
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];
                
                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                if (_authCallBack) {
                    _authCallBack(KAuthenticFailed,[NSDictionary dictionary]);
                }
                
                _authCallBack = nil;
                
                break;
            }
            case -2:
            {//身份认证有问题
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];

                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                NSLog(@"认证状态--------------------------  -2");
                
                if (_authCallBack) {
                    _authCallBack(KAuthenticing,[NSDictionary dictionary]);
                }
                
                _authCallBack = nil;
                
                break;
            }
            case -9:
            {//其它异常（连接数据库失败或者参数错误等）
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];

                [userDefaults setObject:[NSString stringWithFormat:@"%@",[result objectForKey:@"ret"]] forKey:@"AuthenticStatus"];
                [userDefaults synchronize];
                
                if (_authCallBack) {
                    _authCallBack(KAuthenticFailed,[NSDictionary dictionary]);
                }
                
                _authCallBack = nil;
                
                break;
            }
        }
    }
    else if ([type isEqualToString:UGC_COMMON_GETACADEMICTITLE]) {//医生职称列表
        if ([[result objectForKey:@"ret"] integerValue] == 0) {
            
            NSLog(@"医生职称列表\n%@",[result objectForKey:@"list"]);
            
            [[UGCLocalListCenter alloc] writeUGCListArrInPlistWithPlistName:UGC_ACADEMIC_TITLE_LIST_PLIST andListArr:[NSArray arrayWithArray:[result objectForKey:@"list"]]];
        }
    }
}


/**
 *  保存‘获取用户信息’得到的用户信息
 */
+ (void)saveUserDetailProfileWithDic:(NSDictionary *)resultDic
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:NO forKey:@"isGetUserInfo"];
    
    [userDefaults setObject:[resultDic objectForKey:@"nickName"] forKey:@"NickName"];
    [userDefaults setObject:[resultDic objectForKey:@"score"] forKey:@"Userscore"];
    [userDefaults setObject:[resultDic objectForKey:@"times"] forKey:@"loginTimes"];
    
    
    NSString *photoStr = [NSString stringWithString:NSStringPM_IMGFormat([resultDic objectForKey:@"photoLink"])];
    [userDefaults setObject:photoStr forKey:@"HeaderPhoto"];
    [userDefaults synchronize];
    
    //账号界面赋值
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANG_ACCOUNT_IMAGE" object:nil];
}

/**
 *  用户退出清除用户信息
 */
+ (void)logoutCleanUserProfile
{
 
    [[HT_UsercollectionService shareUserCollectionservice] changeUser:NO];//获取上传信息采集的配置文件
    //end

    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:NO forKey:@"IsLogin"];
    [userDefaults setObject:@"" forKey:@"token"];
    [userDefaults setObject:@"" forKey:@"idToken"];
    [userDefaults setObject:@"" forKey:@"mindPWD"];//不清除账号——要求退出后登录框的账号框有账号
    [userDefaults setObject:@"" forKey:@"NickName"];
    [userDefaults setObject:@"" forKey:@"Userscore"];
    [userDefaults setObject:@"" forKey:@"loginTimes"];
    [userDefaults setObject:@"" forKey:@"UserID"];
    
    [userDefaults setObject:@"" forKey:@"UGCAccessToken"];//当虹云token清空

    [userDefaults setBool:NO forKey:@"AuthenticSuccessed"];//置返认证状态
    
    [userDefaults setObject:@"" forKey:@"HeaderPhoto"];//用户头像
    
    NSLog(@"++++++++++++++++++++%d",[[NSUserDefaults standardUserDefaults] boolForKey:@"IsLogin"]);
    
    //原始签到所需
    NSString *iden = [NSString stringWithFormat:@"%@%@",@"IsFirstSignIn",[[NSUserDefaults standardUserDefaults] objectForKey:@"mindUserName"]];
    [userDefaults setBool:NO forKey:iden];//判断账号是不是第一次签到用的
    [userDefaults setBool:NO forKey:@"itIsMe"];//给签到按钮用的
    
    [userDefaults synchronize];
    
    //用户注销成功后，游客上线
    HTRequest *request = [[HTRequest alloc] initWithDelegate:shared];
    [request guestLoginWithTimeout:10];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOG_OUT_SUCCESS" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANG_ACCOUNT_IMAGE" object:nil];
}


- (void)checkDoctorAuthWithCallBack:(authCallBack)callBack
{
    _authCallBack = callBack;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AuthenticSuccessed"]) {

        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"AuthenticSuccessedData"]];
        
        callBack(KAuthenticed,dic);
    }else{
    //发请求

        if (IsLogin) {
            HTRequest *legalRequest = [[HTRequest alloc] initWithDelegate:self];
            [legalRequest UGCCommonIsValidUser];
        }
    }
}


//- (void)checkDoctorAuthWithCallBack:(void (^)(BOOL))callBack
//{
//    _authCallBack = callBack;
//    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AuthenticSuccessed"]) {
//        
//        callBack(YES);
//    }else{
//    //发请求
//        
//        if (IsLogin) {
//            HTRequest *legalRequest = [[HTRequest alloc] initWithDelegate:self];
//            [legalRequest UGCCommonIsValidUser];
//        }
//    }
//}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}












@end
