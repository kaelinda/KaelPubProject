//
//  HTVolumeUtil.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/6/30.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#define Volume_Change_Notification @"Volume_Change_Notification"

@interface HTVolumeUtil : NSObject


@property (nonatomic,assign) CGFloat volumeValue;

+ (HTVolumeUtil *) shareInstance;

-(void)loadMPVolumeView;

- (void)registerVolumeChangeEvent;

- (void)unregisterVolumeChangeEvent;
@end
