//
//  BMKSingleTon.m
//  LocationDemo
//
//  Created by 伟 吴 on 16/1/7.
//  Copyright © 2016年 伟 吴. All rights reserved.
//

#import "BMKSingleTon.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>


@interface BMKSingleTon ()<BMKLocationServiceDelegate,HTRequestDelegate>
{
    
    BMKMapManager *_mapManager;
    
    BMKLocationService *_locService;
    
    NSString *_locStr;//已拼接的经纬度
    
}
@end


@implementation BMKSingleTon

+ (id)shareInstance
{
    static BMKSingleTon *singleTon;
    
    if (!singleTon) {
        
        singleTon = [[BMKSingleTon alloc] init];
    }

    return singleTon;
}


- (id)init
{
    self = [super init];
    if (self) {
        
        _mapManager = [[BMKMapManager alloc] init];
        BOOL ret = [_mapManager start:BMK_LOCATION_KEY generalDelegate:nil];
        if (!ret) {
            
            NSLog(@"manager start failed!");
            
        }
        
        _locService = [[BMKLocationService alloc] init];
        _locService.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        _locService.distanceFilter = 100.0f;
        _locService.delegate = self;

        [_locService startUserLocationService];
        
    }
    return self;
}


- (void)restartBMKLocation
{
    _locService.delegate = self;
    [_locService startUserLocationService];
}


- (void)stopBMKLocation
{
    [_locService stopUserLocationService];
    _locService.delegate = nil;
}

/**
 *用户方向更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
    
    //NSLog(@"heading is %@",userLocation.heading);
    
    //NSLog(@"UserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
}


/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    NSLog(@"单例didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    
    _locStr = [NSString stringWithFormat:@"%f,%f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude];
    
    NSLog(@"拼接好的经纬度%@",_locStr);
    
    [[NSUserDefaults standardUserDefaults] setObject:_locStr forKey:@"location"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_LOCATION_SUCCESS" object:_locStr];
    
    
    
    
    
    
    NSString *latitude = [NSString stringWithFormat:@"%f",userLocation.location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",userLocation.location.coordinate.longitude];
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request YJIntroduceNearbyWithLongitude:longitude withLatitude:latitude];

    
    
    
    
    
    [_locService stopUserLocationService];
    _locService.delegate = nil;

}


- (NSDictionary *)getLocationCityAndHospital
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([[userDefault objectForKey:@"InstanceCode"] length]) {
        
        [dic setObject:[userDefault objectForKey:@"InstanceCode"] forKey:@"InstanceCode"];
    }
    if ([[userDefault objectForKey:@"hospitalName"] length]) {
        
        [dic setObject:[userDefault objectForKey:@"hospitalName"] forKey:@"hospitalName"];
    }
    if ([[userDefault objectForKey:@"areaID"] length]) {
        
        [dic setObject:[userDefault objectForKey:@"areaID"] forKey:@"areaID"];
    }
    
    NSDictionary *returnDic = [NSDictionary dictionaryWithDictionary:dic];
    
    return returnDic;
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:YJ_INTRODUCE_NEARBY]) {
        
        NSLog(@"YJ_INTRODUCE_NEARBY----%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue]==0) {
            NSDictionary *region = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"info"]];
            NSString *code = [region objectForKey:@"instanceCode"];
            NSString *name = [region objectForKey:@"name"];
            NSString *areaId = [region objectForKey:@"areaId"];
            [[NSUserDefaults standardUserDefaults] setObject:code.length?code:@"" forKey:@"InstanceCode"];
            [[NSUserDefaults standardUserDefaults] setObject:name.length?name:@"" forKey:@"hospitalName"];
            [[NSUserDefaults standardUserDefaults] setObject:areaId.length?areaId:@"" forKey:@"areaID"];
            
            
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LocationSuccess"];
            
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GetNearByHospitalSuccess" object:nil];
            
        }
    }
}


/**
 *  定位失败
 *
 *  @param error 报错信息
 */
- (void)didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"你定位失败啦！！！！");
    
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LocationSuccess"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}


//- (void)dealloc
//{
//    //_locService.delegate = nil;
//    //_locService = nil;
//    //_mapManager = nil;
//}

@end
