//
//  PortalListModel.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/5/31.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYModel.h"

//#import "MJExtension.h"
//#import "PortalModel.h"

//--------------------------------------
@interface PortalModel : NSObject

@property (nonatomic,copy) NSString *projectName;

@property (nonatomic,copy) NSString *projectCode;

@property (nonatomic, copy) NSString *projectUrl;

@end
//--------------------------------------


@interface PortalListModel : NSObject

/**
 状态码
 */
@property (nonatomic,copy) NSString *ret;

/**
 版本号
 */
@property (nonatomic, copy) NSString *ver;

/**
 区域编码
 */
@property (nonatomic, copy) NSString *regionCode;

/**
 区域名称
 */
@property (nonatomic, copy) NSString *regionName;

/**
 系统对象列表
 */
@property (nonatomic, strong) NSMutableArray <PortalModel *>*projectList;


@end
