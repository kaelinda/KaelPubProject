//
//  PortalListModel.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/5/31.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import "PortalListModel.h"


@implementation PortalModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"projectName" : @"projectName",
             @"projectCode" : @"projectCode",
             @"projectUrl" : @"projectUrl"
             };
}

@end

@implementation PortalListModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _projectList = [[NSMutableArray alloc] init];
    }
    return self;
}

//-------YYModel
//+ (NSDictionary *)modelCustomPropertyMapper {
//    return @{@"projectList" : @"projectList"};
//}

+(NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"projectList": [PortalModel class]};
}

//------------------------------------------------------------
//------MJExtesion
/* 设置模型属性名和字典key之间的映射关系 */
//+ (NSDictionary *)replacedKeyFromPropertyName{
//    /* 返回的字典，key为模型属性名，value为转化的字典的多级key */
//    return @{@"list" : @"projectList"};
//}

//实现这个方法的目的：告诉MJExtension框架statuses和ads数组里面装的是什么模型
//*
//+ (NSDictionary *)objectClassInArray
//{
//    return @{
//             @"projectList" : [PortalModel class],
//             };
//}
//
//+ (Class)objectClassInArray:(NSString *)propertyName
//{
//    if ([propertyName isEqualToString:@"projectList"]) {
//        return [PortalModel class];
//    }
//    return nil;
//}
 //*/


@end
