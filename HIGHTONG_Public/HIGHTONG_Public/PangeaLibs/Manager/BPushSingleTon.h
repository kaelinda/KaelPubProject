//
//  BPushSingleTon.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/1/6.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^BPushCallBack)(id result, NSError *error);

@interface BPushSingleTon : NSObject

+ (id)shareInstance;
/*
* @brief 在didReceiveRemoteNotification中调用，用于推送反馈
* @param
*     userInfo
* @return
*     none
*/
- (void)handleNotification:(NSDictionary *)userInfo;

/**
 * @brief 向云推送注册 device token，只有在注册deviceToken后才可以绑定
 * @param
 *     deviceToken - 通过 AppDelegate 中的 didRegisterForRemoteNotificationsWithDeviceToken 回调获取
 * @return
 *     none
 */
- (void)registerDeviceToken:(NSData *)deviceToken;


/**
 * @brief 绑定channel.将会在回调中看获得channnelid appid userid 等。
 * @param
 *     none
 * @return
 *     none
 */

- (void)bindChannel;
//- (void)bindChannelWithCompleteHandler:(BPushCallBack)handler;


//用户用户已经更换了手机号，那么就应该更新绑定一下数据

-(void)bindUpdateChannel;


/**
 * @brief解除对 channel 的绑定。
 * @param
 *     none
 * @return
 *     none
 */
- (void)unbindChannelWithCompleteHandler:(BPushCallBack)handler;

/**
 * @brief设置tag。
 * @param
 *     tag - 需要设置的tag
 * @return
 *     none
 */
- (void)setTag:(NSString *)tag withCompleteHandler:(BPushCallBack)handler;

/**
 * @brief设置多个tag。
 * @param
 *     tags - 需要设置的tag数组
 * @return
 *     none
 */
- (void)setTags:(NSArray *)tags withCompleteHandler:(BPushCallBack)handler;

/**
 * @brief删除tag。
 * @param
 *     tag - 需要删除的tag
 * @return
 *     none
 */
- (void)delTag:(NSString *)tag withCompleteHandler:(BPushCallBack)handler;

/**
 * @brief删除多个tag。
 * @param
 *     tags - 需要删除的tag数组
 * @return
 *     none
 */
- (void)delTags:(NSArray *)tags withCompleteHandler:(BPushCallBack)handler;

/**
 * @brief获取当前设备应用的tag列表。
 * @param
 *     none
 * @return
 *     none
 */
- (void)listTagsWithCompleteHandler:(BPushCallBack)handler;

/**
 * @brief获取应用ID，Channel ID，User ID。如果应用没有绑定，那么返回空
 * @param
 *     none
 * @return
 *     appid/channelid/userid
 */
- (NSString *)getChannelId;
- (NSString *)getUserId;
- (NSString *)getAppId;

@end
