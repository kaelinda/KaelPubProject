//
//  DataPlist.m
//  HIGHTONG
//
//  Created by 赖利波 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DataPlist.h"

@implementation DataPlist
//   *******   读取  本地  plist  文件  ********
+(NSMutableArray *)ReadToFileArr:(NSString *)Plist;
{
    //建立文件管理
    NSFileManager *fm = [NSFileManager defaultManager];
    //找到Documents文件所在的路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得第一个Documents文件夹的路径
    NSString *filePath = [path objectAtIndex:0];
    //把Plist文件加入
    NSString *plistPaths = [filePath stringByAppendingPathComponent:Plist];
    //开始创建文件
    if(fm)
    {
        
    }
    else
    {
        [fm createFileAtPath:plistPaths contents:nil attributes:nil];
    }
    //读取
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:plistPaths];
    //打印
    NSLog(@"get_plist:%@ -- \n plist_name:%@",array,Plist);
    
    NSLog(@"%lu",(unsigned long)array.count);
    return array;
}

//   *******   写入  本地  plist  文件  ********
+(void)WriteToPlistArray:(NSMutableArray *)array ToFile:(NSString *)Plist;
{
    //建立文件管理
    NSFileManager *fm = [NSFileManager defaultManager];
    //找到Documents文件所在的路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得第一个Documents文件夹的路径
    NSString *filePath = [path objectAtIndex:0];
    //把Plist文件加入
    NSString *plistPaths = [filePath stringByAppendingPathComponent:Plist];
    //开始创建文件
    if(fm)
    {
        
    }
    else
    {
        [fm createFileAtPath:plistPaths contents:nil attributes:nil];
    }
    //写入
    //test 测试登陆后退出登陆，然后进入播放记录crash
    NSLog(@"！！！！！！！！！！！！写入的array = %@ --- plist_name:%@",array,Plist);
    BOOL success = [array writeToFile:plistPaths atomically:YES];
    NSLog(@"\n success:%d - \n plist_name:%@",success,Plist);

}


+(NSMutableDictionary *)ReadToFileDic:(NSString *)Plist
{
    //建立文件管理
    NSFileManager *fm = [NSFileManager defaultManager];
    //找到Documents文件所在的路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得第一个Documents文件夹的路径
    NSString *filePath = [path objectAtIndex:0];
    //把Plist文件加入
    NSString *plistPaths = [filePath stringByAppendingPathComponent:Plist];
    //开始创建文件
    if(fm)
    {
        
    }
    else
    {
        [fm createFileAtPath:plistPaths contents:nil attributes:nil];
    }
    //读取
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPaths];
    //打印
    NSLog(@"%@",dictionary);
   
    
//    NSLog(@"%lu",(unsigned long)array.count);
    return dictionary;
}

+(void)WriteToPlistDictionary:(NSMutableDictionary *)dic ToFile:(NSString *)Plist
{
    //建立文件管理
    NSFileManager *fm = [NSFileManager defaultManager];
    //找到Documents文件所在的路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得第一个Documents文件夹的路径
    NSString *filePath = [path objectAtIndex:0];
    //把Plist文件加入
    NSString *plistPaths = [filePath stringByAppendingPathComponent:Plist];
    //开始创建文件
    if(fm)
    {
        
    }
    else
    {
        [fm createFileAtPath:plistPaths contents:nil attributes:nil];
    }
    //写入
    //test 测试登陆后退出登陆，然后进入播放记录crash
    NSLog(@"！！！！！！！！！！！！写入的dic = %@",dic);
   BOOL success =  [dic writeToFile:plistPaths atomically:YES];
    NSLog(@"%d",success);
    
}

@end
