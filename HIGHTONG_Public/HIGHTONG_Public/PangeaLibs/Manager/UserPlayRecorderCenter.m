//
//  UserPlayRecorderCenter.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "UserPlayRecorderCenter.h"

@implementation UserPlayRecorderCenter

- (instancetype)init
{
    self = [super init];
    if (self) {
        //播放记录
        
        _playRecordListArr = [[NSMutableArray alloc] init];
        
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request VODNewPlayRecordListWithStartSeq:1 andCount:100 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThePlayRecorderList) name:REFRESH_THE_PLAY_RECORDER_LIST object:nil];
        
    
        _lookBackArr = [[NSMutableArray alloc] init];//回看数据源

        [request EventPlayListWithEventID:@"" andCount:100 andTimeout:10];//查询回看播放记录列表
        
        //回看播放记录列表
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTheReWatchList) name:REFRESH_THE_RE_WATCH_LIST object:nil];

    }
    return self;
}


- (NSString *)checkTheVODListWithContentID:(NSString *)contentID
{
    for (int i = 0; i < _playRecordListArr.count; i++) {
        
//        NSDictionary *video = [[NSDictionary alloc] init];
//        video = [_playRecordListArr objectAtIndex:i];
        
        NSDictionary *video = [NSDictionary dictionaryWithDictionary:[_playRecordListArr objectAtIndex:i]];
        
        if ([contentID isEqualToString:[video objectForKey:@"contentID"]]) {
            
            NSLog(@"YYYYYYYYY%@",[video objectForKey:@"breakPoint"]);
            return [video objectForKey:@"breakPoint"];
        }
    }
    return @"0";
}

- (NSDictionary *)getVODListItemWithContentID:(NSString *)contentID
{
    for (int i = 0; i < _playRecordListArr.count; i++) {
        
//        NSDictionary *video = [[NSDictionary alloc] init];
//        video = [_playRecordListArr objectAtIndex:i];
        
        NSDictionary *video = [NSDictionary dictionaryWithDictionary:[_playRecordListArr objectAtIndex:i]];
        if ([[video objectForKey:@"contentID"] isEqualToString:contentID]) {
            
            return video;
            
        }
    }
    
    return [NSDictionary dictionary];

}


//点播播放记录通知触发方法
- (void)refreshThePlayRecorderList
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request VODNewPlayRecordListWithStartSeq:1 andCount:100 andSatrtDte:@"" andEndDate:@"" andTimeout:10];
}


//回看播放记录列表通知触发方法
- (void)refreshTheReWatchList
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request EventPlayListWithEventID:@"" andCount:100 andTimeout:10];
}


- (NSString *)checkTheReWatchListWithEventID:(NSString *)eventID
{
    for (int i = 0; i < _lookBackArr.count; i++) {
        
        NSDictionary *video = [NSDictionary dictionaryWithDictionary:[_lookBackArr objectAtIndex:i]];
        if ([[video objectForKey:@"eventID"] isEqualToString:[NSString stringWithFormat:@"%@",eventID]]) {
            
            return [video objectForKey:@"breakPoint"];
        }
    }
    return @"0";
}

- (NSDictionary *)getAReWatchItemWithEventID:(NSString *)eventID
{
    for (int i = 0; i < _lookBackArr.count; i++) {
        
        NSDictionary *video = [NSDictionary dictionaryWithDictionary:[_lookBackArr objectAtIndex:i]];
        
        if ([[video objectForKey:@"eventID"] isEqualToString:eventID]) {
            
            return video;
        }
    }
    
    return [NSDictionary dictionary];
}

+ (id)defaultCenter
{
    static UserPlayRecorderCenter *center;
    
    if (!center) {
        center = [[UserPlayRecorderCenter alloc] init];
    }
    return center;
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:VOD_NewPlayRecordList]) {//点播播放记录
        
        if ([[result objectForKey:@"count"] integerValue] == 0||[[result objectForKey:@"count"] integerValue] > 0) {
            
            [_playRecordListArr removeAllObjects];
            
            _playRecordListArr = [NSMutableArray arrayWithArray:[result objectForKey:@"vodList"]];
                    
        }else{
            
            [_playRecordListArr removeAllObjects];
        }
        
        //kael
        if (_VOD_FinishedBlcok) {
            _VOD_FinishedBlcok([_playRecordListArr copy]);
        }
        
    }else if ([type isEqualToString:EPG_EVENTPLAYLIST]){//回看播放记录
        
        if ([result objectForKey:@"eventlist"] != nil && status == 0) {
            
            [_lookBackArr removeAllObjects];
            
            _lookBackArr = [NSMutableArray arrayWithArray:[result objectForKey:@"eventlist"]];
            
        }else{
            
            [_lookBackArr removeAllObjects];
        }
        
        //Kael
        if (_EPG_FinishedBlcok) {
            _EPG_FinishedBlcok([_lookBackArr copy]);
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Kael新添加的方法
-(void)refreshPlayRecorderWithType:(PlayRecorderType)playRecorderType withDidFinished:(FinishedRefeshBlock)didFinishedBlock{
    /* 记录下Block请求完成的时候回调一下 */
    switch (playRecorderType) {
        case EPG_PlayRecorder:
        {
            _EPG_FinishedBlcok = didFinishedBlock;
            [self refreshTheReWatchList];
        }
            break;
        case VOD_PlayRecorder:
        {
            _VOD_FinishedBlcok = didFinishedBlock;
            [self refreshThePlayRecorderList];
        }
            break;
    }
}

-(void)deletEventWith:(PlayRecorderType)playRecorderType andEventID:(NSString *)eventID{
    switch (playRecorderType) {
        case EPG_PlayRecorder:
        {
            //删除直播播放记录
            NSLog(@"这里需要遍历一下 然后寻找到相应节目 删除该直播播放记录");
            
            for (int i = 0; i<_lookBackArr.count; i++) {
                
                NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_lookBackArr objectAtIndex:i]];
                
                if ([[video objectForKey:@"eventID"] isEqualToString:[NSString stringWithFormat:@"%@",eventID]]) {
                    [_lookBackArr removeObjectAtIndex:i];
                    NSLog(@"成功删除预约节目");
                    return;
                }
            }
        }
            break;
        case VOD_PlayRecorder:
        {
            //删除点播播放记录
            NSLog(@"这里需要遍历一下 然后寻找到相应节目 删除该播放记录");

            for (int i = 0; i<_playRecordListArr.count; i++) {
                
                NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_playRecordListArr objectAtIndex:i]];
                
                if ([[video objectForKey:@"contentID"] isEqualToString:[NSString stringWithFormat:@"%@",eventID]]) {
                    [_playRecordListArr removeObjectAtIndex:i];
                    NSLog(@"成功删除预约节目");
                    return;
                }
            }
        }
            break;
    }
}

-(void)addVODPlayRecorderWith:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andPlayTime:(NSString *)playTime andStyle:(NSString *)style andBreakPoint:(NSString *)breakPoint andDuration:(NSString *)duration andImageLink:(NSString *)imageLink{

    NSMutableDictionary *vodRecorderList = [NSMutableDictionary new];
    
    [vodRecorderList setObject:contentID.length>0?contentID:@"" forKey:@"contentID"];
    [vodRecorderList setObject:name.length>0?name:@"" forKey:@"name"];
    [vodRecorderList setObject:subTitle.length>0?subTitle:@"" forKey:@"subTitle"];
    [vodRecorderList setObject:playTime.length>0?playTime:@"" forKey:@"playTime"];
    [vodRecorderList setObject:style.length>0?style:@"" forKey:@"style"];
    [vodRecorderList setObject:breakPoint.length>0?breakPoint:@"" forKey:@"breakPoint"];
    [vodRecorderList setObject:duration.length>0?duration:@"" forKey:@"duration"];
    [vodRecorderList setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];

    [_playRecordListArr addObject:vodRecorderList];
}

-(void)addEPGPlayRecorderWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andBreakPoint:(NSString *)breakPoint{

    NSMutableDictionary *epgRecorderList = [NSMutableDictionary new];
    
    [epgRecorderList setObject:serviceID.length>0?serviceID:@"" forKey:@"serviceID"];
    [epgRecorderList setObject:serviceName.length>0?serviceName:@"" forKey:@"serviceName"];
    [epgRecorderList setObject:eventID.length>0?eventID:@"" forKey:@"eventID"];
    [epgRecorderList setObject:eventName.length>0?eventName:@"" forKey:@"eventName"];
    [epgRecorderList setObject:startTime.length>0?startTime:@"" forKey:@"startTime"];
    [epgRecorderList setObject:endTime.length>0?endTime:@"" forKey:@"endTime"];
    [epgRecorderList setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [epgRecorderList setObject:levelImageLink.length>0?levelImageLink:@"" forKey:@"levelImageLink"];
    [epgRecorderList setObject:breakPoint.length>0?breakPoint:@"" forKey:@"breakPoint"];
    
    [_lookBackArr addObject:epgRecorderList];
}

-(void)cleanRecorderList
{
    [_lookBackArr removeAllObjects];
    [_playRecordListArr removeAllObjects];
}

@end

