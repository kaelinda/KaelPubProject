//
//  UserCollectCenter.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "UserCollectCenter.h"

@implementation UserCollectCenter

- (id)init
{
    self = [super init];
    
    if (self) {
        
        _favoriteChannelArr = [[NSMutableArray alloc] init];//喜爱频道数据源
        
        _favoriteDemandArr = [[NSMutableArray alloc] init];//点播收藏数据源
                
        HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
        
        [request TVFavorateListWithTimeout:10];//频道收藏列表(直播)
        
        //[request VODFavoryteListWithStartSeq:1 andCount:100 andTimeout:10];//点播收藏列表
        
        [request VODNewFavorateListWithStartSeq:1 andCount:100 andTimeout:10];//新版点播收藏列表

        //注册通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTheFavoriteChannelList) name:REFRESH_THE_FAVORITE_CHANNEL_LIST object:nil];//频道收藏
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTheFavoriteDemandList) name:REFRESH_THE_FAVORITE_DEMAND_LIST object:nil];//点播收藏
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTheFavoriteChannelList:) name:NOTI_UPDATE_FAVORATE_CHANELL_LIST object:nil];
        
    }
    
    return self;
}

+ (id)defaultCenter
{
    static UserCollectCenter *center;
    
    if (!center) {
        center = [[UserCollectCenter alloc] init];
    }
    return center;
}


-(void)updateTheFavoriteChannelList:(NSNotification*)notifi
{
    _favoriteChannelArr = (NSMutableArray*)notifi.object;
}



//直播收藏通知触发方法
- (void)refreshTheFavoriteChannelList
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    [request TVFavorateListWithTimeout:10];
}

//点播收藏通知触发方法
- (void)refreshTheFavoriteDemandList
{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    
    //[request VODFavoryteListWithStartSeq:1 andCount:100 andTimeout:10];
    NSLog(@"vod请求时间：%@",[NSDate date]);

    [request VODNewFavorateListWithStartSeq:1 andCount:100 andTimeout:3];
}

- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID
{
    
    for (int i = 0; i<_favoriteChannelArr.count; i++) {
        
//        NSDictionary *video = [[NSDictionary alloc]init];
//        
//        video = [_favoriteChannelArr objectAtIndex:i];
        
        NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_favoriteChannelArr objectAtIndex:i]];
        
        NSLog(@"video*****%@",[video objectForKey:@"id"]);
        
        if ([[video objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",serviceID]]) {
            
            return YES;
        }
    }
    return NO;
}


- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID
{
    for (int i = 0; i<_favoriteDemandArr.count; i++) {
        
//        NSDictionary *video = [[NSDictionary alloc]init];
//        
//        video = [_favoriteDemandArr objectAtIndex:i];
        
        NSDictionary *video = [[NSDictionary alloc] initWithDictionary:[_favoriteDemandArr objectAtIndex:i]];
        
        NSLog(@"video*****%@",[video objectForKey:@"contentID"]);
        
        if ([[video objectForKey:@"contentID"] isEqualToString:[NSString stringWithFormat:@"%@",contentID]]) {
            
            return YES;
        }
    }
    return NO;
}


-(void)refreshEPGFavorateChannelListWith:(ListReturnBlcok)resultBlcok{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request TVFavorateListWithTimeout:5];
    
    _channelListBlcok = resultBlcok;
}

-(void)refreshDemandListWith:(void (^)(BOOL isSuccess, NSArray *demandList))resultBlock{
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    [request VODNewFavorateListWithStartSeq:1 andCount:100 andTimeout:5];
    
    _demandListBlock = resultBlock;
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if ([type isEqualToString:EPG_TVFAVORATELIST]) {//直播收藏
        
        if ([result objectForKey:@"servicelist"] != nil && status == 0) {
            
            [_favoriteChannelArr removeAllObjects];
            
            _favoriteChannelArr = [NSMutableArray arrayWithArray:[result objectForKey:@"servicelist"]];
            
            //kael
            if (_channelListBlcok) {
                _channelListBlcok(YES,[_favoriteDemandArr copy]);
            }
            
        }else{
            
            [_favoriteChannelArr removeAllObjects];
            
            //kael
            if (_channelListBlcok) {
                _channelListBlcok(NO,[_favoriteDemandArr copy]);
            }
        }
        
    }else if ([type isEqualToString:VOD_NewFavorateList])
    {//点播收藏
        if ([result objectForKey:@"vodList"] != nil && status == 0) {
            
            [_favoriteDemandArr removeAllObjects];
            
            _favoriteDemandArr = [NSMutableArray arrayWithArray:[result objectForKey:@"vodList"]];
            
            //kael
            if (_demandListBlock) {
                _demandListBlock(YES,[_favoriteDemandArr copy]);
            }
            
        }else{
            [_favoriteDemandArr removeAllObjects];
            
            //kael
            if (_demandListBlock) {
                _demandListBlock(NO,[_favoriteDemandArr copy]);
            }
        }
    }
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
