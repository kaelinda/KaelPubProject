//
//  UGCLocalListCenter.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/4/6.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCLocalListCenter.h"
#import "DataPlist.h"

static UGCLocalListCenter *ugcCenter = nil;

@interface UGCLocalListCenter ()<HTRequestDelegate>

@end

@implementation UGCLocalListCenter


+ (id)defaultCenter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        ugcCenter = [[[self class] alloc] init];
    });
    
    return ugcCenter;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        HTRequest *classListRequest = [[HTRequest alloc] initWithDelegate:self];
        [classListRequest UGCCommonGetCategoryListWithUserId:nil];//视频编辑分类列表
        
        HTRequest *tagListRequest = [[HTRequest alloc] initWithDelegate:self];
        [tagListRequest UGCCommonGetTagList];//视频编辑标签列表
    }
    
    return self;
}

- (NSMutableArray *)categoryArr
{
    if (!_categoryArr) {
        _categoryArr = [[NSMutableArray alloc] init];
    }
    
    return _categoryArr;
}

- (NSMutableArray *)tagArr
{
    if (!_tagArr) {
        _tagArr = [[NSMutableArray alloc] init];
    }
    
    return _tagArr;
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    if (status == 0 && result.count >0) {
        if ([type isEqualToString:UGC_COMMON_GETCATEGORYLIST]) {//获取分类列表
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                
                NSLog(@"分类列表\n%@",[result objectForKey:@"categoryList"]);
                
//                NSArray *arr = [NSArray arrayWithArray:[result objectForKey:@"categoryList"]];
                
                _categoryArr = [NSMutableArray arrayWithArray:[result objectForKey:@"categoryList"]];
                
//                [[UGCLocalListCenter alloc] writeUGCListArrInPlistWithPlistName:UGC_CATEGORY_LIST_PLIST andListArr:[arr copy]];//[NSArray arrayWithArray:[result objectForKey:@"categoryList"]]
            }
        }
        else if ([type isEqualToString:UGC_COMMON_GETTAGLIST]) {//获取标签列表
            
            if ([[result objectForKey:@"ret"] integerValue] == 0) {
                
                NSLog(@"标签列表\n%@",[result objectForKey:@"tagList"]);
                
//                NSArray *arr = [NSArray arrayWithArray:[result objectForKey:@"tagList"]];
                
                _tagArr = [NSMutableArray arrayWithArray:[result objectForKey:@"tagList"]];
                
//                [[UGCLocalListCenter alloc] writeUGCListArrInPlistWithPlistName:UGC_TAG_LIST_PLIST andListArr:[arr copy]];
            }
        }
    }
}

- (NSMutableArray *)getTagArr
{
    return _tagArr;
}

- (NSMutableArray *)getCategoryArr
{
    return _categoryArr;
}

+ (NSArray *)getUGCTagList
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    dataArr = [DataPlist ReadToFileArr:UGC_TAG_LIST_PLIST];
    
    return dataArr;
}


+ (NSArray *)getUGCCategoryList
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    dataArr = [DataPlist ReadToFileArr:UGC_CATEGORY_LIST_PLIST];
    
    return dataArr;
}


+ (NSArray *)getUGCAcademicTitleList
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    dataArr = [DataPlist ReadToFileArr:UGC_ACADEMIC_TITLE_LIST_PLIST];

    return dataArr;
}

+ (void)cleanUGCLocalList
{
    //标签列表
    NSMutableArray *recordArr1 = [DataPlist ReadToFileArr:UGC_TAG_LIST_PLIST];
    [recordArr1 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr1 ToFile:UGC_TAG_LIST_PLIST];
    
    //分类列表
    NSMutableArray *recordArr2 = [DataPlist ReadToFileArr:UGC_CATEGORY_LIST_PLIST];
    [recordArr2 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr2 ToFile:UGC_CATEGORY_LIST_PLIST];
    
    //医生职称列表
    NSMutableArray *recordArr3 = [DataPlist ReadToFileArr:UGC_ACADEMIC_TITLE_LIST_PLIST];
    [recordArr3 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr3 ToFile:UGC_ACADEMIC_TITLE_LIST_PLIST];

}

- (void)writeUGCListArrInPlistWithPlistName:(NSString *)plistName andListArr:(NSArray *)listArr
{
    [DataPlist WriteToPlistArray:[NSMutableArray arrayWithArray:listArr] ToFile:plistName];
}

@end
