//
//  PeriodFile.h
//  HIGHTONG
//
//  Created by Alaca on 15/5/27.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PeriodFile : NSObject
/**
 *  将配置文件写入plist
 *
 *  @param dic 请求返回来的配置文件字典
 */
-(void)writePlistWith:(NSDictionary *)dic;
/**
 *  获取某个元素
 *
 *  @param firstKey  第一个 外层关键字
 *  @param secondKey 第二个 内层关键字
 *
 *  @return 返回值即是所需内层参数
 */
-(NSInteger)getItmeWith:(NSString *)firstKey andSecondKey:(NSString *)secondKey;



@end
