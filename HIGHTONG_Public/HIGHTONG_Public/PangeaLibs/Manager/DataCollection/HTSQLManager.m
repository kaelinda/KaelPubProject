//
//  HTSQLManager.m
//  HIGHTONG
//
//  Created by apple on 15/6/10.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import "HTSQLManager.h"
#import "SAIInformationManager.h"

static HTSQLManager *sqlManager;
@interface HTSQLManager ()<HT_CollectionResuqestDelegate>

@end

@implementation HTSQLManager
+ (id)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (sqlManager == nil)
        {
            sqlManager = [[HTSQLManager alloc] init];
        }
    });
    
    return sqlManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self creatDataTable];
        _db = [FMDatabase databaseWithPath:[self databaseFilePath]];

    }
    return self;
}


//-(FMDatabase *)_db
//{
//    
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
//    
//    
//    return _db;
//}

//- (void)dealloc
//{
//    [sqlManager release];
//    [super dealloc];
//}


#pragma mark --建表语句
- (void)creatDataTable
{
    NSString *sqlPath = [self databaseFilePath];
    
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    
    _db = [FMDatabase databaseWithPath:sqlPath];
    //为数据库设置缓存，提高查询效率
    [_db setShouldCacheStatements:YES];
    
    
    if ([_db open]) {
        
        if([_db tableExists:@"People"])
        {
            [_db close];
            return;
        }
        
        NSString *sqlCreateTable =  [NSString stringWithFormat:@"create table People (number INTEGER PRIMARY KEY AUTOINCREMENT,OnOffTime text, UserAccount text,Carousel text, Live text,TimeMove text,Lookingback text,EPG text,VOD text,EPGSearch text,POLY text,MedicineHealth text,PubHumour text,PubHumourInteractive text,ShareUmeng text,VODSearch text )"];
        
        BOOL res = [_db executeUpdate:sqlCreateTable];
        
        if (!res) {
            NSLog(@"error when creating _db table");
        } else {
            NSLog(@"success to creating _db table");
        }
        [_db close];
        
    }
    
    
    if ([_db open]) {
        
        
        if([_db tableExists:@"ETH"])
        {
            [_db close];
            return;
        }
        NSString *sqlCreateTable2 =  [NSString stringWithFormat:@"create table ETH (number INTEGER PRIMARY KEY AUTOINCREMENT,UserAccount text, TIME text, IP text, AW text, TTL text, BW text )"];
        
        BOOL res = [_db executeUpdate:sqlCreateTable2];
        
        
        if (!res) {
            NSLog(@"error when creating db table");
        } else {
            NSLog(@"success to creating db table");
        }
        [_db close];
        
        
    }
    
    
    if ([_db open]) {
        
        
        if([_db tableExists:@"OPS"])
        {
            [_db close];
            return;
        }
        NSString *sqlCreateTable3 =  [NSString stringWithFormat:@"create table OPS (number INTEGER PRIMARY KEY AUTOINCREMENT,UserAccount text, NETTYPE text,WIFISSID text,SOURCETYPE text,AVNAME text,OPSCODE text,OPSST text, DURATION text)"];
        
        BOOL res = [_db executeUpdate:sqlCreateTable3];
        
        
        if (!res) {
            NSLog(@"error when creating db table");
        } else {
            NSLog(@"success to creating db table");
        }
        [_db close];
        
        
    }
    
    
}


#pragma mark ------>:插入轮播图数据

-(void)insertCarousel:(NSString*)carouselStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"Carousel" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (Carousel,UserAccount) values ('%@','%@')",carouselStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入轮播图信息成功");
        }else
        {
            NSLog(@"插入轮播图信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}



-(void)insertPub_humourVideo:(NSString*)pubHumourStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"PubHumour" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (PubHumour,UserAccount) values ('%@','%@')",pubHumourStr,UserAccount];
        
        if ([_db executeUpdate:sql]) {
            NSLog(@"插入幽默视频信息成功");
        }else
        {
            NSLog(@"插入幽默视频信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];

    
}



-(void)insertPub_humourInteractiveVideo:(NSString*)pubHumourInteractiveStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"PubHumourInteractive" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (PubHumourInteractive,UserAccount) values ('%@','%@')",pubHumourInteractiveStr,UserAccount];
        
        if ([_db executeUpdate:sql]) {
            NSLog(@"插入幽默交互信息成功");
        }else
        {
            NSLog(@"插入幽默交互信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
    

}


-(void)insertShareUmengVideo:(NSString*)shareUmengStr
{
    
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"ShareUmeng" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (ShareUmeng,UserAccount) values ('%@','%@')",shareUmengStr,UserAccount];
        
        if ([_db executeUpdate:sql]) {
            NSLog(@"插入分享友盟信息成功");
        }else
        {
            NSLog(@"插入分享友盟信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
    
}






#pragma mark ------->:插入数据
//  增加 直播 表数据
-(void)insertLive:(NSString *)LiveStr
{
    
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"Live" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (Live,UserAccount) values ('%@','%@')",LiveStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入直播成功");
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];

    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


//  增加 时移数据 表数据
-(void)insertTimeMove:(NSString *)TimeMoveStr
{
    
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"TimeMove" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (TimeMove,UserAccount) values ('%@','%@')",TimeMoveStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入成功");
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


//  增加 EPG 表数据
-(void)insertEPG:(NSString *)EPGStr
{
    
    NSString *UserAccount = [self userIDFromAPP];

    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"EPG" andTableFMDB:@"People"];
        sql = [NSString stringWithFormat:@"insert into 'People' (EPG,UserAccount) values ('%@','%@')",EPGStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入EPG成功");
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


//  增加 回看 表数据
-(void)insertLookingback:(NSString *)LookingbackStr
{
    NSString *UserAccount = [self userIDFromAPP];

    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"Lookingback" andTableFMDB:@"People"];
        sql = [NSString stringWithFormat:@"insert into 'People' (Lookingback,UserAccount) values ('%@','%@')",LookingbackStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入LookingBack成功");
            
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}

//  增加 点播 表数据
-(void)insertVOD:(NSString *)VODStr
{
    
    NSString *UserAccount = [self userIDFromAPP];

    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"VOD" andTableFMDB:@"People"];
        sql = [NSString stringWithFormat:@"insert into 'People' (VOD,UserAccount) values ('%@','%@')",VODStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入VoD成功  %@",VODStr);
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


-(void)insertPoly:(NSString*)PolyStr
{
    NSString *UserAccount = [self userIDFromAPP];

    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"POLY" andTableFMDB:@"People"];
        sql = [NSString stringWithFormat:@"insert into 'People' (POLY,UserAccount) values ('%@','%@')",PolyStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入POLY成功  %@",PolyStr);
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];}


-(void)insertJKTVMedicineHealth:(NSString*)MedicineHealthStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"MedicineHealth" andTableFMDB:@"People"];
        sql = [NSString stringWithFormat:@"insert into 'People' (MedicineHealth,UserAccount) values ('%@','%@')",MedicineHealthStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入医疗健康成功  %@",MedicineHealthStr);
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}






//  增加EPGSearch

-(void)insertEPGSearch:(NSString *)EPGSearchStr
{
    
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"EPGSearch" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (EPGSearch,UserAccount) values ('%@','%@')",EPGSearchStr,UserAccount];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入EPGsearch成功");
        }else
        {
            NSLog(@"插入EPGsearch失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


//  增加VODSearch

-(void)insertVODSearch:(NSString *)VODSearchStr {
    
    NSString *UserAccount = [self userIDFromAPP];

   
    
    if ([_db open]) {
        
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"VODSearch" andTableFMDB:@"People"];
        
        sql = [NSString stringWithFormat:@"insert into 'People' (VODSearch,UserAccount) values ('%@','%@')",VODSearchStr,UserAccount];
        if ([_db executeUpdate:sql]) {
            NSLog(@"插入vodsearch成功");
        }else
        {
            NSLog(@"插入vodsearch失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}

/**
 *  插入的网络信息
 *
 *  @param IPStr   当前网络IP地址（String）
 *  @param AWStr   网络接入方式，有线/无线（String）
 *  @param TTLStr  网络时延（String）
 *  @param BWStr   当网络带宽（String）
 *  @param TimeStr 当前采集的时间
 */
-(void)insertETHForIP:(NSString*)IPStr andAW:(NSString*)AWStr andTTL:(NSString*)TTLStr andBW:(NSString*)BWStr andTime:(NSString*)TimeStr{
    NSString *UserAccount = [self userIDFromAPP];
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"TIME" andTableFMDB:@"ETH"];
        
        sql =  [self stringInsertKeyWordFMDB:@"IP" andTableFMDB:@"ETH"];
        
        sql =  [self stringInsertKeyWordFMDB:@"AW" andTableFMDB:@"ETH"];
        
        sql =  [self stringInsertKeyWordFMDB:@"TTL" andTableFMDB:@"ETH"];
        
        sql =  [self stringInsertKeyWordFMDB:@"BW" andTableFMDB:@"ETH"];
        
        sql = [NSString stringWithFormat:@"insert into 'ETH' (UserAccount,TIME,IP,AW,TTL,BW) values ('%@','%@','%@','%@','%@','%@')",UserAccount,TimeStr,IPStr,AWStr,TTLStr,BWStr];
        if ([_db executeUpdate:sql]) {
            NSLog(@"插入ETH信息成功");
        }else
        {
            NSLog(@"插入ETH信息失败");
        }
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:NETINFO];
}

/**
 *  采集的错误信息
 *
 *  @param AVTYPEStr   视频类型（String）
 *  @param OPTYPEStr   异常类型（String）AV：视频类
 *  @param OPSSTStr    异常开始时间（String）格式：YYYY-MM-DD HH:mm:SS
 *  @param DurationStr 异常持续时间（String）格式：mm:SS
 */
-(void)insertOPSForAVTYPE:(NSString*)AVTYPEStr andOPTYPE:(NSString*)OPTYPEStr andOPSST:(NSString*)OPSSTStr andDuration:(NSString*)DurationStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    

    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"AVTYPE" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"OPTYPE" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"OPSST" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"DURATION" andTableFMDB:@"OPS"];
        sql = [NSString stringWithFormat:@"insert into 'OPS' (UserAccount,AVTYPE,OPTYPE,OPSST,DURATION) values ('%@','%@','%@','%@','%@')",UserAccount,AVTYPEStr,OPTYPEStr,OPSSTStr,DurationStr];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入OPS信息成功");
        }else
        {
            NSLog(@"插入OPS信息失败");
        }
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:ERRORINFO];
}




/**
 采集的错误信息

 @param netTypeStr 网络类型
 @param wifiSSIDStr WiFi连接时的SSID
 @param sourceTypeStr 视频的网络来源，INTERNET：互联网；CABLENET：广电网
 @param avNameStr 视频名称
 @param opsCodeStr 异常编码（901：视频加载超时；902：离开播放画面；903：视频不存在）
 @param opsstStr 异常开始时间
 @param durationStr 异常持续时间
 */
-(void)insertOPSForNetType:(NSString *)netTypeStr andWIFISSID:(NSString*)wifiSSIDStr andSourceType:(NSString *)sourceTypeStr andAVName:(NSString *)avNameStr andOPSCode:(NSString *)opsCodeStr andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr
{
    NSString *UserAccount = [self userIDFromAPP];
    
    if ([_db open]) {
        NSString *sql = @"";
        sql =  [self stringInsertKeyWordFMDB:@"NETTYPE" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"WIFISSID" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"SOURCETYPE" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"AVNAME" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"OPSCODE" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"OPSST" andTableFMDB:@"OPS"];
        
        sql =  [self stringInsertKeyWordFMDB:@"DURATION" andTableFMDB:@"OPS"];
        
        sql = [NSString stringWithFormat:@"insert into 'OPS' (UserAccount,NETTYPE,WIFISSID,SOURCETYPE,AVNAME,OPSCODE,OPSST,DURATION) values ('%@','%@','%@','%@','%@','%@','%@','%@')",UserAccount,netTypeStr,wifiSSIDStr,sourceTypeStr,avNameStr,opsCodeStr,opsstStr,durationStr];

        if ([_db executeUpdate:sql]) {
            NSLog(@"插入OPS信息成功");
        }else
        {
            NSLog(@"插入OPS信息失败");
        }
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:ERRORINFO];

    
    
    
    
}


-(NSString *)stringInsertKeyWordFMDB:(NSString *)strFMDB andTableFMDB:(NSString *)tableFMDB
{
    NSString *strSQL = @"";
    
    if (![_db columnExists:strFMDB inTableWithName:tableFMDB]) {
        
        strSQL = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ INTEGER",tableFMDB,strFMDB];
        
        [_db executeUpdate:strSQL];
    }
    
    return strSQL;
}


-(void)insertUserOnTime:(NSString*)Online  AndOffLine:(NSString*)Offline
{
    NSString *UserAccount = [self userIDFromAPP];
    
    
    NSString *mOnOffTime = [NSString stringWithFormat:@"%@+%@",Online,Offline];
    
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    
    if ([_db open]) {
        if ([_db executeUpdate:@"insert into 'People' (OnOffTime,UserAccount) values (?,?)",mOnOffTime,UserAccount]) {
            NSLog(@"插入上线信息成功");
        }else
        {
            NSLog(@"插入上线信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}





//  插入上线时间
-(void)insertUserOnTime:(NSString *)Time
{
    
    
    NSString *UserAccount = [self userIDFromAPP];

    
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    if ([_db open]) {
        if ([_db executeUpdate:@"insert into 'People' (OnOffTime,UserAccount) values (?,?)",Time,UserAccount]) {
            NSLog(@"插入上线信息成功");
        }else
        {
            NSLog(@"插入上线信息失败");
        }
        
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}


#pragma mark ------->
-(void)UpdateUserOffTime:(NSString *)Time
{
    NSString *UserAccount = [self userIDFromAPP];
    
    
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    if ([_db open]) {
        
        
        NSString *onLineTimeStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"ONLINE"];
        
        [_db executeUpdate:@"insert into 'People' (OnOffTime,UserAccount) values (?,?)",[NSString stringWithFormat:@"%@+%@",onLineTimeStr,[SAIInformationManager serviceTimeTransformStr]],UserAccount];
        
        NSString *updateSql = [NSString stringWithFormat:
                               @"UPDATE People SET OnOffTime = '%@'  WHERE rowid = (select max(rowid)FROM People)",
                               Time ];
        
        BOOL res = [_db executeUpdate:updateSql];
        
        if (!res) {
            NSLog(@"更新下线信息失败");
        } else {
            NSLog(@"更新下线信息成功");
        }
        
    }else
    {
        NSLog(@"数据库未打开");
    }
    
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
    
}




//  插入下线时间
-(void)insertUserOffTime:(NSString *)Time
{
    
    
    
    NSString *UserAccount = [self userIDFromAPP];

    
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    
    if ([_db open]) {
        
        if ([_db executeUpdate:@"insert into 'People' (OffTime,UserAccount) values (?,?)",Time,UserAccount]) {
            NSLog(@"插入成功");
        }else
        {
            NSLog(@"插入失败");
        }
        
        
    }else
    {
        
        NSLog(@"数据库未打开");
    }
    
    
    [_db close];
    
    //if 即刻上传 ------>>: 发送信息采集的数据
    [self postICS:USERACTIONINFO];
}
//  获取最新行为信息
-(NSMutableArray *)getNewAllPeople
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    //定义一个可变数组，用来存放查询的结果，返回给调用者
    NSMutableArray *peopleArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *Livearray=[[NSMutableArray alloc] init];
    NSMutableArray *TimeMovearray=[[NSMutableArray alloc] init];
    NSMutableArray *Lookingbackarray=[[NSMutableArray alloc] init];
    NSMutableArray *EPGarray=[[NSMutableArray alloc] init];
    NSMutableArray *VODarray=[[NSMutableArray alloc] init];
    NSMutableArray *UserAccountarray=[[NSMutableArray alloc]init];
    NSMutableArray *searchEPGarray=[[NSMutableArray alloc]init];
    NSMutableArray *searchVODarray=[[NSMutableArray alloc]init];
    NSMutableArray *TimeArray = [[NSMutableArray alloc]init];
    NSMutableArray *polyArray = [[NSMutableArray alloc]init];
    NSMutableArray *JKTVArray = [[NSMutableArray alloc]init];
    NSMutableArray *CarouselArray = [[NSMutableArray alloc]init];
    NSMutableArray *pubHumourArray = [[NSMutableArray alloc]init];
    NSMutableArray *pubHumourInteractiveArray = [[NSMutableArray alloc]init];
    NSMutableArray *shareUmengArray = [[NSMutableArray alloc]init];


    if ([_db open]) {
        FMResultSet * rs = [_db executeQuery:[NSString stringWithFormat:@"SELECT * FROM People"]];
        
        //判断结果集中是否有数据，如果有则取出数据
        while ([rs next]) {
            NSMutableDictionary *dictTime=[[NSMutableDictionary alloc]init];
            NSString *UserAccount=[rs stringForColumn:@"UserAccount"];
            if(UserAccount)
            {
                [UserAccountarray addObject:UserAccount];
            }
            
            
            NSString *Live=[rs stringForColumn:@"Live"];
            if(Live)
            {
                [Livearray addObject:Live];
            }
            
            NSString *TimeMove=[rs stringForColumn:@"TimeMove"];
            if(TimeMove)
            {
                [TimeMovearray addObject:TimeMove];
            }
            
            NSString *Lookingback=[rs stringForColumn:@"Lookingback"];
            if(Lookingback)
            {
                [Lookingbackarray addObject:Lookingback];
            }
            
            NSString *EPG=[rs stringForColumn:@"EPG"];
            if(EPG)
            {
                [EPGarray addObject:EPG];
            }
            
            NSString *VOD=[rs stringForColumn:@"VOD"];
            if(VOD)
            {
                [VODarray addObject:VOD];
            }
            
            NSString *searchEPG=[rs stringForColumn:@"EPGSearch"];
            if(searchEPG)
            {
                [searchEPGarray addObject:searchEPG];
            }
            
            NSString *searchVOD=[rs stringForColumn:@"VODSearch"];
            if(searchVOD)
            {
                [searchVODarray addObject:searchVOD];
            }
            
            NSString *polyMedic = [rs stringForColumn:@"POLY"];
            if (polyMedic) {
                [polyArray addObject:polyMedic];
            }
            
            NSString *JKTVMedic = [rs stringForColumn:@"MedicineHealth"];
            if (JKTVMedic) {
                [JKTVArray addObject:JKTVMedic];
            }
            
            NSString *Carousel = [rs stringForColumn:@"Carousel"];
            if (Carousel) {
                [CarouselArray addObject:Carousel];
            }
            
            NSString *pubHumour = [rs stringForColumn:@"PubHumour"];
            if (pubHumour) {
                [pubHumourArray addObject:pubHumour];
            }
            
            NSString *pubHumourInteractive = [rs stringForColumn:@"PubHumourInteractive"];
            if (pubHumourInteractive) {
                [pubHumourInteractiveArray addObject:pubHumourInteractive];
            }
            
            NSString *shareUmeng = [rs stringForColumn:@"ShareUmeng"];
            if (shareUmeng) {
                [shareUmengArray addObject:shareUmeng];
            }
            

            
            NSString *OnOffTime=[rs stringForColumn:@"OnOffTime"];
            
            if (OnOffTime.length>0) {
                
                NSLog(@"onOfftime%@",OnOffTime);
                
                if ([OnOffTime rangeOfString:@"+"].location!=NSNotFound) {
                    NSArray *TimeArr = [OnOffTime componentsSeparatedByString:@"+"];
                    NSString *OnTime = [TimeArr objectAtIndex:0];
                    NSString *OffTime = [TimeArr objectAtIndex:1];
                    
                    [dictTime setValue:OnTime forKey:@"OLT1"];
                    [dictTime setValue:OffTime forKey:@"OLT2"];
                    [dictTime setValue:[SAIInformationManager serviceTimeTransformStr] forKey:@"UT"];
                    
                    
                }else
                {
                        [dictTime setValue:OnOffTime forKey:@"OLT1"];
                        [dictTime setValue:@"" forKey:@"OLT2"];
                    [dictTime setValue:[SAIInformationManager serviceTimeTransformStr] forKey:@"UT"];

                    
                }
                
            }else
            {
                OnOffTime  = [[NSUserDefaults standardUserDefaults] valueForKey:@"ONLINE"];
                [dictTime setValue:OnOffTime forKey:@"OLT1"];
                [dictTime setValue:@"" forKey:@"OLT2"];
                [dictTime setValue:[SAIInformationManager serviceTimeTransformStr] forKey:@"UT"];


            }
            
            
            [TimeArray addObject:dictTime];

            
        }
        
        if (TimeArray.count==0) {
            NSString *onlineString = [[NSUserDefaults standardUserDefaults] valueForKey:@"ONLINE"];
            NSDictionary *timeDic = [[NSDictionary alloc] initWithObjectsAndKeys:onlineString,@"OLT1",@"",@"OLT2",[SAIInformationManager serviceTimeTransformStr],@"UT", nil];
            [TimeArray addObject:timeDic];
            
        }
        //zzs 直播
        Livearray = [self getZZSLiveArrWith:Livearray];
        //zzs 回看
        Lookingbackarray = [self getZZSPlayBackArrWith:Lookingbackarray];
        //zzs EPG
        EPGarray = [self getZZSEPGArrWith:EPGarray];
        //zzs EPG搜索
        searchEPGarray = [self getZZSSearchEPGArrWith:searchEPGarray];
        
        VODarray = [self getZZSVODArrWith:VODarray];
        
        
        TimeMovearray = [self getZZSTimeMoveArrWith:TimeMovearray];
        
        JKTVArray = [self getZZSJKTVHealthArrWith:JKTVArray];
        
        polyArray = [self getZZSPolyMedicArrWith:polyArray];
        
        CarouselArray = [self getZZSCarouselArrWith:CarouselArray];
        
        pubHumourArray = [self getZZSPubHumourArrWith:pubHumourArray];
        
        pubHumourInteractiveArray = [self getZZSPubHumourInteractiveArrWith:pubHumourInteractiveArray];
        
        shareUmengArray = [self getZZSShareUmengArrWith:shareUmengArray];
        
        
        [peopleArray addObject:TimeArray];//上下线时间0
        [peopleArray addObject:Livearray];//直播1
        [peopleArray addObject:Lookingbackarray];//回看2
        [peopleArray addObject:EPGarray];//EPG3
        [peopleArray addObject:searchEPGarray];//EPG search4
        [peopleArray addObject:VODarray];//VOD5
        [peopleArray addObject:searchVODarray];//6
        [peopleArray addObject:TimeMovearray];//时移7
        [peopleArray addObject:JKTVArray];//医疗视频8
        [peopleArray addObject:polyArray];//医视频9
        [peopleArray addObject:CarouselArray];//轮播图10
        [peopleArray addObject:pubHumourArray];//幽默视频11
        [peopleArray addObject:pubHumourInteractiveArray];//幽默交互12
        [peopleArray addObject:shareUmengArray];//分享13

        
    }
    
//    [TimeArray release];
//    [Livearray release];
//    [Lookingbackarray release];
//    [EPGarray release];
//    [searchEPGarray release];
//    [VODarray release];
//    [searchVODarray release];
//    [TimeMovearray release];
    
    NSLog(@"用户信息 ：%@",peopleArray);
//    return [peopleArray autorelease];
    return peopleArray;
    
}


//*  @param IPStr   当前网络IP地址（String）
//*  @param AWStr   网络接入方式，有线/无线（String）
//*  @param TTLStr  网络时延（String）
//*  @param BWStr   当网络带宽（String）
//*  @param TimeStr 当前采集的时间


-(NSMutableArray *)getETHInformations
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    //定义一个可变数组，用来存放查询的结果，返回给调用者
    NSMutableArray *ETHArr = [[NSMutableArray alloc]init];
    
    
    
    // NSMutableDictionary *UserAccountDic= [[NSMutableDictionary alloc]init];
    //    UserAccount,TIME,IP,AW,TTL,BW
    if ([_db open]) {
        FMResultSet * rs = [_db executeQuery:[NSString stringWithFormat:@"SELECT * FROM ETH"]];
        
        while ([rs next]) {
            NSMutableDictionary *tempdic= [[NSMutableDictionary alloc]init];
            //            NSString *UserAccount=[rs stringForColumn:@"UserAccount"];
            //            if(UserAccount)
            //            {
            //                [UserAccountArray addObject:UserAccount];
            //            }
            NSString *Time=[rs stringForColumn:@"TIME"];
            if(Time)
            {
                [tempdic setObject:Time forKey:@"GT"];
            }
            NSString *IP=[rs stringForColumn:@"IP"];
            if(IP)
            {
                [tempdic setObject:IP forKey:@"IP"];
            }
            NSString *AWStr=[rs stringForColumn:@"AW"];
            if(AWStr)
            {
                [tempdic setObject:AWStr forKey:@"AW"];
            }
            NSString *TTLStr=[rs stringForColumn:@"TTL"];
            if(TTLStr)
            {
                [tempdic setObject:TTLStr forKey:@"TTL"];
            }
            NSString *BWStr=[rs stringForColumn:@"BW"];
            if(BWStr)
            {
                [tempdic setObject:BWStr forKey:@"BW"];
            }
            
            [ETHArr addObject:tempdic];
            
//            [tempdic release];
        }
        
    }
    
    
//    return [ETHArr autorelease];
    
    return ETHArr;
}

//*  @param AVTYPEStr   视频类型（String）
//*  @param OPTYPEStr   异常类型（String）AV：视频类
//*  @param OPSSTStr    异常开始时间（String）格式：YYYY-MM-DD HH:mm:SS
//*  @param DurationStr 异常持续时间（String）格式：mm:SS

-(NSMutableArray *)getOPSInformations
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    //定义一个可变数组，用来存放查询的结果，返回给调用者
    NSMutableArray *OPSArray = [[NSMutableArray alloc]init];
    NSMutableArray *UserAccountArray = [[NSMutableArray alloc] init];
    
    if ([_db open]) {
        FMResultSet * rs = [_db executeQuery:[NSString stringWithFormat:@"SELECT * FROM OPS"]];
        
        while ([rs next]) {
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
            NSString *UserAccount=[rs stringForColumn:@"UserAccount"];
            if(UserAccount)
            {
                [UserAccountArray addObject:UserAccount];
            }
//            NSString *AVTYPEStr=[rs stringForColumn:@"AVTYPE"];
//            if(AVTYPEStr)
//            {
//                
//                [tempDic setValue:AVTYPEStr forKey:@"AVTYPE"];
//            }
//            NSString *OPTYPEStr=[rs stringForColumn:@"OPTYPE"];
//            if(OPTYPEStr)
//            {
//                [tempDic setValue:OPTYPEStr forKey:@"OPTYPE"];
//            }
//            NSString *OPSSTStr=[rs stringForColumn:@"OPSST"];
//            if(OPSSTStr)
//            {
//                [tempDic setValue:OPSSTStr forKey:@"OPSST"];
//            }
//            NSString *DurationStr=[rs stringForColumn:@"DURATION"];
//            if(DurationStr)
//            {
//                [tempDic setValue:DurationStr forKey:@"DURATION"];
//            }
            
            NSString *NETTYPEStr=[rs stringForColumn:@"NETTYPE"];
            if(NETTYPEStr)
            {
                
                [tempDic setValue:NETTYPEStr forKey:@"NETTYPE"];
            }
            NSString *WIFISSIDStr=[rs stringForColumn:@"WIFISSID"];
            if(WIFISSIDStr)
            {
                [tempDic setValue:WIFISSIDStr forKey:@"WIFISSID"];
            }
            NSString *SOURCETYPEStr=[rs stringForColumn:@"SOURCETYPE"];
            if(SOURCETYPEStr)
            {
                [tempDic setValue:SOURCETYPEStr forKey:@"SOURCETYPE"];
            }
            NSString *AVNAMEStr=[rs stringForColumn:@"AVNAME"];
            if(AVNAMEStr)
            {
                [tempDic setValue:AVNAMEStr forKey:@"AVNAME"];
            }
            NSString *OPSCODEStr=[rs stringForColumn:@"OPSCODE"];
            if(OPSCODEStr)
            {
                [tempDic setValue:OPSCODEStr forKey:@"OPSCODE"];
            }
            NSString *OPSSTStr=[rs stringForColumn:@"OPSST"];
            if(OPSSTStr)
            {
                [tempDic setValue:OPSSTStr forKey:@"OPSST"];
            }
            NSString *DurationStr=[rs stringForColumn:@"DURATION"];
            if(DurationStr)
            {
                [tempDic setValue:DurationStr forKey:@"DURATION"];
            }

            
            [OPSArray addObject:tempDic];
            
//            [tempDic release];
        }
        
    }
    
    NSLog(@"打印错误信息%@",OPSArray);
    
//    return [OPSArray autorelease];
    return OPSArray;
    
}













//  上次上传失败的行为信息删除方法
-(void)deleteUSerinformationNew:(NSString *)sqlstr
{
    
}

//  删除直播  回看  EPG   方法
-(void)deleteUserAction//  删除直播  回看  EPG   方法
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    if ([_db open]) {
        NSString *deleteUserAccount = [self userIDFromAPP];
        
        if ([_db executeUpdate:@"delete from People where UserAccount=?",deleteUserAccount]) {
            NSLog(@"删除用户行为成功");
        }else
        {
            NSLog(@"删除用户失败");
        }
        
    }
    
    [_db close];
}
//删除OPS信息
-(void)deleteUserOPSInformation
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    if ([_db open]) {
        
        NSString *deleteUserAccount = [self userIDFromAPP];
        
        if ([_db executeUpdate:@"delete from OPS where UserAccount=?",deleteUserAccount]) {
            NSLog(@"删除OPS成功");
        }else
        {
            NSLog(@"删除OPS失败");
        }
        
    }
    
    [_db close];
    
}

//删除ETH信息
-(void)deleteUserETHInformation
{
//    _db = [FMDatabase databaseWithPath:[self databaseFilePath]];
    
    if ([_db open]) {
        
        NSString *deleteUserAccount = [self userIDFromAPP];
        if ([_db executeUpdate:@"delete from ETH where UserAccount=?",deleteUserAccount]) {
            NSLog(@"删除ETH成功");
        }else
        {
            NSLog(@"删除ETH失败");
        }
        
    }
    
    [_db close];
    
}



/**
 得到轮播图的数组

 @param carouselArr 轮播图数组
 @return 返回已经分隔号的数据
 */
-(NSMutableArray *)getZZSCarouselArrWith:(NSMutableArray *)carouselArr{
    
    NSMutableArray *resultCarouselArry = [[NSMutableArray alloc]init];
    for (int i=0; i<carouselArr.count; i++) {
        NSString *str = [carouselArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==5) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"SEQ"];
            if (!isEmptyStringOrNilOrNull([arr objectAtIndex:1])) {
                [mDic setObject:[arr objectAtIndex:1] forKey:@"ID"];
            }
            [mDic setObject:[arr objectAtIndex:2] forKey:@"NAME"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"TYPE"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"BT"];
            
        }
        [resultCarouselArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    
    
    //    return [resultLiveArry autorelease];
    
    
    return resultCarouselArry;
}






/**
 得到幽默视频的数组
 
 @param carouselArr 幽默视频数组
 @return 返回已经分隔号的数据
 */
-(NSMutableArray *)getZZSPubHumourArrWith:(NSMutableArray *)pubHumourArr{
    
    NSMutableArray *resultPubHumourArry = [[NSMutableArray alloc]init];
    for (int i=0; i<pubHumourArr.count; i++) {
        NSString *str = [pubHumourArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==7) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"HPID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"NAME"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"EN"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"TRACKID"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"TRACKNAME"];
            [mDic setObject:[arr objectAtIndex:5] forKey:@"SWT"];
            [mDic setObject:[arr objectAtIndex:6] forKey:@"DURATION"];

            
        }
        [resultPubHumourArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    
    
    //    return [resultLiveArry autorelease];
    
    
    return resultPubHumourArry;
}



/**
 得到幽默交互的数组
 
 @param carouselArr 幽默交互数组
 @return 返回已经分隔号的数据
 */
-(NSMutableArray *)getZZSPubHumourInteractiveArrWith:(NSMutableArray *)pubHumourInteractiveArr{
    
    NSMutableArray *resultPubHumourInteractiveArry = [[NSMutableArray alloc]init];
    for (int i=0; i<pubHumourInteractiveArr.count; i++) {
        NSString *str = [pubHumourInteractiveArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==3) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"HPID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"NAME"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"AT"];
            
        }
        [resultPubHumourInteractiveArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    
    
    //    return [resultLiveArry autorelease];
    
    
    return resultPubHumourInteractiveArry;
}



/**
 得到分享的数组

 @param shareUmengArr 分享友盟的数组
 @return 返回已经分割好的数据
 */
-(NSMutableArray *)getZZSShareUmengArrWith:(NSMutableArray *)shareUmengArr{
    
    NSMutableArray *resultShareUmengArry = [[NSMutableArray alloc]init];
    for (int i=0; i<shareUmengArr.count; i++) {
        NSString *str = [shareUmengArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==5) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"ID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"TITLE"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"ST"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"STYPE"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"SMODE"];

        }
        [resultShareUmengArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    
    
    //    return [resultLiveArry autorelease];
    
    
    return resultShareUmengArry;
}















//拼接直播

/**
 *  拼接的直播信息
 *
 *  @param liveArr 从服务器获取的直播信息
 *
 *  @return return 获取的拼接后上传服务器的直播数组信息
 */



-(NSMutableArray *)getZZSLiveArrWith:(NSMutableArray *)liveArr{
    
    NSMutableArray *resultLiveArry = [[NSMutableArray alloc]init];
    for (int i=0; i<liveArr.count; i++) {
        NSString *str = [liveArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==10) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"SID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"CHNAME"];
//            [mDic setObject:[arr objectAtIndex:2] forKey:@"EVENTNAME"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"SWT"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"DURATION"];
//            [mDic setObject:[arr objectAtIndex:5] forKey:@"ED"];
//            [mDic setObject:[arr objectAtIndex:6] forKey:@"EST"];
            [mDic setObject:[arr objectAtIndex:7] forKey:@"TRACKID"];
            [mDic setObject:[arr objectAtIndex:8] forKey:@"TRACKNAME"];
            [mDic setObject:[arr objectAtIndex:9] forKey:@"EN"];
        }
        [resultLiveArry addObject:mDic];
        
//        [mDic autorelease];
    }
    
    
//    return [resultLiveArry autorelease];
    
    
    return resultLiveArry;
}


-(NSMutableArray *)getZZSTimeMoveArrWith:(NSMutableArray *)timeMoveArr
{
    
//    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@",mserviceID,mchanelName,meventName,mwatchTime,mwatchDuration,meventLength,meventTime];
   // SID=SName=EName=T=WD=ED=EST
    
    NSMutableArray *resultTimeMoveArry = [[NSMutableArray alloc]init];
    for (int i=0; i<timeMoveArr.count; i++) {
        NSString *str = [timeMoveArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==11) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"SID"];//
            [mDic setObject:[arr objectAtIndex:1] forKey:@"CHNAME"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"EVENTNAME"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"SWT"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"DURATION"];
//            [mDic setObject:[arr objectAtIndex:5] forKey:@"ED"];//
//            [mDic setObject:[arr objectAtIndex:6] forKey:@"EST"];//
//            [mDic setObject:[arr objectAtIndex:7] forKey:@"TRACKID"];
//            [mDic setObject:[arr objectAtIndex:8] forKey:@"TRACKNAME"];
//            [mDic setObject:[arr objectAtIndex:9] forKey:@"EN"];
            [mDic setObject:[arr objectAtIndex:10] forKey:@"TWT"];
        }
        [resultTimeMoveArry addObject:mDic];
        
//        [mDic autorelease];
    }
    
    
//    return [resultTimeMoveArry autorelease];
    
    return resultTimeMoveArry;
}



-(NSMutableArray *)getZZSPolyMedicArrWith:(NSMutableArray *)polyMedicArr
{
    NSMutableArray *resultLiveArry = [[NSMutableArray alloc]init];
    for (int i=0; i<polyMedicArr.count; i++) {
        NSString *str = [polyMedicArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==7) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"HPID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"TRACKID"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"TRACKNAME"];
            [mDic setObject:[arr objectAtIndex:3] forKey:@"EN"];
            [mDic setObject:[arr objectAtIndex:4] forKey:@"NAME"];
            [mDic setObject:[arr objectAtIndex:5] forKey:@"SWT"];
            [mDic setObject:[arr objectAtIndex:6] forKey:@"DURATION"];
        }
        [resultLiveArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    
    
    //    return [resultLiveArry autorelease];
    
    
    return resultLiveArry;
 
}


-(NSMutableArray *)getZZSJKTVHealthArrWith:(NSMutableArray *)JKTVHealthArr
{
    NSMutableArray *resultLiveArry = [[NSMutableArray alloc]init];
    for (int i=0; i<JKTVHealthArr.count; i++) {
        NSString *str = [JKTVHealthArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==3) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"ID"];
            [mDic setObject:[arr objectAtIndex:1] forKey:@"NAME"];
            [mDic setObject:[arr objectAtIndex:2] forKey:@"BT"];
            
        }
        [resultLiveArry addObject:mDic];
        
        //        [mDic autorelease];
    }
    return resultLiveArry;

}

//拼接回看
/**
 *  拼接的回看信息
 *
 *  @param backArr 读取数据库中得回看信息
 *
 *  @return 拼接成功后需要上传服务器的回看信息
 */



-(NSMutableArray *)getZZSPlayBackArrWith:(NSMutableArray *)backArr{
    NSMutableArray *resultBackArr = [[NSMutableArray alloc]init];
    for (int i=0; i<backArr.count; i++) {
        NSString *str = [backArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc]init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==11) {
            //            [mDic setObject:[arr objectAtIndex:0] forKey:@"SID"];//回看事件编号 (这个参数在数据库并没有存储)
            [mDic setObject:[arr objectAtIndex:0] forKey:@"EVENTNAME"];//事件名称
            [mDic setObject:[arr objectAtIndex:1] forKey:@"EID"];//事件ID
            [mDic setObject:[arr objectAtIndex:2] forKey:@"SID"];//频道ID
            [mDic setObject:[arr objectAtIndex:3] forKey:@"CHNAME"];//频道名称
            [mDic setObject:[arr objectAtIndex:4] forKey:@"ED"];//回看节目时长
            [mDic setObject:[arr objectAtIndex:5] forKey:@"EST"];//开始时间
            [mDic setObject:[arr objectAtIndex:6] forKey:@"DURATION"];//观看时长
            [mDic setObject:[arr objectAtIndex:7] forKey:@"SWT"];//观看时刻
            [mDic setObject:[arr objectAtIndex:8] forKey:@"TRACKID"];
            [mDic setObject:[arr objectAtIndex:9] forKey:@"TRACKNAME"];
            [mDic setObject:[arr objectAtIndex:10] forKey:@"EN"];

        }
        [resultBackArr addObject:mDic];
        
//        [mDic autorelease];
    }
    
//    return [resultBackArr autorelease];
    
    return resultBackArr;
}
/**
 *  拼接的EPG信息
 *
 *  @param EPGArr 读取的EPG的数组
 *
 *  @return return 返回拼接后需要上传的EPG信息
 */



//拼接EPG
-(NSMutableArray *)getZZSEPGArrWith:(NSMutableArray *)EPGArr{
    NSMutableArray *resultEPGArr = [[NSMutableArray alloc]init];
    for (int i=0; i<EPGArr.count; i++) {
        NSString *str = [EPGArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==5) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"STYPE"];//频道分类
            [mDic setObject:[arr objectAtIndex:1] forKey:@"SNAME"];//频道名称
            [mDic setObject:[arr objectAtIndex:2] forKey:@"ETRANGE"];//事件区间
            [mDic setObject:[arr objectAtIndex:3] forKey:@"BD"];//浏览时长
            [mDic setObject:[arr objectAtIndex:4] forKey:@"BT"];//浏览时刻
        }
        [resultEPGArr addObject:mDic];
        
//        [mDic autorelease];
    }
//    return [resultEPGArr autorelease];
    return resultEPGArr;
}


-(NSMutableArray *)getZZSVODArrWith:(NSMutableArray *)VODArr
{
    
    
//     NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@",vodFenlei,vodName,vodTime,vodDuration];
    
    NSMutableArray *resultVODArr = [[NSMutableArray alloc]init];
    for (int i=0; i<VODArr.count; i++) {
        NSString *str = [VODArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        if (arr.count==12) {
            [mDic setObject:[arr objectAtIndex:0] forKey:@"PID"];//点播节目contentID
            [mDic setObject:[arr objectAtIndex:1] forKey:@"PROGNT"];//点播节目分类
            [mDic setObject:[arr objectAtIndex:2] forKey:@"PROGNAME"];//点播节目名称名称
            [mDic setObject:[arr objectAtIndex:3] forKey:@"SWT"];//点播时刻
            [mDic setObject:[arr objectAtIndex:4] forKey:@"DURATION"];//浏览时长
            [mDic setObject:[arr objectAtIndex:5] forKey:@"BID"];//总集ID,单集类型节目传@""
            [mDic setObject:[arr objectAtIndex:6] forKey:@"BNAME"];
            [mDic setObject:[arr objectAtIndex:7] forKey:@"TYPE"];
            [mDic setObject:[arr objectAtIndex:8] forKey:@"CLASS"];
            [mDic setObject:[arr objectAtIndex:9] forKey:@"TRACKID"];
            [mDic setObject:[arr objectAtIndex:10] forKey:@"TRACKNAME"];
            [mDic setObject:[arr objectAtIndex:11] forKey:@"EN"];
            
        }
        [resultVODArr addObject:mDic];
        
//        [mDic autorelease];
    }
//    return [resultVODArr autorelease];
    
    
    return resultVODArr;
  
    
    
    
    
}






/**
 *  拼接的搜索EPG信息
 *
 *  @param searchArr 搜索EPG的数组
 *
 *  @return 拼接后需要上传服务器的数组
 */

-(NSMutableArray *)getZZSSearchEPGArrWith:(NSMutableArray *)searchArr{
    
    NSMutableArray *resultArr = [[NSMutableArray alloc]init];
    for (int i=0; i<searchArr.count; i++) {
        NSString *str = [searchArr objectAtIndex:i];
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc]init];
        NSArray *arr = [str componentsSeparatedByString:@"+"];
        
        if (arr.count==4) {
            
            //zzs采集 搜索
            [mDic setObject:[arr objectAtIndex:0] forKey:@"SType"];//频道分类
            [mDic setObject:[arr objectAtIndex:1] forKey:@"SName"];//频道名称
            [mDic setObject:[arr objectAtIndex:2] forKey:@"SK"];//搜索关键字
            [mDic setObject:[arr objectAtIndex:3] forKey:@"ST"];//搜索时刻
            
            
        }
        
        [mDic removeObjectForKey:@"SType"];
        [mDic removeObjectForKey:@"SName"];//暂不需要频道分类 和 频道名称
        
        [resultArr addObject:mDic];
        
        
//        [mDic autorelease];
        
    }
    
//    return [resultArr autorelease];
    
    
    return resultArr;
}





#pragma mark  取值进行userID的确认
-(NSString *)userIDFromAPP
{
    NSString *userID;
//    NSString *mindPWD = [[NSUserDefaults standardUserDefaults]objectForKey:@"mindPWD"];
//    
//    if (isEmptyStringOrNilOrNull(mindPWD)) {
//        
//        userID = @"admin";
//    }else
//    {
//        userID =  [[NSUserDefaults standardUserDefaults]objectForKey:@"mindUserName"];
//
//    }

    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
    return userID;
}




#pragma mark --------------------
#pragma mark -- 获取文件路径

//   数据库  创建
//  获得存放数据库文件的沙盒地址
-(NSString *)databaseFilePath
{
    
    NSArray *filePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [filePath objectAtIndex:0];
    //    NSLog(@"filePath  ===    %@",filePath);
    NSString *dbFilePath = [documentPath stringByAppendingPathComponent:@"UserBehaviorSQL.db"];
    NSLog(@"filePath  ===    %@",dbFilePath);
    
    // NSString *filePath = @"/Users/apple/Desktop/Sqlite/UserBehaviorSQL.db";
    
    
    
    return dbFilePath;
}

-(void)postICS:(NSInteger)infoType{
    
    
    switch (infoType) {
        case BASEINFO://基本信息
        {
//            PeriodFile *plistManager = [[PeriodFile alloc] init];
//            
//            NSInteger uploadMode = [plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
//            
//            if (uploadMode == 3) {
//                HT_CollectionRequest *ics_request = [[HT_CollectionRequest alloc] initWithDelegate:self];
//                [ics_request postDeviceBaseInfoWith:<#(UC_GTTYPE)#>];
//            }
        }
            break;
        case USERACTIONINFO://用户行为
        {
            PeriodFile *plistManager = [[PeriodFile alloc] init];
            
            NSInteger uploadMode = [plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
            
            if (uploadMode == 3) {
                HT_CollectionRequest *ics_request = [[HT_CollectionRequest alloc] initWithDelegate:self];
                [ics_request postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];
            }
        }
            break;
        case NETINFO://网络信息
        {
            PeriodFile *plistManager = [[PeriodFile alloc] init];
            
            NSInteger uploadMode = [plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
            
            if (uploadMode == 3) {
                HT_CollectionRequest *ics_request = [[HT_CollectionRequest alloc] initWithDelegate:self];
                [ics_request postNetInfo];
            }
        }
            break;
        case ERRORINFO://错误信息
        {
            PeriodFile *plistManager = [[PeriodFile alloc] init];
            
            NSInteger uploadMode = [plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
            
            if (uploadMode == 3) {
                HT_CollectionRequest *ics_request = [[HT_CollectionRequest alloc] initWithDelegate:self];
                [ics_request postErrorInfo];
            }
        }
            break;
            
        default:
            break;
    }
    
    
    
}
-(void)postICS{

    PeriodFile *plistManager = [[PeriodFile alloc] init];
    
    NSInteger uploadMode = [plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
    
    
    
    if (uploadMode == 3) {
        HT_CollectionRequest *ics_request = [[HT_CollectionRequest alloc] initWithDelegate:self];
        [ics_request postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];
    }

}

@end
