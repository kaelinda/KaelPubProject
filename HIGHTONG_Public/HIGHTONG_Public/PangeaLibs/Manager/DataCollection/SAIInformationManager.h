//
//  SAIInformationManager.h
//  HIGHTONG
//
//  Created by apple on 14/12/18.
//  Copyright (c) 2014年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SystermTimeControl.h"
//#import "HTSW_MSI_DATA_PROTOCOL.h"
//#import "HttpRequest.h"
#import "HealthVodCollectionModel.h"
#import "JKTVHomeCollectionModel.h"
#import "DemandModel.h"
#import "EPGCollectionModel.h"
#import "CarouselPageModel.h"
#import "PubVodPlayCollectionModel.h"
#import "PubVodInteractiveCollectionModel.h"
#import "ShareCollectionModel.h"
@interface SAIInformationManager : NSObject
+(SAIInformationManager *)shareSAIInformation;


+(void)information:(NSString*)mserviceID andChanel:(NSString*)mchanelName andEventName:(NSString *)meventName andwatchTime:(NSString*)mwatchTime andWatchDuration:(NSString*)mwatchDuration andEventLength:(NSString*)meventLength andEventTime:(NSString*)meventTime;
+(void)epgLiveInformation:(EPGCollectionModel*)liveEpgCollectionModel;

+(void)timeMoveinformationServiceID:(NSString*)mserviceID andChanel:(NSString*)mchanelName andEventName:(NSString *)meventName andwatchTime:(NSString*)mwatchTime andWatchDuration:(NSString*)mwatchDuration andEventLength:(NSString*)meventLength andEventTime:(NSString*)meventTime;
+(void)epgTimeMoveInformation:(EPGCollectionModel*)timeMoveEpgCollectionModel;

+(void)BackinformationForeventName:(NSString*)beventName andEventID:(NSString*)bEventID andSerivceID:(NSString *)bserivceID andserviceName:(NSString*)bserviceName  andEventLength:(NSString*)beventLength andEventTime:(NSString*)beventTime andWatchDuration:(NSString*)bwatchDuration andWatchTime:(NSString*)bwatchTime;
+(void)epgBackEventInformation:(EPGCollectionModel*)backEventEpgCollectionModel;

+(void)EPGinformationForChanelFenlei:(NSString*)chanelFenlei andChanelName:(NSString*)chanelName andChanelInterval:(NSString*)chanelInterval andSkimDuration:(NSString*)skimDuration andSkimTime:(NSString*)skimTime;

+(void)VODinformationForVodFenlei:(NSString*)vodFenlei andVodName:(NSString*)vodName andVodTime:(NSString*)vodTime andVodDuration:(NSString*)vodDuration;
+(void)VODinformationForVodContentID:(NSString *)vodContentID andVodFenlei:(NSString*)vodFenlei andVodName:(NSString*)vodName andVodTime:(NSString*)vodTime andVodDuration:(NSString*)vodDuration;

//点播的采集信息
+(void)VodInformation:(DemandModel*)demandModel;



//医视频的采集信息

+(void)polyMedicineInfomation:(HealthVodCollectionModel*)healthModel;



//医视频界面业务数据采集

+(void)JKTVHomeInformation:(JKTVHomeCollectionModel*)jkTVModel;

//轮播图的信息采集

+(void)carouselInfomation:(CarouselPageModel*)carouselModel;



//幽默视频的采集信息

+(void)pub_humourInformation:(PubVodPlayCollectionModel*)pubCollectionModel;

//幽默视频的交互信息的采集点赞
+(void)pub_humourInteractiveInformation:(PubVodInteractiveCollectionModel*)pubInteractiveModel;


+(void)shareUMengInformation:(ShareCollectionModel*)shareCollectionModel;






+(void)EPGSearchHotKeyandSearchTime:(NSString *)searchHotKeyandTime;
+(void)VODSearchHotKeyandSearchTime:(NSString *)searchHotKeyandTime;

+(void)ETHForIP:(NSString*)IPStr andAW:(NSString*)AWStr andTTL:(NSString*)TTLStr andBW:(NSString*)BWStr;

+(void)OPSForAVTYPE:(NSString*)AVTYPEStr andOPTYPE:(NSString*)OPTYPEStr andOPSST:(NSString*)OPSSTStr andDuration:(NSString*)DurationStr;

+(void)OPSForNetType:(NSString *)netTypeStr andWIFISSID:(NSString*)wifiSSIDStr andSourceType:(NSString *)sourceTypeStr andAVName:(NSString *)avNameStr andOPSCode:(NSString *)opsCodeStr andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr;



+(NSString *)changeWathchTimeDurationForWatchDuration:(NSString*)watchDuration andEventDuration:(NSString*)eventDuration;

//+(void)currentMedia:(NSDictionary*)dic  andPlayType:(NSInteger)playType;

//暂时不用，你就装作看不见
//+(void)getONLineTime:(NSString*)ONLine  AndOFFLineTime:(NSString *)OFFLine;

+(void)ONLine;
+(void)OFFLine;
+(NSString *)serviceTimeTransformStr;
+(NSString *)LookForUserNameInformatin;

+(void)deleteUserInformation;
+(void)deleteUserOPS;
+(void)deleteUserETH;



@end
