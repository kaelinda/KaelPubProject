//
//  HT_UsercollectionService.h
//  HIGHTONG
//
//  Created by Alaca on 15/5/13.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HT_CollectionRequest.h"
#import "GETBaseInfo.h"
#import "HTSQLManager.h"
#import "SAIInformationManager.h"


#define COM_CONFIG            @"config"
#define COM_DEVINFO_UPLOAD    @"uploadDevInfo"
#define COM_ERRORINFO_UPLOAD  @"uploadOpsInfo"
#define COM_USERACTION_UPLOAD @"uploadUserAction"
#define COM_NETINFO_UPLOAD    @"uploadNetInfo"


#define PREFILEINFO                   0
#define BASEINFO                      1
#define USERACTIONINFO                2
#define NETINFO                       3
#define ERRORINFO                     4

@interface HT_UsercollectionService : NSObject<HT_CollectionResuqestDelegate>
{
    NSInteger _postMode;//模式 配置文件中的上传模式
    BOOL _isPeriod;//允许周期上传
    BOOL _isResign;
    
    NSInteger _devInfoMode;
    NSInteger _netInfoMode;
    NSInteger _opsInfoMode;
    NSInteger _useractionMode;
    
}


@property (nonatomic,assign) HT_CollectionRequest *uRequest;


//上传信息分类
@property (nonatomic,assign) BOOL isBaseInfo;//设备基本信息  是否上传
@property (nonatomic,assign) BOOL isUserAction;//用户行为信息采集  是否上传
@property (nonatomic,assign) BOOL isNetInfo;//网络信息采集  是否上传
@property (nonatomic,assign) BOOL isErrorInfo;//错误信息采集 是否上传
/*
 *新版本暂不考虑用户行为中 各个小类的上传策略区分
@property (nonatomic,assign) BOOL isTVLiveData;//直播信息
@property (nonatomic,assign) BOOL isBackData;//回看信息
@property (nonatomic,assign) BOOL isTimeMoveData;//时移信息
@property (nonatomic,assign) BOOL isEPGData;//EPG信息
@property (nonatomic,assign) BOOL isVODData;//点播信息
*/
//各部分上传失败后 再次上传的时间间隔
@property (nonatomic,assign) NSInteger postBaseInfoSpace;//设备基本信息上传周期
@property (nonatomic,assign) NSInteger postUserActionSpace;//用户行为信息上传周期
@property (nonatomic,assign) NSInteger postNetInfoSpace;//网络信息上传周期
@property (nonatomic,assign) NSInteger postErrorInfoSpace;//错误信息上传周期

@property (nonatomic,assign) NSInteger prefileSpace;//错误信息上传周期

//各部分上传周期
@property (nonatomic,assign) NSInteger postBaseInfoTime;//设备基本信息上传周期
@property (nonatomic,assign) NSInteger postUserActionTime;//用户行为信息上传周期
@property (nonatomic,assign) NSInteger postNetInfoTime;//网络信息上传周期
@property (nonatomic,assign) NSInteger postErrorInfoTime;//错误信息上传周期

//各部分上传失败后 重试次数
@property (nonatomic,assign) NSInteger baseInfoRetryNum;//设备基本信息重传次数
@property (nonatomic,assign) NSInteger actionInfoRetryNum;//行为信息重传次数
@property (nonatomic,assign) NSInteger netInfoRetryNum;//网络信息重传次数
@property (nonatomic,assign) NSInteger errorRetryNum;//错误信息重传次数


//上传的模式
@property (nonatomic,assign) NSInteger baseInfoMode;//设备基本信息上传模式
@property (nonatomic,assign) NSInteger actionInfoMode;//行为信息上传模式
@property (nonatomic,assign) NSInteger netInfoMode;//网络信息上传模式
@property (nonatomic,assign) NSInteger errorMode;//错误信息上传模式


+(HT_UsercollectionService *)shareUserCollectionservice;
/**
 *  发送设备基本信息
 */
-(void)postTBEData;


-(void)postBaseInfoData:(UC_GTTYPE)startType;

/**
 是否有用户切换

 @param isNewUser YES:游客上线 或者 user上线；  NO:游客下线或者用户下线
 YES--->>当前token刚刚生效
 NO--->>当前token 即将被注销
 
 
 */
-(void)changeUser:(BOOL)isNewUser;

/**
 *  发送用户行为信息
 */
-(void)postUserActionData;
/**
 *  发送网络信息
 */
-(void)postNetInfoData;
/**
 *  发送错误信息
 */
-(void)postErrorInfoData;
/**
 *  获取配置文件
 */
-(void)getPrefileData;
/**
 *  四种信息 各发送一遍
 */
//-(void)postData;



@end
