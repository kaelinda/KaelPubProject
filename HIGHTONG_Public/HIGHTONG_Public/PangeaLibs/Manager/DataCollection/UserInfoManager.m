//
//  UserInfoManager.m
//  HIGHTONG
//
//  Created by Alaca on 15/5/11.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import "UserInfoManager.h"
#import "GTMBase64.h"
#import "SBJson.h"
#import "HTSQLManager.h"
#import "GETBaseInfo.h"
#import "SAIInformationManager.h"
@interface UserInfoManager()
{

    
}

@end

@implementation UserInfoManager

#pragma mark - 获取设备基本信息
-(NSString *)getDeviceinfo{
    NSMutableDictionary *DevInfo = [[NSMutableDictionary alloc] init];
    [DevInfo setObject:[self getSDInfo] forKey:@"SD"];
    [DevInfo setObject:[self getFWInfo] forKey:@"FW"];
    NSLog(@"TBE 字典：%@",DevInfo);
    //这里需要返回JSON串
    NSString *str = [DevInfo JSONRepresentation];
    NSLog(@"TBE数据加密前 Str： %@",str);
    str = [self getTheResultStringWith:str];
    return str;
}


-(NSString *)getDeviceinfoAndIsScheme:(BOOL)isScheme andIsFW:(BOOL)isFW andIsSD:(BOOL)isSD
{
    NSString *str = [[NSString alloc]init];
    NSMutableDictionary *DevInfo = [[NSMutableDictionary alloc] init];

    if (isFW) {
        [DevInfo setObject:[self getFWInfoAndisScheme:isScheme] forKey:@"FW"];
    }
    
    if (isSD) {
        [DevInfo setObject:[self getSDInfo] forKey:@"SD"];
    }
    NSLog(@"新的信息采集我要的数据%@",DevInfo);
    //这里需要返回JSON串
    str = [DevInfo JSONRepresentation];
    NSLog(@"TBE数据加密前 Str： %@",str);
    str = [self getTheResultStringWith:str];
    return str;

}



//-------SDIfo
-(NSDictionary *)getSDInfo{
   // GETBaseInfo *baeseInfo = [[GETBaseInfo alloc] init];
  
    NSMutableDictionary *BDic = [[NSMutableDictionary alloc] init];
    [BDic setObject:[[UIDevice currentDevice] systemName] forKey:@"OSTYPE"];
    [BDic setObject:[[UIDevice currentDevice] systemVersion] forKey:@"OSVER"];
    [BDic setObject:APP_VERSION forKey:@"SWVER"];//软件版本  OK
    //获取无线网络的类型
    GETBaseInfo *baseInfo = [[GETBaseInfo alloc] init];
    NSString *networktype = [GETBaseInfo getNetworkType];
    if (![networktype isEqualToString:@"NOTREACH"]) {
        [BDic setObject:networktype forKey:@"NETTYPE"];//  待解决   网络类型
    }
    
    //运营商
    [BDic setObject:[baseInfo getMobileServiceType_STRING] forKey:@"OP"];
    
    //------获取经纬度的信息
    NSString *loc = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
    if (loc.length==0) {
        loc =@"0.000000,0.000000";
    }
    [BDic setObject:loc forKey:@"LOCATION"];
    [BDic setObject:[baseInfo getIPAddressType] forKey:@"IP"];   //  待解决   IP地址
    
    [BDic setObject:[SAIInformationManager serviceTimeTransformStr] forKey:@"GT"];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"]) {
        [BDic setObject:@"" forKey:@"GTTYPE"];
        
    }else
    {
        [BDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"] forKey:@"GTTYPE"];
        
    }
    
    
    return BDic;
}
//---------FWIfo
-(NSDictionary *)getFWInfo{
    GETBaseInfo *baseInfo = [[GETBaseInfo alloc] init];
    
//    [BDic setObject:[baeseInfo getModel] forKey:@"MODEL"]; //设备型号
//    [BDic setObject:[baeseInfo getSSIDwith:NO AndWIFIMacIPwith:YES] forKey:@"WIFISSID"];//
    
    //zzs add 摄像头像素
//    NSString *FCAMR = [baseInfo getResolutionOfFront:YES andBehind:NO];
//    NSString *BCAMR = [baseInfo getResolutionOfFront:NO andBehind:YES];
    
    NSMutableDictionary *EDic = [[NSMutableDictionary alloc] init];
    //[EDic setObject:[baseInfo getSystem] forKey:@"OS"];//OK
    [EDic setObject:@"" forKey:@"TFLASH"];// 待解决FLASH容量是啥？
    [EDic setObject:[baseInfo totalDiskSpace] forKey:@"TRAM"];//总容量 OK （单位MB）
    [EDic setObject:@"" forKey:@"SDCARD"];
    [EDic setObject:[baseInfo getSSIDwith:NO AndWIFIMacIPwith:YES] forKey:@"WIFIMAC"];//
    [EDic setObject:[baseInfo getSSIDwith:YES AndWIFIMacIPwith:NO] forKey:@"WIFISSID"];//
    [EDic setObject:[baseInfo getModel] forKey:@"MODEL"]; //设备型号
    [EDic setObject:@"Apple" forKey:@"MANU"];//厂家ID   OK   "DEVSN"://设备序列号
    [EDic setObject:[baseInfo getUUID] forKey:@"DEVSN"];//设备序列号
    [EDic setObject:@"" forKey:@"SCSN"];//智能卡序列号
    [EDic setObject:[baseInfo getUUID] forKey:@"CHIPID"];//芯片唯一标识码
    [EDic setObject:@"" forKey:@"HWVER"];//硬件版本号
    [EDic setObject:@"" forKey:@"INID"];//设备内部编号
    [EDic setObject:@"" forKey:@"ETHMAC"];//设备以太网MAC
    [EDic setObject:[baseInfo totalDiskSpace] forKey:@"HD"];//硬盘总容量，单位为GB
    
    
//    [EDic setObject:FCAMR forKey:@"FCAMR"];//zzs 采集 前置摄像头
//    [EDic setObject:BCAMR forKey:@"BCAMR"];//zzs 采集 后置摄像头
    
    //    [EDic setObject:@"" forKey:@"SCSN"];
    //    [EDic setObject:str forKey:@"WMAC"];
    //    [EDic setObject:@"" forKey:@"WSTR"];
    [EDic setObject:[baseInfo getResolution] forKeyedSubscript:@"RES"];//屏幕分辨率 OK
    [EDic setObject:[SAIInformationManager serviceTimeTransformStr] forKey:@"GT"];
    
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"location"];
    return EDic;
}


//用户信息采集新形式的固件信息

-(NSDictionary *)getFWInfoAndisScheme:(BOOL)isScheme
{
    GETBaseInfo *baseInfo = [[GETBaseInfo alloc] init];
    
    //    [BDic setObject:[baeseInfo getModel] forKey:@"MODEL"]; //设备型号
    //    [BDic setObject:[baeseInfo getSSIDwith:NO AndWIFIMacIPwith:YES] forKey:@"WIFISSID"];//
    
    //zzs add 摄像头像素
    //    NSString *FCAMR = [baseInfo getResolutionOfFront:YES andBehind:NO];
    //    NSString *BCAMR = [baseInfo getResolutionOfFront:NO andBehind:YES];
    
    NSMutableDictionary *EDic = [[NSMutableDictionary alloc] init];
    //[EDic setObject:[baseInfo getSystem] forKey:@"OS"];//OK
    [EDic setObject:@"Apple" forKey:@"MANU"];//厂家ID   OK   "DEVSN"://设备序列号
    [EDic setObject:[baseInfo iphoneType] forKey:@"MODEL"]; //设备型号
//    [EDic setObject:[baseInfo totalDiskSpace] forKey:@"TRAM"];//总容量 OK （单位MB）
    [EDic setObject:[baseInfo getRAM_Capacity] forKey:@"TRAM"];
    [EDic setObject:[baseInfo getResolution] forKeyedSubscript:@"RES"];//屏幕分辨率 OK
    if ([baseInfo getSSIDwith:NO AndWIFIMacIPwith:YES]) {
    
        NSMutableString *mustring = [NSMutableString stringWithString:[baseInfo getSSIDwith:NO AndWIFIMacIPwith:YES]];
        NSArray *arr = [[NSArray alloc]init];
        
        arr = [mustring componentsSeparatedByString:@":"];
        NSString *lastString;
        
        NSString *string;
        NSMutableArray *mArr = [NSMutableArray array];
        for (int i = 0; i<arr.count; i++) {
            string = [arr objectAtIndex:i];
            
            if (string.length<=1) {
                
                string = [NSString stringWithFormat:@"0%@",string];
            }
            
            
            [mArr addObject:string];
        }
        
        lastString =  [mArr componentsJoinedByString:@":"];

        
        [EDic setObject:lastString forKey:@"WIFIMAC"];//



    }
    
    
    [EDic setObject:[baseInfo getSSIDwith:YES AndWIFIMacIPwith:NO] forKey:@"WIFISSID"];//

    
    if (isScheme) {
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"USERMAC"]) {
            
          [EDic setObject:@"" forKey:@"USERMAC"];
        }else
        {
            [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERMAC"] forKey:@"USERMAC"];
        }
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"INSTANCECODE"]) {
         [EDic setObject:@"" forKey:@"INSTANCECODE"];
        }else
        {
            [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"INSTANCECODE"] forKey:@"INSTANCECODE"];
        }
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"OPENID"]) {
            
            [EDic setObject:@"" forKey:@"OPENID"];
        }else
        {
            [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENID"] forKey:@"OPENID"];
        }
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"MOBILE"]) {
            [EDic setObject:@"" forKey:@"MOBILE"];
        }else
        {
            [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MOBILE"] forKey:@"MOBILE"];
        }
        
        
//        [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERMAC"] forKey:@"USERMAC"];
//        [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"INSTANCECODE"] forKey:@"INSTANCECODE"];
//        [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENID"] forKey:@"OPENID"];
//        [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MOBILE"] forKey:@"MOBILE"];

    }
    
    
//    [EDic setObject:@"" forKey:@"TFLASH"];// 待解决FLASH容量是啥？
//    [EDic setObject:@"" forKey:@"SDCARD"];
//    [EDic setObject:[baseInfo getUUID] forKey:@"DEVSN"];//设备序列号
//    [EDic setObject:@"" forKey:@"SCSN"];//智能卡序列号
//    [EDic setObject:[baseInfo getUUID] forKey:@"CHIPID"];//芯片唯一标识码
//    [EDic setObject:@"" forKey:@"HWVER"];//硬件版本号
//    [EDic setObject:@"" forKey:@"INID"];//设备内部编号
//    [EDic setObject:@"" forKey:@"ETHMAC"];//设备以太网MAC
//    [EDic setObject:[baseInfo totalDiskSpace] forKey:@"HD"];//硬盘总容量，单位为GB
    
    
    //    [EDic setObject:FCAMR forKey:@"FCAMR"];//zzs 采集 前置摄像头
    //    [EDic setObject:BCAMR forKey:@"BCAMR"];//zzs 采集 后置摄像头
    
    //    [EDic setObject:@"" forKey:@"SCSN"];
    //    [EDic setObject:str forKey:@"WMAC"];
    //    [EDic setObject:@"" forKey:@"WSTR"];
    [EDic setObject:[SAIInformationManager serviceTimeTransformStr] forKey:@"GT"];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"]) {
        [EDic setObject:@"" forKey:@"GTTYPE"];

    }else
    {
        [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"] forKey:@"GTTYPE"];

    }
    
//    [EDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"] forKey:@"GTTYPE"];
    
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"location"];
    return EDic;

}






#pragma mark - 获取用户行为信息
-(NSString *)getUserActionInfoWithTVLiveData:(BOOL)isTVLiveData andTimeMove:(BOOL)isTimeMove andBackData:(BOOL)isBackData andEPGDaga:(BOOL)isEpgData andVODData:(BOOL)isVODData andIsNewData:(BOOL)isaNewData{
    
    HTSQLManager *htsq = [HTSQLManager shareInstance];
    self.userActionArr = [htsq getNewAllPeople];
    
    NSLog(@"usermanager 里面的用户行为信息 %@",_userActionArr);
    
    //从数据库中获取数据
    NSMutableArray *actionArray = [[NSMutableArray alloc] init];
    
    NSDictionary *onLinemDic  = [[NSDictionary alloc] init];//在线时间 01
    NSDictionary *TVLiveDic   = [[NSDictionary alloc] init];//直播
    NSDictionary *TimeMoveDic = [[NSDictionary alloc] init];//时移
    NSDictionary *BackDic     = [[NSDictionary alloc] init];//回看
    NSDictionary *VODDic      = [[NSDictionary alloc] init];//点播
    NSDictionary *EPGBDic     = [[NSDictionary alloc] init];//EPG浏览
    NSDictionary *EPGSDic     = [[NSDictionary alloc] init];//EPG搜索
    NSDictionary *VODSDic     = [[NSDictionary alloc] init];//VOD搜索
    NSDictionary *PolyENDic    = [[NSDictionary alloc] init];//医院入口
    NSDictionary *PolyVideoDic = [[NSDictionary alloc] init];//医视频观看信息
    NSDictionary *CarouselDic = [[NSDictionary alloc]init];//轮播图的信息
    NSDictionary *PubHumourDic = [[NSDictionary alloc]init];//幽默视频的信息
    NSDictionary *PubHumourInteractiveDic = [[NSDictionary alloc]init];//幽默交互的信息
    NSDictionary *ShareUmengDic = [[NSDictionary alloc]init];//分享友盟的信息;
    //上线时间、下线时间 必须上传
    onLinemDic = [self getUserActionWithSInfo:@"01" andAInfo:@"Online Time" andIsNewData:isaNewData];
    
    NSMutableArray *mmarr = [onLinemDic objectForKey:@"I"];
    if (mmarr.count>0) {
        [actionArray addObject:onLinemDic];
    }

//视频相关
    BackDic      = [self getUserActionWithSInfo:@"11" andAInfo:@"P" andIsNewData:isaNewData];
    VODDic       = [self getUserActionWithSInfo:@"12" andAInfo:@"P" andIsNewData:isaNewData];
    TimeMoveDic  = [self getUserActionWithSInfo:@"13" andAInfo:@"P" andIsNewData:isaNewData];
    PolyVideoDic = [self getUserActionWithSInfo:@"14" andAInfo:@"p" andIsNewData:isaNewData];
    CarouselDic = [self  getUserActionWithSInfo:@"31" andAInfo:@"B" andIsNewData:isaNewData];
//界面操作相关
    TVLiveDic    = [self getUserActionWithSInfo:@"10" andAInfo:@"P" andIsNewData:isaNewData];
    EPGBDic      = [self getUserActionWithSInfo:@"20" andAInfo:@"B" andIsNewData:isaNewData];
    EPGSDic      = [self getUserActionWithSInfo:@"20" andAInfo:@"S" andIsNewData:isaNewData];
    VODSDic      = [self getUserActionWithSInfo:@"12" andAInfo:@"S" andIsNewData:isaNewData];
    PolyENDic    = [self getUserActionWithSInfo:@"50" andAInfo:@"B" andIsNewData:isaNewData];
    PubHumourDic = [self getUserActionWithSInfo:@"15" andAInfo:@"P" andIsNewData:isaNewData];
    PubHumourInteractiveDic = [self getUserActionWithSInfo:@"15" andAInfo:@"FAV" andIsNewData:isaNewData];
    ShareUmengDic = [self getUserActionWithSInfo:@"01" andAInfo:@"S" andIsNewData:isaNewData];
    
    if (TVLiveDic.count>0 && isTVLiveData) {
        [actionArray addObject:TVLiveDic];

    }
    if (BackDic.count>0 && isBackData) {
        [actionArray addObject:BackDic];

    }
    if (VODDic.count>0 && isVODData) {
        [actionArray addObject:VODDic];

    }
    if (TimeMoveDic.count>0 && isTimeMove) {
        [actionArray addObject:TimeMoveDic];

    }
    if (EPGBDic.count>0 && isEpgData) {
        [actionArray addObject:EPGBDic];
 
    }
    if (EPGSDic.count>0 && isEpgData) {
        [actionArray addObject:EPGSDic];
 
    }
    if (PolyENDic.count>0) {
        [actionArray addObject:PolyENDic];
    }
    if (PolyVideoDic.count>0) {
        [actionArray addObject:PolyVideoDic];
    }
    
    if (CarouselDic.count>0) {
        
        [actionArray addObject:CarouselDic];
    }
    
    if (PubHumourDic.count>0) {
        
        [actionArray addObject:PubHumourDic];
    }
    
    if (PubHumourInteractiveDic.count>0) {
        
        [actionArray addObject:PubHumourInteractiveDic];
    }
    
    if (ShareUmengDic.count>0) {
        
        [actionArray addObject:ShareUmengDic];
    }
    
    
    
    
    NSLog(@"用户行为信息加密前%@",actionArray);

    NSString *resultStr = [self getTheResultStringWith:[actionArray JSONRepresentation]];
    
    return resultStr;
}
//获取行为信息的字典
-(NSDictionary *)getUserActionWithSInfo:(NSString *)SInfo andAInfo:(NSString *)AInfo andIsNewData:(BOOL)isaNewData{
    NSMutableDictionary *actDic = [[NSMutableDictionary alloc] init];
    [actDic setObject:SInfo forKey:@"S"];
    [actDic setObject:AInfo forKey:@"A"];
    
    NSMutableArray *IArray = [self getUserActionArrWithSInfo:SInfo AInfo:AInfo andIsNewData:isaNewData];
    [actDic setObject:IArray forKey:@"I"];
    if (IArray.count==0) {
        return [NSDictionary dictionary];
    }
    return actDic;
}
//获取行为信息的 数组
-(NSMutableArray *)getUserActionArrWithSInfo:(NSString *)SInfo AInfo:(NSString *)AInfo andIsNewData:(BOOL)isaNewData{
    //这个需要从数据库中--- 查询 ---获取
    NSMutableArray *arr = [[NSMutableArray alloc] init];
//    HTSQLManager *htsq = [HTSQLManager shareInstance];
//    self.userActionArr = [htsq getNewAllPeople];
//
//    if (self.userActionArr.count==0) {
//        self.userActionArr = [htsq getNewAllPeople];
//    }else{
//        NSArray *timeArr = [self.userActionArr objectAtIndex:0];
//        if (timeArr.count == 0) {
//            self.userActionArr = [htsq getNewAllPeople];
//        }
//    }
//根据信息 往数组中添加各用户行为
    //0:上下线时间 1：直播  2：回看  3：EPG浏览  4：EPG搜索  5：点播播放  6：点播搜索 7：时移播放 8：医疗健康 9：医视频播放 10:轮播图视频的播放 11:幽默视频 12:幽默交互 13:分享友盟
    if ([SInfo isEqualToString:@"01"]) {
        
        if ([AInfo isEqualToString:@"S"]) {
            
            arr = [self.userActionArr objectAtIndex:13];//分享友盟
        }
        
        if ([AInfo isEqualToString:@"Online Time"]) {
            
            arr =[self.userActionArr objectAtIndex:0];//基础功能 上下线时间
        }
       
    }
    if ([SInfo isEqualToString:@"10"]) {
        arr =[self.userActionArr objectAtIndex:1];//直播
    }
    if ([SInfo isEqualToString:@"11"]) {
        arr =[self.userActionArr objectAtIndex:2];//回看
    }
    if ([SInfo isEqualToString:@"20"]) {
        if ([AInfo isEqualToString:@"B"]) {
            arr =[self.userActionArr objectAtIndex:3];//EPG浏览
        }
        if([AInfo isEqualToString:@"S"]){
            arr =[self.userActionArr objectAtIndex:4];//EPG搜索
        }
    }
    if ([SInfo isEqualToString:@"12"]) {
        if ([AInfo isEqualToString:@"P"]) {
            arr = [self.userActionArr objectAtIndex:5];//点播播放
        }
        if ([AInfo isEqualToString:@"S"]) {
            arr = [self.userActionArr objectAtIndex:6];//点播搜索

        }
    }
    if ([SInfo isEqualToString:@"13"]) {
        arr = [self.userActionArr objectAtIndex:7];//时移
    }

    if ([SInfo isEqualToString:@"50"]) {
        arr = [self.userActionArr objectAtIndex:8];//医疗健康入口

    }
    if ([SInfo isEqualToString:@"14"]) {
        arr = [self.userActionArr objectAtIndex:9];//医视频播放

    }
    
    if ([SInfo isEqualToString:@"31"]) {
        
        arr = [self.userActionArr objectAtIndex:10];//轮播图的信息
    }
    
    if ([SInfo isEqualToString:@"15"]) {
        if ([AInfo isEqualToString:@"P"]) {
            arr = [self.userActionArr objectAtIndex:11];//幽默视频的信息
         }
        if ([AInfo isEqualToString:@"FAV"]) {
            arr = [self.userActionArr objectAtIndex:12];//幽默视频的信息

        }
    }
    
    
    
    return arr;
}
#pragma mark - 获取网络信息
-(NSString *)getNetInfo{

    HTSQLManager *htsq = [HTSQLManager shareInstance];
    NSMutableArray *mArr =[htsq getETHInformations];
    NSString *resultStr = [mArr JSONRepresentation];
    NSLog(@"网络信息加密前%@",resultStr);

//    resultStr = @"这里是网络信息";

    resultStr = [self getTheResultStringWith:resultStr];
    return resultStr;//这里应该是加密以后的JSON串
}
#pragma mark - 获取错误信息
-(NSString *)getErrorInfo{
    
    HTSQLManager *htsq = [HTSQLManager shareInstance];
    NSMutableArray *mArr =[htsq getOPSInformations];
    NSString *resultStr = [mArr JSONRepresentation];
    NSLog(@"错误信息加密前%@",resultStr);
//    resultStr = @"这里是错误信息";
    resultStr = [self getTheResultStringWith:resultStr];
    return resultStr;//这里应该是加密以后的JSON串
}
#pragma mark - 基本的封装方法
/**
 *  删除字符串中的回车键并对其进行base64加密
 *  @param JSONStr JSON化的字符串
 *  @return JSON并加密后的字符串
 */
-(NSString *)getTheResultStringWith:(NSString *)JSONStr{
    NSString *str;
    //删掉换行符
    str = [self deleteTheStr:@"\n" fromLongStr:JSONStr];
//    str = [self base64CodeWithStr:str];
//    str = [self deleteTheStr:@"\n" fromLongStr:JSONStr];
    return [self deleteTheStr:@"\n" fromLongStr:[self base64CodeWithStr:str]];
}
/**
 *  获取信息的唯一ID
 *
 *  @return 返回设备信息的唯一ID
 */
-(NSString *)getMsgID{
    
    NSString* dateStr;
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    //[formatter setDateFormat:@"YYYY.MM.dd.hh.mm.ss"];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss:SSS"];//这里要求精确到毫秒
    dateStr = [formatter stringFromDate:[NSDate date]];
    
    dateStr = [self deleteTheStr:@"-" fromLongStr:dateStr];
    dateStr = [self deleteTheStr:@":" fromLongStr:dateStr];
    dateStr = [self deleteTheStr:@" " fromLongStr:dateStr];
    
    NSString *msgID = [[NSString alloc] initWithFormat:@"%@",dateStr];//这里有些啰嗦了 直接可以 return dateStr;
    if (msgID.length==0) {
        msgID = @"";
    }
    return msgID;
}
-(NSString *)deleteTheStr:(NSString *)Str fromLongStr:(NSString *)longStr{
    NSMutableString *mStr = [[NSMutableString alloc] init];
    NSArray *arr = [longStr componentsSeparatedByString:Str];
    for (int i=0; i<arr.count; i++) {
        [mStr appendString:[arr objectAtIndex:i]];
    }
    
    return mStr;
}
-(NSString *)base64CodeWithStr:(NSString *)str{
    
    NSData *Data=[str dataUsingEncoding:NSUTF8StringEncoding];
    Data = [GTMBase64 encodeData:Data];
    NSString *codestr=[[NSString alloc] initWithData:Data encoding:NSUTF8StringEncoding] ;
    return codestr;
}

@end
