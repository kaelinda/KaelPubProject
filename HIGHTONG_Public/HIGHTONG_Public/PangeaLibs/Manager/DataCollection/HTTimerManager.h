//
//  HTTimerManager.h
//
//  Created by lailibo on 16/8/24.
//  Copyright © lailibo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HTTimerManager;
@protocol HTTimerManagerDelegate <NSObject>
@optional

-(void)countingTo:(NSTimeInterval)time timeToShowString:(NSString*)timeToShowString;


@end


@interface HTTimerManager : NSObject
{
    NSTimeInterval timeUserValue;
    
    NSDate *startCountDate;
    NSDate *pausedTime;
    
    NSDate *date1970;
    NSDate *timeToCountOff;
    NSDateFormatter *dateFormatter;
}
@property (strong) NSTimer *timer;

/*Time format wish to display in label*/
@property (nonatomic,strong) NSString *timeFormat;

/*Target label obejct, default self if you do not initWithLabel nor set*/
@property (strong) UILabel *timeLabel;

/*Type to choose from stopwatch or timer*/
//@property (assign) MZTimerLabelType timerType;

/*is The Timer Running?*/
@property (assign,readonly) BOOL counting;

/*do you reset the Timer after countdown?*/
@property (assign) BOOL resetTimerAfterFinish;

@property (strong) id<HTTimerManagerDelegate> delegate;


-(void)start;
-(void)pause;
-(void)reset;
-(void)setup;
-(void)updateLabel:(NSTimer*)timer;
@end
