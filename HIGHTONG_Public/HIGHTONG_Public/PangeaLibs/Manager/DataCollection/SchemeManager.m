//
//  SchemeManager.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/10/21.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "SchemeManager.h"
#import "CustomAlertView.h"
static SchemeManager *shared = nil;
@implementation SchemeManager

+ (instancetype)shardSchemeManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc]init];
        
    });
    return shared;

}


-(instancetype)init{
    self = [super init];
    
    if (self) {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            //这里是1.0f后 你要做的事情
//            [self startNotifi];
//        });
    }
    return self;
}






-(void)schemeForSchemeUrl:(NSString *)schemeUrl
{
//    [CustomAlertView showAlertView:@"是的我走了" andMessage:@"你真好啊"];
    if (schemeUrl.length>0) {
        NSArray *schemeArray = [NSArray array];
        schemeArray = [schemeUrl componentsSeparatedByString:@"://"];
       
        if (!IsArrEmpty(schemeArray)) {
            NSString *schemeString = [schemeArray objectAtIndex:0];
            
            if ([schemeString isEqualToString:KSCHEMEID]) {
                
                NSString *urlStr = [schemeArray objectAtIndex:1];
                
                NSArray *strArr = [[NSArray alloc]init];
                
                if ([urlStr rangeOfString:@"?"].location !=NSNotFound) {
                    
                    strArr = [urlStr componentsSeparatedByString:@"?"];
                
                    if (!IsArrEmpty(strArr)) {
                        
                        NSString *oppenIDString = [strArr objectAtIndex:0];
                        if ([oppenIDString isEqualToString:@"openapp/wx"]) {
                            NSString *codestring = [strArr objectAtIndex:1];
                            NSArray *arr = [[NSArray alloc]init];
                            arr = [codestring componentsSeparatedByString:@"&"];
                            
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                            for (int i =0; i<arr.count; i++) {
                                
                                NSString *str = [NSString stringWithFormat:@"%@",[arr objectAtIndex:i]];
                                
                                NSArray * amarr = [[NSArray alloc]init];
                                
                                amarr = [str componentsSeparatedByString:@"="];
                                
                                [dic setObject:[amarr objectAtIndex:1] forKey:[amarr objectAtIndex:0]];
                            }

                            [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"userMac"] forKey:@"USERMAC"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"instanceCode"] forKey:@"INSTANCECODE"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"openId"] forKey:@"OPENID"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"mobile"] forKey:@"MOBILE"];
                        }
                        
                    }else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"USERMAC"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"INSTANCECODE"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"OPENID"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"MOBILE"];
                        
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"GTTYPE"];
                    
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"SchemeAction"];
                    [SAIInformationManager ONLine];
                    
                    [[HT_UsercollectionService shareUserCollectionservice] postBaseInfoData:kSchemeStart];
                    
                    [[HT_UsercollectionService shareUserCollectionservice] postNetInfoData];
                    [[HT_UsercollectionService shareUserCollectionservice] postUserActionData];
                    [[HT_UsercollectionService shareUserCollectionservice] postErrorInfoData];



                    [[NSUserDefaults standardUserDefaults]synchronize];

                }

                
                
                
                
                
                
            }
            
            
            
            
        }
        
        
        
    }
    
    
    
}





-(void)schemeForSourceApplication:(NSString *)sourceApplication andSchemeUrl:(NSString *)schemeUrl
{
    NSString *urlStr = [NSString stringWithFormat:@"%@",schemeUrl];
    
    if (urlStr.length>0) {
        
        NSArray *MainArr = [NSArray array];
        
        MainArr = [urlStr componentsSeparatedByString:@"://"];
                
        if ([sourceApplication isEqualToString:KSCHEMEID]) {
            
            
            
            
        }
        
        
        
        
        
        
        NSArray *strArr = [[NSArray alloc]init];
        
        if ([urlStr rangeOfString:@"?"].location !=NSNotFound) {
            
            strArr = [urlStr componentsSeparatedByString:@"?"];
            
            if (strArr.count>0) {
                
                NSString *codestring = [strArr objectAtIndex:1];
                
                
                NSArray *arr = [[NSArray alloc]init];
                
                arr = [codestring componentsSeparatedByString:@"&"];
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                
                
                for (int i =0; i<arr.count; i++) {
                    
                    NSString *str = [NSString stringWithFormat:@"%@",[arr objectAtIndex:i]];
                    
                    NSArray * amarr = [[NSArray alloc]init];
                    
                    amarr = [str componentsSeparatedByString:@"="];
                    
                    [dic setObject:[amarr objectAtIndex:1] forKey:[amarr objectAtIndex:0]];
                    
                }
                
                //            [CustomAlertView showAlertView:@"您输入的参数是" andMessage:[NSString stringWithFormat:@"%@",dic]];
                
                [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"USERMAC"] forKey:@"USERMAC"];
                [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"INSTANCECODE"] forKey:@"INSTANCECODE"];
                [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"OPENID"] forKey:@"OPENID"];
                [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"MOBILE"] forKey:@"MOBILE"];
                
                
                
            }else
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"USERMAC"];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"INSTANCECODE"];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"OPENID"];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"MOBILE"];
                
            }
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            [[HT_UsercollectionService shareUserCollectionservice] postBaseInfoData:kSchemeStart];
        }
        
        
    }
    
    

}
@end
