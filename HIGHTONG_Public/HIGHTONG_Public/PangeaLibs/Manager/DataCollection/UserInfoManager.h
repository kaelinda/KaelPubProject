//
//  UserInfoManager.h
//  HIGHTONG
//
//  Created by Alaca on 15/5/11.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface UserInfoManager : NSObject


@property (nonatomic,strong) NSMutableArray *userActionArr;//用来存储用户行为


/**
 *  获取设备基本信息
 *
 *  @return 设备基本信息的JSON串
 */
- (NSString *)getDeviceinfo;

/**
 新用户采集基本信息设备信息

 @param isScheme 是否是scheme
 @param isFW     是否固件信息
 @param isSD     是否软件信息

 @return 设备基本JSON串
 */
- (NSString *)getDeviceinfoAndIsScheme:(BOOL)isScheme andIsFW:(BOOL)isFW andIsSD:(BOOL)isSD;




/**
 *  获取信息的唯一标识
 *
 *  @return 设备信息的唯一ID标识
 */
- (NSString *)getMsgID;
/**
 *  获取用户行为信息
 *
 *  @param isTVLiveData 是否上传直播信息
 *  @param isTimeMove   是否上传时移信息
 *  @param isBackData   是否上传回看信息
 *  @param isEpgData    是否上传EPG信息
 *  @param isVODData    是否上传点播信息
 *  @param isaNewData   是否上传新数据
 *
 *  @return 返回用户行为 字符串
 */
-(NSString *)getUserActionInfoWithTVLiveData:(BOOL)isTVLiveData andTimeMove:(BOOL)isTimeMove andBackData:(BOOL)isBackData andEPGDaga:(BOOL)isEpgData andVODData:(BOOL)isVODData andIsNewData:(BOOL)isaNewData;

/**
 *  @author Kael
 *
 *  @brief 获取异常信息
 *  @return 异常信息
 */
-(NSString *)getErrorInfo;

/**
 *  @author Kael
 *
 *  @brief 获取网络信息
 *  @return 网络信息
 */
-(NSString *)getNetInfo;




@end
