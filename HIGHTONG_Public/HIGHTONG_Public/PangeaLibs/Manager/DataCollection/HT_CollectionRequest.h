//
//  HT_CollectionRequest.h
//  Usercolection_DateBase
//
//  Created by Alaca on 15/5/11.
//  Copyright (c) 2015年 zzs. All rights reserved.
//

#import <Foundation/Foundation.h>

#define COM_CONFIG            @"config"
#define COM_DEVINFO_UPLOAD    @"uploadDevInfo"
#define COM_ERRORINFO_UPLOAD  @"uploadOpsInfo"
#define COM_USERACTION_UPLOAD @"uploadUserAction"
#define COM_NETINFO_UPLOAD    @"uploadNetInfo"



#define DEV_INFO_UPLOAD             @"DEV_INFO_UPLOAD"
#define USER_ACTION_UPLOAD          @"USER_ACTION_UPLOAD"
#define REGION_ID_COLLECTION        @"N"//不同区域做不同区分qhd
//#define DEVTYPE                     @"20"//设备编号 iphone---10  iPad --- 20
#define DEVTYPE                     [[[[[UIDevice currentDevice] model] componentsSeparatedByString:@" "] objectAtIndex:0] isEqual:@"iPad"]?@"20":@"10"   //设备编号 iphone---10  iPad --- 20
//#define DEVTYPE                     [[[[[UIDevice currentDevice] model] componentsSeparatedByString:@" "] objectAtIndex:0] isEqual:@"iPad"]?@"4":@"3"   //设备编号 iphone---10  iPad --- 20

//#define DEV_INFO_URL                @"http://qhddtv.com:8080/ICS/json"
#define DEV_INFO_URL                @"http://192.168.3.27:8080/ICS/json"
//#define DEV_INFO_URL                  @"http://zjsmdvbstb.com:8080/ICS/json"

#define PREFILEINFO                   0
#define BASEINFO                      1
#define USERACTIONINFO                2
#define NETINFO                       3
#define ERRORINFO                     4

#define KTimeout                      5

typedef NS_ENUM(NSUInteger, UC_GTTYPE) {
    kLaunchStart = 1,
    kSchemeStart = 2,
    kUserLoginStart = 3,
};

@protocol HT_CollectionResuqestDelegate <NSObject>

/**
 * @brief 网络请求回调协议
 * @param ret
 *      返回成功：ret = 0；
 *      超时：ret = 100（借用HTTP状态码表示）；
 *      其它ret值参照portal文档中的描述
 * @param result   返回数据
 * @param type     请求类型
 **/
- (void)didFinishedPost:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type;

@end


@interface HT_CollectionRequest : NSObject
{

    id<HT_CollectionResuqestDelegate>_delegate;
    

}

/**
 *  初始化对象
 *
 *  @param delegate 代理啊
 *
 *  @return 泛型 -- self
 */
- (id)initWithDelegate:(id<HT_CollectionResuqestDelegate>)delegate;
/**
 *  获取配置信息
 */
- (void)getPrefileInfo;
/**
 *  发送设备基本信息
 */
- (void)postTerminalInfo;



/**
 新版本的用户信息采集

 @param isScheme 是否是scheme启动
 @param isFW     是否上传硬件数据
 @param isSD     是否上传软数据
 */
-(void)postDeviceBaseInfoIsScheme:(BOOL)isScheme andFW:(BOOL)isFW andSD:(BOOL)isSD;

-(void)postDeviceBaseInfoWith:(UC_GTTYPE)gtType;

/**
 *  上传用户行为信息
 *
 *  @param isTVLiveData 是否上传直播信息
 *  @param isTimemove   是否上传时移信息
 *  @param isBackData   是否上传回看信息
 *  @param isEpgData    是否上传EPG信息
 *  @param isVODData    是否上传VOD信息
 */
-(void)postUserActionInfoWithTVLiveData:(BOOL)isTVLiveData andTimeMove:(BOOL)isTimemove andBackData:(BOOL)isBackData andEPGDaga:(BOOL)isEpgData andVODData:(BOOL)isVODData;
/**
 *  上传网络信息
 */
-(void)postNetInfo;
/**
 *  上传错误信息
 */
-(void)postErrorInfo;







@end
