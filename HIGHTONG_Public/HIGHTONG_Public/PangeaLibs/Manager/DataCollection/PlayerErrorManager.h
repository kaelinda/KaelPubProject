//
//  PlayerErrorManager.h
//  HIGHTONG_Public
//
//  Created by Kael on 2016/12/8.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GETBaseInfo.h"
#import "SAIInformationManager.h"

typedef NS_ENUM(NSUInteger, PlayerErrorType) {
    PlayerOutTimeError,/* 视频加载时间超出了预定时间 N s（秒） */
    PlayerLoadingExitError,/* 视频未加载完成用户就退出该界面 */
    PlayerAVError,/* 有视频链接 取不到视频流的状态 */
};

typedef NS_ENUM(NSUInteger, VideoSourceType) {
    VideoSource_InterNet, /* 互联网的资源 */
    VideoSource_CableNet, /* 广电网的资源 */
};

@interface PlayerErrorManager : NSObject


/**
 播放器是否处于 Loading 状态
 */
@property (nonatomic,assign) BOOL isAVLoading;


/**
 播放器是否已经正常播放起来过
 */
@property (nonatomic,assign) BOOL isNormalPlayed;




@property (nonatomic,strong) NSDate *startErrorDate;

@property (nonatomic,strong) NSDate *endErrorDate;

@property (nonatomic,strong) GETBaseInfo *baseInfo;


/**
 重置播放器状态信息
 */
-(void)resetPlayerStatusInfo;


/**
 存储播放器错误信息 （不加过滤）

 @param netTypeStr 网络类型
 @param wifiSSIDStr WiFi连接时的SSID
 @param sourceType 视频的网络来源，INTERNET：互联网；CABLENET：广电网
 @param avNameStr 视频名称
 @param errorType 异常编码（901：视频加载超时；902：离开播放画面；903：视频不存在）
 @param opsstStr 异常开始时间
 @param durationStr 异常持续时间
 */


-(void)OPSForPlayerErrorWithSourceType:(VideoSourceType)sourceType andAVName:(NSString *)avNameStr andOPSCode:(PlayerErrorType)errorType andOPSST:(NSString *)opsstStr;




/**
 存储播放器错误信息 （添加过滤）
 
 @param netTypeStr 网络类型
 @param wifiSSIDStr WiFi连接时的SSID
 @param sourceType 视频的网络来源，INTERNET：互联网；CABLENET：广电网
 @param avNameStr 视频名称
 @param errorType 异常编码（901：视频加载超时；902：离开播放画面；903：视频不存在）
 @param opsstStr 异常开始时间
 @param durationStr 异常持续时间
 @param isNormalPlayed 是否正常播放
 */
-(void)OPSForPlayerErrorWith:(NSString *)netTypeStr andWIFISSID:(NSString*)wifiSSIDStr andSourceType:(VideoSourceType)sourceType andAVName:(NSString *)avNameStr andOPSCode:(PlayerErrorType)errorType andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr andIsNormalPlayed:(BOOL)isNormalPlayed;


@end
