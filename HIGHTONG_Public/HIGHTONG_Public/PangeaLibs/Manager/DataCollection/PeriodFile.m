//
//  PeriodFile.m
//  HIGHTONG
//
//  Created by Alaca on 15/5/27.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "PeriodFile.h"

@implementation PeriodFile

-(void)writePlistWith:(NSDictionary *)dic{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path=[paths    objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:@"periodfile.plist"];   //获取路径
//    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"periodfile" ofType:@"plist"];
//    NSDictionary *plistDic = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    [dic writeToFile:filename atomically:YES];
    
    
    NSMutableDictionary *data1 = [[NSMutableDictionary alloc] initWithContentsOfFile:filename];
    NSLog(@"写入的配置文件 %@", data1);
}
-(NSInteger)getItmeWith:(NSString *)firstKey andSecondKey:(NSString *)secondKey{
    
//    NSDictionary* plistDic = [NSDictionary dictionaryWithContentsOfFile:@"periodfile.plist"];
//    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"periodfile" ofType:@"plist"];
//    NSMutableDictionary *plistDic = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path=[paths    objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:@"periodfile.plist"];   //获取路径
    NSMutableDictionary *plistDic = [[NSMutableDictionary alloc] initWithContentsOfFile:filename];

    NSDictionary *selectDci = [[NSDictionary alloc] initWithDictionary:[plistDic valueForKey:firstKey]];
    NSString *resultStr ;
    if ([firstKey isEqualToString:@"netInfo"]&&([secondKey isEqualToString:@"uploadCycle"]||[secondKey isEqualToString:@"collectCycle"])) {
        resultStr = [[selectDci valueForKey:@"eth"] valueForKey:secondKey];
    
    }else{
        resultStr = [selectDci valueForKey:secondKey];

    }
    return [resultStr integerValue];
}

-(NSInteger)getCollectionPeriodWithKey:(NSString *)key{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"periodfile" ofType:@"plist"];
    NSDictionary *plistDic = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSString *result;
    result = [[plistDic valueForKey:@"netInfo"] valueForKey:@"collectCycle"];
    return [result integerValue];
}

@end
