//
//  SchemeManager.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/10/21.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HT_UsercollectionService.h"

@interface SchemeManager : NSObject

+ (instancetype)shardSchemeManager;

-(void)schemeForSourceApplication:(NSString *)sourceApplication andSchemeUrl:(NSString *)schemeUrl;

-(void)schemeForSchemeUrl:(NSString *)schemeUrl;

//+(void)schemeForSchemeUrl:(NSString *)schemeUrl;



@end
