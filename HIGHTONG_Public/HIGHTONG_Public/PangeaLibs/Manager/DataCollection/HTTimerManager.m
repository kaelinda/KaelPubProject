//
//  HTTimerManager.m
//
//  Created by lailibo on 16/8/24.
//  Copyright © lailibo. All rights reserved.
//

#import "HTTimerManager.h"
#define kDefaultTimeFormat  @"HH:mm:ss"
#define kDefaultFireIntervalNormal  0.1
@interface HTTimerManager ()

@end

@implementation HTTimerManager



-(id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)start{
    
    [self setup];
    if(_timer == nil){

    _timer = [NSTimer scheduledTimerWithTimeInterval:kDefaultFireIntervalNormal target:self selector:@selector(updateLabel:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        
        if(startCountDate == nil){
            startCountDate = [NSDate date];
            
            startCountDate = [startCountDate dateByAddingTimeInterval:(timeUserValue<0)?0:-timeUserValue];
        }
        if(pausedTime != nil){
            NSTimeInterval countedTime = [pausedTime timeIntervalSinceDate:startCountDate];
            startCountDate = [[NSDate date] dateByAddingTimeInterval:-countedTime];
            pausedTime = nil;
        }
        _counting = YES;
        [_timer fire];
        
    }
}

-(void)updateLabel:(NSTimer*)timer{
    
    NSTimeInterval timeDiff = [[[NSDate alloc] init] timeIntervalSinceDate:startCountDate];
    NSDate *timeToShow = [NSDate date];
    NSString *strDate = [dateFormatter stringFromDate:timeToShow];
    _timeLabel.text = strDate;
        if (_counting) {
            timeToShow = [date1970 dateByAddingTimeInterval:timeDiff];
        }else{
            timeToShow = [date1970 dateByAddingTimeInterval:(!startCountDate)?0:timeDiff];
        }
        
        if([_delegate respondsToSelector:@selector(countingTo:timeToShowString:)]){
            [_delegate  countingTo:timeDiff timeToShowString:strDate];
        }
    }

-(void)setTimeFormat:(NSString *)timeFormat{
    
    _timeFormat = timeFormat;
    if ([_timeFormat length] != 0) {
        _timeFormat = timeFormat;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:_timeFormat];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    }
}



#pragma mark - Private method

-(void)setup{
    
    if ([_timeFormat length] == 0) {
        _timeFormat = kDefaultTimeFormat;
    }
    
    if(dateFormatter == nil){
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:_timeFormat];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    }
    
    if (date1970 == nil) {
        date1970 = [NSDate dateWithTimeIntervalSince1970:0];
    }
    
}



-(void)pause{
    [_timer invalidate];
    _timer = nil;
    _counting = NO;
    pausedTime = [NSDate date];
}


-(void)reset{
    
    pausedTime = nil;
    timeUserValue = 0;
    
    if(_counting){
        startCountDate = [NSDate date];
    }else{
        startCountDate = nil;
    }
    
}



@end
