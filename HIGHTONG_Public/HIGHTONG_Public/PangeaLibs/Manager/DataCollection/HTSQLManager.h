//
//  HTSQLManager.h
//  HIGHTONG
//
//  Created by apple on 15/6/10.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "PeriodFile.h"
#import "HT_CollectionRequest.h"

@interface HTSQLManager : NSObject
{
    FMDatabase *_db;
}
@property (nonatomic,strong)FMDatabase *db;
+ (id)shareInstance;
- (void)creatDataTable;


/**
 轮播图

 @param carouselStr 插入轮播图的数据
 */
-(void)insertCarousel:(NSString*)carouselStr;




/**
 幽默视频播放的信息

 @param pubHumourStr 插入幽默视频的数据
 */
-(void)insertPub_humourVideo:(NSString*)pubHumourStr;




/**
 幽默视频的交互信息

 @param pubHumourInteractiveStr 插入幽默视频的点赞交互信息数据库
 */
-(void)insertPub_humourInteractiveVideo:(NSString*)pubHumourInteractiveStr;




/**
 插入分享友盟的相关信息

 @param shareUmengStr 插入分享采集的相关信息
 */
-(void)insertShareUmengVideo:(NSString*)shareUmengStr;






//  增加 直播 表数据

/**
 *  插入直播
 *
 *  @param LiveStr 直播的字符串
 */
-(void)insertLive:(NSString *)LiveStr;
//  增加 时移数据 表数据
/**
 *  时移的信息
 *
 *  @param TimeMoveStr 返回时移字符串
 */
-(void)insertTimeMove:(NSString *)TimeMoveStr;
//  增加 EPG 表数据

/**
 *  增加EPG信息
 *
 *  @param EPGStr EPG字符串
 */
-(void)insertEPG:(NSString *)EPGStr;

//  增加 回看 表数据

/**
 *  增加回看数据
 *
 *  @param LookingbackStr 回看的字符串
 */

-(void)insertLookingback:(NSString *)LookingbackStr;

//  增加 点播 表数据
/**
 *  增加点播数据
 *
 *  @param VODStr 点播字符串
 */
-(void)insertVOD:(NSString *)VODStr;

/*
 增加 医视频的的表数据
 
 @param polyStr 医视频的字符春
 
 
 */

-(void)insertPoly:(NSString*)PolyStr;



/**
 
 增加医疗健康数据
 
 */

-(void)insertJKTVMedicineHealth:(NSString*)MedicineHealthStr;


//  增加EPGSearch

/**
 *  EPG搜索
 *
 *  @param EPGSearchStr EPG搜索字符串
 */

-(void)insertEPGSearch:(NSString *)EPGSearchStr;


//  增加VODSearch
/**
 *  插入点播搜索
 *
 *  @param VODSearchStr 点播搜索的字符串
 */
-(void)insertVODSearch:(NSString *)VODSearchStr;


//增加用户行为的网络信息

/**
 *  插入的网络信息
 *
 *  @param IPStr   当前网络IP地址（String）
 *  @param AWStr   网络接入方式，有线/无线（String）
 *  @param TTLStr  网络时延（String）
 *  @param BWStr   当网络带宽（String）
 *  @param TimeStr 当前采集的时间
 */
-(void)insertETHForIP:(NSString*)IPStr andAW:(NSString*)AWStr andTTL:(NSString*)TTLStr andBW:(NSString*)BWStr andTime:(NSString*)TimeStr;


//增加错误行为信息

/**
 *  采集的错误信息
 *
 *  @param AVTYPEStr   视频类型（String）
 *  @param OPTYPEStr   异常类型（String）AV：视频类
 *  @param OPSSTStr    异常开始时间（String）格式：YYYY-MM-DD HH:mm:SS
 *  @param DurationStr 异常持续时间（String）格式：mm:SS
 */
-(void)insertOPSForAVTYPE:(NSString*)AVTYPEStr andOPTYPE:(NSString*)OPTYPEStr andOPSST:(NSString*)OPSSTStr andDuration:(NSString*)DurationStr;


/**
 采集的错误信息
 
 @param netTypeStr 网络类型
 @param wifiSSIDStr WiFi连接时的SSID
 @param sourceTypeStr 视频的网络来源，INTERNET：互联网；CABLENET：广电网
 @param avNameStr 视频名称
 @param opsCodeStr 异常编码（901：视频加载超时；902：离开播放画面；903：视频不存在）
 @param opsstStr 异常开始时间
 @param durationStr 异常持续时间
 */

-(void)insertOPSForNetType:(NSString *)netTypeStr andWIFISSID:(NSString*)wifiSSIDStr andSourceType:(NSString *)sourceTypeStr andAVName:(NSString *)avNameStr andOPSCode:(NSString *)opsCodeStr andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr;




/**
 *  插入上线时间
 *
 *  @return nil
 */

//  插入上线时间
-(void)insertUserOnTime:(NSString *)Time;



/**
 *  插入上线时间
 *
 *  @return nil
 */
//  插入下线时间
-(void)insertUserOffTime:(NSString *)Time;


/**
 *  更新下线时间
 *
 *  @return nil
 */
//  更新下线时间
-(void)UpdateUserOffTime:(NSString *)Time;




/**
 *  插入上线下线时间
 *
 *  @param Online  上线
 *  @param Offline 下线
 */
-(void)insertUserOnTime:(NSString*)Online  AndOffLine:(NSString*)Offline;


/**
 *  //获取个人用户信息行为的数组
 *
 *  @return 获取数据库的格式数组//  获取最新行为信息
 */
-(NSMutableArray *)getNewAllPeople;


/**
 *  // 获取最新的网络行为信息
 *
 *  @return 获取数据库的格式数组
 */
-(NSMutableArray *)getETHInformations;


/**
 *  // 获取最新的错误行为信息
 *
 *  @return 获取错误的信息
 */


-(NSMutableArray *)getOPSInformations;

-(void)deleteUserAction;//  删除直播  回看  EPG   方法
-(void)deleteUserOPSInformation;//删除OPS信息
-(void)deleteUserETHInformation;//删除ETH信息

@end
