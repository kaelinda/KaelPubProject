//
//  SAIInformationManager.m
//  HIGHTONG
//
//  Created by apple on 14/12/18.
//  Copyright (c) 2014年 ZhangDongfeng. All rights reserved.
//

#import "SAIInformationManager.h"
#import "HTSQLManager.h"
@implementation SAIInformationManager
/**
 *  采集获取的服务器的时间
 *
 *
 */


+(NSString *)serviceTimeTransformStr
{

    NSDate *serviceTime = [SystermTimeControl GetSystemTimeWith];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *TimeStr = [dateFormatter stringFromDate:serviceTime];
    return TimeStr;
}



+(void)getONLineTime:(NSString*)ONLine  AndOFFLineTime:(NSString *)OFFLine
{
    
    
    [[HTSQLManager shareInstance]insertUserOnTime:ONLine AndOffLine:OFFLine];
    
    
}




/**
 *  采集获取的上限时间
 */

+(void)ONLine
{
    NSLog(@"上线时间%@",[self serviceTimeTransformStr]);
    
    [[HTSQLManager shareInstance]insertUserOnTime:[self serviceTimeTransformStr]];
   
    [[NSUserDefaults standardUserDefaults]setObject:[self serviceTimeTransformStr] forKey:@"ONLINE"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}


/**
 *  采集获取下线的时间
 */

+(void)OFFLine
{
    //[[HTSQLManager shareInstance]insertUserOffTime:[self serviceTimeTransformStr]];
    
    //从default获取上线的时间
    
    NSString *onLineTimeStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"ONLINE"];
    
    NSString *offTime = [NSString stringWithFormat:@"%@+%@",onLineTimeStr,[self serviceTimeTransformStr]];
    
    
    
    [[HTSQLManager shareInstance]UpdateUserOffTime:offTime];
    
    
}


/**
 *  采集的直播行为信息
 *
 *  @param mserviceID     直播的serviceID
 *  @param mchanelName    直播的频道名称
 *  @param meventName     直播的eventName 节目名称
 *  @param mwatchTime     直播的观看时刻
 *  @param mwatchDuration 直播的观看时长
 *  @param meventLength   直播的Event时长
 *  @param meventTime     直播的Event开始时间
 */

+(void)information:(NSString*)mserviceID andChanel:(NSString*)mchanelName andEventName:(NSString *)meventName andwatchTime:(NSString*)mwatchTime andWatchDuration:(NSString*)mwatchDuration andEventLength:(NSString*)meventLength andEventTime:(NSString*)meventTime
{
    

    if (mserviceID == nil) {
        mserviceID = @"";
    }
    if (mchanelName == nil) {
        mchanelName = @"";
    }
    if (meventName == nil) {
        meventName = @"";
    }
    if (mwatchTime == nil) {
        mwatchTime = @"";
    }
    if (mwatchDuration == nil) {
        mwatchDuration = @"";
    }
    if (meventLength == nil) {
        meventLength = @"";
    }
    if (meventLength == nil) {
        meventLength = @"";
    }
    
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@",mserviceID,mchanelName,meventName,mwatchTime,mwatchDuration,meventLength,meventTime];
    NSLog(@"采集直播信息%@+%@+%@+%@+%@+%@+%@",mserviceID,mchanelName,meventName,mwatchTime,mwatchDuration,meventLength,meventTime);
    [[HTSQLManager shareInstance] insertLive:str];
    
}


+(void)epgLiveInformation:(EPGCollectionModel*)liveEpgCollectionModel
{
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveServiceID)) {
        liveEpgCollectionModel.liveServiceID = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveViedoName)) {
        liveEpgCollectionModel.liveViedoName = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveWatchTime)) {
        liveEpgCollectionModel.liveWatchTime = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveEventLength)) {
        liveEpgCollectionModel.liveEventLength = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveServiceName)) {
        liveEpgCollectionModel.liveServiceName = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveWatchDuration)) {
        liveEpgCollectionModel.liveWatchDuration = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveTRACKID)) {
        liveEpgCollectionModel.liveTRACKID = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveTRACKNAME)) {
        liveEpgCollectionModel.liveTRACKNAME = @"";
    }
    if (isEmptyStringOrNilOrNull(liveEpgCollectionModel.liveEN)) {
        liveEpgCollectionModel.liveEN = @"";
    }
    
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",liveEpgCollectionModel.liveServiceID,liveEpgCollectionModel.liveServiceName,liveEpgCollectionModel.liveViedoName,liveEpgCollectionModel.liveWatchTime,[SAIInformationManager changeWathchTimeDurationForWatchDuration:liveEpgCollectionModel.liveWatchDuration andEventDuration:liveEpgCollectionModel.liveEventLength],liveEpgCollectionModel.liveEventLength,liveEpgCollectionModel.liveLiveTime,liveEpgCollectionModel.liveTRACKID,liveEpgCollectionModel.liveTRACKNAME,liveEpgCollectionModel.liveEN];
    NSLog(@"采集直播信息%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",liveEpgCollectionModel.liveServiceID,liveEpgCollectionModel.liveServiceName,liveEpgCollectionModel.liveViedoName,liveEpgCollectionModel.liveWatchTime,[SAIInformationManager changeWathchTimeDurationForWatchDuration:liveEpgCollectionModel.liveWatchDuration andEventDuration:liveEpgCollectionModel.liveEventLength],liveEpgCollectionModel.liveEventLength,liveEpgCollectionModel.liveLiveTime,liveEpgCollectionModel.liveTRACKID,liveEpgCollectionModel.liveTRACKNAME,liveEpgCollectionModel.liveEN);
    [[HTSQLManager shareInstance]insertLive:str];
}






/**
 *  采集的时移的信息
 *
 *  @param mserviceID     时移的serviceID
 *  @param mchanelName    时移的频道名称
 *  @param meventName     时移的eventName 节目名称
 *  @param mwatchTime     时移的观看时刻
 *  @param mwatchDuration 时移的观看时长
 *  @param meventLength   时移的Event时长
 *  @param meventTime     时移的Event开始时间
 */



+(void)timeMoveinformationServiceID:(NSString*)mserviceID andChanel:(NSString*)mchanelName andEventName:(NSString *)meventName andwatchTime:(NSString*)mwatchTime andWatchDuration:(NSString*)mwatchDuration andEventLength:(NSString*)meventLength andEventTime:(NSString*)meventTime
{
    if (mserviceID == nil) {
        mserviceID = @"";
    }
    if (mchanelName == nil) {
        mchanelName = @"";
    }
    if (meventName == nil) {
        meventName = @"";
    }
    if (mwatchTime == nil) {
        mwatchTime = @"";
    }
    if (mwatchDuration == nil) {
        mwatchDuration = @"";
    }
    if (meventLength == nil) {
        meventLength = @"";
    }
    if (meventLength == nil) {
        meventLength = @"";
    }


    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@",mserviceID,mchanelName,meventName,mwatchTime,mwatchDuration,meventLength,meventTime];
    
    
    NSLog(@"采集的时移信息%@+%@+%@+%@+%@+%@+%@",mserviceID,mchanelName,meventName,mwatchTime,mwatchDuration,meventLength,meventTime);
    
    
    [[HTSQLManager shareInstance]insertTimeMove:str];
    
}
+(void)epgTimeMoveInformation:(EPGCollectionModel*)timeMoveEpgCollectionModel
{
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveServiceID)) {
        timeMoveEpgCollectionModel.timeMoveServiceID = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveViedoName)) {
        timeMoveEpgCollectionModel.timeMoveViedoName = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveWatchTime)) {
        timeMoveEpgCollectionModel.timeMoveWatchTime = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveEventLength)) {
        timeMoveEpgCollectionModel.timeMoveEventLength = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveServiceName)) {
        timeMoveEpgCollectionModel.timeMoveServiceName = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveWatchDuration)) {
        timeMoveEpgCollectionModel.timeMoveWatchDuration = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveTRACKID)) {
        timeMoveEpgCollectionModel.timeMoveTRACKID = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveTRACKNAME)) {
        timeMoveEpgCollectionModel.timeMoveTRACKNAME = @"";
    }if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveEN)) {
        timeMoveEpgCollectionModel.timeMoveEN = @"";
    }
    if (isEmptyStringOrNilOrNull(timeMoveEpgCollectionModel.timeMoveTWT)) {
        timeMoveEpgCollectionModel.timeMoveTWT = @"";
    }
    
    NSCalendar * Mcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [Mcalendar setTimeZone: timeZone];
    NSDateComponents * dayComponentA = [Mcalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond | kCFCalendarUnitWeek |kCFCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:[NSDate dateWithTimeInterval:0 sinceDate:[SystermTimeControl GetSystemTimeWith]]];
   NSInteger years = [dayComponentA year];
    NSInteger months = [dayComponentA month];
    NSInteger days = [dayComponentA day];
   
    NSString *dateStr = [NSString stringWithFormat:@"%ld-%ld-%ld",years,months,days];
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@+%@+%@+%@%@",timeMoveEpgCollectionModel.timeMoveServiceID,timeMoveEpgCollectionModel.timeMoveServiceName,timeMoveEpgCollectionModel.timeMoveViedoName,timeMoveEpgCollectionModel.timeMoveWatchTime,timeMoveEpgCollectionModel.timeMoveWatchDuration,timeMoveEpgCollectionModel.timeMoveEventLength,timeMoveEpgCollectionModel.timeMoveTimeMoveTime,timeMoveEpgCollectionModel.timeMoveTRACKID,timeMoveEpgCollectionModel.timeMoveTRACKNAME,timeMoveEpgCollectionModel.timeMoveEN,[NSString stringWithFormat:@"%@ %@",dateStr,timeMoveEpgCollectionModel.timeMoveTWT]];
    
    
    NSLog(@"采集的时移信息%@+%@+%@+%@+%@+%@+%@+%@+%@+%@,%@",timeMoveEpgCollectionModel.timeMoveServiceID,timeMoveEpgCollectionModel.timeMoveServiceName,timeMoveEpgCollectionModel.timeMoveViedoName,timeMoveEpgCollectionModel.timeMoveWatchTime,timeMoveEpgCollectionModel.timeMoveWatchDuration,timeMoveEpgCollectionModel.timeMoveEventLength,timeMoveEpgCollectionModel.timeMoveTimeMoveTime,timeMoveEpgCollectionModel.timeMoveTRACKID,timeMoveEpgCollectionModel.timeMoveTRACKNAME,timeMoveEpgCollectionModel.timeMoveEN,[NSString stringWithFormat:@"%@ %@",dateStr,timeMoveEpgCollectionModel.timeMoveTWT]);
    
    
    [[HTSQLManager shareInstance]insertTimeMove:str];

}







/**
 *  采集的回看行为信息
 *
 *  @param beventName     回看的eventName
 *  @param bEventID       回看的bEventID
 *  @param bserivceID     回看的bserivceID 频道ID
 *  @param bserviceName   回看的bserviceName 频道名称
 *  @param beventLength   回看的Event时长
 *  @param beventTime     回看的Event开始时间
 *  @param bwatchDuration 回看的观看时长
 *  @param bwatchTime     回看的观看时刻
 */

+(void)BackinformationForeventName:(NSString*)beventName andEventID:(NSString*)bEventID andSerivceID:(NSString *)bserivceID andserviceName:(NSString*)bserviceName  andEventLength:(NSString*)beventLength andEventTime:(NSString*)beventTime andWatchDuration:(NSString*)bwatchDuration andWatchTime:(NSString*)bwatchTime
{
    if (beventName == nil) {
        beventName = @"";
    }
    if (bEventID == nil) {
        bEventID = @"";
    }
    if (bserivceID == nil) {
        bserivceID = @"";
    }
    if (bserviceName == nil) {
        bserviceName = @"";
    }
    if (beventLength == nil) {
        beventLength = @"";
    }
    if (beventTime == nil) {
        beventTime = @"";
    }
    if (bwatchDuration == nil) {
        bwatchDuration = @"";
    }
    if (bwatchTime == nil) {
        bwatchTime = @"";
    }
    
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@+%@",beventName,bEventID,bserivceID,bserviceName,beventLength,beventTime,bwatchDuration,bwatchTime];
    NSLog(@"采集回看信息%@+%@+%@+%@+%@+%@+%@+%@",beventName,bEventID,bserivceID,bserviceName,beventLength,beventTime,bwatchDuration,bwatchTime);
    [[HTSQLManager shareInstance]insertLookingback:str];
}

+(void)epgBackEventInformation:(EPGCollectionModel *)backEventEpgCollectionModel
{
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventEventID)) {
        backEventEpgCollectionModel.eventEventID = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventServiceID)) {
        backEventEpgCollectionModel.eventServiceID = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventWatchTime)) {
        backEventEpgCollectionModel.eventWatchTime = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventWatchDuration)) {
        backEventEpgCollectionModel.eventWatchDuration = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventEventTime)) {
        backEventEpgCollectionModel.eventEventTime = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventEventLength)) {
        backEventEpgCollectionModel.eventEventLength = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventVideoName)) {
        backEventEpgCollectionModel.eventVideoName = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventTRACKID)) {
        backEventEpgCollectionModel.eventTRACKID = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventTRACKNAME)) {
        backEventEpgCollectionModel.eventTRACKNAME = @"";
    }
    if (isEmptyStringOrNilOrNull(backEventEpgCollectionModel.eventEN)) {
        backEventEpgCollectionModel.eventEN = @"";
    }
    
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",backEventEpgCollectionModel.eventVideoName,backEventEpgCollectionModel.eventEventID,backEventEpgCollectionModel.eventServiceID,backEventEpgCollectionModel.eventServiceName,backEventEpgCollectionModel.eventEventLength,backEventEpgCollectionModel.eventEventTime,[SAIInformationManager changeWathchTimeDurationForWatchDuration:backEventEpgCollectionModel.eventWatchDuration andEventDuration:backEventEpgCollectionModel.eventWatchDuration],backEventEpgCollectionModel.eventWatchTime,backEventEpgCollectionModel.eventTRACKID,backEventEpgCollectionModel.eventTRACKNAME,backEventEpgCollectionModel.eventEN];
    NSLog(@"采集回看信息%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",backEventEpgCollectionModel.eventVideoName,backEventEpgCollectionModel.eventEventID,backEventEpgCollectionModel.eventServiceID,backEventEpgCollectionModel.eventServiceName,backEventEpgCollectionModel.eventEventLength,backEventEpgCollectionModel.eventEventTime,[SAIInformationManager changeWathchTimeDurationForWatchDuration:backEventEpgCollectionModel.eventWatchDuration andEventDuration:backEventEpgCollectionModel.eventWatchDuration],backEventEpgCollectionModel.eventWatchTime,backEventEpgCollectionModel.eventTRACKID,backEventEpgCollectionModel.eventTRACKNAME,backEventEpgCollectionModel.eventEN);
    [[HTSQLManager shareInstance]insertLookingback:str];

}


+(NSString *)changeWathchTimeDurationForWatchDuration:(NSString*)watchDuration andEventDuration:(NSString*)eventDuration
{
    
    if (watchDuration == nil) {
        
        watchDuration = @"";
    }
    
    if (eventDuration == nil) {
        
        eventDuration = @"";
    }
    
    if ([watchDuration integerValue]>[eventDuration integerValue]) {
        
        watchDuration = eventDuration;
    }
    

    return watchDuration;
}



/**
 *  EPG的采集信息
 *
 *  @param chanelFenlei   EPG的频道分类
 *  @param chanelName     EPG的频道名称
 *  @param chanelInterval 浏览EPG信息覆盖的时间区间
 *  @param skimDuration   浏览时长
 *  @param skimTime       浏览时刻
 */

+(void)EPGinformationForChanelFenlei:(NSString*)chanelFenlei andChanelName:(NSString*)chanelName andChanelInterval:(NSString*)chanelInterval andSkimDuration:(NSString*)skimDuration andSkimTime:(NSString*)skimTime
{
    if (chanelFenlei == nil) {
        chanelFenlei = @"";
    }
    if (chanelName == nil) {
        chanelName = @"";
    }
    if (chanelInterval == nil) {
        chanelInterval = @"";
    }
    if (skimDuration == nil || [skimDuration intValue]<0) {
        skimDuration = @"";
    }
    if (skimTime == nil) {
        skimTime = @"";
    }
    
    
    
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@",chanelFenlei,chanelName,chanelInterval,skimDuration,skimTime];
    
    
    NSLog(@"采集的EPG的信息%@+%@+%@+%@+%@",chanelFenlei,chanelName,chanelInterval,skimDuration,skimTime);
    
    
    [[HTSQLManager shareInstance]insertEPG:str];
    
}


/**
 *  VOD的采集信息
 *  @param vodcontentID点播的节目contentID
 *  @param vodFenlei   点播的节目类别/分类
 *  @param vodName     点播节目的名称
 *  @param vodTime     点播节目的时刻
 *  @param vodDuration 点播节目的市场
 */


+(void)VODinformationForVodContentID:(NSString *)vodContentID andVodFenlei:(NSString*)vodFenlei andVodName:(NSString*)vodName andVodTime:(NSString*)vodTime andVodDuration:(NSString*)vodDuration
{
    if (vodContentID == nil) {
        vodContentID = @"";
    }
    
    if (vodFenlei == nil) {
        vodFenlei = @"";
    }
    if (vodName == nil) {
        vodName = @"";
    }
    if (vodTime == nil) {
        vodTime = @"";
    }
    if (vodDuration == nil) {
        vodDuration = @"";
    }
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@",vodContentID,vodFenlei,vodName,vodTime,vodDuration];
    NSLog(@"采集的点播的东西%@+%@+%@+%@",vodFenlei,vodName,vodTime,vodDuration);
    
    
    [[HTSQLManager shareInstance]insertVOD:str];
    
}



+(void)VodInformation:(DemandModel*)demandModel
{
    if (isEmptyStringOrNilOrNull(demandModel.contentID)) {
        demandModel.contentID = @"";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.vodFenleiType)) {
        demandModel.vodFenleiType = @"";
    }

    if (isEmptyStringOrNilOrNull(demandModel.name)) {
        demandModel.name = @"";
    }

    if (isEmptyStringOrNilOrNull(demandModel.vodWatchTime)) {
        demandModel.vodWatchTime = @"";
    }

    if (isEmptyStringOrNilOrNull(demandModel.vodWatchDuration)) {
        demandModel.vodWatchDuration = @"";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.vodBID)) {
        demandModel.vodBID = @"";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.VodBName)) {
        demandModel.VodBName = @"";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.category)) {
        demandModel.category = @"";
    }
    if (isEmptyStringOrNilOrNull(demandModel.rootCategory)) {
        demandModel.rootCategory = @"";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.TRACKID_VOD)) {
        demandModel.TRACKID_VOD = @"_";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.TRACKNAME_VOD)) {
        demandModel.TRACKNAME_VOD = @"_";
    }
    
    if (isEmptyStringOrNilOrNull(demandModel.EN_VOD)) {
        demandModel.EN_VOD = @"_";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",demandModel.contentID,demandModel.vodFenleiType,demandModel.name,demandModel.vodWatchTime,demandModel.vodWatchDuration,demandModel.vodBID,demandModel.VodBName,demandModel.category,demandModel.rootCategory,demandModel.TRACKID_VOD,demandModel.TRACKNAME_VOD,demandModel.EN_VOD];
    
    
    NSLog(@"采集的点播的东西%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@+%@",demandModel.contentID,demandModel.vodFenleiType,demandModel.name,demandModel.vodWatchTime,demandModel.vodWatchDuration,demandModel.vodBID,demandModel.VodBName,demandModel.category,demandModel.rootCategory,demandModel.TRACKID_VOD,demandModel.TRACKNAME_VOD,demandModel.EN_VOD);
    
    
    [[HTSQLManager shareInstance] insertVOD:str];

    
    
}



+(void)polyMedicineInfomation:(HealthVodCollectionModel*)healthModel
{
    if (isEmptyStringOrNilOrNull(healthModel.TrackId)) {
        healthModel.TrackId = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.ENtype)) {
        healthModel.ENtype = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.watchDuration)) {
        healthModel.watchDuration = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.watchTime)) {
        healthModel.watchTime = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.itemId)) {
        healthModel.itemId = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.name)) {
        healthModel.name = @"";
    }
    if (isEmptyStringOrNilOrNull(healthModel.TrackName)) {
        healthModel.TrackName = @"";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@",healthModel.itemId,healthModel.TrackId,healthModel.TrackName,healthModel.ENtype,healthModel.name,healthModel.watchTime,healthModel.watchDuration];
    NSLog(@"我采集到的医视频的信息是%@",str);
    [[HTSQLManager shareInstance] insertPoly:str];
}



+(void)JKTVHomeInformation:(JKTVHomeCollectionModel*)jkTVModel
{
    if (isEmptyStringOrNilOrNull(jkTVModel.moduleCode)) {
        jkTVModel.moduleCode = @"";
    }
    if (isEmptyStringOrNilOrNull(jkTVModel.moduleName)) {
        jkTVModel.moduleName = @"";
    }
    if (isEmptyStringOrNilOrNull(jkTVModel.systemTime)) {
        jkTVModel.systemTime = @"";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@",jkTVModel.moduleCode,jkTVModel.moduleName,jkTVModel.systemTime];
    [[HTSQLManager shareInstance]insertJKTVMedicineHealth:str];
}


+(void)carouselInfomation:(CarouselPageModel*)carouselModel
{
    if (isEmptyStringOrNilOrNull(carouselModel.carouselSEQ)) {
        
        carouselModel.carouselSEQ = @"";
    }
    if (isEmptyStringOrNilOrNull(carouselModel.carouselID)) {
        carouselModel.carouselID = @"";
    }
    if (isEmptyStringOrNilOrNull(carouselModel.carouselName)) {
        carouselModel.carouselName = @"";
    }
    if (isEmptyStringOrNilOrNull(carouselModel.carouselType)) {
        carouselModel.carouselType = @"";
    }
    if (isEmptyStringOrNilOrNull(carouselModel.carouselBT)) {
        carouselModel.carouselBT = @"";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@",carouselModel.carouselSEQ,carouselModel.carouselID,carouselModel.carouselName,carouselModel.carouselType,carouselModel.carouselBT];
    NSLog(@"我采集到的轮播图的信息是%@",str);

    [[HTSQLManager shareInstance]insertCarousel:str];
}




+(void)pub_humourInformation:(PubVodPlayCollectionModel*)pubCollectionModel
{
    
    if (isEmptyStringOrNilOrNull(pubCollectionModel.itemId)) {
        
        pubCollectionModel.itemId = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.name)) {
        pubCollectionModel.name = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.ENtype)) {
        pubCollectionModel.ENtype = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.TrackId)) {
        pubCollectionModel.TrackId = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.TrackName)) {
        pubCollectionModel.TrackName = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.watchTime)) {
        pubCollectionModel.watchTime = @"";
    }
    if (isEmptyStringOrNilOrNull(pubCollectionModel.watchDuration)) {
        pubCollectionModel.watchDuration = @"";
    }
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@+%@+%@",pubCollectionModel.itemId,pubCollectionModel.name,pubCollectionModel.ENtype,pubCollectionModel.TrackId,pubCollectionModel.TrackName,pubCollectionModel.watchTime,pubCollectionModel.watchDuration];
    NSLog(@"我采集到的幽默视频的信息是%@",str);
    
    [[HTSQLManager shareInstance]insertPub_humourVideo:str];

 
}


+(void)pub_humourInteractiveInformation:(PubVodInteractiveCollectionModel*)pubInteractiveModel
{
    if (isEmptyStringOrNilOrNull(pubInteractiveModel.programId)) {
        
        pubInteractiveModel.programId = @"";
    }
    if (isEmptyStringOrNilOrNull(pubInteractiveModel.programName)) {
        pubInteractiveModel.programName = @"";
    }
    if (isEmptyStringOrNilOrNull(pubInteractiveModel.systemTime)) {
        pubInteractiveModel.systemTime = @"";
    }
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@",pubInteractiveModel.programId,pubInteractiveModel.programName,pubInteractiveModel.systemTime];
    NSLog(@"我采集到的幽默交互的信息是%@",str);
    
   
    [[HTSQLManager shareInstance]insertPub_humourInteractiveVideo:str];
    
    
}



+(void)shareUMengInformation:(ShareCollectionModel*)shareCollectionModel
{
    if (isEmptyStringOrNilOrNull(shareCollectionModel.shareID)) {
        
        shareCollectionModel.shareID = @"";
    }
    if (isEmptyStringOrNilOrNull(shareCollectionModel.shareTime)) {
        shareCollectionModel.shareTime = @"";
    }
    if (isEmptyStringOrNilOrNull(shareCollectionModel.shartType)) {
        shareCollectionModel.shartType = @"";
    }
    if (isEmptyStringOrNilOrNull(shareCollectionModel.shareTitle)) {
        shareCollectionModel.shareTitle = @"";
    }
    if (isEmptyStringOrNilOrNull(shareCollectionModel.shartModel)) {
        shareCollectionModel.shartModel = @"";
    }
    
    
    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@+%@",shareCollectionModel.shareID,shareCollectionModel.shareTitle,shareCollectionModel.shareTime,shareCollectionModel.shartType,shareCollectionModel.shartModel];
    NSLog(@"我采集到的分享友盟信息是%@",str);
    
    
    [[HTSQLManager shareInstance]insertShareUmengVideo:str];
    
}





//+(void)VODinformationForVodFenlei:(NSString*)vodFenlei andVodName:(NSString*)vodName andVodTime:(NSString*)vodTime andVodDuration:(NSString*)vodDuration
//{
//    
//    if (vodFenlei == nil) {
//        vodFenlei = @"";
//    }
//    if (vodName == nil) {
//        vodName = @"";
//    }
//    if (vodTime == nil) {
//        vodTime = @"";
//    }
//    if (vodDuration == nil) {
//        vodDuration = @"";
//    }
//   
//    
//    
//    NSString *str = [NSString stringWithFormat:@"%@+%@+%@+%@",vodFenlei,vodName,vodTime,vodDuration];
//    
//    
//    NSLog(@"采集的点播的东西%@+%@+%@+%@",vodFenlei,vodName,vodTime,vodDuration);
//    
//    
//    [[HTSQLManager shareInstance]insertVOD:str];
//    
//    
//}

/**
 *  EPG搜索采集的信息
 *
 *  @param searchHotKeyandTime 搜索的关键字和搜索的时刻
 */
+(void)EPGSearchHotKeyandSearchTime:(NSString *)searchHotKeyandTime
{
    if (searchHotKeyandTime == nil) {
        searchHotKeyandTime = @"";
    }
    
    [[HTSQLManager shareInstance]insertEPGSearch:searchHotKeyandTime];
    
 
}

/**
 *  Vod搜索的采集的信息
 *
 *  @param searchHotKeyandTime 搜索的关键字和搜索的时刻
 */

+(void)VODSearchHotKeyandSearchTime:(NSString *)searchHotKeyandTime
{
    if (searchHotKeyandTime == nil) {
        searchHotKeyandTime = @"";
    }
    NSLog(@"采集的搜索字段%@",searchHotKeyandTime);
    [[HTSQLManager shareInstance]insertVODSearch:searchHotKeyandTime];
    
}


+(void)ETHForIP:(NSString*)IPStr andAW:(NSString*)AWStr andTTL:(NSString*)TTLStr andBW:(NSString*)BWStr
{
    
    
    [[HTSQLManager shareInstance]insertETHForIP:IPStr andAW:AWStr andTTL:TTLStr andBW:BWStr andTime:[self serviceTimeTransformStr]];
}



+(void)OPSForAVTYPE:(NSString*)AVTYPEStr andOPTYPE:(NSString*)OPTYPEStr andOPSST:(NSString*)OPSSTStr andDuration:(NSString*)DurationStr
{
    [[HTSQLManager shareInstance]insertOPSForAVTYPE:AVTYPEStr andOPTYPE:OPSSTStr andOPSST:OPSSTStr andDuration:DurationStr];
}



+(void)OPSForNetType:(NSString *)netTypeStr andWIFISSID:(NSString*)wifiSSIDStr andSourceType:(NSString *)sourceTypeStr andAVName:(NSString *)avNameStr andOPSCode:(NSString *)opsCodeStr andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr
{
    [[HTSQLManager shareInstance]insertOPSForNetType:netTypeStr andWIFISSID:wifiSSIDStr andSourceType:sourceTypeStr andAVName:avNameStr andOPSCode:opsCodeStr andOPSST:opsstStr andDuration:durationStr];
}


+(void)deleteUserInformation
{
    
    
    [[HTSQLManager shareInstance] deleteUserAction];
    
}

+(void)deleteUserOPS{
    
    
    [[HTSQLManager shareInstance] deleteUserOPSInformation];
    

}
+(void)deleteUserETH{
    [[HTSQLManager shareInstance]deleteUserETHInformation];
}


@end
