//
//  GETBaseInfo.m
//  zzsRequest
//
//  Created by Alaca on 14/12/18.
//  Copyright (c) 2014年 zzs. All rights reserved.
//

#import "GETBaseInfo.h"
#import "sys/utsname.h"
#import "IPAddress.h"
#import <CoreLocation/CoreLocation.h>
#import "LBIdentifierManager.h"
#import "LBKeyChain.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "Reachability.h"

#import <Foundation/NSProcessInfo.h>//ram 总量
#import <sys/mount.h>
#import <UIKit/UIKit.h>

#import <sys/utsname.h>

#define ip @"www.baidu.com"
enum{
    ChinaMobile,
    ChinaUnicom,
    ChinaTelecom,
    ChinaTietong
};


@interface GETBaseInfo ()<CLLocationManagerDelegate>

@property (nonatomic,copy) NSString *deviceIP;

@end
NSString * _currentLatitude;
NSString * _currentLongitude;
CLLocationManager *_locationManager;
CLLocation *_checkinLocation;

@implementation GETBaseInfo
{
    //实现获取当前地理位置：第一步：
    //这两个变量，locationManaager用于获取位置，checkinLocation用于保存获取到的位置信息
//    NSString * _currentLatitude;
//    NSString * _currentLongitude;
//    CLLocationManager *_locationManager;
//    CLLocation *_checkinLocation;
//    
//    NSString *_locStr;//已拼接的经纬度
}
#pragma mark 获取  前后摄像头像素

NSString *_locStr;//已拼接的经纬度
-(NSString *)getResolutionOfFront:(BOOL)isFront andBehind:(BOOL)isBehind{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *machine =[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];

    /*
     设备名：后置摄像头：前置摄像头
     iPhone 4:500W:30W
     iPhone 4S：800W：30W
     iPhone 5：800W：120W
     iPhone 5C：800W：120W
     iPhone 5S：800W：120W
     iPhone 6：800W：120W
     iPhone 6Plus：800W：120W
     */
    
    machine = [[machine componentsSeparatedByString:@","] objectAtIndex:0];
    
    NSArray *iPhoneArr = [[NSArray alloc] initWithObjects:
                          @"iPhone4:500W:30W",
                          @"iPhone4S：800W：30W",
                          @"iPhone5：800W：120W",
                          @"iPhone5C：800W：120W",
                          @"iPhone5S：800W：120W",
                          @"iPhone6：800W：120W",
                          @"iPhone6Plus：800W：120W",
                          
                          
                          @"iPad1:无摄像头:无摄像头",
                          @"iPad2:30W:70W",
                          @"iPad4:120W:500W",
                          @"The New iPad:30W:500W",
                          @"iPad mini:120W:500W",
                          @"iPad mini2:120W:500W",
                          @"iPad mini3:120W:500W",
                          @"iPad Air:120W:500W",
                          @"iPad Air2:120W:800W",nil];
    /*
     设备名：前置摄像头：后置摄像头
     iPad 1:无摄像头:无摄像头
     iPad 2:30W:70W
     iPad 4:120W:500W
     
     The New iPad:30W:500W
     iPad mini:120W:500W
     iPad mini 2:120W:500W
     iPad mini 3:120W:500W
     
     iPad Air:120W:500W
     iPad Air 2:120W:800W
     
     */
    NSArray *iPadArr = [[NSArray alloc] initWithObjects:
                        @"iPad1:无摄像头:无摄像头",
                        @"iPad2:30W:70W",
                        @"iPad4:120W:500W",
                        @"The New iPad:30W:500W",
                        @"iPad mini:120W:500W",
                        @"iPad mini2:120W:500W",
                        @"iPad mini3:120W:500W",
                        @"iPad Air:120W:500W",
                        @"iPad Air2:120W:800W", nil];

    NSString *roStr = @"";
    
    if (isFront) {
        for (NSString *str in iPhoneArr) {
            if ([machine isEqualToString:[[str componentsSeparatedByString:@":"] objectAtIndex:0]]) {
                roStr = [[str componentsSeparatedByString:@":"] objectAtIndex:2];
                }
        }
        
    }else{
        for (NSString *str in iPhoneArr) {
            if ([machine isEqualToString:[[str componentsSeparatedByString:@":"] objectAtIndex:0]]) {
                roStr = [[str componentsSeparatedByString:@":"] objectAtIndex:1];
            }
        }
    
    }
    [iPadArr release];
    [iPhoneArr release];
    return roStr;
}

#pragma mark 获取  设备型号

- (NSString *)iphoneType {
    
    //需要导入头文件：#import <sys/utsname.h>
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])  return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])  return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])  return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])  return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])  return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])  return @"iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,2"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,3"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    
    return platform;
    
}

-(NSString *)Equipment_Type
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSArray *modelArray = @[
                            
                            @"i386", @"x86_64",
                            
                            @"iPhone1,1",
                            @"iPhone1,2",
                            @"iPhone2,1",
                            @"iPhone3,1",
                            @"iPhone3,2",
                            @"iPhone3,3",
                            @"iPhone4,1",
                            @"iPhone5,1",
                            @"iPhone5,2",
                            @"iPhone5,3",
                            @"iPhone5,4",
                            @"iPhone6,1",
                            @"iPhone6,2",
                            
                            @"iPod1,1",
                            @"iPod2,1",
                            @"iPod3,1",
                            @"iPod4,1",
                            @"iPod5,1",
                            
                            @"iPad1,1",
                            @"iPad2,1",
                            @"iPad2,2",
                            @"iPad2,3",
                            @"iPad2,4",
                            @"iPad3,1",
                            @"iPad3,2",
                            @"iPad3,3",
                            @"iPad3,4",
                            @"iPad3,5",
                            @"iPad3,6",
                            
                            @"iPad2,5",
                            @"iPad2,6",
                            @"iPad2,7",
                            ];
    NSArray *modelNameArray = @[
                                
                                @"iPhone Simulator", @"iPhone Simulator",
                                
                                @"iPhone 2G",
                                @"iPhone 3G",
                                @"iPhone 3GS",
                                @"iPhone 4(GSM)",
                                @"iPhone 4(GSM Rev A)",
                                @"iPhone 4(CDMA)",
                                @"iPhone 4S",
                                @"iPhone 5(GSM)",
                                @"iPhone 5(GSM+CDMA)",
                                @"iPhone 5c(GSM)",
                                @"iPhone 5c(Global)",
                                @"iphone 5s(GSM)",
                                @"iphone 5s(Global)",
                                
                                @"iPod Touch 1G",
                                @"iPod Touch 2G",
                                @"iPod Touch 3G",
                                @"iPod Touch 4G",
                                @"iPod Touch 5G",
                                
                                @"iPad",
                                @"iPad 2(WiFi)",
                                @"iPad 2(GSM)",
                                @"iPad 2(CDMA)",
                                @"iPad 2(WiFi + New Chip)",
                                @"iPad 3(WiFi)",
                                @"iPad 3(GSM+CDMA)",
                                @"iPad 3(GSM)",
                                @"iPad 4(WiFi)",
                                @"iPad 4(GSM)",
                                @"iPad 4(GSM+CDMA)",
                                
                                @"iPad mini (WiFi)",
                                @"iPad mini (GSM)",
                                @"ipad mini (GSM+CDMA)"
                                ];
    NSInteger modelIndex = - 1;
    NSString *modelNameString = nil;
    modelIndex = [modelArray indexOfObject:deviceString];
    if (modelIndex >= 0 && modelIndex < [modelNameArray count]) {
        modelNameString = [modelNameArray objectAtIndex:modelIndex];
    }
    
    
    NSLog(@"----设备类型---%@",modelNameString);
    
    return modelNameString;
}
#pragma mark获取 设备id号
-(NSString *)Manufacturers_ID
{
    //CFUUID
    CFUUIDRef cfuuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *cfuuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, cfuuid));
    NSLog(@"CFUUID:%@", cfuuidString);
    
    
    //NSUUID
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSLog(@"NSUDID:%@", uuid);
    
    
    //IDFV
    NSString *idfv = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSLog(@"idfv:%@", idfv);
    
    
    return cfuuidString;
}

#pragma mark获取 设备UUID号
-(NSString*) uuid
{
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    return result ;
}
#pragma mark获取 RAM容量
-(NSString*)getRAM_Capacity
{
    NSProcessInfo *info = [[NSProcessInfo alloc]init];
    return [NSString stringWithFormat:@"%lluMB",[info physicalMemory]/1024/1024];
}
#pragma mark 获取  设备总容量
-(NSString *)totalDiskSpace
{
    NSDictionary *fattributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil];
    
    long long totalSpace = [[fattributes objectForKey:NSFileSystemSize] longLongValue];
    NSString *spaceStr = [NSString stringWithFormat:@"%lldMB",totalSpace/1024/1024];
    
    return spaceStr;
}
#pragma mark 获取  设备可用容量
-(NSString *)freeDiskSpace
{
    NSDictionary *fattributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil];
    
    long long totalSpace = [[fattributes objectForKey:NSFileSystemFreeSize] longLongValue];
    NSString *spaceStr = [NSString stringWithFormat:@"%lldMB",totalSpace/1024/1024];
    
    return spaceStr;
}
#pragma mark 获取  IP
-(NSString * )getIPAddressType
{
    InitAddresses();
    GetIPAddresses();
    GetHWAddresses();
    
    int i;
    
    for (i=0; i<MAXADDRS; ++i)
    {
        static unsigned long localHost = 0x7F000001;            // 127.0.0.1
        unsigned long theAddr;
        
        theAddr = ip_addrs[i];
        
        if (theAddr == 0) break;
        if (theAddr == localHost) continue;
        
//        NSLog(@"Name: %s MAC: %s IP: %s      %d\n", if_names[i], hw_addrs[i], ip_names[i],i);
        _deviceIP = [NSString stringWithFormat:@"%s",ip_names[i]];
    }
    
    NSLog(@"deviceip = %@",_deviceIP);
    return _deviceIP;
    
}
#pragma mark - 获取网络名字 以及 WIFI Mac 地址
-(NSString *)getSSIDwith:(BOOL)SSID AndWIFIMacIPwith:(BOOL)wifiMac{
//
//}
    NSString *ssid = @"Not Found";
    NSString *macIp = @"Not Found";
    CFArrayRef myArray = CNCopySupportedInterfaces();
//    CFArrayRef myArray = CFCopyHomeDirectoryURL();
    if (myArray != nil) {
        CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        if (myDict != nil) {
            NSDictionary *dict = (NSDictionary*)CFBridgingRelease(myDict);
            
            ssid = [dict valueForKey:@"SSID"];
            macIp = [dict valueForKey:@"BSSID"];
        }
    }
    NSLog(@"SSID  =  %@     MacIP  =  %@",ssid,macIp);
    
    if (SSID) {
        return ssid;
    }
    if (wifiMac) {
        return macIp;
    }
     return @"";
}
#pragma mark 获取 网络类型
+ (NSString *)getNetworkType
{
    NSString *netconnType = @"";
    
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:// 没有网络
        {
            
            netconnType = @"no network";
        }
            break;
            
        case ReachableViaWiFi:// Wifi
        {
            netconnType = @"Wifi";
        }
            break;
            
        case ReachableViaWWAN:// 手机自带网络
        {
            // 获取手机网络类型
            CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
            
            NSString *currentStatus = info.currentRadioAccessTechnology;
            
            if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyGPRS"]) {
                
                netconnType = @"GPRS";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyEdge"]) {
                
                netconnType = @"2.75G EDGE";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyWCDMA"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSDPA"]){
                
                netconnType = @"3.5G HSDPA";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSUPA"]){
                
                netconnType = @"3.5G HSUPA";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMA1x"]){
                
                netconnType = @"2G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORev0"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevA"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevB"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyeHRPD"]){
                
                netconnType = @"HRPD";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyLTE"]){
                
                netconnType = @"4G";
            }
        }
            break;
            
        default:
            break;
    }
    
    return netconnType;
}
#pragma mark 获取  设备名
-(NSString *)getSystem
{
    // Name of the phone as named by user
    NSLog(@"设备名称 ------ %@",[[UIDevice currentDevice] name]);
    
    // "iPhone OS"
    NSLog(@"系统名称 ------ %@",[[UIDevice currentDevice] systemName]);
    
    // "2.2.1"
    NSLog(@"系统版本 ------ %@",[[UIDevice currentDevice] systemVersion]);
    
    return [NSString stringWithFormat:@"%@  %@",[[UIDevice currentDevice] systemName],[[UIDevice currentDevice] systemVersion]];
}
-(NSString *)getSystemType
{
    return [[UIDevice currentDevice] systemName];
}
-(NSString *)getSystemVersion
{
    return [[UIDevice currentDevice]systemVersion];
}

#pragma mark 获取  位置
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
            break;
        default:
            break;
            
            
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    /*
     // 6.0 调用此函数
     -(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
     NSLog(@"%@", @"ok");
     }
     */
    _checkinLocation = newLocation;
    
    _locStr = [NSString stringWithFormat:@"%f,%f",_checkinLocation.coordinate.latitude,_checkinLocation.coordinate.longitude];
    //zzs存储

    [[NSUserDefaults standardUserDefaults] setObject:_locStr forKey:@"location"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"GET_LOCATION_SUCCESS" object:_locStr];
    
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
    _locationManager = nil;
//    NSLog(@"经纬度 ------ %@",_checkinLocation);

    
}
// 错误信息
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"location error");
}

- (void) setupLocationManager {
    
//    _locationManager = [[[CLLocationManager alloc] init] autorelease] ;
    
    // 判断定位操作是否被允许
    if([CLLocationManager locationServicesEnabled]) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        //兼容iOS8定位
        SEL requestSelectorer = NSSelectorFromString(@"requestAlwaysAuthorization");
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined &&
            [_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [_locationManager requestWhenInUseAuthorization];
        }
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined &&
                  [_locationManager respondsToSelector:requestSelectorer])
        {
            
            [_locationManager requestAlwaysAuthorization];

            
        }
//        else {
            [_locationManager startUpdatingLocation];
//        }
    }else {
        NSString *location = @"-1.000000,-1.000000";
        [[NSUserDefaults standardUserDefaults] setObject:location forKey:@"location"];
        //提示用户无法进行定位操作
    }
    
}
#pragma mark - 手机model
-(NSString *)getModel{
    
    return [[UIDevice currentDevice] model];
}
#pragma mark - 分辨率
-(NSString *)getResolution{
    
    CGRect rect_screen = [[UIScreen mainScreen] bounds];
    CGSize size_screen = rect_screen.size;
    long long width = size_screen.width;
    long long height = size_screen.height;
    
    NSString *str = [NSString stringWithFormat:@"%lldx%lld",width,height];
    return str;
}

#pragma mark - 设备唯一标识
-(NSString*) getUUID {
    CFUUIDRef puuid;
    CFStringRef uuidString;
    NSString *result;
    NSString *passwordddd;
    passwordddd = [LBIdentifierManager readPassWord];
    
    if (passwordddd.length == 0) {
        puuid = CFUUIDCreate( nil );
        uuidString = CFUUIDCreateString( nil, puuid );
        result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
        [LBIdentifierManager savePassWord:result];
    }
    
    
    
    
    NSString *UUID = [LBIdentifierManager readPassWord];
    if (UUID.length==0) {
        UUID = @"";
    }
    return UUID;
    
//    CFRelease(puuid);
//    CFRelease(uuidString);
    
    
}

#pragma mark - 服务商名称
- (NSInteger)checkMobileServieType
{
    //    BOOL ret = NO;
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    if (carrier == nil) {
        //        [info release];
        //        return NO;
        return -1;
    }
    NSString *code = [carrier mobileNetworkCode];
    if (code == nil) {
        //        [info release];
        //        return NO;
        return -1;
    }
    
    
    
    //    NSInteger retcode = [code integerValue] ;
    if ([code isEqualToString:@"00"] || [code isEqualToString:@"02"] || [code isEqualToString:@"07"]) {
        //        ret = YES;
        return ChinaMobile;
    }else if ([code isEqualToString:@"01"] || [code isEqualToString:@"06"])
    {
        return ChinaUnicom;
        //    [info release];
    }else if ([code isEqualToString:@"03"] || [code isEqualToString:@"05"])
    {
        return ChinaTelecom;
    }
    else
    {
        return ChinaTietong;
    }
    
}
/**
 *  检测运营商
 *
 *  @return 返回值 运营商的名字
 */
- (NSString*)getMobileServiceType_STRING
{
    switch ([self checkMobileServieType]) {
        case 0:
            return @"ChinaMobile";
            break;
        case -1:
            return @"NoServe";
            break;
        case 1:
            return @"ChinaUnicom";
            break;
        case 2:
            return @"ChinaTelecom";
            break;
        case 3:
            return @"ChinaTietong";
            break;
        default:
            return @"NoServe——ERROR";
            break;
    }
}
- (NSString *)getCurrentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (id)CNCopySupportedInterfaces();

    
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (id)CNCopyCurrentNetworkInfo((CFStringRef)ifnam);
        ssid = [[info objectForKey:@"SSID"] autorelease];
        [info release];

    }
    [ifs release];
    return ssid ;
}


/**
 *  软件版本
 *
 *  @return 版本号 1.0.1
 */
- (NSString*)getSoftVersion
{
    NSDictionary *dic = [[NSBundle mainBundle]infoDictionary];
    return [dic objectForKeyedSubscript:@"CFBundleShortVersionString"];
}


+(NSInteger)getNetWorkStatusWithReachability{
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *children = [[[app valueForKeyPath:@"statusBar"]valueForKeyPath:@"foregroundView"]subviews];
    NSString *state = [[NSString alloc]init];
    int netType = 0;
    //获取到网络返回码
    for (id child in children) {
        if ([child isKindOfClass:NSClassFromString(@"UIStatusBarDataNetworkItemView")]) {
            //获取到状态栏
            netType = [[child valueForKeyPath:@"dataNetworkType"]intValue];
            
            switch (netType) {
                case 0:
                    state = @"无网络";
                    //无网模式
                    break;
                case 1:
                    state = @"2G";
                    break;
                case 2:
                    state = @"3G";
                    break;
                case 3:
                    state = @"4G";
                    break;
                case 5:
                {
                    state = @"WIFI";
                }
                    break;
                default:
                    break;
            }
        }
        
        
    }
    return netType;
}



@end
