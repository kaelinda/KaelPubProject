//
//  HT_CollectionRequest.m
//  Usercolection_DateBase
//
//  Created by Alaca on 15/5/11.
//  Copyright (c) 2015年 zzs. All rights reserved.
//

#import "HT_CollectionRequest.h"
#import "SBJson.h"
#import "UserInfoManager.h"
#import "GETBaseInfo.h"
#import "HT_AESCrypt.h"
#import "AFNetworking.h"
@interface HT_CollectionRequest()
{

    NSMutableArray *_resultArr;//用户行为数组
    UserInfoManager *_userInfoManager;
    GETBaseInfo *_baseInfo;
    NSString *UserInfo_URL;
    
}
@property (nonatomic,strong) NSMutableArray *resultArr;
@property (nonatomic,strong) UserInfoManager *userInfoManager;
@property (nonatomic,strong) GETBaseInfo *baseInfo;


@end



@implementation HT_CollectionRequest

-(id)initWithDelegate:(id<HT_CollectionResuqestDelegate>)delegate{
    self = [super init];
    if (self) {
        _delegate = delegate;
        _userInfoManager = [[UserInfoManager alloc] init];
        _baseInfo = [[GETBaseInfo alloc] init];
        UserInfo_URL = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ICS"]];
    }

    return self;
}
- (instancetype) init{
    NSAssert(NO, @"请使用 initWithDelegate 方法进行初始化");
    return nil;

}
#pragma mark - 请求配置信息
- (void)getPrefileInfo{
    //取值
    NSString *devType = DEVTYPE;
    //赋值
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:devType forKey:@"devType"];
    
    [self postRequestWithDictionary:paramDic andType:COM_CONFIG andIsNew:YES];
}

#pragma mark - 上传设备基本信息
/**
 *  设备基本信息
 */
- (void)postTerminalInfo{

    NSLog(@"上传设备基本信息");
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];//token永远都是取当前有效的token 所以就是直接从 NSUserDefaults 这边去取出来
    NSString *devID = dev_ID;
    
    
    NSString *msgID = [_userInfoManager getMsgID];
//    NSString *regionID = @"";//预留字段
    NSString *devType = DEVTYPE;
    NSString *Info = [_userInfoManager getDeviceinfo];
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    if (operatorCode.length <= 0) {
        operatorCode = @"";
    }
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    
    if ([APIVERSION isEqualToString:@"1"]) {
        
        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
        NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
        if (!UserID) {
            UserID = @"";
        }
        [paramDic setObject:UserID forKey:@"u"];
        
        
        if (token.length == 0 ) {
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];
        }
        
    }else
    {
        devID = [_baseInfo getUUID];
        if (token.length==0) {
//            [paramDic setObject: forKey:<#(nonnull id<NSCopying>)#>]
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];

        }else{
            [paramDic setObject:token forKey:@"token"];
            
        }
    }
    
    [paramDic setObject:devID forKey:@"devID"];
    [paramDic setObject:operatorCode forKey:@"n"];
    
    
    [paramDic setObject:msgID forKey:@"msgID"];
//    [paramDic setObject:regionID forKey:@"regionID"];
    [paramDic setObject:devType forKey:@"devType"];
    [paramDic setObject:Info forKey:@"info"];

    [self postRequestWithDictionary:paramDic andType:COM_DEVINFO_UPLOAD andIsNew:YES];
}


#pragma mark - 上传设备基本信息--新采集方式

-(void)postDeviceBaseInfoIsScheme:(BOOL)isScheme andFW:(BOOL)isFW andSD:(BOOL)isSD
{
    NSLog(@"上传设备基本信息");
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];//token永远都是取当前有效的token 所以就是直接从 NSUserDefaults 这边去取出来
    NSString *devID = dev_ID;
    
    
    NSString *msgID = [_userInfoManager getMsgID];
    //    NSString *regionID = @"";//预留字段
    NSString *devType = DEVTYPE;
//    NSString *Info = [_userInfoManager getDeviceinfo];
    
    NSString *Info = [_userInfoManager getDeviceinfoAndIsScheme:isScheme andIsFW:isFW andIsSD:isSD];
    
    
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    if (operatorCode.length <= 0) {
        operatorCode = @"";
    }
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    
    if ([APIVERSION isEqualToString:@"1"]) {

        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
        NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
        if (!UserID) {
            UserID = @"";
        }
        [paramDic setObject:UserID forKey:@"u"];
        if (token.length == 0 ) {
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];
        }
       
        
    }else
    {
        devID = [_baseInfo getUUID];
        if (token.length==0) {
         devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];
            
        }else{
            [paramDic setObject:token forKey:@"token"];
            
        }
    }
    
    [paramDic setObject:devID forKey:@"devID"];
    [paramDic setObject:operatorCode forKey:@"n"];
    
    
    [paramDic setObject:msgID forKey:@"msgID"];
    //    [paramDic setObject:regionID forKey:@"regionID"];
    [paramDic setObject:devType forKey:@"devType"];
    [paramDic setObject:Info forKey:@"info"];
    
    [self postRequestWithDictionary:paramDic andType:COM_DEVINFO_UPLOAD andIsNew:YES];
}


-(void)postDeviceBaseInfoWith:(UC_GTTYPE)gtType{
    
 
    switch (gtType) {
        case kLaunchStart:
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"GTTYPE"];
            NSLog(@"=========%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"GTTYPE"]);
            [self postDeviceBaseInfoIsScheme:NO andFW:YES andSD:YES];
        }
            break;
        case kSchemeStart:
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"GTTYPE"];
                [self postDeviceBaseInfoIsScheme:YES andFW:YES andSD:YES];
            });
          
        }
            break;
        case kUserLoginStart:
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"3" forKey:@"GTTYPE"];
            [self postDeviceBaseInfoIsScheme:NO andFW:NO andSD:YES];
        }
            break;
            
        default:
            break;
    }

}




#pragma mark - 上传用户行为信息
//获取用户行为信息
-(void)postUserActionInfoWithTVLiveData:(BOOL)isTVLiveData andTimeMove:(BOOL)isTimemove andBackData:(BOOL)isBackData andEPGDaga:(BOOL)isEpgData andVODData:(BOOL)isVODData{

    _resultArr = [[NSMutableArray alloc] init];
    NSString *actionStr = [_userInfoManager getUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES andIsNewData:YES];
    
    NSString *nilString = @"W10=";
    if ([actionStr isEqualToString:nilString]) {
        //这里说明数据为空
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EMPTYDATA" object:self userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:USERACTIONINFO],@"type", nil]];

        NSLog(@"用户行为信息为空 暂不上传");
        return;
        
    }else{
    //数据不为空的时候上传数据
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        NSString *devID = dev_ID;
        NSString *devType = DEVTYPE;
        NSString *msg = [_userInfoManager getUserActionInfoWithTVLiveData:isTVLiveData andTimeMove:isTimemove andBackData:isBackData andEPGDaga:isEpgData andVODData:isVODData andIsNewData:YES];//获取用户的具体行为
        NSString *msgID = [_userInfoManager getMsgID];//这个专门写一个函数
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
        if (operatorCode.length <= 0) {
            operatorCode = @"";
        }
        
        if ([APIVERSION isEqualToString:@"1"]) {
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
            if (!UserID) {
                UserID = @"";
            }
            [paramDic setObject:UserID forKey:@"u"];
            
        }else
        {
            devID = [_baseInfo getUUID];
            if (token.length==0) {
                
            }else{
                [paramDic setObject:token forKey:@"token"];
                
            }
        }

//        if (token.length==0) {
//            
//        }else{
//            [paramDic setObject:token forKey:@"token"];
//            
//        }//        [paramDic setObject:UserID forKey:@"UserID"];
        [paramDic setObject:devID forKey:@"devID"];
//        [paramDic setObject:regionID forKey:@"regionID"];
        [paramDic setObject:devType forKey:@"devType"];
        [paramDic setObject:@"3" forKey:@"ch"];
        [paramDic setObject:msg forKey:@"msg"];
        [paramDic setObject:msgID forKey:@"msgID"];
        [paramDic setObject:operatorCode forKey:@"n"];

        [self postRequestWithDictionary:paramDic andType:COM_USERACTION_UPLOAD andIsNew:YES];
        
        
        NSLog(@"用户行为paramDic%@",paramDic);
        
    }
    

}

#pragma mark - 上传网络信息
-(void)postNetInfo{
    //取值
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
//    NSString *devID = [_baseInfo getUUID];
    NSString *devID = dev_ID;
    NSString *msgID = [_userInfoManager getMsgID];
//    NSString *regionID = @"";//预留字段
    NSString *devType = DEVTYPE;
    NSString *netType = @"ETH";
    NSString *Info = [_userInfoManager getNetInfo];
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    if (operatorCode.length <= 0) {
        operatorCode = @"";
    }
    NSString *nilString = @"W10=";
    if ([Info isEqualToString:nilString]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EMPTYDATA" object:self userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:NETINFO],@"type", nil]];

        NSLog(@"网络信息为空 暂不上传");
        return;
    }
    //拼接
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    if ([APIVERSION isEqualToString:@"1"]) {
        
//        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
//        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"] forKey:@"u"];
//        
        
        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
        NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
        if (!UserID) {
            UserID = @"";
        }
        [paramDic setObject:UserID forKey:@"u"];
        if (token.length == 0 ) {
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];
        }

        
        
    }else
    {
        devID = [_baseInfo getUUID];
        if (token.length==0) {
            
        }else{
            [paramDic setObject:token forKey:@"token"];
            
        }
    }

    
    
    
//    
//    if (token.length==0) {
//        
//    }else{
//        [paramDic setObject:token forKey:@"token"];
//        
//    }
    [paramDic setObject:devID forKey:@"devID"];
    [paramDic setObject:msgID forKey:@"msgID"];
//    [paramDic setObject:regionID forKey:@"regionID"];
    [paramDic setObject:devType forKey:@"devType"];
    [paramDic setObject:netType forKey:@"netType"];
    [paramDic setObject:Info forKey:@"info"];
    [paramDic setObject:operatorCode forKey:@"n"];

    [self postRequestWithDictionary:paramDic andType:COM_NETINFO_UPLOAD andIsNew:YES];
    
}
#pragma mark - 上传异常信息
-(void)postErrorInfo{
    //取值
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
//    NSString *devID = [_baseInfo getUUID];
    NSString *devID = dev_ID;
//    NSString *regionID = @"";//预留字段
    NSString *devType = DEVTYPE;
    NSString *msgID = [_userInfoManager getMsgID];
    NSString *Info = [_userInfoManager getErrorInfo];
    NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
    if (operatorCode.length <= 0) {
        operatorCode = @"";
    }
    NSString *nilString = @"W10=";
    if ([Info isEqualToString:nilString]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EMPTYDATA" object:self userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:ERRORINFO],@"type", nil]];
        NSLog(@"异常信息为空 暂不上传");
        return;
    }
    //拼接
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    if ([APIVERSION isEqualToString:@"1"]) {
        
//        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
//        NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
//        if (!UserID) {
//            UserID = @"";
//        }
//        [paramDic setObject:UserID forKey:@"u"];
////        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"] forKey:@"u"];
        devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
        NSString *UserID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
        if (!UserID) {
            UserID = @"";
        }
        [paramDic setObject:UserID forKey:@"u"];
        if (token.length == 0 ) {
            
            devID = [HT_AESCrypt encrypt:devID password:@"Skyworth@@Hitune"];
            [paramDic setObject:devID forKey:@"c"];
        }

    }else
    {
        devID = [_baseInfo getUUID];
        if (token.length==0) {
            
        }else{
            [paramDic setObject:token forKey:@"token"];
            
        }
    }

    
    
//    if (token.length==0) {
//        
//    }else{
//        [paramDic setObject:token forKey:@"token"];
//        
//    }
    [paramDic setObject:devID forKey:@"devID"];
    [paramDic setObject:msgID forKey:@"msgID"];
//    [paramDic setObject:regionID forKey:@"regionID"];
    [paramDic setObject:devType forKey:@"devType"];
    [paramDic setObject:Info forKey:@"info"];
    [paramDic setObject:operatorCode forKey:@"n"];

    [self postRequestWithDictionary:paramDic andType:COM_ERRORINFO_UPLOAD andIsNew:YES];
        
}


#pragma mark - 发送上传信息请求
/**
 *  发送请求
 *
 *  @param requestDic 已经拼接好的用户信息
 *  @param type       请求类型
 *  @param isNew      是否是新数据
 */
-(void)postRequestWithDictionary:(NSDictionary *)requestDic andType:(NSString *)type andIsNew:(BOOL)isNew {

//    NSLog(@"上传的数据---mdic:%@ -- type:%@",requestDic,type);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if ([NSJSONSerialization isValidJSONObject:requestDic])
    {

        NSString *lengthStr;
//-------
        if ([type isEqualToString:COM_USERACTION_UPLOAD]) {
            
            lengthStr = [requestDic objectForKey:@"msg"];
            
        }else if ([type isEqualToString:COM_CONFIG]){
            NSLog(@"这是在请求配置文件");
            lengthStr = @"这是在请求配置文件";
        }else{
            lengthStr = [requestDic objectForKey:@"info"];
        }
//-----------
        if ([type isEqualToString:@"ICS_CONFIG"]||lengthStr.length>0) {
            manager.responseSerializer = [AFJSONResponseSerializer serializer];//申明返回的结果是json类型
            manager.requestSerializer = [AFJSONRequestSerializer serializer];//申明请求的数据是json类型
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data",@"text/plain",@"text/plain",@"text/json",nil];//如果报接受类型不一致请替换一致text/html或别的 
            [manager.requestSerializer setValue:APIVERSION forHTTPHeaderField:@"api-version"];
            [manager.requestSerializer setValue:TERMINAL_TYPE forHTTPHeaderField:@"t_type"];
            [manager.requestSerializer setValue:KREGION_CODE forHTTPHeaderField:@"regionCode"];
            NSString *urlString = [NSString stringWithFormat:@"%@/%@",UserInfo_URL,type];
            NSInteger requestTag = 0;
            if ([type isEqualToString:COM_CONFIG]) {
                requestTag = 0;//配置信息
            }
            if ([type isEqualToString:COM_DEVINFO_UPLOAD]) {
                requestTag = 1;//终端设备信息
                
            }if ([type isEqualToString:COM_USERACTION_UPLOAD]) {
                requestTag = 2;//用户行为信息
                
            }if ([type isEqualToString:COM_NETINFO_UPLOAD]) {
                requestTag = 3;//网络信息
                
            }if ([type isEqualToString:COM_ERRORINFO_UPLOAD]) {
                requestTag = 4;//异常信息
            }

            NSLog(@"k信息采集 上传地址--%@\n上传数据:%@\n---uploadtype:%@",urlString,requestDic,type);
            [manager POST:urlString parameters:requestDic progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
               
                [self AFFinishedRequestWith:[NSDictionary dictionaryWithDictionary:responseObject] andType:requestTag];
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                [self AFFailedWith:error andType:requestTag];
                
            }];
    
        }
        
        
    }
    

}
#pragma mark - 成功 或者失败
-(void)AFFailedWith:(NSError *)error andType:(NSInteger)type{
    NSLog(@"%@",error);
    NSLog(@"连接失败！");
    if (_delegate && [_delegate respondsToSelector:@selector(didFinishedPost:andResult:andType:)]) {
        
        //上传失败  需要重传 n次
        switch (type) {
            case 0:
            {
                [_delegate didFinishedPost:-1 andResult:nil andType:COM_CONFIG];
                
                break;
            }
            case 1:
            {
                [_delegate didFinishedPost:-1 andResult:nil andType:COM_DEVINFO_UPLOAD];
                
                break;
            }
            case 2:
            {
                [_delegate didFinishedPost:-1 andResult:nil andType:COM_USERACTION_UPLOAD];
                
                break;
            }
            case 3:
            {
                [_delegate didFinishedPost:-1 andResult:nil andType:COM_NETINFO_UPLOAD];
                
                break;
            }
            case 4:
            {
                [_delegate didFinishedPost:-1 andResult:nil andType:COM_ERRORINFO_UPLOAD];
                
                break;
            }
            default:
                break;
        }

    }
   


}

-(void)AFFinishedRequestWith:(NSDictionary *)resultDic andType:(NSInteger)type{
    if (_delegate && [_delegate respondsToSelector:@selector(didFinishedPost:andResult:andType:)]){
        switch (type) {
            case 0:
            {
                [_delegate didFinishedPost:0 andResult:resultDic andType:COM_CONFIG];
                
                break;
            }
            case 1:
            {
                [_delegate didFinishedPost:0 andResult:resultDic andType:COM_DEVINFO_UPLOAD];
                
                break;
            }
            case 2:
            {
                [_delegate didFinishedPost:0 andResult:resultDic andType:COM_USERACTION_UPLOAD];
                
                break;
            }
            case 3:
            {
                [_delegate didFinishedPost:0 andResult:resultDic andType:COM_NETINFO_UPLOAD];
                
                break;
            }
            case 4:
            {
                [_delegate didFinishedPost:0 andResult:resultDic andType:COM_ERRORINFO_UPLOAD];
                
                break;
            }
            default:
                break;
        }
        
        
    }

}


@end
