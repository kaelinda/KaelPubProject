//
//  GETBaseInfo.h
//  zzsRequest
//
//  Created by Alaca on 14/12/18.
//  Copyright (c) 2014年 zzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@interface GETBaseInfo : NSObject


-(NSString *)getResolutionOfFront:(BOOL)isFront andBehind:(BOOL)isBehind;//获取设备的前后摄像头像素

/**
 获取iPhone手机型号

 @return 手机型号字符传
 */
- (NSString *)iphoneType;

-(NSString *)Equipment_Type;//设备类型

-(NSString *)getModel;//设备类型

-(NSString *)getResolution;//获取设备分辨率

-(NSString *)Manufacturers_ID;//设备唯一识别ID

-(NSString*) getUUID;//设备唯一识别码

-(NSString *)totalDiskSpace;//设备全部空间

-(NSNumber *)freeDiskSpace;//设备剩余空间

-(NSString * )getIPAddressType;//IP MAC地址

-(NSString *)getSSIDwith:(BOOL)SSID AndWIFIMacIPwith:(BOOL)wifiMac;//获取WIFI mac地址    

-(NSString *)getSystem;//系统名系统版本

-(NSString *)getSystemType;//系统名

-(NSString *)getSystemVersion;//系统版本

+(NSString *)getNetworkType;//网络类型

//-(NSString *)getLocation;//获取经纬度

- (void)setupLocationManager;

- (NSString*)getMobileServiceType_STRING;//运营商名称

- (NSString *)getCurrentWifiSSID;//当前连接 wifi名称;

- (NSString*)getSoftVersion;//软件版本

-(NSString*)getRAM_Capacity;//RAM总量

/**
 *  网络类型监测
 *
 *  @return 网络类型 0 ：无网络 --- 1 ：2G  --- 2：3G --- 3---4G 5 ： WIFI
 */
+ (NSInteger)getNetWorkStatusWithReachability;

@end
