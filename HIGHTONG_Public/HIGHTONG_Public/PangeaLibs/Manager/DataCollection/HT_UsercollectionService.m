//
//  HT_UsercollectionService.m
//  HIGHTONG
//
//  Created by Alaca on 15/5/13.
//  Copyright (c) 2015年 ZhangDongfeng. All rights reserved.
//

#import "HT_UsercollectionService.h"
#import "PeriodFile.h"
#import "SAIInformationManager.h"

static HT_UsercollectionService *usercollectionservice;
@implementation HT_UsercollectionService
{
    
    NSTimer *postFailTimer;//用户行为信息 发送失败 固定周期上传
    NSInteger collectCycleNet;
    NSTimer *_collectNetTimer;
    PeriodFile *_plistManager;
    
    //*******************************
    /**
     *  周期上传信息时 用于定时上传的定时器
     */
    NSTimer *_TBETimer;
    NSTimer *_UserActionTimer;
    NSTimer *_NetInfoTimer;
    NSTimer *_ErrorInfoTimer;
    
    
    /**
     *  失败重传 时间间隔定时器
     */
    
    NSTimer *_TBESpace;
    NSTimer *_UserActionSpace;
    NSTimer *_NetInfoSpace;
    NSTimer *_ErrorInfoSpace;
    
    //********************************
    
}

+(HT_UsercollectionService *)shareUserCollectionservice{

    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        if (usercollectionservice == nil) {
            
        usercollectionservice = [[HT_UsercollectionService alloc] init];
        }
    
    });
    return usercollectionservice;
}
- (void)dealloc
{
    [usercollectionservice release];

    [super dealloc];

    
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        //注册通知中心接到通知以后 请求相应数据 或者上传相应的数据
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postaDataNotification:) name:@"COLLECTION_POST_USER_ACTION_DATA" object:nil];
        //接收 空数据不上传时候 让循环再次继续的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emptyDataNotification:) name:@"EMPTYDATA" object:nil];
        _uRequest = [[HT_CollectionRequest alloc] initWithDelegate:self];
    }
    return self;
}


#pragma mark - 通知响应事件
/**
 * 如果收集到得数据 会受到通知 然后调用此方法
 */
- (void)emptyDataNotification:(NSNotification *)notification{
    
    NSLog(@"现在这个方法已经作废了");
    return;
//    
//    NSDictionary *dic  = [notification userInfo];
//    NSInteger type = [[dic valueForKey:@"type"] integerValue];
//    switch (type) {
//        case 0:{
//            [self sendDely:1 andSelecter:PREFILEINFO andIsCircle:YES];
//            break;
//        }
//        case 1:{
//            [self sendDely:_postBaseInfoTime andSelecter:BASEINFO andIsCircle:YES];
//            break;
//        }
//        case 2:{
//            [self sendDely:_postUserActionTime andSelecter:USERACTIONINFO andIsCircle:YES];
//            break;
//        }
//        case 3:{
//            [self sendDely:_postNetInfoTime andSelecter:NETINFO andIsCircle:YES];
//            break;
//        }
//        case 4:{
//            [self sendDely:_postErrorInfoTime andSelecter:ERRORINFO andIsCircle:YES];
//            break;
//        }
//        default:
//            break;
//    }

}

/**
 * 需要上传用户信息时候 会接受到通知 然后调用此方法
 */
- (void)postaDataNotification:(NSNotification *)notification
{
    return;
//    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[notification object]];
//    NSString *postStr = [dic objectForKey:@"poststr"];
//    NSLog(@"postStr -- %@",postStr);
//    if ([postStr isEqualToString:@"resign"]) {
//        
//         //****  这里应该做一些 停止延时上传的操作
//        [self spaceInvalidateWith:10];
//        [self timerInvalidateWith:10];
//        //注册之前 需要上传一下之前用户的数据
//        [self postData];//发送下请求
//        
//        _isResign = YES;
//    }
//    
//    if ([postStr isEqualToString:@"login"]) {
//        
//        _isResign = NO;
//        //**** 这里可以开始 周期上传了
//        [self getPrefileData];//获取配置文件 成功以后会有设置定时器发送数据的操作在这里不必发送数据
//        [self postData];//发送下请求 定时器的发送是要延时的
//
//    }
    
}

-(void)changeUser:(BOOL)isNewUser{
    if (isNewUser) {
        [SAIInformationManager ONLine];
        
        GETBaseInfo *baseInfo = [[GETBaseInfo alloc]init];
        
        [SAIInformationManager ETHForIP:[baseInfo getIPAddressType] andAW:[GETBaseInfo getNetworkType] andTTL:@"" andBW:@""];
       
        _isResign = NO;
        //**** 这里可以开始 周期上传了
        [self getPrefileData];//获取配置文件 成功以后会有设置定时器发送数据的操作在这里不必发送数据
        [self postBaseInfoData:kUserLoginStart];
        
        [_uRequest postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];//用户行为
        [_uRequest postNetInfo];//网络状态
        [_uRequest postErrorInfo];//错误信息
    
    }else{
        
         [[NSUserDefaults standardUserDefaults]setObject:[SAIInformationManager serviceTimeTransformStr] forKey:@"ONLINE"];
        [SAIInformationManager OFFLine];

        //****  这里应该做一些 停止延时上传的操作
        [self spaceInvalidateWith:10];
        [self timerInvalidateWith:10];
        //注册之前 需要上传一下之前用户的数据
        [_uRequest postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];//用户行为
        [_uRequest postNetInfo];//网络状态
        [_uRequest postErrorInfo];//错误信息
        _isResign = YES;
    }

}

#pragma mark - POST Request
-(void)postData{
    return;
    //上传方式 0：不采集信息 1:开机上传 2：周期上传 3：即刻上传  默认开机上传
//    [_uRequest postTerminalInfo];//设备基本信息  设备基本信息中的地理位置信息需要耗时 所以 需要重新获取地理位置  在获取地理位置之后5秒 再去取数据然后上传
//    [_uRequest postDeviceBaseInfoWith:kUserLoginStart];
//
//    [_uRequest postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];//用户行为
//    [_uRequest postNetInfo];//网络状态
//    [_uRequest postErrorInfo];//错误信息
    
}

-(void)postTBEData{
    
//    [_uRequest postTerminalInfo];
    
    [_uRequest postDeviceBaseInfoWith:kUserLoginStart];
    
}


-(void)postBaseInfoData:(UC_GTTYPE)startType
{
    [_uRequest postDeviceBaseInfoWith:startType];
    
    
}



-(void)postUserActionData{
    
    [_uRequest postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];
    
}
-(void)postNetInfoData{


    [_uRequest postNetInfo];
    
}
-(void)postErrorInfoData{
    
    [_uRequest postErrorInfo];

}

#pragma mark - Retry POST
-(void)retryPostTBEInfo{
    
//    [_uRequest postTerminalInfo];
    [_uRequest postDeviceBaseInfoWith:kUserLoginStart];

    _baseInfoRetryNum--;

}
-(void)retryPostUserActionInfo{
    
    [_uRequest postUserActionInfoWithTVLiveData:YES andTimeMove:YES andBackData:YES andEPGDaga:YES andVODData:YES];
    _actionInfoRetryNum--;
    
}
-(void)retryPostNetInfo{
    
    [_uRequest postNetInfo];
    _netInfoRetryNum--;
    
}
-(void)retryPostErrorInfo{
    
    [_uRequest postErrorInfo];
    _errorRetryNum--;
    
}


#pragma mark - 获取配置文件
-(void)getPrefileData{
    
    if (_prefileSpace < 0) {
        return;
    }
    
    [_uRequest getPrefileInfo];
    _prefileSpace--;
}
#pragma mark - 工具方法

//存储网络信息的方法

-(void)networkForCycleinsert
{
    //用定时器之前 清掉之前的定时器 以免冲突
    collectCycleNet =  [[[PeriodFile alloc]init]getItmeWith:@"netInfo" andSecondKey:@"collectCycle"];
    
    if (collectCycleNet > 0) {
        
        if (_collectNetTimer) {
            
            [_collectNetTimer invalidate];
            _collectNetTimer = nil;
            _collectNetTimer = [NSTimer scheduledTimerWithTimeInterval:collectCycleNet target:self selector:@selector(collectNet) userInfo:nil repeats:YES];


        }else{
        _collectNetTimer = [NSTimer scheduledTimerWithTimeInterval:collectCycleNet target:self selector:@selector(collectNet) userInfo:nil repeats:YES];

        }
        

    }
    
    
    
}

/**
 * BOBO采集网络信息
 */
-(void)collectNet
{
    GETBaseInfo *baseInfo = [[GETBaseInfo alloc]init];
    
    [SAIInformationManager ETHForIP:[baseInfo getIPAddressType] andAW:[GETBaseInfo getNetworkType] andTTL:@"" andBW:[NSString stringWithFormat:@"%ld",(long)collectCycleNet]];
}



/**
 *  失败之后 重传次数 时间间隔
 */
-(void)setRetryWithNum{
    if (!_plistManager) {
        _plistManager = [[PeriodFile alloc] init];
    }

    //重试次数
    _baseInfoRetryNum   = [_plistManager getItmeWith:@"devInfo" andSecondKey:@"retryTimes"] ;
    _actionInfoRetryNum = [_plistManager getItmeWith:@"userAction" andSecondKey:@"retryTimes"];
    _netInfoRetryNum    = [_plistManager getItmeWith:@"netInfo" andSecondKey:@"retryTimes"];
    _errorRetryNum       = [_plistManager getItmeWith:@"opsInfo" andSecondKey:@"retryTimes"];
    
    //重试时间间隔
    _postBaseInfoSpace   = [_plistManager getItmeWith:@"devInfo" andSecondKey:@"retryInterval"];
    _postUserActionSpace = [_plistManager getItmeWith:@"userAction" andSecondKey:@"retryInterval"];
    _postNetInfoSpace    = [_plistManager getItmeWith:@"netInfo" andSecondKey:@"retryInterval"];
    _postErrorInfoSpace  = [_plistManager getItmeWith:@"opsInfo" andSecondKey:@"retryInterval"];
 
    
    _prefileSpace = 5;
}
/**
 *  上传周期
 */
-(void)setPeriod{
    if (!_plistManager) {
        _plistManager = [[PeriodFile alloc] init];
    }

    //--------上传周期
//    _postBaseInfoTime   = 20;//[plistManager getItmeWith:@"devInfo" andSecondKey:@"uploadCycle"];
//    _postUserActionTime = 20;//[plistManager getItmeWith:@"userAction" andSecondKey:@"uploadCycle"];
//    _postNetInfoTime    = 20;//[plistManager getItmeWith:@"netInfo" andSecondKey:@"uploadCycle"];
//    _postErrorInfoTime  = 20;//[plistManager getItmeWith:@"opsInfo" andSecondKey:@"uploadCycle"];
    
    _postBaseInfoTime   = [_plistManager getItmeWith:@"devInfo" andSecondKey:@"uploadCycle"];
    _postUserActionTime = [_plistManager getItmeWith:@"userAction" andSecondKey:@"uploadCycle"];
    _postNetInfoTime    = [_plistManager getItmeWith:@"netInfo" andSecondKey:@"uploadCycle"];
    _postErrorInfoTime  = [_plistManager getItmeWith:@"opsInfo" andSecondKey:@"uploadCycle"];
    
    
}
-(void)setIsPeriod{
    //这里是用用户行为的上传周期来判断的了
    if (_postUserActionTime>0) {
        _isPeriod = YES;
    }else{
        _isPeriod = NO;
    }
    
//    _isPeriod = YES;
}

-(void) setMode{
    //上传方式 0：不采集信息 1:开机上传 2：周期上传 3：即刻上传  默认开机上传

    if (!_plistManager) {
        _plistManager = [[PeriodFile alloc] init];
    }
    _devInfoMode    = [_plistManager getItmeWith:@"devInfo" andSecondKey:@"uploadMode"];
    _useractionMode = [_plistManager getItmeWith:@"userAction" andSecondKey:@"uploadMode"];
    _opsInfoMode    = [_plistManager getItmeWith:@"opsInfo" andSecondKey:@"uploadMode"];
    _netInfoMode    = [_plistManager getItmeWith:@"netInfo" andSecondKey:@"uploadMode"];
    
    //这里设置了上传模式以后 才能设置 定时器 按照策略上传
    
    //如果是模式 0 则不上传 如果是模式1 只上传一次   如果 等于2 周期上传  假如等于三 那是不是应该发一个通知 然后让存储完毕以后 立马上传？
//********************************
    switch (_devInfoMode) {
        case 0:{
            
            break;
        }
        case 1:{
            [self setupTimerWithType:BASEINFO andIsRepeats:NO];

            break;
        }
        case 2:{
            [self setupTimerWithType:BASEINFO andIsRepeats:YES];
            break;
        }
        case 3:{
            
            break;
        }
        default:
            break;
    }
//**************************
    switch (_useractionMode) {
        case 0:{
            
            break;
        }
        case 1:{
            [self setupTimerWithType:USERACTIONINFO andIsRepeats:NO];
            
            break;
        }
        case 2:{
            [self setupTimerWithType:USERACTIONINFO andIsRepeats:YES];
            break;
        }
        case 3:{
            
            break;
        }
        default:
            break;
    }
//*******************
    switch (_netInfoMode) {
        case 0:{
            
            break;
        }
        case 1:{
            [self setupTimerWithType:NETINFO andIsRepeats:NO];
            
            break;
        }
        case 2:{
            [self setupTimerWithType:NETINFO andIsRepeats:YES];
            break;
        }
        case 3:{
            
            break;
        }
        default:
            break;
    }
//***********************
    switch (_errorMode) {
        case 0:{
            
            break;
        }
        case 1:{
            [self setupTimerWithType:ERRORINFO andIsRepeats:NO];
            
            break;
        }
        case 2:{
            [self setupTimerWithType:ERRORINFO andIsRepeats:YES];
            break;
        }
        case 3:{
            
            break;
        }
        default:
            break;
    }
//******************
    
    _postMode = _useractionMode;//这里用 “用户信息” 上传模式替代了整个的所有的信息的上传模式
    
}
/**
 *  创建 周期上传信息的定时器
 */
-(void)setupTimerWithType:(NSInteger )type andIsRepeats:(BOOL)repeats{
    
    switch (type) {
        case BASEINFO:{
            if (_postBaseInfoTime >0 && _TBETimer==nil) {
                _TBETimer        = [NSTimer scheduledTimerWithTimeInterval:_postBaseInfoTime target:self selector:@selector(postTBEData) userInfo:nil repeats:repeats];
                
            }
            break;
        }
        case USERACTIONINFO:{
            if (_postUserActionTime > 0 && _UserActionTimer==nil) {
                _UserActionTimer = [NSTimer scheduledTimerWithTimeInterval:_postUserActionTime target:self selector:@selector(postUserActionData) userInfo:nil repeats:repeats];
                
            }
            break;
        }
        case NETINFO:{
            if (_postNetInfoTime > 0 && _NetInfoTimer==nil) {
                _NetInfoTimer    = [NSTimer scheduledTimerWithTimeInterval:_postNetInfoTime target:self selector:@selector(postNetInfoData) userInfo:nil repeats:repeats];
                
            }
            break;
        }
        case ERRORINFO:{
            
            if (_postErrorInfoTime > 0 && _ErrorInfoTimer==nil) {
                
                _ErrorInfoTimer  = [NSTimer scheduledTimerWithTimeInterval:_postErrorInfoTime target:self selector:@selector(postErrorInfoData) userInfo:nil repeats:repeats];
                
            }
            break;
        }
        default:
            break;
    }
    
    
    
    
//默认  type == 10 的时候是全部都设置 但是  在设置时应该判断下周期是不是大于0
    if (type == 10) {
        if (_postBaseInfoTime >0 && _TBETimer==nil) {
            _TBETimer        = [NSTimer scheduledTimerWithTimeInterval:_postBaseInfoTime target:self selector:@selector(postTBEData) userInfo:nil repeats:repeats];

        }
        if (_postUserActionTime > 0 && _UserActionTimer==nil) {
            _UserActionTimer = [NSTimer scheduledTimerWithTimeInterval:_postUserActionTime target:self selector:@selector(postUserActionData) userInfo:nil repeats:repeats];

        }
        if (_postNetInfoTime > 0 && _NetInfoTimer==nil) {
            _NetInfoTimer    = [NSTimer scheduledTimerWithTimeInterval:_postNetInfoTime target:self selector:@selector(postNetInfoData) userInfo:nil repeats:repeats];

        }
        
        if (_postErrorInfoTime > 0 && _ErrorInfoTimer==nil) {
            
            _ErrorInfoTimer  = [NSTimer scheduledTimerWithTimeInterval:_postErrorInfoTime target:self selector:@selector(postErrorInfoData) userInfo:nil repeats:repeats];
 
        }
        
    }

    
}
/**
 *  创建 失败重传的定时器
 *
 *  @param type 定时器类别
 */
-(void)setupSpaceTmerWith:(NSInteger )type{

    if (type == 10) {
        if (_postBaseInfoSpace > 0) {
            
            _TBESpace = [NSTimer scheduledTimerWithTimeInterval:_postBaseInfoSpace target:self selector:@selector(postTBEData) userInfo:nil repeats:YES];
        }
        
        if (_postUserActionSpace > 0) {
            
            _UserActionSpace = [NSTimer scheduledTimerWithTimeInterval:_postUserActionSpace target:self selector:@selector(postUserActionData) userInfo:nil repeats:YES];
        }
        if (_postNetInfoSpace > 0) {
            
            _NetInfoSpace = [NSTimer scheduledTimerWithTimeInterval:_postNetInfoSpace target:NSStringEncodingDetectionLikelyLanguageKey selector:@selector(postNetInfoData) userInfo:nil repeats:YES];
        }
        if (_postErrorInfoSpace > 0) {
            
            _ErrorInfoSpace = [NSTimer scheduledTimerWithTimeInterval:_postErrorInfoSpace target:self selector:@selector(postErrorInfoData) userInfo:nil repeats:YES];
        }
        
    }
    

}
/**
 *  注销 周期上传的定时器
 *
 *  @param type 定时器类别
 */
-(void)timerInvalidateWith:(NSInteger )type{
    
    switch (type) {
        case BASEINFO:{
            
            [_TBETimer invalidate];
            _TBETimer = nil;
            
            break;
        }
        case USERACTIONINFO:{
            
            [_UserActionTimer invalidate];
            _UserActionTimer = nil;
            break;
        }
        case NETINFO:{
            
            [_NetInfoTimer invalidate];
            _NetInfoTimer = nil;
            break;
        }
        case ERRORINFO:{
            
            [_ErrorInfoTimer invalidate];
            _ErrorInfoTimer = nil;
            break;
        }
        default:
            break;
    }


}
/**
 *  注销 失败重传的定时器
 *
 *  @param type 定时器类别
 */
-(void)spaceInvalidateWith:(NSInteger )type{

    switch (type) {
        case BASEINFO:{
            if (_TBESpace) {
                
                [_TBESpace invalidate];
                _TBESpace = nil;
            }
            break;
        }
        case USERACTIONINFO:{
            
            if (_UserActionSpace) {
               
                [_UserActionSpace invalidate];
                _UserActionSpace = nil;
            }

            break;
        }
        case NETINFO:{
            
            if (_NetInfoSpace) {
                [_NetInfoSpace invalidate];
                _NetInfoSpace = nil;
            }

            break;
        }
        case ERRORINFO:{
            
            if (_ErrorInfoSpace) {
                
                [_ErrorInfoSpace invalidate];
                _ErrorInfoSpace = nil;
            }

            break;
        }
        default:
            break;
    }
    

}


#pragma mark - 延时操作
/**
 * 延时操作方法
 * @param daly  :  需要延迟的时间
 * @param type  :  需要延迟调用的方法 0：获取配置信息 1：设备基本信息 2：用户行为信息 3：网络信息 4：异常信息
 */
-(void)sendDely:(NSInteger)daly andSelecter:(NSInteger)type andIsCircle:(BOOL)isCircle{
    NSLog(@"采集  %ld--周期  %ld--类型",(long)daly,(long)type);
    if (daly <= 0) {
        return;
    }
//*****************延时时间 >=0 时候 才有效
    double delayInSeconds = daly;
    __block HT_UsercollectionService* bself = self; //GCD 中得block 是强引用
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        switch (type) {
            case PREFILEINFO:
            {
                [bself getPrefileData];
                break;
            }
            case BASEINFO:
            {
                [bself retryPostTBEInfo];
                break;
            }
            case USERACTIONINFO:
            {
                [bself retryPostUserActionInfo];
                break;
            }
            case NETINFO:
            {
                [bself retryPostNetInfo];
                break;
            }
            case ERRORINFO:
            {
                [bself retryPostErrorInfo];
                break;
            }
                
            default:
                break;
        }
    });
    
}


#pragma mark
#pragma mark
#pragma mark
#pragma mark
#pragma mark - request delegate
-(void)didFinishedPost:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{
    
    NSLog(@"CJ********status---%ld  result -----%@ type----%@",(long)status,result,type);
    
    NSInteger ret = [[result objectForKey:@"ret"] integerValue];
    
    if ([type isEqualToString:COM_DEVINFO_UPLOAD]) {
        //设备基本信息
        if (ret == 0 && status == 0)
        {
           
            
        }else
        {
            //如果是注销操作  失败后 不需要重传
            if (_isResign) {
            
                return;
            }
            
            if (_baseInfoRetryNum>0) {
                
                [self sendDely:self.postBaseInfoSpace andSelecter:BASEINFO andIsCircle:NO];//这里反而不能使用定时器
            }

        }
        
        
    }else if ([type isEqualToString:COM_USERACTION_UPLOAD]) {
        //用户行为信息
        if (ret == 0 && status == 0) {
            //bb删除用户行为信息
            
            [SAIInformationManager deleteUserInformation];
            
            //end

        }else{
            //如果是注销操作  失败后 不需要重传
            if (_isResign) {
                return;
            }
            
            if (_actionInfoRetryNum>0) {
                [self sendDely:self.postUserActionSpace andSelecter:USERACTIONINFO andIsCircle:NO];//这里不能使用定时器

            }
            
        }
        
    }else if ([type isEqualToString:COM_NETINFO_UPLOAD]) {
        //网络信息
        if (ret == 0 && status == 0) {
            //bb删除网络信息
            
            
            [SAIInformationManager deleteUserETH];
            
            //end
            
        }else{
            //如果是注销操作  失败后 不需要重传
            if (_isResign) {
                return;
            }
            if (_netInfoRetryNum>0) {
                
                [self sendDely:self.postNetInfoSpace andSelecter:NETINFO andIsCircle:NO];//
            }

        }
    }else if ([type isEqualToString:COM_ERRORINFO_UPLOAD]) {
        //异常信息
        if (ret == 0 && status == 0) {
            //bb删除错误信息
            
            [SAIInformationManager deleteUserOPS];
            //end
            
        }else{
            //如果是注销操作  失败后 不需要重传
            if (_isResign) {
                return;
            }
            if (_errorRetryNum) {
                [self sendDely:self.postErrorInfoSpace andSelecter:ERRORINFO andIsCircle:NO];

            }

        }
    }else if ([type isEqualToString:COM_CONFIG]){
        //配置信息
        if (ret == 0 && status == 0)
        {
           //获取配置信息成功后
            /*
             1、存储到plist文件中
             2、赋值给各个属性
             */
            if (!_plistManager) {
                _plistManager = [[PeriodFile alloc] init];
            }
            [_plistManager writePlistWith:result];//写入plist文件
            
            //这里应该先 设置上传的参数 ===》 按照模式上传
            [self setPeriod];//设置上传周期
            [self setRetryWithNum];//设置重传次数
            [self setMode];//设置上传模式  上传周期 重传次数 其实应该在上传模式里面设置的吧

            [self networkForCycleinsert];//网络信息按照周期循环采集
            
            
            NSLog(@"查看**** _postBaseInfoTime :%ld_postUserActionTime :%ld_postNetInfoTime :%ld_postErrorInfoTime :%ld",(long)_postBaseInfoTime,(long)_postUserActionTime,(long)_postNetInfoTime,(long)_postErrorInfoTime);
            
            NSLog(@"_postBaseInfoSpace :%ld_postUserActionSpace :%ld_postNetInfoSpace :%ld_postErrorInfoSpace :%ld",(long)_postBaseInfoSpace,(long)_postUserActionSpace,(long)_postNetInfoSpace,(long)_postErrorInfoSpace);
            
            
//            [self postData];
            
        }else
        {
            //如果是注销操作  失败后 不需要重传
            if (_isResign) {
                return;
            }
            [self performSelector:@selector(getPrefileData) withObject:nil afterDelay:10.0f];
        }
    }
    
    
    

}

#pragma mark - 底部

@end
