//
//  CarouselPageModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/11/24.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarouselPageModel : NSObject
@property (nonatomic,copy) NSString *carouselSEQ;
@property (nonatomic,copy) NSString *carouselID;
@property (nonatomic,copy) NSString *carouselName;
@property (nonatomic,copy) NSString *carouselType;
@property (nonatomic,copy) NSString *carouselBT;
+ (instancetype)carouselCollectionWithDict:(NSDictionary *)dict;

@end
