//
//  HealthVodCollectionModel.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/8/19.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthVodCollectionModel : NSObject

@property (nonatomic,strong)NSString *itemId;//节目ID
@property (nonatomic,strong)NSString *name;//节目名称
@property (nonatomic,strong)NSString *ENtype;//节目类型
@property (nonatomic,strong)NSString *TrackId;//节目路径
@property (nonatomic,strong)NSString *TrackName;//节目名称路径
@property (nonatomic,strong)NSString *watchTime;//观看的时刻
@property (nonatomic,strong)NSString *watchDuration;//观看的时常

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)heaalthVodWithDict:(NSDictionary *)dict;

@end
