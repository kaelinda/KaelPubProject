//
//  CarouselPageModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/11/24.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "CarouselPageModel.h"

@implementation CarouselPageModel

- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}


+ (instancetype)carouselCollectionWithDict:(NSDictionary *)dict
{
    return  [[self alloc]initWithDict:dict];

}

@end
