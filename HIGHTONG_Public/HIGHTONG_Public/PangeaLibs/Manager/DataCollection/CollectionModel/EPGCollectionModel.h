//
//  EPGCollectionModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/26.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPGCollectionModel : NSObject
@property (nonatomic,copy)NSString *liveServiceID;
@property (nonatomic,copy)NSString *liveServiceName;
@property (nonatomic,copy)NSString *liveViedoName;
@property (nonatomic,copy)NSString *liveWatchTime;
@property (nonatomic,copy)NSString *liveWatchDuration;
@property (nonatomic,copy)NSString *liveEventLength;
@property (nonatomic,copy)NSString *liveLiveTime;
@property (nonatomic,copy)NSString *liveTRACKID;
@property (nonatomic,copy)NSString *liveTRACKNAME;
@property (nonatomic,copy)NSString *liveEN;
@property (nonatomic,copy)NSString *eventServiceID;
@property (nonatomic,copy)NSString *eventServiceName;
@property (nonatomic,copy)NSString *eventVideoName;
@property (nonatomic,copy)NSString *eventEventID;
@property (nonatomic,copy)NSString *eventWatchTime;
@property (nonatomic,copy)NSString *eventWatchDuration;
@property (nonatomic,copy)NSString *eventEventLength;
@property (nonatomic,copy)NSString *eventEventTime;
@property (nonatomic,copy)NSString *eventTRACKID;
@property (nonatomic,copy)NSString *eventTRACKNAME;
@property (nonatomic,copy)NSString *eventEN;
@property (nonatomic,copy)NSString *timeMoveServiceID;
@property (nonatomic,copy)NSString *timeMoveServiceName;
@property (nonatomic,copy)NSString *timeMoveViedoName;
@property (nonatomic,copy)NSString *timeMoveWatchTime;
@property (nonatomic,copy)NSString *timeMoveWatchDuration;
@property (nonatomic,copy)NSString *timeMoveEventLength;
@property (nonatomic,copy)NSString *timeMoveTimeMoveTime;
@property (nonatomic,copy)NSString *timeMoveTRACKID;
@property (nonatomic,copy)NSString *timeMoveTRACKNAME;
@property (nonatomic,copy)NSString *timeMoveEN;
@property (nonatomic,copy)NSString *timeMoveTWT;




+ (instancetype)liveCollectionWithDict:(NSDictionary *)dict;
+ (instancetype)eventCollectionWithDict:(NSDictionary *)dict;
+ (instancetype)timeMoveCollectionWithDict:(NSDictionary *)dict;


@end
