//
//  ShareCollectionModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/7/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "ShareCollectionModel.h"

@implementation ShareCollectionModel
- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)shareCollectionWithDict:(NSDictionary *)dict{
    
    return  [[self alloc]initWithDict:dict];
}
@end
