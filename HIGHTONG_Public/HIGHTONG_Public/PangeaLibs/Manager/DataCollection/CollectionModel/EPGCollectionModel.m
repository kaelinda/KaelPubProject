//
//  EPGCollectionModel.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/8/26.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "EPGCollectionModel.h"

@implementation EPGCollectionModel



- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)liveCollectionWithDict:(NSDictionary *)dict;
{
    return  [[self alloc]initWithDict:dict];
}
+ (instancetype)eventCollectionWithDict:(NSDictionary *)dict
{
    return  [[self alloc]initWithDict:dict];
}
+ (instancetype)timeMoveCollectionWithDict:(NSDictionary *)dict
{
    return  [[self alloc]initWithDict:dict];
}
@end
