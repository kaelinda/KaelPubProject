//
//  ShareCollectionModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/7/27.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareCollectionModel : NSObject
@property (nonatomic,copy)NSString *shareID;
@property (nonatomic,copy)NSString *shareTitle;
@property (nonatomic,copy)NSString *shareTime;
@property (nonatomic,copy)NSString *shartType;
@property (nonatomic,copy)NSString *shartModel;
- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)shareCollectionWithDict:(NSDictionary *)dict;
@end
