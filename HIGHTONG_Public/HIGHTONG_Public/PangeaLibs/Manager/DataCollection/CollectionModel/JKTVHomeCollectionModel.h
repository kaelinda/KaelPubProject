//
//  JKTVHomeCollectionModel.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 16/8/18.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKTVHomeCollectionModel : NSObject

@property (nonatomic,strong)NSString *moduleCode;
@property (nonatomic,strong)NSString *moduleName;
@property (nonatomic,strong)NSString *systemTime;

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)homeWithDict:(NSDictionary *)dict;

@end
