//
//  PubVodInteractiveCollectionModel.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/6/19.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PubVodInteractiveCollectionModel : NSObject

@property (nonatomic,strong)NSString *programId;
@property (nonatomic,strong)NSString *programName;
@property (nonatomic,strong)NSString *systemTime;

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)pubVodInteractiveWithDict:(NSDictionary *)dict;

@end
