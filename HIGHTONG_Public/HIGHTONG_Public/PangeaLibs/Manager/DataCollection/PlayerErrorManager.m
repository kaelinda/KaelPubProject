//
//  PlayerErrorManager.m
//  HIGHTONG_Public
//
//  Created by Kael on 2016/12/8.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PlayerErrorManager.h"

@implementation PlayerErrorManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isNormalPlayed = NO;
        _isAVLoading = NO;
        _startErrorDate = nil;
        _endErrorDate = nil;
        
    }
    return self;
}

-(void)setIsNormalPlayed:(BOOL)isNormalPlayed{
    _isNormalPlayed = isNormalPlayed;
    if (isNormalPlayed) {
        _isAVLoading = NO;
    }
}

-(void)setIsAVLoading:(BOOL)isAVLoading{
    _isAVLoading = isAVLoading;
}

-(void)resetPlayerStatusInfo{
    _isNormalPlayed = NO;
    _isAVLoading = NO;
    _startErrorDate = nil;
    _endErrorDate = nil;
}




-(void)OPSForPlayerErrorWithSourceType:(VideoSourceType)sourceType andAVName:(NSString *)avNameStr andOPSCode:(PlayerErrorType)errorType andOPSST:(NSString *)opsstStr;
{
    NSString *durationStr = @"";
    
    if (_isNormalPlayed && !(errorType == PlayerAVError)) {
        return;
    }
    
    NSString *sourceTypeStr;
    NSString *errorTypeStr;
    switch (sourceType) {
        case VideoSource_CableNet:
        {
            sourceTypeStr = @"CABLENET";
        }
            break;
        case VideoSource_InterNet:
        {
            sourceTypeStr = @"INTERNET";
        }
            break;
        default:
            break;
    }
    
    _baseInfo = [[GETBaseInfo alloc]init];
    
    
    switch (errorType) {
        case PlayerOutTimeError:
        {
            errorTypeStr = @"901";
             durationStr =[NSString stringWithFormat:@"%.f",[_endErrorDate timeIntervalSinceDate:_startErrorDate]];
            if ([durationStr intValue]>=10) {
                [SAIInformationManager OPSForNetType:[GETBaseInfo getNetworkType] andWIFISSID:[_baseInfo getSSIDwith:YES AndWIFIMacIPwith:NO] andSourceType:sourceTypeStr andAVName:avNameStr andOPSCode:errorTypeStr andOPSST:opsstStr andDuration:durationStr];
            }

        }
            break;
        case PlayerLoadingExitError:
        {
            errorTypeStr = @"902";
            
            durationStr =[NSString stringWithFormat:@"%.f",[_endErrorDate timeIntervalSinceDate:_startErrorDate]];
            
            if ([durationStr intValue]>=10) {
                [SAIInformationManager OPSForNetType:[GETBaseInfo getNetworkType] andWIFISSID:[_baseInfo getSSIDwith:YES AndWIFIMacIPwith:NO] andSourceType:sourceTypeStr andAVName:avNameStr andOPSCode:errorTypeStr andOPSST:opsstStr andDuration:durationStr];
            }

        }
            break;
        case PlayerAVError:
        {
            errorTypeStr = @"903";
            durationStr = @"";
            [SAIInformationManager OPSForNetType:[GETBaseInfo getNetworkType] andWIFISSID:[_baseInfo getSSIDwith:YES AndWIFIMacIPwith:NO] andSourceType:sourceTypeStr andAVName:avNameStr andOPSCode:errorTypeStr andOPSST:opsstStr andDuration:durationStr];
            

        }
            break;
        default:
            break;
    }
    
    
    
   
    
}




-(void)OPSForPlayerErrorWith:(NSString *)netTypeStr andWIFISSID:(NSString *)wifiSSIDStr andSourceType:(VideoSourceType)sourceType andAVName:(NSString *)avNameStr andOPSCode:(PlayerErrorType)errorType andOPSST:(NSString *)opsstStr andDuration:(NSString *)durationStr andIsNormalPlayed:(BOOL)isNormalPlayed{
    
    _isNormalPlayed = isNormalPlayed;
    if (_isNormalPlayed && !(errorType == PlayerAVError)) {
        return;
    }
    
    NSString *sourceTypeStr;
    NSString *errorTypeStr;
    switch (sourceType) {
        case VideoSource_CableNet:
        {
            sourceTypeStr = @"CABLENET";
        }
            break;
        case VideoSource_InterNet:
        {
            sourceTypeStr = @"INTERNET";
        }
            break;
        default:
            break;
    }
    
    switch (errorType) {
        case PlayerOutTimeError:
        {
            errorTypeStr = @"901";
        }
            break;
        case PlayerLoadingExitError:
        {
            errorTypeStr = @"902";
        }
            break;
        case PlayerAVError:
        {
            errorTypeStr = @"903";
        }
            break;
        default:
            break;
    }
    
    [SAIInformationManager OPSForNetType:netTypeStr andWIFISSID:wifiSSIDStr andSourceType:sourceTypeStr andAVName:avNameStr andOPSCode:errorTypeStr andOPSST:opsstStr andDuration:durationStr];
}

@end
