//
//  BPushSingleTon.m
//  HIGHTONG_Public
//
//  Created by lailibo on 2017/1/6.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "BPushSingleTon.h"
#import "BPush.h"
#import "HTRequest.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import "HTVideoAVPlayerVC.h"
#import "EpgSmallVideoPlayerVC.h"
#import "DemandVideoPlayerVC.h"
#import "UserLoginViewController.h"
#import "MineViewcontrol.h"
#import "KnowledgeBaseViewController.h"
#import "CustomAlertView.h"
#import "UrlPageJumpManeger.h"
#import "HT_NavigationController.h"
#import "HealthVodViewController.h"
#import "YJHealthVideoSecondViewcontrol.h"
//#import "HTALViewController.h"
//#import "UGCManagerHomeViewController.h"
#import "PubVideoChannelViewController.h"
#import "PubVideoHomeViewController.h"


#import "EpgSmallVideoPlayerVC.h"
@interface BPushSingleTon ()<UIAlertViewDelegate,HTRequestDelegate,CustomAlertViewDelegate>
{
    UINavigationController *rootnav;
    UserLoginViewController *user;
//    UIAlertView *cusAlert;
    
    NSString *channelID;
    NSString *channelName;
    
    NSInteger  healthIndex;
}










/**
 消息标题(必填)
 */
@property (strong,nonatomic) NSString *msgTitle
;
/**
 groupIds = 分组编号(必填) 多组时用逗号分隔

 */
@property (strong,nonatomic) NSString *groupIds;
/**
 pushTime = 推送时间(非必填),空值时默认为及时推送
 */
@property (strong,nonatomic) NSString *pushTime;
/**
 msgType = 通知类型 0：透传  1：通知(默认)
 */
@property (strong,nonatomic) NSString *msgType;
/**
 msgBody = 消息内容 (必填
 */
@property (strong,nonatomic) NSString *msgBody;
/**
 extra = 其他业务参数
 */
@property (strong,nonatomic) NSDictionary *dicIN;

@property (strong,nonatomic) NSString *extraId;

@property (strong,nonatomic) NSString *extraFilterId;

@property (strong,nonatomic) NSString *extraBusFlag;

@property (strong,nonatomic) NSString *extraTypeFlag;

@property (strong,nonatomic) NSString *extraInfo;

@property (strong,nonatomic) NSString *extraUrl;

@property (strong,nonatomic) NSString *extraImageUrl;

@property (strong,nonatomic) NSString *extraAction;

@property (strong,nonatomic) NSString *extraName;

@property (assign,nonatomic) NSInteger alertTag;

/**
 *  首页类型
 */
@property (nonatomic,assign) HomePageType pagetype;


@end
@implementation BPushSingleTon


+ (id)shareInstance
{
    static BPushSingleTon *singleTon;
    
    if (!singleTon) {
        
        singleTon = [[BPushSingleTon alloc] init];
        
    }
    
    return singleTon;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBind) name:@"LGOIN_SUCCESS_JUMP_TO_FATHER_VIEW" object:nil];
        rootnav = (UINavigationController*)[UIApplication sharedApplication].keyWindow.rootViewController;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enterBpushSingten:) name:@"MSG_ADEND_ENTER_APPLICATION" object:nil];
        
        self.pagetype = kApp_HomePageType;
        
        [self chooseAppType];
        
//        channelID = @"a68ff954fe0246559bd1b843b1a5f917";
//        channelName = @"我是大医生";
    }
    return self;
}

-(void)updateBind
{
    if (![KMobileNum isEqualToString:MSG_USERMOBILE]) {
        
        [self bindUpdateChannel];
    }
}

- (void)chooseAppType
{
    
    switch (self.pagetype) {
        case kHomePageType_JXCable_v1_0:
        {
            
        }
            
            break;
            
        case kHomePageType_JKTV_v1_1:
            
        {
            
        }
            
            break;
            
        case kHomePageType_JKTV_V1_2:
        case kHomePageType_JKTV_V1_3:
        case kHomePageType_JKTV_V1_4:
        {
            
            healthIndex = 1;
            
        }
            
            break;
        case kHomePageType_JKTV_V1_5:{
            
            healthIndex = 2;
            
        }
            break;
        default:
            break;
    }

    
}

- (void)handleNotification:(NSDictionary *)userInfo
{
    [BPush handleNotification:userInfo];
}

- (void)registerDeviceToken:(NSData *)deviceToken
{
    [BPush registerDeviceToken:deviceToken];
}

- (void)bindChannel
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"BindGoupSuccess"]== YES) {
        return;
    }
    [BPush bindChannelWithCompleteHandler:^(id result, NSError *error) {
        
        // 网络错误
        if (result) {
            // 确认绑定成功
//            if ([result[@"error_code"]intValue]!=0) {
//                return;
//            }
            // 获取channel_id
            NSString *myChannel_id = [BPush getChannelId];
            NSLog(@"==channelID%@",myChannel_id);
            
            [[NSUserDefaults standardUserDefaults]setObject:myChannel_id forKey:@"channelId"];
            
            
            HTRequest *htRequest = [[HTRequest alloc]initWithDelegate:self];
            
            [htRequest getMobileMessageBindGoupWithChannelID:MSG_CHANNELID withGroupID:GROUPID withDeviceType:@"0" withBusSystem:@"medicine"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
    }];
}

-(void)bindUpdateChannel
{
    [BPush bindChannelWithCompleteHandler:^(id result, NSError *error) {
        //        [self.viewController addLogString:[NSString stringWithFormat:@"Method: %@\n%@",BPushRequestMethodBind,result]];
        // 需要在绑定成功后进行 settag listtag deletetag unbind 操作否则会失败
        
        // 网络错误
        if (error) {
            NSLog(@"推送：%@",error);
            return ;
        }
        if (result) {
            // 确认绑定成功
            if ([result[@"error_code"]intValue]!=0) {
                return;
            }
            // 获取channel_id
            NSString *myChannel_id = [BPush getChannelId];
            NSLog(@"==%@",myChannel_id);
            
            HTRequest *htRequest = [[HTRequest alloc]initWithDelegate:self];
            
            [htRequest getMobileMessageBindingUpdateWithChannelId:[BPush getChannelId] withMobile:KMobileNum];
            
            //            [BPush listTagsWithCompleteHandler:^(id result, NSError *error) {
            //                if (result) {
            //                    NSLog(@"result ============== %@",result);
            //                }
            //            }];
            //            [BPush setTag:@"Mytag" withCompleteHandler:^(id result, NSError *error) {
            //                if (result) {
            //                    NSLog(@"设置tag成功");
            //                }
            //            }];
        }
        
    }];

}


-(void)enterBpushSingten:(NSNotification*)noti
{
    NSLog(@"收到的AlertView%@===",noti.object);
    
    
    if (!isEmptyStringOrNilOrNull(noti.object)) {
            NSString *notiObject = [NSString stringWithFormat:@"%@",noti.object];
            NSMutableDictionary *Muldic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"msgUserInfo"],@"KeyNoti", nil];
            
            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
                
                            dic = [Muldic objectForKey:@"KeyNoti"];
            
            if (!dic || dic == (id)kCFNull || [dic isEqual:@""]) {
                
                return ;
            }else {
                
                        _extraBusFlag = isEmptyStringOrNilOrNull([dic objectForKey:@"busFlag"])?@"":[dic objectForKey:@"busFlag"];
                        
                _extraInfo = isEmptyStringOrNilOrNull([dic objectForKey:@"info"])?@"":[dic objectForKey:@"info"];
                
                        _extraId = isEmptyStringOrNilOrNull([dic objectForKey:@"id"])?@"":[dic objectForKey:@"id"];
                        
                        _extraTypeFlag = isEmptyStringOrNilOrNull([dic objectForKey:@"typeFlag"])?@"":[dic objectForKey:@"typeFlag"];
                        
                        _extraUrl = isEmptyStringOrNilOrNull([dic objectForKey:@"url"])?@"":[dic objectForKey:@"url"];
                        
                        _extraImageUrl = isEmptyStringOrNilOrNull([dic objectForKey:@"imageUrl"])?@"":[dic objectForKey:@"imageUrl"];
                        
                        _extraAction = isEmptyStringOrNilOrNull([dic objectForKey:@"action"])?@"":[dic objectForKey:@"action"];
                        
                        _extraName = isEmptyStringOrNilOrNull([dic objectForKey:@"name"])?@"":[dic objectForKey:@"name"];

                        _extraFilterId = isEmptyStringOrNilOrNull([dic objectForKey:@"filterId"])?@"":[dic objectForKey:@"filterId"];
                
                        _msgBody = isEmptyStringOrNilOrNull([[[dic objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"])?@"":[[[dic objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
                
                
                
                if ([_extraAction intValue]== 0) {
                    
    
                    if ([rootnav.topViewController isKindOfClass:[HTVideoAVPlayerVC class]]) {
                        
                        [self rootNaviForHTVideoAVPlayerVCAndNotification:notiObject];
                    }

                    else if ([rootnav.topViewController isKindOfClass:[DemandVideoPlayerVC class]]) {
                        
                        [self rootNaviForDemandVideoPlayerVCAndNotification:notiObject];
                        
                    }

                    else if ([rootnav.topViewController isKindOfClass:[EpgSmallVideoPlayerVC class]]) {

                        [self rootNaviForEpgSmallVideoPlayerVCAndNotification:notiObject];
                    }
    
                    else if ([rootnav.topViewController isKindOfClass:[PubVideoHomeViewController class]])
                    {
                        [self rootNaviForPubVideoHomeViewControllerAndNotification:notiObject];
                    }
                    else if ([[(UINavigationController*)rootnav.presentedViewController topViewController] isKindOfClass:[UserLoginViewController class]]) {
                        
                        [self rootNaviForUserLoginViewControllerAndNotification:notiObject];
                    }
                    else
                    {
                        
                        [self rootNaviForOtherViewControllerAndNotification:notiObject];
                        
                    }
                    
                        
                    }else
                    {
                        return;
                    }
                
           
            }

    }
    
    
   
}




-(void)portraitForOrientation
{
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];

}

#pragma mark AlertView回调


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    
    switch (alertView.tag) {
        case MSG_PUBHUMOURTAG:
        {
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    if (buttonIndex == 1) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                        [self portraitForOrientation];
                        [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                        
                        [self inBackGroundSelectPubVideoHomeViewController];
                    }else
                    {
                        [self clickLeftCancel];
                        
                    }

                    
                    
                }else
                {
                    [self rootNaviForPubVideoHomeViewControllerAndClickedButtonAtIndex:buttonIndex];
                }
                
            }else
            {
                [self rootNaviForPubVideoHomeViewControllerAndClickedButtonAtIndex:buttonIndex];
            }
            
        }
            break;
        case MSG_PUBHUMOURH5TAG:
        {
            if (buttonIndex == 1) {
                //                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [self portraitForOrientation];
                
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                
                [self clickRightForJumpH5];
                
            }else
            {
                [self clickLeftCancel];
            }
            

        }
            break;
        case MSG_HEALTHPLAYERTAG:
        {
            
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    if (buttonIndex == 1) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                        [self portraitForOrientation];
                        [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                        
                        [self inBackGroundSelectPubVideoHomeViewController];
                        
                    }else
                    {
                        [self clickLeftCancel];
                    }
                }else
                {
                    [self rootNaviForHTVideoAVPlayerVCAndClickedButtonAtIndex:buttonIndex];
                }
            }else
            {
                [self rootNaviForHTVideoAVPlayerVCAndClickedButtonAtIndex:buttonIndex];
            }
            
            
 
        }
            break;
        case MSG_HEALTHH5TAG:
        {
            if (buttonIndex == 1) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [self portraitForOrientation];

                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];

                
                [self clickRightForJumpH5];
                
            }else
            {
                [self clickLeftCancel];
            }

        }
            break;
        case MSG_LOGINTAG:
        {
            
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    
                    if (buttonIndex == 1) {
                        
                        [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];

                        [self inBackGroundSelectPubVideoHomeViewController];
                        
                    }else
                    {
                        [self clickLeftCancel];
                    }
                }else
                {
                    [self rootNaviForUserLoginViewControllerAndClickedButtonAtIndex:buttonIndex];
                }
                
                
            }else
            {
                [self rootNaviForUserLoginViewControllerAndClickedButtonAtIndex:buttonIndex];
            }
        }
            break;
        case MSG_LOGINH5TAG:
        {
            if (buttonIndex == 1) {

                [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                KnowledgeBaseViewController *skipCtr = [[KnowledgeBaseViewController alloc]init];
                skipCtr.urlStr = [UrlPageJumpManeger bPushJumpWithExtraInfo:_extraUrl andExtraId:_extraId];
                skipCtr.existNaviBar = YES;
                [rootnav pushViewController:skipCtr animated:NO];
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }else
            {
                [self clickLeftCancel];
            }

        }
            break;
        case MSG_EPGDEMANDTAG:
        {
            
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    if (buttonIndex == 1) {
                        [self portraitForOrientation];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                        
                        [self inBackGroundSelectPubVideoHomeViewController];
                        
                    }else
                    {
                        
                        [self clickLeftCancel];
                    }
   
                    
                }else
                {
                    [self rootNaviForEpgDemandAndClickedButtonAtIndex:buttonIndex];
                }
            }else
            {
                [self rootNaviForEpgDemandAndClickedButtonAtIndex:buttonIndex];
            }
            
            
           
        }
            break;
        case MSG_EPGDEMANDH5TAG:
        {
            if (buttonIndex == 1) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];

                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];

                [self clickRightForJumpH5];
                
                
            }else
            {
              
                
                [self clickLeftCancel];
            }

        }
            break;
        case MSG_UGCLIVEPLAYER_PROGRAMPLAYERTAG:
        {
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    if (buttonIndex == 1)
                    {
                        [self portraitForOrientation];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                       
                        [self inBackGroundSelectPubVideoHomeViewController];
                        
                    }else
                    {
                        [self clickLeftCancel];
                        
                    }
  
                    
                }else
                {
                    [self rootNaviForUGCLiveViewControllerAndClickedButtonAtIndex:buttonIndex];
 
                }
                
            }else
            {
                [self rootNaviForUGCLiveViewControllerAndClickedButtonAtIndex:buttonIndex];
            }
            
            
                   }
            break;
        case MSG_UGCLIVEPLAYER_PROGRAMH5TAG:
        {
            if (buttonIndex == 1)
            {
                [self portraitForOrientation];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [self clickRightForJumpH5];
                


            }else
            {
                [self clickLeftCancel];

            }
        }
            break;
        case MSG_UGCALVIEWCONTROLLERH5TAG:
        {
            if (buttonIndex == 1)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];

                [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                
            }else
            {
                [self clickLeftCancel];
            }
        }
            break;
       
        case MSG_COMMONPLAYERTAG:
        {
            if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
                if ([_extraBusFlag isEqualToString:@"2"]) {
                    
                    if (buttonIndex == 1) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                        [rootnav popToRootViewControllerAnimated:YES];
                        
                        [self inBackGroundSelectPubVideoHomeViewController];
                        
                    }else
                    {
                        [self clickLeftCancel];
                    }
 
                }else
                {
                    [self rootNaviForOtherViewControllerAndClickedButtonAtIndex:buttonIndex];

                }
                
            }else
            {
                [self rootNaviForOtherViewControllerAndClickedButtonAtIndex:buttonIndex];
            }
           
        }
            break;
        case MSG_COMMONH5TAG:
        {
            if (buttonIndex == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];

                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];

                [self clickRightForJumpH5];
                
            }else
            {
                [self clickLeftCancel];
            }

        }
            break;
        default:
            break;
    }
    
    
   }






/**
 如果当前的navi是HTVideoAVPlayerVC
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForHTVideoAVPlayerVCAndNotification:(NSString*)notiObject
{
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        if ([_extraBusFlag intValue]== 1|| [_extraBusFlag intValue]== 3) {
            //在医视频播放器中/健康百科
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
            }else
            {
                //前台
                
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_HEALTHH5TAG;
                [cusAlert show];
                
            }
            
            
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在医视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                
                //目前是要增加跳转一级界面，二级界面，以及标签界面
                
                [self inBackGroundSelectVC];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_HEALTHPLAYERTAG;
                [cusAlert show];
            }
            
            
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_HEALTHPLAYERTAG;
                [cusAlert show];
            }
            
        }

    }else{
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        //在医视频播放器中/健康百科
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
        }else
        {
            //前台
            
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_HEALTHH5TAG;
            [cusAlert show];
            
        }
        
        
        
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在医视频播放器中/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            
            //目前是要增加跳转一级界面，二级界面，以及标签界面
            
            [self inBackGroundSelectVC];
            
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_HEALTHPLAYERTAG;
            [cusAlert show];
        }
        
        
        
    }
    }

}



-(void)rootNaviForHTVideoAVPlayerVCAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
        [self portraitForOrientation];
        [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
        
        [self inBackGroundSelectVC];
        
    }else
    {
        [self clickLeftCancel];
    }

}


/**
 如果当前的navi是EpgSmallVideoPlayerVC
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForEpgSmallVideoPlayerVCAndNotification:(NSString*)notiObject
{
    //在直播播放器中/健康百科
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
            }else
            {
                //在前台
                
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDH5TAG;
                [cusAlert show];
            }
            
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在直播播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDTAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDTAG;
                [cusAlert show];
            }
            
        }
    }else
    {
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
        }else
        {
            //在前台
            
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_EPGDEMANDH5TAG;
            [cusAlert show];
        }
        
        
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在直播播放器中/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            
            [self inBackGroundSelectVC];
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_EPGDEMANDTAG;
            [cusAlert show];
        }
        
    }
    }
}


-(void)rootNaviForEpgDemandAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self portraitForOrientation];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
        
        [self inBackGroundSelectVC];
        
    }else
    {
        
        [self clickLeftCancel];
    }

    
}

/**
 如果当前的navi是DemandVideoPlayerVC

 
 @param notiObject 过来的通知
 */
-(void)rootNaviForDemandVideoPlayerVCAndNotification:(NSString*)notiObject
{
    //在点播播放器中/健康百科
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        
        if ([_extraBusFlag intValue]== 1|| [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDH5TAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在点播播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDTAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_EPGDEMANDH5TAG;
                [cusAlert show];
            }
            
        }

  
    }else
    {
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
            
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_EPGDEMANDH5TAG;
            [cusAlert show];
        }
        
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在点播播放器中/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            
            [self inBackGroundSelectVC];
            
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_EPGDEMANDTAG;
            [cusAlert show];
        }
        
    }
    }
}

/**
 如果当前的navi是UGCLiveViewController
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForUGCLiveViewControllerAndNotification:(NSString*)notiObject
{
    //在UGC播放器中/健康百科
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        
        if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
            }else
            {
                //在前台
               
            }
            
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在UGC播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
            }else
            {
                
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_UGCLIVEPLAYER_PROGRAMPLAYERTAG;
                [cusAlert show];
            }
            
        }

    }else
    {
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
        }else
        {
           
            
        }
        
        
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在UGC播放器中/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            
            [self inBackGroundSelectVC];
        }else
        {
           
            
        }
        
    }
    }
}

-(void)rootNaviForUGCLiveViewControllerAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self portraitForOrientation];
        
       
        
    }else
    {
        [self clickLeftCancel];
        
    }

}
/**
 如果当前的navi是UGCManagerHomeViewController
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForUGCManagerHomeViewControllerAndNotification:(NSString*)notiObject
{
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        
        if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_UGCALVIEWCONTROLLERH5TAG;
                [cusAlert show];
                
                
                
            }
            
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在UGC播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_UGCALVIEWCONTROLLERPLAYERTAG;
                [cusAlert show];
                
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_UGCALVIEWCONTROLLERPLAYERTAG;
                [cusAlert show];
            }
            
        }

  
    }else{
    
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_UGCALVIEWCONTROLLERH5TAG;
            [cusAlert show];
            
            
            
        }
        
        
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在UGC播放器中/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
            
            [self inBackGroundSelectVC];
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_UGCALVIEWCONTROLLERPLAYERTAG;
            [cusAlert show];
            
        }
        
    }
    }
}

-(void)rootNaviForUGCManagerHomeViewControllerAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
        
        [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        
        
        [self inBackGroundSelectVC];
    }else
    {
        [self clickLeftCancel];
    }

}

/**
 如果当前的navi界面是PubVideoChannelViewController

 @param notiObject notiObject 过来的通知
 */
-(void)rootNaviForPubVideoHomeViewControllerAndNotification:(NSString*)notiObject
{
    //在幽默视频播放器中/健康百科
    
    
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        
        if ([_extraBusFlag intValue]== 1  || [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_PUBHUMOURH5TAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在点播播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_PUBHUMOURTAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_PUBHUMOURTAG;
                [cusAlert show];
            }
            
        }
    }else
    {
        if ([_extraBusFlag intValue]== 1  || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_PAUSEPLAYER" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_PUBHUMOURH5TAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在点播播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectVC];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_PUBHUMOURTAG;
                [cusAlert show];
            }
            
        }
    }
    

}

-(void)rootNaviForPubVideoHomeViewControllerAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
        [self portraitForOrientation];
        [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
        
        [self inBackGroundSelectVC];
    }else
    {
        [self clickLeftCancel];
        
    }

}

/**
 如果当前的navi是UserLoginViewController
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForUserLoginViewControllerAndNotification:(NSString*)notiObject
{
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 3) {
            //在登录界面/健康百科
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                KnowledgeBaseViewController *skipCtr = [[KnowledgeBaseViewController alloc]init];
                skipCtr.urlStr = [UrlPageJumpManeger bPushJumpWithExtraInfo:_extraUrl andExtraId:_extraId];
                skipCtr.existNaviBar = YES;
                [rootnav pushViewController:skipCtr animated:NO];
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_LOGINH5TAG;
                [cusAlert show];
                
            }
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在登录界面/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                
                [self inBackGroundSelectVC];
                
                
            }else
            {
                //在前台
                
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_LOGINTAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_LOGINTAG;
                [cusAlert show];
            }
  
        }
        
    }else
    
    {
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        //在登录界面/健康百科
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            KnowledgeBaseViewController *skipCtr = [[KnowledgeBaseViewController alloc]init];
            skipCtr.urlStr = [UrlPageJumpManeger bPushJumpWithExtraInfo:_extraUrl andExtraId:_extraId];
            skipCtr.existNaviBar = YES;
            [rootnav pushViewController:skipCtr animated:NO];
            
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_LOGINH5TAG;
            [cusAlert show];
            
        }
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在登录界面/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            
            [self inBackGroundSelectVC];
            
            
        }else
        {
            //在前台
            
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_LOGINTAG;
            [cusAlert show];
        }
        
    }
        
    }

}

-(void)rootNaviForUserLoginViewControllerAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [(UINavigationController*)rootnav.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        
        [self inBackGroundSelectVC];
        
        
    }else
    {
        [self clickLeftCancel];
    }
}

/**
 如果当前的navi是剩余的VC
 
 @param notiObject 过来的通知
 */
-(void)rootNaviForOtherViewControllerAndNotification:(NSString*)notiObject
{
    
    if (IS_MOBILEPUSH_NATIVE_HUMOUR) {
        
        if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 3) {
            
            //在普通界面/健康百科
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
                
                [self clickRightForJumpH5];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_COMMONH5TAG;
                [cusAlert show];
                
            }
        }else if ([_extraBusFlag intValue]== 0)
        {
            //在登录界面/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                
                [rootnav popToRootViewControllerAnimated:YES];
                
                
                [self inBackGroundSelectVC];
                
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_COMMONPLAYERTAG;
                [cusAlert show];
            }
            
        }else if ([_extraBusFlag intValue]== 2)
        {
            //在幽默视频播放器中/健康视频
            
            if ([notiObject isEqualToString:@"1"]) {
                //在后台
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
                
                [self inBackGroundSelectPubVideoHomeViewController];
                
            }else
            {
                //在前台
                UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                cusAlert.tag = MSG_COMMONPLAYERTAG;
                [cusAlert show];
            }
            
        }

   
    }else
    {
    
    if ([_extraBusFlag intValue]== 1 || [_extraBusFlag intValue]== 2 || [_extraBusFlag intValue]== 3) {
        
        //在普通界面/健康百科
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            [rootnav.topViewController.navigationController popViewControllerAnimated:YES];
            
            [self clickRightForJumpH5];
            
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_COMMONH5TAG;
            [cusAlert show];
            
        }
    }else if ([_extraBusFlag intValue]== 0)
    {
        //在登录界面/健康视频
        
        if ([notiObject isEqualToString:@"1"]) {
            //在后台
            
            [rootnav popToRootViewControllerAnimated:YES];
            
            
            [self inBackGroundSelectVC];
            
            
        }else
        {
            //在前台
            UIAlertView *cusAlert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"您收到%@,是否继续",_msgBody] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            cusAlert.tag = MSG_COMMONPLAYERTAG;
            [cusAlert show];
        }
        
    }
        
    }
}

-(void)rootNaviForOtherViewControllerAndClickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_STOPPLAYER" object:nil];
        [rootnav popToRootViewControllerAnimated:YES];
        
        [self inBackGroundSelectVC];
        
    }else
    {
        [self clickLeftCancel];
    }

}





-(void)inBackGroundSelectPubVideoHomeViewController
{
    PubVideoHomeViewController *pubHome = [[PubVideoHomeViewController alloc]init];
    
    [rootnav pushViewController:pubHome animated:YES];
    
    [pubHome setPubChannelId:_extraFilterId andPubProgramId:_extraId andTrackId:@"__" andTrackName:[NSString stringWithFormat:@"推送+%@",_extraName] andEnType:@"10"];
    
    
}



-(void)inBackGroundSelectVC
{
    switch ([_extraTypeFlag intValue]) {
        case 0:
        {
            //一级频道
            
            UITabBarController *tabVC = (UITabBarController *)rootnav.topViewController;
            [tabVC setSelectedIndex:healthIndex];
            
            [self clearNotiUserInfo];
        }
            break;
        case 1:
        {
            //二级频道
            
            YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
            yj.categoryID = _extraId;
            yj.name = _msgBody;
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",yj.categoryID];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"推送,%@",yj.name];
            
            yj.TrackName = TrackName;
            
            yj.TrackId = TrackId;
            [rootnav pushViewController:yj animated:NO];
            [self clearNotiUserInfo];
            
        }
            break;
            
        case 2:
        {
            //一般视频节目
            
            HTVideoAVPlayerVC *skipCtr = [[HTVideoAVPlayerVC alloc]init];
            skipCtr.contentID = _extraId;
            skipCtr.titleStr = _extraName;
            NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_extraId,@"itemId",_extraName,@"name",@"10",@"ENtype",@"",@"TrackId",@"",@"TrackName", nil];
            NSMutableArray *recommendAray = [NSMutableArray array];
            NSMutableDictionary *recommendDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_extraId,@"id",_extraName,@"name",nil];
            
            [recommendAray addObject:recommendDic];
            
            skipCtr.recommendArray = recommendAray;
            
            HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
            [skipCtr setPolyModel:healthVodModel];
            
            [rootnav pushViewController:skipCtr animated:NO];
            [self clearNotiUserInfo];
            
        }
            break;
            
        case 3:
        {
            //二级频道标签
            
            YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
            yj.categoryID = _extraFilterId;
            yj.name = _msgBody;
            yj.itemID = _extraId;
            
            //                        yj.selectID = @"895ae5a75a1f11e691ec00163e0018c3";
            //                        yj.itemID = @"057e8d73cf874172a6f86ff1c23bf44e";
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",yj.categoryID];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"推送,%@",yj.name];
            
            yj.TrackName = TrackName;
            
            yj.TrackId = TrackId;
            [rootnav pushViewController:yj animated:NO];
            [self clearNotiUserInfo];
            
        }
            break;
            
        case 4:
        {
            //精选频道
            
            //一级频道
            
            UITabBarController *tabVC = (UITabBarController *)rootnav.topViewController;
            [tabVC setSelectedIndex:healthIndex];
            HealthVodViewController *hVC = (HealthVodViewController *)[tabVC viewControllers][healthIndex];
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",_extraId];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"推送,%@",_msgBody];
            
            [hVC setHealthId:_extraId andTrackId:TrackId andTrackName:TrackName];
            
            
            [self clearNotiUserInfo];
            
        }
            break;
            
        case 5:
        {
            //精选二级频道
            
            YJHealthVideoSecondViewcontrol *yj = [[YJHealthVideoSecondViewcontrol alloc]init];
            yj.categoryID = _extraId;
            yj.name = _msgBody;
            //轨道ID
            NSString *TrackId  = [NSString stringWithFormat:@"0,%@",yj.categoryID];
            
            //轨道名称
            NSString *TrackName = [NSString stringWithFormat:@"推送,%@",yj.name];
            
            yj.TrackName = TrackName;
            
            yj.TrackId = TrackId;
            [rootnav pushViewController:yj animated:NO];
            
            [self clearNotiUserInfo];
            
        }
            break;
            
        case 6:
        {
            //精选节目
            
            //一般视频节目
            
            HTVideoAVPlayerVC *skipCtr = [[HTVideoAVPlayerVC alloc]init];
            skipCtr.contentID = _extraId;
            skipCtr.titleStr = _extraName;
            NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:_extraId,@"itemId",_extraName,@"name",@"10",@"ENtype",@"",@"TrackId",@"",@"TrackName", nil];
            NSMutableArray *recommendAray = [NSMutableArray array];
            NSMutableDictionary *recommendDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:_extraId,@"id",_extraName,@"name",nil];
            
            [recommendAray addObject:recommendDic];
            
            skipCtr.recommendArray = recommendAray;

            HealthVodCollectionModel *healthVodModel = [HealthVodCollectionModel heaalthVodWithDict:paramDic];
            [skipCtr setPolyModel:healthVodModel];
            
            [rootnav pushViewController:skipCtr animated:NO];
            
            [self clearNotiUserInfo];
            
            
            
        }
            break;
            
            
        default:
            break;
    }

}






-(void)clearNotiUserInfo
{
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];

}


-(void)clickLeftCancel
{
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)clickRightForJumpH5
{
     [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    KnowledgeBaseViewController *h5 = [[KnowledgeBaseViewController alloc]init];
    h5.urlStr = [UrlPageJumpManeger bPushJumpWithExtraInfo:_extraUrl andExtraId:_extraId];
    h5.existNaviBar = YES;

    [rootnav pushViewController:h5 animated:NO];
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}


#pragma mark--------------Bpush
- (void)unbindChannelWithCompleteHandler:(BPushCallBack)handler
{
    [BPush unbindChannelWithCompleteHandler:handler];
}

- (void)setTag:(NSString *)tag withCompleteHandler:(BPushCallBack)handler
{
    [BPush setTag:tag withCompleteHandler:handler];
}

- (void)setTags:(NSArray *)tags withCompleteHandler:(BPushCallBack)handler
{
    [BPush setTags:tags withCompleteHandler:handler];
}

- (void)delTag:(NSString *)tag withCompleteHandler:(BPushCallBack)handler
{
    [BPush delTag:tag withCompleteHandler:handler];
}

- (void)delTags:(NSArray *)tags withCompleteHandler:(BPushCallBack)handler
{
    [BPush delTags:tags withCompleteHandler:handler];
}

- (void)listTagsWithCompleteHandler:(BPushCallBack)handler
{
    [BPush listTagsWithCompleteHandler:handler];
}

- (NSString *)getChannelId
{
    return  [BPush getChannelId];
}

- (NSString *)getUserId
{
    return [BPush getUserId];
}

- (NSString *)getAppId
{
    return  [BPush getAppId];
}




#pragma  mark 回调方法

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
     switch (status) {
        case 0:
        {
            if ([type isEqualToString:MSG_BINDGOUP]) {
                
                NSLog(@"YJ_INTRODUCE_NEARBY----%@",result);
                
                if ([[result objectForKey:@"ret"] integerValue]==0) {
                    
                    NSLog(@"用户绑定成功");
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"BindGoupSuccess"];
                    [[NSUserDefaults standardUserDefaults]synchronize];

                }else
                {
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"BindGoupSuccess"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
            }
            
            if ([type isEqualToString:MSG_BINDINGUPDATE]) {
                if ([[result objectForKey:@"ret"] integerValue]==0) {
                    
                    NSLog(@"用户更新信息成功");
                    
                }
            }

            
        }
             break;
         default:
             break;
     }

    
    
}



@end
