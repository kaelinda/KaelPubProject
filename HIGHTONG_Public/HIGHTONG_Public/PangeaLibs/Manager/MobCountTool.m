//
//  MobCountTool.m
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/11/18.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "MobCountTool.h"
#import "UMMobClick/MobClick.h"
@implementation MobCountTool

+ (void)pushInWithPageView:(NSString *)pageView
{
    
    if (MOB_COUNT) {//用
        
        [MobClick beginLogPageView:pageView];
    }else
    {//不用
    
        return;
    }
}

+ (void)popOutWithPageView:(NSString *)pageView
{
    
    if (MOB_COUNT) {//用
               
        [MobClick endLogPageView:pageView];
    }else
    {//不用
        
        return;
    }
}

@end
