//
//  UIViewController+MessageCollect.m
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/6/26.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UIViewController+MessageCollect.h"
#import "Aspects.h"

@implementation UIViewController (MessageCollect)

- (void)UMengViewMessageCollect
{
    NSError *error = nil;
    
    [UIViewController aspect_hookSelector:@selector(viewWillAppear:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo, BOOL animated){
        
        UIViewController *vc = [aspectInfo instance];
//        NSArray *args = [aspectInfo arguments];
        
//        NSLog(@"页面出现名字名字名字\n%@",vc);
//        NSLog(@"页面出现所有的参数\n%lu", (unsigned long)[args count]);
        
        NSString *VCIdenStr = [NSString stringWithUTF8String:object_getClassName(vc)];
        
        NSLog(@"viewWillAppearVC的字符串名字%@",VCIdenStr);
        
        [MobCountTool pushInWithPageView:VCIdenStr];
        
    } error:&error];
    
    [UIViewController aspect_hookSelector:@selector(viewWillDisappear:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo, BOOL animated){
        
        
        UIViewController *vc = [aspectInfo instance];
//        NSArray *args = [aspectInfo arguments];
        
//        NSLog(@"页面消失名字名字名字\n%@",vc);
//        NSLog(@"页面消失所有的参数\n%@", [[args firstObject] class]);
        
        //NSString *VCIdenStr = [NSString stringWithUTF8String:object_getClassName(self)];
        NSString *VCIdenStr = [NSString stringWithUTF8String:object_getClassName(vc)];
        NSLog(@"viewWillDisappearVC的字符串名字%@",VCIdenStr);
        
        [MobCountTool popOutWithPageView:VCIdenStr];
        
    } error:&error];
}

@end
