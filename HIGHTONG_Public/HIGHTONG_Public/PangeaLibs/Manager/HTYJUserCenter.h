//
//  HTYJUserCenter.h
//  HIGHTONG_Public
//
//  Created by 吴伟 on 16/4/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, UGCAuthenticStatus) {
    KAuthenticed,//已认证
    KAuthenticing,//认证中
    KunAuthentic,//未认证
    KAuthenticFailed,//认证失败
};

typedef void(^authCallBack)(UGCAuthenticStatus authStatus, NSDictionary *dic);

typedef void(^HTYJUserCenterRequestManageBlock)();

@interface HTYJUserCenter : NSObject

@property (nonatomic, copy)HTYJUserCenterRequestManageBlock HTYJUserCenterRequestManageBlock;


+ (instancetype)userCenter;


/**
 *  获取鉴权token成功后的相关操作
 *
 *  @param resultDic 获取鉴权token回调的字典
 */
- (void)relativeOperationAfterGetAuthenticTokenSuccessWithResultDic:(NSDictionary *)resultDic;


/**
 *  原手机号登录成功后--保存用户登录信息
 *
 *  @param resultDic 服务器返回数据
 *  @param account   手机号——账号
 *  @param password  密码
 */
+ (void)saveUserLoginProfileWithDic:(NSDictionary *)resultDic andUserAccount:(NSString *)account andUserPassword:(NSString *)password;


/**
 *  保存用户信息
 *
 *  @param resultDic “用户信息”接口返回数据
 */
+ (void)saveUserDetailProfileWithDic:(NSDictionary *)resultDic;


/**
 *  用户退出清除用户信息
 */
+ (void)logoutCleanUserProfile;



/**
 动态登录接口成功后的信息存储方法

 @param requestDic 返回数据字典
 @param phone 手机号
 */
- (void)saveAuthCodeLoginDataWithDic:(NSDictionary *)requestDic andPhoneNum:(NSString *)phone;

//@property (nonatomic,copy) void (^authCallBack)(BOOL isAuth);
//
//- (void)checkDoctorAuthWithCallBack:(void (^)(BOOL isAuth))callBack;


@property (nonatomic, copy)authCallBack authCallBack;

- (void)checkDoctorAuthWithCallBack:(authCallBack)callBack;



@end
