//
//  LocalVideoModel.h
//  HIGHTONG_Public
//
//  Created by lailibo on 2016/12/12.
//  Copyright © 2016年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalVideoModel : NSObject
@property (strong,nonatomic)NSString *lvID;//直播播放器的频道ID
@property (strong,nonatomic)NSString *lvEventID;//直播播放器的eventID
@property (strong,nonatomic)NSString *lvServiceName;//直播播放器的频道名字
@property (strong,nonatomic)NSString *lvChannelImage;//直播播放器的频道的LOGO
@property (strong,nonatomic)NSString *lvEventName;//直播播放器的事件名字
@property (strong,nonatomic)NSString *lvBreakPoint;//播放器的断点(包括直播和点播)
@property (strong,nonatomic)NSString *lvStartTime;//直播播放器的预约的开始时间
@property (strong,nonatomic)NSString *lvFullStartTime;//直播播放器的预约的完成开始时间
@property (strong,nonatomic)NSString *lvEndTime;//直播播放器的预约的结束时间
@property (strong,nonatomic)NSString *lvCONTENTID;//点播播放器的外层contentID
@property (strong,nonatomic)NSString *lvNAME;//点播播放器的外层名字，电影或者电视剧的名字
@property (strong,nonatomic)NSString *lvMultipleType;//点播播放器的Type，来区分电影和电视剧、综艺
@property (strong,nonatomic)NSString *lvname;//点播播放器的副标题
@property (strong,nonatomic)NSString *lvDefinition;//点播播放器的清晰度，暂时先不传吧
@property (strong,nonatomic)NSString *lvLastPlayEpisode;//点播播放器的最后播放的剧集数
@property (strong,nonatomic)NSString *lvLastEpisodeTitle;//点播播放器的最后播放剧集的标题
@property (strong,nonatomic)NSString *lvAmount;//点播播放器的总集数
@property (strong,nonatomic)NSString *lvImageLink;//点播播放器的图片
@property (strong,nonatomic)NSString *lvStyle;//点播播放器的Style


-(void)resetLocalVideoModel;

@end
