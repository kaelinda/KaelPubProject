//
//  KLocalNotificationManager.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/7.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "KLocalNotificationManager.h"

static KLocalNotificationManager *LNManager;

@implementation KLocalNotificationManager

#pragma mark ------ 初始化
+(instancetype)shareLocalNotificationManager{
    if (!LNManager) {
        LNManager = [[KLocalNotificationManager alloc] init];
        //置为0
        //BO注释本地角标
//        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
       
    }
    return LNManager;

}

-(instancetype)init{

    self = [super init];
    if (self) {
        //注册通知 这个通知会把最新的预约列表传过来
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kLocalNotificationAction:) name:@"UPDATE_RESERVELIST_TO_LOCALNOTIFACATION" object:nil];
        
        _localListCenter = [[LocalCollectAndPlayRecorder alloc] init];
        
        [_localListCenter refreshScheduleListWithBlock:^(BOOL isExist, NSArray *arr) {
            
            [self removeAllLocalNotification];//注册本地推送前 删除掉之前所有通知 因为有可能是删除预约后 导致 预约列表更新
            NSArray *orderListArr = [NSArray arrayWithArray:arr];
            _showNotiNum=0;
            self.localNotiArr = [[NSMutableArray alloc] init];
            
            for (int i=0; i<orderListArr.count; i++)
            {
                //显示通知
                NSDictionary *schedulDic = [[NSDictionary alloc] initWithDictionary:[orderListArr objectAtIndex:i]];
                NSDate *nowDate = [NSDate date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *showDate = [dateFormatter dateFromString:[schedulDic objectForKey:@"startTime"]];
                
                if ([showDate timeIntervalSinceDate:nowDate]>0) {
                    
                    [self.localNotiArr addObject:schedulDic];//localNoti
                    [self showLocalNotificationWithDictionary:schedulDic];
                    
                }
            }

        }];
        
        //清空所有本地通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAllLocalNotification) name:@"removeAllLocalNotification" object:nil];
        //用户（非游客）登陆成功后
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getOrderList) name:@"User_Login_Succeed" object:nil];
#if DEBUG
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self checkNoti];
        });
#else
        
#endif

    }
    return self;
}
-(void)didReceiveLocalNotification:(UILocalNotification *)nitofication{
    NSLog(@"接收到本地通知的响应，最好还是在appdelegate里面实现好一些~");

}
#pragma mark ------ 通知方式接收预约节目信息
-(void)kLocalNotificationAction:(NSNotification *) noti{
    
    [self removeAllLocalNotification];//注册本地推送前 删除掉之前所有通知 因为有可能是删除预约后 导致 预约列表更新
    NSArray *orderListArr = [[NSArray alloc] initWithArray:[noti object]];
    _showNotiNum=0;
    self.localNotiArr = [[NSMutableArray alloc] init];//localNoti
    
    for (int i=0; i<orderListArr.count; i++)
    {
        //显示通知
        NSDictionary *schedulDic = [[NSDictionary alloc] initWithDictionary:[orderListArr objectAtIndex:i]];
        NSDate *nowDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *showDate = [dateFormatter dateFromString:[schedulDic objectForKey:@"startTime"]];
        
        if ([showDate timeIntervalSinceDate:nowDate]>0) {
            
            [self.localNotiArr addObject:schedulDic];//localNoti
            [self showLocalNotificationWithDictionary:schedulDic];
            
        }
    }
    
}
#pragma mark ------ 手动loading本地通知
-(void)loadingLocalNotificationWithNotiArray:(NSArray *)notiArray{
    
    [self removeAllLocalNotification];//注册本地推送前 删除掉之前所有通知 因为有可能是删除预约后 导致 预约列表更新
    NSArray *orderListArr = [[NSArray alloc] initWithArray:notiArray];

    _showNotiNum=0;
    self.localNotiArr = [[NSMutableArray alloc] init];//localNoti
    
    for (int i=0; i<orderListArr.count; i++)
    {
        //显示通知
        NSDictionary *schedulDic = [[NSDictionary alloc] initWithDictionary:[orderListArr objectAtIndex:i]];
        NSDate *nowDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *showDate = [dateFormatter dateFromString:[schedulDic objectForKey:@"startTime"]];
        
        if ([showDate timeIntervalSinceDate:nowDate]>0) {
            
            [self.localNotiArr addObject:schedulDic];//localNoti
            [self showLocalNotificationWithDictionary:schedulDic];
            
        }
    }
}


#pragma mark ------ 真正注册本地通知
//zzs预定 提醒
-(void)showLocalNotificationWithDictionary:(NSDictionary *)dic{
    //发送通知
    NSLog(@"开始注册预定消息");
    
    //-------
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];//已经添加本地推送的数组
    if (acount>0)
    {
        // 遍历找到对应nfkey和notificationtag的通知
        for (int i=0; i<acount; i++)
        {
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];
            NSDictionary *userInfo = myUILocalNotification.userInfo;
            NSString *nfkey = [userInfo objectForKey:@"nfkey"];
            
            if ([nfkey isEqualToString:[dic objectForKey:@"eventName"]])
            {
                //不注册该通知
                return;
            }
        }
    }
    
    _showNotiNum++;
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    if (notification!=nil) {

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *showDate = [dateFormatter dateFromString:[dic objectForKey:@"startTime"]];
        //        NSDate *showDate = [dateFormatter dateFromString:@""];
        //        NSDate *showDate = [[NSDate date] dateByAddingTimeInterval:20];
        
        notification.fireDate=showDate;//设置显示时间戳
        notification.repeatInterval=0;//循环次数，kCFCalendarUnitWeekday一周一次
        notification.timeZone=[NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];//[NSTimeZone defaultTimeZone]
        //BO注释本地角标
//        notification.applicationIconBadgeNumber=_showNotiNum; //应用的红色数字
        notification.soundName= UILocalNotificationDefaultSoundName;//声音，可以换成alarm.soundName = @"myMusic.caf"
        //去掉下面2行就不会弹出提示框
        notification.alertBody=[NSString stringWithFormat:@"您预订的节目：%@就要播出了，届时请观看！",[dic objectForKey:@"eventName"]];//提示信息 弹出提示框
        notification.userInfo = dic;
        
        notification.alertAction = @"马上观看";  //提示框按钮
        //notification.hasAction = NO; //是否显示额外的按钮，为no时alertAction消失
        // NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
        //notification.userInfo = infoDict; //添加额外的信息
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        NSLog(@"%@节目预定 本地通知注册成功  这是第%ld个通知   显示时间：%@",[dic objectForKey:@"eventName"],(long)_showNotiNum,showDate);
        
    }
    
}

//移除所有本地通知
-(void)removeAllLocalNotification{
    //-------
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];//已经添加本地推送的数组
    if (acount>0)
    {
        // 遍历找到对应nfkey和notificationtag的通知
        for (int i=0; i<acount; i++)
        {
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];
//            NSDictionary *userInfo = myUILocalNotification.userInfo;
//            NSString *nfkey = [userInfo objectForKey:@"nfkey"];
            [[UIApplication sharedApplication] cancelLocalNotification:myUILocalNotification];
        }
    }
    _showNotiNum=0;
    //BO注释本地角标
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
}


-(void)checkNoti{
#if DEBUG

#else
    return;

#endif
    NSLog(@"通知来了------本地通知检测（假数据）...");
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"621",@"serviceID",@"14:52",@"startTime",@"15:40",@"endTime", nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"testLocalNoti" object:dic];
    
    return;
}


#pragma  mark ------ 请求回调
-(void)getOrderList{
//    HTRequest *hrequest = [[HTRequest alloc] initWithDelegate:self];
//    [hrequest EventRevserveWithTimeou:5];

    NSMutableArray *reserveArray = [NSMutableArray arrayWithArray:[_localListCenter getScheduledList]];
    //重新手动注册
    [self loadingLocalNotificationWithNotiArray:reserveArray];
}

-(void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type{

    if ([type isEqualToString:EPG_EVENTRESERVELIST]) {
        
        if (status==0 && [result objectForKey:@"ret"] && [[result objectForKey:@"ret"] integerValue]==0) {
            NSMutableArray *reserveArray = [NSMutableArray arrayWithArray:[result objectForKey:@"eventlist"]];
            //重新手动注册
            [self loadingLocalNotificationWithNotiArray:reserveArray];
        }
        
    }

}

@end
