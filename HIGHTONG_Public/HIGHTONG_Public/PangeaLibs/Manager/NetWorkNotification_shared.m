//
//  NetWorkNotification_shared.m
//  NetWork
//
//  Created by testteam on 15/11/28.
//  Copyright © 2015年 cap. All rights reserved.
//

#import "NetWorkNotification_shared.h"
#import "GETBaseInfo.h"
#import "AppDelegate.h"
#import "PortalManger.h"

static NetWorkNotification_shared *shared = nil;

@interface NetWorkNotification_shared ()<HTRequestDelegate>
{
    BOOL _canShowToast;
    
    NSString *_ssidNameStr;
}
@end

@implementation NetWorkNotification_shared

+ (instancetype)shardNetworkNotification
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc]init];

    });
    return shared;
}
-(instancetype)init{
    self = [super init];

    if (self) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //这里是1.0f后 你要做的事情
            [self startNotifi];
        });
    }
    return self;
}


- (void)startNotifi
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    [GLobalRealReachability startNotifier];
    
    
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));

    if (status == RealStatusNotReachable)
    {
        _ssidNameStr = @"当前无可用网络";
    }

    if (status == RealStatusViaWiFi)
    {
        _ssidNameStr = [NetWorkNotification_shared currentWifiSSID];
    }

    if (status == RealStatusViaWWAN)
    {
        _ssidNameStr = @"您正在使用移动网络";
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushPlayerVC) name:@"CAN_SHOW_TOAST" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popPlayerVC) name:@"CAN_HIDDEN_TOAST" object:nil];
//     NetWorkNotification_shared *shared =  [NetWorkNotification_shared shardNetworkNotification];
//    shared.reach = [Reachability reachabilityWithHostname:@"https://www.baidu.com"];
    NSLog(@"==%@",shared);
    
//    shared.reach = [Reachability reachabilityForInternetConnection];
//    [shared.reach startNotifier];
    
    
}
-(void)pushPlayerVC{

    _canShowToast = YES;
}
-(void)popPlayerVC{
   
    _canShowToast = NO;
}

-(void)getNetWorkingTypeAndNameWith:(NetWorkingTypeBlock)kNetWorkingBlock{
    
    [GLobalRealReachability startNotifier];
    
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));

    if (status == RealStatusNotReachable)
    {
        _ssidNameStr = @"当前无可用网络";
    }
    
    if (status == RealStatusViaWiFi)
    {
        _ssidNameStr = [NetWorkNotification_shared currentWifiSSID];
    }
    
    if (status == RealStatusViaWWAN)
    {
        _ssidNameStr = @"您正在使用移动网络";
    }
    

    kNetWorkingBlock(status,[_ssidNameStr copy]);
    
}

+ (NSString *)currentWifiSSID {

    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}


- (void)networkChanged:(NSNotification *)notification
{
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    ReachabilityStatus previousStatus = [reachability previousReachabilityStatus];
    NSLog(@"networkChanged, currentStatus:%@, previousStatus:%@", @(status), @(previousStatus));
    
    switch (status) {
        case RealStatusNotReachable:
        {
            if (_canShowToast) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGED" object:@"无网络"];
            }
        }
            break;
        case RealStatusUnknown:{
            if (_canShowToast) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGED" object:@"未知网络"];
            }

        }break;
        case RealStatusViaWiFi:{
            if (_canShowToast) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGED" object:@"wifi网络"];
            }
            
            [self netChangeRequest];
        
        }break;
        case RealStatusViaWWAN:{
            if (_canShowToast) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGED" object:@"蜂窝网络"];
            }
            
            [self netChangeRequest];

        }break;
            
        default:
            break;
    }
    
    GETBaseInfo *baseInfo = [[GETBaseInfo alloc]init];
    
    [SAIInformationManager ETHForIP:[baseInfo getIPAddressType] andAW:[GETBaseInfo getNetworkType] andTTL:@"" andBW:@""];
    
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    if ([type isEqualToString:@"projectList"]) {
        NSLog(@"网络切换   更新portal 的版本列表  projectList--->>%@",result);
        [[NSNotificationCenter defaultCenter]postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];//获取身份token通知
        
        //切换网络后，重新请求运营商信息
        NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
        if (operatorCode.length <= 0) {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorLogo"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorName"];
        }
        else
        {
            
            HTRequest *cnrequest = [[HTRequest alloc] initWithDelegate:self];
            
            [cnrequest CNOperatorOperatorInfo];
            
        }
    }
    
    
    if ([type isEqualToString:CN_OPERATOR_OPERATORINFO]) {
        
        NSLog(@"CN_OPERATOR_OPERATORINFO----%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue]==0 && [result objectForKey:@"ret"]) {
            NSDictionary *operatorInfo = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"operatorInfo"]];
            NSString *operatorLogo = [operatorInfo objectForKey:@"operatorLogo"];
            NSString *operatorName = [operatorInfo objectForKey:@"operatorName"];
            [[NSUserDefaults standardUserDefaults] setObject:operatorLogo forKey:@"operatorLogo"];
            [[NSUserDefaults standardUserDefaults] setObject:operatorName forKey:@"operatorName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)netChangeRequest
{
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    if(IS_MERGEPORJECTLIST){
        
        PortalManger *portalManager = [PortalManger sharedPortalManager];
        
        [portalManager retryRequestProjectListWith:kAPPNetEnvType_ALL];
        
        portalManager.callNetWorkAlert = ^(NSString *status) {
            
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];//获取身份token通知
            
            //切换网络后，重新请求运营商信息
            NSString *operatorCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"operatorCode"];
            if (operatorCode.length <= 0) {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorLogo"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorName"];
            }
            else
            {
                
                HTRequest *cnrequest = [[HTRequest alloc] initWithDelegate:self];
                
                [cnrequest CNOperatorOperatorInfo];
                
            }
            
        };
        
    }else
    {
        
        [request getHightongPORTALProjectListWithTimeout:5];
        
    }
    
}

@end
