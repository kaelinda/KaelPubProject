//
//  LocalCollectAndPlayRecorder.m
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "LocalCollectAndPlayRecorder.h"
#import "DataPlist.h"
#import "UserToolClass.h"

@interface LocalCollectAndPlayRecorder ()

@end

@implementation LocalCollectAndPlayRecorder

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (void)refreshScheduleListWithBlock:(LocalListCenterBlock)listBlock
{
    NSArray *arr = [NSArray arrayWithArray:[self getScheduledList]];
    
    _scheduleListBlock = listBlock;
    
    if (arr.count) {
        
        if (_scheduleListBlock) {
            _scheduleListBlock(YES, arr);
        }
    }else{
        if (_scheduleListBlock) {
            _scheduleListBlock(NO, nil);
        }
    }
}
- (void)refreshEPGCollectListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock
{
    NSArray *arr = [NSArray arrayWithArray:[self getLiveFavoriteListWithUserType:isUser]];
    _EPGCollectListBlock = listBlock;
    [self refreshListWithBlock:_EPGCollectListBlock andListArr:arr];
}
- (void)refreshVODCollectListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock
{
    NSArray *arr = [NSArray arrayWithArray:[self getVODFavoriteListWithUserType:isUser]];
    _VODCollectListBlock = listBlock;
    [self refreshListWithBlock:_VODCollectListBlock andListArr:arr];
}
- (void)refreshHealthyCollListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock
{
    NSArray *arr = [NSArray arrayWithArray:[self getHealthyCollectionRecordWithUserType:isUser]];
    _healthyCollListBlock = listBlock;
    [self refreshListWithBlock:_healthyCollListBlock andListArr:arr];
}
- (void)refreshListWithBlock:(ListValueBlock)listBlock andListArr:(NSArray *)arr
{
    if (arr.count) {
        if (listBlock) {
            listBlock(YES);
        }
    }else{
        if (listBlock) {
            listBlock(NO);
        }
    }
}

#pragma mark ----------SCHEDULE LIST BEGIN----------
//是否已预约
- (BOOL)checkTheScheduledListwithID:(NSString *)eventID
{
    
    if (IsLogin) {
        NSMutableArray *localArr = [DataPlist ReadToFileArr:USER_SCHEDULE_LIST_PLIST];
        
        for (NSDictionary *video in localArr) {
            
            if ([[video objectForKey:@"eventID"] isEqualToString:eventID]||[[video objectForKey:@"eventId"] isEqualToString:eventID]) {
                return YES;
            }
        }
        return NO;

    }else
    {
        return NO;
    }
}
////添加预约节目 (属性取不到的时候，可以传空传进来的)
//- (void)addScheduledListWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andKey:(NSString *)key
//{
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
//    [paramDic setObject:serviceID.length>0?serviceID:@"" forKey:@"serviceID"];
//    [paramDic setObject:serviceName.length>0?serviceName:@"" forKey:@"serviceName"];
//    [paramDic setObject:eventID.length>0?eventID:@"" forKey:@"eventID"];
//    [paramDic setObject:eventName.length>0?eventName:@"" forKey:@"eventName"];
//    [paramDic setObject:startTime.length>0?startTime:@"" forKey:@"startTime"];
//    [paramDic setObject:endTime.length>0?endTime:@"" forKey:@"endTime"];
//    [paramDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
//    [paramDic setObject:levelImageLink.length>0?levelImageLink:@"" forKey:@"levelImageLink"];
//   
////    [self addRecordWithDic:paramDic withPlist:USER_SCHEDULE_LIST_PLIST withKeyID:key];
//}
//播放器界面添加预约节目
- (void)addScheduledListWithModel:(LocalVideoModel *)model
{
    LocalVideoModel *modelTemp = [[LocalVideoModel alloc] init];
    modelTemp = model;
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:modelTemp.lvID.length?modelTemp.lvID:@"" forKey:@"serviceID"];
    [paramDic setObject:modelTemp.lvServiceName.length?modelTemp.lvServiceName:@"" forKey:@"serviceName"];
    [paramDic setObject:modelTemp.lvEventID.length?modelTemp.lvEventID:@"" forKey:@"eventID"];
    [paramDic setObject:modelTemp.lvEventName.length?modelTemp.lvEventName:@"" forKey:@"eventName"];
    
//    NSLog(@"预约节目完整的开始时间%@,%lu",modelTemp.lvFullStartTime,(unsigned long)modelTemp.lvFullStartTime.length);
    
    NSString *startStr = [NSString stringWithFormat:@"%@%@",modelTemp.lvFullStartTime,@":00"];
    NSString *yearStr = [startStr substringToIndex:11];
//    NSLog(@"节目开始完整时间:%@,截取的年月日：%@",startStr,yearStr);
    
    NSString *endStr = [NSString stringWithFormat:@"%@%@%@",yearStr,modelTemp.lvEndTime,@":00"];
//    NSLog(@"开始时间：%@，结束时间：%@",startStr,endStr);
    
    [paramDic setObject:startStr.length?startStr:@"" forKey:@"startTime"];
    [paramDic setObject:endStr.length?endStr:@"" forKey:@"endTime"];
    [paramDic setObject:modelTemp.lvChannelImage.length?modelTemp.lvChannelImage:@"" forKey:@"imageLink"];
    
    [self addScheduleItemDic:paramDic withPlist:USER_SCHEDULE_LIST_PLIST];
}
- (void)addScheduleItemDic:(NSDictionary *)addDic withPlist:(NSString *)plistName
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    
    if (recordArr.count) {

        if (recordArr.count<100) {
            
            [recordArr insertObject:addDic atIndex:0];
        }else{
            [recordArr removeLastObject];
            [recordArr insertObject:addDic atIndex:0];
        }
        
        [self sortScheduleListWithScheduleArr:recordArr withPlist:plistName];
    }else{
        
        [recordArr addObject:addDic];
//        [self addSpecialKeyInScheduleListWithArr:recordArr];
        [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
    }
}
//添加完排序
- (void)sortScheduleListWithScheduleArr:(NSMutableArray *)scheduleArr withPlist:(NSString *)plistName
{
    NSLog(@"开始无序的数组：%@",scheduleArr);
    for (int i = 0; i<scheduleArr.count; i++) {
        for (int j = i+1; j<scheduleArr.count; j++) {
            
            NSString *startTimeStrI = [[scheduleArr objectAtIndex:i] objectForKey:@"startTime"];
            NSString *startTimeStrJ = [[scheduleArr objectAtIndex:j] objectForKey:@"startTime"];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            
            NSDate *dateI = [formatter dateFromString:startTimeStrI];
            NSDate *dateJ = [formatter dateFromString:startTimeStrJ];
            
            if ([dateJ isEarlierThanOrEqualTo:dateI]) {
                
                [scheduleArr exchangeObjectAtIndex:i withObjectAtIndex:j];
            }
        }
    }
    
    NSLog(@"排序好的数组：%@",scheduleArr);
    
//    [self addSpecialKeyInScheduleListWithArr:scheduleArr];
    [DataPlist WriteToPlistArray:scheduleArr ToFile:plistName];
}
//搜索界面添加预约节目
- (void)addScheduleListWithDic:(NSDictionary *)addDic
{    
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:addDic];
    NSString *eventIDstr = [tempDic objectForKey:@"eventId"];
    
    [tempDic removeObjectForKey:@"eventId"];
    [tempDic setObject:eventIDstr forKey:@"eventID"];
    
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:USER_SCHEDULE_LIST_PLIST]];
    
    if (recordArr.count) {
        
        if (recordArr.count<100) {
            
            [recordArr insertObject:tempDic atIndex:0];
        }else{
            [recordArr removeLastObject];
            [recordArr insertObject:tempDic atIndex:0];
        }
        
        [self sortScheduleListWithScheduleArr:recordArr withPlist:USER_SCHEDULE_LIST_PLIST];
    }else{
        
        [recordArr addObject:tempDic];

        [DataPlist WriteToPlistArray:recordArr ToFile:USER_SCHEDULE_LIST_PLIST];
    }
}
- (void)addSpecialKeyInScheduleListWithArr:(NSMutableArray *)arr
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSMutableArray *resultArr = [NSMutableArray arrayWithCapacity:0];

    [arr enumerateObjectsUsingBlock:^(id tempObj, NSUInteger idx, BOOL * stop) {
        
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithDictionary:tempObj];
        
        NSDate *startDate = [formatter dateFromString:[obj objectForKey:@"startTime"]];
        
        NSDate *endDate = [formatter dateFromString:[obj objectForKey:@"endTime"]];
        
        NSString *nowTimeStr = [formatter stringFromDate:[NSDate date]];
        
        NSDate *nowDate = [formatter dateFromString:nowTimeStr];
        
        
        if ([nowDate compare:startDate] < 0) {//当前时间小于开始时间======预约
            
            [obj setObject:@"2" forKey:@"playType"];//playType 0播放, 1回看, 2预约, 3播放失效, 4回看失效
            [obj setObject:@"0" forKey:@"ifCanPlay"];//ifCanPlay 1 可以播放  0 不可以播放
        }else if (([nowDate compare:startDate] >= 0) && ([nowDate compare:endDate] <= 0)){//当前时间在开始、结束时间之间======正在播放
            
            [obj setObject:@"0" forKey:@"playType"];
            [obj setObject:@"1" forKey:@"ifCanPlay"];
            
            
        }else{//当前时间大于结束时间======回看
            [obj setObject:@"1" forKey:@"playType"];
            [obj setObject:@"1" forKey:@"ifCanPlay"];
        }
        
        [resultArr addObject:obj];
    }];
    
    NSLog(@"*********%@++++++++%@",arr,resultArr);

    [DataPlist WriteToPlistArray:resultArr ToFile:USER_SCHEDULE_LIST_PLIST];
}

//删除预约节目(播放器界面、搜索的单个删除)
- (void)singleDeletScheduledListWith:(NSString *)eventID andKey:(NSString *)key
{
    [self singleDeleteCollectListWithDeleteID:eventID withPlistName:USER_SCHEDULE_LIST_PLIST withKeyName:key];
}
//删除预约节目（我的部分的多选删除）--------为空的时候全部取消应该处理
- (void)multiDeleteScheduledListWith:(NSArray *)eventIDArr andKey:(NSString *)key
{
    [self deleteRecordWithDeleteArr:eventIDArr withPList:USER_SCHEDULE_LIST_PLIST withKeyID:key];
}
//清空本地预约列表
- (void)cleanScheduleList
{
    NSMutableArray *recordArr = [DataPlist ReadToFileArr:USER_SCHEDULE_LIST_PLIST];
    [recordArr removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr ToFile:USER_SCHEDULE_LIST_PLIST];
}
//获取预约列表
- (NSArray *)getScheduledList
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    dataArr = [DataPlist ReadToFileArr:USER_SCHEDULE_LIST_PLIST];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSMutableArray *resultArr = [NSMutableArray arrayWithCapacity:0];
    
    [dataArr enumerateObjectsUsingBlock:^(id tempObj, NSUInteger idx, BOOL * stop) {
        
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithDictionary:tempObj];
        
        NSDate *startDate = [formatter dateFromString:[obj objectForKey:@"startTime"]];
        
        NSDate *endDate = [formatter dateFromString:[obj objectForKey:@"endTime"]];
        
        NSString *nowTimeStr = [formatter stringFromDate:[NSDate date]];
        
        NSDate *nowDate = [formatter dateFromString:nowTimeStr];
        
        
        if ([nowDate compare:startDate] < 0) {//当前时间小于开始时间======预约
            
            [obj setObject:@"2" forKey:@"playType"];//playType 0播放, 1回看, 2预约, 3播放失效, 4回看失效
            [obj setObject:@"0" forKey:@"ifCanPlay"];//ifCanPlay 1 可以播放  0 不可以播放
        }else if (([nowDate compare:startDate] >= 0) && ([nowDate compare:endDate] <= 0)){//当前时间在开始、结束时间之间======正在播放
            
            [obj setObject:@"0" forKey:@"playType"];
            [obj setObject:@"1" forKey:@"ifCanPlay"];
            
            
        }else{//当前时间大于结束时间======回看
            [obj setObject:@"1" forKey:@"playType"];
            [obj setObject:@"1" forKey:@"ifCanPlay"];
        }
        
        [resultArr addObject:obj];
    }];
    
    NSLog(@"*********%@++++++++%@",dataArr,resultArr);
    
    
    return resultArr;
}
#pragma mark ----------SCHEDULE LIST ENDING----------



#pragma mark ----------EPG COLLECT BEGIN----------
/** 获取直播频道收藏列表 */
- (NSArray *)getLiveFavoriteListWithUserType:(BOOL)user
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    if (user) {
        dataArr = [DataPlist ReadToFileArr:LIVEFAVORITEPLIST];
    }else{
        dataArr = [DataPlist ReadToFileArr:GUESTLIVEFAVORITEPLIST];
    }
    return dataArr;
}
/** 增加直播收藏 */
- (void)addLiveFavoriteWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {//用户
        
        [self addCollectItemWithDic:addDic withPlist:LIVEFAVORITEPLIST withKeyID:key];
    }else{//游客
        [self addCollectItemWithDic:addDic withPlist:GUESTLIVEFAVORITEPLIST withKeyID:key];
    }
}
- (void)addCollectItemWithDic:(NSDictionary *)addDic withPlist:(NSString *)plistName withKeyID:(NSString *)keyName
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    
    if (recordArr.count) {
        
        if (recordArr.count<100) {
            
            [recordArr insertObject:addDic atIndex:0];
        }else{
            [recordArr removeLastObject];
            [recordArr insertObject:addDic atIndex:0];
        }
    }else{
        
        [recordArr addObject:addDic];
    }
    [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
}
/** 增加直播收藏 */
- (void)addLiveFavoriteWithSeq:(NSString *)seq andID:(NSString *)Iid andName:(NSString *)name andIntro:(NSString *)intro andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:seq.length>0?seq:@"" forKey:@"seq"];
    [paramDic setObject:Iid.length>0?Iid:@"" forKey:@"id"];
    [paramDic setObject:name.length>0?name:@"" forKey:@"name"];
    [paramDic setObject:intro.length>0?intro:@"" forKey:@"intro"];
    [paramDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [paramDic setObject:levelImageLink.length>0?levelImageLink:@"" forKey:@"levelImageLink"];

    if (user) {//用户
        [self addCollectItemWithDic:paramDic withPlist:LIVEFAVORITEPLIST withKeyID:key];
    }else{//游客
        [self addCollectItemWithDic:paramDic withPlist:GUESTLIVEFAVORITEPLIST withKeyID:key];
    }
}

- (void)addLiveFavoriteWithModel:(LocalVideoModel *)model andUserType:(BOOL)user
{
    LocalVideoModel *modelTemp = [[LocalVideoModel alloc] init];
    modelTemp = model;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:modelTemp.lvID.length?modelTemp.lvID:@"" forKey:@"id"];
    [paramDic setObject:modelTemp.lvServiceName.length?modelTemp.lvServiceName:@"" forKey:@"name"];
    [paramDic setObject:modelTemp.lvChannelImage.length?modelTemp.lvChannelImage:@"" forKey:@"imageLink"];
    
    if (user) {//用户
        [self addCollectItemWithDic:paramDic withPlist:LIVEFAVORITEPLIST withKeyID:@"id"];
    }else{//游客
        [self addCollectItemWithDic:paramDic withPlist:GUESTLIVEFAVORITEPLIST withKeyID:@"id"];
    }
}
/** 删除直播收藏 (单删，播放器界面使用) */
- (void)singleDeleteLiveFavoriteWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:LIVEFAVORITEPLIST withKeyName:key];
    }else{
        
        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:GUESTLIVEFAVORITEPLIST withKeyName:key];
    }
}

- (void)singleDeleteLiveFavoriteWithDeleteID:(NSString *)deleteID andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self singleDeleteCollectListWithDeleteID:deleteID withPlistName:LIVEFAVORITEPLIST withKeyName:key];

    }else{
    
        [self singleDeleteCollectListWithDeleteID:deleteID withPlistName:GUESTLIVEFAVORITEPLIST withKeyName:key];
    }
}

/** 删除直播频道收藏 (多选删除，我的部分使用)*/
- (void)multiDeleteLiveFavoriteWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        [self deleteRecordWithDeleteArr:deleteArr withPList:LIVEFAVORITEPLIST withKeyID:key];
    }else{
        [self deleteRecordWithDeleteArr:deleteArr withPList:GUESTLIVEFAVORITEPLIST withKeyID:key];
    }
}
/** 清空直播频道收藏列表*/
- (void)cleanLiveFavoriteChannelListWithUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:LIVEFAVORITEPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:LIVEFAVORITEPLIST];
    }else{
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:GUESTLIVEFAVORITEPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:GUESTLIVEFAVORITEPLIST];
    }
}
////判断直播节目是否被收藏过
//- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID
//{
//    NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:GUESTLIVEFAVORITEPLIST];
//    
//    for (NSDictionary *video in guestLocalArr) {
//        if ([[video objectForKey:@"id"] isEqualToString:serviceID]) {
//            return YES;
//        }
//    }
//    return NO;
//}
//判断直播节目是否被收藏过
- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID andUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:LIVEFAVORITEPLIST];
        
        for (NSDictionary *video in guestLocalArr) {
            if ([[video objectForKey:@"id"] isEqualToString:serviceID]) {
                return YES;
            }
        }
        return NO;
    }else{
        NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:GUESTLIVEFAVORITEPLIST];
        
        for (NSDictionary *video in guestLocalArr) {
            if ([[video objectForKey:@"id"] isEqualToString:serviceID]) {
                return YES;
            }
        }
        return NO;
    }
}
#pragma mark ----------EPG COLLECT ENDING----------



#pragma mark ----------VOD COLLECY BEGIN----------
/** 获取点播收藏记录列表 */
- (NSArray *)getVODFavoriteListWithUserType:(BOOL)user
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    if (user) {
        dataArr = [DataPlist ReadToFileArr:VODFAVORITEPLIST];
    }else{
        dataArr = [DataPlist ReadToFileArr:GUESTVODFAVORITEPLIST];
    }
    return dataArr;
}
/** 增加点播收藏 */
- (void)addVODFavoriteWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {//用户
        
        [self addCollectItemWithDic:addDic withPlist:VODFAVORITEPLIST withKeyID:key];
    }else{//游客
        
        [self addCollectItemWithDic:addDic withPlist:GUESTVODFAVORITEPLIST withKeyID:key];
    }
}
/** 增加点播收藏 */
- (void)addVODFavoriteWithSeq:(NSString *)seq andContentID:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andFlagPlayOuter:(NSString *)flagPlayOuter andDefinition:(NSString *)definition andType:(NSString *)type andIsOffShelves:(NSString *)isOffShelves andLastEpisode:(NSString *)lastEpisode andLastEpisodeTitle:(NSString *)lastEpisodeTitle andAmount:(NSString *)amount andImageLink:(NSString *)imageLink andStyle:(NSString *)style andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableDictionary *pamaDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [pamaDic setObject:seq.length>0?seq:@"" forKey:@"seq"];
    [pamaDic setObject:contentID.length>0?contentID:@"" forKey:@"contentID"];
    [pamaDic setObject:name.length>0?name:@"" forKey:@"name"];
    [pamaDic setObject:subTitle.length>0?subTitle:@"" forKey:@"subTitle"];
    [pamaDic setObject:flagPlayOuter.length>0?flagPlayOuter:@"" forKey:@"flagPlayOuter"];
    [pamaDic setObject:definition.length>0?definition:@"" forKey:@"definition"];
    [pamaDic setObject:type.length>0?type:@"" forKey:@"type"];
    [pamaDic setObject:isOffShelves.length>0?isOffShelves:@"" forKey:@"isOffShelves"];
    [pamaDic setObject:lastEpisode.length>0?lastEpisode:@"" forKey:@"lastEpisode"];
    [pamaDic setObject:lastEpisodeTitle.length>0?lastEpisodeTitle:@"" forKey:@"lastEpisodeTitle"];
    [pamaDic setObject:amount.length>0?amount:@"" forKey:@"amount"];
    [pamaDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [pamaDic setObject:style.length>0?style:@"" forKey:@"style"];
    
    if (user) {
        [self addCollectItemWithDic:pamaDic withPlist:VODFAVORITEPLIST withKeyID:key];
    }else{
        [self addCollectItemWithDic:pamaDic withPlist:GUESTVODFAVORITEPLIST withKeyID:key];
    }
}

- (void)addVODFavoriteWithModel:(LocalVideoModel *)model andUserType:(BOOL)user
{
    LocalVideoModel *tempModel = [[LocalVideoModel alloc] init];
    tempModel = model;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:tempModel.lvCONTENTID.length?tempModel.lvCONTENTID:@"" forKey:@"contentID"];
    [paramDic setObject:tempModel.lvNAME.length?tempModel.lvNAME:@"" forKey:@"name"];
    [paramDic setObject:tempModel.lvMultipleType.length?tempModel.lvMultipleType:@"" forKey:@"type"];
    [paramDic setObject:tempModel.lvDefinition.length?tempModel.lvDefinition:@"" forKey:@"definition"];
    [paramDic setObject:tempModel.lvLastEpisodeTitle.length?tempModel.lvLastEpisodeTitle:@"" forKey:@"lastEpisodeTitle"];
    [paramDic setObject:tempModel.lvImageLink.length?tempModel.lvImageLink:@"" forKey:@"imageLink"];
    [paramDic setObject:tempModel.lvStyle.length?tempModel.lvStyle:@"" forKey:@"style"];
    [paramDic setObject:tempModel.lvAmount.length?tempModel.lvAmount:@"" forKey:@"amount"];
    
    if (user) {
        [self addCollectItemWithDic:paramDic withPlist:VODFAVORITEPLIST withKeyID:@"contentID"];
    }else{
        [self addCollectItemWithDic:paramDic withPlist:GUESTVODFAVORITEPLIST withKeyID:@"contentID"];
    }
}
/** 删除点播收藏记录 (单删，播放器界面使用) */
- (void)singleDeleteVODFavoriteWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:VODFAVORITEPLIST withKeyName:key];
    }else{
        
        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:GUESTVODFAVORITEPLIST withKeyName:key];
    }
}

- (void)singleDeleteVODFavoriteWithDeleteID:(NSString *)deleteID andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self singleDeleteCollectListWithDeleteID:deleteID withPlistName:VODFAVORITEPLIST withKeyName:key];
    }else{

        [self singleDeleteCollectListWithDeleteID:deleteID withPlistName:GUESTVODFAVORITEPLIST withKeyName:key];
    }
}

/** 删除点播频道收藏 (多选删除，我的部分使用) */
- (void)multiDeleteVODFavoriteWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        [self deleteRecordWithDeleteArr:deleteArr withPList:VODFAVORITEPLIST withKeyID:key];
    }else{
        [self deleteRecordWithDeleteArr:deleteArr withPList:GUESTVODFAVORITEPLIST withKeyID:key];
    }
}
/** 清空点播收藏列表*/
- (void)cleanVODFavoriteListWithUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:VODFAVORITEPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:VODFAVORITEPLIST];
    }else{
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:GUESTVODFAVORITEPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:GUESTVODFAVORITEPLIST];
    }
}
//判断点播节目是否被收藏过
//- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID
//{
//    NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:GUESTVODFAVORITEPLIST];
//    
//    for (NSDictionary *video in guestLocalArr) {
//        if ([[video objectForKey:@"contentID"] isEqualToString:contentID]) {
//            return YES;
//        }
//    }
//    return NO;
//}
- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID andUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:VODFAVORITEPLIST];
        
        for (NSDictionary *video in guestLocalArr) {
            if ([[video objectForKey:@"contentID"] isEqualToString:contentID]) {
                return YES;
            }
        }
        return NO;
    }else{
        NSMutableArray *guestLocalArr = [DataPlist ReadToFileArr:GUESTVODFAVORITEPLIST];
        
        for (NSDictionary *video in guestLocalArr) {
            if ([[video objectForKey:@"contentID"] isEqualToString:contentID]) {
                return YES;
            }
        }
        return NO;
    }
}
#pragma mark ----------VOD COLLECT ENDING----------




#pragma mark ----------VOD RECORDER BEGIN----------
///** 增加点播播放记录 */
- (void)addVODPlayRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        [self addRecordWithDic:addDic withPlist:PLAYRECORDERPLIST withKeyID:key];
    }else{
        [self addRecordWithDic:addDic withPlist:GUESTPLAYRECORDERPLIST withKeyID:key];
    }
}
///** 增加点播播放记录 */
- (void)addVODPlayRecorderWithSeq:(NSString *)seq andContentID:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andFlagPlayOuter:(NSString *)flagPlayOuter andDefinition:(NSString *)definition andImageLink:(NSString *)imageLink andBreakPoint:(NSString *)breakPoint andIsOffShelves:(NSString *)isOffShelves andDuration:(NSString *)duration andPlayTime:(NSString *)playTime andStyle:(NSString *)style andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [paramDic setObject:seq.length>0?seq:@"" forKey:@"seq"];
    [paramDic setObject:contentID.length>0?contentID:@"" forKey:@"contentID"];
    [paramDic setObject:name.length>0?name:@"" forKey:@"name"];
    [paramDic setObject:subTitle.length>0?subTitle:@"" forKey:@"subTitle"];
    [paramDic setObject:flagPlayOuter.length>0?flagPlayOuter:@"" forKey:@"flagPlayOuter"];
    [paramDic setObject:definition.length>0?definition:@"" forKey:@"definition"];
    [paramDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [paramDic setObject:breakPoint.length>0?breakPoint:@"" forKey:@"breakPoint"];
    [paramDic setObject:isOffShelves.length>0?isOffShelves:@"" forKey:@"isOffShelves"];
    [paramDic setObject:duration.length>0?duration:@"" forKey:@"duration"];
    [paramDic setObject:playTime.length>0?playTime:@"" forKey:@"playTime"];
    [paramDic setObject:style.length>0?style:@"" forKey:@"style"];

    if (user) {
        [self addRecordWithDic:paramDic withPlist:PLAYRECORDERPLIST withKeyID:key];
    }else{
        [self addRecordWithDic:paramDic withPlist:GUESTPLAYRECORDERPLIST withKeyID:key];
    }
}

- (void)addVODPlayRecordWithModel:(LocalVideoModel *)model andUserType:(BOOL)user
{
    LocalVideoModel *mdl = [[LocalVideoModel alloc] init];
    mdl = model;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:mdl.lvCONTENTID.length?mdl.lvCONTENTID:@"" forKey:@"contentID"];
    [paramDic setObject:mdl.lvNAME.length?mdl.lvNAME:@"" forKey:@"name"];
    [paramDic setObject:mdl.lvImageLink.length?mdl.lvImageLink:@"" forKey:@"imageLink"];
    [paramDic setObject:mdl.lvStyle.length?mdl.lvStyle:@"" forKey:@"style"];
    [paramDic setObject:mdl.lvBreakPoint.length?mdl.lvBreakPoint:@"" forKey:@"breakPoint"];
    
    if (user) {
        
        [self addItemSpecialForRecorderListWithDic:paramDic withPlist:PLAYRECORDERPLIST withKeyID:@"contentID"];
    }else{
        
        [self addItemSpecialForRecorderListWithDic:paramDic withPlist:GUESTPLAYRECORDERPLIST withKeyID:@"contentID"];
    }
}

/** 删除点播播放记录 */
- (NSMutableArray *)deleteVODPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (user) {
        
        arr = [self mulDeleteRecorderWithDeleteArr:deleteArr withPList:PLAYRECORDERPLIST withKeyID:key];
    }else{
        
        arr = [self mulDeleteRecorderWithDeleteArr:deleteArr withPList:GUESTPLAYRECORDERPLIST withKeyID:key];
    }
    return arr;
}
/** 清空点播播放记录*/
- (void)cleanVODPlayRecordWithUserType:(BOOL)user
{
    if (user) {
        
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:PLAYRECORDERPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:PLAYRECORDERPLIST];
    }else{
        
        NSMutableArray *recordArr = [DataPlist ReadToFileArr:GUESTPLAYRECORDERPLIST];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:GUESTPLAYRECORDERPLIST];
    }
}
/** 获取点播播放记录*/
- (NSArray *)GetVODPlayRecordWithUserType:(BOOL)user
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    if (user) {
        
        dataArr = [DataPlist ReadToFileArr:PLAYRECORDERPLIST];
    }else{
        
        dataArr = [DataPlist ReadToFileArr:GUESTPLAYRECORDERPLIST];
    }
    return dataArr;
}
#pragma mark ----------VOD RECORDER ENDING----------



#pragma mark ----------EVENT RECORDER LIST BEGIN----------

//根据eventID查询断点
- (NSString *)checkTheReWatchListWithEventID:(NSString *)eventID andUserType:(BOOL)user
{
    if (user) {
        
        NSMutableArray *localArr = [DataPlist ReadToFileArr:REWATCHPLAYRECORDERPLIST];
        
        for (NSDictionary *dic in localArr) {
            
            if ([[dic objectForKey:@"eventID"] isEqualToString:eventID]) {
                
                return [dic objectForKey:@"breakPoint"];
            }
        }
        return @"0";
    }else{
    
        NSMutableArray *localArr = [DataPlist ReadToFileArr:GUESTREWATCHPLAYRECORDERPLIST];
        
        for (NSDictionary *dic in localArr) {
            
            if ([[dic objectForKey:@"eventID"] isEqualToString:eventID]) {
                
                return [dic objectForKey:@"breakPoint"];
            }
        }
        return @"0";
    }
}
//根据eventID查询相应对象字典
- (NSDictionary *)getAReWatchItemWithEventID:(NSString *)eventID andUserType:(BOOL)user
{
    if (user) {
        
        NSMutableArray *localArr = [DataPlist ReadToFileArr:REWATCHPLAYRECORDERPLIST];
        
        for (NSDictionary *dic in localArr){
        
            if ([[dic objectForKey:@"eventID"] isEqualToString:eventID]) {
                
                return dic;
            }
        }
        return [NSDictionary dictionary];
    }else{
    
        NSMutableArray *localArr = [DataPlist ReadToFileArr:GUESTREWATCHPLAYRECORDERPLIST];
        
        for (NSDictionary *dic in localArr){
            if ([[dic objectForKey:@"eventID"] isEqualToString:eventID]) {
                
                return dic;
            }
        }
        return [NSDictionary dictionary];
    }
}
/** 增加回看播放记录 */
- (void)addReWatchPlayRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self addRecordWithDic:addDic withPlist:REWATCHPLAYRECORDERPLIST withKeyID:key];
    }else{
        [self addRecordWithDic:addDic withPlist:GUESTREWATCHPLAYRECORDERPLIST withKeyID:key];
    }
}
/** 增加回看播放记录 */
- (void)addReWatchPlayRecordWithServiceID:(NSString *)serviceID andServiceName:(NSString *)serviceName andEventID:(NSString *)eventID andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andEventName:(NSString *)eventName andBreakPoint:(NSString *)breakPoint andPlayTime:(NSString *)playTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:serviceID.length>0?serviceID:@"" forKey:@"serviceID"];
    [paramDic setObject:serviceName.length>0?serviceName:@"" forKey:@"serviceName"];
    [paramDic setObject:eventID.length>0?eventID:@"" forKey:@"eventID"];
    [paramDic setObject:startTime.length>0?startTime:@"" forKey:@"startTime"];
    [paramDic setObject:endTime.length>0?endTime:@"" forKey:@"endTime"];
    [paramDic setObject:eventName.length>0?eventName:@"" forKey:@"eventName"];
    [paramDic setObject:breakPoint.length>0?breakPoint:@"" forKey:@"breakPoint"];
    [paramDic setObject:playTime.length>0?playTime:@"" forKey:@"playTime"];
    [paramDic setObject:imageLink.length>0?imageLink:@"" forKey:@"imageLink"];
    [paramDic setObject:levelImageLink.length>0?levelImageLink:@"" forKey:@"levelImageLink"];
    
    if (user) {
        
        [self addRecordWithDic:paramDic withPlist:REWATCHPLAYRECORDERPLIST withKeyID:key];
    }else{
        [self addRecordWithDic:paramDic withPlist:GUESTREWATCHPLAYRECORDERPLIST withKeyID:key];
    }
}

- (void)addReWatchPlayRecordWithModel:(LocalVideoModel *)model andUserTye:(BOOL)user
{
    LocalVideoModel *mdl = [[LocalVideoModel alloc] init];
    mdl = model;
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:mdl.lvID.length?mdl.lvID:@"" forKey:@"serviceID"];
    [paramDic setObject:mdl.lvServiceName.length?mdl.lvServiceName:@"" forKey:@"serviceName"];
    [paramDic setObject:mdl.lvEventID.length?mdl.lvEventID:@"" forKey:@"eventID"];
    [paramDic setObject:mdl.lvEventName.length?mdl.lvEventName:@"" forKey:@"eventName"];
    [paramDic setObject:mdl.lvBreakPoint.length?mdl.lvBreakPoint:@"" forKey:@"breakPoint"];
    [paramDic setObject:mdl.lvChannelImage.length?mdl.lvChannelImage:@"" forKey:@"imageLink"];

    if (user) {
        
        [self addItemSpecialForRecorderListWithDic:paramDic withPlist:REWATCHPLAYRECORDERPLIST withKeyID:@"eventID"];
    }else{

        [self addItemSpecialForRecorderListWithDic:paramDic withPlist:GUESTREWATCHPLAYRECORDERPLIST withKeyID:@"eventID"];
    }
}
/** 删除回看播放记录 */
- (NSMutableArray *)deleteReWatchPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (user) {
        
        arr = [self mulDeleteRecorderWithDeleteArr:deleteArr withPList:REWATCHPLAYRECORDERPLIST withKeyID:key];
    }else{
        
        arr = [self mulDeleteRecorderWithDeleteArr:deleteArr withPList:GUESTREWATCHPLAYRECORDERPLIST withKeyID:key];
    }
    return arr;
}
/** 清空回看播放记录*/
- (void)cleanReWatchPlayRecordWithUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:REWATCHPLAYRECORDERPLIST]];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:REWATCHPLAYRECORDERPLIST];
    }else{
        
        NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:GUESTREWATCHPLAYRECORDERPLIST]];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:GUESTREWATCHPLAYRECORDERPLIST];
    }
}
/** 获取回看播放记录*/
- (NSArray *)GetReWatchPlayRecordWithUserType:(BOOL)user
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    if (user) {
        
        dataArr = [DataPlist ReadToFileArr:REWATCHPLAYRECORDERPLIST];
    }else{
        
        dataArr = [DataPlist ReadToFileArr:GUESTREWATCHPLAYRECORDERPLIST];
    }
    return dataArr;
}

- (NSInteger)getVODListLengthWithUserType:(BOOL)user
{
    NSInteger listLength = 0;
    if (user) {
        
        listLength = [self getRecordListLengthWithPlist:PLAYRECORDERPLIST];
    }else{
        
        listLength = [self getRecordListLengthWithPlist:GUESTPLAYRECORDERPLIST];
    }
    
    return listLength;
}

- (NSInteger)getRewatchListLengthWithUserType:(BOOL)user
{
    NSInteger listLength = 0;
    if (user) {
        
        listLength = [self getRecordListLengthWithPlist:REWATCHPLAYRECORDERPLIST];
    }else{
        
        listLength = [self getRecordListLengthWithPlist:GUESTREWATCHPLAYRECORDERPLIST];
    }
    
    return listLength;

}

#pragma mark ----------EVENT RECORDER LIST ENDING----------



#pragma mark ----------HEALTHY COLLECT BEGIN----------
//增加健康视频收藏记录
- (void)addHealthyCollectionRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
//        [self addRecordWithDic:addDic withPlist:USER_HEALTHY_COLLECTION_PLIST withKeyID:key];
        [self addCollectItemWithDic:addDic withPlist:USER_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }else{
        NSLog(@"添加游客的健康视频收藏成功——————%@",addDic);
//        [self addRecordWithDic:addDic withPlist:GUEST_HEALTHY_COLLECTION_PLIST withKeyID:key];
        [self addCollectItemWithDic:addDic withPlist:GUEST_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }
}
//增加健康视频收藏记录
- (void)addHealthyCollectionRecordWithID:(NSString *)hID andName:(NSString *)name andReleaseYear:(NSString *)releaseYear andReleaseDate:(NSString *)releaseDate andDescription:(NSString *)description andDuration:(NSString *)duration andImageUrl:(NSString *)imageUrl andUserType:(BOOL)user andKey:(NSString *)key
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:hID.length>0?hID:@"" forKey:@"id"];//programId
    [paramDic setObject:name.length>0?name:@"" forKey:@"name"];
    [paramDic setObject:releaseYear.length>0?releaseYear:@"" forKey:@"releaseYear"];
    [paramDic setObject:releaseDate.length>0?releaseDate:@"" forKey:@"releaseDate"];
    [paramDic setObject:description.length>0?description:@"" forKey:@"description"];
    [paramDic setObject:duration.length>0?duration:@"" forKey:@"duration"];
    [paramDic setObject:imageUrl.length>0?imageUrl:@"" forKey:@"imageUrl"];

    if (user) {
//        [self addRecordWithDic:paramDic withPlist:USER_HEALTHY_COLLECTION_PLIST withKeyID:key];
        [self addCollectItemWithDic:paramDic withPlist:USER_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }else{
        NSLog(@"添加游客的健康视频收藏成功——————%@",paramDic);
//        [self addRecordWithDic:paramDic withPlist:GUEST_HEALTHY_COLLECTION_PLIST withKeyID:key];
        [self addCollectItemWithDic:paramDic withPlist:GUEST_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }
}

//删除健康视频收藏记录(播放器界面的点击删除)
- (void)deleteHealthyCollectionRecordWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        
        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:USER_HEALTHY_COLLECTION_PLIST withKeyName:key];
    }else{

        [self singleDeleteCollectListWithDeleteDic:deleteDic withPlistName:GUEST_HEALTHY_COLLECTION_PLIST withKeyName:key];//programId
    }
}
//删除健康视频收藏记录(用户中心多选删除)
- (void)deleteHealthyCollectionRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key
{
    if (user) {
        [self deleteRecordWithDeleteArr:deleteArr withPList:USER_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }else{
        [self deleteRecordWithDeleteArr:deleteArr withPList:GUEST_HEALTHY_COLLECTION_PLIST withKeyID:key];
    }
}
//清空健康视频收藏记录
- (void)cleanHealthyCollectionRecordWithUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:USER_HEALTHY_COLLECTION_PLIST]];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:USER_HEALTHY_COLLECTION_PLIST];

    }else{
        NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:GUEST_HEALTHY_COLLECTION_PLIST]];
        [recordArr removeAllObjects];
        [DataPlist WriteToPlistArray:recordArr ToFile:GUEST_HEALTHY_COLLECTION_PLIST];
    }
}
//获取健康视频收藏记录
- (NSArray *)getHealthyCollectionRecordWithUserType:(BOOL)user
{
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:0];
    if (user) {
        dataArr = [DataPlist ReadToFileArr:USER_HEALTHY_COLLECTION_PLIST];
    }else{
        dataArr = [DataPlist ReadToFileArr:GUEST_HEALTHY_COLLECTION_PLIST];
    }
    return dataArr;
}
//健康视频是否被收藏过
- (BOOL)checkHealthyCollectionListWithProgramID:(NSString *)programID andUserType:(BOOL)user
{
    if (user) {
        NSMutableArray *dataArr = [DataPlist ReadToFileArr:USER_HEALTHY_COLLECTION_PLIST];
        for (NSDictionary *video in dataArr) {
            if ([[video objectForKey:@"programId"] isEqualToString:programID]) {
                return YES;
            }
        }
        return NO;
    }else{
        NSMutableArray *dataArr = [DataPlist ReadToFileArr:GUEST_HEALTHY_COLLECTION_PLIST];
        for (NSDictionary *video in dataArr) {
            if ([[video objectForKey:@"programId"] isEqualToString:programID]) {
                return YES;
            }
        }
        return NO;
    }
}
#pragma mark ----------HEALTHY COLLECT ENDING----------



//#pragma mark ----------SEARCH BEGIN----------
///** 增加搜索记录 */
//+ (void)addSearchRecorderWithDic:(NSDictionary *)redorderDic andUserType:(BOOL)user
//{
//    if (user) {
//        NSMutableArray *recorderArr = [DataPlist ReadToFileArr:SEARCHRECORDERPLIST];
//        recorderArr = [NSMutableArray arrayWithArray:recorderArr];
//        BOOL isInsert = NO;
//        for (NSDictionary *dic in recorderArr) {
//            if ([dic isEqualToDictionary:redorderDic]) {
//                [recorderArr removeObject:dic];
//                [recorderArr insertObject:redorderDic atIndex:0];
//                isInsert = YES;
//                 [DataPlist WriteToPlistArray:recorderArr ToFile:SEARCHRECORDERPLIST];
//                return;
//            }
//        }
//        
//        if (!isInsert) {
//            if (recorderArr.count<10) {//少于10个数据的话
//                
//                [recorderArr insertObject:redorderDic atIndex:0];
//            }else//多于10个或等于10个
//            {
//                
//                [recorderArr removeLastObject];
//                [recorderArr insertObject:redorderDic atIndex:0];
//            }
//
//        }
//        
//        [DataPlist WriteToPlistArray:recorderArr ToFile:SEARCHRECORDERPLIST];
//    }else{
//        NSMutableArray *recorderArr = [DataPlist ReadToFileArr:GUESTSEARCHRECORDERPLIST];
//        recorderArr = [NSMutableArray arrayWithArray:recorderArr];
//        BOOL isInsert = NO;
//        for (NSDictionary *dic in recorderArr) {
//            if ([dic isEqualToDictionary:redorderDic]) {
//                [recorderArr removeObject:dic];
//                [recorderArr insertObject:redorderDic atIndex:0];
//                isInsert = YES;
//                [DataPlist WriteToPlistArray:recorderArr ToFile:GUESTSEARCHRECORDERPLIST];
//                return;
//            }
//        }
//        
//        if (!isInsert) {
//            if (recorderArr.count<10) {//少于10个数据的话
//                
//                [recorderArr insertObject:redorderDic atIndex:0];
//            }else//多于10个或等于10个
//            {
//                
//                [recorderArr removeLastObject];
//                [recorderArr insertObject:redorderDic atIndex:0];
//            }
//            
//        }
//        
//        [DataPlist WriteToPlistArray:recorderArr ToFile:GUESTSEARCHRECORDERPLIST];
//    }
//}
///** 删除搜索记录 */
//+ (void)deleteSearchRecorderWithsearchName:(NSString *)searchName andUserType:(BOOL)user
//{
//    if (user) {
//        NSMutableArray *recorderArr = [DataPlist ReadToFileArr:SEARCHRECORDERPLIST];
//        
//        for (int i = 0; i<recorderArr.count; i++) {
//            if ([searchName isEqualToString:[recorderArr[i] objectForKey:@"searchName"]]) {
//                [recorderArr removeObject:recorderArr[i]];
//            }
//        }
//        
//        [DataPlist WriteToPlistArray:recorderArr ToFile:SEARCHRECORDERPLIST];
//    }else{
//        NSMutableArray *recorderArr = [DataPlist ReadToFileArr:GUESTSEARCHRECORDERPLIST];
//        
//        for (int i = 0; i<recorderArr.count; i++) {
//            if ([searchName isEqualToString:[recorderArr[i] objectForKey:@"searchName"]]) {
//                [recorderArr removeObject:recorderArr[i]];
//            }
//        }
//        
//        [DataPlist WriteToPlistArray:recorderArr ToFile:GUESTSEARCHRECORDERPLIST];
//        
//    }
//}
///** 获取搜索记录 */
//+ (NSArray*)SearchRecorderWithandUserType:(BOOL)user
//{
//    NSMutableArray *recorderArr;
//    if (user) {
//       recorderArr  = [DataPlist ReadToFileArr:SEARCHRECORDERPLIST];
//        
//    }else{
//       recorderArr = [DataPlist ReadToFileArr:GUESTSEARCHRECORDERPLIST];
//       
//    }
//    return recorderArr;
//}
//#pragma mark ----------SEARCH ENDING----------

#pragma mark ----------NEW SEARCH BEGIN----------
/** 增加搜索记录 */
+ (void)addSearchRecorderWithDic:(NSDictionary *)redorderDic
{

    NSMutableArray *recorderArr = [DataPlist ReadToFileArr:MOBILESEARCHRECORDERPLIST];
    recorderArr = [NSMutableArray arrayWithArray:recorderArr];
    BOOL isInsert = NO;
    for (NSDictionary *dic in recorderArr) {
        if ([dic isEqualToDictionary:redorderDic]) {
            [recorderArr removeObject:dic];
            [recorderArr insertObject:redorderDic atIndex:0];
            isInsert = YES;
            [DataPlist WriteToPlistArray:recorderArr ToFile:MOBILESEARCHRECORDERPLIST];
            return;
        }
    }
        
    if (!isInsert) {
        if (recorderArr.count<10) {//少于10个数据的话
                
        [recorderArr insertObject:redorderDic atIndex:0];
        }else//多于10个或等于10个
        {
                
        [recorderArr removeLastObject];
        [recorderArr insertObject:redorderDic atIndex:0];
        }
            
    }
        
    [DataPlist WriteToPlistArray:recorderArr ToFile:MOBILESEARCHRECORDERPLIST];
    
}
/** 删除搜索记录 */
+ (void)deleteSearchRecorderWithsearchName:(NSString *)searchName
{
    
    NSMutableArray *recorderArr = [DataPlist ReadToFileArr:MOBILESEARCHRECORDERPLIST];
        
    for (int i = 0; i<recorderArr.count; i++) {
        if ([searchName isEqualToString:[recorderArr[i] objectForKey:@"searchName"]]) {
            [recorderArr removeObject:recorderArr[i]];
        }
    }
        
    [DataPlist WriteToPlistArray:recorderArr ToFile:MOBILESEARCHRECORDERPLIST];
 
}
/** 获取搜索记录 */
+ (NSArray*)SearchRecorder
{
    NSMutableArray *recorderArr;

    recorderArr  = [DataPlist ReadToFileArr:MOBILESEARCHRECORDERPLIST];
        

    return recorderArr;
}
#pragma mark ----------NEW SEARCH ENDING----------



#pragma mark---------通用方法
/** 通用添加记录方法 只记录一百条 */
- (void)addRecordWithDic:(NSDictionary *)addDic withPlist:(NSString *)plistName withKeyID:(NSString *)keyID
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    
    if (recordArr.count) {
        BOOL isExist = NO;
        for (NSDictionary *dic in recordArr) {
            if ([[dic objectForKey:keyID] isEqualToString:[addDic objectForKey:keyID]]) {
                [recordArr removeObject:dic];
                [recordArr insertObject:addDic atIndex:0];
                
                isExist = YES;
                
                [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
                return;
            }
        }
        if (!isExist) {
            if (recordArr.count<100) {//只记录一百条
                
                [recordArr insertObject:addDic atIndex:0];
            }else{
                
                [recordArr removeLastObject];
                [recordArr insertObject:addDic atIndex:0];
            }
        }
    }else{
        
        [recordArr addObject:addDic];
    }
    
    [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
}


/**
 获取原始数据 playList的长度

 @param plistName 文件名
 @return 所有playList数组长度
 */
- (NSInteger)getRecordListLengthWithPlist:(NSString *)plistName
{
    NSInteger listLength = 0;
    
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    NSLog(@"最初数组：%@",recordArr);

    for (NSDictionary *dic in recordArr) {
        
        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[dic objectForKey:@"playList"]];
        
        if (tempArr.count>0) {
            listLength += tempArr.count;
        }
    }

    return listLength;
}

/** 播放记录页面专用的添加方法 */
- (void)addItemSpecialForRecorderListWithDic:(NSDictionary *)addDic withPlist:(NSString *)plistName withKeyID:(NSString *)keyID
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    NSLog(@"最初数组：%@",recordArr);
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary *dic in recordArr) {
        
        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[dic objectForKey:@"playList"]];
        
        for (NSDictionary *tempDic in tempArr) {
            
            [arr addObject:tempDic];
        }
    }
    
    NSLog(@"第一层数组：%@",arr);
    
    if (arr.count) {
        BOOL isExist = NO;
        for (NSDictionary *dic in arr) {
            if ([[dic objectForKey:keyID] isEqualToString:[addDic objectForKey:keyID]]) {
            
                [arr removeObject:dic];
                [arr insertObject:addDic atIndex:0];
                
                isExist = YES;
                
                NSLog(@"第三层（1）：%@",arr);
                
                [DataPlist WriteToPlistArray:[UserToolClass dependIfTheSameWeekCompareWithTodayDateGroupWithArray:arr] ToFile:plistName];
                
                return;

            }
        }
        if (!isExist) {
            if (arr.count<100) {//只记录一百条
                
                [arr insertObject:addDic atIndex:0];
            }else{
                
                [arr removeLastObject];
                [arr insertObject:addDic atIndex:0];
            }
        }
    }else{
        [arr addObject:addDic];
    }
    
    NSLog(@"第三层（2）：%@",arr);
    
    [DataPlist WriteToPlistArray:[UserToolClass dependIfTheSameWeekCompareWithTodayDateGroupWithArray:arr] ToFile:plistName];
}
/** 播放记录页面专用的多选删除方法 */
- (NSMutableArray *)mulDeleteRecorderWithDeleteArr:(NSArray *)deleteArr withPList:(NSString *)plistName withKeyID:(NSString *)keyID
{
    NSMutableArray *originalArr = [DataPlist ReadToFileArr:plistName];
    
    __block NSMutableArray *itemArr = [NSMutableArray array];
    
    for (NSDictionary *delDic in deleteArr ) {
        
        [originalArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
            
            itemArr = [obj objectForKey:@"playList"];
            
            for (int i = 0; i<itemArr.count; i++) {
                
                NSMutableDictionary *dic = itemArr[i];
                
                if ([[delDic objectForKey:keyID] isEqualToString:[dic objectForKey:keyID]]) {
                    
                    *stop = YES;
                    
                    if (*stop == YES) {
                        
                        [[[originalArr objectAtIndex:idx] objectForKey:@"playList"] removeObjectAtIndex:i];
                        
                        if ([[[originalArr objectAtIndex:idx] objectForKey:@"playList"] count] == 0) {
                            
                            [originalArr removeObjectAtIndex:idx];
                        }
                    }
                }
            }
            
        }];
    }
    
    [DataPlist WriteToPlistArray:originalArr ToFile:plistName];
    
    return originalArr;
}
/** 通用多选删除记录方法 */
- (void)deleteRecordWithDeleteArr:(NSArray *)deleteArr withPList:(NSString *)plistName withKeyID:(NSString *)keyID
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    for (NSDictionary *deleDic in deleteArr) {
        
        [recordArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
            
            NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:obj];
            if ([[tempDic objectForKey:keyID] isEqualToString:[deleDic objectForKey:keyID]]) {
                
                *stop = YES;
            }
            
            if (*stop) {
                [recordArr removeObject:obj];
            }
        }];
    }
    
    [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
}

//通用单个删方法
- (void)singleDeleteCollectListWithDeleteDic:(NSDictionary *)deleteDic withPlistName:(NSString *)plistName withKeyName:(NSString *)keyName
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    [recordArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
        
        NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:obj];
        if ([[tempDic objectForKey:keyName] isEqualToString:[deleteDic objectForKey:keyName]]) {
            
            *stop = YES;
        }
        
        if (*stop) {
            [recordArr removeObject:obj];
        }
    }];
    
    [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
}

- (void)singleDeleteCollectListWithDeleteID:(NSString *)deleteID withPlistName:(NSString *)plistName withKeyName:(NSString *)keyName
{
    NSMutableArray *recordArr = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:plistName]];
    [recordArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
        
        NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:obj];
        if ([[tempDic objectForKey:keyName] isEqualToString:deleteID]) {
            
            *stop = YES;
        }
        
        if (*stop) {
            [recordArr removeObject:obj];
        }
    }];
    
    [DataPlist WriteToPlistArray:recordArr ToFile:plistName];
}

//清空本地用户所有列表   目前没有预约列表
- (void)cleanUserAllList
{
    //直播频道收藏（喜爱频道）
    NSMutableArray *recordArr1 = [DataPlist ReadToFileArr:LIVEFAVORITEPLIST];
    [recordArr1 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr1 ToFile:LIVEFAVORITEPLIST];

    //点播收藏
    NSMutableArray *recordArr2 = [DataPlist ReadToFileArr:VODFAVORITEPLIST];
    [recordArr2 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr2 ToFile:VODFAVORITEPLIST];
    
    //点播播放记录
    NSMutableArray *recordArr3 = [DataPlist ReadToFileArr:PLAYRECORDERPLIST];
    [recordArr3 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr3 ToFile:PLAYRECORDERPLIST];
    
    //回看播放记录
    NSMutableArray *recordArr4 = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:REWATCHPLAYRECORDERPLIST]];
    [recordArr4 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr4 ToFile:REWATCHPLAYRECORDERPLIST];
    
    //健康视频收藏记录
    NSMutableArray *recordArr5 = [NSMutableArray arrayWithArray:[DataPlist ReadToFileArr:USER_HEALTHY_COLLECTION_PLIST]];
    [recordArr5 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr5 ToFile:USER_HEALTHY_COLLECTION_PLIST];
    
    //预约列表
    NSMutableArray *recordArr6 = [DataPlist ReadToFileArr:USER_SCHEDULE_LIST_PLIST];
    [recordArr6 removeAllObjects];
    [DataPlist WriteToPlistArray:recordArr6 ToFile:USER_SCHEDULE_LIST_PLIST];
}


//usercenter 请求返回列表数据存入对应的PLIST文件
- (void)writeListArrInPlistWithPlistName:(NSString *)plistName andListArr:(NSArray *)listArr
{
    [DataPlist WriteToPlistArray:[NSMutableArray arrayWithArray:listArr] ToFile:plistName];
}




@end
