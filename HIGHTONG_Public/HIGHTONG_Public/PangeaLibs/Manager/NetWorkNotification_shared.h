//
//  NetWorkNotification_shared.h
//  NetWork
//
//  Created by testteam on 15/11/28.
//  Copyright © 2015年 cap. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealReachability.h"
#import <SystemConfiguration/CaptiveNetwork.h>

typedef void(^NetWorkingTypeBlock)(ReachabilityStatus status,NSString *msg);

@interface NetWorkNotification_shared : NSObject

@property (nonatomic,assign) BOOL canShowToast;

+ (instancetype)shardNetworkNotification;

- (void)startNotifi;

- (void)getNetWorkingTypeAndNameWith:(NetWorkingTypeBlock)kNetWorkingBlock;


@end
