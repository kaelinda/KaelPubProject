//
//  LocalCollectAndPlayRecorder.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/22.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalVideoModel.h"

typedef void(^ListValueBlock)(BOOL isExist);

typedef void(^LocalListCenterBlock)(BOOL isExist,NSArray *arr);

@interface LocalCollectAndPlayRecorder : NSObject

//@property (nonatomic, copy)ListValueBlock scheduleListBlock;//用户预约列表回调
@property (nonatomic, copy)LocalListCenterBlock scheduleListBlock;//用户预约列表回调

//@property (nonatomic, copy)ListValueBlock userEPGCollectListBlock;//用户频道收藏列表回调
//@property (nonatomic, copy)ListValueBlock guestEPGCollectListBlock;//游客频道收藏列表回调
//@property (nonatomic, copy)ListValueBlock userVODCollectListBlock;//用户点播收藏列表回调
//@property (nonatomic, copy)ListValueBlock guestVODCollectListBlock;//游客点播收藏列表回调
//@property (nonatomic, copy)ListValueBlock userHealthyCollListBlock;//用户健康视频收藏列表回调
//@property (nonatomic, copy)ListValueBlock guestHealthyCollListBlock;//游客健康视频收藏列表回调
@property (nonatomic, copy)ListValueBlock EPGCollectListBlock;//频道收藏列表回调
@property (nonatomic, copy)ListValueBlock VODCollectListBlock;//点播收藏列表回调
@property (nonatomic, copy)ListValueBlock healthyCollListBlock;//健康视频收藏列表回调


- (void)refreshScheduleListWithBlock:(LocalListCenterBlock)listBlock;
- (void)refreshEPGCollectListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock;
- (void)refreshVODCollectListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock;
- (void)refreshHealthyCollListWithUserType:(BOOL)isUser andBlock:(ListValueBlock)listBlock;



#pragma mark --------------预约列表部分

/**
 是否预约

 @param eventID 节目ID
 @return BOOL
 */
- (BOOL)checkTheScheduledListwithID:(NSString *)eventID;
/**
 添加预约节目 (key取不到的时候，可以传空传进来的)
 
 @param serviceID 频道ID
 @param serviceName 频道名称
 @param eventID 节目ID
 @param eventName 节目名称
 @param startTime 节目开始时间
 @param endTime 节目结束时间
 @param imageLink 图片链接
 @param levelImageLink 横版图片链接
 @param key 对比是否已存在的key
 */
- (void)addScheduledListWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andKey:(NSString *)key;
/**
 添加预约节目 (model形式---播放器界面使用)
 
 @param model  属性model
 */
- (void)addScheduledListWithModel:(LocalVideoModel *)model;
/**
 添加预约节目 (字典的形式---搜索界面使用)

 @param addDic  属性字典
 @param key 对比是否已存在的key
 */
- (void)addScheduleListWithDic:(NSDictionary *)addDic;
/**
 删除预约节目(播放器界面的单个删除)
 
 @param eventID 预约节目的eventID
 @param key 对比是否已存在的key
 */
- (void)singleDeletScheduledListWith:(NSString *)eventID andKey:(NSString *)key;

/**
 删除预约节目（我的部分的多选删除）--------为空的时候全部取消应该处理

 @param eventIDArr 节目ID数组
 @param key 对比是否已存在的key
 */
- (void)multiDeleteScheduledListWith:(NSArray *)eventIDArr andKey:(NSString *)key;

/**
 清空本地预约列表
 */
- (void)cleanScheduleList;
/**
 获取预约列表

 @return 已预约列表
 */
- (NSArray *)getScheduledList;




#pragma mark --------------直播频道收藏列表部分
/** 获取直播频道收藏列表 */
- (NSArray *)getLiveFavoriteListWithUserType:(BOOL)user;
/**
 增加收藏直播频道记录

 @param addDic 添加的字典
 @param user 是否是用户
 @param key 对比是否已存在的key
 */
- (void)addLiveFavoriteWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key;
/**
 增加收藏直播频道记录

 @param seq 排序号（Integer）
 @param Iid 频道编号（Integer）
 @param name 频道名称（string）
 @param intro 频道介绍（string）
 @param imageLink 频道图标（string）注：terminal_type=5时不用返回
 @param levelImageLink 横条频道图标（string）注：terminal_type=5时不用返回
 @param user 用户类型（用户/游客，必传）
 @param key 对比是否已存在的key
 */
- (void)addLiveFavoriteWithSeq:(NSString *)seq andID:(NSString *)Iid andName:(NSString *)name andIntro:(NSString *)intro andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andUserType:(BOOL)user andKey:(NSString *)key;

- (void)addLiveFavoriteWithModel:(LocalVideoModel *)model andUserType:(BOOL)user;

/** 删除直播频道收藏 (单删，播放器界面使用)*/
// @param key 对比是否已存在的key
- (void)singleDeleteLiveFavoriteWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key;
- (void)singleDeleteLiveFavoriteWithDeleteID:(NSString *)deleteID andUserType:(BOOL)user andKey:(NSString *)key;
/** 删除直播频道收藏 (多选删除，我的部分使用)*/
- (void)multiDeleteLiveFavoriteWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
/** 清空直播频道收藏列表*/
- (void)cleanLiveFavoriteChannelListWithUserType:(BOOL)user;
//判断直播节目是否被收藏过
//- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID;
- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID andUserType:(BOOL)user;



#pragma mark --------------点播收藏列表部分
/** 获取点播收藏记录列表 */
- (NSArray *)getVODFavoriteListWithUserType:(BOOL)user;
/** 增加点播收藏记录 */
- (void)addVODFavoriteWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key;
/**
 增加点播收藏记录

 @param seq 序号（Integer 终端显示时按此排序）
 @param contentID 视频编号（String，不可为空）
 @param name 视频名称（String，不可为空）
 @param subTitle 副标题（String，可为空）
 @param flagPlayOuter 外网播放标识(Integer ,可为空) 0：可播放 1：不可播放(打标签)
 @param definition 清晰度 （Integer ,可为空）0：标清 1：高清 2:4K
 @param type 显示样式(String)
 @param isOffShelves 是否已下架(Boolean，不可为空)
 @param lastEpisode 更新到的集数(String，可为空) 电视剧独有
 @param lastEpisodeTitle 更新剧集标题(String，可为空)电视剧独有
 @param amount 总集数（Integer，可为空） 常规电视剧独有
 @param imageLink 海报链接（String）
 @param style 显示样式(String)
 @param user 用户类型（用户/游客，必传）
 @param key 对比是否已存在的key
 */
- (void)addVODFavoriteWithSeq:(NSString *)seq andContentID:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andFlagPlayOuter:(NSString *)flagPlayOuter andDefinition:(NSString *)definition andType:(NSString *)type andIsOffShelves:(NSString *)isOffShelves andLastEpisode:(NSString *)lastEpisode andLastEpisodeTitle:(NSString *)lastEpisodeTitle andAmount:(NSString *)amount andImageLink:(NSString *)imageLink andStyle:(NSString *)style andUserType:(BOOL)user andKey:(NSString *)key;

- (void)addVODFavoriteWithModel:(LocalVideoModel *)model andUserType:(BOOL)user;
/** 删除点播收藏记录 (单删，播放器界面使用) */
- (void)singleDeleteVODFavoriteWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key;
- (void)singleDeleteVODFavoriteWithDeleteID:(NSString *)deleteID andUserType:(BOOL)user andKey:(NSString *)key;
/** 删除点播频道收藏 (多选删除，我的部分使用) */
- (void)multiDeleteVODFavoriteWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
/** 清空点播收藏列表*/
- (void)cleanVODFavoriteListWithUserType:(BOOL)user;
//判断点播节目是否被收藏过
//- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID;
- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID andUserType:(BOOL)user;



#pragma mark --------------点播播放记录部分
/** 增加点播播放记录 */
- (void)addVODPlayRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key;
/**
 增加点播播放记录

 @param seq 序号（Integer 终端显示时按此排序）
 @param contentID 视频编号（String，不可为空）
 @param name 视频名称（String，不可为空）
 @param subTitle 副标题（String，可为空）
 @param flagPlayOuter 外网播放标识(Integer ,可为空) 0：可播放 1：不可播放(打标签)
 @param definition 清晰度 （Integer ,可为空）0：标清 1：高清 2:4K
 @param imageLink 海报链接（String）
 @param breakPoint 断点位置（Integer）记录观看到多长时间的位置，单位：秒。多集视频此参数传最后播放剧集的信息
 @param isOffShelves 是否已下架(Boolean，不可为空)
 @param duration 影片时长(Integer，单位分钟) 多集视频此参数传最后播放剧集的信息
 @param playTime 最后播放时间（string格式：yyyy-MM-dd hh:mm:ss，如：2011-06-16 19:20:00）。多集视频此参数传最后播放剧集的信息
 @param style 显示样式(String)
 @param user 用户类型（用户/游客，必传）
 @param key 对比是否已存在的key
 */
- (void)addVODPlayRecorderWithSeq:(NSString *)seq andContentID:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andFlagPlayOuter:(NSString *)flagPlayOuter andDefinition:(NSString *)definition andImageLink:(NSString *)imageLink andBreakPoint:(NSString *)breakPoint andIsOffShelves:(NSString *)isOffShelves andDuration:(NSString *)duration andPlayTime:(NSString *)playTime andStyle:(NSString *)style andUserType:(BOOL)user andKey:(NSString *)key;

- (void)addVODPlayRecordWithModel:(LocalVideoModel *)model andUserType:(BOOL)user;
/** 删除点播播放记录 */
//- (void)deleteVODPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
- (NSMutableArray *)deleteVODPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
/** 清空点播播放记录*/
- (void)cleanVODPlayRecordWithUserType:(BOOL)user;
/** 获取点播播放记录*/
- (NSArray *)GetVODPlayRecordWithUserType:(BOOL)user;
/** 获取所有playlist的长度*/
- (NSInteger)getVODListLengthWithUserType:(BOOL)user;



#pragma mark --------------回看播放记录部分
//根据ID查询回看断点
- (NSString *)checkTheReWatchListWithEventID:(NSString *)eventID andUserType:(BOOL)user;
//根据ID查询回看对象字典
- (NSDictionary *)getAReWatchItemWithEventID:(NSString *)eventID andUserType:(BOOL)user;
/** 增加回看播放记录 */
- (void)addReWatchPlayRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key;
/**
 增加回看播放记录

 @param serviceID 频道编号（Integer）
 @param serviceName 频道名称（string）
 @param eventID 节目编号（String）
 @param startTime 播出时间（String）格式为yyyy-MM-dd HH:mm:ss
 @param endTime 结束时间（String）格式为yyyy-MM-dd HH:mm:ss
 @param eventName 节目信息（String）
 @param breakPoint 断点位置（Integer）记录观看到多长时间的位置，单位：秒
 @param playTime 最后播放时间（string格式：yyyy-MM-dd hh:mm:ss，如：2011-06-16 19:20:00）
 @param imageLink 频道图标（string）注：terminal_type=5时不用返回
 @param levelImageLink 横条频道图标（string）注：terminal_type=5时不用返回
 @param user 用户类型（用户/游客，必传）
 @param key 对比是否已存在的key
 */
- (void)addReWatchPlayRecordWithServiceID:(NSString *)serviceID andServiceName:(NSString *)serviceName andEventID:(NSString *)eventID andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andEventName:(NSString *)eventName andBreakPoint:(NSString *)breakPoint andPlayTime:(NSString *)playTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andUserType:(BOOL)user andKey:(NSString *)key;

- (void)addReWatchPlayRecordWithModel:(LocalVideoModel *)model andUserTye:(BOOL)user;
/** 删除回看播放记录 */
//- (void)deleteReWatchPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
- (NSMutableArray *)deleteReWatchPlayRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
/** 清空回看播放记录*/
- (void)cleanReWatchPlayRecordWithUserType:(BOOL)user;
/** 获取回看播放记录*/
- (NSArray *)GetReWatchPlayRecordWithUserType:(BOOL)user;
/** 获取所有playlist的长度*/
- (NSInteger)getRewatchListLengthWithUserType:(BOOL)user;



#pragma mark --------------健康视频收藏部分
//增加健康视频收藏记录
- (void)addHealthyCollectionRecordWithDic:(NSDictionary *)addDic andUserType:(BOOL)user andKey:(NSString *)key;
/**
 增加健康视频收藏记录

 @param hID 节目ID
 @param name 节目名称
 @param releaseYear 节目首播年份
 @param releaseDate 节目首播日期
 @param description 节目描述
 @param duration 节目时长(单位：秒)
 @param imageUrl 节目图片链接
 @param user 用户类型（用户/游客，必传）
 @param key 对比是否已存在的key
 */
- (void)addHealthyCollectionRecordWithID:(NSString *)hID andName:(NSString *)name andReleaseYear:(NSString *)releaseYear andReleaseDate:(NSString *)releaseDate andDescription:(NSString *)description andDuration:(NSString *)duration andImageUrl:(NSString *)imageUrl andUserType:(BOOL)user andKey:(NSString *)key;

//删除健康视频收藏记录(播放器界面的点击删除)
- (void)deleteHealthyCollectionRecordWithDeleteDic:(NSDictionary *)deleteDic andUserType:(BOOL)user andKey:(NSString *)key;
//删除健康视频收藏记录(用户中心多选删除)
- (void)deleteHealthyCollectionRecordWithDeleteArr:(NSArray *)deleteArr andUserType:(BOOL)user andKey:(NSString *)key;
//清空健康视频收藏记录
- (void)cleanHealthyCollectionRecordWithUserType:(BOOL)user;
//获取健康视频收藏记录
- (NSArray *)getHealthyCollectionRecordWithUserType:(BOOL)user;
//健康视频是否被收藏过
- (BOOL)checkHealthyCollectionListWithProgramID:(NSString *)programID andUserType:(BOOL)user;



#pragma mark --------------搜索部分
/** 增加搜索记录 */
+ (void)addSearchRecorderWithDic:(NSDictionary *)redorderDic;
/** 删除搜索记录 */
+ (void)deleteSearchRecorderWithsearchName:(NSString *)searchName;
/** 获取搜索记录 */
+ (NSArray*)SearchRecorder;



/**
 清空本地用户所有列表   目前没有预约列表
 */
- (void)cleanUserAllList;



/**
 请求返回列表数据存入对应的PLIST文件

 @param plistName plist名字
 @param listArr 列表数组
 */
- (void)writeListArrInPlistWithPlistName:(NSString *)plistName andListArr:(NSArray *)listArr;


@end
