//
//  PortalManger.h
//  HIGHTONG_Public
//
//  Created by Kael on 2017/5/31.
//  Copyright © 2017年 创维海通. All rights reserved.
//

/*
 策略描述：
 1、每次启动APP 公网、内网 ProjectList 都需要请求一遍
 2、将公网 、内网的PORTAL列表 融合下；内网数据优先于公网数据；按理来说，公网是内网的子集，内网跟公网可能会有覆盖的数据需要保证跟公网一致，特殊测试环境可视情况而定，保持公网、内网不一致，内网覆盖覆盖公网，以此来更换测试环境；
 3、糅合之后的PORTAL列表,用来进行APP网络请求，存储到 NSUserdefults 中；
 4、内网、公网的PORTAL列表放到 temp  或者 对象中。
 5、
 
 
 */


#import <Foundation/Foundation.h>

#import "PortalListModel.h"


typedef NS_ENUM(NSUInteger, APPNetEnvType) {
    kAPPNetEnvType_ALL = 0,//公网内网都请求
    kAPPNetEnvType_PubNet = 1,//公网
    kAPPNetEnvType_LAN = 2,//内网
};



@interface PortalManger : NSObject

/**
 公网PORTALlist数据模型
 */
@property (nonatomic,strong) PortalListModel *pubNetPortalList;

/**
 内网PORTALlist数据模型
 */
@property (nonatomic,strong) PortalListModel *LANPortalList;


/**
 初始化 PORTAL 管理对象；返回一个PORTAL管理对象

 @return PORTAL 管理对象
 */
+(instancetype)sharedPortalManager;

/**
 重新请求 PORTAL

 @param protalType 请求类型
 */
-(void)retryRequestProjectListWith:(APPNetEnvType)netEnvType;

@property (nonatomic,copy)  void (^callAppAlert)(NSString *status);

@property (nonatomic,copy)  void (^callNetWorkAlert)(NSString *status);




@end
