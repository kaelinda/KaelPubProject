//
//  UGCUploadManager.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/20.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "UGCUploadManager.h"
#import "HTUGCUpLoadInterface.h"
#import "DataPlist.h"
#import "HTRequest.h"
#import "GETBaseInfo.h"
#import "NetWorkNotification_shared.h"
#import "VideoCompressTool.h"

static dispatch_once_t onceToken;

static UGCUploadManager *shared = nil;

@interface UGCUploadManager ()<HTRequestDelegate>
{
    
    CGFloat _uploadProgress;
    
    BOOL firstUpload;
    
    BOOL upLoading;
    
}

@property (strong,nonatomic) HTUGCUpLoadInterface *ugcRequest;

@property (strong,nonatomic) NSMutableArray *taskArr;

@end

@implementation UGCUploadManager

- (NSMutableArray *)taskArr
{
    if (!_taskArr) {
        _taskArr = [NSMutableArray array];
    }
    return _taskArr;
}

+ (id)shareInstance
{
    
    dispatch_once(&onceToken, ^{
        
        shared = [[[self class] alloc] init];
        
    });
    
    return shared;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        upLoading = NO;
        
        _ugcRequest = [HTUGCUpLoadInterface shareInstance];
        
        [[NetWorkNotification_shared shardNetworkNotification]setCanShowToast:YES];//实时网络监测
        //注册通知的观察
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChange:) name:@"NETCHANGED" object:nil];
        
        //检测用户退出，停止所有任务
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopUGCUploadTask) name:@"LOG_OUT_SUCCESS" object:nil];

        //检测用户登陆，开启任务队列
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeUploadingTaskStatus) name:@"UGC_ISVALIDUSER_SUCCESS" object:nil];
        
    }
    return self;
}

- (void)changeUploadingTaskStatus
{
    
    self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
    
    if (self.taskArr.count > 0) {
        
        for (NSDictionary *dic in self.taskArr) {
            
            if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                
                NSMutableArray *tempArr = [dic objectForKey:@"list"];
                
                for (NSMutableDictionary *tempDic in tempArr) {
                    
                    if (![[tempDic objectForKey:@"status"]isEqualToString:@"1"]) {
                        
                        [tempDic setObject:@"1" forKey:@"status"];
                        
                    }
                    
                }
                
                NSLog(@"用户登录后来此检查任务写操作");
                [DataPlist WriteToPlistArray:self.taskArr ToFile:UGC_UPLOADING_LIST_PLIST];
                
                [self startUGCUploadManager];
                
            }
            
        }
        
    }else
    {
        
    }
    
    
}

- (void)startUGCUploadManager
{
    
    NSString *UGCAccessToken =  [[NSUserDefaults standardUserDefaults] objectForKey:@"UGCAccessToken"];
    
    if (UGCAccessToken.length == 0) {
        
        HTRequest *rt = [[HTRequest alloc]initWithDelegate:self];
        
        [rt UGCCommonGetToken];
        
    }
    
    
    if (_ugcRequest.task) {
        
    }else
    {

        BOOL isSuccessed =  [[NSUserDefaults standardUserDefaults] objectForKey:@"AuthenticSuccessed"];
        
        //1.判断权限
        if (isSuccessed) {
            
            self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
            
            if (self.taskArr.count > 0) {
                
                for (NSDictionary *dic in self.taskArr) {
                    
                    if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                        
                        NSMutableArray *tempArr = [dic objectForKey:@"list"];
                        
                        //2.判断是否有任务
                        if (tempArr.count) {
                            
                            NSString *netStatus = [GETBaseInfo getNetworkType];
                            
                            //3.判断网络
                            if ([netStatus isEqualToString:@"WIFI"]) {
                                
                                //检查plist文件中是否有任务需要开启
                                [self chooseTaskToUpLoadFromPlist];
                                
                            }else if(![netStatus isEqualToString:@"无网络"]){
                                
                                if (_callAppAlert) {
                                    _callAppAlert(@"未开始");
                                }
                            }
                        }else
                        {
                            
                        }
                        
                    }
                    
                }
                
            }else
            {
                
            }
        }
    }

}

- (void)chooseTaskToUpLoadFromPlist
{
    
    self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
    
    if (upLoading) {
        
    }else
    {
        
        if (self.taskArr.count > 0) {
            
            for (NSDictionary *dic in self.taskArr) {
                
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                    
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                    
                    for (NSMutableDictionary *tempDic in tempArr) {
                        
                        if (![[tempDic objectForKey:@"status"]isEqualToString:@"0"]) {
                            
                            [tempDic setObject:@"2" forKey:@"status"];
                            
                            NSLog(@"准备开启任务修改任务转台写操作");
                            [DataPlist WriteToPlistArray:self.taskArr ToFile:UGC_UPLOADING_LIST_PLIST];
                            
                            
                            upLoading = YES;
                            
                            //开始压缩
                            [self startVideoCompressWithTaskDic:tempDic];
                            
                            return;
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }else
        {
            
        }
        
    }
    
}

- (void)startVideoCompressWithTaskDic:(NSMutableDictionary *)taskDic
{
    
    //每次任务开始执行，提示正在上传界面更新数据
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UGC_STARTUPLOADTASK" object:nil];
    
    NSString *url = [taskDic objectForKey:@"fileUrl"];
    
    VideoCompressTool *VideoTool = [[VideoCompressTool alloc]init];
    
    [VideoTool compressAssetUrlWithCompressionType:@"AVAssetExportPresetHighestQuality" andCompressionUrl:url andReturn:^(NSInteger status, NSURL *videoUrl) {
        
        if (status == 0) {//压缩成功
            
            [taskDic setObject:videoUrl forKey:@"videoUrl"];
            
             [self startUpLoadTaskWithTaskDic:taskDic];
            
        }
        else//压缩失败
        {
            
            self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
            
            //删除plist文件里面的下载条目信息
            NSString *sign = [taskDic objectForKey:@"sign"];

            //修改plist文件里面的下载条目信息
            for (NSMutableDictionary *dic in self.taskArr) {
                    
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                        
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                        
                    for (NSMutableDictionary *tempDic in tempArr) {
                            
                        if ([[tempDic objectForKey:@"sign"]isEqualToString:sign]) {
                                
                            [tempDic setObject:@"0" forKey:@"status"];
                                
                            break;
                                
                        }
                            
                    }
                        
                }
                    
            }
            
            NSLog(@"压缩失败后写操作");
            
            [DataPlist WriteToPlistArray:self.taskArr ToFile:UGC_UPLOADING_LIST_PLIST];
            
            upLoading = NO;
            
            [self startUGCUploadManager];
                
        }
        
    }];

   
    
}

- (void)startUpLoadTaskWithTaskDic:(NSMutableDictionary *)taskDic
{
    
    //每次任务开始执行，提示正在上传界面更新数据
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UGC_STARTUPLOADTASK" object:nil];
    
    NSURL *url = [taskDic objectForKey:@"videoUrl"];
    
    [_ugcRequest UGCUpLoadVideoWithVideoUrl:url andVideoDic:taskDic andReturn:^(NSInteger status, NSMutableDictionary *result) {
        
        upLoading = NO;
        
        self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
        
        VideoCompressTool *VideoTool = [[VideoCompressTool alloc]init];
        
        //清除缓存文件
        [VideoTool removeCompressedVideoFromDocumentsWithFilePath:[NSString stringWithFormat:@"%@",url]];
        
        //删除plist文件里面的下载条目信息
        NSString *sign = [taskDic objectForKey:@"sign"];
        
        if (status == 0&&[result objectForKey:@"code"]==0) {
            
            NSDictionary *dic = [[result objectForKey:@"result"]objectAtIndex:0];
            
            NSMutableDictionary *requestDic = [NSMutableDictionary dictionary];
            
            for (NSMutableDictionary *dic in self.taskArr) {
                
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                    
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                    
                    for (NSMutableDictionary *tempDic in tempArr) {
                        
                        if ([[tempDic objectForKey:@"sign"]isEqualToString:sign]) {
                            
                            requestDic = tempDic;
                            
                            [tempArr removeObject:tempDic];
                            
                            break;
                            
                        }
                    }
                    
                }
            }
            
            NSString *imageStr64 = [requestDic objectForKey:@"pictureFile"];
            
            NSData *decodedImageData   = [[NSData alloc]initWithBase64EncodedString:imageStr64 options:0];
            UIImage *decodedImage      = [UIImage imageWithData:decodedImageData];
                        
            HTRequest *rt = [[HTRequest alloc]initWithDelegate:self];
            
            [rt UGCCommonInsertProgramWithVideoId:[dic objectForKey:@"videoId"] andName:[requestDic objectForKey:@"name"]  andThumbnaill:[dic objectForKey:@"thumbnaill"] andCover:[dic objectForKey:@"cover"] andPictureFile:decodedImage andSize:[dic objectForKey:@"size"] andIsEncrypt:@"" andCategoryId:[requestDic objectForKey:@"categoryId"] andCategory:[requestDic objectForKey:@"category"] andStatus:[dic objectForKey:@"status"] andRemarks:[requestDic objectForKey:@"remarks"] andUrl:@"" andTags:[requestDic objectForKey:@"tags"] andDuration:@""];
            
        }else
        {
            
            //修改plist文件里面的下载条目信息
            for (NSMutableDictionary *dic in self.taskArr) {
                
                if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
                    
                    NSMutableArray *tempArr = [dic objectForKey:@"list"];
                    
                    for (NSMutableDictionary *tempDic in tempArr) {
                        
                        if ([[tempDic objectForKey:@"sign"]isEqualToString:sign]) {
                            
                            [tempDic setObject:@"0" forKey:@"status"];
                            
                            break;
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        NSLog(@"上传有回调后进入写操作");
        [DataPlist WriteToPlistArray:self.taskArr ToFile:UGC_UPLOADING_LIST_PLIST];
                
        [self startUGCUploadManager];
        
        if (_UGCUploadManagerResultBlock) {
            _UGCUploadManagerResultBlock(status,sign);
        }
        
    }andSpeedReturn:^(NSString *speed,CGFloat uploadProgress) {
        
        _uploadProgress = uploadProgress;
        
        if (_UGCUploadManagerSpeedBlock) {
            _UGCUploadManagerSpeedBlock(speed,uploadProgress);
        }
        
    }];
    
}

//结束任务
- (void)stopUGCUploadTask
{
    
    [_ugcRequest cancelOperations];
    
}

//录入新任务
- (void)addTaskWithDateDic:(NSMutableDictionary *)dateDic
{
    
    firstUpload = YES;
    
    if ([dateDic objectForKey:@"pictureFile"]) {
        
        NSData *_data = UIImageJPEGRepresentation([dateDic objectForKey:@"pictureFile"], 0.1f);
        //将图片的data转化为字符串
        NSString *strimage64 = [_data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        [dateDic setValue:strimage64 forKey:@"pictureFile"];
        
    }
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    
    NSString *DateTime = [formatter stringFromDate:date];
    
    [dateDic setValue:DateTime forKey:@"time"];
    
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    
    NSString *SignTime = [formatter stringFromDate:date];
    
    NSString *sign = [NSString stringWithFormat:@"%@-%@",[dateDic objectForKey:@"name"],SignTime];
    
    [dateDic setValue:sign forKey:@"sign"];
    
    [dateDic setValue:@"1" forKey:@"status"];
    
    
    self.taskArr = [DataPlist ReadToFileArr:UGC_UPLOADING_LIST_PLIST];
    
    for (NSMutableDictionary *dic in self.taskArr) {
        
        if ([[dic objectForKey:@"phone"]isEqualToString:KMobileNum]) {
            
            NSMutableArray *tempArr = [dic objectForKey:@"list"];
            
            [tempArr addObject:dateDic];
            
            [dic setObject:tempArr forKey:@"list"];
            
            firstUpload = NO;
            
        }
    
    }
    
    if (firstUpload) {
        
        NSMutableArray *tempArr = [NSMutableArray array];
        
        [tempArr addObject:dateDic];
        
        NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:KMobileNum,@"phone",tempArr,@"list", nil];
        
        [self.taskArr addObject:tempDic];
        
    }
    
    NSLog(@"新增任务后写操作");
    [DataPlist WriteToPlistArray:self.taskArr ToFile:UGC_UPLOADING_LIST_PLIST];
    
    [self startUGCUploadManager];
    
}

- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    
    NSLog(@"status-------%ld,result------%@,type-------%@",(long)status,result,type);
    
    //上传点播
    if ([type isEqualToString:UGC_COMMON_INSERTPROGRAM]) {
        
        NSLog(@"UGC_COMMON_INSERTPROGRAM返回数据: %ld result:%@  type:%@",status,result,type );
        
        if ([[result objectForKey:@"ret"] integerValue] == 0 && [result objectForKey:@"ret"] )
        {
            
          
            
        }
    }
    
}




- (void)networkChange:(NSNotification *)noti
{
    
    NSString *objStr = (NSString *)[noti object];
    
    if ([objStr isEqualToString:@"蜂窝网络"]) {
        
        if (upLoading&&_uploadProgress<0.8) {
            
            if (_callAppAlert) {
                _callAppAlert(@"进行中");
            }
            
        }
        
    }
//    else if ([objStr isEqualToString:@"wifi网络"]){
//        
//        if (!upLoading) {
//            
//            if (_callAppAlert) {
//                _callAppAlert(@"又切回来了");
//            }
//            
//            [self changeUploadingTaskStatus];
//            
//        }
//    }
}

- (void)userLogout
{
    
    [self stopUGCUploadTask];
    

    
}

- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}






@end
