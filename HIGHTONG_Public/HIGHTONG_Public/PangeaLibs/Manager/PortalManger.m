//
//  PortalManger.m
//  HIGHTONG_Public
//
//  Created by Kael on 2017/5/31.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "PortalManger.h"

#import "HTUNPortalInterface.h"

#import "BPushSingleTon.h"

@interface PortalManger()
{
    
    BOOL isRequesting;//是否正在请求
    
    NSInteger getRequestTime;//接收返回次数
    
    BOOL pubRequestIsSuc;
    
    BOOL lanRequestIsSuc;
    
}

@end

@implementation PortalManger


+(instancetype)sharedPortalManager{
    
    static id _sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(instancetype)init{
    
    self = [super init];
    
    isRequesting = NO;
    
    getRequestTime = 0;
    
    pubRequestIsSuc = NO;
    
    lanRequestIsSuc = NO;
    
    return self;
    
}

-(void)retryRequestProjectListWith:(APPNetEnvType)netEnvType{
    
    if (!isRequesting) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];//默认是支持VOD
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];//默认 不支持广告
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EPGPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"VODPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"UPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ICS"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shphone"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"moreWifiLink"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"polymedicine"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMGPORTAL"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMG_PUBLIC"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shphone_public"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"appbase"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_PUBLIC_ENV"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shtv"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"operatorCode"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pubapi"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pubportal"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"mobilepush"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"shportal"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ugc"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if (!_pubNetPortalList) {
            _pubNetPortalList = [[PortalListModel alloc] init];
        }
        
        if (!_LANPortalList) {
            _LANPortalList = [[PortalListModel alloc] init];
        }
        
        switch (netEnvType) {
            case kAPPNetEnvType_ALL:
            {
                isRequesting = YES;
                
                HTUNPortalInterface *pubInterface =  [[HTUNPortalInterface alloc] init];
                
                pubInterface.requestType = @"PUB";
                
                [pubInterface UNPortalProjectListWithRegionKey:KREGION_CODE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
                    
                    getRequestTime++;
                    
                    if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]){
                        
                        pubRequestIsSuc = YES;
                        
                        _pubNetPortalList = [PortalListModel yy_modelWithDictionary:result];
                        NSLog(@"pubNetPortalList.......YYModel\n--->ret:%@ \n--- portalList:%@",_pubNetPortalList.ret,_pubNetPortalList.projectList);
                        
                    }else{
                        pubRequestIsSuc = NO;
                    }
                    
                    
                    [self mergeProtalList];
                    
                }];
                
                HTUNPortalInterface *lanInterface =  [[HTUNPortalInterface alloc] init];
                
                lanInterface.requestType = @"LAN";
                
                [lanInterface UNPortalProjectListWithRegionKey:KREGION_CODE andReturn:^(NSInteger status, NSMutableDictionary *result, NSString *type) {
                    
                    getRequestTime++;
                    
                    if ([[result objectForKey:@"ret"]integerValue] == 0 && [result objectForKey:@"ret"]){
                        
                        lanRequestIsSuc = YES;
                        
                        _LANPortalList = [PortalListModel yy_modelWithDictionary:result];
                        NSLog(@"LANPortalList.......YYModel\n--->ret:%@ \n--- portalList:%@",_LANPortalList.ret,_LANPortalList.projectList);

                        
                    }else{
                        lanRequestIsSuc = NO;
                    }
                    
                    [self mergeProtalList];
                    
                }];
            }
                break;
                
            case kAPPNetEnvType_PubNet:
            {
                
            }
                break;
                
            case kAPPNetEnvType_LAN:
            {
                
            }
                break;
                
                
            default:
                break;
        }
        
    }

}

-(void)mergeProtalList
{
    if (getRequestTime == 2) {
        
        if (_pubNetPortalList != nil) {
            
            [_pubNetPortalList.projectList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                PortalModel *tempArr = obj;
                
                NSLog(@"外网--------%@----%@----%@",tempArr.projectUrl,tempArr.projectCode,tempArr.projectName);
                
                NSString *projectCode = [NSString stringWithFormat:@"%@",tempArr.projectCode];
                NSMutableString *projectUrl = [NSMutableString stringWithFormat:@"%@",tempArr.projectUrl];
                
                projectUrl = projectUrl.length>0 ? projectUrl :[NSMutableString stringWithFormat:@""];
                
                if ([projectUrl hasSuffix:@"/"]) {
                    [projectUrl deleteCharactersInRange:NSMakeRange(projectUrl.length-1, 1)];
                }
                
                [self mergeingProjectCode:projectCode andProjectUrl:projectUrl];
                

            }];
            
            _pubNetPortalList = nil;
            
            
        }else{
            
        }
        
        if (_LANPortalList != nil) {
            
            [_LANPortalList.projectList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                PortalModel *tempArr = obj;
                
                NSLog(@"内网--------%@----%@----%@",tempArr.projectUrl,tempArr.projectCode,tempArr.projectName);
                
                NSString *projectCode = [NSString stringWithFormat:@"%@",tempArr.projectCode];
                NSMutableString *projectUrl = [NSMutableString stringWithFormat:@"%@",tempArr.projectUrl];
                
                projectUrl = projectUrl.length>0 ? projectUrl :[NSMutableString stringWithFormat:@""];
                
                if ([projectUrl hasSuffix:@"/"]) {
                    [projectUrl deleteCharactersInRange:NSMakeRange(projectUrl.length-1, 1)];
                }
                
                [self mergeingProjectCode:projectCode andProjectUrl:projectUrl];
                
                
            }];
            
            _LANPortalList = nil;
            
        }
        
        if (_callAppAlert) {
            if (!pubRequestIsSuc&&!lanRequestIsSuc) {
                
                _callAppAlert(@"0");
                
            }else
            {
                
                 [[BPushSingleTon shareInstance]bindChannel];
                
                _callAppAlert(@"1");
                
            }
        }
        
        if (_callNetWorkAlert) {
            if (!pubRequestIsSuc&&!lanRequestIsSuc) {
                
                _callNetWorkAlert(@"0");
                
            }else
            {
                
                [[BPushSingleTon shareInstance]bindChannel];
                
                _callNetWorkAlert(@"1");
                
            }
        }

        //重制请求返回次数和是否正在请求状态
        getRequestTime = 0;
        
        isRequesting = NO;
        
    }
    
}

-(void)mergeingProjectCode:(NSString *)projectCode andProjectUrl:(NSMutableString *)projectUrl
{
    
    if ([projectCode isEqualToString:@"EPGPORTAL"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"EPGPORTAL"];
        
        if (projectUrl.length == 0) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_EPG_CONFIGURED"];
        }
        
    }
    if ([projectCode isEqualToString:@"VODPORTAL"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"VODPORTAL"];
        
        if (projectUrl.length == 0) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_EPG_CONFIGURED"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_EPG_CONFIGURED"];
        }
        
    }
    if ([projectCode isEqualToString:@"UPORTAL"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"UPORTAL"];
        
    }
    if ([projectCode isEqualToString:@"ICS"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ICS"];
        
    }
    if ([projectCode isEqualToString:@"VODSWITCH"]) {
        if (![projectUrl isEqualToString:@"close"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_VOD"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_VOD"];
        }
        
    }
    if ([projectCode isEqualToString:@"ADPORTAL"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ADPORTAL"];
        if (projectUrl.length==0) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SUPPORT_AD"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SUPPORT_AD"];
        }
    }
    if ([projectCode isEqualToString:@"shphone"]) {
        //shphone
        //兼容 后缀 无论有没有 都删除一下 再拼接上
        if ([projectUrl hasSuffix:@"/shphone"]) {
            [KaelTool deleteString:@"/shphone" fromString:projectUrl];
            
        }
        if ([projectUrl hasSuffix:@"/shphone/"]) {
            [KaelTool deleteString:@"/shphone/" fromString:projectUrl];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone"];
        //            [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
        [[NSUserDefaults standardUserDefaults] setObject:[KaelTool adjustUIWebViewURLWith:[NSString stringWithFormat:@"%@/shphone/pages/wifi/morewifi.html",projectUrl]]forKey:@"moreWifiLink"];
        
    }
    
    
    if ([projectCode isEqualToString:@"polymedicine"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"polymedicine"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSuccessPortal"];
    }
    
    
    if ([projectCode isEqualToString:@"IMG"]) {
        
        if (APIVERSION.length>0) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMGPORTAL"];
        }else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IMGPORTAL"];
        }
    }
    if ([projectCode isEqualToString:@"IMG_PUBLIC"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"IMG_PUBLIC"];
    }
    
    if ([projectCode isEqualToString:@"shphone_public"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shphone_public"];
        
    }
    if ([projectCode isEqualToString:@"appbase"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"appbase"];
        
    }
    if ([projectCode isEqualToString:@"IS_PUBLIC_ENV"]) {
        
        BOOL kIS_PUBLIC_ENV = [projectUrl integerValue]==1 ? YES : NO;
        [[NSUserDefaults standardUserDefaults] setBool:kIS_PUBLIC_ENV forKey:@"IS_PUBLIC_ENV"];
        
    }
    if ([projectCode isEqualToString:@"shtv"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shtv"];
        
        
    }
    
    
    if ([projectCode isEqualToString:@"operatorCode"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"operatorCode"];
        
    }
    if ([projectCode isEqualToString:@"tvbiz"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"tvbiz"];
        
    }
    
    if ([projectCode isEqualToString:@"pubapi"]) {
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"pubapi"];
        
    }
    
    if ([projectCode isEqualToString:@"mobilepush"]) {
        [[NSUserDefaults standardUserDefaults]setObject:projectUrl forKey:@"mobilepush"];
    }
    
    if ([projectCode isEqualToString:@"ugc"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"ugc"];
        
        
    }
    
    if ([projectCode isEqualToString:@"shportal"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"shportal"];
        
        
    }else
    {
        if ([projectCode isEqualToString:@"pubportal"]) {
            [[NSUserDefaults standardUserDefaults] setObject:projectUrl forKey:@"pubportal"];
            
        }
    }
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}





















@end
