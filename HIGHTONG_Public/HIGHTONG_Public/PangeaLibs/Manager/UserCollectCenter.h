//
//  UserCollectCenter.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^ListReturnBlcok)(BOOL isSuccess,NSArray *resultList);
@interface UserCollectCenter : NSObject<HTRequestDelegate>

/** 直播收藏数组 */
@property (nonatomic,strong) NSMutableArray *favoriteChannelArr;

/** 点播收藏数组 */
@property (nonatomic,strong) NSMutableArray *favoriteDemandArr;

+ (id)defaultCenter;

- (BOOL)checkTheFavoriteChannelListWithServiceID:(NSString *)serviceID;

- (BOOL)checkTheFavoriteDemandListWithContentID:(NSString *)contentID;
//直播收藏通知触发方法
- (void)refreshTheFavoriteChannelList;

- (void)refreshTheFavoriteDemandList;



@property (nonatomic,copy) ListReturnBlcok channelListBlcok;

-(void)refreshEPGFavorateChannelListWith:(ListReturnBlcok)resultBlcok;

@property (nonatomic,copy) ListReturnBlcok demandListBlock;

-(void)refreshDemandListWith:(ListReturnBlcok)resultBlock;
@end
