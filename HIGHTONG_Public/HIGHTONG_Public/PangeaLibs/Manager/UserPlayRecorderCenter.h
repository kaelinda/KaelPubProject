//
//  UserPlayRecorderCenter.h
//  HIGHTONG
//
//  Created by 伟 吴 on 15/8/11.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^FinishedRefeshBlock)(NSArray *);

typedef NS_ENUM(NSUInteger, PlayRecorderType) {
    EPG_PlayRecorder = 0,/* 直播播放记录 */
    VOD_PlayRecorder,/* 点播播放记录 */
};


@interface UserPlayRecorderCenter : NSObject<HTRequestDelegate>

/** 点播播放记录数组 */
@property (nonatomic,strong) NSMutableArray *playRecordListArr;

- (NSString *)checkTheVODListWithContentID:(NSString *)contentID;

- (NSDictionary *)getVODListItemWithContentID:(NSString *)contentID;

+ (id)defaultCenter;

/** 回看播放记录数组 */
@property (nonatomic,strong) NSMutableArray *lookBackArr;

- (NSString *)checkTheReWatchListWithEventID:(NSString *)eventID;

- (NSDictionary *)getAReWatchItemWithEventID:(NSString *)eventID;


#pragma mark - Kael新添加的方法和属性
/**
 直播刷新完毕的回调
 */
@property (nonatomic,copy) FinishedRefeshBlock EPG_FinishedBlcok;

/**
 点播刷新完毕的回调
 */
@property (nonatomic,copy) FinishedRefeshBlock VOD_FinishedBlcok;

/**
 根据类型刷新 直播 或 点播 播放记录

 @param playRecorderType 播放记录类型
 @param didFinishedBlock 刷新完成之后的回调
 */
- (void)refreshPlayRecorderWithType:(PlayRecorderType)playRecorderType withDidFinished:(FinishedRefeshBlock) didFinishedBlock;

/**
 删除 点播/直播(回看) 节目播放记录
 
 @param eventID 点播/直播(回看) 节目的eventID
 */
-(void)deletEventWith:(PlayRecorderType)playRecorderType andEventID:(NSString *)eventID;

/**
 添加预约节目 (属性取不到的时候，可以传空传进来的)
 
 @param serviceID 频道ID
 @param serviceName 频道名称
 @param eventID 节目ID
 @param eventName 节目名称
 @param startTime 节目开始时间
 @param endTime 节目结束时间
 @param imageLink 图片链接
 @param levelImageLink 横版图片链接
 @param breakPoint 断点
 */
-(void)addEPGPlayRecorderWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink andBreakPoint:(NSString *)breakPoint;

/**
 添加点播播放记录 (属性取不到的时候，可以传空传进来的)

 @param contentID 点播节目ID
 @param name 点播节目名称
 @param subTitle 点播节目的子标题
 @param playTime 点播节目的播放时间
 @param style 显示样式
 @param breakPoint 点播节目断点
 @param duration 点播节目时长
 @param imageLink 点播节目海报
 */
-(void)addVODPlayRecorderWith:(NSString *)contentID andName:(NSString *)name andSubTitle:(NSString *)subTitle andPlayTime:(NSString *)playTime andStyle:(NSString *)style andBreakPoint:(NSString *)breakPoint andDuration:(NSString *)duration andImageLink:(NSString *)imageLink;


/**
 清空本地播放记录列表
 */
-(void)cleanRecorderList;


@end
