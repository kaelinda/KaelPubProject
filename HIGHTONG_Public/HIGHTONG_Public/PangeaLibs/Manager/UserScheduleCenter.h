//
//  UserScheduleCenter.h
//  HIGHTONG_Public
//
//  Created by 伟 吴 on 15/9/28.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^FinishedRefeshBlock)(NSArray *);

@interface UserScheduleCenter : NSObject<HTRequestDelegate>

@property (strong, nonatomic) NSMutableArray * didScheduledArr;

+ (id)defaultCenter;

- (BOOL)checkTheScheduledListwithID:(NSString *)eventID;

- (NSArray *)getScheduleArr;


/**
 刷新完毕的回调
 */
@property (nonatomic,copy) FinishedRefeshBlock finishedBlcok;


/**
 刷新预约节目列表的回调方法

 @param didFinishedBlock 已经完成回调
 */
- (void)refreshUserSchedule:(FinishedRefeshBlock) didFinishedBlock;

/**
 添加预约节目 (属性取不到的时候，可以传空传进来的)

 @param serviceID 频道ID
 @param serviceName 频道名称
 @param eventID 节目ID
 @param eventName 节目名称
 @param startTime 节目开始时间
 @param endTime 节目结束时间
 @param imageLink 图片链接
 @param levelImageLink 横版图片链接
 */
-(void)addEventWith:(NSString *)serviceID andEventName:(NSString *)serviceName andEventID:(NSString *)eventID andEventName:(NSString *)eventName andStartTime:(NSString *)startTime andEndTime:(NSString *)endTime andImageLink:(NSString *)imageLink andLevelImageLink:(NSString *)levelImageLink;


/**
 删除预约节目

 @param eventID 预约节目的eventID
 */
-(void)deletEventWith:(NSString *)eventID;


/**
 删除本地预约列表
 */
-(void)cleanScheduleList;


@end
