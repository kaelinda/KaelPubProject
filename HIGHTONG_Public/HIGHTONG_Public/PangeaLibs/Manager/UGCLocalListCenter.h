//
//  UGCLocalListCenter.h
//  HIGHTONG_Public
//
//  Created by  吴伟 on 2017/4/6.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UGCLocalListCenter : NSObject


@property (nonatomic, strong)NSMutableArray *categoryArr;
@property (nonatomic, strong)NSMutableArray *tagArr;


- (NSMutableArray *)getTagArr;


- (NSMutableArray *)getCategoryArr;


//获取分类列表
+ (NSArray *)getUGCCategoryList;

//获取标签列表
+ (NSArray *)getUGCTagList;

//获取医生职称列表
+ (NSArray *)getUGCAcademicTitleList;


//清空UGC列表
+ (void)cleanUGCLocalList;


+ (id)defaultCenter;

/**
 请求返回列表数据存入对应的PLIST文件
 
 @param plistName plist名字
 @param listArr 列表数组
 */
- (void)writeUGCListArrInPlistWithPlistName:(NSString *)plistName andListArr:(NSArray *)listArr;

@end
