//
//  BMKSingleTon.h
//  LocationDemo
//
//  Created by 伟 吴 on 16/1/7.
//  Copyright © 2016年 伟 吴. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMKSingleTon : NSObject

+ (id)shareInstance;

- (void)restartBMKLocation;

- (void)stopBMKLocation;

- (NSDictionary *)getLocationCityAndHospital;

@end
