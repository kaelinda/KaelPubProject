//
//  UGCUploadManager.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/3/20.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^UGCUploadManager_Block)(NSInteger status, NSString *sign);

typedef void(^UGCUploadManagerSpeed_Block)(NSString *speed,CGFloat uploadProgress);


@interface UGCUploadManager : NSObject

@property (nonatomic,copy)UGCUploadManager_Block  UGCUploadManagerResultBlock;

@property (nonatomic,copy)UGCUploadManagerSpeed_Block  UGCUploadManagerSpeedBlock;

@property (nonatomic,copy)  void (^callAppAlert)(NSString *status);

+ (id)shareInstance;

//开始运行管理类（检查环境）
- (void)startUGCUploadManager;


//开启任务队列（在通过第一步环境监察成功的情况下可以直接调用）
- (void)chooseTaskToUpLoadFromPlist;


//结束任务
- (void)stopUGCUploadTask;


//录入新任务
- (void)addTaskWithDateDic:(NSMutableDictionary *)dateDic;


@end
