//
//  KLocalNotificationManager.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/6/7.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalCollectAndPlayRecorder.h"

@interface KLocalNotificationManager : NSObject<HTRequestDelegate>

@property (nonatomic,strong) NSMutableArray *localNotiArr;//本地通知的数组
@property (nonatomic,strong) NSMutableDictionary *localNotiDic;//本地通知的字典 携带节目信息

@property (nonatomic,assign)NSInteger showNotiNum;

@property (nonatomic,strong)LocalCollectAndPlayRecorder *localListCenter;

//获取本地通知 管理对象（单利）
+(instancetype)shareLocalNotificationManager;

/**
 *  @author Kael
 *
 *  @brief 手动直接操纵单例类来管理本地通知
 *  @param notiArray 需要注册的通知列表
 */
-(void)loadingLocalNotificationWithNotiArray:(NSArray *)notiArray;


/**
 移除所有本地通知
 */
-(void)removeAllLocalNotification;

@end
