//
//  KaelTool.h
//  HIGHTONG
//
//  Created by Kael on 15/7/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView_extra.h"
#import <UIKit/UIKit.h>
#import "POP.h"
#import "DateTools.h"
#import "HT_AESCrypt.h"
#import "Header.h"

@interface KaelTool : NSObject

#pragma mark ------->> token 以及其加密处理
/**
 *  @author Kael
 *
 *  @brief 获取真实业务token（32位）
 *  @param token 接口获取到的可能是48位可能是32位的业务token
 *  @return 真实32位业务token
 */
+(NSString *)getTruthTokenWith:(NSString *)token;


/**
 *  @author Kael
 *
 *  @brief 获取时间戳
 *  @return 时间戳
 */
+(NSString *)getTimeStampWith:(NSDateFormatter *)formatter;

/**
 *  @author Kael
 *
 *  @brief 获取请求的签名
 *  @return 签名
 */
+(NSString *)getRquestSignValue;
/**
 *  @author Kael
 *
 *  @brief 获取请求header的 sign（签名）参数
 *  @param token      IDtoken换取的获取到的token
 *  @param versionNum 加密版本号
 *  @return sign参数
 */
+ (NSString *)getRquestSignValueWith:(NSString *)token andVersionNum:(NSString *)versionNum;

#pragma mark ------->> 字符串处理
/**
 *  要删除的字符
 *
 *  @param DStrArr 要删除字符数组
 *  @param FStr    原字符
 *
 *  @return 处理之后的字符
 */
+(NSMutableString *)deleteStringArr:(NSArray *)DStrArr fromString:(NSMutableString *)FStr;

/**
 *  适配UIWebView 对于异常URL格式的处理
 *
 *  @param urlStr 原URL
 *
 *  @return 处理过的URL
 */
+(NSString *)adjustUIWebViewURLWith:(NSString *)urlStr;

/**
 *  删除一个字符串中的 某几个字符
 *
 *  @param DStr 要删除的字符
 *  @param FStr 原字符
 *
 *  @return 已经处理过的字符
 */
+(NSMutableString *)deleteString:(NSString *)DStr fromString:(NSMutableString *)FStr;

#pragma mark ------->> UI 控件的属性添加
/**
 *  初始化一个UIbutton
 *
 *  @param buttonType       button的类型
 *  @param backgroundColor  背景色
 *  @param cornerRadius     圆角
 *  @param normalImg        正常图片
 *  @param highlightedImage 高亮图片
 *
 *  @return 返回一个UIbutton
 */
+(UIButton *)setButtonPropertyWithButtonType:(UIButtonType)buttonType
                          andBackGroundColor:(UIColor *)backgroundColor
                             andCornerRadius:(CGFloat)cornerRadius
                              andNormalImage:(UIImage *)normalImg
                         andHighlightedImage:(UIImage *)highlightedImage;

/**
 *  初始化一个UIlabel
 *
 *  @param backgroundColor 背景色
 *  @param textAlignment   字体格式
 *  @param textColor       字体颜色
 *  @param font            字号
 *
 *  @return UIlabel
 */
+(UILabel *)setlabelPropertyWithBackgroundColor:(UIColor *)backgroundColor
                             andNSTextAlignment:(NSTextAlignment)textAlignment
                                   andTextColor:(UIColor *)textColor
                                        andFont:(UIFont *)font;


/**
 *  custom类button的属性工厂方法
 *
 *  @param normalImageName      normal状态的背景图
 *  @param highlightedImageName highlighted状态的背景图
 *  @param selectedImageName    选中状态的背景图
 *  @param target               target
 *  @param isSelect             触发方法
 *
 *  @return button
 */
+(UIButton *)setButtonPropertyWithBackgroundNormalImageName:(NSString *)normalImageName
                          andBackgroundHighlightedImageName:(NSString *)highlightedImageName
                             andBackgroundSelectedImageName:(NSString *)selectedImageName
                                                  andTarget:(id)target
                                                  andSelect:(SEL)isSelect;

#pragma mark ------->> 日期处理

/**
 *  获取前几天后几天的 某种格式的字符串
 *
 *  @param days 天数 正的为 后几天 负值则是前几天
 *
 *  @return 结果 yyyy-MM-dd
 */
+(NSString *)getFormOrAfterDateWithDays:(NSInteger )days;

#pragma mark ------->> 数组排序
/**
 *  数组升序排列--->> 根据数组中某个字典中的某一键值对排序
 *
 *  @param arr 原数组
 *  @param key 数组中某个字典的key值
 *
 *  @return 排序结果
 */
+(NSMutableArray *)increaseArrayWithArray:(NSArray *)arr andKey:(NSString *)key;
/**
 *  数组降序排列 --- >> 根据数组中某个字典中的某一键值对排序
 *
 *  @param arr 原数组
 *  @param key 数组中某个字典的key值
 *
 *  @return 排序结果
 */
+(NSMutableArray *)decreaseArrayWithArray:(NSArray *)arr andKey:(NSString *)key;

#pragma mark ------->> 动画添加
/**
 *  放大后隐藏的动画
 *
 *  @param view 将要实现动画的视图
 */
//+(void)scaleBigAndHiddenAnamition:(UIView *)view;
/**
 *  @author Kael
 *
 *  @brief 缩放动画 和 透明度动画的结合
 *  @param view          将要实现动画的view
 *  @param animationTime 动画时长
 *  @param scale         缩放比率
 *  @param completBlock  完成后回调的方法
 */
+(void)scaleBigAndHiddenAnamition:(UIView *)view andTime:(CGFloat)animationTime andScale:(CGFloat)scale andAnimationComplet:(void (^)())completBlock;

/**
 *  先放大后缩小的 点赞效果
 *
 *  @param view 将要实现动画的视图
 */
+(void)scaleBigAnimation:(UIView *)view;


#pragma mark ------->> Image Effects
/**
 *  高斯模糊 图片
 *
 *  @param image image
 *
 *  @return 返回值是已经模糊的image
 */
+ (UIImage*)blurredImageWith:(UIImage *)image;

/**
 *  图片高斯模糊（自制）
 *
 *  @param blurLevel   模糊度 value : 0 - 1
 *  @param originImage 需要模糊的图片
 *
 *  @return 已经处理过的图片
 */
+ (UIImage *)gaussBlur:( CGFloat )blurLevel andImage:( UIImage *)originImage;



+ (UIImage *)maskImage:(UIImage*)image withMask:(UIImage*)mask;

/**
 *  @author Kael
 *
 *  @brief 获取指定尺寸的图片
 *  @param image 原图
 *  @param size  目标尺寸
 *  @return 切图之后的图片
 */
+(UIImage*) OriginImage:(UIImage*)image scaleToSize:(CGSize)size;

#pragma mark ------->> 颜色处理
/**
 *  @author Kael
 *
 *  @brief 获取随机色对象
 *  @return 随机色
 */
+ (UIColor *)randomColor;

#pragma mark ------->> 分享链接处理

/**
 *  根据所传视频type返回翔一样的分享链接
 *
 *  @param channelId 节目id
 *  @param type      播放类型
 *
 *  @return 处理之后的分享链接
 */
+(NSString *)getChannelId:(NSString*)channelId andChannelTitle:(NSString*)channelTitle withPlayType:(CurrentPLAYType)type;
@end
