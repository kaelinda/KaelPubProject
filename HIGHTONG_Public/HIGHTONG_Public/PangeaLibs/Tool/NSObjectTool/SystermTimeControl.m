//
//  SystermTimeControl.m
//  HIGHTONG
//
//  Created by apple on 15/4/17.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import "SystermTimeControl.h"

@implementation SystermTimeControl
+(NSDate *)GetSystemTimeWith{
    
    NSUserDefaults *UserInfo = [NSUserDefaults standardUserDefaults];
    NSString *SystimeDeffence;
    if([UserInfo valueForKey:@"SystemTimeDiffence"] == nil){
        SystimeDeffence = @"0";
        [UserInfo setValue:@"0" forKey:@"SystemTimeDiffence"];
    }
    else{
        SystimeDeffence = [UserInfo valueForKey:@"SystemTimeDiffence"];
    }
    
       
    return [NSDate dateWithTimeInterval:[SystimeDeffence intValue] sinceDate:[NSDate date]];
}

// 获取当前服务器时间和系统时间的时间差（单位秒）

+(NSString *)GetNowTimeIntervalHTSystemTimeString
{
    NSDate *serviceTime = [SystermTimeControl GetSystemTimeWith];
    NSTimeInterval delta = [serviceTime timeIntervalSinceNow];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:delta] forKey:@"delta"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    return [[NSUserDefaults standardUserDefaults] valueForKey:@"delta"];
}


+ (NSDate *)getHTServiceTimeAndIsHTTIME:(BOOL)isHTTime
{
    NSTimeInterval delta = [[[NSUserDefaults standardUserDefaults] objectForKey:@"delta"] doubleValue];

    //这个是当前已经调整之后准确修正好的时间
    NSDate *serviceTime = [[NSDate date] dateByAddingTimeInterval:delta];

    if (isHTTime == YES) {
        // 获取当前服务器时间和系统时间的时间差（单位秒）

        //如果差值是正或者负，那么都增加这个值，如果是负的，直接就是当前修正的时间减去差值，相反则反
        return [serviceTime dateByAddingSeconds:[[[NSUserDefaults standardUserDefaults] objectForKey:@"delta"] integerValue]];
        
        
    }else
    {

        return serviceTime;
    }
}






// 获取现在服务器的时间
+ (NSString *)getNowServiceTimeString
{   // 特别注意下这里的时间
    NSTimeInterval delta = [[[NSUserDefaults standardUserDefaults] objectForKey:@"delta"] doubleValue];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *serviceTime = [[NSDate date] dateByAddingTimeInterval:delta];
    return [dateFormatter stringFromDate:serviceTime];
}




+ (NSDate *)getNowServiceTimeDate
{
    NSTimeInterval delta = [[[NSUserDefaults standardUserDefaults] objectForKey:@"delta"] doubleValue];
    NSDate *serviceTime = [[NSDate date] dateByAddingTimeInterval:delta];
    return serviceTime;
}


+ (NSString *)getNowServiceTimeWith:(NSDateFormatter *)dateFormatter{
    NSTimeInterval delta = [[[NSUserDefaults standardUserDefaults] objectForKey:@"delta"] doubleValue];
    //    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *serviceTime = [[NSDate date] dateByAddingTimeInterval:delta];
    return [dateFormatter stringFromDate:serviceTime];

}



@end
