//
//  CHECKFORMAT.m
//  HIGHTONG
//
//  Created by smart on 12-11-29.
//  Copyright (c) 2012年 ZhangDongfeng. All rights reserved.
//

#import "CHECKFORMAT.h"

#define EMAILFROART @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"

@implementation CHECKFORMAT

+ (BOOL)checkUserName:(NSString *)name
{
    return YES;
}

+(BOOL)checkEmailFormarwithEmail:(NSString *)email
{
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", EMAILFROART];
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"];

    return [emailTest evaluateWithObject:email];
}
//中国人的姓名

+ (BOOL)checkChineseName:(NSString *)chinesetName
{
    NSPredicate *chinesetNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^[\u4E00-\u9FA5]{2,4}$"];
    return [chinesetNameTest evaluateWithObject:chinesetName];
    
}
//昵称要求 不为空 长度小于16
+ (BOOL)checkNickName:(NSString *)nickName
{
//    NSPredicate *nickNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[\\u4E00-\\u9FFF0-9a-zA-Z_\\-]{1,16}"];

    NSPredicate *nickNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[\\u4E00-\\u9FFF0-9a-zA-Z_\\-]{1,16}"];
    return [nickNameTest evaluateWithObject:nickName];
}


//验证护照号
+(BOOL)checkPassport:(NSString *)passPortString
{
    NSPredicate *passPortStringTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^1[45][0-9]{7}|G[0-9]{8}|P[0-9]{7}|S[0-9]{7,8}|D[0-9]+$"];
    return [passPortStringTest evaluateWithObject:passPortString];

}



//验证身份证号

+ (BOOL)judgeIdentityStringValid:(NSString *)identityString {
    
    if (identityString.length != 18) return NO;
    // 正则表达式判断基本 身份证号是否满足格式
    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
    NSPredicate *identityStringPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if(![identityStringPredicate evaluateWithObject:identityString]) return NO;
    
    //** 开始进行校验 *//
    
    //将前17位加权因子保存在数组里
    NSArray *idCardWiArray = @[@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2"];
    
    //这是除以11后，可能产生的11位余数、验证码，也保存成数组
    NSArray *idCardYArray = @[@"1", @"0", @"10", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2"];
    
    //用来保存前17位各自乖以加权因子后的总和
    NSInteger idCardWiSum = 0;
    for(int i = 0;i < 17;i++) {
        NSInteger subStrIndex = [[identityString substringWithRange:NSMakeRange(i, 1)] integerValue];
        NSInteger idCardWiIndex = [[idCardWiArray objectAtIndex:i] integerValue];
        idCardWiSum+= subStrIndex * idCardWiIndex;
    }
    
    //计算出校验码所在数组的位置
    NSInteger idCardMod=idCardWiSum%11;
    //得到最后一位身份证号码
    NSString *idCardLast= [identityString substringWithRange:NSMakeRange(17, 1)];
    //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
    if(idCardMod==2) {
        if(![idCardLast isEqualToString:@"X"]||[idCardLast isEqualToString:@"x"]) {
            return NO;
        }
    }
    else{
        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
        if(![idCardLast isEqualToString: [idCardYArray objectAtIndex:idCardMod]]) {
            return NO;
        }
    }
    return YES;
}



+ (BOOL)checkPhoneNumber:(NSString *)numberStr
{
    NSPredicate * phone = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^1[3-9]\\d{9}"];//这个是比较笼统的匹配方式
//    NSPredicate * phone = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}"];//这个是最新的手机号码匹配方式
//    NSPredicate * phone = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^((13[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$"];
    return [phone evaluateWithObject:numberStr];
}
+ (BOOL)checkPassword:(NSString *)password
{
    NSPredicate * pwd = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^[a-zA-Z0-9( _!@#~$%^&)]{6,16}$"];
    return [pwd evaluateWithObject:password];
}
+ (BOOL)checkVerifyCode:(NSString *)verfyCode
{
    NSPredicate * verify = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"\\d{6}"];
    return [verify evaluateWithObject:verfyCode];
}

//...ww
+ (BOOL)checkQQNum:(NSString *)qqNum
{
    NSPredicate *QQ = [NSPredicate predicateWithFormat:@"SELE MATCHES %@",@"[1-9][0-9]{4,}"];
    return [QQ evaluateWithObject:qqNum];
}


+ (NSDictionary *)changeJsonStringToTrueJsonString:(NSString *)json
{
    NSDictionary *jsonDictionary = [NSDictionary dictionary];
    
    //替换成双引号
    NSString *validString = [json stringByReplacingOccurrencesOfString:@"(\\w+)\\s*:([^A-Za-z0-9_])"
                                                            withString:@"\"$1\":$2"
                                                               options:NSRegularExpressionSearch
                                                                 range:NSMakeRange(0, [json length])];
    
    //把'单引号替换成双引号"
    //前
    validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])'"
                                                         withString:@"$1\""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //后
    validString = [validString stringByReplacingOccurrencesOfString:@"'([:\\],\\}])"
                                                         withString:@"\"$1"
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //,前后加双引号
    validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])(\\w+)\\s*,"
                                                         withString:@"$1\"$2\","
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //去掉\r\n
    validString = [validString stringByReplacingOccurrencesOfString:@"\r\n"
                                                         withString:@""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //去掉空格
    validString = [validString stringByReplacingOccurrencesOfString:@" "
                                                         withString:@""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //去掉\t
    validString = [validString stringByReplacingOccurrencesOfString:@"\t"
                                                         withString:@""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    //去掉最后的一个单引号变成双引号
    validString = [validString stringByReplacingOccurrencesOfString:@"'"
                                                         withString:@"\""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    
    NSData *JSONData = [validString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    jsonDictionary = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    
    
    return jsonDictionary;
}


@end
