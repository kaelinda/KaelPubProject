//
//  SystermTimeControl.h
//  HIGHTONG
//
//  Created by apple on 15/4/17.
//  Copyright (c) 2015年 HTYunjiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystermTimeControl : NSObject
+(NSDate *)GetSystemTimeWith;
// 获取当前服务器时间和系统时间的时间差（单位秒）
+(NSString *)GetNowTimeIntervalHTSystemTimeString;
// 获取现在服务器的时间
+ (NSString *)getNowServiceTimeString;
+ (NSDate *)getNowServiceTimeDate;
+ (NSString *)getNowServiceTimeWith:(NSDateFormatter *)dateFormatter;
@end
