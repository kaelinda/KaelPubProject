//
//  LBIdentifierManager.m
//  DemoforExercise
//
//  Created by apple on 14/12/12.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import "LBIdentifierManager.h"

@implementation LBIdentifierManager
static NSString * const KEY_IN_KEYCHAIN = @"Hightong";
static NSString * const KEY_PASSWORD = @"HightongIdentifierPassword";

+(void)savePassWord:(NSString *)password
{
    NSMutableDictionary *usernamepasswordKVPairs = [NSMutableDictionary dictionary];
    [usernamepasswordKVPairs setObject:password forKey:KEY_PASSWORD];
    [LBKeyChain save:KEY_IN_KEYCHAIN data:usernamepasswordKVPairs];
}

+(id)readPassWord
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[LBKeyChain load:KEY_IN_KEYCHAIN];
    return [usernamepasswordKVPair objectForKey:KEY_PASSWORD];
}

+(void)deletePassWord
{
    [LBKeyChain delete:KEY_IN_KEYCHAIN];
}

@end
