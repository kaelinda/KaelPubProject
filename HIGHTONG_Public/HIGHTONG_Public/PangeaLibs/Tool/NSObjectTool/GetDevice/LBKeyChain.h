//
//  LBKeyChain.h
//  DemoforExercise
//
//  Created by apple on 14/12/12.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Security/Security.h>
#import <Security/Security.h>

@interface LBKeyChain : NSObject
+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;
@end
