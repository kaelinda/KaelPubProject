//
//  VideoCompressTool.m
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/7.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import "VideoCompressTool.h"
#import <AVFoundation/AVFoundation.h>

@implementation VideoCompressTool

- (id)init
{
    
    self = [super init];
    
    if (self) {

        
    }
    
    return self;
}


- (void)compressAssetUrlWithCompressionType:(NSString *)compressionTypeStr andCompressionUrl:(NSString *)compressionTypeUrl andReturn:(VideoCompressTool_Block)returnBlock
{
    NSURL *originalUrl = [NSURL URLWithString:compressionTypeUrl];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];//用时间戳给文件重命名，避免冲突
    [formatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    
    //    NSData *data = [NSData dataWithContentsOfURL:originalUrl];
    //    CGFloat totalSize = (float)data.length / 1024 / 1024;
    //    NSLog(@"压缩前的大小\n %f",totalSize);
    
    NSURL *compressedUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mov",[formatter stringFromDate:[NSDate date]]]];//存在沙盒中的路径
    NSLog(@"自己沙盒中的URL\n %@",compressedUrl);
    
    
    //是否压缩部分
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:originalUrl options:nil];//用原始URL初始化
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:compressionTypeStr];
    exportSession.outputURL = compressedUrl;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse = YES;
        
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void){
        switch (exportSession.status){
            case AVAssetExportSessionStatusCancelled:
            {
                NSLog(@"AVAssetExportSessionStatusCancelled");
                break;
            }
            case AVAssetExportSessionStatusUnknown:
            {
                NSLog(@"AVAssetExportSessionStatusUnknown");
                break;
            }
            case AVAssetExportSessionStatusWaiting:
            {
                NSLog(@"AVAssetExportSessionStatusWaiting");
                break;
            }
            case AVAssetExportSessionStatusExporting:
            {
                NSLog(@"AVAssetExportSessionStatusExporting");
                break;
            }
            case AVAssetExportSessionStatusCompleted:
            {
                NSLog(@"压缩成功");
                    
                NSData *tempData = [NSData dataWithContentsOfURL:compressedUrl];
                CGFloat tempTotalSize = (float)tempData.length / 1024 / 1024;
                NSLog(@"压缩后的大小\n %f",tempTotalSize);
                    
                returnBlock(0,compressedUrl);
                
//                HTUGCUpLoadInterface *ugcRequest = [HTUGCUpLoadInterface shareInstance];
//                [ugcRequest UGCUpLoadVideoWithVideoUrl:compressedUrl andVideoName:@"" andReturn:^(NSInteger status, NSMutableDictionary *result) {
//                        
//                } andSpeedReturn:^(NSString *speed, CGFloat uploadProgress) {
//                        
//                }];
                
                break;
            }
            case AVAssetExportSessionStatusFailed:
            {
                NSLog(@"压缩失败啦");
                
                returnBlock(1,nil);
                
                break;
            }
        }
    }];
   
}

- (void)removeCompressedVideoFromDocumentsWithFilePath:(NSString *)filePath {
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}


@end
