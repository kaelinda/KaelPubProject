//
//  KaelTool.m
//  HIGHTONG
//
//  Created by Kael on 15/7/16.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "KaelTool.h"

#import <CoreImage/CoreImage.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Accelerate/Accelerate.h>
#import "SystermTimeControl.h"
#import "UrlPageJumpManeger.h"
@interface KaelTool (){
    
    
    
    
    
}

@property (nonatomic,strong)UIImageView *bgImageView;

@end

@implementation KaelTool

+(NSString *)getTruthTokenWith:(NSString *)token{
    //如果是48位 则需要截取其前32位 作为业务token来使用
    if (token.length ==  48) {
        token = [token substringToIndex:32];
        
    }
    if (token.length == 32) {
        return token;
        
    }
    if (token.length == 0) {
        token = @"";
        
    }
    
    return token;
}

/**
 *  @author Kael
 *
 *  @brief 获取时间戳
 *  @return 时间戳
 */
+(NSString *)getTimeStampWith:(NSDateFormatter *)formatter{
    
    NSString* dateStr;
    if (formatter.dateFormat.length == 0) {
        //[formatter setDateFormat:@"YYYY.MM.dd.hh.mm.ss"];
        [formatter setDateFormat:@"YYYYMMddHHmmss"];//这里要求精确到毫秒
    }
    dateStr = [formatter stringFromDate:[NSDate date]];
    NSString *msgID = [[NSString alloc] initWithFormat:@"%@",dateStr];//这里有些啰嗦了 直接可以 return dateStr;
    
    return msgID.length>0 ? msgID : @"";
}


+(NSString *)getRquestSignValue{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYYMMddHHmmss";
//    NSString * timeStamp= [KaelTool getTimeStampWith:formatter];
    NSString * timeStamp= [SystermTimeControl getNowServiceTimeWith:formatter];

    NSString *versionNum = @"00";
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *AES_Key = [[NSUserDefaults standardUserDefaults] objectForKey:@"AES_Key"];
    
    //业务token + 时间戳 用秘钥 AES_Key进行AES加密
    NSString *AES_result = [HT_AESCrypt encrypt:[NSString stringWithFormat:@"%@%@",token,timeStamp] password:AES_Key];
    
    NSMutableString *sign = [NSMutableString stringWithFormat:@"%@%@%@",versionNum,token,AES_result];
    
    return sign.length>0 ? sign : @"";
}

+(NSString *)getRquestSignValueWith:(NSString *)token andVersionNum:(NSString *)versionNum{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYYMMddHHmmss";
//    NSString * timeStamp= [KaelTool getTimeStampWith:formatter];
    NSString * timeStamp= [SystermTimeControl getNowServiceTimeWith:formatter];
    NSString *AES_Key;
    
    if (token.length == 48) {
        AES_Key = [token substringFromIndex:32];
        token = [token substringToIndex:32];
        
    }else if(token.length == 32){
        //32位的时候不应该上传签名参数
        return @"";
    }else{
        //如果不是48位 也不是32位 那就直接将token参数返回出去吧
        NSLog(@"这个token不对吧");
        return token.length>0 ? token :@"";
    }
    
    //业务token + 时间戳 用秘钥 AES_Key进行AES加密
    NSString *AES_result = [HT_AESCrypt encrypt:[NSString stringWithFormat:@"%@%@",token,timeStamp] password:AES_Key];
    
    NSMutableString *sign = [NSMutableString stringWithFormat:@"%@%@%@",versionNum,token,AES_result];
    return sign.length>0 ? sign : @"";
}



+(NSMutableString *)deleteStringArr:(NSArray *)DStrArr fromString:(NSMutableString *)FStr{

    for (NSString *itemStr in DStrArr) {
        [KaelTool deleteString:itemStr fromString:FStr];
    }
    return FStr;
}

+(NSString *)adjustUIWebViewURLWith:(NSString *)urlStr{
    NSMutableString *m_urlStr = [NSMutableString stringWithFormat:@"%@",urlStr];
    
    m_urlStr = [NSMutableString stringWithFormat:@"%@",[KaelTool deleteString:@" " fromString:m_urlStr]];
    m_urlStr = [NSMutableString stringWithFormat:@"%@",[KaelTool deleteString:@"\n" fromString:m_urlStr]];
    m_urlStr = [NSMutableString stringWithFormat:@"%@",[KaelTool deleteString:@"\r" fromString:m_urlStr]];

    NSString *http_prefix = @"http://";
    NSString *https_prefix = @"https://";
    if (![m_urlStr hasPrefix:http_prefix] && ![m_urlStr hasPrefix:https_prefix]) {
        [m_urlStr insertString:http_prefix atIndex:0];
    }
    return m_urlStr;
}


+(NSMutableString *)deleteString:(NSString *)DStr fromString:(NSMutableString *)FStr{
    NSMutableString *mFstr = [[NSMutableString alloc] initWithFormat:@"%@",FStr];
    while ([mFstr rangeOfString:DStr].location != NSNotFound) {
        NSInteger location = [mFstr rangeOfString:DStr].location;
        [mFstr deleteCharactersInRange:NSMakeRange(location, DStr.length)];
    }
    return mFstr;
}

+(UIButton *)setButtonPropertyWithButtonType:(UIButtonType)buttonType andBackGroundColor:(UIColor *)backgroundColor andCornerRadius:(CGFloat)cornerRadius andNormalImage:(UIImage *)normalImg andHighlightedImage:(UIImage *)highlightedImage{

    UIButton *button = [UIButton buttonWithType:buttonType];
    button.backgroundColor = backgroundColor;
    button.layer.cornerRadius = cornerRadius;
    if (normalImg) {
        [button setImage:normalImg forState:UIControlStateNormal];
    }
    if (highlightedImage) {
        [button setImage:highlightedImage forState:UIControlStateHighlighted];
    }
    

    return button;
}

+(UILabel *)setlabelPropertyWithBackgroundColor:(UIColor *)backgroundColor andNSTextAlignment:(NSTextAlignment)textAlignment andTextColor:(UIColor *)textColor andFont:(UIFont *)font{
    UILabel *KLabel = [[UILabel alloc] init];
    
    [KLabel setBackgroundColor:backgroundColor];
    [KLabel setTextAlignment:textAlignment];
    [KLabel setTextColor:textColor];
    [KLabel setFont:font];
    
    return KLabel;
}


+(UIButton *)setButtonPropertyWithBackgroundNormalImageName:(NSString *)normalImageName andBackgroundHighlightedImageName:(NSString *)highlightedImageName andBackgroundSelectedImageName:(NSString *)selectedImageName andTarget:(id)target andSelect:(SEL)isSelect
{
    UIButton *Fbutton = [[UIButton alloc] init];
    [Fbutton setBackgroundImage:[UIImage imageNamed:normalImageName] forState:UIControlStateNormal];
    [Fbutton setBackgroundImage:[UIImage imageNamed:highlightedImageName] forState:UIControlStateHighlighted];
    [Fbutton setBackgroundImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    
    [Fbutton addTarget:target action:isSelect forControlEvents:UIControlEventTouchUpInside];
    
    return Fbutton;
}


+(NSString *)getFormOrAfterDateWithDays:(NSInteger )days{
    
    NSDateFormatter *mydateformat = [[NSDateFormatter alloc] init];
    [mydateformat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *mydate = [NSDate date];
    
    if (days>=0) {
        mydate = [mydate dateByAddingDays:days];
        
    }else{
        
        mydate = [mydate dateBySubtractingDays:-days];
    }
    
    NSString *mydateStr= [mydateformat stringFromDate:mydate];
    return mydateStr;
}
+(NSMutableArray *)increaseArrayWithArray:(NSArray *)arr andKey:(NSString *)key{
    NSMutableArray *mArr = [[NSMutableArray alloc] initWithArray:arr];
    for (int j=0; j<mArr.count; j++) {
        
        for (int k=0; k<(mArr.count-1); k++) {
            NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithDictionary:[mArr objectAtIndex:j]];
            NSMutableDictionary *dic2 = [[NSMutableDictionary alloc] initWithDictionary:[mArr objectAtIndex:k]];
            
            if ([[dic1 objectForKey:key] integerValue] < [[dic2 objectForKey:key] integerValue]) {
                [mArr exchangeObjectAtIndex:j withObjectAtIndex:k];
                continue;
            }
            
        }
        
    }
    return mArr;
}

+(NSMutableArray *)decreaseArrayWithArray:(NSArray *)arr andKey:(NSString *)key{
    NSMutableArray *mArr = [[NSMutableArray alloc] initWithArray:arr];
    for (int j=0; j<mArr.count; j++) {
        
        for (int k=0; k<(mArr.count-1); k++) {
            NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithDictionary:[mArr objectAtIndex:j]];
            NSMutableDictionary *dic2 = [[NSMutableDictionary alloc] initWithDictionary:[mArr objectAtIndex:k]];
            
            if ([[dic1 objectForKey:key] integerValue] > [[dic2 objectForKey:key] integerValue]) {
                [mArr exchangeObjectAtIndex:j withObjectAtIndex:k];
                continue;
            }
            
        }
        
    }
    return mArr;
    
}
+(void)scaleBigAndHiddenAnamition:(UIView *)view andTime:(CGFloat)animationTime andScale:(CGFloat)scale andAnimationComplet:(void (^)())completBlock{
//    CGFloat bigRate = 1.3;
//    CGFloat animationTime = 0.75;
//    CGFloat fromWidth = view.frame.size.width;
//    CGFloat fromHeight = view.frame.size.height;
//    CGPoint fromCenter = view.center;
//    
//    CGFloat toWidth = fromWidth * scale;
//    CGFloat toHeight = fromHeight * scale;
    
    CABasicAnimation *animation1=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation1.fromValue=[NSNumber numberWithFloat:1.0];
    animation1.toValue=[NSNumber numberWithFloat:0.0];
    animation1.repeatCount=1;
    animation1.duration=animationTime;
    animation1.removedOnCompletion=NO;
    animation1.fillMode=kCAFillModeForwards;
    animation1.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CABasicAnimation *animation2=[CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation2.fromValue=[NSNumber numberWithFloat:1.0];
    animation2.toValue=[NSNumber numberWithFloat:scale];
    animation2.duration=animationTime;
    animation2.autoreverses=YES;
    animation2.repeatCount=1;
    animation2.removedOnCompletion=NO;
    animation2.fillMode=kCAFillModeForwards;


    NSArray *animationArr = @[animation1,animation2];
    
    CAAnimationGroup *animation=[CAAnimationGroup animation];
    animation.animations=animationArr;
    animation.duration=animationTime;
    animation.repeatCount=1;
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeForwards;
    
    [view.layer addAnimation:animation forKey:@""];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completBlock();
    });
    
    
//    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
//    
//    opacityAnimation.toValue = @(0);
//    opacityAnimation.duration = 2.0;
//    
//    [view.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
//    [opacityAnimation setCompletionBlock:^(POPAnimation *animation, BOOL finished) {
//        view.hidden = YES;
//    }];
//    
//    
//    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
//    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.8, 1.8)];
//    scaleAnimation.springBounciness = 4.f;
//    [view pop_addAnimation:scaleAnimation forKey:@"scaleAnim"];
//    
//    [scaleAnimation setCompletionBlock:^(POPAnimation *animation, BOOL finished) {
////        view.hidden = YES;
//    
//    }];
    

}


+(void)scaleBigAnimation:(UIView *)view{
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.5, 1.5)];
    scaleAnimation.springBounciness = 4.f;
    [view pop_addAnimation:scaleAnimation forKey:@"scaleAnim"];
    
    [scaleAnimation setCompletionBlock:^(POPAnimation *animation, BOOL finished) {
        [self scaleSamllAnimation:view];
    }];
    
}
+(void)scaleSamllAnimation:(UIView *)view{
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    scaleAnimation.springBounciness = 20.f;
    [view pop_addAnimation:scaleAnimation forKey:@"scaleAnim"];
    
    
    
}

+ (UIImage*)blurredImageWith:(UIImage *)image
{
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:image.CGImage];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:10.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return returnImage;
}

-(UIImage *)getBlurImageWith:(NSString *)urlStr{
    if (urlStr.length<2) {
        
        return nil;
    }
    NSURL *imageURL = [NSURL URLWithString:urlStr];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *image = [CIImage imageWithContentsOfURL:imageURL];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:image forKey:kCIInputImageKey];
    [filter setValue:@2.0f forKey: @"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef outImage = [context createCGImage: result fromRect:[result extent]];
    UIImage * blurImage = [UIImage imageWithCGImage:outImage];
    return blurImage;
}






-(void)show
{
    
    UIImage* img = [self getBlurImage:[UIImage imageNamed:@"Default-568h.png"]];
    [_bgImageView setImage:img];
    
}


-(UIImage*)getBlurImage:(UIImage*)image

{
    return nil;
//    return [self gaussBlur:0.2 andImage:image];
    
}
+ (UIImage *)gaussBlur:( CGFloat )blurLevel andImage:( UIImage *)originImage
{
    
    blurLevel = MIN ( 1.0 , MAX ( 0.0 , blurLevel));
    //int boxSize = (int)(blurLevel * 0.1 * MIN(self.size.width, self.size.height));
    int boxSize = 50 ; // 模糊度。
    boxSize = boxSize - (boxSize % 2 ) + 1 ;
    NSData *imageData = UIImageJPEGRepresentation (originImage, 1 );
    UIImage *tmpImage = [ UIImage imageWithData :imageData];
    CGImageRef img = tmpImage. CGImage ;
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    void *pixelBuffer;
    
    //create vImage_Buffer with data from CGImageRef
    CGDataProviderRef inProvider = CGImageGetDataProvider (img);
    CFDataRef inBitmapData = CGDataProviderCopyData (inProvider);
    inBuffer. width = CGImageGetWidth (img);
    inBuffer. height = CGImageGetHeight (img);
    
    inBuffer. rowBytes = CGImageGetBytesPerRow (img);
    
    inBuffer. data = ( void *) CFDataGetBytePtr (inBitmapData);
    
    //create vImage_Buffer for output
    
    pixelBuffer = malloc ( CGImageGetBytesPerRow (img) * CGImageGetHeight (img));
    
    outBuffer. data = pixelBuffer;
    
    outBuffer. width = CGImageGetWidth (img);
    
    outBuffer. height = CGImageGetHeight (img);
    
    outBuffer. rowBytes = CGImageGetBytesPerRow (img);
    
    NSInteger windowR = boxSize/ 2 ;
    
    CGFloat sig2 = windowR / 3.0 ;
    
    if (windowR> 0 ){ sig2 = - 1 /( 2 *sig2*sig2); }
    
    int16_t *kernel = ( int16_t *) malloc (boxSize* sizeof ( int16_t ));
    
    int32_t   sum = 0 ;
    
    for ( NSInteger i= 0 ; i<boxSize; ++i){
        
        kernel[i] = 255 * exp (sig2*(i-windowR)*(i-windowR));
        
        sum += kernel[i];
        
    }
    
    free (kernel);
    
    // convolution
    
    error = vImageConvolve_ARGB8888 (&inBuffer, &outBuffer, NULL , 0 , 0 , kernel, boxSize, 1 , sum, NULL , kvImageEdgeExtend );
    
    error = vImageConvolve_ARGB8888 (&outBuffer, &inBuffer, NULL , 0 , 0 , kernel, 1 , boxSize, sum, NULL , kvImageEdgeExtend );
    
    outBuffer = inBuffer;
    
    if (error) {
        
        //NSLog(@"error from convolution %ld", error);
        
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB ();
    
    CGContextRef ctx = CGBitmapContextCreate (outBuffer. data ,
                                              
                                              outBuffer. width ,
                                              
                                              outBuffer. height ,
                                              
                                              8 ,
                                              
                                              outBuffer. rowBytes ,
                                              
                                              colorSpace,
                                              
                                              kCGBitmapAlphaInfoMask & kCGImageAlphaNoneSkipLast );
    
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    
    UIImage *returnImage = [ UIImage imageWithCGImage :imageRef];
    
    //clean up
    
    CGContextRelease (ctx);
    
    CGColorSpaceRelease (colorSpace);
    
    free (pixelBuffer);
    
    CFRelease (inBitmapData);
    
    CGImageRelease (imageRef);
    
    return returnImage;
    
}




+ (UIImage*) maskImage:(UIImage*)image withMask:(UIImage*)mask {
    
    CGImageRef imgRef = [image CGImage];CGImageRef maskRef = [mask CGImage];
    CGImageRef actualMask =CGImageMaskCreate(CGImageGetWidth(maskRef),
                      CGImageGetHeight(maskRef),
                      CGImageGetBitsPerComponent(maskRef),
                      CGImageGetBitsPerPixel(maskRef),
                      CGImageGetBytesPerRow(maskRef),
                      CGImageGetDataProvider(maskRef), NULL, false);
    CGImageRef masked = CGImageCreateWithMask(imgRef,actualMask);
    return [UIImage imageWithCGImage:masked];
}

+ (UIImage *) resizeImage:(UIImage *)image size:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();UIGraphicsEndImageContext();
    return newImage;
}

-(void)getMaskWith:(UIImage *)image{
//    UIImage *newImg = [Utils resizeImage:orgImg size:CGSizeMake(120, 120)];
//    UIImage *maskImg = [UIImage imageNamed:@"mask.png"];
//    // 取得mask的圖片物件
//    newImg = [ImageUtils maskImage:newImg withMask:maskImg];
//    // 開始做裁切(Clip)圖片
//
//    return newImg;
}



#pragma mark - 获取制定尺寸的图片

+(UIImage*) OriginImage:(UIImage*)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);//size为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0,0, size.width, size.height)];
    
    UIImage* scaledImage =UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}



+ (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

#pragma mark - 分享链接处理
+(NSString *)getChannelId:(NSString*)channelId andChannelTitle:(NSString*)channelTitle withPlayType:(CurrentPLAYType)type
{
    NSString *healthsharelink = [[NSString alloc]init];
    NSString *relaxsharelink = [[NSString alloc]init];
    switch (type) {
        case 0:
        case 1:
        case 2:
        case 3:
            return SHARE_LINK;
            break;
        case 4:
            if ([UrlPageJumpManeger healthyShareLinkWithChannelID:channelId].length == 0) {
                return SHARE_LINK;
            }
            else
            {
                healthsharelink = [UrlPageJumpManeger healthyShareLinkWithChannelID:channelId];
                
                return healthsharelink;
            }

            break;
        case 5:
            
            
            if ([UrlPageJumpManeger relaxShareLinkWithChannelID:channelId andChannelName:channelTitle].length == 0) {
                return SHARE_LINK;
            }
            else
            {
                relaxsharelink = [UrlPageJumpManeger relaxShareLinkWithChannelID:channelId andChannelName:channelTitle];
                
                return relaxsharelink;
            }
            
            break;

        default:
            break;
    }
}



@end
