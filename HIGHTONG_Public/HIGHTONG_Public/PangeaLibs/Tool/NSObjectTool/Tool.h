//
//  Tool.h
//  HIGHTONG
//
//  Created by testteam on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIButton_Block.h"

typedef NS_ENUM(NSUInteger, VideoType) {
    kLiveVideo,//直播
    kDemandVideo,//点播
    kPolymedicineVideo,//医视频
};

//视频事件类型
typedef NS_ENUM(NSUInteger, VideoEventType) {
    kBackLookVideoEvent,//回看
    kLiveVideoEvent,//直播
    kOrderVideoEvent,//预约
};

@interface Tool : NSObject


/**
 *  打印按钮的id和赋值的字典
 *
 *  @param btn 要打的按钮
 */
+ (void)print_Button_block:(UIButton_Block*)btn;

/**
 *  随机颜色
 *
 *  @return 随机颜色
 */
+ (UIColor *)randomColor;

/**
 *  返回区间数值，scrollView 滑动用
 *
 *  @param num 滑动返回数值
 *
 *  @return 滑动  0--0.5 返回  0
 */
+ (NSInteger)returnRangeNumb:(float)num;

/**
 *  目标到当前距离
 *
 *  @param togal 目标页数
 *  @param index 当前页数
 *
 *  @return 是否在0.5之内    
 */
+ (BOOL)togal:(NSInteger) togal WidthIndex:(NSInteger)index;


//将直播搜索结果分类成这种类型的数据
/**
 *  将直播搜索结果分类成这种类型的数据
 *
 *  @param array 原始数据
 *
 *  @return 返回的格式数组
 */
+ (NSMutableDictionary*)dateTypeWith:(NSArray*)array;
//+ (NSMutableArray*)dateTypeWith:(NSArray*)array;
+ (NSMutableDictionary*)CQdateTypeWith:(NSArray *)array;

/**
 *  获得到周一周二周三这个类型数据，从搜索结果中转化
 *
 *  @param array 搜索结果页
 *
 *  @return 格式化好的结果
 */
+ (NSMutableDictionary*)dateTypeWithArray:(NSArray*)array;


//时间转化展示方式
+ (NSString*)YearMouthDayWithString:(NSString*)string;



//这个是计算有多少个cell的
+ (NSInteger)numberOfCellRowsWithArray:(NSArray*)array withRowNum:(NSInteger)num;

//这个是取出相应行cell 中的那2个数据放到  programlist的array中
+ (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index withRowNum:(NSInteger)num;

//这个是取出相应行cell 中的那三个数据放到  programlist的array中
+ (NSArray*)arrayFOR_ALLArray:(NSArray*  ) array withBeginnumb:(NSInteger)index;

/**
 *  时间转换为
 *
 *  @param secend 时间数
 *
 *  @return 字符串
 */
+ (NSString*)secendtoTime:(NSInteger)secend;


/**
 *  计算字符串为周几
 *
 *  @param inputDate 时间
 *
 *  @return 时间字符
 */
+ (NSString *)weekDayStringFromDate:(NSString *)inputDate;

/**
 *  日期转为月 日
 *
 *  @param inputDate 输入时间串
 *
 *  @return 输出格式时间串
 */
+ (NSString *)MMddStringFromDate:(NSString *)inputDate;



/**
 *  日期转为yyyy-MM-dd HH-mm-ss  转换为->   yyyy-MM-dd
 *
 *  @param inputDate 输入时间串 yyyy-MM-dd HH-mm-ss
 *
 *  @return 输出格式时间串 yyyy-MM-dd
 */
+ (NSString *)MMddssToyyMMddStringFromDate:(NSString *)inputDate;

/**
 *  按照给定格式转换时间字符串
 *
 *  @param inputDate 输入时间
 *  @param formatr   输入格式化字符串
 *
 *  @return 返回格式过的字符串
 */
+(NSString *)MMddssToyyMMddStringFromDate:(NSString *)inputDate andFormaterString:(NSString*)formaStr;


/**
 *  直播的类型
 *
 *  @param startTime 开始时间
 *  @param endTime   结束时间
 *
 *  @return 播放类型   -1  回看   0 直播中  1 预约
 */
+(NSInteger)isPlayingWith:(NSString *)startTime endTime:(NSString *)endTime;

/**
 *  比较时间
 *
 *  @param startTime 开始
 *  @param endTime   结束
 *
 *  @return 大小
 */
+(NSInteger)compareTime:(NSString *)startTime endTime:(NSString *)endTime;

//判断当前是否正在直播    1是预约   0 正在直播  -1回看
+ (NSInteger)typePlayStartTime:(NSString*)start andEndTime:(NSString*)end;
@end
