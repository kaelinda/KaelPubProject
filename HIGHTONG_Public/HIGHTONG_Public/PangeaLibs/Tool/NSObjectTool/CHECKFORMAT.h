//
//  CHECKFORMAT.h
//  HIGHTONG
//
//  Created by smart on 12-11-29.
//  Copyright (c) 2012年 ZhangDongfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHECKFORMAT : NSObject

//检查用户明

+ (BOOL)checkUserName:(NSString *)name;

+ (BOOL)checkChineseName:(NSString *)chinesetName;


//验证护照
+(BOOL)checkPassport:(NSString *)passPortString;

//验证身份证号
+ (BOOL)judgeIdentityStringValid:(NSString *)identityString;
+ (BOOL)checkPhoneNumber:(NSString *)numberStr;

+ (BOOL)checkPassword:(NSString *)password;

+ (BOOL)checkVerifyCode:(NSString *)verfyCode;

+ (BOOL)checkNickName:(NSString *)nickName;

//...ww
+ (BOOL)checkQQNum:(NSString *)qqNum;

//检查密码

//检查邮箱
+ (BOOL)checkEmailFormarwithEmail:(NSString *)email;

//用于手机归属地接口返回过来不是标准的json进行了能够解析的JSON
+ (NSDictionary *)changeJsonStringToTrueJsonString:(NSString *)json;


@end
