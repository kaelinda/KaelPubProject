//
//  VideoCompressTool.h
//  HIGHTONG_Public
//
//  Created by 宋博闻 on 2017/4/7.
//  Copyright © 2017年 ÂàõÁª¥Êµ∑ÈÄö. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^VideoCompressTool_Block)(NSInteger status, NSURL *videoUrl);

@interface VideoCompressTool : NSObject

- (void)compressAssetUrlWithCompressionType:(NSString *)compressionTypeStr andCompressionUrl:(NSString *)compressionTypeUrl andReturn:(VideoCompressTool_Block)returnBlock;


- (void)removeCompressedVideoFromDocumentsWithFilePath:(NSString *)filePath;

@end
