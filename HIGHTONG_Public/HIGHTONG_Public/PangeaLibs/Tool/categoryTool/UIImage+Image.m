//
//  UIImage+Image.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "UIImage+Image.h"
#import <objc/message.h>
@implementation UIImage (Image)

// 加载这个分类的时候调用
+ (void)load
{
    
    // 交换方法实现,方法都是定义在类里面
    // class_getMethodImplementation:获取方法实现
    // class_getInstanceMethod:获取对象
    // class_getClassMethod:获取类方法
    // IMP:方法实现
    
    // imageNamed
    // Class:获取哪个类方法
    // SEL:获取方法编号,根据SEL就能去对应的类找方法
    Method imageNameMethod = class_getClassMethod([UIImage class], @selector(imageNamed:));
    
    // ht_imageNamed
    Method ht_imageNamedMethod = class_getClassMethod([UIImage class], @selector(ht_imageNamed:));
    
    // 交换方法实现
    method_exchangeImplementations(imageNameMethod, ht_imageNamedMethod);
    
}



+ (__kindof UIImage *)ht_imageNamed:(NSString *)imageName
{
    
    
    // 1.加载图片
    UIImage *image = [UIImage ht_imageNamed:imageName];
    NSString *IMage= [NSString stringWithFormat:@"加载*%@*路径的图片为空，小心没有图片",imageName];
    // 2.判断功能
    if (image == nil) {
#warning 图片加载为空
        //用断言不好，先不用了
        //        NSAssert(image = nil,IMage);
        NSLog(@"%@",IMage);
    }
    
    return image;
}

@end
