//
//  UIImageView+ImageView.m
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "UIImageView+ImageView.h"

@implementation UIImageView (ImageView)
-(UIImage*)circularImage
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0);
    
    CGContextRef textRef = UIGraphicsGetCurrentContext();
    
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    
    CGContextAddEllipseInRect(textRef, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
@end
