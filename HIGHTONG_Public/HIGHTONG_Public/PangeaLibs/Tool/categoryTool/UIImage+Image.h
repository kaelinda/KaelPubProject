//
//  UIImage+Image.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/13.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)
+ (__kindof UIImage *)ht_imageNamed:(NSString *)imageName;

@end
