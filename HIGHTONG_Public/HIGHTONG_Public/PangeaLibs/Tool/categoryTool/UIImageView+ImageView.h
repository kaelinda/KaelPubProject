//
//  UIImageView+ImageView.h
//  HIGHTONG_Public
//
//  Created by lailibo on 16/6/14.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageView)
-(UIImage*)circularImage;
@end
