//
//  ShareButton.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ShareButton.h"

@implementation ShareButton

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initSubView];
    }
    return self;

}

-(void)initSubView{
    WS(wself);
    
    _imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _imageBtn.backgroundColor = [UIColor clearColor];
    _imageBtn.layer.cornerRadius = 5;
    _imageBtn.tag = self.tag;
    [_imageBtn addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_imageBtn];
    
    CGSize btnSize = CGSizeMake(50*kRateSize, 50*kRateSize);
    [_imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself).offset(1);
        make.left.mas_equalTo(wself.mas_left).offset(1);
        make.size.mas_equalTo(btnSize);
        
        
    }];
    
    
    _titleLabel = [[UILabel alloc] init];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
//    _titleLabel.font = [UIFont fontWithName:HEITI_Light_FONT size:8.0f];
    _titleLabel.font = [UIFont systemFontOfSize:10*kRateSize];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
    
    CGSize labelSize = CGSizeMake(52*kRateSize, 15*kRateSize);
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_imageBtn.mas_bottom).offset(2*kRateSize);
        make.left.mas_equalTo(_imageBtn).offset(-2*kRateSize);
        make.size.mas_equalTo(labelSize);
    }];


}


-(void)shareAction:(UIButton *)btn{

    if ([self.delegate respondsToSelector:@selector(shareBtnSelected:)]) {
        [self.delegate shareBtnSelected:btn];
    }
    
}

-(void)setBtnWithNormalImage:(UIImage *)normalImage andHighlightedImage:(UIImage *)highlightedImage andTitle:(NSString *)title{
    _imageBtn.tag = self.tag;
    [_imageBtn setImage:normalImage forState:UIControlStateNormal];
    [_imageBtn setImage:highlightedImage forState:UIControlStateHighlighted];
    
    _titleLabel.text = title;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
