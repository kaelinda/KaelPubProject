//
//  ShareView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "ShareView.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import <UMSocialCore/UMSocialCore.h>

@implementation ShareView

-(instancetype)init{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        widht = kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight;
        hight = KDeviceHeight>kDeviceWidth?KDeviceHeight:kDeviceWidth;
        
        BORateSize = widht/320;
        
    [self initUI];
    }
    return self;
}


- (void)UMSocialShareWithAppLinkUrl:(NSString *)linkUrl shareContent:(NSString *)contentStr shareImage:(NSString *)imageUrl shareType:(LandscapeShareType)shareType currentController:(UIViewController *)currentController
{
    [self shareWebPageToPlatformType:shareType andContentString:contentStr andLinkUrl:linkUrl andImageUrl:imageUrl andCurrentVC:currentController];
}


-(id)resetThumbImage:(NSString *)image
{
    
    if([image hasPrefix:@"https"])
    {
        return image;
    }else
    {
        return [UIImage imageNamed:@"ic_launcher"];
    }
    
}


-(void)shareWebPageToPlatformType:(LandscapeShareType)shareType andContentString:(NSString *)contentString andLinkUrl:(NSString*)linkUrl andImageUrl:(NSString*)imageUrl andCurrentVC:(UIViewController*)currentViewController
{
    
    UMSocialPlatformType platformType;
    switch (shareType) {
        case LsWechat:
        {
            platformType = UMSocialPlatformType_WechatSession;
        }
            break;
        case LsWechatZone:
        {
            platformType = UMSocialPlatformType_WechatTimeLine;
            
        }
            break;
        case LsQQ:
        {
            platformType = UMSocialPlatformType_QQ;
            
        }
            break;
        case LsQQZone:
        {
            platformType = UMSocialPlatformType_Qzone;
            
        }
            break;
            
        default:
            break;
    }
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:contentString descr:contentString thumImage:[self resetThumbImage:imageUrl]];
    //设置网页地址
    shareObject.webpageUrl = linkUrl;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
#ifdef UM_Swift
    [UMSocialSwiftInterface shareWithPlattype:platformType messageObject:messageObject viewController:self completion:^(UMSocialShareResponse * data, NSError * error) {
#else
        //调用分享接口
        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:currentViewController completion:^(id data, NSError *error) {
#endif
            if (error) {
                UMSocialLogInfo(@"************Share fail with error %@*********",error);
            }else{
                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                    UMSocialShareResponse *resp = data;
                    //分享结果消息
                    UMSocialLogInfo(@"response message is %@",resp.message);
                    //第三方原始返回的数据
                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                    NSLog(@"分享成功！");
                    if (_shareSuccessBlock) {
                        _shareSuccessBlock(1);
                    }
                    
                }else{
                    UMSocialLogInfo(@"response data is %@",data);
                }
            }
            NSLog(@"kjkjk分享不成功");
            if (_shareSuccessBlock) {
                _shareSuccessBlock(2);
            }
            
            //            [self alertWithError:error];
        }
    ];
}



-(void)initUI{
    WS(wself);
    _BGImageView = [[UIImageView alloc] init];
    [_BGImageView setBackgroundColor:[UIColor blackColor]];
    _BGImageView.alpha = 0.7;

    [self addSubview:_BGImageView];
    
    [_BGImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself);
    }];
    
    
    _centerBackView = [[UIView alloc] init];
//    [_centerBackView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_share"]]];
    _centerBackView.userInteractionEnabled = YES;
    [_centerBackView setBackgroundColor:UIColorFromRGB(0xffffff)];
    _centerBackView.layer.cornerRadius = 6;
    [self addSubview:_centerBackView];
    
    CGSize backSize = CGSizeMake(250*BORateSize , 140*BORateSize);
    [_centerBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(backSize);
        make.center.mas_equalTo(wself);
        
    }];
    
    _shareTitleLabel = [[UILabel alloc] init];
    [_shareTitleLabel setBackgroundColor:[UIColor clearColor]];
    _shareTitleLabel.text = @"分享给朋友";
    _shareTitleLabel.tag = 111;
    [_shareTitleLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:18.0f]];
    [_shareTitleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [_centerBackView addSubview:_shareTitleLabel];
//    CGSize labelSize = CGSizeMake(200*BORateSize , 30*BORateSize);
    [_shareTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_centerBackView.mas_top);
        make.left.mas_equalTo(_centerBackView.mas_left).offset(8*BORateSize);
        make.right.mas_equalTo(_centerBackView.mas_right).offset(-10*BORateSize);
        make.height.mas_equalTo(30*BORateSize);
        
    }];
    
    for (int i=0; i<4; i++) {
        ShareButton *shareBtn = [[ShareButton alloc] init];
        shareBtn.delegate = self;
        shareBtn.tag = i;
        [shareBtn setBackgroundColor:[UIColor clearColor]];
        
        [_centerBackView addSubview:shareBtn];
        
        CGSize shareBtnSize = CGSizeMake(40*BORateSize, 60*BORateSize);
        [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_centerBackView.mas_top).offset(35*BORateSize);
            make.left.mas_equalTo(_centerBackView.mas_left).offset((10*BORateSize+i*(shareBtnSize.width+20*BORateSize)));
            make.size.mas_equalTo(shareBtnSize);
            
        }];
        
        
        
        switch (i) {
            case 0:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_wechat"] andHighlightedImage:[UIImage imageNamed:@"share_weixin_click"] andTitle:@"微信好友"];
                
                break;
            }
                
            case 1:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_friends"] andHighlightedImage:[UIImage imageNamed:@"btn_player_share_friends_click"] andTitle:@"微信朋友圈"];
                
                break;
            }
            case 2:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_qqfriends"] andHighlightedImage:[UIImage imageNamed:@"share_QQ_click"] andTitle:@"QQ好友"];
                
                break;
            }
            case 3:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_qqzone"] andHighlightedImage:[UIImage imageNamed:@"btn_player_share_qqzone_click"] andTitle:@"QQ空间"];
                
                break;
            }
                
            default:
                break;
        }
        
        
        
        
    }
    //************* 分割线 **************
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = UIColorFromRGB(0xeeeeee);
    lineView.tag = 99;
    [_centerBackView addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(_centerBackView.mas_left);
        make.right.mas_equalTo(_centerBackView.mas_right);
        make.bottom.mas_equalTo(_centerBackView.bottom).offset(-31*kRateSize);
    }];
    
    
    _cancelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_cancelBtn setBackgroundColor:UIColorFromRGB(0xffffff)];
    [_cancelBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    [_cancelBtn setTitleColor:UIColorFromRGB(0x3ab7ff) forState:UIControlStateHighlighted];
    [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    _cancelBtn.titleLabel.text = @"取消";
    _cancelBtn.layer.cornerRadius = 6;
    _cancelBtn.tag = 99;
    [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [_centerBackView addSubview:_cancelBtn];
    
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(30*kRateSize);
        make.bottom.mas_equalTo(_centerBackView.mas_bottom);
    }];
    
    
    
}


-(void)cancelAction{
    self.hidden = YES;
}
-(void)shareBtnSelected:(UIButton *)btn{

    WS(wself);
    if (wself.returnBlock) {
        wself.returnBlock(btn.tag);
    }

    
    if ([self.delegate respondsToSelector:@selector(shareViewBtnSelected:)]) {
        [self.delegate shareViewBtnSelected:btn];
    }

}
-(void)setInstalledQQ:(BOOL)installedQQ{
    if (!installedQQ) {
        NSArray *subViewArr = [NSArray arrayWithArray:[_centerBackView subviews]];
        for (ShareButton*btn in subViewArr) {
            if (btn.tag==2||btn.tag==3) {
                btn.hidden = YES;
            }else{
                btn.hidden = NO;
            }
        }
    }

}

-(void)setInstalledWX:(BOOL)installedWX{
    if (!installedWX) {
        NSArray *subViewArr = [NSArray arrayWithArray:[_centerBackView subviews]];
        for (ShareButton*btn in subViewArr) {
            if (btn.tag==0||btn.tag==1) {
                btn.hidden = YES;
            }else{
                btn.hidden = NO;
                if (btn.tag == 2) {
                    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_centerBackView.mas_top).offset(35*kRateSize);
                        make.left.mas_equalTo(_centerBackView.mas_left).offset(10*kRateSize);
                        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 60*kRateSize));
                    }];
                }
                if (btn.tag == 3) {
                    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_centerBackView.mas_top).offset(35*kRateSize);
                        make.left.mas_equalTo(_centerBackView.mas_left).offset(30*kRateSize+40*kRateSize);
                        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 60*kRateSize));
                    }];

                }
                
            }
        }
    }


}


-(void)setPosition:(NSInteger)position{
    WS(wself);
    CGSize backSize = CGSizeMake(250*BORateSize , 140*BORateSize);

    
    switch (position) {
        case 0:{
            [_centerBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(backSize);
                make.center.mas_equalTo(wself);
                
            }];
            break;
        }
        case 1:{
            [_centerBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(backSize);
                make.bottom.mas_equalTo(wself.mas_bottom);
                make.centerX.mas_equalTo(wself.mas_centerX);
                
            }];
            break;
        }
        case 2:{
            
            break;
        }
        default:
            break;
    }
}

-(void)setBGImageVIewWithMask:(BOOL)isMask{
    _BGImageView.hidden = isMask;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
