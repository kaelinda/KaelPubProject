//
//  ShareView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShareButton.h"

typedef NS_ENUM(NSUInteger, LandscapeShareType) {
    LsWechat,
    LsWechatZone,
    LsQQ,
    LsQQZone,
};

typedef void(^landscapeShareViewReturnBlock)(NSInteger);

@protocol shareViewDelegate <NSObject>

-(void)shareViewBtnSelected:(UIButton *)btn;

@end


@interface ShareView : UIView<shareBtnDelegate>
{
    UIButton *_tencentQQBtn;
    UIButton *_QZoneBtn;
    UIButton *_weChattimeLineBtn;
    UIButton *_weChatSesionBtn;
    
    UIView   *_centerBackView;//承载视图
    UILabel  *_shareTitleLabel;//分享给朋友 label
    UIButton *_cancelBtn;

    CGFloat BORateSize;

}

@property (nonatomic,assign)id <shareViewDelegate>delegate;
@property (nonatomic,assign)BOOL installedQQ;
@property (nonatomic,assign)BOOL installedWX;
@property (nonatomic,assign)NSInteger position;
@property (nonatomic,strong)UIImageView *BGImageView;

@property (nonatomic, copy)landscapeShareViewReturnBlock shareSuccessBlock;

/**
 返回的是整型
 */
@property (nonatomic,copy) landscapeShareViewReturnBlock returnBlock;

-(void)setBGImageVIewWithMask:(BOOL)isMask;

/**
 分享公用方法
 
 @param linkUrl 分享链接
 @param contentStr 分享的内容
 @param imageUrl 图片链接
 @param shareType 分享类型：QQ、微信、QQ空间、朋友圈
 @param currentController 调用的VC
 */
- (void)UMSocialShareWithAppLinkUrl:(NSString *)linkUrl shareContent:(NSString *)contentStr shareImage:(NSString *)imageUrl shareType:(LandscapeShareType)shareType currentController:(UIViewController *)currentController;

@end
