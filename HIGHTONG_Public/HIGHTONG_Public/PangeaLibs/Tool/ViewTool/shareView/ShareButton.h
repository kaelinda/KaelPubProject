//
//  ShareButton.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol shareBtnDelegate <NSObject>

-(void)shareBtnSelected:(UIButton *)btn;

@end

@interface ShareButton : UIView
{
    UIButton *_imageBtn;
    UILabel  *_titleLabel;

}
@property (nonatomic,assign) id<shareBtnDelegate>delegate;


-(void)setBtnWithNormalImage:(UIImage *)normalImage andHighlightedImage:(UIImage *)highlightedImage andTitle:(NSString *)title;


@end
