//
//  ShareViewVertical.h
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareButton.h"

typedef NS_ENUM(NSUInteger, ShareType) {
    Wechat,
    WechatZone,
    QQ,
    QQZone,
};

typedef void(^shareViewReturnBlock)(NSInteger);

typedef void(^shareReturnBlock)(ShareType type);

@interface ShareViewVertical : UIView<shareBtnDelegate>
{
    UIButton *_tencentQQBtn;
    UIButton *_QZoneBtn;
    UIButton *_weChattimeLineBtn;
    UIButton *_weChatSesionBtn;
    
    UIView   *_centerBackView;//承载视图
    UILabel  *_shareTitleLabel;//分享给朋友 label
    UIButton *_cancelBtn;
    
    CGFloat BORateSize;

}

//@property (nonatomic,assign)id <shareViewDelegate>delegate;
@property (nonatomic,assign) BOOL installedQQ;
@property (nonatomic,assign) BOOL installedWX;
@property (nonatomic,assign) NSInteger position;
@property (nonatomic,strong) UIImageView *BGImageView;

/**
 返回的是整型
 */
@property (nonatomic,copy) shareViewReturnBlock returnBlock;

/**
 返回的是枚举类型
 */
@property (nonatomic,copy) shareReturnBlock shareBlock;

@property (nonatomic,copy) void (^ dismissHandle)(BOOL isDismiss);

@property (nonatomic,copy) shareViewReturnBlock shareSuccessBlock;


/**
 分享公用方法

 @param linkUrl 分享链接
 @param contentStr 分享的内容
 @param imageUrl 图片链接
 @param shareType 分享类型：QQ、微信、QQ空间、朋友圈
 @param currentController 调用的VC
 */
- (void)UMSocialShareWithAppLinkUrl:(NSString *)linkUrl shareContent:(NSString *)contentStr shareImage:(NSString *)imageUrl shareType:(ShareType)shareType currentController:(UIViewController *)currentController;


@end
