//
//  ShareViewVertical.m
//  HIGHTONG_Public
//
//  Created by Kael on 16/3/17.
//  Copyright © 2016年 创维海通. All rights reserved.
//

#import "ShareViewVertical.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
//#import "UMSocial.h"
#import <UMSocialCore/UMSocialCore.h>

@implementation ShareViewVertical
-(instancetype)init{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        widht = kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight;
        hight = KDeviceHeight>kDeviceWidth?KDeviceHeight:kDeviceWidth;
        
        BORateSize = widht/320;
        
        [self initUI];

    }
    return self;
}


- (void)UMSocialShareWithAppLinkUrl:(NSString *)linkUrl shareContent:(NSString *)contentStr shareImage:(NSString *)imageUrl shareType:(ShareType)shareType currentController:(UIViewController *)currentController
{
    [self shareWebPageToPlatformType:shareType andContentString:contentStr andLinkUrl:linkUrl andImageUrl:imageUrl andCurrentVC:currentController];
}

-(id)resetThumbImage:(NSString *)image
{
    
    if([image hasPrefix:@"https"])
    {
        return image;
    }else
    {
        return [UIImage imageNamed:@"ic_launcher"];
    }
    
}
-(void)shareWebPageToPlatformType:(ShareType)shareType andContentString:(NSString *)contentString andLinkUrl:(NSString*)linkUrl andImageUrl:(NSString*)imageUrl andCurrentVC:(UIViewController*)currentViewController
{
    
    UMSocialPlatformType platformType;
    switch (shareType) {
        case Wechat:
        {
            platformType = UMSocialPlatformType_WechatSession;
        }
            break;
        case WechatZone:
        {
            platformType = UMSocialPlatformType_WechatTimeLine;
            
        }
            break;
        case QQ:
        {
            platformType = UMSocialPlatformType_QQ;
            
        }
            break;
        case QQZone:
        {
            platformType = UMSocialPlatformType_Qzone;
            
        }
            break;
            
        default:
            break;
    }
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:contentString descr:contentString thumImage:[self resetThumbImage:imageUrl]];
    //设置网页地址
    shareObject.webpageUrl = linkUrl;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
#ifdef UM_Swift
    [UMSocialSwiftInterface shareWithPlattype:platformType messageObject:messageObject viewController:self completion:^(UMSocialShareResponse * data, NSError * error) {
#else
        //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:currentViewController completion:^(id data, NSError *error) {
#endif
            if (error) {
                UMSocialLogInfo(@"************Share fail with error %@*********",error);
            }else{
                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                    UMSocialShareResponse *resp = data;
                    //分享结果消息
                    UMSocialLogInfo(@"response message is %@",resp.message);
                    //第三方原始返回的数据
                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                    NSLog(@"分享成功！");
                    if (_shareSuccessBlock) {
                        _shareSuccessBlock(1);
                    }

                }else{
                    UMSocialLogInfo(@"response data is %@",data);
                }
            }
                        NSLog(@"kjkjk分享不成功");
                        if (_shareSuccessBlock) {
                            _shareSuccessBlock(2);
                        }

            //            [self alertWithError:error];
        }
    ];
}



-(void)initUI{
    WS(wself);
    _BGImageView = [[UIImageView alloc] init];
    [_BGImageView setBackgroundColor:[UIColor blackColor]];
    _BGImageView.alpha = 0.7;
    _BGImageView.userInteractionEnabled = YES;
    
    [self addSubview:_BGImageView];
    
    [_BGImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(wself);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelAction)];
    tap.numberOfTapsRequired = 1;
    [_BGImageView addGestureRecognizer:tap];
    
    
    
    _centerBackView = [[UIView alloc] init];
    //    [_centerBackView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_share"]]];
    [_centerBackView setBackgroundColor:UIColorFromRGB(0xffffff)];
//    _centerBackView.layer.cornerRadius = 6;
    [self addSubview:_centerBackView];
    
//    CGSize backSize = CGSizeMake(250*BORateSize , 140*BORateSize);
    [_centerBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(wself.mas_bottom);
        make.left.mas_equalTo(wself.mas_left);
        make.right.mas_equalTo(wself.mas_right);
        make.height.mas_equalTo(140*kRateSize);
//        make.size.mas_equalTo(backSize);
//        make.center.mas_equalTo(wself);
    }];
    
    
    
    _shareTitleLabel = [[UILabel alloc] init];
    [_shareTitleLabel setBackgroundColor:[UIColor clearColor]];
    _shareTitleLabel.text = @"分享给朋友";
    _shareTitleLabel.tag = 111;
    [_shareTitleLabel setFont:[UIFont fontWithName:HEITI_Light_FONT size:18.0f]];
    [_shareTitleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [_centerBackView addSubview:_shareTitleLabel];
    //    CGSize labelSize = CGSizeMake(200*BORateSize , 30*BORateSize);
    [_shareTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_centerBackView.mas_top);
        make.left.mas_equalTo(_centerBackView.mas_left).offset(8*BORateSize);
        make.right.mas_equalTo(_centerBackView.mas_right).offset(-10*BORateSize);
        make.height.mas_equalTo(30*BORateSize);
        
    }];
    
    for (int i=0; i<4; i++) {
        ShareButton *shareBtn = [[ShareButton alloc] init];
        shareBtn.delegate = self;
        shareBtn.tag = i;
        [shareBtn setBackgroundColor:[UIColor clearColor]];
        
        [_centerBackView addSubview:shareBtn];
        
        CGSize shareBtnSize = CGSizeMake(40*kRateSize, 60*kRateSize);
        CGFloat space = (kDeviceWidth-shareBtnSize.width*4)/4;
        
        
        [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_centerBackView.mas_top).offset(35*kRateSize);
            make.left.mas_equalTo(_centerBackView.mas_left).offset((space/3+i*(shareBtnSize.width+space)));
            make.size.mas_equalTo(shareBtnSize);
            
        }];
        
        
        
        switch (i) {
            case 0:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_wechat"] andHighlightedImage:[UIImage imageNamed:@"share_weixin_click"] andTitle:@"微信好友"];
                
                break;
            }
                
            case 1:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_friends"] andHighlightedImage:[UIImage imageNamed:@"btn_player_share_friends_click"] andTitle:@"微信朋友圈"];
                
                break;
            }
            case 2:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_qqfriends"] andHighlightedImage:[UIImage imageNamed:@"share_QQ_click"] andTitle:@"QQ好友"];
                
                break;
            }
            case 3:
            {
                [shareBtn setBtnWithNormalImage:[UIImage imageNamed:@"btn_player_share_qqzone"] andHighlightedImage:[UIImage imageNamed:@"btn_player_share_qqzone_click"] andTitle:@"QQ空间"];
                
                break;
            }
                
            default:
                break;
        }
        
        
        
        
    }
    //************* 分割线 **************
    UIView *lineView = [[UIView alloc] init];
    lineView.tag = 99;
    lineView.backgroundColor = UIColorFromRGB(0xeeeeee);
    [_centerBackView addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(_centerBackView.mas_left);
        make.right.mas_equalTo(_centerBackView.mas_right);
        make.bottom.mas_equalTo(_centerBackView.bottom).offset(-31*kRateSize);
    }];
    
    
    _cancelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_cancelBtn setBackgroundColor:UIColorFromRGB(0xffffff)];
    [_cancelBtn setTitleColor:App_selected_color forState:UIControlStateNormal];
    [_cancelBtn setTitleColor:UIColorFromRGB(0x3ab7ff) forState:UIControlStateHighlighted];
    [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    _cancelBtn.titleLabel.text = @"取消";
//    _cancelBtn.layer.cornerRadius = 6;
    _cancelBtn.tag = 99;
    [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [_centerBackView addSubview:_cancelBtn];
    
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(30*kRateSize);
        make.bottom.mas_equalTo(_centerBackView.mas_bottom);
    }];

    
    
    

}
-(void)cancelAction{
    self.hidden = YES;
}
-(void)shareBtnSelected:(UIButton *)btn{
    
    if (self.returnBlock) {
        self.returnBlock(btn.tag);
    }
    
    ShareType shareType = Wechat;
    switch (btn.tag) {
        case 0:
        {
            shareType = Wechat;
        }
            break;
        case 1:
        {
            shareType = WechatZone;
        }
            break;
        case 2:
        {
            shareType = QQ;
        }
            break;
        case 3:
        {
            shareType = QQZone;
        }
            break;
            
        default:
            break;
    }
    if (self.shareBlock) {
        self.shareBlock(shareType);
    }
    
    
}

-(void)setInstalledQQ:(BOOL)installedQQ{
    if (!installedQQ) {
        NSArray *subViewArr = [NSArray arrayWithArray:[_centerBackView subviews]];
        for (ShareButton*btn in subViewArr) {
            if (btn.tag==2||btn.tag==3) {
                btn.hidden = YES;
            }else{
                btn.hidden = NO;
            }
        }
    }


}

-(void)setInstalledWX:(BOOL)installedWX{
    if (!installedWX) {
        
        CGSize shareBtnSize = CGSizeMake(40*BORateSize, 60*BORateSize);
        CGFloat space = (kDeviceWidth-40*kRateSize*4)/5;
        
        NSArray *subViewArr = [NSArray arrayWithArray:[_centerBackView subviews]];
        for (ShareButton*btn in subViewArr) {
            if (btn.tag==0||btn.tag==1) {
                btn.hidden = YES;
            }else{
                btn.hidden = NO;
                if (btn.tag == 2) {
                    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_centerBackView.mas_top).offset(35*kRateSize);
                        make.left.mas_equalTo(_centerBackView.mas_left).offset(space/2);
                        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 60*kRateSize));
                    }];
                }
                if (btn.tag == 3) {
                    [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(_centerBackView.mas_top).offset(35*kRateSize);
                        make.left.mas_equalTo(_centerBackView.mas_left).offset(space/2+(shareBtnSize.width+space*BORateSize));
                        make.size.mas_equalTo(CGSizeMake(40*kRateSize, 60*kRateSize));
                    }];
                    
                }
                
            }
        }
    }


}
-(void)setHidden:(BOOL)hidden{
    WS(wself);
    if (hidden) {
        [_centerBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(wself.mas_bottom).offset(140*kRateSize);
            make.left.mas_equalTo(wself.mas_left);
            make.right.mas_equalTo(wself.mas_right);
            make.height.mas_equalTo(140*kRateSize);
            
        }];
    }else{
        [super setHidden:hidden];

            [_centerBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(wself.mas_bottom);
                make.left.mas_equalTo(wself.mas_left);
                make.right.mas_equalTo(wself.mas_right);
                make.height.mas_equalTo(140*kRateSize);
                
            }];
    }
    
    // tell constraints they need updating
    [self setNeedsUpdateConstraints];
    
    // update constraints now so we can animate the change
    [self updateConstraintsIfNeeded];
    

    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
        if (hidden) {
            _BGImageView.alpha = 0;
        }else{
            _BGImageView.alpha = 0.6;
        }
    } completion:^(BOOL finished) {
        [super setHidden:hidden];

    }];
    
    if (_dismissHandle) {
        _dismissHandle(hidden);
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
