//
//  OrderView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/11/28.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton_Block.h"

typedef void (^OrderClick)();


@interface OrderView : UIView



@property (nonatomic,strong) UIImageView *backImageView;//背景图
@property (nonatomic,strong) UILabel *orderLbael;//该影片为订购影片
@property (nonatomic,strong) UIButton_Block *orderBtn;//订购按钮
@property (nonatomic,assign) NSInteger programType;//节目类型 1、电影  2、电视剧
@property (nonatomic,copy) OrderClick orderClick;


@end
