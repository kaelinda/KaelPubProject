//
//  LoadingView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/10/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
        
    }
    
    
    return self;
}
-(instancetype)initWithImage:(UIImage *)image{
    self = [super initWithImage:image];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

-(void)setupSubviews{
    
    
    // 对Y轴进行旋转（指定Z轴的话，就和UIView的动画一样绕中心旋转）
    _animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // 设定动画选项
    _animation.duration = 2; // 持续时间
    _animation.repeatCount = CGFLOAT_MAX; // 重复次数
    // 设定旋转角度
    _animation.fromValue = [NSNumber numberWithFloat:0.0]; // 起始角度
    _animation.toValue = [NSNumber numberWithFloat: -2*M_PI]; // 终止角度
    
    // 添加动画
    [self.layer addAnimation:_animation forKey:@"rotate-layer"];

    
}

-(void)setHidden:(BOOL)hidden{
    [super setHidden:hidden];
    if (hidden) {
        [self.layer removeAnimationForKey:@"rotate-layer"];
    }else
    {
        [self.layer addAnimation:_animation forKey:@"rotate-layer"];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
