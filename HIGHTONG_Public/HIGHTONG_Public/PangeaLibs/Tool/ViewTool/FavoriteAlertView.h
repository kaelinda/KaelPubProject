//
//  FavoriteAlertView.h
//  HIGHTONG
//
//  Created by smart on 12-11-23.
//  Copyright (c) 2012年 ZhangDongfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteAlertView : UIImageView

- (void) hiddenWithAnimation;

+ (void)showAlertViewWithText:(NSString *)message inView:(UIView *)view andWidth:(int)width;
+ (void)showOtherAlertViewWithText:(NSString *)message inView:(UIView *)view andWidth:(int)width;
+ (void)showAlertViewWithRet:(NSInteger)aret inView:(UIView *)view andWidth:(int)width;
@end
