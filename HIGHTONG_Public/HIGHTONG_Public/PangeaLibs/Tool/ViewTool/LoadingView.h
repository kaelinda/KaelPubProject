//
//  LoadingView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/10/19.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface LoadingView : UIImageView
@property (nonatomic,strong) UIImageView *LView;
@property (nonatomic,strong) CABasicAnimation *animation;
@end
