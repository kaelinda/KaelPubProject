//
//  AlertLabel.h
//  HIGHTONGV
//
//  Created by HTYunjiang on 14-8-29.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertLabel : UIImageView
#define NSLOG_DATA NSLog(@"ret = %d type= %@,result = %@,",ret,type,result);
//#define NSLOG_DATA ;
#define debugMethod() NSLog(@"%s", __func__)






@property (strong, nonatomic) UILabel * contentLabel;

/**
 *@brief 固定内容的吐司提示
 *@param aret 错误类型，具体请参考接口文档
 *@param view 要显示的view
 *@param locationTag 0中间
 */
+ (void) AlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag;
+ (AlertLabel *)DefaultAlertLabel;
//  获取直播和回看的 连接  ➢	ret   值 判断
+(void) TV_PLAYAlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag;

// 查询 和 删除 回看 播放记录    ➢	  ret   值 判断
+ (void) EVENT_PLAY_LISTTV_CHANGEAlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag;






/**
 *@brief 吐司提示text内容
 *@param text 将要显示的内容文字
 *@param view 要显示的view
 *@param locationTag 0中间
 */
+ (void)AlertInfoWithText:(NSString *)text andWith:(UIView *)view withLocationTag:(int)locationTag;
/**
 *@brief 吐司提示text内容(仅限于获取SSID)
 *@param text 将要显示的内容文字
 *@param view 要显示的view
 *@param locationTag 0中间
 */
+(void)AlertSSIDInfoWithText:(NSString *)text andWith:(UIView *)view withLocationTag:(int)locationTag;
- (void) hiddenWithAnimation;

                                    //******常用方法******//
+ (BOOL)UserIsLogin;
+(void)Common_Used_Method_SetUIAutoLogin:(BOOL)IsAutoLogin;
+(NSDate *)Common_Used_MethodGetSystemTimeWith;
//回看，播放，预览分别是 1 2 3
+ (int)Common_Used_Method_GetPlayStatusWithStartTime:(NSString *)startT andEndTime:(NSString *)endTime;
//回看，播放，预览分别是 1 2 3
+ (int)Common_Used_Method_GetPlayStatusInEPGPageWithStartTime:(NSString *)startT andEndTime:(NSString *)endTime;
+ (int)Common_countWord:(NSString*)s;
/**
 *@brief 检测网络连接状态
 *@param text 将要显示的内容文字
 *@param view 要显示的view
 *@param locationTag 0中间
 */
+ (NSInteger)getNetWorkStatusWithReachability;
@end
