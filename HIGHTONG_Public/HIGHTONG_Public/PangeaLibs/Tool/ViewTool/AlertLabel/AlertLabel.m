//
//  AlertLabel.m
//  HIGHTONGV
//
//  Created by HTYunjiang on 14-8-29.
//  Copyright (c) 2014年 HTYunjiang. All rights reserved.
//

#import "AlertLabel.h"
#import "GetBaseInfo.h"

@implementation AlertLabel
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(55*kDeviceWidth/320, 0, 240, 42)];
        [self setImage:[UIImage imageNamed:@"bg_toast"]];
        [self setBackgroundColor:[UIColor clearColor]];
        self.contentLabel = [[UILabel alloc] init];
        [self.contentLabel setTextAlignment:NSTextAlignmentCenter];
        [self.contentLabel setTextColor:[UIColor whiteColor]];
        [self.contentLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentLabel setFont:[UIFont systemFontOfSize:15]];
        [self addSubview:self.contentLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.contentLabel setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}


+(AlertLabel *)DefaultAlertLabel
{
    static AlertLabel * alertView ;
    if (alertView) {
        alertView = nil;
    }
    if (alertView == nil) {
        alertView = [[AlertLabel alloc] init];
    }
    return alertView;
}




+(void)AlertSSIDInfoWithText:(NSString *)text andWith:(UIView *)view withLocationTag:(int)locationTag{
    AlertLabel * alertLabel = [AlertLabel DefaultAlertLabel];
    
    alertLabel.contentLabel.text = text;
    alertLabel.frame=CGRectMake(0, 0, kDeviceWidth, 42);
    [view addSubview:alertLabel];
    [view bringSubviewToFront:alertLabel];
    alertLabel.alpha = 1;
    //强制转换到屏幕中间
    
    switch (locationTag) {
        case 0://nav
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2-kViewOriginY)];
            break;
        case 1://无nav
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }else{
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }
            break;
        case 2:
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, 100)];
        default:
            break;
    }
    [NSTimer scheduledTimerWithTimeInterval:0 target:alertLabel selector:@selector(hiddenWithAnimation) userInfo:nil repeats:NO];
    
}


+(void)AlertInfoWithText:(NSString *)text andWith:(UIView *)view
         withLocationTag:(int)locationTag
{
    AlertLabel * alertLabel = [AlertLabel DefaultAlertLabel];
    
    alertLabel.contentLabel.text = text;
    
    [view addSubview:alertLabel];
    [view bringSubviewToFront:alertLabel];
    alertLabel.alpha = 1;
    //强制转换到屏幕中间

    switch (locationTag) {
        case 0://nav
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2-kViewOriginY)];
            break;
        case 1://无nav
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }else{
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }
            break;
        case 2:
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, 500)];
        default:
            break;
    }
    if (locationTag==1) {
        alertLabel.center = CGPointMake(view.center.x, view.center.y+60);

    }else{
        alertLabel.center = view.center;

    }
    
    [NSTimer scheduledTimerWithTimeInterval:0 target:alertLabel selector:@selector(hiddenWithAnimation) userInfo:nil repeats:NO];
}



- (void)hiddenWithAnimation
{
    [UIView animateWithDuration:3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
//播放的时候
+ (void)AlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag{
    AlertLabel * alertLabel = [AlertLabel DefaultAlertLabel];
    switch (aret) {
        case -1004:
            alertLabel.contentLabel.text = @"无网络连接";
        case -1001:
            alertLabel.contentLabel.text = @"网络请求超时";
            
        case -9:
            alertLabel.contentLabel.text = @"获取数据失败";
            break;
        case -7:
            alertLabel.contentLabel.text = @"频道取消发布";
            break;
        case -6:
            alertLabel.contentLabel.text = @"该频道不提供回看功能";
            break;
        case -5:
            alertLabel.contentLabel.text = @"频道对应此终端无播放链接";
            break;
        case -4:
            alertLabel.contentLabel.text = @"扣费失败";
            break;
        case -3:
            alertLabel.contentLabel.text = @"鉴权失败";
            break;
        case -2:
            alertLabel.contentLabel.text = nil;
            return;
            break;
        case -1:
            alertLabel.contentLabel.text = @"当前节目不存在";
            break;
        case 0:
            alertLabel.contentLabel.text = @"OK,一切正常";
            return;
            break;
        default:
//            alertLabel.contentLabel.text = [NSString stringWithFormat:@"tag === %d",aret];
            break;
    }
    
    [view addSubview:alertLabel];
    [view bringSubviewToFront:alertLabel];
    alertLabel.alpha = 1;
    //强制在屏幕中间

    switch (locationTag) {
        case 0://nav
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2-kViewOriginY)];
            break;
        case 1:// 无nav
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }else{
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }
            break;
        case 2:
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, 100)];
        default:
            break;
    }
    alertLabel.center = view.center;

    [NSTimer scheduledTimerWithTimeInterval:0 target:alertLabel selector:@selector(hiddenWithAnimation) userInfo:nil repeats:NO];
    
}
//  获取直播连接  ➢	ret   值 判断
+(void)TV_PLAYAlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag
{
    AlertLabel * alertLabel = [AlertLabel DefaultAlertLabel];
    switch (aret) {
        case -1001:
            alertLabel.contentLabel.text = @"获取数据失败";
            
        case -9:
            alertLabel.contentLabel.text = @"获取数据失败";
            break;
        case -8:
            alertLabel.contentLabel.text = @"鉴权成功待付费";
            break;
        case -6:
            alertLabel.contentLabel.text = @"该频道不提供回看功能";
            break;
        case -5:
            alertLabel.contentLabel.text = @"频道无播放链接";
            break;
        case -3:
            alertLabel.contentLabel.text = @"鉴权失败";
            break;
        case -2:
            alertLabel.contentLabel.text = @"用户未登录或者登录超时";
            return;
            break;
        case -1:
            alertLabel.contentLabel.text = @"当前频道不存在";
            break;
        case 0:
            alertLabel.contentLabel.text = @"OK,一切正常";
            return;
            break;
        default:
            alertLabel.contentLabel.text = [NSString stringWithFormat:@"tag === %d",aret];
            break;
    }
    
    [view addSubview:alertLabel];
    [view bringSubviewToFront:alertLabel];
    alertLabel.alpha = 1;
    //强制在屏幕中间
    
    switch (locationTag) {
        case 0://nav
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2-kViewOriginY)];
            break;
        case 1:// 无nav
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }else{
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }
            break;
        case 2:
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, 100)];
        default:
            break;
    }
    [NSTimer scheduledTimerWithTimeInterval:0 target:alertLabel selector:@selector(hiddenWithAnimation) userInfo:nil repeats:NO];
}

// 查询 和 删除 回看 播放记录    ➢	  ret   值 判断
+ (void) EVENT_PLAY_LISTTV_CHANGEAlertWithRet:(int)aret andWithView:(UIView *)view withLocationTag:(int)locationTag
{
    AlertLabel * alertLabel = [AlertLabel DefaultAlertLabel];
    switch (aret) {
        case -1001:
            alertLabel.contentLabel.text = @"获取数据失败";
            
        case -9:
            alertLabel.contentLabel.text = @"获取数据失败";
            break;
        case -1:
            alertLabel.contentLabel.text = @"没有符合条件的回看播放记录";
            break;
        case 0:
            alertLabel.contentLabel.text = @"OK,一切正常";
            return;
            break;
        case 1:
            alertLabel.contentLabel.text = @"该节目不在回看播放记录中";
            return;
            break;
        default:
            alertLabel.contentLabel.text = [NSString stringWithFormat:@"tag === %d",aret];
            break;
    }
    
    [view addSubview:alertLabel];
    [view bringSubviewToFront:alertLabel];
    alertLabel.alpha = 1;
    //强制在屏幕中间
    
    switch (locationTag) {
        case 0://nav
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2-kViewOriginY)];
            break;
        case 1:// 无nav
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }else{
                [alertLabel setCenter:CGPointMake(kDeviceWidth/2, view.frame.size.height/2)];
            }
            break;
        case 2:
            [alertLabel setCenter:CGPointMake(kDeviceWidth/2, 100)];
        default:
            break;
    }
    [NSTimer scheduledTimerWithTimeInterval:0 target:alertLabel selector:@selector(hiddenWithAnimation) userInfo:nil repeats:NO];
}




//+(void)HTSW_MSI_Datebase_SetUIAutoLogin:(BOOL)IsAutoLogin{
+(void)Common_Used_Method_SetUIAutoLogin:(BOOL)IsAutoLogin{
    NSUserDefaults *UserInfo = [NSUserDefaults standardUserDefaults];
    if(IsAutoLogin){
        [UserInfo setValue:@"1" forKey:@"UIAutoLogin"];
    }
    else{
        [UserInfo setValue:@"0" forKey:@"UIAutoLogin"];
    }
}

+ (NSDate *)Common_Used_MethodGetSystemTimeWith{
    NSUserDefaults *UserInfo = [NSUserDefaults standardUserDefaults];
    NSString *SystimeDeffence;
    if([UserInfo valueForKey:@"SystemTimeDiffence"] == nil){
        SystimeDeffence = @"0";
        [UserInfo setValue:@"0" forKey:@"SystemTimeDiffence"];
    }
    else{
        SystimeDeffence = [UserInfo valueForKey:@"SystemTimeDiffence"];
    }
    
    return [[NSDate date] dateByAddingTimeInterval:[SystimeDeffence intValue]];
}
+ (int)Common_Used_Method_GetPlayStatusWithStartTime:(NSString *)startT andEndTime:(NSString *)endTime{
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *NewstartTime = [formatter dateFromString:startT];
    NSDate *NewendTime   = [formatter dateFromString:endTime];
    NSDate *nowDate = [NSDate date];
    if ([nowDate compare:NewstartTime] == NSOrderedAscending) {
        NSLog(@"预约 =3");
        return 3;
    }else if ([NewendTime compare:nowDate] == NSOrderedAscending){
        NSLog(@"回看时间 ==1");
        return 1;
    }else{
        NSLog(@"播放 =2");
        return 2;
    }
    
}
+ (int)Common_Used_Method_GetPlayStatusInEPGPageWithStartTime:(NSString *)startT andEndTime:(NSString *)endTime{
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *NewstartTime = [formatter dateFromString:startT];
    NSDate *NewendTime   = [formatter dateFromString:endTime];
    
    //    NSString *nowDateStr = [[NSDate date] description];
    NSString *nowDateStr = [formatter stringFromDate:[NSDate date]];
    NSDate *nowDate = [formatter dateFromString:nowDateStr];
    if ([nowDate compare:NewstartTime] == NSOrderedAscending) {
        NSLog(@"预约 =3");
        return 3;
    }else if ([NewendTime compare:nowDate] == NSOrderedAscending){
        NSLog(@"回看时间 ==1");
        return 1;
    }else{
        NSLog(@"播放 =2");
        return 2;
    }
    
}
+ (int)Common_countWord:(NSString *)s{
    int i,n=[s length],l=0,a=0,b=0;
    
    unichar c;
    
    for(i=0;i<n;i++){
        
        c=[s characterAtIndex:i];
        
        if(isblank(c)){
            
            b++;
            
        }else if(isascii(c)){
            
            a++;
            
        }else{
            
            l++;
            
        }
        
    }
    
    if(a==0 && l==0) return 0;
    
    return l+(int)ceilf((float)(a+b)/2.0);
}

+(NSInteger)getNetWorkStatusWithReachability{
    
    return [GETBaseInfo getNetWorkStatusWithReachability];
}
@end
