//
//  NumberView.h
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NumberView;

@protocol NumberViewDelegate <NSObject>

-(void)NumberViewBtnSelectedWith:(NSString *)selectedBtn;

@end



@interface NumberView : UIView

@property (nonatomic,assign) id<NumberViewDelegate>delegate;


@end
