//
//  DirectionView.h
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DirectionView;

@protocol DirectionViewDelegate <NSObject>

-(void)directionViewSelectedWith:(NSString *)direction;

@end


@interface DirectionView : UIView


@property (assign,nonatomic) id<DirectionViewDelegate>delegate;

@end
