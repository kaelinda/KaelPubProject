//
//  RemoteControlView.h
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectionView.h"
#import "NumberView.h"
@class RemoteControlView;

@protocol RemoteControlViewDelegate <NSObject>

-(void)remoteControlViewSelectedWith:(NSString *)selectedBtn;

@end


@interface RemoteControlView : UIView<DirectionViewDelegate,NumberViewDelegate>


@property (nonatomic,assign) id<RemoteControlViewDelegate>delagete;


@end
