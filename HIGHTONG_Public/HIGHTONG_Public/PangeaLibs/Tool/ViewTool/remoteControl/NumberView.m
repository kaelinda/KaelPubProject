//
//  NumberView.m
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "NumberView.h"

@implementation NumberView
// width 220 high 226

-(instancetype)init{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpNumberView];
        
    }

    return self;
}




-(void)setUpNumberView{
    CGSize btnSize = CGSizeMake(70*kRateSize, 50*kRateSize);
    
    NSInteger BtnNum = 0;
    
//--------------创建UIButton
    for (int i=1; i<5; i++) {
            for (int j=1; j<4; j++) {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = CGRectMake(0, 0, btnSize.width, btnSize.height);
                [btn addTarget:self action:@selector(numBtnAction:) forControlEvents:UIControlEventTouchUpInside];
               
                //--------------设置标题
                if (BtnNum<9) {
                    [btn setTitle:[NSString stringWithFormat:@"%ld",(long)BtnNum+1] forState:UIControlStateNormal];
                    
                    
                   
                    NSString *normalImage = [NSString stringWithFormat:@"r_%ld",(long)BtnNum+1];
                    NSString *highlightImage = [NSString stringWithFormat:@"r_%ldlight",(long)BtnNum+1];
                    [btn setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateHighlighted];

                    
                }else{
                    switch (BtnNum) {
                        case 9:
                        {
//                            [btn setTitle:@"菜单" forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_menu" ] forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_menulight"] forState:UIControlStateHighlighted];
                            break;
                        }
                        case 10:
                        {
//                            [btn setTitle:@"0" forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_0" ] forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_0light"] forState:UIControlStateHighlighted];

                            break;
                        }
                        case 11:
                        {
//                            [btn setTitle:@"返回" forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_back" ] forState:UIControlStateNormal];
                            [btn setImage:[UIImage imageNamed:@"r_backlight"] forState:UIControlStateHighlighted];

                            break;
                        }
                        default:
                            break;
                    }
                    
                }
                
                //设置中心点
                NSInteger X = 4*j+(2*j-1)*btnSize.width/2+1*kRateSize;
                NSInteger Y = 4*i+(2*i-1)*btnSize.height/2+1*kRateSize;
                btn.center = CGPointMake(X, Y);
                [btn setBackgroundColor:[UIColor clearColor]];
                
                [self addSubview:btn];//添加按钮
                BtnNum++;//计数+1

            }
        }
        
    
    
    
    
    
}
-(void)numBtnAction:(UIButton *)btn{

//    NSLog(@"numBtn action first place----%@",btn.titleLabel.text);
    
    if ([_delegate respondsToSelector:@selector(NumberViewBtnSelectedWith:)]) {
        [_delegate NumberViewBtnSelectedWith:btn.titleLabel.text];
    }
    

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
