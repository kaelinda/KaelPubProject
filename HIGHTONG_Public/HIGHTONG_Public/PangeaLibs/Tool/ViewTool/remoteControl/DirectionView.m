//
//  DirectionView.m
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "DirectionView.h"



@interface DirectionView ()



@property (nonatomic,strong) UIButton *ConfirmBtn;
@property (nonatomic,strong) UIButton *UpBtn;
@property (nonatomic,strong) UIButton *DownBtn;
@property (nonatomic,strong) UIButton *LeftBtn;
@property (nonatomic,strong) UIButton *RightBtn;



@end


@implementation DirectionView


-(instancetype)init{
    self = [super init];
    if (self) {
     
        [self createBtn];
    }
    return self;
}

-(void)createBtn{
    
    
    _ConfirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];

    [_ConfirmBtn setBackgroundColor:[UIColor clearColor]];
    [_ConfirmBtn setImage:[UIImage imageNamed:@"r_confirmlight"] forState:UIControlStateHighlighted];
    [_ConfirmBtn setImage:[UIImage imageNamed:@"r_confirm"] forState:UIControlStateNormal];
    
    [_ConfirmBtn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    _ConfirmBtn.tag = 0;
    [self  addSubview:_ConfirmBtn];
    
    WS(wself);
    
    [_ConfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(wself.center);
        make.size.mas_equalTo(CGSizeMake(71*kRateSize, 44*kRateSize));
    }];
    
    
    
    [self createUPBtn];
    [self createDownBtn];
    [self cretaeLeftBtn];
    [self cretaeRightBtn];
    
    
    
}
-(void)createUPBtn{
    
    
    CGSize btnSize = CGSizeMake(167*kRateSize, 71*kRateSize);
    
    _UpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_UpBtn setBackgroundColor:[UIColor clearColor]];
    _UpBtn.tag = 1;
    [_UpBtn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_UpBtn setImage:[UIImage imageNamed:@"r_arrowU"] forState:UIControlStateNormal];
    [_UpBtn setImage:[UIImage imageNamed:@"r_arrowUlight"] forState:UIControlStateHighlighted];
    [self  addSubview:_UpBtn];

    [_UpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_ConfirmBtn.mas_centerX);
        make.centerY.mas_equalTo(_ConfirmBtn.mas_centerY).offset(-btnSize.width/2-1);
        
        make.size.mas_equalTo(btnSize);
        
    }];
    
    
    
}
-(void)createDownBtn{
    
    CGSize btnSize = CGSizeMake(167*kRateSize, 71*kRateSize);

    
    _DownBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_DownBtn setBackgroundColor:[UIColor clearColor]];
    
    _DownBtn.transform = CGAffineTransformMakeRotation(-M_PI);
    _DownBtn.tag = 2;
    [_DownBtn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_DownBtn setImage:[UIImage imageNamed:@"r_arrowU"] forState:UIControlStateNormal];
    [_DownBtn setImage:[UIImage imageNamed:@"r_arrowUlight"] forState:UIControlStateHighlighted];
    [self  addSubview:_DownBtn];

    [_DownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_ConfirmBtn.mas_centerX);
        make.centerY.mas_equalTo(_ConfirmBtn.mas_centerY).offset(btnSize.width/2+1);
        
        make.size.mas_equalTo(btnSize);

    }];
    

    
}
-(void)cretaeLeftBtn{
    
    CGSize btnSize = CGSizeMake(167*kRateSize, 71*kRateSize);

    
    _LeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_LeftBtn setBackgroundColor:[UIColor clearColor]];
   
    _LeftBtn.transform = CGAffineTransformMakeRotation(-M_PI/2);
    _LeftBtn.tag = 3;
    [_LeftBtn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_LeftBtn setImage:[UIImage imageNamed:@"r_arrowU"] forState:UIControlStateNormal];
    [_LeftBtn setImage:[UIImage imageNamed:@"r_arrowUlight"] forState:UIControlStateHighlighted];
    [self addSubview:_LeftBtn];
    
    [_LeftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_ConfirmBtn.mas_centerX).offset(-btnSize.width/2-1);
        make.centerY.mas_equalTo(_ConfirmBtn.mas_centerY);
        
        make.size.mas_equalTo(btnSize);

    }];

    
    
    
}
-(void)cretaeRightBtn{
    
    CGSize btnSize = CGSizeMake(167*kRateSize, 71*kRateSize);
    
    _RightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_RightBtn setBackgroundColor:[UIColor clearColor]];
    _RightBtn.frame = CGRectMake(0, 0, 167, 71);
    
    _RightBtn.center = CGPointMake(_ConfirmBtn.centerX+167/2, _ConfirmBtn.centerY);
    
    _RightBtn.transform = CGAffineTransformMakeRotation(M_PI/2);
    _RightBtn.tag = 4;
    [_RightBtn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_RightBtn setImage:[UIImage imageNamed:@"r_arrowU"] forState:UIControlStateNormal];
    [_RightBtn setImage:[UIImage imageNamed:@"r_arrowUlight"] forState:UIControlStateHighlighted];
    [self addSubview:_RightBtn];
    
    
    [_RightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_ConfirmBtn.mas_centerX).offset(btnSize.width/2+1);
        make.centerY.mas_equalTo(_ConfirmBtn.mas_centerY);
        
        make.size.mas_equalTo(btnSize);

    }];
    

    
}

-(void)BtnAction:(UIButton *)Btn{
    
    if ([_delegate respondsToSelector:@selector(directionViewSelectedWith:)]) {
        
        switch (Btn.tag) {
            case 0:
            {
                [self.delegate directionViewSelectedWith:@"confirm"];
                break;
            }
            case 1:
            {
                [self.delegate directionViewSelectedWith:@"up"];
                break;
            }
            case 2:
            {
                [self.delegate directionViewSelectedWith:@"down"];
                break;
            }
            case 3:
            {
                [self.delegate directionViewSelectedWith:@"left"];
                break;
            }
            case 4:
            {
                [self.delegate directionViewSelectedWith:@"right"];
                break;
            }
                
            default:
                break;
        }
        
    }
    

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
