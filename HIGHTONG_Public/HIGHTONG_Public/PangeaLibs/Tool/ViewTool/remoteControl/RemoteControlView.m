//
//  RemoteControlView.m
//  DrawRectTest
//
//  Created by Kael on 15/8/5.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//

#import "RemoteControlView.h"



@implementation RemoteControlView




-(instancetype)init{
    self  = [super init];
    
    if (self) {

        [self setupSubviews];

    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupSubviews];
        
    }
    return self;
}



-(void)setupSubviews{
    
    //---------------
    
    CGSize numViewSize = CGSizeMake(226*kRateSize, 220*kRateSize);

    CGSize directionViewSize = CGSizeMake(250*kRateSize, 250*kRateSize);
    
    //---------------
    CGFloat three_Space = (KDeviceHeight-numViewSize.width-directionViewSize.width)/4;
    
//----------------返回按钮
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setTitle:@"hiden" forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"arrow_left_01"] forState:UIControlStateNormal];
    [self addSubview:backBtn];
    
    
    UIImageView *VLine = [[UIImageView alloc] init];
    VLine.image = [UIImage imageNamed:@"remoteVerticalLine"];
    [self addSubview:VLine];
    
    
    
    WS(wself);
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(wself.mas_top).offset(4);
        make.left.mas_equalTo(wself.mas_left).offset(4);
        make.size.mas_equalTo(CGSizeMake(50, 50));
        
    }];

    [VLine mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(wself.mas_top).offset(4);
        make.bottom.mas_equalTo(wself.mas_bottom).offset(-4);
        make.width.mas_equalTo(1);
        make.centerX.mas_equalTo(wself.mas_centerX);
        
    }];

    

//-------------方向键盘
    DirectionView *directionView = [[DirectionView alloc] init];
    directionView.delegate = self;
    [directionView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:directionView];

    CGFloat space_beetown_numViewAndDirectionView = directionViewSize.width-directionViewSize.width;//这里暂时先用定值了
    [directionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(directionViewSize);
        make.centerY.mas_equalTo(wself.centerY+10);
        make.centerX.mas_equalTo(wself.mas_right).offset(-(three_Space-space_beetown_numViewAndDirectionView/2+directionViewSize.width/2));
        
    }];
    //---------------数字键盘
    
    NumberView *numView = [[NumberView alloc] init];
    numView.delegate = self;
    [numView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:numView];
    
    [numView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(numViewSize);
        make.centerY.mas_equalTo(directionView.centerY+10);
        make.left.mas_equalTo(wself.mas_left).offset(three_Space);
        
    }];
    
}


-(void)backBtnAction:(UIButton *)btn{
    if ([self.delagete respondsToSelector:@selector(remoteControlViewSelectedWith:)]) {
        [self.delagete remoteControlViewSelectedWith:btn.titleLabel.text];
    }
}


#pragma mark - direction delegate

-(void)directionViewSelectedWith:(NSString *)direction{

    if ([self.delagete respondsToSelector:@selector(remoteControlViewSelectedWith:)]) {
        [self.delagete remoteControlViewSelectedWith:direction];
    }
    
}
#pragma mark - numview delegate 

-(void)NumberViewBtnSelectedWith:(NSString *)selectedBtn{

    if ([self.delagete respondsToSelector:@selector(remoteControlViewSelectedWith:)]) {
        [self.delagete remoteControlViewSelectedWith:selectedBtn];
    }

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
