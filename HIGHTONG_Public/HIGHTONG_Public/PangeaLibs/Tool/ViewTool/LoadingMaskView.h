//
//  LoadingMaskView.h
//  HIGHTONG_Public
//
//  Created by Kael on 15/12/31.
//  Copyright © 2015年 创维海通. All rights reserved.
//
//这是一个单利 采用自动布局

#import <UIKit/UIKit.h>
#import "LoadingView.h"
#import "Masonry.h"
@interface LoadingMaskView : UIView

@property (nonatomic,strong) LoadingView *loadingView;





//显示加载框
- (void)show;

//关闭加载框
- (void)close;

+(LoadingMaskView *)shareLoadingMaskView;
-(void)addLoadingActionView;
//-(void)addLaodingViewOnView:(UIView *)superView;

//-(id)initIsLikeSynchro:(BOOL)isSynchro;

@end
