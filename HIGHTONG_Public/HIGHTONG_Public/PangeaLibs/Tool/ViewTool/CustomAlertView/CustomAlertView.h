//
//  CustomAlertView.h
//  CustomAlertView
//
//  Created by MATRIX on 14-3-18.
//  Copyright (c) 2014年 SkyWorth-HighTong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomAlertViewDelegate;

@interface CustomAlertView : UIWindow
{
    UIView *_maskView;
    UIView *_mainView;
    id<CustomAlertViewDelegate> _alertDelegate;
    UITextView *userMesTV;//用户中心弹框所需textView
}
@property (nonatomic,strong)NSURL *URLStr;

@property (nonatomic,strong)NSString *messageStr;

//UGC点击新增视频时弹出的提示按钮
- (id)initWithButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;


- (id)initWithMessage:(NSString *)message andButtonNum:(int)num withDelegate:(id<CustomAlertViewDelegate>)delegate;
- (id)initWithMoreMessage:(NSString *)message andButtonNum:(int)num withDelegate:(id<CustomAlertViewDelegate>)delegate;

- (id)initWithTitle:(NSString *)title andMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;//ButtonNum不能大于2

//UGC上传界面提示框
- (id)initWithTitle:(NSString *)title andRemandMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;//ButtonNum不能大于2

-(id)initShareViewWith;

//
- (id)initWithMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;

//医生认证多行内容的弹框
- (id)initDoctoryAuthenticAlertTitle:(NSString *)title andDescribeString:(NSString *)desStr andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnStr andOKButtonMessage:(NSString *)OKBtnStr withDelegate:(id<CustomAlertViewDelegate>)delegate;

//账号绑定所用弹框
- (id)initWithAccountBindingTitle:(NSString *)title andDescribeString:(NSString *)desStr andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnStr andOKButtonMessage:(NSString *)OKBtnStr withDelegate:(id<CustomAlertViewDelegate>)delegate;

//用户中心--清楚缓存用的弹框
- (id)initWithcacheTitle:(NSString *)title andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;//ButtonNum不能大于2


- (id)initWithTitle:(NSString *)title andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate;//ButtonNum不能大于2


- (void)show;

- (void)bottomShow;

- (void)hide;

@end

@protocol CustomAlertViewDelegate <NSObject>

- (void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

