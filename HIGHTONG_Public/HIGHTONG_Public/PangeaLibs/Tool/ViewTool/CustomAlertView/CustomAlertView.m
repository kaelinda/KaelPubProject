//
//  CustomAlertView.m
//  CustomAlertView
//
//  Created by MATRIX on 14-3-18.
//  Copyright (c) 2014年 SkyWorth-HighTong. All rights reserved.
//

#import "CustomAlertView.h"

@implementation CustomAlertView

- (void)dealloc
{
    [_maskView release];
    [_mainView release];
    [_alertDelegate release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect )frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.5;
        [self addSubview:_maskView];
        _mainView.hidden = YES;
        
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, hight - 117*kDeviceRate, widht, 117*kDeviceRate)];
        [self addSubview:_mainView];
        _mainView.backgroundColor = UIColorFromRGB(0xd0d0d0);
        
        [_mainView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom);
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.height.equalTo(@(117*kDeviceRate));
        }];

        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:UIColorFromRGB(0x040000) forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = UIColorFromRGB(0xffffff);
        
        [_mainView addSubview:confirmButton];
        
        [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(55*kDeviceRate));
            make.left.equalTo(_mainView.mas_left);
            make.right.equalTo(_mainView.mas_right);
            make.top.equalTo(_mainView.mas_top);
        }];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:UIColorFromRGB(0x040000) forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = UIColorFromRGB(0xffffff);
        
        [_mainView addSubview:cancleButton];
        
        [cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(55*kDeviceRate));
            make.left.equalTo(_mainView.mas_left);
            make.right.equalTo(_mainView.mas_right);
            make.bottom.equalTo(_mainView.mas_bottom);
        }];
        
        
    }
    return self;
    
}


- (id)initWithMessage:(NSString *)message andButtonNum:(int)num withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;

            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        

        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20, 140, 260, 120)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 4;
        [_mainView setClipsToBounds:YES];
        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        
        UITextView *messageTV = [[UITextView alloc] initWithFrame:CGRectMake(10, 20, 240, 60)];
        messageTV.text = message;
        messageTV.textAlignment = NSTextAlignmentCenter;
        messageTV.textColor = [UIColor blackColor];
        messageTV.userInteractionEnabled = NO;
        messageTV.font = [UIFont systemFontOfSize:14];
        messageTV.editable = NO;
        messageTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:messageTV];
        [messageTV release];
        

        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake(280 / 2 - 1, 81, 1, 39)];
        [sepLine_ver setBackgroundColor: [UIColor colorWithRed:189.0f/255.0f green:187.0f/255.0f blue:186.0f/255.0f alpha:1.0f]];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:@"确 定" forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:14];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [confirmButton setImage:[UIImage imageNamed:] forState:<#(UIControlState)#>]
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:@"取 消" forState:UIControlStateNormal];
        [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:14];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            sepLine_ver.hidden = YES;
            confirmButton.frame = CGRectMake(0, 120 - 55, 280, 55);
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 65, 280, 1)];
            [sepLine_hor setBackgroundColor: [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f]];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)
        {
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 80, 260, 1)];
            [sepLine_hor setBackgroundColor: [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f]];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            confirmButton.frame =CGRectMake(260 / 2 , 120 - 39, 260 / 2 - 1, 39);
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            
            cancleButton.frame = CGRectMake(0, 120 - 39, 260 / 2 - 1, 39);
            [_mainView addSubview:cancleButton];
            [cancleButton release];
        }
    }
    
    return self;
}


- (id)initWithMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kRateSize, 140*kRateSize, 230*kRateSize, 101*kRateSize)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        //        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        _mainView.backgroundColor = UIColorFromRGB(0xffffff);
        
        
        
        
        
        UITextView *messageTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 17*kRateSize, 220*kRateSize, 30*kRateSize)];
        messageTV.text = message;
        messageTV.textAlignment = NSTextAlignmentCenter;
        messageTV.textColor = UIColorFromRGB(0x333333);
        messageTV.userInteractionEnabled = NO;
        messageTV.font = [UIFont systemFontOfSize:13];
        messageTV.editable = NO;
        messageTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:messageTV];
        [messageTV release];
        
        
        
     
        
        
        
        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((230/2)*kRateSize- 1, 66*kRateSize, 1*kRateSize, 35*kRateSize)];
        [sepLine_ver setBackgroundColor: UIColorFromRGB(0xeeeeee)];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
    
        if (num == 1) {
            sepLine_ver.hidden = YES;
            confirmButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            
            
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            sepLine_ver.hidden = YES;
            cancleButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [confirmButton release];
            
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            
            
        }else if (num == 2)
        {
            
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor:UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];

            
           
            cancleButton.frame =CGRectMake(0,  66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);
            [_mainView addSubview:cancleButton];
            [cancleButton release];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            
            confirmButton.frame = CGRectMake((230 / 2 - 1)*kRateSize, 66*kRateSize,(230 / 2 - 1)*kRateSize, 35*kRateSize);
            [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton release];

        }
    }
    
    return self;
}


- (id)initWithMoreMessage:(NSString *)message andButtonNum:(int)num withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            NSInteger orientation = [[UIDevice currentDevice] orientation];
            if (orientation == 1) {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }else{
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                moveStatus = 0;
            }
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20, 140, 280, 140)];
        _mainView.center = self.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        
        UITextView *messageTV = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, 260, 80)];
        messageTV.text = message;
        messageTV.textAlignment = NSTextAlignmentCenter;
        messageTV.textColor = UIColorFromRGB(0x333333);
        messageTV.userInteractionEnabled = NO;
        messageTV.font = [UIFont systemFontOfSize:16];
        messageTV.editable = NO;
        messageTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:messageTV];
        [messageTV release];
        
        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake(280 / 2 - 1, 106, 1, 34)];
        [sepLine_ver setBackgroundColor: [UIColor colorWithRed:189.0f/255.0f green:187.0f/255.0f blue:186.0f/255.0f alpha:1.0f]];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:@"确 定" forState:UIControlStateNormal];
        [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:14];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:@"取 消" forState:UIControlStateNormal];
        [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:14];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            sepLine_ver.hidden = YES;
            confirmButton.frame = CGRectMake(0, 140 - 50, 280, 50);
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 105, 280, 1)];
            [sepLine_hor setBackgroundColor: [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f]];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)
        {
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 105, 280, 1)];
            [sepLine_hor setBackgroundColor: [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f]];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            confirmButton.frame =CGRectMake(280 / 2 + 1, 120 - 39+20, 280 / 2 - 1, 39);
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            
            cancleButton.frame = CGRectMake(0, 120 - 39+20, 280 / 2 - 1, 39);
            [_mainView addSubview:cancleButton];
            [cancleButton release];
        }
    }
    
    return self;

}

-(NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpace; // 调整行间距
    NSRange range = NSMakeRange(0, [string length]);
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributedString;
}


- (id)initDoctoryAuthenticAlertTitle:(NSString *)title andDescribeString:(NSString *)desStr andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnStr andOKButtonMessage:(NSString *)OKBtnStr withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kDeviceRate, 140*kDeviceRate, 290*kDeviceRate, 150*kDeviceRate)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        [self addSubview:_mainView];
        _mainView.backgroundColor = UIColorFromRGB(0xffffff);
        
        if (num == 1) {
            
            UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kDeviceRate, 10*kDeviceRate, 280*kDeviceRate, 25*kDeviceRate)];
            titleTV.text = title;
            titleTV.textAlignment = NSTextAlignmentCenter;
            titleTV.textColor = UIColorFromRGB(0x151515);
            titleTV.userInteractionEnabled = NO;
            titleTV.font = [UIFont systemFontOfSize:16*kDeviceRate];
            titleTV.editable = NO;
            titleTV.backgroundColor = [UIColor clearColor];
            [_mainView addSubview:titleTV];
            [titleTV release];
            
            
            userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kDeviceRate, 40*kDeviceRate, 280*kDeviceRate, 50*kDeviceRate)];
            userMesTV.textColor = UIColorFromRGB(0x151515);
            userMesTV.userInteractionEnabled = NO;
            userMesTV.editable = NO;
            userMesTV.backgroundColor = [UIColor clearColor];
            userMesTV.attributedText = [self getAttributedStringWithString:desStr lineSpace:5];
            userMesTV.textAlignment = NSTextAlignmentCenter;
            userMesTV.font = [UIFont systemFontOfSize:14*kDeviceRate];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
        
            
            
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 110*kDeviceRate, 290*kDeviceRate, 1*kDeviceRate)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            
            UIButton *cancleButton = [[UIButton alloc] init];
            [cancleButton setTitle:cancelBtnStr forState:UIControlStateNormal];
            cancleButton.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            cancleButton.tag = 11;
            [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cancleButton.backgroundColor = [UIColor clearColor];
            cancleButton.frame = CGRectMake(0, 111*kDeviceRate, 290*kDeviceRate, 39*kDeviceRate);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            
        }else if (num == 2)
        {
            
            UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kDeviceRate, 10*kDeviceRate, 280*kDeviceRate, 25*kDeviceRate)];
            titleTV.text = title;
            titleTV.textAlignment = NSTextAlignmentCenter;
            titleTV.textColor = UIColorFromRGB(0x151515);
            titleTV.userInteractionEnabled = NO;
            titleTV.font = [UIFont systemFontOfSize:16*kDeviceRate];
            titleTV.editable = NO;
            titleTV.backgroundColor = [UIColor clearColor];
            [_mainView addSubview:titleTV];
            [titleTV release];
            
            
            userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kDeviceRate, 40*kDeviceRate, 280*kDeviceRate, 50*kDeviceRate)];
            userMesTV.textColor = UIColorFromRGB(0x151515);
            userMesTV.userInteractionEnabled = NO;
            userMesTV.editable = NO;
            userMesTV.backgroundColor = [UIColor clearColor];
            userMesTV.attributedText = [self getAttributedStringWithString:desStr lineSpace:5];
            userMesTV.textAlignment = NSTextAlignmentCenter;
            userMesTV.font = [UIFont systemFontOfSize:14*kDeviceRate];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            
            
            
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 110*kDeviceRate, 290*kDeviceRate, 1*kDeviceRate)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            
            UIButton *cancleButton = [[UIButton alloc] init];
            [cancleButton setTitle:cancelBtnStr forState:UIControlStateNormal];
            cancleButton.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            cancleButton.tag = 11;
            [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cancleButton.backgroundColor = [UIColor clearColor];
            cancleButton.frame = CGRectMake(0, 111*kDeviceRate, 290*kDeviceRate/2-1*kDeviceRate, 39*kDeviceRate);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            
            UIButton *confirmButton = [[UIButton alloc] init];
            [confirmButton setTitle:OKBtnStr forState:UIControlStateNormal];
            confirmButton.titleLabel.font = [UIFont systemFontOfSize:14*kDeviceRate];
            confirmButton.tag = 10;
            [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            confirmButton.backgroundColor = [UIColor clearColor];
            confirmButton.frame = CGRectMake(290*kDeviceRate/2+1*kDeviceRate, 111*kDeviceRate, 290*kDeviceRate/2-1*kDeviceRate, 39*kDeviceRate);
            confirmButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:confirmButton];
            [confirmButton setTitleColor:UIColorFromRGB(0x3ab7f5) forState:UIControlStateNormal];
            
            
            UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake(290*kDeviceRate / 2 - 1*kDeviceRate, 109*kDeviceRate, 1*kDeviceRate, 40*kDeviceRate)];
            [sepLine_ver setBackgroundColor: App_line_color];
            [_mainView addSubview:sepLine_ver];
            [sepLine_ver release];
        }
    }
    return self;

}


- (id)initWithAccountBindingTitle:(NSString *)title andDescribeString:(NSString *)desStr andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnStr andOKButtonMessage:(NSString *)OKBtnStr withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kRateSize, 140*kRateSize, 230*kRateSize, 101*kRateSize)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        [self addSubview:_mainView];
        _mainView.backgroundColor = UIColorFromRGB(0xffffff);
        
        if (num == 1) {
            
            UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 6*kRateSize, 220*kRateSize, 24*kRateSize)];
            titleTV.text = title;
            titleTV.textAlignment = NSTextAlignmentCenter;
            titleTV.textColor = UIColorFromRGB(0x333333);
            titleTV.userInteractionEnabled = NO;
            titleTV.font = [UIFont systemFontOfSize:15];
            titleTV.editable = NO;
            titleTV.backgroundColor = [UIColor clearColor];
            [_mainView addSubview:titleTV];
            [titleTV release];
            
            
            userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 30*kRateSize, 220*kRateSize, 35*kRateSize)];
            userMesTV.textColor = UIColorFromRGB(0x333333);
            userMesTV.userInteractionEnabled = NO;
            userMesTV.editable = NO;
            userMesTV.backgroundColor = [UIColor clearColor];
            userMesTV.attributedText = [self getAttributedStringWithString:desStr lineSpace:5];
            userMesTV.textAlignment = NSTextAlignmentCenter;
            
            UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((230/2)*kRateSize- 1, 66*kRateSize, 1*kRateSize, 35*kRateSize)];
            [sepLine_ver setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_ver];
            [sepLine_ver release];
            
            UIButton *cancleButton = [[UIButton alloc] init];
            [cancleButton setTitle:cancelBtnStr forState:UIControlStateNormal];
            [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
            cancleButton.tag = 11;
            [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cancleButton.backgroundColor = [UIColor clearColor];

#pragma mark-------------
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            sepLine_ver.hidden = YES;
            cancleButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];//App_selected_color
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)
        {
//            UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 0*kRateSize, 220*kRateSize, 23*kRateSize)];
            UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(5*kRateSize, 3*kRateSize, 220*kRateSize, 20*kRateSize)];
            titleLB.text = title;
            titleLB.textAlignment = NSTextAlignmentCenter;
            titleLB.textColor = UIColorFromRGB(0x333333);
            titleLB.userInteractionEnabled = NO;
            titleLB.font = [UIFont systemFontOfSize:15];
//            titleTV.editable = NO;
            titleLB.backgroundColor = [UIColor clearColor];
            [_mainView addSubview:titleLB];
            [titleLB release];
            
            
//            userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 23*kRateSize, 220*kRateSize, 42*kRateSize)];
            UILabel *userMesLB = [[UILabel alloc] initWithFrame:CGRectMake(5*kRateSize, 23*kRateSize, 220*kRateSize, 42*kRateSize)];
            userMesLB.textColor = UIColorFromRGB(0x333333);
            userMesLB.userInteractionEnabled = NO;
//            userMesTV.editable = NO;
            userMesLB.backgroundColor = [UIColor clearColor];
            userMesLB.attributedText = [self getAttributedStringWithString:desStr lineSpace:5];
            userMesLB.numberOfLines = 0;
            userMesLB.textAlignment = NSTextAlignmentCenter;
            userMesLB.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesLB];
            [userMesLB release];

            
            UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((230/2)*kRateSize- 1, 66*kRateSize, 1*kRateSize, 35*kRateSize)];
            [sepLine_ver setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_ver];
            [sepLine_ver release];
            
            UIButton *confirmButton = [[UIButton alloc] init];
            [confirmButton setTitle:OKBtnStr forState:UIControlStateNormal];
            [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
            confirmButton.tag = 10;
            [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            confirmButton.backgroundColor = [UIColor clearColor];
            
            UIButton *cancleButton = [[UIButton alloc] init];
            [cancleButton setTitle:cancelBtnStr forState:UIControlStateNormal];
            [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
            cancleButton.tag = 11;
            [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cancleButton.backgroundColor = [UIColor clearColor];

#pragma mark--------------
        
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor:UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            cancleButton.frame =CGRectMake(0,  66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);
            [_mainView addSubview:cancleButton];
            [cancleButton release];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            confirmButton.frame = CGRectMake((230 / 2 - 1)*kRateSize, 66*kRateSize,(230 / 2 - 1)*kRateSize, 35*kRateSize);
            [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton release];
        }
    }
    return self;
    
}


- (id)initWithcacheTitle:(NSString *)title andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kRateSize, 140*kRateSize, 230*kRateSize, 101*kRateSize)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
//        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        _mainView.backgroundColor = UIColorFromRGB(0xffffff);
        
        
        UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 6*kRateSize, 220*kRateSize, 24*kRateSize)];
        titleTV.text = @"当前缓存包括您观看的视频缓存";
        titleTV.textAlignment = NSTextAlignmentCenter;
        titleTV.textColor = UIColorFromRGB(0x333333);
        titleTV.userInteractionEnabled = NO;
        titleTV.font = [UIFont systemFontOfSize:13];
        titleTV.editable = NO;
        titleTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:titleTV];
        [titleTV release];
        
        
        
        userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 30*kRateSize, 220*kRateSize, 35*kRateSize)];
        userMesTV.textAlignment = NSTextAlignmentCenter;
        userMesTV.textColor = UIColorFromRGB(0x333333);
        userMesTV.userInteractionEnabled = NO;
        userMesTV.editable = NO;
        userMesTV.backgroundColor = [UIColor clearColor];/////
//        userMesTV.text =  @"当前缓存包括您观看的视频缓存\n您确定要清除吗？";
        userMesTV.text = @"您确定要清除吗？";
        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((230/2)*kRateSize- 1, 66*kRateSize, 1*kRateSize, 35*kRateSize)];
        [sepLine_ver setBackgroundColor: UIColorFromRGB(0xeeeeee)];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            sepLine_ver.hidden = YES;
            cancleButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [confirmButton release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)//目前只调整了按钮为1时的布局格式，如果用两个按钮，下面布局需要调整
        {
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor:UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            //<<<<<<< .mine
            //cancleButton.frame =CGRectMake(0, 66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);
            //=======
            cancleButton.frame =CGRectMake(0,  66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);
            //>>>>>>> .r90
            [_mainView addSubview:cancleButton];
            [cancleButton release];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            //<<<<<<< .mine
            //            //confirmButton.frame = CGRectMake((230 / 2 - 1)*kRateSize , 66*kRateSize, 260 / 2 - 1, 35*kRateSize);
            //=======
            confirmButton.frame = CGRectMake((230 / 2 - 1)*kRateSize, 66*kRateSize,(230 / 2 - 1)*kRateSize, 35*kRateSize);
            //>>>>>>> .r90
            [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton release];
        }
    }
    return self;

}

- (id)initWithTitle:(NSString *)title andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isAfterIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kRateSize, 140*kRateSize, 230*kRateSize, 101*kRateSize)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        
        UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 5*kRateSize, 220*kRateSize, 25*kRateSize)];
        titleTV.text = title;
        titleTV.textAlignment = NSTextAlignmentCenter;
        titleTV.textColor = UIColorFromRGB(0x333333);
        titleTV.userInteractionEnabled = NO;
        titleTV.font = [UIFont systemFontOfSize:15];
        titleTV.editable = NO;
        titleTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:titleTV];
        [titleTV release];
        
        userMesTV = [[UITextView alloc] initWithFrame:CGRectMake(5*kRateSize, 30*kRateSize, 220*kRateSize, 30*kRateSize)];
        userMesTV.textAlignment = NSTextAlignmentCenter;
        userMesTV.textColor = UIColorFromRGB(0x333333);
        userMesTV.userInteractionEnabled = NO;
        userMesTV.editable = NO;
        userMesTV.backgroundColor = [UIColor clearColor];/////

        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((230 / 2)*kRateSize- 1, 66*kRateSize, 1*kRateSize, 35*kRateSize)];
        [sepLine_ver setBackgroundColor:UIColorFromRGB(0xeeeeee)];

        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            sepLine_ver.hidden = YES;
            cancleButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            cancleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [confirmButton release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)
        {
            userMesTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:userMesTV];
            [userMesTV release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor:UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            cancleButton.frame =CGRectMake(0, 66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);

            [_mainView addSubview:cancleButton];
            [cancleButton release];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            
            confirmButton.frame = CGRectMake((230 / 2 - 1)*kRateSize , 66*kRateSize, (230 / 2 - 1)*kRateSize, 35*kRateSize);

            [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton release];
            
        }
    }
    
    return self;
}


- (void)setMessageStr:(NSString *)messageStr{
    userMesTV.text = messageStr;
}


- (id)initWithTitle:(NSString *)title andMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = UIColorFromRGB(0x000000);
        _maskView.alpha = 0.3;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20*kRateSize, 140*kRateSize, 230*kRateSize, 101*kRateSize)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5;
        [_mainView setClipsToBounds:YES];
        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        
        UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(10*kRateSize, 10*kRateSize, 220*kRateSize, 25*kRateSize)];
        titleTV.text = title;
        titleTV.textAlignment = NSTextAlignmentCenter;
        titleTV.textColor = UIColorFromRGB(0x333333);
        titleTV.userInteractionEnabled = NO;
        titleTV.font = [UIFont systemFontOfSize:15];
        titleTV.editable = NO;
        titleTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:titleTV];
        [titleTV release];
        
        UITextView *messageTV = [[UITextView alloc] initWithFrame:CGRectMake(10*kRateSize, 35*kRateSize, 220*kRateSize, 50*kRateSize)];
        messageTV.text = message;
        messageTV.textAlignment = NSTextAlignmentCenter;
        messageTV.textColor = UIColorFromRGB(0x333333);
        messageTV.userInteractionEnabled = NO;
        messageTV.editable = NO;
        messageTV.backgroundColor = [UIColor clearColor];/////
        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake((260 / 2)*kRateSize- 1, 90*kRateSize, 1*kRateSize, 55*kRateSize)];
        [sepLine_ver setBackgroundColor: [UIColor colorWithRed:189.0f/255.0f green:187.0f/255.0f blue:186.0f/255.0f alpha:1.0f]];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:16];
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:13];
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            
            messageTV.font = [UIFont systemFontOfSize:13];
            [_mainView addSubview:messageTV];
            [messageTV release];
            sepLine_ver.hidden = YES;
            cancleButton.frame = CGRectMake(0, 66*kRateSize, 230*kRateSize, 35*kRateSize);
            [_mainView addSubview:cancleButton];
            [cancleButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [confirmButton release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 66*kRateSize, 230*kRateSize, 1*kRateSize)];
            [sepLine_hor setBackgroundColor: UIColorFromRGB(0xeeeeee)];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
        }else if (num == 2)//目前只调整了按钮为1时的布局格式，如果用两个按钮，下面布局需要调整
        {
            messageTV.font = [UIFont systemFontOfSize:14];
            [_mainView addSubview:messageTV];
            [messageTV release];
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 90, 260, 1)];
            [sepLine_hor setBackgroundColor: [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f]];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor release];
            
            cancleButton.frame =CGRectMake(0, 140 - 45, 260 / 2 - 1, 45);
            [_mainView addSubview:cancleButton];
            [cancleButton release];
            
            confirmButton.frame = CGRectMake(260 / 2 , 140 - 45, 260 / 2 - 1, 45);
            [confirmButton setTitleColor:App_selected_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton release];
        }
    }
    
    return self;
}

//UGC上传界面提示框
- (id)initWithTitle:(NSString *)title andRemandMessage:(NSString *)message andButtonNum:(int)num andCancelButtonMessage:(NSString *)cancelBtnMessage andOtherButtonMessage:(NSString *)otherBtnMessage withDelegate:(id<CustomAlertViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
            
            
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }
        
        
        _alertDelegate = [delegate retain];
        self.frame = CGRectMake(0, 0, hight, widht);
        
        self.backgroundColor = [UIColor clearColor];
        self.windowLevel = UIWindowLevelAlert;
        
        _maskView = [[UIView alloc] init];
        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = self.center;
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0;
        [self addSubview:_maskView];
        
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(20, 140, 295*kDeviceRate, 129*kDeviceRate)];
        _mainView.center = _maskView.center;
        _mainView.layer.cornerRadius = 5*kDeviceRate;
        [_mainView setClipsToBounds:YES];
        _mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_msgbox"]];//////
        [self addSubview:_mainView];
        
        [_mainView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(290*kDeviceRate));
            make.height.equalTo(@(130*kDeviceRate));
        }];
        
        UITextView *titleTV = [[UITextView alloc] initWithFrame:CGRectMake(0, 20*kDeviceRate,  295*kDeviceRate, 20*kDeviceRate)];
        titleTV.text = title;
        titleTV.textAlignment = NSTextAlignmentCenter;
        titleTV.textColor = UIColorFromRGB(0x151515);
        titleTV.userInteractionEnabled = NO;
        titleTV.font = HT_FONT_SECOND;
        titleTV.editable = NO;
        titleTV.backgroundColor = [UIColor clearColor];/////
        [_mainView addSubview:titleTV];
        
        [titleTV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_mainView.mas_centerX);
            make.top.equalTo(_mainView.mas_top).offset(20*kDeviceRate);
            make.width.equalTo(@(290*kDeviceRate));
            make.height.equalTo(@(20*kDeviceRate));
        }];
        [titleTV release];
        
        UITextView *messageTV = [[UITextView alloc] initWithFrame:CGRectMake(0, 58*kDeviceRate, 295*kDeviceRate, 15*kDeviceRate)];
        messageTV.text = message;
        messageTV.textAlignment = NSTextAlignmentCenter;
        messageTV.textColor = UIColorFromRGB(0x151515);
        messageTV.userInteractionEnabled = NO;
        messageTV.editable = NO;
        messageTV.backgroundColor = [UIColor clearColor];/////
        
        UIView *sepLine_ver = [[UIView alloc] initWithFrame:CGRectMake(290*kDeviceRate / 2 - 1*kDeviceRate, 89*kDeviceRate, 1*kDeviceRate, 40*kDeviceRate)];
        [sepLine_ver setBackgroundColor: App_line_color];
        [_mainView addSubview:sepLine_ver];
        [sepLine_ver mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_mainView.mas_bottom).offset(-40*kDeviceRate);
            make.top.equalTo(_mainView.mas_top).offset(89*kDeviceRate);
            make.width.equalTo(@(290*kDeviceRate));
            make.height.equalTo(@(1*kDeviceRate));
        }];
        [sepLine_ver release];
        
        UIButton *confirmButton = [[UIButton alloc] init];
        [confirmButton setTitle:otherBtnMessage forState:UIControlStateNormal];
        [confirmButton setTitleColor:APP_TirdBlack_color forState:UIControlStateNormal];
        confirmButton.titleLabel.font = HT_FONT_THIRD;
        confirmButton.tag = 10;
        [confirmButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        confirmButton.backgroundColor = [UIColor clearColor];
        
        UIButton *cancleButton = [[UIButton alloc] init];
        [cancleButton setTitle:cancelBtnMessage forState:UIControlStateNormal];
        [cancleButton setTitleColor:APP_TirdBlack_color forState:UIControlStateNormal];
        cancleButton.titleLabel.font = HT_FONT_THIRD;
        cancleButton.tag = 11;
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cancleButton.backgroundColor = [UIColor clearColor];
        
        if (num == 1) {
            
            messageTV.font = HT_FONT_THIRD;
            [_mainView addSubview:messageTV];
            [messageTV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_mainView.mas_centerX);
                make.bottom.equalTo(_mainView.mas_bottom).offset(-63*kDeviceRate);
                make.width.equalTo(@(290*kDeviceRate));
                make.height.equalTo(@(15*kDeviceRate));
            }];
            [messageTV release];
            cancleButton.frame = CGRectMake(0, 89*kDeviceRate, 290*kDeviceRate, 40*kDeviceRate);
            [_mainView addSubview:cancleButton];
            [cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_mainView.mas_left);
                make.bottom.equalTo(_mainView.mas_bottom);
                make.width.equalTo(@(289*kDeviceRate));
                make.height.equalTo(@(40*kDeviceRate));
            }];

            [cancleButton setTitleColor:App_blue_color forState:UIControlStateNormal];
            [confirmButton release];

        }else if (num == 2)
        {
            messageTV.font = HT_FONT_THIRD;
            [_mainView addSubview:messageTV];
            
            [messageTV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_mainView.mas_centerX);
                make.bottom.equalTo(_mainView.mas_bottom).offset(-63*kDeviceRate);
                make.width.equalTo(@(290*kDeviceRate));
                make.height.equalTo(@(15*kDeviceRate));
            }];
            
            [messageTV release];
            
            UIView *sepLine_hor = [[UIView alloc] initWithFrame:CGRectMake(0, 89*kDeviceRate, 295*kDeviceRate, 1*kDeviceRate)];
            [sepLine_hor setBackgroundColor: App_line_color];
            [_mainView addSubview:sepLine_hor];
            [sepLine_hor mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_mainView.mas_centerX);
                make.bottom.equalTo(_mainView.mas_bottom);
                make.width.equalTo(@(1*kDeviceRate));
                make.height.equalTo(@(40*kDeviceRate));
            }];
            [sepLine_hor release];
            
            cancleButton.frame =CGRectMake(0, 89*kDeviceRate, 290*kDeviceRate / 2 - 1*kDeviceRate, 40*kDeviceRate);
            [cancleButton setTitleColor:App_blue_color forState:UIControlStateNormal];
            [_mainView addSubview:cancleButton];
            [cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_mainView.mas_left);
                make.bottom.equalTo(_mainView.mas_bottom);
                make.width.equalTo(@(289*kDeviceRate/2));
                make.height.equalTo(@(40*kDeviceRate));
            }];
            [cancleButton release];
            
            confirmButton.frame = CGRectMake(290*kDeviceRate / 2 , 89*kDeviceRate, 290*kDeviceRate / 2 - 1*kDeviceRate, 40*kDeviceRate);
            [confirmButton setTitleColor:App_blue_color forState:UIControlStateNormal];
            [_mainView addSubview:confirmButton];
            [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(_mainView.mas_right);
                make.bottom.equalTo(_mainView.mas_bottom);
                make.width.equalTo(@(289*kDeviceRate/2));
                make.height.equalTo(@(40*kDeviceRate));
            }];
            [confirmButton release];
        }
    }
    
    return self;
}


- (void)buttonClicked:(UIButton *)aButton
{
    [self dismissAlertAnimation];
    if (_alertDelegate && [_alertDelegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
        if (aButton.tag == 10) {
            [_alertDelegate alertView:self clickedButtonAtIndex:0];
        }
        else if (aButton.tag == 11)
        {
            [_alertDelegate alertView:self clickedButtonAtIndex:1];
        }
    }
    [self release];
}

- (void)show
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self retain];
    [self makeKeyAndVisible];
    [self showAlertAnimation];
}

- (void)bottomShow
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_ROTATION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self retain];
    [self makeKeyAndVisible];
    [self bottomShowAlertAnimation];
}


- (void)hide
{
    [self dismissAlertAnimation];
    
    [self release];
}

-(CGAffineTransform)transformForOrientation
{
    CGAffineTransform transform ;
    //获取设备的方向
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication ] statusBarOrientation];
    
    switch (orientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
            transform =  CGAffineTransformMakeRotation(M_PI * 1.5);
            if (isAfterIOS9) {
                transform = CGAffineTransformMakeRotation(0);
            }
            break;
        case UIInterfaceOrientationLandscapeRight:
            transform = CGAffineTransformMakeRotation(M_PI / 2);
            if (isAfterIOS9) {
                transform = CGAffineTransformMakeRotation(0);
            }
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            transform = CGAffineTransformMakeRotation(-M_PI);
           
            break;
        default:
            transform = CGAffineTransformIdentity;
           
            break;
    }
    return transform;
}

- (void)bottomShowAlertAnimation
{
    
    _mainView.transform = CGAffineTransformScale([self transformForOrientation], 1.0f, 1.0f);
    
    [UIView animateWithDuration:0.2 animations:^{
        [UIView setAnimationDelegate:self];
        _mainView.alpha = 1.0f;
        _maskView.alpha = 0.5f;
        _maskView.transform = CGAffineTransformScale([self transformForOrientation], 1.0, 1.0);
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }

        
        _mainView.hidden = NO;
       
        
        NSLog(@"window %@",NSStringFromCGRect(self.window.frame));
    } completion:^(BOOL finished) {
        
    }];

    
}

- (void)showAlertAnimation
{
  
    
//    NSInteger orientation = [[UIDevice currentDevice] orientation];
//    if ([[UIDevice currentDevice] orientation]== UIInterfaceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation]== UIInterfaceOrientationLandscapeRight) {
//        
//        _mainView.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
//    }
    
    _mainView.transform = CGAffineTransformScale([self transformForOrientation], 1.0f, 1.0f);

    [UIView animateWithDuration:0.2 animations:^{
        [UIView setAnimationDelegate:self];
        _mainView.alpha = 1.0f;
        _maskView.alpha = 0.5f;
        _maskView.transform = CGAffineTransformScale([self transformForOrientation], 1.0, 1.0);
        float widht;
        float hight;
        float moveStatus;
        
        if (isIOS8)
        {
            
            NSInteger xcount = kDeviceWidth-KDeviceHeight;
            
            if (xcount >0) {
                
                widht = kDeviceWidth;
                hight = KDeviceHeight;
                
            }else
            {
                widht = KDeviceHeight;
                hight = kDeviceWidth;
            }
            
        }else if (isIOS7)
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 0;
        }else
        {
            widht = KDeviceHeight;
            hight = kDeviceWidth;
            moveStatus = 20;
        }

        _maskView.frame = CGRectMake(0, 0, hight, widht);
        _maskView.center = CGPointMake(hight/2, widht/2);
//        _maskView.center = self.center;
        _mainView.center = _maskView.center;
        
        
        NSLog(@"window %@",NSStringFromCGRect(self.window.frame));
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissAlertAnimation
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIView animateWithDuration:0.2 animations:^{
        [UIView setAnimationDelegate:self];
        _mainView.alpha = 0;
        _maskView.alpha = 0;
        _mainView.transform = CGAffineTransformScale([self transformForOrientation], 1.5, 1.5);
    } completion:^(BOOL finished) {
        [_mainView removeFromSuperview];
        [_maskView removeFromSuperview];
    }];
}




@end
