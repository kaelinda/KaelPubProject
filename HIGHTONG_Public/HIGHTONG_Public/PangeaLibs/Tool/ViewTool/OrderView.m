//
//  OrderView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/11/28.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "OrderView.h"

@implementation OrderView


-(instancetype)init{
    self = [super init];
    if (self) {
        [self setupSubview];
    }
    return self;
}

-(void) setupSubview{
    WS(wself);

    self.backgroundColor = CLEARCOLOR;
    
    _backImageView = [[UIImageView alloc] init];
    _backImageView.image = [UIImage imageNamed:@""];//健康电视要求
    _backImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:_backImageView];
    [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
 
        make.edges.equalTo(wself);
    }];
    
    
    _orderLbael = [[UILabel alloc] init];
    _orderLbael.backgroundColor = CLEARCOLOR;
    _orderLbael.text = @"本影片为订购影片!";
    _orderLbael.font = [UIFont systemFontOfSize:17.0f];
    _orderLbael.textColor = [UIColor whiteColor];
    _orderLbael.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_orderLbael];
    
    [_orderLbael mas_makeConstraints:^(MASConstraintMaker *make) {
       
        
        make.size.mas_equalTo(CGSizeMake(200, 30*kRateSize));
        make.centerX.mas_equalTo(wself.mas_centerX);
        make.centerY.mas_equalTo(wself.mas_centerY).offset(-15*kRateSize);
    }];
    
    _orderBtn = [[UIButton_Block alloc] init];
    _orderBtn.backgroundColor = App_selected_color;
    [_orderBtn setTitle:@"订购" forState:UIControlStateNormal];
    [_orderBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [_orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _orderBtn.layer.cornerRadius = 3.0f;
    
    _orderBtn.Click = ^(UIButton_Block *btn,NSString *name){
        if (wself.orderClick) {
            NSLog(@"点击了订购按钮~~");
            wself.orderClick();
            
        }else
        {
            NSLog(@"没有实现订购Block");
        }
        
    };
    [self addSubview:_orderBtn];
    
    [_orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.size.mas_equalTo(CGSizeMake(60, 33));
        make.top.mas_equalTo(_orderLbael.mas_bottom).offset(12*kRateSize);
        make.centerX.mas_equalTo(wself.centerX);
        
    }];
    
    
    
    


}

-(void)setProgramType:(NSInteger)programType{
    switch (programType) {
        case 1:
        {
            _orderLbael.text = @"本影片为订购影片!";
            break;

        }
        case 2:
        {
            _orderLbael.text = @"本剧集为订购剧集!";

            break;
            
        }
        default:
            break;
    }

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
