//
//  LoadingMaskView.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/12/31.
//  Copyright © 2015年 创维海通. All rights reserved.
//

#import "LoadingMaskView.h"

static LoadingMaskView *loadingMaskView = nil;

@implementation LoadingMaskView

+(LoadingMaskView *)shareLoadingMaskView{

    @synchronized(self){
        if (loadingMaskView == nil) {
//            loadingMaskView = [[self alloc] initIsLikeSynchro:YES];
            loadingMaskView = [[LoadingMaskView alloc] init];

            
        }
    }
    
    return loadingMaskView;
}
-(void)addLoadingActionView{
    WS(wself);

    if ([UIApplication sharedApplication].keyWindow.rootViewController.navigationController) {
        [[UIApplication sharedApplication].keyWindow.rootViewController.navigationController.view addSubview:wself];
        UIView *superView = [UIApplication sharedApplication].keyWindow.rootViewController.navigationController.view;
        CGSize superViewSize = CGSizeMake(superView.bounds.size.width, superView.bounds.size.height-140);
        [wself mas_makeConstraints:^(MASConstraintMaker *make) {

            make.size.mas_equalTo(superViewSize);
            make.center.mas_equalTo(superView);
        }];
        
    }else{
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        UIView *superView = [UIApplication sharedApplication].keyWindow;
        CGSize superViewSize = CGSizeMake(superView.bounds.size.width, superView.bounds.size.height-140);

        [wself mas_makeConstraints:^(MASConstraintMaker *make) {

            make.size.mas_equalTo(superViewSize);
            make.center.mas_equalTo(superView);
        }];
    }
    
    CGSize imageSize = CGSizeMake(46*kRateSize, 46*kRateSize);
    if (!_loadingView) {
        _loadingView = [[LoadingView alloc] initWithImage:[UIImage imageNamed:@"lightLoading"]];
    }else{
    
        return;
    }
    
    [wself addSubview:_loadingView];
    
    [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(imageSize);
        make.center.mas_equalTo(wself);
    }];

}


- (void)show{
    
    if (_loadingView.hidden == NO) {
        return;
    }
    _loadingView.hidden = NO;//开启动画

    [self addLoadingActionView];//将单利自己添加到 父视图
}

- (void)close{
    if (_loadingView.hidden == YES) {
        return;
    }
    _loadingView.hidden = YES;//关掉动画

    [self removeFromSuperview];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
@end
