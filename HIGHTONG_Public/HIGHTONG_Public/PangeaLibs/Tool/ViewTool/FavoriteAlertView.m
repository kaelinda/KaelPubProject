//
//  FavoriteAlertView.m
//  HIGHTONG
//
//  Created by smart on 12-11-23.
//  Copyright (c) 2012年 ZhangDongfeng. All rights reserved.
//

#import "FavoriteAlertView.h"
#import "AppDelegate.h"

@interface FavoriteAlertView()

@property (retain, nonatomic) UILabel * contentLabel;

@end

@implementation FavoriteAlertView



- (id)init
{
    self = [super init];
    if (self) {
        [self setImage:[UIImage imageNamed:@"bg_toast"]];
//        [self setBackgroundColor:[UIColor grayColor]];
        [self setAlpha:1];
        self.contentLabel = [[UILabel alloc] init] ;
        [self.contentLabel setTextAlignment:NSTextAlignmentCenter];
        [self.contentLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.contentLabel];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutIfNeeded];
    [self.contentLabel setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

- (void)hiddenWithAnimation
{
    [UIView animateWithDuration:2 animations:^{
        [self setAlpha:0];
    } completion:^(BOOL finished) {
        [self setAlpha:1];
        [self setTransform:CGAffineTransformMakeRotation(0)];
        [self setHidden:YES];
        [self removeFromSuperview];
    }];
}
+(void)showAlertViewWithText:(NSString *)message inView:(UIView *)view andWidth:(int)width
{
    FavoriteAlertView * favoriteAV = nil;
    if (!favoriteAV) {
        favoriteAV = [[FavoriteAlertView alloc] init] ;
    }
    [favoriteAV setHidden:NO];
    [favoriteAV setFrame:CGRectMake(0, kyyy, width, 40)];
    favoriteAV.contentLabel.text = message;
    [view addSubview:favoriteAV];
    
    [favoriteAV performSelector:@selector(hiddenWithAnimation) withObject:nil afterDelay:2];
}

+(void)showOtherAlertViewWithText:(NSString *)message inView:(UIView *)view andWidth:(int)width
{
    FavoriteAlertView * favoriteAV = nil;
    if (!favoriteAV) {
        favoriteAV = [[FavoriteAlertView alloc] init] ;
    }
    [favoriteAV setHidden:NO];
    
    [favoriteAV setFrame:CGRectMake(((isAfterIOS8
                                      ?kDeviceWidth:KDeviceHeight)-180)/2, kDeviceWidth-114, 180, 40)];
   
    favoriteAV.contentLabel.text = message;
    favoriteAV.contentLabel.textColor = [UIColor whiteColor];
    [view addSubview:favoriteAV];
    
    [favoriteAV performSelector:@selector(hiddenWithAnimation) withObject:nil afterDelay:2];
}
//-------上面--
+ (void)showAlertViewWithRet:(NSInteger)aret inView:(UIView *)view andWidth:(int)width{
    
    FavoriteAlertView * favoriteAV = nil;
    if (!favoriteAV) {
        favoriteAV = [[FavoriteAlertView alloc] init] ;
    }
    favoriteAV.contentLabel.textColor = [UIColor whiteColor];
    [favoriteAV setHidden:NO];
    
    NSInteger orientation = [[UIDevice currentDevice] orientation];
    if (orientation==1) {
        [favoriteAV setFrame:CGRectMake(0, kDeviceWidth-114, kDeviceWidth, 40)];

    }else{
        [favoriteAV setFrame:CGRectMake(((isAfterIOS8?kDeviceWidth:KDeviceHeight)-180)/2, kDeviceWidth-114, 180, 40)];

        
    }
    
    switch (aret) {
        case -1001:
            favoriteAV.contentLabel.text  = @"获取数据失败";
            
        case -9:
            favoriteAV.contentLabel.text = @"获取数据失败";
            break;
        case -7:
            favoriteAV.contentLabel.text = @"频道取消发布";
            break;
        case -6:
            favoriteAV.contentLabel.text = @"该频道不提供回看功能";
            break;
        case -5:
            favoriteAV.contentLabel.text = @"频道对应此终端无播放链接";
            break;
        case -4:
            favoriteAV.contentLabel.text = @"扣费失败";
            break;
        case -3:
            favoriteAV.contentLabel.text = @"鉴权失败";
            break;
        case -1:
            favoriteAV.contentLabel.text = @"当前节目不存在";
            break;
        default:
            favoriteAV.contentLabel.text = [NSString stringWithFormat:@"获取数据失败"];
            break;
    }
    [view addSubview:favoriteAV];
    
    [favoriteAV performSelector:@selector(hiddenWithAnimation) withObject:nil afterDelay:2];
}
@end
