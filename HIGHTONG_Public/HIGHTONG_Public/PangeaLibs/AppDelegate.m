//
//  AppDelegate.m
//  HIGHTONG_Public
//
//  Created by Kael on 15/9/17.
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//  Copyright (c) 2015年 创维海通. All rights reserved.
//
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                         ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易
//

#import "AppDelegate.h"
#import "TabBarViewontroller.h"
#import "HT_NavigationController.h"
#import "GETBaseInfo.h"
#import "CustomAlertView.h"
#import "DateTools.h"
//#import "MobClick.h"
#import "UMMobClick/MobClick.h"
#import "NetWorkNotification_shared.h"
#import "BMKSingleTon.h"
#import "HTYJUserCenter.h"
#import "HT_AESCrypt.h"
#import "KLocalNotificationManager.h"
#import "SchemeManager.h"
#import "BPushSingleTon.h"
#import "UGCUploadManager.h"
#import "NotificationService.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "PlayerHeader.h"
#import "PortalManger.h"
#import "UIViewController+MessageCollect.h"

#endif


@interface AppDelegate ()<HTRequestDelegate,CustomAlertViewDelegate,UNUserNotificationCenterDelegate>
{
   
    NSInteger showNotiNum;
    NSString *_schemeURL;
    SchemeManager *_schemeManger;
    UGCUploadManager *_ugcUploadManager;
    CustomAlertView *_upLoadWorkAlert;
    GETBaseInfo *baseInfo;
    
    NSInteger badgeRemoteCount;

}

@property (nonatomic, assign) UIBackgroundTaskIdentifier  backgroundTaskIdentifier;
@property (nonatomic, retain) NSTimer                     *myTimer;

@end
@implementation AppDelegate
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

/**
 外部应用打开
 
 @param application       本应用
 @param url               传入的url
 @param sourceApplication 打开此APP的外部APP的 URLScheme ID
 @param annotation        这个我也不知道是什么东西
 
 @return 是否允许 某些APP打开
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"SchemeAction"];
    
    _schemeURL = [NSString stringWithFormat:@"%@",url];
    
    
    if (_schemeManger) {
        [_schemeManger schemeForSchemeUrl:_schemeURL];
    }
    
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
    
    //    return  [UMSocialSnsService handleOpenURL:url];
}




-(void)setupbaseAppData{
    _isLaunch = YES;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_ROTATION"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"token"];
    
    
    
    NSString *devIdStr = [[[GETBaseInfo alloc] init] getUUID];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",devIdStr] forKey:@"dev_ID"];
    
    NSLog(@"原始的deviceID————%@",devIdStr);
    
    NSString *encryptDevIDStr = [HT_AESCrypt encrypt:devIdStr password:@"Skyworth@@Hitune"];
    
    NSLog(@"加密后的deviceID————%@",encryptDevIDStr);
    
    [[NSUserDefaults standardUserDefaults] setObject:encryptDevIDStr forKey:@"ICS_deviceID"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"UGCAccessToken"];
    
    //清除区域code
    //    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"InstanceCode"];
    //    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"hospitalName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //处理状态条显示的
    _CanDisplay = YES;
    [UIApplication sharedApplication].statusBarHidden = YES;
    if ([APP_ID isEqual:JXCABLE_SERIAL_NUMBER]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    // Override point for customization after application launch.
    
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    //[request getHightongPORTALVersionWithTimeout:5];
    NSLog(@"myDate - projectList request ------>>> :%@",[NSDate date]);
    
    if(IS_MERGEPORJECTLIST){
        
        PortalManger *portalManager = [PortalManger sharedPortalManager];
        
        [portalManager retryRequestProjectListWith:kAPPNetEnvType_ALL];
        
        portalManager.callAppAlert = ^(NSString *status) {
            
            [self projectListStatus:status];
            
        };
        
    }else
    {
        
        [request getHightongPORTALProjectListWithTimeout:5];
        
    }


    [[NetWorkNotification_shared shardNetworkNotification] setCanShowToast:YES];//实时网络监测
    //注册通知的观察
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeImageView) name:@"REMOVE_IMAGEVIEW" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNetpoorView) name:@"NETCHANGED" object:nil];
    //------------
    
    
    //这个是广告图的缓存图片
    _imagev = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"launcher_568h"]];
    _imagev.backgroundColor = [UIColor whiteColor];
    _imagev.frame = CGRectMake(0, 0, kDeviceWidth<KDeviceHeight?kDeviceWidth:KDeviceHeight, KDeviceHeight>kDeviceWidth?KDeviceHeight:kDeviceWidth);
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        NSLog(@"第一次启动");
        
        _imagev.image = [UIImage imageNamed:@"guide-01"];
    }
    
    [self.window addSubview:_imagev];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //这里是1.0f后 你要做的事情
        [_imagev removeFromSuperview];
    });
    

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [self setupbaseAppData];
    
    [application setIdleTimerDisabled:YES];
    
    //处理icon 右上角的徽章的
    application.applicationIconBadgeNumber = 0;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    TabBarViewontroller *tabbarVC = [[TabBarViewontroller alloc] init];
    
    HT_NavigationController *nvc = [[HT_NavigationController alloc] initWithRootViewController:tabbarVC];
    
    self.window.rootViewController = nvc;
   
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    
    [self BPushAndAplication:application didFinishLaunchingWithOptions:launchOptions];

    
    
    //zzs处理本地通知
    NSDictionary *notiDic = [[NSDictionary alloc] initWithDictionary:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
    
    
    NSString *serviceID = [notiDic objectForKey:@"serviceID"];
    
    if ((serviceID.length > 0)) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCAL_NOTIFICATION_TO_TABVC" object:notiDic];
            //        [AlertLabel AlertInfoWithText:@"前台也走这里？？" andWith:self.window withLocationTag:1];
            NSLog(@"前台也走这里？？");
        }
    
    
    
    _ugcUploadManager = [UGCUploadManager shareInstance];
    
//    [_ugcUploadManager startUGCUploadManager];
    
    __weak CustomAlertView *weakupLoadWorkAlert = _upLoadWorkAlert;
    
    _ugcUploadManager.callAppAlert = ^(NSString *status){
        
        if (weakupLoadWorkAlert) {
            
            [weakupLoadWorkAlert resignKeyWindow];
            _upLoadWorkAlert = nil;
            
        }
        
        if (![status isEqualToString:@"又切回来了"]) {
            
            _upLoadWorkAlert = [[CustomAlertView alloc]initDoctoryAuthenticAlertTitle:@"温馨提示" andDescribeString:@"检测到当前您使用的是非wifi网络可能会\n产生流量费用，您确定要继续上传吗？" andButtonNum:2 andCancelButtonMessage:@"确认" andOKButtonMessage:@"不继续" withDelegate:self];
            
            if ([status isEqualToString:@"未开始"]) {
                
                weakupLoadWorkAlert.tag = 200;
                
                
            }else
            {
                
                weakupLoadWorkAlert.tag = 300;
                
            }
            
            [weakupLoadWorkAlert show];
            
        }
    };

    
    return YES;
}

//云推送通知开始

-(void)BPushAndAplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
            
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound + UNAuthorizationOptionBadge)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  // Enable or disable features based on authorization.
                                  if (granted) {
                                      [[UIApplication sharedApplication] registerForRemoteNotifications];
                                  }
                              }];
#endif
    }
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType myTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
//#warning 测试 开发环境 时需要修改BPushMode为BPushModeDevelopment 需要修改Apikey为自己的Apikey
    
    // 在 App 启动时注册百度云推送服务，需要提供 Apikey
    [BPushSingleTon shareInstance];

    [BPush registerChannel:launchOptions apiKey:@"CTLYi6peS5ESvA1sM6rV9NQu" pushMode:BPushModeDevelopment withFirstAction:@"打开" withSecondAction:@"关闭" withCategory:@"test" useBehaviorTextInput:YES isDebug:YES];
    
    // 禁用地理位置推送 需要再绑定接口前调用。
    
    [BPush disableLbs];
    
    if (launchOptions) {
        
        // App 是用户点击推送消息启动
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo) {
            [[BPushSingleTon shareInstance]handleNotification:userInfo];
        }
 
    }
    
#if TARGET_IPHONE_SIMULATOR
    Byte dt[32] = {0xc6, 0x1e, 0x5a, 0x13, 0x2d, 0x04, 0x83, 0x82, 0x12, 0x4c, 0x26, 0xcd, 0x0c, 0x16, 0xf6, 0x7c, 0x74, 0x78, 0xb3, 0x5f, 0x6b, 0x37, 0x0a, 0x42, 0x4f, 0xe7, 0x97, 0xdc, 0x9f, 0x3a, 0x54, 0x10};
    [self application:application didRegisterForRemoteNotificationsWithDeviceToken:[NSData dataWithBytes:dt length:32]];
#endif
    //角标清0
        //BO注释本地角标
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        //BO注释本地角标

    // 测试本地通知
    //     [self performSelector:@selector(testLocalNotifi) withObject:nil afterDelay:10.0];
    
}

//- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
//    NSDictionary * userInfo = response.notification.request.content.userInfo;
//    UNNotificationRequest *request = response.notification.request; // 收到推送的请求
//    UNNotificationContent *content = request.content; // 收到推送的消息内容
//    
//    NSNumber *badge = content.badge;  // 推送消息的角标
//    NSString *body = content.body;    // 推送消息体
//    UNNotificationSound *sound = content.sound;  // 推送消息的声音
//    NSString *subtitle = content.subtitle;  // 推送消息的副标题
//    NSString *title = content.title;  // 推送消息的标题
//    
//    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
////        NSLog(@"iOS10 收到远程通知:%@", [self logDic:userInfo]);
//        
//    }
//    else {
//        // 判断为本地通知
////        NSLog(@"iOS10 收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
//    }
//    
//    
//    
//    // Warning: UNUserNotificationCenter delegate received call to -userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: but the completion handler was never called.
//    completionHandler();  // 系统要求执行这个方法
//
//}
//
//// iOS 10收到前台通知
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    
//    NSDictionary * userInfo = notification.request.content.userInfo;
//    
//    UNNotificationRequest *request = notification.request; // 收到推送的请求
//    UNNotificationContent *content = request.content; // 收到推送的消息内容
//    NSNumber *badge = content.badge;  // 推送消息的角标
//    NSString *body = content.body;    // 推送消息体
//    UNNotificationSound *sound = content.sound;  // 推送消息的声音
//    NSString *subtitle = content.subtitle;  // 推送消息的副标题
//    NSString *title = content.title;  // 推送消息的标题
//    
//
//    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//        
//        
//    }
//    else {
//        // 判断为本地通知
//        NSLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
//    }
//    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
//    
//    
//    
//    
//}

// 此方法是 用户点击了通知，应用在前台 或者开启后台并且应用在后台 时调起
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    completionHandler(UIBackgroundFetchResultNewData);

    NSLog(@"我打印推送的通知看看是个什么%@",userInfo);
    
    NSMutableDictionary *dicTionary = [NSMutableDictionary dictionaryWithDictionary:userInfo];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isCloudNoti"];
           [[NSUserDefaults standardUserDefaults]setObject:dicTionary forKey:@"msgUserInfo"];
        [[NSUserDefaults standardUserDefaults]synchronize];

    
    if (application.applicationState == UIApplicationStateActive) {
        NSLog(@"通知在前台acitve ");
                if (MSG_USERINFO) {
                    
                   
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_ADEND_ENTER_APPLICATION" object:MSG_ISFOREGROUND];
                }
        }
    
    if (application.applicationState == UIApplicationStateInactive) {

        badgeRemoteCount -=1;
//        application.applicationIconBadgeNumber = badgeRemoteCount;
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeRemoteCount];

        if ( [[NSUserDefaults standardUserDefaults]boolForKey:@"enterBG"]== YES) {
            
            if (MSG_USERINFO) {
             

                    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
                if(IS_MERGEPORJECTLIST){
                    
                    PortalManger *portalManager = [PortalManger sharedPortalManager];
                    
                    [portalManager retryRequestProjectListWith:kAPPNetEnvType_ALL];
                    
                    portalManager.callAppAlert = ^(NSString *status) {
                        
                        [self projectListStatus:status];
                        
                    };
                    
                }else
                {
                    
                    [request getHightongPORTALProjectListWithTimeout:5];
                    
                }
//#warning 不知道加了会不会有问题先加上吧，已经测试过，加过显示广告的Label
                //引导图 或者是广图
                [self imageAnimationAction];//进入首页之前
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_ADEND_ENTER_APPLICATION" object:MSG_ISBACKGROUND];
                    });
                    
                
               
            }
        }
        
        
    }
    
    if (application.applicationState == UIApplicationStateBackground) {
        NSLog(@"我走了后台");
        badgeRemoteCount +=1;
//        application.applicationIconBadgeNumber = badgeRemoteCount;
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeRemoteCount];
    }
    
    

}


// 在 iOS8 系统中，还需要添加这个方法。通过新的 API 注册推送服务
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    
    [application registerForRemoteNotifications];
    
    
}





- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"test:%@",deviceToken);
    [[BPushSingleTon shareInstance]registerDeviceToken:deviceToken];
    
    
}

// 当 DeviceToken 获取失败时，系统会回调此方法
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"DeviceToken 获取失败，原因：%@",error);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // App 收到推送的通知
    [[BPushSingleTon shareInstance]handleNotification:userInfo];
    NSLog(@"********** ios7.0之前 **********");
    NSMutableDictionary *dicTionary = [NSMutableDictionary dictionaryWithDictionary:userInfo];

    
        [[NSUserDefaults standardUserDefaults]setObject:dicTionary forKey:@"msgUserInfo"];
        [[NSUserDefaults standardUserDefaults]synchronize];

      
    if (application.applicationState == UIApplicationStateActive) {
        NSLog(@"通知在前台acitve ");
        
        if (MSG_USERINFO) {
            
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MSG_ADEND_ENTER_APPLICATION" object:MSG_ISFOREGROUND];

        }
    }
    
   }


//云推送通知结束end











-(void)removeImageView{

    [_imagev removeFromSuperview];

}

-(void)startWorking{
    NSLog(@"现在可以进入app了~");
   
    HTRequest *cnrequest = [[HTRequest alloc] initWithDelegate:self];
    [cnrequest CNOperatorOperatorInfo];

    
    //...百度定位
    [BMKSingleTon shareInstance];
    
    [self removeAllLocalNotification];//应用 开始前 清除所有本地推送。
    [KLocalNotificationManager shareLocalNotificationManager];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAllLocalNotification) name:@"removeAllLocalNotification" object:nil];
#if DEBUG
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(testLocalNoti:) name:@"testLocalNoti" object:nil];
#else
    
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRegionCode) name:@"GET_LOCATION_SUCCESS" object:nil];//定位成功后 会接收到这个通知
    
 
    if (USER_ACTION_COLLECTION) {
        if (!baseInfo) {
            baseInfo = [[GETBaseInfo alloc] init];
            NSLog(@"手机型号 ->>%@",[baseInfo iphoneType]);
            NSLog(@"UUID ->>%@",[baseInfo getUUID]);
        }
        [baseInfo setupLocationManager];
        
    }
    
    

    [HTYJUserCenter userCenter];
    
    //新的用户信息采集     //开机launchStart
    NSLog(@"开机登录成功的信息采集");
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"SchemeAction"] == NO) {
        [[HT_UsercollectionService shareUserCollectionservice] postBaseInfoData:kLaunchStart];
        [[HT_UsercollectionService shareUserCollectionservice] postNetInfoData];
        [[HT_UsercollectionService shareUserCollectionservice] postUserActionData];
        [[HT_UsercollectionService shareUserCollectionservice] postErrorInfoData];
    }

    //友盟统计--------------------
    if (MOB_COUNT) {
        
//        [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    
        [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    
//        [MobClick startWithAppkey:UM_SHARE_APP_KEY reportPolicy:BATCH channelId:nil];//REALTIME只在“集成测试”设备的DEBUG模式下有效，其它情况下的REALTIME会改为使用BATCH策略。
        UMConfigInstance.appKey = UM_SHARE_APP_KEY;
        [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！


    }
#pragma ----------------ww友盟页面统计-----
    [[[UIViewController alloc] init] UMengViewMessageCollect];
    //统计--------------------

    
    //友盟分享---------------------
    [UMSocialGlobal shareInstance].isClearCacheWhenGetUserInfo = NO;
    [[UMSocialManager defaultManager] setUmSocialAppkey:UM_SHARE_APP_KEY];//设置友盟appkey
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WECHAT_SHARE_APP_ID appSecret:WECHAT_SHARE_APP_SECRET redirectURL:@"http://mobile.umeng.com/social"];/* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1105228389"appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];//设置QQ平台的appID
    //分享------------------------------
    
    //zzsc iOS8以后需要在这里注册一下本地通知
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    //引导图 或者是广图
    [self imageAnimationAction];//进入首页之前
    
    HTRequest *adrequest = [[HTRequest alloc] initWithDelegate:self];
    
    if (!is_Separate_API || !HomeNewDisplayTypeOn) {
//        [adrequest EPGChannelWithType:-1 andLookbackFlag:3 andTimeout:5];
        [adrequest EPGGetLivePageChannelInfoAndisType:-1];
    }
    
    
    if (IS_SUPPORT_MULTI_SCREEN_INERACT) {
        //多屏互动  socket 初始化
        [self initTheSocket];//发送视频消息
        [self setupSendSocket];//发送 接收IP信息
        //多屏互动相关的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendIPMessage) name:@"SENDLOCALLIP" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendTVInfo:) name:@"SENDINFOTOBOX" object:nil];

    }
    
    
//    [SAIInformationManager ONLine];
//    NSMutableDictionary *diction = [[NSMutableDictionary alloc]init];
//    [diction setValue:@"resign" forKey:@"poststr"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"COLLECTION_POST_USER_ACTION_DATA" object:diction];//发送通知要上传数据

    
    //...ww 默认为每次非WiFi播放视频都会提醒
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"NetworkTypeShowStatus"] == nil)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"NetworkTypeShowStatus"];
    }

    //BOBO第一次Alert
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"pointToPointIP"];

     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstAlert"];
    [[NSUserDefaults standardUserDefaults]setInteger:SMOOTH forKey:@"BtnTag"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:4 forKey:@"PlayTVType"];

    [[NSUserDefaults standardUserDefaults] synchronize];//数据同步到系统文件中
    
        if (YES || [APP_ID isEqualToString:@"1099034502"] && is_Separate_API) {
            
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request CNCommonSystemTime];
            
        }else
        {
            
            HTRequest *request = [[HTRequest alloc]initWithDelegate:self];
            [request SystemTimeWithTimeout:KTimeout];
            
        }
    
    
    
}


- (void)HTRequestFinishedWithReturnStatus:(NSInteger)status andResult:(NSDictionary *)result andType:(NSString *)type
{
    NSLog(@"返回字典是===========%@/n返回状态是+++++++++++%ld/n返回类型+++++++++++是%@",result,(long)status,type);
    
    if ([type isEqualToString:CN_COMMON_ADLIST]){
        [self removeImageView];

        NSArray *ADArray = [[NSArray alloc] initWithArray:[result objectForKey:@"adList"]];
        if ([ADArray count]>0) {
            [[NSUserDefaults standardUserDefaults] setObject:ADArray forKey:@"ADArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AD_Noti" object:ADArray];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AD_Noti" object:ADArray];
            [UIApplication sharedApplication].statusBarHidden = NO;

        }
    
    }
    else if ([type isEqualToString:EPG_CHANNELLIST]){
    
    
//        NSArray *listArray = [[NSArray alloc] initWithArray:[result objectForKey:@"servicelist"]];
        NSArray *listArray = [[NSArray alloc] initWithArray:[result objectForKey:@"channelList"]];

        [[NSUserDefaults standardUserDefaults] setObject:listArray forKey:EPG_CHANNELLIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (listArray.count<=0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //这里是5s后 你要做的事情
                HTRequest *getChannelList = [[HTRequest alloc] initWithDelegate:self];
                [getChannelList EPGChannelWithType:-1 andLookbackFlag:-1 andTimeout:5];
            });

        }
    }
    else if ([type isEqualToString:EPG_CHANNELALLLIST])
    {
        NSArray *listArray = [[NSArray alloc] initWithArray:[result objectForKey:@"serviceList"]];
        
        [[NSUserDefaults standardUserDefaults] setObject:listArray forKey:EPG_CHANNELALLLIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (listArray.count<=0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //这里是5s后 你要做的事情
                HTRequest *getChannelList = [[HTRequest alloc] initWithDelegate:self];
                [getChannelList EPGChannelAllListWithLookbackFlag:-1 andTimeout:5];
            });
            
        }
    }
    
    if ([type isEqualToString:@"projectVersion"]) {
        
        if (status!=0 ||([[result objectForKey:@"ret"] integerValue]!=0  && [result objectForKey:@"ret"])) {
           // CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"请连接广电网络！" andButtonNum:2 withDelegate:self];
            
            
            CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"网络开小差了，请稍后再试" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确定" withDelegate:self];
            

            cusAlert.tag = 2;
            [cusAlert show];
            return;
        }
        
        NSString *getVersion = [result objectForKey:@"ver"];

        NSString *versionStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"version"];
        
        if (([getVersion integerValue] != [versionStr integerValue]) || !versionStr) {
            HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
            if(IS_MERGEPORJECTLIST){
                
                PortalManger *portalManager = [PortalManger sharedPortalManager];
                
                [portalManager retryRequestProjectListWith:kAPPNetEnvType_ALL];
                
                portalManager.callAppAlert = ^(NSString *status) {
                    
                    
                    [self projectListStatus:status];
                };
                
            }else
            {
                
                [request getHightongPORTALProjectListWithTimeout:5];
                
            }
            if (getVersion) {
                [[NSUserDefaults standardUserDefaults] setObject:getVersion forKey:@"version"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        } else if ([getVersion integerValue] == [versionStr integerValue]) {
            NSLog(@"暂不需要更新统一PORTAL");
            [self startWorking];
        }
        return;
    }
    
    if ([type isEqualToString:@"projectList"]) {
        NSLog(@"myDate - projectList receive ------>>> :%@",[NSDate date]);

        if (status!=0||[[result objectForKey:@"ret"] integerValue]!=0) {
           // CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"请连接广电网络！" andButtonNum:2 withDelegate:self];
            
            CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"网络开小差了，请稍后再试" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确定" withDelegate:self];
            
            
            cusAlert.tag = 3;
            [cusAlert show];
            return;
        }
        
         BOOL isScheme =  [[NSUserDefaults standardUserDefaults] boolForKey:@"SchemeAction"];
        if (isScheme) {
            _schemeManger = [SchemeManager shardSchemeManager];
            [_schemeManger schemeForSchemeUrl:_schemeURL];
        }
        
        [self startWorking];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SchemeAction"];
    }
    
    if ([type isEqualToString:EPG_FIND_REGION]) {
        
        NSLog(@"EPG_FIND_REGION----%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue]==0) {
            NSDictionary *region = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"region"]];
            NSString *code = [region objectForKey:@"code"];
            NSString *name = [region objectForKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"InstanceCode"];
            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"hospitalName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshHome" object:nil];
            
            
            
            
        }
    }
    if ([type isEqualToString:YJ_INTRODUCE_NEARBY]) {
        
        NSLog(@"EPG_FIND_REGION----%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue]==0) {
            NSDictionary *region = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"info"]];
            NSString *code = [region objectForKey:@"instanceCode"];
            NSString *name = [region objectForKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"InstanceCode"];
            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"hospitalName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshHome" object:nil];
        }
    }
    
    if ([type isEqualToString:CN_OPERATOR_OPERATORINFO]) {
        
        NSLog(@"CN_OPERATOR_OPERATORINFO----%@",result);
        
        if ([[result objectForKey:@"ret"] integerValue]==0 && [result objectForKey:@"ret"]) {
            NSDictionary *operatorInfo = [NSDictionary dictionaryWithDictionary:[result objectForKey:@"operatorInfo"]];
            NSString *operatorLogo = [operatorInfo objectForKey:@"operatorLogo"];
            NSString *operatorName = [operatorInfo objectForKey:@"operatorName"];
            [[NSUserDefaults standardUserDefaults] setObject:operatorLogo forKey:@"operatorLogo"];
            [[NSUserDefaults standardUserDefaults] setObject:operatorName forKey:@"operatorName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        
        }
    }

    
    
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   //
    HTRequest *request = [[HTRequest alloc] initWithDelegate:self];
    if (alertView.tag==2) {
        if (buttonIndex==1) {
            exit(1);
            return;
        }
        [request getHightongPORTALVersionWithTimeout:5];
    }
    if (alertView.tag == 3) {
        if (buttonIndex==1) {
            exit(1);
            return;
        }
        if(IS_MERGEPORJECTLIST){
            
            PortalManger *portalManager = [PortalManger sharedPortalManager];
            
            [portalManager retryRequestProjectListWith:kAPPNetEnvType_ALL];
            
            portalManager.callAppAlert = ^(NSString *status) {
                
                
                [self projectListStatus:status];
            };
            
        }else
        {
            
            [request getHightongPORTALProjectListWithTimeout:5];
            
        }
    }
    if (alertView.tag == 16) {
        
            _CanDisplay = YES;
        
        if (buttonIndex==0 && self.localNotiDic.count>0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCAL_NOTIFICATION_TO_TABVC" object:self.localNotiDic];
 
        }else{
        
            if (buttonIndex==1) {
                
            }else{
                [AlertLabel AlertInfoWithText:@"节目信息不全" andWith:self.window withLocationTag:1];
            }
        }
    }
    
    
    if (alertView.tag==200) {
        
        if (buttonIndex==1) {
            
            [_ugcUploadManager chooseTaskToUpLoadFromPlist];

        }else
        {
            
            NSLog(@"未开始任务取消上传");
            
        }
        
        if (_upLoadWorkAlert) {
            
            [_upLoadWorkAlert resignKeyWindow];
            _upLoadWorkAlert = nil;
            
        }


    }
    
    if (alertView.tag==300) {
        
        if (buttonIndex==1) {
            
            [_ugcUploadManager stopUGCUploadTask];
            
        }else
        {
            
            NSLog(@"进行任务不取消上传");
            
        }
        
        if (_upLoadWorkAlert) {
            
            [_upLoadWorkAlert resignKeyWindow];
            _upLoadWorkAlert = nil;
            
        }

        
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_WILL_ENTER_BACKGROUND" object:nil];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    //后台不发送弹图斯的通知
//    [NetWorkNotification_shared shardNetworkNotification].canShowToast = NO;

    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"enterBG"];
    [[HT_UsercollectionService shareUserCollectionservice] changeUser:NO];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    //进入前台 发通知
//    [NetWorkNotification_shared shardNetworkNotification].canShowToast = YES;

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"SchemeAction"];

    });
    
    // 从后台进入前台时, 继续播放视频
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PauseVideo" object:nil];
        

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    if (baseInfo) {
        //[baseInfo setupLocationManager];
        
        [[BMKSingleTon shareInstance] restartBMKLocation];
    }

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{//为确保获取projectList成功，此处配合延时5秒钟
        
        if (!_isLaunch) {
            
            //从后台进入前台做一次登录，确保token的有效性
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_AUTHENTIC_TOKEN" object:nil];
        }
        _isLaunch = NO;
    });
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"enterBG"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCloudNoti"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"msgUserInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
#pragma mark - --------
-(void)showNetpoorView{

    [AlertLabel AlertInfoWithText:@"网络状况不好，请重试!" andWith:self.window withLocationTag:0];
}

-(void)projectListStatus:(NSString *)status
{
    
    if ([status isEqualToString:@"0"]) {
        
        CustomAlertView *cusAlert = [[CustomAlertView alloc] initWithMessage:@"网络开小差了，请稍后再试" andButtonNum:2 andCancelButtonMessage:@"取消" andOtherButtonMessage:@"确定" withDelegate:self];
        
        
        cusAlert.tag = 3;
        [cusAlert show];
        return;
    }
    
    BOOL isScheme =  [[NSUserDefaults standardUserDefaults] boolForKey:@"SchemeAction"];
    if (isScheme) {
        _schemeManger = [SchemeManager shardSchemeManager];
        [_schemeManger schemeForSchemeUrl:_schemeURL];
    }
    
    [self startWorking];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SchemeAction"];
    
}


-(void)imageAnimationAction{

    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        NSLog(@"第一次启动");
 
//        [adrequest ADListWith:3];
    }else{
        NSLog(@"不是第一次启动");
        HTRequest *adrequest = [[HTRequest alloc] initWithDelegate:self];
        
        if (IS_SUPPORT_ADList) {
            [adrequest CNCommonAdList];
        }else{
        
            [adrequest ADInfoWithADCode:PANGEA_PHONE_BOOT_IMAGE andType:@"0" andAssetID:@"" andAssetName:@"" andGroup:AD_GroupID andTimeout:3 andReturnBlcok:^(NSInteger status, NSDictionary *result, NSString *type) {
                if (status!=0) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AD_Noti" object:[NSArray new]];
                    [UIApplication sharedApplication].statusBarHidden = NO;

                    return ;
                }
                NSLog(@"新版本广告页数据：%@",result);
                NSArray *ADArray = [[NSArray alloc] initWithArray:[result objectForKey:@"adList"]];
                if ([ADArray count]>0) {
                    [[NSUserDefaults standardUserDefaults] setObject:ADArray forKey:@"ADArray"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AD_Noti" object:ADArray];
                }else{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AD_Noti" object:ADArray];
                    [UIApplication sharedApplication].statusBarHidden = NO;
                }
                
                
            }];

        }
        
    }
    
}

-(void)getRegionCode{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_REGIONCODE_WITH_LOACTIONINFO" object:nil];//告诉医院选择界面 定位成功
    
    HTRequest *hrequest = [[HTRequest alloc] initWithDelegate:self];
    if (!HomeNewDisplayTypeOn) {
        return;
    }
    if (is_Separate_API) {
        NSString *longitudeAndLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
        NSString *longitude = @"0.000000";//@"114.989792";//0.000000
        NSString *latitude  = @"0.000000";//@"27.116745";//0.000000
        if ([longitudeAndLatitude rangeOfString:@","].location != NSNotFound) {
            longitude = [[longitudeAndLatitude componentsSeparatedByString:@","] lastObject];
            latitude  = [[longitudeAndLatitude componentsSeparatedByString:@","] firstObject];
        }

        [hrequest YJIntroduceNearbyWithLongitude:longitude withLatitude:latitude];
    }else{
        [hrequest EPGFindRegionWithTimeout:10];//

    }
    
}

#pragma mark - socket&UDP 相关
//...zzs
//初始化socket 并且 绑定端口
-(void)initTheSocket{
    if (!_TVSocket) {
        _TVSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
        if (![_TVSocket bindToPort:6060 error:nil]) {
            NSLog(@"TVInfo客户端 6060 端口 绑定失败!");
            return;
        }else{
            NSLog(@"TVInfo客户端  6060 端口 绑定成功!");
            
        }
        
    }
    
    //    [_TVSocket enableBroadcast:YES error:nil];//这个是 可发送广播
    [_TVSocket receiveWithTimeout:-1 tag:99];//实时接收 socket信息
}

-(void)setupSendSocket{
    if (!_sendSocket) {
        
        _sendSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
        if (![_sendSocket bindToPort:5001 error:nil]) {
            NSLog(@"_sendSocket 客户端  5001 端口 绑定失败!");
            return;
        }else{
            NSLog(@"_sendSocket 客户端  5001 端口 绑定成功!");
        }
        NSError *error;
        
        [_sendSocket enableBroadcast:YES error:&error];//可以发送广播
        NSLog(@"  是否成功广播 %d",[_sendSocket enableBroadcast:YES error:&error]);
        
//        [_sendSocket joinMulticastGroup:@"239.254.254.254"error:nil];//加入组播  发送组播的时候 可以不用这句话
        [self setupReceiveSocket];//初始化接收信息的 服务端
        
    }
    //无论初始化还是没初始化 都需要发送本机IP地址
    [self sendIPMessage];
    
}

-(void)setupReceiveSocket{
    //接收 盒子IP地址的 socket
    if (!_receiveSocket) {
        _receiveSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
        if (![_receiveSocket bindToPort:5002 error:nil]) {
            NSLog(@"_receiveSocket 服务端 5002 端口 绑定失败!");
            return;
        }else{
            NSLog(@"_receiveSocket 服务端 5002 端口绑定成功!");
        }
        
        [_receiveSocket joinMulticastGroup:@"239.254.254.254" error:nil];//加入组播
        [_receiveSocket receiveWithTimeout:-1 tag:101];//实时接收
 
    }
}


//将从通知传递过来的参数 发送给指定IP的终端
-(void)sendTVInfo:(NSNotification *)notification
{
    NSDictionary *TVInfoDic = [[NSDictionary alloc] initWithDictionary:[notification userInfo]];
    _message = [TVInfoDic JSONRepresentation];//数据json化
    NSString *IP = [[NSUserDefaults standardUserDefaults] objectForKey:@"pointToPointIP"];
    
    NSLog(@"socket发送----%@",_message);
    NSData *data = [_message dataUsingEncoding:NSUTF8StringEncoding];
    if (IP.length>5) {
        [_TVSocket sendData:data toHost:IP port:6060 withTimeout:-1 tag:100];
    }else{
        NSLog(@"IP 地址为空,暂不发送消息 ");
    }
    [_TVSocket enableBroadcast:YES error:nil];
    [_TVSocket receiveWithTimeout:-1 tag:100];
}
-(void)sendIPMessage{

    //清理——IPArray
    [_IPArray removeAllObjects];
    
    if (!_sendSocket) {
        return;
    }
    if (!baseInfo) {
        baseInfo = [[GETBaseInfo alloc] init];
    }
    NSString *IPNumber = [baseInfo getIPAddressType];
    
    //只要知道服务器的地址和端口号，就能发送消息
    _message = [NSString stringWithFormat:@"{\"@command\":\"DLNAPLAY_DISCOVER\",\"param\":{\"IPAddress\":\"%@\"}}",IPNumber];
    
    NSString *message = [NSString stringWithFormat:@"%@",_message];
    NSData *msgData = [message dataUsingEncoding:NSUTF8StringEncoding];//数据转化成 二进制
    [_sendSocket sendData:msgData toHost:@"239.254.254.254" port:5001 withTimeout:-1 tag:0];//广播发送数据
    NSLog(@"发送的本机IP信息 %@",_message);
    
}


#pragma  mark - socket delegate
//接收到消息 触发此代理方法
-(BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port{
    
    
    NSDictionary *msgDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    
    NSString *commandStr = [msgDic objectForKey:@"@command"];
    NSLog(@"接收到了 socket 消息----%@",msgDic);
//    [AlertLabel AlertSSIDInfoWithText:commandStr andWith:self.window withLocationTag:0];
//    [AlertLabel AlertInfoWithText:commandStr andWith:self.window withLocationTag:0];
    if ([commandStr isEqualToString:@"DLNAPLAY_VOD"]) {
        
        NSLog(@"接收到TVsocket  点播消息 ----%@",msgDic);
        [[NSNotificationCenter  defaultCenter] postNotificationName:@"RECEVEPULLTVINFO" object:self userInfo:msgDic];
    }
    if ([commandStr isEqualToString:@"DLNAPLAY_DISCOVER"]) {
        
        NSDictionary *IPDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RECEIVE_BOXIP" object:IPDic];
        NSLog(@"接收到receiveSocket  ----%@",IPDic);
        
    }
    if ([commandStr isEqualToString:@"DLNAPLAY_RESPONSE"]) {
        NSDictionary *IPDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RECEIVE_BOXIP" object:IPDic];
        NSLog(@"接收到receiveSocket 盒子发送过来的IP ----%@",IPDic);
//        [AlertLabel AlertInfoWithText:[IPDic JSONRepresentation] andWith:self.window withLocationTag:0];

    }
    if ([commandStr isEqualToString:@"DLNAPLAY_LIVE"]) {
        NSLog(@"接收到TVsocket  直播的拉屏信息 ----%@",msgDic);
        [[NSNotificationCenter  defaultCenter] postNotificationName:@"RECEVEPULLTVINFO" object:self userInfo:msgDic];
        
    }
    if ([commandStr isEqualToString:@"DLNAPLAY_EVENT"]) {
        NSLog(@"接收到TVsocket  回看的拉屏信息  ----%@",msgDic);
        [[NSNotificationCenter  defaultCenter] postNotificationName:@"RECEVEPULLTVINFO" object:self userInfo:msgDic];
    }
    if ([commandStr isEqualToString:@"DLNAPLAY_PAUSE"]) {
        
        [[NSNotificationCenter  defaultCenter] postNotificationName:@"RECEVEPULLTVINFO" object:self userInfo:msgDic];

        NSLog(@"接收到receiveSocket  ----%@",msgDic);
        
    }

    //继续监听
    [sock receiveWithTimeout:-1 tag:100];
    return YES;
    
}

//关闭 socket
-(void)onUdpSocketDidClose:(AsyncUdpSocket *)sock{
    
    
}

//发送了数据 触发
-(void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
   
    NSLog(@"socket 发送消息成功~");
    
}

//发送失败触发此方法 发送失败了就走这个？？？
-(void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    NSLog(@"发送失败？");

    
}

//接受失败  UDP接受失败能走这个方法？？？原理上够呛吧。。。
-(void)onUdpSocket:(AsyncUdpSocket *)sock didNotReceiveDataWithTag:(long)tag dueToError:(NSError *)error{
    NSLog(@"接收失败？");
    
}

#pragma mark - 本地通知相关
-(void)localNotificationAction:(NSNotification *) noti{
    NSLog(@"这个位置的本地通知 暂时不用了 一定要注意");
    return;
    [self removeAllLocalNotification];//注册本地推送前 删除掉之前所有通知 因为有可能是删除预约后 导致 预约列表更新
    NSArray *orderListArr = [[NSArray alloc] initWithArray:[noti object]];
    showNotiNum=0;
    self.localNotiArr = [[NSMutableArray alloc] init];//localNoti
    
    for (int i=0; i<orderListArr.count; i++)
    {
        //显示通知
        NSDictionary *schedulDic = [[NSDictionary alloc] initWithDictionary:[orderListArr objectAtIndex:i]];
        NSDate *nowDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *showDate = [dateFormatter dateFromString:[schedulDic objectForKey:@"startTime"]];
        
        if ([showDate timeIntervalSinceDate:nowDate]>0) {
            
            [self.localNotiArr addObject:schedulDic];//localNoti
            [self showLocalNotificationWithDictionary:schedulDic];
            
        }
    }
    
}
//zzs预定 提醒
-(void)showLocalNotificationWithDictionary:(NSDictionary *)dic{
    //发送通知
    NSLog(@"发送预定消息");
    
    //-------
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];//已经添加本地推送的数组
    if (acount>0)
    {
        // 遍历找到对应nfkey和notificationtag的通知
        for (int i=0; i<acount; i++)
        {
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];
            NSDictionary *userInfo = myUILocalNotification.userInfo;
            NSString *nfkey = [userInfo objectForKey:@"nfkey"];
            
            if ([nfkey isEqualToString:[dic objectForKey:@"eventName"]])
            {
                //不注册该通知
                return;
            }
        }
    }
    
    showNotiNum++;
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    if (notification!=nil) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *showDate = [dateFormatter dateFromString:[dic objectForKey:@"startTime"]];
        //        NSDate *showDate = [dateFormatter dateFromString:@""];
        //        NSDate *showDate = [[NSDate date] dateByAddingTimeInterval:20];
        
        notification.fireDate=showDate;//设置显示时间戳
        notification.repeatInterval=0;//循环次数，kCFCalendarUnitWeekday一周一次
        notification.timeZone=[NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];//[NSTimeZone defaultTimeZone]
        //BO注释本地角标
//        notification.applicationIconBadgeNumber=showNotiNum; //应用的红色数字
        notification.soundName= UILocalNotificationDefaultSoundName;//声音，可以换成alarm.soundName = @"myMusic.caf"
        //去掉下面2行就不会弹出提示框
        notification.alertBody=[NSString stringWithFormat:@"您预订的节目：%@就要播出了，届时请观看！",[dic objectForKey:@"eventName"]];//提示信息 弹出提示框
        notification.userInfo = dic;
        
        notification.alertAction = @"马上观看";  //提示框按钮
        //notification.hasAction = NO; //是否显示额外的按钮，为no时alertAction消失
        // NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
        //notification.userInfo = infoDict; //添加额外的信息
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        NSLog(@"%@节目预定 本地通知注册成功  这是第%ld个通知   显示时间：%@",[dic objectForKey:@"eventName"],(long)showNotiNum,showDate);
        
    }
    
}
-(void)removeAllLocalNotification{
    //-------
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];//已经添加本地推送的数组
    if (acount>0)
    {
        // 遍历找到对应nfkey和notificationtag的通知
        for (int i=0; i<acount; i++)
        {
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];

            [[UIApplication sharedApplication] cancelLocalNotification:myUILocalNotification];
           
        }
    }
    showNotiNum=0;
    //BO注释本地角标

//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;


}
-(void)testLocalNoti:(NSNotification *)noti{
    self.localNotiDic = [NSMutableDictionary dictionaryWithDictionary:noti.object];
    if (_CanDisplay) {
        _CanDisplay = NO;
        CustomAlertView *localNoti = [[CustomAlertView alloc] initWithMoreMessage:@"通知来了" andButtonNum:2 withDelegate:self];
        localNoti.tag = 16;
        [localNoti show];
        
    }
    
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{

    
    self.localNotiDic = [[NSMutableDictionary alloc] initWithDictionary:notification.userInfo];
    
    if (_CanDisplay) {
        _CanDisplay = NO;
        CustomAlertView *localNoti = [[CustomAlertView alloc] initWithMoreMessage:notification.alertBody andButtonNum:2 withDelegate:self];
        
        localNoti.tag = 16;
        [localNoti show];
 
    }
    
    //这里，你就可以通过notification的useinfo，干一些你想做的事情了
    //BO注释本地角标
//    application.applicationIconBadgeNumber -= 1;
//    application.applicationIconBadgeNumber = 0;
    //BOend
    [application cancelLocalNotification:notification];//取消该本地通知
}




@end
