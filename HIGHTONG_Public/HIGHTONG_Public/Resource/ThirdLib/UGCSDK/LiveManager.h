//
//  LiveManager.h
//  KLive
//
//  Created by Kael on 2017/3/3.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CameraManage.h"
#import "AFNetworkReachabilityManager.h"
#include "mv3MediaPublisher_api.h"


//------------------
#define kTag_OpenPreview       0x500
#define kTag_ClosePreview      0x501
#define kTag_Torch             0x502
#define kTag_Toggle            0x503
#define kTag_Preset            0x504
#define kTag_StartCamera       0x505
#define kTag_CloseCamera       0x506
#define kTag_Recorder          0x507
#define kTag_Stop              0x508
#define kTag_textField         0x509
#define kTag_showError         0x510
#define kTag_addwartermark     0x511
#define kTag_removewartermark  0x512
//------------------

//-------------------
typedef enum {
    kCameraType_selectPreset = 0,
    kCameraType_startCamera,
    kCameraType_startCameraFailed,
    kCameraType_closeCamera,
    kCameraType_recorder,
    kCameraType_stop
}CameraType;
//------------------



@interface LiveManager : NSObject<UIGestureRecognizerDelegate>
{
    CGFloat     beginGestureScale;
    CGFloat     frameScale;
    
//----------------------------------
    NSString            *m_preset;
    
    void                *m_pMediaPublisher;
    BOOL                m_videoPermissionsState;
    BOOL                m_audioPermissionsState;
    
    AFNetworkReachabilityStatus _status;
}

@property (nonatomic,copy) NSString *streamURL;


@end
