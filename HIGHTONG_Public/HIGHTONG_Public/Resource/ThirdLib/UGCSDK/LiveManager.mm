//
//  LiveManager.m
//  KLive
//
//  Created by Kael on 2017/3/3.
//  Copyright © 2017年 创维海通. All rights reserved.
//

#import "LiveManager.h"
#import "AFNetworkReachabilityManager.h"
//#import "UIAlertController+Blocks.h"

static LiveManager *manager = nil;

@implementation LiveManager

+(LiveManager *)shareLiveManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LiveManager alloc] init];
    });
    return manager;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initBaseData];
        [self initBaseOBJ];
    }
    
    return self;
}

-(void)initBaseData{
    frameScale = 1.0;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [self startReachability];
}

-(void)initBaseOBJ{

}

//---
// 接收录制返回的状态值
#pragma mark - callback
-(void)HT_RreceivePublishStateCallBackWith:(id)pInstance andResult:(NSInteger)result andCode:(NSInteger)statusCode andObject:(NSObject *)OC_OBJ{
    
    receivePublishStateCallBack((__bridge void *)(pInstance), result, statusCode, (__bridge void *)(OC_OBJ));
    
}

static void receivePublishStateCallBack(void *pInstance, int res, int code, void *pObj)
{
    NSLog(@"receivePublishStateCallBack res:%d  code:%d", res, code);
   
    LiveManager *livemanager = (__bridge LiveManager *)pObj;

    switch (res) {
            // 提示用户当前网络抖动，app层可以给用户一些友好提示网络原因导致的推流缓慢
            // 建议：只做提示
            case MPUBLISH_WARN_DELAY:
        {
//当前网络抖动
           dispatch_async(dispatch_get_main_queue(), ^{
              ZSToast(@"推流延迟，请检查网络")
           });
        }
            break;
            
            // 提示用户当前已经连接到服务器端，并开始推流。app层可以更新ui状态
            // 建议：只做提示
            case MPUBLISH_CONNECTED:
        {

    //已连接到服务器 准备推流
        }
            break;
            
            // 提示用户当前已经开始自动重连，这会app可以让用户感知到当前正在尝试恢复网络
            // 建议：只做提示
            case MPUBLISH_ERR_AUTOCONNECTING:
        {
            // 后台已经启动重连网络操作
            ZSToast(@"正在恢复网络...")
            
        }
            break;
            
            // 这个case包含首次推流打开socket就失败和重连后的打开socket失败。
            // 建议：间隔1秒，重连3次，如果还失败直接MediaStop（有需要可以接着重新推流尝试）
            case MPUBLISH_ERR_CONNECTERROR:
        {
            printf("connect failed, stop recorder\r\n");

            //连接失败，应该停止推流 然后
            ZSToast(@"正在重连...")
            [livemanager MediaStop];
            [livemanager startPushMediaStreamWith:livemanager.streamURL];


        }
            break;
            
            // 这个case是音视频编码产生失败，恢复只能MediaStop后重新推流才可以
            // 建议：停止推流，重新推
            case MPUBLISH_ERR_ENCODEAUDIO:
            case MPUBLISH_ERR_ENCODEVIDEO:
        {
            //stop 后重新推流
            [livemanager MediaStop];
            [livemanager startPushMediaStreamWith:livemanager.streamURL];
        }
            break;
            
            // rtmp的socket断开或者不稳定，音视频编码失败
            // 推流过程返回的错误，主要是发送数据失败
            // 建议：友好提示和调用强制重连功能MediaPublisher_ForceReConnect(tempControl->m_pMediaPublisher);   （请记得是主线程调用）
            case MPUBLISH_ERR_SENDDATA:
            case MPUBLISH_ERR_STREAMING_N_TIMEOUT:
            case MPUBLISH_ERR_STREAMING_N_CONNFAIL:
            case MPUBLISH_ERR_STREAMING_N_RECVTIMEOUT:
            case MPUBLISH_ERR_STREAMING_N_RECVFAIL:
            case MPUBLISH_ERR_STREAMING_N_SENDTIMEOUT:
            case MPUBLISH_ERR_STREAMING_N_SENDFAIL:
        {
            printf("MPUBLISH_ERR_SENDDATA force re connect\r\n");
            
            //推流失败 强制重连
            MediaPublisher_ForceReConnect( livemanager->m_pMediaPublisher);
        }
            break;
            
        default:
        {
            //未知错误，强制停止推流之后 重新推流
            [livemanager MediaStop];
            [livemanager startPushMediaStreamWith:livemanager.streamURL];
        }
            break;
    }
}

#pragma mark - 检测APP进入前后台
// 切换回前台后，尝试激活重新推流
- (void) appWillEnterForeground
{
    MediaPublisher_Resume(m_pMediaPublisher);
}

// 切换进后台后，暂停录制和推流
- (void) appWillEnterBackground
{
    MediaPublisher_Pause(m_pMediaPublisher);
}

// 超出申请时间限制后，停止和释放掉推流和播放资源
- (void) cameraRecorderWillStop
{
    NSLog(@"cameraRecorderWillStop");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self MediaStop];
    });
}



#pragma mark - 启动摄像头
// 启动系统摄像头（包含授权摄像头和麦克风）
- (void) startCamera
{
    if (!m_preset) {
//        [self showHudWithLabel:@"你需要先选择分辨率!"];
        return;
    }
    
    //----------摄像头授权
    int res = [CameraManage openCamera:@{@"FrameRate": @"30", @"sessionPreset":m_preset} callbackBlock:^(bool authorize) {
        m_videoPermissionsState = authorize;
        
        // 授权摄像头
        if (!authorize) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //这里需要用户给摄像头授权
            });
        }
    }];
    
    //----------麦克风授权
    [CameraManage getRecordPermissionForAudio:^(bool authorize) {
        m_audioPermissionsState = authorize;
        
        // 授权麦克风
        if (!authorize) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //用户需要授权给麦克风哦
            });
        }
    }];
}

#pragma mark - 关闭摄像头
// 关闭系统摄像头
- (void) clickCloseCamera
{
    [CameraManage closePreview];
    [CameraManage closeCamera];

}

#pragma mark - 打开摄像头预览功能
// 打开摄像头预览
- (void) openPreviewWith:(UIView *)cameraView
{
    [CameraManage openPreview:cameraView];
}
#pragma mark - 关闭摄像头预览功能
// 关闭摄像头预览
- (void) closePreview
{
    [CameraManage closePreview];
}
#pragma mark - 开启&关闭 闪光灯
//开关闪光灯
-(void)switchTorchWithStatus:(BOOL)isClose andResultBlcok: (void (^)(BOOL isOpened)) resultBlock{

    BOOL ret = NO;
    ret = [CameraManage setTorchState:isClose];
    resultBlock(ret);
    
}

#pragma mark - 切换前后置摄像头
-(void)switchToggleWithStatus:(BOOL)isFront andResultBlcok: (void (^)(BOOL isOpened)) resultBlock{
    BOOL ret = NO;
    if (isFront) {
        ret = [CameraManage setCameraState:CameraPosition_front];
        if (ret) {
            MediaPublisher_ResetContent(m_pMediaPublisher);
        }else{
        //提示前置摄像头打不开
        }
        
    }else{
        ret = [CameraManage setCameraState:CameraPosition_back];
        if (ret) {
            MediaPublisher_ResetContent(m_pMediaPublisher);
        }else{
            //提示后置摄像头打不开
        }
    }

}
#pragma mark - 手动聚焦 需要一个聚焦点
// 手动聚焦
- (void) singleTapOnView:(UIView *)superView andPoint:(CGPoint)tapPoint
{
    
    [CameraManage autoFocusAtPoint:tapPoint];
}
#pragma mark - 捏合手势缩放
// 手动缩放
- (void) handlePinch:(UIPinchGestureRecognizer*) recognizer
{
    // min
    if (beginGestureScale * recognizer.scale < 1)
    return;
    
    // max
    if (beginGestureScale * recognizer.scale > 10) {
        return;
    }
    
    frameScale = beginGestureScale * recognizer.scale;
    NSLog(@"handlePinch frameScale %f", frameScale);
    
    [CameraManage pichZoomWithScale:frameScale];
}
//手势的代理方法
- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *) gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
        beginGestureScale = frameScale;
    }
    
    return YES;
}

#pragma mark - 添加&移除 水印
// 添加水印
- (void) addwartermark
{
    [CameraManage addWatermarkWithImageName:@"logo.png" info:CGRectMake(150, 100, 120, 120)];
}

// 移除水印
- (void) removewartermark
{
    [CameraManage addWatermarkWithImageName:@"" info:CGRectZero];
}

#pragma mark - 开始推流
// 开始推流录制
// 建议：初始化和释放都应该放在主线程来处理.
-(void)startPushMediaStreamWith:(NSString *)streamIP{
    
    _streamURL = streamIP;
    
    int res = 0;
    int width = 0;
    int height = 0;
    
    //首先验证推流地址是否正确
    if ([streamIP length] == 0 || (![streamIP hasPrefix:@"rtmp"] && ![streamIP hasPrefix:@"http"])) {

        //推流地址有误 请查证后再次尝试
        return;
    }
    //接着验证摄像头和麦克风的权限
    if (!m_videoPermissionsState || !m_audioPermissionsState) {

        //摄像头和麦克风权限失败
        return;
    }
    
    // check network
    if (_status == AFNetworkReachabilityStatusUnknown || _status == AFNetworkReachabilityStatusNotReachable) {

        //提示检查网络
        return;
    }
    
    //流量
    if (_status == AFNetworkReachabilityStatusReachableViaWWAN) {
        //流量
//                int res = [self startPushMediaStreamWith:@""];
//                
//                if (res) {
//                    
//                }else {
//                   
//                    [self MediaStop];
//                }
        
    }else {
//        int res = [self startMediaRecorder];
//        
//        if (res) {
//            
//        }else {
//            [self MediaStop];
//        }
    }
    
}

#pragma mark - 检测是否推流成功
-(BOOL)checkStartMediaRecorder{
    int res = 0;
    int width = 0;
    int height = 0;
    
    if (m_preset) {
        CGSize size = [CameraManage updateSizeWithPreset:m_preset];
        width = size.width;
        height = size.height;
    }
    
    char* cfgFilePath = "/data/local/tmp/ArcPlugin.ini";
    if (NULL == m_pMediaPublisher)
    {
        res = MediaPublisher_CreateInstance(&m_pMediaPublisher,cfgFilePath);
    }
    else
    {
        NSLog(@"ReClick Recorder");
        return false;
    }
    if (res != 0) {
//        [self showHudWithLabel:@"init recorder failed! please try again"];
        return false;
    }
    
    // 排查rtmp的错误时候，打开一下, 发布版本不要打开
    //    MediaPublisher_SetTracePath(m_pMediaPublisher, [filePath UTF8String]);
    CLIPINFO info = {0};
    info.bHasAudio = true;
    info.bHasVideo = true;
    info.lBitrate = 0;
    info.lFileType = 0;
    info.lHeight = height;
    info.lWidth = width;
    
    AUDIOINFO m_audioInfo = {0};
    m_audioInfo.lAudioType = AUDIO_CODEC_TYPE_AAC;//MV2_CODEC_TYPE_G711A;
    m_audioInfo.lChannel = 1;
    m_audioInfo.lBitsPerSample = 16;
    m_audioInfo.lSamplingRate = 44100;
    m_audioInfo.lBitrate = 64000;
    m_audioInfo.lBlockAlign = m_audioInfo.lChannel * (m_audioInfo.lBitsPerSample/8);
    
    VIDEOINFO videoInfo = {0};
    videoInfo.lVideoType = VIDEO_CODEC_TYPE_H264;
    videoInfo.lPicWidth = width;
    videoInfo.lPicHeight = height;
    videoInfo.fFPS = 30;
    videoInfo.lBitrate = width*height*2; // width*height = 流畅  width*height*2 = 标清 width*height*3 or width*height*4 = 高清
    videoInfo.lRotationDegree = 0;
    
    // 设置rtmp参数
    res = MediaPublisher_SetClipInfo(m_pMediaPublisher,&info);
    
    // 设置音频参数
    res = MediaPublisher_SetAudioInfo(m_pMediaPublisher,&m_audioInfo);
    
    if (MEDIA_ERR_INVALID_PARAM == res)
    // 不支持音频采样率或者比特率，建议参考testbed的输入就好
    return false;
    
    int recodTimeSpan = 100;
    res = MediaPublisher_SetConfig(m_pMediaPublisher,CFG_RECORDER_AUDIO_FRAME_TIMESPAN,&recodTimeSpan);
    
    // 设置视频参数
    res = MediaPublisher_SetVideoInfo(m_pMediaPublisher,&videoInfo);
    // 接收录制返回的状态值（很关键）有些需要反馈给app处理ui和适当重录制
    MediaPublisher_RegisterPublishStateCallback(m_pMediaPublisher, receivePublishStateCallBack, (__bridge void*)self);
    
    
    const char *expr = [@"这里是什么啊" UTF8String];
    char *buf = new char[strlen(expr)+1];
    strcpy(buf, expr);
    
    // 启动录制(需要推流地址)
    res = MediaPublisher_Start(m_pMediaPublisher, buf);
    delete buf;
    
    if (res != 0) {
//        [self showHudWithLabel:@"start rtmp failed!"];
        return false;
    }
    
    [CameraManage lockOrientation];
//    [self updateBtnStateWithClick:kCameraType_recorder];
    
    return true;
}

#pragma mark - 停止推流
//停止推流
-(void)MediaStop{
    // 是否推流资源
    if (NULL != m_pMediaPublisher)
    {
        MediaPublisher_Stop(m_pMediaPublisher);
        MediaPublisher_ReleaseInstance(m_pMediaPublisher);
        m_pMediaPublisher = NULL;
    }
    
    // 去掉方向锁定
    [CameraManage unlockOrientation];
    //记得更新UI
}


#pragma mark - 网络监测
- (void) startReachability
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
                
                case AFNetworkReachabilityStatusNotReachable:{
                    
                    NSLog(@"无网络");
                    _status = AFNetworkReachabilityStatusNotReachable;
                    
                    if (m_pMediaPublisher) {
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self showHudWithLabel:@"网络不可用，请检查!"];
                        });
                    }
                    break;
                }
                
                case AFNetworkReachabilityStatusReachableViaWiFi:{
                    
                    NSLog(@"WiFi网络");
                    _status = AFNetworkReachabilityStatusReachableViaWiFi;
                    break;
                }
                
                case AFNetworkReachabilityStatusReachableViaWWAN:{
                    
                    NSLog(@"无线网络");
                    _status = AFNetworkReachabilityStatusReachableViaWWAN;
                    
                    if (m_pMediaPublisher) {
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self realWithNetworkChange];
                        });
                    }
                    break;
                }
                
            default:
                break;
                
        }
        
    }];
}

#pragma mark - 网络变化
- (void) realWithNetworkChange
{
//    [UIAlertController showAlertInViewController:self withTitle:@"提示" message:@"您正在使用的是运营商网络，继续将产生流量费用" cancelButtonTitle:@"继续" destructiveButtonTitle:@"取消" otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex) {
//        if (buttonIndex == 0) {
//            // 播放
//        }else {
////            [self MediaStop];
////            [self updateBtnStateWithClick:kCameraType_stop];
//        }
//    }];
}
@end
