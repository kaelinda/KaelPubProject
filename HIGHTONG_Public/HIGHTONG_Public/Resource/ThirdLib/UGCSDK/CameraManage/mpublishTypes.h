/*----------------------------------------------------------------------------------------------
*
* This file is ArcVideo's property. It contains ArcVideo's trade secret, proprietary and 		
* confidential information. 
* 
* The information and code contained in this file is only for authorized ArcVideo employees 
* to design, create, modify, or review.
* 
* DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
* 
* If you are not an intended recipient of this file, you must not copy, distribute, modify, 
* or take any action in reliance on it. 
* 
* If you have received this file in error, please immediately notify ArcVideo and 
* permanently delete the original and any copy of any file and any printout thereof.
*
*-------------------------------------------------------------------------------------------------*/

/*
 *	mpublishTypes.h
 */

#ifndef __MPUBLISHTYPES__H__
#define __MPUBLISHTYPES__H__


// error
#define MPUBLISH_ERR_BASE               0x9000
#define MPUBLISH_WARN_DELAY             MPUBLISH_ERR_BASE + 1
#define MPUBLISH_ERR_SENDDATA           MPUBLISH_ERR_BASE + 2
#define MPUBLISH_ERR_CONNECTERROR       MPUBLISH_ERR_BASE + 3
#define MPUBLISH_CONNECTED              MPUBLISH_ERR_BASE + 4
#define MPUBLISH_ERR_ENCODEVIDEO        MPUBLISH_ERR_BASE + 5
#define MPUBLISH_ERR_ENCODEAUDIO        MPUBLISH_ERR_BASE + 6
#define MPUBLISH_ERR_AUTOCONNECTING     MPUBLISH_ERR_BASE + 7

#define MPUBLISH_ERR_STREAMING_NETWORK_BASE     (0x0000 + 0x400)
#define MPUBLISH_ERR_STREAMING_N_TIMEOUT		(MPUBLISH_ERR_STREAMING_NETWORK_BASE + 1)//Connection timed out
#define MPUBLISH_ERR_STREAMING_N_CONNFAIL       (MPUBLISH_ERR_STREAMING_NETWORK_BASE + 2)//Connect failed
#define MPUBLISH_ERR_STREAMING_N_RECVTIMEOUT	(MPUBLISH_ERR_STREAMING_NETWORK_BASE + 3)//Read timed out
#define MPUBLISH_ERR_STREAMING_N_RECVFAIL       (MPUBLISH_ERR_STREAMING_NETWORK_BASE + 4)//Read failed
#define MPUBLISH_ERR_STREAMING_N_SENDTIMEOUT    (MPUBLISH_ERR_STREAMING_NETWORK_BASE + 5)//Send timed out
#define MPUBLISH_ERR_STREAMING_N_SENDFAIL       (MPUBLISH_ERR_STREAMING_NETWORK_BASE + 6)//Send failed

#define MEDIA_ERR_INVALID_PARAM                 0x0002


// init error
#define MPUBLISH_ERR_GENERAL_BASE			0x0000
#define MPUBLISH_ERR_UNKOWN					(MPUBLISH_ERR_GENERAL_BASE+1)
#define MPUBLISH_ERR_INVALID_PARAM			(MPUBLISH_ERR_GENERAL_BASE+2)
#define MPUBLISH_ERR_OPERATION_NOT_SUPPORT	(MPUBLISH_ERR_GENERAL_BASE+4)
#define MPUBLISH_ERR_NOT_INIT				(MPUBLISH_ERR_GENERAL_BASE+8)
#define MPUBLISH_ERR_GET_PLUGIN_INPUTSTREAM	(MPUBLISH_ERR_GENERAL_BASE+50+1)
#define MPUBLISH_ERR_MEM_NOT_ENGOUGH		(MPUBLISH_ERR_GENERAL_BASE+3)
#define MPUBLISH_ERR_NOT_READY				(MPUBLISH_ERR_GENERAL_BASE+5)

#define MPUBLISH_ERR_RECORDER_WRONG_STATE			(0x2000+1)
#define MPUBLISH_ERR_AUDIOINPUT_BASE				(0x2000+0x100)                  //base err for audio input
#define MPUBLISH_ERR_AUDIO_INPUT_OPEN				(MPUBLISH_ERR_AUDIOINPUT_BASE+1)
#define MPUBLISH_ERR_AUDIO_INPUT_CLOSE				(MPUBLISH_ERR_AUDIOINPUT_BASE+2)
#define MPUBLISH_ERR_AUDIO_INPUT_STOP				(MPUBLISH_ERR_AUDIOINPUT_BASE+5)
#define MPUBLISH_ERR_AUDIO_INPUT_RECORDING			(MPUBLISH_ERR_AUDIOINPUT_BASE+3)


// set params
#define CFG_RECORDER_VIDEO_BITRATE               0x11000000 + 16
#define CFG_RECORDER_VIDEO_BLACKFRAME            0x03000000 + 24
#define CFG_RECORDER_AUDIO_MUTE                  0x03000000 + 25

#define CFG_RECORDER_LIVECHAT_TYPE               0x90000000 + 1

//support audio codec type
#define AUDIO_CODEC_TYPE_AAC 1633772320
//support video codec type
#define VIDEO_CODEC_TYPE_H264 842413088
//get config type
#define CFG_RECORDER_AUDIO_DECIBEL 0x02000006
//set config type
#define CFG_RECORDER_AUDIO_FRAME_TIMESPAN 0x02000007

// filter
#define ARC_GPUIMG_FILTER_BRIGHTNESS        0x101
#define ARC_GPUIMG_FILTER_SATURATION        0x102
#define ARC_GPUIMG_FILTER_FACEDEFORMATION   0x103
#define ARC_GPUIMG_FILTER_CONTRAST          0x104
#define ARC_GPUIMG_FILTER_REDEFFECT         0x105
#define ARC_GPUIMG_FILTER_COLORLEVELS       0x106
#define ARC_GPUIMG_FILTER_COLORWHITEBALANCE 0x107
#define ARC_GPUIMG_FILTER_EXPOSURE          0x108
#define ARC_GPUIMG_FILTER_SHARPEN           0x109

// license error
#define LICENSE_ERR_DISABLE_SDK_RMTP_NO_RIGHT   		40010	//推流无权限
#define LICENSE_ERR_DISABLE_SDK_MP4_NO_RIGHT    		40011	//录制无权限
#define LICENSE_ERR_DISABLE_LIVECHAT_NO_RIGHT   		40012	//连麦无权限
#define LICENSE_ERR_DISABLE_SDK_EYE_WRSP_NO_RIGHT    	40013	//大眼无权限
#define LICENSE_ERR_DISABLE_SDK_FACE_WRSP_NO_RIGHT    	40014	//瘦脸无权限
#define LICENSE_ERR_DISABLE_SDK_FACE_BEAUTY_NO_RIGHT    40015	//美颜无权限
#define LICENSE_ERR_DISABLE_SDK_FACE_CONTOUR_NO_RIGHT   40016	//脸部识别无权限
#define LICENSE_ERR_DISABLE_SDK_STICKER_NO_RIGHT    	40017	//互动道具无权限
#define LICENSE_ERR_DISABLE_SDK_FACE_BEAUTY_EXPIRED     40020	//美颜失效
#define LICENSE_ERR_DISABLE_SDK_FACE_CONTOUR_EXPIRED    40021	//脸部识别失效

#define CFG_RECORDER_RTMP_MP4 0x02000008

#define CFG_RECORDER_MP4_PATH 0x02000009

typedef enum
{
    LIVECHATACCESS_ANCHOR = 0,
    LIVECHATACCESS_AUDIENCE
    
}LIVECHATACCESSTYPE;

typedef enum
{
    LIVECHAT_ARC_TYPE_NONE = 0,
    LIVECHAT_ARC_TYPE_ARC,
    LIVECHAT_ARC_TYPE_ARC_EX,
    LIVECHAT_ARC_TYPE_AGORA
}LIVECHATARCTYPE;


typedef enum {
    LIVECHATINFO_NONE = 0,
    LIVECHATINFO_SESSION_CREATED,
    LIVECHATINFO_SESSION_JOINED,
    LIVECHATINFO_SESSION_LEFT,
    LIVECHATINFO_SESSION_END,
    LIVECHATINFO_STARTED,
    LIVECHATINFO_STOP
    
}LIVECHATINFOTYPE;

typedef enum {
    LIVECHATERROR_UNKNOWN = 100,
    LIVECHATERROR_CREATE_LIVECHAT_FAILED,
    LIVECHATERROR_JOIN_LIVECHAT_FAILED,
    LIVECHATERROR_KICKOFF_LIVECHAT_FAILED,
    LIVECHATERROR_INIT_ENCODE_FAILED,
    LIVECHATERROR_INIT_DECODE_FAILED,
    LIVECHATERROR_INIT_SOCKET_FAILED,
    LIVECHATERROR_ENCODE_FAILED,
    LIVECHATERROR_DECODE_FAILED,
    LIVECHATERROR_SOCKET_TIMEOUT,
    LIVECHATERROR_SOCKET_FAILED
    
}LIVECHATERRORTYPE;

// struct
typedef struct _mv3_clip_info {
	unsigned long 	lFileType;			// the clip file format
	unsigned long 	lDuration;			// the playback duration of clip in ms
	unsigned long 	lWidth;			// the width of picture of clip , 0 if audio only
	unsigned long 	lHeight;			// the height of picture of clip , 0 if audio only
	unsigned long 	lBitrate;			// the average data bitrate of clip in bit
	bool	        bHasAudio;			// has audio in this clip?
	bool 	        bHasVideo;			// has video in this clip?
}CLIPINFO , *LPCLIPINFO;

typedef struct _mv3_audio_info {
	unsigned long 	lAudioType;		// the audio codec type
	unsigned long	lChannel;			// the number of channels of aduio , 1 for mono , 2 for stereo
	unsigned long	lBitsPerSample;		// the bit depth of audio , 8 or 16
	unsigned long	lBlockAlign;			// the block alignment is the minimum atomic unit of data for the audio codec  type(unit is byte)
	unsigned long  lSamplingRate;	// the sampling rate of audio
	unsigned long  lBitrate;			// the bitrate of audio  track in bit
}AUDIOINFO ,  *LPAUDIOINFO;

typedef struct _mv3_video_info {
	unsigned long	lVideoType;		// the video codec type
	unsigned long	lPicWidth;			// the picture width
	unsigned long   lPicHeight;		// the picture height
	float           fFPS;				// the FPS by average of video track
	unsigned long   lBitrate;			// the bitrate of video track in bit
	unsigned long	lRotationDegree;	// 0, 90, 180, 270.
}VIDEOINFO,*LPVIDEOINFO;

typedef struct _mv3_recorder_callback_data {
	unsigned long	lStatus;				//recorder status
	int				resStatus;				// // the result code of current status, for example , if unexcepect error occurs , the callback will be called with the STOP status and an error code
	unsigned long	lRecordedTime;		//recorded time
	unsigned long	lTotalEstimatedTime;	//The total estimatedTime
	unsigned long	lRecordedSize;			// the current recorded data size
	unsigned char	*pRecordedData;			// the recorded data buffer
}RECORDERCBDATA, *LPRECORDERCBDATA;

typedef struct _tag_publish_specfic_info {
    unsigned char*	pInfoBuf;		    //buf for information
    signed int      lBufSize;			//buf size
}MV2PUBLISHSPECFICINFO,*LPMV2PUBLISHSPECFICINFO;

typedef void  (*PFNRECORDERCALLBACK) ( LPRECORDERCBDATA lpRecorderData , void* lUserData);
typedef void  (*PFNWRITERVIDEOCALLBACK) (unsigned char * pFrameBuf, signed int lBufSize ,
                                                unsigned int * dwTimeStamp ,signed int *  bKeyFrame, long errcode,void *pObj);
typedef void  (*PFNINITRTMPCALLBACK) (LPMV2PUBLISHSPECFICINFO specData, void *pObj);
typedef void  (*PFNPUBLISHSTATECALLBACK) (void *pInstance, int res, int code, void *pObj);

typedef void  (* PFNLIVECHATINFOCALLBACK)(int infocode, void* subcode, void *pObj);
typedef void  (* PFNLIVECHATERRORCALLBACK)(int errorcode, unsigned long long subcode, void *pObj);
typedef void  (* PFNRENDERRESULTCALLBACK)(int errorcode, unsigned int* texture, void *pObj);

typedef void  (* LIVECHATMESSAGECALLBACK)(unsigned long long userid, unsigned int type, void *pObj);
typedef void  (* LIVECHATMESSAGEERRORCALLBACK)(unsigned long long userid, unsigned int type, void *pObj);
typedef void  (* LIVECHATMESSAGEFORMAPCALLBACK)(char* userid, unsigned long long mapuserid, unsigned int token, const char* sessionid, unsigned int type, const char* address, void *pObj);

#endif //__MPUBLISHTYPES__H__
