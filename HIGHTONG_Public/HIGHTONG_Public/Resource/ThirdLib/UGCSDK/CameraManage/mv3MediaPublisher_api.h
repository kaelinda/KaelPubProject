/*----------------------------------------------------------------------------------------------
*
* This file is Arcvideo's property. It contains Arcvideo's trade secret, proprietary and 		
* confidential information. 
* 
* The information and code contained in this file is only for authorized Arcvideo employees 
* to design, create, modify, or review.
* 
* DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
* 
* If you are not an intended recipient of this file, you must not copy, distribute, modify, 
* or take any action in reliance on it. 
* 
* If you have received this file in error, please immediately notify Arcvideo and 
* permanently delete the original and any copy of any file and any printout thereof.
*
*-------------------------------------------------------------------------------------------------*/

/*
 *	mv3MediaPublisher_api.h
 */

#ifndef __MV3MEDIAPUBLISHER_API__H__
#define __MV3MEDIAPUBLISHER_API__H__

#include "mpublishTypes.h"

#ifdef __WIN32__
#define PUBLISHER_EXPORTS
#endif

#ifdef  PUBLISHER_EXPORTS
#define PUBLISHER_API __declspec(dllexport)
#else
#define PUBLISHER_API 
#endif


#ifdef __cplusplus
extern "C" {
#endif

	int PUBLISHER_API MediaPublisher_CreateInstance(void **ppInstance,char *pIniFilePath);
	int PUBLISHER_API MediaPublisher_ReleaseInstance(void *pInstance);
	int PUBLISHER_API MediaPublisher_SetClipInfo(void *pInstance, LPCLIPINFO  lpClipInfo);
	int PUBLISHER_API MediaPublisher_SetAudioInfo(void *pInstance, LPAUDIOINFO lpAudioInfo);
    int PUBLISHER_API MediaPublisher_SetVideoInfo(void *pInstance, LPVIDEOINFO lpVideoInfo);
 	int PUBLISHER_API MediaPublisher_Start(void *pInstance,char *pServerUrl);
	int PUBLISHER_API MediaPublisher_Stop(void *pInstance);
    int PUBLISHER_API MediaPublisher_Pause(void *pInstance);
    int PUBLISHER_API MediaPublisher_Resume(void *pInstance);
    int PUBLISHER_API MediaPublisher_OpenAutoConnect(void *pInstance);
    int PUBLISHER_API MediaPublisher_ForceReConnect(void *pInstance);
    int PUBLISHER_API MediaPublisher_ResetContent(void *pInstance);
    int PUBLISHER_API MediaPublisher_SetRecorderVideoFrameState(void *pInstance, bool state);
    int PUBLISHER_API MediaPublisher_SetRecorderAudioFrameState(void *pInstance, bool state);
	int PUBLISHER_API MediaPublisher_SetConfig(void *pInstance, unsigned long lCfgType , void * pValue);
	int PUBLISHER_API MediaPublisher_GetConfig(void *pInstance, unsigned long lCfgType , void * pValue);
    void PUBLISHER_API MediaPublisher_RegisterPublishStateCallback (void *pInstance, PFNPUBLISHSTATECALLBACK pCallback, void *pObj );
    
    void PUBLISHER_API MediaPublisher_SetTracePath(void *pInstance, const char* filePath);
#ifdef __cplusplus
}
#endif

#endif //__MV3MediaPublisher_API__H__