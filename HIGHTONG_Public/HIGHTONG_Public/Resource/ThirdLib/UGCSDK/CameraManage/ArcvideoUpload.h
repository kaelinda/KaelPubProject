//
//  ArcvideoUpload.h
//  ArcvideoUpload
//
//  Created by allenyang1986 on 2017/5/27.
//  Copyright © 2017年 Allen Yang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArcvideoUpload : NSObject

- (instancetype)initWithAccessKey: (NSString*) accessKey
                     AccessSecret: (NSString*) accessSecret;

- (void)startUpload: (NSString*) filePath;

- (void)stopUpload;

@end

extern NSString* const ArcvideoUploadNotificationMessage;

extern NSString* const ArcvideoUploadNotificationUpdateProcess;

extern NSString* const ArcvideoUploadNotificationKeyErrorCode;

extern NSString* const ArcvideoUploadNotificationKeyErrorDescription;

extern NSString* const ArcvideoUploadNotificationKeyProcess;

extern NSString* const ArcvideoUploadNotificationKeyUploadRate;
