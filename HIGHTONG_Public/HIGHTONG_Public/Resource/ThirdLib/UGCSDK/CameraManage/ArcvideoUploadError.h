//
//  ArcvideoUploadError.h
//  ArcvideoUpload
//
//  Created by allenyang1986 on 2017/5/27.
//  Copyright © 2017年 Allen Yang. All rights reserved.
//

#ifndef ArcvideoUploadError_h
#define ArcvideoUploadError_h


#define ERR_UPLOAD_NONE				0
#define ERR_UPLOAD_UNKNOW           1
#define ERR_UPLOAD_INVALID_PARAM	2
#define ERR_UPLOAD_NOT_ENOUGH_MEM   3


#define ERR_UPLOAD_THREAD_CREATE	101
#define ERR_UPLOAD_NETWORK			102
#define ERR_UPLOAD_FILE_NO_EXITS	103
#define ERR_UPLOAD_FILE_OPERATION   104     //file operation fail
#define ERR_UPLOAD_AUTHENTICATION	105		//authentication
#define ERR_UPLOAD_FAIL				106		//upload fail
#define	ERR_UPLOAD_DELETE_TASK_FAIL	107
#define ERR_UPLOAD_MD5_FAIL         108     //file operation fail
#define ERR_UPLOAD_MERGE_CHUNK      109     //

#define INFO_UPLOAD_SUCCESS          202
#define INFO_UPLOAD_NORMAL_STOP      203      //normal


#endif /* ArcvideoUploadError_h */
