//
//  CameraManage.h
//  CameraRecoder
//
//  Created by li fu on 16/2/29.
//  Copyright © 2016年 li fu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "mpublishTypes.h"

typedef enum
{
    CameraPosition_front = 0,
    CameraPosition_back
}CameraPosition;

typedef enum
{
    CameraDisplay_fillIn = 0,
    CameraDisplay_fillOut,
    CameraDisplay_stretch
}CameraDisplayModel;

typedef enum
{
    ACCESS_ANCHOR = 0,          // 主播
    ACCESS_AUDIENCE             // 观众
}ACCESSTYPE;

typedef void (^callbackBlock)(bool authorize);

// open camera error
#define CAMERA_INIT_SUCCESS             0       // 初始化成功
#define CAMERA_INIT_FAILED              -1      // 初始化失败
#define CAMERA_NOT_SUPPORT_RESOLUTION   -2      // 不支持此分辨率
#define CAMERARECORD_SDK_EXPIRED        -100    // sdk过期

#define CAMERARECORD_LICENSE_ERR                                    0x0000
#define CAMERARECORD_LICENSE_ERR_DISABLE_APP_NAME					(LICENSE_ERR+1)
#define CAMERARECORD_LICENSE_ERR_DISABLE_AUTHENTICATE_FAIL			(LICENSE_ERR+2)
#define CAMERARECORD_LICENSE_ERR_DISABLE_INVALID_PARAM				(LICENSE_ERR+3)
#define CAMERARECORD_LICENSE_ERR_DISABLE_PARAMETER_DISACCORD		(LICENSE_ERR+4)
#define CAMERARECORD_LICENSE_ERR_DISABLE_DIRECTORY_ERR				(LICENSE_ERR+5)
#define CAMERARECORD_LICENSE_ERR_DISABLE_MEM_NOT_ENOUGH				(LICENSE_ERR+6)
#define CAMERARECORD_LICENSE_ERR_DISABLE_SDK_PLANTFORM_NO_SUPPORT	(LICENSE_ERR+7)
#define CAMERARECORD_LICENSE_ERR_DISABLE_SDK_NO_EXITS				(LICENSE_ERR+8)
#define CAMERARECORD_LICENSE_ERR_DISABLE_NO_UPDATE					(LICENSE_ERR+11)
#define CAMERARECORD_LICENSE_ERR_DISABLE_NETWORK					(LICENSE_ERR+12)
#define CAMERARECORD_LICENSE_ERR_DISABLE_DOWNLOAD_DATA_FORMAT		(LICENSE_ERR+13)
#define CAMERARECORD_LICENSE_ERR_ENABLE_NO_UPDATE					(LICENSE_ERR+21)
#define CAMERARECORD_LICENSE_ERR_ENABLE_NETWORK						(LICENSE_ERR+22)
#define CAMERARECORD_LICENSE_ERR_ENABLE_DOWNLOAD_DATA_FORMAT		(LICENSE_ERR+23)
#define CAMERARECORD_LICENSE_ERR_ENABLE_SDK_EXPIREDATE				(LICENSE_ERR+31)

// end

@interface CameraManage : NSObject
{
    //
}

/**
 *  Create a camera
 *
 *  @param paramDic Dictionarys of the camera's,contains frameRate and videoSize and cameraPosition
 *
 *  @return return code (success or camera error)
 */
+ (int) openCamera:(NSDictionary *) paramDic callbackBlock:(callbackBlock) callback;

/**
 *  Apply the tape permissions
 *
 *  @param callback Get state of Permission
 *
 *  @return none
 */
+ (void) getRecordPermissionForAudio:(callbackBlock) callback;

/**
 *  close the camera
 *
 *  @return return true while close success
 */
+ (BOOL) closeCamera;

/**
 *  set the position of the camera
 *
 *  @param state The position of the camera's,contains front and back
 *
 *  @return return true while set success
 */
+ (BOOL) setCameraState:(CameraPosition) state;

/**
 *  set the torch state of the camera
 *
 *  @param state The state of the camera's torch
 *
 *  @return return true while set success
 */
+ (BOOL) setTorchState:(BOOL) state;

/**
 *  set the display Model of the camera
 *
 *  @param model The model of the camera's display in the preview
 *
 *  @return return true while set success
 */
+ (BOOL) setCameraDisplayModel:(CameraDisplayModel) model;

/**
 *  oepn the preview of the camera
 *
 *  @param preview The camera's preview
 *
 */
+ (void) openPreview:(UIView *) preview;

/**
 *  close the preview of the camera
 *
 */
+ (void) closePreview;

/**
 *  lock the orientation of the camera when start recorder
 *
 */
+ (void) lockOrientation;

/**
 *  unlock the orientation of the camera when stop recorder
 *
 */
+ (void) unlockOrientation;

/**
 *  check the preset of the camera Whether or not be supported
 *
 *  @param preset the param of the camera, you can find from the document
 *  @param position the camera state (front, back)
 *
 *  @return return true while support success, or false
 */
+ (BOOL) checkSupportPreset:(NSString *) preset position:(CameraPosition) state;

/**
 *  update the size when the orientation changed
 *
 *  @param preset the param of the camera, you can find from the document
 *
 *  @return return new size
 */
+ (CGSize) updateSizeWithPreset:(NSString *) preset;

/**
 *  get the camera support's frameRate(min - max)
 *
 *  @param minNum the out data frame camera(frameRate)
 *  @param maxNum the out data frame camera(frameRate)
 *
 *  @return return true while support success, or false
 */
+ (BOOL) getFrameRate:(double *) minNum maxNum:(double *) maxNum;

/**
 *  get the reference bitrates with width and height
 *
 *  @param width The width of selected preset
 *  @param height The height of selected preset
 *
 *  @return return array
 */
+ (NSArray *) getReferenceBitratesWithWidth:(double) width height:(double) height;

/**
 *  Has support for resolution of the equipment
 *
 *  @param position the camera state (front, back)
 *
 *  @return return array
 */
+ (NSArray *) getSupportPresetsWithPosition:(CameraPosition) state;

/**
 *  set the ruddy value of shader
 *
 *  @param value the value of ruddy (0 - 100)
 *
 *  @return return none
 */
+ (void) setRuddyLevel:(float) level;

/**
 *  enable or disable all filters
 *
 *  @param state enable or disable
 *
 *  @return return none
 */
+ (void) enableFilter:(BOOL) state;

/**
 *  add shader filter with filterType (mpublishTypes.h)
 *
 *  @param filterType the shader filter type (mpublishTypes.h)
 *
 *  @return return sucess or error code
 */
+ (int) addFilter:(int) filterType;

/**
 *  remove shader filter with filterType (mpublishTypes.h)
 *
 *  @param filterType the shader filter type (mpublishTypes.h)
 *
 *  @return return none
 */
+ (void) removeFilter:(int) filterType;

/**
 *  add shader filter (the pluginFilter Be sure to GPUImageFIlter inheritance)
 *
 *  @param pluginFilter the GPUImageFIlter subclass
 *
 *  @return return sucess or fail
 */
+ (BOOL) addPluginFilter:(id) pluginFilter;

/**
 *  remove shader filter (the pluginFilter Be sure to GPUImageFIlter inheritance)
 *
 *  @param filterType the GPUImageFIlter subclass
 *
 *  @return return none
 */
+ (void) removePluginFilter:(id) pluginFilter;

/**
 *  set the value of the saturation
 *
 *  @param level the saturation value (0 - 100)
 *
 *  @return return none
 */
+ (void) setSaturationLevel:(float) level;

/**
 *  set the contrast value
 *
 *  @param value the value of contrast (0 - 100)
 *
 *  @return none
 */
+ (void) setContrastLevel:(float) value;

/**
 *  set the exposure value of shader
 *
 *  @param value the value of exposure (0 - 100)
 *
 *  @return none
 */
+ (void) setExposureOfShaderLevel:(float) value;

/**
 *  set the brightness value of shader
 *
 *  @param value the value of brightness (0 - 100)
 *
 *  @return none
 */
+ (void) setBrightnessLevel:(float) value;

/**
 *  set the temperature value of shader
 *
 *  @param value the value of temperature (0 - 100)
 *
 *  @return none
 */
+ (void) setTemperatureLevel:(float) value;

/**
 *  set the tint value of shader
 *
 *  @param value the value of tint (0 - 100)
 *
 *  @return none
 */
+ (void) setTintLevel:(float) value;

/**
 *  set the sharpen value of shader
 *
 *  @param value the value of sharpen (0 - 100)
 *
 *  @return none
 */
+ (void) setSharpenLevel:(float) value;

/**
 *  set the red channel of colorlevel value
 *
 *  @param value the value of red (0 - 100)
 *
 *  @return none
 */
+ (void) setRedColorLevel:(float) value;

/**
 *  set the green channel of colorlevel value
 *
 *  @param value the value of green (0 - 100)
 *
 *  @return none
 */
+ (void) setGreenColorLevel:(float) value;

/**
 *  set the blue channel of colorlevel value
 *
 *  @param value the value of blue (0 - 100)
 *
 *  @return none
 */
+ (void) setBlueColorLevel:(float) value;

/**
 *  set the Level value of colorlevel value
 *
 *  @param value the value of colorlevel with red, green, blue (0 - 100)
 *
 *  @return none
 */
+ (void) setColorLevelAdjust:(float) value;

/**
 *  set the eye scale of face
 *
 *  @param value the value of eye (0 - 100)
 *
 *  @return sucess or error code
 */
+ (int) setEyeScale:(float) value;

/**
 *  set the face scale of face
 *
 *  @param value the value of face (0 - 100)
 *
 *  @return sucess or error code
 */
+ (int) setFaceScale:(float) value;

/**
 *  set the sharpen value of shader
 *
 *  @param value enable or disable
 *
 *  @return sucess or error code
 */
+ (int) enableSticker: (BOOL) state;

/**
 *  switch the sticker with index
 *
 *  @param index the value of stickers [0 ~ (stickers's count - 1)]
 *
 *  @return none
 */
+ (void) switchSticker: (int) index;

/**
 *  enable or disable the Mirrore of Video
 *
 *  @param state enable or disable
 *  @param position the camera state (front, back)
 *
 *  @return return none
 */
+ (void) enableVideoMirrored:(BOOL) state position:(CameraPosition) position;

/**
 *  set face skin soften level for beauty
 *
 *  @param level the kin soften level
 *
 *  @return return none
 */
+ (void) setFaceSkinSoftenLevel:(unsigned int) level;

/**
 *  set face bright level for beauty
 *
 *  @param level the bright level
 *
 *  @return return none
 */
+ (void) setFaceBrightLevel:(unsigned int) level;

/**
 *  set the exposure level
 *
 *  @param level the exposure value of camera (1 - 100)
 *
 *  @return return success or failure
 */
+ (BOOL) setExposureLevel:(float) level;

/**
 *  open or close the beauty process
 *
 *  @param state the state of beauty
 *
 *  @return return sucess or error code
 */
+ (int) setBeautyState:(BOOL) state;

/**
 *  open or close the face recognize process
 *
 *  @param state the state of face recognize
 *
 *  @return return sucess or error code
 */
+ (int) setFaceRecognizeState:(BOOL) state;

/**
 *  click the point of view to focus
 *
 *  @param point the position of view, that you want focus
 *
 *  @return return none
 */
+ (void) autoFocusAtPoint:(CGPoint) point;

/**
 *  zoom the video preview with scale
 *
 *  @param scale the value of pich zoom
 *
 *  @return return none
 */
+ (void) pichZoomWithScale:(CGFloat) scale;

/**
 *  add image watermark to the preview of camera
 *
 *  @param imageName the image of watermark
 *  @param rect the value of watemark(rect.x:for the x axis offset to the preview，rect.y:for the y axis offset to the preview, *rect must be contained in preset)
 *
 *  @return return none
 */
+ (void) addWatermarkWithImageName:(NSString *) imageName info:(CGRect) rect;

/**
 *  set the information of liveChat
 *
 *  @param dic contain (appid, channelkey, channelname, uid)
 *
 *  @return return none
 */
+ (void) setUserInfo:(NSDictionary *) dic;

/**
 *  create liveChat
 *
 *  @param sessionid Live session ID
 *  @param userid The user's ID of LiveSession
 *
 *  @return return result (0:success, !0:faile)
 */
+ (int) createLiveChatWithSessionID:(NSString *) sessionid userID:(NSString *) userid accessType:(ACCESSTYPE) type accessKey:(NSString *) accesskey secretKey:(NSString *) secretkey;

/**
 *  join in liveChat
 *
 *  @param sessionid Live session ID
 *  @param userid The user's ID of LiveSession
 *
 *  @return return result (0:success, !0:faile)
 */
+ (int) joinLiveChatWithSessionID:(NSString *) sessionid liveChatID:(NSString *) livechatid userID:(NSString *) userid accessType:(ACCESSTYPE) type accessKey:(NSString *) accesskey secretKey:(NSString *) secretkey;

/**
 *  kickoff member from liveChat
 *
 *  @param userid The user's ID of LiveSession
 *
 *  @return return result (0:success, !0:faile)
 */
+ (int) kickoffMemberFromLiveChatWithUserID:(NSString *) userid;

/**
 *  start liveChat
 *
 *  @param none
 *
 *  @return return none
 */
+ (void) startLiveChat;

/**
 *  stop liveChat
 *
 *  @param none
 *
 *  @return return none
 */
+ (void) stopLiveChat;

/**
 *  set relative to the recording resolution of the audience area
 *
 *  @param rect Position relative to the recording resolution audience
 *  @param userid The user id assigned in advance
 *
 *  @return return none
 */
+ (void) setLiveChatPosition:(CGRect) rect userID:(NSString *) userid accessType:(ACCESSTYPE) type;

/**
 *  set the camera preview's rect
 *
 *  @param rect The camera's preview
 *
 *  @return return none
 */
+ (void) setLiveChatCameraRect:(CGRect) rect anchorPreset:(NSString *) size;

/**
 *  get liveChat info result
 *
 *  @param callback Get info of the liveChat
 *  @param pObj The caller
 *
 *  @return return none
 */
+ (void) getLiveChatInfo:(PFNLIVECHATINFOCALLBACK) callback target:(void *) pObj;

/**
 *  get liveChat info result
 *
 *  @param callback Get info of the liveChat
 *  @param pObj The caller
 *
 *  @return return none
 */
+ (void) getLiveChatError:(PFNLIVECHATERRORCALLBACK) callback target:(void *) pObj;

/**
 *  when into the background, suspend liveChat
 *
 *  @return return none
 */
+ (void) pause;

/**
 *  when into the foreground, resume liveChat
 *
 *  @return return none
 */
+ (void) resume;

/**
 *  get render result (draw preview)
 *
 *  @param callback Get textureid of render
 *  @param pObj The caller
 *
 *  @return return none
 */
+ (void) getRenderResult:(PFNRENDERRESULTCALLBACK) callback target:(void *) pObj;

/**
 *  enable or disable black frame
 *
 *  @param state The black frame status value
 *
 *  @return return none
 */
+ (void) enableBlackFrame:(BOOL) state;

@end
